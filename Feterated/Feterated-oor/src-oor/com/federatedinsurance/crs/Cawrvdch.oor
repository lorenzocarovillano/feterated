package com.federatedinsurance.crs;

import java.lang.Override;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.federatedinsurance.crs.ws.CawrvdchData;
import com.federatedinsurance.crs.ws.LCawlcvdc;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;

/**Original name: CAWRVDCH<br>
 * <pre>****************************************************************
 * * PROGRAM TITLE - EDIT NAMES/ADDRESSES FOR INVALID CHARACTERS **
 * *                                                             **
 * * WRITTEN BY:  FEDERATED MUTUAL INS. CO.                      **
 * *                                                             **
 * * DATE WRITTEN: MARCH 2003                                    **
 * *                                                             **
 * * PLATFORM - HOST                                             **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - MVS COBOL                                        **
 * *                                                             **
 * * PURPOSE - A UTILITY PROGRAM USED TO CHECK FOR INVALID       **
 * *           CHARACTERS IN THE DATA PASSED FROM A BDO.         **
 * *                                                             **
 * * PROGRAM INITIATION -  A DYNAMICALLY CALLED SUB-ROUTINE      **
 * *                       INVOKED BY A CALLING PROGRAM.         **
 * *                                                             **
 * * DATA ACCESS METHODS - INPUT                                 **
 * *                                                             **
 * --------------------------------------------------------------**
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #       DATE     PROG                DESCRIPTION         **
 * * -------- --------   ------- --------------------------------**
 * * F66398   MAR 2003   E404ASW INITIAL PROGRAM                 **
 * * F69088   08/12/2004 E404JJM ADDED '<' AND '>' AND NEW       **
 * *                             INVALID CHARACTERS              **
 * * 11397    09/14/2016 E404ABL CHANGE INVALID CHARACTER LIST   **
 * *                             TO ACCOMMODATE CRM.             **
 * *                                                             **
 * ****************************************************************</pre>*/
class Cawrvdch extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private CawrvdchData ws := new CawrvdchData();
	//Original name: L-CAWLCVDC
	private LCawlcvdc lCawlcvdc;


	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LCawlcvdc lCawlcvdc) {
		this.lCawlcvdc := lCawlcvdc;
		mainProgram();
		mainProgramX();
		return 0;
	}

	public static Cawrvdch getInstance() {
		return (Cawrvdch)Programs.getInstance(Cawrvdch.clazz);
	}

	/**Original name: 1000-MAIN-PROGRAM<br>*/
	private void mainProgram() {
		// COB_CODE: PERFORM 2000-INITIALIZE
		//              THRU 2000-EXIT.
		initialize();
		// COB_CODE: PERFORM 3000-PERFORM-EDIT
		//              THRU 3000-EXIT.
		performEdit();
		// COB_CODE: MOVE SW-INVALID-CHAR-FOUND-SWITCH
		//                                       TO CWVDC-INVALID-CHAR-IND.
		lCawlcvdc.getInvalidCharInd().setInvalidCharInd(ws.getSwInvalidCharFoundSwitch().getSwInvalidCharFoundSwitch());
	}

	/**Original name: 1000-MAIN-PROGRAM-X<br>*/
	private void mainProgramX() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 2000-INITIALIZE<br>
	 * <pre>***************************************************************
	 * *  INITIALIZE WORKING STORAGE VARIABLES                      **
	 * ***************************************************************</pre>*/
	private void initialize() {
		// COB_CODE: MOVE SPACES                 TO SA-CHECK-STRING-GRP.
		ws.getSaCheckStringGrp().setSaCheckStringGrp("");
		// COB_CODE: MOVE LENGTH OF CWVDC-EDIT-FIELD
		//                                       TO SA-SEARCH-STR-LENGTH.
		ws.setSaSearchStrLength((short)LCawlcvdc.Len.EDIT_FIELD);
		// COB_CODE: SET SW-INVALID-CHAR-NOT-FOUND
		//                                       TO TRUE.
		ws.getSwInvalidCharFoundSwitch().setNotFound();
	}

	/**Original name: 3000-PERFORM-EDIT<br>
	 * <pre>***************************************************************
	 * *  INITIALIZE INDEX AND PERFORM SEARCH FOR BAD CHARACTERS    **
	 * ***************************************************************</pre>*/
	private void performEdit() {
		// COB_CODE: MOVE CWVDC-EDIT-FIELD       TO SA-CHECK-STRING-GRP.
		ws.getSaCheckStringGrp().setSaCheckStringGrp(lCawlcvdc.getEditField());
		// COB_CODE: SET IX-CS                   TO +1.
		ws.setIxCs(1);
// COB_CODE: PERFORM 3100-CHECK-BYTE
//              THRU 3100-EXIT
//               VARYING IX-CS FROM 1 BY 1
//                 UNTIL IX-CS > SA-SEARCH-STR-LENGTH
//                    OR SW-INVALID-CHAR-FOUND.
		ws.setIxCs(1);
		do
		while (! (ws.getIxCs() > ws.getSaSearchStrLength() | ws.getSwInvalidCharFoundSwitch().isFound()))
			checkByte();
			ws.setIxCs(Trunc.toInt(ws.getIxCs() + 1, 9));
		enddo
	}

	/**Original name: 3100-CHECK-BYTE<br>
	 * <pre>***************************************************************
	 * *  CHECK EACH INDIVIDUAL BYTE TO SEE IF IT IS AN INVALID     **
	 * *  CHARACTER                                                 **
	 * ***************************************************************</pre>*/
	private void checkByte() {
		// COB_CODE: MOVE SA-CHECK-STRING-CHAR(IX-CS)
		//                                       TO SA-CHECK-CHARACTER.
		ws.setSaCheckCharacter(ws.getSaCheckStringGrp().getCharFld(ws.getIxCs()));
		// COB_CODE: IF SA-INVALID-CHARACTER
		//                                       TO TRUE
		//           END-IF.
		if (ws.isSaInvalidCharacter()) then
			// COB_CODE: SET SW-INVALID-CHAR-FOUND
			//                                   TO TRUE
			ws.getSwInvalidCharFoundSwitch().setFound();
		endif
	}

}//Cawrvdch