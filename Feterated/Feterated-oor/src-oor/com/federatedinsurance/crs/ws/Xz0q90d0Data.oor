package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0Q90D0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0q90d0Data {

	//==== PROPERTIES ====
	//Original name: CF-BUS-OBJ-NM-UPDATE-BILLLING
	private string cfBusObjNmUpdateBillling := "XZ_NOT_BIL_OF_POL_CAN_DTS";
	//Original name: FILLER-CF-INVALID-OPERATION
	private string flr1 := "REQUEST MODULE";
	//Original name: FILLER-CF-INVALID-OPERATION-1
	private string flr2 := "- INVALID";
	//Original name: FILLER-CF-INVALID-OPERATION-2
	private string flr3 := "OPERATION";
	//Original name: FILLER-EA-01-INVALID-OPERATION-MSG
	private string flr4 := "XZ0Q90D0 -";
	//Original name: FILLER-EA-01-INVALID-OPERATION-MSG-1
	private string flr5 := "INVALID OPER:";
	//Original name: EA-01-INVALID-OPERATION-NAME
	private string ea01InvalidOperationName := DefaultValues.stringVal(Len.EA01_INVALID_OPERATION_NAME);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0q90d0 workingStorageArea := new WorkingStorageAreaXz0q90d0();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage := new WsHalrurqaLinkage();
	//Original name: HALLUSW
	private Hallusw hallusw := new Hallusw();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc := new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom := new Hallcom();
	//Original name: WS-APPLID
	private string wsApplid := DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw := new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo := new WsEstoInfo();


	//==== METHODS ====
	public string getCfBusObjNmUpdateBillling() {
		return this.cfBusObjNmUpdateBillling;
	}

	public string getCfInvalidOperationFormatted() {
		return MarshalByteExt.bufferToStr(getCfInvalidOperationBytes());
	}

	/**Original name: CF-INVALID-OPERATION<br>*/
	public []byte getCfInvalidOperationBytes() {
		[]byte buffer := new [Len.CF_INVALID_OPERATION]byte;
		return getCfInvalidOperationBytes(buffer, 1);
	}

	public []byte getCfInvalidOperationBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public string getFlr3() {
		return this.flr3;
	}

	public string getEa01InvalidOperationMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01InvalidOperationMsgBytes());
	}

	/**Original name: EA-01-INVALID-OPERATION-MSG<br>*/
	public []byte getEa01InvalidOperationMsgBytes() {
		[]byte buffer := new [Len.EA01_INVALID_OPERATION_MSG]byte;
		return getEa01InvalidOperationMsgBytes(buffer, 1);
	}

	public []byte getEa01InvalidOperationMsgBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position +:= Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position +:= Len.FLR5;
		MarshalByte.writeString(buffer, position, ea01InvalidOperationName, Len.EA01_INVALID_OPERATION_NAME);
		return buffer;
	}

	public string getFlr4() {
		return this.flr4;
	}

	public string getFlr5() {
		return this.flr5;
	}

	public void setEa01InvalidOperationName(string ea01InvalidOperationName) {
		this.ea01InvalidOperationName:=Functions.subString(ea01InvalidOperationName, Len.EA01_INVALID_OPERATION_NAME);
	}

	public string getEa01InvalidOperationName() {
		return this.ea01InvalidOperationName;
	}

	/**Original name: WS-USW-MSG<br>*/
	public []byte getWsUswMsgBytes() {
		[]byte buffer := new [Len.WS_USW_MSG]byte;
		return getWsUswMsgBytes(buffer, 1);
	}

	public []byte getWsUswMsgBytes([]byte buffer, integer offset) {
		integer position := offset;
		hallusw.getCommonBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(string wsApplid) {
		this.wsApplid:=Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public string getWsApplid() {
		return this.wsApplid;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	public WorkingStorageAreaXz0q90d0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer EA01_INVALID_OPERATION_NAME := 32;
		public final static integer WS_SE3_CUR_ISO_DATE := 10;
		public final static integer WS_SE3_CUR_ISO_TIME := 16;
		public final static integer WS_APPLID := 8;
		public final static integer WS_USW_MSG := Hallusw.Len.COMMON;
		public final static integer FLR1 := 15;
		public final static integer FLR2 := 10;
		public final static integer FLR3 := 9;
		public final static integer CF_INVALID_OPERATION := FLR1 + FLR2 + FLR3;
		public final static integer FLR4 := 11;
		public final static integer FLR5 := 14;
		public final static integer EA01_INVALID_OPERATION_MSG := EA01_INVALID_OPERATION_NAME + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xz0q90d0Data