package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.copy.Xzc002ActNotPolData;
import com.federatedinsurance.crs.copy.Xzc002ActNotPolFixed;
import com.federatedinsurance.crs.copy.Xzc002ActNotPolKey;
import com.federatedinsurance.crs.copy.Xzc002ActNotPolKeyCi;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0C0002-LAYOUT<br>
 * Variable: WS-XZ0C0002-LAYOUT from program XZ0F0002<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0c0002Layout extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZC002-ACT-NOT-POL-FIXED
	private Xzc002ActNotPolFixed actNotPolFixed := new Xzc002ActNotPolFixed();
	//Original name: XZC002-TRANS-PROCESS-DT
	private string transProcessDt := DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC002-ACT-NOT-POL-KEY
	private Xzc002ActNotPolKey actNotPolKey := new Xzc002ActNotPolKey();
	//Original name: XZC002-ACT-NOT-POL-KEY-CI
	private Xzc002ActNotPolKeyCi actNotPolKeyCi := new Xzc002ActNotPolKeyCi();
	//Original name: XZC002-ACT-NOT-POL-DATA
	private Xzc002ActNotPolData actNotPolData := new Xzc002ActNotPolData();
	//Original name: XZC002-POL-TYP-DES
	private string polTypDes := DefaultValues.stringVal(Len.POL_TYP_DES);


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WS_XZ0C0002_LAYOUT;
	}

	@Override
	public void deserialize([]byte buf) {
		setWsXz0c0002LayoutBytes(buf);
	}

	public void setWsXz0c0002LayoutFormatted(string data) {
		[]byte buffer := new [Len.WS_XZ0C0002_LAYOUT]byte;
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0C0002_LAYOUT);
		setWsXz0c0002LayoutBytes(buffer, 1);
	}

	public string getWsXz0c0002LayoutFormatted() {
		return getActNotPolRowFormatted();
	}

	public void setWsXz0c0002LayoutBytes([]byte buffer) {
		setWsXz0c0002LayoutBytes(buffer, 1);
	}

	public []byte getWsXz0c0002LayoutBytes() {
		[]byte buffer := new [Len.WS_XZ0C0002_LAYOUT]byte;
		return getWsXz0c0002LayoutBytes(buffer, 1);
	}

	public void setWsXz0c0002LayoutBytes([]byte buffer, integer offset) {
		integer position := offset;
		setActNotPolRowBytes(buffer, position);
	}

	public []byte getWsXz0c0002LayoutBytes([]byte buffer, integer offset) {
		integer position := offset;
		getActNotPolRowBytes(buffer, position);
		return buffer;
	}

	public string getActNotPolRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotPolRowBytes());
	}

	/**Original name: XZC002-ACT-NOT-POL-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0C0002 - ACT_NOT_POL TABLE                                   *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 30 Sep 2008 E404GRK   GENERATED                        *
	 *  TO07614 09 Mar 2009 E404DLP   CHANGED ADR-SEQ-NBR TO ADR-ID    *
	 *  20163.20 13 Jul 2018 E404DMW  UPDATED ADDRESS AND CLIENT IDS   *
	 *                                FROM 20 TO 64                    *
	 * *****************************************************************</pre>*/
	public []byte getActNotPolRowBytes() {
		[]byte buffer := new [Len.ACT_NOT_POL_ROW]byte;
		return getActNotPolRowBytes(buffer, 1);
	}

	public void setActNotPolRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		actNotPolFixed.setActNotPolFixedBytes(buffer, position);
		position +:= Xzc002ActNotPolFixed.Len.ACT_NOT_POL_FIXED;
		setActNotPolDatesBytes(buffer, position);
		position +:= Len.ACT_NOT_POL_DATES;
		actNotPolKey.setActNotPolKeyBytes(buffer, position);
		position +:= Xzc002ActNotPolKey.Len.ACT_NOT_POL_KEY;
		actNotPolKeyCi.setActNotPolKeyCiBytes(buffer, position);
		position +:= Xzc002ActNotPolKeyCi.Len.ACT_NOT_POL_KEY_CI;
		actNotPolData.setActNotPolDataBytes(buffer, position);
		position +:= Xzc002ActNotPolData.Len.ACT_NOT_POL_DATA;
		setExtensionFieldsBytes(buffer, position);
	}

	public []byte getActNotPolRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		actNotPolFixed.getActNotPolFixedBytes(buffer, position);
		position +:= Xzc002ActNotPolFixed.Len.ACT_NOT_POL_FIXED;
		getActNotPolDatesBytes(buffer, position);
		position +:= Len.ACT_NOT_POL_DATES;
		actNotPolKey.getActNotPolKeyBytes(buffer, position);
		position +:= Xzc002ActNotPolKey.Len.ACT_NOT_POL_KEY;
		actNotPolKeyCi.getActNotPolKeyCiBytes(buffer, position);
		position +:= Xzc002ActNotPolKeyCi.Len.ACT_NOT_POL_KEY_CI;
		actNotPolData.getActNotPolDataBytes(buffer, position);
		position +:= Xzc002ActNotPolData.Len.ACT_NOT_POL_DATA;
		getExtensionFieldsBytes(buffer, position);
		return buffer;
	}

	public void setActNotPolDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		transProcessDt := MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public []byte getActNotPolDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(string transProcessDt) {
		this.transProcessDt:=Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public string getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setExtensionFieldsBytes([]byte buffer, integer offset) {
		integer position := offset;
		polTypDes := MarshalByte.readString(buffer, position, Len.POL_TYP_DES);
	}

	public []byte getExtensionFieldsBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, polTypDes, Len.POL_TYP_DES);
		return buffer;
	}

	public void setPolTypDes(string polTypDes) {
		this.polTypDes:=Functions.subString(polTypDes, Len.POL_TYP_DES);
	}

	public string getPolTypDes() {
		return this.polTypDes;
	}

	public Xzc002ActNotPolData getActNotPolData() {
		return actNotPolData;
	}

	public Xzc002ActNotPolFixed getActNotPolFixed() {
		return actNotPolFixed;
	}

	public Xzc002ActNotPolKey getActNotPolKey() {
		return actNotPolKey;
	}

	public Xzc002ActNotPolKeyCi getActNotPolKeyCi() {
		return actNotPolKeyCi;
	}

	@Override
	public []byte serialize() {
		return getWsXz0c0002LayoutBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer TRANS_PROCESS_DT := 10;
		public final static integer ACT_NOT_POL_DATES := TRANS_PROCESS_DT;
		public final static integer POL_TYP_DES := 30;
		public final static integer EXTENSION_FIELDS := POL_TYP_DES;
		public final static integer ACT_NOT_POL_ROW := Xzc002ActNotPolFixed.Len.ACT_NOT_POL_FIXED + ACT_NOT_POL_DATES + Xzc002ActNotPolKey.Len.ACT_NOT_POL_KEY + Xzc002ActNotPolKeyCi.Len.ACT_NOT_POL_KEY_CI + Xzc002ActNotPolData.Len.ACT_NOT_POL_DATA + EXTENSION_FIELDS;
		public final static integer WS_XZ0C0002_LAYOUT := ACT_NOT_POL_ROW;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsXz0c0002Layout