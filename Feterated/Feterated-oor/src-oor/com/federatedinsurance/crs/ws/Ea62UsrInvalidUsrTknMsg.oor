package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-62-USR-INVALID-USR-TKN-MSG<br>
 * Variable: EA-62-USR-INVALID-USR-TKN-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea62UsrInvalidUsrTknMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-62-USR-INVALID-USR-TKN-MSG
	private string flr1 := "TS548099 -";
	//Original name: FILLER-EA-62-USR-INVALID-USR-TKN-MSG-1
	private string flr2 := "INVALID USER";
	//Original name: FILLER-EA-62-USR-INVALID-USR-TKN-MSG-2
	private string flr3 := "TOKEN PROVIDED.";
	//Original name: FILLER-EA-62-USR-INVALID-USR-TKN-MSG-3
	private string flr4 := "  (";
	//Original name: EA-62-USER-TOKEN
	private string ea62UserToken := DefaultValues.stringVal(Len.EA62_USER_TOKEN);
	//Original name: FILLER-EA-62-USR-INVALID-USR-TKN-MSG-4
	private char flr5 := ')';


	//==== METHODS ====
	public string getEa62UsrInvalidUsrTknMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa62UsrInvalidUsrTknMsgBytes());
	}

	public []byte getEa62UsrInvalidUsrTknMsgBytes() {
		[]byte buffer := new [Len.EA62_USR_INVALID_USR_TKN_MSG]byte;
		return getEa62UsrInvalidUsrTknMsgBytes(buffer, 1);
	}

	public []byte getEa62UsrInvalidUsrTknMsgBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position +:= Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position +:= Len.FLR4;
		MarshalByte.writeString(buffer, position, ea62UserToken, Len.EA62_USER_TOKEN);
		position +:= Len.EA62_USER_TOKEN;
		MarshalByte.writeChar(buffer, position, flr5);
		return buffer;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public string getFlr3() {
		return this.flr3;
	}

	public string getFlr4() {
		return this.flr4;
	}

	public void setEa62UserToken(long ea62UserToken) {
		this.ea62UserToken := NumericDisplay.asString(ea62UserToken, Len.EA62_USER_TOKEN);
	}

	public long getEa62UserToken() {
		return NumericDisplay.asLong(this.ea62UserToken);
	}

	public char getFlr5() {
		return this.flr5;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer EA62_USER_TOKEN := 10;
		public final static integer FLR1 := 11;
		public final static integer FLR2 := 13;
		public final static integer FLR3 := 15;
		public final static integer FLR4 := 3;
		public final static integer FLR5 := 1;
		public final static integer EA62_USR_INVALID_USR_TKN_MSG := EA62_USER_TOKEN + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ea62UsrInvalidUsrTknMsg