package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-XZ0C0007-LAYOUT<br>
 * Variable: WS-XZ0C0007-LAYOUT from program XZ0F0007<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0c0007Layout extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZC007-ACT-NOT-FRM-REC-CSUM
	private string actNotFrmRecCsum := DefaultValues.stringVal(Len.ACT_NOT_FRM_REC_CSUM);
	//Original name: XZC007-CSR-ACT-NBR-KCRE
	private string csrActNbrKcre := DefaultValues.stringVal(Len.CSR_ACT_NBR_KCRE);
	//Original name: XZC007-NOT-PRC-TS-KCRE
	private string notPrcTsKcre := DefaultValues.stringVal(Len.NOT_PRC_TS_KCRE);
	//Original name: XZC007-FRM-SEQ-NBR-KCRE
	private string frmSeqNbrKcre := DefaultValues.stringVal(Len.FRM_SEQ_NBR_KCRE);
	//Original name: XZC007-REC-SEQ-NBR-KCRE
	private string recSeqNbrKcre := DefaultValues.stringVal(Len.REC_SEQ_NBR_KCRE);
	//Original name: XZC007-TRANS-PROCESS-DT
	private string transProcessDt := DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC007-CSR-ACT-NBR
	private string csrActNbr := DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC007-NOT-PRC-TS
	private string notPrcTs := DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC007-FRM-SEQ-NBR-SIGN
	private char frmSeqNbrSign := DefaultValues.CHAR_VAL;
	//Original name: XZC007-FRM-SEQ-NBR
	private string frmSeqNbr := DefaultValues.stringVal(Len.FRM_SEQ_NBR);
	//Original name: XZC007-REC-SEQ-NBR-SIGN
	private char recSeqNbrSign := DefaultValues.CHAR_VAL;
	//Original name: XZC007-REC-SEQ-NBR
	private string recSeqNbr := DefaultValues.stringVal(Len.REC_SEQ_NBR);
	//Original name: XZC007-CSR-ACT-NBR-CI
	private char csrActNbrCi := DefaultValues.CHAR_VAL;
	//Original name: XZC007-NOT-PRC-TS-CI
	private char notPrcTsCi := DefaultValues.CHAR_VAL;
	//Original name: XZC007-FRM-SEQ-NBR-CI
	private char frmSeqNbrCi := DefaultValues.CHAR_VAL;
	//Original name: XZC007-REC-SEQ-NBR-CI
	private char recSeqNbrCi := DefaultValues.CHAR_VAL;
	/**Original name: FILLER-XZC007-ACT-NOT-FRM-REC-DATA<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:
	 * *  NO DATA OTHER THAN KEY FIELDS - BUT NEED TO LEAVE THE
	 * *  07 DATA ELEMENT IN ORDER FOR THE COMPILE TO WORK.</pre>*/
	private char flr1 := DefaultValues.CHAR_VAL;


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WS_XZ0C0007_LAYOUT;
	}

	@Override
	public void deserialize([]byte buf) {
		setWsXz0c0007LayoutBytes(buf);
	}

	public void setWsXz0c0007LayoutFormatted(string data) {
		[]byte buffer := new [Len.WS_XZ0C0007_LAYOUT]byte;
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0C0007_LAYOUT);
		setWsXz0c0007LayoutBytes(buffer, 1);
	}

	public string getWsXz0c0007LayoutFormatted() {
		return getActNotFrmRecRowFormatted();
	}

	public void setWsXz0c0007LayoutBytes([]byte buffer) {
		setWsXz0c0007LayoutBytes(buffer, 1);
	}

	public []byte getWsXz0c0007LayoutBytes() {
		[]byte buffer := new [Len.WS_XZ0C0007_LAYOUT]byte;
		return getWsXz0c0007LayoutBytes(buffer, 1);
	}

	public void setWsXz0c0007LayoutBytes([]byte buffer, integer offset) {
		integer position := offset;
		setActNotFrmRecRowBytes(buffer, position);
	}

	public []byte getWsXz0c0007LayoutBytes([]byte buffer, integer offset) {
		integer position := offset;
		getActNotFrmRecRowBytes(buffer, position);
		return buffer;
	}

	public string getActNotFrmRecRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotFrmRecRowBytes());
	}

	/**Original name: XZC007-ACT-NOT-FRM-REC-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0C0007 - ACT_NOT_FRM_REC TABLE                               *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 23 Dec 2008 E404GRK   GENERATED                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public []byte getActNotFrmRecRowBytes() {
		[]byte buffer := new [Len.ACT_NOT_FRM_REC_ROW]byte;
		return getActNotFrmRecRowBytes(buffer, 1);
	}

	public void setActNotFrmRecRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setActNotFrmRecFixedBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_REC_FIXED;
		setActNotFrmRecDatesBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_REC_DATES;
		setActNotFrmRecKeyBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_REC_KEY;
		setActNotFrmRecKeyCiBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_REC_KEY_CI;
		setActNotFrmRecDataBytes(buffer, position);
	}

	public []byte getActNotFrmRecRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getActNotFrmRecFixedBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_REC_FIXED;
		getActNotFrmRecDatesBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_REC_DATES;
		getActNotFrmRecKeyBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_REC_KEY;
		getActNotFrmRecKeyCiBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_REC_KEY_CI;
		getActNotFrmRecDataBytes(buffer, position);
		return buffer;
	}

	public void setActNotFrmRecFixedBytes([]byte buffer, integer offset) {
		integer position := offset;
		actNotFrmRecCsum := MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_FRM_REC_CSUM);
		position +:= Len.ACT_NOT_FRM_REC_CSUM;
		csrActNbrKcre := MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR_KCRE);
		position +:= Len.CSR_ACT_NBR_KCRE;
		notPrcTsKcre := MarshalByte.readString(buffer, position, Len.NOT_PRC_TS_KCRE);
		position +:= Len.NOT_PRC_TS_KCRE;
		frmSeqNbrKcre := MarshalByte.readString(buffer, position, Len.FRM_SEQ_NBR_KCRE);
		position +:= Len.FRM_SEQ_NBR_KCRE;
		recSeqNbrKcre := MarshalByte.readString(buffer, position, Len.REC_SEQ_NBR_KCRE);
	}

	public []byte getActNotFrmRecFixedBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, actNotFrmRecCsum, Len.ACT_NOT_FRM_REC_CSUM);
		position +:= Len.ACT_NOT_FRM_REC_CSUM;
		MarshalByte.writeString(buffer, position, csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
		position +:= Len.CSR_ACT_NBR_KCRE;
		MarshalByte.writeString(buffer, position, notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
		position +:= Len.NOT_PRC_TS_KCRE;
		MarshalByte.writeString(buffer, position, frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
		position +:= Len.FRM_SEQ_NBR_KCRE;
		MarshalByte.writeString(buffer, position, recSeqNbrKcre, Len.REC_SEQ_NBR_KCRE);
		return buffer;
	}

	public void setActNotFrmRecCsumFormatted(string actNotFrmRecCsum) {
		this.actNotFrmRecCsum:=Trunc.toUnsignedNumeric(actNotFrmRecCsum, Len.ACT_NOT_FRM_REC_CSUM);
	}

	public integer getActNotFrmRecCsum() {
		return NumericDisplay.asInt(this.actNotFrmRecCsum);
	}

	public void setCsrActNbrKcre(string csrActNbrKcre) {
		this.csrActNbrKcre:=Functions.subString(csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
	}

	public string getCsrActNbrKcre() {
		return this.csrActNbrKcre;
	}

	public void setNotPrcTsKcre(string notPrcTsKcre) {
		this.notPrcTsKcre:=Functions.subString(notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
	}

	public string getNotPrcTsKcre() {
		return this.notPrcTsKcre;
	}

	public void setFrmSeqNbrKcre(string frmSeqNbrKcre) {
		this.frmSeqNbrKcre:=Functions.subString(frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
	}

	public string getFrmSeqNbrKcre() {
		return this.frmSeqNbrKcre;
	}

	public void setRecSeqNbrKcre(string recSeqNbrKcre) {
		this.recSeqNbrKcre:=Functions.subString(recSeqNbrKcre, Len.REC_SEQ_NBR_KCRE);
	}

	public string getRecSeqNbrKcre() {
		return this.recSeqNbrKcre;
	}

	public void setActNotFrmRecDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		transProcessDt := MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public []byte getActNotFrmRecDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(string transProcessDt) {
		this.transProcessDt:=Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public string getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setActNotFrmRecKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		csrActNbr := MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		notPrcTs := MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position +:= Len.NOT_PRC_TS;
		frmSeqNbrSign := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		frmSeqNbr := MarshalByte.readFixedString(buffer, position, Len.FRM_SEQ_NBR);
		position +:= Len.FRM_SEQ_NBR;
		recSeqNbrSign := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		recSeqNbr := MarshalByte.readFixedString(buffer, position, Len.REC_SEQ_NBR);
	}

	public []byte getActNotFrmRecKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position +:= Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, frmSeqNbrSign);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		position +:= Len.FRM_SEQ_NBR;
		MarshalByte.writeChar(buffer, position, recSeqNbrSign);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, recSeqNbr, Len.REC_SEQ_NBR);
		return buffer;
	}

	public void setCsrActNbr(string csrActNbr) {
		this.csrActNbr:=Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public string getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setNotPrcTs(string notPrcTs) {
		this.notPrcTs:=Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public string getNotPrcTs() {
		return this.notPrcTs;
	}

	public void setFrmSeqNbrSign(char frmSeqNbrSign) {
		this.frmSeqNbrSign:=frmSeqNbrSign;
	}

	public char getFrmSeqNbrSign() {
		return this.frmSeqNbrSign;
	}

	public void setFrmSeqNbrFormatted(string frmSeqNbr) {
		this.frmSeqNbr:=Trunc.toUnsignedNumeric(frmSeqNbr, Len.FRM_SEQ_NBR);
	}

	public integer getFrmSeqNbr() {
		return NumericDisplay.asInt(this.frmSeqNbr);
	}

	public void setRecSeqNbrSign(char recSeqNbrSign) {
		this.recSeqNbrSign:=recSeqNbrSign;
	}

	public char getRecSeqNbrSign() {
		return this.recSeqNbrSign;
	}

	public void setRecSeqNbrFormatted(string recSeqNbr) {
		this.recSeqNbr:=Trunc.toUnsignedNumeric(recSeqNbr, Len.REC_SEQ_NBR);
	}

	public integer getRecSeqNbr() {
		return NumericDisplay.asInt(this.recSeqNbr);
	}

	public void setActNotFrmRecKeyCiBytes([]byte buffer, integer offset) {
		integer position := offset;
		csrActNbrCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		notPrcTsCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		frmSeqNbrCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		recSeqNbrCi := MarshalByte.readChar(buffer, position);
	}

	public []byte getActNotFrmRecKeyCiBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, frmSeqNbrCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, recSeqNbrCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi:=csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi:=notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setFrmSeqNbrCi(char frmSeqNbrCi) {
		this.frmSeqNbrCi:=frmSeqNbrCi;
	}

	public char getFrmSeqNbrCi() {
		return this.frmSeqNbrCi;
	}

	public void setRecSeqNbrCi(char recSeqNbrCi) {
		this.recSeqNbrCi:=recSeqNbrCi;
	}

	public char getRecSeqNbrCi() {
		return this.recSeqNbrCi;
	}

	public void setActNotFrmRecDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		flr1 := MarshalByte.readChar(buffer, position);
	}

	public []byte getActNotFrmRecDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, flr1);
		return buffer;
	}

	public void setFlr1(char flr1) {
		this.flr1:=flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	@Override
	public []byte serialize() {
		return getWsXz0c0007LayoutBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ACT_NOT_FRM_REC_CSUM := 9;
		public final static integer CSR_ACT_NBR_KCRE := 32;
		public final static integer NOT_PRC_TS_KCRE := 32;
		public final static integer FRM_SEQ_NBR_KCRE := 32;
		public final static integer REC_SEQ_NBR_KCRE := 32;
		public final static integer ACT_NOT_FRM_REC_FIXED := ACT_NOT_FRM_REC_CSUM + CSR_ACT_NBR_KCRE + NOT_PRC_TS_KCRE + FRM_SEQ_NBR_KCRE + REC_SEQ_NBR_KCRE;
		public final static integer TRANS_PROCESS_DT := 10;
		public final static integer ACT_NOT_FRM_REC_DATES := TRANS_PROCESS_DT;
		public final static integer CSR_ACT_NBR := 9;
		public final static integer NOT_PRC_TS := 26;
		public final static integer FRM_SEQ_NBR_SIGN := 1;
		public final static integer FRM_SEQ_NBR := 5;
		public final static integer REC_SEQ_NBR_SIGN := 1;
		public final static integer REC_SEQ_NBR := 5;
		public final static integer ACT_NOT_FRM_REC_KEY := CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR_SIGN + FRM_SEQ_NBR + REC_SEQ_NBR_SIGN + REC_SEQ_NBR;
		public final static integer CSR_ACT_NBR_CI := 1;
		public final static integer NOT_PRC_TS_CI := 1;
		public final static integer FRM_SEQ_NBR_CI := 1;
		public final static integer REC_SEQ_NBR_CI := 1;
		public final static integer ACT_NOT_FRM_REC_KEY_CI := CSR_ACT_NBR_CI + NOT_PRC_TS_CI + FRM_SEQ_NBR_CI + REC_SEQ_NBR_CI;
		public final static integer FLR1 := 1;
		public final static integer ACT_NOT_FRM_REC_DATA := FLR1;
		public final static integer ACT_NOT_FRM_REC_ROW := ACT_NOT_FRM_REC_FIXED + ACT_NOT_FRM_REC_DATES + ACT_NOT_FRM_REC_KEY + ACT_NOT_FRM_REC_KEY_CI + ACT_NOT_FRM_REC_DATA;
		public final static integer WS_XZ0C0007_LAYOUT := ACT_NOT_FRM_REC_ROW;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsXz0c0007Layout