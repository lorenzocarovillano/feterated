package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-FD-DATA<br>
 * Variable: L-FD-DATA from program TS514099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LFdData extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: FILLER-L-FD-DATA
	private string flr1 := DefaultValues.stringVal(Len.FLR1);
	//Original name: L-FD-DDNAME
	private string lFdDdname := DefaultValues.stringVal(Len.L_FD_DDNAME);


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.L_FD_DATA;
	}

	@Override
	public void deserialize([]byte buf) {
		setlFdDataBytes(buf);
	}

	public void setlFdDataBytes([]byte buffer) {
		setlFdDataBytes(buffer, 1);
	}

	public []byte getlFdDataBytes() {
		[]byte buffer := new [Len.L_FD_DATA]byte;
		return getlFdDataBytes(buffer, 1);
	}

	public void setlFdDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		flr1 := MarshalByte.readString(buffer, position, Len.FLR1);
		position +:= Len.FLR1;
		lFdDdname := MarshalByte.readString(buffer, position, Len.L_FD_DDNAME);
	}

	public []byte getlFdDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, lFdDdname, Len.L_FD_DDNAME);
		return buffer;
	}

	public void setFlr1(string flr1) {
		this.flr1:=Functions.subString(flr1, Len.FLR1);
	}

	public string getFlr1() {
		return this.flr1;
	}

	public void setlFdDdname(string lFdDdname) {
		this.lFdDdname:=Functions.subString(lFdDdname, Len.L_FD_DDNAME);
	}

	public string getlFdDdname() {
		return this.lFdDdname;
	}

	@Override
	public []byte serialize() {
		return getlFdDataBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer FLR1 := 40;
		public final static integer L_FD_DDNAME := 8;
		public final static integer L_FD_DATA := L_FD_DDNAME + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//LFdData