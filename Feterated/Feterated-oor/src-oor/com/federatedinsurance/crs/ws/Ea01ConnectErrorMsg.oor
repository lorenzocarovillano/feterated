package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-CONNECT-ERROR-MSG<br>
 * Variable: EA-01-CONNECT-ERROR-MSG from program TS030299<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01ConnectErrorMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG
	private char flr1 := Types.SPACE_CHAR;
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-1
	private string flr2 := "TS030299 -";
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-2
	private string flr3 := "CONNECT ERROR;";
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-3
	private string flr4 := "FUNCTION =";
	//Original name: EA-01-FUNCTION
	private string function := DefaultValues.stringVal(Len.FUNCTION);
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-4
	private string flr5 := "  SSID =";
	//Original name: EA-01-SSID
	private string ssid := DefaultValues.stringVal(Len.SSID);
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-5
	private string flr6 := ", RETURN =";
	//Original name: EA-01-RETURN
	private string returnFld := DefaultValues.stringVal(Len.RETURN_FLD);
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-6
	private string flr7 := ", REASON =";
	//Original name: EA-01-REASON
	private string reason := DefaultValues.stringVal(Len.REASON);


	//==== METHODS ====
	public string getEa01ConnectErrorMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01ConnectErrorMsgBytes());
	}

	public []byte getEa01ConnectErrorMsgBytes() {
		[]byte buffer := new [Len.EA01_CONNECT_ERROR_MSG]byte;
		return getEa01ConnectErrorMsgBytes(buffer, 1);
	}

	public []byte getEa01ConnectErrorMsgBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position +:= Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, function, Len.FUNCTION);
		position +:= Len.FUNCTION;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position +:= Len.FLR5;
		MarshalByte.writeString(buffer, position, ssid, Len.SSID);
		position +:= Len.SSID;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, returnFld, Len.RETURN_FLD);
		position +:= Len.RETURN_FLD;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, reason, Len.REASON);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public string getFlr3() {
		return this.flr3;
	}

	public string getFlr4() {
		return this.flr4;
	}

	public void setFunction(string function) {
		this.function:=Functions.subString(function, Len.FUNCTION);
	}

	public string getFunction() {
		return this.function;
	}

	public string getFlr5() {
		return this.flr5;
	}

	public void setSsid(string ssid) {
		this.ssid:=Functions.subString(ssid, Len.SSID);
	}

	public string getSsid() {
		return this.ssid;
	}

	public string getFlr6() {
		return this.flr6;
	}

	public void setReturnFld(string returnFld) {
		this.returnFld:=Functions.subString(returnFld, Len.RETURN_FLD);
	}

	public string getReturnFld() {
		return this.returnFld;
	}

	public string getFlr7() {
		return this.flr7;
	}

	public void setReason(string reason) {
		this.reason:=Functions.subString(reason, Len.REASON);
	}

	public string getReason() {
		return this.reason;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer FUNCTION := 18;
		public final static integer SSID := 4;
		public final static integer RETURN_FLD := 4;
		public final static integer REASON := 4;
		public final static integer FLR1 := 1;
		public final static integer FLR2 := 11;
		public final static integer FLR3 := 15;
		public final static integer FLR5 := 9;
		public final static integer EA01_CONNECT_ERROR_MSG := FUNCTION + SSID + RETURN_FLD + REASON + FLR1 + 4 * FLR2 + FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ea01ConnectErrorMsg