package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-CURRENT-TIMESTAMP-X<br>
 * Variable: WS-CURRENT-TIMESTAMP-X from program XZ0P0022<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsCurrentTimestampX {

	//==== PROPERTIES ====
	//Original name: WS-CURRENT-DATE
	private string dateFld := DefaultValues.stringVal(Len.DATE_FLD);
	//Original name: FILLER-WS-CURRENT-TIMESTAMP-X
	private string flr1 := DefaultValues.stringVal(Len.FLR1);


	//==== METHODS ====
	public void setWsCurrentTimestamp(string wsCurrentTimestamp) {
		integer position := 1;
		[]byte buffer := getWsCurrentTimestampXBytes();
		MarshalByte.writeString(buffer, position, wsCurrentTimestamp, Len.WS_CURRENT_TIMESTAMP);
		setWsCurrentTimestampXBytes(buffer);
	}

	/**Original name: WS-CURRENT-TIMESTAMP<br>*/
	public string getWsCurrentTimestamp() {
		integer position := 1;
		return MarshalByte.readString(getWsCurrentTimestampXBytes(), position, Len.WS_CURRENT_TIMESTAMP);
	}

	public void setWsCurrentTimestampXBytes([]byte buffer) {
		setWsCurrentTimestampXBytes(buffer, 1);
	}

	/**Original name: WS-CURRENT-TIMESTAMP-X<br>*/
	public []byte getWsCurrentTimestampXBytes() {
		[]byte buffer := new [Len.WS_CURRENT_TIMESTAMP_X]byte;
		return getWsCurrentTimestampXBytes(buffer, 1);
	}

	public void setWsCurrentTimestampXBytes([]byte buffer, integer offset) {
		integer position := offset;
		dateFld := MarshalByte.readString(buffer, position, Len.DATE_FLD);
		position +:= Len.DATE_FLD;
		flr1 := MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public []byte getWsCurrentTimestampXBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, dateFld, Len.DATE_FLD);
		position +:= Len.DATE_FLD;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setDateFld(string dateFld) {
		this.dateFld:=Functions.subString(dateFld, Len.DATE_FLD);
	}

	public string getDateFld() {
		return this.dateFld;
	}

	public void setFlr1(string flr1) {
		this.flr1:=Functions.subString(flr1, Len.FLR1);
	}

	public string getFlr1() {
		return this.flr1;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer DATE_FLD := 10;
		public final static integer FLR1 := 16;
		public final static integer WS_CURRENT_TIMESTAMP_X := DATE_FLD + FLR1;
		public final static integer WS_CURRENT_TIMESTAMP := 26;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer WS_CURRENT_TIMESTAMP := 26;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//WsCurrentTimestampX