package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.ws.enums.SaCicsApplidXzc08090;
import com.federatedinsurance.crs.ws.enums.SaTarSys;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaveAreaXzc08090 {

	//==== PROPERTIES ====
	//Original name: SA-RESPONSE-CODE
	private integer responseCode := DefaultValues.BIN_INT_VAL;
	//Original name: SA-RESPONSE-CODE2
	private integer responseCode2 := DefaultValues.BIN_INT_VAL;
	//Original name: SA-CHN-NM
	private string chnNm := DefaultValues.stringVal(Len.CHN_NM);
	//Original name: SA-WEB-SVC-URL
	private string webSvcUrl := "";
	//Original name: SA-UC-USR-ID
	private string ucUsrId := DefaultValues.stringVal(Len.UC_USR_ID);
	//Original name: SA-UC-PWD
	private string ucPwd := DefaultValues.stringVal(Len.UC_PWD);
	//Original name: SA-CICS-APPLID
	private SaCicsApplidXzc08090 cicsApplid := new SaCicsApplidXzc08090();
	//Original name: SA-TAR-SYS
	private SaTarSys tarSys := new SaTarSys();


	//==== METHODS ====
	public void setResponseCode(integer responseCode) {
		this.responseCode:=responseCode;
	}

	public integer getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(integer responseCode2) {
		this.responseCode2:=responseCode2;
	}

	public integer getResponseCode2() {
		return this.responseCode2;
	}

	public void setChnNm(string chnNm) {
		this.chnNm:=Functions.subString(chnNm, Len.CHN_NM);
	}

	public string getChnNm() {
		return this.chnNm;
	}

	public string getChnNmFormatted() {
		return Functions.padBlanks(getChnNm(), Len.CHN_NM);
	}

	public void setWebSvcUrl(string webSvcUrl) {
		this.webSvcUrl:=Functions.subString(webSvcUrl, Len.WEB_SVC_URL);
	}

	public string getWebSvcUrl() {
		return this.webSvcUrl;
	}

	public void setUsrCredentialsBytes([]byte buffer) {
		setUsrCredentialsBytes(buffer, 1);
	}

	public void setUsrCredentialsBytes([]byte buffer, integer offset) {
		integer position := offset;
		ucUsrId := MarshalByte.readString(buffer, position, Len.UC_USR_ID);
		position +:= Len.UC_USR_ID;
		ucPwd := MarshalByte.readString(buffer, position, Len.UC_PWD);
	}

	public void setUcUsrId(string ucUsrId) {
		this.ucUsrId:=Functions.subString(ucUsrId, Len.UC_USR_ID);
	}

	public string getUcUsrId() {
		return this.ucUsrId;
	}

	public void setUcPwd(string ucPwd) {
		this.ucPwd:=Functions.subString(ucPwd, Len.UC_PWD);
	}

	public string getUcPwd() {
		return this.ucPwd;
	}

	public SaCicsApplidXzc08090 getCicsApplid() {
		return cicsApplid;
	}

	public SaTarSys getTarSys() {
		return tarSys;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CHN_NM := 16;
		public final static integer UC_USR_ID := 8;
		public final static integer UC_PWD := 8;
		public final static integer WEB_SVC_URL := 256;
		public final static integer USR_CREDENTIALS := UC_USR_ID + UC_PWD;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//SaveAreaXzc08090