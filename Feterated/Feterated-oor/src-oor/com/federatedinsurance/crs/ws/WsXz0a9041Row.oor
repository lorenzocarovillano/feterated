package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9041-ROW<br>
 * Variable: WS-XZ0A9041-ROW from program XZ0B9040<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9041Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA941-FRM-SEQ-NBR
	private integer frmSeqNbr := DefaultValues.INT_VAL;
	//Original name: XZA941-FRM-NBR
	private string frmNbr := DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: XZA941-FRM-EDT-DT
	private string frmEdtDt := DefaultValues.stringVal(Len.FRM_EDT_DT);
	//Original name: XZA941-PO-START-POINT
	private long poStartPoint := DefaultValues.LONG_VAL;


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WS_XZ0A9041_ROW;
	}

	@Override
	public void deserialize([]byte buf) {
		setWsXz0a9041RowBytes(buf);
	}

	public string getWsXz0a9041RowFormatted() {
		return getGetFrmListDetailFormatted();
	}

	public void setWsXz0a9041RowBytes([]byte buffer) {
		setWsXz0a9041RowBytes(buffer, 1);
	}

	public []byte getWsXz0a9041RowBytes() {
		[]byte buffer := new [Len.WS_XZ0A9041_ROW]byte;
		return getWsXz0a9041RowBytes(buffer, 1);
	}

	public void setWsXz0a9041RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setGetFrmListDetailBytes(buffer, position);
	}

	public []byte getWsXz0a9041RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getGetFrmListDetailBytes(buffer, position);
		return buffer;
	}

	public string getGetFrmListDetailFormatted() {
		return MarshalByteExt.bufferToStr(getGetFrmListDetailBytes());
	}

	/**Original name: XZA941-GET-FRM-LIST-DETAIL<br>
	 * <pre>*************************************************************
	 *  XZ0A9041 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_FRM_LIST                           *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  20JAN2009 E404DLP    NEW                          *
	 *  PP02756  04FEB2011 E404DLP    ADDED PAGING FIELDS          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public []byte getGetFrmListDetailBytes() {
		[]byte buffer := new [Len.GET_FRM_LIST_DETAIL]byte;
		return getGetFrmListDetailBytes(buffer, 1);
	}

	public void setGetFrmListDetailBytes([]byte buffer, integer offset) {
		integer position := offset;
		frmSeqNbr := MarshalByte.readInt(buffer, position, Len.FRM_SEQ_NBR);
		position +:= Len.FRM_SEQ_NBR;
		frmNbr := MarshalByte.readString(buffer, position, Len.FRM_NBR);
		position +:= Len.FRM_NBR;
		frmEdtDt := MarshalByte.readString(buffer, position, Len.FRM_EDT_DT);
		position +:= Len.FRM_EDT_DT;
		setPagingOutputsBytes(buffer, position);
	}

	public []byte getGetFrmListDetailBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeInt(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		position +:= Len.FRM_SEQ_NBR;
		MarshalByte.writeString(buffer, position, frmNbr, Len.FRM_NBR);
		position +:= Len.FRM_NBR;
		MarshalByte.writeString(buffer, position, frmEdtDt, Len.FRM_EDT_DT);
		position +:= Len.FRM_EDT_DT;
		getPagingOutputsBytes(buffer, position);
		return buffer;
	}

	public void setFrmSeqNbr(integer frmSeqNbr) {
		this.frmSeqNbr:=frmSeqNbr;
	}

	public integer getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	public void setFrmNbr(string frmNbr) {
		this.frmNbr:=Functions.subString(frmNbr, Len.FRM_NBR);
	}

	public string getFrmNbr() {
		return this.frmNbr;
	}

	public void setFrmEdtDt(string frmEdtDt) {
		this.frmEdtDt:=Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	public string getFrmEdtDt() {
		return this.frmEdtDt;
	}

	public void setPagingOutputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		poStartPoint := MarshalByte.readLong(buffer, position, Len.PO_START_POINT);
	}

	public []byte getPagingOutputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeLong(buffer, position, poStartPoint, Len.PO_START_POINT);
		return buffer;
	}

	public void setPoStartPoint(long poStartPoint) {
		this.poStartPoint:=poStartPoint;
	}

	public long getPoStartPoint() {
		return this.poStartPoint;
	}

	@Override
	public []byte serialize() {
		return getWsXz0a9041RowBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer FRM_SEQ_NBR := 5;
		public final static integer FRM_NBR := 30;
		public final static integer FRM_EDT_DT := 10;
		public final static integer PO_START_POINT := 10;
		public final static integer PAGING_OUTPUTS := PO_START_POINT;
		public final static integer GET_FRM_LIST_DETAIL := FRM_SEQ_NBR + FRM_NBR + FRM_EDT_DT + PAGING_OUTPUTS;
		public final static integer WS_XZ0A9041_ROW := GET_FRM_LIST_DETAIL;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsXz0a9041Row