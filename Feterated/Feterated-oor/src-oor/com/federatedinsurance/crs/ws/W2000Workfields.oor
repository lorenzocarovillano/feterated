package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: W2000-WORKFIELDS<br>
 * Variable: W2000-WORKFIELDS from program HALOUIDG<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class W2000Workfields {

	//==== PROPERTIES ====
	//Original name: W2000-INPUT-BASE10
	private string inputBase10 := DefaultValues.stringVal(Len.INPUT_BASE10);
	/**Original name: W2000-INDEX<br>
	 * <pre>*     05     W2000-INPUT-BASE10  PIC  9(12).</pre>*/
	private short index2 := DefaultValues.SHORT_VAL;
	//Original name: W2000-OUTPUT-BASE36
	private string outputBase36 := DefaultValues.stringVal(Len.OUTPUT_BASE36);
	//Original name: W2000-REMAINDER
	private long remainder := DefaultValues.LONG_VAL;
	//Original name: W2000-QUOTIENT
	private long quotient := DefaultValues.LONG_VAL;
	/**Original name: W2000-REF-DIGITS<br>
	 * <pre>*   03       W2000-QUOTIENT      PIC  9(12)    PACKED-DECIMAL.</pre>*/
	private string refDigits := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";


	//==== METHODS ====
	public void setInputBase10Formatted(string inputBase10) {
		this.inputBase10:=Trunc.toUnsignedNumeric(inputBase10, Len.INPUT_BASE10);
	}

	public long getInputBase10() {
		return NumericDisplay.asLong(this.inputBase10);
	}

	public void setIndex2(short index2) {
		this.index2 := index2;
	}

	public short getIndex2() {
		return this.index2;
	}

	public void setOutputBase36(string outputBase36) {
		this.outputBase36:=Functions.subString(outputBase36, Len.OUTPUT_BASE36);
	}

	public string getOutputBase36() {
		return this.outputBase36;
	}

	public string getOutputBase36Formatted() {
		return Functions.padBlanks(getOutputBase36(), Len.OUTPUT_BASE36);
	}

	public void setRemainder(long remainder) {
		this.remainder:=remainder;
	}

	public long getRemainder() {
		return this.remainder;
	}

	public void setQuotient(long quotient) {
		this.quotient:=quotient;
	}

	public long getQuotient() {
		return this.quotient;
	}

	public string getRefDigits() {
		return this.refDigits;
	}

	public string getRefDigitsFormatted() {
		return Functions.padBlanks(getRefDigits(), Len.REF_DIGITS);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer INPUT_BASE10 := 16;
		public final static integer INDEX2 := 2;
		public final static integer OUTPUT_BASE36 := 10;
		public final static integer REF_DIGITS := 36;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//W2000Workfields