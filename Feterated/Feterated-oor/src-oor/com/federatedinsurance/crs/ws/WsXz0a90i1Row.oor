package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90I1-ROW<br>
 * Variable: WS-XZ0A90I1-ROW from program XZ0B90I0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90i1Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9I1-FRM-SEQ-NBR
	private integer frmSeqNbr := DefaultValues.INT_VAL;
	//Original name: XZA9I1-REC-SEQ-NBR
	private integer recSeqNbr := DefaultValues.INT_VAL;


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WS_XZ0A90I1_ROW;
	}

	@Override
	public void deserialize([]byte buf) {
		setWsXz0a90i1RowBytes(buf);
	}

	public string getWsXz0a90i1RowFormatted() {
		return getGetFrmRecListDtlFormatted();
	}

	public void setWsXz0a90i1RowBytes([]byte buffer) {
		setWsXz0a90i1RowBytes(buffer, 1);
	}

	public []byte getWsXz0a90i1RowBytes() {
		[]byte buffer := new [Len.WS_XZ0A90I1_ROW]byte;
		return getWsXz0a90i1RowBytes(buffer, 1);
	}

	public void setWsXz0a90i1RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setGetFrmRecListDtlBytes(buffer, position);
	}

	public []byte getWsXz0a90i1RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getGetFrmRecListDtlBytes(buffer, position);
		return buffer;
	}

	public string getGetFrmRecListDtlFormatted() {
		return MarshalByteExt.bufferToStr(getGetFrmRecListDtlBytes());
	}

	/**Original name: XZA9I1-GET-FRM-REC-LIST-DTL<br>
	 * <pre>*************************************************************
	 *  XZ0A90I1 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_FRM_REC_LIST                       *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  04MAR2009 E404KXS    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public []byte getGetFrmRecListDtlBytes() {
		[]byte buffer := new [Len.GET_FRM_REC_LIST_DTL]byte;
		return getGetFrmRecListDtlBytes(buffer, 1);
	}

	public void setGetFrmRecListDtlBytes([]byte buffer, integer offset) {
		integer position := offset;
		frmSeqNbr := MarshalByte.readInt(buffer, position, Len.FRM_SEQ_NBR);
		position +:= Len.FRM_SEQ_NBR;
		recSeqNbr := MarshalByte.readInt(buffer, position, Len.REC_SEQ_NBR);
	}

	public []byte getGetFrmRecListDtlBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeInt(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		position +:= Len.FRM_SEQ_NBR;
		MarshalByte.writeInt(buffer, position, recSeqNbr, Len.REC_SEQ_NBR);
		return buffer;
	}

	public void setFrmSeqNbr(integer frmSeqNbr) {
		this.frmSeqNbr:=frmSeqNbr;
	}

	public integer getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	public void setRecSeqNbr(integer recSeqNbr) {
		this.recSeqNbr:=recSeqNbr;
	}

	public integer getRecSeqNbr() {
		return this.recSeqNbr;
	}

	@Override
	public []byte serialize() {
		return getWsXz0a90i1RowBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer FRM_SEQ_NBR := 5;
		public final static integer REC_SEQ_NBR := 5;
		public final static integer GET_FRM_REC_LIST_DTL := FRM_SEQ_NBR + REC_SEQ_NBR;
		public final static integer WS_XZ0A90I1_ROW := GET_FRM_REC_LIST_DTL;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsXz0a90i1Row