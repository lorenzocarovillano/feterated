package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TYP-BASED-FIELDS<br>
 * Variable: WS-TYP-BASED-FIELDS from program XZ0B90Q0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsTypBasedFields {

	//==== PROPERTIES ====
	//Original name: WS-ACT-TYP-CD
	private string actTypCd := DefaultValues.stringVal(Len.ACT_TYP_CD);
	//Original name: WS-PDC-NBR-START
	private char pdcNbrStart := DefaultValues.CHAR_VAL;
	//Original name: FILLER-WS-PDC-NBR
	private char flr1 := '-';
	//Original name: WS-PDC-NBR-END
	private string pdcNbrEnd := DefaultValues.stringVal(Len.PDC_NBR_END);
	//Original name: WS-PDC-NM
	private string pdcNm := DefaultValues.stringVal(Len.PDC_NM);
	//Original name: WS-LAST-NAME
	private string lastName := DefaultValues.stringVal(Len.LAST_NAME);
	//Original name: WS-COWN-NM
	private string cownNm := "";


	//==== METHODS ====
	public void setActTypCd(string actTypCd) {
		this.actTypCd:=Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	public string getActTypCd() {
		return this.actTypCd;
	}

	public void setPdcNbrFormatted(string data) {
		[]byte buffer := new [Len.PDC_NBR]byte;
		MarshalByte.writeString(buffer, 1, data, Len.PDC_NBR);
		setPdcNbrBytes(buffer, 1);
	}

	public string getPdcNbrFormatted() {
		return MarshalByteExt.bufferToStr(getPdcNbrBytes());
	}

	/**Original name: WS-PDC-NBR<br>*/
	public []byte getPdcNbrBytes() {
		[]byte buffer := new [Len.PDC_NBR]byte;
		return getPdcNbrBytes(buffer, 1);
	}

	public void setPdcNbrBytes([]byte buffer, integer offset) {
		integer position := offset;
		pdcNbrStart := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		flr1 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pdcNbrEnd := MarshalByte.readString(buffer, position, Len.PDC_NBR_END);
	}

	public []byte getPdcNbrBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, pdcNbrStart);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, flr1);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pdcNbrEnd, Len.PDC_NBR_END);
		return buffer;
	}

	public void setPdcNbrStart(char pdcNbrStart) {
		this.pdcNbrStart:=pdcNbrStart;
	}

	public void setPdcNbrStartFormatted(string pdcNbrStart) {
		setPdcNbrStart(Functions.charAt(pdcNbrStart, Types.CHAR_SIZE));
	}

	public char getPdcNbrStart() {
		return this.pdcNbrStart;
	}

	public void setFlr1(char flr1) {
		this.flr1:=flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setPdcNbrEnd(string pdcNbrEnd) {
		this.pdcNbrEnd:=Functions.subString(pdcNbrEnd, Len.PDC_NBR_END);
	}

	public string getPdcNbrEnd() {
		return this.pdcNbrEnd;
	}

	public void setPdcNm(string pdcNm) {
		this.pdcNm:=Functions.subString(pdcNm, Len.PDC_NM);
	}

	public string getPdcNm() {
		return this.pdcNm;
	}

	public void setLastName(string lastName) {
		this.lastName:=Functions.subString(lastName, Len.LAST_NAME);
	}

	public string getLastName() {
		return this.lastName;
	}

	public void setCownNm(string cownNm) {
		this.cownNm:=Functions.subString(cownNm, Len.COWN_NM);
	}

	public string getCownNm() {
		return this.cownNm;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ACT_TYP_CD := 2;
		public final static integer PDC_NBR_END := 3;
		public final static integer PDC_NM := 120;
		public final static integer LAST_NAME := 60;
		public final static integer PDC_NBR_START := 1;
		public final static integer FLR1 := 1;
		public final static integer PDC_NBR := PDC_NBR_START + PDC_NBR_END + FLR1;
		public final static integer COWN_NM := 45;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsTypBasedFields