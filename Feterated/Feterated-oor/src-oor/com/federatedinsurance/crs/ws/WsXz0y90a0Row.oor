package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0Y90A0-ROW<br>
 * Variable: WS-XZ0Y90A0-ROW from program XZ0P90A0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0y90a0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZY9A0-CSR-ACT-NBR
	private string csrActNbr := DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZY9A0-NOT-PRC-TS
	private string notPrcTs := DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZY9A0-USERID
	private string userid := DefaultValues.stringVal(Len.USERID);


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WS_XZ0Y90A0_ROW;
	}

	@Override
	public void deserialize([]byte buf) {
		setWsXz0y90a0RowBytes(buf);
	}

	public string getWsXz0y90a0RowFormatted() {
		return getThirdPartyRowFormatted();
	}

	public void setWsXz0y90a0RowBytes([]byte buffer) {
		setWsXz0y90a0RowBytes(buffer, 1);
	}

	public []byte getWsXz0y90a0RowBytes() {
		[]byte buffer := new [Len.WS_XZ0Y90A0_ROW]byte;
		return getWsXz0y90a0RowBytes(buffer, 1);
	}

	public void setWsXz0y90a0RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setThirdPartyRowBytes(buffer, position);
	}

	public []byte getWsXz0y90a0RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getThirdPartyRowBytes(buffer, position);
		return buffer;
	}

	public string getThirdPartyRowFormatted() {
		return MarshalByteExt.bufferToStr(getThirdPartyRowBytes());
	}

	/**Original name: XZY9A0-THIRD-PARTY-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0Y90A0 - SERVICE CONTRACT COPYBOOK FOR                   *
	 *             UOW : XZ_PREPARE_THIRD_PARTY_LIST               *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  11FEB2009 E404DLP    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public []byte getThirdPartyRowBytes() {
		[]byte buffer := new [Len.THIRD_PARTY_ROW]byte;
		return getThirdPartyRowBytes(buffer, 1);
	}

	public void setThirdPartyRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		csrActNbr := MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		notPrcTs := MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position +:= Len.NOT_PRC_TS;
		userid := MarshalByte.readString(buffer, position, Len.USERID);
	}

	public []byte getThirdPartyRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position +:= Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setCsrActNbr(string csrActNbr) {
		this.csrActNbr:=Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public string getCsrActNbr() {
		return this.csrActNbr;
	}

	public string getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(string notPrcTs) {
		this.notPrcTs:=Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public string getNotPrcTs() {
		return this.notPrcTs;
	}

	public string getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setUserid(string userid) {
		this.userid:=Functions.subString(userid, Len.USERID);
	}

	public string getUserid() {
		return this.userid;
	}

	@Override
	public []byte serialize() {
		return getWsXz0y90a0RowBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CSR_ACT_NBR := 9;
		public final static integer NOT_PRC_TS := 26;
		public final static integer USERID := 8;
		public final static integer THIRD_PARTY_ROW := CSR_ACT_NBR + NOT_PRC_TS + USERID;
		public final static integer WS_XZ0Y90A0_ROW := THIRD_PARTY_ROW;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsXz0y90a0Row