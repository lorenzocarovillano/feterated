package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Ts020tbl;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0R0014<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0r0014Data {

	//==== PROPERTIES ====
	//Original name: CF-TF-ACT-NOT-FRM
	private string cfTfActNotFrm := "XZ0F0006";
	//Original name: FILLER-CF-INVALID-OPERATION
	private string flr1 := "RESPONSE MODULE";
	//Original name: FILLER-CF-INVALID-OPERATION-1
	private string flr2 := " - INVALID";
	//Original name: FILLER-CF-INVALID-OPERATION-2
	private string flr3 := "OPERATION";
	//Original name: FILLER-EA-01-PROGRAM-LINK-FAILED
	private string flr4 := "CALL TO";
	//Original name: EA-01-FAILED-LINK-PGM-NAME
	private string ea01FailedLinkPgmName := DefaultValues.stringVal(Len.EA01_FAILED_LINK_PGM_NAME);
	//Original name: FILLER-EA-01-PROGRAM-LINK-FAILED-1
	private string flr5 := " FAILED.";
	//Original name: FILLER-EA-02-INVALID-OPERATION-MSG
	private string flr6 := "XZ0R0014 -";
	//Original name: FILLER-EA-02-INVALID-OPERATION-MSG-1
	private string flr7 := "INVALID OPER:";
	//Original name: EA-02-INVALID-OPERATION-NAME
	private string ea02InvalidOperationName := DefaultValues.stringVal(Len.EA02_INVALID_OPERATION_NAME);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0r0014 workingStorageArea := new WorkingStorageAreaXz0r0014();
	//Original name: TS020TBL
	private Ts020tbl ts020tbl := new Ts020tbl();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc := new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom := new Hallcom();
	//Original name: WS-APPLID
	private string wsApplid := DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw := new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo := new WsEstoInfo();


	//==== METHODS ====
	public string getCfTfActNotFrm() {
		return this.cfTfActNotFrm;
	}

	public string getCfInvalidOperationFormatted() {
		return MarshalByteExt.bufferToStr(getCfInvalidOperationBytes());
	}

	/**Original name: CF-INVALID-OPERATION<br>*/
	public []byte getCfInvalidOperationBytes() {
		[]byte buffer := new [Len.CF_INVALID_OPERATION]byte;
		return getCfInvalidOperationBytes(buffer, 1);
	}

	public []byte getCfInvalidOperationBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public string getFlr3() {
		return this.flr3;
	}

	public string getEa01ProgramLinkFailedFormatted() {
		return MarshalByteExt.bufferToStr(getEa01ProgramLinkFailedBytes());
	}

	/**Original name: EA-01-PROGRAM-LINK-FAILED<br>*/
	public []byte getEa01ProgramLinkFailedBytes() {
		[]byte buffer := new [Len.EA01_PROGRAM_LINK_FAILED]byte;
		return getEa01ProgramLinkFailedBytes(buffer, 1);
	}

	public []byte getEa01ProgramLinkFailedBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position +:= Len.FLR4;
		MarshalByte.writeString(buffer, position, ea01FailedLinkPgmName, Len.EA01_FAILED_LINK_PGM_NAME);
		position +:= Len.EA01_FAILED_LINK_PGM_NAME;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR4);
		return buffer;
	}

	public string getFlr4() {
		return this.flr4;
	}

	public void setEa01FailedLinkPgmName(string ea01FailedLinkPgmName) {
		this.ea01FailedLinkPgmName:=Functions.subString(ea01FailedLinkPgmName, Len.EA01_FAILED_LINK_PGM_NAME);
	}

	public string getEa01FailedLinkPgmName() {
		return this.ea01FailedLinkPgmName;
	}

	public string getFlr5() {
		return this.flr5;
	}

	public string getEa02InvalidOperationMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidOperationMsgBytes());
	}

	/**Original name: EA-02-INVALID-OPERATION-MSG<br>*/
	public []byte getEa02InvalidOperationMsgBytes() {
		[]byte buffer := new [Len.EA02_INVALID_OPERATION_MSG]byte;
		return getEa02InvalidOperationMsgBytes(buffer, 1);
	}

	public []byte getEa02InvalidOperationMsgBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position +:= Len.FLR7;
		MarshalByte.writeString(buffer, position, ea02InvalidOperationName, Len.EA02_INVALID_OPERATION_NAME);
		return buffer;
	}

	public string getFlr6() {
		return this.flr6;
	}

	public string getFlr7() {
		return this.flr7;
	}

	public void setEa02InvalidOperationName(string ea02InvalidOperationName) {
		this.ea02InvalidOperationName:=Functions.subString(ea02InvalidOperationName, Len.EA02_INVALID_OPERATION_NAME);
	}

	public string getEa02InvalidOperationName() {
		return this.ea02InvalidOperationName;
	}

	public void setTableFormatterDataFormatted(string data) {
		[]byte buffer := new [Len.TABLE_FORMATTER_DATA]byte;
		MarshalByte.writeString(buffer, 1, data, Len.TABLE_FORMATTER_DATA);
		setTableFormatterDataBytes(buffer, 1);
	}

	public string getTableFormatterDataFormatted() {
		return ts020tbl.getTableFormatterParmsFormatted();
	}

	public void setTableFormatterDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		ts020tbl.setTableFormatterParmsBytes(buffer, position);
	}

	public void setWsApplid(string wsApplid) {
		this.wsApplid:=Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public string getWsApplid() {
		return this.wsApplid;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Ts020tbl getTs020tbl() {
		return ts020tbl;
	}

	public WorkingStorageAreaXz0r0014 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer EA01_FAILED_LINK_PGM_NAME := 8;
		public final static integer EA02_INVALID_OPERATION_NAME := 32;
		public final static integer WS_SE3_CUR_ISO_DATE := 10;
		public final static integer WS_SE3_CUR_ISO_TIME := 16;
		public final static integer WS_APPLID := 8;
		public final static integer FLR1 := 15;
		public final static integer FLR2 := 11;
		public final static integer FLR3 := 9;
		public final static integer CF_INVALID_OPERATION := FLR1 + FLR2 + FLR3;
		public final static integer FLR7 := 14;
		public final static integer EA02_INVALID_OPERATION_MSG := EA02_INVALID_OPERATION_NAME + FLR2 + FLR7;
		public final static integer TABLE_FORMATTER_DATA := Ts020tbl.Len.TABLE_FORMATTER_PARMS;
		public final static integer FLR4 := 8;
		public final static integer EA01_PROGRAM_LINK_FAILED := EA01_FAILED_LINK_PGM_NAME + 2 * FLR4;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xz0r0014Data