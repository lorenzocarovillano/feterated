package com.federatedinsurance.crs.ws.ptr;

import java.lang.Override;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0021<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
class LServiceContractAreaXz0x0021 extends BytesClass {

	//==== PROPERTIES ====
	public final static integer XZT21I_POL_NBR_LIST_MAXOCCURS := 150;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0021() {	}
	public LServiceContractAreaXz0x0021([]byte data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt21iCsrActNbr(string xzt21iCsrActNbr) {
		writeString(Pos.XZT21I_CSR_ACT_NBR, xzt21iCsrActNbr, Len.XZT21I_CSR_ACT_NBR);
	}

	/**Original name: XZT21I-CSR-ACT-NBR<br>*/
	public string getXzt21iCsrActNbr() {
		return readString(Pos.XZT21I_CSR_ACT_NBR, Len.XZT21I_CSR_ACT_NBR);
	}

	public void setXzt21iActTmnDt(string xzt21iActTmnDt) {
		writeString(Pos.XZT21I_ACT_TMN_DT, xzt21iActTmnDt, Len.XZT21I_ACT_TMN_DT);
	}

	/**Original name: XZT21I-ACT-TMN-DT<br>*/
	public string getXzt21iActTmnDt() {
		return readString(Pos.XZT21I_ACT_TMN_DT, Len.XZT21I_ACT_TMN_DT);
	}

	public void setXzt21iNotTypCd(string xzt21iNotTypCd) {
		writeString(Pos.XZT21I_NOT_TYP_CD, xzt21iNotTypCd, Len.XZT21I_NOT_TYP_CD);
	}

	/**Original name: XZT21I-NOT-TYP-CD<br>*/
	public string getXzt21iNotTypCd() {
		return readString(Pos.XZT21I_NOT_TYP_CD, Len.XZT21I_NOT_TYP_CD);
	}

	/**Original name: XZT21I-POL-NBR-LIST<br>*/
	public []byte getXzt21iPolNbrListBytes(integer xzt21iPolNbrListIdx) {
		[]byte buffer := new [Len.XZT21I_POL_NBR_LIST]byte;
		return getXzt21iPolNbrListBytes(xzt21iPolNbrListIdx, buffer, 1);
	}

	public []byte getXzt21iPolNbrListBytes(integer xzt21iPolNbrListIdx, []byte buffer, integer offset) {
		integer position := Pos.xzt21iPolNbrList(xzt21iPolNbrListIdx - 1);
		getBytes(buffer, offset, Len.XZT21I_POL_NBR_LIST, position);
		return buffer;
	}

	public void setXzt21iPolNbr(integer xzt21iPolNbrIdx, string xzt21iPolNbr) {
		integer position := Pos.xzt21iPolNbr(xzt21iPolNbrIdx - 1);
		writeString(position, xzt21iPolNbr, Len.XZT21I_POL_NBR);
	}

	/**Original name: XZT21I-POL-NBR<br>*/
	public string getXzt21iPolNbr(integer xzt21iPolNbrIdx) {
		integer position := Pos.xzt21iPolNbr(xzt21iPolNbrIdx - 1);
		return readString(position, Len.XZT21I_POL_NBR);
	}

	public void setXzt21iUserid(string xzt21iUserid) {
		writeString(Pos.XZT21I_USERID, xzt21iUserid, Len.XZT21I_USERID);
	}

	/**Original name: XZT21I-USERID<br>*/
	public string getXzt21iUserid() {
		return readString(Pos.XZT21I_USERID, Len.XZT21I_USERID);
	}

	public void setXzt21oCsrActNbr(string xzt21oCsrActNbr) {
		writeString(Pos.XZT21O_CSR_ACT_NBR, xzt21oCsrActNbr, Len.XZT21O_CSR_ACT_NBR);
	}

	/**Original name: XZT21O-CSR-ACT-NBR<br>*/
	public string getXzt21oCsrActNbr() {
		return readString(Pos.XZT21O_CSR_ACT_NBR, Len.XZT21O_CSR_ACT_NBR);
	}

	public void setXzt21oNotDt(string xzt21oNotDt) {
		writeString(Pos.XZT21O_NOT_DT, xzt21oNotDt, Len.XZT21O_NOT_DT);
	}

	/**Original name: XZT21O-NOT-DT<br>*/
	public string getXzt21oNotDt() {
		return readString(Pos.XZT21O_NOT_DT, Len.XZT21O_NOT_DT);
	}


	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public final static integer L_SERVICE_CONTRACT_AREA := 1;
		public final static integer XZT021_SERVICE_INPUTS := L_SERVICE_CONTRACT_AREA;
		public final static integer XZT21I_CSR_ACT_NBR := XZT021_SERVICE_INPUTS;
		public final static integer XZT21I_ACT_TMN_DT := XZT21I_CSR_ACT_NBR + Len.XZT21I_CSR_ACT_NBR;
		public final static integer XZT21I_NOT_TYP_CD := XZT21I_ACT_TMN_DT + Len.XZT21I_ACT_TMN_DT;
		public final static integer XZT21I_USERID := xzt21iPolNbr(XZT21I_POL_NBR_LIST_MAXOCCURS - 1) + Len.XZT21I_POL_NBR;
		public final static integer XZT021_SERVICE_OUTPUTS := XZT21I_USERID + Len.XZT21I_USERID;
		public final static integer XZT21O_CSR_ACT_NBR := XZT021_SERVICE_OUTPUTS;
		public final static integer XZT21O_NOT_DT := XZT21O_CSR_ACT_NBR + Len.XZT21O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {		}

		//==== METHODS ====
		public static integer xzt21iPolNbrList(integer idx) {
			return XZT21I_NOT_TYP_CD + Len.XZT21I_NOT_TYP_CD + idx * Len.XZT21I_POL_NBR_LIST;
		}

		public static integer xzt21iPolNbr(integer idx) {
			return xzt21iPolNbrList(idx);
		}

	}
	public static class Len {

		//==== PROPERTIES ====
		public final static integer XZT21I_CSR_ACT_NBR := 9;
		public final static integer XZT21I_ACT_TMN_DT := 10;
		public final static integer XZT21I_NOT_TYP_CD := 5;
		public final static integer XZT21I_POL_NBR := 25;
		public final static integer XZT21I_POL_NBR_LIST := XZT21I_POL_NBR;
		public final static integer XZT21I_USERID := 8;
		public final static integer XZT21O_CSR_ACT_NBR := 9;
		public final static integer XZT021_SERVICE_INPUTS := XZT21I_CSR_ACT_NBR + XZT21I_ACT_TMN_DT + XZT21I_NOT_TYP_CD + LServiceContractAreaXz0x0021.XZT21I_POL_NBR_LIST_MAXOCCURS * XZT21I_POL_NBR_LIST + XZT21I_USERID;
		public final static integer XZT21O_NOT_DT := 10;
		public final static integer XZT021_SERVICE_OUTPUTS := XZT21O_CSR_ACT_NBR + XZT21O_NOT_DT;
		public final static integer L_SERVICE_CONTRACT_AREA := XZT021_SERVICE_INPUTS + XZT021_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//LServiceContractAreaXz0x0021