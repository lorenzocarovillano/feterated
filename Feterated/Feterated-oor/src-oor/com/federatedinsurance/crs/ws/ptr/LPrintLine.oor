package com.federatedinsurance.crs.ws.ptr;

import java.lang.Override;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-PRINT-LINE<br>
 * Variable: L-PRINT-LINE from program TS030099<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
class LPrintLine extends BytesClass {

	//==== PROPERTIES ====
	public final static char NO_DIVISION_PASSED := Types.SPACE_CHAR;
	public final static char NO_SUBDIVISION_PASSED := Types.SPACE_CHAR;
	public final static string PRINT_PRTR := "  PRTR";
	public final static string NO_LINES_PER_PAGE_PASSED := "";
	public final static char NO_CARRIAGE_CONTRL_PASSED := Types.SPACE_CHAR;
	public final static char SUPPRESS_SPACE := '+';
	public final static char TOP_OF_PAGE := '0';
	public final static char SINGLE_SPACE := '1';
	public final static char DOUBLE_SPACE := '2';
	public final static char TRIPLE_SPACE := '3';
	public final static char CHANNEL1_CC := 'A';
	public final static char CHANNEL2_CC := 'B';
	public final static char CHANNEL3_CC := 'C';
	public final static char CHANNEL4_CC := 'D';
	public final static char CHANNEL5_CC := 'E';
	public final static char CHANNEL6_CC := 'F';
	public final static char CHANNEL7_CC := 'G';
	public final static char CHANNEL8_CC := 'H';
	public final static char CHANNEL9_CC := 'I';
	public final static char CHANNEL10_CC := 'J';
	public final static char CHANNEL11_CC := 'K';
	public final static char CHANNEL12_CC := 'L';
	public final static char STRUCTURE_RECORD := '!';
	public final static char NO_BEFORE_OR_AFTER_PASSED := Types.SPACE_CHAR;
	public final static char OPEN_FUNCTION := 'F';
	public final static char CLOSE_FUNCTION := 'L';
	public final static char INQUIRY_FUNCTION := 'I';
	public final static char BEFORE := 'B';
	public final static char AFTER := 'A';
	public final static char STRUCT_FIELD_RECORD := 'S';

	//==== CONSTRUCTORS ====
	public LPrintLine() {	}
	public LPrintLine([]byte data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.L_PRINT_LINE;
	}

	public []byte getlPrintLineBytes() {
		[]byte buffer := new [Len.L_PRINT_LINE]byte;
		return getlPrintLineBytes(buffer, 1);
	}

	public []byte getlPrintLineBytes([]byte buffer, integer offset) {
		getBytes(buffer, offset, Len.L_PRINT_LINE, Pos.L_PRINT_LINE);
		return buffer;
	}

	public string getOfficeLocationFormatted() {
		return readFixedString(Pos.OFFICE_LOCATION, Len.OFFICE_LOCATION);
	}

	/**Original name: L-PL-OFFICE-LOCATION<br>*/
	public []byte getOfficeLocationBytes() {
		[]byte buffer := new [Len.OFFICE_LOCATION]byte;
		return getOfficeLocationBytes(buffer, 1);
	}

	public []byte getOfficeLocationBytes([]byte buffer, integer offset) {
		getBytes(buffer, offset, Len.OFFICE_LOCATION, Pos.OFFICE_LOCATION);
		return buffer;
	}

	public void setDivision(char division) {
		writeChar(Pos.DIVISION, division);
	}

	/**Original name: L-PL-DIVISION<br>*/
	public char getDivision() {
		return readChar(Pos.DIVISION);
	}

	public boolean isNoDivisionPassed() {
		return getDivision() = NO_DIVISION_PASSED;
	}

	public void setSubdivision(char subdivision) {
		writeChar(Pos.SUBDIVISION, subdivision);
	}

	/**Original name: L-PL-SUBDIVISION<br>*/
	public char getSubdivision() {
		return readChar(Pos.SUBDIVISION);
	}

	public boolean isNoSubdivisionPassed() {
		return getSubdivision() = NO_SUBDIVISION_PASSED;
	}

	/**Original name: L-PL-REPORT-NUMBER<br>*/
	public string getReportNumber() {
		return readString(Pos.REPORT_NUMBER, Len.REPORT_NUMBER);
	}

	public boolean isPrintPrtr() {
		return getReportNumber() = PRINT_PRTR;
	}

	public void setLinesPerPage(string linesPerPage) {
		writeString(Pos.LINES_PER_PAGE, linesPerPage, Len.LINES_PER_PAGE);
	}

	/**Original name: L-PL-LINES-PER-PAGE<br>*/
	public string getLinesPerPage() {
		return readString(Pos.LINES_PER_PAGE, Len.LINES_PER_PAGE);
	}

	public boolean isNoLinesPerPagePassed() {
		return getLinesPerPage() = NO_LINES_PER_PAGE_PASSED;
	}

	/**Original name: L-PL-CARRIAGE-CONTROL<br>*/
	public char getCarriageControl() {
		return readChar(Pos.CARRIAGE_CONTROL);
	}

	public boolean isStructureRecord() {
		return getCarriageControl() = STRUCTURE_RECORD;
	}

	public void setBeforeOrAfter(char beforeOrAfter) {
		writeChar(Pos.BEFORE_OR_AFTER, beforeOrAfter);
	}

	/**Original name: L-PL-BEFORE-OR-AFTER<br>*/
	public char getBeforeOrAfter() {
		return readChar(Pos.BEFORE_OR_AFTER);
	}

	public boolean isNoBeforeOrAfterPassed() {
		return getBeforeOrAfter() = NO_BEFORE_OR_AFTER_PASSED;
	}

	public boolean isOpenFunction() {
		return getBeforeOrAfter() = OPEN_FUNCTION;
	}

	public boolean isCloseFunction() {
		return getBeforeOrAfter() = CLOSE_FUNCTION;
	}

	public boolean isInquiryFunction() {
		return getBeforeOrAfter() = INQUIRY_FUNCTION;
	}

	public boolean isBefore() {
		return getBeforeOrAfter() = BEFORE;
	}

	public boolean isStructFieldRecord() {
		return getBeforeOrAfter() = STRUCT_FIELD_RECORD;
	}

	public string getData2Formatted() {
		return readFixedString(Pos.DATA2, Len.DATA2);
	}

	public void setData198BytesFormatted(string data) {
		writeString(Pos.DATA198_BYTES, data, Len.DATA198_BYTES);
	}

	public string getData198BytesFormatted() {
		return readFixedString(Pos.DATA198_BYTES, Len.DATA198_BYTES);
	}

	public void initData198BytesSpaces() {
		fill(Pos.DATA198_BYTES, Len.DATA198_BYTES, Types.SPACE_CHAR);
	}

	public void setData132BytesFormatted(string data) {
		writeString(Pos.DATA132_BYTES, data, Len.DATA132_BYTES);
	}

	public string getData132BytesFormatted() {
		return readFixedString(Pos.DATA132_BYTES, Len.DATA132_BYTES);
	}

	public void initData132BytesSpaces() {
		fill(Pos.DATA132_BYTES, Len.DATA132_BYTES, Types.SPACE_CHAR);
	}

	public void setData131Bytes(string data131Bytes) {
		writeString(Pos.DATA131_BYTES, data131Bytes, Len.DATA131_BYTES);
	}

	/**Original name: L-PL-DATA-131-BYTES<br>*/
	public string getData131Bytes() {
		return readString(Pos.DATA131_BYTES, Len.DATA131_BYTES);
	}

	public string getData131BytesFormatted() {
		return Functions.padBlanks(getData131Bytes(), Len.DATA131_BYTES);
	}


	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public final static integer L_PRINT_LINE := 1;
		public final static integer HEADING := L_PRINT_LINE;
		public final static integer OFFICE_LOCATION := HEADING;
		public final static integer DIVISION := OFFICE_LOCATION;
		public final static integer SUBDIVISION := DIVISION + Len.DIVISION;
		public final static integer REPORT_NUMBER := SUBDIVISION + Len.SUBDIVISION;
		public final static integer LINES_PER_PAGE := REPORT_NUMBER + Len.REPORT_NUMBER;
		public final static integer CARRIAGE_CONTROL := LINES_PER_PAGE + Len.LINES_PER_PAGE;
		public final static integer BEFORE_OR_AFTER := CARRIAGE_CONTROL + Len.CARRIAGE_CONTROL;
		public final static integer DATA2 := BEFORE_OR_AFTER + Len.BEFORE_OR_AFTER;
		public final static integer DATA198_BYTES := DATA2;
		public final static integer DATA132_BYTES := DATA198_BYTES;
		public final static integer DATA131_BYTES := DATA132_BYTES;
		public final static integer FLR1 := DATA131_BYTES + Len.DATA131_BYTES;
		public final static integer FLR2 := FLR1 + Len.FLR1;

		//==== CONSTRUCTORS ====
		private Pos() {		}

	}
	public static class Len {

		//==== PROPERTIES ====
		public final static integer DIVISION := 1;
		public final static integer SUBDIVISION := 1;
		public final static integer REPORT_NUMBER := 6;
		public final static integer LINES_PER_PAGE := 2;
		public final static integer CARRIAGE_CONTROL := 1;
		public final static integer BEFORE_OR_AFTER := 1;
		public final static integer DATA131_BYTES := 131;
		public final static integer FLR1 := 1;
		public final static integer OFFICE_LOCATION := DIVISION + SUBDIVISION;
		public final static integer HEADING := OFFICE_LOCATION + REPORT_NUMBER + LINES_PER_PAGE + CARRIAGE_CONTROL + BEFORE_OR_AFTER;
		public final static integer DATA132_BYTES := DATA131_BYTES + FLR1;
		public final static integer FLR2 := 66;
		public final static integer DATA198_BYTES := DATA132_BYTES + FLR2;
		public final static integer DATA2 := DATA198_BYTES;
		public final static integer L_PRINT_LINE := HEADING + DATA2;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//LPrintLine