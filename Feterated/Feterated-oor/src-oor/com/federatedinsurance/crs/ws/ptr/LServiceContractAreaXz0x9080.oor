package com.federatedinsurance.crs.ws.ptr;

import java.lang.Override;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9080<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
class LServiceContractAreaXz0x9080 extends BytesClass {

	//==== PROPERTIES ====
	public final static integer XZT98O_TTY_LIST_MAXOCCURS := 2500;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9080() {	}
	public LServiceContractAreaXz0x9080([]byte data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt98iTkNotPrcTs(string xzt98iTkNotPrcTs) {
		writeString(Pos.XZT98I_TK_NOT_PRC_TS, xzt98iTkNotPrcTs, Len.XZT98I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT98I-TK-NOT-PRC-TS<br>*/
	public string getXzt98iTkNotPrcTs() {
		return readString(Pos.XZT98I_TK_NOT_PRC_TS, Len.XZT98I_TK_NOT_PRC_TS);
	}

	public void setXzt98iCsrActNbr(string xzt98iCsrActNbr) {
		writeString(Pos.XZT98I_CSR_ACT_NBR, xzt98iCsrActNbr, Len.XZT98I_CSR_ACT_NBR);
	}

	/**Original name: XZT98I-CSR-ACT-NBR<br>*/
	public string getXzt98iCsrActNbr() {
		return readString(Pos.XZT98I_CSR_ACT_NBR, Len.XZT98I_CSR_ACT_NBR);
	}

	public void setXzt98iUserid(string xzt98iUserid) {
		writeString(Pos.XZT98I_USERID, xzt98iUserid, Len.XZT98I_USERID);
	}

	/**Original name: XZT98I-USERID<br>*/
	public string getXzt98iUserid() {
		return readString(Pos.XZT98I_USERID, Len.XZT98I_USERID);
	}

	public string getXzt98iUseridFormatted() {
		return Functions.padBlanks(getXzt98iUserid(), Len.XZT98I_USERID);
	}

	public void setXzt98oTkNotPrcTs(string xzt98oTkNotPrcTs) {
		writeString(Pos.XZT98O_TK_NOT_PRC_TS, xzt98oTkNotPrcTs, Len.XZT98O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT98O-TK-NOT-PRC-TS<br>*/
	public string getXzt98oTkNotPrcTs() {
		return readString(Pos.XZT98O_TK_NOT_PRC_TS, Len.XZT98O_TK_NOT_PRC_TS);
	}

	public void setXzt98oCsrActNbr(string xzt98oCsrActNbr) {
		writeString(Pos.XZT98O_CSR_ACT_NBR, xzt98oCsrActNbr, Len.XZT98O_CSR_ACT_NBR);
	}

	/**Original name: XZT98O-CSR-ACT-NBR<br>*/
	public string getXzt98oCsrActNbr() {
		return readString(Pos.XZT98O_CSR_ACT_NBR, Len.XZT98O_CSR_ACT_NBR);
	}

	public void setXzt98oTkClientId(integer xzt98oTkClientIdIdx, string xzt98oTkClientId) {
		integer position := Pos.xzt98oTkClientId(xzt98oTkClientIdIdx - 1);
		writeString(position, xzt98oTkClientId, Len.XZT98O_TK_CLIENT_ID);
	}

	/**Original name: XZT98O-TK-CLIENT-ID<br>*/
	public string getXzt98oTkClientId(integer xzt98oTkClientIdIdx) {
		integer position := Pos.xzt98oTkClientId(xzt98oTkClientIdIdx - 1);
		return readString(position, Len.XZT98O_TK_CLIENT_ID);
	}

	public void setXzt98oTkAdrId(integer xzt98oTkAdrIdIdx, string xzt98oTkAdrId) {
		integer position := Pos.xzt98oTkAdrId(xzt98oTkAdrIdIdx - 1);
		writeString(position, xzt98oTkAdrId, Len.XZT98O_TK_ADR_ID);
	}

	/**Original name: XZT98O-TK-ADR-ID<br>*/
	public string getXzt98oTkAdrId(integer xzt98oTkAdrIdIdx) {
		integer position := Pos.xzt98oTkAdrId(xzt98oTkAdrIdIdx - 1);
		return readString(position, Len.XZT98O_TK_ADR_ID);
	}

	public void setXzt98oRecTypCd(integer xzt98oRecTypCdIdx, string xzt98oRecTypCd) {
		integer position := Pos.xzt98oRecTypCd(xzt98oRecTypCdIdx - 1);
		writeString(position, xzt98oRecTypCd, Len.XZT98O_REC_TYP_CD);
	}

	/**Original name: XZT98O-REC-TYP-CD<br>*/
	public string getXzt98oRecTypCd(integer xzt98oRecTypCdIdx) {
		integer position := Pos.xzt98oRecTypCd(xzt98oRecTypCdIdx - 1);
		return readString(position, Len.XZT98O_REC_TYP_CD);
	}

	public void setXzt98oRecTypDes(integer xzt98oRecTypDesIdx, string xzt98oRecTypDes) {
		integer position := Pos.xzt98oRecTypDes(xzt98oRecTypDesIdx - 1);
		writeString(position, xzt98oRecTypDes, Len.XZT98O_REC_TYP_DES);
	}

	/**Original name: XZT98O-REC-TYP-DES<br>*/
	public string getXzt98oRecTypDes(integer xzt98oRecTypDesIdx) {
		integer position := Pos.xzt98oRecTypDes(xzt98oRecTypDesIdx - 1);
		return readString(position, Len.XZT98O_REC_TYP_DES);
	}

	public void setXzt98oName(integer xzt98oNameIdx, string xzt98oName) {
		integer position := Pos.xzt98oName(xzt98oNameIdx - 1);
		writeString(position, xzt98oName, Len.XZT98O_NAME);
	}

	/**Original name: XZT98O-NAME<br>*/
	public string getXzt98oName(integer xzt98oNameIdx) {
		integer position := Pos.xzt98oName(xzt98oNameIdx - 1);
		return readString(position, Len.XZT98O_NAME);
	}

	public void setXzt98oAdrLin1(integer xzt98oAdrLin1Idx, string xzt98oAdrLin1) {
		integer position := Pos.xzt98oAdrLin1(xzt98oAdrLin1Idx - 1);
		writeString(position, xzt98oAdrLin1, Len.XZT98O_ADR_LIN1);
	}

	/**Original name: XZT98O-ADR-LIN1<br>*/
	public string getXzt98oAdrLin1(integer xzt98oAdrLin1Idx) {
		integer position := Pos.xzt98oAdrLin1(xzt98oAdrLin1Idx - 1);
		return readString(position, Len.XZT98O_ADR_LIN1);
	}

	public void setXzt98oAdrLin2(integer xzt98oAdrLin2Idx, string xzt98oAdrLin2) {
		integer position := Pos.xzt98oAdrLin2(xzt98oAdrLin2Idx - 1);
		writeString(position, xzt98oAdrLin2, Len.XZT98O_ADR_LIN2);
	}

	/**Original name: XZT98O-ADR-LIN2<br>*/
	public string getXzt98oAdrLin2(integer xzt98oAdrLin2Idx) {
		integer position := Pos.xzt98oAdrLin2(xzt98oAdrLin2Idx - 1);
		return readString(position, Len.XZT98O_ADR_LIN2);
	}

	public void setXzt98oCityNm(integer xzt98oCityNmIdx, string xzt98oCityNm) {
		integer position := Pos.xzt98oCityNm(xzt98oCityNmIdx - 1);
		writeString(position, xzt98oCityNm, Len.XZT98O_CITY_NM);
	}

	/**Original name: XZT98O-CITY-NM<br>*/
	public string getXzt98oCityNm(integer xzt98oCityNmIdx) {
		integer position := Pos.xzt98oCityNm(xzt98oCityNmIdx - 1);
		return readString(position, Len.XZT98O_CITY_NM);
	}

	public void setXzt98oStateAbb(integer xzt98oStateAbbIdx, string xzt98oStateAbb) {
		integer position := Pos.xzt98oStateAbb(xzt98oStateAbbIdx - 1);
		writeString(position, xzt98oStateAbb, Len.XZT98O_STATE_ABB);
	}

	/**Original name: XZT98O-STATE-ABB<br>*/
	public string getXzt98oStateAbb(integer xzt98oStateAbbIdx) {
		integer position := Pos.xzt98oStateAbb(xzt98oStateAbbIdx - 1);
		return readString(position, Len.XZT98O_STATE_ABB);
	}

	public void setXzt98oPstCd(integer xzt98oPstCdIdx, string xzt98oPstCd) {
		integer position := Pos.xzt98oPstCd(xzt98oPstCdIdx - 1);
		writeString(position, xzt98oPstCd, Len.XZT98O_PST_CD);
	}

	/**Original name: XZT98O-PST-CD<br>*/
	public string getXzt98oPstCd(integer xzt98oPstCdIdx) {
		integer position := Pos.xzt98oPstCd(xzt98oPstCdIdx - 1);
		return readString(position, Len.XZT98O_PST_CD);
	}


	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public final static integer L_SERVICE_CONTRACT_AREA := 1;
		public final static integer XZT908_SERVICE_INPUTS := L_SERVICE_CONTRACT_AREA;
		public final static integer XZT98I_TECHNICAL_KEY := XZT908_SERVICE_INPUTS;
		public final static integer XZT98I_TK_NOT_PRC_TS := XZT98I_TECHNICAL_KEY;
		public final static integer XZT98I_CSR_ACT_NBR := XZT98I_TK_NOT_PRC_TS + Len.XZT98I_TK_NOT_PRC_TS;
		public final static integer XZT98I_USERID := XZT98I_CSR_ACT_NBR + Len.XZT98I_CSR_ACT_NBR;
		public final static integer XZT908_SERVICE_OUTPUTS := XZT98I_USERID + Len.XZT98I_USERID;
		public final static integer XZT98O_TECHNICAL_KEY := XZT908_SERVICE_OUTPUTS;
		public final static integer XZT98O_TK_NOT_PRC_TS := XZT98O_TECHNICAL_KEY;
		public final static integer XZT98O_CSR_ACT_NBR := XZT98O_TK_NOT_PRC_TS + Len.XZT98O_TK_NOT_PRC_TS;
		public final static integer XZT98O_TTY_LIST_TBL := XZT98O_CSR_ACT_NBR + Len.XZT98O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {		}

		//==== METHODS ====
		public static integer xzt98oTtyList(integer idx) {
			return XZT98O_TTY_LIST_TBL + idx * Len.XZT98O_TTY_LIST;
		}

		public static integer xzt98oTtyTechnicalKey(integer idx) {
			return xzt98oTtyList(idx);
		}

		public static integer xzt98oTkClientId(integer idx) {
			return xzt98oTtyTechnicalKey(idx);
		}

		public static integer xzt98oTkAdrId(integer idx) {
			return xzt98oTkClientId(idx) + Len.XZT98O_TK_CLIENT_ID;
		}

		public static integer xzt98oRecTyp(integer idx) {
			return xzt98oTkAdrId(idx) + Len.XZT98O_TK_ADR_ID;
		}

		public static integer xzt98oRecTypCd(integer idx) {
			return xzt98oRecTyp(idx);
		}

		public static integer xzt98oRecTypDes(integer idx) {
			return xzt98oRecTypCd(idx) + Len.XZT98O_REC_TYP_CD;
		}

		public static integer xzt98oName(integer idx) {
			return xzt98oRecTypDes(idx) + Len.XZT98O_REC_TYP_DES;
		}

		public static integer xzt98oAdrLin1(integer idx) {
			return xzt98oName(idx) + Len.XZT98O_NAME;
		}

		public static integer xzt98oAdrLin2(integer idx) {
			return xzt98oAdrLin1(idx) + Len.XZT98O_ADR_LIN1;
		}

		public static integer xzt98oCityNm(integer idx) {
			return xzt98oAdrLin2(idx) + Len.XZT98O_ADR_LIN2;
		}

		public static integer xzt98oStateAbb(integer idx) {
			return xzt98oCityNm(idx) + Len.XZT98O_CITY_NM;
		}

		public static integer xzt98oPstCd(integer idx) {
			return xzt98oStateAbb(idx) + Len.XZT98O_STATE_ABB;
		}

	}
	public static class Len {

		//==== PROPERTIES ====
		public final static integer XZT98I_TK_NOT_PRC_TS := 26;
		public final static integer XZT98I_CSR_ACT_NBR := 9;
		public final static integer XZT98I_USERID := 8;
		public final static integer XZT98O_TK_NOT_PRC_TS := 26;
		public final static integer XZT98O_CSR_ACT_NBR := 9;
		public final static integer XZT98O_TK_CLIENT_ID := 64;
		public final static integer XZT98O_TK_ADR_ID := 64;
		public final static integer XZT98O_TTY_TECHNICAL_KEY := XZT98O_TK_CLIENT_ID + XZT98O_TK_ADR_ID;
		public final static integer XZT98O_REC_TYP_CD := 5;
		public final static integer XZT98O_REC_TYP_DES := 13;
		public final static integer XZT98O_REC_TYP := XZT98O_REC_TYP_CD + XZT98O_REC_TYP_DES;
		public final static integer XZT98O_NAME := 120;
		public final static integer XZT98O_ADR_LIN1 := 45;
		public final static integer XZT98O_ADR_LIN2 := 45;
		public final static integer XZT98O_CITY_NM := 30;
		public final static integer XZT98O_STATE_ABB := 2;
		public final static integer XZT98O_PST_CD := 13;
		public final static integer XZT98O_TTY_LIST := XZT98O_TTY_TECHNICAL_KEY + XZT98O_REC_TYP + XZT98O_NAME + XZT98O_ADR_LIN1 + XZT98O_ADR_LIN2 + XZT98O_CITY_NM + XZT98O_STATE_ABB + XZT98O_PST_CD;
		public final static integer XZT98I_TECHNICAL_KEY := XZT98I_TK_NOT_PRC_TS;
		public final static integer XZT908_SERVICE_INPUTS := XZT98I_TECHNICAL_KEY + XZT98I_CSR_ACT_NBR + XZT98I_USERID;
		public final static integer XZT98O_TECHNICAL_KEY := XZT98O_TK_NOT_PRC_TS;
		public final static integer XZT98O_TTY_LIST_TBL := LServiceContractAreaXz0x9080.XZT98O_TTY_LIST_MAXOCCURS * XZT98O_TTY_LIST;
		public final static integer XZT908_SERVICE_OUTPUTS := XZT98O_TECHNICAL_KEY + XZT98O_CSR_ACT_NBR + XZT98O_TTY_LIST_TBL;
		public final static integer L_SERVICE_CONTRACT_AREA := XZT908_SERVICE_INPUTS + XZT908_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//LServiceContractAreaXz0x9080