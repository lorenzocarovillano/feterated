package com.federatedinsurance.crs.ws.ptr;

import java.lang.Override;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program MU0X0004<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
class DfhcommareaMu0x0004 extends BytesClass {

	//==== PROPERTIES ====
	public final static integer PPC_NON_LOGGABLE_ERRORS_MAXOCCURS := 10;
	public final static integer PPC_WARNINGS_MAXOCCURS := 10;
	public final static char PPC_BYPASS_SYNCPOINT_IN_MDRV := 'B';
	private final static []char PPC_DO_NOT_BYPASS_SYNCPOINT := {Types.SPACE_CHAR,Types.LOW_CHAR_VAL,Types.HIGH_CHAR_VAL};
	public final static string PPC_FATAL_ERROR_CODE := "0300";
	public final static string PPC_NLBE_CODE := "0200";
	public final static string PPC_WARNING_CODE := "0100";
	public final static string PPC_NO_ERROR_CODE := "0000";

	//==== CONSTRUCTORS ====
	public DfhcommareaMu0x0004() {	}
	public DfhcommareaMu0x0004([]byte data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.DFHCOMMAREA;
	}

	public void setDfhcommareaBytes([]byte buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public []byte getDfhcommareaBytes() {
		[]byte buffer := new [Len.DFHCOMMAREA]byte;
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes([]byte buffer, integer offset) {
		setBytes(buffer, offset, Len.DFHCOMMAREA, Pos.DFHCOMMAREA);
	}

	public []byte getDfhcommareaBytes([]byte buffer, integer offset) {
		getBytes(buffer, offset, Len.DFHCOMMAREA, Pos.DFHCOMMAREA);
		return buffer;
	}

	public void setPpcInputParmsBytes([]byte buffer) {
		setPpcInputParmsBytes(buffer, 1);
	}

	public void setPpcInputParmsBytes([]byte buffer, integer offset) {
		setBytes(buffer, offset, Len.PPC_INPUT_PARMS, Pos.PPC_INPUT_PARMS);
	}

	public void setPpcServiceDataSize(integer ppcServiceDataSize) {
		writeBinaryInt(Pos.PPC_SERVICE_DATA_SIZE, ppcServiceDataSize);
	}

	/**Original name: PPC-SERVICE-DATA-SIZE<br>*/
	public integer getPpcServiceDataSize() {
		return readBinaryInt(Pos.PPC_SERVICE_DATA_SIZE);
	}

	public integer getPpcServiceDataPointer() {
		return readBinaryInt(Pos.PPC_SERVICE_DATA_POINTER);
	}

	public void setPpcBypassSyncpointMdrvInd(char ppcBypassSyncpointMdrvInd) {
		writeChar(Pos.PPC_BYPASS_SYNCPOINT_MDRV_IND, ppcBypassSyncpointMdrvInd);
	}

	/**Original name: PPC-BYPASS-SYNCPOINT-MDRV-IND<br>
	 * <pre>* FLAG TO INDICATE WHETHER OR NOT MAIN DRIVER DOES SYNCPOINT</pre>*/
	public char getPpcBypassSyncpointMdrvInd() {
		return readChar(Pos.PPC_BYPASS_SYNCPOINT_MDRV_IND);
	}

	public boolean isPpcBypassSyncpointInMdrv() {
		return getPpcBypassSyncpointMdrvInd() = PPC_BYPASS_SYNCPOINT_IN_MDRV;
	}

	public void setPpcOperation(string ppcOperation) {
		writeString(Pos.PPC_OPERATION, ppcOperation, Len.PPC_OPERATION);
	}

	/**Original name: PPC-OPERATION<br>*/
	public string getPpcOperation() {
		return readString(Pos.PPC_OPERATION, Len.PPC_OPERATION);
	}

	/**Original name: PPC-OUTPUT-PARMS<br>*/
	public []byte getPpcOutputParmsBytes() {
		[]byte buffer := new [Len.PPC_OUTPUT_PARMS]byte;
		return getPpcOutputParmsBytes(buffer, 1);
	}

	public []byte getPpcOutputParmsBytes([]byte buffer, integer offset) {
		getBytes(buffer, offset, Len.PPC_OUTPUT_PARMS, Pos.PPC_OUTPUT_PARMS);
		return buffer;
	}

	public void setPpcErrorHandlingParmsBytes([]byte buffer) {
		setPpcErrorHandlingParmsBytes(buffer, 1);
	}

	public void setPpcErrorHandlingParmsBytes([]byte buffer, integer offset) {
		setBytes(buffer, offset, Len.PPC_ERROR_HANDLING_PARMS, Pos.PPC_ERROR_HANDLING_PARMS);
	}

	public void setPpcErrorReturnCode(short ppcErrorReturnCode) {
		writeShort(Pos.PPC_ERROR_RETURN_CODE, ppcErrorReturnCode, Len.Int.PPC_ERROR_RETURN_CODE, SignType.NO_SIGN);
	}

	public void setPpcErrorReturnCodeFormatted(string ppcErrorReturnCode) {
		writeString(Pos.PPC_ERROR_RETURN_CODE, Trunc.toUnsignedNumeric(ppcErrorReturnCode, Len.PPC_ERROR_RETURN_CODE), Len.PPC_ERROR_RETURN_CODE);
	}

	/**Original name: PPC-ERROR-RETURN-CODE<br>*/
	public short getPpcErrorReturnCode() {
		return readNumDispUnsignedShort(Pos.PPC_ERROR_RETURN_CODE, Len.PPC_ERROR_RETURN_CODE);
	}

	public string getPpcErrorReturnCodeFormatted() {
		return readFixedString(Pos.PPC_ERROR_RETURN_CODE, Len.PPC_ERROR_RETURN_CODE);
	}

	public boolean isPpcFatalErrorCode() {
		return getPpcErrorReturnCodeFormatted() = PPC_FATAL_ERROR_CODE;
	}

	public void setPpcFatalErrorCode() {
		setPpcErrorReturnCodeFormatted(PPC_FATAL_ERROR_CODE);
	}

	public boolean isPpcNlbeCode() {
		return getPpcErrorReturnCodeFormatted() = PPC_NLBE_CODE;
	}

	public boolean isPpcWarningCode() {
		return getPpcErrorReturnCodeFormatted() = PPC_WARNING_CODE;
	}

	public boolean isPpcNoErrorCode() {
		return getPpcErrorReturnCodeFormatted() = PPC_NO_ERROR_CODE;
	}

	public void setPpcNoErrorCode() {
		setPpcErrorReturnCodeFormatted(PPC_NO_ERROR_CODE);
	}

	public void setPpcFatalErrorMessage(string ppcFatalErrorMessage) {
		writeString(Pos.PPC_FATAL_ERROR_MESSAGE, ppcFatalErrorMessage, Len.PPC_FATAL_ERROR_MESSAGE);
	}

	/**Original name: PPC-FATAL-ERROR-MESSAGE<br>*/
	public string getPpcFatalErrorMessage() {
		return readString(Pos.PPC_FATAL_ERROR_MESSAGE, Len.PPC_FATAL_ERROR_MESSAGE);
	}

	public string getPpcFatalErrorMessageFormatted() {
		return Functions.padBlanks(getPpcFatalErrorMessage(), Len.PPC_FATAL_ERROR_MESSAGE);
	}

	public void setPpcNonLoggableErrorCnt(short ppcNonLoggableErrorCnt) {
		writeShort(Pos.PPC_NON_LOGGABLE_ERROR_CNT, ppcNonLoggableErrorCnt, Len.Int.PPC_NON_LOGGABLE_ERROR_CNT, SignType.NO_SIGN);
	}

	public void setPpcNonLoggableErrorCntFormatted(string ppcNonLoggableErrorCnt) {
		writeString(Pos.PPC_NON_LOGGABLE_ERROR_CNT, Trunc.toUnsignedNumeric(ppcNonLoggableErrorCnt, Len.PPC_NON_LOGGABLE_ERROR_CNT), Len.PPC_NON_LOGGABLE_ERROR_CNT);
	}

	/**Original name: PPC-NON-LOGGABLE-ERROR-CNT<br>*/
	public short getPpcNonLoggableErrorCnt() {
		return readNumDispUnsignedShort(Pos.PPC_NON_LOGGABLE_ERROR_CNT, Len.PPC_NON_LOGGABLE_ERROR_CNT);
	}

	public void setPpcNonLogErrMsg(integer ppcNonLogErrMsgIdx, string ppcNonLogErrMsg) {
		integer position := Pos.ppcNonLogErrMsg(ppcNonLogErrMsgIdx - 1);
		writeString(position, ppcNonLogErrMsg, Len.PPC_NON_LOG_ERR_MSG);
	}

	/**Original name: PPC-NON-LOG-ERR-MSG<br>*/
	public string getPpcNonLogErrMsg(integer ppcNonLogErrMsgIdx) {
		integer position := Pos.ppcNonLogErrMsg(ppcNonLogErrMsgIdx - 1);
		return readString(position, Len.PPC_NON_LOG_ERR_MSG);
	}

	public void setPpcWarningCnt(short ppcWarningCnt) {
		writeShort(Pos.PPC_WARNING_CNT, ppcWarningCnt, Len.Int.PPC_WARNING_CNT, SignType.NO_SIGN);
	}

	public void setPpcWarningCntFormatted(string ppcWarningCnt) {
		writeString(Pos.PPC_WARNING_CNT, Trunc.toUnsignedNumeric(ppcWarningCnt, Len.PPC_WARNING_CNT), Len.PPC_WARNING_CNT);
	}

	/**Original name: PPC-WARNING-CNT<br>*/
	public short getPpcWarningCnt() {
		return readNumDispUnsignedShort(Pos.PPC_WARNING_CNT, Len.PPC_WARNING_CNT);
	}

	public void setPpcWarnMsg(integer ppcWarnMsgIdx, string ppcWarnMsg) {
		integer position := Pos.ppcWarnMsg(ppcWarnMsgIdx - 1);
		writeString(position, ppcWarnMsg, Len.PPC_WARN_MSG);
	}

	/**Original name: PPC-WARN-MSG<br>*/
	public string getPpcWarnMsg(integer ppcWarnMsgIdx) {
		integer position := Pos.ppcWarnMsg(ppcWarnMsgIdx - 1);
		return readString(position, Len.PPC_WARN_MSG);
	}


	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public final static integer DFHCOMMAREA := 1;
		public final static integer PROXY_PROGRAM_COMMON := DFHCOMMAREA;
		public final static integer PPC_INPUT_PARMS := PROXY_PROGRAM_COMMON;
		public final static integer PPC_MEMORY_ALLOCATION_PARMS := PPC_INPUT_PARMS;
		public final static integer PPC_SERVICE_DATA_SIZE := PPC_MEMORY_ALLOCATION_PARMS;
		public final static integer PPC_SERVICE_DATA_POINTER := PPC_SERVICE_DATA_SIZE + Len.PPC_SERVICE_DATA_SIZE;
		public final static integer PPC_BYPASS_SYNCPOINT_MDRV_IND := PPC_SERVICE_DATA_POINTER + Len.PPC_SERVICE_DATA_POINTER;
		public final static integer PPC_OPERATION := PPC_BYPASS_SYNCPOINT_MDRV_IND + Len.PPC_BYPASS_SYNCPOINT_MDRV_IND;
		public final static integer PPC_OUTPUT_PARMS := PPC_OPERATION + Len.PPC_OPERATION;
		public final static integer PPC_ERROR_HANDLING_PARMS := PPC_OUTPUT_PARMS;
		public final static integer PPC_ERROR_RETURN_CODE := PPC_ERROR_HANDLING_PARMS;
		public final static integer PPC_FATAL_ERROR_MESSAGE := PPC_ERROR_RETURN_CODE + Len.PPC_ERROR_RETURN_CODE;
		public final static integer PPC_NON_LOGGABLE_ERROR_CNT := PPC_FATAL_ERROR_MESSAGE + Len.PPC_FATAL_ERROR_MESSAGE;
		public final static integer PPC_WARNING_CNT := ppcNonLogErrMsg(PPC_NON_LOGGABLE_ERRORS_MAXOCCURS - 1) + Len.PPC_NON_LOG_ERR_MSG;

		//==== CONSTRUCTORS ====
		private Pos() {		}

		//==== METHODS ====
		public static integer ppcNonLoggableErrors(integer idx) {
			return PPC_NON_LOGGABLE_ERROR_CNT + Len.PPC_NON_LOGGABLE_ERROR_CNT + idx * Len.PPC_NON_LOGGABLE_ERRORS;
		}

		public static integer ppcNonLogErrMsg(integer idx) {
			return ppcNonLoggableErrors(idx);
		}

		public static integer ppcWarnings(integer idx) {
			return PPC_WARNING_CNT + Len.PPC_WARNING_CNT + idx * Len.PPC_WARNINGS;
		}

		public static integer ppcWarnMsg(integer idx) {
			return ppcWarnings(idx);
		}

	}
	public static class Len {

		//==== PROPERTIES ====
		public final static integer PPC_SERVICE_DATA_SIZE := 4;
		public final static integer PPC_SERVICE_DATA_POINTER := 4;
		public final static integer PPC_BYPASS_SYNCPOINT_MDRV_IND := 1;
		public final static integer PPC_OPERATION := 32;
		public final static integer PPC_ERROR_RETURN_CODE := 4;
		public final static integer PPC_FATAL_ERROR_MESSAGE := 250;
		public final static integer PPC_NON_LOGGABLE_ERROR_CNT := 4;
		public final static integer PPC_NON_LOG_ERR_MSG := 500;
		public final static integer PPC_NON_LOGGABLE_ERRORS := PPC_NON_LOG_ERR_MSG;
		public final static integer PPC_WARNING_CNT := 4;
		public final static integer PPC_WARN_MSG := 500;
		public final static integer PPC_WARNINGS := PPC_WARN_MSG;
		public final static integer PPC_MEMORY_ALLOCATION_PARMS := PPC_SERVICE_DATA_SIZE + PPC_SERVICE_DATA_POINTER;
		public final static integer PPC_INPUT_PARMS := PPC_MEMORY_ALLOCATION_PARMS + PPC_BYPASS_SYNCPOINT_MDRV_IND + PPC_OPERATION;
		public final static integer PPC_ERROR_HANDLING_PARMS := PPC_ERROR_RETURN_CODE + PPC_FATAL_ERROR_MESSAGE + PPC_NON_LOGGABLE_ERROR_CNT + DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS * PPC_NON_LOGGABLE_ERRORS + PPC_WARNING_CNT + DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS * PPC_WARNINGS;
		public final static integer PPC_OUTPUT_PARMS := PPC_ERROR_HANDLING_PARMS;
		public final static integer PROXY_PROGRAM_COMMON := PPC_INPUT_PARMS + PPC_OUTPUT_PARMS;
		public final static integer DFHCOMMAREA := PROXY_PROGRAM_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer PPC_ERROR_RETURN_CODE := 4;
			public final static integer PPC_NON_LOGGABLE_ERROR_CNT := 4;
			public final static integer PPC_WARNING_CNT := 4;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//DfhcommareaMu0x0004