package com.federatedinsurance.crs.ws.ptr;

import java.lang.Override;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9080<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
class LFrameworkResponseAreaXz0r9080 extends BytesClass {

	//==== PROPERTIES ====
	public final static integer L_FW_RESP_XZ0A9080_MAXOCCURS := 1000;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9080() {	}
	public LFrameworkResponseAreaXz0r9080([]byte data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public string getlFwRespXz0a9080Formatted(integer lFwRespXz0a9080Idx) {
		integer position := Pos.lFwRespXz0a9080(lFwRespXz0a9080Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9080);
	}

	public void setlFwRespXz0a9080Bytes(integer lFwRespXz0a9080Idx, []byte buffer) {
		setlFwRespXz0a9080Bytes(lFwRespXz0a9080Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A9080<br>*/
	public []byte getlFwRespXz0a9080Bytes(integer lFwRespXz0a9080Idx) {
		[]byte buffer := new [Len.L_FW_RESP_XZ0A9080]byte;
		return getlFwRespXz0a9080Bytes(lFwRespXz0a9080Idx, buffer, 1);
	}

	public void setlFwRespXz0a9080Bytes(integer lFwRespXz0a9080Idx, []byte buffer, integer offset) {
		integer position := Pos.lFwRespXz0a9080(lFwRespXz0a9080Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A9080, position);
	}

	public []byte getlFwRespXz0a9080Bytes(integer lFwRespXz0a9080Idx, []byte buffer, integer offset) {
		integer position := Pos.lFwRespXz0a9080(lFwRespXz0a9080Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A9080, position);
		return buffer;
	}


	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public final static integer L_FRAMEWORK_RESPONSE_AREA := 1;

		//==== CONSTRUCTORS ====
		private Pos() {		}

		//==== METHODS ====
		public static integer lFwRespXz0a9080(integer idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9080;
		}

		public static integer xza980TtyInfoRow(integer idx) {
			return lFwRespXz0a9080(idx);
		}

		public static integer xza980MaxTtyRows(integer idx) {
			return xza980TtyInfoRow(idx);
		}

		public static integer xza980CsrActNbr(integer idx) {
			return xza980MaxTtyRows(idx) + Len.XZA980_MAX_TTY_ROWS;
		}

		public static integer xza980NotPrcTs(integer idx) {
			return xza980CsrActNbr(idx) + Len.XZA980_CSR_ACT_NBR;
		}

		public static integer xza980Userid(integer idx) {
			return xza980NotPrcTs(idx) + Len.XZA980_NOT_PRC_TS;
		}

		public static integer xza980TtyList(integer idx) {
			return xza980Userid(idx) + Len.XZA980_USERID;
		}

		public static integer xza980ClientId(integer idx) {
			return xza980TtyList(idx);
		}

		public static integer xza980AdrId(integer idx) {
			return xza980ClientId(idx) + Len.XZA980_CLIENT_ID;
		}

		public static integer xza980RecTypCd(integer idx) {
			return xza980AdrId(idx) + Len.XZA980_ADR_ID;
		}

		public static integer xza980RecTypDes(integer idx) {
			return xza980RecTypCd(idx) + Len.XZA980_REC_TYP_CD;
		}

		public static integer xza980Name(integer idx) {
			return xza980RecTypDes(idx) + Len.XZA980_REC_TYP_DES;
		}

		public static integer xza980AdrLin1(integer idx) {
			return xza980Name(idx) + Len.XZA980_NAME;
		}

		public static integer xza980AdrLin2(integer idx) {
			return xza980AdrLin1(idx) + Len.XZA980_ADR_LIN1;
		}

		public static integer xza980CityNm(integer idx) {
			return xza980AdrLin2(idx) + Len.XZA980_ADR_LIN2;
		}

		public static integer xza980StateAbb(integer idx) {
			return xza980CityNm(idx) + Len.XZA980_CITY_NM;
		}

		public static integer xza980PstCd(integer idx) {
			return xza980StateAbb(idx) + Len.XZA980_STATE_ABB;
		}

	}
	public static class Len {

		//==== PROPERTIES ====
		public final static integer XZA980_MAX_TTY_ROWS := 2;
		public final static integer XZA980_CSR_ACT_NBR := 9;
		public final static integer XZA980_NOT_PRC_TS := 26;
		public final static integer XZA980_USERID := 8;
		public final static integer XZA980_CLIENT_ID := 64;
		public final static integer XZA980_ADR_ID := 64;
		public final static integer XZA980_REC_TYP_CD := 5;
		public final static integer XZA980_REC_TYP_DES := 13;
		public final static integer XZA980_NAME := 120;
		public final static integer XZA980_ADR_LIN1 := 45;
		public final static integer XZA980_ADR_LIN2 := 45;
		public final static integer XZA980_CITY_NM := 30;
		public final static integer XZA980_STATE_ABB := 2;
		public final static integer XZA980_PST_CD := 13;
		public final static integer XZA980_TTY_LIST := XZA980_CLIENT_ID + XZA980_ADR_ID + XZA980_REC_TYP_CD + XZA980_REC_TYP_DES + XZA980_NAME + XZA980_ADR_LIN1 + XZA980_ADR_LIN2 + XZA980_CITY_NM + XZA980_STATE_ABB + XZA980_PST_CD;
		public final static integer XZA980_TTY_INFO_ROW := XZA980_MAX_TTY_ROWS + XZA980_CSR_ACT_NBR + XZA980_NOT_PRC_TS + XZA980_USERID + XZA980_TTY_LIST;
		public final static integer L_FW_RESP_XZ0A9080 := XZA980_TTY_INFO_ROW;
		public final static integer L_FRAMEWORK_RESPONSE_AREA := LFrameworkResponseAreaXz0r9080.L_FW_RESP_XZ0A9080_MAXOCCURS * L_FW_RESP_XZ0A9080;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//LFrameworkResponseAreaXz0r9080