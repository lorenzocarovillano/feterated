package com.federatedinsurance.crs.ws.ptr;

import java.lang.Override;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9050<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
class LServiceContractAreaXz0x9050 extends BytesClass {

	//==== PROPERTIES ====
	public final static integer O_POLICY_ROW_MAXOCCURS := 100;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9050() {	}
	public LServiceContractAreaXz0x9050([]byte data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiTkNotPrcTs(string iTkNotPrcTs) {
		writeString(Pos.I_TK_NOT_PRC_TS, iTkNotPrcTs, Len.I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT95I-TK-NOT-PRC-TS<br>*/
	public string getiTkNotPrcTs() {
		return readString(Pos.I_TK_NOT_PRC_TS, Len.I_TK_NOT_PRC_TS);
	}

	public void setiTkFrmSeqNbr(integer iTkFrmSeqNbr) {
		writeInt(Pos.I_TK_FRM_SEQ_NBR, iTkFrmSeqNbr, Len.Int.I_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT95I-TK-FRM-SEQ-NBR<br>*/
	public integer getiTkFrmSeqNbr() {
		return readNumDispInt(Pos.I_TK_FRM_SEQ_NBR, Len.I_TK_FRM_SEQ_NBR);
	}

	public void setiCsrActNbr(string iCsrActNbr) {
		writeString(Pos.I_CSR_ACT_NBR, iCsrActNbr, Len.I_CSR_ACT_NBR);
	}

	/**Original name: XZT95I-CSR-ACT-NBR<br>*/
	public string getiCsrActNbr() {
		return readString(Pos.I_CSR_ACT_NBR, Len.I_CSR_ACT_NBR);
	}

	public void setiUserid(string iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT95I-USERID<br>*/
	public string getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	public string getiUseridFormatted() {
		return Functions.padBlanks(getiUserid(), Len.I_USERID);
	}

	public void setoTkNotPrcTs(string oTkNotPrcTs) {
		writeString(Pos.O_TK_NOT_PRC_TS, oTkNotPrcTs, Len.O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT95O-TK-NOT-PRC-TS<br>*/
	public string getoTkNotPrcTs() {
		return readString(Pos.O_TK_NOT_PRC_TS, Len.O_TK_NOT_PRC_TS);
	}

	public string getoTkNotPrcTsFormatted() {
		return Functions.padBlanks(getoTkNotPrcTs(), Len.O_TK_NOT_PRC_TS);
	}

	public void setoCsrActNbr(string oCsrActNbr) {
		writeString(Pos.O_CSR_ACT_NBR, oCsrActNbr, Len.O_CSR_ACT_NBR);
	}

	/**Original name: XZT95O-CSR-ACT-NBR<br>*/
	public string getoCsrActNbr() {
		return readString(Pos.O_CSR_ACT_NBR, Len.O_CSR_ACT_NBR);
	}

	public string getoCsrActNbrFormatted() {
		return Functions.padBlanks(getoCsrActNbr(), Len.O_CSR_ACT_NBR);
	}

	public void setoTkNinCltId(integer oTkNinCltIdIdx, string oTkNinCltId) {
		integer position := Pos.xzt95oTkNinCltId(oTkNinCltIdIdx - 1);
		writeString(position, oTkNinCltId, Len.O_TK_NIN_CLT_ID);
	}

	/**Original name: XZT95O-TK-NIN-CLT-ID<br>*/
	public string getoTkNinCltId(integer oTkNinCltIdIdx) {
		integer position := Pos.xzt95oTkNinCltId(oTkNinCltIdIdx - 1);
		return readString(position, Len.O_TK_NIN_CLT_ID);
	}

	public void setoTkNinAdrId(integer oTkNinAdrIdIdx, string oTkNinAdrId) {
		integer position := Pos.xzt95oTkNinAdrId(oTkNinAdrIdIdx - 1);
		writeString(position, oTkNinAdrId, Len.O_TK_NIN_ADR_ID);
	}

	/**Original name: XZT95O-TK-NIN-ADR-ID<br>*/
	public string getoTkNinAdrId(integer oTkNinAdrIdIdx) {
		integer position := Pos.xzt95oTkNinAdrId(oTkNinAdrIdIdx - 1);
		return readString(position, Len.O_TK_NIN_ADR_ID);
	}

	public void setoTkWfStartedInd(integer oTkWfStartedIndIdx, char oTkWfStartedInd) {
		integer position := Pos.xzt95oTkWfStartedInd(oTkWfStartedIndIdx - 1);
		writeChar(position, oTkWfStartedInd);
	}

	/**Original name: XZT95O-TK-WF-STARTED-IND<br>*/
	public char getoTkWfStartedInd(integer oTkWfStartedIndIdx) {
		integer position := Pos.xzt95oTkWfStartedInd(oTkWfStartedIndIdx - 1);
		return readChar(position);
	}

	public void setoTkPolBilStaCd(integer oTkPolBilStaCdIdx, char oTkPolBilStaCd) {
		integer position := Pos.xzt95oTkPolBilStaCd(oTkPolBilStaCdIdx - 1);
		writeChar(position, oTkPolBilStaCd);
	}

	/**Original name: XZT95O-TK-POL-BIL-STA-CD<br>*/
	public char getoTkPolBilStaCd(integer oTkPolBilStaCdIdx) {
		integer position := Pos.xzt95oTkPolBilStaCd(oTkPolBilStaCdIdx - 1);
		return readChar(position);
	}

	public void setoPolNbr(integer oPolNbrIdx, string oPolNbr) {
		integer position := Pos.xzt95oPolNbr(oPolNbrIdx - 1);
		writeString(position, oPolNbr, Len.O_POL_NBR);
	}

	/**Original name: XZT95O-POL-NBR<br>*/
	public string getoPolNbr(integer oPolNbrIdx) {
		integer position := Pos.xzt95oPolNbr(oPolNbrIdx - 1);
		return readString(position, Len.O_POL_NBR);
	}

	public string getoPolNbrFormatted(integer oPolNbrIdx) {
		return Functions.padBlanks(getoPolNbr(oPolNbrIdx), Len.O_POL_NBR);
	}

	public void setoPolTypCd(integer oPolTypCdIdx, string oPolTypCd) {
		integer position := Pos.xzt95oPolTypCd(oPolTypCdIdx - 1);
		writeString(position, oPolTypCd, Len.O_POL_TYP_CD);
	}

	/**Original name: XZT95O-POL-TYP-CD<br>*/
	public string getoPolTypCd(integer oPolTypCdIdx) {
		integer position := Pos.xzt95oPolTypCd(oPolTypCdIdx - 1);
		return readString(position, Len.O_POL_TYP_CD);
	}

	public void setoPolTypDes(integer oPolTypDesIdx, string oPolTypDes) {
		integer position := Pos.xzt95oPolTypDes(oPolTypDesIdx - 1);
		writeString(position, oPolTypDes, Len.O_POL_TYP_DES);
	}

	/**Original name: XZT95O-POL-TYP-DES<br>*/
	public string getoPolTypDes(integer oPolTypDesIdx) {
		integer position := Pos.xzt95oPolTypDes(oPolTypDesIdx - 1);
		return readString(position, Len.O_POL_TYP_DES);
	}

	public void setoPolPriRskStAbb(integer oPolPriRskStAbbIdx, string oPolPriRskStAbb) {
		integer position := Pos.xzt95oPolPriRskStAbb(oPolPriRskStAbbIdx - 1);
		writeString(position, oPolPriRskStAbb, Len.O_POL_PRI_RSK_ST_ABB);
	}

	/**Original name: XZT95O-POL-PRI-RSK-ST-ABB<br>*/
	public string getoPolPriRskStAbb(integer oPolPriRskStAbbIdx) {
		integer position := Pos.xzt95oPolPriRskStAbb(oPolPriRskStAbbIdx - 1);
		return readString(position, Len.O_POL_PRI_RSK_ST_ABB);
	}

	public string getoPolPriRskStAbbFormatted(integer oPolPriRskStAbbIdx) {
		return Functions.padBlanks(getoPolPriRskStAbb(oPolPriRskStAbbIdx), Len.O_POL_PRI_RSK_ST_ABB);
	}

	public void setoNotEffDt(integer oNotEffDtIdx, string oNotEffDt) {
		integer position := Pos.xzt95oNotEffDt(oNotEffDtIdx - 1);
		writeString(position, oNotEffDt, Len.O_NOT_EFF_DT);
	}

	/**Original name: XZT95O-NOT-EFF-DT<br>*/
	public string getoNotEffDt(integer oNotEffDtIdx) {
		integer position := Pos.xzt95oNotEffDt(oNotEffDtIdx - 1);
		return readString(position, Len.O_NOT_EFF_DT);
	}

	public void setoPolEffDt(integer oPolEffDtIdx, string oPolEffDt) {
		integer position := Pos.xzt95oPolEffDt(oPolEffDtIdx - 1);
		writeString(position, oPolEffDt, Len.O_POL_EFF_DT);
	}

	/**Original name: XZT95O-POL-EFF-DT<br>*/
	public string getoPolEffDt(integer oPolEffDtIdx) {
		integer position := Pos.xzt95oPolEffDt(oPolEffDtIdx - 1);
		return readString(position, Len.O_POL_EFF_DT);
	}

	public string getoPolEffDtFormatted(integer oPolEffDtIdx) {
		return Functions.padBlanks(getoPolEffDt(oPolEffDtIdx), Len.O_POL_EFF_DT);
	}

	public void setoPolExpDt(integer oPolExpDtIdx, string oPolExpDt) {
		integer position := Pos.xzt95oPolExpDt(oPolExpDtIdx - 1);
		writeString(position, oPolExpDt, Len.O_POL_EXP_DT);
	}

	/**Original name: XZT95O-POL-EXP-DT<br>*/
	public string getoPolExpDt(integer oPolExpDtIdx) {
		integer position := Pos.xzt95oPolExpDt(oPolExpDtIdx - 1);
		return readString(position, Len.O_POL_EXP_DT);
	}

	public void setoPolDueAmt(integer oPolDueAmtIdx, decimal(10,2) oPolDueAmt) {
		integer position := Pos.xzt95oPolDueAmt(oPolDueAmtIdx - 1);
		writeDecimal(position, oPolDueAmt);
	}

	/**Original name: XZT95O-POL-DUE-AMT<br>*/
	public decimal(10,2) getoPolDueAmt(integer oPolDueAmtIdx) {
		integer position := Pos.xzt95oPolDueAmt(oPolDueAmtIdx - 1);
		return readDecimal(position, Len.Int.O_POL_DUE_AMT, Len.Fract.O_POL_DUE_AMT);
	}

	public void setoMasterCompanyNbr(integer oMasterCompanyNbrIdx, string oMasterCompanyNbr) {
		integer position := Pos.xzt95oMasterCompanyNbr(oMasterCompanyNbrIdx - 1);
		writeString(position, oMasterCompanyNbr, Len.O_MASTER_COMPANY_NBR);
	}

	/**Original name: XZT95O-MASTER-COMPANY-NBR<br>*/
	public string getoMasterCompanyNbr(integer oMasterCompanyNbrIdx) {
		integer position := Pos.xzt95oMasterCompanyNbr(oMasterCompanyNbrIdx - 1);
		return readString(position, Len.O_MASTER_COMPANY_NBR);
	}

	public void setoMasterCompanyDes(integer oMasterCompanyDesIdx, string oMasterCompanyDes) {
		integer position := Pos.xzt95oMasterCompanyDes(oMasterCompanyDesIdx - 1);
		writeString(position, oMasterCompanyDes, Len.O_MASTER_COMPANY_DES);
	}

	/**Original name: XZT95O-MASTER-COMPANY-DES<br>*/
	public string getoMasterCompanyDes(integer oMasterCompanyDesIdx) {
		integer position := Pos.xzt95oMasterCompanyDes(oMasterCompanyDesIdx - 1);
		return readString(position, Len.O_MASTER_COMPANY_DES);
	}


	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public final static integer L_SERVICE_CONTRACT_AREA := 1;
		public final static integer XZT950_SERVICE_INPUTS := L_SERVICE_CONTRACT_AREA;
		public final static integer I_TECHNICAL_KEY := XZT950_SERVICE_INPUTS;
		public final static integer I_TK_NOT_PRC_TS := I_TECHNICAL_KEY;
		public final static integer I_TK_FRM_SEQ_NBR := I_TK_NOT_PRC_TS + Len.I_TK_NOT_PRC_TS;
		public final static integer I_CSR_ACT_NBR := I_TK_FRM_SEQ_NBR + Len.I_TK_FRM_SEQ_NBR;
		public final static integer I_USERID := I_CSR_ACT_NBR + Len.I_CSR_ACT_NBR;
		public final static integer XZT950_SERVICE_OUTPUTS := I_USERID + Len.I_USERID;
		public final static integer O_TECHNICAL_KEY := XZT950_SERVICE_OUTPUTS;
		public final static integer O_TK_NOT_PRC_TS := O_TECHNICAL_KEY;
		public final static integer O_CSR_ACT_NBR := O_TK_NOT_PRC_TS + Len.O_TK_NOT_PRC_TS;
		public final static integer O_POLICY_ROW_TBL := O_CSR_ACT_NBR + Len.O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {		}

		//==== METHODS ====
		public static integer xzt95oPolicyRow(integer idx) {
			return O_POLICY_ROW_TBL + idx * Len.O_POLICY_ROW;
		}

		public static integer xzt95oPolicyTechnicalKey(integer idx) {
			return xzt95oPolicyRow(idx);
		}

		public static integer xzt95oTkNinCltId(integer idx) {
			return xzt95oPolicyTechnicalKey(idx);
		}

		public static integer xzt95oTkNinAdrId(integer idx) {
			return xzt95oTkNinCltId(idx) + Len.O_TK_NIN_CLT_ID;
		}

		public static integer xzt95oTkWfStartedInd(integer idx) {
			return xzt95oTkNinAdrId(idx) + Len.O_TK_NIN_ADR_ID;
		}

		public static integer xzt95oTkPolBilStaCd(integer idx) {
			return xzt95oTkWfStartedInd(idx) + Len.O_TK_WF_STARTED_IND;
		}

		public static integer xzt95oPolNbr(integer idx) {
			return xzt95oTkPolBilStaCd(idx) + Len.O_TK_POL_BIL_STA_CD;
		}

		public static integer xzt95oPolicyType(integer idx) {
			return xzt95oPolNbr(idx) + Len.O_POL_NBR;
		}

		public static integer xzt95oPolTypCd(integer idx) {
			return xzt95oPolicyType(idx);
		}

		public static integer xzt95oPolTypDes(integer idx) {
			return xzt95oPolTypCd(idx) + Len.O_POL_TYP_CD;
		}

		public static integer xzt95oPolPriRskStAbb(integer idx) {
			return xzt95oPolTypDes(idx) + Len.O_POL_TYP_DES;
		}

		public static integer xzt95oNotEffDt(integer idx) {
			return xzt95oPolPriRskStAbb(idx) + Len.O_POL_PRI_RSK_ST_ABB;
		}

		public static integer xzt95oPolEffDt(integer idx) {
			return xzt95oNotEffDt(idx) + Len.O_NOT_EFF_DT;
		}

		public static integer xzt95oPolExpDt(integer idx) {
			return xzt95oPolEffDt(idx) + Len.O_POL_EFF_DT;
		}

		public static integer xzt95oPolDueAmt(integer idx) {
			return xzt95oPolExpDt(idx) + Len.O_POL_EXP_DT;
		}

		public static integer xzt95oMasterCompany(integer idx) {
			return xzt95oPolDueAmt(idx) + Len.O_POL_DUE_AMT;
		}

		public static integer xzt95oMasterCompanyNbr(integer idx) {
			return xzt95oMasterCompany(idx);
		}

		public static integer xzt95oMasterCompanyDes(integer idx) {
			return xzt95oMasterCompanyNbr(idx) + Len.O_MASTER_COMPANY_NBR;
		}

	}
	public static class Len {

		//==== PROPERTIES ====
		public final static integer I_TK_NOT_PRC_TS := 26;
		public final static integer I_TK_FRM_SEQ_NBR := 5;
		public final static integer I_CSR_ACT_NBR := 9;
		public final static integer I_USERID := 8;
		public final static integer O_TK_NOT_PRC_TS := 26;
		public final static integer O_CSR_ACT_NBR := 9;
		public final static integer O_TK_NIN_CLT_ID := 64;
		public final static integer O_TK_NIN_ADR_ID := 64;
		public final static integer O_TK_WF_STARTED_IND := 1;
		public final static integer O_TK_POL_BIL_STA_CD := 1;
		public final static integer O_POLICY_TECHNICAL_KEY := O_TK_NIN_CLT_ID + O_TK_NIN_ADR_ID + O_TK_WF_STARTED_IND + O_TK_POL_BIL_STA_CD;
		public final static integer O_POL_NBR := 25;
		public final static integer O_POL_TYP_CD := 3;
		public final static integer O_POL_TYP_DES := 30;
		public final static integer O_POLICY_TYPE := O_POL_TYP_CD + O_POL_TYP_DES;
		public final static integer O_POL_PRI_RSK_ST_ABB := 2;
		public final static integer O_NOT_EFF_DT := 10;
		public final static integer O_POL_EFF_DT := 10;
		public final static integer O_POL_EXP_DT := 10;
		public final static integer O_POL_DUE_AMT := 10;
		public final static integer O_MASTER_COMPANY_NBR := 2;
		public final static integer O_MASTER_COMPANY_DES := 40;
		public final static integer O_MASTER_COMPANY := O_MASTER_COMPANY_NBR + O_MASTER_COMPANY_DES;
		public final static integer O_POLICY_ROW := O_POLICY_TECHNICAL_KEY + O_POL_NBR + O_POLICY_TYPE + O_POL_PRI_RSK_ST_ABB + O_NOT_EFF_DT + O_POL_EFF_DT + O_POL_EXP_DT + O_POL_DUE_AMT + O_MASTER_COMPANY;
		public final static integer I_TECHNICAL_KEY := I_TK_NOT_PRC_TS + I_TK_FRM_SEQ_NBR;
		public final static integer XZT950_SERVICE_INPUTS := I_TECHNICAL_KEY + I_CSR_ACT_NBR + I_USERID;
		public final static integer O_TECHNICAL_KEY := O_TK_NOT_PRC_TS;
		public final static integer O_POLICY_ROW_TBL := LServiceContractAreaXz0x9050.O_POLICY_ROW_MAXOCCURS * O_POLICY_ROW;
		public final static integer XZT950_SERVICE_OUTPUTS := O_TECHNICAL_KEY + O_CSR_ACT_NBR + O_POLICY_ROW_TBL;
		public final static integer L_SERVICE_CONTRACT_AREA := XZT950_SERVICE_INPUTS + XZT950_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer I_TK_FRM_SEQ_NBR := 5;
			public final static integer O_POL_DUE_AMT := 8;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer O_POL_DUE_AMT := 2;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//LServiceContractAreaXz0x9050