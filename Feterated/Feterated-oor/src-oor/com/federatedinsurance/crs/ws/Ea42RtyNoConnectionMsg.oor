package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-42-RTY-NO-CONNECTION-MSG<br>
 * Variable: EA-42-RTY-NO-CONNECTION-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea42RtyNoConnectionMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG
	private string flr1 := "TS548099 -";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-1
	private string flr2 := "UNABLE TO";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-2
	private string flr3 := "CONNECT TO";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-3
	private string flr4 := "TARGETED CICS";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-4
	private string flr5 := "SESSIONS.";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-5
	private string flr6 := "REGIONS TRIED:";
	//Original name: EA-42-REGIONS-ATTEMPTED
	private string ea42RegionsAttempted := DefaultValues.stringVal(Len.EA42_REGIONS_ATTEMPTED);


	//==== METHODS ====
	public string getEa42RtyNoConnectionMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa42RtyNoConnectionMsgBytes());
	}

	public []byte getEa42RtyNoConnectionMsgBytes() {
		[]byte buffer := new [Len.EA42_RTY_NO_CONNECTION_MSG]byte;
		return getEa42RtyNoConnectionMsgBytes(buffer, 1);
	}

	public []byte getEa42RtyNoConnectionMsgBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position +:= Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position +:= Len.FLR6;
		MarshalByte.writeString(buffer, position, ea42RegionsAttempted, Len.EA42_REGIONS_ATTEMPTED);
		return buffer;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public string getFlr3() {
		return this.flr3;
	}

	public string getFlr4() {
		return this.flr4;
	}

	public string getFlr5() {
		return this.flr5;
	}

	public string getFlr6() {
		return this.flr6;
	}

	public void setEa42RegionsAttempted(string ea42RegionsAttempted) {
		this.ea42RegionsAttempted:=Functions.subString(ea42RegionsAttempted, Len.EA42_REGIONS_ATTEMPTED);
	}

	public string getEa42RegionsAttempted() {
		return this.ea42RegionsAttempted;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer EA42_REGIONS_ATTEMPTED := 30;
		public final static integer FLR1 := 11;
		public final static integer FLR2 := 10;
		public final static integer FLR4 := 14;
		public final static integer FLR6 := 15;
		public final static integer EA42_RTY_NO_CONNECTION_MSG := EA42_REGIONS_ATTEMPTED + 3 * FLR1 + FLR2 + FLR4 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ea42RtyNoConnectionMsg