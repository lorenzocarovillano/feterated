package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.commons.data.to.ICoTypNot;
import com.federatedinsurance.crs.copy.DclcoTyp;
import com.federatedinsurance.crs.copy.DclnotCoTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9090<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9090Data implements ICoTypNot {

	//==== PROPERTIES ====
	//Original name: CF-YES
	private char cfYes := 'Y';
	/**Original name: NI-SPE-CRT-CD<br>
	 * <pre>* NULL INDICATORS</pre>*/
	private short niSpeCrtCd := DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-END-NOT-CO-CSR-FLAG
	private boolean swEndNotCoCsrFlag := false;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9090 workingStorageArea := new WorkingStorageAreaXz0b9090();
	//Original name: WS-XZ0A9090-ROW
	private WsXz0a9090Row wsXz0a9090Row := new WsXz0a9090Row();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage := new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage := new WsHalrrespLinkage();
	//Original name: DCLNOT-CO-TYP
	private DclnotCoTyp dclnotCoTyp := new DclnotCoTyp();
	//Original name: DCLCO-TYP
	private DclcoTyp dclcoTyp := new DclcoTyp();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc := new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom := new Hallcom();
	//Original name: WS-APPLID
	private string wsApplid := DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw := new WsLogWarningOrErrorSw();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon := new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon := new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo := new WsEstoInfo();


	//==== METHODS ====
	public char getCfYes() {
		return this.cfYes;
	}

	public void setNiSpeCrtCd(short niSpeCrtCd) {
		this.niSpeCrtCd:=niSpeCrtCd;
	}

	public short getNiSpeCrtCd() {
		return this.niSpeCrtCd;
	}

	public void setSwEndNotCoCsrFlag(boolean swEndNotCoCsrFlag) {
		this.swEndNotCoCsrFlag:=swEndNotCoCsrFlag;
	}

	public boolean isSwEndNotCoCsrFlag() {
		return this.swEndNotCoCsrFlag;
	}

	public void setWsApplid(string wsApplid) {
		this.wsApplid:=Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public string getWsApplid() {
		return this.wsApplid;
	}

	@Override
	public string getActNotTypCd() {
		return dclnotCoTyp.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(string actNotTypCd) {
		this.dclnotCoTyp.setActNotTypCd(actNotTypCd);
	}

	@Override
	public string getCoCd() {
		return dclnotCoTyp.getCoCd();
	}

	@Override
	public void setCoCd(string coCd) {
		this.dclnotCoTyp.setCoCd(coCd);
	}

	@Override
	public string getCoDes() {
		return dclcoTyp.getCoDes();
	}

	@Override
	public void setCoDes(string coDes) {
		this.dclcoTyp.setCoDes(coDes);
	}

	@Override
	public short getCoOfsNbr() {
		return dclcoTyp.getCoOfsNbr();
	}

	@Override
	public void setCoOfsNbr(short coOfsNbr) {
		this.dclcoTyp.setCoOfsNbr(coOfsNbr);
	}

	public DclcoTyp getDclcoTyp() {
		return dclcoTyp;
	}

	public DclnotCoTyp getDclnotCoTyp() {
		return dclnotCoTyp;
	}

	@Override
	public char getDtbCd() {
		return dclnotCoTyp.getDtbCd();
	}

	@Override
	public void setDtbCd(char dtbCd) {
		this.dclnotCoTyp.setDtbCd(dtbCd);
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public string getSpeCrtCd() {
		return dclnotCoTyp.getSpeCrtCd();
	}

	@Override
	public void setSpeCrtCd(string speCrtCd) {
		this.dclnotCoTyp.setSpeCrtCd(speCrtCd);
	}

	@Override
	public String getSpeCrtCdObj() {
		if (getNiSpeCrtCd()>= 0) then
		    return getSpeCrtCd();
		else
		    return null;
		endif;
	}

	@Override
	public void setSpeCrtCdObj(String speCrtCdObj) {
		if (speCrtCdObj!= null) then
		    setSpeCrtCd(speCrtCdObj);
		    setNiSpeCrtCd(0);
		else
		    setNiSpeCrtCd(-1);
		endif;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b9090 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9090Row getWsXz0a9090Row() {
		return wsXz0a9090Row;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WS_SE3_CUR_ISO_DATE := 10;
		public final static integer WS_SE3_CUR_ISO_TIME := 16;
		public final static integer WS_APPLID := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xz0b9090Data