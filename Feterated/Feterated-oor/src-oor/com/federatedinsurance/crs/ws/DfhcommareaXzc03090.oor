package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.copy.Xzc030ProgramOutput;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program XZC03090<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaXzc03090 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZC03I-ACT-NBR
	private string iActNbr := DefaultValues.stringVal(Len.I_ACT_NBR);
	//Original name: FILLER-XZC030-PROGRAM-INPUT
	private string flr1 := DefaultValues.stringVal(Len.FLR1);
	//Original name: XZC030-PROGRAM-OUTPUT
	private Xzc030ProgramOutput xzc030ProgramOutput := new Xzc030ProgramOutput();


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize([]byte buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes([]byte buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public []byte getDfhcommareaBytes() {
		[]byte buffer := new [Len.DFHCOMMAREA]byte;
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes([]byte buffer, integer offset) {
		integer position := offset;
		setXzc030ProgramInputBytes(buffer, position);
		position +:= Len.XZC030_PROGRAM_INPUT;
		xzc030ProgramOutput.setXzc030ProgramOutputBytes(buffer, position);
	}

	public []byte getDfhcommareaBytes([]byte buffer, integer offset) {
		integer position := offset;
		getXzc030ProgramInputBytes(buffer, position);
		position +:= Len.XZC030_PROGRAM_INPUT;
		xzc030ProgramOutput.getXzc030ProgramOutputBytes(buffer, position);
		return buffer;
	}

	public void setXzc030ProgramInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		iActNbr := MarshalByte.readString(buffer, position, Len.I_ACT_NBR);
		position +:= Len.I_ACT_NBR;
		flr1 := MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public []byte getXzc030ProgramInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, iActNbr, Len.I_ACT_NBR);
		position +:= Len.I_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setiActNbr(string iActNbr) {
		this.iActNbr:=Functions.subString(iActNbr, Len.I_ACT_NBR);
	}

	public string getiActNbr() {
		return this.iActNbr;
	}

	public string getiActNbrFormatted() {
		return Functions.padBlanks(getiActNbr(), Len.I_ACT_NBR);
	}

	public void setFlr1(string flr1) {
		this.flr1:=Functions.subString(flr1, Len.FLR1);
	}

	public string getFlr1() {
		return this.flr1;
	}

	public Xzc030ProgramOutput getXzc030ProgramOutput() {
		return xzc030ProgramOutput;
	}

	@Override
	public []byte serialize() {
		return getDfhcommareaBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer I_ACT_NBR := 9;
		public final static integer FLR1 := 291;
		public final static integer XZC030_PROGRAM_INPUT := I_ACT_NBR + FLR1;
		public final static integer DFHCOMMAREA := XZC030_PROGRAM_INPUT + Xzc030ProgramOutput.Len.XZC030_PROGRAM_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//DfhcommareaXzc03090