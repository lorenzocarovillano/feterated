package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P90K0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p90k0 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-TBL-POLICIES
	private short maxTblPolicies := (short)30;
	//Original name: CF-ACT-NOT-IMPENDING
	private string actNotImpending := "IMP";
	//Original name: CF-ACT-NOT-RESCIND
	private string actNotRescind := "RES";
	//Original name: CF-ACT-NOT-NONPAY
	private string actNotNonpay := "NPC";
	//Original name: CF-ACT-NOT-STA-CD
	private CfActNotStaCd actNotStaCd := new CfActNotStaCd();
	//Original name: CF-ATC-COMMERCIAL-LINES
	private string atcCommercialLines := "CL";
	//Original name: CF-ATC-PERSONAL-LINES
	private string atcPersonalLines := "PL";
	//Original name: CF-ADDED-BY-BUSINESS-WORKS
	private string addedByBusinessWorks := "NA";
	//Original name: CF-BILLING-USERID
	private string billingUserid := "BILLSRV";
	//Original name: CF-GET-BILLING-DETAIL-SVC-INTF
	private string getBillingDetailSvcIntf := "BX0G0003";
	//Original name: CF-GET-NOT-DAY-RQR-UTY-PGM
	private string getNotDayRqrUtyPgm := "XZ0U8000";
	//Original name: CF-PL-PRODUCER-NBR
	private string plProducerNbr := "4-000";
	//Original name: FILLER-CF-PL-PRODUCER-NM
	private string flr1 := "PERSONAL";
	//Original name: FILLER-CF-PL-PRODUCER-NM-1
	private string flr2 := "SERVICE";
	//Original name: FILLER-CF-PL-PRODUCER-NM-2
	private string flr3 := "REPRESENTATIVE";
	//Original name: FILLER-CF-PL-PRODUCER-NM-3
	private string flr4 := "";
	//Original name: CF-SERVICE-PROXY
	private CfServiceProxyXz0p90k0 serviceProxy := new CfServiceProxyXz0p90k0();
	//Original name: CF-YES
	private char yes := 'Y';
	//Original name: CF-MAX-DATE
	private string maxDate := "9999-12-31";


	//==== METHODS ====
	public short getMaxTblPolicies() {
		return this.maxTblPolicies;
	}

	public void setActNotImpending(string actNotImpending) {
		this.actNotImpending:=Functions.subString(actNotImpending, Len.ACT_NOT_IMPENDING);
	}

	public string getActNotImpending() {
		return this.actNotImpending;
	}

	public void setActNotRescind(string actNotRescind) {
		this.actNotRescind:=Functions.subString(actNotRescind, Len.ACT_NOT_RESCIND);
	}

	public string getActNotRescind() {
		return this.actNotRescind;
	}

	public void setActNotNonpay(string actNotNonpay) {
		this.actNotNonpay:=Functions.subString(actNotNonpay, Len.ACT_NOT_NONPAY);
	}

	public string getActNotNonpay() {
		return this.actNotNonpay;
	}

	public string getAtcCommercialLines() {
		return this.atcCommercialLines;
	}

	public string getAtcPersonalLines() {
		return this.atcPersonalLines;
	}

	public string getAddedByBusinessWorks() {
		return this.addedByBusinessWorks;
	}

	public string getBillingUserid() {
		return this.billingUserid;
	}

	public string getGetBillingDetailSvcIntf() {
		return this.getBillingDetailSvcIntf;
	}

	public string getGetNotDayRqrUtyPgm() {
		return this.getNotDayRqrUtyPgm;
	}

	public string getPlProducerNbr() {
		return this.plProducerNbr;
	}

	public string getPlProducerNmFormatted() {
		return MarshalByteExt.bufferToStr(getPlProducerNmBytes());
	}

	/**Original name: CF-PL-PRODUCER-NM<br>*/
	public []byte getPlProducerNmBytes() {
		[]byte buffer := new [Len.PL_PRODUCER_NM]byte;
		return getPlProducerNmBytes(buffer, 1);
	}

	public []byte getPlProducerNmBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position +:= Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public string getFlr3() {
		return this.flr3;
	}

	public string getFlr4() {
		return this.flr4;
	}

	public char getYes() {
		return this.yes;
	}

	public string getMaxDate() {
		return this.maxDate;
	}

	public CfActNotStaCd getActNotStaCd() {
		return actNotStaCd;
	}

	public CfServiceProxyXz0p90k0 getServiceProxy() {
		return serviceProxy;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ACT_NOT_IMPENDING := 5;
		public final static integer ACT_NOT_RESCIND := 5;
		public final static integer ACT_NOT_NONPAY := 5;
		public final static integer FLR1 := 9;
		public final static integer FLR2 := 8;
		public final static integer FLR3 := 14;
		public final static integer FLR4 := 89;
		public final static integer PL_PRODUCER_NM := FLR1 + FLR2 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//ConstantFieldsXz0p90k0