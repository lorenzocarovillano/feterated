package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-MISC-WORK-FLDS<br>
 * Variable: WS-MISC-WORK-FLDS from program CAWI004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsMiscWorkFldsCawi004 {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private string programName := "CAWI004";
	//Original name: WS-BUS-OBJ-NAME
	private string busObjName := "CLIENT_PHONE_V";
	//Original name: FILLER-WS-PROGRAM-LINK-FAILED
	private string flr1 := "CALL TO";
	//Original name: WS-FAILED-LINK-PGM-NAME
	private string failedLinkPgmName := DefaultValues.stringVal(Len.FAILED_LINK_PGM_NAME);
	//Original name: FILLER-WS-PROGRAM-LINK-FAILED-1
	private string flr2 := " FAILED.";


	//==== METHODS ====
	public string getProgramName() {
		return this.programName;
	}

	public string getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public string getBusObjName() {
		return this.busObjName;
	}

	public string getProgramLinkFailedFormatted() {
		return MarshalByteExt.bufferToStr(getProgramLinkFailedBytes());
	}

	/**Original name: WS-PROGRAM-LINK-FAILED<br>*/
	public []byte getProgramLinkFailedBytes() {
		[]byte buffer := new [Len.PROGRAM_LINK_FAILED]byte;
		return getProgramLinkFailedBytes(buffer, 1);
	}

	public []byte getProgramLinkFailedBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, failedLinkPgmName, Len.FAILED_LINK_PGM_NAME);
		position +:= Len.FAILED_LINK_PGM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		return buffer;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public void setFailedLinkPgmName(string failedLinkPgmName) {
		this.failedLinkPgmName:=Functions.subString(failedLinkPgmName, Len.FAILED_LINK_PGM_NAME);
	}

	public string getFailedLinkPgmName() {
		return this.failedLinkPgmName;
	}

	public string getFlr2() {
		return this.flr2;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer FAILED_LINK_PGM_NAME := 8;
		public final static integer FLR1 := 8;
		public final static integer PROGRAM_LINK_FAILED := FAILED_LINK_PGM_NAME + 2 * FLR1;
		public final static integer PROGRAM_NAME := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsMiscWorkFldsCawi004