package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

import com.federatedinsurance.crs.ws.enums.WsOperationNameXz0x0018;
import com.federatedinsurance.crs.ws.redefines.WsCaPtr;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0X0018<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class WorkingStorageAreaXz0x0018 {

	//==== PROPERTIES ====
	//Original name: WS-SERVICE-CONTRACT-ATB
	private WsServiceContractAtb wsServiceContractAtb := new WsServiceContractAtb();
	//Original name: WS-PROXY-CONTRACT-ATB
	private WsServiceContractAtb wsProxyContractAtb := new WsServiceContractAtb();
	//Original name: WS-CA-PTR
	private WsCaPtr wsCaPtr := new WsCaPtr();
	//Original name: WS-RESPONSE-CODE
	private integer wsResponseCode := DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private integer wsResponseCode2 := DefaultValues.BIN_INT_VAL;
	//Original name: WS-PROGRAM-NAME
	private string wsProgramName := DefaultValues.stringVal(Len.WS_PROGRAM_NAME);
	//Original name: WS-EIBRESP-DISPLAY
	private string wsEibrespDisplay := DefaultValues.stringVal(Len.WS_EIBRESP_DISPLAY);
	//Original name: WS-EIBRESP2-DISPLAY
	private string wsEibresp2Display := DefaultValues.stringVal(Len.WS_EIBRESP2_DISPLAY);
	//Original name: WS-OPERATION-NAME
	private WsOperationNameXz0x0018 wsOperationName := new WsOperationNameXz0x0018();


	//==== METHODS ====
	public void setWsResponseCode(integer wsResponseCode) {
		this.wsResponseCode:=wsResponseCode;
	}

	public integer getWsResponseCode() {
		return this.wsResponseCode;
	}

	public void setWsResponseCode2(integer wsResponseCode2) {
		this.wsResponseCode2:=wsResponseCode2;
	}

	public integer getWsResponseCode2() {
		return this.wsResponseCode2;
	}

	public void setWsProgramName(string wsProgramName) {
		this.wsProgramName:=Functions.subString(wsProgramName, Len.WS_PROGRAM_NAME);
	}

	public string getWsProgramName() {
		return this.wsProgramName;
	}

	public string getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public void setWsEibrespDisplay(long wsEibrespDisplay) {
		this.wsEibrespDisplay:=PicFormatter.display("-Z(8)9").format(wsEibrespDisplay).toString();
	}

	public long getWsEibrespDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.wsEibrespDisplay);
	}

	public string getWsEibrespDisplayFormatted() {
		return this.wsEibrespDisplay;
	}

	public string getWsEibrespDisplayAsString() {
		return getWsEibrespDisplayFormatted();
	}

	public void setWsEibresp2Display(long wsEibresp2Display) {
		this.wsEibresp2Display:=PicFormatter.display("-Z(8)9").format(wsEibresp2Display).toString();
	}

	public long getWsEibresp2Display() {
		return PicParser.display("-Z(8)9").parseLong(this.wsEibresp2Display);
	}

	public string getWsEibresp2DisplayFormatted() {
		return this.wsEibresp2Display;
	}

	public string getWsEibresp2DisplayAsString() {
		return getWsEibresp2DisplayFormatted();
	}

	public WsCaPtr getWsCaPtr() {
		return wsCaPtr;
	}

	public WsOperationNameXz0x0018 getWsOperationName() {
		return wsOperationName;
	}

	public WsServiceContractAtb getWsProxyContractAtb() {
		return wsProxyContractAtb;
	}

	public WsServiceContractAtb getWsServiceContractAtb() {
		return wsServiceContractAtb;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WS_PROGRAM_NAME := 8;
		public final static integer WS_EIBRESP_DISPLAY := 10;
		public final static integer WS_EIBRESP2_DISPLAY := 10;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WorkingStorageAreaXz0x0018