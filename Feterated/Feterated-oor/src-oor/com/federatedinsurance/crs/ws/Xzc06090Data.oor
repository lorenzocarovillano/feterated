package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;

import com.federatedinsurance.crs.copy.Ivoryh;
import com.federatedinsurance.crs.copy.Xz0690co;
import com.federatedinsurance.crs.copy.Xzc0690iofCallableInputs;
import com.federatedinsurance.crs.copy.Xzc0690o;
import com.federatedinsurance.crs.ws.occurs.TlLocv;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZC06090<br>
 * Generated as a class for rule WS.<br>*/
public class Xzc06090Data {

	//==== PROPERTIES ====
	public final static integer TL_LOCV_MAXOCCURS := 10;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXzc06090 constantFields := new ConstantFieldsXzc06090();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXzc06090 errorAndAdviceMessages := new ErrorAndAdviceMessagesXzc06090();
	//Original name: SAVE-AREA
	private SaveAreaXzc08090 saveArea := new SaveAreaXzc08090();
	//Original name: SUBSCRIPTS
	private SubscriptsXzc06090 subscripts := new SubscriptsXzc06090();
	//Original name: TL-LOCV
	private []TlLocv tlLocv := new [TL_LOCV_MAXOCCURS]TlLocv;
	//Original name: URI-LKU-LINKAGE
	private Ts571cb1 ts571cb1 := new Ts571cb1();
	//Original name: IVORYH
	private Ivoryh ivoryh := new Ivoryh();
	//Original name: XZC0690I
	private Xzc0690iofCallableInputs xzc0690iofCallableInputs := new Xzc0690iofCallableInputs();
	//Original name: XZC0690O
	private Xzc0690o xzc0690o := new Xzc0690o();
	//Original name: XZC0690I
	private Xzc0690iofCallableInputs xzc0690iofServiceInputs := new Xzc0690iofCallableInputs();
	//Original name: XZ0690CO
	private Xz0690co xz0690co := new Xz0690co();

	//==== CONSTRUCTORS ====
	public Xzc06090Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int tlLocvIdx in 1.. TL_LOCV_MAXOCCURS 
		do
			tlLocv[tlLocvIdx] := new TlLocv();
		enddo
	}

	public string getTableOfLocvFormatted() {
		return MarshalByteExt.bufferToStr(getTableOfLocvBytes());
	}

	/**Original name: TABLE-OF-LOCV<br>*/
	public []byte getTableOfLocvBytes() {
		[]byte buffer := new [Len.TABLE_OF_LOCV]byte;
		return getTableOfLocvBytes(buffer, 1);
	}

	public []byte getTableOfLocvBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. TL_LOCV_MAXOCCURS 
		do
			tlLocv[idx].getTlLocvBytes(buffer, position);
			position +:= TlLocv.Len.TL_LOCV;
		enddo
		return buffer;
	}

	public void initTableOfLocvSpaces() {
		for integer idx in 1.. TL_LOCV_MAXOCCURS 
		do
			tlLocv[idx].initTlLocvSpaces();
		enddo
	}

	/**Original name: CALLABLE-INPUTS<br>*/
	public []byte getCallableInputsBytes() {
		[]byte buffer := new [Len.CALLABLE_INPUTS]byte;
		return getCallableInputsBytes(buffer, 1);
	}

	public []byte getCallableInputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		xzc0690iofCallableInputs.getGetPolTrmLisByActBytes(buffer, position);
		return buffer;
	}

	public void setCallableOutputsBytes([]byte buffer) {
		setCallableOutputsBytes(buffer, 1);
	}

	/**Original name: CALLABLE-OUTPUTS<br>*/
	public []byte getCallableOutputsBytes() {
		[]byte buffer := new [Len.CALLABLE_OUTPUTS]byte;
		return getCallableOutputsBytes(buffer, 1);
	}

	public void setCallableOutputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		xzc0690o.setGetPolTrmLisByActBytes(buffer, position);
		position +:= Xzc0690o.Len.GET_POL_TRM_LIS_BY_ACT;
		xzc0690o.setErrorInformationBytes(buffer, position);
	}

	public []byte getCallableOutputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		xzc0690o.getGetPolTrmLisByActBytes(buffer, position);
		position +:= Xzc0690o.Len.GET_POL_TRM_LIS_BY_ACT;
		xzc0690o.getErrorInformationBytes(buffer, position);
		return buffer;
	}

	public void setServiceInputsBytes([]byte buffer) {
		setServiceInputsBytes(buffer, 1);
	}

	public void setServiceInputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		xzc0690iofServiceInputs.setGetPolTrmLisByActBytes(buffer, position);
	}

	/**Original name: SERVICE-OUTPUTS<br>*/
	public []byte getServiceOutputsBytes() {
		[]byte buffer := new [Len.SERVICE_OUTPUTS]byte;
		return getServiceOutputsBytes(buffer, 1);
	}

	public []byte getServiceOutputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		xz0690co.getGetPolTrmLisByActBytes(buffer, position);
		position +:= Xz0690co.Len.GET_POL_TRM_LIS_BY_ACT;
		xz0690co.getErrorInformationBytes(buffer, position);
		return buffer;
	}

	public ConstantFieldsXzc06090 getConstantFields() {
		return constantFields;
	}

	public ErrorAndAdviceMessagesXzc06090 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public Ivoryh getIvoryh() {
		return ivoryh;
	}

	public SaveAreaXzc08090 getSaveArea() {
		return saveArea;
	}

	public SubscriptsXzc06090 getSubscripts() {
		return subscripts;
	}

	public TlLocv getTlLocv(integer idx) {
		return tlLocv[idx];
	}

	public Ts571cb1 getTs571cb1() {
		return ts571cb1;
	}

	public Xz0690co getXz0690co() {
		return xz0690co;
	}

	public Xzc0690iofCallableInputs getXzc0690iofCallableInputs() {
		return xzc0690iofCallableInputs;
	}

	public Xzc0690iofCallableInputs getXzc0690iofServiceInputs() {
		return xzc0690iofServiceInputs;
	}

	public Xzc0690o getXzc0690o() {
		return xzc0690o;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer SERVICE_INPUTS := Xzc0690iofCallableInputs.Len.GET_POL_TRM_LIS_BY_ACT;
		public final static integer CALLABLE_INPUTS := Xzc0690iofCallableInputs.Len.GET_POL_TRM_LIS_BY_ACT;
		public final static integer CALLABLE_OUTPUTS := Xzc0690o.Len.GET_POL_TRM_LIS_BY_ACT + Xzc0690o.Len.ERROR_INFORMATION;
		public final static integer TABLE_OF_LOCV := Xzc06090Data.TL_LOCV_MAXOCCURS * TlLocv.Len.TL_LOCV;
		public final static integer SERVICE_OUTPUTS := Xz0690co.Len.GET_POL_TRM_LIS_BY_ACT + Xz0690co.Len.ERROR_INFORMATION;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xzc06090Data