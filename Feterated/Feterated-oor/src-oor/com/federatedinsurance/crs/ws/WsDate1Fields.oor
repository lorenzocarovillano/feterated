package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-DATE1-FIELDS<br>
 * Variable: WS-DATE1-FIELDS from program XPIODAT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDate1Fields {

	//==== PROPERTIES ====
	//Original name: WS-DATE1-DAY
	private short day := DefaultValues.SHORT_VAL;
	//Original name: WS-DATE1-MONTH
	private short month := DefaultValues.SHORT_VAL;
	//Original name: WS-DATE1-YEAR
	private short year := DefaultValues.SHORT_VAL;
	//Original name: WS-DATE1-YEAR2
	private short year2 := DefaultValues.SHORT_VAL;
	//Original name: WS-DATE1-HOURS
	private short hours := DefaultValues.SHORT_VAL;
	//Original name: WS-DATE1-MINUTES
	private short minutes := DefaultValues.SHORT_VAL;
	//Original name: WS-DATE1-SECONDS
	private short seconds := DefaultValues.SHORT_VAL;
	//Original name: WS-DATE1-MSECS
	private integer msecs := DefaultValues.INT_VAL;
	//Original name: WS-DATE1-AMPM
	private string ampm := DefaultValues.stringVal(Len.AMPM);


	//==== METHODS ====
	public void setWsDate2FieldsBytes([]byte buffer) {
		setWsDate2FieldsBytes(buffer, 1);
	}

	public []byte getWsDate1FieldsBytes() {
		[]byte buffer := new [Len.WS_DATE1_FIELDS]byte;
		return getWsDate1FieldsBytes(buffer, 1);
	}

	public void setWsDate2FieldsBytes([]byte buffer, integer offset) {
		integer position := offset;
		day := MarshalByte.readPackedAsShort(buffer, position, Len.Int.DAY, 0, SignType.NO_SIGN);
		position +:= Len.DAY;
		month := MarshalByte.readPackedAsShort(buffer, position, Len.Int.MONTH, 0, SignType.NO_SIGN);
		position +:= Len.MONTH;
		year := MarshalByte.readPackedAsShort(buffer, position, Len.Int.YEAR, 0, SignType.NO_SIGN);
		position +:= Len.YEAR;
		year2 := MarshalByte.readPackedAsShort(buffer, position, Len.Int.YEAR2, 0, SignType.NO_SIGN);
		position +:= Len.YEAR2;
		hours := MarshalByte.readPackedAsShort(buffer, position, Len.Int.HOURS, 0, SignType.NO_SIGN);
		position +:= Len.HOURS;
		minutes := MarshalByte.readPackedAsShort(buffer, position, Len.Int.MINUTES, 0, SignType.NO_SIGN);
		position +:= Len.MINUTES;
		seconds := MarshalByte.readPackedAsShort(buffer, position, Len.Int.SECONDS, 0, SignType.NO_SIGN);
		position +:= Len.SECONDS;
		msecs := MarshalByte.readPackedAsInt(buffer, position, Len.Int.MSECS, 0, SignType.NO_SIGN);
		position +:= Len.MSECS;
		ampm := MarshalByte.readString(buffer, position, Len.AMPM);
	}

	public []byte getWsDate1FieldsBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeShortAsPacked(buffer, position, day, Len.Int.DAY, 0, SignType.NO_SIGN);
		position +:= Len.DAY;
		MarshalByte.writeShortAsPacked(buffer, position, month, Len.Int.MONTH, 0, SignType.NO_SIGN);
		position +:= Len.MONTH;
		MarshalByte.writeShortAsPacked(buffer, position, year, Len.Int.YEAR, 0, SignType.NO_SIGN);
		position +:= Len.YEAR;
		MarshalByte.writeShortAsPacked(buffer, position, year2, Len.Int.YEAR2, 0, SignType.NO_SIGN);
		position +:= Len.YEAR2;
		MarshalByte.writeShortAsPacked(buffer, position, hours, Len.Int.HOURS, 0, SignType.NO_SIGN);
		position +:= Len.HOURS;
		MarshalByte.writeShortAsPacked(buffer, position, minutes, Len.Int.MINUTES, 0, SignType.NO_SIGN);
		position +:= Len.MINUTES;
		MarshalByte.writeShortAsPacked(buffer, position, seconds, Len.Int.SECONDS, 0, SignType.NO_SIGN);
		position +:= Len.SECONDS;
		MarshalByte.writeIntAsPacked(buffer, position, msecs, Len.Int.MSECS, 0, SignType.NO_SIGN);
		position +:= Len.MSECS;
		MarshalByte.writeString(buffer, position, ampm, Len.AMPM);
		return buffer;
	}

	public void setDay(short day) {
		this.day:=day;
	}

	public short getDay() {
		return this.day;
	}

	public void setMonth(short month) {
		this.month:=month;
	}

	public short getMonth() {
		return this.month;
	}

	public void setYear(short year) {
		this.year:=year;
	}

	public short getYear() {
		return this.year;
	}

	public void setYear2(short year2) {
		this.year2:=year2;
	}

	public short getYear2() {
		return this.year2;
	}

	public void setHours(short hours) {
		this.hours:=hours;
	}

	public short getHours() {
		return this.hours;
	}

	public void setMinutes(short minutes) {
		this.minutes:=minutes;
	}

	public short getMinutes() {
		return this.minutes;
	}

	public void setSeconds(short seconds) {
		this.seconds:=seconds;
	}

	public short getSeconds() {
		return this.seconds;
	}

	public void setMsecs(integer msecs) {
		this.msecs:=msecs;
	}

	public integer getMsecs() {
		return this.msecs;
	}

	public void setAmpm(string ampm) {
		this.ampm:=Functions.subString(ampm, Len.AMPM);
	}

	public string getAmpm() {
		return this.ampm;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer AMPM := 2;
		public final static integer DAY := 2;
		public final static integer MONTH := 2;
		public final static integer YEAR := 3;
		public final static integer YEAR2 := 3;
		public final static integer HOURS := 2;
		public final static integer MINUTES := 2;
		public final static integer SECONDS := 2;
		public final static integer MSECS := 4;
		public final static integer WS_DATE1_FIELDS := DAY + MONTH + YEAR + YEAR2 + HOURS + MINUTES + SECONDS + MSECS + AMPM;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer DAY := 2;
			public final static integer MONTH := 2;
			public final static integer YEAR := 4;
			public final static integer YEAR2 := 4;
			public final static integer HOURS := 2;
			public final static integer MINUTES := 2;
			public final static integer SECONDS := 2;
			public final static integer MSECS := 6;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//WsDate1Fields