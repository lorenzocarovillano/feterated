package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.enums.PpcBypassSyncpointMdrvInd;
import com.federatedinsurance.crs.ws.occurs.PpcNonLoggableErrors;
import com.federatedinsurance.crs.ws.occurs.PpcWarnings;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: PROXY-PROGRAM-COMMON<br>
 * Variable: PROXY-PROGRAM-COMMON from copybook TS020PRO<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ProxyProgramCommonXz0g0004 extends SerializableParameter {

	//==== PROPERTIES ====
	public final static integer NON_LOGGABLE_ERRORS_MAXOCCURS := 10;
	public final static integer WARNINGS_MAXOCCURS := 10;
	//Original name: PPC-SERVICE-DATA-SIZE
	private integer serviceDataSize := DefaultValues.BIN_INT_VAL;
	//Original name: PPC-SERVICE-DATA-POINTER
	private integer serviceDataPointer := DefaultValues.BIN_INT_VAL;
	/**Original name: PPC-BYPASS-SYNCPOINT-MDRV-IND<br>
	 * <pre>* FLAG TO INDICATE WHETHER OR NOT MAIN DRIVER DOES SYNCPOINT</pre>*/
	private PpcBypassSyncpointMdrvInd bypassSyncpointMdrvInd := new PpcBypassSyncpointMdrvInd();
	//Original name: PPC-OPERATION
	private string operation := DefaultValues.stringVal(Len.OPERATION);
	//Original name: PPC-ERROR-RETURN-CODE
	private DsdErrorReturnCode errorReturnCode := new DsdErrorReturnCode();
	//Original name: PPC-FATAL-ERROR-MESSAGE
	private string fatalErrorMessage := DefaultValues.stringVal(Len.FATAL_ERROR_MESSAGE);
	//Original name: PPC-NON-LOGGABLE-ERROR-CNT
	private string nonLoggableErrorCnt := DefaultValues.stringVal(Len.NON_LOGGABLE_ERROR_CNT);
	//Original name: PPC-NON-LOGGABLE-ERRORS
	private []PpcNonLoggableErrors nonLoggableErrors := new [NON_LOGGABLE_ERRORS_MAXOCCURS]PpcNonLoggableErrors;
	//Original name: PPC-WARNING-CNT
	private string warningCnt := DefaultValues.stringVal(Len.WARNING_CNT);
	//Original name: PPC-WARNINGS
	private []PpcWarnings warnings := new [WARNINGS_MAXOCCURS]PpcWarnings;

	//==== CONSTRUCTORS ====
	public ProxyProgramCommonXz0g0004() {
		init();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.PROXY_PROGRAM_COMMON;
	}

	@Override
	public void deserialize([]byte buf) {
		setProxyProgramCommonBytes(buf);
	}

	public void init() {
		for int nonLoggableErrorsIdx in 1.. NON_LOGGABLE_ERRORS_MAXOCCURS 
		do
			nonLoggableErrors[nonLoggableErrorsIdx] := new PpcNonLoggableErrors();
		enddo
		for int warningsIdx in 1.. WARNINGS_MAXOCCURS 
		do
			warnings[warningsIdx] := new PpcWarnings();
		enddo
	}

	public void setProxyProgramCommonBytes([]byte buffer) {
		setProxyProgramCommonBytes(buffer, 1);
	}

	public []byte getProxyProgramCommonBytes() {
		[]byte buffer := new [Len.PROXY_PROGRAM_COMMON]byte;
		return getProxyProgramCommonBytes(buffer, 1);
	}

	public void setProxyProgramCommonBytes([]byte buffer, integer offset) {
		integer position := offset;
		setInputParmsBytes(buffer, position);
		position +:= Len.INPUT_PARMS;
		setOutputParmsBytes(buffer, position);
	}

	public []byte getProxyProgramCommonBytes([]byte buffer, integer offset) {
		integer position := offset;
		getInputParmsBytes(buffer, position);
		position +:= Len.INPUT_PARMS;
		getOutputParmsBytes(buffer, position);
		return buffer;
	}

	public void setInputParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		setMemoryAllocationParmsBytes(buffer, position);
		position +:= Len.MEMORY_ALLOCATION_PARMS;
		bypassSyncpointMdrvInd.setPpcBypassSyncpointMdrvInd(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		operation := MarshalByte.readString(buffer, position, Len.OPERATION);
	}

	public []byte getInputParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		getMemoryAllocationParmsBytes(buffer, position);
		position +:= Len.MEMORY_ALLOCATION_PARMS;
		MarshalByte.writeChar(buffer, position, bypassSyncpointMdrvInd.getPpcBypassSyncpointMdrvInd());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, operation, Len.OPERATION);
		return buffer;
	}

	public void setMemoryAllocationParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		serviceDataSize := MarshalByte.readBinaryInt(buffer, position);
		position +:= Types.INT_SIZE;
		serviceDataPointer := MarshalByte.readBinaryInt(buffer, position);
	}

	public []byte getMemoryAllocationParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryInt(buffer, position, serviceDataSize);
		position +:= Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, serviceDataPointer);
		return buffer;
	}

	public void setServiceDataSize(integer serviceDataSize) {
		this.serviceDataSize:=serviceDataSize;
	}

	public integer getServiceDataSize() {
		return this.serviceDataSize;
	}

	public void setServiceDataPointer(integer serviceDataPointer) {
		this.serviceDataPointer:=serviceDataPointer;
	}

	public integer getServiceDataPointer() {
		return this.serviceDataPointer;
	}

	public void setOperation(string operation) {
		this.operation:=Functions.subString(operation, Len.OPERATION);
	}

	public string getOperation() {
		return this.operation;
	}

	public void setOutputParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		setErrorHandlingParmsBytes(buffer, position);
	}

	public []byte getOutputParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		getErrorHandlingParmsBytes(buffer, position);
		return buffer;
	}

	public void setErrorHandlingParmsBytes([]byte buffer) {
		setErrorHandlingParmsBytes(buffer, 1);
	}

	public void setErrorHandlingParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		errorReturnCode.value := MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position +:= DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		fatalErrorMessage := MarshalByte.readString(buffer, position, Len.FATAL_ERROR_MESSAGE);
		position +:= Len.FATAL_ERROR_MESSAGE;
		nonLoggableErrorCnt := MarshalByte.readFixedString(buffer, position, Len.NON_LOGGABLE_ERROR_CNT);
		position +:= Len.NON_LOGGABLE_ERROR_CNT;
		for integer idx in 1.. NON_LOGGABLE_ERRORS_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				nonLoggableErrors[idx].setPpcNonLoggableErrorsBytes(buffer, position);
				position +:= PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS;
			else
				nonLoggableErrors[idx].initPpcNonLoggableErrorsSpaces();
				position +:= PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS;
			endif
		enddo
		warningCnt := MarshalByte.readFixedString(buffer, position, Len.WARNING_CNT);
		position +:= Len.WARNING_CNT;
		for integer idx in 1.. WARNINGS_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				warnings[idx].setPpcWarningsBytes(buffer, position);
				position +:= PpcWarnings.Len.PPC_WARNINGS;
			else
				warnings[idx].initPpcWarningsSpaces();
				position +:= PpcWarnings.Len.PPC_WARNINGS;
			endif
		enddo
	}

	public []byte getErrorHandlingParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, errorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position +:= DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, fatalErrorMessage, Len.FATAL_ERROR_MESSAGE);
		position +:= Len.FATAL_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, nonLoggableErrorCnt, Len.NON_LOGGABLE_ERROR_CNT);
		position +:= Len.NON_LOGGABLE_ERROR_CNT;
		for integer idx in 1.. NON_LOGGABLE_ERRORS_MAXOCCURS 
		do
			nonLoggableErrors[idx].getPpcNonLoggableErrorsBytes(buffer, position);
			position +:= PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS;
		enddo
		MarshalByte.writeString(buffer, position, warningCnt, Len.WARNING_CNT);
		position +:= Len.WARNING_CNT;
		for integer idx in 1.. WARNINGS_MAXOCCURS 
		do
			warnings[idx].getPpcWarningsBytes(buffer, position);
			position +:= PpcWarnings.Len.PPC_WARNINGS;
		enddo
		return buffer;
	}

	public void setFatalErrorMessage(string fatalErrorMessage) {
		this.fatalErrorMessage:=Functions.subString(fatalErrorMessage, Len.FATAL_ERROR_MESSAGE);
	}

	public string getFatalErrorMessage() {
		return this.fatalErrorMessage;
	}

	public string getFatalErrorMessageFormatted() {
		return Functions.padBlanks(getFatalErrorMessage(), Len.FATAL_ERROR_MESSAGE);
	}

	public void setNonLoggableErrorCntFormatted(string nonLoggableErrorCnt) {
		this.nonLoggableErrorCnt:=Trunc.toUnsignedNumeric(nonLoggableErrorCnt, Len.NON_LOGGABLE_ERROR_CNT);
	}

	public short getNonLoggableErrorCnt() {
		return NumericDisplay.asShort(this.nonLoggableErrorCnt);
	}

	public void setWarningCntFormatted(string warningCnt) {
		this.warningCnt:=Trunc.toUnsignedNumeric(warningCnt, Len.WARNING_CNT);
	}

	public short getWarningCnt() {
		return NumericDisplay.asShort(this.warningCnt);
	}

	public PpcBypassSyncpointMdrvInd getBypassSyncpointMdrvInd() {
		return bypassSyncpointMdrvInd;
	}

	public DsdErrorReturnCode getErrorReturnCode() {
		return errorReturnCode;
	}

	public PpcNonLoggableErrors getNonLoggableErrors(integer idx) {
		return nonLoggableErrors[idx];
	}

	public PpcWarnings getWarnings(integer idx) {
		return warnings[idx];
	}

	@Override
	public []byte serialize() {
		return getProxyProgramCommonBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer SERVICE_DATA_SIZE := 4;
		public final static integer SERVICE_DATA_POINTER := 4;
		public final static integer MEMORY_ALLOCATION_PARMS := SERVICE_DATA_SIZE + SERVICE_DATA_POINTER;
		public final static integer OPERATION := 32;
		public final static integer INPUT_PARMS := MEMORY_ALLOCATION_PARMS + PpcBypassSyncpointMdrvInd.Len.PPC_BYPASS_SYNCPOINT_MDRV_IND + OPERATION;
		public final static integer FATAL_ERROR_MESSAGE := 250;
		public final static integer NON_LOGGABLE_ERROR_CNT := 4;
		public final static integer WARNING_CNT := 4;
		public final static integer ERROR_HANDLING_PARMS := DsdErrorReturnCode.Len.ERROR_RETURN_CODE + FATAL_ERROR_MESSAGE + NON_LOGGABLE_ERROR_CNT + ProxyProgramCommonXz0g0004.NON_LOGGABLE_ERRORS_MAXOCCURS * PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS + WARNING_CNT + ProxyProgramCommonXz0g0004.WARNINGS_MAXOCCURS * PpcWarnings.Len.PPC_WARNINGS;
		public final static integer OUTPUT_PARMS := ERROR_HANDLING_PARMS;
		public final static integer PROXY_PROGRAM_COMMON := INPUT_PARMS + OUTPUT_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//ProxyProgramCommonXz0g0004