package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P90A0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p90a0 {

	//==== PROPERTIES ====
	//Original name: WS-MAX-TTY-CERT-ROWS
	private short maxTtyCertRows := DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-MAX-TTY-ROWS
	private short maxTtyRows := DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-EIBRESP-CD
	private short eibrespCd := DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd := DefaultValues.SHORT_VAL;
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo := new WsErrorCheckInfo();
	//Original name: WS-PROGRAM-NAME
	private string programName := "XZ0P90A0";
	//Original name: WS-APPLICATION-NM
	private string applicationNm := "CRS";
	//Original name: WS-BUS-OBJ-NM-TTY-LIST
	private string busObjNmTtyList := "XZ_PREPARE_THIRD_PARTY_LIST";
	//Original name: WS-OPERATIONS-CALLED
	private WsOperationsCalledXz0p90a0 operationsCalled := new WsOperationsCalledXz0p90a0();


	//==== METHODS ====
	public void setMaxTtyCertRows(short maxTtyCertRows) {
		this.maxTtyCertRows:=maxTtyCertRows;
	}

	public short getMaxTtyCertRows() {
		return this.maxTtyCertRows;
	}

	public void setMaxTtyRows(short maxTtyRows) {
		this.maxTtyRows:=maxTtyRows;
	}

	public short getMaxTtyRows() {
		return this.maxTtyRows;
	}

	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd:=eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public string getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public string getEibrespCdAsString() {
		return getEibrespCdFormatted();
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd:=eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public string getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public string getEibresp2CdAsString() {
		return getEibresp2CdFormatted();
	}

	public string getProgramName() {
		return this.programName;
	}

	public string getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public string getApplicationNm() {
		return this.applicationNm;
	}

	public string getBusObjNmTtyList() {
		return this.busObjNmTtyList;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	public WsOperationsCalledXz0p90a0 getOperationsCalled() {
		return operationsCalled;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer PROGRAM_NAME := 8;
		public final static integer EIBRESP_CD := 4;
		public final static integer EIBRESP2_CD := 4;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WorkingStorageAreaXz0p90a0