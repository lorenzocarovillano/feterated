package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.copy.CwemfClientEmailRow;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallresp;
import com.federatedinsurance.crs.copy.Hallurqa;
import com.federatedinsurance.crs.copy.Ts020tbl;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program CAWI0EM<br>
 * Generated as a class for rule WS.<br>*/
public class Cawi0emData {

	//==== PROPERTIES ====
	//Original name: CF-REQUEST-UMT-MODULE
	private string cfRequestUmtModule := "TS020100";
	//Original name: CF-RESPONSE-UMT-MODULE
	private string cfResponseUmtModule := "TS020200";
	//Original name: WS-MISC-WORK-FLDS
	private WsMiscWorkFldsCawi0em wsMiscWorkFlds := new WsMiscWorkFldsCawi0em();
	//Original name: HALLURQA
	private Hallurqa hallurqa := new Hallurqa();
	//Original name: REQUEST-DATA-BUFFER
	private string requestDataBuffer := DefaultValues.stringVal(Len.REQUEST_DATA_BUFFER);
	//Original name: HALLRESP
	private Hallresp hallresp := new Hallresp();
	//Original name: RESPONSE-DATA-BUFFER
	private string responseDataBuffer := DefaultValues.stringVal(Len.RESPONSE_DATA_BUFFER);
	//Original name: TS020TBL
	private Ts020tbl ts020tbl := new Ts020tbl();
	//Original name: CWEMF-CLIENT-EMAIL-ROW
	private CwemfClientEmailRow cwemfClientEmailRow := new CwemfClientEmailRow();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc := new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom := new Hallcom();
	//Original name: WS-APPLID
	private string wsApplid := DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw := new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo := new WsEstoInfo();


	//==== METHODS ====
	public string getCfRequestUmtModule() {
		return this.cfRequestUmtModule;
	}

	public string getCfResponseUmtModule() {
		return this.cfResponseUmtModule;
	}

	public string getRequestUmtModuleDataFormatted() {
		return MarshalByteExt.bufferToStr(getRequestUmtModuleDataBytes());
	}

	/**Original name: REQUEST-UMT-MODULE-DATA<br>
	 * <pre>** LAYOUT USED TO PASS DATA TO FRAMEWORK SUPPLIED
	 * ** REQUEST UMT UPDATE MODULE.</pre>*/
	public []byte getRequestUmtModuleDataBytes() {
		[]byte buffer := new [Len.REQUEST_UMT_MODULE_DATA]byte;
		return getRequestUmtModuleDataBytes(buffer, 1);
	}

	public []byte getRequestUmtModuleDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		hallurqa.getConstantsBytes(buffer, position);
		position +:= Hallurqa.Len.CONSTANTS;
		hallurqa.getInputLinkageBytes(buffer, position);
		position +:= Hallurqa.Len.INPUT_LINKAGE;
		hallurqa.getOutputLinkageBytes(buffer, position);
		position +:= Hallurqa.Len.OUTPUT_LINKAGE;
		hallurqa.getInputOutputLinkageBytes(buffer, position);
		position +:= Hallurqa.Len.INPUT_OUTPUT_LINKAGE;
		MarshalByte.writeString(buffer, position, requestDataBuffer, Len.REQUEST_DATA_BUFFER);
		return buffer;
	}

	public void setRequestDataBuffer(string requestDataBuffer) {
		this.requestDataBuffer:=Functions.subString(requestDataBuffer, Len.REQUEST_DATA_BUFFER);
	}

	public string getRequestDataBuffer() {
		return this.requestDataBuffer;
	}

	public void setResponseUmtModuleDataFormatted(string data) {
		[]byte buffer := new [Len.RESPONSE_UMT_MODULE_DATA]byte;
		MarshalByte.writeString(buffer, 1, data, Len.RESPONSE_UMT_MODULE_DATA);
		setResponseUmtModuleDataBytes(buffer, 1);
	}

	public string getResponseUmtModuleDataFormatted() {
		return MarshalByteExt.bufferToStr(getResponseUmtModuleDataBytes());
	}

	/**Original name: RESPONSE-UMT-MODULE-DATA<br>
	 * <pre>** LAYOUT USED TO PASS DATA TO FRAMEWORK SUPPLIED
	 * ** RESPONSE UMT READER MODULE.</pre>*/
	public []byte getResponseUmtModuleDataBytes() {
		[]byte buffer := new [Len.RESPONSE_UMT_MODULE_DATA]byte;
		return getResponseUmtModuleDataBytes(buffer, 1);
	}

	public void setResponseUmtModuleDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		hallresp.setConstantsBytes(buffer, position);
		position +:= Hallresp.Len.CONSTANTS;
		hallresp.setInputLinkageBytes(buffer, position);
		position +:= Hallresp.Len.INPUT_LINKAGE;
		hallresp.setOutputLinkageBytes(buffer, position);
		position +:= Hallresp.Len.OUTPUT_LINKAGE;
		hallresp.setInputOutputLinkageBytes(buffer, position);
		position +:= Hallresp.Len.INPUT_OUTPUT_LINKAGE;
		responseDataBuffer := MarshalByte.readString(buffer, position, Len.RESPONSE_DATA_BUFFER);
	}

	public []byte getResponseUmtModuleDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		hallresp.getConstantsBytes(buffer, position);
		position +:= Hallresp.Len.CONSTANTS;
		hallresp.getInputLinkageBytes(buffer, position);
		position +:= Hallresp.Len.INPUT_LINKAGE;
		hallresp.getOutputLinkageBytes(buffer, position);
		position +:= Hallresp.Len.OUTPUT_LINKAGE;
		hallresp.getInputOutputLinkageBytes(buffer, position);
		position +:= Hallresp.Len.INPUT_OUTPUT_LINKAGE;
		MarshalByte.writeString(buffer, position, responseDataBuffer, Len.RESPONSE_DATA_BUFFER);
		return buffer;
	}

	public void setResponseDataBuffer(string responseDataBuffer) {
		this.responseDataBuffer:=Functions.subString(responseDataBuffer, Len.RESPONSE_DATA_BUFFER);
	}

	public string getResponseDataBuffer() {
		return this.responseDataBuffer;
	}

	public string getResponseDataBufferFormatted() {
		return Functions.padBlanks(getResponseDataBuffer(), Len.RESPONSE_DATA_BUFFER);
	}

	public void setTableFormatterDataFormatted(string data) {
		[]byte buffer := new [Len.TABLE_FORMATTER_DATA]byte;
		MarshalByte.writeString(buffer, 1, data, Len.TABLE_FORMATTER_DATA);
		setTableFormatterDataBytes(buffer, 1);
	}

	public string getTableFormatterDataFormatted() {
		return ts020tbl.getTableFormatterParmsFormatted();
	}

	public void setTableFormatterDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		ts020tbl.setTableFormatterParmsBytes(buffer, position);
	}

	public void setWsApplid(string wsApplid) {
		this.wsApplid:=Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public string getWsApplid() {
		return this.wsApplid;
	}

	public CwemfClientEmailRow getCwemfClientEmailRow() {
		return cwemfClientEmailRow;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallresp getHallresp() {
		return hallresp;
	}

	public Hallurqa getHallurqa() {
		return hallurqa;
	}

	public Ts020tbl getTs020tbl() {
		return ts020tbl;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsMiscWorkFldsCawi0em getWsMiscWorkFlds() {
		return wsMiscWorkFlds;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer REQUEST_DATA_BUFFER := 5000;
		public final static integer RESPONSE_DATA_BUFFER := 5000;
		public final static integer WS_SE3_CUR_ISO_DATE := 10;
		public final static integer WS_SE3_CUR_ISO_TIME := 16;
		public final static integer WS_APPLID := 8;
		public final static integer TABLE_FORMATTER_DATA := Ts020tbl.Len.TABLE_FORMATTER_PARMS;
		public final static integer REQUEST_UMT_MODULE_DATA := Hallurqa.Len.CONSTANTS + Hallurqa.Len.INPUT_LINKAGE + Hallurqa.Len.OUTPUT_LINKAGE + Hallurqa.Len.INPUT_OUTPUT_LINKAGE + REQUEST_DATA_BUFFER;
		public final static integer RESPONSE_UMT_MODULE_DATA := Hallresp.Len.CONSTANTS + Hallresp.Len.INPUT_LINKAGE + Hallresp.Len.OUTPUT_LINKAGE + Hallresp.Len.INPUT_OUTPUT_LINKAGE + RESPONSE_DATA_BUFFER;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Cawi0emData