package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ003O-EXT-MSG-STA-LIS<br>
 * Variables: XZ003O-EXT-MSG-STA-LIS from copybook XZ03CI1O<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xz003oExtMsgStaLis {

	//==== PROPERTIES ====
	//Original name: XZ003O-EXT-MSG-STA-CD
	private string staCd := DefaultValues.stringVal(Len.STA_CD);
	//Original name: XZ003O-EXT-MSG-STA-DES
	private string staDes := DefaultValues.stringVal(Len.STA_DES);
	//Original name: XZ003O-EXT-MSG-ID-RFR
	private string idRfr := DefaultValues.stringVal(Len.ID_RFR);


	//==== METHODS ====
	public void setExtMsgStaLisBytes([]byte buffer, integer offset) {
		integer position := offset;
		staCd := MarshalByte.readString(buffer, position, Len.STA_CD);
		position +:= Len.STA_CD;
		staDes := MarshalByte.readString(buffer, position, Len.STA_DES);
		position +:= Len.STA_DES;
		idRfr := MarshalByte.readString(buffer, position, Len.ID_RFR);
	}

	public []byte getExtMsgStaLisBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, staCd, Len.STA_CD);
		position +:= Len.STA_CD;
		MarshalByte.writeString(buffer, position, staDes, Len.STA_DES);
		position +:= Len.STA_DES;
		MarshalByte.writeString(buffer, position, idRfr, Len.ID_RFR);
		return buffer;
	}

	public void initExtMsgStaLisLowValues() {
		staCd := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.STA_CD);
		staDes := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.STA_DES);
		idRfr := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.ID_RFR);
	}

	public void initExtMsgStaLisSpaces() {
		staCd := "";
		staDes := "";
		idRfr := "";
	}

	public void setStaCd(string staCd) {
		this.staCd:=Functions.subString(staCd, Len.STA_CD);
	}

	public string getStaCd() {
		return this.staCd;
	}

	public void setStaDes(string staDes) {
		this.staDes:=Functions.subString(staDes, Len.STA_DES);
	}

	public string getStaDes() {
		return this.staDes;
	}

	public void setIdRfr(string idRfr) {
		this.idRfr:=Functions.subString(idRfr, Len.ID_RFR);
	}

	public string getIdRfr() {
		return this.idRfr;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer STA_CD := 25;
		public final static integer STA_DES := 255;
		public final static integer ID_RFR := 36;
		public final static integer EXT_MSG_STA_LIS := STA_CD + STA_DES + ID_RFR;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xz003oExtMsgStaLis