package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.lang.ICopyable;

/**Original name: XZ005O-CER-INF<br>
 * Variables: XZ005O-CER-INF from copybook XZ05CL1O<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xz005oCerInf implements <Xz005oCerInf>ICopyable {

	//==== PROPERTIES ====
	public final static integer WRD_INF_MAXOCCURS := 20;
	//Original name: XZ005O-STA-CD
	private char staCd := DefaultValues.CHAR_VAL;
	//Original name: XZ005O-CER-NBR
	private string cerNbr := DefaultValues.stringVal(Len.CER_NBR);
	//Original name: XZ005O-ACY-TYP-CD
	private char acyTypCd := DefaultValues.CHAR_VAL;
	//Original name: XZ005O-CER-HLD-ID
	private string cerHldId := DefaultValues.stringVal(Len.CER_HLD_ID);
	//Original name: XZ005O-CER-HLD-NM
	private string cerHldNm := DefaultValues.stringVal(Len.CER_HLD_NM);
	//Original name: XZ005O-OTH-INF-TYP-CD
	private string othInfTypCd := DefaultValues.stringVal(Len.OTH_INF_TYP_CD);
	//Original name: XZ005O-OTH-INF-PFX
	private string othInfPfx := DefaultValues.stringVal(Len.OTH_INF_PFX);
	//Original name: XZ005O-OTH-INF-TXT
	private string othInfTxt := DefaultValues.stringVal(Len.OTH_INF_TXT);
	//Original name: XZ005O-CER-HLD-ADR-ID
	private string cerHldAdrId := DefaultValues.stringVal(Len.CER_HLD_ADR_ID);
	//Original name: XZ005O-CER-HLD-ADR1
	private string cerHldAdr1 := DefaultValues.stringVal(Len.CER_HLD_ADR1);
	//Original name: XZ005O-CER-HLD-ADR2
	private string cerHldAdr2 := DefaultValues.stringVal(Len.CER_HLD_ADR2);
	//Original name: XZ005O-CER-HLD-CIT
	private string cerHldCit := DefaultValues.stringVal(Len.CER_HLD_CIT);
	//Original name: XZ005O-CER-HLD-ST-ABB
	private string cerHldStAbb := DefaultValues.stringVal(Len.CER_HLD_ST_ABB);
	//Original name: XZ005O-CER-HLD-PST-CD
	private string cerHldPstCd := DefaultValues.stringVal(Len.CER_HLD_PST_CD);
	//Original name: XZ005O-CER-HLD-ADD-INS-IND
	private char cerHldAddInsInd := DefaultValues.CHAR_VAL;
	//Original name: XZ005O-CERT-RNL-STATUS-CD
	private char certRnlStatusCd := DefaultValues.CHAR_VAL;
	//Original name: XZ005O-WRD-INF
	private []Xz005oWrdInf wrdInf := new [WRD_INF_MAXOCCURS]Xz005oWrdInf;

	//==== CONSTRUCTORS ====
	public Xz005oCerInf() {
		init();
	}
	public Xz005oCerInf(Xz005oCerInf xz005oCerInf) {
		this();
		this.staCd := xz005oCerInf.staCd;
		this.cerNbr := xz005oCerInf.cerNbr;
		this.acyTypCd := xz005oCerInf.acyTypCd;
		this.cerHldId := xz005oCerInf.cerHldId;
		this.cerHldNm := xz005oCerInf.cerHldNm;
		this.othInfTypCd := xz005oCerInf.othInfTypCd;
		this.othInfPfx := xz005oCerInf.othInfPfx;
		this.othInfTxt := xz005oCerInf.othInfTxt;
		this.cerHldAdrId := xz005oCerInf.cerHldAdrId;
		this.cerHldAdr1 := xz005oCerInf.cerHldAdr1;
		this.cerHldAdr2 := xz005oCerInf.cerHldAdr2;
		this.cerHldCit := xz005oCerInf.cerHldCit;
		this.cerHldStAbb := xz005oCerInf.cerHldStAbb;
		this.cerHldPstCd := xz005oCerInf.cerHldPstCd;
		this.cerHldAddInsInd := xz005oCerInf.cerHldAddInsInd;
		this.certRnlStatusCd := xz005oCerInf.certRnlStatusCd;
		for int wrdInfIdx in 1.. Xz005oCerInf.WRD_INF_MAXOCCURS 
		do
			this.wrdInf[wrdInfIdx] := xz005oCerInf.wrdInf[wrdInfIdx].copy();
		enddo
	}

	//==== METHODS ====
	public void init() {
		for int wrdInfIdx in 1.. WRD_INF_MAXOCCURS 
		do
			wrdInf[wrdInfIdx] := new Xz005oWrdInf();
		enddo
	}

	public void setXz005oCerInfBytes([]byte buffer, integer offset) {
		integer position := offset;
		staCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		cerNbr := MarshalByte.readString(buffer, position, Len.CER_NBR);
		position +:= Len.CER_NBR;
		acyTypCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		cerHldId := MarshalByte.readString(buffer, position, Len.CER_HLD_ID);
		position +:= Len.CER_HLD_ID;
		cerHldNm := MarshalByte.readString(buffer, position, Len.CER_HLD_NM);
		position +:= Len.CER_HLD_NM;
		othInfTypCd := MarshalByte.readString(buffer, position, Len.OTH_INF_TYP_CD);
		position +:= Len.OTH_INF_TYP_CD;
		othInfPfx := MarshalByte.readString(buffer, position, Len.OTH_INF_PFX);
		position +:= Len.OTH_INF_PFX;
		othInfTxt := MarshalByte.readString(buffer, position, Len.OTH_INF_TXT);
		position +:= Len.OTH_INF_TXT;
		cerHldAdrId := MarshalByte.readString(buffer, position, Len.CER_HLD_ADR_ID);
		position +:= Len.CER_HLD_ADR_ID;
		cerHldAdr1 := MarshalByte.readString(buffer, position, Len.CER_HLD_ADR1);
		position +:= Len.CER_HLD_ADR1;
		cerHldAdr2 := MarshalByte.readString(buffer, position, Len.CER_HLD_ADR2);
		position +:= Len.CER_HLD_ADR2;
		cerHldCit := MarshalByte.readString(buffer, position, Len.CER_HLD_CIT);
		position +:= Len.CER_HLD_CIT;
		cerHldStAbb := MarshalByte.readString(buffer, position, Len.CER_HLD_ST_ABB);
		position +:= Len.CER_HLD_ST_ABB;
		cerHldPstCd := MarshalByte.readString(buffer, position, Len.CER_HLD_PST_CD);
		position +:= Len.CER_HLD_PST_CD;
		cerHldAddInsInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		certRnlStatusCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		for integer idx in 1.. WRD_INF_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				wrdInf[idx].setWrdInfBytes(buffer, position);
				position +:= Xz005oWrdInf.Len.WRD_INF;
			else
				wrdInf[idx].initWrdInfSpaces();
				position +:= Xz005oWrdInf.Len.WRD_INF;
			endif
		enddo
	}

	public []byte getXz005oCerInfBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, staCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cerNbr, Len.CER_NBR);
		position +:= Len.CER_NBR;
		MarshalByte.writeChar(buffer, position, acyTypCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cerHldId, Len.CER_HLD_ID);
		position +:= Len.CER_HLD_ID;
		MarshalByte.writeString(buffer, position, cerHldNm, Len.CER_HLD_NM);
		position +:= Len.CER_HLD_NM;
		MarshalByte.writeString(buffer, position, othInfTypCd, Len.OTH_INF_TYP_CD);
		position +:= Len.OTH_INF_TYP_CD;
		MarshalByte.writeString(buffer, position, othInfPfx, Len.OTH_INF_PFX);
		position +:= Len.OTH_INF_PFX;
		MarshalByte.writeString(buffer, position, othInfTxt, Len.OTH_INF_TXT);
		position +:= Len.OTH_INF_TXT;
		MarshalByte.writeString(buffer, position, cerHldAdrId, Len.CER_HLD_ADR_ID);
		position +:= Len.CER_HLD_ADR_ID;
		MarshalByte.writeString(buffer, position, cerHldAdr1, Len.CER_HLD_ADR1);
		position +:= Len.CER_HLD_ADR1;
		MarshalByte.writeString(buffer, position, cerHldAdr2, Len.CER_HLD_ADR2);
		position +:= Len.CER_HLD_ADR2;
		MarshalByte.writeString(buffer, position, cerHldCit, Len.CER_HLD_CIT);
		position +:= Len.CER_HLD_CIT;
		MarshalByte.writeString(buffer, position, cerHldStAbb, Len.CER_HLD_ST_ABB);
		position +:= Len.CER_HLD_ST_ABB;
		MarshalByte.writeString(buffer, position, cerHldPstCd, Len.CER_HLD_PST_CD);
		position +:= Len.CER_HLD_PST_CD;
		MarshalByte.writeChar(buffer, position, cerHldAddInsInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, certRnlStatusCd);
		position +:= Types.CHAR_SIZE;
		for integer idx in 1.. WRD_INF_MAXOCCURS 
		do
			wrdInf[idx].getWrdInfBytes(buffer, position);
			position +:= Xz005oWrdInf.Len.WRD_INF;
		enddo
		return buffer;
	}

	public Xz005oCerInf initXz005oCerInfLowValues() {
		staCd := Types.LOW_CHAR_VAL;
		cerNbr := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CER_NBR);
		acyTypCd := Types.LOW_CHAR_VAL;
		cerHldId := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CER_HLD_ID);
		cerHldNm := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CER_HLD_NM);
		othInfTypCd := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.OTH_INF_TYP_CD);
		othInfPfx := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.OTH_INF_PFX);
		othInfTxt := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.OTH_INF_TXT);
		cerHldAdrId := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CER_HLD_ADR_ID);
		cerHldAdr1 := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CER_HLD_ADR1);
		cerHldAdr2 := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CER_HLD_ADR2);
		cerHldCit := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CER_HLD_CIT);
		cerHldStAbb := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CER_HLD_ST_ABB);
		cerHldPstCd := LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CER_HLD_PST_CD);
		cerHldAddInsInd := Types.LOW_CHAR_VAL;
		certRnlStatusCd := Types.LOW_CHAR_VAL;
		for integer idx in 1.. WRD_INF_MAXOCCURS 
		do
			wrdInf[idx].initWrdInfLowValues();
		enddo
		return this;
	}

	public Xz005oCerInf initXz005oCerInfSpaces() {
		staCd := Types.SPACE_CHAR;
		cerNbr := "";
		acyTypCd := Types.SPACE_CHAR;
		cerHldId := "";
		cerHldNm := "";
		othInfTypCd := "";
		othInfPfx := "";
		othInfTxt := "";
		cerHldAdrId := "";
		cerHldAdr1 := "";
		cerHldAdr2 := "";
		cerHldCit := "";
		cerHldStAbb := "";
		cerHldPstCd := "";
		cerHldAddInsInd := Types.SPACE_CHAR;
		certRnlStatusCd := Types.SPACE_CHAR;
		for integer idx in 1.. WRD_INF_MAXOCCURS 
		do
			wrdInf[idx].initWrdInfSpaces();
		enddo
		return this;
	}

	public void setStaCd(char staCd) {
		this.staCd:=staCd;
	}

	public char getStaCd() {
		return this.staCd;
	}

	public void setCerNbr(string cerNbr) {
		this.cerNbr:=Functions.subString(cerNbr, Len.CER_NBR);
	}

	public string getCerNbr() {
		return this.cerNbr;
	}

	public void setAcyTypCd(char acyTypCd) {
		this.acyTypCd:=acyTypCd;
	}

	public char getAcyTypCd() {
		return this.acyTypCd;
	}

	public void setCerHldId(string cerHldId) {
		this.cerHldId:=Functions.subString(cerHldId, Len.CER_HLD_ID);
	}

	public string getCerHldId() {
		return this.cerHldId;
	}

	public void setCerHldNm(string cerHldNm) {
		this.cerHldNm:=Functions.subString(cerHldNm, Len.CER_HLD_NM);
	}

	public string getCerHldNm() {
		return this.cerHldNm;
	}

	public void setOthInfTypCd(string othInfTypCd) {
		this.othInfTypCd:=Functions.subString(othInfTypCd, Len.OTH_INF_TYP_CD);
	}

	public string getOthInfTypCd() {
		return this.othInfTypCd;
	}

	public void setOthInfPfx(string othInfPfx) {
		this.othInfPfx:=Functions.subString(othInfPfx, Len.OTH_INF_PFX);
	}

	public string getOthInfPfx() {
		return this.othInfPfx;
	}

	public void setOthInfTxt(string othInfTxt) {
		this.othInfTxt:=Functions.subString(othInfTxt, Len.OTH_INF_TXT);
	}

	public string getOthInfTxt() {
		return this.othInfTxt;
	}

	public void setCerHldAdrId(string cerHldAdrId) {
		this.cerHldAdrId:=Functions.subString(cerHldAdrId, Len.CER_HLD_ADR_ID);
	}

	public string getCerHldAdrId() {
		return this.cerHldAdrId;
	}

	public void setCerHldAdr1(string cerHldAdr1) {
		this.cerHldAdr1:=Functions.subString(cerHldAdr1, Len.CER_HLD_ADR1);
	}

	public string getCerHldAdr1() {
		return this.cerHldAdr1;
	}

	public void setCerHldAdr2(string cerHldAdr2) {
		this.cerHldAdr2:=Functions.subString(cerHldAdr2, Len.CER_HLD_ADR2);
	}

	public string getCerHldAdr2() {
		return this.cerHldAdr2;
	}

	public void setCerHldCit(string cerHldCit) {
		this.cerHldCit:=Functions.subString(cerHldCit, Len.CER_HLD_CIT);
	}

	public string getCerHldCit() {
		return this.cerHldCit;
	}

	public void setCerHldStAbb(string cerHldStAbb) {
		this.cerHldStAbb:=Functions.subString(cerHldStAbb, Len.CER_HLD_ST_ABB);
	}

	public string getCerHldStAbb() {
		return this.cerHldStAbb;
	}

	public void setCerHldPstCd(string cerHldPstCd) {
		this.cerHldPstCd:=Functions.subString(cerHldPstCd, Len.CER_HLD_PST_CD);
	}

	public string getCerHldPstCd() {
		return this.cerHldPstCd;
	}

	public void setCerHldAddInsInd(char cerHldAddInsInd) {
		this.cerHldAddInsInd:=cerHldAddInsInd;
	}

	public char getCerHldAddInsInd() {
		return this.cerHldAddInsInd;
	}

	public void setCertRnlStatusCd(char certRnlStatusCd) {
		this.certRnlStatusCd:=certRnlStatusCd;
	}

	public char getCertRnlStatusCd() {
		return this.certRnlStatusCd;
	}

	public Xz005oWrdInf getWrdInf(integer idx) {
		return wrdInf[idx];
	}

	public Xz005oCerInf copy() {
		return new Xz005oCerInf(this);
	}

	public Xz005oCerInf initXz005oCerInf() {
		staCd := Types.SPACE_CHAR;
		cerNbr := "";
		acyTypCd := Types.SPACE_CHAR;
		cerHldId := "";
		cerHldNm := "";
		othInfTypCd := "";
		othInfPfx := "";
		othInfTxt := "";
		cerHldAdrId := "";
		cerHldAdr1 := "";
		cerHldAdr2 := "";
		cerHldCit := "";
		cerHldStAbb := "";
		cerHldPstCd := "";
		cerHldAddInsInd := Types.SPACE_CHAR;
		certRnlStatusCd := Types.SPACE_CHAR;
		for integer idx0 in 1.. Xz005oCerInf.WRD_INF_MAXOCCURS 
		do
			wrdInf[idx0].setXz005oWrdNm("");
		enddo
		return this;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CER_NBR := 25;
		public final static integer CER_HLD_ID := 64;
		public final static integer CER_HLD_NM := 120;
		public final static integer OTH_INF_TYP_CD := 3;
		public final static integer OTH_INF_PFX := 10;
		public final static integer OTH_INF_TXT := 45;
		public final static integer CER_HLD_ADR_ID := 64;
		public final static integer CER_HLD_ADR1 := 45;
		public final static integer CER_HLD_ADR2 := 45;
		public final static integer CER_HLD_CIT := 30;
		public final static integer CER_HLD_ST_ABB := 2;
		public final static integer CER_HLD_PST_CD := 13;
		public final static integer STA_CD := 1;
		public final static integer ACY_TYP_CD := 1;
		public final static integer CER_HLD_ADD_INS_IND := 1;
		public final static integer CERT_RNL_STATUS_CD := 1;
		public final static integer XZ005O_CER_INF := STA_CD + CER_NBR + ACY_TYP_CD + CER_HLD_ID + CER_HLD_NM + OTH_INF_TYP_CD + OTH_INF_PFX + OTH_INF_TXT + CER_HLD_ADR_ID + CER_HLD_ADR1 + CER_HLD_ADR2 + CER_HLD_CIT + CER_HLD_ST_ABB + CER_HLD_PST_CD + CER_HLD_ADD_INS_IND + CERT_RNL_STATUS_CD + Xz005oCerInf.WRD_INF_MAXOCCURS * Xz005oWrdInf.Len.WRD_INF;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xz005oCerInf