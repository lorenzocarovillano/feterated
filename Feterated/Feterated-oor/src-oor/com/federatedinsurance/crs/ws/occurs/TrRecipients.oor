package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TR-RECIPIENTS<br>
 * Variables: TR-RECIPIENTS from program XZ0P90C0<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TrRecipients implements <TrRecipients>ICopyable {

	//==== PROPERTIES ====
	//Original name: TR-REC-CLT-ID
	private string recCltId := DefaultValues.stringVal(Len.REC_CLT_ID);
	public final static string END_OF_TABLE := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_CLT_ID);
	//Original name: TR-REC-ADR-ID
	private string recAdrId := DefaultValues.stringVal(Len.REC_ADR_ID);
	//Original name: TR-REC-NM
	private string recNm := DefaultValues.stringVal(Len.REC_NM);
	//Original name: TR-LIN-1-ADR
	private string lin1Adr := DefaultValues.stringVal(Len.LIN1_ADR);
	//Original name: TR-CIT-NM
	private string citNm := DefaultValues.stringVal(Len.CIT_NM);
	//Original name: TR-ST-ABB
	private string stAbb := DefaultValues.stringVal(Len.ST_ABB);

	//==== CONSTRUCTORS ====
	public TrRecipients() {	}
	public TrRecipients(TrRecipients trRecipients) {
		this();
		this.recCltId := trRecipients.recCltId;
		this.recAdrId := trRecipients.recAdrId;
		this.recNm := trRecipients.recNm;
		this.lin1Adr := trRecipients.lin1Adr;
		this.citNm := trRecipients.citNm;
		this.stAbb := trRecipients.stAbb;
	}

	//==== METHODS ====
	public TrRecipients initTrRecipientsHighValues() {
		recCltId := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_CLT_ID);
		recAdrId := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_ADR_ID);
		recNm := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_NM);
		lin1Adr := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LIN1_ADR);
		citNm := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CIT_NM);
		stAbb := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ST_ABB);
		return this;
	}

	public void setRecCltId(string recCltId) {
		this.recCltId:=Functions.subString(recCltId, Len.REC_CLT_ID);
	}

	public string getRecCltId() {
		return this.recCltId;
	}

	public boolean isEndOfTable() {
		return recCltId = END_OF_TABLE;
	}

	public void setRecAdrId(string recAdrId) {
		this.recAdrId:=Functions.subString(recAdrId, Len.REC_ADR_ID);
	}

	public string getRecAdrId() {
		return this.recAdrId;
	}

	public void setRecNm(string recNm) {
		this.recNm:=Functions.subString(recNm, Len.REC_NM);
	}

	public string getRecNm() {
		return this.recNm;
	}

	public void setLin1Adr(string lin1Adr) {
		this.lin1Adr:=Functions.subString(lin1Adr, Len.LIN1_ADR);
	}

	public string getLin1Adr() {
		return this.lin1Adr;
	}

	public void setCitNm(string citNm) {
		this.citNm:=Functions.subString(citNm, Len.CIT_NM);
	}

	public string getCitNm() {
		return this.citNm;
	}

	public void setStAbb(string stAbb) {
		this.stAbb:=Functions.subString(stAbb, Len.ST_ABB);
	}

	public string getStAbb() {
		return this.stAbb;
	}

	public TrRecipients copy() {
		return new TrRecipients(this);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer REC_CLT_ID := 64;
		public final static integer REC_ADR_ID := 64;
		public final static integer REC_NM := 120;
		public final static integer LIN1_ADR := 45;
		public final static integer CIT_NM := 30;
		public final static integer ST_ABB := 2;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//TrRecipients