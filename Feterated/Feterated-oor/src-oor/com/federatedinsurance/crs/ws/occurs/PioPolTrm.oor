package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.copy.PioTrsDtl;

/**Original name: PIO-POL-TRM<br>
 * Variables: PIO-POL-TRM from copybook XZC0690O<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class PioPolTrm {

	//==== PROPERTIES ====
	public final static integer LOCV_DATA_MAXOCCURS := 10;
	//Original name: PIO-POL-NBR
	private string polNbr := DefaultValues.stringVal(Len.POL_NBR);
	//Original name: PIO-POL-KEY
	private string polKey := DefaultValues.stringVal(Len.POL_KEY);
	//Original name: PIO-QTE-NBR
	private integer qteNbr := DefaultValues.INT_VAL;
	//Original name: PIO-EFF-DT
	private string effDt := DefaultValues.stringVal(Len.EFF_DT);
	//Original name: PIO-EXP-DT
	private string expDt := DefaultValues.stringVal(Len.EXP_DT);
	//Original name: PIO-ACT-NBR
	private string actNbr := DefaultValues.stringVal(Len.ACT_NBR);
	//Original name: PIO-ACT-NBR-FMT
	private string actNbrFmt := DefaultValues.stringVal(Len.ACT_NBR_FMT);
	//Original name: PIO-BSE-ST-ABB
	private string bseStAbb := DefaultValues.stringVal(Len.BSE_ST_ABB);
	//Original name: PIO-BSE-ST-NM
	private string bseStNm := DefaultValues.stringVal(Len.BSE_ST_NM);
	//Original name: PIO-PRD-CD
	private string prdCd := DefaultValues.stringVal(Len.PRD_CD);
	//Original name: PIO-PRD-DES
	private string prdDes := DefaultValues.stringVal(Len.PRD_DES);
	//Original name: PIO-TOB-CD
	private string tobCd := DefaultValues.stringVal(Len.TOB_CD);
	//Original name: PIO-TOB-DES
	private string tobDes := DefaultValues.stringVal(Len.TOB_DES);
	//Original name: PIO-SEG-CD
	private string segCd := DefaultValues.stringVal(Len.SEG_CD);
	//Original name: PIO-SEG-DES
	private string segDes := DefaultValues.stringVal(Len.SEG_DES);
	//Original name: PIO-LA-IND
	private char laInd := DefaultValues.CHAR_VAL;
	//Original name: PIO-MNL-POL-IND
	private char mnlPolInd := DefaultValues.CHAR_VAL;
	//Original name: PIO-CS-CD
	private char csCd := DefaultValues.CHAR_VAL;
	//Original name: PIO-CS-DES
	private string csDes := DefaultValues.stringVal(Len.CS_DES);
	//Original name: PIO-TRS-DTL
	private PioTrsDtl trsDtl := new PioTrsDtl();
	//Original name: PIO-LOCV-DATA
	private []PioLocvData locvData := new [LOCV_DATA_MAXOCCURS]PioLocvData;
	//Original name: PIO-BND-IND
	private char bndInd := DefaultValues.CHAR_VAL;
	//Original name: PIO-OFC-CD
	private string ofcCd := DefaultValues.stringVal(Len.OFC_CD);
	//Original name: PIO-OFC-DES
	private string ofcDes := DefaultValues.stringVal(Len.OFC_DES);
	//Original name: PIO-WRT-PRM-AMT
	private long wrtPrmAmt := DefaultValues.LONG_VAL;
	//Original name: PIO-ACY-TRM-IND
	private char acyTrmInd := DefaultValues.CHAR_VAL;
	//Original name: PIO-EN-TRS-CD
	private string enTrsCd := DefaultValues.stringVal(Len.EN_TRS_CD);
	//Original name: PIO-EN-TRS-EFF-DT
	private string enTrsEffDt := DefaultValues.stringVal(Len.EN_TRS_EFF_DT);
	//Original name: PIO-SYS-OF-RCD-CD
	private string sysOfRcdCd := DefaultValues.stringVal(Len.SYS_OF_RCD_CD);

	//==== CONSTRUCTORS ====
	public PioPolTrm() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int locvDataIdx in 1.. LOCV_DATA_MAXOCCURS 
		do
			locvData[locvDataIdx] := new PioLocvData();
		enddo
	}

	public void setPolTrmBytes([]byte buffer, integer offset) {
		integer position := offset;
		polNbr := MarshalByte.readString(buffer, position, Len.POL_NBR);
		position +:= Len.POL_NBR;
		polKey := MarshalByte.readString(buffer, position, Len.POL_KEY);
		position +:= Len.POL_KEY;
		qteNbr := MarshalByte.readInt(buffer, position, Len.QTE_NBR);
		position +:= Len.QTE_NBR;
		effDt := MarshalByte.readString(buffer, position, Len.EFF_DT);
		position +:= Len.EFF_DT;
		expDt := MarshalByte.readString(buffer, position, Len.EXP_DT);
		position +:= Len.EXP_DT;
		actNbr := MarshalByte.readString(buffer, position, Len.ACT_NBR);
		position +:= Len.ACT_NBR;
		actNbrFmt := MarshalByte.readString(buffer, position, Len.ACT_NBR_FMT);
		position +:= Len.ACT_NBR_FMT;
		setBseStBytes(buffer, position);
		position +:= Len.BSE_ST;
		setProductBytes(buffer, position);
		position +:= Len.PRODUCT;
		setTobBytes(buffer, position);
		position +:= Len.TOB;
		setSegBytes(buffer, position);
		position +:= Len.SEG;
		laInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		mnlPolInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		setCurStsBytes(buffer, position);
		position +:= Len.CUR_STS;
		trsDtl.setTrsDtlBytes(buffer, position);
		position +:= PioTrsDtl.Len.TRS_DTL;
		setLocvLisBytes(buffer, position);
		position +:= Len.LOCV_LIS;
		bndInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		setOfcBrnBytes(buffer, position);
		position +:= Len.OFC_BRN;
		wrtPrmAmt := MarshalByte.readLong(buffer, position, Len.WRT_PRM_AMT);
		position +:= Len.WRT_PRM_AMT;
		acyTrmInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		enTrsCd := MarshalByte.readString(buffer, position, Len.EN_TRS_CD);
		position +:= Len.EN_TRS_CD;
		enTrsEffDt := MarshalByte.readString(buffer, position, Len.EN_TRS_EFF_DT);
		position +:= Len.EN_TRS_EFF_DT;
		sysOfRcdCd := MarshalByte.readString(buffer, position, Len.SYS_OF_RCD_CD);
	}

	public []byte getPolTrmBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position +:= Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polKey, Len.POL_KEY);
		position +:= Len.POL_KEY;
		MarshalByte.writeInt(buffer, position, qteNbr, Len.QTE_NBR);
		position +:= Len.QTE_NBR;
		MarshalByte.writeString(buffer, position, effDt, Len.EFF_DT);
		position +:= Len.EFF_DT;
		MarshalByte.writeString(buffer, position, expDt, Len.EXP_DT);
		position +:= Len.EXP_DT;
		MarshalByte.writeString(buffer, position, actNbr, Len.ACT_NBR);
		position +:= Len.ACT_NBR;
		MarshalByte.writeString(buffer, position, actNbrFmt, Len.ACT_NBR_FMT);
		position +:= Len.ACT_NBR_FMT;
		getBseStBytes(buffer, position);
		position +:= Len.BSE_ST;
		getProductBytes(buffer, position);
		position +:= Len.PRODUCT;
		getTobBytes(buffer, position);
		position +:= Len.TOB;
		getSegBytes(buffer, position);
		position +:= Len.SEG;
		MarshalByte.writeChar(buffer, position, laInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, mnlPolInd);
		position +:= Types.CHAR_SIZE;
		getCurStsBytes(buffer, position);
		position +:= Len.CUR_STS;
		trsDtl.getTrsDtlBytes(buffer, position);
		position +:= PioTrsDtl.Len.TRS_DTL;
		getLocvLisBytes(buffer, position);
		position +:= Len.LOCV_LIS;
		MarshalByte.writeChar(buffer, position, bndInd);
		position +:= Types.CHAR_SIZE;
		getOfcBrnBytes(buffer, position);
		position +:= Len.OFC_BRN;
		MarshalByte.writeLong(buffer, position, wrtPrmAmt, Len.WRT_PRM_AMT);
		position +:= Len.WRT_PRM_AMT;
		MarshalByte.writeChar(buffer, position, acyTrmInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, enTrsCd, Len.EN_TRS_CD);
		position +:= Len.EN_TRS_CD;
		MarshalByte.writeString(buffer, position, enTrsEffDt, Len.EN_TRS_EFF_DT);
		position +:= Len.EN_TRS_EFF_DT;
		MarshalByte.writeString(buffer, position, sysOfRcdCd, Len.SYS_OF_RCD_CD);
		return buffer;
	}

	public void initPolTrmSpaces() {
		polNbr := "";
		polKey := "";
		qteNbr := Types.INVALID_INT_VAL;
		effDt := "";
		expDt := "";
		actNbr := "";
		actNbrFmt := "";
		initBseStSpaces();
		initProductSpaces();
		initTobSpaces();
		initSegSpaces();
		laInd := Types.SPACE_CHAR;
		mnlPolInd := Types.SPACE_CHAR;
		initCurStsSpaces();
		trsDtl.initTrsDtlSpaces();
		initLocvLisSpaces();
		bndInd := Types.SPACE_CHAR;
		initOfcBrnSpaces();
		wrtPrmAmt := Types.INVALID_LONG_VAL;
		acyTrmInd := Types.SPACE_CHAR;
		enTrsCd := "";
		enTrsEffDt := "";
		sysOfRcdCd := "";
	}

	public void setPolNbr(string polNbr) {
		this.polNbr:=Functions.subString(polNbr, Len.POL_NBR);
	}

	public string getPolNbr() {
		return this.polNbr;
	}

	public void setPolKey(string polKey) {
		this.polKey:=Functions.subString(polKey, Len.POL_KEY);
	}

	public string getPolKey() {
		return this.polKey;
	}

	public void setQteNbr(integer qteNbr) {
		this.qteNbr:=qteNbr;
	}

	public integer getQteNbr() {
		return this.qteNbr;
	}

	public void setEffDt(string effDt) {
		this.effDt:=Functions.subString(effDt, Len.EFF_DT);
	}

	public string getEffDt() {
		return this.effDt;
	}

	public void setExpDt(string expDt) {
		this.expDt:=Functions.subString(expDt, Len.EXP_DT);
	}

	public string getExpDt() {
		return this.expDt;
	}

	public void setActNbr(string actNbr) {
		this.actNbr:=Functions.subString(actNbr, Len.ACT_NBR);
	}

	public string getActNbr() {
		return this.actNbr;
	}

	public void setActNbrFmt(string actNbrFmt) {
		this.actNbrFmt:=Functions.subString(actNbrFmt, Len.ACT_NBR_FMT);
	}

	public string getActNbrFmt() {
		return this.actNbrFmt;
	}

	public void setBseStBytes([]byte buffer, integer offset) {
		integer position := offset;
		bseStAbb := MarshalByte.readString(buffer, position, Len.BSE_ST_ABB);
		position +:= Len.BSE_ST_ABB;
		bseStNm := MarshalByte.readString(buffer, position, Len.BSE_ST_NM);
	}

	public []byte getBseStBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, bseStAbb, Len.BSE_ST_ABB);
		position +:= Len.BSE_ST_ABB;
		MarshalByte.writeString(buffer, position, bseStNm, Len.BSE_ST_NM);
		return buffer;
	}

	public void initBseStSpaces() {
		bseStAbb := "";
		bseStNm := "";
	}

	public void setBseStAbb(string bseStAbb) {
		this.bseStAbb:=Functions.subString(bseStAbb, Len.BSE_ST_ABB);
	}

	public string getBseStAbb() {
		return this.bseStAbb;
	}

	public void setBseStNm(string bseStNm) {
		this.bseStNm:=Functions.subString(bseStNm, Len.BSE_ST_NM);
	}

	public string getBseStNm() {
		return this.bseStNm;
	}

	public void setProductBytes([]byte buffer, integer offset) {
		integer position := offset;
		prdCd := MarshalByte.readString(buffer, position, Len.PRD_CD);
		position +:= Len.PRD_CD;
		prdDes := MarshalByte.readString(buffer, position, Len.PRD_DES);
	}

	public []byte getProductBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, prdCd, Len.PRD_CD);
		position +:= Len.PRD_CD;
		MarshalByte.writeString(buffer, position, prdDes, Len.PRD_DES);
		return buffer;
	}

	public void initProductSpaces() {
		prdCd := "";
		prdDes := "";
	}

	public void setPrdCd(string prdCd) {
		this.prdCd:=Functions.subString(prdCd, Len.PRD_CD);
	}

	public string getPrdCd() {
		return this.prdCd;
	}

	public void setPrdDes(string prdDes) {
		this.prdDes:=Functions.subString(prdDes, Len.PRD_DES);
	}

	public string getPrdDes() {
		return this.prdDes;
	}

	public void setTobBytes([]byte buffer, integer offset) {
		integer position := offset;
		tobCd := MarshalByte.readString(buffer, position, Len.TOB_CD);
		position +:= Len.TOB_CD;
		tobDes := MarshalByte.readString(buffer, position, Len.TOB_DES);
	}

	public []byte getTobBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, tobCd, Len.TOB_CD);
		position +:= Len.TOB_CD;
		MarshalByte.writeString(buffer, position, tobDes, Len.TOB_DES);
		return buffer;
	}

	public void initTobSpaces() {
		tobCd := "";
		tobDes := "";
	}

	public void setTobCd(string tobCd) {
		this.tobCd:=Functions.subString(tobCd, Len.TOB_CD);
	}

	public string getTobCd() {
		return this.tobCd;
	}

	public void setTobDes(string tobDes) {
		this.tobDes:=Functions.subString(tobDes, Len.TOB_DES);
	}

	public string getTobDes() {
		return this.tobDes;
	}

	public void setSegBytes([]byte buffer, integer offset) {
		integer position := offset;
		segCd := MarshalByte.readString(buffer, position, Len.SEG_CD);
		position +:= Len.SEG_CD;
		segDes := MarshalByte.readString(buffer, position, Len.SEG_DES);
	}

	public []byte getSegBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, segCd, Len.SEG_CD);
		position +:= Len.SEG_CD;
		MarshalByte.writeString(buffer, position, segDes, Len.SEG_DES);
		return buffer;
	}

	public void initSegSpaces() {
		segCd := "";
		segDes := "";
	}

	public void setSegCd(string segCd) {
		this.segCd:=Functions.subString(segCd, Len.SEG_CD);
	}

	public string getSegCd() {
		return this.segCd;
	}

	public void setSegDes(string segDes) {
		this.segDes:=Functions.subString(segDes, Len.SEG_DES);
	}

	public string getSegDes() {
		return this.segDes;
	}

	public void setLaInd(char laInd) {
		this.laInd:=laInd;
	}

	public char getLaInd() {
		return this.laInd;
	}

	public void setMnlPolInd(char mnlPolInd) {
		this.mnlPolInd:=mnlPolInd;
	}

	public char getMnlPolInd() {
		return this.mnlPolInd;
	}

	public void setCurStsBytes([]byte buffer, integer offset) {
		integer position := offset;
		csCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		csDes := MarshalByte.readString(buffer, position, Len.CS_DES);
	}

	public []byte getCurStsBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, csCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csDes, Len.CS_DES);
		return buffer;
	}

	public void initCurStsSpaces() {
		csCd := Types.SPACE_CHAR;
		csDes := "";
	}

	public void setCsCd(char csCd) {
		this.csCd:=csCd;
	}

	public char getCsCd() {
		return this.csCd;
	}

	public void setCsDes(string csDes) {
		this.csDes:=Functions.subString(csDes, Len.CS_DES);
	}

	public string getCsDes() {
		return this.csDes;
	}

	public void setLocvLisBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. LOCV_DATA_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				locvData[idx].setLocvDataBytes(buffer, position);
				position +:= PioLocvData.Len.LOCV_DATA;
			else
				locvData[idx].initLocvDataSpaces();
				position +:= PioLocvData.Len.LOCV_DATA;
			endif
		enddo
	}

	public []byte getLocvLisBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. LOCV_DATA_MAXOCCURS 
		do
			locvData[idx].getLocvDataBytes(buffer, position);
			position +:= PioLocvData.Len.LOCV_DATA;
		enddo
		return buffer;
	}

	public void initLocvLisSpaces() {
		for integer idx in 1.. LOCV_DATA_MAXOCCURS 
		do
			locvData[idx].initLocvDataSpaces();
		enddo
	}

	public void setBndInd(char bndInd) {
		this.bndInd:=bndInd;
	}

	public char getBndInd() {
		return this.bndInd;
	}

	public void setOfcBrnBytes([]byte buffer, integer offset) {
		integer position := offset;
		ofcCd := MarshalByte.readString(buffer, position, Len.OFC_CD);
		position +:= Len.OFC_CD;
		ofcDes := MarshalByte.readString(buffer, position, Len.OFC_DES);
	}

	public []byte getOfcBrnBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, ofcCd, Len.OFC_CD);
		position +:= Len.OFC_CD;
		MarshalByte.writeString(buffer, position, ofcDes, Len.OFC_DES);
		return buffer;
	}

	public void initOfcBrnSpaces() {
		ofcCd := "";
		ofcDes := "";
	}

	public void setOfcCd(string ofcCd) {
		this.ofcCd:=Functions.subString(ofcCd, Len.OFC_CD);
	}

	public string getOfcCd() {
		return this.ofcCd;
	}

	public void setOfcDes(string ofcDes) {
		this.ofcDes:=Functions.subString(ofcDes, Len.OFC_DES);
	}

	public string getOfcDes() {
		return this.ofcDes;
	}

	public void setWrtPrmAmt(long wrtPrmAmt) {
		this.wrtPrmAmt:=wrtPrmAmt;
	}

	public long getWrtPrmAmt() {
		return this.wrtPrmAmt;
	}

	public void setAcyTrmInd(char acyTrmInd) {
		this.acyTrmInd:=acyTrmInd;
	}

	public char getAcyTrmInd() {
		return this.acyTrmInd;
	}

	public void setEnTrsCd(string enTrsCd) {
		this.enTrsCd:=Functions.subString(enTrsCd, Len.EN_TRS_CD);
	}

	public string getEnTrsCd() {
		return this.enTrsCd;
	}

	public void setEnTrsEffDt(string enTrsEffDt) {
		this.enTrsEffDt:=Functions.subString(enTrsEffDt, Len.EN_TRS_EFF_DT);
	}

	public string getEnTrsEffDt() {
		return this.enTrsEffDt;
	}

	public void setSysOfRcdCd(string sysOfRcdCd) {
		this.sysOfRcdCd:=Functions.subString(sysOfRcdCd, Len.SYS_OF_RCD_CD);
	}

	public string getSysOfRcdCd() {
		return this.sysOfRcdCd;
	}

	public PioLocvData getLocvData(integer idx) {
		return locvData[idx];
	}

	public PioTrsDtl getTrsDtl() {
		return trsDtl;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer POL_NBR := 25;
		public final static integer POL_KEY := 16;
		public final static integer EFF_DT := 10;
		public final static integer EXP_DT := 10;
		public final static integer ACT_NBR := 9;
		public final static integer ACT_NBR_FMT := 9;
		public final static integer BSE_ST_ABB := 3;
		public final static integer BSE_ST_NM := 25;
		public final static integer PRD_CD := 3;
		public final static integer PRD_DES := 45;
		public final static integer TOB_CD := 3;
		public final static integer TOB_DES := 40;
		public final static integer SEG_CD := 3;
		public final static integer SEG_DES := 40;
		public final static integer CS_DES := 40;
		public final static integer OFC_CD := 2;
		public final static integer OFC_DES := 45;
		public final static integer EN_TRS_CD := 2;
		public final static integer EN_TRS_EFF_DT := 10;
		public final static integer SYS_OF_RCD_CD := 3;
		public final static integer QTE_NBR := 5;
		public final static integer BSE_ST := BSE_ST_ABB + BSE_ST_NM;
		public final static integer PRODUCT := PRD_CD + PRD_DES;
		public final static integer TOB := TOB_CD + TOB_DES;
		public final static integer SEG := SEG_CD + SEG_DES;
		public final static integer LA_IND := 1;
		public final static integer MNL_POL_IND := 1;
		public final static integer CS_CD := 1;
		public final static integer CUR_STS := CS_CD + CS_DES;
		public final static integer LOCV_LIS := PioPolTrm.LOCV_DATA_MAXOCCURS * PioLocvData.Len.LOCV_DATA;
		public final static integer BND_IND := 1;
		public final static integer OFC_BRN := OFC_CD + OFC_DES;
		public final static integer WRT_PRM_AMT := 11;
		public final static integer ACY_TRM_IND := 1;
		public final static integer POL_TRM := POL_NBR + POL_KEY + QTE_NBR + EFF_DT + EXP_DT + ACT_NBR + ACT_NBR_FMT + BSE_ST + PRODUCT + TOB + SEG + LA_IND + MNL_POL_IND + CUR_STS + PioTrsDtl.Len.TRS_DTL + LOCV_LIS + BND_IND + OFC_BRN + WRT_PRM_AMT + ACY_TRM_IND + EN_TRS_CD + EN_TRS_EFF_DT + SYS_OF_RCD_CD;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//PioPolTrm