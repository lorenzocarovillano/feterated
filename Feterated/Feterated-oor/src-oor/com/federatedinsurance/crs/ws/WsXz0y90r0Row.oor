package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.copy.Xzt9roCncData;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0Y90R0-ROW<br>
 * Variable: WS-XZ0Y90R0-ROW from program XZ0P90R0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0y90r0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZY9R0-POL-NBR
	private string polNbr := DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZY9R0-POL-EFF-DT
	private string polEffDt := DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZY9R0-POL-EXP-DT
	private string polExpDt := DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZY9R0-USERID
	private string userid := DefaultValues.stringVal(Len.USERID);
	//Original name: FILLER-XZY9R0-GET-INFO-INPUT-ROW
	private string flr1 := DefaultValues.stringVal(Len.FLR1);
	//Original name: XZY9R0-CNC-DATA
	private Xzt9roCncData cncData := new Xzt9roCncData();
	//Original name: XZY9R0-TD-UW-TMN-FLG
	private char tdUwTmnFlg := DefaultValues.CHAR_VAL;
	//Original name: XZY9R0-TD-TMN-EMP-ID
	private string tdTmnEmpId := DefaultValues.stringVal(Len.TD_TMN_EMP_ID);
	//Original name: XZY9R0-TD-TMN-EMP-NM
	private string tdTmnEmpNm := DefaultValues.stringVal(Len.TD_TMN_EMP_NM);
	//Original name: XZY9R0-TD-TMN-PRC-DT
	private string tdTmnPrcDt := DefaultValues.stringVal(Len.TD_TMN_PRC_DT);
	//Original name: FILLER-XZY9R0-GET-INFO-OUTPUT-ROW
	private string flr2 := DefaultValues.stringVal(Len.FLR2);


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WS_XZ0Y90R0_ROW;
	}

	@Override
	public void deserialize([]byte buf) {
		setWsXz0y90r0RowBytes(buf);
	}

	public string getWsXz0y90r0RowFormatted() {
		return getGetAddPolInfoRowFormatted();
	}

	public void setWsXz0y90r0RowBytes([]byte buffer) {
		setWsXz0y90r0RowBytes(buffer, 1);
	}

	public []byte getWsXz0y90r0RowBytes() {
		[]byte buffer := new [Len.WS_XZ0Y90R0_ROW]byte;
		return getWsXz0y90r0RowBytes(buffer, 1);
	}

	public void setWsXz0y90r0RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setGetAddPolInfoRowBytes(buffer, position);
	}

	public []byte getWsXz0y90r0RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getGetAddPolInfoRowBytes(buffer, position);
		return buffer;
	}

	public string getGetAddPolInfoRowFormatted() {
		return MarshalByteExt.bufferToStr(getGetAddPolInfoRowBytes());
	}

	/**Original name: XZY9R0-GET-ADD-POL-INFO-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0Y90R0 - SERVICE CONTRACT COPYBOOK FOR                   *
	 *             UOW : XZ_GET_ADDITIONAL_POLICY_INFO             *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 * TO0760222 17MAR2010 E404KXS    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public []byte getGetAddPolInfoRowBytes() {
		[]byte buffer := new [Len.GET_ADD_POL_INFO_ROW]byte;
		return getGetAddPolInfoRowBytes(buffer, 1);
	}

	public void setGetAddPolInfoRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setGetInfoInputRowBytes(buffer, position);
		position +:= Len.GET_INFO_INPUT_ROW;
		setGetInfoOutputRowBytes(buffer, position);
	}

	public []byte getGetAddPolInfoRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getGetInfoInputRowBytes(buffer, position);
		position +:= Len.GET_INFO_INPUT_ROW;
		getGetInfoOutputRowBytes(buffer, position);
		return buffer;
	}

	public void setGetInfoInputRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		polNbr := MarshalByte.readString(buffer, position, Len.POL_NBR);
		position +:= Len.POL_NBR;
		polEffDt := MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position +:= Len.POL_EFF_DT;
		polExpDt := MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
		position +:= Len.POL_EXP_DT;
		userid := MarshalByte.readString(buffer, position, Len.USERID);
		position +:= Len.USERID;
		flr1 := MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public []byte getGetInfoInputRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position +:= Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position +:= Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position +:= Len.POL_EXP_DT;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position +:= Len.USERID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setPolNbr(string polNbr) {
		this.polNbr:=Functions.subString(polNbr, Len.POL_NBR);
	}

	public string getPolNbr() {
		return this.polNbr;
	}

	public string getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	public void setPolEffDt(string polEffDt) {
		this.polEffDt:=Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public string getPolEffDt() {
		return this.polEffDt;
	}

	public string getPolEffDtFormatted() {
		return Functions.padBlanks(getPolEffDt(), Len.POL_EFF_DT);
	}

	public void setPolExpDt(string polExpDt) {
		this.polExpDt:=Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public string getPolExpDt() {
		return this.polExpDt;
	}

	public string getPolExpDtFormatted() {
		return Functions.padBlanks(getPolExpDt(), Len.POL_EXP_DT);
	}

	public void setUserid(string userid) {
		this.userid:=Functions.subString(userid, Len.USERID);
	}

	public string getUserid() {
		return this.userid;
	}

	public void setFlr1(string flr1) {
		this.flr1:=Functions.subString(flr1, Len.FLR1);
	}

	public string getFlr1() {
		return this.flr1;
	}

	public void setGetInfoOutputRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		cncData.setCncDataBytes(buffer, position);
		position +:= Xzt9roCncData.Len.CNC_DATA;
		setTmnDataBytes(buffer, position);
		position +:= Len.TMN_DATA;
		flr2 := MarshalByte.readString(buffer, position, Len.FLR2);
	}

	public []byte getGetInfoOutputRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		cncData.getCncDataBytes(buffer, position);
		position +:= Xzt9roCncData.Len.CNC_DATA;
		getTmnDataBytes(buffer, position);
		position +:= Len.TMN_DATA;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void setTmnDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		tdUwTmnFlg := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		tdTmnEmpId := MarshalByte.readString(buffer, position, Len.TD_TMN_EMP_ID);
		position +:= Len.TD_TMN_EMP_ID;
		tdTmnEmpNm := MarshalByte.readString(buffer, position, Len.TD_TMN_EMP_NM);
		position +:= Len.TD_TMN_EMP_NM;
		tdTmnPrcDt := MarshalByte.readString(buffer, position, Len.TD_TMN_PRC_DT);
	}

	public []byte getTmnDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, tdUwTmnFlg);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tdTmnEmpId, Len.TD_TMN_EMP_ID);
		position +:= Len.TD_TMN_EMP_ID;
		MarshalByte.writeString(buffer, position, tdTmnEmpNm, Len.TD_TMN_EMP_NM);
		position +:= Len.TD_TMN_EMP_NM;
		MarshalByte.writeString(buffer, position, tdTmnPrcDt, Len.TD_TMN_PRC_DT);
		return buffer;
	}

	public void setTdUwTmnFlg(char tdUwTmnFlg) {
		this.tdUwTmnFlg:=tdUwTmnFlg;
	}

	public char getTdUwTmnFlg() {
		return this.tdUwTmnFlg;
	}

	public void setTdTmnEmpId(string tdTmnEmpId) {
		this.tdTmnEmpId:=Functions.subString(tdTmnEmpId, Len.TD_TMN_EMP_ID);
	}

	public string getTdTmnEmpId() {
		return this.tdTmnEmpId;
	}

	public void setTdTmnEmpNm(string tdTmnEmpNm) {
		this.tdTmnEmpNm:=Functions.subString(tdTmnEmpNm, Len.TD_TMN_EMP_NM);
	}

	public string getTdTmnEmpNm() {
		return this.tdTmnEmpNm;
	}

	public void setTdTmnPrcDt(string tdTmnPrcDt) {
		this.tdTmnPrcDt:=Functions.subString(tdTmnPrcDt, Len.TD_TMN_PRC_DT);
	}

	public string getTdTmnPrcDt() {
		return this.tdTmnPrcDt;
	}

	public void setFlr2(string flr2) {
		this.flr2:=Functions.subString(flr2, Len.FLR2);
	}

	public string getFlr2() {
		return this.flr2;
	}

	public Xzt9roCncData getCncData() {
		return cncData;
	}

	@Override
	public []byte serialize() {
		return getWsXz0y90r0RowBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer POL_NBR := 9;
		public final static integer POL_EFF_DT := 10;
		public final static integer POL_EXP_DT := 10;
		public final static integer USERID := 8;
		public final static integer FLR1 := 100;
		public final static integer GET_INFO_INPUT_ROW := POL_NBR + POL_EFF_DT + POL_EXP_DT + USERID + FLR1;
		public final static integer TD_UW_TMN_FLG := 1;
		public final static integer TD_TMN_EMP_ID := 6;
		public final static integer TD_TMN_EMP_NM := 67;
		public final static integer TD_TMN_PRC_DT := 10;
		public final static integer TMN_DATA := TD_UW_TMN_FLG + TD_TMN_EMP_ID + TD_TMN_EMP_NM + TD_TMN_PRC_DT;
		public final static integer FLR2 := 800;
		public final static integer GET_INFO_OUTPUT_ROW := Xzt9roCncData.Len.CNC_DATA + TMN_DATA + FLR2;
		public final static integer GET_ADD_POL_INFO_ROW := GET_INFO_INPUT_ROW + GET_INFO_OUTPUT_ROW;
		public final static integer WS_XZ0Y90R0_ROW := GET_ADD_POL_INFO_ROW;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsXz0y90r0Row