package com.federatedinsurance.crs.ws.redefines;

import java.lang.Override;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WS-TIME-ARRAY<br>
 * Variable: WS-TIME-ARRAY from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
class WsTimeArray extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WsTimeArray() {
		super();
	}

	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WS_TIME_ARRAY;
	}

	public void setWsTimeArrayFormatted(string data) {
		writeString(Pos.WS_TIME_ARRAY, data, Len.WS_TIME_ARRAY);
	}

	public void setWsTimeArrayBytes([]byte buffer) {
		setWsTimeArrayBytes(buffer, 1);
	}

	public void setWsTimeArrayBytes([]byte buffer, integer offset) {
		setBytes(buffer, offset, Len.WS_TIME_ARRAY, Pos.WS_TIME_ARRAY);
	}

	public void initWsTimeArrayZeroes() {
		fill(Pos.WS_TIME_ARRAY, Len.WS_TIME_ARRAY, '0');
	}

	public void setArrayHours(short arrayHours) {
		writeShort(Pos.ARRAY_HOURS, arrayHours, Len.Int.ARRAY_HOURS, SignType.NO_SIGN);
	}

	/**Original name: WS-TIME-ARRAY-HOURS<br>*/
	public short getArrayHours() {
		return readNumDispUnsignedShort(Pos.ARRAY_HOURS, Len.ARRAY_HOURS);
	}

	public string getArrayHoursFormatted() {
		return readFixedString(Pos.ARRAY_HOURS, Len.ARRAY_HOURS);
	}

	/**Original name: WS-TIME-ARRAY-HMSS<br>*/
	public char getArrayHmss() {
		return readChar(Pos.ARRAY_HMSS);
	}

	public void setArrayMinutes(short arrayMinutes) {
		writeShort(Pos.ARRAY_MINUTES, arrayMinutes, Len.Int.ARRAY_MINUTES, SignType.NO_SIGN);
	}

	/**Original name: WS-TIME-ARRAY-MINUTES<br>*/
	public short getArrayMinutes() {
		return readNumDispUnsignedShort(Pos.ARRAY_MINUTES, Len.ARRAY_MINUTES);
	}

	public string getArrayMinutesFormatted() {
		return readFixedString(Pos.ARRAY_MINUTES, Len.ARRAY_MINUTES);
	}

	/**Original name: WS-TIME-ARRAY-MSS<br>*/
	public char getArrayMss() {
		return readChar(Pos.ARRAY_MSS);
	}

	public void setArraySeconds(short arraySeconds) {
		writeShort(Pos.ARRAY_SECONDS, arraySeconds, Len.Int.ARRAY_SECONDS, SignType.NO_SIGN);
	}

	/**Original name: WS-TIME-ARRAY-SECONDS<br>*/
	public short getArraySeconds() {
		return readNumDispUnsignedShort(Pos.ARRAY_SECONDS, Len.ARRAY_SECONDS);
	}

	public string getArraySecondsFormatted() {
		return readFixedString(Pos.ARRAY_SECONDS, Len.ARRAY_SECONDS);
	}

	/**Original name: WS-TIME-ARRAY-SMS<br>*/
	public char getArraySms() {
		return readChar(Pos.ARRAY_SMS);
	}

	public string getArrayMsFormatted() {
		return readFixedString(Pos.ARRAY_MS, Len.ARRAY_MS);
	}

	public void setArrayMsAmpm(string arrayMsAmpm) {
		writeString(Pos.ARRAY_MS_AMPM, arrayMsAmpm, Len.ARRAY_MS_AMPM);
	}

	/**Original name: WS-TIME-ARRAY-MS-AMPM<br>*/
	public string getArrayMsAmpm() {
		return readString(Pos.ARRAY_MS_AMPM, Len.ARRAY_MS_AMPM);
	}


	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public final static integer WS_TIME_ARRAY := 1;
		public final static integer ARRAY_IN := WS_TIME_ARRAY;
		public final static integer WS_TIME_ARRAY_HMS := 1;
		public final static integer ARRAY_HOURS := WS_TIME_ARRAY_HMS;
		public final static integer ARRAY_HMSS := ARRAY_HOURS + Len.ARRAY_HOURS;
		public final static integer ARRAY_MINUTES := ARRAY_HMSS + Len.ARRAY_HMSS;
		public final static integer ARRAY_MSS := ARRAY_MINUTES + Len.ARRAY_MINUTES;
		public final static integer ARRAY_SECONDS := ARRAY_MSS + Len.ARRAY_MSS;
		public final static integer ARRAY_SMS := ARRAY_SECONDS + Len.ARRAY_SECONDS;
		public final static integer ARRAY_MS := ARRAY_SMS + Len.ARRAY_SMS;
		public final static integer ARRAY_MS_RED := ARRAY_MS;
		public final static integer FLR1 := ARRAY_MS_RED;
		public final static integer ARRAY_MS_AMPM := FLR1 + Len.FLR1;
		public final static integer FLR2 := ARRAY_MS_AMPM + Len.ARRAY_MS_AMPM;
		public final static integer FLR3 := ARRAY_MS + Len.ARRAY_MS;

		//==== CONSTRUCTORS ====
		private Pos() {		}

	}
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ARRAY_HOURS := 2;
		public final static integer ARRAY_HMSS := 1;
		public final static integer ARRAY_MINUTES := 2;
		public final static integer ARRAY_MSS := 1;
		public final static integer ARRAY_SECONDS := 2;
		public final static integer ARRAY_SMS := 1;
		public final static integer FLR1 := 1;
		public final static integer ARRAY_MS_AMPM := 2;
		public final static integer ARRAY_MS := 6;
		public final static integer ARRAY_IN := 20;
		public final static integer WS_TIME_ARRAY := ARRAY_IN;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer ARRAY_HOURS := 2;
			public final static integer ARRAY_MINUTES := 2;
			public final static integer ARRAY_SECONDS := 2;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//WsTimeArray