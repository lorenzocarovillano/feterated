package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: L-FA-RETURN-CODE<br>
 * Variable: L-FA-RETURN-CODE from copybook TS52901<br>
 * Generated as a class for rule 88_GROUP.<br>*/
class LFaReturnCode {

	//==== PROPERTIES ====
	public string value := DefaultValues.stringVal(Len.RETURN_CODE);
	public final static string GOOD_RUN := "00";
	public final static string INPUT_ERR := "20";
	public final static string TRUNCATION_OCCURRED := "30";
	public final static string FORMATTED_NM_TOO_LONG := "35";


	//==== METHODS ====
	public void setReturnCode(short returnCode) {
		this.value := NumericDisplay.asString(returnCode, Len.RETURN_CODE);
	}

	public void setReturnCodeFormatted(string returnCode) {
		this.value:=Trunc.toUnsignedNumeric(returnCode, Len.RETURN_CODE);
	}

	public short getReturnCode() {
		return NumericDisplay.asShort(this.value);
	}

	public string getReturnCodeFormatted() {
		return this.value;
	}

	public string getReturnCodeAsString() {
		return getReturnCodeFormatted();
	}

	public boolean isFaRcGoodRun() {
		return getReturnCodeFormatted() = GOOD_RUN;
	}

	public void setGoodRun() {
		setReturnCodeFormatted(GOOD_RUN);
	}

	public void setInputErr() {
		setReturnCodeFormatted(INPUT_ERR);
	}

	public void setTruncationOccurred() {
		setReturnCodeFormatted(TRUNCATION_OCCURRED);
	}

	public void setFormattedNmTooLong() {
		setReturnCodeFormatted(FORMATTED_NM_TOO_LONG);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer RETURN_CODE := 2;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//LFaReturnCode