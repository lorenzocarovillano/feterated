package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: W-READ-VSAM-IND<br>
 * Variable: W-READ-VSAM-IND from program HALOETRA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
class WReadVsamInd {

	//==== PROPERTIES ====
	private string value := DefaultValues.stringVal(Len.READ_VSAM_IND);
	public final static string NOT_FOUND := "0";
	public final static string FOUND := "1";


	//==== METHODS ====
	public void setReadVsamInd(short readVsamInd) {
		this.value := NumericDisplay.asString(readVsamInd, Len.READ_VSAM_IND);
	}

	public void setReadVsamIndFormatted(string readVsamInd) {
		this.value:=Trunc.toUnsignedNumeric(readVsamInd, Len.READ_VSAM_IND);
	}

	public short getReadVsamInd() {
		return NumericDisplay.asShort(this.value);
	}

	public string getReadVsamIndFormatted() {
		return this.value;
	}

	public void setNotFound() {
		setReadVsamIndFormatted(NOT_FOUND);
	}

	public boolean isFound() {
		return getReadVsamIndFormatted() = FOUND;
	}

	public void setFound() {
		setReadVsamIndFormatted(FOUND);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer READ_VSAM_IND := 1;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WReadVsamInd