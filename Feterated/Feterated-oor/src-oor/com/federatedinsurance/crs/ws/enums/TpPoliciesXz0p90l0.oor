package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: TP-POLICIES<br>
 * Variable: TP-POLICIES from program XZ0P90L0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TpPoliciesXz0p90l0 {

	//==== PROPERTIES ====
	public final static string END_OF_TABLE := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TP_POLICIES);
	//Original name: TP-POL-NBR
	private string polNbr := DefaultValues.stringVal(Len.POL_NBR);
	//Original name: TP-POL-EFF-DT
	private string polEffDt := DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: TP-POL-EXP-DT
	private string polExpDt := DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: TP-NOT-EFF-DT
	private string notEffDt := DefaultValues.stringVal(Len.NOT_EFF_DT);


	//==== METHODS ====
	public string getTpPoliciesFormatted() {
		return MarshalByteExt.bufferToStr(getTpPoliciesBytes());
	}

	public []byte getTpPoliciesBytes() {
		[]byte buffer := new [Len.TP_POLICIES]byte;
		return getTpPoliciesBytes(buffer, 1);
	}

	public []byte getTpPoliciesBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position +:= Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position +:= Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position +:= Len.POL_EXP_DT;
		MarshalByte.writeString(buffer, position, notEffDt, Len.NOT_EFF_DT);
		return buffer;
	}

	public void initTpPoliciesHighValues() {
		polNbr := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_NBR);
		polEffDt := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_EFF_DT);
		polExpDt := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_EXP_DT);
		notEffDt := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NOT_EFF_DT);
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTpPoliciesFormatted()) = END_OF_TABLE;
	}

	public void setPolNbr(string polNbr) {
		this.polNbr:=Functions.subString(polNbr, Len.POL_NBR);
	}

	public string getPolNbr() {
		return this.polNbr;
	}

	public void setPolEffDt(string polEffDt) {
		this.polEffDt:=Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public string getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(string polExpDt) {
		this.polExpDt:=Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public string getPolExpDt() {
		return this.polExpDt;
	}

	public void setNotEffDt(string notEffDt) {
		this.notEffDt:=Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	public string getNotEffDt() {
		return this.notEffDt;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer POL_NBR := 25;
		public final static integer POL_EFF_DT := 10;
		public final static integer POL_EXP_DT := 10;
		public final static integer NOT_EFF_DT := 10;
		public final static integer TP_POLICIES := POL_NBR + POL_EFF_DT + POL_EXP_DT + NOT_EFF_DT;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//TpPoliciesXz0p90l0