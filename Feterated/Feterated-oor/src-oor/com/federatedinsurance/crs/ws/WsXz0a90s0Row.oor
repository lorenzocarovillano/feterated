package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90S0-ROW<br>
 * Variable: WS-XZ0A90S0-ROW from program XZ0B90S0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90s0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9S0-MAX-ROWS-RETURNED
	private short maxRowsReturned := DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA9S0-PRIMARY-FUNCTION
	private string primaryFunction := DefaultValues.stringVal(Len.PRIMARY_FUNCTION);
	public final static string PF_BY_ACT := "GetAcyPndCncTmnPolListByAct";
	//Original name: XZA9S0-ACT-NBR
	private string actNbr := DefaultValues.stringVal(Len.ACT_NBR);
	//Original name: XZA9S0-USERID
	private string userid := DefaultValues.stringVal(Len.USERID);


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WS_XZ0A90S0_ROW;
	}

	@Override
	public void deserialize([]byte buf) {
		setWsXz0a90s0RowBytes(buf);
	}

	public string getWsXz0a90s0RowFormatted() {
		return getPolicyLocateRowFormatted();
	}

	public void setWsXz0a90s0RowBytes([]byte buffer) {
		setWsXz0a90s0RowBytes(buffer, 1);
	}

	public []byte getWsXz0a90s0RowBytes() {
		[]byte buffer := new [Len.WS_XZ0A90S0_ROW]byte;
		return getWsXz0a90s0RowBytes(buffer, 1);
	}

	public void setWsXz0a90s0RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setPolicyLocateRowBytes(buffer, position);
	}

	public []byte getWsXz0a90s0RowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getPolicyLocateRowBytes(buffer, position);
		return buffer;
	}

	public string getPolicyLocateRowFormatted() {
		return getPolicyInputDataFormatted();
	}

	public void setPolicyLocateRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setPolicyInputDataBytes(buffer, position);
	}

	public []byte getPolicyLocateRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getPolicyInputDataBytes(buffer, position);
		return buffer;
	}

	public string getPolicyInputDataFormatted() {
		return MarshalByteExt.bufferToStr(getPolicyInputDataBytes());
	}

	/**Original name: XZA9S0-POLICY-INPUT-DATA<br>*/
	public []byte getPolicyInputDataBytes() {
		[]byte buffer := new [Len.POLICY_INPUT_DATA]byte;
		return getPolicyInputDataBytes(buffer, 1);
	}

	public void setPolicyInputDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		maxRowsReturned := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		primaryFunction := MarshalByte.readString(buffer, position, Len.PRIMARY_FUNCTION);
		position +:= Len.PRIMARY_FUNCTION;
		actNbr := MarshalByte.readString(buffer, position, Len.ACT_NBR);
		position +:= Len.ACT_NBR;
		userid := MarshalByte.readString(buffer, position, Len.USERID);
	}

	public []byte getPolicyInputDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryShort(buffer, position, maxRowsReturned);
		position +:= Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, primaryFunction, Len.PRIMARY_FUNCTION);
		position +:= Len.PRIMARY_FUNCTION;
		MarshalByte.writeString(buffer, position, actNbr, Len.ACT_NBR);
		position +:= Len.ACT_NBR;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setMaxRowsReturned(short maxRowsReturned) {
		this.maxRowsReturned:=maxRowsReturned;
	}

	public short getMaxRowsReturned() {
		return this.maxRowsReturned;
	}

	public void setPrimaryFunction(string primaryFunction) {
		this.primaryFunction:=Functions.subString(primaryFunction, Len.PRIMARY_FUNCTION);
	}

	public string getPrimaryFunction() {
		return this.primaryFunction;
	}

	public void setActNbr(string actNbr) {
		this.actNbr:=Functions.subString(actNbr, Len.ACT_NBR);
	}

	public string getActNbr() {
		return this.actNbr;
	}

	public void setUserid(string userid) {
		this.userid:=Functions.subString(userid, Len.USERID);
	}

	public string getUserid() {
		return this.userid;
	}

	@Override
	public []byte serialize() {
		return getWsXz0a90s0RowBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer MAX_ROWS_RETURNED := 2;
		public final static integer PRIMARY_FUNCTION := 32;
		public final static integer ACT_NBR := 9;
		public final static integer USERID := 9;
		public final static integer POLICY_INPUT_DATA := MAX_ROWS_RETURNED + PRIMARY_FUNCTION + ACT_NBR + USERID;
		public final static integer POLICY_LOCATE_ROW := POLICY_INPUT_DATA;
		public final static integer WS_XZ0A90S0_ROW := POLICY_LOCATE_ROW;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsXz0a90s0Row