package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-RUN-DT<br>
 * Variable: WS-RUN-DT from program XZ0P0021<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsRunDt {

	//==== PROPERTIES ====
	//Original name: WS-RD-YYYY
	private string yyyy := DefaultValues.stringVal(Len.YYYY);
	//Original name: WS-RD-MM
	private string mm := DefaultValues.stringVal(Len.MM);
	//Original name: WS-RD-DD
	private string dd := DefaultValues.stringVal(Len.DD);


	//==== METHODS ====
	public void setRunDtFormatted(string data) {
		[]byte buffer := new [Len.RUN_DT]byte;
		MarshalByte.writeString(buffer, 1, data, Len.RUN_DT);
		setRunDtBytes(buffer, 1);
	}

	public void setRunDtBytes([]byte buffer, integer offset) {
		integer position := offset;
		yyyy := MarshalByte.readString(buffer, position, Len.YYYY);
		position +:= Len.YYYY;
		mm := MarshalByte.readString(buffer, position, Len.MM);
		position +:= Len.MM;
		dd := MarshalByte.readString(buffer, position, Len.DD);
	}

	public void setYyyy(string yyyy) {
		this.yyyy:=Functions.subString(yyyy, Len.YYYY);
	}

	public string getYyyy() {
		return this.yyyy;
	}

	public string getYyyyFormatted() {
		return Functions.padBlanks(getYyyy(), Len.YYYY);
	}

	public void setMm(string mm) {
		this.mm:=Functions.subString(mm, Len.MM);
	}

	public string getMm() {
		return this.mm;
	}

	public string getMmFormatted() {
		return Functions.padBlanks(getMm(), Len.MM);
	}

	public void setDd(string dd) {
		this.dd:=Functions.subString(dd, Len.DD);
	}

	public string getDd() {
		return this.dd;
	}

	public string getDdFormatted() {
		return Functions.padBlanks(getDd(), Len.DD);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer YYYY := 4;
		public final static integer MM := 2;
		public final static integer DD := 2;
		public final static integer RUN_DT := YYYY + MM + DD;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsRunDt