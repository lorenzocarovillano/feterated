package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsMu0x0004 {

	//==== PROPERTIES ====
	//Original name: CF-BLANK
	private char blank := Types.SPACE_CHAR;
	//Original name: CF-POSITIVE
	private char positive := '+';
	//Original name: CF-DASH
	private char dash := '-';
	//Original name: CF-YES
	private char yes := 'Y';
	//Original name: CF-NO
	private char no := 'N';
	//Original name: CF-NAME-DELIMITER
	private string nameDelimiter := "";
	//Original name: CF-SORT-DELIMITER
	private string sortDelimiter := ",";
	//Original name: CF-DSP-DELIMITER
	private char dspDelimiter := '@';
	//Original name: CF-SPACE-DSP-DELIMITER
	private string spaceDspDelimiter := " @";
	//Original name: CF-INDIVIDUAL
	private string individual := "IN";
	//Original name: CF-PC-SIC
	private string pcSic := "SIC";
	//Original name: CF-PC-TOB
	private string pcTob := "TOB";
	//Original name: CF-PRIMARY
	private string primary := "PR";
	//Original name: CF-AC-BSM
	private string acBsm := "BSM";
	//Original name: CF-AC-BSL
	private string acBsl := "BSL";
	//Original name: CF-PHONE-NBR-CODE
	private CfPhoneNbrCode phoneNbrCode := new CfPhoneNbrCode();
	//Original name: CF-TT-FEIN
	private string ttFein := "FED";
	//Original name: CF-TT-SSN
	private string ttSsn := "SSN";
	//Original name: CF-ACT-NBR-CODE
	private CfActNbrCode actNbrCode := new CfActNbrCode();
	//Original name: CF-MARKETING-CODE
	private CfMarketingCode marketingCode := new CfMarketingCode();
	//Original name: CF-MA-AGC-CD
	private char maAgcCd := 'A';
	//Original name: CF-MA-MUT-CD
	private char maMutCd := 'M';
	//Original name: CF-MA-AGC-CD-DES
	private string maAgcCdDes := "AGENCY";
	//Original name: CF-MA-MUT-CD-DES
	private string maMutCdDes := "MUTUAL";
	//Original name: CF-COOWNER-LOOKUP
	private string coownerLookup := "04";
	//Original name: CF-COL-IS-NULL
	private char colIsNull := 'Y';
	//Original name: CF-MAIN-DRIVER
	private string mainDriver := "TS020000";
	//Original name: CF-UNIT-OF-WORK
	private string unitOfWork := "GET_CLIENT_DETAIL";
	//Original name: CF-2ND-UNIT-OF-WORK
	private string cf2ndUnitOfWork := "GET_CLIENT_DETAIL_NO_SWAP";
	//Original name: CF-PROGRAM-NAME
	private string programName := "MU0X0004";
	//Original name: CF-REQUEST-MODULE
	private string requestModule := "MU0Q0004";
	//Original name: CF-RESPONSE-MODULE
	private string responseModule := "MU0R0004";
	//Original name: CF-COPYBOOK-NAMES
	private CfCopybookNames copybookNames := new CfCopybookNames();


	//==== METHODS ====
	public char getBlank() {
		return this.blank;
	}

	public char getPositive() {
		return this.positive;
	}

	public char getDash() {
		return this.dash;
	}

	public char getYes() {
		return this.yes;
	}

	public char getNo() {
		return this.no;
	}

	public string getNameDelimiter() {
		return this.nameDelimiter;
	}

	public string getNameDelimiterFormatted() {
		return Functions.padBlanks(getNameDelimiter(), Len.NAME_DELIMITER);
	}

	public string getSortDelimiter() {
		return this.sortDelimiter;
	}

	public char getDspDelimiter() {
		return this.dspDelimiter;
	}

	public string getSpaceDspDelimiter() {
		return this.spaceDspDelimiter;
	}

	public string getIndividual() {
		return this.individual;
	}

	public string getPcSic() {
		return this.pcSic;
	}

	public string getPcTob() {
		return this.pcTob;
	}

	public string getPrimary() {
		return this.primary;
	}

	public string getAcBsm() {
		return this.acBsm;
	}

	public string getAcBsl() {
		return this.acBsl;
	}

	public string getTtFein() {
		return this.ttFein;
	}

	public string getTtSsn() {
		return this.ttSsn;
	}

	public char getMaAgcCd() {
		return this.maAgcCd;
	}

	public char getMaMutCd() {
		return this.maMutCd;
	}

	public string getMaAgcCdDes() {
		return this.maAgcCdDes;
	}

	public string getMaMutCdDes() {
		return this.maMutCdDes;
	}

	public string getCoownerLookup() {
		return this.coownerLookup;
	}

	public char getColIsNull() {
		return this.colIsNull;
	}

	public string getMainDriver() {
		return this.mainDriver;
	}

	public string getUnitOfWork() {
		return this.unitOfWork;
	}

	public string getCf2ndUnitOfWork() {
		return this.cf2ndUnitOfWork;
	}

	public string getProgramName() {
		return this.programName;
	}

	public string getRequestModule() {
		return this.requestModule;
	}

	public string getResponseModule() {
		return this.responseModule;
	}

	public CfActNbrCode getActNbrCode() {
		return actNbrCode;
	}

	public CfCopybookNames getCopybookNames() {
		return copybookNames;
	}

	public CfMarketingCode getMarketingCode() {
		return marketingCode;
	}

	public CfPhoneNbrCode getPhoneNbrCode() {
		return phoneNbrCode;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer NAME_DELIMITER := 2;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//ConstantFieldsMu0x0004