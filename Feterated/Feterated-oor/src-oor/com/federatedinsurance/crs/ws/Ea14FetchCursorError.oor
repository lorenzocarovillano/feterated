package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: EA-14-FETCH-CURSOR-ERROR<br>
 * Variable: EA-14-FETCH-CURSOR-ERROR from program XZ003000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea14FetchCursorError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-14-FETCH-CURSOR-ERROR
	private string flr1 := "XZ003000 -";
	//Original name: FILLER-EA-14-FETCH-CURSOR-ERROR-1
	private string flr2 := "DB2 FETCH";
	//Original name: FILLER-EA-14-FETCH-CURSOR-ERROR-2
	private string flr3 := "CURSOR";
	//Original name: FILLER-EA-14-FETCH-CURSOR-ERROR-3
	private string flr4 := "ERROR OCCURRED";
	//Original name: FILLER-EA-14-FETCH-CURSOR-ERROR-4
	private string flr5 := "FOR CURSOR";
	//Original name: EA-14-CSR-NAME
	private string csrName := DefaultValues.stringVal(Len.CSR_NAME);
	//Original name: FILLER-EA-14-FETCH-CURSOR-ERROR-5
	private string flr6 := " SQLCODE =";
	//Original name: EA-14-SQLCODE
	private string sqlcode := DefaultValues.stringVal(Len.SQLCODE);
	//Original name: FILLER-EA-14-FETCH-CURSOR-ERROR-6
	private string flr7 := ".  (PARAGRAPH #";
	//Original name: EA-14-PARAGRAPH-NBR
	private string paragraphNbr := DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-14-FETCH-CURSOR-ERROR-7
	private string flr8 := ").";


	//==== METHODS ====
	public string getEa14FetchCursorErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa14FetchCursorErrorBytes());
	}

	public []byte getEa14FetchCursorErrorBytes() {
		[]byte buffer := new [Len.EA14_FETCH_CURSOR_ERROR]byte;
		return getEa14FetchCursorErrorBytes(buffer, 1);
	}

	public []byte getEa14FetchCursorErrorBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position +:= Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position +:= Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, csrName, Len.CSR_NAME);
		position +:= Len.CSR_NAME;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
		position +:= Len.SQLCODE;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR4);
		position +:= Len.FLR4;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position +:= Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		return buffer;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public string getFlr3() {
		return this.flr3;
	}

	public string getFlr4() {
		return this.flr4;
	}

	public string getFlr5() {
		return this.flr5;
	}

	public void setCsrName(string csrName) {
		this.csrName:=Functions.subString(csrName, Len.CSR_NAME);
	}

	public string getCsrName() {
		return this.csrName;
	}

	public string getFlr6() {
		return this.flr6;
	}

	public void setSqlcode(long sqlcode) {
		this.sqlcode:=PicFormatter.display("++(4)9").format(sqlcode).toString();
	}

	public long getSqlcode() {
		return PicParser.display("++(4)9").parseLong(this.sqlcode);
	}

	public string getFlr7() {
		return this.flr7;
	}

	public void setParagraphNbr(string paragraphNbr) {
		this.paragraphNbr:=Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public string getParagraphNbr() {
		return this.paragraphNbr;
	}

	public string getFlr8() {
		return this.flr8;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CSR_NAME := 18;
		public final static integer SQLCODE := 6;
		public final static integer PARAGRAPH_NBR := 5;
		public final static integer FLR1 := 11;
		public final static integer FLR2 := 10;
		public final static integer FLR3 := 7;
		public final static integer FLR4 := 15;
		public final static integer FLR8 := 4;
		public final static integer EA14_FETCH_CURSOR_ERROR := CSR_NAME + SQLCODE + PARAGRAPH_NBR + 3 * FLR1 + FLR2 + FLR3 + 2 * FLR4 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ea14FetchCursorError