package com.federatedinsurance.crs.ws;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-GENERIC-REC<br>
 * Variable: WS-GENERIC-REC from program HALOUIEH<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsGenericRec extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: GEUR-DUMMY-DATA-FIELD
	private string dummyDataField := DefaultValues.stringVal(Len.DUMMY_DATA_FIELD);


	//==== METHODS ====
	@Override
	public integer getLength() {
		return Len.WS_GENERIC_REC;
	}

	@Override
	public void deserialize([]byte buf) {
		setWsGenericRecBytes(buf);
	}

	public string getWsGenericRecFormatted() {
		return getGenEmptyDataRowFormatted();
	}

	public void setWsGenericRecBytes([]byte buffer) {
		setWsGenericRecBytes(buffer, 1);
	}

	public []byte getWsGenericRecBytes() {
		[]byte buffer := new [Len.WS_GENERIC_REC]byte;
		return getWsGenericRecBytes(buffer, 1);
	}

	public void setWsGenericRecBytes([]byte buffer, integer offset) {
		integer position := offset;
		setGenEmptyDataRowBytes(buffer, position);
	}

	public []byte getWsGenericRecBytes([]byte buffer, integer offset) {
		integer position := offset;
		getGenEmptyDataRowBytes(buffer, position);
		return buffer;
	}

	public string getGenEmptyDataRowFormatted() {
		return getDummyDataFieldFormatted();
	}

	public void setGenEmptyDataRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		dummyDataField := MarshalByte.readString(buffer, position, Len.DUMMY_DATA_FIELD);
	}

	public []byte getGenEmptyDataRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, dummyDataField, Len.DUMMY_DATA_FIELD);
		return buffer;
	}

	public void setDummyDataField(string dummyDataField) {
		this.dummyDataField:=Functions.subString(dummyDataField, Len.DUMMY_DATA_FIELD);
	}

	public string getDummyDataField() {
		return this.dummyDataField;
	}

	public string getDummyDataFieldFormatted() {
		return Functions.padBlanks(getDummyDataField(), Len.DUMMY_DATA_FIELD);
	}

	@Override
	public []byte serialize() {
		return getWsGenericRecBytes();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer DUMMY_DATA_FIELD := 32;
		public final static integer GEN_EMPTY_DATA_ROW := DUMMY_DATA_FIELD;
		public final static integer WS_GENERIC_REC := GEN_EMPTY_DATA_ROW;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WsGenericRec