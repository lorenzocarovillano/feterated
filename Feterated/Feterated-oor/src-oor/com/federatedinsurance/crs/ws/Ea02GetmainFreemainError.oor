package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.ws.enums.Ea02ParagraphNbr;
import com.federatedinsurance.crs.ws.enums.FillerEa02GetmainFreemainError3;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-02-GETMAIN-FREEMAIN-ERROR<br>
 * Variable: EA-02-GETMAIN-FREEMAIN-ERROR from program XZ0G0005<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02GetmainFreemainError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-GETMAIN-FREEMAIN-ERROR
	private string flr1 := "PGM XZ0G0005";
	//Original name: FILLER-EA-02-GETMAIN-FREEMAIN-ERROR-1
	private string flr2 := "PARA =";
	//Original name: EA-02-PARAGRAPH-NBR
	private Ea02ParagraphNbr paragraphNbr := new Ea02ParagraphNbr();
	//Original name: FILLER-EA-02-GETMAIN-FREEMAIN-ERROR-2
	private string flr3 := " CICS ERROR";
	//Original name: FILLER-EA-02-GETMAIN-FREEMAIN-ERROR-3
	private FillerEa02GetmainFreemainError3 flr4 := new FillerEa02GetmainFreemainError3();
	//Original name: FILLER-EA-02-GETMAIN-FREEMAIN-ERROR-4
	private string flr5 := "SYSTEM MEMORY";
	//Original name: FILLER-EA-02-GETMAIN-FREEMAIN-ERROR-5
	private string flr6 := " RESP =";
	//Original name: EA-02-RESP
	private string resp := DefaultValues.stringVal(Len.RESP);
	//Original name: FILLER-EA-02-GETMAIN-FREEMAIN-ERROR-6
	private string flr7 := " RESP2 =";
	//Original name: EA-02-RESP2
	private string resp2 := DefaultValues.stringVal(Len.RESP2);


	//==== METHODS ====
	public string getEa02GetmainFreemainErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa02GetmainFreemainErrorBytes());
	}

	public []byte getEa02GetmainFreemainErrorBytes() {
		[]byte buffer := new [Len.EA02_GETMAIN_FREEMAIN_ERROR]byte;
		return getEa02GetmainFreemainErrorBytes(buffer, 1);
	}

	public []byte getEa02GetmainFreemainErrorBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, paragraphNbr.getParagraphNbr(), Ea02ParagraphNbr.Len.PARAGRAPH_NBR);
		position +:= Ea02ParagraphNbr.Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position +:= Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4.getFlr4(), FillerEa02GetmainFreemainError3.Len.FLR4);
		position +:= FillerEa02GetmainFreemainError3.Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position +:= Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position +:= Len.FLR6;
		MarshalByte.writeString(buffer, position, resp, Len.RESP);
		position +:= Len.RESP;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position +:= Len.FLR7;
		MarshalByte.writeString(buffer, position, resp2, Len.RESP2);
		return buffer;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public string getFlr3() {
		return this.flr3;
	}

	public string getFlr5() {
		return this.flr5;
	}

	public string getFlr6() {
		return this.flr6;
	}

	public void setResp(integer resp) {
		this.resp := NumericDisplay.asString(resp, Len.RESP);
	}

	public integer getResp() {
		return NumericDisplay.asInt(this.resp);
	}

	public string getFlr7() {
		return this.flr7;
	}

	public void setResp2(string resp2) {
		this.resp2:=Functions.subString(resp2, Len.RESP2);
	}

	public string getResp2() {
		return this.resp2;
	}

	public FillerEa02GetmainFreemainError3 getFlr4() {
		return flr4;
	}

	public Ea02ParagraphNbr getParagraphNbr() {
		return paragraphNbr;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer RESP := 8;
		public final static integer RESP2 := 5;
		public final static integer FLR1 := 13;
		public final static integer FLR2 := 7;
		public final static integer FLR3 := 12;
		public final static integer FLR5 := 14;
		public final static integer FLR6 := 8;
		public final static integer FLR7 := 9;
		public final static integer EA02_GETMAIN_FREEMAIN_ERROR := Ea02ParagraphNbr.Len.PARAGRAPH_NBR + RESP + RESP2 + FLR1 + FLR2 + FLR3 + FLR5 + FLR6 + FLR7 + FillerEa02GetmainFreemainError3.Len.FLR4;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ea02GetmainFreemainError