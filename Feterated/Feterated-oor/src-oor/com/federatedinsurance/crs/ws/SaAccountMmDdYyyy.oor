package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: SA-ACCOUNT-MM-DD-YYYY<br>
 * Variable: SA-ACCOUNT-MM-DD-YYYY from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaAccountMmDdYyyy {

	//==== PROPERTIES ====
	//Original name: SA-ACCOUNT-MM
	private string mm := DefaultValues.stringVal(Len.MM);
	//Original name: FILLER-SA-ACCOUNT-MM-DD-YYYY
	private char flr1 := '/';
	//Original name: SA-ACCOUNT-DD
	private string dd := DefaultValues.stringVal(Len.DD);
	//Original name: FILLER-SA-ACCOUNT-MM-DD-YYYY-1
	private char flr2 := '/';
	//Original name: SA-ACCOUNT-YYYY
	private string yyyy := DefaultValues.stringVal(Len.YYYY);


	//==== METHODS ====
	public []byte getSaAccountMmDdYyyyBytes() {
		[]byte buffer := new [Len.SA_ACCOUNT_MM_DD_YYYY]byte;
		return getSaAccountMmDdYyyyBytes(buffer, 1);
	}

	public []byte getSaAccountMmDdYyyyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position +:= Len.MM;
		MarshalByte.writeChar(buffer, position, flr1);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		position +:= Len.DD;
		MarshalByte.writeChar(buffer, position, flr2);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, yyyy, Len.YYYY);
		return buffer;
	}

	public void setMmFormatted(string mm) {
		this.mm:=Trunc.toUnsignedNumeric(mm, Len.MM);
	}

	public short getMm() {
		return NumericDisplay.asShort(this.mm);
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setDdFormatted(string dd) {
		this.dd:=Trunc.toUnsignedNumeric(dd, Len.DD);
	}

	public short getDd() {
		return NumericDisplay.asShort(this.dd);
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setYyyyFormatted(string yyyy) {
		this.yyyy:=Trunc.toUnsignedNumeric(yyyy, Len.YYYY);
	}

	public short getYyyy() {
		return NumericDisplay.asShort(this.yyyy);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer MM := 2;
		public final static integer DD := 2;
		public final static integer YYYY := 4;
		public final static integer FLR1 := 1;
		public final static integer SA_ACCOUNT_MM_DD_YYYY := MM + DD + YYYY + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//SaAccountMmDdYyyy