package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-SYSTEM-TIME-RED<br>
 * Variable: WS-SYSTEM-TIME-RED from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsSystemTimeRed {

	//==== PROPERTIES ====
	//Original name: WS-SYSTEM-HOURS
	private string hours := DefaultValues.stringVal(Len.HOURS);
	//Original name: FILLER-WS-SYSTEM-TIME-RED
	private char flr1 := DefaultValues.CHAR_VAL;
	//Original name: WS-SYSTEM-MINUTES
	private string minutes := DefaultValues.stringVal(Len.MINUTES);
	//Original name: FILLER-WS-SYSTEM-TIME-RED-1
	private char flr2 := DefaultValues.CHAR_VAL;
	//Original name: WS-SYSTEM-SECONDS
	private string seconds := DefaultValues.stringVal(Len.SECONDS);


	//==== METHODS ====
	public void setWsSystemTime(string wsSystemTime) {
		integer position := 1;
		[]byte buffer := getWsSystemTimeRedBytes();
		MarshalByte.writeString(buffer, position, wsSystemTime, Len.WS_SYSTEM_TIME);
		setWsSystemTimeRedBytes(buffer);
	}

	/**Original name: WS-SYSTEM-TIME<br>*/
	public string getWsSystemTime() {
		integer position := 1;
		return MarshalByte.readString(getWsSystemTimeRedBytes(), position, Len.WS_SYSTEM_TIME);
	}

	public void setWsSystemTimeRedBytes([]byte buffer) {
		setWsSystemTimeRedBytes(buffer, 1);
	}

	/**Original name: WS-SYSTEM-TIME-RED<br>*/
	public []byte getWsSystemTimeRedBytes() {
		[]byte buffer := new [Len.WS_SYSTEM_TIME_RED]byte;
		return getWsSystemTimeRedBytes(buffer, 1);
	}

	public void setWsSystemTimeRedBytes([]byte buffer, integer offset) {
		integer position := offset;
		hours := MarshalByte.readFixedString(buffer, position, Len.HOURS);
		position +:= Len.HOURS;
		flr1 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		minutes := MarshalByte.readFixedString(buffer, position, Len.MINUTES);
		position +:= Len.MINUTES;
		flr2 := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		seconds := MarshalByte.readFixedString(buffer, position, Len.SECONDS);
	}

	public []byte getWsSystemTimeRedBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, hours, Len.HOURS);
		position +:= Len.HOURS;
		MarshalByte.writeChar(buffer, position, flr1);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, minutes, Len.MINUTES);
		position +:= Len.MINUTES;
		MarshalByte.writeChar(buffer, position, flr2);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, seconds, Len.SECONDS);
		return buffer;
	}

	public string getHoursFormatted() {
		return this.hours;
	}

	public void setFlr1(char flr1) {
		this.flr1:=flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public string getMinutesFormatted() {
		return this.minutes;
	}

	public void setFlr2(char flr2) {
		this.flr2:=flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public string getSecondsFormatted() {
		return this.seconds;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer HOURS := 2;
		public final static integer MINUTES := 2;
		public final static integer SECONDS := 2;
		public final static integer FLR1 := 1;
		public final static integer WS_SYSTEM_TIME_RED := HOURS + MINUTES + SECONDS + 2 * FLR1;
		public final static integer WS_SYSTEM_TIME := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer WS_SYSTEM_TIME := 8;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//WsSystemTimeRed