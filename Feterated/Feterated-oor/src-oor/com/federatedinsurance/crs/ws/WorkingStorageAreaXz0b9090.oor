package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0B9090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0b9090 {

	//==== PROPERTIES ====
	//Original name: WS-ROW-COUNT
	private short rowCount := (short)0;
	public final static short NOTHING_FOUND := (short)0;
	//Original name: WS-MAX-ROWS
	private string maxRows := DefaultValues.stringVal(Len.MAX_ROWS);
	//Original name: WS-PROGRAM-NAME
	private string programName := "XZ0B9090";
	//Original name: WS-BUS-OBJ-NM
	private string busObjNm := "XZ_GET_NOT_CO_LIST";


	//==== METHODS ====
	public void setRowCount(short rowCount) {
		this.rowCount:=rowCount;
	}

	public short getRowCount() {
		return this.rowCount;
	}

	public boolean isNothingFound() {
		return rowCount = NOTHING_FOUND;
	}

	public void setNothingFound() {
		rowCount := NOTHING_FOUND;
	}

	public void setMaxRows(long maxRows) {
		this.maxRows:=PicFormatter.display("Z(3)9").format(maxRows).toString();
	}

	public long getMaxRows() {
		return PicParser.display("Z(3)9").parseLong(this.maxRows);
	}

	public string getMaxRowsFormatted() {
		return this.maxRows;
	}

	public string getMaxRowsAsString() {
		return getMaxRowsFormatted();
	}

	public string getProgramName() {
		return this.programName;
	}

	public string getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public string getBusObjNm() {
		return this.busObjNm;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer MAX_ROWS := 4;
		public final static integer PROGRAM_NAME := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WorkingStorageAreaXz0b9090