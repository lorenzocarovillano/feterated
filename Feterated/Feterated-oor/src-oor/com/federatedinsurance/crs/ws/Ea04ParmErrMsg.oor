package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-04-PARM-ERR-MSG<br>
 * Variable: EA-04-PARM-ERR-MSG from program XZ003000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea04ParmErrMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-04-PARM-ERR-MSG
	private string flr1 := "XZ003000";
	//Original name: FILLER-EA-04-PARM-ERR-MSG-1
	private string flr2 := "CICS PARAMETER";
	//Original name: FILLER-EA-04-PARM-ERR-MSG-2
	private string flr3 := "ERROR.";
	//Original name: FILLER-EA-04-PARM-ERR-MSG-3
	private string flr4 := " THIS SHOULD BE";
	//Original name: FILLER-EA-04-PARM-ERR-MSG-4
	private string flr5 := " A VALID CICS:";
	//Original name: EA-04-PARAMETER
	private string ea04Parameter := DefaultValues.stringVal(Len.EA04_PARAMETER);


	//==== METHODS ====
	public string getEa04ParmErrMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa04ParmErrMsgBytes());
	}

	public []byte getEa04ParmErrMsgBytes() {
		[]byte buffer := new [Len.EA04_PARM_ERR_MSG]byte;
		return getEa04ParmErrMsgBytes(buffer, 1);
	}

	public []byte getEa04ParmErrMsgBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position +:= Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position +:= Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR2);
		position +:= Len.FLR2;
		MarshalByte.writeString(buffer, position, ea04Parameter, Len.EA04_PARAMETER);
		return buffer;
	}

	public string getFlr1() {
		return this.flr1;
	}

	public string getFlr2() {
		return this.flr2;
	}

	public string getFlr3() {
		return this.flr3;
	}

	public string getFlr4() {
		return this.flr4;
	}

	public string getFlr5() {
		return this.flr5;
	}

	public void setEa04Parameter(string ea04Parameter) {
		this.ea04Parameter:=Functions.subString(ea04Parameter, Len.EA04_PARAMETER);
	}

	public string getEa04Parameter() {
		return this.ea04Parameter;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer EA04_PARAMETER := 80;
		public final static integer FLR1 := 9;
		public final static integer FLR2 := 15;
		public final static integer FLR3 := 6;
		public final static integer EA04_PARM_ERR_MSG := EA04_PARAMETER + FLR1 + 3 * FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ea04ParmErrMsg