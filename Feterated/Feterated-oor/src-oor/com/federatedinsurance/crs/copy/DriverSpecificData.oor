package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.federatedinsurance.crs.ws.enums.DsdBypassSyncpointMdrvInd;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.occurs.DsdNonLoggableErrors;
import com.federatedinsurance.crs.ws.occurs.DsdWarnings;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DRIVER-SPECIFIC-DATA<br>
 * Variable: DRIVER-SPECIFIC-DATA from copybook TS020DRV<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class DriverSpecificData {

	//==== PROPERTIES ====
	public final static integer NON_LOGGABLE_ERRORS_MAXOCCURS := 10;
	public final static integer WARNINGS_MAXOCCURS := 10;
	/**Original name: DSD-BYPASS-SYNCPOINT-MDRV-IND<br>
	 * <pre>* FLAG TO INDICATE WHETHER OR NOT MAIN DRIVER DOES SYNCPOINT</pre>*/
	private DsdBypassSyncpointMdrvInd bypassSyncpointMdrvInd := new DsdBypassSyncpointMdrvInd();
	//Original name: DSD-ERROR-RETURN-CODE
	private DsdErrorReturnCode errorReturnCode := new DsdErrorReturnCode();
	//Original name: DSD-FATAL-ERROR-MESSAGE
	private string fatalErrorMessage := DefaultValues.stringVal(Len.FATAL_ERROR_MESSAGE);
	//Original name: DSD-NON-LOGGABLE-ERROR-CNT
	private string nonLoggableErrorCnt := DefaultValues.stringVal(Len.NON_LOGGABLE_ERROR_CNT);
	//Original name: DSD-NON-LOGGABLE-ERRORS
	private []DsdNonLoggableErrors nonLoggableErrors := new [NON_LOGGABLE_ERRORS_MAXOCCURS]DsdNonLoggableErrors;
	//Original name: DSD-WARNING-CNT
	private string warningCnt := DefaultValues.stringVal(Len.WARNING_CNT);
	//Original name: DSD-WARNINGS
	private []DsdWarnings warnings := new [WARNINGS_MAXOCCURS]DsdWarnings;
	//Original name: DSD-PERF-MONITORING-PARMS
	private DsdPerfMonitoringParms perfMonitoringParms := new DsdPerfMonitoringParms();

	//==== CONSTRUCTORS ====
	public DriverSpecificData() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int nonLoggableErrorsIdx in 1.. NON_LOGGABLE_ERRORS_MAXOCCURS 
		do
			nonLoggableErrors[nonLoggableErrorsIdx] := new DsdNonLoggableErrors();
		enddo
		for int warningsIdx in 1.. WARNINGS_MAXOCCURS 
		do
			warnings[warningsIdx] := new DsdWarnings();
		enddo
	}

	public void setDriverSpecificDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		bypassSyncpointMdrvInd.setBypassSyncpointMdrvInd(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		setErrorHandlingParmsBytes(buffer, position);
		position +:= Len.ERROR_HANDLING_PARMS;
		perfMonitoringParms.setPerfMonitoringParmsBytes(buffer, position);
	}

	public []byte getDriverSpecificDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, bypassSyncpointMdrvInd.getBypassSyncpointMdrvInd());
		position +:= Types.CHAR_SIZE;
		getErrorHandlingParmsBytes(buffer, position);
		position +:= Len.ERROR_HANDLING_PARMS;
		perfMonitoringParms.getPerfMonitoringParmsBytes(buffer, position);
		return buffer;
	}

	/**Original name: DSD-ERROR-HANDLING-PARMS<br>
	 * <pre>* ERROR HANDLING</pre>*/
	public []byte getDsdErrorHandlingParmsBytes() {
		[]byte buffer := new [Len.ERROR_HANDLING_PARMS]byte;
		return getErrorHandlingParmsBytes(buffer, 1);
	}

	public void setErrorHandlingParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		errorReturnCode.value := MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position +:= DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		fatalErrorMessage := MarshalByte.readString(buffer, position, Len.FATAL_ERROR_MESSAGE);
		position +:= Len.FATAL_ERROR_MESSAGE;
		nonLoggableErrorCnt := MarshalByte.readFixedString(buffer, position, Len.NON_LOGGABLE_ERROR_CNT);
		position +:= Len.NON_LOGGABLE_ERROR_CNT;
		for integer idx in 1.. NON_LOGGABLE_ERRORS_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				nonLoggableErrors[idx].setNonLoggableErrorsBytes(buffer, position);
				position +:= DsdNonLoggableErrors.Len.NON_LOGGABLE_ERRORS;
			else
				nonLoggableErrors[idx].initNonLoggableErrorsSpaces();
				position +:= DsdNonLoggableErrors.Len.NON_LOGGABLE_ERRORS;
			endif
		enddo
		warningCnt := MarshalByte.readFixedString(buffer, position, Len.WARNING_CNT);
		position +:= Len.WARNING_CNT;
		for integer idx in 1.. WARNINGS_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				warnings[idx].setWarningsBytes(buffer, position);
				position +:= DsdWarnings.Len.WARNINGS;
			else
				warnings[idx].initWarningsSpaces();
				position +:= DsdWarnings.Len.WARNINGS;
			endif
		enddo
	}

	public []byte getErrorHandlingParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, errorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position +:= DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, fatalErrorMessage, Len.FATAL_ERROR_MESSAGE);
		position +:= Len.FATAL_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, nonLoggableErrorCnt, Len.NON_LOGGABLE_ERROR_CNT);
		position +:= Len.NON_LOGGABLE_ERROR_CNT;
		for integer idx in 1.. NON_LOGGABLE_ERRORS_MAXOCCURS 
		do
			nonLoggableErrors[idx].getNonLoggableErrorsBytes(buffer, position);
			position +:= DsdNonLoggableErrors.Len.NON_LOGGABLE_ERRORS;
		enddo
		MarshalByte.writeString(buffer, position, warningCnt, Len.WARNING_CNT);
		position +:= Len.WARNING_CNT;
		for integer idx in 1.. WARNINGS_MAXOCCURS 
		do
			warnings[idx].getWarningsBytes(buffer, position);
			position +:= DsdWarnings.Len.WARNINGS;
		enddo
		return buffer;
	}

	public void setFatalErrorMessage(string fatalErrorMessage) {
		this.fatalErrorMessage:=Functions.subString(fatalErrorMessage, Len.FATAL_ERROR_MESSAGE);
	}

	public string getFatalErrorMessage() {
		return this.fatalErrorMessage;
	}

	public void setNonLoggableErrorCntFormatted(string nonLoggableErrorCnt) {
		this.nonLoggableErrorCnt:=Trunc.toUnsignedNumeric(nonLoggableErrorCnt, Len.NON_LOGGABLE_ERROR_CNT);
	}

	public short getNonLoggableErrorCnt() {
		return NumericDisplay.asShort(this.nonLoggableErrorCnt);
	}

	public void setWarningCntFormatted(string warningCnt) {
		this.warningCnt:=Trunc.toUnsignedNumeric(warningCnt, Len.WARNING_CNT);
	}

	public short getWarningCnt() {
		return NumericDisplay.asShort(this.warningCnt);
	}

	public DsdBypassSyncpointMdrvInd getBypassSyncpointMdrvInd() {
		return bypassSyncpointMdrvInd;
	}

	public DsdErrorReturnCode getErrorReturnCode() {
		return errorReturnCode;
	}

	public DsdNonLoggableErrors getNonLoggableErrors(integer idx) {
		return nonLoggableErrors[idx];
	}

	public DsdPerfMonitoringParms getPerfMonitoringParms() {
		return perfMonitoringParms;
	}

	public DsdWarnings getWarnings(integer idx) {
		return warnings[idx];
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer FATAL_ERROR_MESSAGE := 250;
		public final static integer NON_LOGGABLE_ERROR_CNT := 4;
		public final static integer WARNING_CNT := 4;
		public final static integer ERROR_HANDLING_PARMS := DsdErrorReturnCode.Len.ERROR_RETURN_CODE + FATAL_ERROR_MESSAGE + NON_LOGGABLE_ERROR_CNT + DriverSpecificData.NON_LOGGABLE_ERRORS_MAXOCCURS * DsdNonLoggableErrors.Len.NON_LOGGABLE_ERRORS + WARNING_CNT + DriverSpecificData.WARNINGS_MAXOCCURS * DsdWarnings.Len.WARNINGS;
		public final static integer DRIVER_SPECIFIC_DATA := DsdBypassSyncpointMdrvInd.Len.BYPASS_SYNCPOINT_MDRV_IND + ERROR_HANDLING_PARMS + DsdPerfMonitoringParms.Len.PERF_MONITORING_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//DriverSpecificData