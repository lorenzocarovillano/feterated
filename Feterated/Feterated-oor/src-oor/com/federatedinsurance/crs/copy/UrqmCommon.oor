package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.federatedinsurance.crs.ws.enums.UbocPassThruAction;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: URQM-COMMON<br>
 * Variable: URQM-COMMON from copybook HALLURQM<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class UrqmCommon {

	//==== PROPERTIES ====
	//Original name: URQM-ID
	private string id := DefaultValues.stringVal(Len.ID);
	//Original name: URQM-BUS-OBJ
	private string busObj := DefaultValues.stringVal(Len.BUS_OBJ);
	//Original name: URQM-REC-SEQ
	private string recSeq := DefaultValues.stringVal(Len.REC_SEQ);
	/**Original name: URQM-UOW-BUFFER-LENGTH<br>
	 * <pre>**       10  URQM-REC-SEQ                   PIC 9(03).</pre>*/
	private string uowBufferLength := DefaultValues.stringVal(Len.UOW_BUFFER_LENGTH);
	/**Original name: URQM-MSG-BUS-OBJ-NM<br>
	 * <pre>** UOW MESSAGE DATA HEADER FOR DATA ROWS IN THE INCOMING
	 * ** MESSAGE AND THE UOW MESSAGE UMT.</pre>*/
	private string msgBusObjNm := DefaultValues.stringVal(Len.MSG_BUS_OBJ_NM);
	//Original name: URQM-ACTION-CODE
	private UbocPassThruAction actionCode := new UbocPassThruAction();
	//Original name: URQM-BUS-OBJ-DATA-LENGTH
	private string busObjDataLength := DefaultValues.stringVal(Len.BUS_OBJ_DATA_LENGTH);
	//Original name: URQM-MSG-DATA
	private string msgData := DefaultValues.stringVal(Len.MSG_DATA);


	//==== METHODS ====
	public void setUrqmCommonBytes([]byte buffer) {
		setUrqmCommonBytes(buffer, 1);
	}

	public []byte getUrqmCommonBytes() {
		[]byte buffer := new [Len.URQM_COMMON]byte;
		return getUrqmCommonBytes(buffer, 1);
	}

	public void setUrqmCommonBytes([]byte buffer, integer offset) {
		integer position := offset;
		setKeyBytes(buffer, position);
		position +:= Len.KEY;
		uowBufferLength := MarshalByte.readFixedString(buffer, position, Len.UOW_BUFFER_LENGTH);
		position +:= Len.UOW_BUFFER_LENGTH;
		setUowMessageBufferBytes(buffer, position);
	}

	public []byte getUrqmCommonBytes([]byte buffer, integer offset) {
		integer position := offset;
		getKeyBytes(buffer, position);
		position +:= Len.KEY;
		MarshalByte.writeString(buffer, position, uowBufferLength, Len.UOW_BUFFER_LENGTH);
		position +:= Len.UOW_BUFFER_LENGTH;
		getUowMessageBufferBytes(buffer, position);
		return buffer;
	}

	public string getKeyFormatted() {
		return MarshalByteExt.bufferToStr(getKeyBytes());
	}

	public void setKeyBytes([]byte buffer) {
		setKeyBytes(buffer, 1);
	}

	/**Original name: URQM-KEY<br>*/
	public []byte getKeyBytes() {
		[]byte buffer := new [Len.KEY]byte;
		return getKeyBytes(buffer, 1);
	}

	public void setKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		setPartialKeyBytes(buffer, position);
		position +:= Len.PARTIAL_KEY;
		recSeq := MarshalByte.readFixedString(buffer, position, Len.REC_SEQ);
	}

	public []byte getKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		getPartialKeyBytes(buffer, position);
		position +:= Len.PARTIAL_KEY;
		MarshalByte.writeString(buffer, position, recSeq, Len.REC_SEQ);
		return buffer;
	}

	/**Original name: URQM-PARTIAL-KEY<br>*/
	public []byte getPartialKeyBytes() {
		[]byte buffer := new [Len.PARTIAL_KEY]byte;
		return getPartialKeyBytes(buffer, 1);
	}

	public void setPartialKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		id := MarshalByte.readString(buffer, position, Len.ID);
		position +:= Len.ID;
		busObj := MarshalByte.readString(buffer, position, Len.BUS_OBJ);
	}

	public []byte getPartialKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, id, Len.ID);
		position +:= Len.ID;
		MarshalByte.writeString(buffer, position, busObj, Len.BUS_OBJ);
		return buffer;
	}

	public void setId(string id) {
		this.id:=Functions.subString(id, Len.ID);
	}

	public string getId() {
		return this.id;
	}

	public string getIdFormatted() {
		return Functions.padBlanks(getId(), Len.ID);
	}

	public void setBusObj(string busObj) {
		this.busObj:=Functions.subString(busObj, Len.BUS_OBJ);
	}

	public string getBusObj() {
		return this.busObj;
	}

	public string getBusObjFormatted() {
		return Functions.padBlanks(getBusObj(), Len.BUS_OBJ);
	}

	public void setRecSeq(integer recSeq) {
		this.recSeq := NumericDisplay.asString(recSeq, Len.REC_SEQ);
	}

	public void setRecSeqFormatted(string recSeq) {
		this.recSeq:=Trunc.toUnsignedNumeric(recSeq, Len.REC_SEQ);
	}

	public integer getRecSeq() {
		return NumericDisplay.asInt(this.recSeq);
	}

	public string getRecSeqFormatted() {
		return this.recSeq;
	}

	public string getRecSeqAsString() {
		return getRecSeqFormatted();
	}

	public void setUowBufferLength(short uowBufferLength) {
		this.uowBufferLength := NumericDisplay.asString(uowBufferLength, Len.UOW_BUFFER_LENGTH);
	}

	public void setUowBufferLengthFormatted(string uowBufferLength) {
		this.uowBufferLength:=Trunc.toUnsignedNumeric(uowBufferLength, Len.UOW_BUFFER_LENGTH);
	}

	public short getUowBufferLength() {
		return NumericDisplay.asShort(this.uowBufferLength);
	}

	public void setUowMessageBufferBytes([]byte buffer, integer offset) {
		integer position := offset;
		setMsgHdrBytes(buffer, position);
		position +:= Len.MSG_HDR;
		msgData := MarshalByte.readString(buffer, position, Len.MSG_DATA);
	}

	public []byte getUowMessageBufferBytes([]byte buffer, integer offset) {
		integer position := offset;
		getMsgHdrBytes(buffer, position);
		position +:= Len.MSG_HDR;
		MarshalByte.writeString(buffer, position, msgData, Len.MSG_DATA);
		return buffer;
	}

	public void initUowMessageBufferSpaces() {
		initMsgHdrSpaces();
		msgData := "";
	}

	public void setMsgHdrBytes([]byte buffer, integer offset) {
		integer position := offset;
		msgBusObjNm := MarshalByte.readString(buffer, position, Len.MSG_BUS_OBJ_NM);
		position +:= Len.MSG_BUS_OBJ_NM;
		actionCode.setUbocPassThruAction(MarshalByte.readString(buffer, position, UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION));
		position +:= UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION;
		busObjDataLength := MarshalByte.readFixedString(buffer, position, Len.BUS_OBJ_DATA_LENGTH);
	}

	public []byte getMsgHdrBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, msgBusObjNm, Len.MSG_BUS_OBJ_NM);
		position +:= Len.MSG_BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, actionCode.getUbocPassThruAction(), UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION);
		position +:= UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION;
		MarshalByte.writeString(buffer, position, busObjDataLength, Len.BUS_OBJ_DATA_LENGTH);
		return buffer;
	}

	public void initMsgHdrSpaces() {
		msgBusObjNm := "";
		actionCode.setUbocPassThruAction("");
		busObjDataLength := "";
	}

	public void setMsgBusObjNm(string msgBusObjNm) {
		this.msgBusObjNm:=Functions.subString(msgBusObjNm, Len.MSG_BUS_OBJ_NM);
	}

	public string getMsgBusObjNm() {
		return this.msgBusObjNm;
	}

	public void setBusObjDataLength(short busObjDataLength) {
		this.busObjDataLength := NumericDisplay.asString(busObjDataLength, Len.BUS_OBJ_DATA_LENGTH);
	}

	public void setBusObjDataLengthFormatted(string busObjDataLength) {
		this.busObjDataLength:=Trunc.toUnsignedNumeric(busObjDataLength, Len.BUS_OBJ_DATA_LENGTH);
	}

	public short getBusObjDataLength() {
		return NumericDisplay.asShort(this.busObjDataLength);
	}

	public void setMsgData(string msgData) {
		this.msgData:=Functions.subString(msgData, Len.MSG_DATA);
	}

	public void setMsgDataSubstring(string replacement, integer start, integer length) {
		msgData := Functions.setSubstring(msgData, replacement, start, length);
	}

	public string getMsgData() {
		return this.msgData;
	}

	public string getMsgDataFormatted() {
		return Functions.padBlanks(getMsgData(), Len.MSG_DATA);
	}

	public UbocPassThruAction getActionCode() {
		return actionCode;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ID := 32;
		public final static integer BUS_OBJ := 32;
		public final static integer REC_SEQ := 5;
		public final static integer UOW_BUFFER_LENGTH := 4;
		public final static integer MSG_BUS_OBJ_NM := 32;
		public final static integer BUS_OBJ_DATA_LENGTH := 4;
		public final static integer MSG_DATA := 5000;
		public final static integer PARTIAL_KEY := ID + BUS_OBJ;
		public final static integer KEY := PARTIAL_KEY + REC_SEQ;
		public final static integer MSG_HDR := MSG_BUS_OBJ_NM + UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION + BUS_OBJ_DATA_LENGTH;
		public final static integer UOW_MESSAGE_BUFFER := MSG_HDR + MSG_DATA;
		public final static integer URQM_COMMON := KEY + UOW_BUFFER_LENGTH + UOW_MESSAGE_BUFFER;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//UrqmCommon