package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.ws.enums.UbocApplyDataPrivacySw;
import com.federatedinsurance.crs.ws.enums.UbocSecAutNbr;

/**Original name: UBOC-SEC-AND-DATA-PRIV-INFO<br>
 * Variable: UBOC-SEC-AND-DATA-PRIV-INFO from copybook HALLUBOC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class UbocSecAndDataPrivInfo {

	//==== PROPERTIES ====
	//Original name: UBOC-APPLY-DATA-PRIVACY-SW
	private UbocApplyDataPrivacySw applyDataPrivacySw := new UbocApplyDataPrivacySw();
	//Original name: UBOC-SECURITY-CONTEXT
	private string securityContext := DefaultValues.stringVal(Len.SECURITY_CONTEXT);
	public final static string NO_CONTEXT := "NO_CONTEXT";
	//Original name: UBOC-SEC-AUT-NBR
	private UbocSecAutNbr secAutNbr := new UbocSecAutNbr();
	//Original name: UBOC-SEC-ASSOCIATION-TYPE
	private string secAssociationType := DefaultValues.stringVal(Len.SEC_ASSOCIATION_TYPE);


	//==== METHODS ====
	public void setUbocSecAndDataPrivInfoBytes([]byte buffer, integer offset) {
		integer position := offset;
		applyDataPrivacySw.setApplyDataPrivacySw(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		securityContext := MarshalByte.readString(buffer, position, Len.SECURITY_CONTEXT);
		position +:= Len.SECURITY_CONTEXT;
		secAutNbr.value := MarshalByte.readFixedString(buffer, position, UbocSecAutNbr.Len.SEC_AUT_NBR);
		position +:= UbocSecAutNbr.Len.SEC_AUT_NBR;
		secAssociationType := MarshalByte.readString(buffer, position, Len.SEC_ASSOCIATION_TYPE);
	}

	public []byte getUbocSecAndDataPrivInfoBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, applyDataPrivacySw.getApplyDataPrivacySw());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, securityContext, Len.SECURITY_CONTEXT);
		position +:= Len.SECURITY_CONTEXT;
		MarshalByte.writeString(buffer, position, secAutNbr.value, UbocSecAutNbr.Len.SEC_AUT_NBR);
		position +:= UbocSecAutNbr.Len.SEC_AUT_NBR;
		MarshalByte.writeString(buffer, position, secAssociationType, Len.SEC_ASSOCIATION_TYPE);
		return buffer;
	}

	public void setSecurityContext(string securityContext) {
		this.securityContext:=Functions.subString(securityContext, Len.SECURITY_CONTEXT);
	}

	public string getSecurityContext() {
		return this.securityContext;
	}

	public boolean isNoContext() {
		return securityContext = NO_CONTEXT;
	}

	public void setNoContext() {
		securityContext := NO_CONTEXT;
	}

	public void setSecAssociationType(string secAssociationType) {
		this.secAssociationType:=Functions.subString(secAssociationType, Len.SEC_ASSOCIATION_TYPE);
	}

	public string getSecAssociationType() {
		return this.secAssociationType;
	}

	public UbocApplyDataPrivacySw getApplyDataPrivacySw() {
		return applyDataPrivacySw;
	}

	public UbocSecAutNbr getSecAutNbr() {
		return secAutNbr;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer SECURITY_CONTEXT := 20;
		public final static integer SEC_ASSOCIATION_TYPE := 8;
		public final static integer UBOC_SEC_AND_DATA_PRIV_INFO := UbocApplyDataPrivacySw.Len.APPLY_DATA_PRIVACY_SW + SECURITY_CONTEXT + UbocSecAutNbr.Len.SEC_AUT_NBR + SEC_ASSOCIATION_TYPE;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//UbocSecAndDataPrivInfo