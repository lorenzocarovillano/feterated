package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC090CO<br>
 * Variable: XZC090CO from copybook XZC090CO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc090co {

	//==== PROPERTIES ====
	//Original name: XZ009O-TRS-DTL-RSP
	private Xz009oTrsDtlRsp trsDtlRsp := new Xz009oTrsDtlRsp();
	//Original name: XZ009O-ERROR-INDICATOR
	private string errorIndicator := DefaultValues.stringVal(Len.ERROR_INDICATOR);
	//Original name: XZ009O-FAULT-CODE
	private string faultCode := DefaultValues.stringVal(Len.FAULT_CODE);
	//Original name: XZ009O-FAULT-STRING
	private string faultString := DefaultValues.stringVal(Len.FAULT_STRING);
	//Original name: XZ009O-FAULT-ACTOR
	private string faultActor := DefaultValues.stringVal(Len.FAULT_ACTOR);
	//Original name: XZ009O-FAULT-DETAIL
	private string faultDetail := DefaultValues.stringVal(Len.FAULT_DETAIL);
	//Original name: XZ009O-NON-SOAP-FAULT-ERR-TXT
	private string nonSoapFaultErrTxt := DefaultValues.stringVal(Len.NON_SOAP_FAULT_ERR_TXT);


	//==== METHODS ====
	public void setErrorInformationBytes([]byte buffer, integer offset) {
		integer position := offset;
		errorIndicator := MarshalByte.readString(buffer, position, Len.ERROR_INDICATOR);
		position +:= Len.ERROR_INDICATOR;
		faultCode := MarshalByte.readString(buffer, position, Len.FAULT_CODE);
		position +:= Len.FAULT_CODE;
		faultString := MarshalByte.readString(buffer, position, Len.FAULT_STRING);
		position +:= Len.FAULT_STRING;
		faultActor := MarshalByte.readString(buffer, position, Len.FAULT_ACTOR);
		position +:= Len.FAULT_ACTOR;
		faultDetail := MarshalByte.readString(buffer, position, Len.FAULT_DETAIL);
		position +:= Len.FAULT_DETAIL;
		nonSoapFaultErrTxt := MarshalByte.readString(buffer, position, Len.NON_SOAP_FAULT_ERR_TXT);
	}

	public []byte getErrorInformationBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, errorIndicator, Len.ERROR_INDICATOR);
		position +:= Len.ERROR_INDICATOR;
		MarshalByte.writeString(buffer, position, faultCode, Len.FAULT_CODE);
		position +:= Len.FAULT_CODE;
		MarshalByte.writeString(buffer, position, faultString, Len.FAULT_STRING);
		position +:= Len.FAULT_STRING;
		MarshalByte.writeString(buffer, position, faultActor, Len.FAULT_ACTOR);
		position +:= Len.FAULT_ACTOR;
		MarshalByte.writeString(buffer, position, faultDetail, Len.FAULT_DETAIL);
		position +:= Len.FAULT_DETAIL;
		MarshalByte.writeString(buffer, position, nonSoapFaultErrTxt, Len.NON_SOAP_FAULT_ERR_TXT);
		return buffer;
	}

	public void setErrorIndicator(string errorIndicator) {
		this.errorIndicator:=Functions.subString(errorIndicator, Len.ERROR_INDICATOR);
	}

	public string getErrorIndicator() {
		return this.errorIndicator;
	}

	public void setFaultCode(string faultCode) {
		this.faultCode:=Functions.subString(faultCode, Len.FAULT_CODE);
	}

	public string getFaultCode() {
		return this.faultCode;
	}

	public void setFaultString(string faultString) {
		this.faultString:=Functions.subString(faultString, Len.FAULT_STRING);
	}

	public string getFaultString() {
		return this.faultString;
	}

	public void setFaultActor(string faultActor) {
		this.faultActor:=Functions.subString(faultActor, Len.FAULT_ACTOR);
	}

	public string getFaultActor() {
		return this.faultActor;
	}

	public void setFaultDetail(string faultDetail) {
		this.faultDetail:=Functions.subString(faultDetail, Len.FAULT_DETAIL);
	}

	public string getFaultDetail() {
		return this.faultDetail;
	}

	public void setNonSoapFaultErrTxt(string nonSoapFaultErrTxt) {
		this.nonSoapFaultErrTxt:=Functions.subString(nonSoapFaultErrTxt, Len.NON_SOAP_FAULT_ERR_TXT);
	}

	public string getNonSoapFaultErrTxt() {
		return this.nonSoapFaultErrTxt;
	}

	public Xz009oTrsDtlRsp getTrsDtlRsp() {
		return trsDtlRsp;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ERROR_INDICATOR := 30;
		public final static integer FAULT_CODE := 30;
		public final static integer FAULT_STRING := 256;
		public final static integer FAULT_ACTOR := 256;
		public final static integer FAULT_DETAIL := 256;
		public final static integer NON_SOAP_FAULT_ERR_TXT := 256;
		public final static integer ERROR_INFORMATION := ERROR_INDICATOR + FAULT_CODE + FAULT_STRING + FAULT_ACTOR + FAULT_DETAIL + NON_SOAP_FAULT_ERR_TXT;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xzc090co