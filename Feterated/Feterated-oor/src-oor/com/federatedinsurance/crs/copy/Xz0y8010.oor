package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ0Y8010<br>
 * Variable: XZ0Y8010 from copybook XZ0Y8010<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz0y8010 {

	//==== PROPERTIES ====
	//Original name: XZY810-TK-NOT-PRC-TS
	private string tkNotPrcTs := DefaultValues.stringVal(Len.TK_NOT_PRC_TS);
	//Original name: XZY810-CSR-ACT-NBR
	private string csrActNbr := DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZY810-USERID
	private string userid := DefaultValues.stringVal(Len.USERID);
	//Original name: FILLER-XZY810-INPUTS
	private string flr1 := DefaultValues.stringVal(Len.FLR1);
	//Original name: XZY810-FRM-ATC-IND
	private char frmAtcInd := DefaultValues.CHAR_VAL;
	//Original name: FILLER-XZY810-OUTPUTS
	private string flr2 := DefaultValues.stringVal(Len.FLR2);


	//==== METHODS ====
	public string getInputOutputParmsFormatted() {
		return MarshalByteExt.bufferToStr(getInputOutputParmsBytes());
	}

	/**Original name: XZY810-INPUT-OUTPUT-PARMS<br>
	 * <pre>***************************************************************
	 *  XZ0Y8010 - COPYBOOK FOR GET FORMS ATTACHED UTILITY PROGRAM   *
	 *                                                               *
	 * ***************************************************************
	 *  MAINTENANCE LOG                                              *
	 *                                                               *
	 *  SI#        DATE      PRGRMR    DESCRIPTION                   *
	 *  ---------  --------- --------- ------------------------------*
	 * TO07602-22  30MAR2010 E404KXS   NEW                           *
	 *                                                               *
	 * ***************************************************************</pre>*/
	public []byte getInputOutputParmsBytes() {
		[]byte buffer := new [Len.INPUT_OUTPUT_PARMS]byte;
		return getInputOutputParmsBytes(buffer, 1);
	}

	public void setInputOutputParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		setInputsBytes(buffer, position);
		position +:= Len.INPUTS;
		setOutputsBytes(buffer, position);
	}

	public []byte getInputOutputParmsBytes([]byte buffer, integer offset) {
		integer position := offset;
		getInputsBytes(buffer, position);
		position +:= Len.INPUTS;
		getOutputsBytes(buffer, position);
		return buffer;
	}

	public void setInputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		setTechnicalKeyBytes(buffer, position);
		position +:= Len.TECHNICAL_KEY;
		csrActNbr := MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		userid := MarshalByte.readString(buffer, position, Len.USERID);
		position +:= Len.USERID;
		flr1 := MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public []byte getInputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		getTechnicalKeyBytes(buffer, position);
		position +:= Len.TECHNICAL_KEY;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position +:= Len.USERID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setTechnicalKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		tkNotPrcTs := MarshalByte.readString(buffer, position, Len.TK_NOT_PRC_TS);
	}

	public []byte getTechnicalKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, tkNotPrcTs, Len.TK_NOT_PRC_TS);
		return buffer;
	}

	public void setTkNotPrcTs(string tkNotPrcTs) {
		this.tkNotPrcTs:=Functions.subString(tkNotPrcTs, Len.TK_NOT_PRC_TS);
	}

	public string getTkNotPrcTs() {
		return this.tkNotPrcTs;
	}

	public void setCsrActNbr(string csrActNbr) {
		this.csrActNbr:=Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public string getCsrActNbr() {
		return this.csrActNbr;
	}

	public string getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setUserid(string userid) {
		this.userid:=Functions.subString(userid, Len.USERID);
	}

	public string getUserid() {
		return this.userid;
	}

	public void setFlr1(string flr1) {
		this.flr1:=Functions.subString(flr1, Len.FLR1);
	}

	public string getFlr1() {
		return this.flr1;
	}

	public void setOutputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		frmAtcInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		flr2 := MarshalByte.readString(buffer, position, Len.FLR2);
	}

	public []byte getOutputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, frmAtcInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void setFrmAtcInd(char frmAtcInd) {
		this.frmAtcInd:=frmAtcInd;
	}

	public char getFrmAtcInd() {
		return this.frmAtcInd;
	}

	public void setFlr2(string flr2) {
		this.flr2:=Functions.subString(flr2, Len.FLR2);
	}

	public string getFlr2() {
		return this.flr2;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer TK_NOT_PRC_TS := 26;
		public final static integer CSR_ACT_NBR := 9;
		public final static integer USERID := 8;
		public final static integer FLR1 := 100;
		public final static integer FLR2 := 400;
		public final static integer TECHNICAL_KEY := TK_NOT_PRC_TS;
		public final static integer INPUTS := TECHNICAL_KEY + CSR_ACT_NBR + USERID + FLR1;
		public final static integer FRM_ATC_IND := 1;
		public final static integer OUTPUTS := FRM_ATC_IND + FLR2;
		public final static integer INPUT_OUTPUT_PARMS := INPUTS + OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xz0y8010