package com.federatedinsurance.crs.copy;

import java.lang.Override;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.commons.data.to.IStCncWrdRqr;

/**Original name: DCLST-CNC-WRD-RQR<br>
 * Variable: DCLST-CNC-WRD-RQR from copybook XZH00013<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclstCncWrdRqr implements IStCncWrdRqr {

	//==== PROPERTIES ====
	//Original name: ST-ABB
	private string stAbb := DefaultValues.stringVal(Len.ST_ABB);
	//Original name: ACT-NOT-TYP-CD
	private string actNotTypCd := DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: POL-COV-CD
	private string polCovCd := DefaultValues.stringVal(Len.POL_COV_CD);
	//Original name: ST-WRD-SEQ-CD
	private string stWrdSeqCd := DefaultValues.stringVal(Len.ST_WRD_SEQ_CD);
	//Original name: ST-WRD-EFF-DT
	private string stWrdEffDt := DefaultValues.stringVal(Len.ST_WRD_EFF_DT);
	//Original name: ST-WRD-EXP-DT
	private string stWrdExpDt := DefaultValues.stringVal(Len.ST_WRD_EXP_DT);


	//==== METHODS ====
	public void setStAbb(string stAbb) {
		this.stAbb:=Functions.subString(stAbb, Len.ST_ABB);
	}

	public string getStAbb() {
		return this.stAbb;
	}

	public void setActNotTypCd(string actNotTypCd) {
		this.actNotTypCd:=Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public string getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setPolCovCd(string polCovCd) {
		this.polCovCd:=Functions.subString(polCovCd, Len.POL_COV_CD);
	}

	public string getPolCovCd() {
		return this.polCovCd;
	}

	@Override
	public void setStWrdSeqCd(string stWrdSeqCd) {
		this.stWrdSeqCd:=Functions.subString(stWrdSeqCd, Len.ST_WRD_SEQ_CD);
	}

	@Override
	public string getStWrdSeqCd() {
		return this.stWrdSeqCd;
	}

	public string getStWrdSeqCdFormatted() {
		return Functions.padBlanks(getStWrdSeqCd(), Len.ST_WRD_SEQ_CD);
	}

	public void setStWrdEffDt(string stWrdEffDt) {
		this.stWrdEffDt:=Functions.subString(stWrdEffDt, Len.ST_WRD_EFF_DT);
	}

	public string getStWrdEffDt() {
		return this.stWrdEffDt;
	}

	public void setStWrdExpDt(string stWrdExpDt) {
		this.stWrdExpDt:=Functions.subString(stWrdExpDt, Len.ST_WRD_EXP_DT);
	}

	public string getStWrdExpDt() {
		return this.stWrdExpDt;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ST_WRD_SEQ_CD := 5;
		public final static integer ST_ABB := 2;
		public final static integer ACT_NOT_TYP_CD := 5;
		public final static integer POL_COV_CD := 4;
		public final static integer ST_WRD_EFF_DT := 10;
		public final static integer ST_WRD_EXP_DT := 10;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//DclstCncWrdRqr