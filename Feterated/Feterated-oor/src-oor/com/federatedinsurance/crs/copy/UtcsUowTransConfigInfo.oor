package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.ws.enums.UbocUowLockStrategyCd;
import com.federatedinsurance.crs.ws.enums.UtcsAuditInd;
import com.federatedinsurance.crs.ws.enums.UtcsCnLvlSecInd;
import com.federatedinsurance.crs.ws.enums.UtcsDataPrivacyInd;

/**Original name: UTCS-UOW-TRANS-CONFIG-INFO<br>
 * Variable: UTCS-UOW-TRANS-CONFIG-INFO from copybook HALLUTCS<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class UtcsUowTransConfigInfo {

	//==== PROPERTIES ====
	//Original name: UTCS-UOW-NAME
	private string uowName := DefaultValues.stringVal(Len.UOW_NAME);
	//Original name: UTCS-UOW-MODULE
	private string uowModule := DefaultValues.stringVal(Len.UOW_MODULE);
	//Original name: UTCS-FOLDER-LOKS-DEL-TIME
	private integer folderLoksDelTime := DefaultValues.BIN_INT_VAL;
	//Original name: UTCS-UOW-LOCK-ACTION-CD
	private UbocUowLockStrategyCd uowLockActionCd := new UbocUowLockStrategyCd();
	//Original name: UTCS-CN-LVL-SEC-IND
	private UtcsCnLvlSecInd cnLvlSecInd := new UtcsCnLvlSecInd();
	//Original name: UTCS-CONTROL-LVL-SEC-MOD-NI
	private char controlLvlSecModNi := DefaultValues.CHAR_VAL;
	//Original name: UTCS-CONTROL-LVL-SEC-MODULE
	private string controlLvlSecModule := DefaultValues.stringVal(Len.CONTROL_LVL_SEC_MODULE);
	//Original name: UTCS-DATA-PRIVACY-IND
	private UtcsDataPrivacyInd dataPrivacyInd := new UtcsDataPrivacyInd();
	//Original name: UTCS-AUDIT-IND
	private UtcsAuditInd auditInd := new UtcsAuditInd();


	//==== METHODS ====
	public void initUtcsUowTransConfigInfoSpaces() {
		uowName := "";
		uowModule := "";
		folderLoksDelTime := Types.INVALID_BINARY_INT_VAL;
		uowLockActionCd.setUbocUowLockStrategyCd(Types.SPACE_CHAR);
		cnLvlSecInd.setCnLvlSecInd(Types.SPACE_CHAR);
		controlLvlSecModNi := Types.SPACE_CHAR;
		controlLvlSecModule := "";
		dataPrivacyInd.setDataPrivacyInd(Types.SPACE_CHAR);
		auditInd.setAuditInd(Types.SPACE_CHAR);
	}

	public void setUowName(string uowName) {
		this.uowName:=Functions.subString(uowName, Len.UOW_NAME);
	}

	public string getUowName() {
		return this.uowName;
	}

	public void setUowModule(string uowModule) {
		this.uowModule:=Functions.subString(uowModule, Len.UOW_MODULE);
	}

	public string getUowModule() {
		return this.uowModule;
	}

	public void setFolderLoksDelTime(integer folderLoksDelTime) {
		this.folderLoksDelTime:=folderLoksDelTime;
	}

	public integer getFolderLoksDelTime() {
		return this.folderLoksDelTime;
	}

	public void setControlLvlSecModNi(char controlLvlSecModNi) {
		this.controlLvlSecModNi:=controlLvlSecModNi;
	}

	public char getControlLvlSecModNi() {
		return this.controlLvlSecModNi;
	}

	public void setControlLvlSecModule(string controlLvlSecModule) {
		this.controlLvlSecModule:=Functions.subString(controlLvlSecModule, Len.CONTROL_LVL_SEC_MODULE);
	}

	public string getControlLvlSecModule() {
		return this.controlLvlSecModule;
	}

	public UtcsAuditInd getAuditInd() {
		return auditInd;
	}

	public UtcsCnLvlSecInd getCnLvlSecInd() {
		return cnLvlSecInd;
	}

	public UtcsDataPrivacyInd getDataPrivacyInd() {
		return dataPrivacyInd;
	}

	public UbocUowLockStrategyCd getUowLockActionCd() {
		return uowLockActionCd;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer UOW_NAME := 32;
		public final static integer UOW_MODULE := 32;
		public final static integer CONTROL_LVL_SEC_MODULE := 32;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//UtcsUowTransConfigInfo