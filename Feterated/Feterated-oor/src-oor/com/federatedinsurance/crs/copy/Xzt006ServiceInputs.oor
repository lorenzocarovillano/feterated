package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZT006-SERVICE-INPUTS<br>
 * Variable: XZT006-SERVICE-INPUTS from copybook XZ0Z0006<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzt006ServiceInputs {

	//==== PROPERTIES ====
	//Original name: XZT06I-TECHNICAL-KEY
	private Xzt05oTechnicalKey technicalKey := new Xzt05oTechnicalKey();
	//Original name: XZT06I-CSR-ACT-NBR
	private string csrActNbr := DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZT06I-ACT-NOT-TYP-CD
	private string actNotTypCd := DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZT06I-NOT-DT
	private string notDt := DefaultValues.stringVal(Len.NOT_DT);
	//Original name: XZT06I-PDC-NBR
	private string pdcNbr := DefaultValues.stringVal(Len.PDC_NBR);
	//Original name: XZT06I-PDC-NM
	private string pdcNm := DefaultValues.stringVal(Len.PDC_NM);
	//Original name: XZT06I-TOT-FEE-AMT
	private decimal(10,2) totFeeAmt := DefaultValues.DEC_VAL;
	//Original name: XZT06I-ST-ABB
	private string stAbb := DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZT06I-CER-HLD-NOT-IND
	private char cerHldNotInd := DefaultValues.CHAR_VAL;
	//Original name: XZT06I-ADD-CNC-DAY
	private integer addCncDay := DefaultValues.INT_VAL;
	//Original name: XZT06I-REA-DES
	private string reaDes := DefaultValues.stringVal(Len.REA_DES);
	//Original name: XZT06I-USERID
	private string userid := DefaultValues.stringVal(Len.USERID);


	//==== METHODS ====
	public []byte getServiceInputsBytes() {
		[]byte buffer := new [Len.SERVICE_INPUTS]byte;
		return getServiceInputsBytes(buffer, 1);
	}

	public void setServiceInputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		technicalKey.setTechnicalKeyBytes(buffer, position);
		position +:= Xzt05oTechnicalKey.Len.TECHNICAL_KEY;
		csrActNbr := MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		actNotTypCd := MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_CD);
		position +:= Len.ACT_NOT_TYP_CD;
		notDt := MarshalByte.readString(buffer, position, Len.NOT_DT);
		position +:= Len.NOT_DT;
		setPdcBytes(buffer, position);
		position +:= Len.PDC;
		totFeeAmt := MarshalByte.readDecimal(buffer, position, Len.Int.TOT_FEE_AMT, Len.Fract.TOT_FEE_AMT);
		position +:= Len.TOT_FEE_AMT;
		stAbb := MarshalByte.readString(buffer, position, Len.ST_ABB);
		position +:= Len.ST_ABB;
		cerHldNotInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		addCncDay := MarshalByte.readInt(buffer, position, Len.ADD_CNC_DAY);
		position +:= Len.ADD_CNC_DAY;
		reaDes := MarshalByte.readString(buffer, position, Len.REA_DES);
		position +:= Len.REA_DES;
		userid := MarshalByte.readString(buffer, position, Len.USERID);
	}

	public []byte getServiceInputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		technicalKey.getTechnicalKeyBytes(buffer, position);
		position +:= Xzt05oTechnicalKey.Len.TECHNICAL_KEY;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position +:= Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, notDt, Len.NOT_DT);
		position +:= Len.NOT_DT;
		getPdcBytes(buffer, position);
		position +:= Len.PDC;
		MarshalByte.writeDecimal(buffer, position, totFeeAmt);
		position +:= Len.TOT_FEE_AMT;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position +:= Len.ST_ABB;
		MarshalByte.writeChar(buffer, position, cerHldNotInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeInt(buffer, position, addCncDay, Len.ADD_CNC_DAY);
		position +:= Len.ADD_CNC_DAY;
		MarshalByte.writeString(buffer, position, reaDes, Len.REA_DES);
		position +:= Len.REA_DES;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setCsrActNbr(string csrActNbr) {
		this.csrActNbr:=Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public string getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setActNotTypCd(string actNotTypCd) {
		this.actNotTypCd:=Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public string getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setNotDt(string notDt) {
		this.notDt:=Functions.subString(notDt, Len.NOT_DT);
	}

	public string getNotDt() {
		return this.notDt;
	}

	public void setPdcBytes([]byte buffer, integer offset) {
		integer position := offset;
		pdcNbr := MarshalByte.readString(buffer, position, Len.PDC_NBR);
		position +:= Len.PDC_NBR;
		pdcNm := MarshalByte.readString(buffer, position, Len.PDC_NM);
	}

	public []byte getPdcBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, pdcNbr, Len.PDC_NBR);
		position +:= Len.PDC_NBR;
		MarshalByte.writeString(buffer, position, pdcNm, Len.PDC_NM);
		return buffer;
	}

	public void setPdcNbr(string pdcNbr) {
		this.pdcNbr:=Functions.subString(pdcNbr, Len.PDC_NBR);
	}

	public string getPdcNbr() {
		return this.pdcNbr;
	}

	public void setPdcNm(string pdcNm) {
		this.pdcNm:=Functions.subString(pdcNm, Len.PDC_NM);
	}

	public string getPdcNm() {
		return this.pdcNm;
	}

	public void setTotFeeAmt(decimal(10,2) totFeeAmt) {
		this.totFeeAmt:=totFeeAmt;
	}

	public decimal(10,2) getTotFeeAmt() {
		return this.totFeeAmt;
	}

	public void setStAbb(string stAbb) {
		this.stAbb:=Functions.subString(stAbb, Len.ST_ABB);
	}

	public string getStAbb() {
		return this.stAbb;
	}

	public void setCerHldNotInd(char cerHldNotInd) {
		this.cerHldNotInd:=cerHldNotInd;
	}

	public char getCerHldNotInd() {
		return this.cerHldNotInd;
	}

	public void setAddCncDay(integer addCncDay) {
		this.addCncDay:=addCncDay;
	}

	public integer getAddCncDay() {
		return this.addCncDay;
	}

	public void setReaDes(string reaDes) {
		this.reaDes:=Functions.subString(reaDes, Len.REA_DES);
	}

	public string getReaDes() {
		return this.reaDes;
	}

	public void setUserid(string userid) {
		this.userid:=Functions.subString(userid, Len.USERID);
	}

	public string getUserid() {
		return this.userid;
	}

	public Xzt05oTechnicalKey getTechnicalKey() {
		return technicalKey;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CSR_ACT_NBR := 9;
		public final static integer ACT_NOT_TYP_CD := 5;
		public final static integer NOT_DT := 10;
		public final static integer PDC_NBR := 5;
		public final static integer PDC_NM := 120;
		public final static integer ST_ABB := 2;
		public final static integer REA_DES := 500;
		public final static integer USERID := 8;
		public final static integer PDC := PDC_NBR + PDC_NM;
		public final static integer TOT_FEE_AMT := 10;
		public final static integer CER_HLD_NOT_IND := 1;
		public final static integer ADD_CNC_DAY := 5;
		public final static integer SERVICE_INPUTS := Xzt05oTechnicalKey.Len.TECHNICAL_KEY + CSR_ACT_NBR + ACT_NOT_TYP_CD + NOT_DT + PDC + TOT_FEE_AMT + ST_ABB + CER_HLD_NOT_IND + ADD_CNC_DAY + REA_DES + USERID;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer TOT_FEE_AMT := 8;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer TOT_FEE_AMT := 2;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//Xzt006ServiceInputs