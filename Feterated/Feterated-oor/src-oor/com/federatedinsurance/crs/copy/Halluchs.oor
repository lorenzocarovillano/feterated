package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HALLUCHS<br>
 * Variable: HALLUCHS from copybook HALLUCHS<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Halluchs {

	//==== PROPERTIES ====
	public final static integer STRING_X_MAXOCCURS := 150;
	/**Original name: HALOUCHS-LENGTH<br>
	 * <pre>***************************************************************
	 *  COMMAREA FOR HALOUCHS (CHECK SUM ROUTINE)                    *
	 * ***************************************************************
	 *  CHANGE LOG                                                   *
	 *                                                               *
	 *  SI#      DATE     PROG#     DESCRIPTION                      *
	 *  -------- -------- --------- ---------------------------------*
	 *  SAVANNAH 22MAR00  JAF       NEW                              *
	 *  C15425   18JUL01  ARSI600   INCREASE LENGTH OF STRING THAT   *
	 *                              HOLDS THE KEY BEING CHECKED.     *
	 * ***************************************************************</pre>*/
	private short length2 := DefaultValues.BIN_SHORT_VAL;
	/**Original name: HALOUCHS-STRING-X<br>
	 * <pre>      07 HALOUCHS-STRING-X           PIC X  OCCURS 100.</pre>*/
	private []char stringX := new [STRING_X_MAXOCCURS]char;
	//Original name: HALOUCHS-CHECK-SUM
	public string checkSum := DefaultValues.stringVal(Len.CHECK_SUM);

	//==== CONSTRUCTORS ====
	public Halluchs() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int stringXIdx in 1.. STRING_X_MAXOCCURS 
		do
			setStringX(stringXIdx, DefaultValues.CHAR_VAL);
		enddo
	}

	public void setLength2(short length2) {
		this.length2:=length2;
	}

	public short getLength2() {
		return this.length2;
	}

	public void setStringFldBytes([]byte buffer) {
		setStringFldBytes(buffer, 1);
	}

	public void setStringFldBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. STRING_X_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				setStringX(idx, MarshalByte.readChar(buffer, position));
				position +:= Types.CHAR_SIZE;
			else
				setStringX(idx, Types.SPACE_CHAR);
				position +:= Types.CHAR_SIZE;
			endif
		enddo
	}

	public []byte getStringFldBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. STRING_X_MAXOCCURS 
		do
			MarshalByte.writeChar(buffer, position, getStringX(idx));
			position +:= Types.CHAR_SIZE;
		enddo
		return buffer;
	}

	public void setStringX(integer stringXIdx, char stringX) {
		this.stringX[stringXIdx]:=stringX;
	}

	public char getStringX(integer stringXIdx) {
		return this.stringX[stringXIdx];
	}

	public void setCheckSum(integer checkSum) {
		this.checkSum := NumericDisplay.asString(checkSum, Len.CHECK_SUM);
	}

	public integer getCheckSum() {
		return NumericDisplay.asInt(this.checkSum);
	}

	public string getCheckSumFormatted() {
		return this.checkSum;
	}

	public string getCheckSumAsString() {
		return getCheckSumFormatted();
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer CHECK_SUM := 9;
		public final static integer LENGTH2 := 2;
		public final static integer STRING_X := 1;
		public final static integer STRING_FLD := Halluchs.STRING_X_MAXOCCURS * STRING_X;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Halluchs