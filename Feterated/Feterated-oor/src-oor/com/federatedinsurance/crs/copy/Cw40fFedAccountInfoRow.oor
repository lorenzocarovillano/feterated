package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW40F-FED-ACCOUNT-INFO-ROW<br>
 * Variable: CW40F-FED-ACCOUNT-INFO-ROW from copybook CAWLF040<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw40fFedAccountInfoRow {

	//==== PROPERTIES ====
	//Original name: CW40F-FED-ACCOUNT-INFO-CSUM
	private string fedAccountInfoCsum := DefaultValues.stringVal(Len.FED_ACCOUNT_INFO_CSUM);
	//Original name: CW40F-CIAI-ACT-TCH-KEY-KCRE
	private string ciaiActTchKeyKcre := DefaultValues.stringVal(Len.CIAI_ACT_TCH_KEY_KCRE);
	//Original name: CW40F-HISTORY-VLD-NBR-KCRE
	private string historyVldNbrKcre := DefaultValues.stringVal(Len.HISTORY_VLD_NBR_KCRE);
	//Original name: CW40F-EFFECTIVE-DT-KCRE
	private string effectiveDtKcre := DefaultValues.stringVal(Len.EFFECTIVE_DT_KCRE);
	//Original name: CW40F-TRANS-PROCESS-DT
	private string transProcessDt := DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW40F-CIAI-ACT-TCH-KEY
	private string ciaiActTchKey := DefaultValues.stringVal(Len.CIAI_ACT_TCH_KEY);
	//Original name: CW40F-HISTORY-VLD-NBR-SIGN
	private char historyVldNbrSign := DefaultValues.CHAR_VAL;
	//Original name: CW40F-HISTORY-VLD-NBR
	private string historyVldNbr := DefaultValues.stringVal(Len.HISTORY_VLD_NBR);
	//Original name: CW40F-EFFECTIVE-DT
	private string effectiveDt := DefaultValues.stringVal(Len.EFFECTIVE_DT);
	//Original name: CW40F-CIAI-ACT-TCH-KEY-CI
	private char ciaiActTchKeyCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-HISTORY-VLD-NBR-CI
	private char historyVldNbrCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-EFFECTIVE-DT-CI
	private char effectiveDtCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-FED-ACCOUNT-INFO-DATA
	private Cw40fFedAccountInfoData fedAccountInfoData := new Cw40fFedAccountInfoData();


	//==== METHODS ====
	public void setCw40fFedAccountInfoRowFormatted(string data) {
		[]byte buffer := new [Len.CW40F_FED_ACCOUNT_INFO_ROW]byte;
		MarshalByte.writeString(buffer, 1, data, Len.CW40F_FED_ACCOUNT_INFO_ROW);
		setCw40fFedAccountInfoRowBytes(buffer, 1);
	}

	public string getCw40fFedAccountInfoRowFormatted() {
		return MarshalByteExt.bufferToStr(getCw40fFedAccountInfoRowBytes());
	}

	public []byte getCw40fFedAccountInfoRowBytes() {
		[]byte buffer := new [Len.CW40F_FED_ACCOUNT_INFO_ROW]byte;
		return getCw40fFedAccountInfoRowBytes(buffer, 1);
	}

	public void setCw40fFedAccountInfoRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setFedAccountInfoFixedBytes(buffer, position);
		position +:= Len.FED_ACCOUNT_INFO_FIXED;
		setFedAccountInfoDatesBytes(buffer, position);
		position +:= Len.FED_ACCOUNT_INFO_DATES;
		setFedAccountInfoKeyBytes(buffer, position);
		position +:= Len.FED_ACCOUNT_INFO_KEY;
		setFedAccountInfoKeyCiBytes(buffer, position);
		position +:= Len.FED_ACCOUNT_INFO_KEY_CI;
		fedAccountInfoData.setFedAccountInfoDataBytes(buffer, position);
	}

	public []byte getCw40fFedAccountInfoRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getFedAccountInfoFixedBytes(buffer, position);
		position +:= Len.FED_ACCOUNT_INFO_FIXED;
		getFedAccountInfoDatesBytes(buffer, position);
		position +:= Len.FED_ACCOUNT_INFO_DATES;
		getFedAccountInfoKeyBytes(buffer, position);
		position +:= Len.FED_ACCOUNT_INFO_KEY;
		getFedAccountInfoKeyCiBytes(buffer, position);
		position +:= Len.FED_ACCOUNT_INFO_KEY_CI;
		fedAccountInfoData.getFedAccountInfoDataBytes(buffer, position);
		return buffer;
	}

	public void setFedAccountInfoFixedBytes([]byte buffer, integer offset) {
		integer position := offset;
		fedAccountInfoCsum := MarshalByte.readFixedString(buffer, position, Len.FED_ACCOUNT_INFO_CSUM);
		position +:= Len.FED_ACCOUNT_INFO_CSUM;
		ciaiActTchKeyKcre := MarshalByte.readString(buffer, position, Len.CIAI_ACT_TCH_KEY_KCRE);
		position +:= Len.CIAI_ACT_TCH_KEY_KCRE;
		historyVldNbrKcre := MarshalByte.readString(buffer, position, Len.HISTORY_VLD_NBR_KCRE);
		position +:= Len.HISTORY_VLD_NBR_KCRE;
		effectiveDtKcre := MarshalByte.readString(buffer, position, Len.EFFECTIVE_DT_KCRE);
	}

	public []byte getFedAccountInfoFixedBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, fedAccountInfoCsum, Len.FED_ACCOUNT_INFO_CSUM);
		position +:= Len.FED_ACCOUNT_INFO_CSUM;
		MarshalByte.writeString(buffer, position, ciaiActTchKeyKcre, Len.CIAI_ACT_TCH_KEY_KCRE);
		position +:= Len.CIAI_ACT_TCH_KEY_KCRE;
		MarshalByte.writeString(buffer, position, historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
		position +:= Len.HISTORY_VLD_NBR_KCRE;
		MarshalByte.writeString(buffer, position, effectiveDtKcre, Len.EFFECTIVE_DT_KCRE);
		return buffer;
	}

	public void setCiaiActTchKeyKcre(string ciaiActTchKeyKcre) {
		this.ciaiActTchKeyKcre:=Functions.subString(ciaiActTchKeyKcre, Len.CIAI_ACT_TCH_KEY_KCRE);
	}

	public string getCiaiActTchKeyKcre() {
		return this.ciaiActTchKeyKcre;
	}

	public void setHistoryVldNbrKcre(string historyVldNbrKcre) {
		this.historyVldNbrKcre:=Functions.subString(historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
	}

	public string getHistoryVldNbrKcre() {
		return this.historyVldNbrKcre;
	}

	public void setEffectiveDtKcre(string effectiveDtKcre) {
		this.effectiveDtKcre:=Functions.subString(effectiveDtKcre, Len.EFFECTIVE_DT_KCRE);
	}

	public string getEffectiveDtKcre() {
		return this.effectiveDtKcre;
	}

	public void setFedAccountInfoDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		transProcessDt := MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public []byte getFedAccountInfoDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(string transProcessDt) {
		this.transProcessDt:=Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public string getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setFedAccountInfoKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		ciaiActTchKey := MarshalByte.readString(buffer, position, Len.CIAI_ACT_TCH_KEY);
		position +:= Len.CIAI_ACT_TCH_KEY;
		historyVldNbrSign := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		historyVldNbr := MarshalByte.readFixedString(buffer, position, Len.HISTORY_VLD_NBR);
		position +:= Len.HISTORY_VLD_NBR;
		effectiveDt := MarshalByte.readString(buffer, position, Len.EFFECTIVE_DT);
	}

	public []byte getFedAccountInfoKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, ciaiActTchKey, Len.CIAI_ACT_TCH_KEY);
		position +:= Len.CIAI_ACT_TCH_KEY;
		MarshalByte.writeChar(buffer, position, historyVldNbrSign);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, historyVldNbr, Len.HISTORY_VLD_NBR);
		position +:= Len.HISTORY_VLD_NBR;
		MarshalByte.writeString(buffer, position, effectiveDt, Len.EFFECTIVE_DT);
		return buffer;
	}

	public void setCiaiActTchKey(string ciaiActTchKey) {
		this.ciaiActTchKey:=Functions.subString(ciaiActTchKey, Len.CIAI_ACT_TCH_KEY);
	}

	public string getCiaiActTchKey() {
		return this.ciaiActTchKey;
	}

	public void setHistoryVldNbrSign(char historyVldNbrSign) {
		this.historyVldNbrSign:=historyVldNbrSign;
	}

	public char getHistoryVldNbrSign() {
		return this.historyVldNbrSign;
	}

	public void setEffectiveDt(string effectiveDt) {
		this.effectiveDt:=Functions.subString(effectiveDt, Len.EFFECTIVE_DT);
	}

	public string getEffectiveDt() {
		return this.effectiveDt;
	}

	public void setFedAccountInfoKeyCiBytes([]byte buffer, integer offset) {
		integer position := offset;
		ciaiActTchKeyCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		historyVldNbrCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		effectiveDtCi := MarshalByte.readChar(buffer, position);
	}

	public []byte getFedAccountInfoKeyCiBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, ciaiActTchKeyCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, historyVldNbrCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, effectiveDtCi);
		return buffer;
	}

	public void setCiaiActTchKeyCi(char ciaiActTchKeyCi) {
		this.ciaiActTchKeyCi:=ciaiActTchKeyCi;
	}

	public char getCiaiActTchKeyCi() {
		return this.ciaiActTchKeyCi;
	}

	public void setHistoryVldNbrCi(char historyVldNbrCi) {
		this.historyVldNbrCi:=historyVldNbrCi;
	}

	public char getHistoryVldNbrCi() {
		return this.historyVldNbrCi;
	}

	public void setEffectiveDtCi(char effectiveDtCi) {
		this.effectiveDtCi:=effectiveDtCi;
	}

	public char getEffectiveDtCi() {
		return this.effectiveDtCi;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer FED_ACCOUNT_INFO_CSUM := 9;
		public final static integer CIAI_ACT_TCH_KEY_KCRE := 32;
		public final static integer HISTORY_VLD_NBR_KCRE := 32;
		public final static integer EFFECTIVE_DT_KCRE := 32;
		public final static integer TRANS_PROCESS_DT := 10;
		public final static integer CIAI_ACT_TCH_KEY := 20;
		public final static integer HISTORY_VLD_NBR := 5;
		public final static integer EFFECTIVE_DT := 10;
		public final static integer FED_ACCOUNT_INFO_FIXED := FED_ACCOUNT_INFO_CSUM + CIAI_ACT_TCH_KEY_KCRE + HISTORY_VLD_NBR_KCRE + EFFECTIVE_DT_KCRE;
		public final static integer FED_ACCOUNT_INFO_DATES := TRANS_PROCESS_DT;
		public final static integer HISTORY_VLD_NBR_SIGN := 1;
		public final static integer FED_ACCOUNT_INFO_KEY := CIAI_ACT_TCH_KEY + HISTORY_VLD_NBR_SIGN + HISTORY_VLD_NBR + EFFECTIVE_DT;
		public final static integer CIAI_ACT_TCH_KEY_CI := 1;
		public final static integer HISTORY_VLD_NBR_CI := 1;
		public final static integer EFFECTIVE_DT_CI := 1;
		public final static integer FED_ACCOUNT_INFO_KEY_CI := CIAI_ACT_TCH_KEY_CI + HISTORY_VLD_NBR_CI + EFFECTIVE_DT_CI;
		public final static integer CW40F_FED_ACCOUNT_INFO_ROW := FED_ACCOUNT_INFO_FIXED + FED_ACCOUNT_INFO_DATES + FED_ACCOUNT_INFO_KEY + FED_ACCOUNT_INFO_KEY_CI + Cw40fFedAccountInfoData.Len.FED_ACCOUNT_INFO_DATA;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Cw40fFedAccountInfoRow