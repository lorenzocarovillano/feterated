package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.federatedinsurance.crs.ws.enums.EstoRecordingLevel;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ESTO-INPUT-KEY<br>
 * Variable: ESTO-INPUT-KEY from copybook HALLESTO<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class EstoInputKey {

	//==== PROPERTIES ====
	//Original name: ESTO-STORE-ID
	private string storeId := DefaultValues.stringVal(Len.STORE_ID);
	//Original name: ESTO-RECORDING-LEVEL
	private EstoRecordingLevel recordingLevel := new EstoRecordingLevel();
	//Original name: ESTO-ERR-SEQ-NUM
	private string errSeqNum := DefaultValues.stringVal(Len.ERR_SEQ_NUM);


	//==== METHODS ====
	public void setEstoInputKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		storeId := MarshalByte.readString(buffer, position, Len.STORE_ID);
		position +:= Len.STORE_ID;
		recordingLevel.setRecordingLevel(MarshalByte.readString(buffer, position, EstoRecordingLevel.Len.RECORDING_LEVEL));
		position +:= EstoRecordingLevel.Len.RECORDING_LEVEL;
		errSeqNum := MarshalByte.readFixedString(buffer, position, Len.ERR_SEQ_NUM);
	}

	public []byte getEstoInputKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, storeId, Len.STORE_ID);
		position +:= Len.STORE_ID;
		MarshalByte.writeString(buffer, position, recordingLevel.getRecordingLevel(), EstoRecordingLevel.Len.RECORDING_LEVEL);
		position +:= EstoRecordingLevel.Len.RECORDING_LEVEL;
		MarshalByte.writeString(buffer, position, errSeqNum, Len.ERR_SEQ_NUM);
		return buffer;
	}

	public void setStoreId(string storeId) {
		this.storeId:=Functions.subString(storeId, Len.STORE_ID);
	}

	public string getStoreId() {
		return this.storeId;
	}

	public void setEstoErrSeqNum(integer estoErrSeqNum) {
		this.errSeqNum := NumericDisplay.asString(estoErrSeqNum, Len.ERR_SEQ_NUM);
	}

	public void setErrSeqNumFormatted(string errSeqNum) {
		this.errSeqNum:=Trunc.toUnsignedNumeric(errSeqNum, Len.ERR_SEQ_NUM);
	}

	public integer getErrSeqNum() {
		return NumericDisplay.asInt(this.errSeqNum);
	}

	public string getEstoErrSeqNumFormatted() {
		return this.errSeqNum;
	}

	public EstoRecordingLevel getRecordingLevel() {
		return recordingLevel;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer STORE_ID := 32;
		public final static integer ERR_SEQ_NUM := 5;
		public final static integer ESTO_INPUT_KEY := STORE_ID + EstoRecordingLevel.Len.RECORDING_LEVEL + ERR_SEQ_NUM;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//EstoInputKey