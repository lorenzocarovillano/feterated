package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;
import com.federatedinsurance.crs.ws.occurs.Xz08coAddlItsInfo;

/**Original name: XZC080CO<br>
 * Variable: XZC080CO from copybook XZC080CO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc080co {

	//==== PROPERTIES ====
	public final static integer ERR_MSG_MAXOCCURS := 10;
	public final static integer WNG_MSG_MAXOCCURS := 10;
	public final static integer ADDL_ITS_INFO_MAXOCCURS := 250;
	//Original name: XZC08O-RET-CD
	private Xz08coRetCd retCd := new Xz08coRetCd();
	//Original name: XZC08O-ERR-MSG
	private []string errMsg := new [ERR_MSG_MAXOCCURS]string;
	//Original name: XZC08O-WNG-MSG
	private []string wngMsg := new [WNG_MSG_MAXOCCURS]string;
	//Original name: XZC08O-ADDL-ITS-INFO
	private []Xz08coAddlItsInfo addlItsInfo := new [ADDL_ITS_INFO_MAXOCCURS]Xz08coAddlItsInfo;

	//==== CONSTRUCTORS ====
	public Xzc080co() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int errMsgIdx in 1.. ERR_MSG_MAXOCCURS 
		do
			setErrMsg(errMsgIdx, DefaultValues.stringVal(Len.ERR_MSG));
		enddo
		for int wngMsgIdx in 1.. WNG_MSG_MAXOCCURS 
		do
			setWngMsg(wngMsgIdx, DefaultValues.stringVal(Len.WNG_MSG));
		enddo
		for int addlItsInfoIdx in 1.. ADDL_ITS_INFO_MAXOCCURS 
		do
			addlItsInfo[addlItsInfoIdx] := new Xz08coAddlItsInfo();
		enddo
	}

	public void setServiceOutputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		setRetMsgBytes(buffer, position);
		position +:= Len.RET_MSG;
		for integer idx in 1.. ADDL_ITS_INFO_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				addlItsInfo[idx].setXz08coAddlItsInfoBytes(buffer, position);
				position +:= Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;
			else
				addlItsInfo[idx].initXz08coAddlItsInfoSpaces();
				position +:= Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;
			endif
		enddo
	}

	public []byte getServiceOutputsBytes([]byte buffer, integer offset) {
		integer position := offset;
		getRetMsgBytes(buffer, position);
		position +:= Len.RET_MSG;
		for integer idx in 1.. ADDL_ITS_INFO_MAXOCCURS 
		do
			addlItsInfo[idx].getXz08coAddlItsInfoBytes(buffer, position);
			position +:= Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;
		enddo
		return buffer;
	}

	public void setRetMsgBytes([]byte buffer, integer offset) {
		integer position := offset;
		retCd.setRetCd(MarshalByte.readShort(buffer, position, Xz08coRetCd.Len.XZ08CO_RET_CD));
		position +:= Xz08coRetCd.Len.XZ08CO_RET_CD;
		setErrLisBytes(buffer, position);
		position +:= Len.ERR_LIS;
		setWngLisBytes(buffer, position);
	}

	public []byte getRetMsgBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeShort(buffer, position, retCd.getRetCd(), Xz08coRetCd.Len.XZ08CO_RET_CD);
		position +:= Xz08coRetCd.Len.XZ08CO_RET_CD;
		getErrLisBytes(buffer, position);
		position +:= Len.ERR_LIS;
		getWngLisBytes(buffer, position);
		return buffer;
	}

	public void setErrLisBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. ERR_MSG_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				setErrMsg(idx, MarshalByte.readString(buffer, position, Len.ERR_MSG));
				position +:= Len.ERR_MSG;
			else
				setErrMsg(idx, "");
				position +:= Len.ERR_MSG;
			endif
		enddo
	}

	public []byte getErrLisBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. ERR_MSG_MAXOCCURS 
		do
			MarshalByte.writeString(buffer, position, getErrMsg(idx), Len.ERR_MSG);
			position +:= Len.ERR_MSG;
		enddo
		return buffer;
	}

	public void setErrMsg(integer errMsgIdx, string errMsg) {
		this.errMsg[errMsgIdx]:=Functions.subString(errMsg, Len.ERR_MSG);
	}

	public string getErrMsg(integer errMsgIdx) {
		return this.errMsg[errMsgIdx];
	}

	public string getErrMsgFormatted(integer errMsgIdx) {
		return Functions.padBlanks(getErrMsg(errMsgIdx), Len.ERR_MSG);
	}

	public void setWngLisBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. WNG_MSG_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				setWngMsg(idx, MarshalByte.readString(buffer, position, Len.WNG_MSG));
				position +:= Len.WNG_MSG;
			else
				setWngMsg(idx, "");
				position +:= Len.WNG_MSG;
			endif
		enddo
	}

	public []byte getWngLisBytes([]byte buffer, integer offset) {
		integer position := offset;
		for integer idx in 1.. WNG_MSG_MAXOCCURS 
		do
			MarshalByte.writeString(buffer, position, getWngMsg(idx), Len.WNG_MSG);
			position +:= Len.WNG_MSG;
		enddo
		return buffer;
	}

	public void setWngMsg(integer wngMsgIdx, string wngMsg) {
		this.wngMsg[wngMsgIdx]:=Functions.subString(wngMsg, Len.WNG_MSG);
	}

	public string getWngMsg(integer wngMsgIdx) {
		return this.wngMsg[wngMsgIdx];
	}

	public Xz08coAddlItsInfo getAddlItsInfo(integer idx) {
		return addlItsInfo[idx];
	}

	public Xz08coRetCd getRetCd() {
		return retCd;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ERR_MSG := 500;
		public final static integer WNG_MSG := 500;
		public final static integer ERR_LIS := Xzc080co.ERR_MSG_MAXOCCURS * ERR_MSG;
		public final static integer WNG_LIS := Xzc080co.WNG_MSG_MAXOCCURS * WNG_MSG;
		public final static integer RET_MSG := Xz08coRetCd.Len.XZ08CO_RET_CD + ERR_LIS + WNG_LIS;
		public final static integer SERVICE_OUTPUTS := RET_MSG + Xzc080co.ADDL_ITS_INFO_MAXOCCURS * Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xzc080co