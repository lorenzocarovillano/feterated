package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW49F-FED-SEG-INFO-DATA<br>
 * Variable: CW49F-FED-SEG-INFO-DATA from copybook CAWLF049<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw49fFedSegInfoData {

	//==== PROPERTIES ====
	/**Original name: CW49F-SEG-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char segCdCi := DefaultValues.CHAR_VAL;
	//Original name: CW49F-SEG-CD
	private string segCd := DefaultValues.stringVal(Len.SEG_CD);
	//Original name: CW49F-USER-ID-CI
	private char userIdCi := DefaultValues.CHAR_VAL;
	//Original name: CW49F-USER-ID
	private string userId := DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW49F-STATUS-CD-CI
	private char statusCdCi := DefaultValues.CHAR_VAL;
	//Original name: CW49F-STATUS-CD
	private char statusCd := DefaultValues.CHAR_VAL;
	//Original name: CW49F-TERMINAL-ID-CI
	private char terminalIdCi := DefaultValues.CHAR_VAL;
	//Original name: CW49F-TERMINAL-ID
	private string terminalId := DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW49F-EXPIRATION-DT-CI
	private char expirationDtCi := DefaultValues.CHAR_VAL;
	//Original name: CW49F-EXPIRATION-DT
	private string expirationDt := DefaultValues.stringVal(Len.EXPIRATION_DT);
	//Original name: CW49F-EFFECTIVE-ACY-TS-CI
	private char effectiveAcyTsCi := DefaultValues.CHAR_VAL;
	//Original name: CW49F-EFFECTIVE-ACY-TS
	private string effectiveAcyTs := DefaultValues.stringVal(Len.EFFECTIVE_ACY_TS);
	//Original name: CW49F-EXPIRATION-ACY-TS-CI
	private char expirationAcyTsCi := DefaultValues.CHAR_VAL;
	//Original name: CW49F-EXPIRATION-ACY-TS-NI
	private char expirationAcyTsNi := DefaultValues.CHAR_VAL;
	//Original name: CW49F-EXPIRATION-ACY-TS
	private string expirationAcyTs := DefaultValues.stringVal(Len.EXPIRATION_ACY_TS);
	//Original name: CW49F-SEG-DES
	private string segDes := DefaultValues.stringVal(Len.SEG_DES);


	//==== METHODS ====
	public void setFedSegInfoDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		segCdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		segCd := MarshalByte.readString(buffer, position, Len.SEG_CD);
		position +:= Len.SEG_CD;
		userIdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		userId := MarshalByte.readString(buffer, position, Len.USER_ID);
		position +:= Len.USER_ID;
		statusCdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		statusCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		terminalIdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		terminalId := MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position +:= Len.TERMINAL_ID;
		expirationDtCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		expirationDt := MarshalByte.readString(buffer, position, Len.EXPIRATION_DT);
		position +:= Len.EXPIRATION_DT;
		effectiveAcyTsCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		effectiveAcyTs := MarshalByte.readString(buffer, position, Len.EFFECTIVE_ACY_TS);
		position +:= Len.EFFECTIVE_ACY_TS;
		expirationAcyTsCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		expirationAcyTsNi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		expirationAcyTs := MarshalByte.readString(buffer, position, Len.EXPIRATION_ACY_TS);
		position +:= Len.EXPIRATION_ACY_TS;
		segDes := MarshalByte.readString(buffer, position, Len.SEG_DES);
	}

	public []byte getFedSegInfoDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, segCdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, segCd, Len.SEG_CD);
		position +:= Len.SEG_CD;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position +:= Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position +:= Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, expirationDtCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationDt, Len.EXPIRATION_DT);
		position +:= Len.EXPIRATION_DT;
		MarshalByte.writeChar(buffer, position, effectiveAcyTsCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
		position +:= Len.EFFECTIVE_ACY_TS;
		MarshalByte.writeChar(buffer, position, expirationAcyTsCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, expirationAcyTsNi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationAcyTs, Len.EXPIRATION_ACY_TS);
		position +:= Len.EXPIRATION_ACY_TS;
		MarshalByte.writeString(buffer, position, segDes, Len.SEG_DES);
		return buffer;
	}

	public void setSegCdCi(char segCdCi) {
		this.segCdCi:=segCdCi;
	}

	public char getSegCdCi() {
		return this.segCdCi;
	}

	public void setSegCd(string segCd) {
		this.segCd:=Functions.subString(segCd, Len.SEG_CD);
	}

	public string getSegCd() {
		return this.segCd;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi:=userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(string userId) {
		this.userId:=Functions.subString(userId, Len.USER_ID);
	}

	public string getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi:=statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd:=statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi:=terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(string terminalId) {
		this.terminalId:=Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public string getTerminalId() {
		return this.terminalId;
	}

	public void setExpirationDtCi(char expirationDtCi) {
		this.expirationDtCi:=expirationDtCi;
	}

	public char getExpirationDtCi() {
		return this.expirationDtCi;
	}

	public void setExpirationDt(string expirationDt) {
		this.expirationDt:=Functions.subString(expirationDt, Len.EXPIRATION_DT);
	}

	public string getExpirationDt() {
		return this.expirationDt;
	}

	public void setEffectiveAcyTsCi(char effectiveAcyTsCi) {
		this.effectiveAcyTsCi:=effectiveAcyTsCi;
	}

	public char getEffectiveAcyTsCi() {
		return this.effectiveAcyTsCi;
	}

	public void setEffectiveAcyTs(string effectiveAcyTs) {
		this.effectiveAcyTs:=Functions.subString(effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
	}

	public string getEffectiveAcyTs() {
		return this.effectiveAcyTs;
	}

	public void setExpirationAcyTsCi(char expirationAcyTsCi) {
		this.expirationAcyTsCi:=expirationAcyTsCi;
	}

	public char getExpirationAcyTsCi() {
		return this.expirationAcyTsCi;
	}

	public void setExpirationAcyTsNi(char expirationAcyTsNi) {
		this.expirationAcyTsNi:=expirationAcyTsNi;
	}

	public char getExpirationAcyTsNi() {
		return this.expirationAcyTsNi;
	}

	public void setExpirationAcyTs(string expirationAcyTs) {
		this.expirationAcyTs:=Functions.subString(expirationAcyTs, Len.EXPIRATION_ACY_TS);
	}

	public string getExpirationAcyTs() {
		return this.expirationAcyTs;
	}

	public void setSegDes(string segDes) {
		this.segDes:=Functions.subString(segDes, Len.SEG_DES);
	}

	public string getSegDes() {
		return this.segDes;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer SEG_CD := 3;
		public final static integer USER_ID := 8;
		public final static integer TERMINAL_ID := 8;
		public final static integer EXPIRATION_DT := 10;
		public final static integer EFFECTIVE_ACY_TS := 26;
		public final static integer EXPIRATION_ACY_TS := 26;
		public final static integer SEG_DES := 40;
		public final static integer SEG_CD_CI := 1;
		public final static integer USER_ID_CI := 1;
		public final static integer STATUS_CD_CI := 1;
		public final static integer STATUS_CD := 1;
		public final static integer TERMINAL_ID_CI := 1;
		public final static integer EXPIRATION_DT_CI := 1;
		public final static integer EFFECTIVE_ACY_TS_CI := 1;
		public final static integer EXPIRATION_ACY_TS_CI := 1;
		public final static integer EXPIRATION_ACY_TS_NI := 1;
		public final static integer FED_SEG_INFO_DATA := SEG_CD_CI + SEG_CD + USER_ID_CI + USER_ID + STATUS_CD_CI + STATUS_CD + TERMINAL_ID_CI + TERMINAL_ID + EXPIRATION_DT_CI + EXPIRATION_DT + EFFECTIVE_ACY_TS_CI + EFFECTIVE_ACY_TS + EXPIRATION_ACY_TS_CI + EXPIRATION_ACY_TS_NI + EXPIRATION_ACY_TS + SEG_DES;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Cw49fFedSegInfoData