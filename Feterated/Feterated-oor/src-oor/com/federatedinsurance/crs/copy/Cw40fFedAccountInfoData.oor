package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW40F-FED-ACCOUNT-INFO-DATA<br>
 * Variable: CW40F-FED-ACCOUNT-INFO-DATA from copybook CAWLF040<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw40fFedAccountInfoData {

	//==== PROPERTIES ====
	/**Original name: CW40F-OFC-LOC-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char ofcLocCdCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-OFC-LOC-CD
	private string ofcLocCd := DefaultValues.stringVal(Len.OFC_LOC_CD);
	//Original name: CW40F-CIAI-AUTHNTIC-LOSS-CI
	private char ciaiAuthnticLossCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-CIAI-AUTHNTIC-LOSS
	private char ciaiAuthnticLoss := DefaultValues.CHAR_VAL;
	//Original name: CW40F-FED-ST-CD-CI
	private char fedStCdCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-FED-ST-CD
	private string fedStCd := DefaultValues.stringVal(Len.FED_ST_CD);
	//Original name: CW40F-FED-COUNTY-CD-CI
	private char fedCountyCdCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-FED-COUNTY-CD
	private string fedCountyCd := DefaultValues.stringVal(Len.FED_COUNTY_CD);
	//Original name: CW40F-FED-TOWN-CD-CI
	private char fedTownCdCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-FED-TOWN-CD
	private string fedTownCd := DefaultValues.stringVal(Len.FED_TOWN_CD);
	//Original name: CW40F-USER-ID-CI
	private char userIdCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-USER-ID
	private string userId := DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW40F-STATUS-CD-CI
	private char statusCdCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-STATUS-CD
	private char statusCd := DefaultValues.CHAR_VAL;
	//Original name: CW40F-TERMINAL-ID-CI
	private char terminalIdCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-TERMINAL-ID
	private string terminalId := DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW40F-EXPIRATION-DT-CI
	private char expirationDtCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-EXPIRATION-DT
	private string expirationDt := DefaultValues.stringVal(Len.EXPIRATION_DT);
	//Original name: CW40F-EFFECTIVE-ACY-TS-CI
	private char effectiveAcyTsCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-EFFECTIVE-ACY-TS
	private string effectiveAcyTs := DefaultValues.stringVal(Len.EFFECTIVE_ACY_TS);
	//Original name: CW40F-EXPIRATION-ACY-TS-CI
	private char expirationAcyTsCi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-EXPIRATION-ACY-TS-NI
	private char expirationAcyTsNi := DefaultValues.CHAR_VAL;
	//Original name: CW40F-EXPIRATION-ACY-TS
	private string expirationAcyTs := DefaultValues.stringVal(Len.EXPIRATION_ACY_TS);


	//==== METHODS ====
	public void setFedAccountInfoDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		ofcLocCdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		ofcLocCd := MarshalByte.readString(buffer, position, Len.OFC_LOC_CD);
		position +:= Len.OFC_LOC_CD;
		ciaiAuthnticLossCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		ciaiAuthnticLoss := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		fedStCdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		fedStCd := MarshalByte.readString(buffer, position, Len.FED_ST_CD);
		position +:= Len.FED_ST_CD;
		fedCountyCdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		fedCountyCd := MarshalByte.readString(buffer, position, Len.FED_COUNTY_CD);
		position +:= Len.FED_COUNTY_CD;
		fedTownCdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		fedTownCd := MarshalByte.readString(buffer, position, Len.FED_TOWN_CD);
		position +:= Len.FED_TOWN_CD;
		userIdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		userId := MarshalByte.readString(buffer, position, Len.USER_ID);
		position +:= Len.USER_ID;
		statusCdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		statusCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		terminalIdCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		terminalId := MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position +:= Len.TERMINAL_ID;
		expirationDtCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		expirationDt := MarshalByte.readString(buffer, position, Len.EXPIRATION_DT);
		position +:= Len.EXPIRATION_DT;
		effectiveAcyTsCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		effectiveAcyTs := MarshalByte.readString(buffer, position, Len.EFFECTIVE_ACY_TS);
		position +:= Len.EFFECTIVE_ACY_TS;
		expirationAcyTsCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		expirationAcyTsNi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		expirationAcyTs := MarshalByte.readString(buffer, position, Len.EXPIRATION_ACY_TS);
	}

	public []byte getFedAccountInfoDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, ofcLocCdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ofcLocCd, Len.OFC_LOC_CD);
		position +:= Len.OFC_LOC_CD;
		MarshalByte.writeChar(buffer, position, ciaiAuthnticLossCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciaiAuthnticLoss);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, fedStCdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, fedStCd, Len.FED_ST_CD);
		position +:= Len.FED_ST_CD;
		MarshalByte.writeChar(buffer, position, fedCountyCdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, fedCountyCd, Len.FED_COUNTY_CD);
		position +:= Len.FED_COUNTY_CD;
		MarshalByte.writeChar(buffer, position, fedTownCdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, fedTownCd, Len.FED_TOWN_CD);
		position +:= Len.FED_TOWN_CD;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position +:= Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position +:= Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, expirationDtCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationDt, Len.EXPIRATION_DT);
		position +:= Len.EXPIRATION_DT;
		MarshalByte.writeChar(buffer, position, effectiveAcyTsCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
		position +:= Len.EFFECTIVE_ACY_TS;
		MarshalByte.writeChar(buffer, position, expirationAcyTsCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, expirationAcyTsNi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationAcyTs, Len.EXPIRATION_ACY_TS);
		return buffer;
	}

	public void setOfcLocCdCi(char ofcLocCdCi) {
		this.ofcLocCdCi:=ofcLocCdCi;
	}

	public char getOfcLocCdCi() {
		return this.ofcLocCdCi;
	}

	public void setOfcLocCd(string ofcLocCd) {
		this.ofcLocCd:=Functions.subString(ofcLocCd, Len.OFC_LOC_CD);
	}

	public string getOfcLocCd() {
		return this.ofcLocCd;
	}

	public void setCiaiAuthnticLossCi(char ciaiAuthnticLossCi) {
		this.ciaiAuthnticLossCi:=ciaiAuthnticLossCi;
	}

	public char getCiaiAuthnticLossCi() {
		return this.ciaiAuthnticLossCi;
	}

	public void setCiaiAuthnticLoss(char ciaiAuthnticLoss) {
		this.ciaiAuthnticLoss:=ciaiAuthnticLoss;
	}

	public char getCiaiAuthnticLoss() {
		return this.ciaiAuthnticLoss;
	}

	public void setFedStCdCi(char fedStCdCi) {
		this.fedStCdCi:=fedStCdCi;
	}

	public char getFedStCdCi() {
		return this.fedStCdCi;
	}

	public void setFedStCd(string fedStCd) {
		this.fedStCd:=Functions.subString(fedStCd, Len.FED_ST_CD);
	}

	public string getFedStCd() {
		return this.fedStCd;
	}

	public void setFedCountyCdCi(char fedCountyCdCi) {
		this.fedCountyCdCi:=fedCountyCdCi;
	}

	public char getFedCountyCdCi() {
		return this.fedCountyCdCi;
	}

	public void setFedCountyCd(string fedCountyCd) {
		this.fedCountyCd:=Functions.subString(fedCountyCd, Len.FED_COUNTY_CD);
	}

	public string getFedCountyCd() {
		return this.fedCountyCd;
	}

	public void setFedTownCdCi(char fedTownCdCi) {
		this.fedTownCdCi:=fedTownCdCi;
	}

	public char getFedTownCdCi() {
		return this.fedTownCdCi;
	}

	public void setFedTownCd(string fedTownCd) {
		this.fedTownCd:=Functions.subString(fedTownCd, Len.FED_TOWN_CD);
	}

	public string getFedTownCd() {
		return this.fedTownCd;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi:=userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(string userId) {
		this.userId:=Functions.subString(userId, Len.USER_ID);
	}

	public string getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi:=statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd:=statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi:=terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(string terminalId) {
		this.terminalId:=Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public string getTerminalId() {
		return this.terminalId;
	}

	public void setExpirationDtCi(char expirationDtCi) {
		this.expirationDtCi:=expirationDtCi;
	}

	public char getExpirationDtCi() {
		return this.expirationDtCi;
	}

	public void setExpirationDt(string expirationDt) {
		this.expirationDt:=Functions.subString(expirationDt, Len.EXPIRATION_DT);
	}

	public string getExpirationDt() {
		return this.expirationDt;
	}

	public void setEffectiveAcyTsCi(char effectiveAcyTsCi) {
		this.effectiveAcyTsCi:=effectiveAcyTsCi;
	}

	public char getEffectiveAcyTsCi() {
		return this.effectiveAcyTsCi;
	}

	public void setEffectiveAcyTs(string effectiveAcyTs) {
		this.effectiveAcyTs:=Functions.subString(effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
	}

	public string getEffectiveAcyTs() {
		return this.effectiveAcyTs;
	}

	public void setExpirationAcyTsCi(char expirationAcyTsCi) {
		this.expirationAcyTsCi:=expirationAcyTsCi;
	}

	public char getExpirationAcyTsCi() {
		return this.expirationAcyTsCi;
	}

	public void setExpirationAcyTsNi(char expirationAcyTsNi) {
		this.expirationAcyTsNi:=expirationAcyTsNi;
	}

	public char getExpirationAcyTsNi() {
		return this.expirationAcyTsNi;
	}

	public void setExpirationAcyTs(string expirationAcyTs) {
		this.expirationAcyTs:=Functions.subString(expirationAcyTs, Len.EXPIRATION_ACY_TS);
	}

	public string getExpirationAcyTs() {
		return this.expirationAcyTs;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer OFC_LOC_CD := 2;
		public final static integer FED_ST_CD := 3;
		public final static integer FED_COUNTY_CD := 3;
		public final static integer FED_TOWN_CD := 4;
		public final static integer USER_ID := 8;
		public final static integer TERMINAL_ID := 8;
		public final static integer EXPIRATION_DT := 10;
		public final static integer EFFECTIVE_ACY_TS := 26;
		public final static integer EXPIRATION_ACY_TS := 26;
		public final static integer OFC_LOC_CD_CI := 1;
		public final static integer CIAI_AUTHNTIC_LOSS_CI := 1;
		public final static integer CIAI_AUTHNTIC_LOSS := 1;
		public final static integer FED_ST_CD_CI := 1;
		public final static integer FED_COUNTY_CD_CI := 1;
		public final static integer FED_TOWN_CD_CI := 1;
		public final static integer USER_ID_CI := 1;
		public final static integer STATUS_CD_CI := 1;
		public final static integer STATUS_CD := 1;
		public final static integer TERMINAL_ID_CI := 1;
		public final static integer EXPIRATION_DT_CI := 1;
		public final static integer EFFECTIVE_ACY_TS_CI := 1;
		public final static integer EXPIRATION_ACY_TS_CI := 1;
		public final static integer EXPIRATION_ACY_TS_NI := 1;
		public final static integer FED_ACCOUNT_INFO_DATA := OFC_LOC_CD_CI + OFC_LOC_CD + CIAI_AUTHNTIC_LOSS_CI + CIAI_AUTHNTIC_LOSS + FED_ST_CD_CI + FED_ST_CD + FED_COUNTY_CD_CI + FED_COUNTY_CD + FED_TOWN_CD_CI + FED_TOWN_CD + USER_ID_CI + USER_ID + STATUS_CD_CI + STATUS_CD + TERMINAL_ID_CI + TERMINAL_ID + EXPIRATION_DT_CI + EXPIRATION_DT + EFFECTIVE_ACY_TS_CI + EFFECTIVE_ACY_TS + EXPIRATION_ACY_TS_CI + EXPIRATION_ACY_TS_NI + EXPIRATION_ACY_TS;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Cw40fFedAccountInfoData