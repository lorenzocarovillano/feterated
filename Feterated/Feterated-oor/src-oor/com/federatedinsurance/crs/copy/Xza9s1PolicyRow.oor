package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZA9S1-POLICY-ROW<br>
 * Variable: XZA9S1-POLICY-ROW from copybook XZ0A90S1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xza9s1PolicyRow {

	//==== PROPERTIES ====
	//Original name: XZA9S1-POLICY-ID
	private string policyId := DefaultValues.stringVal(Len.POLICY_ID);
	//Original name: XZA9S1-POL-NBR
	private string polNbr := DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZA9S1-OGN-EFF-DT
	private string ognEffDt := DefaultValues.stringVal(Len.OGN_EFF_DT);
	//Original name: XZA9S1-PLN-EXP-DT
	private string plnExpDt := DefaultValues.stringVal(Len.PLN_EXP_DT);
	//Original name: XZA9S1-QUOTE-SEQUENCE-NBR
	private integer quoteSequenceNbr := DefaultValues.INT_VAL;
	//Original name: XZA9S1-ACT-NBR
	private string actNbr := DefaultValues.stringVal(Len.ACT_NBR);
	//Original name: XZA9S1-ACT-NBR-FMT
	private string actNbrFmt := DefaultValues.stringVal(Len.ACT_NBR_FMT);
	//Original name: XZA9S1-PRI-RSK-ST-ABB
	private string priRskStAbb := DefaultValues.stringVal(Len.PRI_RSK_ST_ABB);
	//Original name: XZA9S1-PRI-RSK-ST-DES
	private string priRskStDes := DefaultValues.stringVal(Len.PRI_RSK_ST_DES);
	//Original name: XZA9S1-LOB-CD
	private string lobCd := DefaultValues.stringVal(Len.LOB_CD);
	//Original name: XZA9S1-LOB-DES
	private string lobDes := DefaultValues.stringVal(Len.LOB_DES);
	//Original name: XZA9S1-TOB-CD
	private string tobCd := DefaultValues.stringVal(Len.TOB_CD);
	//Original name: XZA9S1-TOB-DES
	private string tobDes := DefaultValues.stringVal(Len.TOB_DES);
	//Original name: XZA9S1-SEG-CD
	private string segCd := DefaultValues.stringVal(Len.SEG_CD);
	//Original name: XZA9S1-SEG-DES
	private string segDes := DefaultValues.stringVal(Len.SEG_DES);
	//Original name: XZA9S1-LGE-ACT-IND
	private char lgeActInd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-STATUS-CD
	private char statusCd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-PND-TRS-IND
	private char curPndTrsInd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-STA-CD
	private char curStaCd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-STA-DES
	private string curStaDes := DefaultValues.stringVal(Len.CUR_STA_DES);
	//Original name: XZA9S1-CUR-TRS-TYP-CD
	private char curTrsTypCd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-TRS-TYP-DES
	private string curTrsTypDes := DefaultValues.stringVal(Len.CUR_TRS_TYP_DES);
	//Original name: XZA9S1-COVERAGE-PARTS
	private string coverageParts := DefaultValues.stringVal(Len.COVERAGE_PARTS);
	//Original name: XZA9S1-RLT-TYP-CD
	private string rltTypCd := DefaultValues.stringVal(Len.RLT_TYP_CD);
	//Original name: XZA9S1-MNL-POL-IND
	private char mnlPolInd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-OGN-CD
	private char curOgnCd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-OGN-DES
	private string curOgnDes := DefaultValues.stringVal(Len.CUR_OGN_DES);
	//Original name: XZA9S1-BOND-IND
	private char bondInd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-BRANCH-NBR
	private string branchNbr := DefaultValues.stringVal(Len.BRANCH_NBR);
	//Original name: XZA9S1-BRANCH-DESC
	private string branchDesc := DefaultValues.stringVal(Len.BRANCH_DESC);
	//Original name: XZA9S1-WRT-PREM-AMT
	private long wrtPremAmt := DefaultValues.LONG_VAL;
	//Original name: XZA9S1-POL-ACT-IND
	private char polActInd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-PND-CNC-IND
	private char pndCncInd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-SCH-CNC-DT
	private string schCncDt := DefaultValues.stringVal(Len.SCH_CNC_DT);
	//Original name: XZA9S1-NOT-TYP-CD
	private string notTypCd := DefaultValues.stringVal(Len.NOT_TYP_CD);
	//Original name: XZA9S1-NOT-TYP-DES
	private string notTypDes := DefaultValues.stringVal(Len.NOT_TYP_DES);
	//Original name: XZA9S1-NOT-EMP-ID
	private string notEmpId := DefaultValues.stringVal(Len.NOT_EMP_ID);
	//Original name: XZA9S1-NOT-EMP-NM
	private string notEmpNm := DefaultValues.stringVal(Len.NOT_EMP_NM);
	//Original name: XZA9S1-NOT-PRC-DT
	private string notPrcDt := DefaultValues.stringVal(Len.NOT_PRC_DT);
	//Original name: XZA9S1-TMN-IND
	private char tmnInd := DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-TMN-EMP-ID
	private string tmnEmpId := DefaultValues.stringVal(Len.TMN_EMP_ID);
	//Original name: XZA9S1-TMN-EMP-NM
	private string tmnEmpNm := DefaultValues.stringVal(Len.TMN_EMP_NM);
	//Original name: XZA9S1-TMN-PRC-DT
	private string tmnPrcDt := DefaultValues.stringVal(Len.TMN_PRC_DT);


	//==== METHODS ====
	public string getRowFormatted() {
		return MarshalByteExt.bufferToStr(getRowBytes());
	}

	public []byte getRowBytes() {
		[]byte buffer := new [Len.ROW]byte;
		return getRowBytes(buffer, 1);
	}

	public void setRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		policyId := MarshalByte.readString(buffer, position, Len.POLICY_ID);
		position +:= Len.POLICY_ID;
		polNbr := MarshalByte.readString(buffer, position, Len.POL_NBR);
		position +:= Len.POL_NBR;
		ognEffDt := MarshalByte.readString(buffer, position, Len.OGN_EFF_DT);
		position +:= Len.OGN_EFF_DT;
		plnExpDt := MarshalByte.readString(buffer, position, Len.PLN_EXP_DT);
		position +:= Len.PLN_EXP_DT;
		quoteSequenceNbr := MarshalByte.readInt(buffer, position, Len.QUOTE_SEQUENCE_NBR);
		position +:= Len.QUOTE_SEQUENCE_NBR;
		actNbr := MarshalByte.readString(buffer, position, Len.ACT_NBR);
		position +:= Len.ACT_NBR;
		actNbrFmt := MarshalByte.readString(buffer, position, Len.ACT_NBR_FMT);
		position +:= Len.ACT_NBR_FMT;
		priRskStAbb := MarshalByte.readString(buffer, position, Len.PRI_RSK_ST_ABB);
		position +:= Len.PRI_RSK_ST_ABB;
		priRskStDes := MarshalByte.readString(buffer, position, Len.PRI_RSK_ST_DES);
		position +:= Len.PRI_RSK_ST_DES;
		lobCd := MarshalByte.readString(buffer, position, Len.LOB_CD);
		position +:= Len.LOB_CD;
		lobDes := MarshalByte.readString(buffer, position, Len.LOB_DES);
		position +:= Len.LOB_DES;
		tobCd := MarshalByte.readString(buffer, position, Len.TOB_CD);
		position +:= Len.TOB_CD;
		tobDes := MarshalByte.readString(buffer, position, Len.TOB_DES);
		position +:= Len.TOB_DES;
		segCd := MarshalByte.readString(buffer, position, Len.SEG_CD);
		position +:= Len.SEG_CD;
		segDes := MarshalByte.readString(buffer, position, Len.SEG_DES);
		position +:= Len.SEG_DES;
		lgeActInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		statusCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		curPndTrsInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		curStaCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		curStaDes := MarshalByte.readString(buffer, position, Len.CUR_STA_DES);
		position +:= Len.CUR_STA_DES;
		curTrsTypCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		curTrsTypDes := MarshalByte.readString(buffer, position, Len.CUR_TRS_TYP_DES);
		position +:= Len.CUR_TRS_TYP_DES;
		coverageParts := MarshalByte.readString(buffer, position, Len.COVERAGE_PARTS);
		position +:= Len.COVERAGE_PARTS;
		rltTypCd := MarshalByte.readString(buffer, position, Len.RLT_TYP_CD);
		position +:= Len.RLT_TYP_CD;
		mnlPolInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		curOgnCd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		curOgnDes := MarshalByte.readString(buffer, position, Len.CUR_OGN_DES);
		position +:= Len.CUR_OGN_DES;
		bondInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		branchNbr := MarshalByte.readString(buffer, position, Len.BRANCH_NBR);
		position +:= Len.BRANCH_NBR;
		branchDesc := MarshalByte.readString(buffer, position, Len.BRANCH_DESC);
		position +:= Len.BRANCH_DESC;
		wrtPremAmt := MarshalByte.readLong(buffer, position, Len.WRT_PREM_AMT);
		position +:= Len.WRT_PREM_AMT;
		polActInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		pndCncInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		schCncDt := MarshalByte.readString(buffer, position, Len.SCH_CNC_DT);
		position +:= Len.SCH_CNC_DT;
		notTypCd := MarshalByte.readString(buffer, position, Len.NOT_TYP_CD);
		position +:= Len.NOT_TYP_CD;
		notTypDes := MarshalByte.readString(buffer, position, Len.NOT_TYP_DES);
		position +:= Len.NOT_TYP_DES;
		notEmpId := MarshalByte.readString(buffer, position, Len.NOT_EMP_ID);
		position +:= Len.NOT_EMP_ID;
		notEmpNm := MarshalByte.readString(buffer, position, Len.NOT_EMP_NM);
		position +:= Len.NOT_EMP_NM;
		notPrcDt := MarshalByte.readString(buffer, position, Len.NOT_PRC_DT);
		position +:= Len.NOT_PRC_DT;
		tmnInd := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		tmnEmpId := MarshalByte.readString(buffer, position, Len.TMN_EMP_ID);
		position +:= Len.TMN_EMP_ID;
		tmnEmpNm := MarshalByte.readString(buffer, position, Len.TMN_EMP_NM);
		position +:= Len.TMN_EMP_NM;
		tmnPrcDt := MarshalByte.readString(buffer, position, Len.TMN_PRC_DT);
	}

	public []byte getRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, policyId, Len.POLICY_ID);
		position +:= Len.POLICY_ID;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position +:= Len.POL_NBR;
		MarshalByte.writeString(buffer, position, ognEffDt, Len.OGN_EFF_DT);
		position +:= Len.OGN_EFF_DT;
		MarshalByte.writeString(buffer, position, plnExpDt, Len.PLN_EXP_DT);
		position +:= Len.PLN_EXP_DT;
		MarshalByte.writeInt(buffer, position, quoteSequenceNbr, Len.QUOTE_SEQUENCE_NBR);
		position +:= Len.QUOTE_SEQUENCE_NBR;
		MarshalByte.writeString(buffer, position, actNbr, Len.ACT_NBR);
		position +:= Len.ACT_NBR;
		MarshalByte.writeString(buffer, position, actNbrFmt, Len.ACT_NBR_FMT);
		position +:= Len.ACT_NBR_FMT;
		MarshalByte.writeString(buffer, position, priRskStAbb, Len.PRI_RSK_ST_ABB);
		position +:= Len.PRI_RSK_ST_ABB;
		MarshalByte.writeString(buffer, position, priRskStDes, Len.PRI_RSK_ST_DES);
		position +:= Len.PRI_RSK_ST_DES;
		MarshalByte.writeString(buffer, position, lobCd, Len.LOB_CD);
		position +:= Len.LOB_CD;
		MarshalByte.writeString(buffer, position, lobDes, Len.LOB_DES);
		position +:= Len.LOB_DES;
		MarshalByte.writeString(buffer, position, tobCd, Len.TOB_CD);
		position +:= Len.TOB_CD;
		MarshalByte.writeString(buffer, position, tobDes, Len.TOB_DES);
		position +:= Len.TOB_DES;
		MarshalByte.writeString(buffer, position, segCd, Len.SEG_CD);
		position +:= Len.SEG_CD;
		MarshalByte.writeString(buffer, position, segDes, Len.SEG_DES);
		position +:= Len.SEG_DES;
		MarshalByte.writeChar(buffer, position, lgeActInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, curPndTrsInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, curStaCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, curStaDes, Len.CUR_STA_DES);
		position +:= Len.CUR_STA_DES;
		MarshalByte.writeChar(buffer, position, curTrsTypCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, curTrsTypDes, Len.CUR_TRS_TYP_DES);
		position +:= Len.CUR_TRS_TYP_DES;
		MarshalByte.writeString(buffer, position, coverageParts, Len.COVERAGE_PARTS);
		position +:= Len.COVERAGE_PARTS;
		MarshalByte.writeString(buffer, position, rltTypCd, Len.RLT_TYP_CD);
		position +:= Len.RLT_TYP_CD;
		MarshalByte.writeChar(buffer, position, mnlPolInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, curOgnCd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, curOgnDes, Len.CUR_OGN_DES);
		position +:= Len.CUR_OGN_DES;
		MarshalByte.writeChar(buffer, position, bondInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, branchNbr, Len.BRANCH_NBR);
		position +:= Len.BRANCH_NBR;
		MarshalByte.writeString(buffer, position, branchDesc, Len.BRANCH_DESC);
		position +:= Len.BRANCH_DESC;
		MarshalByte.writeLong(buffer, position, wrtPremAmt, Len.WRT_PREM_AMT);
		position +:= Len.WRT_PREM_AMT;
		MarshalByte.writeChar(buffer, position, polActInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pndCncInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, schCncDt, Len.SCH_CNC_DT);
		position +:= Len.SCH_CNC_DT;
		MarshalByte.writeString(buffer, position, notTypCd, Len.NOT_TYP_CD);
		position +:= Len.NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, notTypDes, Len.NOT_TYP_DES);
		position +:= Len.NOT_TYP_DES;
		MarshalByte.writeString(buffer, position, notEmpId, Len.NOT_EMP_ID);
		position +:= Len.NOT_EMP_ID;
		MarshalByte.writeString(buffer, position, notEmpNm, Len.NOT_EMP_NM);
		position +:= Len.NOT_EMP_NM;
		MarshalByte.writeString(buffer, position, notPrcDt, Len.NOT_PRC_DT);
		position +:= Len.NOT_PRC_DT;
		MarshalByte.writeChar(buffer, position, tmnInd);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tmnEmpId, Len.TMN_EMP_ID);
		position +:= Len.TMN_EMP_ID;
		MarshalByte.writeString(buffer, position, tmnEmpNm, Len.TMN_EMP_NM);
		position +:= Len.TMN_EMP_NM;
		MarshalByte.writeString(buffer, position, tmnPrcDt, Len.TMN_PRC_DT);
		return buffer;
	}

	public void setPolicyId(string policyId) {
		this.policyId:=Functions.subString(policyId, Len.POLICY_ID);
	}

	public string getPolicyId() {
		return this.policyId;
	}

	public void setPolNbr(string polNbr) {
		this.polNbr:=Functions.subString(polNbr, Len.POL_NBR);
	}

	public string getPolNbr() {
		return this.polNbr;
	}

	public string getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	public void setOgnEffDt(string ognEffDt) {
		this.ognEffDt:=Functions.subString(ognEffDt, Len.OGN_EFF_DT);
	}

	public string getOgnEffDt() {
		return this.ognEffDt;
	}

	public string getOgnEffDtFormatted() {
		return Functions.padBlanks(getOgnEffDt(), Len.OGN_EFF_DT);
	}

	public void setPlnExpDt(string plnExpDt) {
		this.plnExpDt:=Functions.subString(plnExpDt, Len.PLN_EXP_DT);
	}

	public string getPlnExpDt() {
		return this.plnExpDt;
	}

	public string getPlnExpDtFormatted() {
		return Functions.padBlanks(getPlnExpDt(), Len.PLN_EXP_DT);
	}

	public void setQuoteSequenceNbr(integer quoteSequenceNbr) {
		this.quoteSequenceNbr:=quoteSequenceNbr;
	}

	public integer getQuoteSequenceNbr() {
		return this.quoteSequenceNbr;
	}

	public void setActNbr(string actNbr) {
		this.actNbr:=Functions.subString(actNbr, Len.ACT_NBR);
	}

	public string getActNbr() {
		return this.actNbr;
	}

	public void setActNbrFmt(string actNbrFmt) {
		this.actNbrFmt:=Functions.subString(actNbrFmt, Len.ACT_NBR_FMT);
	}

	public string getActNbrFmt() {
		return this.actNbrFmt;
	}

	public void setPriRskStAbb(string priRskStAbb) {
		this.priRskStAbb:=Functions.subString(priRskStAbb, Len.PRI_RSK_ST_ABB);
	}

	public string getPriRskStAbb() {
		return this.priRskStAbb;
	}

	public void setPriRskStDes(string priRskStDes) {
		this.priRskStDes:=Functions.subString(priRskStDes, Len.PRI_RSK_ST_DES);
	}

	public string getPriRskStDes() {
		return this.priRskStDes;
	}

	public void setLobCd(string lobCd) {
		this.lobCd:=Functions.subString(lobCd, Len.LOB_CD);
	}

	public string getLobCd() {
		return this.lobCd;
	}

	public void setLobDes(string lobDes) {
		this.lobDes:=Functions.subString(lobDes, Len.LOB_DES);
	}

	public string getLobDes() {
		return this.lobDes;
	}

	public void setTobCd(string tobCd) {
		this.tobCd:=Functions.subString(tobCd, Len.TOB_CD);
	}

	public string getTobCd() {
		return this.tobCd;
	}

	public void setTobDes(string tobDes) {
		this.tobDes:=Functions.subString(tobDes, Len.TOB_DES);
	}

	public string getTobDes() {
		return this.tobDes;
	}

	public void setSegCd(string segCd) {
		this.segCd:=Functions.subString(segCd, Len.SEG_CD);
	}

	public string getSegCd() {
		return this.segCd;
	}

	public void setSegDes(string segDes) {
		this.segDes:=Functions.subString(segDes, Len.SEG_DES);
	}

	public string getSegDes() {
		return this.segDes;
	}

	public void setLgeActInd(char lgeActInd) {
		this.lgeActInd:=lgeActInd;
	}

	public char getLgeActInd() {
		return this.lgeActInd;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd:=statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setCurPndTrsInd(char curPndTrsInd) {
		this.curPndTrsInd:=curPndTrsInd;
	}

	public char getCurPndTrsInd() {
		return this.curPndTrsInd;
	}

	public void setCurStaCd(char curStaCd) {
		this.curStaCd:=curStaCd;
	}

	public char getCurStaCd() {
		return this.curStaCd;
	}

	public void setCurStaDes(string curStaDes) {
		this.curStaDes:=Functions.subString(curStaDes, Len.CUR_STA_DES);
	}

	public string getCurStaDes() {
		return this.curStaDes;
	}

	public void setCurTrsTypCd(char curTrsTypCd) {
		this.curTrsTypCd:=curTrsTypCd;
	}

	public char getCurTrsTypCd() {
		return this.curTrsTypCd;
	}

	public void setCurTrsTypDes(string curTrsTypDes) {
		this.curTrsTypDes:=Functions.subString(curTrsTypDes, Len.CUR_TRS_TYP_DES);
	}

	public string getCurTrsTypDes() {
		return this.curTrsTypDes;
	}

	public void setCoverageParts(string coverageParts) {
		this.coverageParts:=Functions.subString(coverageParts, Len.COVERAGE_PARTS);
	}

	public string getCoverageParts() {
		return this.coverageParts;
	}

	public void setRltTypCd(string rltTypCd) {
		this.rltTypCd:=Functions.subString(rltTypCd, Len.RLT_TYP_CD);
	}

	public string getRltTypCd() {
		return this.rltTypCd;
	}

	public void setMnlPolInd(char mnlPolInd) {
		this.mnlPolInd:=mnlPolInd;
	}

	public char getMnlPolInd() {
		return this.mnlPolInd;
	}

	public void setCurOgnCd(char curOgnCd) {
		this.curOgnCd:=curOgnCd;
	}

	public char getCurOgnCd() {
		return this.curOgnCd;
	}

	public void setCurOgnDes(string curOgnDes) {
		this.curOgnDes:=Functions.subString(curOgnDes, Len.CUR_OGN_DES);
	}

	public string getCurOgnDes() {
		return this.curOgnDes;
	}

	public void setBondInd(char bondInd) {
		this.bondInd:=bondInd;
	}

	public char getBondInd() {
		return this.bondInd;
	}

	public void setBranchNbr(string branchNbr) {
		this.branchNbr:=Functions.subString(branchNbr, Len.BRANCH_NBR);
	}

	public string getBranchNbr() {
		return this.branchNbr;
	}

	public void setBranchDesc(string branchDesc) {
		this.branchDesc:=Functions.subString(branchDesc, Len.BRANCH_DESC);
	}

	public string getBranchDesc() {
		return this.branchDesc;
	}

	public void setWrtPremAmt(long wrtPremAmt) {
		this.wrtPremAmt:=wrtPremAmt;
	}

	public long getWrtPremAmt() {
		return this.wrtPremAmt;
	}

	public void setPolActInd(char polActInd) {
		this.polActInd:=polActInd;
	}

	public char getPolActInd() {
		return this.polActInd;
	}

	public void setPndCncInd(char pndCncInd) {
		this.pndCncInd:=pndCncInd;
	}

	public char getPndCncInd() {
		return this.pndCncInd;
	}

	public void setSchCncDt(string schCncDt) {
		this.schCncDt:=Functions.subString(schCncDt, Len.SCH_CNC_DT);
	}

	public string getSchCncDt() {
		return this.schCncDt;
	}

	public void setNotTypCd(string notTypCd) {
		this.notTypCd:=Functions.subString(notTypCd, Len.NOT_TYP_CD);
	}

	public string getNotTypCd() {
		return this.notTypCd;
	}

	public void setNotTypDes(string notTypDes) {
		this.notTypDes:=Functions.subString(notTypDes, Len.NOT_TYP_DES);
	}

	public string getNotTypDes() {
		return this.notTypDes;
	}

	public void setNotEmpId(string notEmpId) {
		this.notEmpId:=Functions.subString(notEmpId, Len.NOT_EMP_ID);
	}

	public string getNotEmpId() {
		return this.notEmpId;
	}

	public void setNotEmpNm(string notEmpNm) {
		this.notEmpNm:=Functions.subString(notEmpNm, Len.NOT_EMP_NM);
	}

	public string getNotEmpNm() {
		return this.notEmpNm;
	}

	public void setNotPrcDt(string notPrcDt) {
		this.notPrcDt:=Functions.subString(notPrcDt, Len.NOT_PRC_DT);
	}

	public string getNotPrcDt() {
		return this.notPrcDt;
	}

	public void setTmnInd(char tmnInd) {
		this.tmnInd:=tmnInd;
	}

	public char getTmnInd() {
		return this.tmnInd;
	}

	public void setTmnEmpId(string tmnEmpId) {
		this.tmnEmpId:=Functions.subString(tmnEmpId, Len.TMN_EMP_ID);
	}

	public string getTmnEmpId() {
		return this.tmnEmpId;
	}

	public void setTmnEmpNm(string tmnEmpNm) {
		this.tmnEmpNm:=Functions.subString(tmnEmpNm, Len.TMN_EMP_NM);
	}

	public string getTmnEmpNm() {
		return this.tmnEmpNm;
	}

	public void setTmnPrcDt(string tmnPrcDt) {
		this.tmnPrcDt:=Functions.subString(tmnPrcDt, Len.TMN_PRC_DT);
	}

	public string getTmnPrcDt() {
		return this.tmnPrcDt;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer POLICY_ID := 16;
		public final static integer POL_NBR := 25;
		public final static integer OGN_EFF_DT := 10;
		public final static integer PLN_EXP_DT := 10;
		public final static integer QUOTE_SEQUENCE_NBR := 5;
		public final static integer ACT_NBR := 9;
		public final static integer ACT_NBR_FMT := 9;
		public final static integer PRI_RSK_ST_ABB := 3;
		public final static integer PRI_RSK_ST_DES := 25;
		public final static integer LOB_CD := 3;
		public final static integer LOB_DES := 45;
		public final static integer TOB_CD := 3;
		public final static integer TOB_DES := 40;
		public final static integer SEG_CD := 3;
		public final static integer SEG_DES := 40;
		public final static integer LGE_ACT_IND := 1;
		public final static integer STATUS_CD := 1;
		public final static integer CUR_PND_TRS_IND := 1;
		public final static integer CUR_STA_CD := 1;
		public final static integer CUR_STA_DES := 40;
		public final static integer CUR_TRS_TYP_CD := 1;
		public final static integer CUR_TRS_TYP_DES := 35;
		public final static integer COVERAGE_PARTS := 40;
		public final static integer RLT_TYP_CD := 4;
		public final static integer MNL_POL_IND := 1;
		public final static integer CUR_OGN_CD := 1;
		public final static integer CUR_OGN_DES := 20;
		public final static integer BOND_IND := 1;
		public final static integer BRANCH_NBR := 2;
		public final static integer BRANCH_DESC := 45;
		public final static integer WRT_PREM_AMT := 11;
		public final static integer POL_ACT_IND := 1;
		public final static integer PND_CNC_IND := 1;
		public final static integer SCH_CNC_DT := 10;
		public final static integer NOT_TYP_CD := 5;
		public final static integer NOT_TYP_DES := 35;
		public final static integer NOT_EMP_ID := 6;
		public final static integer NOT_EMP_NM := 67;
		public final static integer NOT_PRC_DT := 10;
		public final static integer TMN_IND := 1;
		public final static integer TMN_EMP_ID := 6;
		public final static integer TMN_EMP_NM := 67;
		public final static integer TMN_PRC_DT := 10;
		public final static integer ROW := POLICY_ID + POL_NBR + OGN_EFF_DT + PLN_EXP_DT + QUOTE_SEQUENCE_NBR + ACT_NBR + ACT_NBR_FMT + PRI_RSK_ST_ABB + PRI_RSK_ST_DES + LOB_CD + LOB_DES + TOB_CD + TOB_DES + SEG_CD + SEG_DES + LGE_ACT_IND + STATUS_CD + CUR_PND_TRS_IND + CUR_STA_CD + CUR_STA_DES + CUR_TRS_TYP_CD + CUR_TRS_TYP_DES + COVERAGE_PARTS + RLT_TYP_CD + MNL_POL_IND + CUR_OGN_CD + CUR_OGN_DES + BOND_IND + BRANCH_NBR + BRANCH_DESC + WRT_PREM_AMT + POL_ACT_IND + PND_CNC_IND + SCH_CNC_DT + NOT_TYP_CD + NOT_TYP_DES + NOT_EMP_ID + NOT_EMP_NM + NOT_PRC_DT + TMN_IND + TMN_EMP_ID + TMN_EMP_NM + TMN_PRC_DT;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xza9s1PolicyRow