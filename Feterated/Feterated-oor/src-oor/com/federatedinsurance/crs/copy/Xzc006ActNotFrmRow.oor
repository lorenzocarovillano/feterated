package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XZC006-ACT-NOT-FRM-ROW<br>
 * Variable: XZC006-ACT-NOT-FRM-ROW from copybook XZ0C0006<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc006ActNotFrmRow {

	//==== PROPERTIES ====
	//Original name: XZC006-ACT-NOT-FRM-CSUM
	private string actNotFrmCsum := DefaultValues.stringVal(Len.ACT_NOT_FRM_CSUM);
	//Original name: XZC006-CSR-ACT-NBR-KCRE
	private string csrActNbrKcre := DefaultValues.stringVal(Len.CSR_ACT_NBR_KCRE);
	//Original name: XZC006-NOT-PRC-TS-KCRE
	private string notPrcTsKcre := DefaultValues.stringVal(Len.NOT_PRC_TS_KCRE);
	//Original name: XZC006-FRM-SEQ-NBR-KCRE
	private string frmSeqNbrKcre := DefaultValues.stringVal(Len.FRM_SEQ_NBR_KCRE);
	//Original name: XZC006-TRANS-PROCESS-DT
	private string transProcessDt := DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC006-CSR-ACT-NBR
	private string csrActNbr := DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC006-NOT-PRC-TS
	private string notPrcTs := DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC006-FRM-SEQ-NBR-SIGN
	private char frmSeqNbrSign := DefaultValues.CHAR_VAL;
	//Original name: XZC006-FRM-SEQ-NBR
	private string frmSeqNbr := DefaultValues.stringVal(Len.FRM_SEQ_NBR);
	//Original name: XZC006-CSR-ACT-NBR-CI
	private char csrActNbrCi := DefaultValues.CHAR_VAL;
	//Original name: XZC006-NOT-PRC-TS-CI
	private char notPrcTsCi := DefaultValues.CHAR_VAL;
	//Original name: XZC006-FRM-SEQ-NBR-CI
	private char frmSeqNbrCi := DefaultValues.CHAR_VAL;
	/**Original name: XZC006-FRM-NBR-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char frmNbrCi := DefaultValues.CHAR_VAL;
	//Original name: XZC006-FRM-NBR
	private string frmNbr := DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: XZC006-FRM-EDT-DT-CI
	private char frmEdtDtCi := DefaultValues.CHAR_VAL;
	//Original name: XZC006-FRM-EDT-DT
	private string frmEdtDt := DefaultValues.stringVal(Len.FRM_EDT_DT);


	//==== METHODS ====
	public void setXzc006ActNotFrmRowFormatted(string data) {
		[]byte buffer := new [Len.XZC006_ACT_NOT_FRM_ROW]byte;
		MarshalByte.writeString(buffer, 1, data, Len.XZC006_ACT_NOT_FRM_ROW);
		setXzc006ActNotFrmRowBytes(buffer, 1);
	}

	public string getXzc006ActNotFrmRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzc006ActNotFrmRowBytes());
	}

	public []byte getXzc006ActNotFrmRowBytes() {
		[]byte buffer := new [Len.XZC006_ACT_NOT_FRM_ROW]byte;
		return getXzc006ActNotFrmRowBytes(buffer, 1);
	}

	public void setXzc006ActNotFrmRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		setActNotFrmFixedBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_FIXED;
		setActNotFrmDatesBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_DATES;
		setActNotFrmKeyBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_KEY;
		setActNotFrmKeyCiBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_KEY_CI;
		setActNotFrmDataBytes(buffer, position);
	}

	public []byte getXzc006ActNotFrmRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		getActNotFrmFixedBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_FIXED;
		getActNotFrmDatesBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_DATES;
		getActNotFrmKeyBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_KEY;
		getActNotFrmKeyCiBytes(buffer, position);
		position +:= Len.ACT_NOT_FRM_KEY_CI;
		getActNotFrmDataBytes(buffer, position);
		return buffer;
	}

	public void setActNotFrmFixedBytes([]byte buffer, integer offset) {
		integer position := offset;
		actNotFrmCsum := MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_FRM_CSUM);
		position +:= Len.ACT_NOT_FRM_CSUM;
		csrActNbrKcre := MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR_KCRE);
		position +:= Len.CSR_ACT_NBR_KCRE;
		notPrcTsKcre := MarshalByte.readString(buffer, position, Len.NOT_PRC_TS_KCRE);
		position +:= Len.NOT_PRC_TS_KCRE;
		frmSeqNbrKcre := MarshalByte.readString(buffer, position, Len.FRM_SEQ_NBR_KCRE);
	}

	public []byte getActNotFrmFixedBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, actNotFrmCsum, Len.ACT_NOT_FRM_CSUM);
		position +:= Len.ACT_NOT_FRM_CSUM;
		MarshalByte.writeString(buffer, position, csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
		position +:= Len.CSR_ACT_NBR_KCRE;
		MarshalByte.writeString(buffer, position, notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
		position +:= Len.NOT_PRC_TS_KCRE;
		MarshalByte.writeString(buffer, position, frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
		return buffer;
	}

	public void initActNotFrmFixedSpaces() {
		actNotFrmCsum := "";
		csrActNbrKcre := "";
		notPrcTsKcre := "";
		frmSeqNbrKcre := "";
	}

	public void setActNotFrmCsumFormatted(string actNotFrmCsum) {
		this.actNotFrmCsum:=Trunc.toUnsignedNumeric(actNotFrmCsum, Len.ACT_NOT_FRM_CSUM);
	}

	public integer getActNotFrmCsum() {
		return NumericDisplay.asInt(this.actNotFrmCsum);
	}

	public string getActNotFrmCsumFormatted() {
		return this.actNotFrmCsum;
	}

	public string getActNotFrmCsumAsString() {
		return getActNotFrmCsumFormatted();
	}

	public void setCsrActNbrKcre(string csrActNbrKcre) {
		this.csrActNbrKcre:=Functions.subString(csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
	}

	public string getCsrActNbrKcre() {
		return this.csrActNbrKcre;
	}

	public string getCsrActNbrKcreFormatted() {
		return Functions.padBlanks(getCsrActNbrKcre(), Len.CSR_ACT_NBR_KCRE);
	}

	public void setNotPrcTsKcre(string notPrcTsKcre) {
		this.notPrcTsKcre:=Functions.subString(notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
	}

	public string getNotPrcTsKcre() {
		return this.notPrcTsKcre;
	}

	public string getNotPrcTsKcreFormatted() {
		return Functions.padBlanks(getNotPrcTsKcre(), Len.NOT_PRC_TS_KCRE);
	}

	public void setFrmSeqNbrKcre(string frmSeqNbrKcre) {
		this.frmSeqNbrKcre:=Functions.subString(frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
	}

	public string getFrmSeqNbrKcre() {
		return this.frmSeqNbrKcre;
	}

	public string getFrmSeqNbrKcreFormatted() {
		return Functions.padBlanks(getFrmSeqNbrKcre(), Len.FRM_SEQ_NBR_KCRE);
	}

	public void setActNotFrmDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		transProcessDt := MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public []byte getActNotFrmDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(string transProcessDt) {
		this.transProcessDt:=Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public string getTransProcessDt() {
		return this.transProcessDt;
	}

	public string getTransProcessDtFormatted() {
		return Functions.padBlanks(getTransProcessDt(), Len.TRANS_PROCESS_DT);
	}

	public string getActNotFrmKeyFormatted() {
		return MarshalByteExt.bufferToStr(getActNotFrmKeyBytes());
	}

	/**Original name: XZC006-ACT-NOT-FRM-KEY<br>*/
	public []byte getActNotFrmKeyBytes() {
		[]byte buffer := new [Len.ACT_NOT_FRM_KEY]byte;
		return getActNotFrmKeyBytes(buffer, 1);
	}

	public void setActNotFrmKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		csrActNbr := MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		notPrcTs := MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position +:= Len.NOT_PRC_TS;
		frmSeqNbrSign := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		frmSeqNbr := MarshalByte.readFixedString(buffer, position, Len.FRM_SEQ_NBR);
	}

	public []byte getActNotFrmKeyBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position +:= Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position +:= Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, frmSeqNbrSign);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		return buffer;
	}

	public void setCsrActNbr(string csrActNbr) {
		this.csrActNbr:=Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public string getCsrActNbr() {
		return this.csrActNbr;
	}

	public string getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(string notPrcTs) {
		this.notPrcTs:=Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public string getNotPrcTs() {
		return this.notPrcTs;
	}

	public string getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setFrmSeqNbrSign(char frmSeqNbrSign) {
		this.frmSeqNbrSign:=frmSeqNbrSign;
	}

	public void setFrmSeqNbrSignFormatted(string frmSeqNbrSign) {
		setFrmSeqNbrSign(Functions.charAt(frmSeqNbrSign, Types.CHAR_SIZE));
	}

	public char getFrmSeqNbrSign() {
		return this.frmSeqNbrSign;
	}

	public void setFrmSeqNbr(integer frmSeqNbr) {
		this.frmSeqNbr := NumericDisplay.asString(frmSeqNbr, Len.FRM_SEQ_NBR);
	}

	public void setFrmSeqNbrFormatted(string frmSeqNbr) {
		this.frmSeqNbr:=Trunc.toUnsignedNumeric(frmSeqNbr, Len.FRM_SEQ_NBR);
	}

	public integer getFrmSeqNbr() {
		return NumericDisplay.asInt(this.frmSeqNbr);
	}

	public string getFrmSeqNbrFormatted() {
		return this.frmSeqNbr;
	}

	public string getFrmSeqNbrAsString() {
		return getFrmSeqNbrFormatted();
	}

	public void setActNotFrmKeyCiBytes([]byte buffer, integer offset) {
		integer position := offset;
		csrActNbrCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		notPrcTsCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		frmSeqNbrCi := MarshalByte.readChar(buffer, position);
	}

	public []byte getActNotFrmKeyCiBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, frmSeqNbrCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi:=csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi:=notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setFrmSeqNbrCi(char frmSeqNbrCi) {
		this.frmSeqNbrCi:=frmSeqNbrCi;
	}

	public char getFrmSeqNbrCi() {
		return this.frmSeqNbrCi;
	}

	public void setActNotFrmDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		frmNbrCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		frmNbr := MarshalByte.readString(buffer, position, Len.FRM_NBR);
		position +:= Len.FRM_NBR;
		frmEdtDtCi := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		frmEdtDt := MarshalByte.readString(buffer, position, Len.FRM_EDT_DT);
	}

	public []byte getActNotFrmDataBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, frmNbrCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, frmNbr, Len.FRM_NBR);
		position +:= Len.FRM_NBR;
		MarshalByte.writeChar(buffer, position, frmEdtDtCi);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, frmEdtDt, Len.FRM_EDT_DT);
		return buffer;
	}

	public void initActNotFrmDataSpaces() {
		frmNbrCi := Types.SPACE_CHAR;
		frmNbr := "";
		frmEdtDtCi := Types.SPACE_CHAR;
		frmEdtDt := "";
	}

	public void setFrmNbrCi(char frmNbrCi) {
		this.frmNbrCi:=frmNbrCi;
	}

	public char getFrmNbrCi() {
		return this.frmNbrCi;
	}

	public void setFrmNbr(string frmNbr) {
		this.frmNbr:=Functions.subString(frmNbr, Len.FRM_NBR);
	}

	public string getFrmNbr() {
		return this.frmNbr;
	}

	public string getFrmNbrFormatted() {
		return Functions.padBlanks(getFrmNbr(), Len.FRM_NBR);
	}

	public void setFrmEdtDtCi(char frmEdtDtCi) {
		this.frmEdtDtCi:=frmEdtDtCi;
	}

	public char getFrmEdtDtCi() {
		return this.frmEdtDtCi;
	}

	public void setFrmEdtDt(string frmEdtDt) {
		this.frmEdtDt:=Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	public string getFrmEdtDt() {
		return this.frmEdtDt;
	}

	public string getFrmEdtDtFormatted() {
		return Functions.padBlanks(getFrmEdtDt(), Len.FRM_EDT_DT);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer ACT_NOT_FRM_CSUM := 9;
		public final static integer CSR_ACT_NBR_KCRE := 32;
		public final static integer NOT_PRC_TS_KCRE := 32;
		public final static integer FRM_SEQ_NBR_KCRE := 32;
		public final static integer TRANS_PROCESS_DT := 10;
		public final static integer CSR_ACT_NBR := 9;
		public final static integer NOT_PRC_TS := 26;
		public final static integer FRM_SEQ_NBR := 5;
		public final static integer FRM_NBR := 30;
		public final static integer FRM_EDT_DT := 10;
		public final static integer ACT_NOT_FRM_FIXED := ACT_NOT_FRM_CSUM + CSR_ACT_NBR_KCRE + NOT_PRC_TS_KCRE + FRM_SEQ_NBR_KCRE;
		public final static integer ACT_NOT_FRM_DATES := TRANS_PROCESS_DT;
		public final static integer FRM_SEQ_NBR_SIGN := 1;
		public final static integer ACT_NOT_FRM_KEY := CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR_SIGN + FRM_SEQ_NBR;
		public final static integer CSR_ACT_NBR_CI := 1;
		public final static integer NOT_PRC_TS_CI := 1;
		public final static integer FRM_SEQ_NBR_CI := 1;
		public final static integer ACT_NOT_FRM_KEY_CI := CSR_ACT_NBR_CI + NOT_PRC_TS_CI + FRM_SEQ_NBR_CI;
		public final static integer FRM_NBR_CI := 1;
		public final static integer FRM_EDT_DT_CI := 1;
		public final static integer ACT_NOT_FRM_DATA := FRM_NBR_CI + FRM_NBR + FRM_EDT_DT_CI + FRM_EDT_DT;
		public final static integer XZC006_ACT_NOT_FRM_ROW := ACT_NOT_FRM_FIXED + ACT_NOT_FRM_DATES + ACT_NOT_FRM_KEY + ACT_NOT_FRM_KEY_CI + ACT_NOT_FRM_DATA;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Xzc006ActNotFrmRow