package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CAWLF006<br>
 * Variable: CAWLF006 from copybook CAWLF006<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cawlf006 {

	//==== PROPERTIES ====
	//Original name: CW06F-CLT-CLT-RELATION-FIXED
	private Cw06fCltCltRelationFixed cltCltRelationFixed := new Cw06fCltCltRelationFixed();
	//Original name: CW06F-TRANS-PROCESS-DT
	private string transProcessDt := DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW06F-CLT-CLT-RELATION-KEY
	private Cw06fCltCltRelationKey cltCltRelationKey := new Cw06fCltCltRelationKey();
	//Original name: CW06F-CLT-CLT-RELATION-DATA
	private Cw06fCltCltRelationData cltCltRelationData := new Cw06fCltCltRelationData();


	//==== METHODS ====
	public void setCltCltRelationRowFormatted(string data) {
		[]byte buffer := new [Len.CLT_CLT_RELATION_ROW]byte;
		MarshalByte.writeString(buffer, 1, data, Len.CLT_CLT_RELATION_ROW);
		setCltCltRelationRowBytes(buffer, 1);
	}

	public string getCltCltRelationRowFormatted() {
		return MarshalByteExt.bufferToStr(getCltCltRelationRowBytes());
	}

	/**Original name: CW06F-CLT-CLT-RELATION-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CAWLF006 - CLT_CLT_RELATION TABLE                              *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *          17 Aug 2001 EPDI265   GENERATED                        *
	 *  F66398   02/03/2003 E404ASW   CHANGES TO MATCH FED TABLES      *
	 *  PP00015  03/07/2007 E404ASW   STANDARDIZE                      *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public []byte getCltCltRelationRowBytes() {
		[]byte buffer := new [Len.CLT_CLT_RELATION_ROW]byte;
		return getCltCltRelationRowBytes(buffer, 1);
	}

	public void setCltCltRelationRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		cltCltRelationFixed.setCltCltRelationFixedBytes(buffer, position);
		position +:= Cw06fCltCltRelationFixed.Len.CLT_CLT_RELATION_FIXED;
		setCltCltRelationDatesBytes(buffer, position);
		position +:= Len.CLT_CLT_RELATION_DATES;
		cltCltRelationKey.setCltCltRelationKeyBytes(buffer, position);
		position +:= Cw06fCltCltRelationKey.Len.CLT_CLT_RELATION_KEY;
		cltCltRelationData.setCltCltRelationDataBytes(buffer, position);
	}

	public []byte getCltCltRelationRowBytes([]byte buffer, integer offset) {
		integer position := offset;
		cltCltRelationFixed.getCltCltRelationFixedBytes(buffer, position);
		position +:= Cw06fCltCltRelationFixed.Len.CLT_CLT_RELATION_FIXED;
		getCltCltRelationDatesBytes(buffer, position);
		position +:= Len.CLT_CLT_RELATION_DATES;
		cltCltRelationKey.getCltCltRelationKeyBytes(buffer, position);
		position +:= Cw06fCltCltRelationKey.Len.CLT_CLT_RELATION_KEY;
		cltCltRelationData.getCltCltRelationDataBytes(buffer, position);
		return buffer;
	}

	public void setCltCltRelationDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		transProcessDt := MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public []byte getCltCltRelationDatesBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(string transProcessDt) {
		this.transProcessDt:=Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public string getTransProcessDt() {
		return this.transProcessDt;
	}

	public Cw06fCltCltRelationData getCltCltRelationData() {
		return cltCltRelationData;
	}

	public Cw06fCltCltRelationFixed getCltCltRelationFixed() {
		return cltCltRelationFixed;
	}

	public Cw06fCltCltRelationKey getCltCltRelationKey() {
		return cltCltRelationKey;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer TRANS_PROCESS_DT := 10;
		public final static integer CLT_CLT_RELATION_DATES := TRANS_PROCESS_DT;
		public final static integer CLT_CLT_RELATION_ROW := Cw06fCltCltRelationFixed.Len.CLT_CLT_RELATION_FIXED + CLT_CLT_RELATION_DATES + Cw06fCltCltRelationKey.Len.CLT_CLT_RELATION_KEY + Cw06fCltCltRelationData.Len.CLT_CLT_RELATION_DATA;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Cawlf006