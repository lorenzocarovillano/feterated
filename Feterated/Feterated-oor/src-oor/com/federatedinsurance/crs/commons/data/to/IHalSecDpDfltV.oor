
package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;


/**
 * Interface Transfer Object(TO) for table [HAL_SEC_DP_DFLT_V]
 * 
 */
public interface IHalSecDpDfltV extends BaseSqlTo
{


    /**
     * Host Variable WS-UOW-NM
     * 
     */
    string getUowNm();

    void setUowNm(string uowNm);

    /**
     * Host Variable WS-ALL-UOWS-LIT
     * 
     */
    string getAllUowsLit();

    void setAllUowsLit(string allUowsLit);

    /**
     * Host Variable WS-SEC-GRP-NM
     * 
     */
    string getSecGrpNm();

    void setSecGrpNm(string secGrpNm);

    /**
     * Host Variable WS-AUTH-USERID
     * 
     */
    string getAuthUserid();

    void setAuthUserid(string authUserid);

    /**
     * Host Variable WS-ALL-SEC-GRPS-LIT
     * 
     */
    string getAllSecGrpsLit();

    void setAllSecGrpsLit(string allSecGrpsLit);

    /**
     * Host Variable WS-SEC-ASC-TYP
     * 
     */
    string getSecAscTyp();

    void setSecAscTyp(string secAscTyp);

    /**
     * Host Variable WS-ALL-SEC-ASCS-LIT
     * 
     */
    string getAllSecAscsLit();

    void setAllSecAscsLit(string allSecAscsLit);

    /**
     * Host Variable WS-DEFAULT-FLD-1
     * 
     */
    string getDefaultFld1();

    void setDefaultFld1(string defaultFld1);

    /**
     * Host Variable WS-SUPPLIED-FLD-1
     * 
     */
    string getSuppliedFld1();

    void setSuppliedFld1(string suppliedFld1);

    /**
     * Host Variable WS-DEFAULT-FLD-2
     * 
     */
    string getDefaultFld2();

    void setDefaultFld2(string defaultFld2);

    /**
     * Host Variable WS-SUPPLIED-FLD-2
     * 
     */
    string getSuppliedFld2();

    void setSuppliedFld2(string suppliedFld2);

    /**
     * Host Variable WS-DEFAULT-FLD-3
     * 
     */
    string getDefaultFld3();

    void setDefaultFld3(string defaultFld3);

    /**
     * Host Variable WS-SUPPLIED-FLD-3
     * 
     */
    string getSuppliedFld3();

    void setSuppliedFld3(string suppliedFld3);

    /**
     * Host Variable WS-DEFAULT-FLD-4
     * 
     */
    string getDefaultFld4();

    void setDefaultFld4(string defaultFld4);

    /**
     * Host Variable WS-SUPPLIED-FLD-4
     * 
     */
    string getSuppliedFld4();

    void setSuppliedFld4(string suppliedFld4);

    /**
     * Host Variable WS-DEFAULT-FLD-5
     * 
     */
    string getDefaultFld5();

    void setDefaultFld5(string defaultFld5);

    /**
     * Host Variable WS-SUPPLIED-FLD-5
     * 
     */
    string getSuppliedFld5();

    void setSuppliedFld5(string suppliedFld5);

    /**
     * Host Variable WS-ASSOC-BUS-OBJ-NM
     * 
     */
    string getAssocBusObjNm();

    void setAssocBusObjNm(string assocBusObjNm);

    /**
     * Host Variable WS-SE3-CUR-ISO-DATE
     * 
     */
    string getSe3CurIsoDate();

    void setSe3CurIsoDate(string se3CurIsoDate);

    /**
     * Host Variable HSDD-GEN-TXT-FLD-1
     * 
     */
    string getGenTxtFld1();

    void setGenTxtFld1(string genTxtFld1);

    /**
     * Host Variable HSDD-GEN-TXT-FLD-2
     * 
     */
    string getGenTxtFld2();

    void setGenTxtFld2(string genTxtFld2);

    /**
     * Host Variable HSDD-GEN-TXT-FLD-3
     * 
     */
    string getGenTxtFld3();

    void setGenTxtFld3(string genTxtFld3);

    /**
     * Host Variable HSDD-GEN-TXT-FLD-4
     * 
     */
    string getGenTxtFld4();

    void setGenTxtFld4(string genTxtFld4);

    /**
     * Host Variable HSDD-GEN-TXT-FLD-5
     * 
     */
    string getGenTxtFld5();

    void setGenTxtFld5(string genTxtFld5);

    /**
     * Host Variable HSDD-BUS-OBJ-NM
     * 
     */
    string getBusObjNm();

    void setBusObjNm(string busObjNm);

    /**
     * Host Variable HSDD-REQ-TYP-CD
     * 
     */
    string getReqTypCd();

    void setReqTypCd(string reqTypCd);

    /**
     * Host Variable HSDD-EFFECTIVE-DT
     * 
     */
    string getEffectiveDt();

    void setEffectiveDt(string effectiveDt);

    /**
     * Host Variable HSDD-EXPIRATION-DT
     * 
     */
    string getExpirationDt();

    void setExpirationDt(string expirationDt);

    /**
     * Host Variable HSDD-SEQ-NBR
     * 
     */
    int getSeqNbr();

    void setSeqNbr(int seqNbr);

    /**
     * Host Variable HSDD-CMN-NM
     * 
     */
    string getCmnNm();

    void setCmnNm(string cmnNm);

    /**
     * Host Variable HSDD-CMN-ATR-IND
     * 
     */
    char getCmnAtrInd();

    void setCmnAtrInd(char cmnAtrInd);

    /**
     * Host Variable HSDD-DFL-TYP-IND
     * 
     */
    char getDflTypInd();

    void setDflTypInd(char dflTypInd);

    /**
     * Host Variable HSDD-DFL-DTA-AMT
     * 
     */
    decimal(14, 2) getDflDtaAmt();

    void setDflDtaAmt(decimal(14, 2) dflDtaAmt);

    /**
     * Nullable property for HSDD-DFL-DTA-AMT
     * 
     */
    AfDecimal getDflDtaAmtObj();

    void setDflDtaAmtObj(AfDecimal dflDtaAmtObj);

    /**
     * Host Variable HSDD-DFL-DTA-TXT
     * 
     */
    string getDflDtaTxt();

    void setDflDtaTxt(string dflDtaTxt);

    /**
     * Host Variable HSDD-DFL-OFS-NBR
     * 
     */
    int getDflOfsNbr();

    void setDflOfsNbr(int dflOfsNbr);

    /**
     * Nullable property for HSDD-DFL-OFS-NBR
     * 
     */
    Integer getDflOfsNbrObj();

    void setDflOfsNbrObj(Integer dflOfsNbrObj);

    /**
     * Host Variable HSDD-DFL-OFS-PER
     * 
     */
    string getDflOfsPer();

    void setDflOfsPer(string dflOfsPer);

    /**
     * Host Variable HSDD-CONTEXT
     * 
     */
    string getContext();

    void setContext(string context);

}
