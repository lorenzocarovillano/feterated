package com.federatedinsurance.crs;

import java.lang.Override;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.storage.KeyType;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;

import com.federatedinsurance.crs.copy.EstoInputKey;
import com.federatedinsurance.crs.copy.EstoOutput;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.ws.DfhcommareaTs020100;
import com.federatedinsurance.crs.ws.Ts020200Data;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.enums.EstoErrStoreDetailCd;
import com.federatedinsurance.crs.ws.enums.EstoErrStoreReturnCd;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;

/**Original name: TS020200<br>
 * <pre>AUTHOR.       LORNE LUND.
 * DATE-WRITTEN. 30 JUN 2005.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - RESPONSE UMT (UHRD/UDAT) ACCESS MODULE.     **
 * *                                                             **
 * * PLATFORM - I-BASE                                           **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -  RESPONSE UMT (UHDR/UDAT) ACCESS MODULE.          **
 * *                                                             **
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * --------  ---------  --------   ----------------------------**
 * * TS129     30 JUN 05  E404LJL    NEW                         **
 * ****************************************************************</pre>*/
class Ts020200 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>****************************************************************
	 * ***** *  START OF:                           BUSINESS FRAMEWORK
	 * ***** *                                      BUSINESS FRAMEWORK
	 * ***** *  GENERAL BPO WORKING-STOARGE         BUSINESS FRAMEWORK
	 * ***** *  (SPECIFIC TO ALL BPOS)              BUSINESS FRAMEWORK
	 * ****************************************************************
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca := new Sqlca();
	private ExecContext execContext := null;
	//Original name: WORKING-STORAGE
	private Ts020200Data ws := new Ts020200Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaTs020100 dfhcommarea;


	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaTs020100 dfhcommarea) {
		this.execContext := execContext;
		this.dfhcommarea := dfhcommarea;
		mainline();
		mainlineX();
		return 0;
	}

	public static Ts020200 getInstance() {
		return (Ts020200)Programs.getInstance(Ts020200.clazz);
	}

	/**Original name: 0000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  MAIN PROCESSING CONTROL                                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 1000-INITIALIZATION.                                 
		initialization();
		// COB_CODE: PERFORM 2000-READ-FUNC.                                      
		readFunc();
	}

	/**Original name: 0000-MAINLINE-X<br>*/
	private void mainlineX() {
		// COB_CODE: MOVE RESPONSE-UMT-MODULE-DATA                                
		//                                       TO CSC-DATA-BUFFER.              
		dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getResponseUmtModuleDataFormatted());
		// COB_CODE: EXEC CICS                                                    
		//               RETURN                                                   
		//           END-EXEC.                                                    
		throw new TpReturnException();
		// COB_CODE: GOBACK.                                                      
		throw new ReturnException();
	}

	/**Original name: 1000-INITIALIZATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  INITIALIZATION.                                                *
	 *                                                                 *
	 * *****************************************************************
	 *  INITIALIZE ERROR PROCESSING FIELDS</pre>*/
	private void initialization() {
		// COB_CODE: INITIALIZE ESTO-STORE-INFO                                   
		//                      ESTO-RETURN-INFO.                                 
		initEstoStoreInfo();
		initEstoReturnInfo();
			// INITIALIZE WORKING STORAGE VARIABLES                                    
		// COB_CODE: MOVE CSC-DATA-BUFFER(1:LENGTH OF RESPONSE-UMT-MODULE-DATA)   
		//                                       TO RESPONSE-UMT-MODULE-DATA.     
		ws.setResponseUmtModuleDataFormatted(dfhcommarea.getCommunicationShellCommon().getCscDataBufferFormatted()[1:Ts020200Data.Len.RESPONSE_UMT_MODULE_DATA]);
	}

	/**Original name: 2000-READ-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  READ FUNCTION: READ A SPECIFIC RESPONSE DATA RECORD            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readFunc() {
		IRowDAO iRowDAO := null;
		IRowData iRowData := null;
		ConcatUtil concatUtil := null;
		// COB_CODE: MOVE UBOC-MSG-ID            TO UDAT-ID.                      
		ws.getHalludat().setId(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM    TO UDAT-BUS-OBJ-NM.              
		ws.getHalludat().setBusObjNm(ws.getHallresp().getBusObjNm());
		// COB_CODE: MOVE HALRRESP-REC-SEQ       TO UDAT-REC-SEQ.                 
		ws.getHalludat().setRecSeqFormatted(ws.getHallresp().getRecSeqFormatted());
		// COB_CODE: EXEC CICS                                                    
		//                READ FILE  ('HALFUDAT')                                 
		//                INTO       (UDAT-COMMON)                                
		//                RIDFLD     (UDAT-KEY)                                   
		//                KEYLENGTH  (LENGTH OF UDAT-KEY)                         
		//                RESP       (WS-RESPONSE-CODE)                           
		//                RESP2      (WS-RESPONSE-CODE2)                          
		//           END-EXEC.                                                    
		iRowDAO := RowDAOFactory.getRowDAO(execContext, "HALFUDAT");
		if (iRowDAO != null) then
			iRowData := iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowData := iRowDAO.select(iRowData, KeyType.EQUAL, Halludat.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) then
				ws.getHalludat().setCommonBytes(iRowData.getData());
			endif
		endif
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE                                    
		//               WHEN DFHRESP(NORMAL)                                     
		//                                       TO RESPONSE-DATA-BUFFER          
		//               WHEN DFHRESP(NOTFND)                                     
		//                   MOVE ALL '?'        TO RESPONSE-DATA-BUFFER          
		//               WHEN OTHER                                               
		//                   GO TO 2000-READ-FUNC-X                               
		//           END-EVALUATE.                                                
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) = TpConditionType.NORMAL) then
			// COB_CODE: SET HALRRESP-REC-FOUND                               
			//                               TO TRUE                          
			ws.getHallresp().getRecFoundSw().setFound();
			// COB_CODE: SUBTRACT LENGTH OF UDAT-UOW-BUS-OBJ-NM               
			//               FROM   UDAT-UOW-BUFFER-LENGTH                    
			//               GIVING HALRRESP-BUS-OBJ-DATA-LEN                 
			ws.getHallresp().setBusObjDataLen((short)(abs(ws.getHalludat().getUowBufferLength() - Halludat.Len.UOW_BUS_OBJ_NM)));
			// COB_CODE: MOVE UDAT-UOW-BUS-OBJ-DATA                           
			//                               TO RESPONSE-DATA-BUFFER          
			ws.setResponseDataBuffer(ws.getHalludat().getUowBusObjData());
		elseif (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) = TpConditionType.NOTFND) then
			// COB_CODE: SET HALRRESP-REC-NOT-FOUND                           
			//                               TO TRUE                          
			ws.getHallresp().getRecFoundSw().setNotFound();
			// COB_CODE: MOVE 0              TO HALRRESP-BUS-OBJ-DATA-LEN     
			ws.getHallresp().setBusObjDataLen((short)0);
			// COB_CODE: MOVE ALL '?'        TO RESPONSE-DATA-BUFFER          
			ws.setResponseDataBuffer(LiteralGenerator.create("?", Ts020200Data.Len.RESPONSE_DATA_BUFFER));
		else
			// COB_CODE: SET WS-LOG-ERROR    TO TRUE                          
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR                                
			//                               TO TRUE                          
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED                                 
			//                               TO TRUE                          
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT                               
			//                               TO TRUE                          
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE                        
			//                               TO EFAL-ERR-OBJECT-NAME          
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE WS-PROGRAM-NAME                                 
			//                               TO CSC-FAILED-MODULE             
			dfhcommarea.getCommunicationShellCommon().setCscFailedModule(ws.getWsProgramName());
			// COB_CODE: MOVE '2000-READ-FUNC'                                
			//                               TO EFAL-ERR-PARAGRAPH            
			//                                  CSC-FAILED-PARAGRAPH          
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2000-READ-FUNC");
			dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("2000-READ-FUNC");
			// COB_CODE: MOVE 'READ RESP DATA UMT FAILED IN TS020200'         
			//                                  TO EFAL-ERR-COMMENT           
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ RESP DATA UMT FAILED IN TS020200");
			// COB_CODE: IF WS-RESPONSE-CODE < ZERO                           
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN       
			//           ELSE                                                 
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN       
			//           END-IF                                               
			if (ws.getWsNotSpecificMisc().getResponseCode() < 0) then
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN       
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			else
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN       
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			endif
			// COB_CODE: MOVE WS-RESPONSE-CODE                                
			//                               TO EFAL-CICS-ERR-RESP            
			//                                  CSC-EIBRESP-DISPLAY           
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			dfhcommarea.getCommunicationShellCommon().setCscEibrespDisplay(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: IF WS-RESPONSE-CODE2 < ZERO                          
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN      
			//           ELSE                                                 
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN      
			//           END-IF                                               
			if (ws.getWsNotSpecificMisc().getResponseCode2() < 0) then
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN      
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			else
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN      
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			endif
			// COB_CODE: MOVE WS-RESPONSE-CODE2                               
			//                               TO EFAL-CICS-ERR-RESP2           
			//                                  CSC-EIBRESP2-DISPLAY          
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			dfhcommarea.getCommunicationShellCommon().setCscEibresp2Display(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  'UDAT-BUS-OBJ-NM='    UDAT-BUS-OBJ-NM      ';'
			//                  'UDAT-REC-SEQ='       UDAT-REC-SEQ         ';'
			//                  DELIMITED BY SIZE                             
			//                  INTO EFAL-OBJ-DATA-KEY                        
			concatUtil := ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, {"UDAT-ID=", ws.getHalludat().getIdFormatted(), ";", "UDAT-BUS-OBJ-NM=", ws.getHalludat().getBusObjNmFormatted(), ";", "UDAT-REC-SEQ=", ws.getHalludat().getRecSeqAsString(), ";"});
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR                    
			logWarningOrError();
			// COB_CODE: GO TO 2000-READ-FUNC-X                               
			return ;
		endif
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil := null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (! (ws.getWsLogWarningOrErrorSw().isWarning() & dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk() | ws.getWsLogWarningOrErrorSw().isError() & ! dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) then
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return ;
		endif
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsProgramNameFormatted()[1:3], "HAL")) then
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		else
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		endif
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()
		case EstoDetailBuffer.EFAL_DB2_FAILED:
			// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) then
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			else
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			endif
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
		case EstoDetailBuffer.EFAL_CICS_FAILED:
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) then
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			else
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			endif
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) then
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			else
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			endif
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
		default:
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
		endswitch
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) then
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		else
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		endif
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getUbocCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getUbocCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020200", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
			//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
			//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
			//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
			//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
			//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
			//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
			//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
			//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
			// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch ws.getWsNotSpecificMisc().getResponseCode()
		case 0:
			// COB_CODE: CONTINUE
			//continue
		default:
				//* IF HALOESTO LINK ERROR THEN RECORD
				//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
		endswitch
			//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
			//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet() | dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) then
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) then
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			else
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			endif
		elseif (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) then
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		else
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		endif
			//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
			//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (! ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) then
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil := ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ", ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(), ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		endif
			//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) then
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setUbocAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setUbocAppDataBufferLength((short)EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT);
		endif
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext:=execContext;
	}

}//Ts020200