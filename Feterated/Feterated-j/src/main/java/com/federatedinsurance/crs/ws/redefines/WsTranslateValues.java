/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-TRANSLATE-VALUES<br>
 * Variable: WS-TRANSLATE-VALUES from program CIWOSDX<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsTranslateValues extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int CHAR_FLD_MAXOCCURS = 26;

	//==== CONSTRUCTORS ====
	public WsTranslateValues() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_TRANSLATE_VALUES;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, " 123 12  22455 12623 1 2 2", Len.WS_TRANSLATE_VALUES);
	}

	/**Original name: WS-TRANSLATE-CHAR<br>*/
	public char getCharFld(int charFldIdx) {
		int position = Pos.wsTranslateChar(charFldIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_TRANSLATE_VALUES = 1;
		public static final int WS_TRANSLATE_CHARACTER = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int wsTranslateChar(int idx) {
			return WS_TRANSLATE_CHARACTER + idx * Len.CHAR_FLD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CHAR_FLD = 1;
		public static final int WS_TRANSLATE_VALUES = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
