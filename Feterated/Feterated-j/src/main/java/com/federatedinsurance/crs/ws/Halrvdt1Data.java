/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.federatedinsurance.crs.ws.enums.WsLeapYearInd;
import com.federatedinsurance.crs.ws.redefines.WsDaysInMonthTab;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALRVDT1<br>
 * Generated as a class for rule WS.<br>*/
public class Halrvdt1Data {

	//==== PROPERTIES ====
	//Original name: WS-REMAINDER
	private short wsRemainder = DefaultValues.SHORT_VAL;
	//Original name: WS-DUMMY
	private int wsDummy = DefaultValues.INT_VAL;
	//Original name: WS-LEAP-YEAR-IND
	private WsLeapYearInd wsLeapYearInd = new WsLeapYearInd();
	//Original name: WS-DAYS-IN-MONTH-TAB
	private WsDaysInMonthTab wsDaysInMonthTab = new WsDaysInMonthTab();

	//==== METHODS ====
	public void setWsRemainder(short wsRemainder) {
		this.wsRemainder = wsRemainder;
	}

	public short getWsRemainder() {
		return this.wsRemainder;
	}

	public void setWsDummy(int wsDummy) {
		this.wsDummy = wsDummy;
	}

	public int getWsDummy() {
		return this.wsDummy;
	}

	public WsDaysInMonthTab getWsDaysInMonthTab() {
		return wsDaysInMonthTab;
	}

	public WsLeapYearInd getWsLeapYearInd() {
		return wsLeapYearInd;
	}
}
