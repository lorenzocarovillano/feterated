/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-04-INVALID-POLICY<br>
 * Variable: EA-04-INVALID-POLICY from program XZC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea04InvalidPolicy {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-04-INVALID-POLICY
	private String flr1 = "PGM =";
	//Original name: EA-04-PROGRAM-NAME
	private String programName = DefaultValues.stringVal(Len.PROGRAM_NAME);
	//Original name: FILLER-EA-04-INVALID-POLICY-1
	private String flr2 = " PARA =";
	//Original name: EA-04-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-04-INVALID-POLICY-2
	private String flr3 = " NO POLICY";
	//Original name: FILLER-EA-04-INVALID-POLICY-3
	private String flr4 = "FOUND.";
	//Original name: FILLER-EA-04-INVALID-POLICY-4
	private String flr5 = "POL NBR =";
	//Original name: EA-04-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);

	//==== METHODS ====
	public String getEa04InvalidPolicyFormatted() {
		return MarshalByteExt.bufferToStr(getEa04InvalidPolicyBytes());
	}

	public byte[] getEa04InvalidPolicyBytes() {
		byte[] buffer = new byte[Len.EA04_INVALID_POLICY];
		return getEa04InvalidPolicyBytes(buffer, 1);
	}

	public byte[] getEa04InvalidPolicyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, programName, Len.PROGRAM_NAME);
		position += Len.PROGRAM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setProgramName(String programName) {
		this.programName = Functions.subString(programName, Len.PROGRAM_NAME);
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int PARAGRAPH_NBR = 5;
		public static final int POL_NBR = 9;
		public static final int FLR1 = 6;
		public static final int FLR2 = 8;
		public static final int FLR3 = 11;
		public static final int FLR4 = 7;
		public static final int FLR5 = 10;
		public static final int EA04_INVALID_POLICY = PROGRAM_NAME + PARAGRAPH_NBR + POL_NBR + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
