/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-XZ0C0004-LAYOUT<br>
 * Variable: WS-XZ0C0004-LAYOUT from program XZ0F0004<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0c0004Layout extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZC004-ACT-NOT-POL-FRM-CSUM
	private String actNotPolFrmCsum = DefaultValues.stringVal(Len.ACT_NOT_POL_FRM_CSUM);
	//Original name: XZC004-CSR-ACT-NBR-KCRE
	private String csrActNbrKcre = DefaultValues.stringVal(Len.CSR_ACT_NBR_KCRE);
	//Original name: XZC004-NOT-PRC-TS-KCRE
	private String notPrcTsKcre = DefaultValues.stringVal(Len.NOT_PRC_TS_KCRE);
	//Original name: XZC004-FRM-SEQ-NBR-KCRE
	private String frmSeqNbrKcre = DefaultValues.stringVal(Len.FRM_SEQ_NBR_KCRE);
	//Original name: XZC004-POL-NBR-KCRE
	private String polNbrKcre = DefaultValues.stringVal(Len.POL_NBR_KCRE);
	//Original name: XZC004-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC004-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC004-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC004-FRM-SEQ-NBR-SIGN
	private char frmSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: XZC004-FRM-SEQ-NBR
	private String frmSeqNbr = DefaultValues.stringVal(Len.FRM_SEQ_NBR);
	//Original name: XZC004-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZC004-CSR-ACT-NBR-CI
	private char csrActNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC004-NOT-PRC-TS-CI
	private char notPrcTsCi = DefaultValues.CHAR_VAL;
	//Original name: XZC004-FRM-SEQ-NBR-CI
	private char frmSeqNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC004-POL-NBR-CI
	private char polNbrCi = DefaultValues.CHAR_VAL;
	//Original name: FILLER-XZC004-ACT-NOT-POL-FRM-DATA
	private char flr1 = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0C0004_LAYOUT;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0c0004LayoutBytes(buf);
	}

	public void setWsXz0c0004LayoutFormatted(String data) {
		byte[] buffer = new byte[Len.WS_XZ0C0004_LAYOUT];
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0C0004_LAYOUT);
		setWsXz0c0004LayoutBytes(buffer, 1);
	}

	public String getWsXz0c0004LayoutFormatted() {
		return getActNotPolFrmRowFormatted();
	}

	public void setWsXz0c0004LayoutBytes(byte[] buffer) {
		setWsXz0c0004LayoutBytes(buffer, 1);
	}

	public byte[] getWsXz0c0004LayoutBytes() {
		byte[] buffer = new byte[Len.WS_XZ0C0004_LAYOUT];
		return getWsXz0c0004LayoutBytes(buffer, 1);
	}

	public void setWsXz0c0004LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotPolFrmRowBytes(buffer, position);
	}

	public byte[] getWsXz0c0004LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotPolFrmRowBytes(buffer, position);
		return buffer;
	}

	public String getActNotPolFrmRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotPolFrmRowBytes());
	}

	/**Original name: XZC004-ACT-NOT-POL-FRM-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0C0004 - ACT_NOT_POL_FRM TABLE                               *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 05 FEB 2009 E404GRK   GENERATED                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getActNotPolFrmRowBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_POL_FRM_ROW];
		return getActNotPolFrmRowBytes(buffer, 1);
	}

	public void setActNotPolFrmRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotPolFrmFixedBytes(buffer, position);
		position += Len.ACT_NOT_POL_FRM_FIXED;
		setActNotPolFrmDatesBytes(buffer, position);
		position += Len.ACT_NOT_POL_FRM_DATES;
		setActNotPolFrmKeyBytes(buffer, position);
		position += Len.ACT_NOT_POL_FRM_KEY;
		setActNotPolFrmKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_POL_FRM_KEY_CI;
		setActNotPolFrmDataBytes(buffer, position);
	}

	public byte[] getActNotPolFrmRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotPolFrmFixedBytes(buffer, position);
		position += Len.ACT_NOT_POL_FRM_FIXED;
		getActNotPolFrmDatesBytes(buffer, position);
		position += Len.ACT_NOT_POL_FRM_DATES;
		getActNotPolFrmKeyBytes(buffer, position);
		position += Len.ACT_NOT_POL_FRM_KEY;
		getActNotPolFrmKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_POL_FRM_KEY_CI;
		getActNotPolFrmDataBytes(buffer, position);
		return buffer;
	}

	public void setActNotPolFrmFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotPolFrmCsum = MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_POL_FRM_CSUM);
		position += Len.ACT_NOT_POL_FRM_CSUM;
		csrActNbrKcre = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		notPrcTsKcre = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		frmSeqNbrKcre = MarshalByte.readString(buffer, position, Len.FRM_SEQ_NBR_KCRE);
		position += Len.FRM_SEQ_NBR_KCRE;
		polNbrKcre = MarshalByte.readString(buffer, position, Len.POL_NBR_KCRE);
	}

	public byte[] getActNotPolFrmFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotPolFrmCsum, Len.ACT_NOT_POL_FRM_CSUM);
		position += Len.ACT_NOT_POL_FRM_CSUM;
		MarshalByte.writeString(buffer, position, csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		MarshalByte.writeString(buffer, position, notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		MarshalByte.writeString(buffer, position, frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
		position += Len.FRM_SEQ_NBR_KCRE;
		MarshalByte.writeString(buffer, position, polNbrKcre, Len.POL_NBR_KCRE);
		return buffer;
	}

	public void setActNotPolFrmCsumFormatted(String actNotPolFrmCsum) {
		this.actNotPolFrmCsum = Trunc.toUnsignedNumeric(actNotPolFrmCsum, Len.ACT_NOT_POL_FRM_CSUM);
	}

	public int getActNotPolFrmCsum() {
		return NumericDisplay.asInt(this.actNotPolFrmCsum);
	}

	public void setCsrActNbrKcre(String csrActNbrKcre) {
		this.csrActNbrKcre = Functions.subString(csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
	}

	public String getCsrActNbrKcre() {
		return this.csrActNbrKcre;
	}

	public void setNotPrcTsKcre(String notPrcTsKcre) {
		this.notPrcTsKcre = Functions.subString(notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
	}

	public String getNotPrcTsKcre() {
		return this.notPrcTsKcre;
	}

	public void setFrmSeqNbrKcre(String frmSeqNbrKcre) {
		this.frmSeqNbrKcre = Functions.subString(frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
	}

	public String getFrmSeqNbrKcre() {
		return this.frmSeqNbrKcre;
	}

	public void setPolNbrKcre(String polNbrKcre) {
		this.polNbrKcre = Functions.subString(polNbrKcre, Len.POL_NBR_KCRE);
	}

	public String getPolNbrKcre() {
		return this.polNbrKcre;
	}

	public void setActNotPolFrmDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getActNotPolFrmDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setActNotPolFrmKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		frmSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		frmSeqNbr = MarshalByte.readFixedString(buffer, position, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
	}

	public byte[] getActNotPolFrmKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, frmSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public void setFrmSeqNbrSign(char frmSeqNbrSign) {
		this.frmSeqNbrSign = frmSeqNbrSign;
	}

	public char getFrmSeqNbrSign() {
		return this.frmSeqNbrSign;
	}

	public void setFrmSeqNbrFormatted(String frmSeqNbr) {
		this.frmSeqNbr = Trunc.toUnsignedNumeric(frmSeqNbr, Len.FRM_SEQ_NBR);
	}

	public int getFrmSeqNbr() {
		return NumericDisplay.asInt(this.frmSeqNbr);
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setActNotPolFrmKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notPrcTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		frmSeqNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polNbrCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotPolFrmKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, frmSeqNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polNbrCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi = csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi = notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setFrmSeqNbrCi(char frmSeqNbrCi) {
		this.frmSeqNbrCi = frmSeqNbrCi;
	}

	public char getFrmSeqNbrCi() {
		return this.frmSeqNbrCi;
	}

	public void setPolNbrCi(char polNbrCi) {
		this.polNbrCi = polNbrCi;
	}

	public char getPolNbrCi() {
		return this.polNbrCi;
	}

	public void setActNotPolFrmDataBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotPolFrmDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		return buffer;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0c0004LayoutBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_POL_FRM_CSUM = 9;
		public static final int CSR_ACT_NBR_KCRE = 32;
		public static final int NOT_PRC_TS_KCRE = 32;
		public static final int FRM_SEQ_NBR_KCRE = 32;
		public static final int POL_NBR_KCRE = 32;
		public static final int ACT_NOT_POL_FRM_FIXED = ACT_NOT_POL_FRM_CSUM + CSR_ACT_NBR_KCRE + NOT_PRC_TS_KCRE + FRM_SEQ_NBR_KCRE + POL_NBR_KCRE;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int ACT_NOT_POL_FRM_DATES = TRANS_PROCESS_DT;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FRM_SEQ_NBR_SIGN = 1;
		public static final int FRM_SEQ_NBR = 5;
		public static final int POL_NBR = 25;
		public static final int ACT_NOT_POL_FRM_KEY = CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR_SIGN + FRM_SEQ_NBR + POL_NBR;
		public static final int CSR_ACT_NBR_CI = 1;
		public static final int NOT_PRC_TS_CI = 1;
		public static final int FRM_SEQ_NBR_CI = 1;
		public static final int POL_NBR_CI = 1;
		public static final int ACT_NOT_POL_FRM_KEY_CI = CSR_ACT_NBR_CI + NOT_PRC_TS_CI + FRM_SEQ_NBR_CI + POL_NBR_CI;
		public static final int FLR1 = 1;
		public static final int ACT_NOT_POL_FRM_DATA = FLR1;
		public static final int ACT_NOT_POL_FRM_ROW = ACT_NOT_POL_FRM_FIXED + ACT_NOT_POL_FRM_DATES + ACT_NOT_POL_FRM_KEY + ACT_NOT_POL_FRM_KEY_CI
				+ ACT_NOT_POL_FRM_DATA;
		public static final int WS_XZ0C0004_LAYOUT = ACT_NOT_POL_FRM_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
