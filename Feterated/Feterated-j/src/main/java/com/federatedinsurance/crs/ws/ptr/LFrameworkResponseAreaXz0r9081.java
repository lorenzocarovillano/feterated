/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9081<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9081 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9081_MAXOCCURS = 2000;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9081() {
	}

	public LFrameworkResponseAreaXz0r9081(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a9081Formatted(int lFwRespXz0a9081Idx) {
		int position = Pos.lFwRespXz0a9081(lFwRespXz0a9081Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9081);
	}

	public void setlFwRespXz0a9081Bytes(int lFwRespXz0a9081Idx, byte[] buffer) {
		setlFwRespXz0a9081Bytes(lFwRespXz0a9081Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A9081<br>*/
	public byte[] getlFwRespXz0a9081Bytes(int lFwRespXz0a9081Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_XZ0A9081];
		return getlFwRespXz0a9081Bytes(lFwRespXz0a9081Idx, buffer, 1);
	}

	public void setlFwRespXz0a9081Bytes(int lFwRespXz0a9081Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9081(lFwRespXz0a9081Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A9081, position);
	}

	public byte[] getlFwRespXz0a9081Bytes(int lFwRespXz0a9081Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9081(lFwRespXz0a9081Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A9081, position);
		return buffer;
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9081(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9081;
		}

		public static int xza981TtyCertInfoRow(int idx) {
			return lFwRespXz0a9081(idx);
		}

		public static int xza981MaxTtyRows(int idx) {
			return xza981TtyCertInfoRow(idx);
		}

		public static int xza981CsrActNbr(int idx) {
			return xza981MaxTtyRows(idx) + Len.XZA981_MAX_TTY_ROWS;
		}

		public static int xza981NotPrcTs(int idx) {
			return xza981CsrActNbr(idx) + Len.XZA981_CSR_ACT_NBR;
		}

		public static int xza981Userid(int idx) {
			return xza981NotPrcTs(idx) + Len.XZA981_NOT_PRC_TS;
		}

		public static int xza981TtyCertList(int idx) {
			return xza981Userid(idx) + Len.XZA981_USERID;
		}

		public static int xza981ClientId(int idx) {
			return xza981TtyCertList(idx);
		}

		public static int xza981AdrId(int idx) {
			return xza981ClientId(idx) + Len.XZA981_CLIENT_ID;
		}

		public static int xza981RecTypCd(int idx) {
			return xza981AdrId(idx) + Len.XZA981_ADR_ID;
		}

		public static int xza981RecTypDes(int idx) {
			return xza981RecTypCd(idx) + Len.XZA981_REC_TYP_CD;
		}

		public static int xza981Name(int idx) {
			return xza981RecTypDes(idx) + Len.XZA981_REC_TYP_DES;
		}

		public static int xza981AdrLin1(int idx) {
			return xza981Name(idx) + Len.XZA981_NAME;
		}

		public static int xza981AdrLin2(int idx) {
			return xza981AdrLin1(idx) + Len.XZA981_ADR_LIN1;
		}

		public static int xza981CityNm(int idx) {
			return xza981AdrLin2(idx) + Len.XZA981_ADR_LIN2;
		}

		public static int xza981StateAbb(int idx) {
			return xza981CityNm(idx) + Len.XZA981_CITY_NM;
		}

		public static int xza981PstCd(int idx) {
			return xza981StateAbb(idx) + Len.XZA981_STATE_ABB;
		}

		public static int xza981CerNbr(int idx) {
			return xza981PstCd(idx) + Len.XZA981_PST_CD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA981_MAX_TTY_ROWS = 2;
		public static final int XZA981_CSR_ACT_NBR = 9;
		public static final int XZA981_NOT_PRC_TS = 26;
		public static final int XZA981_USERID = 8;
		public static final int XZA981_CLIENT_ID = 64;
		public static final int XZA981_ADR_ID = 64;
		public static final int XZA981_REC_TYP_CD = 5;
		public static final int XZA981_REC_TYP_DES = 13;
		public static final int XZA981_NAME = 120;
		public static final int XZA981_ADR_LIN1 = 45;
		public static final int XZA981_ADR_LIN2 = 45;
		public static final int XZA981_CITY_NM = 30;
		public static final int XZA981_STATE_ABB = 2;
		public static final int XZA981_PST_CD = 13;
		public static final int XZA981_CER_NBR = 25;
		public static final int XZA981_TTY_CERT_LIST = XZA981_CLIENT_ID + XZA981_ADR_ID + XZA981_REC_TYP_CD + XZA981_REC_TYP_DES + XZA981_NAME
				+ XZA981_ADR_LIN1 + XZA981_ADR_LIN2 + XZA981_CITY_NM + XZA981_STATE_ABB + XZA981_PST_CD + XZA981_CER_NBR;
		public static final int XZA981_TTY_CERT_INFO_ROW = XZA981_MAX_TTY_ROWS + XZA981_CSR_ACT_NBR + XZA981_NOT_PRC_TS + XZA981_USERID
				+ XZA981_TTY_CERT_LIST;
		public static final int L_FW_RESP_XZ0A9081 = XZA981_TTY_CERT_INFO_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0r9081.L_FW_RESP_XZ0A9081_MAXOCCURS * L_FW_RESP_XZ0A9081;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
