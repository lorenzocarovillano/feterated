/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-ACT-TYP-CD<br>
 * Variable: SA-ACT-TYP-CD from program XZ0P90E0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaActTypCd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.ACT_TYP_CD);
	public static final String COMMERCIAL_LINE = "CL";
	public static final String PERSONAL_LINE = "PL";

	//==== METHODS ====
	public void setActTypCd(String actTypCd) {
		this.value = Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	public String getActTypCd() {
		return this.value;
	}

	public boolean isCommercialLine() {
		return value.equals(COMMERCIAL_LINE);
	}

	public boolean isPersonalLine() {
		return value.equals(PERSONAL_LINE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_TYP_CD = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
