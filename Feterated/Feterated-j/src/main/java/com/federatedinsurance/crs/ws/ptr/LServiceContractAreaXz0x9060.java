/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9060<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9060 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT96O_POL_LIST_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9060() {
	}

	public LServiceContractAreaXz0x9060(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt96iCsrActNbr(String xzt96iCsrActNbr) {
		writeString(Pos.XZT96I_CSR_ACT_NBR, xzt96iCsrActNbr, Len.XZT96I_CSR_ACT_NBR);
	}

	/**Original name: XZT96I-CSR-ACT-NBR<br>*/
	public String getXzt96iCsrActNbr() {
		return readString(Pos.XZT96I_CSR_ACT_NBR, Len.XZT96I_CSR_ACT_NBR);
	}

	public void setXzt96iUserid(String xzt96iUserid) {
		writeString(Pos.XZT96I_USERID, xzt96iUserid, Len.XZT96I_USERID);
	}

	/**Original name: XZT96I-USERID<br>*/
	public String getXzt96iUserid() {
		return readString(Pos.XZT96I_USERID, Len.XZT96I_USERID);
	}

	public String getXzt96iUseridFormatted() {
		return Functions.padBlanks(getXzt96iUserid(), Len.XZT96I_USERID);
	}

	public void setXzt96oTkNotPrcTs(String xzt96oTkNotPrcTs) {
		writeString(Pos.XZT96O_TK_NOT_PRC_TS, xzt96oTkNotPrcTs, Len.XZT96O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT96O-TK-NOT-PRC-TS<br>*/
	public String getXzt96oTkNotPrcTs() {
		return readString(Pos.XZT96O_TK_NOT_PRC_TS, Len.XZT96O_TK_NOT_PRC_TS);
	}

	public void setXzt96oCsrActNbr(String xzt96oCsrActNbr) {
		writeString(Pos.XZT96O_CSR_ACT_NBR, xzt96oCsrActNbr, Len.XZT96O_CSR_ACT_NBR);
	}

	/**Original name: XZT96O-CSR-ACT-NBR<br>*/
	public String getXzt96oCsrActNbr() {
		return readString(Pos.XZT96O_CSR_ACT_NBR, Len.XZT96O_CSR_ACT_NBR);
	}

	public void setXzt96oNotDt(String xzt96oNotDt) {
		writeString(Pos.XZT96O_NOT_DT, xzt96oNotDt, Len.XZT96O_NOT_DT);
	}

	/**Original name: XZT96O-NOT-DT<br>*/
	public String getXzt96oNotDt() {
		return readString(Pos.XZT96O_NOT_DT, Len.XZT96O_NOT_DT);
	}

	public void setXzt96oTotFeeAmt(AfDecimal xzt96oTotFeeAmt) {
		writeDecimal(Pos.XZT96O_TOT_FEE_AMT, xzt96oTotFeeAmt.copy());
	}

	/**Original name: XZT96O-TOT-FEE-AMT<br>*/
	public AfDecimal getXzt96oTotFeeAmt() {
		return readDecimal(Pos.XZT96O_TOT_FEE_AMT, Len.Int.XZT96O_TOT_FEE_AMT, Len.Fract.XZT96O_TOT_FEE_AMT);
	}

	public void setXzt96oStAbb(String xzt96oStAbb) {
		writeString(Pos.XZT96O_ST_ABB, xzt96oStAbb, Len.XZT96O_ST_ABB);
	}

	/**Original name: XZT96O-ST-ABB<br>*/
	public String getXzt96oStAbb() {
		return readString(Pos.XZT96O_ST_ABB, Len.XZT96O_ST_ABB);
	}

	public void setXzt96oDsyPrtGrp(String xzt96oDsyPrtGrp) {
		writeString(Pos.XZT96O_DSY_PRT_GRP, xzt96oDsyPrtGrp, Len.XZT96O_DSY_PRT_GRP);
	}

	/**Original name: XZT96O-DSY-PRT-GRP<br>*/
	public String getXzt96oDsyPrtGrp() {
		return readString(Pos.XZT96O_DSY_PRT_GRP, Len.XZT96O_DSY_PRT_GRP);
	}

	public void setXzt96oPolNbr(int xzt96oPolNbrIdx, String xzt96oPolNbr) {
		int position = Pos.xzt96oPolNbr(xzt96oPolNbrIdx - 1);
		writeString(position, xzt96oPolNbr, Len.XZT96O_POL_NBR);
	}

	/**Original name: XZT96O-POL-NBR<br>*/
	public String getXzt96oPolNbr(int xzt96oPolNbrIdx) {
		int position = Pos.xzt96oPolNbr(xzt96oPolNbrIdx - 1);
		return readString(position, Len.XZT96O_POL_NBR);
	}

	public void setXzt96oPolTypCd(int xzt96oPolTypCdIdx, String xzt96oPolTypCd) {
		int position = Pos.xzt96oPolTypCd(xzt96oPolTypCdIdx - 1);
		writeString(position, xzt96oPolTypCd, Len.XZT96O_POL_TYP_CD);
	}

	/**Original name: XZT96O-POL-TYP-CD<br>*/
	public String getXzt96oPolTypCd(int xzt96oPolTypCdIdx) {
		int position = Pos.xzt96oPolTypCd(xzt96oPolTypCdIdx - 1);
		return readString(position, Len.XZT96O_POL_TYP_CD);
	}

	public void setXzt96oPolTypDes(int xzt96oPolTypDesIdx, String xzt96oPolTypDes) {
		int position = Pos.xzt96oPolTypDes(xzt96oPolTypDesIdx - 1);
		writeString(position, xzt96oPolTypDes, Len.XZT96O_POL_TYP_DES);
	}

	/**Original name: XZT96O-POL-TYP-DES<br>*/
	public String getXzt96oPolTypDes(int xzt96oPolTypDesIdx) {
		int position = Pos.xzt96oPolTypDes(xzt96oPolTypDesIdx - 1);
		return readString(position, Len.XZT96O_POL_TYP_DES);
	}

	public void setXzt96oPriRskStAbb(int xzt96oPriRskStAbbIdx, String xzt96oPriRskStAbb) {
		int position = Pos.xzt96oPriRskStAbb(xzt96oPriRskStAbbIdx - 1);
		writeString(position, xzt96oPriRskStAbb, Len.XZT96O_PRI_RSK_ST_ABB);
	}

	/**Original name: XZT96O-PRI-RSK-ST-ABB<br>*/
	public String getXzt96oPriRskStAbb(int xzt96oPriRskStAbbIdx) {
		int position = Pos.xzt96oPriRskStAbb(xzt96oPriRskStAbbIdx - 1);
		return readString(position, Len.XZT96O_PRI_RSK_ST_ABB);
	}

	public void setXzt96oNotEffDt(int xzt96oNotEffDtIdx, String xzt96oNotEffDt) {
		int position = Pos.xzt96oNotEffDt(xzt96oNotEffDtIdx - 1);
		writeString(position, xzt96oNotEffDt, Len.XZT96O_NOT_EFF_DT);
	}

	/**Original name: XZT96O-NOT-EFF-DT<br>*/
	public String getXzt96oNotEffDt(int xzt96oNotEffDtIdx) {
		int position = Pos.xzt96oNotEffDt(xzt96oNotEffDtIdx - 1);
		return readString(position, Len.XZT96O_NOT_EFF_DT);
	}

	public void setXzt96oPolEffDt(int xzt96oPolEffDtIdx, String xzt96oPolEffDt) {
		int position = Pos.xzt96oPolEffDt(xzt96oPolEffDtIdx - 1);
		writeString(position, xzt96oPolEffDt, Len.XZT96O_POL_EFF_DT);
	}

	/**Original name: XZT96O-POL-EFF-DT<br>*/
	public String getXzt96oPolEffDt(int xzt96oPolEffDtIdx) {
		int position = Pos.xzt96oPolEffDt(xzt96oPolEffDtIdx - 1);
		return readString(position, Len.XZT96O_POL_EFF_DT);
	}

	public void setXzt96oPolExpDt(int xzt96oPolExpDtIdx, String xzt96oPolExpDt) {
		int position = Pos.xzt96oPolExpDt(xzt96oPolExpDtIdx - 1);
		writeString(position, xzt96oPolExpDt, Len.XZT96O_POL_EXP_DT);
	}

	/**Original name: XZT96O-POL-EXP-DT<br>*/
	public String getXzt96oPolExpDt(int xzt96oPolExpDtIdx) {
		int position = Pos.xzt96oPolExpDt(xzt96oPolExpDtIdx - 1);
		return readString(position, Len.XZT96O_POL_EXP_DT);
	}

	public void setXzt96oPolDueAmt(int xzt96oPolDueAmtIdx, AfDecimal xzt96oPolDueAmt) {
		int position = Pos.xzt96oPolDueAmt(xzt96oPolDueAmtIdx - 1);
		writeDecimal(position, xzt96oPolDueAmt.copy());
	}

	/**Original name: XZT96O-POL-DUE-AMT<br>*/
	public AfDecimal getXzt96oPolDueAmt(int xzt96oPolDueAmtIdx) {
		int position = Pos.xzt96oPolDueAmt(xzt96oPolDueAmtIdx - 1);
		return readDecimal(position, Len.Int.XZT96O_POL_DUE_AMT, Len.Fract.XZT96O_POL_DUE_AMT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT906_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT96I_CSR_ACT_NBR = XZT906_SERVICE_INPUTS;
		public static final int XZT96I_USERID = XZT96I_CSR_ACT_NBR + Len.XZT96I_CSR_ACT_NBR;
		public static final int XZT906_SERVICE_OUTPUTS = XZT96I_USERID + Len.XZT96I_USERID;
		public static final int XZT96O_TECHNICAL_KEY = XZT906_SERVICE_OUTPUTS;
		public static final int XZT96O_TK_NOT_PRC_TS = XZT96O_TECHNICAL_KEY;
		public static final int XZT96O_CSR_ACT_NBR = XZT96O_TK_NOT_PRC_TS + Len.XZT96O_TK_NOT_PRC_TS;
		public static final int XZT96O_NOT_DT = XZT96O_CSR_ACT_NBR + Len.XZT96O_CSR_ACT_NBR;
		public static final int XZT96O_TOT_FEE_AMT = XZT96O_NOT_DT + Len.XZT96O_NOT_DT;
		public static final int XZT96O_ST_ABB = XZT96O_TOT_FEE_AMT + Len.XZT96O_TOT_FEE_AMT;
		public static final int XZT96O_DSY_PRT_GRP = XZT96O_ST_ABB + Len.XZT96O_ST_ABB;
		public static final int XZT96O_POL_LIST_TBL = XZT96O_DSY_PRT_GRP + Len.XZT96O_DSY_PRT_GRP;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt96oPolList(int idx) {
			return XZT96O_POL_LIST_TBL + idx * Len.XZT96O_POL_LIST;
		}

		public static int xzt96oPolNbr(int idx) {
			return xzt96oPolList(idx);
		}

		public static int xzt96oPolTyp(int idx) {
			return xzt96oPolNbr(idx) + Len.XZT96O_POL_NBR;
		}

		public static int xzt96oPolTypCd(int idx) {
			return xzt96oPolTyp(idx);
		}

		public static int xzt96oPolTypDes(int idx) {
			return xzt96oPolTypCd(idx) + Len.XZT96O_POL_TYP_CD;
		}

		public static int xzt96oPriRskStAbb(int idx) {
			return xzt96oPolTypDes(idx) + Len.XZT96O_POL_TYP_DES;
		}

		public static int xzt96oNotEffDt(int idx) {
			return xzt96oPriRskStAbb(idx) + Len.XZT96O_PRI_RSK_ST_ABB;
		}

		public static int xzt96oPolEffDt(int idx) {
			return xzt96oNotEffDt(idx) + Len.XZT96O_NOT_EFF_DT;
		}

		public static int xzt96oPolExpDt(int idx) {
			return xzt96oPolEffDt(idx) + Len.XZT96O_POL_EFF_DT;
		}

		public static int xzt96oPolDueAmt(int idx) {
			return xzt96oPolExpDt(idx) + Len.XZT96O_POL_EXP_DT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT96I_CSR_ACT_NBR = 9;
		public static final int XZT96I_USERID = 8;
		public static final int XZT96O_TK_NOT_PRC_TS = 26;
		public static final int XZT96O_CSR_ACT_NBR = 9;
		public static final int XZT96O_NOT_DT = 10;
		public static final int XZT96O_TOT_FEE_AMT = 10;
		public static final int XZT96O_ST_ABB = 2;
		public static final int XZT96O_DSY_PRT_GRP = 5;
		public static final int XZT96O_POL_NBR = 25;
		public static final int XZT96O_POL_TYP_CD = 3;
		public static final int XZT96O_POL_TYP_DES = 30;
		public static final int XZT96O_POL_TYP = XZT96O_POL_TYP_CD + XZT96O_POL_TYP_DES;
		public static final int XZT96O_PRI_RSK_ST_ABB = 2;
		public static final int XZT96O_NOT_EFF_DT = 10;
		public static final int XZT96O_POL_EFF_DT = 10;
		public static final int XZT96O_POL_EXP_DT = 10;
		public static final int XZT96O_POL_DUE_AMT = 10;
		public static final int XZT96O_POL_LIST = XZT96O_POL_NBR + XZT96O_POL_TYP + XZT96O_PRI_RSK_ST_ABB + XZT96O_NOT_EFF_DT + XZT96O_POL_EFF_DT
				+ XZT96O_POL_EXP_DT + XZT96O_POL_DUE_AMT;
		public static final int XZT906_SERVICE_INPUTS = XZT96I_CSR_ACT_NBR + XZT96I_USERID;
		public static final int XZT96O_TECHNICAL_KEY = XZT96O_TK_NOT_PRC_TS;
		public static final int XZT96O_POL_LIST_TBL = LServiceContractAreaXz0x9060.XZT96O_POL_LIST_MAXOCCURS * XZT96O_POL_LIST;
		public static final int XZT906_SERVICE_OUTPUTS = XZT96O_TECHNICAL_KEY + XZT96O_CSR_ACT_NBR + XZT96O_NOT_DT + XZT96O_TOT_FEE_AMT
				+ XZT96O_ST_ABB + XZT96O_DSY_PRT_GRP + XZT96O_POL_LIST_TBL;
		public static final int L_SERVICE_CONTRACT_AREA = XZT906_SERVICE_INPUTS + XZT906_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT96O_TOT_FEE_AMT = 8;
			public static final int XZT96O_POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZT96O_TOT_FEE_AMT = 2;
			public static final int XZT96O_POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
