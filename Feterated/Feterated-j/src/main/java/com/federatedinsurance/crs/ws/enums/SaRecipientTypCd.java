/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-RECIPIENT-TYP-CD<br>
 * Variable: SA-RECIPIENT-TYP-CD from program XZ0P90E0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaRecipientTypCd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.RECIPIENT_TYP_CD);
	public static final String ADINS = "ADINS";
	public static final String ANI = "ANI";
	public static final String CERT = "CERT";
	public static final String INS = "INS";
	public static final String LPEMG = "LPEMG";

	//==== METHODS ====
	public void setRecipientTypCd(String recipientTypCd) {
		this.value = Functions.subString(recipientTypCd, Len.RECIPIENT_TYP_CD);
	}

	public String getRecipientTypCd() {
		return this.value;
	}

	public String getRecipientTypCdFormatted() {
		return Functions.padBlanks(getRecipientTypCd(), Len.RECIPIENT_TYP_CD);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RECIPIENT_TYP_CD = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
