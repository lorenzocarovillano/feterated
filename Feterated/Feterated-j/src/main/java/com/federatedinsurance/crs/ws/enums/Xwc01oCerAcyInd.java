/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: XWC01O-CER-ACY-IND<br>
 * Variable: XWC01O-CER-ACY-IND from copybook XWC010C1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xwc01oCerAcyInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char ACY = 'A';
	public static final char NOT_ACY = 'I';

	//==== METHODS ====
	public void setoCerAcyInd(char oCerAcyInd) {
		this.value = oCerAcyInd;
	}

	public char getoCerAcyInd() {
		return this.value;
	}

	public boolean isNotAcy() {
		return value == NOT_ACY;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int O_CER_ACY_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
