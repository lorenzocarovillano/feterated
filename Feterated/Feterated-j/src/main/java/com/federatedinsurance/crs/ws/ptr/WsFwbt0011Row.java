/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-FWBT0011-ROW<br>
 * Variable: WS-FWBT0011-ROW from program XZ0P9000<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class WsFwbt0011Row extends BytesClass {

	//==== PROPERTIES ====
	public static final int O_WNG_MSG_MAXOCCURS = 6;
	public static final int O_AUTOMATED_LINES_MAXOCCURS = 10;
	public static final int O_MANUAL_LINES_MAXOCCURS = 10;

	//==== CONSTRUCTORS ====
	public WsFwbt0011Row() {
	}

	public WsFwbt0011Row(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_FWBT0011_ROW;
	}

	public void setiTkPolId(String iTkPolId) {
		writeString(Pos.I_TK_POL_ID, iTkPolId, Len.I_TK_POL_ID);
	}

	/**Original name: FWBT11I-TK-POL-ID<br>*/
	public String getiTkPolId() {
		return readString(Pos.I_TK_POL_ID, Len.I_TK_POL_ID);
	}

	public void setiTkPolNbr(String iTkPolNbr) {
		writeString(Pos.I_TK_POL_NBR, iTkPolNbr, Len.I_TK_POL_NBR);
	}

	/**Original name: FWBT11I-TK-POL-NBR<br>*/
	public String getiTkPolNbr() {
		return readString(Pos.I_TK_POL_NBR, Len.I_TK_POL_NBR);
	}

	public void setiTkPolEffDt(String iTkPolEffDt) {
		writeString(Pos.I_TK_POL_EFF_DT, iTkPolEffDt, Len.I_TK_POL_EFF_DT);
	}

	/**Original name: FWBT11I-TK-POL-EFF-DT<br>*/
	public String getiTkPolEffDt() {
		return readString(Pos.I_TK_POL_EFF_DT, Len.I_TK_POL_EFF_DT);
	}

	public void setiTkPolExpDt(String iTkPolExpDt) {
		writeString(Pos.I_TK_POL_EXP_DT, iTkPolExpDt, Len.I_TK_POL_EXP_DT);
	}

	/**Original name: FWBT11I-TK-POL-EXP-DT<br>*/
	public String getiTkPolExpDt() {
		return readString(Pos.I_TK_POL_EXP_DT, Len.I_TK_POL_EXP_DT);
	}

	public void setiTkQteNbr(int iTkQteNbr) {
		writeInt(Pos.I_TK_QTE_NBR, iTkQteNbr, Len.Int.I_TK_QTE_NBR);
	}

	/**Original name: FWBT11I-TK-QTE-NBR<br>*/
	public int getiTkQteNbr() {
		return readNumDispInt(Pos.I_TK_QTE_NBR, Len.I_TK_QTE_NBR);
	}

	public void setiTkActNbr(String iTkActNbr) {
		writeString(Pos.I_TK_ACT_NBR, iTkActNbr, Len.I_TK_ACT_NBR);
	}

	/**Original name: FWBT11I-TK-ACT-NBR<br>*/
	public String getiTkActNbr() {
		return readString(Pos.I_TK_ACT_NBR, Len.I_TK_ACT_NBR);
	}

	public void setiTkWrtDt(String iTkWrtDt) {
		writeString(Pos.I_TK_WRT_DT, iTkWrtDt, Len.I_TK_WRT_DT);
	}

	/**Original name: FWBT11I-TK-WRT-DT<br>*/
	public String getiTkWrtDt() {
		return readString(Pos.I_TK_WRT_DT, Len.I_TK_WRT_DT);
	}

	public void setiTkPriRskStAbb(String iTkPriRskStAbb) {
		writeString(Pos.I_TK_PRI_RSK_ST_ABB, iTkPriRskStAbb, Len.I_TK_PRI_RSK_ST_ABB);
	}

	/**Original name: FWBT11I-TK-PRI-RSK-ST-ABB<br>*/
	public String getiTkPriRskStAbb() {
		return readString(Pos.I_TK_PRI_RSK_ST_ABB, Len.I_TK_PRI_RSK_ST_ABB);
	}

	public void setiTkLobCd(String iTkLobCd) {
		writeString(Pos.I_TK_LOB_CD, iTkLobCd, Len.I_TK_LOB_CD);
	}

	/**Original name: FWBT11I-TK-LOB-CD<br>*/
	public String getiTkLobCd() {
		return readString(Pos.I_TK_LOB_CD, Len.I_TK_LOB_CD);
	}

	public void setiTkCmpNbr(String iTkCmpNbr) {
		writeString(Pos.I_TK_CMP_NBR, iTkCmpNbr, Len.I_TK_CMP_NBR);
	}

	/**Original name: FWBT11I-TK-CMP-NBR<br>*/
	public String getiTkCmpNbr() {
		return readString(Pos.I_TK_CMP_NBR, Len.I_TK_CMP_NBR);
	}

	public void setiTkTobCd(String iTkTobCd) {
		writeString(Pos.I_TK_TOB_CD, iTkTobCd, Len.I_TK_TOB_CD);
	}

	/**Original name: FWBT11I-TK-TOB-CD<br>*/
	public String getiTkTobCd() {
		return readString(Pos.I_TK_TOB_CD, Len.I_TK_TOB_CD);
	}

	public void setiTkSegCd(String iTkSegCd) {
		writeString(Pos.I_TK_SEG_CD, iTkSegCd, Len.I_TK_SEG_CD);
	}

	/**Original name: FWBT11I-TK-SEG-CD<br>*/
	public String getiTkSegCd() {
		return readString(Pos.I_TK_SEG_CD, Len.I_TK_SEG_CD);
	}

	public void setiTkLgeActInd(char iTkLgeActInd) {
		writeChar(Pos.I_TK_LGE_ACT_IND, iTkLgeActInd);
	}

	/**Original name: FWBT11I-TK-LGE-ACT-IND<br>*/
	public char getiTkLgeActInd() {
		return readChar(Pos.I_TK_LGE_ACT_IND);
	}

	public void setiTkAlAdacInd(char iTkAlAdacInd) {
		writeChar(Pos.I_TK_AL_ADAC_IND, iTkAlAdacInd);
	}

	/**Original name: FWBT11I-TK-AL-ADAC-IND<br>*/
	public char getiTkAlAdacInd() {
		return readChar(Pos.I_TK_AL_ADAC_IND);
	}

	public void setiPolNbr(String iPolNbr) {
		writeString(Pos.I_POL_NBR, iPolNbr, Len.I_POL_NBR);
	}

	/**Original name: FWBT11I-POL-NBR<br>*/
	public String getiPolNbr() {
		return readString(Pos.I_POL_NBR, Len.I_POL_NBR);
	}

	public void setiAsOfDt(String iAsOfDt) {
		writeString(Pos.I_AS_OF_DT, iAsOfDt, Len.I_AS_OF_DT);
	}

	/**Original name: FWBT11I-AS-OF-DT<br>*/
	public String getiAsOfDt() {
		return readString(Pos.I_AS_OF_DT, Len.I_AS_OF_DT);
	}

	public void setiPolEffDt(String iPolEffDt) {
		writeString(Pos.I_POL_EFF_DT, iPolEffDt, Len.I_POL_EFF_DT);
	}

	/**Original name: FWBT11I-POL-EFF-DT<br>*/
	public String getiPolEffDt() {
		return readString(Pos.I_POL_EFF_DT, Len.I_POL_EFF_DT);
	}

	public void setiPolExpDt(String iPolExpDt) {
		writeString(Pos.I_POL_EXP_DT, iPolExpDt, Len.I_POL_EXP_DT);
	}

	/**Original name: FWBT11I-POL-EXP-DT<br>*/
	public String getiPolExpDt() {
		return readString(Pos.I_POL_EXP_DT, Len.I_POL_EXP_DT);
	}

	public void setiQteNbr(int iQteNbr) {
		writeInt(Pos.I_QTE_NBR, iQteNbr, Len.Int.I_QTE_NBR);
	}

	/**Original name: FWBT11I-QTE-NBR<br>*/
	public int getiQteNbr() {
		return readNumDispInt(Pos.I_QTE_NBR, Len.I_QTE_NBR);
	}

	public void setiUsrId(String iUsrId) {
		writeString(Pos.I_USR_ID, iUsrId, Len.I_USR_ID);
	}

	/**Original name: FWBT11I-USR-ID<br>*/
	public String getiUsrId() {
		return readString(Pos.I_USR_ID, Len.I_USR_ID);
	}

	public void setoTkPolId(String oTkPolId) {
		writeString(Pos.O_TK_POL_ID, oTkPolId, Len.O_TK_POL_ID);
	}

	/**Original name: FWBT11O-TK-POL-ID<br>*/
	public String getoTkPolId() {
		return readString(Pos.O_TK_POL_ID, Len.O_TK_POL_ID);
	}

	public void setoTkPolNbr(String oTkPolNbr) {
		writeString(Pos.O_TK_POL_NBR, oTkPolNbr, Len.O_TK_POL_NBR);
	}

	/**Original name: FWBT11O-TK-POL-NBR<br>*/
	public String getoTkPolNbr() {
		return readString(Pos.O_TK_POL_NBR, Len.O_TK_POL_NBR);
	}

	public void setoTkPolEffDt(String oTkPolEffDt) {
		writeString(Pos.O_TK_POL_EFF_DT, oTkPolEffDt, Len.O_TK_POL_EFF_DT);
	}

	/**Original name: FWBT11O-TK-POL-EFF-DT<br>*/
	public String getoTkPolEffDt() {
		return readString(Pos.O_TK_POL_EFF_DT, Len.O_TK_POL_EFF_DT);
	}

	public void setoTkPolExpDt(String oTkPolExpDt) {
		writeString(Pos.O_TK_POL_EXP_DT, oTkPolExpDt, Len.O_TK_POL_EXP_DT);
	}

	/**Original name: FWBT11O-TK-POL-EXP-DT<br>*/
	public String getoTkPolExpDt() {
		return readString(Pos.O_TK_POL_EXP_DT, Len.O_TK_POL_EXP_DT);
	}

	public void setoTkQteNbr(int oTkQteNbr) {
		writeInt(Pos.O_TK_QTE_NBR, oTkQteNbr, Len.Int.O_TK_QTE_NBR);
	}

	/**Original name: FWBT11O-TK-QTE-NBR<br>*/
	public int getoTkQteNbr() {
		return readNumDispInt(Pos.O_TK_QTE_NBR, Len.O_TK_QTE_NBR);
	}

	public void setoTkActNbr(String oTkActNbr) {
		writeString(Pos.O_TK_ACT_NBR, oTkActNbr, Len.O_TK_ACT_NBR);
	}

	/**Original name: FWBT11O-TK-ACT-NBR<br>*/
	public String getoTkActNbr() {
		return readString(Pos.O_TK_ACT_NBR, Len.O_TK_ACT_NBR);
	}

	public void setoTkWrtDt(String oTkWrtDt) {
		writeString(Pos.O_TK_WRT_DT, oTkWrtDt, Len.O_TK_WRT_DT);
	}

	/**Original name: FWBT11O-TK-WRT-DT<br>*/
	public String getoTkWrtDt() {
		return readString(Pos.O_TK_WRT_DT, Len.O_TK_WRT_DT);
	}

	public void setoTkPriRskStAbb(String oTkPriRskStAbb) {
		writeString(Pos.O_TK_PRI_RSK_ST_ABB, oTkPriRskStAbb, Len.O_TK_PRI_RSK_ST_ABB);
	}

	/**Original name: FWBT11O-TK-PRI-RSK-ST-ABB<br>*/
	public String getoTkPriRskStAbb() {
		return readString(Pos.O_TK_PRI_RSK_ST_ABB, Len.O_TK_PRI_RSK_ST_ABB);
	}

	public void setoTkLobCd(String oTkLobCd) {
		writeString(Pos.O_TK_LOB_CD, oTkLobCd, Len.O_TK_LOB_CD);
	}

	/**Original name: FWBT11O-TK-LOB-CD<br>*/
	public String getoTkLobCd() {
		return readString(Pos.O_TK_LOB_CD, Len.O_TK_LOB_CD);
	}

	public void setoTkCmpNbr(String oTkCmpNbr) {
		writeString(Pos.O_TK_CMP_NBR, oTkCmpNbr, Len.O_TK_CMP_NBR);
	}

	/**Original name: FWBT11O-TK-CMP-NBR<br>*/
	public String getoTkCmpNbr() {
		return readString(Pos.O_TK_CMP_NBR, Len.O_TK_CMP_NBR);
	}

	public void setoTkTobCd(String oTkTobCd) {
		writeString(Pos.O_TK_TOB_CD, oTkTobCd, Len.O_TK_TOB_CD);
	}

	/**Original name: FWBT11O-TK-TOB-CD<br>*/
	public String getoTkTobCd() {
		return readString(Pos.O_TK_TOB_CD, Len.O_TK_TOB_CD);
	}

	public void setoTkSegCd(String oTkSegCd) {
		writeString(Pos.O_TK_SEG_CD, oTkSegCd, Len.O_TK_SEG_CD);
	}

	/**Original name: FWBT11O-TK-SEG-CD<br>*/
	public String getoTkSegCd() {
		return readString(Pos.O_TK_SEG_CD, Len.O_TK_SEG_CD);
	}

	public void setoTkLgeActInd(char oTkLgeActInd) {
		writeChar(Pos.O_TK_LGE_ACT_IND, oTkLgeActInd);
	}

	/**Original name: FWBT11O-TK-LGE-ACT-IND<br>*/
	public char getoTkLgeActInd() {
		return readChar(Pos.O_TK_LGE_ACT_IND);
	}

	public void setoTkAlAdacInd(char oTkAlAdacInd) {
		writeChar(Pos.O_TK_AL_ADAC_IND, oTkAlAdacInd);
	}

	/**Original name: FWBT11O-TK-AL-ADAC-IND<br>*/
	public char getoTkAlAdacInd() {
		return readChar(Pos.O_TK_AL_ADAC_IND);
	}

	public void setoPolNbr(String oPolNbr) {
		writeString(Pos.O_POL_NBR, oPolNbr, Len.O_POL_NBR);
	}

	/**Original name: FWBT11O-POL-NBR<br>*/
	public String getoPolNbr() {
		return readString(Pos.O_POL_NBR, Len.O_POL_NBR);
	}

	public void setoActNbr(String oActNbr) {
		writeString(Pos.O_ACT_NBR, oActNbr, Len.O_ACT_NBR);
	}

	/**Original name: FWBT11O-ACT-NBR<br>*/
	public String getoActNbr() {
		return readString(Pos.O_ACT_NBR, Len.O_ACT_NBR);
	}

	public void setoActNbrFmt(String oActNbrFmt) {
		writeString(Pos.O_ACT_NBR_FMT, oActNbrFmt, Len.O_ACT_NBR_FMT);
	}

	/**Original name: FWBT11O-ACT-NBR-FMT<br>*/
	public String getoActNbrFmt() {
		return readString(Pos.O_ACT_NBR_FMT, Len.O_ACT_NBR_FMT);
	}

	public void setoOvlOgnCd(String oOvlOgnCd) {
		writeString(Pos.O_OVL_OGN_CD, oOvlOgnCd, Len.O_OVL_OGN_CD);
	}

	/**Original name: FWBT11O-OVL-OGN-CD<br>
	 * <pre>                S = SERIES 3 ONLY
	 *                 M = MANUAL ONLY
	 *                 B = BOTH SERIES 3 AND MANUAL
	 *                 OVERALL ORIGIN CODE</pre>*/
	public String getoOvlOgnCd() {
		return readString(Pos.O_OVL_OGN_CD, Len.O_OVL_OGN_CD);
	}

	public void setoPolEffDt(String oPolEffDt) {
		writeString(Pos.O_POL_EFF_DT, oPolEffDt, Len.O_POL_EFF_DT);
	}

	/**Original name: FWBT11O-POL-EFF-DT<br>*/
	public String getoPolEffDt() {
		return readString(Pos.O_POL_EFF_DT, Len.O_POL_EFF_DT);
	}

	public void setoPolExpDt(String oPolExpDt) {
		writeString(Pos.O_POL_EXP_DT, oPolExpDt, Len.O_POL_EXP_DT);
	}

	/**Original name: FWBT11O-POL-EXP-DT<br>*/
	public String getoPolExpDt() {
		return readString(Pos.O_POL_EXP_DT, Len.O_POL_EXP_DT);
	}

	public void setoLobCd(String oLobCd) {
		writeString(Pos.O_LOB_CD, oLobCd, Len.O_LOB_CD);
	}

	/**Original name: FWBT11O-LOB-CD<br>
	 * <pre>                CPP, WC, UMB, ETC.</pre>*/
	public String getoLobCd() {
		return readString(Pos.O_LOB_CD, Len.O_LOB_CD);
	}

	public void setoLobDes(String oLobDes) {
		writeString(Pos.O_LOB_DES, oLobDes, Len.O_LOB_DES);
	}

	/**Original name: FWBT11O-LOB-DES<br>*/
	public String getoLobDes() {
		return readString(Pos.O_LOB_DES, Len.O_LOB_DES);
	}

	public void setoPolSymCd(String oPolSymCd) {
		writeString(Pos.O_POL_SYM_CD, oPolSymCd, Len.O_POL_SYM_CD);
	}

	/**Original name: FWBT11O-POL-SYM-CD<br>
	 * <pre>                SUBLINE OF POLICY. MOSTLY SAME AS THE LOB.
	 *                 CAN BE ASSIGNED RISK OR NC FACILITY (REINSURED)</pre>*/
	public String getoPolSymCd() {
		return readString(Pos.O_POL_SYM_CD, Len.O_POL_SYM_CD);
	}

	public void setoPolSymDes(String oPolSymDes) {
		writeString(Pos.O_POL_SYM_DES, oPolSymDes, Len.O_POL_SYM_DES);
	}

	/**Original name: FWBT11O-POL-SYM-DES<br>*/
	public String getoPolSymDes() {
		return readString(Pos.O_POL_SYM_DES, Len.O_POL_SYM_DES);
	}

	public void setoTobCd(String oTobCd) {
		writeString(Pos.O_TOB_CD, oTobCd, Len.O_TOB_CD);
	}

	/**Original name: FWBT11O-TOB-CD<br>*/
	public String getoTobCd() {
		return readString(Pos.O_TOB_CD, Len.O_TOB_CD);
	}

	public void setoTobDes(String oTobDes) {
		writeString(Pos.O_TOB_DES, oTobDes, Len.O_TOB_DES);
	}

	/**Original name: FWBT11O-TOB-DES<br>*/
	public String getoTobDes() {
		return readString(Pos.O_TOB_DES, Len.O_TOB_DES);
	}

	public void setoSegCd(String oSegCd) {
		writeString(Pos.O_SEG_CD, oSegCd, Len.O_SEG_CD);
	}

	/**Original name: FWBT11O-SEG-CD<br>*/
	public String getoSegCd() {
		return readString(Pos.O_SEG_CD, Len.O_SEG_CD);
	}

	public void setoSegDes(String oSegDes) {
		writeString(Pos.O_SEG_DES, oSegDes, Len.O_SEG_DES);
	}

	/**Original name: FWBT11O-SEG-DES<br>*/
	public String getoSegDes() {
		return readString(Pos.O_SEG_DES, Len.O_SEG_DES);
	}

	public void setoLgeActInd(char oLgeActInd) {
		writeChar(Pos.O_LGE_ACT_IND, oLgeActInd);
	}

	/**Original name: FWBT11O-LGE-ACT-IND<br>*/
	public char getoLgeActInd() {
		return readChar(Pos.O_LGE_ACT_IND);
	}

	public void setoCmpNbr(String oCmpNbr) {
		writeString(Pos.O_CMP_NBR, oCmpNbr, Len.O_CMP_NBR);
	}

	/**Original name: FWBT11O-CMP-NBR<br>*/
	public String getoCmpNbr() {
		return readString(Pos.O_CMP_NBR, Len.O_CMP_NBR);
	}

	public void setoCmpDes(String oCmpDes) {
		writeString(Pos.O_CMP_DES, oCmpDes, Len.O_CMP_DES);
	}

	/**Original name: FWBT11O-CMP-DES<br>*/
	public String getoCmpDes() {
		return readString(Pos.O_CMP_DES, Len.O_CMP_DES);
	}

	public void setoBrnCd(String oBrnCd) {
		writeString(Pos.O_BRN_CD, oBrnCd, Len.O_BRN_CD);
	}

	/**Original name: FWBT11O-BRN-CD<br>*/
	public String getoBrnCd() {
		return readString(Pos.O_BRN_CD, Len.O_BRN_CD);
	}

	public void setoBrnDes(String oBrnDes) {
		writeString(Pos.O_BRN_DES, oBrnDes, Len.O_BRN_DES);
	}

	/**Original name: FWBT11O-BRN-DES<br>*/
	public String getoBrnDes() {
		return readString(Pos.O_BRN_DES, Len.O_BRN_DES);
	}

	public void setoPriRskStAbb(String oPriRskStAbb) {
		writeString(Pos.O_PRI_RSK_ST_ABB, oPriRskStAbb, Len.O_PRI_RSK_ST_ABB);
	}

	/**Original name: FWBT11O-PRI-RSK-ST-ABB<br>*/
	public String getoPriRskStAbb() {
		return readString(Pos.O_PRI_RSK_ST_ABB, Len.O_PRI_RSK_ST_ABB);
	}

	public void setoPriRskStDes(String oPriRskStDes) {
		writeString(Pos.O_PRI_RSK_ST_DES, oPriRskStDes, Len.O_PRI_RSK_ST_DES);
	}

	/**Original name: FWBT11O-PRI-RSK-ST-DES<br>*/
	public String getoPriRskStDes() {
		return readString(Pos.O_PRI_RSK_ST_DES, Len.O_PRI_RSK_ST_DES);
	}

	public void setoPolTrmCd(String oPolTrmCd) {
		writeString(Pos.O_POL_TRM_CD, oPolTrmCd, Len.O_POL_TRM_CD);
	}

	/**Original name: FWBT11O-POL-TRM-CD<br>*/
	public String getoPolTrmCd() {
		return readString(Pos.O_POL_TRM_CD, Len.O_POL_TRM_CD);
	}

	public void setoPolTrmDes(String oPolTrmDes) {
		writeString(Pos.O_POL_TRM_DES, oPolTrmDes, Len.O_POL_TRM_DES);
	}

	/**Original name: FWBT11O-POL-TRM-DES<br>*/
	public String getoPolTrmDes() {
		return readString(Pos.O_POL_TRM_DES, Len.O_POL_TRM_DES);
	}

	public void setoLegEtyCd(String oLegEtyCd) {
		writeString(Pos.O_LEG_ETY_CD, oLegEtyCd, Len.O_LEG_ETY_CD);
	}

	/**Original name: FWBT11O-LEG-ETY-CD<br>*/
	public String getoLegEtyCd() {
		return readString(Pos.O_LEG_ETY_CD, Len.O_LEG_ETY_CD);
	}

	public void setoLegEtyDes(String oLegEtyDes) {
		writeString(Pos.O_LEG_ETY_DES, oLegEtyDes, Len.O_LEG_ETY_DES);
	}

	/**Original name: FWBT11O-LEG-ETY-DES<br>*/
	public String getoLegEtyDes() {
		return readString(Pos.O_LEG_ETY_DES, Len.O_LEG_ETY_DES);
	}

	public void setoCurStaCd(char oCurStaCd) {
		writeChar(Pos.O_CUR_STA_CD, oCurStaCd);
	}

	/**Original name: FWBT11O-CUR-STA-CD<br>*/
	public char getoCurStaCd() {
		return readChar(Pos.O_CUR_STA_CD);
	}

	public void setoCurStaDes(String oCurStaDes) {
		writeString(Pos.O_CUR_STA_DES, oCurStaDes, Len.O_CUR_STA_DES);
	}

	/**Original name: FWBT11O-CUR-STA-DES<br>*/
	public String getoCurStaDes() {
		return readString(Pos.O_CUR_STA_DES, Len.O_CUR_STA_DES);
	}

	public void setoRewInd(char oRewInd) {
		writeChar(Pos.O_REW_IND, oRewInd);
	}

	/**Original name: FWBT11O-REW-IND<br>*/
	public char getoRewInd() {
		return readChar(Pos.O_REW_IND);
	}

	public void setoPrePolNbr(String oPrePolNbr) {
		writeString(Pos.O_PRE_POL_NBR, oPrePolNbr, Len.O_PRE_POL_NBR);
	}

	/**Original name: FWBT11O-PRE-POL-NBR<br>*/
	public String getoPrePolNbr() {
		return readString(Pos.O_PRE_POL_NBR, Len.O_PRE_POL_NBR);
	}

	public void setoPrePolSymCd(String oPrePolSymCd) {
		writeString(Pos.O_PRE_POL_SYM_CD, oPrePolSymCd, Len.O_PRE_POL_SYM_CD);
	}

	/**Original name: FWBT11O-PRE-POL-SYM-CD<br>*/
	public String getoPrePolSymCd() {
		return readString(Pos.O_PRE_POL_SYM_CD, Len.O_PRE_POL_SYM_CD);
	}

	public void setoPrePolSymDes(String oPrePolSymDes) {
		writeString(Pos.O_PRE_POL_SYM_DES, oPrePolSymDes, Len.O_PRE_POL_SYM_DES);
	}

	/**Original name: FWBT11O-PRE-POL-SYM-DES<br>*/
	public String getoPrePolSymDes() {
		return readString(Pos.O_PRE_POL_SYM_DES, Len.O_PRE_POL_SYM_DES);
	}

	public void setoDsaDedAmt(int oDsaDedAmt) {
		writeInt(Pos.O_DSA_DED_AMT, oDsaDedAmt, Len.Int.O_DSA_DED_AMT);
	}

	/**Original name: FWBT11O-DSA-DED-AMT<br>*/
	public int getoDsaDedAmt() {
		return readNumDispInt(Pos.O_DSA_DED_AMT, Len.O_DSA_DED_AMT);
	}

	public void setoDsaDedOvrInd(char oDsaDedOvrInd) {
		writeChar(Pos.O_DSA_DED_OVR_IND, oDsaDedOvrInd);
	}

	/**Original name: FWBT11O-DSA-DED-OVR-IND<br>
	 * <pre>                    DID SYSTEM GENERATED DED GET OVERRIDDEN?</pre>*/
	public char getoDsaDedOvrInd() {
		return readChar(Pos.O_DSA_DED_OVR_IND);
	}

	public void setoSysBknDedAmt(int oSysBknDedAmt) {
		writeInt(Pos.O_SYS_BKN_DED_AMT, oSysBknDedAmt, Len.Int.O_SYS_BKN_DED_AMT);
	}

	/**Original name: FWBT11O-SYS-BKN-DED-AMT<br>*/
	public int getoSysBknDedAmt() {
		return readNumDispInt(Pos.O_SYS_BKN_DED_AMT, Len.O_SYS_BKN_DED_AMT);
	}

	public void setoSysBknDedOvrInd(char oSysBknDedOvrInd) {
		writeChar(Pos.O_SYS_BKN_DED_OVR_IND, oSysBknDedOvrInd);
	}

	/**Original name: FWBT11O-SYS-BKN-DED-OVR-IND<br>
	 * <pre>                    DID SYSTEM GENERATED DED GET OVERRIDDEN?</pre>*/
	public char getoSysBknDedOvrInd() {
		return readChar(Pos.O_SYS_BKN_DED_OVR_IND);
	}

	public void setoRtnAmt(long oRtnAmt) {
		writeLong(Pos.O_RTN_AMT, oRtnAmt, Len.Int.O_RTN_AMT);
	}

	/**Original name: FWBT11O-RTN-AMT<br>*/
	public long getoRtnAmt() {
		return readNumDispLong(Pos.O_RTN_AMT, Len.O_RTN_AMT);
	}

	public void setoSirDsc(AfDecimal oSirDsc) {
		writeDecimal(Pos.O_SIR_DSC, oSirDsc.copy());
	}

	/**Original name: FWBT11O-SIR-DSC<br>
	 * <pre>                09  FWBT11O-AGG-RTN-AMT         PIC S9(11)V.</pre>*/
	public AfDecimal getoSirDsc() {
		return readDecimal(Pos.O_SIR_DSC, Len.Int.O_SIR_DSC, Len.Fract.O_SIR_DSC);
	}

	public void setoSirRmdAmt(long oSirRmdAmt) {
		writeLong(Pos.O_SIR_RMD_AMT, oSirRmdAmt, Len.Int.O_SIR_RMD_AMT);
	}

	/**Original name: FWBT11O-SIR-RMD-AMT<br>*/
	public long getoSirRmdAmt() {
		return readNumDispLong(Pos.O_SIR_RMD_AMT, Len.O_SIR_RMD_AMT);
	}

	public void setoAudInd(char oAudInd) {
		writeChar(Pos.O_AUD_IND, oAudInd);
	}

	/**Original name: FWBT11O-AUD-IND<br>
	 * <pre>                    WILL THIS POLICY BE AUDITED?</pre>*/
	public char getoAudInd() {
		return readChar(Pos.O_AUD_IND);
	}

	public void setoAudStrCd(char oAudStrCd) {
		writeChar(Pos.O_AUD_STR_CD, oAudStrCd);
	}

	/**Original name: FWBT11O-AUD-STR-CD<br>
	 * <pre>                    Y-AUDIT STARTED  N-NOT STARTED
	 *                     R-REVISED AUDIT</pre>*/
	public char getoAudStrCd() {
		return readChar(Pos.O_AUD_STR_CD);
	}

	public void setoPolFutCncDt(String oPolFutCncDt) {
		writeString(Pos.O_POL_FUT_CNC_DT, oPolFutCncDt, Len.O_POL_FUT_CNC_DT);
	}

	/**Original name: FWBT11O-POL-FUT-CNC-DT<br>
	 * <pre>                    ENTERED ON REINSTATEMENT IF PROCESSOR
	 *                     INTENDS TO CANCEL POLICY AFTER ADJ.
	 *                     FUTURE CANCEL DATE IS USED BY RATE
	 *                     TO ANNUALIZE EXPOSURE.</pre>*/
	public String getoPolFutCncDt() {
		return readString(Pos.O_POL_FUT_CNC_DT, Len.O_POL_FUT_CNC_DT);
	}

	public void setoAceQteSeqNbr(int oAceQteSeqNbr) {
		writeInt(Pos.O_ACE_QTE_SEQ_NBR, oAceQteSeqNbr, Len.Int.O_ACE_QTE_SEQ_NBR);
	}

	/**Original name: FWBT11O-ACE-QTE-SEQ-NBR<br>
	 * <pre>                    QTE SEQ NBR OF QTE ACCEPTED FOR
	 *                     POLICY</pre>*/
	public int getoAceQteSeqNbr() {
		return readNumDispInt(Pos.O_ACE_QTE_SEQ_NBR, Len.O_ACE_QTE_SEQ_NBR);
	}

	public void setoQteStaCd(char oQteStaCd) {
		writeChar(Pos.O_QTE_STA_CD, oQteStaCd);
	}

	/**Original name: FWBT11O-QTE-STA-CD<br>*/
	public char getoQteStaCd() {
		return readChar(Pos.O_QTE_STA_CD);
	}

	public void setoQteStaDes(String oQteStaDes) {
		writeString(Pos.O_QTE_STA_DES, oQteStaDes, Len.O_QTE_STA_DES);
	}

	/**Original name: FWBT11O-QTE-STA-DES<br>*/
	public String getoQteStaDes() {
		return readString(Pos.O_QTE_STA_DES, Len.O_QTE_STA_DES);
	}

	public void setoQteGuaDt(String oQteGuaDt) {
		writeString(Pos.O_QTE_GUA_DT, oQteGuaDt, Len.O_QTE_GUA_DT);
	}

	/**Original name: FWBT11O-QTE-GUA-DT<br>*/
	public String getoQteGuaDt() {
		return readString(Pos.O_QTE_GUA_DT, Len.O_QTE_GUA_DT);
	}

	public void setoAthLossRqrInd(char oAthLossRqrInd) {
		writeChar(Pos.O_ATH_LOSS_RQR_IND, oAthLossRqrInd);
	}

	/**Original name: FWBT11O-ATH-LOSS-RQR-IND<br>*/
	public char getoAthLossRqrInd() {
		return readChar(Pos.O_ATH_LOSS_RQR_IND);
	}

	public void setoAthLossRecDt(String oAthLossRecDt) {
		writeString(Pos.O_ATH_LOSS_REC_DT, oAthLossRecDt, Len.O_ATH_LOSS_REC_DT);
	}

	/**Original name: FWBT11O-ATH-LOSS-REC-DT<br>*/
	public String getoAthLossRecDt() {
		return readString(Pos.O_ATH_LOSS_REC_DT, Len.O_ATH_LOSS_REC_DT);
	}

	public void setoUwRspDt(String oUwRspDt) {
		writeString(Pos.O_UW_RSP_DT, oUwRspDt, Len.O_UW_RSP_DT);
	}

	/**Original name: FWBT11O-UW-RSP-DT<br>*/
	public String getoUwRspDt() {
		return readString(Pos.O_UW_RSP_DT, Len.O_UW_RSP_DT);
	}

	public void setoWrtDt(String oWrtDt) {
		writeString(Pos.O_WRT_DT, oWrtDt, Len.O_WRT_DT);
	}

	/**Original name: FWBT11O-WRT-DT<br>*/
	public String getoWrtDt() {
		return readString(Pos.O_WRT_DT, Len.O_WRT_DT);
	}

	public void setoInsScoreCd(String oInsScoreCd) {
		writeString(Pos.O_INS_SCORE_CD, oInsScoreCd, Len.O_INS_SCORE_CD);
	}

	/**Original name: FWBT11O-INS-SCORE-CD<br>
	 * <pre>            07  FWBT11O-OGN-IPT-DT              PIC X(10).</pre>*/
	public String getoInsScoreCd() {
		return readString(Pos.O_INS_SCORE_CD, Len.O_INS_SCORE_CD);
	}

	public void setoNotCncDay(int oNotCncDay) {
		writeInt(Pos.O_NOT_CNC_DAY, oNotCncDay, Len.Int.O_NOT_CNC_DAY);
	}

	/**Original name: FWBT11O-NOT-CNC-DAY<br>*/
	public int getoNotCncDay() {
		return readNumDispInt(Pos.O_NOT_CNC_DAY, Len.O_NOT_CNC_DAY);
	}

	public void setoPolAppRecDt(String oPolAppRecDt) {
		writeString(Pos.O_POL_APP_REC_DT, oPolAppRecDt, Len.O_POL_APP_REC_DT);
	}

	/**Original name: FWBT11O-POL-APP-REC-DT<br>*/
	public String getoPolAppRecDt() {
		return readString(Pos.O_POL_APP_REC_DT, Len.O_POL_APP_REC_DT);
	}

	public void setoSplitBilledInd(char oSplitBilledInd) {
		writeChar(Pos.O_SPLIT_BILLED_IND, oSplitBilledInd);
	}

	/**Original name: FWBT11O-SPLIT-BILLED-IND<br>*/
	public char getoSplitBilledInd() {
		return readChar(Pos.O_SPLIT_BILLED_IND);
	}

	public void setoAlAdacInd(char oAlAdacInd) {
		writeChar(Pos.O_AL_ADAC_IND, oAlAdacInd);
	}

	/**Original name: FWBT11O-AL-ADAC-IND<br>*/
	public char getoAlAdacInd() {
		return readChar(Pos.O_AL_ADAC_IND);
	}

	public void setoPmaCd(char oPmaCd) {
		writeChar(Pos.O_PMA_CD, oPmaCd);
	}

	/**Original name: FWBT11O-PMA-CD<br>*/
	public char getoPmaCd() {
		return readChar(Pos.O_PMA_CD);
	}

	public void setoPmaDes(String oPmaDes) {
		writeString(Pos.O_PMA_DES, oPmaDes, Len.O_PMA_DES);
	}

	/**Original name: FWBT11O-PMA-DES<br>*/
	public String getoPmaDes() {
		return readString(Pos.O_PMA_DES, Len.O_PMA_DES);
	}

	public void setoReaAmdCd(String oReaAmdCd) {
		writeString(Pos.O_REA_AMD_CD, oReaAmdCd, Len.O_REA_AMD_CD);
	}

	/**Original name: FWBT11O-REA-AMD-CD<br>*/
	public String getoReaAmdCd() {
		return readString(Pos.O_REA_AMD_CD, Len.O_REA_AMD_CD);
	}

	public void setoReaAmdTrsDes(String oReaAmdTrsDes) {
		writeString(Pos.O_REA_AMD_TRS_DES, oReaAmdTrsDes, Len.O_REA_AMD_TRS_DES);
	}

	/**Original name: FWBT11O-REA-AMD-TRS-DES<br>*/
	public String getoReaAmdTrsDes() {
		return readString(Pos.O_REA_AMD_TRS_DES, Len.O_REA_AMD_TRS_DES);
	}

	public void setoReaAmdCdDes(String oReaAmdCdDes) {
		writeString(Pos.O_REA_AMD_CD_DES, oReaAmdCdDes, Len.O_REA_AMD_CD_DES);
	}

	/**Original name: FWBT11O-REA-AMD-CD-DES<br>*/
	public String getoReaAmdCdDes() {
		return readString(Pos.O_REA_AMD_CD_DES, Len.O_REA_AMD_CD_DES);
	}

	public void setoIssAcyTs(String oIssAcyTs) {
		writeString(Pos.O_ISS_ACY_TS, oIssAcyTs, Len.O_ISS_ACY_TS);
	}

	/**Original name: FWBT11O-ISS-ACY-TS<br>*/
	public String getoIssAcyTs() {
		return readString(Pos.O_ISS_ACY_TS, Len.O_ISS_ACY_TS);
	}

	public void setoEffDt(String oEffDt) {
		writeString(Pos.O_EFF_DT, oEffDt, Len.O_EFF_DT);
	}

	/**Original name: FWBT11O-EFF-DT<br>*/
	public String getoEffDt() {
		return readString(Pos.O_EFF_DT, Len.O_EFF_DT);
	}

	public void setoLstMdfAcyTs(String oLstMdfAcyTs) {
		writeString(Pos.O_LST_MDF_ACY_TS, oLstMdfAcyTs, Len.O_LST_MDF_ACY_TS);
	}

	/**Original name: FWBT11O-LST-MDF-ACY-TS<br>*/
	public String getoLstMdfAcyTs() {
		return readString(Pos.O_LST_MDF_ACY_TS, Len.O_LST_MDF_ACY_TS);
	}

	public void setoBondInd(char oBondInd) {
		writeChar(Pos.O_BOND_IND, oBondInd);
	}

	/**Original name: FWBT11O-BOND-IND<br>*/
	public char getoBondInd() {
		return readChar(Pos.O_BOND_IND);
	}

	public void setoWrtPrmAmt(long oWrtPrmAmt) {
		writeLong(Pos.O_WRT_PRM_AMT, oWrtPrmAmt, Len.Int.O_WRT_PRM_AMT);
	}

	/**Original name: FWBT11O-WRT-PRM-AMT<br>*/
	public long getoWrtPrmAmt() {
		return readNumDispLong(Pos.O_WRT_PRM_AMT, Len.O_WRT_PRM_AMT);
	}

	public void setoAcyPolInd(char oAcyPolInd) {
		writeChar(Pos.O_ACY_POL_IND, oAcyPolInd);
	}

	/**Original name: FWBT11O-ACY-POL-IND<br>*/
	public char getoAcyPolInd() {
		return readChar(Pos.O_ACY_POL_IND);
	}

	public void setoOgnOgnCd(char oOgnOgnCd) {
		writeChar(Pos.O_OGN_OGN_CD, oOgnOgnCd);
	}

	/**Original name: FWBT11O-OGN-OGN-CD<br>
	 * <pre>                S = SERIES 3 ONLY
	 *                 M = MANUAL ONLY
	 *                 B = BOTH SERIES 3 AND MANUAL
	 *                 FIRST TRANSACTION ORIGIN CODE</pre>*/
	public char getoOgnOgnCd() {
		return readChar(Pos.O_OGN_OGN_CD);
	}

	public void setoOgnTrsTypCd(char oOgnTrsTypCd) {
		writeChar(Pos.O_OGN_TRS_TYP_CD, oOgnTrsTypCd);
	}

	/**Original name: FWBT11O-OGN-TRS-TYP-CD<br>
	 * <pre>            IS THE ORIGINAL TRANSACTION A NEW BIZ QTE,
	 *             RENEWAL QTE, NEW BIZ, OR RENEWAL?</pre>*/
	public char getoOgnTrsTypCd() {
		return readChar(Pos.O_OGN_TRS_TYP_CD);
	}

	public void setoOgnTrsTypDes(String oOgnTrsTypDes) {
		writeString(Pos.O_OGN_TRS_TYP_DES, oOgnTrsTypDes, Len.O_OGN_TRS_TYP_DES);
	}

	/**Original name: FWBT11O-OGN-TRS-TYP-DES<br>*/
	public String getoOgnTrsTypDes() {
		return readString(Pos.O_OGN_TRS_TYP_DES, Len.O_OGN_TRS_TYP_DES);
	}

	public void setoOgnTrsIssInd(char oOgnTrsIssInd) {
		writeChar(Pos.O_OGN_TRS_ISS_IND, oOgnTrsIssInd);
	}

	/**Original name: FWBT11O-OGN-TRS-ISS-IND<br>*/
	public char getoOgnTrsIssInd() {
		return readChar(Pos.O_OGN_TRS_ISS_IND);
	}

	public void setoCurTrsTypCd(char oCurTrsTypCd) {
		writeChar(Pos.O_CUR_TRS_TYP_CD, oCurTrsTypCd);
	}

	/**Original name: FWBT11O-CUR-TRS-TYP-CD<br>
	 * <pre>            WHAT WAS THE LAST THING DONE ON THE POLICY
	 *             AND IS IT ISSUED?</pre>*/
	public char getoCurTrsTypCd() {
		return readChar(Pos.O_CUR_TRS_TYP_CD);
	}

	public void setoCurTrsTypDes(String oCurTrsTypDes) {
		writeString(Pos.O_CUR_TRS_TYP_DES, oCurTrsTypDes, Len.O_CUR_TRS_TYP_DES);
	}

	/**Original name: FWBT11O-CUR-TRS-TYP-DES<br>*/
	public String getoCurTrsTypDes() {
		return readString(Pos.O_CUR_TRS_TYP_DES, Len.O_CUR_TRS_TYP_DES);
	}

	public void setoCurTrsDt(String oCurTrsDt) {
		writeString(Pos.O_CUR_TRS_DT, oCurTrsDt, Len.O_CUR_TRS_DT);
	}

	/**Original name: FWBT11O-CUR-TRS-DT<br>*/
	public String getoCurTrsDt() {
		return readString(Pos.O_CUR_TRS_DT, Len.O_CUR_TRS_DT);
	}

	public void setoCurTrsPndInd(char oCurTrsPndInd) {
		writeChar(Pos.O_CUR_TRS_PND_IND, oCurTrsPndInd);
	}

	/**Original name: FWBT11O-CUR-TRS-PND-IND<br>*/
	public char getoCurTrsPndInd() {
		return readChar(Pos.O_CUR_TRS_PND_IND);
	}

	public void setoCurOgnCd(char oCurOgnCd) {
		writeChar(Pos.O_CUR_OGN_CD, oCurOgnCd);
	}

	/**Original name: FWBT11O-CUR-OGN-CD<br>
	 * <pre>                S = SERIES 3 ONLY
	 *                 M = MANUAL ONLY
	 *                 B = BOTH SERIES 3 AND MANUAL
	 *                 CURRENT TRANSACTION ORIGIN CODE</pre>*/
	public char getoCurOgnCd() {
		return readChar(Pos.O_CUR_OGN_CD);
	}

	public void setoWrapUpDt(String oWrapUpDt) {
		writeString(Pos.O_WRAP_UP_DT, oWrapUpDt, Len.O_WRAP_UP_DT);
	}

	/**Original name: FWBT11O-WRAP-UP-DT<br>
	 * <pre>                IS IT THROUGH ACT PRINT?</pre>*/
	public String getoWrapUpDt() {
		return readString(Pos.O_WRAP_UP_DT, Len.O_WRAP_UP_DT);
	}

	public void setoPolCncInd(char oPolCncInd) {
		writeChar(Pos.O_POL_CNC_IND, oPolCncInd);
	}

	/**Original name: FWBT11O-POL-CNC-IND<br>*/
	public char getoPolCncInd() {
		return readChar(Pos.O_POL_CNC_IND);
	}

	public void setoPndCncInd(char oPndCncInd) {
		writeChar(Pos.O_PND_CNC_IND, oPndCncInd);
	}

	/**Original name: FWBT11O-PND-CNC-IND<br>*/
	public char getoPndCncInd() {
		return readChar(Pos.O_PND_CNC_IND);
	}

	public void setoOosStrInd(char oOosStrInd) {
		writeChar(Pos.O_OOS_STR_IND, oOosStrInd);
	}

	/**Original name: FWBT11O-OOS-STR-IND<br>*/
	public char getoOosStrInd() {
		return readChar(Pos.O_OOS_STR_IND);
	}

	public void setoPolGoneMnlInd(char oPolGoneMnlInd) {
		writeChar(Pos.O_POL_GONE_MNL_IND, oPolGoneMnlInd);
	}

	/**Original name: FWBT11O-POL-GONE-MNL-IND<br>*/
	public char getoPolGoneMnlInd() {
		return readChar(Pos.O_POL_GONE_MNL_IND);
	}

	public void setoInsLinGoneMnlInd(char oInsLinGoneMnlInd) {
		writeChar(Pos.O_INS_LIN_GONE_MNL_IND, oInsLinGoneMnlInd);
	}

	/**Original name: FWBT11O-INS-LIN-GONE-MNL-IND<br>*/
	public char getoInsLinGoneMnlInd() {
		return readChar(Pos.O_INS_LIN_GONE_MNL_IND);
	}

	public void setoTmnInd(char oTmnInd) {
		writeChar(Pos.O_TMN_IND, oTmnInd);
	}

	/**Original name: FWBT11O-TMN-IND<br>*/
	public char getoTmnInd() {
		return readChar(Pos.O_TMN_IND);
	}

	public void setoExpRewInd(char oExpRewInd) {
		writeChar(Pos.O_EXP_REW_IND, oExpRewInd);
	}

	/**Original name: FWBT11O-EXP-REW-IND<br>*/
	public char getoExpRewInd() {
		return readChar(Pos.O_EXP_REW_IND);
	}

	public void setoWngMsg(int oWngMsgIdx, String oWngMsg) {
		int position = Pos.fwbt11oWngMsg(oWngMsgIdx - 1);
		writeString(position, oWngMsg, Len.O_WNG_MSG);
	}

	/**Original name: FWBT11O-WNG-MSG<br>*/
	public String getoWngMsg(int oWngMsgIdx) {
		int position = Pos.fwbt11oWngMsg(oWngMsgIdx - 1);
		return readString(position, Len.O_WNG_MSG);
	}

	public void setoCncDt(String oCncDt) {
		writeString(Pos.O_CNC_DT, oCncDt, Len.O_CNC_DT);
	}

	/**Original name: FWBT11O-CNC-DT<br>*/
	public String getoCncDt() {
		return readString(Pos.O_CNC_DT, Len.O_CNC_DT);
	}

	public void setoCncReaCd(String oCncReaCd) {
		writeString(Pos.O_CNC_REA_CD, oCncReaCd, Len.O_CNC_REA_CD);
	}

	/**Original name: FWBT11O-CNC-REA-CD<br>
	 * <pre>                    THIS IS THE CAPS CANCEL CODE</pre>*/
	public String getoCncReaCd() {
		return readString(Pos.O_CNC_REA_CD, Len.O_CNC_REA_CD);
	}

	public void setoCncReaDes(String oCncReaDes) {
		writeString(Pos.O_CNC_REA_DES, oCncReaDes, Len.O_CNC_REA_DES);
	}

	/**Original name: FWBT11O-CNC-REA-DES<br>*/
	public String getoCncReaDes() {
		return readString(Pos.O_CNC_REA_DES, Len.O_CNC_REA_DES);
	}

	public void setoWcCncCdFormatted(String oWcCncCd) {
		writeString(Pos.O_WC_CNC_CD, Trunc.toUnsignedNumeric(oWcCncCd, Len.O_WC_CNC_CD), Len.O_WC_CNC_CD);
	}

	/**Original name: FWBT11O-WC-CNC-CD<br>
	 * <pre>                    THIS IS THE BUREAU CANCEL CODE.
	 *                     FEDERATED DID NOT DEFINE VALUES.</pre>*/
	public short getoWcCncCd() {
		return readNumDispUnsignedShort(Pos.O_WC_CNC_CD, Len.O_WC_CNC_CD);
	}

	public void setoWcCncDes(String oWcCncDes) {
		writeString(Pos.O_WC_CNC_DES, oWcCncDes, Len.O_WC_CNC_DES);
	}

	/**Original name: FWBT11O-WC-CNC-DES<br>*/
	public String getoWcCncDes() {
		return readString(Pos.O_WC_CNC_DES, Len.O_WC_CNC_DES);
	}

	public void setoNinCltId(String oNinCltId) {
		writeString(Pos.O_NIN_CLT_ID, oNinCltId, Len.O_NIN_CLT_ID);
	}

	/**Original name: FWBT11O-NIN-CLT-ID<br>*/
	public String getoNinCltId() {
		return readString(Pos.O_NIN_CLT_ID, Len.O_NIN_CLT_ID);
	}

	public void setoNinCrmId(String oNinCrmId) {
		writeString(Pos.O_NIN_CRM_ID, oNinCrmId, Len.O_NIN_CRM_ID);
	}

	/**Original name: FWBT11O-NIN-CRM-ID<br>*/
	public String getoNinCrmId() {
		return readString(Pos.O_NIN_CRM_ID, Len.O_NIN_CRM_ID);
	}

	public void setoNinAdrSeqNbr(int oNinAdrSeqNbr) {
		writeInt(Pos.O_NIN_ADR_SEQ_NBR, oNinAdrSeqNbr, Len.Int.O_NIN_ADR_SEQ_NBR);
	}

	/**Original name: FWBT11O-NIN-ADR-SEQ-NBR<br>*/
	public int getoNinAdrSeqNbr() {
		return readNumDispInt(Pos.O_NIN_ADR_SEQ_NBR, Len.O_NIN_ADR_SEQ_NBR);
	}

	public void setoNmNbr(int oNmNbr) {
		writeInt(Pos.O_NM_NBR, oNmNbr, Len.Int.O_NM_NBR);
	}

	/**Original name: FWBT11O-NM-NBR<br>*/
	public int getoNmNbr() {
		return readNumDispInt(Pos.O_NM_NBR, Len.O_NM_NBR);
	}

	public void setoNmSeqNbr(int oNmSeqNbr) {
		writeInt(Pos.O_NM_SEQ_NBR, oNmSeqNbr, Len.Int.O_NM_SEQ_NBR);
	}

	/**Original name: FWBT11O-NM-SEQ-NBR<br>*/
	public int getoNmSeqNbr() {
		return readNumDispInt(Pos.O_NM_SEQ_NBR, Len.O_NM_SEQ_NBR);
	}

	public void setoWcLinkNbr(int oWcLinkNbr) {
		writeInt(Pos.O_WC_LINK_NBR, oWcLinkNbr, Len.Int.O_WC_LINK_NBR);
	}

	/**Original name: FWBT11O-WC-LINK-NBR<br>*/
	public int getoWcLinkNbr() {
		return readNumDispInt(Pos.O_WC_LINK_NBR, Len.O_WC_LINK_NBR);
	}

	public void setoFreFrmSeqNbr(int oFreFrmSeqNbr) {
		writeInt(Pos.O_FRE_FRM_SEQ_NBR, oFreFrmSeqNbr, Len.Int.O_FRE_FRM_SEQ_NBR);
	}

	/**Original name: FWBT11O-FRE-FRM-SEQ-NBR<br>*/
	public int getoFreFrmSeqNbr() {
		return readNumDispInt(Pos.O_FRE_FRM_SEQ_NBR, Len.O_FRE_FRM_SEQ_NBR);
	}

	public void setoNinIdvNmInd(char oNinIdvNmInd) {
		writeChar(Pos.O_NIN_IDV_NM_IND, oNinIdvNmInd);
	}

	/**Original name: FWBT11O-NIN-IDV-NM-IND<br>*/
	public char getoNinIdvNmInd() {
		return readChar(Pos.O_NIN_IDV_NM_IND);
	}

	public void setoNinDsyNm(String oNinDsyNm) {
		writeString(Pos.O_NIN_DSY_NM, oNinDsyNm, Len.O_NIN_DSY_NM);
	}

	/**Original name: FWBT11O-NIN-DSY-NM<br>
	 * <pre>                    DISPLAY NAME INCLUDES EXTENDED NAME</pre>*/
	public String getoNinDsyNm() {
		return readString(Pos.O_NIN_DSY_NM, Len.O_NIN_DSY_NM);
	}

	public void setoNinSrNm(String oNinSrNm) {
		writeString(Pos.O_NIN_SR_NM, oNinSrNm, Len.O_NIN_SR_NM);
	}

	/**Original name: FWBT11O-NIN-SR-NM<br>*/
	public String getoNinSrNm() {
		return readString(Pos.O_NIN_SR_NM, Len.O_NIN_SR_NM);
	}

	public void setoNinLstNm(String oNinLstNm) {
		writeString(Pos.O_NIN_LST_NM, oNinLstNm, Len.O_NIN_LST_NM);
	}

	/**Original name: FWBT11O-NIN-LST-NM<br>*/
	public String getoNinLstNm() {
		return readString(Pos.O_NIN_LST_NM, Len.O_NIN_LST_NM);
	}

	public void setoNinFstNm(String oNinFstNm) {
		writeString(Pos.O_NIN_FST_NM, oNinFstNm, Len.O_NIN_FST_NM);
	}

	/**Original name: FWBT11O-NIN-FST-NM<br>*/
	public String getoNinFstNm() {
		return readString(Pos.O_NIN_FST_NM, Len.O_NIN_FST_NM);
	}

	public void setoNinMdlNm(String oNinMdlNm) {
		writeString(Pos.O_NIN_MDL_NM, oNinMdlNm, Len.O_NIN_MDL_NM);
	}

	/**Original name: FWBT11O-NIN-MDL-NM<br>*/
	public String getoNinMdlNm() {
		return readString(Pos.O_NIN_MDL_NM, Len.O_NIN_MDL_NM);
	}

	public void setoNinSfx(String oNinSfx) {
		writeString(Pos.O_NIN_SFX, oNinSfx, Len.O_NIN_SFX);
	}

	/**Original name: FWBT11O-NIN-SFX<br>*/
	public String getoNinSfx() {
		return readString(Pos.O_NIN_SFX, Len.O_NIN_SFX);
	}

	public void setoNinPfx(String oNinPfx) {
		writeString(Pos.O_NIN_PFX, oNinPfx, Len.O_NIN_PFX);
	}

	/**Original name: FWBT11O-NIN-PFX<br>*/
	public String getoNinPfx() {
		return readString(Pos.O_NIN_PFX, Len.O_NIN_PFX);
	}

	public void setoNinAdr1(String oNinAdr1) {
		writeString(Pos.O_NIN_ADR1, oNinAdr1, Len.O_NIN_ADR1);
	}

	/**Original name: FWBT11O-NIN-ADR-1<br>*/
	public String getoNinAdr1() {
		return readString(Pos.O_NIN_ADR1, Len.O_NIN_ADR1);
	}

	public void setoNinAdr2(String oNinAdr2) {
		writeString(Pos.O_NIN_ADR2, oNinAdr2, Len.O_NIN_ADR2);
	}

	/**Original name: FWBT11O-NIN-ADR-2<br>*/
	public String getoNinAdr2() {
		return readString(Pos.O_NIN_ADR2, Len.O_NIN_ADR2);
	}

	public void setoNinCit(String oNinCit) {
		writeString(Pos.O_NIN_CIT, oNinCit, Len.O_NIN_CIT);
	}

	/**Original name: FWBT11O-NIN-CIT<br>*/
	public String getoNinCit() {
		return readString(Pos.O_NIN_CIT, Len.O_NIN_CIT);
	}

	public void setoNinStAbb(String oNinStAbb) {
		writeString(Pos.O_NIN_ST_ABB, oNinStAbb, Len.O_NIN_ST_ABB);
	}

	/**Original name: FWBT11O-NIN-ST-ABB<br>*/
	public String getoNinStAbb() {
		return readString(Pos.O_NIN_ST_ABB, Len.O_NIN_ST_ABB);
	}

	public void setoNinStDes(String oNinStDes) {
		writeString(Pos.O_NIN_ST_DES, oNinStDes, Len.O_NIN_ST_DES);
	}

	/**Original name: FWBT11O-NIN-ST-DES<br>*/
	public String getoNinStDes() {
		return readString(Pos.O_NIN_ST_DES, Len.O_NIN_ST_DES);
	}

	public void setoNinPstCd(String oNinPstCd) {
		writeString(Pos.O_NIN_PST_CD, oNinPstCd, Len.O_NIN_PST_CD);
	}

	/**Original name: FWBT11O-NIN-PST-CD<br>*/
	public String getoNinPstCd() {
		return readString(Pos.O_NIN_PST_CD, Len.O_NIN_PST_CD);
	}

	public void setoNinCty(String oNinCty) {
		writeString(Pos.O_NIN_CTY, oNinCty, Len.O_NIN_CTY);
	}

	/**Original name: FWBT11O-NIN-CTY<br>*/
	public String getoNinCty() {
		return readString(Pos.O_NIN_CTY, Len.O_NIN_CTY);
	}

	public void setoNinAdrId(String oNinAdrId) {
		writeString(Pos.O_NIN_ADR_ID, oNinAdrId, Len.O_NIN_ADR_ID);
	}

	/**Original name: FWBT11O-NIN-ADR-ID<br>*/
	public String getoNinAdrId() {
		return readString(Pos.O_NIN_ADR_ID, Len.O_NIN_ADR_ID);
	}

	public void setoNinAdrTypCd(String oNinAdrTypCd) {
		writeString(Pos.O_NIN_ADR_TYP_CD, oNinAdrTypCd, Len.O_NIN_ADR_TYP_CD);
	}

	/**Original name: FWBT11O-NIN-ADR-TYP-CD<br>*/
	public String getoNinAdrTypCd() {
		return readString(Pos.O_NIN_ADR_TYP_CD, Len.O_NIN_ADR_TYP_CD);
	}

	public void setoNinAdrTypDes(String oNinAdrTypDes) {
		writeString(Pos.O_NIN_ADR_TYP_DES, oNinAdrTypDes, Len.O_NIN_ADR_TYP_DES);
	}

	/**Original name: FWBT11O-NIN-ADR-TYP-DES<br>*/
	public String getoNinAdrTypDes() {
		return readString(Pos.O_NIN_ADR_TYP_DES, Len.O_NIN_ADR_TYP_DES);
	}

	public void setoNinCitxTaxId(String oNinCitxTaxId) {
		writeString(Pos.O_NIN_CITX_TAX_ID, oNinCitxTaxId, Len.O_NIN_CITX_TAX_ID);
	}

	/**Original name: FWBT11O-NIN-CITX-TAX-ID<br>*/
	public String getoNinCitxTaxId() {
		return readString(Pos.O_NIN_CITX_TAX_ID, Len.O_NIN_CITX_TAX_ID);
	}

	public void setoNinTaxTypeCd(String oNinTaxTypeCd) {
		writeString(Pos.O_NIN_TAX_TYPE_CD, oNinTaxTypeCd, Len.O_NIN_TAX_TYPE_CD);
	}

	/**Original name: FWBT11O-NIN-TAX-TYPE-CD<br>*/
	public String getoNinTaxTypeCd() {
		return readString(Pos.O_NIN_TAX_TYPE_CD, Len.O_NIN_TAX_TYPE_CD);
	}

	public void setoNinTaxTypeDes(String oNinTaxTypeDes) {
		writeString(Pos.O_NIN_TAX_TYPE_DES, oNinTaxTypeDes, Len.O_NIN_TAX_TYPE_DES);
	}

	/**Original name: FWBT11O-NIN-TAX-TYPE-DES<br>*/
	public String getoNinTaxTypeDes() {
		return readString(Pos.O_NIN_TAX_TYPE_DES, Len.O_NIN_TAX_TYPE_DES);
	}

	public void setoOwnCltId(String oOwnCltId) {
		writeString(Pos.O_OWN_CLT_ID, oOwnCltId, Len.O_OWN_CLT_ID);
	}

	/**Original name: FWBT11O-OWN-CLT-ID<br>*/
	public String getoOwnCltId() {
		return readString(Pos.O_OWN_CLT_ID, Len.O_OWN_CLT_ID);
	}

	public void setoOwnCrmId(String oOwnCrmId) {
		writeString(Pos.O_OWN_CRM_ID, oOwnCrmId, Len.O_OWN_CRM_ID);
	}

	/**Original name: FWBT11O-OWN-CRM-ID<br>*/
	public String getoOwnCrmId() {
		return readString(Pos.O_OWN_CRM_ID, Len.O_OWN_CRM_ID);
	}

	public void setoOwnAdrSeqNbr(int oOwnAdrSeqNbr) {
		writeInt(Pos.O_OWN_ADR_SEQ_NBR, oOwnAdrSeqNbr, Len.Int.O_OWN_ADR_SEQ_NBR);
	}

	/**Original name: FWBT11O-OWN-ADR-SEQ-NBR<br>*/
	public int getoOwnAdrSeqNbr() {
		return readNumDispInt(Pos.O_OWN_ADR_SEQ_NBR, Len.O_OWN_ADR_SEQ_NBR);
	}

	public void setoOwnIdvNmInd(char oOwnIdvNmInd) {
		writeChar(Pos.O_OWN_IDV_NM_IND, oOwnIdvNmInd);
	}

	/**Original name: FWBT11O-OWN-IDV-NM-IND<br>*/
	public char getoOwnIdvNmInd() {
		return readChar(Pos.O_OWN_IDV_NM_IND);
	}

	public void setoOwnDsyNm(String oOwnDsyNm) {
		writeString(Pos.O_OWN_DSY_NM, oOwnDsyNm, Len.O_OWN_DSY_NM);
	}

	/**Original name: FWBT11O-OWN-DSY-NM<br>*/
	public String getoOwnDsyNm() {
		return readString(Pos.O_OWN_DSY_NM, Len.O_OWN_DSY_NM);
	}

	public void setoOwnSrNm(String oOwnSrNm) {
		writeString(Pos.O_OWN_SR_NM, oOwnSrNm, Len.O_OWN_SR_NM);
	}

	/**Original name: FWBT11O-OWN-SR-NM<br>*/
	public String getoOwnSrNm() {
		return readString(Pos.O_OWN_SR_NM, Len.O_OWN_SR_NM);
	}

	public void setoOwnLstNm(String oOwnLstNm) {
		writeString(Pos.O_OWN_LST_NM, oOwnLstNm, Len.O_OWN_LST_NM);
	}

	/**Original name: FWBT11O-OWN-LST-NM<br>*/
	public String getoOwnLstNm() {
		return readString(Pos.O_OWN_LST_NM, Len.O_OWN_LST_NM);
	}

	public void setoOwnFstNm(String oOwnFstNm) {
		writeString(Pos.O_OWN_FST_NM, oOwnFstNm, Len.O_OWN_FST_NM);
	}

	/**Original name: FWBT11O-OWN-FST-NM<br>*/
	public String getoOwnFstNm() {
		return readString(Pos.O_OWN_FST_NM, Len.O_OWN_FST_NM);
	}

	public void setoOwnMdlNm(String oOwnMdlNm) {
		writeString(Pos.O_OWN_MDL_NM, oOwnMdlNm, Len.O_OWN_MDL_NM);
	}

	/**Original name: FWBT11O-OWN-MDL-NM<br>*/
	public String getoOwnMdlNm() {
		return readString(Pos.O_OWN_MDL_NM, Len.O_OWN_MDL_NM);
	}

	public void setoOwnSfx(String oOwnSfx) {
		writeString(Pos.O_OWN_SFX, oOwnSfx, Len.O_OWN_SFX);
	}

	/**Original name: FWBT11O-OWN-SFX<br>*/
	public String getoOwnSfx() {
		return readString(Pos.O_OWN_SFX, Len.O_OWN_SFX);
	}

	public void setoOwnPfx(String oOwnPfx) {
		writeString(Pos.O_OWN_PFX, oOwnPfx, Len.O_OWN_PFX);
	}

	/**Original name: FWBT11O-OWN-PFX<br>*/
	public String getoOwnPfx() {
		return readString(Pos.O_OWN_PFX, Len.O_OWN_PFX);
	}

	public void setoOwnAdr1(String oOwnAdr1) {
		writeString(Pos.O_OWN_ADR1, oOwnAdr1, Len.O_OWN_ADR1);
	}

	/**Original name: FWBT11O-OWN-ADR-1<br>*/
	public String getoOwnAdr1() {
		return readString(Pos.O_OWN_ADR1, Len.O_OWN_ADR1);
	}

	public void setoOwnAdr2(String oOwnAdr2) {
		writeString(Pos.O_OWN_ADR2, oOwnAdr2, Len.O_OWN_ADR2);
	}

	/**Original name: FWBT11O-OWN-ADR-2<br>*/
	public String getoOwnAdr2() {
		return readString(Pos.O_OWN_ADR2, Len.O_OWN_ADR2);
	}

	public void setoOwnCit(String oOwnCit) {
		writeString(Pos.O_OWN_CIT, oOwnCit, Len.O_OWN_CIT);
	}

	/**Original name: FWBT11O-OWN-CIT<br>*/
	public String getoOwnCit() {
		return readString(Pos.O_OWN_CIT, Len.O_OWN_CIT);
	}

	public void setoOwnStAbb(String oOwnStAbb) {
		writeString(Pos.O_OWN_ST_ABB, oOwnStAbb, Len.O_OWN_ST_ABB);
	}

	/**Original name: FWBT11O-OWN-ST-ABB<br>*/
	public String getoOwnStAbb() {
		return readString(Pos.O_OWN_ST_ABB, Len.O_OWN_ST_ABB);
	}

	public void setoOwnStDes(String oOwnStDes) {
		writeString(Pos.O_OWN_ST_DES, oOwnStDes, Len.O_OWN_ST_DES);
	}

	/**Original name: FWBT11O-OWN-ST-DES<br>*/
	public String getoOwnStDes() {
		return readString(Pos.O_OWN_ST_DES, Len.O_OWN_ST_DES);
	}

	public void setoOwnPstCd(String oOwnPstCd) {
		writeString(Pos.O_OWN_PST_CD, oOwnPstCd, Len.O_OWN_PST_CD);
	}

	/**Original name: FWBT11O-OWN-PST-CD<br>*/
	public String getoOwnPstCd() {
		return readString(Pos.O_OWN_PST_CD, Len.O_OWN_PST_CD);
	}

	public void setoOwnCty(String oOwnCty) {
		writeString(Pos.O_OWN_CTY, oOwnCty, Len.O_OWN_CTY);
	}

	/**Original name: FWBT11O-OWN-CTY<br>*/
	public String getoOwnCty() {
		return readString(Pos.O_OWN_CTY, Len.O_OWN_CTY);
	}

	public void setoOwnAdrId(String oOwnAdrId) {
		writeString(Pos.O_OWN_ADR_ID, oOwnAdrId, Len.O_OWN_ADR_ID);
	}

	/**Original name: FWBT11O-OWN-ADR-ID<br>*/
	public String getoOwnAdrId() {
		return readString(Pos.O_OWN_ADR_ID, Len.O_OWN_ADR_ID);
	}

	public void setoInsLinCd(int oInsLinCdIdx, String oInsLinCd) {
		int position = Pos.fwbt11oInsLinCd(oInsLinCdIdx - 1);
		writeString(position, oInsLinCd, Len.O_INS_LIN_CD);
	}

	/**Original name: FWBT11O-INS-LIN-CD<br>*/
	public String getoInsLinCd(int oInsLinCdIdx) {
		int position = Pos.fwbt11oInsLinCd(oInsLinCdIdx - 1);
		return readString(position, Len.O_INS_LIN_CD);
	}

	public void setoInsLinDes(int oInsLinDesIdx, String oInsLinDes) {
		int position = Pos.fwbt11oInsLinDes(oInsLinDesIdx - 1);
		writeString(position, oInsLinDes, Len.O_INS_LIN_DES);
	}

	/**Original name: FWBT11O-INS-LIN-DES<br>*/
	public String getoInsLinDes(int oInsLinDesIdx) {
		int position = Pos.fwbt11oInsLinDes(oInsLinDesIdx - 1);
		return readString(position, Len.O_INS_LIN_DES);
	}

	public void setoOgnEffDt(int oOgnEffDtIdx, String oOgnEffDt) {
		int position = Pos.fwbt11oOgnEffDt(oOgnEffDtIdx - 1);
		writeString(position, oOgnEffDt, Len.O_OGN_EFF_DT);
	}

	/**Original name: FWBT11O-OGN-EFF-DT<br>*/
	public String getoOgnEffDt(int oOgnEffDtIdx) {
		int position = Pos.fwbt11oOgnEffDt(oOgnEffDtIdx - 1);
		return readString(position, Len.O_OGN_EFF_DT);
	}

	public void setoExpDt(int oExpDtIdx, String oExpDt) {
		int position = Pos.fwbt11oExpDt(oExpDtIdx - 1);
		writeString(position, oExpDt, Len.O_EXP_DT);
	}

	/**Original name: FWBT11O-EXP-DT<br>*/
	public String getoExpDt(int oExpDtIdx) {
		int position = Pos.fwbt11oExpDt(oExpDtIdx - 1);
		return readString(position, Len.O_EXP_DT);
	}

	public void setoGoneMnlInd(int oGoneMnlIndIdx, char oGoneMnlInd) {
		int position = Pos.fwbt11oGoneMnlInd(oGoneMnlIndIdx - 1);
		writeChar(position, oGoneMnlInd);
	}

	/**Original name: FWBT11O-GONE-MNL-IND<br>*/
	public char getoGoneMnlInd(int oGoneMnlIndIdx) {
		int position = Pos.fwbt11oGoneMnlInd(oGoneMnlIndIdx - 1);
		return readChar(position);
	}

	public void setoRtInd(int oRtIndIdx, char oRtInd) {
		int position = Pos.fwbt11oRtInd(oRtIndIdx - 1);
		writeChar(position, oRtInd);
	}

	/**Original name: FWBT11O-RT-IND<br>*/
	public char getoRtInd(int oRtIndIdx) {
		int position = Pos.fwbt11oRtInd(oRtIndIdx - 1);
		return readChar(position);
	}

	public void setoMnlInsLinCd(int oMnlInsLinCdIdx, String oMnlInsLinCd) {
		int position = Pos.fwbt11oMnlInsLinCd(oMnlInsLinCdIdx - 1);
		writeString(position, oMnlInsLinCd, Len.O_MNL_INS_LIN_CD);
	}

	/**Original name: FWBT11O-MNL-INS-LIN-CD<br>*/
	public String getoMnlInsLinCd(int oMnlInsLinCdIdx) {
		int position = Pos.fwbt11oMnlInsLinCd(oMnlInsLinCdIdx - 1);
		return readString(position, Len.O_MNL_INS_LIN_CD);
	}

	public void setoMnlInsLinDes(int oMnlInsLinDesIdx, String oMnlInsLinDes) {
		int position = Pos.fwbt11oMnlInsLinDes(oMnlInsLinDesIdx - 1);
		writeString(position, oMnlInsLinDes, Len.O_MNL_INS_LIN_DES);
	}

	/**Original name: FWBT11O-MNL-INS-LIN-DES<br>*/
	public String getoMnlInsLinDes(int oMnlInsLinDesIdx) {
		int position = Pos.fwbt11oMnlInsLinDes(oMnlInsLinDesIdx - 1);
		return readString(position, Len.O_MNL_INS_LIN_DES);
	}

	public void setoMnlOgnEffDt(int oMnlOgnEffDtIdx, String oMnlOgnEffDt) {
		int position = Pos.fwbt11oMnlOgnEffDt(oMnlOgnEffDtIdx - 1);
		writeString(position, oMnlOgnEffDt, Len.O_MNL_OGN_EFF_DT);
	}

	/**Original name: FWBT11O-MNL-OGN-EFF-DT<br>*/
	public String getoMnlOgnEffDt(int oMnlOgnEffDtIdx) {
		int position = Pos.fwbt11oMnlOgnEffDt(oMnlOgnEffDtIdx - 1);
		return readString(position, Len.O_MNL_OGN_EFF_DT);
	}

	public void setoMnlExpDt(int oMnlExpDtIdx, String oMnlExpDt) {
		int position = Pos.fwbt11oMnlExpDt(oMnlExpDtIdx - 1);
		writeString(position, oMnlExpDt, Len.O_MNL_EXP_DT);
	}

	/**Original name: FWBT11O-MNL-EXP-DT<br>*/
	public String getoMnlExpDt(int oMnlExpDtIdx) {
		int position = Pos.fwbt11oMnlExpDt(oMnlExpDtIdx - 1);
		return readString(position, Len.O_MNL_EXP_DT);
	}

	public void setoBondAmt(long oBondAmt) {
		writeLong(Pos.O_BOND_AMT, oBondAmt, Len.Int.O_BOND_AMT);
	}

	/**Original name: FWBT11O-BOND-AMT<br>*/
	public long getoBondAmt() {
		return readNumDispLong(Pos.O_BOND_AMT, Len.O_BOND_AMT);
	}

	public void setoBondCmt(String oBondCmt) {
		writeString(Pos.O_BOND_CMT, oBondCmt, Len.O_BOND_CMT);
	}

	/**Original name: FWBT11O-BOND-CMT<br>*/
	public String getoBondCmt() {
		return readString(Pos.O_BOND_CMT, Len.O_BOND_CMT);
	}

	public void setoBondTypCd(String oBondTypCd) {
		writeString(Pos.O_BOND_TYP_CD, oBondTypCd, Len.O_BOND_TYP_CD);
	}

	/**Original name: FWBT11O-BOND-TYP-CD<br>*/
	public String getoBondTypCd() {
		return readString(Pos.O_BOND_TYP_CD, Len.O_BOND_TYP_CD);
	}

	public void setoBondTypDes(String oBondTypDes) {
		writeString(Pos.O_BOND_TYP_DES, oBondTypDes, Len.O_BOND_TYP_DES);
	}

	/**Original name: FWBT11O-BOND-TYP-DES<br>*/
	public String getoBondTypDes() {
		return readString(Pos.O_BOND_TYP_DES, Len.O_BOND_TYP_DES);
	}

	public void setoOgnTobCd(String oOgnTobCd) {
		writeString(Pos.O_OGN_TOB_CD, oOgnTobCd, Len.O_OGN_TOB_CD);
	}

	/**Original name: FWBT11O-OGN-TOB-CD<br>*/
	public String getoOgnTobCd() {
		return readString(Pos.O_OGN_TOB_CD, Len.O_OGN_TOB_CD);
	}

	public void setoPolPriRskStCd(String oPolPriRskStCd) {
		writeString(Pos.O_POL_PRI_RSK_ST_CD, oPolPriRskStCd, Len.O_POL_PRI_RSK_ST_CD);
	}

	/**Original name: FWBT11O-POL-PRI-RSK-ST-CD<br>*/
	public String getoPolPriRskStCd() {
		return readString(Pos.O_POL_PRI_RSK_ST_CD, Len.O_POL_PRI_RSK_ST_CD);
	}

	public void setoPolPriRskCtyCd(String oPolPriRskCtyCd) {
		writeString(Pos.O_POL_PRI_RSK_CTY_CD, oPolPriRskCtyCd, Len.O_POL_PRI_RSK_CTY_CD);
	}

	/**Original name: FWBT11O-POL-PRI-RSK-CTY-CD<br>*/
	public String getoPolPriRskCtyCd() {
		return readString(Pos.O_POL_PRI_RSK_CTY_CD, Len.O_POL_PRI_RSK_CTY_CD);
	}

	public void setoPolPriRskTwnCd(String oPolPriRskTwnCd) {
		writeString(Pos.O_POL_PRI_RSK_TWN_CD, oPolPriRskTwnCd, Len.O_POL_PRI_RSK_TWN_CD);
	}

	/**Original name: FWBT11O-POL-PRI-RSK-TWN-CD<br>*/
	public String getoPolPriRskTwnCd() {
		return readString(Pos.O_POL_PRI_RSK_TWN_CD, Len.O_POL_PRI_RSK_TWN_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_FWBT0011_ROW = 1;
		public static final int SERVICE_INPUTS = WS_FWBT0011_ROW;
		public static final int I_TECHNICAL_KEYS = SERVICE_INPUTS;
		public static final int I_TK_POL_ID = I_TECHNICAL_KEYS;
		public static final int I_TK_POL_NBR = I_TK_POL_ID + Len.I_TK_POL_ID;
		public static final int I_TK_POL_EFF_DT = I_TK_POL_NBR + Len.I_TK_POL_NBR;
		public static final int I_TK_POL_EXP_DT = I_TK_POL_EFF_DT + Len.I_TK_POL_EFF_DT;
		public static final int I_TK_QTE_NBR = I_TK_POL_EXP_DT + Len.I_TK_POL_EXP_DT;
		public static final int I_TK_ACT_NBR = I_TK_QTE_NBR + Len.I_TK_QTE_NBR;
		public static final int I_TK_WRT_DT = I_TK_ACT_NBR + Len.I_TK_ACT_NBR;
		public static final int I_TK_PRI_RSK_ST_ABB = I_TK_WRT_DT + Len.I_TK_WRT_DT;
		public static final int I_TK_LOB_CD = I_TK_PRI_RSK_ST_ABB + Len.I_TK_PRI_RSK_ST_ABB;
		public static final int I_TK_CMP_NBR = I_TK_LOB_CD + Len.I_TK_LOB_CD;
		public static final int I_TK_TOB_CD = I_TK_CMP_NBR + Len.I_TK_CMP_NBR;
		public static final int I_TK_SEG_CD = I_TK_TOB_CD + Len.I_TK_TOB_CD;
		public static final int I_TK_LGE_ACT_IND = I_TK_SEG_CD + Len.I_TK_SEG_CD;
		public static final int I_TK_AL_ADAC_IND = I_TK_LGE_ACT_IND + Len.I_TK_LGE_ACT_IND;
		public static final int FLR1 = I_TK_AL_ADAC_IND + Len.I_TK_AL_ADAC_IND;
		public static final int I_POL_NBR = FLR1 + Len.FLR1;
		public static final int I_AS_OF_DT = I_POL_NBR + Len.I_POL_NBR;
		public static final int I_POL_EFF_DT = I_AS_OF_DT + Len.I_AS_OF_DT;
		public static final int I_POL_EXP_DT = I_POL_EFF_DT + Len.I_POL_EFF_DT;
		public static final int I_QTE_NBR = I_POL_EXP_DT + Len.I_POL_EXP_DT;
		public static final int I_USR_ID = I_QTE_NBR + Len.I_QTE_NBR;
		public static final int FLR2 = I_USR_ID + Len.I_USR_ID;
		public static final int SERVICE_OUTPUTS = FLR2 + Len.FLR1;
		public static final int O_TECHNICAL_KEYS = SERVICE_OUTPUTS;
		public static final int O_TK_POL_ID = O_TECHNICAL_KEYS;
		public static final int O_TK_POL_NBR = O_TK_POL_ID + Len.O_TK_POL_ID;
		public static final int O_TK_POL_EFF_DT = O_TK_POL_NBR + Len.O_TK_POL_NBR;
		public static final int O_TK_POL_EXP_DT = O_TK_POL_EFF_DT + Len.O_TK_POL_EFF_DT;
		public static final int O_TK_QTE_NBR = O_TK_POL_EXP_DT + Len.O_TK_POL_EXP_DT;
		public static final int O_TK_ACT_NBR = O_TK_QTE_NBR + Len.O_TK_QTE_NBR;
		public static final int O_TK_WRT_DT = O_TK_ACT_NBR + Len.O_TK_ACT_NBR;
		public static final int O_TK_PRI_RSK_ST_ABB = O_TK_WRT_DT + Len.O_TK_WRT_DT;
		public static final int O_TK_LOB_CD = O_TK_PRI_RSK_ST_ABB + Len.O_TK_PRI_RSK_ST_ABB;
		public static final int O_TK_CMP_NBR = O_TK_LOB_CD + Len.O_TK_LOB_CD;
		public static final int O_TK_TOB_CD = O_TK_CMP_NBR + Len.O_TK_CMP_NBR;
		public static final int O_TK_SEG_CD = O_TK_TOB_CD + Len.O_TK_TOB_CD;
		public static final int O_TK_LGE_ACT_IND = O_TK_SEG_CD + Len.O_TK_SEG_CD;
		public static final int O_TK_AL_ADAC_IND = O_TK_LGE_ACT_IND + Len.O_TK_LGE_ACT_IND;
		public static final int FLR3 = O_TK_AL_ADAC_IND + Len.O_TK_AL_ADAC_IND;
		public static final int O_POL_CURRENT_INFO = FLR3 + Len.FLR1;
		public static final int O_POL_NBR = O_POL_CURRENT_INFO;
		public static final int O_ACT_NBR = O_POL_NBR + Len.O_POL_NBR;
		public static final int O_ACT_NBR_FMT = O_ACT_NBR + Len.O_ACT_NBR;
		public static final int O_OVL_OGN_CD = O_ACT_NBR_FMT + Len.O_ACT_NBR_FMT;
		public static final int O_POL_PERIOD = O_OVL_OGN_CD + Len.O_OVL_OGN_CD;
		public static final int O_POL_EFF_DT = O_POL_PERIOD;
		public static final int O_POL_EXP_DT = O_POL_EFF_DT + Len.O_POL_EFF_DT;
		public static final int O_LINE_OF_BUSINESS = O_POL_EXP_DT + Len.O_POL_EXP_DT;
		public static final int O_LOB_CD = O_LINE_OF_BUSINESS;
		public static final int O_LOB_DES = O_LOB_CD + Len.O_LOB_CD;
		public static final int O_POL_SYM_CD = O_LOB_DES + Len.O_LOB_DES;
		public static final int O_POL_SYM_DES = O_POL_SYM_CD + Len.O_POL_SYM_CD;
		public static final int O_TYPE_OF_BUSINESS = O_POL_SYM_DES + Len.O_POL_SYM_DES;
		public static final int O_TOB_CD = O_TYPE_OF_BUSINESS;
		public static final int O_TOB_DES = O_TOB_CD + Len.O_TOB_CD;
		public static final int O_SEGMENT_INFORMATION = O_TOB_DES + Len.O_TOB_DES;
		public static final int O_SEG_CD = O_SEGMENT_INFORMATION;
		public static final int O_SEG_DES = O_SEG_CD + Len.O_SEG_CD;
		public static final int O_LGE_ACT_IND = O_SEG_DES + Len.O_SEG_DES;
		public static final int O_COMPANY_BRANCH = O_LGE_ACT_IND + Len.O_LGE_ACT_IND;
		public static final int O_CMP_NBR = O_COMPANY_BRANCH;
		public static final int O_CMP_DES = O_CMP_NBR + Len.O_CMP_NBR;
		public static final int O_BRN_CD = O_CMP_DES + Len.O_CMP_DES;
		public static final int O_BRN_DES = O_BRN_CD + Len.O_BRN_CD;
		public static final int O_PRIMARY_RISK_ST = O_BRN_DES + Len.O_BRN_DES;
		public static final int O_PRI_RSK_ST_ABB = O_PRIMARY_RISK_ST;
		public static final int O_PRI_RSK_ST_DES = O_PRI_RSK_ST_ABB + Len.O_PRI_RSK_ST_ABB;
		public static final int O_POLICY_TERM = O_PRI_RSK_ST_DES + Len.O_PRI_RSK_ST_DES;
		public static final int O_POL_TRM_CD = O_POLICY_TERM;
		public static final int O_POL_TRM_DES = O_POL_TRM_CD + Len.O_POL_TRM_CD;
		public static final int O_NIN_LEGAL_ENTITY = O_POL_TRM_DES + Len.O_POL_TRM_DES;
		public static final int O_LEG_ETY_CD = O_NIN_LEGAL_ENTITY;
		public static final int O_LEG_ETY_DES = O_LEG_ETY_CD + Len.O_LEG_ETY_CD;
		public static final int O_CURRENT_STATUS = O_LEG_ETY_DES + Len.O_LEG_ETY_DES;
		public static final int O_CUR_STA_CD = O_CURRENT_STATUS;
		public static final int O_CUR_STA_DES = O_CUR_STA_CD + Len.O_CUR_STA_CD;
		public static final int O_CANCEL_REWRITE_INFO = O_CUR_STA_DES + Len.O_CUR_STA_DES;
		public static final int O_REW_IND = O_CANCEL_REWRITE_INFO;
		public static final int O_PRE_POL_NBR = O_REW_IND + Len.O_REW_IND;
		public static final int O_PRE_POL_SYM_CD = O_PRE_POL_NBR + Len.O_PRE_POL_NBR;
		public static final int O_PRE_POL_SYM_DES = O_PRE_POL_SYM_CD + Len.O_PRE_POL_SYM_CD;
		public static final int O_DEDUCTIBLES = O_PRE_POL_SYM_DES + Len.O_PRE_POL_SYM_DES;
		public static final int O_DSA_DED_AMT = O_DEDUCTIBLES;
		public static final int O_DSA_DED_OVR_IND = O_DSA_DED_AMT + Len.O_DSA_DED_AMT;
		public static final int O_SYS_BKN_DED_AMT = O_DSA_DED_OVR_IND + Len.O_DSA_DED_OVR_IND;
		public static final int O_SYS_BKN_DED_OVR_IND = O_SYS_BKN_DED_AMT + Len.O_SYS_BKN_DED_AMT;
		public static final int O_SIR_INFO = O_SYS_BKN_DED_OVR_IND + Len.O_SYS_BKN_DED_OVR_IND;
		public static final int O_RTN_AMT = O_SIR_INFO;
		public static final int FLR4 = O_RTN_AMT + Len.O_RTN_AMT;
		public static final int O_SIR_DSC = FLR4 + Len.FLR4;
		public static final int O_SIR_RMD_AMT = O_SIR_DSC + Len.O_SIR_DSC;
		public static final int O_AUDIT_INFO = O_SIR_RMD_AMT + Len.O_SIR_RMD_AMT;
		public static final int O_AUD_IND = O_AUDIT_INFO;
		public static final int O_AUD_STR_CD = O_AUD_IND + Len.O_AUD_IND;
		public static final int O_POL_FUT_CNC_DT = O_AUD_STR_CD + Len.O_AUD_STR_CD;
		public static final int O_QTE_REPORTING_INFO = O_POL_FUT_CNC_DT + Len.O_POL_FUT_CNC_DT;
		public static final int O_ACE_QTE_SEQ_NBR = O_QTE_REPORTING_INFO;
		public static final int O_QTE_STA_CD = O_ACE_QTE_SEQ_NBR + Len.O_ACE_QTE_SEQ_NBR;
		public static final int O_QTE_STA_DES = O_QTE_STA_CD + Len.O_QTE_STA_CD;
		public static final int O_QTE_GUA_DT = O_QTE_STA_DES + Len.O_QTE_STA_DES;
		public static final int O_ATH_LOSS_RQR_IND = O_QTE_GUA_DT + Len.O_QTE_GUA_DT;
		public static final int O_ATH_LOSS_REC_DT = O_ATH_LOSS_RQR_IND + Len.O_ATH_LOSS_RQR_IND;
		public static final int O_UW_RSP_DT = O_ATH_LOSS_REC_DT + Len.O_ATH_LOSS_REC_DT;
		public static final int O_WRT_DT = O_UW_RSP_DT + Len.O_UW_RSP_DT;
		public static final int FLR5 = O_WRT_DT + Len.O_WRT_DT;
		public static final int O_INS_SCORE_CD = FLR5 + Len.FLR5;
		public static final int O_NOT_CNC_DAY = O_INS_SCORE_CD + Len.O_INS_SCORE_CD;
		public static final int O_POL_APP_REC_DT = O_NOT_CNC_DAY + Len.O_NOT_CNC_DAY;
		public static final int O_SPLIT_BILLED_IND = O_POL_APP_REC_DT + Len.O_POL_APP_REC_DT;
		public static final int O_AL_ADAC_IND = O_SPLIT_BILLED_IND + Len.O_SPLIT_BILLED_IND;
		public static final int O_PMA = O_AL_ADAC_IND + Len.O_AL_ADAC_IND;
		public static final int O_PMA_CD = O_PMA;
		public static final int O_PMA_DES = O_PMA_CD + Len.O_PMA_CD;
		public static final int O_TRS_REA_AMD = O_PMA_DES + Len.O_PMA_DES;
		public static final int O_REA_AMD_CD = O_TRS_REA_AMD;
		public static final int O_REA_AMD_TRS_DES = O_REA_AMD_CD + Len.O_REA_AMD_CD;
		public static final int O_REA_AMD_CD_DES = O_REA_AMD_TRS_DES + Len.O_REA_AMD_TRS_DES;
		public static final int O_ISS_ACY_TS = O_REA_AMD_CD_DES + Len.O_REA_AMD_CD_DES;
		public static final int O_EFF_DT = O_ISS_ACY_TS + Len.O_ISS_ACY_TS;
		public static final int O_LST_MDF_ACY_TS = O_EFF_DT + Len.O_EFF_DT;
		public static final int O_BOND_IND = O_LST_MDF_ACY_TS + Len.O_LST_MDF_ACY_TS;
		public static final int O_WRT_PRM_AMT = O_BOND_IND + Len.O_BOND_IND;
		public static final int O_ACY_POL_IND = O_WRT_PRM_AMT + Len.O_WRT_PRM_AMT;
		public static final int FLR6 = O_ACY_POL_IND + Len.O_ACY_POL_IND;
		public static final int O_OGN_POL_STATUS_INFO = FLR6 + Len.FLR6;
		public static final int O_OGN_OGN_CD = O_OGN_POL_STATUS_INFO;
		public static final int O_OGN_TRS_TYP_CD = O_OGN_OGN_CD + Len.O_OGN_OGN_CD;
		public static final int O_OGN_TRS_TYP_DES = O_OGN_TRS_TYP_CD + Len.O_OGN_TRS_TYP_CD;
		public static final int O_OGN_TRS_ISS_IND = O_OGN_TRS_TYP_DES + Len.O_OGN_TRS_TYP_DES;
		public static final int O_CURRENT_TRANSACTION = O_OGN_TRS_ISS_IND + Len.O_OGN_TRS_ISS_IND;
		public static final int O_CUR_TRS_TYP_CD = O_CURRENT_TRANSACTION;
		public static final int O_CUR_TRS_TYP_DES = O_CUR_TRS_TYP_CD + Len.O_CUR_TRS_TYP_CD;
		public static final int O_CUR_TRS_DT = O_CUR_TRS_TYP_DES + Len.O_CUR_TRS_TYP_DES;
		public static final int O_CUR_TRS_PND_IND = O_CUR_TRS_DT + Len.O_CUR_TRS_DT;
		public static final int O_CUR_OGN_CD = O_CUR_TRS_PND_IND + Len.O_CUR_TRS_PND_IND;
		public static final int O_WRAP_UP_DT = O_CUR_OGN_CD + Len.O_CUR_OGN_CD;
		public static final int O_POL_STATUS_ALERT = O_WRAP_UP_DT + Len.O_WRAP_UP_DT;
		public static final int O_POL_CNC_IND = O_POL_STATUS_ALERT;
		public static final int O_PND_CNC_IND = O_POL_CNC_IND + Len.O_POL_CNC_IND;
		public static final int O_OOS_STR_IND = O_PND_CNC_IND + Len.O_PND_CNC_IND;
		public static final int O_POL_GONE_MNL_IND = O_OOS_STR_IND + Len.O_OOS_STR_IND;
		public static final int O_INS_LIN_GONE_MNL_IND = O_POL_GONE_MNL_IND + Len.O_POL_GONE_MNL_IND;
		public static final int O_TMN_IND = O_INS_LIN_GONE_MNL_IND + Len.O_INS_LIN_GONE_MNL_IND;
		public static final int O_EXP_REW_IND = O_TMN_IND + Len.O_TMN_IND;
		public static final int O_CANCEL_INFO = fwbt11oWngMsg(O_WNG_MSG_MAXOCCURS - 1) + Len.O_WNG_MSG;
		public static final int O_CNC_DT = O_CANCEL_INFO;
		public static final int O_CNC_REA_CD = O_CNC_DT + Len.O_CNC_DT;
		public static final int O_CNC_REA_DES = O_CNC_REA_CD + Len.O_CNC_REA_CD;
		public static final int O_WC_CNC_CD = O_CNC_REA_DES + Len.O_CNC_REA_DES;
		public static final int O_WC_CNC_DES = O_WC_CNC_CD + Len.O_WC_CNC_CD;
		public static final int FLR7 = O_WC_CNC_DES + Len.O_WC_CNC_DES;
		public static final int O_NAMED_INSURED = FLR7 + Len.FLR1;
		public static final int FWBT110_NIN_TECHNICAL_KEYS = O_NAMED_INSURED;
		public static final int O_NIN_CLT_ID = FWBT110_NIN_TECHNICAL_KEYS;
		public static final int O_NIN_CRM_ID = O_NIN_CLT_ID + Len.O_NIN_CLT_ID;
		public static final int O_NIN_ADR_SEQ_NBR = O_NIN_CRM_ID + Len.O_NIN_CRM_ID;
		public static final int O_NM_NBR = O_NIN_ADR_SEQ_NBR + Len.O_NIN_ADR_SEQ_NBR;
		public static final int O_NM_SEQ_NBR = O_NM_NBR + Len.O_NM_NBR;
		public static final int O_WC_LINK_NBR = O_NM_SEQ_NBR + Len.O_NM_SEQ_NBR;
		public static final int O_FRE_FRM_SEQ_NBR = O_WC_LINK_NBR + Len.O_WC_LINK_NBR;
		public static final int O_NIN_NAME = O_FRE_FRM_SEQ_NBR + Len.O_FRE_FRM_SEQ_NBR;
		public static final int O_NIN_IDV_NM_IND = O_NIN_NAME;
		public static final int O_NIN_DSY_NM = O_NIN_IDV_NM_IND + Len.O_NIN_IDV_NM_IND;
		public static final int O_NIN_SR_NM = O_NIN_DSY_NM + Len.O_NIN_DSY_NM;
		public static final int O_NIN_LST_NM = O_NIN_SR_NM + Len.O_NIN_SR_NM;
		public static final int O_NIN_FST_NM = O_NIN_LST_NM + Len.O_NIN_LST_NM;
		public static final int O_NIN_MDL_NM = O_NIN_FST_NM + Len.O_NIN_FST_NM;
		public static final int O_NIN_SFX = O_NIN_MDL_NM + Len.O_NIN_MDL_NM;
		public static final int O_NIN_PFX = O_NIN_SFX + Len.O_NIN_SFX;
		public static final int O_NIN_ADDRESS = O_NIN_PFX + Len.O_NIN_PFX;
		public static final int O_NIN_ADR1 = O_NIN_ADDRESS;
		public static final int O_NIN_ADR2 = O_NIN_ADR1 + Len.O_NIN_ADR1;
		public static final int O_NIN_CIT = O_NIN_ADR2 + Len.O_NIN_ADR2;
		public static final int O_NIN_ST_ABB = O_NIN_CIT + Len.O_NIN_CIT;
		public static final int O_NIN_ST_DES = O_NIN_ST_ABB + Len.O_NIN_ST_ABB;
		public static final int O_NIN_PST_CD = O_NIN_ST_DES + Len.O_NIN_ST_DES;
		public static final int O_NIN_CTY = O_NIN_PST_CD + Len.O_NIN_PST_CD;
		public static final int O_NIN_ADR_ID = O_NIN_CTY + Len.O_NIN_CTY;
		public static final int O_NIN_ADR_TYP_CD = O_NIN_ADR_ID + Len.O_NIN_ADR_ID;
		public static final int O_NIN_ADR_TYP_DES = O_NIN_ADR_TYP_CD + Len.O_NIN_ADR_TYP_CD;
		public static final int O_NIN_TAX_INF = O_NIN_ADR_TYP_DES + Len.O_NIN_ADR_TYP_DES;
		public static final int O_NIN_CITX_TAX_ID = O_NIN_TAX_INF;
		public static final int O_NIN_TAX_TYPE_CD = O_NIN_CITX_TAX_ID + Len.O_NIN_CITX_TAX_ID;
		public static final int O_NIN_TAX_TYPE_DES = O_NIN_TAX_TYPE_CD + Len.O_NIN_TAX_TYPE_CD;
		public static final int FLR8 = O_NIN_TAX_TYPE_DES + Len.O_NIN_TAX_TYPE_DES;
		public static final int O_ACCOUNT_OWNER = FLR8 + Len.FLR4;
		public static final int FWBT110_OWN_TECHNICAL_KEYS = O_ACCOUNT_OWNER;
		public static final int O_OWN_CLT_ID = FWBT110_OWN_TECHNICAL_KEYS;
		public static final int O_OWN_CRM_ID = O_OWN_CLT_ID + Len.O_OWN_CLT_ID;
		public static final int O_OWN_ADR_SEQ_NBR = O_OWN_CRM_ID + Len.O_OWN_CRM_ID;
		public static final int O_OWN_NAME = O_OWN_ADR_SEQ_NBR + Len.O_OWN_ADR_SEQ_NBR;
		public static final int O_OWN_IDV_NM_IND = O_OWN_NAME;
		public static final int O_OWN_DSY_NM = O_OWN_IDV_NM_IND + Len.O_OWN_IDV_NM_IND;
		public static final int O_OWN_SR_NM = O_OWN_DSY_NM + Len.O_OWN_DSY_NM;
		public static final int O_OWN_LST_NM = O_OWN_SR_NM + Len.O_OWN_SR_NM;
		public static final int O_OWN_FST_NM = O_OWN_LST_NM + Len.O_OWN_LST_NM;
		public static final int O_OWN_MDL_NM = O_OWN_FST_NM + Len.O_OWN_FST_NM;
		public static final int O_OWN_SFX = O_OWN_MDL_NM + Len.O_OWN_MDL_NM;
		public static final int O_OWN_PFX = O_OWN_SFX + Len.O_OWN_SFX;
		public static final int O_OWN_ADDRESS = O_OWN_PFX + Len.O_OWN_PFX;
		public static final int O_OWN_ADR1 = O_OWN_ADDRESS;
		public static final int O_OWN_ADR2 = O_OWN_ADR1 + Len.O_OWN_ADR1;
		public static final int O_OWN_CIT = O_OWN_ADR2 + Len.O_OWN_ADR2;
		public static final int O_OWN_ST_ABB = O_OWN_CIT + Len.O_OWN_CIT;
		public static final int O_OWN_ST_DES = O_OWN_ST_ABB + Len.O_OWN_ST_ABB;
		public static final int O_OWN_PST_CD = O_OWN_ST_DES + Len.O_OWN_ST_DES;
		public static final int O_OWN_CTY = O_OWN_PST_CD + Len.O_OWN_PST_CD;
		public static final int O_OWN_ADR_ID = O_OWN_CTY + Len.O_OWN_CTY;
		public static final int FLR9 = O_OWN_ADR_ID + Len.O_OWN_ADR_ID;
		public static final int O_BOND_DETAIL_INFO = flr11(O_MANUAL_LINES_MAXOCCURS - 1) + Len.FLR10;
		public static final int O_BOND_AMT = O_BOND_DETAIL_INFO;
		public static final int O_BOND_CMT = O_BOND_AMT + Len.O_BOND_AMT;
		public static final int O_BOND_TYP_CD = O_BOND_CMT + Len.O_BOND_CMT;
		public static final int O_BOND_TYP_DES = O_BOND_TYP_CD + Len.O_BOND_TYP_CD;
		public static final int FLR12 = O_BOND_TYP_DES + Len.O_BOND_TYP_DES;
		public static final int O_OGN_TOB_CD = FLR12 + Len.FLR10;
		public static final int O_PRI_RSK_INFO = O_OGN_TOB_CD + Len.O_OGN_TOB_CD;
		public static final int O_POL_PRI_RSK_ST_CD = O_PRI_RSK_INFO;
		public static final int O_POL_PRI_RSK_CTY_CD = O_POL_PRI_RSK_ST_CD + Len.O_POL_PRI_RSK_ST_CD;
		public static final int O_POL_PRI_RSK_TWN_CD = O_POL_PRI_RSK_CTY_CD + Len.O_POL_PRI_RSK_CTY_CD;
		public static final int FLR13 = O_POL_PRI_RSK_TWN_CD + Len.O_POL_PRI_RSK_TWN_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int fwbt11oWngMsg(int idx) {
			return O_EXP_REW_IND + Len.O_EXP_REW_IND + idx * Len.O_WNG_MSG;
		}

		public static int fwbt11oAutomatedLines(int idx) {
			return FLR9 + Len.FLR9 + idx * Len.O_AUTOMATED_LINES;
		}

		public static int fwbt11oInsLinCd(int idx) {
			return fwbt11oAutomatedLines(idx);
		}

		public static int fwbt11oInsLinDes(int idx) {
			return fwbt11oInsLinCd(idx) + Len.O_INS_LIN_CD;
		}

		public static int fwbt11oOgnEffDt(int idx) {
			return fwbt11oInsLinDes(idx) + Len.O_INS_LIN_DES;
		}

		public static int fwbt11oExpDt(int idx) {
			return fwbt11oOgnEffDt(idx) + Len.O_OGN_EFF_DT;
		}

		public static int fwbt11oGoneMnlInd(int idx) {
			return fwbt11oExpDt(idx) + Len.O_EXP_DT;
		}

		public static int fwbt11oRtInd(int idx) {
			return fwbt11oGoneMnlInd(idx) + Len.O_GONE_MNL_IND;
		}

		public static int flr10(int idx) {
			return fwbt11oRtInd(idx) + Len.O_RT_IND;
		}

		public static int fwbt11oManualLines(int idx) {
			return flr10(O_AUTOMATED_LINES_MAXOCCURS - 1) + Len.FLR10 + idx * Len.O_MANUAL_LINES;
		}

		public static int fwbt11oMnlInsLinCd(int idx) {
			return fwbt11oManualLines(idx);
		}

		public static int fwbt11oMnlInsLinDes(int idx) {
			return fwbt11oMnlInsLinCd(idx) + Len.O_MNL_INS_LIN_CD;
		}

		public static int fwbt11oMnlOgnEffDt(int idx) {
			return fwbt11oMnlInsLinDes(idx) + Len.O_MNL_INS_LIN_DES;
		}

		public static int fwbt11oMnlExpDt(int idx) {
			return fwbt11oMnlOgnEffDt(idx) + Len.O_MNL_OGN_EFF_DT;
		}

		public static int flr11(int idx) {
			return fwbt11oMnlExpDt(idx) + Len.O_MNL_EXP_DT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_TK_POL_ID = 16;
		public static final int I_TK_POL_NBR = 25;
		public static final int I_TK_POL_EFF_DT = 10;
		public static final int I_TK_POL_EXP_DT = 10;
		public static final int I_TK_QTE_NBR = 5;
		public static final int I_TK_ACT_NBR = 9;
		public static final int I_TK_WRT_DT = 10;
		public static final int I_TK_PRI_RSK_ST_ABB = 3;
		public static final int I_TK_LOB_CD = 3;
		public static final int I_TK_CMP_NBR = 2;
		public static final int I_TK_TOB_CD = 3;
		public static final int I_TK_SEG_CD = 3;
		public static final int I_TK_LGE_ACT_IND = 1;
		public static final int I_TK_AL_ADAC_IND = 1;
		public static final int FLR1 = 100;
		public static final int I_POL_NBR = 25;
		public static final int I_AS_OF_DT = 10;
		public static final int I_POL_EFF_DT = 10;
		public static final int I_POL_EXP_DT = 10;
		public static final int I_QTE_NBR = 5;
		public static final int I_USR_ID = 8;
		public static final int O_TK_POL_ID = 16;
		public static final int O_TK_POL_NBR = 25;
		public static final int O_TK_POL_EFF_DT = 10;
		public static final int O_TK_POL_EXP_DT = 10;
		public static final int O_TK_QTE_NBR = 5;
		public static final int O_TK_ACT_NBR = 9;
		public static final int O_TK_WRT_DT = 10;
		public static final int O_TK_PRI_RSK_ST_ABB = 3;
		public static final int O_TK_LOB_CD = 3;
		public static final int O_TK_CMP_NBR = 2;
		public static final int O_TK_TOB_CD = 3;
		public static final int O_TK_SEG_CD = 3;
		public static final int O_TK_LGE_ACT_IND = 1;
		public static final int O_TK_AL_ADAC_IND = 1;
		public static final int O_POL_NBR = 25;
		public static final int O_ACT_NBR = 9;
		public static final int O_ACT_NBR_FMT = 9;
		public static final int O_OVL_OGN_CD = 2;
		public static final int O_POL_EFF_DT = 10;
		public static final int O_POL_EXP_DT = 10;
		public static final int O_LOB_CD = 3;
		public static final int O_LOB_DES = 45;
		public static final int O_POL_SYM_CD = 3;
		public static final int O_POL_SYM_DES = 40;
		public static final int O_TOB_CD = 4;
		public static final int O_TOB_DES = 40;
		public static final int O_SEG_CD = 3;
		public static final int O_SEG_DES = 40;
		public static final int O_LGE_ACT_IND = 1;
		public static final int O_CMP_NBR = 2;
		public static final int O_CMP_DES = 45;
		public static final int O_BRN_CD = 2;
		public static final int O_BRN_DES = 45;
		public static final int O_PRI_RSK_ST_ABB = 3;
		public static final int O_PRI_RSK_ST_DES = 25;
		public static final int O_POL_TRM_CD = 3;
		public static final int O_POL_TRM_DES = 20;
		public static final int O_LEG_ETY_CD = 2;
		public static final int O_LEG_ETY_DES = 40;
		public static final int O_CUR_STA_CD = 1;
		public static final int O_CUR_STA_DES = 40;
		public static final int O_REW_IND = 1;
		public static final int O_PRE_POL_NBR = 25;
		public static final int O_PRE_POL_SYM_CD = 3;
		public static final int O_PRE_POL_SYM_DES = 40;
		public static final int O_DSA_DED_AMT = 7;
		public static final int O_DSA_DED_OVR_IND = 1;
		public static final int O_SYS_BKN_DED_AMT = 7;
		public static final int O_SYS_BKN_DED_OVR_IND = 1;
		public static final int O_RTN_AMT = 11;
		public static final int FLR4 = 11;
		public static final int O_SIR_DSC = 5;
		public static final int O_SIR_RMD_AMT = 11;
		public static final int O_AUD_IND = 1;
		public static final int O_AUD_STR_CD = 1;
		public static final int O_POL_FUT_CNC_DT = 10;
		public static final int O_ACE_QTE_SEQ_NBR = 5;
		public static final int O_QTE_STA_CD = 1;
		public static final int O_QTE_STA_DES = 20;
		public static final int O_QTE_GUA_DT = 10;
		public static final int O_ATH_LOSS_RQR_IND = 1;
		public static final int O_ATH_LOSS_REC_DT = 10;
		public static final int O_UW_RSP_DT = 10;
		public static final int O_WRT_DT = 10;
		public static final int FLR5 = 10;
		public static final int O_INS_SCORE_CD = 3;
		public static final int O_NOT_CNC_DAY = 5;
		public static final int O_POL_APP_REC_DT = 10;
		public static final int O_SPLIT_BILLED_IND = 1;
		public static final int O_AL_ADAC_IND = 1;
		public static final int O_PMA_CD = 1;
		public static final int O_PMA_DES = 30;
		public static final int O_REA_AMD_CD = 3;
		public static final int O_REA_AMD_TRS_DES = 15;
		public static final int O_REA_AMD_CD_DES = 70;
		public static final int O_ISS_ACY_TS = 26;
		public static final int O_EFF_DT = 10;
		public static final int O_LST_MDF_ACY_TS = 26;
		public static final int O_BOND_IND = 1;
		public static final int O_WRT_PRM_AMT = 12;
		public static final int O_ACY_POL_IND = 1;
		public static final int FLR6 = 500;
		public static final int O_OGN_OGN_CD = 1;
		public static final int O_OGN_TRS_TYP_CD = 1;
		public static final int O_OGN_TRS_TYP_DES = 35;
		public static final int O_OGN_TRS_ISS_IND = 1;
		public static final int O_CUR_TRS_TYP_CD = 1;
		public static final int O_CUR_TRS_TYP_DES = 35;
		public static final int O_CUR_TRS_DT = 10;
		public static final int O_CUR_TRS_PND_IND = 1;
		public static final int O_CUR_OGN_CD = 1;
		public static final int O_WRAP_UP_DT = 10;
		public static final int O_POL_CNC_IND = 1;
		public static final int O_PND_CNC_IND = 1;
		public static final int O_OOS_STR_IND = 1;
		public static final int O_POL_GONE_MNL_IND = 1;
		public static final int O_INS_LIN_GONE_MNL_IND = 1;
		public static final int O_TMN_IND = 1;
		public static final int O_EXP_REW_IND = 1;
		public static final int O_WNG_MSG = 80;
		public static final int O_CNC_DT = 10;
		public static final int O_CNC_REA_CD = 3;
		public static final int O_CNC_REA_DES = 85;
		public static final int O_WC_CNC_CD = 3;
		public static final int O_WC_CNC_DES = 66;
		public static final int O_NIN_CLT_ID = 20;
		public static final int O_NIN_CRM_ID = 36;
		public static final int O_NIN_ADR_SEQ_NBR = 5;
		public static final int O_NM_NBR = 5;
		public static final int O_NM_SEQ_NBR = 5;
		public static final int O_WC_LINK_NBR = 5;
		public static final int O_FRE_FRM_SEQ_NBR = 5;
		public static final int O_NIN_IDV_NM_IND = 1;
		public static final int O_NIN_DSY_NM = 120;
		public static final int O_NIN_SR_NM = 60;
		public static final int O_NIN_LST_NM = 60;
		public static final int O_NIN_FST_NM = 30;
		public static final int O_NIN_MDL_NM = 30;
		public static final int O_NIN_SFX = 4;
		public static final int O_NIN_PFX = 4;
		public static final int O_NIN_ADR1 = 45;
		public static final int O_NIN_ADR2 = 45;
		public static final int O_NIN_CIT = 30;
		public static final int O_NIN_ST_ABB = 3;
		public static final int O_NIN_ST_DES = 40;
		public static final int O_NIN_PST_CD = 13;
		public static final int O_NIN_CTY = 30;
		public static final int O_NIN_ADR_ID = 20;
		public static final int O_NIN_ADR_TYP_CD = 4;
		public static final int O_NIN_ADR_TYP_DES = 40;
		public static final int O_NIN_CITX_TAX_ID = 15;
		public static final int O_NIN_TAX_TYPE_CD = 10;
		public static final int O_NIN_TAX_TYPE_DES = 40;
		public static final int O_OWN_CLT_ID = 20;
		public static final int O_OWN_CRM_ID = 36;
		public static final int O_OWN_ADR_SEQ_NBR = 5;
		public static final int O_OWN_IDV_NM_IND = 1;
		public static final int O_OWN_DSY_NM = 120;
		public static final int O_OWN_SR_NM = 60;
		public static final int O_OWN_LST_NM = 60;
		public static final int O_OWN_FST_NM = 30;
		public static final int O_OWN_MDL_NM = 30;
		public static final int O_OWN_SFX = 4;
		public static final int O_OWN_PFX = 4;
		public static final int O_OWN_ADR1 = 45;
		public static final int O_OWN_ADR2 = 45;
		public static final int O_OWN_CIT = 30;
		public static final int O_OWN_ST_ABB = 3;
		public static final int O_OWN_ST_DES = 40;
		public static final int O_OWN_PST_CD = 13;
		public static final int O_OWN_CTY = 30;
		public static final int O_OWN_ADR_ID = 20;
		public static final int FLR9 = 80;
		public static final int O_INS_LIN_CD = 3;
		public static final int O_INS_LIN_DES = 50;
		public static final int O_OGN_EFF_DT = 10;
		public static final int O_EXP_DT = 10;
		public static final int O_GONE_MNL_IND = 1;
		public static final int O_RT_IND = 1;
		public static final int FLR10 = 50;
		public static final int O_AUTOMATED_LINES = O_INS_LIN_CD + O_INS_LIN_DES + O_OGN_EFF_DT + O_EXP_DT + O_GONE_MNL_IND + O_RT_IND + FLR10;
		public static final int O_MNL_INS_LIN_CD = 3;
		public static final int O_MNL_INS_LIN_DES = 50;
		public static final int O_MNL_OGN_EFF_DT = 10;
		public static final int O_MNL_EXP_DT = 10;
		public static final int O_MANUAL_LINES = O_MNL_INS_LIN_CD + O_MNL_INS_LIN_DES + O_MNL_OGN_EFF_DT + O_MNL_EXP_DT + FLR10;
		public static final int O_BOND_AMT = 11;
		public static final int O_BOND_CMT = 250;
		public static final int O_BOND_TYP_CD = 4;
		public static final int O_BOND_TYP_DES = 75;
		public static final int O_OGN_TOB_CD = 4;
		public static final int O_POL_PRI_RSK_ST_CD = 3;
		public static final int O_POL_PRI_RSK_CTY_CD = 3;
		public static final int O_POL_PRI_RSK_TWN_CD = 4;
		public static final int I_TECHNICAL_KEYS = I_TK_POL_ID + I_TK_POL_NBR + I_TK_POL_EFF_DT + I_TK_POL_EXP_DT + I_TK_QTE_NBR + I_TK_ACT_NBR
				+ I_TK_WRT_DT + I_TK_PRI_RSK_ST_ABB + I_TK_LOB_CD + I_TK_CMP_NBR + I_TK_TOB_CD + I_TK_SEG_CD + I_TK_LGE_ACT_IND + I_TK_AL_ADAC_IND
				+ FLR1;
		public static final int SERVICE_INPUTS = I_TECHNICAL_KEYS + I_POL_NBR + I_AS_OF_DT + I_POL_EFF_DT + I_POL_EXP_DT + I_QTE_NBR + I_USR_ID
				+ FLR1;
		public static final int O_TECHNICAL_KEYS = O_TK_POL_ID + O_TK_POL_NBR + O_TK_POL_EFF_DT + O_TK_POL_EXP_DT + O_TK_QTE_NBR + O_TK_ACT_NBR
				+ O_TK_WRT_DT + O_TK_PRI_RSK_ST_ABB + O_TK_LOB_CD + O_TK_CMP_NBR + O_TK_TOB_CD + O_TK_SEG_CD + O_TK_LGE_ACT_IND + O_TK_AL_ADAC_IND
				+ FLR1;
		public static final int O_POL_PERIOD = O_POL_EFF_DT + O_POL_EXP_DT;
		public static final int O_LINE_OF_BUSINESS = O_LOB_CD + O_LOB_DES + O_POL_SYM_CD + O_POL_SYM_DES;
		public static final int O_TYPE_OF_BUSINESS = O_TOB_CD + O_TOB_DES;
		public static final int O_SEGMENT_INFORMATION = O_SEG_CD + O_SEG_DES;
		public static final int O_COMPANY_BRANCH = O_CMP_NBR + O_CMP_DES + O_BRN_CD + O_BRN_DES;
		public static final int O_PRIMARY_RISK_ST = O_PRI_RSK_ST_ABB + O_PRI_RSK_ST_DES;
		public static final int O_POLICY_TERM = O_POL_TRM_CD + O_POL_TRM_DES;
		public static final int O_NIN_LEGAL_ENTITY = O_LEG_ETY_CD + O_LEG_ETY_DES;
		public static final int O_CURRENT_STATUS = O_CUR_STA_CD + O_CUR_STA_DES;
		public static final int O_CANCEL_REWRITE_INFO = O_REW_IND + O_PRE_POL_NBR + O_PRE_POL_SYM_CD + O_PRE_POL_SYM_DES;
		public static final int O_DEDUCTIBLES = O_DSA_DED_AMT + O_DSA_DED_OVR_IND + O_SYS_BKN_DED_AMT + O_SYS_BKN_DED_OVR_IND;
		public static final int O_SIR_INFO = O_RTN_AMT + O_SIR_DSC + O_SIR_RMD_AMT + FLR4;
		public static final int O_AUDIT_INFO = O_AUD_IND + O_AUD_STR_CD + O_POL_FUT_CNC_DT;
		public static final int O_QTE_REPORTING_INFO = O_ACE_QTE_SEQ_NBR + O_QTE_STA_CD + O_QTE_STA_DES + O_QTE_GUA_DT + O_ATH_LOSS_RQR_IND
				+ O_ATH_LOSS_REC_DT + O_UW_RSP_DT;
		public static final int O_PMA = O_PMA_CD + O_PMA_DES;
		public static final int O_TRS_REA_AMD = O_REA_AMD_CD + O_REA_AMD_TRS_DES + O_REA_AMD_CD_DES;
		public static final int O_POL_CURRENT_INFO = O_POL_NBR + O_ACT_NBR + O_ACT_NBR_FMT + O_OVL_OGN_CD + O_POL_PERIOD + O_LINE_OF_BUSINESS
				+ O_TYPE_OF_BUSINESS + O_SEGMENT_INFORMATION + O_LGE_ACT_IND + O_COMPANY_BRANCH + O_PRIMARY_RISK_ST + O_POLICY_TERM
				+ O_NIN_LEGAL_ENTITY + O_CURRENT_STATUS + O_CANCEL_REWRITE_INFO + O_DEDUCTIBLES + O_SIR_INFO + O_AUDIT_INFO + O_QTE_REPORTING_INFO
				+ O_WRT_DT + O_INS_SCORE_CD + O_NOT_CNC_DAY + O_POL_APP_REC_DT + O_SPLIT_BILLED_IND + O_AL_ADAC_IND + O_PMA + O_TRS_REA_AMD
				+ O_ISS_ACY_TS + O_EFF_DT + O_LST_MDF_ACY_TS + O_BOND_IND + O_WRT_PRM_AMT + O_ACY_POL_IND + FLR5 + FLR6;
		public static final int O_OGN_POL_STATUS_INFO = O_OGN_OGN_CD + O_OGN_TRS_TYP_CD + O_OGN_TRS_TYP_DES + O_OGN_TRS_ISS_IND;
		public static final int O_CURRENT_TRANSACTION = O_CUR_TRS_TYP_CD + O_CUR_TRS_TYP_DES + O_CUR_TRS_DT + O_CUR_TRS_PND_IND + O_CUR_OGN_CD
				+ O_WRAP_UP_DT;
		public static final int O_CANCEL_INFO = O_CNC_DT + O_CNC_REA_CD + O_CNC_REA_DES + O_WC_CNC_CD + O_WC_CNC_DES;
		public static final int O_POL_STATUS_ALERT = O_POL_CNC_IND + O_PND_CNC_IND + O_OOS_STR_IND + O_POL_GONE_MNL_IND + O_INS_LIN_GONE_MNL_IND
				+ O_TMN_IND + O_EXP_REW_IND + WsFwbt0011Row.O_WNG_MSG_MAXOCCURS * O_WNG_MSG + O_CANCEL_INFO + FLR1;
		public static final int FWBT110_NIN_TECHNICAL_KEYS = O_NIN_CLT_ID + O_NIN_CRM_ID + O_NIN_ADR_SEQ_NBR + O_NM_NBR + O_NM_SEQ_NBR + O_WC_LINK_NBR
				+ O_FRE_FRM_SEQ_NBR;
		public static final int O_NIN_NAME = O_NIN_IDV_NM_IND + O_NIN_DSY_NM + O_NIN_SR_NM + O_NIN_LST_NM + O_NIN_FST_NM + O_NIN_MDL_NM + O_NIN_SFX
				+ O_NIN_PFX;
		public static final int O_NIN_ADDRESS = O_NIN_ADR1 + O_NIN_ADR2 + O_NIN_CIT + O_NIN_ST_ABB + O_NIN_ST_DES + O_NIN_PST_CD + O_NIN_CTY
				+ O_NIN_ADR_ID + O_NIN_ADR_TYP_CD + O_NIN_ADR_TYP_DES;
		public static final int O_NIN_TAX_INF = O_NIN_CITX_TAX_ID + O_NIN_TAX_TYPE_CD + O_NIN_TAX_TYPE_DES;
		public static final int O_NAMED_INSURED = FWBT110_NIN_TECHNICAL_KEYS + O_NIN_NAME + O_NIN_ADDRESS + O_NIN_TAX_INF + FLR4;
		public static final int FWBT110_OWN_TECHNICAL_KEYS = O_OWN_CLT_ID + O_OWN_CRM_ID + O_OWN_ADR_SEQ_NBR;
		public static final int O_OWN_NAME = O_OWN_IDV_NM_IND + O_OWN_DSY_NM + O_OWN_SR_NM + O_OWN_LST_NM + O_OWN_FST_NM + O_OWN_MDL_NM + O_OWN_SFX
				+ O_OWN_PFX;
		public static final int O_OWN_ADDRESS = O_OWN_ADR1 + O_OWN_ADR2 + O_OWN_CIT + O_OWN_ST_ABB + O_OWN_ST_DES + O_OWN_PST_CD + O_OWN_CTY
				+ O_OWN_ADR_ID;
		public static final int O_ACCOUNT_OWNER = FWBT110_OWN_TECHNICAL_KEYS + O_OWN_NAME + O_OWN_ADDRESS + FLR9;
		public static final int O_BOND_DETAIL_INFO = O_BOND_AMT + O_BOND_CMT + O_BOND_TYP_CD + O_BOND_TYP_DES + FLR10;
		public static final int O_PRI_RSK_INFO = O_POL_PRI_RSK_ST_CD + O_POL_PRI_RSK_CTY_CD + O_POL_PRI_RSK_TWN_CD;
		public static final int FLR13 = 446;
		public static final int SERVICE_OUTPUTS = O_TECHNICAL_KEYS + O_POL_CURRENT_INFO + O_OGN_POL_STATUS_INFO + O_CURRENT_TRANSACTION
				+ O_POL_STATUS_ALERT + O_NAMED_INSURED + O_ACCOUNT_OWNER + WsFwbt0011Row.O_AUTOMATED_LINES_MAXOCCURS * O_AUTOMATED_LINES
				+ WsFwbt0011Row.O_MANUAL_LINES_MAXOCCURS * O_MANUAL_LINES + O_BOND_DETAIL_INFO + O_OGN_TOB_CD + O_PRI_RSK_INFO + FLR13;
		public static final int WS_FWBT0011_ROW = SERVICE_INPUTS + SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int I_TK_QTE_NBR = 5;
			public static final int I_QTE_NBR = 5;
			public static final int O_TK_QTE_NBR = 5;
			public static final int O_DSA_DED_AMT = 7;
			public static final int O_SYS_BKN_DED_AMT = 7;
			public static final int O_RTN_AMT = 11;
			public static final int O_SIR_DSC = 2;
			public static final int O_SIR_RMD_AMT = 11;
			public static final int O_ACE_QTE_SEQ_NBR = 5;
			public static final int O_NOT_CNC_DAY = 5;
			public static final int O_WRT_PRM_AMT = 12;
			public static final int O_NIN_ADR_SEQ_NBR = 5;
			public static final int O_NM_NBR = 5;
			public static final int O_NM_SEQ_NBR = 5;
			public static final int O_WC_LINK_NBR = 5;
			public static final int O_FRE_FRM_SEQ_NBR = 5;
			public static final int O_OWN_ADR_SEQ_NBR = 5;
			public static final int O_BOND_AMT = 11;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int O_SIR_DSC = 3;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
