/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.ISharedRecord;
import com.bphx.ctu.af.core.Types;

/**Original name: CALLING-PGM-IS-TS030097-IND<br>
 * Variable: CALLING-PGM-IS-TS030097-IND from program TS030099<br>
 * Generated as a class for rule DATA_WITH_EXTERNAL_CLAUSE.<br>*/
public class CallingPgmIsTs030097Ind implements ISharedRecord {

	//==== PROPERTIES ====
	//Original name: FILLER-CALLING-PGM-IS-TS030097-IND
	private char flr1 = DefaultValues.CHAR_VAL;
	public static final char CP_CALLING_PGM_IS_TS030097 = Types.HIGH_CHAR_VAL;

	//==== METHODS ====
	public char getFlr1() {
		return this.flr1;
	}

	public boolean isCpCallingPgmIsTs030097() {
		return flr1 == CP_CALLING_PGM_IS_TS030097;
	}
}
