/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-SE3-CUR-ISO-DATE-TIME<br>
 * Variable: WS-SE3-CUR-ISO-DATE-TIME from program CAWS002<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsSe3CurIsoDateTime extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WsSe3CurIsoDateTime() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_SE3_CUR_ISO_DATE_TIME;
	}

	public void setWsSe3CurIsoDateTimeFormatted(String data) {
		writeString(Pos.WS_SE3_CUR_ISO_DATE_TIME, data, Len.WS_SE3_CUR_ISO_DATE_TIME);
	}

	public void setSe3CurIsoDate(String se3CurIsoDate) {
		writeString(Pos.SE3_CUR_ISO_DATE, se3CurIsoDate, Len.SE3_CUR_ISO_DATE);
	}

	/**Original name: WS-SE3-CUR-ISO-DATE<br>*/
	public String getSe3CurIsoDate() {
		return readString(Pos.SE3_CUR_ISO_DATE, Len.SE3_CUR_ISO_DATE);
	}

	public String getWsCurrentSe3TimestampFormatted() {
		return readFixedString(Pos.WS_CURRENT_SE3_TIMESTAMP, Len.WS_CURRENT_SE3_TIMESTAMP);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE_TIME = 1;
		public static final int SE3_CUR_ISO_DATE = WS_SE3_CUR_ISO_DATE_TIME;
		public static final int SE3_CUR_ISO_TIME = SE3_CUR_ISO_DATE + Len.SE3_CUR_ISO_DATE;
		public static final int WS_CURRENT_SE3_TIMESTAMP = 1;
		public static final int WORK_CUR_TS_YR = WS_CURRENT_SE3_TIMESTAMP;
		public static final int WORK_CUR_TS_SEP1 = WORK_CUR_TS_YR + Len.WORK_CUR_TS_YR;
		public static final int WORK_CUR_TS_MO = WORK_CUR_TS_SEP1 + Len.WORK_CUR_TS_SEP1;
		public static final int WORK_CUR_TS_SEP2 = WORK_CUR_TS_MO + Len.WORK_CUR_TS_MO;
		public static final int WORK_CUR_TS_DAY = WORK_CUR_TS_SEP2 + Len.WORK_CUR_TS_SEP2;
		public static final int WORK_CUR_TS_SEP3 = WORK_CUR_TS_DAY + Len.WORK_CUR_TS_DAY;
		public static final int WORK_CUR_TS_HR = WORK_CUR_TS_SEP3 + Len.WORK_CUR_TS_SEP3;
		public static final int WORK_CUR_TS_SEP4 = WORK_CUR_TS_HR + Len.WORK_CUR_TS_HR;
		public static final int WORK_CUR_TS_MIN = WORK_CUR_TS_SEP4 + Len.WORK_CUR_TS_SEP4;
		public static final int WORK_CUR_TS_SEP5 = WORK_CUR_TS_MIN + Len.WORK_CUR_TS_MIN;
		public static final int WORK_CUR_TS_SEC = WORK_CUR_TS_SEP5 + Len.WORK_CUR_TS_SEP5;
		public static final int WORK_CUR_TS_SEP6 = WORK_CUR_TS_SEC + Len.WORK_CUR_TS_SEC;
		public static final int WORK_CUR_TS_MILSEC = WORK_CUR_TS_SEP6 + Len.WORK_CUR_TS_SEP6;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int SE3_CUR_ISO_DATE = 10;
		public static final int WORK_CUR_TS_YR = 4;
		public static final int WORK_CUR_TS_SEP1 = 1;
		public static final int WORK_CUR_TS_MO = 2;
		public static final int WORK_CUR_TS_SEP2 = 1;
		public static final int WORK_CUR_TS_DAY = 2;
		public static final int WORK_CUR_TS_SEP3 = 1;
		public static final int WORK_CUR_TS_HR = 2;
		public static final int WORK_CUR_TS_SEP4 = 1;
		public static final int WORK_CUR_TS_MIN = 2;
		public static final int WORK_CUR_TS_SEP5 = 1;
		public static final int WORK_CUR_TS_SEC = 2;
		public static final int WORK_CUR_TS_SEP6 = 1;
		public static final int SE3_CUR_ISO_TIME = 16;
		public static final int WS_SE3_CUR_ISO_DATE_TIME = SE3_CUR_ISO_DATE + SE3_CUR_ISO_TIME;
		public static final int WORK_CUR_TS_MILSEC = 6;
		public static final int WS_CURRENT_SE3_TIMESTAMP = WORK_CUR_TS_YR + WORK_CUR_TS_SEP1 + WORK_CUR_TS_MO + WORK_CUR_TS_SEP2 + WORK_CUR_TS_DAY
				+ WORK_CUR_TS_SEP3 + WORK_CUR_TS_HR + WORK_CUR_TS_SEP4 + WORK_CUR_TS_MIN + WORK_CUR_TS_SEP5 + WORK_CUR_TS_SEC + WORK_CUR_TS_SEP6
				+ WORK_CUR_TS_MILSEC;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
