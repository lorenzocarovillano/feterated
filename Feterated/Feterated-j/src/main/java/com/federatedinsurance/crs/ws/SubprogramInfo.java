/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.occurs.SiIdAtbList;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: SUBPROGRAM-INFO<br>
 * Variable: SUBPROGRAM-INFO from program XZ001000<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class SubprogramInfo extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int ID_ATB_LIST_MAXOCCURS = 100;
	//Original name: SI-RD-RETURN-CD
	private String rdReturnCd = DefaultValues.stringVal(Len.RD_RETURN_CD);
	public static final String RD_GOOD_RETURN = "";
	//Original name: SI-RD-RETURN-MSG
	private String rdReturnMsg = DefaultValues.stringVal(Len.RD_RETURN_MSG);
	//Original name: SI-ID-REGION
	private String idRegion = DefaultValues.stringVal(Len.ID_REGION);
	//Original name: SI-ID-OWNER-CD
	private String idOwnerCd = DefaultValues.stringVal(Len.ID_OWNER_CD);
	//Original name: SI-ID-WORKFLOW-NM
	private String idWorkflowNm = DefaultValues.stringVal(Len.ID_WORKFLOW_NM);
	//Original name: SI-ID-KEY-ID
	private String idKeyId = DefaultValues.stringVal(Len.ID_KEY_ID);
	//Original name: SI-ID-ATB-LIST
	private SiIdAtbList[] idAtbList = new SiIdAtbList[ID_ATB_LIST_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public SubprogramInfo() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.SUBPROGRAM_INFO;
	}

	@Override
	public void deserialize(byte[] buf) {
		setSubprogramInfoBytes(buf);
	}

	public void init() {
		for (int idAtbListIdx = 1; idAtbListIdx <= ID_ATB_LIST_MAXOCCURS; idAtbListIdx++) {
			idAtbList[idAtbListIdx - 1] = new SiIdAtbList();
		}
	}

	public String getSubprogramInfoFormatted() {
		return MarshalByteExt.bufferToStr(getSubprogramInfoBytes());
	}

	public void setSubprogramInfoBytes(byte[] buffer) {
		setSubprogramInfoBytes(buffer, 1);
	}

	public byte[] getSubprogramInfoBytes() {
		byte[] buffer = new byte[Len.SUBPROGRAM_INFO];
		return getSubprogramInfoBytes(buffer, 1);
	}

	public void setSubprogramInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setReturnDataBytes(buffer, position);
		position += Len.RETURN_DATA;
		setInputDataBytes(buffer, position);
	}

	public byte[] getSubprogramInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		getReturnDataBytes(buffer, position);
		position += Len.RETURN_DATA;
		getInputDataBytes(buffer, position);
		return buffer;
	}

	public void setReturnDataBytes(byte[] buffer, int offset) {
		int position = offset;
		rdReturnCd = MarshalByte.readString(buffer, position, Len.RD_RETURN_CD);
		position += Len.RD_RETURN_CD;
		rdReturnMsg = MarshalByte.readString(buffer, position, Len.RD_RETURN_MSG);
	}

	public byte[] getReturnDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, rdReturnCd, Len.RD_RETURN_CD);
		position += Len.RD_RETURN_CD;
		MarshalByte.writeString(buffer, position, rdReturnMsg, Len.RD_RETURN_MSG);
		return buffer;
	}

	public void setRdReturnCd(String rdReturnCd) {
		this.rdReturnCd = Functions.subString(rdReturnCd, Len.RD_RETURN_CD);
	}

	public String getRdReturnCd() {
		return this.rdReturnCd;
	}

	public boolean isRdGoodReturn() {
		return rdReturnCd.equals(RD_GOOD_RETURN);
	}

	public void setRdGoodReturn() {
		rdReturnCd = RD_GOOD_RETURN;
	}

	public void setRdReturnMsg(String rdReturnMsg) {
		this.rdReturnMsg = Functions.subString(rdReturnMsg, Len.RD_RETURN_MSG);
	}

	public String getRdReturnMsg() {
		return this.rdReturnMsg;
	}

	public void setInputDataBytes(byte[] buffer, int offset) {
		int position = offset;
		idRegion = MarshalByte.readString(buffer, position, Len.ID_REGION);
		position += Len.ID_REGION;
		idOwnerCd = MarshalByte.readString(buffer, position, Len.ID_OWNER_CD);
		position += Len.ID_OWNER_CD;
		idWorkflowNm = MarshalByte.readString(buffer, position, Len.ID_WORKFLOW_NM);
		position += Len.ID_WORKFLOW_NM;
		idKeyId = MarshalByte.readString(buffer, position, Len.ID_KEY_ID);
		position += Len.ID_KEY_ID;
		for (int idx = 1; idx <= ID_ATB_LIST_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				idAtbList[idx - 1].setIdAtbListBytes(buffer, position);
				position += SiIdAtbList.Len.ID_ATB_LIST;
			} else {
				idAtbList[idx - 1].initIdAtbListSpaces();
				position += SiIdAtbList.Len.ID_ATB_LIST;
			}
		}
	}

	public byte[] getInputDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, idRegion, Len.ID_REGION);
		position += Len.ID_REGION;
		MarshalByte.writeString(buffer, position, idOwnerCd, Len.ID_OWNER_CD);
		position += Len.ID_OWNER_CD;
		MarshalByte.writeString(buffer, position, idWorkflowNm, Len.ID_WORKFLOW_NM);
		position += Len.ID_WORKFLOW_NM;
		MarshalByte.writeString(buffer, position, idKeyId, Len.ID_KEY_ID);
		position += Len.ID_KEY_ID;
		for (int idx = 1; idx <= ID_ATB_LIST_MAXOCCURS; idx++) {
			idAtbList[idx - 1].getIdAtbListBytes(buffer, position);
			position += SiIdAtbList.Len.ID_ATB_LIST;
		}
		return buffer;
	}

	public void setIdRegion(String idRegion) {
		this.idRegion = Functions.subString(idRegion, Len.ID_REGION);
	}

	public String getIdRegion() {
		return this.idRegion;
	}

	public void setIdOwnerCd(String idOwnerCd) {
		this.idOwnerCd = Functions.subString(idOwnerCd, Len.ID_OWNER_CD);
	}

	public String getIdOwnerCd() {
		return this.idOwnerCd;
	}

	public void setIdWorkflowNm(String idWorkflowNm) {
		this.idWorkflowNm = Functions.subString(idWorkflowNm, Len.ID_WORKFLOW_NM);
	}

	public String getIdWorkflowNm() {
		return this.idWorkflowNm;
	}

	public void setIdKeyId(String idKeyId) {
		this.idKeyId = Functions.subString(idKeyId, Len.ID_KEY_ID);
	}

	public String getIdKeyId() {
		return this.idKeyId;
	}

	public SiIdAtbList getIdAtbList(int idx) {
		return idAtbList[idx - 1];
	}

	@Override
	public byte[] serialize() {
		return getSubprogramInfoBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RD_RETURN_CD = 20;
		public static final int RD_RETURN_MSG = 132;
		public static final int RETURN_DATA = RD_RETURN_CD + RD_RETURN_MSG;
		public static final int ID_REGION = 8;
		public static final int ID_OWNER_CD = 10;
		public static final int ID_WORKFLOW_NM = 70;
		public static final int ID_KEY_ID = 100;
		public static final int INPUT_DATA = ID_REGION + ID_OWNER_CD + ID_WORKFLOW_NM + ID_KEY_ID
				+ SubprogramInfo.ID_ATB_LIST_MAXOCCURS * SiIdAtbList.Len.ID_ATB_LIST;
		public static final int SUBPROGRAM_INFO = RETURN_DATA + INPUT_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
