/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [POL_DTA_EXT]
 * 
 */
public interface IPolDtaExt extends BaseSqlTo {

	/**
	 * Host Variable POL-TYP-CD
	 * 
	 */
	String getOlTypCd();

	void setOlTypCd(String olTypCd);

	/**
	 * Host Variable PRI-RSK-ST-ABB
	 * 
	 */
	String getRiRskStAbb();

	void setRiRskStAbb(String riRskStAbb);

	/**
	 * Host Variable PLN-EXP-DT
	 * 
	 */
	String getLnExpDt();

	void setLnExpDt(String lnExpDt);

	/**
	 * Host Variable POL-ID
	 * 
	 */
	String getPolId();

	void setPolId(String polId);

	/**
	 * Host Variable ACT-NBR
	 * 
	 */
	String getActNbr();

	void setActNbr(String actNbr);

	/**
	 * Host Variable POL-EFF-DT
	 * 
	 */
	String getPolEffDt();

	void setPolEffDt(String polEffDt);
};
