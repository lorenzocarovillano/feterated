/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: PM-PLF-V<br>
 * Variable: PM-PLF-V from copybook XPXLPLAT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class PmPlfV {

	//==== PROPERTIES ====
	//Original name: PMPF-DT-OFFSET
	private short pmpfDtOffset = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setPmpfDtOffset(short pmpfDtOffset) {
		this.pmpfDtOffset = pmpfDtOffset;
	}

	public short getPmpfDtOffset() {
		return this.pmpfDtOffset;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ISSUE_NODE_ID = 15;
		public static final int PMPF_SYSTEM_DT = 10;
		public static final int PMPF_MONITOR_NM = 8;
		public static final int PMPF_HOST_DB_NM = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
