/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TR-RECIPIENTS<br>
 * Variables: TR-RECIPIENTS from program XZ0P90C0<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TrRecipients implements ICopyable<TrRecipients> {

	//==== PROPERTIES ====
	//Original name: TR-REC-CLT-ID
	private String recCltId = DefaultValues.stringVal(Len.REC_CLT_ID);
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_CLT_ID);
	//Original name: TR-REC-ADR-ID
	private String recAdrId = DefaultValues.stringVal(Len.REC_ADR_ID);
	//Original name: TR-REC-NM
	private String recNm = DefaultValues.stringVal(Len.REC_NM);
	//Original name: TR-LIN-1-ADR
	private String lin1Adr = DefaultValues.stringVal(Len.LIN1_ADR);
	//Original name: TR-CIT-NM
	private String citNm = DefaultValues.stringVal(Len.CIT_NM);
	//Original name: TR-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);

	//==== CONSTRUCTORS ====
	public TrRecipients() {
	}

	public TrRecipients(TrRecipients trRecipients) {
		this();
		this.recCltId = trRecipients.recCltId;
		this.recAdrId = trRecipients.recAdrId;
		this.recNm = trRecipients.recNm;
		this.lin1Adr = trRecipients.lin1Adr;
		this.citNm = trRecipients.citNm;
		this.stAbb = trRecipients.stAbb;
	}

	//==== METHODS ====
	public TrRecipients initTrRecipientsHighValues() {
		recCltId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_CLT_ID);
		recAdrId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_ADR_ID);
		recNm = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_NM);
		lin1Adr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.LIN1_ADR);
		citNm = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CIT_NM);
		stAbb = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ST_ABB);
		return this;
	}

	public void setRecCltId(String recCltId) {
		this.recCltId = Functions.subString(recCltId, Len.REC_CLT_ID);
	}

	public String getRecCltId() {
		return this.recCltId;
	}

	public boolean isEndOfTable() {
		return recCltId.equals(END_OF_TABLE);
	}

	public void setRecAdrId(String recAdrId) {
		this.recAdrId = Functions.subString(recAdrId, Len.REC_ADR_ID);
	}

	public String getRecAdrId() {
		return this.recAdrId;
	}

	public void setRecNm(String recNm) {
		this.recNm = Functions.subString(recNm, Len.REC_NM);
	}

	public String getRecNm() {
		return this.recNm;
	}

	public void setLin1Adr(String lin1Adr) {
		this.lin1Adr = Functions.subString(lin1Adr, Len.LIN1_ADR);
	}

	public String getLin1Adr() {
		return this.lin1Adr;
	}

	public void setCitNm(String citNm) {
		this.citNm = Functions.subString(citNm, Len.CIT_NM);
	}

	public String getCitNm() {
		return this.citNm;
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	@Override
	public TrRecipients copy() {
		return new TrRecipients(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_CLT_ID = 64;
		public static final int REC_ADR_ID = 64;
		public static final int REC_NM = 120;
		public static final int LIN1_ADR = 45;
		public static final int CIT_NM = 30;
		public static final int ST_ABB = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
