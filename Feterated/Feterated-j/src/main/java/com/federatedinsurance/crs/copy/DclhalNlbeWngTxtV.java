/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IHalNlbeWngTxtV;

/**Original name: DCLHAL-NLBE-WNG-TXT-V<br>
 * Variable: DCLHAL-NLBE-WNG-TXT-V from copybook HALLGNLB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalNlbeWngTxtV implements IHalNlbeWngTxtV {

	//==== PROPERTIES ====
	//Original name: APP-NM
	private String appNm = DefaultValues.stringVal(Len.APP_NM);
	//Original name: HNLB-ERR-WNG-CD
	private String hnlbErrWngCd = DefaultValues.stringVal(Len.HNLB_ERR_WNG_CD);
	//Original name: HNLB-ERR-WNG-TXT-L
	private short hnlbErrWngTxtL = DefaultValues.BIN_SHORT_VAL;
	//Original name: HNLB-ERR-WNG-TXT-D
	private String hnlbErrWngTxtD = DefaultValues.stringVal(Len.HNLB_ERR_WNG_TXT_D);

	//==== METHODS ====
	public void setAppNm(String appNm) {
		this.appNm = Functions.subString(appNm, Len.APP_NM);
	}

	public String getAppNm() {
		return this.appNm;
	}

	public void setHnlbErrWngCd(String hnlbErrWngCd) {
		this.hnlbErrWngCd = Functions.subString(hnlbErrWngCd, Len.HNLB_ERR_WNG_CD);
	}

	public String getHnlbErrWngCd() {
		return this.hnlbErrWngCd;
	}

	public void setHnlbErrWngTxtL(short hnlbErrWngTxtL) {
		this.hnlbErrWngTxtL = hnlbErrWngTxtL;
	}

	public short getHnlbErrWngTxtL() {
		return this.hnlbErrWngTxtL;
	}

	public void setHnlbErrWngTxtD(String hnlbErrWngTxtD) {
		this.hnlbErrWngTxtD = Functions.subString(hnlbErrWngTxtD, Len.HNLB_ERR_WNG_TXT_D);
	}

	public String getHnlbErrWngTxtD() {
		return this.hnlbErrWngTxtD;
	}

	public String getHnlbErrWngTxtDFormatted() {
		return Functions.padBlanks(getHnlbErrWngTxtD(), Len.HNLB_ERR_WNG_TXT_D);
	}

	public String getHnlbErrWngTxt() {
		return FixedStrings.get(getHnlbErrWngTxtD(), getHnlbErrWngTxtL());
	}

	public void setHnlbErrWngTxt(String hnlbErrWngTxt) {
		this.setHnlbErrWngTxtD(hnlbErrWngTxt);
		this.setHnlbErrWngTxtL((((short) strLen(hnlbErrWngTxt))));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int APP_NM = 10;
		public static final int HNLB_ERR_WNG_CD = 10;
		public static final int HNLB_ERR_WNG_TXT_D = 500;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
