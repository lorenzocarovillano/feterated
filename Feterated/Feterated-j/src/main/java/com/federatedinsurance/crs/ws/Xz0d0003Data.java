/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IActNotRec;
import com.federatedinsurance.crs.commons.data.to.IRecTyp;
import com.federatedinsurance.crs.copy.DclhalBoAudTgrV;
import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclhalUowObjHierV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Halluchs;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Halluidg;
import com.federatedinsurance.crs.copy.Hallukrp;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xz0c0003;
import com.federatedinsurance.crs.copy.Xz0n0003;
import com.federatedinsurance.crs.copy.Xzc007ActNotFrmRecRow;
import com.federatedinsurance.crs.copy.Xzc008ActNotPolRecRow;
import com.federatedinsurance.crs.copy.Xzh001ActNotRow;
import com.federatedinsurance.crs.copy.Xzh003ActNotRecRow;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.redefines.CidpTableInfo;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0D0003<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0d0003Data implements IActNotRec, IRecTyp {

	//==== PROPERTIES ====
	//Original name: CF-YES
	private char cfYes = 'Y';
	//Original name: CF-CER-HLD-ITL-ST
	private String cfCerHldItlSt = "ZI";
	/**Original name: NI-REC-SEQ-NBR<br>
	 * <pre> NULL INDICATORS</pre>*/
	private short niRecSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-SPECIFIC-MISC
	private WsSpecificMiscXz0d0003 wsSpecificMisc = new WsSpecificMiscXz0d0003();
	//Original name: XZ0C0003
	private Xz0c0003 xz0c0003 = new Xz0c0003();
	//Original name: XZC007-ACT-NOT-FRM-REC-ROW
	private Xzc007ActNotFrmRecRow xzc007ActNotFrmRecRow = new Xzc007ActNotFrmRecRow();
	//Original name: XZC008-ACT-NOT-POL-REC-ROW
	private Xzc008ActNotPolRecRow xzc008ActNotPolRecRow = new Xzc008ActNotPolRecRow();
	//Original name: XZ0N0003
	private Xz0n0003 xz0n0003 = new Xz0n0003();
	//Original name: XZH001-ACT-NOT-ROW
	private Xzh001ActNotRow xzh001ActNotRow = new Xzh001ActNotRow();
	//Original name: XZH003-ACT-NOT-REC-ROW
	private Xzh003ActNotRecRow xzh003ActNotRecRow = new Xzh003ActNotRecRow();
	//Original name: WS-BDO-WORK-FIELDS
	private WsBdoWorkFields wsBdoWorkFields = new WsBdoWorkFields();
	//Original name: WS-BDO-SWITCHES
	private WsBdoSwitches wsBdoSwitches = new WsBdoSwitches();
	//Original name: DCLHAL-BO-AUD-TGR-V
	private DclhalBoAudTgrV dclhalBoAudTgrV = new DclhalBoAudTgrV();
	//Original name: HALLUCHS
	private Halluchs halluchs = new Halluchs();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();
	//Original name: HALLUKRP
	private Hallukrp hallukrp = new Hallukrp();
	//Original name: HALRLODR-LOCK-DRIVER-STORAGE
	private WsHalrlodrLinkage halrlodrLockDriverStorage = new WsHalrlodrLinkage();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: HALLUSW
	private Hallusw hallusw = new Hallusw();
	//Original name: HALLUDAT
	private Halludat halludat = new Halludat();
	//Original name: HALLUHDR
	private Halluhdr halluhdr = new Halluhdr();
	//Original name: CIDP-TABLE-INFO
	private CidpTableInfo cidpTableInfo = new CidpTableInfo();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: HALRMON-PERF-MONITOR-STORAGE
	private HalrmonPerfMonitorStorage halrmonPerfMonitorStorage = new HalrmonPerfMonitorStorage();
	//Original name: DATE-STRUCTURE
	private DateStructureCawpcorc dateStructure = new DateStructureCawpcorc();
	//Original name: WS-SE3-CUR-ISO-DATE
	private String wsSe3CurIsoDate = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_DATE);
	//Original name: WS-SE3-CUR-ISO-TIME
	private String wsSe3CurIsoTime = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_TIME);
	//Original name: DCLHAL-UOW-OBJ-HIER-V
	private DclhalUowObjHierV dclhalUowObjHierV = new DclhalUowObjHierV();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public char getCfYes() {
		return this.cfYes;
	}

	public String getCfCerHldItlSt() {
		return this.cfCerHldItlSt;
	}

	public short getNiRecSeqNbr() {
		return this.niRecSeqNbr;
	}

	public String getWsActNotFrmRecCopybookFormatted() {
		return xzc007ActNotFrmRecRow.getXzc007ActNotFrmRecRowFormatted();
	}

	public String getWsActNotPolRecCopybookFormatted() {
		return xzc008ActNotPolRecRow.getXzc008ActNotPolRecRowFormatted();
	}

	public void setHalouchsLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.HALOUCHS_LINKAGE);
		setHalouchsLinkageBytes(buffer, 1);
	}

	public String getHalouchsLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHalouchsLinkageBytes());
	}

	/**Original name: HALOUCHS-LINKAGE<br>*/
	public byte[] getHalouchsLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		return getHalouchsLinkageBytes(buffer, 1);
	}

	public void setHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluchs.setLength2(MarshalByte.readBinaryShort(buffer, position));
		position += Types.SHORT_SIZE;
		halluchs.setStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		halluchs.checkSum = MarshalByte.readFixedString(buffer, position, Halluchs.Len.CHECK_SUM);
	}

	public byte[] getHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, halluchs.getLength2());
		position += Types.SHORT_SIZE;
		halluchs.getStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		MarshalByte.writeString(buffer, position, halluchs.checkSum, Halluchs.Len.CHECK_SUM);
		return buffer;
	}

	public String getHaloukrpLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHaloukrpLinkageBytes());
	}

	/**Original name: HALOUKRP-LINKAGE<br>
	 * <pre>*         07 FILLER                       PIC  X(4).</pre>*/
	public byte[] getHaloukrpLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUKRP_LINKAGE];
		return getHaloukrpLinkageBytes(buffer, 1);
	}

	public byte[] getHaloukrpLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallukrp.getInputFieldsBytes(buffer, position);
		position += Hallukrp.Len.INPUT_FIELDS;
		hallukrp.getInputOutputFieldsBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-DATA-UMT-AREA<br>
	 * <pre>* DATA RESPONSE UMT AREA</pre>*/
	public byte[] getWsDataUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_DATA_UMT_AREA];
		return getWsDataUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsDataUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		halludat.getCommonBytes(buffer, position);
		return buffer;
	}

	public void initWsDataUmtAreaSpaces() {
		halludat.initHalludatSpaces();
	}

	public void initWsHdrUmtAreaSpaces() {
		halluhdr.initHalluhdrSpaces();
	}

	public void setWsDataPrivacyInfoFormatted(String data) {
		byte[] buffer = new byte[Len.WS_DATA_PRIVACY_INFO];
		MarshalByte.writeString(buffer, 1, data, Len.WS_DATA_PRIVACY_INFO);
		setWsDataPrivacyInfoBytes(buffer, 1);
	}

	public String getWsDataPrivacyInfoFormatted() {
		return getCidpPassedInfoFormatted();
	}

	public void setWsDataPrivacyInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setCidpPassedInfoBytes(buffer, position);
	}

	public String getCidpPassedInfoFormatted() {
		return MarshalByteExt.bufferToStr(getCidpPassedInfoBytes());
	}

	/**Original name: CIDP-PASSED-INFO<br>
	 * <pre>****************************************************************
	 *  HALLCIDP                                                      *
	 *  COMMON INTERFACE TO DATA PRIVACY                              *
	 *  USED BY BUSINESS DATA OBJECTS TO PASS DATA ROWS FOR           *
	 *  DATA PRIVACY CHECKING.                                        *
	 * ****************************************************************
	 *  SI#       PRGRMR  DATE       DESCRIPTION                      *
	 *  --------- ------- ---------- ---------------------------------*
	 *  SAVANNAH  PM      31MAY2000  INITIAL CODING                   *
	 *  14969     18448   25JUN2001  MADE CHANGES REQUIRED FOR        *
	 *                               ENHANCED DATA PRIVACY AND SET    *
	 *                               DEFAULTS PROCESSING.             *
	 *                               (ARCHITECTURE 2.3)               *
	 * ****************************************************************</pre>*/
	public byte[] getCidpPassedInfoBytes() {
		byte[] buffer = new byte[Len.CIDP_PASSED_INFO];
		return getCidpPassedInfoBytes(buffer, 1);
	}

	public void setCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.setCidpTableInfoBytes(buffer, position);
	}

	public byte[] getCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.getCidpTableInfoBytes(buffer, position);
		return buffer;
	}

	public void setWsSe3CurIsoDateTimeFormatted(String data) {
		byte[] buffer = new byte[Len.WS_SE3_CUR_ISO_DATE_TIME];
		MarshalByte.writeString(buffer, 1, data, Len.WS_SE3_CUR_ISO_DATE_TIME);
		setWsSe3CurIsoDateTimeBytes(buffer, 1);
	}

	public void setWsSe3CurIsoDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		wsSe3CurIsoDate = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_DATE);
		position += Len.WS_SE3_CUR_ISO_DATE;
		wsSe3CurIsoTime = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_TIME);
	}

	public void setWsSe3CurIsoDate(String wsSe3CurIsoDate) {
		this.wsSe3CurIsoDate = Functions.subString(wsSe3CurIsoDate, Len.WS_SE3_CUR_ISO_DATE);
	}

	public String getWsSe3CurIsoDate() {
		return this.wsSe3CurIsoDate;
	}

	public void setWsSe3CurIsoTime(String wsSe3CurIsoTime) {
		this.wsSe3CurIsoTime = Functions.subString(wsSe3CurIsoTime, Len.WS_SE3_CUR_ISO_TIME);
	}

	public String getWsSe3CurIsoTime() {
		return this.wsSe3CurIsoTime;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getAdrId() {
		throw new FieldNotMappedException("adrId");
	}

	@Override
	public void setAdrId(String adrId) {
		throw new FieldNotMappedException("adrId");
	}

	@Override
	public String getAdrIdObj() {
		return getAdrId();
	}

	@Override
	public void setAdrIdObj(String adrIdObj) {
		setAdrId(adrIdObj);
	}

	@Override
	public String getCerNbr() {
		return xzh003ActNotRecRow.getCerNbr();
	}

	@Override
	public void setCerNbr(String cerNbr) {
		this.xzh003ActNotRecRow.setCerNbr(cerNbr);
	}

	@Override
	public String getCerNbrObj() {
		if (xzh003ActNotRecRow.getCerNbrNi() >= 0) {
			return getCerNbr();
		} else {
			return null;
		}
	}

	@Override
	public void setCerNbrObj(String cerNbrObj) {
		if (cerNbrObj != null) {
			setCerNbr(cerNbrObj);
			xzh003ActNotRecRow.setCerNbrNi(((short) 0));
		} else {
			xzh003ActNotRecRow.setCerNbrNi(((short) -1));
		}
	}

	public CidpTableInfo getCidpTableInfo() {
		return cidpTableInfo;
	}

	@Override
	public String getCitNm() {
		return xzh003ActNotRecRow.getCitNm();
	}

	@Override
	public void setCitNm(String citNm) {
		this.xzh003ActNotRecRow.setCitNm(citNm);
	}

	@Override
	public String getCitNmObj() {
		if (xzh003ActNotRecRow.getCitNmNi() >= 0) {
			return getCitNm();
		} else {
			return null;
		}
	}

	@Override
	public void setCitNmObj(String citNmObj) {
		if (citNmObj != null) {
			setCitNm(citNmObj);
			xzh003ActNotRecRow.setCitNmNi(((short) 0));
		} else {
			xzh003ActNotRecRow.setCitNmNi(((short) -1));
		}
	}

	@Override
	public String getCltId() {
		throw new FieldNotMappedException("cltId");
	}

	@Override
	public void setCltId(String cltId) {
		throw new FieldNotMappedException("cltId");
	}

	@Override
	public String getCltIdObj() {
		return getCltId();
	}

	@Override
	public void setCltIdObj(String cltIdObj) {
		setCltId(cltIdObj);
	}

	@Override
	public String getCsrActNbr() {
		return xzh003ActNotRecRow.getCsrActNbr();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.xzh003ActNotRecRow.setCsrActNbr(csrActNbr);
	}

	public DateStructureCawpcorc getDateStructure() {
		return dateStructure;
	}

	public DclhalBoAudTgrV getDclhalBoAudTgrV() {
		return dclhalBoAudTgrV;
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclhalUowObjHierV getDclhalUowObjHierV() {
		return dclhalUowObjHierV;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Halluchs getHalluchs() {
		return halluchs;
	}

	public Halludat getHalludat() {
		return halludat;
	}

	public Halluhdr getHalluhdr() {
		return halluhdr;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public Hallukrp getHallukrp() {
		return hallukrp;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	public WsHalrlodrLinkage getHalrlodrLockDriverStorage() {
		return halrlodrLockDriverStorage;
	}

	public HalrmonPerfMonitorStorage getHalrmonPerfMonitorStorage() {
		return halrmonPerfMonitorStorage;
	}

	@Override
	public String getLin1Adr() {
		return xzh003ActNotRecRow.getLin1Adr();
	}

	@Override
	public void setLin1Adr(String lin1Adr) {
		this.xzh003ActNotRecRow.setLin1Adr(lin1Adr);
	}

	@Override
	public String getLin1AdrObj() {
		if (xzh003ActNotRecRow.getLin1AdrNi() >= 0) {
			return getLin1Adr();
		} else {
			return null;
		}
	}

	@Override
	public void setLin1AdrObj(String lin1AdrObj) {
		if (lin1AdrObj != null) {
			setLin1Adr(lin1AdrObj);
			xzh003ActNotRecRow.setLin1AdrNi(((short) 0));
		} else {
			xzh003ActNotRecRow.setLin1AdrNi(((short) -1));
		}
	}

	@Override
	public String getLin2Adr() {
		return xzh003ActNotRecRow.getLin2Adr();
	}

	@Override
	public void setLin2Adr(String lin2Adr) {
		this.xzh003ActNotRecRow.setLin2Adr(lin2Adr);
	}

	@Override
	public String getLin2AdrObj() {
		if (xzh003ActNotRecRow.getLin2AdrNi() >= 0) {
			return getLin2Adr();
		} else {
			return null;
		}
	}

	@Override
	public void setLin2AdrObj(String lin2AdrObj) {
		if (lin2AdrObj != null) {
			setLin2Adr(lin2AdrObj);
			xzh003ActNotRecRow.setLin2AdrNi(((short) 0));
		} else {
			xzh003ActNotRecRow.setLin2AdrNi(((short) -1));
		}
	}

	@Override
	public String getLngDes() {
		throw new FieldNotMappedException("lngDes");
	}

	@Override
	public void setLngDes(String lngDes) {
		throw new FieldNotMappedException("lngDes");
	}

	@Override
	public char getMnlInd() {
		return xzh003ActNotRecRow.getMnlInd();
	}

	@Override
	public void setMnlInd(char mnlInd) {
		this.xzh003ActNotRecRow.setMnlInd(mnlInd);
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotPrcTs() {
		return xzh003ActNotRecRow.getNotPrcTs();
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.xzh003ActNotRecRow.setNotPrcTs(notPrcTs);
	}

	@Override
	public String getPstCd() {
		return xzh003ActNotRecRow.getPstCd();
	}

	@Override
	public void setPstCd(String pstCd) {
		this.xzh003ActNotRecRow.setPstCd(pstCd);
	}

	@Override
	public String getPstCdObj() {
		if (xzh003ActNotRecRow.getPstCdNi() >= 0) {
			return getPstCd();
		} else {
			return null;
		}
	}

	@Override
	public void setPstCdObj(String pstCdObj) {
		if (pstCdObj != null) {
			setPstCd(pstCdObj);
			xzh003ActNotRecRow.setPstCdNi(((short) 0));
		} else {
			xzh003ActNotRecRow.setPstCdNi(((short) -1));
		}
	}

	@Override
	public String getRecAdrId() {
		return xzh003ActNotRecRow.getRecAdrId();
	}

	@Override
	public void setRecAdrId(String recAdrId) {
		this.xzh003ActNotRecRow.setRecAdrId(recAdrId);
	}

	@Override
	public String getRecAdrIdObj() {
		if (xzh003ActNotRecRow.getRecAdrIdNi() >= 0) {
			return getRecAdrId();
		} else {
			return null;
		}
	}

	@Override
	public void setRecAdrIdObj(String recAdrIdObj) {
		if (recAdrIdObj != null) {
			setRecAdrId(recAdrIdObj);
			xzh003ActNotRecRow.setRecAdrIdNi(((short) 0));
		} else {
			xzh003ActNotRecRow.setRecAdrIdNi(((short) -1));
		}
	}

	@Override
	public String getRecCltId() {
		return xzh003ActNotRecRow.getRecCltId();
	}

	@Override
	public void setRecCltId(String recCltId) {
		this.xzh003ActNotRecRow.setRecCltId(recCltId);
	}

	@Override
	public String getRecCltIdObj() {
		if (xzh003ActNotRecRow.getRecCltIdNi() >= 0) {
			return getRecCltId();
		} else {
			return null;
		}
	}

	@Override
	public void setRecCltIdObj(String recCltIdObj) {
		if (recCltIdObj != null) {
			setRecCltId(recCltIdObj);
			xzh003ActNotRecRow.setRecCltIdNi(((short) 0));
		} else {
			xzh003ActNotRecRow.setRecCltIdNi(((short) -1));
		}
	}

	@Override
	public String getRecNm() {
		return FixedStrings.get(xzh003ActNotRecRow.getRecNmText(), xzh003ActNotRecRow.getRecNmLen());
	}

	@Override
	public void setRecNm(String recNm) {
		this.xzh003ActNotRecRow.setRecNmText(recNm);
		this.xzh003ActNotRecRow.setRecNmLen((((short) strLen(recNm))));
	}

	@Override
	public String getRecNmObj() {
		return getRecNm();
	}

	@Override
	public void setRecNmObj(String recNmObj) {
		setRecNm(recNmObj);
	}

	@Override
	public short getRecSeqNbr() {
		return xzh003ActNotRecRow.getRecSeqNbr();
	}

	@Override
	public void setRecSeqNbr(short recSeqNbr) {
		this.xzh003ActNotRecRow.setRecSeqNbr(recSeqNbr);
	}

	@Override
	public String getRecTypCd() {
		return xzh003ActNotRecRow.getRecTypCd();
	}

	@Override
	public void setRecTypCd(String recTypCd) {
		this.xzh003ActNotRecRow.setRecTypCd(recTypCd);
	}

	@Override
	public String getShtDes() {
		throw new FieldNotMappedException("shtDes");
	}

	@Override
	public void setShtDes(String shtDes) {
		throw new FieldNotMappedException("shtDes");
	}

	@Override
	public short getSrOrdNbr() {
		throw new FieldNotMappedException("srOrdNbr");
	}

	@Override
	public void setSrOrdNbr(short srOrdNbr) {
		throw new FieldNotMappedException("srOrdNbr");
	}

	@Override
	public String getStAbb() {
		return xzh003ActNotRecRow.getStAbb();
	}

	@Override
	public void setStAbb(String stAbb) {
		this.xzh003ActNotRecRow.setStAbb(stAbb);
	}

	@Override
	public String getStAbbObj() {
		if (xzh003ActNotRecRow.getStAbbNi() >= 0) {
			return getStAbb();
		} else {
			return null;
		}
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		if (stAbbObj != null) {
			setStAbb(stAbbObj);
			xzh003ActNotRecRow.setStAbbNi(((short) 0));
		} else {
			xzh003ActNotRecRow.setStAbbNi(((short) -1));
		}
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WsBdoSwitches getWsBdoSwitches() {
		return wsBdoSwitches;
	}

	public WsBdoWorkFields getWsBdoWorkFields() {
		return wsBdoWorkFields;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSpecificMiscXz0d0003 getWsSpecificMisc() {
		return wsSpecificMisc;
	}

	public Xz0c0003 getXz0c0003() {
		return xz0c0003;
	}

	public Xz0n0003 getXz0n0003() {
		return xz0n0003;
	}

	public Xzc007ActNotFrmRecRow getXzc007ActNotFrmRecRow() {
		return xzc007ActNotFrmRecRow;
	}

	public Xzc008ActNotPolRecRow getXzc008ActNotPolRecRow() {
		return xzc008ActNotPolRecRow;
	}

	public Xzh001ActNotRow getXzh001ActNotRow() {
		return xzh001ActNotRow;
	}

	public Xzh003ActNotRecRow getXzh003ActNotRecRow() {
		return xzh003ActNotRecRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_SE3_CUR_ISO_DATE_TIME = WS_SE3_CUR_ISO_DATE + WS_SE3_CUR_ISO_TIME;
		public static final int WS_DATA_UMT_AREA = Halludat.Len.COMMON;
		public static final int CIDP_PASSED_INFO = CidpTableInfo.Len.CIDP_TABLE_INFO;
		public static final int WS_DATA_PRIVACY_INFO = CIDP_PASSED_INFO;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;
		public static final int HALOUKRP_LINKAGE = Hallukrp.Len.INPUT_FIELDS + Hallukrp.Len.INPUT_OUTPUT_FIELDS;
		public static final int HALOUCHS_LINKAGE = Halluchs.Len.LENGTH2 + Halluchs.Len.STRING_FLD + Halluchs.Len.CHECK_SUM;
		public static final int WS_ACT_NOT_FRM_REC_COPYBOOK = Xzc007ActNotFrmRecRow.Len.XZC007_ACT_NOT_FRM_REC_ROW;
		public static final int WS_ACT_NOT_POL_REC_COPYBOOK = Xzc008ActNotPolRecRow.Len.XZC008_ACT_NOT_POL_REC_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
