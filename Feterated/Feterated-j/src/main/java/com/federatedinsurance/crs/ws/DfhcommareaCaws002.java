/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program CAWS002<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaCaws002 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: UBOC-RECORD
	private UbocRecordCaws002 ubocRecord = new UbocRecordCaws002();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		ubocRecord.setUbocRecordBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		ubocRecord.getUbocRecordBytes(buffer, position);
		return buffer;
	}

	public UbocRecordCaws002 getUbocRecord() {
		return ubocRecord;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DFHCOMMAREA = UbocRecordCaws002.Len.UBOC_RECORD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
