/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsTs030099 {

	//==== PROPERTIES ====
	//Original name: CF-DBMS-ERROR
	private short dbmsError = ((short) 6);
	//Original name: CF-IN-HOUSE-ONLY
	private char inHouseOnly = '1';
	//Original name: CF-CARRIAGE-CONTROL-CHARACTERS
	private CfCarriageControlCharacters carriageControlCharacters = new CfCarriageControlCharacters();
	//Original name: CF-DEFAULT-LINES-PER-PAGE
	private String defaultLinesPerPage = "66";
	//Original name: CF-DEFAULT-BEFORE-OR-AFTER
	private char defaultBeforeOrAfter = 'A';
	//Original name: CF-DEFAULT-DIVISION
	private char defaultDivision = 'D';
	//Original name: CF-DEFAULT-SUBDIVISION
	private char defaultSubdivision = '0';
	//Original name: CF-ALL-COM-OFFICE-LOCATIONS
	private String allComOfficeLocations = "CM";
	//Original name: CF-ALL-BAL-OFFICE-LOCATIONS
	private String allBalOfficeLocations = "BL";

	//==== METHODS ====
	public short getDbmsError() {
		return this.dbmsError;
	}

	public char getInHouseOnly() {
		return this.inHouseOnly;
	}

	public String getDefaultLinesPerPage() {
		return this.defaultLinesPerPage;
	}

	public char getDefaultBeforeOrAfter() {
		return this.defaultBeforeOrAfter;
	}

	public char getDefaultDivision() {
		return this.defaultDivision;
	}

	public char getDefaultSubdivision() {
		return this.defaultSubdivision;
	}

	public String getAllComOfficeLocations() {
		return this.allComOfficeLocations;
	}

	public String getAllBalOfficeLocations() {
		return this.allBalOfficeLocations;
	}

	public CfCarriageControlCharacters getCarriageControlCharacters() {
		return carriageControlCharacters;
	}
}
