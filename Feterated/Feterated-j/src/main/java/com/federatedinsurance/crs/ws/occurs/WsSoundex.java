/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-SOUNDEX<br>
 * Variables: WS-SOUNDEX from program CIWOSDX<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsSoundex {

	//==== PROPERTIES ====
	//Original name: WS-SNDX-CHAR
	private char wsSndxChar = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setSoundexBytes(byte[] buffer, int offset) {
		int position = offset;
		wsSndxChar = MarshalByte.readChar(buffer, position);
	}

	public byte[] getSoundexBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, wsSndxChar);
		return buffer;
	}

	public void initSoundexSpaces() {
		wsSndxChar = Types.SPACE_CHAR;
	}

	public void setWsSndxChar(char wsSndxChar) {
		this.wsSndxChar = wsSndxChar;
	}

	public void setWsSndxCharFormatted(String wsSndxChar) {
		setWsSndxChar(Functions.charAt(wsSndxChar, Types.CHAR_SIZE));
	}

	public char getWsSndxChar() {
		return this.wsSndxChar;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SNDX_CHAR = 1;
		public static final int SOUNDEX = WS_SNDX_CHAR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
