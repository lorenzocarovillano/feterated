/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallurqa;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.ws.enums.SwRecFoundFlag;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS020100<br>
 * Generated as a class for rule WS.<br>*/
public class Ts020100Data {

	//==== PROPERTIES ====
	//Original name: SW-REC-FOUND-FLAG
	private SwRecFoundFlag swRecFoundFlag = new SwRecFoundFlag();
	//Original name: WS-SPECIFIC-WORK-AREAS
	private WsSpecificWorkAreas wsSpecificWorkAreas = new WsSpecificWorkAreas();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: HALLURQA
	private Hallurqa hallurqa = new Hallurqa();
	//Original name: REQUEST-DATA-BUFFER
	private String requestDataBuffer = DefaultValues.stringVal(Len.REQUEST_DATA_BUFFER);
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void setRequestUmtModuleDataFormatted(String data) {
		byte[] buffer = new byte[Len.REQUEST_UMT_MODULE_DATA];
		MarshalByte.writeString(buffer, 1, data, Len.REQUEST_UMT_MODULE_DATA);
		setRequestUmtModuleDataBytes(buffer, 1);
	}

	public void setRequestUmtModuleDataBytes(byte[] buffer, int offset) {
		int position = offset;
		hallurqa.setConstantsBytes(buffer, position);
		position += Hallurqa.Len.CONSTANTS;
		hallurqa.setInputLinkageBytes(buffer, position);
		position += Hallurqa.Len.INPUT_LINKAGE;
		hallurqa.setOutputLinkageBytes(buffer, position);
		position += Hallurqa.Len.OUTPUT_LINKAGE;
		hallurqa.setInputOutputLinkageBytes(buffer, position);
		position += Hallurqa.Len.INPUT_OUTPUT_LINKAGE;
		requestDataBuffer = MarshalByte.readString(buffer, position, Len.REQUEST_DATA_BUFFER);
	}

	public void setRequestDataBuffer(String requestDataBuffer) {
		this.requestDataBuffer = Functions.subString(requestDataBuffer, Len.REQUEST_DATA_BUFFER);
	}

	public String getRequestDataBuffer() {
		return this.requestDataBuffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallurqa getHallurqa() {
		return hallurqa;
	}

	public SwRecFoundFlag getSwRecFoundFlag() {
		return swRecFoundFlag;
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSpecificWorkAreas getWsSpecificWorkAreas() {
		return wsSpecificWorkAreas;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REQUEST_DATA_BUFFER = 5000;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int REQUEST_UMT_MODULE_DATA = Hallurqa.Len.CONSTANTS + Hallurqa.Len.INPUT_LINKAGE + Hallurqa.Len.OUTPUT_LINKAGE
				+ Hallurqa.Len.INPUT_OUTPUT_LINKAGE + REQUEST_DATA_BUFFER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
