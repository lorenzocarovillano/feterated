/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.NonStaticFields;
import com.federatedinsurance.crs.ws.redefines.CallableInputs;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: IVORYH<br>
 * Variable: IVORYH from copybook TS570CB1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class IvoryhFnc02090 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: SOAP-EYECATCHER
	private String soapEyecatcher = "<IVORY CALL>";
	//Original name: SOAP-LAYOUT-VERSION
	private String soapLayoutVersion = "01";
	//Original name: SOAP-FILE-PREFIX
	private String soapFilePrefix = "IV";
	//Original name: NON-STATIC-FIELDS
	private NonStaticFields nonStaticFields = new NonStaticFields();
	//Original name: CALLABLE-INPUTS
	private CallableInputs callableInputs = new CallableInputs();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.IVORYH;
	}

	@Override
	public void deserialize(byte[] buf) {
		setIvoryhBytes(buf);
	}

	public void setIvoryhBytes(byte[] buffer) {
		setIvoryhBytes(buffer, 1);
	}

	public byte[] getIvoryhBytes() {
		byte[] buffer = new byte[Len.IVORYH];
		return getIvoryhBytes(buffer, 1);
	}

	public void setIvoryhBytes(byte[] buffer, int offset) {
		int position = offset;
		soapEyecatcher = MarshalByte.readString(buffer, position, Len.SOAP_EYECATCHER);
		position += Len.SOAP_EYECATCHER;
		soapLayoutVersion = MarshalByte.readString(buffer, position, Len.SOAP_LAYOUT_VERSION);
		position += Len.SOAP_LAYOUT_VERSION;
		soapFilePrefix = MarshalByte.readString(buffer, position, Len.SOAP_FILE_PREFIX);
		position += Len.SOAP_FILE_PREFIX;
		nonStaticFields.setNonStaticFieldsBytes(buffer, position);
		position += NonStaticFields.Len.NON_STATIC_FIELDS;
		callableInputs.setCallableInputsBytes(buffer, position);
	}

	public byte[] getIvoryhBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, soapEyecatcher, Len.SOAP_EYECATCHER);
		position += Len.SOAP_EYECATCHER;
		MarshalByte.writeString(buffer, position, soapLayoutVersion, Len.SOAP_LAYOUT_VERSION);
		position += Len.SOAP_LAYOUT_VERSION;
		MarshalByte.writeString(buffer, position, soapFilePrefix, Len.SOAP_FILE_PREFIX);
		position += Len.SOAP_FILE_PREFIX;
		nonStaticFields.getNonStaticFieldsBytes(buffer, position);
		position += NonStaticFields.Len.NON_STATIC_FIELDS;
		callableInputs.getCallableInputsBytes(buffer, position);
		return buffer;
	}

	public void setSoapEyecatcher(String soapEyecatcher) {
		this.soapEyecatcher = Functions.subString(soapEyecatcher, Len.SOAP_EYECATCHER);
	}

	public String getSoapEyecatcher() {
		return this.soapEyecatcher;
	}

	public void setSoapLayoutVersion(String soapLayoutVersion) {
		this.soapLayoutVersion = Functions.subString(soapLayoutVersion, Len.SOAP_LAYOUT_VERSION);
	}

	public String getSoapLayoutVersion() {
		return this.soapLayoutVersion;
	}

	public void setSoapFilePrefix(String soapFilePrefix) {
		this.soapFilePrefix = Functions.subString(soapFilePrefix, Len.SOAP_FILE_PREFIX);
	}

	public String getSoapFilePrefix() {
		return this.soapFilePrefix;
	}

	public CallableInputs getCallableInputs() {
		return callableInputs;
	}

	public NonStaticFields getNonStaticFields() {
		return nonStaticFields;
	}

	@Override
	public byte[] serialize() {
		return getIvoryhBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SOAP_EYECATCHER = 12;
		public static final int SOAP_LAYOUT_VERSION = 2;
		public static final int SOAP_FILE_PREFIX = 2;
		public static final int IVORYH = SOAP_EYECATCHER + SOAP_LAYOUT_VERSION + SOAP_FILE_PREFIX + NonStaticFields.Len.NON_STATIC_FIELDS
				+ CallableInputs.Len.CALLABLE_INPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
