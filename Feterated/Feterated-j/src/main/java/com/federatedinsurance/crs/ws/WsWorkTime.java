/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-WORK-TIME<br>
 * Variable: WS-WORK-TIME from program XPIODAT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkTime {

	//==== PROPERTIES ====
	//Original name: WS-WORK-HOURS
	private String wsWorkHours = DefaultValues.stringVal(Len.WS_WORK_HOURS);
	//Original name: WS-WORK-HMS
	private char wsWorkHms = DefaultValues.CHAR_VAL;
	//Original name: WS-WORK-MINUTES
	private String wsWorkMinutes = DefaultValues.stringVal(Len.WS_WORK_MINUTES);
	//Original name: WS-WORK-MSS
	private char wsWorkMss = DefaultValues.CHAR_VAL;
	//Original name: WS-WORK-SECONDS
	private String wsWorkSeconds = DefaultValues.stringVal(Len.WS_WORK_SECONDS);
	//Original name: WS-WORK-SMS
	private char wsWorkSms = DefaultValues.CHAR_VAL;
	//Original name: WS-WORK-MSECS-RED
	private WsWorkMsecsRed wsWorkMsecsRed = new WsWorkMsecsRed();
	//Original name: WS-WORK-AMPMS
	private char wsWorkAmpms = DefaultValues.CHAR_VAL;
	//Original name: WS-WORK-AMPM-POS1
	private char wsWorkAmpmPos1 = DefaultValues.CHAR_VAL;
	//Original name: WS-WORK-AMPM-POS2
	private char wsWorkAmpmPos2 = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public String getWsWorkTimeFormatted() {
		return MarshalByteExt.bufferToStr(getWsWorkTimeBytes());
	}

	public byte[] getWsWorkTimeBytes() {
		byte[] buffer = new byte[Len.WS_WORK_TIME];
		return getWsWorkTimeBytes(buffer, 1);
	}

	public byte[] getWsWorkTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, wsWorkHours, Len.WS_WORK_HOURS);
		position += Len.WS_WORK_HOURS;
		MarshalByte.writeChar(buffer, position, wsWorkHms);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, wsWorkMinutes, Len.WS_WORK_MINUTES);
		position += Len.WS_WORK_MINUTES;
		MarshalByte.writeChar(buffer, position, wsWorkMss);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, wsWorkSeconds, Len.WS_WORK_SECONDS);
		position += Len.WS_WORK_SECONDS;
		MarshalByte.writeChar(buffer, position, wsWorkSms);
		position += Types.CHAR_SIZE;
		wsWorkMsecsRed.getWsWorkMsecsRedBytes(buffer, position);
		position += WsWorkMsecsRed.Len.WS_WORK_MSECS_RED;
		MarshalByte.writeChar(buffer, position, wsWorkAmpms);
		position += Types.CHAR_SIZE;
		getWsWorkAmpmBytes(buffer, position);
		return buffer;
	}

	public void initWsWorkTimeZeroes() {
		wsWorkHours = "00";
		wsWorkHms = '0';
		wsWorkMinutes = "00";
		wsWorkMss = '0';
		wsWorkSeconds = "00";
		wsWorkSms = '0';
		wsWorkMsecsRed.initWsWorkMsecsRedZeroes();
		wsWorkAmpms = '0';
		initWsWorkAmpmZeroes();
	}

	public void initWsWorkTimeSpaces() {
		wsWorkHours = "";
		wsWorkHms = Types.SPACE_CHAR;
		wsWorkMinutes = "";
		wsWorkMss = Types.SPACE_CHAR;
		wsWorkSeconds = "";
		wsWorkSms = Types.SPACE_CHAR;
		wsWorkMsecsRed.initWsWorkMsecsRedSpaces();
		wsWorkAmpms = Types.SPACE_CHAR;
		initWsWorkAmpmSpaces();
	}

	public void setWsWorkHours(short wsWorkHours) {
		this.wsWorkHours = NumericDisplay.asString(wsWorkHours, Len.WS_WORK_HOURS);
	}

	public void setWsWorkHoursFormatted(String wsWorkHours) {
		this.wsWorkHours = Trunc.toUnsignedNumeric(wsWorkHours, Len.WS_WORK_HOURS);
	}

	public short getWsWorkHours() {
		return NumericDisplay.asShort(this.wsWorkHours);
	}

	public String getWsWorkHoursFormatted() {
		return this.wsWorkHours;
	}

	public void setWsWorkHms(char wsWorkHms) {
		this.wsWorkHms = wsWorkHms;
	}

	public char getWsWorkHms() {
		return this.wsWorkHms;
	}

	public void setWsWorkMinutes(short wsWorkMinutes) {
		this.wsWorkMinutes = NumericDisplay.asString(wsWorkMinutes, Len.WS_WORK_MINUTES);
	}

	public void setWsWorkMinutesFormatted(String wsWorkMinutes) {
		this.wsWorkMinutes = Trunc.toUnsignedNumeric(wsWorkMinutes, Len.WS_WORK_MINUTES);
	}

	public short getWsWorkMinutes() {
		return NumericDisplay.asShort(this.wsWorkMinutes);
	}

	public String getWsWorkMinutesFormatted() {
		return this.wsWorkMinutes;
	}

	public void setWsWorkMss(char wsWorkMss) {
		this.wsWorkMss = wsWorkMss;
	}

	public char getWsWorkMss() {
		return this.wsWorkMss;
	}

	public void setWsWorkSeconds(short wsWorkSeconds) {
		this.wsWorkSeconds = NumericDisplay.asString(wsWorkSeconds, Len.WS_WORK_SECONDS);
	}

	public void setWsWorkSecondsFormatted(String wsWorkSeconds) {
		this.wsWorkSeconds = Trunc.toUnsignedNumeric(wsWorkSeconds, Len.WS_WORK_SECONDS);
	}

	public short getWsWorkSeconds() {
		return NumericDisplay.asShort(this.wsWorkSeconds);
	}

	public String getWsWorkSecondsFormatted() {
		return this.wsWorkSeconds;
	}

	public void setWsWorkSms(char wsWorkSms) {
		this.wsWorkSms = wsWorkSms;
	}

	public char getWsWorkSms() {
		return this.wsWorkSms;
	}

	public char getWsWorkAmpms() {
		return this.wsWorkAmpms;
	}

	public String getWsWorkAmpmFormatted() {
		return MarshalByteExt.bufferToStr(getWsWorkAmpmBytes());
	}

	/**Original name: WS-WORK-AMPM<br>*/
	public byte[] getWsWorkAmpmBytes() {
		byte[] buffer = new byte[Len.WS_WORK_AMPM];
		return getWsWorkAmpmBytes(buffer, 1);
	}

	public byte[] getWsWorkAmpmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, wsWorkAmpmPos1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, wsWorkAmpmPos2);
		return buffer;
	}

	public void initWsWorkAmpmZeroes() {
		wsWorkAmpmPos1 = '0';
		wsWorkAmpmPos2 = '0';
	}

	public void initWsWorkAmpmSpaces() {
		wsWorkAmpmPos1 = Types.SPACE_CHAR;
		wsWorkAmpmPos2 = Types.SPACE_CHAR;
	}

	public char getWsWorkAmpmPos1() {
		return this.wsWorkAmpmPos1;
	}

	public char getWsWorkAmpmPos2() {
		return this.wsWorkAmpmPos2;
	}

	public WsWorkMsecsRed getWsWorkMsecsRed() {
		return wsWorkMsecsRed;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_WORK_HOURS = 2;
		public static final int WS_WORK_MINUTES = 2;
		public static final int WS_WORK_SECONDS = 2;
		public static final int WS_WORK_HMS = 1;
		public static final int WS_WORK_MSS = 1;
		public static final int WS_WORK_SMS = 1;
		public static final int WS_WORK_AMPMS = 1;
		public static final int WS_WORK_AMPM_POS1 = 1;
		public static final int WS_WORK_AMPM_POS2 = 1;
		public static final int WS_WORK_AMPM = WS_WORK_AMPM_POS1 + WS_WORK_AMPM_POS2;
		public static final int WS_WORK_TIME = WS_WORK_HOURS + WS_WORK_HMS + WS_WORK_MINUTES + WS_WORK_MSS + WS_WORK_SECONDS + WS_WORK_SMS
				+ WsWorkMsecsRed.Len.WS_WORK_MSECS_RED + WS_WORK_AMPMS + WS_WORK_AMPM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
