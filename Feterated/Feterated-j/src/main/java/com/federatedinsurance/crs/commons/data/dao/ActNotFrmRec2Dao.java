/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRec2;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for tables [ACT_NOT_FRM, ACT_NOT_FRM_REC]
 * 
 */
public class ActNotFrmRec2Dao extends BaseSqlDao<IActNotFrmRec2> {

	public ActNotFrmRec2Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotFrmRec2> getToClass() {
		return IActNotFrmRec2.class;
	}

	public String selectRec(String csrActNbr, String notPrcTs, String dft) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).scalarResultString(dft);
	}
}
