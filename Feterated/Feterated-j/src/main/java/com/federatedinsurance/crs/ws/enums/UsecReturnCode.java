/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: USEC-RETURN-CODE<br>
 * Variable: USEC-RETURN-CODE from copybook HALLUSEC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UsecReturnCode {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.RETURN_CODE);
	public static final String SEC_OK = "000";
	public static final String CICS_ERROR = "001";
	public static final String DB2_ERROR = "002";
	public static final String COMMAREA_ERROR = "003";
	public static final String BUSPROC_ERROR = "004";
	public static final String USER_UOW_AUTH_ASS_NOT_FND = "005";
	public static final String USER_UOW_AUTH_ASS_FAILED = "006";
	public static final String USER_CLTID_NOT_FOUND = "007";
	public static final String CLT_USER_ID_NOT_FOUND = "008";
	public static final String USER_MISMATCH_FND = "009";

	//==== METHODS ====
	public void setReturnCode(short returnCode) {
		this.value = NumericDisplay.asString(returnCode, Len.RETURN_CODE);
	}

	public void setReturnCodeFormatted(String returnCode) {
		this.value = Trunc.toUnsignedNumeric(returnCode, Len.RETURN_CODE);
	}

	public short getReturnCode() {
		return NumericDisplay.asShort(this.value);
	}

	public String getReturnCodeFormatted() {
		return this.value;
	}

	public void setSecOk() {
		setReturnCodeFormatted(SEC_OK);
	}

	public boolean isUserUowAuthAssNotFnd() {
		return getReturnCodeFormatted().equals(USER_UOW_AUTH_ASS_NOT_FND);
	}

	public boolean isUserCltidNotFound() {
		return getReturnCodeFormatted().equals(USER_CLTID_NOT_FOUND);
	}

	public boolean isUserMismatchFnd() {
		return getReturnCodeFormatted().equals(USER_MISMATCH_FND);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURN_CODE = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
