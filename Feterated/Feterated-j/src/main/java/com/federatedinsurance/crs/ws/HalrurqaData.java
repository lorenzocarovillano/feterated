/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.Ws0215RecFoundSw;
import com.federatedinsurance.crs.ws.enums.WsClearUowRequestSwitch;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALRURQA<br>
 * Generated as a class for rule WS.<br>*/
public class HalrurqaData {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "HALRURQA";
	//Original name: WS0210-MAX-REC-SEQ
	private String ws0210MaxRecSeq = DefaultValues.stringVal(Len.WS0210_MAX_REC_SEQ);
	//Original name: WS0215-REC-FOUND-SW
	private Ws0215RecFoundSw ws0215RecFoundSw = new Ws0215RecFoundSw();
	//Original name: WS-CLEAR-UOW-REQUEST-SWITCH
	private WsClearUowRequestSwitch wsClearUowRequestSwitch = new WsClearUowRequestSwitch();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public void setWs0210MaxRecSeq(int ws0210MaxRecSeq) {
		this.ws0210MaxRecSeq = NumericDisplay.asString(ws0210MaxRecSeq, Len.WS0210_MAX_REC_SEQ);
	}

	public void setWs0210MaxRecSeqFormatted(String ws0210MaxRecSeq) {
		this.ws0210MaxRecSeq = Trunc.toUnsignedNumeric(ws0210MaxRecSeq, Len.WS0210_MAX_REC_SEQ);
	}

	public int getWs0210MaxRecSeq() {
		return NumericDisplay.asInt(this.ws0210MaxRecSeq);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public Ws0215RecFoundSw getWs0215RecFoundSw() {
		return ws0215RecFoundSw;
	}

	public WsClearUowRequestSwitch getWsClearUowRequestSwitch() {
		return wsClearUowRequestSwitch;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS0210_MAX_REC_SEQ = 5;
		public static final int WS_APPLID = 8;
		public static final int WS_PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
