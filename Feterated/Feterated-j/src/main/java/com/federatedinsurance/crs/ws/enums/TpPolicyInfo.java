/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TP-POLICY-INFO<br>
 * Variable: TP-POLICY-INFO from program XZ0P90H0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TpPolicyInfo {

	//==== PROPERTIES ====
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TP_POLICY_INFO);
	//Original name: TP-SORT-ORDER
	private char sortOrder = DefaultValues.CHAR_VAL;
	//Original name: TP-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: TP-POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: TP-POL-RSK-ST-ABB
	private String polRskStAbb = DefaultValues.stringVal(Len.POL_RSK_ST_ABB);
	//Original name: TP-NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);
	//Original name: TP-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: TP-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: TP-POL-DUE-AMT
	private AfDecimal polDueAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: TP-POL-BIL-STA-CD
	private char polBilStaCd = DefaultValues.CHAR_VAL;
	//Original name: TP-CLT-ID
	private String cltId = DefaultValues.stringVal(Len.CLT_ID);
	//Original name: TP-ADR-ID
	private String adrId = DefaultValues.stringVal(Len.ADR_ID);
	//Original name: TP-ADR-1
	private String adr1 = DefaultValues.stringVal(Len.ADR1);
	//Original name: TP-ADR-2
	private String adr2 = DefaultValues.stringVal(Len.ADR2);
	//Original name: TP-CIT-NM
	private String citNm = DefaultValues.stringVal(Len.CIT_NM);
	//Original name: TP-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: TP-PST-CD
	private String pstCd = DefaultValues.stringVal(Len.PST_CD);
	//Original name: TP-NAME
	private String name = DefaultValues.stringVal(Len.NAME);

	//==== METHODS ====
	public String getTpPolicyInfoFormatted() {
		return MarshalByteExt.bufferToStr(getTpPolicyInfoBytes());
	}

	public byte[] getTpPolicyInfoBytes() {
		byte[] buffer = new byte[Len.TP_POLICY_INFO];
		return getTpPolicyInfoBytes(buffer, 1);
	}

	public byte[] getTpPolicyInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, sortOrder);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polTypCd, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		MarshalByte.writeString(buffer, position, polRskStAbb, Len.POL_RSK_ST_ABB);
		position += Len.POL_RSK_ST_ABB;
		MarshalByte.writeString(buffer, position, notEffDt, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeDecimal(buffer, position, polDueAmt.copy());
		position += Len.POL_DUE_AMT;
		MarshalByte.writeChar(buffer, position, polBilStaCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cltId, Len.CLT_ID);
		position += Len.CLT_ID;
		MarshalByte.writeString(buffer, position, adrId, Len.ADR_ID);
		position += Len.ADR_ID;
		MarshalByte.writeString(buffer, position, adr1, Len.ADR1);
		position += Len.ADR1;
		MarshalByte.writeString(buffer, position, adr2, Len.ADR2);
		position += Len.ADR2;
		MarshalByte.writeString(buffer, position, citNm, Len.CIT_NM);
		position += Len.CIT_NM;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeString(buffer, position, pstCd, Len.PST_CD);
		position += Len.PST_CD;
		MarshalByte.writeString(buffer, position, name, Len.NAME);
		return buffer;
	}

	public void initTpPolicyInfoHighValues() {
		sortOrder = Types.HIGH_CHAR_VAL;
		polNbr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_NBR);
		polTypCd = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_TYP_CD);
		polRskStAbb = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_RSK_ST_ABB);
		notEffDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NOT_EFF_DT);
		polEffDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_EFF_DT);
		polExpDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_EXP_DT);
		polDueAmt.setHigh();
		polBilStaCd = Types.HIGH_CHAR_VAL;
		cltId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CLT_ID);
		adrId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ADR_ID);
		adr1 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ADR1);
		adr2 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ADR2);
		citNm = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CIT_NM);
		stAbb = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ST_ABB);
		pstCd = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PST_CD);
		name = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NAME);
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTpPolicyInfoFormatted()).equals(END_OF_TABLE);
	}

	public void setSortOrder(char sortOrder) {
		this.sortOrder = sortOrder;
	}

	public char getSortOrder() {
		return this.sortOrder;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	public String getPolTypCd() {
		return this.polTypCd;
	}

	public void setPolRskStAbb(String polRskStAbb) {
		this.polRskStAbb = Functions.subString(polRskStAbb, Len.POL_RSK_ST_ABB);
	}

	public String getPolRskStAbb() {
		return this.polRskStAbb;
	}

	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	public String getNotEffDt() {
		return this.notEffDt;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.polDueAmt.assign(polDueAmt);
	}

	public AfDecimal getPolDueAmt() {
		return this.polDueAmt.copy();
	}

	public void setPolBilStaCd(char polBilStaCd) {
		this.polBilStaCd = polBilStaCd;
	}

	public char getPolBilStaCd() {
		return this.polBilStaCd;
	}

	public void setCltId(String cltId) {
		this.cltId = Functions.subString(cltId, Len.CLT_ID);
	}

	public String getCltId() {
		return this.cltId;
	}

	public void setAdrId(String adrId) {
		this.adrId = Functions.subString(adrId, Len.ADR_ID);
	}

	public String getAdrId() {
		return this.adrId;
	}

	public void setAdr1(String adr1) {
		this.adr1 = Functions.subString(adr1, Len.ADR1);
	}

	public String getAdr1() {
		return this.adr1;
	}

	public void setAdr2(String adr2) {
		this.adr2 = Functions.subString(adr2, Len.ADR2);
	}

	public String getAdr2() {
		return this.adr2;
	}

	public void setCitNm(String citNm) {
		this.citNm = Functions.subString(citNm, Len.CIT_NM);
	}

	public String getCitNm() {
		return this.citNm;
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public void setPstCd(String pstCd) {
		this.pstCd = Functions.subString(pstCd, Len.PST_CD);
	}

	public String getPstCd() {
		return this.pstCd;
	}

	public void setName(String name) {
		this.name = Functions.subString(name, Len.NAME);
	}

	public String getName() {
		return this.name;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SORT_ORDER = 1;
		public static final int POL_NBR = 25;
		public static final int POL_TYP_CD = 3;
		public static final int POL_RSK_ST_ABB = 2;
		public static final int NOT_EFF_DT = 10;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int POL_DUE_AMT = 10;
		public static final int POL_BIL_STA_CD = 1;
		public static final int CLT_ID = 64;
		public static final int ADR_ID = 64;
		public static final int ADR1 = 45;
		public static final int ADR2 = 45;
		public static final int CIT_NM = 30;
		public static final int ST_ABB = 3;
		public static final int PST_CD = 13;
		public static final int NAME = 120;
		public static final int TP_POLICY_INFO = SORT_ORDER + POL_NBR + POL_TYP_CD + POL_RSK_ST_ABB + NOT_EFF_DT + POL_EFF_DT + POL_EXP_DT
				+ POL_DUE_AMT + POL_BIL_STA_CD + CLT_ID + ADR_ID + ADR1 + ADR2 + CIT_NM + ST_ABB + PST_CD + NAME;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
