/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: MDRV-LOGGABLE-ERR-LOG-ONLY-SW<br>
 * Variable: MDRV-LOGGABLE-ERR-LOG-ONLY-SW from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvLoggableErrLogOnlySw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char REQUIRED = 'Y';
	public static final char NOT_REQUIRED = 'N';
	public static final char NOT_SET = ' ';

	//==== METHODS ====
	public void setLoggableErrLogOnlySw(char loggableErrLogOnlySw) {
		this.value = loggableErrLogOnlySw;
	}

	public char getLoggableErrLogOnlySw() {
		return this.value;
	}

	public void setNotSet() {
		value = NOT_SET;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LOGGABLE_ERR_LOG_ONLY_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
