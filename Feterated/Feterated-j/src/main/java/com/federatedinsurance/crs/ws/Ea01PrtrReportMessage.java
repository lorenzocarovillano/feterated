/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;

/**Original name: EA-01-PRTR-REPORT-MESSAGE<br>
 * Variable: EA-01-PRTR-REPORT-MESSAGE from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea01PrtrReportMessage {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-PRTR-REPORT-MESSAGE
	private String flr1 = "";
	//Original name: EA-01-MONTH-DAY
	private String monthDay = DefaultValues.stringVal(Len.MONTH_DAY);
	//Original name: EA-01-YEAR
	private String year = DefaultValues.stringVal(Len.YEAR);
	//Original name: FILLER-EA-01-PRTR-REPORT-MESSAGE-1
	private String flr2 = "";
	//Original name: FILLER-EA-01-PRTR-REPORT-MESSAGE-2
	private String flr3 = "**********";
	//Original name: FILLER-EA-01-PRTR-REPORT-MESSAGE-3
	private String flr4 = "REPORT NUMBER =";
	//Original name: FILLER-EA-01-PRTR-REPORT-MESSAGE-4
	private String flr5 = "   PRTR *******";
	//Original name: FILLER-EA-01-PRTR-REPORT-MESSAGE-5
	private String flr6 = "***";
	//Original name: FILLER-EA-01-PRTR-REPORT-MESSAGE-6
	private String flr7 = "";
	//Original name: EA-01-TIME-OF-DAY
	private Ea01TimeOfDay timeOfDay = new Ea01TimeOfDay();

	//==== METHODS ====
	public String getEa01PrtrReportMessageFormatted() {
		return MarshalByteExt.bufferToStr(getEa01PrtrReportMessageBytes());
	}

	public byte[] getEa01PrtrReportMessageBytes() {
		byte[] buffer = new byte[Len.EA01_PRTR_REPORT_MESSAGE];
		return getEa01PrtrReportMessageBytes(buffer, 1);
	}

	public byte[] getEa01PrtrReportMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		getCurrentDateBytes(buffer, position);
		position += Len.CURRENT_DATE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR2);
		position += Len.FLR2;
		timeOfDay.getTimeOfDayBytes(buffer, position);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public byte[] getCurrentDateBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, monthDay, Len.MONTH_DAY);
		position += Len.MONTH_DAY;
		MarshalByte.writeString(buffer, position, year, Len.YEAR);
		return buffer;
	}

	public void setMonthDayFormatted(String monthDay) {
		this.monthDay = PicFormatter.display("X(2)/X(2)/").format(monthDay).toString();
	}

	public String getMonthDay() {
		return Functions.subString(this.monthDay, Len.MONTH_DAY);
	}

	public void setYear(String year) {
		this.year = Functions.subString(year, Len.YEAR);
	}

	public String getYear() {
		return this.year;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public Ea01TimeOfDay getTimeOfDay() {
		return timeOfDay;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MONTH_DAY = 6;
		public static final int YEAR = 4;
		public static final int FLR1 = 32;
		public static final int CURRENT_DATE = MONTH_DAY + YEAR;
		public static final int FLR2 = 2;
		public static final int FLR3 = 11;
		public static final int FLR4 = 15;
		public static final int FLR6 = 3;
		public static final int EA01_PRTR_REPORT_MESSAGE = CURRENT_DATE + Ea01TimeOfDay.Len.TIME_OF_DAY + FLR1 + 2 * FLR2 + FLR3 + 2 * FLR4 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
