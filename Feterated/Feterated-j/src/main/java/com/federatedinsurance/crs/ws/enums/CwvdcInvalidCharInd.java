/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: CWVDC-INVALID-CHAR-IND<br>
 * Variable: CWVDC-INVALID-CHAR-IND from copybook CAWLCVDC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CwvdcInvalidCharInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NOT_FOUND = 'N';
	public static final char FOUND = 'Y';

	//==== METHODS ====
	public void setInvalidCharInd(char invalidCharInd) {
		this.value = invalidCharInd;
	}

	public char getInvalidCharInd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int INVALID_CHAR_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
