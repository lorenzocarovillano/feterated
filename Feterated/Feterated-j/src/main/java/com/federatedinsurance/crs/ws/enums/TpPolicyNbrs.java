/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: TP-POLICY-NBRS<br>
 * Variable: TP-POLICY-NBRS from program XZ001000<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TpPolicyNbrs {

	//==== PROPERTIES ====
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TP_POLICY_NBRS);
	//Original name: TP-POL-NBR-7
	private String polNbr7 = DefaultValues.stringVal(Len.POL_NBR7);
	//Original name: FILLER-TP-POL-NBR
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: TP-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);

	//==== METHODS ====
	public String getTpPolicyNbrsFormatted() {
		return MarshalByteExt.bufferToStr(getTpPolicyNbrsBytes());
	}

	public byte[] getTpPolicyNbrsBytes() {
		byte[] buffer = new byte[Len.TP_POLICY_NBRS];
		return getTpPolicyNbrsBytes(buffer, 1);
	}

	public byte[] getTpPolicyNbrsBytes(byte[] buffer, int offset) {
		int position = offset;
		getPolNbrBytes(buffer, position);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		return buffer;
	}

	public void initTpPolicyNbrsHighValues() {
		initPolNbrHighValues();
		notPrcTs = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NOT_PRC_TS);
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTpPolicyNbrsFormatted()).equals(END_OF_TABLE);
	}

	public void setPolNbrFormatted(String data) {
		byte[] buffer = new byte[Len.POL_NBR];
		MarshalByte.writeString(buffer, 1, data, Len.POL_NBR);
		setPolNbrBytes(buffer, 1);
	}

	public String getPolNbrFormatted() {
		return MarshalByteExt.bufferToStr(getPolNbrBytes());
	}

	/**Original name: TP-POL-NBR<br>*/
	public byte[] getPolNbrBytes() {
		byte[] buffer = new byte[Len.POL_NBR];
		return getPolNbrBytes(buffer, 1);
	}

	public void setPolNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		polNbr7 = MarshalByte.readString(buffer, position, Len.POL_NBR7);
		position += Len.POL_NBR7;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getPolNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr7, Len.POL_NBR7);
		position += Len.POL_NBR7;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void initPolNbrHighValues() {
		polNbr7 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_NBR7);
		flr1 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1);
	}

	public void setPolNbr7(String polNbr7) {
		this.polNbr7 = Functions.subString(polNbr7, Len.POL_NBR7);
	}

	public String getPolNbr7() {
		return this.polNbr7;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR7 = 7;
		public static final int FLR1 = 18;
		public static final int POL_NBR = POL_NBR7 + FLR1;
		public static final int NOT_PRC_TS = 26;
		public static final int TP_POLICY_NBRS = POL_NBR + NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
