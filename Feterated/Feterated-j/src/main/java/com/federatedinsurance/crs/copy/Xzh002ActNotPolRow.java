/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPol;
import com.federatedinsurance.crs.commons.data.to.IStsPolTyp;

/**Original name: XZH002-ACT-NOT-POL-ROW<br>
 * Variable: XZH002-ACT-NOT-POL-ROW from copybook XZ0H0002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzh002ActNotPolRow implements IStsPolTyp, IActNotPol {

	//==== PROPERTIES ====
	//Original name: XZH002-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZH002-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZH002-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZH002-POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: XZH002-POL-PRI-RSK-ST-ABB
	private String polPriRskStAbb = DefaultValues.stringVal(Len.POL_PRI_RSK_ST_ABB);
	//Original name: XZH002-NOT-EFF-DT-NI
	private short notEffDtNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH002-NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);
	//Original name: XZH002-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZH002-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZH002-POL-DUE-AMT-NI
	private short polDueAmtNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH002-POL-DUE-AMT
	private AfDecimal polDueAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: XZH002-NIN-CLT-ID
	private String ninCltId = DefaultValues.stringVal(Len.NIN_CLT_ID);
	//Original name: XZH002-NIN-ADR-ID
	private String ninAdrId = DefaultValues.stringVal(Len.NIN_ADR_ID);
	//Original name: XZH002-WF-STARTED-IND-NI
	private short wfStartedIndNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH002-WF-STARTED-IND
	private char wfStartedInd = DefaultValues.CHAR_VAL;
	//Original name: XZH002-POL-BIL-STA-CD-NI
	private short polBilStaCdNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH002-POL-BIL-STA-CD
	private char polBilStaCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public String getXzh002ActNotPolRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzh002ActNotPolRowBytes());
	}

	public byte[] getXzh002ActNotPolRowBytes() {
		byte[] buffer = new byte[Len.XZH002_ACT_NOT_POL_ROW];
		return getXzh002ActNotPolRowBytes(buffer, 1);
	}

	public byte[] getXzh002ActNotPolRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polTypCd, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		MarshalByte.writeString(buffer, position, polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
		position += Len.POL_PRI_RSK_ST_ABB;
		MarshalByte.writeBinaryShort(buffer, position, notEffDtNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, notEffDt, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeBinaryShort(buffer, position, polDueAmtNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, polDueAmt.copy());
		position += Len.POL_DUE_AMT;
		MarshalByte.writeString(buffer, position, ninCltId, Len.NIN_CLT_ID);
		position += Len.NIN_CLT_ID;
		MarshalByte.writeString(buffer, position, ninAdrId, Len.NIN_ADR_ID);
		position += Len.NIN_ADR_ID;
		MarshalByte.writeBinaryShort(buffer, position, wfStartedIndNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeChar(buffer, position, wfStartedInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeBinaryShort(buffer, position, polBilStaCdNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeChar(buffer, position, polBilStaCd);
		return buffer;
	}

	public void initXzh002ActNotPolRowSpaces() {
		csrActNbr = "";
		notPrcTs = "";
		polNbr = "";
		polTypCd = "";
		polPriRskStAbb = "";
		notEffDtNi = Types.INVALID_BINARY_SHORT_VAL;
		notEffDt = "";
		polEffDt = "";
		polExpDt = "";
		polDueAmtNi = Types.INVALID_BINARY_SHORT_VAL;
		polDueAmt.setNaN();
		ninCltId = "";
		ninAdrId = "";
		wfStartedIndNi = Types.INVALID_BINARY_SHORT_VAL;
		wfStartedInd = Types.SPACE_CHAR;
		polBilStaCdNi = Types.INVALID_BINARY_SHORT_VAL;
		polBilStaCd = Types.SPACE_CHAR;
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	@Override
	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	@Override
	public String getPolTypCd() {
		return this.polTypCd;
	}

	@Override
	public void setPolPriRskStAbb(String polPriRskStAbb) {
		this.polPriRskStAbb = Functions.subString(polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
	}

	@Override
	public String getPolPriRskStAbb() {
		return this.polPriRskStAbb;
	}

	public void setNotEffDtNi(short notEffDtNi) {
		this.notEffDtNi = notEffDtNi;
	}

	public short getNotEffDtNi() {
		return this.notEffDtNi;
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	@Override
	public String getNotEffDt() {
		return this.notEffDt;
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	@Override
	public String getPolEffDt() {
		return this.polEffDt;
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	@Override
	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setPolDueAmtNi(short polDueAmtNi) {
		this.polDueAmtNi = polDueAmtNi;
	}

	public short getPolDueAmtNi() {
		return this.polDueAmtNi;
	}

	@Override
	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.polDueAmt.assign(polDueAmt);
	}

	@Override
	public AfDecimal getPolDueAmt() {
		return this.polDueAmt.copy();
	}

	@Override
	public void setNinCltId(String ninCltId) {
		this.ninCltId = Functions.subString(ninCltId, Len.NIN_CLT_ID);
	}

	@Override
	public String getNinCltId() {
		return this.ninCltId;
	}

	@Override
	public void setNinAdrId(String ninAdrId) {
		this.ninAdrId = Functions.subString(ninAdrId, Len.NIN_ADR_ID);
	}

	@Override
	public String getNinAdrId() {
		return this.ninAdrId;
	}

	public void setWfStartedIndNi(short wfStartedIndNi) {
		this.wfStartedIndNi = wfStartedIndNi;
	}

	public short getWfStartedIndNi() {
		return this.wfStartedIndNi;
	}

	@Override
	public void setWfStartedInd(char wfStartedInd) {
		this.wfStartedInd = wfStartedInd;
	}

	@Override
	public char getWfStartedInd() {
		return this.wfStartedInd;
	}

	public void setPolBilStaCdNi(short polBilStaCdNi) {
		this.polBilStaCdNi = polBilStaCdNi;
	}

	public short getPolBilStaCdNi() {
		return this.polBilStaCdNi;
	}

	@Override
	public void setPolBilStaCd(char polBilStaCd) {
		this.polBilStaCd = polBilStaCd;
	}

	@Override
	public char getPolBilStaCd() {
		return this.polBilStaCd;
	}

	@Override
	public String getNotEffDtObj() {
		return getNotEffDt();
	}

	@Override
	public void setNotEffDtObj(String notEffDtObj) {
		setNotEffDt(notEffDtObj);
	}

	@Override
	public Character getPolBilStaCdObj() {
		return (getPolBilStaCd());
	}

	@Override
	public void setPolBilStaCdObj(Character polBilStaCdObj) {
		setPolBilStaCd((polBilStaCdObj));
	}

	@Override
	public AfDecimal getPolDueAmtObj() {
		return getPolDueAmt().toString();
	}

	@Override
	public void setPolDueAmtObj(AfDecimal polDueAmtObj) {
		setPolDueAmt(new AfDecimal(polDueAmtObj, 10, 2));
	}

	@Override
	public Character getWfStartedIndObj() {
		return (getWfStartedInd());
	}

	@Override
	public void setWfStartedIndObj(Character wfStartedIndObj) {
		setWfStartedInd((wfStartedIndObj));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int POL_NBR = 25;
		public static final int POL_TYP_CD = 3;
		public static final int POL_PRI_RSK_ST_ABB = 2;
		public static final int NOT_EFF_DT = 10;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int NIN_CLT_ID = 64;
		public static final int NIN_ADR_ID = 64;
		public static final int NOT_EFF_DT_NI = 2;
		public static final int POL_DUE_AMT_NI = 2;
		public static final int POL_DUE_AMT = 6;
		public static final int WF_STARTED_IND_NI = 2;
		public static final int WF_STARTED_IND = 1;
		public static final int POL_BIL_STA_CD_NI = 2;
		public static final int POL_BIL_STA_CD = 1;
		public static final int XZH002_ACT_NOT_POL_ROW = CSR_ACT_NBR + NOT_PRC_TS + POL_NBR + POL_TYP_CD + POL_PRI_RSK_ST_ABB + NOT_EFF_DT_NI
				+ NOT_EFF_DT + POL_EFF_DT + POL_EXP_DT + POL_DUE_AMT_NI + POL_DUE_AMT + NIN_CLT_ID + NIN_ADR_ID + WF_STARTED_IND_NI + WF_STARTED_IND
				+ POL_BIL_STA_CD_NI + POL_BIL_STA_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
