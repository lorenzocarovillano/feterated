/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-04-SOAP-FAULT<br>
 * Variable: EA-04-SOAP-FAULT from program FNC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea04SoapFaultFnc02090 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-04-SOAP-FAULT
	private String flr1 = "SOAP FAULT";
	//Original name: FILLER-EA-04-SOAP-FAULT-1
	private String flr2 = "IN CALLABLE";
	//Original name: FILLER-EA-04-SOAP-FAULT-2
	private String flr3 = "SERVICE.";
	//Original name: FILLER-EA-04-SOAP-FAULT-3
	private String flr4 = "SOAP RETURN";
	//Original name: FILLER-EA-04-SOAP-FAULT-4
	private String flr5 = "CODE:";
	//Original name: EA-04-SOAP-RETURN-CODE
	private String code = DefaultValues.stringVal(Len.CODE);
	//Original name: FILLER-EA-04-SOAP-FAULT-5
	private String flr6 = "MESSAGE:";
	//Original name: EA-04-SOAP-RETURN-MSG
	private String msg = DefaultValues.stringVal(Len.MSG);

	//==== METHODS ====
	public String getEa04SoapFaultFormatted() {
		return MarshalByteExt.bufferToStr(getEa04SoapFaultBytes());
	}

	public byte[] getEa04SoapFaultBytes() {
		byte[] buffer = new byte[Len.EA04_SOAP_FAULT];
		return getEa04SoapFaultBytes(buffer, 1);
	}

	public byte[] getEa04SoapFaultBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, code, Len.CODE);
		position += Len.CODE;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, msg, Len.MSG);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setCode(int code) {
		this.code = NumericDisplay.asString(code, Len.CODE);
	}

	public int getCode() {
		return NumericDisplay.asInt(this.code);
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setMsg(String msg) {
		this.msg = Functions.subString(msg, Len.MSG);
	}

	public String getMsg() {
		return this.msg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODE = 8;
		public static final int MSG = 400;
		public static final int FLR1 = 11;
		public static final int FLR2 = 12;
		public static final int FLR3 = 9;
		public static final int FLR5 = 6;
		public static final int EA04_SOAP_FAULT = CODE + MSG + FLR1 + 2 * FLR2 + 2 * FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
