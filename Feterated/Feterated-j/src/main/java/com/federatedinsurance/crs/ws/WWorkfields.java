/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.federatedinsurance.crs.ws.enums.WHalouidgSearchInd;

/**Original name: W-WORKFIELDS<br>
 * Variable: W-WORKFIELDS from program HALOETRA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WWorkfields {

	//==== PROPERTIES ====
	//Original name: W-COUNT
	private short count = DefaultValues.SHORT_VAL;
	//Original name: W-HALOUIDG-SEARCH-IND
	private WHalouidgSearchInd halouidgSearchInd = new WHalouidgSearchInd();

	//==== METHODS ====
	public void setCount(short count) {
		this.count = count;
	}

	public short getCount() {
		return this.count;
	}

	public WHalouidgSearchInd getHalouidgSearchInd() {
		return halouidgSearchInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int COUNT = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
