/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalBoMduXrfVUowObjHierPrcSeq;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [HAL_BO_MDU_XRF_V, HAL_UOW_OBJ_HIER_V, HAL_UOW_PRC_SEQ_V]
 * 
 */
public class HalBoMduXrfVUowObjHierPrcSeqDao extends BaseSqlDao<IHalBoMduXrfVUowObjHierPrcSeq> {

	private Cursor cursor1;
	private final IRowMapper<IHalBoMduXrfVUowObjHierPrcSeq> fetchCursor1Rm = buildNamedRowMapper(IHalBoMduXrfVUowObjHierPrcSeq.class, "wsCur1BobjNm",
			"hbmxDpDflMduNm");

	public HalBoMduXrfVUowObjHierPrcSeqDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalBoMduXrfVUowObjHierPrcSeq> getToClass() {
		return IHalBoMduXrfVUowObjHierPrcSeq.class;
	}

	public DbAccessStatus openCursor1(String psUowNm, String ohUowNm) {
		cursor1 = buildQuery("openCursor1").bind("psUowNm", psUowNm).bind("ohUowNm", ohUowNm).open();
		return dbStatus;
	}

	public IHalBoMduXrfVUowObjHierPrcSeq fetchCursor1(IHalBoMduXrfVUowObjHierPrcSeq iHalBoMduXrfVUowObjHierPrcSeq) {
		return fetch(cursor1, iHalBoMduXrfVUowObjHierPrcSeq, fetchCursor1Rm);
	}

	public DbAccessStatus closeCursor1() {
		return closeCursor(cursor1);
	}
}
