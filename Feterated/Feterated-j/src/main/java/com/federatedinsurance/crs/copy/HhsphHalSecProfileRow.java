/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalSecProfileV;

/**Original name: HHSPH-HAL-SEC-PROFILE-ROW<br>
 * Variable: HHSPH-HAL-SEC-PROFILE-ROW from copybook HALLHHSP<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class HhsphHalSecProfileRow implements IHalSecProfileV {

	//==== PROPERTIES ====
	//Original name: HHSPH-HSP-ID-KEY
	private String hspIdKey = DefaultValues.stringVal(Len.HSP_ID_KEY);
	//Original name: HHSPH-SEC-PROFILE-TYP
	private String secProfileTyp = DefaultValues.stringVal(Len.SEC_PROFILE_TYP);
	//Original name: HHSPH-SEQ-NBR
	private short seqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: HHSPH-HSP-GEN-TXT-FLD-1
	private String hspGenTxtFld1 = DefaultValues.stringVal(Len.HSP_GEN_TXT_FLD1);
	//Original name: HHSPH-HSP-GEN-TXT-FLD-2
	private String hspGenTxtFld2 = DefaultValues.stringVal(Len.HSP_GEN_TXT_FLD2);
	//Original name: HHSPH-HSP-GEN-TXT-FLD-3
	private String hspGenTxtFld3 = DefaultValues.stringVal(Len.HSP_GEN_TXT_FLD3);
	//Original name: HHSPH-HSP-GEN-TXT-FLD-4
	private String hspGenTxtFld4 = DefaultValues.stringVal(Len.HSP_GEN_TXT_FLD4);
	//Original name: HHSPH-HSP-GEN-TXT-FLD-5
	private String hspGenTxtFld5 = DefaultValues.stringVal(Len.HSP_GEN_TXT_FLD5);
	//Original name: HHSPH-EFFECTIVE-DT
	private String effectiveDt = DefaultValues.stringVal(Len.EFFECTIVE_DT);
	//Original name: HHSPH-EXPIRATION-DT
	private String expirationDt = DefaultValues.stringVal(Len.EXPIRATION_DT);
	//Original name: HHSPH-HSP-DTA-TXT
	private String hspDtaTxt = DefaultValues.stringVal(Len.HSP_DTA_TXT);

	//==== METHODS ====
	public void setHspIdKey(String hspIdKey) {
		this.hspIdKey = Functions.subString(hspIdKey, Len.HSP_ID_KEY);
	}

	public String getHspIdKey() {
		return this.hspIdKey;
	}

	public void setSecProfileTyp(String secProfileTyp) {
		this.secProfileTyp = Functions.subString(secProfileTyp, Len.SEC_PROFILE_TYP);
	}

	public String getSecProfileTyp() {
		return this.secProfileTyp;
	}

	public void setSeqNbr(short seqNbr) {
		this.seqNbr = seqNbr;
	}

	public short getSeqNbr() {
		return this.seqNbr;
	}

	public void setHspGenTxtFld1(String hspGenTxtFld1) {
		this.hspGenTxtFld1 = Functions.subString(hspGenTxtFld1, Len.HSP_GEN_TXT_FLD1);
	}

	public String getHspGenTxtFld1() {
		return this.hspGenTxtFld1;
	}

	public void setHspGenTxtFld2(String hspGenTxtFld2) {
		this.hspGenTxtFld2 = Functions.subString(hspGenTxtFld2, Len.HSP_GEN_TXT_FLD2);
	}

	public String getHspGenTxtFld2() {
		return this.hspGenTxtFld2;
	}

	public void setHspGenTxtFld3(String hspGenTxtFld3) {
		this.hspGenTxtFld3 = Functions.subString(hspGenTxtFld3, Len.HSP_GEN_TXT_FLD3);
	}

	public String getHspGenTxtFld3() {
		return this.hspGenTxtFld3;
	}

	public void setHspGenTxtFld4(String hspGenTxtFld4) {
		this.hspGenTxtFld4 = Functions.subString(hspGenTxtFld4, Len.HSP_GEN_TXT_FLD4);
	}

	public String getHspGenTxtFld4() {
		return this.hspGenTxtFld4;
	}

	public void setHspGenTxtFld5(String hspGenTxtFld5) {
		this.hspGenTxtFld5 = Functions.subString(hspGenTxtFld5, Len.HSP_GEN_TXT_FLD5);
	}

	public String getHspGenTxtFld5() {
		return this.hspGenTxtFld5;
	}

	public void setEffectiveDt(String effectiveDt) {
		this.effectiveDt = Functions.subString(effectiveDt, Len.EFFECTIVE_DT);
	}

	public String getEffectiveDt() {
		return this.effectiveDt;
	}

	public void setExpirationDt(String expirationDt) {
		this.expirationDt = Functions.subString(expirationDt, Len.EXPIRATION_DT);
	}

	public String getExpirationDt() {
		return this.expirationDt;
	}

	public void setHspDtaTxt(String hspDtaTxt) {
		this.hspDtaTxt = Functions.subString(hspDtaTxt, Len.HSP_DTA_TXT);
	}

	public String getHspDtaTxt() {
		return this.hspDtaTxt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HSP_ID_KEY = 32;
		public static final int SEC_PROFILE_TYP = 32;
		public static final int HSP_GEN_TXT_FLD1 = 32;
		public static final int HSP_GEN_TXT_FLD2 = 32;
		public static final int HSP_GEN_TXT_FLD3 = 16;
		public static final int HSP_GEN_TXT_FLD4 = 16;
		public static final int HSP_GEN_TXT_FLD5 = 8;
		public static final int EFFECTIVE_DT = 10;
		public static final int EXPIRATION_DT = 10;
		public static final int HSP_DTA_TXT = 200;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
