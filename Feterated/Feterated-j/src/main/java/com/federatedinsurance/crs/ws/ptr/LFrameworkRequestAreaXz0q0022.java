/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q0022<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q0022 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZY022_CNC_POL_LIST_MAXOCCURS = 150;
	public static final int XZY022_WARNINGS_MAXOCCURS = 10;
	public static final char XZY022_NLBE_OCC = 'Y';
	public static final char XZY022_NO_NLBE_OCC = 'N';
	public static final char XZY022_WARNINGS_OCC = 'Y';
	public static final char XZY022_NO_WARNINGS_OCC = 'N';

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q0022() {
	}

	public LFrameworkRequestAreaXz0q0022(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzy022CsrActNbr(String xzy022CsrActNbr) {
		writeString(Pos.XZY022_CSR_ACT_NBR, xzy022CsrActNbr, Len.XZY022_CSR_ACT_NBR);
	}

	/**Original name: XZY022-CSR-ACT-NBR<br>*/
	public String getXzy022CsrActNbr() {
		return readString(Pos.XZY022_CSR_ACT_NBR, Len.XZY022_CSR_ACT_NBR);
	}

	public void setXzy022qCncPolListBytes(int xzy022qCncPolListIdx, byte[] buffer) {
		setXzy022qCncPolListBytes(xzy022qCncPolListIdx, buffer, 1);
	}

	public void setXzy022qCncPolListBytes(int xzy022qCncPolListIdx, byte[] buffer, int offset) {
		int position = Pos.xzy022CncPolList(xzy022qCncPolListIdx - 1);
		setBytes(buffer, offset, Len.XZY022_CNC_POL_LIST, position);
	}

	public void setXzy022CpPolNbr(int xzy022CpPolNbrIdx, String xzy022CpPolNbr) {
		int position = Pos.xzy022CpPolNbr(xzy022CpPolNbrIdx - 1);
		writeString(position, xzy022CpPolNbr, Len.XZY022_CP_POL_NBR);
	}

	/**Original name: XZY022-CP-POL-NBR<br>*/
	public String getXzy022CpPolNbr(int xzy022CpPolNbrIdx) {
		int position = Pos.xzy022CpPolNbr(xzy022CpPolNbrIdx - 1);
		return readString(position, Len.XZY022_CP_POL_NBR);
	}

	public void setXzy022Userid(String xzy022Userid) {
		writeString(Pos.XZY022_USERID, xzy022Userid, Len.XZY022_USERID);
	}

	/**Original name: XZY022-USERID<br>*/
	public String getXzy022Userid() {
		return readString(Pos.XZY022_USERID, Len.XZY022_USERID);
	}

	public void setXzy022NlbeInd(char xzy022NlbeInd) {
		writeChar(Pos.XZY022_NLBE_IND, xzy022NlbeInd);
	}

	/**Original name: XZY022-NLBE-IND<br>*/
	public char getXzy022NlbeInd() {
		return readChar(Pos.XZY022_NLBE_IND);
	}

	public void setXzy022WarningInd(char xzy022WarningInd) {
		writeChar(Pos.XZY022_WARNING_IND, xzy022WarningInd);
	}

	/**Original name: XZY022-WARNING-IND<br>*/
	public char getXzy022WarningInd() {
		return readChar(Pos.XZY022_WARNING_IND);
	}

	public void setXzy022NonLogErrMsg(String xzy022NonLogErrMsg) {
		writeString(Pos.XZY022_NON_LOG_ERR_MSG, xzy022NonLogErrMsg, Len.XZY022_NON_LOG_ERR_MSG);
	}

	/**Original name: XZY022-NON-LOG-ERR-MSG<br>*/
	public String getXzy022NonLogErrMsg() {
		return readString(Pos.XZY022_NON_LOG_ERR_MSG, Len.XZY022_NON_LOG_ERR_MSG);
	}

	public void setXzy022WarnMsg(int xzy022WarnMsgIdx, String xzy022WarnMsg) {
		int position = Pos.xzy022WarnMsg(xzy022WarnMsgIdx - 1);
		writeString(position, xzy022WarnMsg, Len.XZY022_WARN_MSG);
	}

	/**Original name: XZY022-WARN-MSG<br>*/
	public String getXzy022WarnMsg(int xzy022WarnMsgIdx) {
		int position = Pos.xzy022WarnMsg(xzy022WarnMsgIdx - 1);
		return readString(position, Len.XZY022_WARN_MSG);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0Y0022 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZY022_SET_TMN_FLG_ROW = L_FW_REQ_XZ0Y0022;
		public static final int XZY022_CSR_ACT_NBR = XZY022_SET_TMN_FLG_ROW;
		public static final int XZY022_USERID = xzy022CpPolNbr(XZY022_CNC_POL_LIST_MAXOCCURS - 1) + Len.XZY022_CP_POL_NBR;
		public static final int XZY022_ERROR_FLAGS = XZY022_USERID + Len.XZY022_USERID;
		public static final int XZY022_NLBE_IND = XZY022_ERROR_FLAGS;
		public static final int XZY022_WARNING_IND = XZY022_NLBE_IND + Len.XZY022_NLBE_IND;
		public static final int XZY022_NON_LOG_ERR_MSG = XZY022_WARNING_IND + Len.XZY022_WARNING_IND;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzy022CncPolList(int idx) {
			return XZY022_CSR_ACT_NBR + Len.XZY022_CSR_ACT_NBR + idx * Len.XZY022_CNC_POL_LIST;
		}

		public static int xzy022CpPolNbr(int idx) {
			return xzy022CncPolList(idx);
		}

		public static int xzy022Warnings(int idx) {
			return XZY022_NON_LOG_ERR_MSG + Len.XZY022_NON_LOG_ERR_MSG + idx * Len.XZY022_WARNINGS;
		}

		public static int xzy022WarnMsg(int idx) {
			return xzy022Warnings(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY022_CSR_ACT_NBR = 9;
		public static final int XZY022_CP_POL_NBR = 25;
		public static final int XZY022_CNC_POL_LIST = XZY022_CP_POL_NBR;
		public static final int XZY022_USERID = 8;
		public static final int XZY022_NLBE_IND = 1;
		public static final int XZY022_WARNING_IND = 1;
		public static final int XZY022_NON_LOG_ERR_MSG = 500;
		public static final int XZY022_WARN_MSG = 400;
		public static final int XZY022_WARNINGS = XZY022_WARN_MSG;
		public static final int XZY022_ERROR_FLAGS = XZY022_NLBE_IND + XZY022_WARNING_IND;
		public static final int XZY022_SET_TMN_FLG_ROW = XZY022_CSR_ACT_NBR
				+ LFrameworkRequestAreaXz0q0022.XZY022_CNC_POL_LIST_MAXOCCURS * XZY022_CNC_POL_LIST + XZY022_USERID + XZY022_ERROR_FLAGS
				+ XZY022_NON_LOG_ERR_MSG + LFrameworkRequestAreaXz0q0022.XZY022_WARNINGS_MAXOCCURS * XZY022_WARNINGS;
		public static final int L_FW_REQ_XZ0Y0022 = XZY022_SET_TMN_FLG_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0Y0022;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
