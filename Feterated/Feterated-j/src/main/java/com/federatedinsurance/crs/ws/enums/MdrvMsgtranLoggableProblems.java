/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: MDRV-MSGTRAN-LOGGABLE-PROBLEMS<br>
 * Variable: MDRV-MSGTRAN-LOGGABLE-PROBLEMS from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvMsgtranLoggableProblems {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char PRE_UOW_LOG_ERRS = 'B';
	public static final char POST_UOW_LOG_ERRS = 'A';
	public static final char LOGGABLE_WARNS = 'W';
	public static final char OK = Types.SPACE_CHAR;

	//==== METHODS ====
	public void setMsgtranLoggableProblems(char msgtranLoggableProblems) {
		this.value = msgtranLoggableProblems;
	}

	public void setMsgtranLoggableProblemsFormatted(String msgtranLoggableProblems) {
		setMsgtranLoggableProblems(Functions.charAt(msgtranLoggableProblems, Types.CHAR_SIZE));
	}

	public char getMsgtranLoggableProblems() {
		return this.value;
	}

	public void setOk() {
		setMsgtranLoggableProblemsFormatted(String.valueOf(OK));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MSGTRAN_LOGGABLE_PROBLEMS = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
