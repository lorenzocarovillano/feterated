/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-81-CICS-PROGRAM-TIMEOUT-MSG<br>
 * Variable: EA-81-CICS-PROGRAM-TIMEOUT-MSG from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea81CicsProgramTimeoutMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-81-CICS-PROGRAM-TIMEOUT-MSG
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-81-CICS-PROGRAM-TIMEOUT-MSG-1
	private String flr2 = "CICS PROGRAM";
	//Original name: EA-81-TARGET-CICS-PGM
	private String ea81TargetCicsPgm = DefaultValues.stringVal(Len.EA81_TARGET_CICS_PGM);
	//Original name: FILLER-EA-81-CICS-PROGRAM-TIMEOUT-MSG-2
	private String flr3 = " TIMEOUT";
	//Original name: FILLER-EA-81-CICS-PROGRAM-TIMEOUT-MSG-3
	private String flr4 = "OCCURRED.";

	//==== METHODS ====
	public String getEa81CicsProgramTimeoutMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa81CicsProgramTimeoutMsgBytes());
	}

	public byte[] getEa81CicsProgramTimeoutMsgBytes() {
		byte[] buffer = new byte[Len.EA81_CICS_PROGRAM_TIMEOUT_MSG];
		return getEa81CicsProgramTimeoutMsgBytes(buffer, 1);
	}

	public byte[] getEa81CicsProgramTimeoutMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, ea81TargetCicsPgm, Len.EA81_TARGET_CICS_PGM);
		position += Len.EA81_TARGET_CICS_PGM;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setEa81TargetCicsPgm(String ea81TargetCicsPgm) {
		this.ea81TargetCicsPgm = Functions.subString(ea81TargetCicsPgm, Len.EA81_TARGET_CICS_PGM);
	}

	public String getEa81TargetCicsPgm() {
		return this.ea81TargetCicsPgm;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA81_TARGET_CICS_PGM = 8;
		public static final int FLR1 = 11;
		public static final int FLR2 = 13;
		public static final int FLR3 = 9;
		public static final int EA81_CICS_PROGRAM_TIMEOUT_MSG = EA81_TARGET_CICS_PGM + FLR1 + FLR2 + 2 * FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
