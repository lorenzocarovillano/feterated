/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC002-ACT-NOT-POL-DATA<br>
 * Variable: XZC002-ACT-NOT-POL-DATA from copybook XZ0C0002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzc002ActNotPolData {

	//==== PROPERTIES ====
	/**Original name: XZC002-POL-TYP-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char polTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: XZC002-POL-PRI-RSK-ST-ABB-CI
	private char polPriRskStAbbCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-PRI-RSK-ST-ABB
	private String polPriRskStAbb = DefaultValues.stringVal(Len.POL_PRI_RSK_ST_ABB);
	//Original name: XZC002-NOT-EFF-DT-CI
	private char notEffDtCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-NOT-EFF-DT-NI
	private char notEffDtNi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);
	//Original name: XZC002-POL-EFF-DT-CI
	private char polEffDtCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZC002-POL-EXP-DT-CI
	private char polExpDtCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZC002-POL-DUE-AMT-CI
	private char polDueAmtCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-DUE-AMT-NI
	private char polDueAmtNi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-DUE-AMT-SIGN
	private char polDueAmtSign = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-DUE-AMT
	private AfDecimal polDueAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: XZC002-NIN-CLT-ID-CI
	private char ninCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-NIN-CLT-ID
	private String ninCltId = DefaultValues.stringVal(Len.NIN_CLT_ID);
	//Original name: XZC002-NIN-ADR-ID-CI
	private char ninAdrIdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-NIN-ADR-ID
	private String ninAdrId = DefaultValues.stringVal(Len.NIN_ADR_ID);
	//Original name: XZC002-WF-STARTED-IND-CI
	private char wfStartedIndCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-WF-STARTED-IND-NI
	private char wfStartedIndNi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-WF-STARTED-IND
	private char wfStartedInd = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-BIL-STA-CD-CI
	private char polBilStaCdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-BIL-STA-CD-NI
	private char polBilStaCdNi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-BIL-STA-CD
	private char polBilStaCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setActNotPolDataBytes(byte[] buffer, int offset) {
		int position = offset;
		polTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polTypCd = MarshalByte.readString(buffer, position, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		polPriRskStAbbCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polPriRskStAbb = MarshalByte.readString(buffer, position, Len.POL_PRI_RSK_ST_ABB);
		position += Len.POL_PRI_RSK_ST_ABB;
		notEffDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notEffDtNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notEffDt = MarshalByte.readString(buffer, position, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		polEffDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		polExpDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polExpDt = MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		polDueAmtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polDueAmtNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polDueAmtSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polDueAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.POL_DUE_AMT, Len.Fract.POL_DUE_AMT, SignType.NO_SIGN));
		position += Len.POL_DUE_AMT;
		ninCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ninCltId = MarshalByte.readString(buffer, position, Len.NIN_CLT_ID);
		position += Len.NIN_CLT_ID;
		ninAdrIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ninAdrId = MarshalByte.readString(buffer, position, Len.NIN_ADR_ID);
		position += Len.NIN_ADR_ID;
		wfStartedIndCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		wfStartedIndNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		wfStartedInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polBilStaCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polBilStaCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polBilStaCd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotPolDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, polTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, polTypCd, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		MarshalByte.writeChar(buffer, position, polPriRskStAbbCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
		position += Len.POL_PRI_RSK_ST_ABB;
		MarshalByte.writeChar(buffer, position, notEffDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notEffDtNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, notEffDt, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		MarshalByte.writeChar(buffer, position, polEffDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeChar(buffer, position, polExpDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeChar(buffer, position, polDueAmtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polDueAmtNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polDueAmtSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeDecimal(buffer, position, polDueAmt.copy(), SignType.NO_SIGN);
		position += Len.POL_DUE_AMT;
		MarshalByte.writeChar(buffer, position, ninCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ninCltId, Len.NIN_CLT_ID);
		position += Len.NIN_CLT_ID;
		MarshalByte.writeChar(buffer, position, ninAdrIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ninAdrId, Len.NIN_ADR_ID);
		position += Len.NIN_ADR_ID;
		MarshalByte.writeChar(buffer, position, wfStartedIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, wfStartedIndNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, wfStartedInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polBilStaCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polBilStaCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polBilStaCd);
		return buffer;
	}

	public void initActNotPolDataSpaces() {
		polTypCdCi = Types.SPACE_CHAR;
		polTypCd = "";
		polPriRskStAbbCi = Types.SPACE_CHAR;
		polPriRskStAbb = "";
		notEffDtCi = Types.SPACE_CHAR;
		notEffDtNi = Types.SPACE_CHAR;
		notEffDt = "";
		polEffDtCi = Types.SPACE_CHAR;
		polEffDt = "";
		polExpDtCi = Types.SPACE_CHAR;
		polExpDt = "";
		polDueAmtCi = Types.SPACE_CHAR;
		polDueAmtNi = Types.SPACE_CHAR;
		polDueAmtSign = Types.SPACE_CHAR;
		polDueAmt.setNaN();
		ninCltIdCi = Types.SPACE_CHAR;
		ninCltId = "";
		ninAdrIdCi = Types.SPACE_CHAR;
		ninAdrId = "";
		wfStartedIndCi = Types.SPACE_CHAR;
		wfStartedIndNi = Types.SPACE_CHAR;
		wfStartedInd = Types.SPACE_CHAR;
		polBilStaCdCi = Types.SPACE_CHAR;
		polBilStaCdNi = Types.SPACE_CHAR;
		polBilStaCd = Types.SPACE_CHAR;
	}

	public void setPolTypCdCi(char polTypCdCi) {
		this.polTypCdCi = polTypCdCi;
	}

	public char getPolTypCdCi() {
		return this.polTypCdCi;
	}

	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	public String getPolTypCd() {
		return this.polTypCd;
	}

	public String getXzc002PolTypCdFormatted() {
		return Functions.padBlanks(getPolTypCd(), Len.POL_TYP_CD);
	}

	public void setPolPriRskStAbbCi(char polPriRskStAbbCi) {
		this.polPriRskStAbbCi = polPriRskStAbbCi;
	}

	public char getPolPriRskStAbbCi() {
		return this.polPriRskStAbbCi;
	}

	public void setPolPriRskStAbb(String polPriRskStAbb) {
		this.polPriRskStAbb = Functions.subString(polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
	}

	public String getPolPriRskStAbb() {
		return this.polPriRskStAbb;
	}

	public String getXzc002PolPriRskStAbbFormatted() {
		return Functions.padBlanks(getPolPriRskStAbb(), Len.POL_PRI_RSK_ST_ABB);
	}

	public void setNotEffDtCi(char notEffDtCi) {
		this.notEffDtCi = notEffDtCi;
	}

	public char getNotEffDtCi() {
		return this.notEffDtCi;
	}

	public void setNotEffDtNi(char notEffDtNi) {
		this.notEffDtNi = notEffDtNi;
	}

	public void setXzc002NotEffDtNiFormatted(String xzc002NotEffDtNi) {
		setNotEffDtNi(Functions.charAt(xzc002NotEffDtNi, Types.CHAR_SIZE));
	}

	public char getNotEffDtNi() {
		return this.notEffDtNi;
	}

	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	public String getNotEffDt() {
		return this.notEffDt;
	}

	public String getXzc002NotEffDtFormatted() {
		return Functions.padBlanks(getNotEffDt(), Len.NOT_EFF_DT);
	}

	public void setPolEffDtCi(char polEffDtCi) {
		this.polEffDtCi = polEffDtCi;
	}

	public char getPolEffDtCi() {
		return this.polEffDtCi;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public String getXzc002PolEffDtFormatted() {
		return Functions.padBlanks(getPolEffDt(), Len.POL_EFF_DT);
	}

	public void setPolExpDtCi(char polExpDtCi) {
		this.polExpDtCi = polExpDtCi;
	}

	public char getPolExpDtCi() {
		return this.polExpDtCi;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public String getXzc002PolExpDtFormatted() {
		return Functions.padBlanks(getPolExpDt(), Len.POL_EXP_DT);
	}

	public void setPolDueAmtCi(char polDueAmtCi) {
		this.polDueAmtCi = polDueAmtCi;
	}

	public char getPolDueAmtCi() {
		return this.polDueAmtCi;
	}

	public void setPolDueAmtNi(char polDueAmtNi) {
		this.polDueAmtNi = polDueAmtNi;
	}

	public void setXzc002PolDueAmtNiFormatted(String xzc002PolDueAmtNi) {
		setPolDueAmtNi(Functions.charAt(xzc002PolDueAmtNi, Types.CHAR_SIZE));
	}

	public char getPolDueAmtNi() {
		return this.polDueAmtNi;
	}

	public void setPolDueAmtSign(char polDueAmtSign) {
		this.polDueAmtSign = polDueAmtSign;
	}

	public void setXzc002PolDueAmtSignFormatted(String xzc002PolDueAmtSign) {
		setPolDueAmtSign(Functions.charAt(xzc002PolDueAmtSign, Types.CHAR_SIZE));
	}

	public char getPolDueAmtSign() {
		return this.polDueAmtSign;
	}

	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.polDueAmt.assign(polDueAmt);
	}

	public AfDecimal getPolDueAmt() {
		return this.polDueAmt.copy();
	}

	public void setNinCltIdCi(char ninCltIdCi) {
		this.ninCltIdCi = ninCltIdCi;
	}

	public char getNinCltIdCi() {
		return this.ninCltIdCi;
	}

	public void setNinCltId(String ninCltId) {
		this.ninCltId = Functions.subString(ninCltId, Len.NIN_CLT_ID);
	}

	public String getNinCltId() {
		return this.ninCltId;
	}

	public void setNinAdrIdCi(char ninAdrIdCi) {
		this.ninAdrIdCi = ninAdrIdCi;
	}

	public char getNinAdrIdCi() {
		return this.ninAdrIdCi;
	}

	public void setNinAdrId(String ninAdrId) {
		this.ninAdrId = Functions.subString(ninAdrId, Len.NIN_ADR_ID);
	}

	public String getNinAdrId() {
		return this.ninAdrId;
	}

	public void setWfStartedIndCi(char wfStartedIndCi) {
		this.wfStartedIndCi = wfStartedIndCi;
	}

	public char getWfStartedIndCi() {
		return this.wfStartedIndCi;
	}

	public void setWfStartedIndNi(char wfStartedIndNi) {
		this.wfStartedIndNi = wfStartedIndNi;
	}

	public void setXzc002WfStartedIndNiFormatted(String xzc002WfStartedIndNi) {
		setWfStartedIndNi(Functions.charAt(xzc002WfStartedIndNi, Types.CHAR_SIZE));
	}

	public char getWfStartedIndNi() {
		return this.wfStartedIndNi;
	}

	public void setWfStartedInd(char wfStartedInd) {
		this.wfStartedInd = wfStartedInd;
	}

	public char getWfStartedInd() {
		return this.wfStartedInd;
	}

	public void setPolBilStaCdCi(char polBilStaCdCi) {
		this.polBilStaCdCi = polBilStaCdCi;
	}

	public char getPolBilStaCdCi() {
		return this.polBilStaCdCi;
	}

	public void setPolBilStaCdNi(char polBilStaCdNi) {
		this.polBilStaCdNi = polBilStaCdNi;
	}

	public void setXzc002PolBilStaCdNiFormatted(String xzc002PolBilStaCdNi) {
		setPolBilStaCdNi(Functions.charAt(xzc002PolBilStaCdNi, Types.CHAR_SIZE));
	}

	public char getPolBilStaCdNi() {
		return this.polBilStaCdNi;
	}

	public void setPolBilStaCd(char polBilStaCd) {
		this.polBilStaCd = polBilStaCd;
	}

	public char getPolBilStaCd() {
		return this.polBilStaCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_TYP_CD_CI = 1;
		public static final int POL_TYP_CD = 3;
		public static final int POL_PRI_RSK_ST_ABB_CI = 1;
		public static final int POL_PRI_RSK_ST_ABB = 2;
		public static final int NOT_EFF_DT_CI = 1;
		public static final int NOT_EFF_DT_NI = 1;
		public static final int NOT_EFF_DT = 10;
		public static final int POL_EFF_DT_CI = 1;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT_CI = 1;
		public static final int POL_EXP_DT = 10;
		public static final int POL_DUE_AMT_CI = 1;
		public static final int POL_DUE_AMT_NI = 1;
		public static final int POL_DUE_AMT_SIGN = 1;
		public static final int POL_DUE_AMT = 10;
		public static final int NIN_CLT_ID_CI = 1;
		public static final int NIN_CLT_ID = 64;
		public static final int NIN_ADR_ID_CI = 1;
		public static final int NIN_ADR_ID = 64;
		public static final int WF_STARTED_IND_CI = 1;
		public static final int WF_STARTED_IND_NI = 1;
		public static final int WF_STARTED_IND = 1;
		public static final int POL_BIL_STA_CD_CI = 1;
		public static final int POL_BIL_STA_CD_NI = 1;
		public static final int POL_BIL_STA_CD = 1;
		public static final int ACT_NOT_POL_DATA = POL_TYP_CD_CI + POL_TYP_CD + POL_PRI_RSK_ST_ABB_CI + POL_PRI_RSK_ST_ABB + NOT_EFF_DT_CI
				+ NOT_EFF_DT_NI + NOT_EFF_DT + POL_EFF_DT_CI + POL_EFF_DT + POL_EXP_DT_CI + POL_EXP_DT + POL_DUE_AMT_CI + POL_DUE_AMT_NI
				+ POL_DUE_AMT_SIGN + POL_DUE_AMT + NIN_CLT_ID_CI + NIN_CLT_ID + NIN_ADR_ID_CI + NIN_ADR_ID + WF_STARTED_IND_CI + WF_STARTED_IND_NI
				+ WF_STARTED_IND + POL_BIL_STA_CD_CI + POL_BIL_STA_CD_NI + POL_BIL_STA_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
