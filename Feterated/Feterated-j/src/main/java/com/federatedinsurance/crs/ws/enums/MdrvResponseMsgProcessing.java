/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;

/**Original name: MDRV-RESPONSE-MSG-PROCESSING<br>
 * Variable: MDRV-RESPONSE-MSG-PROCESSING from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvResponseMsgProcessing {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CHUNK_RESP = Types.SPACE_CHAR;
	public static final char RESP_IN_MRSM_UMT = 'M';
	public static final char RESP_IN_UOW_UMTS = 'U';

	//==== METHODS ====
	public void setResponseMsgProcessing(char responseMsgProcessing) {
		this.value = responseMsgProcessing;
	}

	public char getResponseMsgProcessing() {
		return this.value;
	}

	public void setRespInMrsmUmt() {
		value = RESP_IN_MRSM_UMT;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESPONSE_MSG_PROCESSING = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
