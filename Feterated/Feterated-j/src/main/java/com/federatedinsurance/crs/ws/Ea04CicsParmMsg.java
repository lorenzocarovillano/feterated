/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-04-CICS-PARM-MSG<br>
 * Variable: EA-04-CICS-PARM-MSG from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea04CicsParmMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-04-CICS-PARM-MSG
	private String flr1 = "XZ001000 -";
	//Original name: FILLER-EA-04-CICS-PARM-MSG-1
	private String flr2 = "CICS TO USE";
	//Original name: FILLER-EA-04-CICS-PARM-MSG-2
	private String flr3 = "PARAMETER:";
	//Original name: EA-04-CICS-PARM
	private String ea04CicsParm = DefaultValues.stringVal(Len.EA04_CICS_PARM);

	//==== METHODS ====
	public String getEa04CicsParmMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa04CicsParmMsgBytes());
	}

	public byte[] getEa04CicsParmMsgBytes() {
		byte[] buffer = new byte[Len.EA04_CICS_PARM_MSG];
		return getEa04CicsParmMsgBytes(buffer, 1);
	}

	public byte[] getEa04CicsParmMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, ea04CicsParm, Len.EA04_CICS_PARM);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEa04CicsParm(String ea04CicsParm) {
		this.ea04CicsParm = Functions.subString(ea04CicsParm, Len.EA04_CICS_PARM);
	}

	public String getEa04CicsParm() {
		return this.ea04CicsParm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA04_CICS_PARM = 8;
		public static final int FLR1 = 11;
		public static final int FLR2 = 12;
		public static final int EA04_CICS_PARM_MSG = EA04_CICS_PARM + 2 * FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
