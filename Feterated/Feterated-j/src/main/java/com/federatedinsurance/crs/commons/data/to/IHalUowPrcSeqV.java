/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HAL_UOW_PRC_SEQ_V]
 * 
 */
public interface IHalUowPrcSeqV extends BaseSqlTo {

	/**
	 * Host Variable ROOT-BOBJ-NM
	 * 
	 */
	String getRootBobjNm();

	void setRootBobjNm(String rootBobjNm);

	/**
	 * Host Variable UOW-SEQ-NBR
	 * 
	 */
	short getUowSeqNbr();

	void setUowSeqNbr(short uowSeqNbr);

	/**
	 * Host Variable HUPS-UOW-NM
	 * 
	 */
	String getHupsUowNm();

	void setHupsUowNm(String hupsUowNm);

	/**
	 * Host Variable HUPS-BRN-PRC-CD
	 * 
	 */
	char getHupsBrnPrcCd();

	void setHupsBrnPrcCd(char hupsBrnPrcCd);
};
