/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HUFSC-HAL-UOW-FUN-SEC-KEY<br>
 * Variable: HUFSC-HAL-UOW-FUN-SEC-KEY from copybook HALLCUFS<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class HufscHalUowFunSecKey {

	//==== PROPERTIES ====
	//Original name: HUFSC-UOW-NM-CI
	private char uowNmCi = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HUFSC-SEC-GRP-NM-CI
	private char secGrpNmCi = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-SEC-GRP-NM
	private String secGrpNm = DefaultValues.stringVal(Len.SEC_GRP_NM);
	//Original name: HUFSC-CONTEXT-CI
	private char contextCi = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-CONTEXT
	private String context = DefaultValues.stringVal(Len.CONTEXT);
	//Original name: HUFSC-SEC-ASC-TYP-CI
	private char secAscTypCi = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-SEC-ASC-TYP
	private String secAscTyp = DefaultValues.stringVal(Len.SEC_ASC_TYP);
	//Original name: HUFSC-FUN-SEQ-NBR-CI
	private char funSeqNbrCi = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-FUN-SEQ-NBR-SIGN
	private char funSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-FUN-SEQ-NBR
	private String funSeqNbr = DefaultValues.stringVal(Len.FUN_SEQ_NBR);

	//==== METHODS ====
	public byte[] getHalUowFunSecKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, uowNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, uowNm, Len.UOW_NM);
		position += Len.UOW_NM;
		MarshalByte.writeChar(buffer, position, secGrpNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, secGrpNm, Len.SEC_GRP_NM);
		position += Len.SEC_GRP_NM;
		MarshalByte.writeChar(buffer, position, contextCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, context, Len.CONTEXT);
		position += Len.CONTEXT;
		MarshalByte.writeChar(buffer, position, secAscTypCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, secAscTyp, Len.SEC_ASC_TYP);
		position += Len.SEC_ASC_TYP;
		MarshalByte.writeChar(buffer, position, funSeqNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, funSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, funSeqNbr, Len.FUN_SEQ_NBR);
		return buffer;
	}

	public void initHalUowFunSecKeySpaces() {
		uowNmCi = Types.SPACE_CHAR;
		uowNm = "";
		secGrpNmCi = Types.SPACE_CHAR;
		secGrpNm = "";
		contextCi = Types.SPACE_CHAR;
		context = "";
		secAscTypCi = Types.SPACE_CHAR;
		secAscTyp = "";
		funSeqNbrCi = Types.SPACE_CHAR;
		funSeqNbrSign = Types.SPACE_CHAR;
		funSeqNbr = "";
	}

	public char getUowNmCi() {
		return this.uowNmCi;
	}

	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public char getSecGrpNmCi() {
		return this.secGrpNmCi;
	}

	public void setSecGrpNm(String secGrpNm) {
		this.secGrpNm = Functions.subString(secGrpNm, Len.SEC_GRP_NM);
	}

	public String getSecGrpNm() {
		return this.secGrpNm;
	}

	public char getContextCi() {
		return this.contextCi;
	}

	public void setContext(String context) {
		this.context = Functions.subString(context, Len.CONTEXT);
	}

	public String getContext() {
		return this.context;
	}

	public char getSecAscTypCi() {
		return this.secAscTypCi;
	}

	public void setSecAscTyp(String secAscTyp) {
		this.secAscTyp = Functions.subString(secAscTyp, Len.SEC_ASC_TYP);
	}

	public String getSecAscTyp() {
		return this.secAscTyp;
	}

	public char getFunSeqNbrCi() {
		return this.funSeqNbrCi;
	}

	public char getFunSeqNbrSign() {
		return this.funSeqNbrSign;
	}

	public void setFunSeqNbr(int funSeqNbr) {
		this.funSeqNbr = NumericDisplay.asString(funSeqNbr, Len.FUN_SEQ_NBR);
	}

	public int getFunSeqNbr() {
		return NumericDisplay.asInt(this.funSeqNbr);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NM = 32;
		public static final int SEC_GRP_NM = 8;
		public static final int CONTEXT = 20;
		public static final int SEC_ASC_TYP = 8;
		public static final int FUN_SEQ_NBR = 5;
		public static final int UOW_NM_CI = 1;
		public static final int SEC_GRP_NM_CI = 1;
		public static final int CONTEXT_CI = 1;
		public static final int SEC_ASC_TYP_CI = 1;
		public static final int FUN_SEQ_NBR_CI = 1;
		public static final int FUN_SEQ_NBR_SIGN = 1;
		public static final int HAL_UOW_FUN_SEC_KEY = UOW_NM_CI + UOW_NM + SEC_GRP_NM_CI + SEC_GRP_NM + CONTEXT_CI + CONTEXT + SEC_ASC_TYP_CI
				+ SEC_ASC_TYP + FUN_SEQ_NBR_CI + FUN_SEQ_NBR_SIGN + FUN_SEQ_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
