/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-17-DLT-ACT-NOT-INPUT-PARMS<br>
 * Variable: EA-17-DLT-ACT-NOT-INPUT-PARMS from program XZ003000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea17DltActNotInputParms {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-17-DLT-ACT-NOT-INPUT-PARMS
	private String flr1 = "DeleteAccount";
	//Original name: FILLER-EA-17-DLT-ACT-NOT-INPUT-PARMS-1
	private String flr2 = "Notification";
	//Original name: FILLER-EA-17-DLT-ACT-NOT-INPUT-PARMS-2
	private String flr3 = "SVC FAILED:";
	//Original name: FILLER-EA-17-DLT-ACT-NOT-INPUT-PARMS-3
	private String flr4 = "PARMS:";
	//Original name: FILLER-EA-17-DLT-ACT-NOT-INPUT-PARMS-4
	private String flr5 = "ACT NBR =";
	//Original name: EA-17-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-17-DLT-ACT-NOT-INPUT-PARMS-5
	private String flr6 = " TS =";
	//Original name: EA-17-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-17-DLT-ACT-NOT-INPUT-PARMS-6
	private String flr7 = " CSUM =";
	//Original name: EA-17-ACT-NOT-CSUM
	private String actNotCsum = DefaultValues.stringVal(Len.ACT_NOT_CSUM);
	//Original name: FILLER-EA-17-DLT-ACT-NOT-INPUT-PARMS-7
	private String flr8 = " USERID =";
	//Original name: EA-17-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== METHODS ====
	public String getEa17DltActNotInputParmsFormatted() {
		return MarshalByteExt.bufferToStr(getEa17DltActNotInputParmsBytes());
	}

	public byte[] getEa17DltActNotInputParmsBytes() {
		byte[] buffer = new byte[Len.EA17_DLT_ACT_NOT_INPUT_PARMS];
		return getEa17DltActNotInputParmsBytes(buffer, 1);
	}

	public byte[] getEa17DltActNotInputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, actNotCsum, Len.ACT_NOT_CSUM);
		position += Len.ACT_NOT_CSUM;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setActNotCsumFormatted(String actNotCsum) {
		this.actNotCsum = Trunc.toUnsignedNumeric(actNotCsum, Len.ACT_NOT_CSUM);
	}

	public int getActNotCsum() {
		return NumericDisplay.asInt(this.actNotCsum);
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int ACT_NOT_CSUM = 9;
		public static final int USERID = 8;
		public static final int FLR1 = 13;
		public static final int FLR3 = 12;
		public static final int FLR4 = 7;
		public static final int FLR5 = 10;
		public static final int FLR6 = 6;
		public static final int FLR7 = 8;
		public static final int EA17_DLT_ACT_NOT_INPUT_PARMS = CSR_ACT_NBR + NOT_PRC_TS + ACT_NOT_CSUM + USERID + 2 * FLR1 + FLR3 + FLR4 + 2 * FLR5
				+ FLR6 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
