/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalLokObjDetV;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: DCLHAL-LOK-OBJ-DET<br>
 * Variable: DCLHAL-LOK-OBJ-DET from copybook HALLGLOD<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalLokObjDet implements IHalLokObjDetV {

	//==== PROPERTIES ====
	//Original name: HLOD-SESSION-ID
	private String sessionId = DefaultValues.stringVal(Len.SESSION_ID);
	//Original name: HLOD-TCH-KEY
	private String tchKey = DefaultValues.stringVal(Len.TCH_KEY);
	//Original name: HLOD-APP-ID
	private String appId = DefaultValues.stringVal(Len.APP_ID);
	//Original name: HLOD-TABLE-NM
	private String tableNm = DefaultValues.stringVal(Len.TABLE_NM);
	//Original name: HLOD-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);
	//Original name: HLOD-LOK-TYPE-CD
	private char lokTypeCd = DefaultValues.CHAR_VAL;
	//Original name: HLOD-LAST-ACY-TS
	private String lastAcyTs = DefaultValues.stringVal(Len.LAST_ACY_TS);
	//Original name: HLOD-LOK-TMO-TS
	private String lokTmoTs = DefaultValues.stringVal(Len.LOK_TMO_TS);
	//Original name: HLOD-UPD-BY-MSG-ID
	private String updByMsgId = DefaultValues.stringVal(Len.UPD_BY_MSG_ID);
	//Original name: HLOD-ZAPPED-IND
	private char zappedInd = DefaultValues.CHAR_VAL;
	//Original name: HLOD-ZAPPED-BY
	private String zappedBy = DefaultValues.stringVal(Len.ZAPPED_BY);
	//Original name: HLOD-ZAPPED-TS
	private String zappedTs = DefaultValues.stringVal(Len.ZAPPED_TS);

	//==== METHODS ====
	public String getDclhalLokObjDetFormatted() {
		return MarshalByteExt.bufferToStr(getDclhalLokObjDetBytes());
	}

	public byte[] getDclhalLokObjDetBytes() {
		byte[] buffer = new byte[Len.DCLHAL_LOK_OBJ_DET];
		return getDclhalLokObjDetBytes(buffer, 1);
	}

	public byte[] getDclhalLokObjDetBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
		position += Len.SESSION_ID;
		MarshalByte.writeString(buffer, position, tchKey, Len.TCH_KEY);
		position += Len.TCH_KEY;
		MarshalByte.writeString(buffer, position, appId, Len.APP_ID);
		position += Len.APP_ID;
		MarshalByte.writeString(buffer, position, tableNm, Len.TABLE_NM);
		position += Len.TABLE_NM;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position += Len.USERID;
		MarshalByte.writeChar(buffer, position, lokTypeCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, lastAcyTs, Len.LAST_ACY_TS);
		position += Len.LAST_ACY_TS;
		MarshalByte.writeString(buffer, position, lokTmoTs, Len.LOK_TMO_TS);
		position += Len.LOK_TMO_TS;
		MarshalByte.writeString(buffer, position, updByMsgId, Len.UPD_BY_MSG_ID);
		position += Len.UPD_BY_MSG_ID;
		MarshalByte.writeChar(buffer, position, zappedInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, zappedBy, Len.ZAPPED_BY);
		position += Len.ZAPPED_BY;
		MarshalByte.writeString(buffer, position, zappedTs, Len.ZAPPED_TS);
		return buffer;
	}

	@Override
	public void setSessionId(String sessionId) {
		this.sessionId = Functions.subString(sessionId, Len.SESSION_ID);
	}

	@Override
	public String getSessionId() {
		return this.sessionId;
	}

	@Override
	public void setTchKey(String tchKey) {
		this.tchKey = Functions.subString(tchKey, Len.TCH_KEY);
	}

	@Override
	public String getTchKey() {
		return this.tchKey;
	}

	@Override
	public void setAppId(String appId) {
		this.appId = Functions.subString(appId, Len.APP_ID);
	}

	@Override
	public String getAppId() {
		return this.appId;
	}

	@Override
	public void setTableNm(String tableNm) {
		this.tableNm = Functions.subString(tableNm, Len.TABLE_NM);
	}

	@Override
	public String getTableNm() {
		return this.tableNm;
	}

	@Override
	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	@Override
	public String getUserid() {
		return this.userid;
	}

	@Override
	public void setLokTypeCd(char lokTypeCd) {
		this.lokTypeCd = lokTypeCd;
	}

	@Override
	public char getLokTypeCd() {
		return this.lokTypeCd;
	}

	public String getLastAcyTs() {
		return this.lastAcyTs;
	}

	public String getLokTmoTs() {
		return this.lokTmoTs;
	}

	@Override
	public void setUpdByMsgId(String updByMsgId) {
		this.updByMsgId = Functions.subString(updByMsgId, Len.UPD_BY_MSG_ID);
	}

	@Override
	public String getUpdByMsgId() {
		return this.updByMsgId;
	}

	@Override
	public void setZappedInd(char zappedInd) {
		this.zappedInd = zappedInd;
	}

	public void setZappedIndFormatted(String zappedInd) {
		setZappedInd(Functions.charAt(zappedInd, Types.CHAR_SIZE));
	}

	@Override
	public char getZappedInd() {
		return this.zappedInd;
	}

	@Override
	public void setZappedBy(String zappedBy) {
		this.zappedBy = Functions.subString(zappedBy, Len.ZAPPED_BY);
	}

	@Override
	public String getZappedBy() {
		return this.zappedBy;
	}

	@Override
	public void setZappedTs(String zappedTs) {
		this.zappedTs = Functions.subString(zappedTs, Len.ZAPPED_TS);
	}

	@Override
	public String getZappedTs() {
		return this.zappedTs;
	}

	@Override
	public String getUserId() {
		throw new FieldNotMappedException("userId");
	}

	@Override
	public void setUserId(String userId) {
		throw new FieldNotMappedException("userId");
	}

	@Override
	public short getWsTmoValue() {
		throw new FieldNotMappedException("wsTmoValue");
	}

	@Override
	public void setWsTmoValue(short wsTmoValue) {
		throw new FieldNotMappedException("wsTmoValue");
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SESSION_ID = 32;
		public static final int TCH_KEY = 126;
		public static final int APP_ID = 10;
		public static final int TABLE_NM = 18;
		public static final int USERID = 32;
		public static final int UPD_BY_MSG_ID = 32;
		public static final int ZAPPED_BY = 32;
		public static final int ZAPPED_TS = 26;
		public static final int LAST_ACY_TS = 26;
		public static final int LOK_TMO_TS = 26;
		public static final int LOK_TYPE_CD = 1;
		public static final int ZAPPED_IND = 1;
		public static final int DCLHAL_LOK_OBJ_DET = SESSION_ID + TCH_KEY + APP_ID + TABLE_NM + USERID + LOK_TYPE_CD + LAST_ACY_TS + LOK_TMO_TS
				+ UPD_BY_MSG_ID + ZAPPED_IND + ZAPPED_BY + ZAPPED_TS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
