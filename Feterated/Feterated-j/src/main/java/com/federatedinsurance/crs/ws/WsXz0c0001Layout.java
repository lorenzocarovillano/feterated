/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Xzc001ActNotData;
import com.federatedinsurance.crs.copy.Xzc001ActNotFixed;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0C0001-LAYOUT<br>
 * Variable: WS-XZ0C0001-LAYOUT from program XZ0F0001<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0c0001Layout extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZC001-ACT-NOT-FIXED
	private Xzc001ActNotFixed actNotFixed = new Xzc001ActNotFixed();
	//Original name: XZC001-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC001-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC001-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC001-CSR-ACT-NBR-CI
	private char csrActNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-NOT-PRC-TS-CI
	private char notPrcTsCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ACT-NOT-DATA
	private Xzc001ActNotData actNotData = new Xzc001ActNotData();
	//Original name: XZC001-ACT-NOT-TYP-DESC
	private String actNotTypDesc = DefaultValues.stringVal(Len.ACT_NOT_TYP_DESC);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0C0001_LAYOUT;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0c0001LayoutBytes(buf);
	}

	public void setWsXz0c0001LayoutFormatted(String data) {
		byte[] buffer = new byte[Len.WS_XZ0C0001_LAYOUT];
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0C0001_LAYOUT);
		setWsXz0c0001LayoutBytes(buffer, 1);
	}

	public String getWsXz0c0001LayoutFormatted() {
		return getActNotRowFormatted();
	}

	public void setWsXz0c0001LayoutBytes(byte[] buffer) {
		setWsXz0c0001LayoutBytes(buffer, 1);
	}

	public byte[] getWsXz0c0001LayoutBytes() {
		byte[] buffer = new byte[Len.WS_XZ0C0001_LAYOUT];
		return getWsXz0c0001LayoutBytes(buffer, 1);
	}

	public void setWsXz0c0001LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotRowBytes(buffer, position);
	}

	public byte[] getWsXz0c0001LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotRowBytes(buffer, position);
		return buffer;
	}

	public String getActNotRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotRowBytes());
	}

	/**Original name: XZC001-ACT-NOT-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0C0001 - ACT_NOT TABLE                                       *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 29 Sep 2008 E404GRK   GENERATED                        *
	 *  TO07614 03 Feb 2009 E404DLP   ADDED ADD-CNC-DAY FIELD          *
	 *  TO07614 09 Mar 2009 E404DLP   CHANGED ADR-SEQNBR TO ADR-ID     *
	 *  TO07614 16 Mar 2009 E404KXS   CHANGED PRT_CPL_TS TO STA_MDF_TS *
	 *                                AND UPDATED PDC_NM               *
	 *  PP02500 24 Jul 2012 E404BPO   CHANGED CER_TRM_CD TO            *
	 *                                CER_HLD_NOT_IND AND REMOVED      *
	 *                                CER_TRM_DES                      *
	 *  20163.20 13 Jul 2018 E404DMW  UPDATED ADDRESS AND CLIENT IDS   *
	 *                                FROM 20 TO 64                    *
	 * *****************************************************************</pre>*/
	public byte[] getActNotRowBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_ROW];
		return getActNotRowBytes(buffer, 1);
	}

	public void setActNotRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotFixed.setActNotFixedBytes(buffer, position);
		position += Xzc001ActNotFixed.Len.ACT_NOT_FIXED;
		setActNotDatesBytes(buffer, position);
		position += Len.ACT_NOT_DATES;
		setActNotKeyBytes(buffer, position);
		position += Len.ACT_NOT_KEY;
		setActNotKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_KEY_CI;
		actNotData.setActNotDataBytes(buffer, position);
		position += Xzc001ActNotData.Len.ACT_NOT_DATA;
		setExtensionFieldsBytes(buffer, position);
	}

	public byte[] getActNotRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotFixed.getActNotFixedBytes(buffer, position);
		position += Xzc001ActNotFixed.Len.ACT_NOT_FIXED;
		getActNotDatesBytes(buffer, position);
		position += Len.ACT_NOT_DATES;
		getActNotKeyBytes(buffer, position);
		position += Len.ACT_NOT_KEY;
		getActNotKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_KEY_CI;
		actNotData.getActNotDataBytes(buffer, position);
		position += Xzc001ActNotData.Len.ACT_NOT_DATA;
		getExtensionFieldsBytes(buffer, position);
		return buffer;
	}

	public void setActNotDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getActNotDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setActNotKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
	}

	public byte[] getActNotKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public void setActNotKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notPrcTsCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi = csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi = notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setExtensionFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotTypDesc = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_DESC);
	}

	public byte[] getExtensionFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotTypDesc, Len.ACT_NOT_TYP_DESC);
		return buffer;
	}

	public void setActNotTypDesc(String actNotTypDesc) {
		this.actNotTypDesc = Functions.subString(actNotTypDesc, Len.ACT_NOT_TYP_DESC);
	}

	public String getActNotTypDesc() {
		return this.actNotTypDesc;
	}

	public Xzc001ActNotData getActNotData() {
		return actNotData;
	}

	public Xzc001ActNotFixed getActNotFixed() {
		return actNotFixed;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0c0001LayoutBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRANS_PROCESS_DT = 10;
		public static final int ACT_NOT_DATES = TRANS_PROCESS_DT;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int ACT_NOT_KEY = CSR_ACT_NBR + NOT_PRC_TS;
		public static final int CSR_ACT_NBR_CI = 1;
		public static final int NOT_PRC_TS_CI = 1;
		public static final int ACT_NOT_KEY_CI = CSR_ACT_NBR_CI + NOT_PRC_TS_CI;
		public static final int ACT_NOT_TYP_DESC = 35;
		public static final int EXTENSION_FIELDS = ACT_NOT_TYP_DESC;
		public static final int ACT_NOT_ROW = Xzc001ActNotFixed.Len.ACT_NOT_FIXED + ACT_NOT_DATES + ACT_NOT_KEY + ACT_NOT_KEY_CI
				+ Xzc001ActNotData.Len.ACT_NOT_DATA + EXTENSION_FIELDS;
		public static final int WS_XZ0C0001_LAYOUT = ACT_NOT_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
