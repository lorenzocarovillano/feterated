/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsCicsApplid;
import com.federatedinsurance.crs.ws.enums.WsTarSys;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZC03090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXzc03090 {

	//==== PROPERTIES ====
	//Original name: WS-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-WEB-SVC-URL
	private String webSvcUrl = "";
	//Original name: WS-UP-USR-ID
	private String upUsrId = DefaultValues.stringVal(Len.UP_USR_ID);
	//Original name: WS-UP-PWD
	private String upPwd = DefaultValues.stringVal(Len.UP_PWD);
	//Original name: WS-CICS-APPLID
	private WsCicsApplid cicsApplid = new WsCicsApplid();
	//Original name: WS-TAR-SYS
	private WsTarSys tarSys = new WsTarSys();

	//==== METHODS ====
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public void setWebSvcUrl(String webSvcUrl) {
		this.webSvcUrl = Functions.subString(webSvcUrl, Len.WEB_SVC_URL);
	}

	public String getWebSvcUrl() {
		return this.webSvcUrl;
	}

	public void setUsrIdPwdBytes(byte[] buffer) {
		setUsrIdPwdBytes(buffer, 1);
	}

	public void setUsrIdPwdBytes(byte[] buffer, int offset) {
		int position = offset;
		upUsrId = MarshalByte.readString(buffer, position, Len.UP_USR_ID);
		position += Len.UP_USR_ID;
		upPwd = MarshalByte.readString(buffer, position, Len.UP_PWD);
	}

	public void setUpUsrId(String upUsrId) {
		this.upUsrId = Functions.subString(upUsrId, Len.UP_USR_ID);
	}

	public String getUpUsrId() {
		return this.upUsrId;
	}

	public void setUpPwd(String upPwd) {
		this.upPwd = Functions.subString(upPwd, Len.UP_PWD);
	}

	public String getUpPwd() {
		return this.upPwd;
	}

	public WsCicsApplid getCicsApplid() {
		return cicsApplid;
	}

	public WsTarSys getTarSys() {
		return tarSys;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UP_USR_ID = 8;
		public static final int UP_PWD = 8;
		public static final int WEB_SVC_URL = 256;
		public static final int USR_ID_PWD = UP_USR_ID + UP_PWD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
