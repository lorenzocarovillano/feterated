/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IClientTabV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [CLIENT_TAB_V]
 * 
 */
public class ClientTabVDao extends BaseSqlDao<IClientTabV> {

	private Cursor wsCltCursor1;
	private final IRowMapper<IClientTabV> fetchWsCltCursor1Rm = buildNamedRowMapper(IClientTabV.class, "clientId", "historyVldNbr", "ciclEffDt",
			"ciclPriSubCd", "ciclFstNm", "ciclLstNm", "ciclMdlNm", "nmPfx", "nmSfx", "primaryProDsnCdObj", "legEntCd", "ciclSdxCd",
			"ciclOgnInceptDtObj", "ciclAddNmIndObj", "ciclDobDt", "ciclBirStCd", "genderCd", "priLggCd", "userId", "statusCd", "terminalId",
			"ciclExpDt", "ciclEffAcyTs", "ciclExpAcyTs", "statutoryTleCd", "ciclLngNmObj");

	public ClientTabVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IClientTabV> getToClass() {
		return IClientTabV.class;
	}

	public DbAccessStatus openWsCltCursor1(String cw02hClientId) {
		wsCltCursor1 = buildQuery("openWsCltCursor1").bind("cw02hClientId", cw02hClientId).open();
		return dbStatus;
	}

	public DbAccessStatus closeWsCltCursor1() {
		return closeCursor(wsCltCursor1);
	}

	public IClientTabV fetchWsCltCursor1(IClientTabV iClientTabV) {
		return fetch(wsCltCursor1, iClientTabV, fetchWsCltCursor1Rm);
	}

	public long selectByCw02hClientId(String cw02hClientId, long dft) {
		return buildQuery("selectByCw02hClientId").bind("cw02hClientId", cw02hClientId).scalarResultLong(dft);
	}
}
