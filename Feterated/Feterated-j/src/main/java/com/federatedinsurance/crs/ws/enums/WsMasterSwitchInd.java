/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-MASTER-SWITCH-IND<br>
 * Variable: WS-MASTER-SWITCH-IND from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMasterSwitchInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SET_FLD = 'Y';
	public static final char NOT_SET = 'N';

	//==== METHODS ====
	public void setMasterSwitchInd(char masterSwitchInd) {
		this.value = masterSwitchInd;
	}

	public void setMasterSwitchIndFormatted(String masterSwitchInd) {
		setMasterSwitchInd(Functions.charAt(masterSwitchInd, Types.CHAR_SIZE));
	}

	public char getMasterSwitchInd() {
		return this.value;
	}

	public boolean isSetFld() {
		return value == SET_FLD;
	}

	public void setSetFld() {
		value = SET_FLD;
	}

	public boolean isNotSet() {
		return value == NOT_SET;
	}

	public void setNotSet() {
		value = NOT_SET;
	}
}
