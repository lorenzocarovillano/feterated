/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-PARM-NOT-FOUND<br>
 * Variable: EA-01-PARM-NOT-FOUND from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea01ParmNotFound {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-PARM-NOT-FOUND
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-01-PARM-NOT-FOUND-1
	private String flr2 = "THE FOLLOWING";
	//Original name: FILLER-EA-01-PARM-NOT-FOUND-2
	private String flr3 = "PARAMETER WAS";
	//Original name: FILLER-EA-01-PARM-NOT-FOUND-3
	private String flr4 = "NOT FOUND:";
	//Original name: FILLER-EA-01-PARM-NOT-FOUND-4
	private String flr5 = "";
	public static final String EA01_CICS_REGION_PARM = "CICS REGION (APPL ID)";

	//==== METHODS ====
	public String getEa01ParmNotFoundFormatted() {
		return MarshalByteExt.bufferToStr(getEa01ParmNotFoundBytes());
	}

	public byte[] getEa01ParmNotFoundBytes() {
		byte[] buffer = new byte[Len.EA01_PARM_NOT_FOUND];
		return getEa01ParmNotFoundBytes(buffer, 1);
	}

	public byte[] getEa01ParmNotFoundBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setFlr5(String flr5) {
		this.flr5 = Functions.subString(flr5, Len.FLR5);
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa01CicsRegionParm() {
		flr5 = EA01_CICS_REGION_PARM;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR5 = 30;
		public static final int FLR1 = 11;
		public static final int FLR2 = 14;
		public static final int FLR4 = 12;
		public static final int EA01_PARM_NOT_FOUND = FLR1 + 2 * FLR2 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
