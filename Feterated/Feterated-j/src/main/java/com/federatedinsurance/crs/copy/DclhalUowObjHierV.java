/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalUowObjHierV;

/**Original name: DCLHAL-UOW-OBJ-HIER-V<br>
 * Variable: DCLHAL-UOW-OBJ-HIER-V from copybook HALLGUOH<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalUowObjHierV implements IHalUowObjHierV {

	//==== PROPERTIES ====
	//Original name: HUOH-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HUOH-ROOT-BOBJ-NM
	private String rootBobjNm = DefaultValues.stringVal(Len.ROOT_BOBJ_NM);
	//Original name: HUOH-HIER-SEQ-NBR
	private short hierSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: HUOH-PNT-BOBJ-NM
	private String pntBobjNm = DefaultValues.stringVal(Len.PNT_BOBJ_NM);
	//Original name: HUOH-CHD-BOBJ-NM
	private String chdBobjNm = DefaultValues.stringVal(Len.CHD_BOBJ_NM);

	//==== METHODS ====
	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public String getUowNmFormatted() {
		return Functions.padBlanks(getUowNm(), Len.UOW_NM);
	}

	public void setRootBobjNm(String rootBobjNm) {
		this.rootBobjNm = Functions.subString(rootBobjNm, Len.ROOT_BOBJ_NM);
	}

	public String getRootBobjNm() {
		return this.rootBobjNm;
	}

	public String getRootBobjNmFormatted() {
		return Functions.padBlanks(getRootBobjNm(), Len.ROOT_BOBJ_NM);
	}

	@Override
	public void setHierSeqNbr(short hierSeqNbr) {
		this.hierSeqNbr = hierSeqNbr;
	}

	@Override
	public short getHierSeqNbr() {
		return this.hierSeqNbr;
	}

	@Override
	public void setPntBobjNm(String pntBobjNm) {
		this.pntBobjNm = Functions.subString(pntBobjNm, Len.PNT_BOBJ_NM);
	}

	@Override
	public String getPntBobjNm() {
		return this.pntBobjNm;
	}

	public String getPntBobjNmFormatted() {
		return Functions.padBlanks(getPntBobjNm(), Len.PNT_BOBJ_NM);
	}

	@Override
	public void setChdBobjNm(String chdBobjNm) {
		this.chdBobjNm = Functions.subString(chdBobjNm, Len.CHD_BOBJ_NM);
	}

	@Override
	public String getChdBobjNm() {
		return this.chdBobjNm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NM = 32;
		public static final int ROOT_BOBJ_NM = 32;
		public static final int PNT_BOBJ_NM = 32;
		public static final int CHD_BOBJ_NM = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
