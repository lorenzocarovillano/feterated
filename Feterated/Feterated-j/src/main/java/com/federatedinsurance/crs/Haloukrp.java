/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.storage.KeyType;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.HaloukrpData;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.enums.HaloukrpFunction;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;

/**Original name: HALOUKRP<br>
 * <pre>AUTHOR.
 *                   J. A. FULCHER FOR MYND.
 * DATE-WRITTEN.
 *                   MAR 2000.
 * DATE-COMPILED.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  KEY REPLACEMENT MODULE                     **
 * *                                                             **
 * * PLATFORM - ?                                                **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE - A. STORE A KEY ON KEY REPLACEMENT UMT             **
 * *           B. RETRIEVE A KEY FROM KEY REPLACEMENT UMT        **
 * *           C. CLEAR OUT ALL KEY REPLACEMENT ENTRIES ON       **
 * *              KEY REPLACEMENT UMT FOR SPECIFIED MSG-ID.      **
 * *                                                             **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED AS FOLLOWS:   **
 * *                                                             **
 * *                       1)LINK FROM A BUSINESS DATA OBJECT    **
 * *                                                             **
 * * DATA ACCESS METHODS - DATA PASSED IN COMMAREA               **
 * *                     - KEY REPLACEMENT UMT (HALFUKRP!)       **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #      DATE      PRGMR   DESCRIPTION                     **
 * * --------  --------  ------  --------------------------------**
 * * SAVANNAH  22MAR00   JAF     NEW PROGRAM.                    **
 * * C15748   08AUG01  ARSI600   INCREASE SIZE OF KEY REPLACEMENT**
 * *                             LABEL FROM 32 TO 150.           **
 * * 16596     29SEP01   18448   SINCE THIS MODULE IS CALLED TO  **
 * *                             CLEAR THE UMT WHETHER THE UOW   **
 * *                             SUCCEEDED OR FAILED, THERE IS A **
 * *                             CHANCE THAT THE UBOC ERROR IND  **
 * *                             WOULD ALREADY BE SET.  HOWEVER, **
 * *                             THE CLEAR REQUEST SHOULD STILL  **
 * *                             PROCEED.                        **
 * * 17241     13NOV01   03539   REPLACE REFERENCES TO IAP WITH  **
 * *                             COMPARABLE INFRASTRUCTURE CODE. **
 * * 24597     30OCT02   18448   ADD FUNCTIONALITY TO DELETE A   **
 * *                             SPECIFIC RECORD FROM THE UMT.   **
 * *                             NEEDED FOR LOCKING PERFORMANCE  **
 * *                             ENHANCEMENT.                    **
 * ****************************************************************</pre>*/
public class Haloukrp extends BatchProgram {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>---*
	 *  SQL AREAS
	 * ---*</pre>*/
	private Sqlca sqlca = new Sqlca();
	private ExecContext execContext = null;
	//Original name: WORKING-STORAGE
	private HaloukrpData ws = new HaloukrpData();
	//Original name: DFHCOMMAREA
	private Dfhcommarea dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, Dfhcommarea dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		main1();
		exitModule();
		return 0;
	}

	public static Haloukrp getInstance() {
		return (Programs.getInstance(Haloukrp.class));
	}

	/**Original name: 0000-MAIN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   THE 0000-MAIN PARAGRAPH IS RESPONSIBLE FOR CONTROLLING THE    *
	 *   PROCESSING OF THIS ROUTINE.                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void main1() {
		// COB_CODE: PERFORM 0100-INITIALISE.
		initialise();
		// COB_CODE: IF HALOUKRP-CLEAR-MSG-ID-FUNC
		//               PERFORM 0600-CLEAR-MSG-ID-FUNC
		//           ELSE
		//               PERFORM 0150-OTHER-FUNCS-CONTROL
		//           END-IF.
		if (ws.getHallukrp().getFunction().isClearMsgIdFunc()) {
			// COB_CODE: PERFORM 0600-CLEAR-MSG-ID-FUNC
			clearMsgIdFunc();
		} else {
			// COB_CODE: PERFORM 0150-OTHER-FUNCS-CONTROL
			otherFuncsControl();
		}
		// COB_CODE: PERFORM 0900-FINALISE.
		finalise();
	}

	/**Original name: EXIT-MODULE<br>*/
	private void exitModule() {
		// COB_CODE: EXEC CICS RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 0100-INITIALISE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  INITALISATION PROCESSING                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void initialise() {
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER TO HALOUKRP-LINKAGE.
		ws.setHaloukrpLinkageFormatted(dfhcommarea.getAppDataBufferFormatted());
	}

	/**Original name: 0150-OTHER-FUNCS-CONTROL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONTROL FUNCTIONS OTHER THAN 'CLEAR'.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void otherFuncsControl() {
		// COB_CODE: PERFORM 0200-VALIDATE-INPUT.
		validateInput();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0150-OTHER-FUNCS-CONTROL-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0150-OTHER-FUNCS-CONTROL-X
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN HALOUKRP-STORE-FUNC
		//                   PERFORM 0300-STORE-FUNC
		//               WHEN HALOUKRP-RETRIEVE-FUNC
		//                   PERFORM 0400-RETRIEVE-FUNC
		//               WHEN HALOUKRP-REWRITE-FUNC
		//                   PERFORM 0500-REWRITE-FUNC
		//               WHEN HALOUKRP-DELETE-FUNC
		//                   PERFORM 0700-DELETE-FUNC
		//           END-EVALUATE.
		switch (ws.getHallukrp().getFunction().getFunction()) {

		case HaloukrpFunction.STORE_FUNC:// COB_CODE: PERFORM 0300-STORE-FUNC
			storeFunc();
			break;

		case HaloukrpFunction.RETRIEVE_FUNC:// COB_CODE: PERFORM 0400-RETRIEVE-FUNC
			retrieveFunc();
			break;

		case HaloukrpFunction.REWRITE_FUNC:// COB_CODE: PERFORM 0500-REWRITE-FUNC
			rewriteFunc();
			break;

		case HaloukrpFunction.DELETE_FUNC:// COB_CODE: PERFORM 0700-DELETE-FUNC
			deleteFunc();
			break;

		default:
			break;
		}
	}

	/**Original name: 0200-VALIDATE-INPUT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE INPUT PARAMETER(S)                                    *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void validateInput() {
		ConcatUtil concatUtil = null;
		// COB_CODE:      IF NOT (HALOUKRP-STORE-FUNC OR
		//                        HALOUKRP-RETRIEVE-FUNC OR
		//                        HALOUKRP-REWRITE-FUNC OR
		//                        HALOUKRP-DELETE-FUNC)
		//           **           HALOUKRP-REWRITE-FUNC)
		//                    GO TO 0200-VALIDATE-INPUT-X
		//                END-IF.
		if (!(ws.getHallukrp().getFunction().isStoreFunc() || ws.getHallukrp().getFunction().isRetrieveFunc()
				|| ws.getHallukrp().getFunction().isRewriteFunc() || ws.getHallukrp().getFunction().isDeleteFunc())) {
			//*           HALOUKRP-REWRITE-FUNC)
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET BUSP-INVALID-FUNCTION    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidFunction();
			// COB_CODE: MOVE '0200-VALIDATE-INPUT'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0200-VALIDATE-INPUT");
			// COB_CODE: MOVE 'INPUT FUNCTION INVALID.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INPUT FUNCTION INVALID.");
			// COB_CODE: STRING 'HALOUKRP-FUNCTION='    HALOUKRP-FUNCTION      ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HALOUKRP-FUNCTION=",
					String.valueOf(ws.getHallukrp().getFunction().getFunction()), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0200-VALIDATE-INPUT-X
			return;
		}
		// COB_CODE: IF (HALOUKRP-STORE-FUNC OR HALOUKRP-REWRITE-FUNC)
		//             AND HALOUKRP-KEY-REPL-KEY-LEN > 40
		//               GO TO 0200-VALIDATE-INPUT-X
		//           END-IF.
		if ((ws.getHallukrp().getFunction().isStoreFunc() || ws.getHallukrp().getFunction().isRewriteFunc())
				&& ws.getHallukrp().getKeyReplKeyLen() > 40) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET BUSP-INV-PARAM-LEN       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvParamLen();
			// COB_CODE: MOVE '0200-VALIDATE-INPUT'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0200-VALIDATE-INPUT");
			// COB_CODE: MOVE 'INPUT KEY REPL KEY LEN > 40.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INPUT KEY REPL KEY LEN > 40.");
			// COB_CODE: MOVE HALOUKRP-KEY-REPL-KEY-LEN TO WS-DATA-KEY-DISP
			ws.getWsWorkFields().setDataKeyDisp(ws.getHallukrp().getKeyReplKeyLen());
			// COB_CODE: STRING 'HALOUKRP-KEY-REPL-KEY-LEN='  WS-DATA-KEY-DISP ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HALOUKRP-KEY-REPL-KEY-LEN=",
					ws.getWsWorkFields().getDataKeyDispAsString(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0200-VALIDATE-INPUT-X
			return;
		}
	}

	/**Original name: 0300-STORE-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PROCESS STORE FUNCTION                                         *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void storeFunc() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID               TO WS-HALFUKRP-UBOC-MSG-ID.
		ws.setWsHalfukrpUbocMsgId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE SPACES                    TO WS-HALFUKRP-BUS-OBJ-NM.
		ws.setWsHalfukrpBusObjNm("");
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-LABEL   TO WS-HALFUKRP-KEY-REPL-LABEL.
		ws.setWsHalfukrpKeyReplLabel(ws.getHallukrp().getKeyReplLabel());
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-KEY-LEN TO
		//                                           WS-HALFUKRP-KEY-REPL-KEY-LEN.
		ws.setWsHalfukrpKeyReplKeyLen(ws.getHallukrp().getKeyReplKeyLen());
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-KEY     TO WS-HALFUKRP-KEY-REPL-KEY.
		ws.setWsHalfukrpKeyReplKey(ws.getHallukrp().getKeyReplKey());
		// COB_CODE: EXEC CICS WRITE
		//               FILE   (UBOC-UOW-KEY-REPLACE-STORE)
		//               FROM   (WS-HALFUKRP-REC)
		//               LENGTH (LENGTH OF WS-HALFUKRP-REC)
		//               RIDFLD (WS-HALFUKRP-KEY)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowKeyReplaceStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsHalfukrpRecBytes());
			iRowData.setKey(ws.getWsHalfukrpKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0300-STORE-FUNC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE UBOC-UOW-KEY-REPLACE-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore());
			// COB_CODE: MOVE '0300-STORE-FUNC'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0300-STORE-FUNC");
			// COB_CODE: MOVE 'ERROR WRITING TO KEY REPLACEMENT UMT.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR WRITING TO KEY REPLACEMENT UMT.");
			// COB_CODE: STRING 'WS-HALFUKRP-UBOC-MSG-ID='
			//                  WS-HALFUKRP-UBOC-MSG-ID       ';'
			//                  'WS-HALFUKRP-BUS-OBJ-NM='
			//                  WS-HALFUKRP-BUS-OBJ-NM        ';'
			//                  'WS-HALFUKRP-KEY-REPL-LABEL='
			//                  WS-HALFUKRP-KEY-REPL-LABEL    ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "WS-HALFUKRP-UBOC-MSG-ID=", ws.getWsHalfukrpUbocMsgIdFormatted(), ";", "WS-HALFUKRP-BUS-OBJ-NM=",
							ws.getWsHalfukrpBusObjNmFormatted(), ";", "WS-HALFUKRP-KEY-REPL-LABEL=", ws.getWsHalfukrpKeyReplLabelFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0300-STORE-FUNC-X
			return;
		}
		// COB_CODE: SET HALOUKRP-OKAY TO TRUE.
		ws.getHallukrp().getReturnCode().setOkay();
	}

	/**Original name: 0400-RETRIEVE-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PROCESS RETRIVE FUNCTION                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void retrieveFunc() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		// COB_CODE: MOVE UBOC-MSG-ID               TO WS-HALFUKRP-UBOC-MSG-ID.
		ws.setWsHalfukrpUbocMsgId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE SPACES                    TO WS-HALFUKRP-BUS-OBJ-NM.
		ws.setWsHalfukrpBusObjNm("");
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-LABEL   TO WS-HALFUKRP-KEY-REPL-LABEL.
		ws.setWsHalfukrpKeyReplLabel(ws.getHallukrp().getKeyReplLabel());
		// COB_CODE: EXEC CICS READ
		//                FILE      (UBOC-UOW-KEY-REPLACE-STORE)
		//                INTO      (WS-HALFUKRP-REC)
		//                RIDFLD    (WS-HALFUKRP-KEY)
		//                KEYLENGTH (LENGTH OF WS-HALFUKRP-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowKeyReplaceStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getWsHalfukrpKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, HaloukrpData.Len.WS_HALFUKRP_KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.setWsHalfukrpRecBytes(iRowData.getData());
			}
		}
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 0400-RETRIEVE-FUNC-X
		//               WHEN OTHER
		//                   GO TO 0400-RETRIEVE-FUNC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET HALOUKRP-NOTFND TO TRUE
			ws.getHallukrp().getReturnCode().setNotfnd();
			// COB_CODE: GO TO 0400-RETRIEVE-FUNC-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-KEY-REPLACE-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore());
			// COB_CODE: MOVE '0400-RETRIEVE-FUNC'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0400-RETRIEVE-FUNC");
			// COB_CODE: MOVE 'ERROR READING KEY REPLACEMENT UMT.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR READING KEY REPLACEMENT UMT.");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0400-RETRIEVE-FUNC-X
			return;
		}
		// COB_CODE: SET HALOUKRP-OKAY TO TRUE.
		ws.getHallukrp().getReturnCode().setOkay();
		// COB_CODE: MOVE WS-HALFUKRP-KEY-REPL-KEY-LEN
		//                                         TO HALOUKRP-KEY-REPL-KEY-LEN.
		ws.getHallukrp().setKeyReplKeyLen(ws.getWsHalfukrpKeyReplKeyLen());
		// COB_CODE: MOVE WS-HALFUKRP-KEY-REPL-KEY TO HALOUKRP-KEY-REPL-KEY.
		ws.getHallukrp().setKeyReplKey(ws.getWsHalfukrpKeyReplKey());
	}

	/**Original name: 0500-REWRITE-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PROCESS REWRITE FUNCTION                                       *
	 *                                                                 *
	 * *****************************************************************
	 * * FIRST RETRIEVE THE ORIGINAL ROW FOR UPDATE.</pre>*/
	private void rewriteFunc() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID               TO WS-HALFUKRP-UBOC-MSG-ID.
		ws.setWsHalfukrpUbocMsgId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE SPACES                    TO WS-HALFUKRP-BUS-OBJ-NM.
		ws.setWsHalfukrpBusObjNm("");
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-LABEL   TO WS-HALFUKRP-KEY-REPL-LABEL.
		ws.setWsHalfukrpKeyReplLabel(ws.getHallukrp().getKeyReplLabel());
		// COB_CODE: EXEC CICS READ
		//                FILE      (UBOC-UOW-KEY-REPLACE-STORE)
		//                INTO      (WS-HALFUKRP-REC)
		//                RIDFLD    (WS-HALFUKRP-KEY)
		//                KEYLENGTH (LENGTH OF WS-HALFUKRP-KEY)
		//                UPDATE
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowKeyReplaceStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getWsHalfukrpKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, HaloukrpData.Len.WS_HALFUKRP_KEY, true);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.setWsHalfukrpRecBytes(iRowData.getData());
			}
		}
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0500-REWRITE-FUNC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-KEY-REPLACE-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore());
			// COB_CODE: MOVE '0500-REWRITE-FUNC'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0500-REWRITE-FUNC");
			// COB_CODE: MOVE 'ERROR READING KEY REPLACEMENT UMT FOR UPDATE.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR READING KEY REPLACEMENT UMT FOR UPDATE.");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0500-REWRITE-FUNC-X
			return;
		}
		//* NOW REWRITE THE ROW WITH THE NEW KEY REPL INFO
		// COB_CODE: MOVE UBOC-MSG-ID               TO WS-HALFUKRP-UBOC-MSG-ID.
		ws.setWsHalfukrpUbocMsgId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE SPACES                    TO WS-HALFUKRP-BUS-OBJ-NM.
		ws.setWsHalfukrpBusObjNm("");
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-LABEL   TO WS-HALFUKRP-KEY-REPL-LABEL.
		ws.setWsHalfukrpKeyReplLabel(ws.getHallukrp().getKeyReplLabel());
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-KEY-LEN TO
		//                                           WS-HALFUKRP-KEY-REPL-KEY-LEN.
		ws.setWsHalfukrpKeyReplKeyLen(ws.getHallukrp().getKeyReplKeyLen());
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-KEY     TO WS-HALFUKRP-KEY-REPL-KEY.
		ws.setWsHalfukrpKeyReplKey(ws.getHallukrp().getKeyReplKey());
		// COB_CODE: EXEC CICS REWRITE
		//               FILE   (UBOC-UOW-KEY-REPLACE-STORE)
		//               FROM   (WS-HALFUKRP-REC)
		//               LENGTH (LENGTH OF WS-HALFUKRP-REC)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowKeyReplaceStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsHalfukrpRecBytes());
			iRowDAO.update(iRowData);
		}
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0500-REWRITE-FUNC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE UBOC-UOW-KEY-REPLACE-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore());
			// COB_CODE: MOVE '0500-REWRITE-FUNC'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0500-REWRITE-FUNC");
			// COB_CODE: MOVE 'ERROR REWRITING TO KEY REPLACEMENT UMT.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR REWRITING TO KEY REPLACEMENT UMT.");
			// COB_CODE: STRING 'WS-HALFUKRP-UBOC-MSG-ID='
			//                  WS-HALFUKRP-UBOC-MSG-ID       ';'
			//                  'WS-HALFUKRP-BUS-OBJ-NM='
			//                  WS-HALFUKRP-BUS-OBJ-NM        ';'
			//                  'WS-HALFUKRP-KEY-REPL-LABEL='
			//                  WS-HALFUKRP-KEY-REPL-LABEL    ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "WS-HALFUKRP-UBOC-MSG-ID=", ws.getWsHalfukrpUbocMsgIdFormatted(), ";", "WS-HALFUKRP-BUS-OBJ-NM=",
							ws.getWsHalfukrpBusObjNmFormatted(), ";", "WS-HALFUKRP-KEY-REPL-LABEL=", ws.getWsHalfukrpKeyReplLabelFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0500-REWRITE-FUNC-X
			return;
		}
		// COB_CODE: SET HALOUKRP-OKAY TO TRUE.
		ws.getHallukrp().getReturnCode().setOkay();
	}

	/**Original name: 0600-CLEAR-MSG-ID-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  DELETE ALL KEY REPLACEMENT ENTRIES FOR SPECIFIED MSG-ID        *
	 *                                                                 *
	 *  THIS 'CLEAR' FUNCTION WILL BE ATTEMPTED WHETHER OR NOT PREVIOUS*
	 *  ERRORS HAVE BEEN DETECTED IN THE CONVERSATION.                 *
	 *                                                                 *
	 *  IF PROBLEMS WITH THIS PROCESS, LOG A WARNING (NOT ERROR) SO    *
	 *  FAILS IN THIS PROCESS DON'T TAKE CONVERSATION OUT.             *
	 *                                                                 *
	 * *****************************************************************
	 *  DELETE EACH KEY REPL UMT REC ASSOCIATED WITH SPECIFIED MSG-ID</pre>*/
	private void clearMsgIdFunc() {
		// COB_CODE: SET HALOUKRP-OKAY TO TRUE.
		ws.getHallukrp().getReturnCode().setOkay();
		// COB_CODE: SET WS-MORE-KEY-REPL-UMT-RECS TO TRUE.
		ws.getWsMoreKeyReplUmtRecsInd().setMoreKeyReplUmtRecs();
		// COB_CODE: PERFORM 0610-CLEAR-MSG-ID-DELETE
		//             UNTIL WS-NO-MORE-KEY-REPL-UMT-RECS.
		while (!ws.getWsMoreKeyReplUmtRecsInd().isNoMoreKeyReplUmtRecs()) {
			clearMsgIdDelete();
		}
	}

	/**Original name: 0610-CLEAR-MSG-ID-DELETE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  DELETE A KEY REPLACEMENT RECORD FOR SPECIFIED MSG-ID           *
	 *                                                                 *
	 * *****************************************************************
	 *  READ A KEY REPLACEMENT RECORD WITH SPECIFIED MSG-ID</pre>*/
	private void clearMsgIdDelete() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		// COB_CODE: MOVE UBOC-MSG-ID      TO WS-HALFUKRP-UBOC-MSG-ID.
		ws.setWsHalfukrpUbocMsgId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: EXEC CICS READ
		//                FILE      (UBOC-UOW-KEY-REPLACE-STORE)
		//                INTO      (WS-HALFUKRP-REC)
		//                RIDFLD    (WS-HALFUKRP-UBOC-MSG-ID)
		//                KEYLENGTH (LENGTH OF WS-HALFUKRP-UBOC-MSG-ID)
		//                GENERIC
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowKeyReplaceStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(MarshalByteExt.strToBuffer(ws.getWsHalfukrpUbocMsgId(), HaloukrpData.Len.WS_HALFUKRP_UBOC_MSG_ID));
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, HaloukrpData.Len.WS_HALFUKRP_UBOC_MSG_ID, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.setWsHalfukrpRecBytes(iRowData.getData());
			}
		}
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 0610-CLEAR-MSG-ID-DELETE-X
		//               WHEN OTHER
		//                   GO TO 0610-CLEAR-MSG-ID-DELETE-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET WS-NO-MORE-KEY-REPL-UMT-RECS TO TRUE
			ws.getWsMoreKeyReplUmtRecsInd().setNoMoreKeyReplUmtRecs();
			// COB_CODE: GO TO 0610-CLEAR-MSG-ID-DELETE-X
			return;
		} else {
			// COB_CODE: SET WS-NO-MORE-KEY-REPL-UMT-RECS TO TRUE
			ws.getWsMoreKeyReplUmtRecsInd().setNoMoreKeyReplUmtRecs();
			// COB_CODE: SET WS-LOG-WARNING           TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-KEY-REPLACE-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore());
			// COB_CODE: MOVE '0610-CLEAR-MSG-ID-DELETE'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0610-CLEAR-MSG-ID-DELETE");
			// COB_CODE: MOVE 'ERROR READING KEY REPLACEMENT UMT.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR READING KEY REPLACEMENT UMT.");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0610-CLEAR-MSG-ID-DELETE-X
			return;
		}
		// DELETE THIS KEY REPL UMT RECORD
		// COB_CODE: EXEC CICS DELETE
		//                FILE      (UBOC-UOW-KEY-REPLACE-STORE)
		//                KEYLENGTH (LENGTH OF WS-HALFUKRP-KEY)
		//                RIDFLD    (WS-HALFUKRP-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowKeyReplaceStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getWsHalfukrpKeyBytes());
			iRowDAO.delete(iRowData, HaloukrpData.Len.WS_HALFUKRP_KEY);
		}
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0610-CLEAR-MSG-ID-DELETE-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-NO-MORE-KEY-REPL-UMT-RECS TO TRUE
			ws.getWsMoreKeyReplUmtRecsInd().setNoMoreKeyReplUmtRecs();
			// COB_CODE: SET WS-LOG-WARNING           TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: MOVE UBOC-UOW-KEY-REPLACE-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore());
			// COB_CODE: MOVE '0610-CLEAR-MSG-ID-DELETE'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0610-CLEAR-MSG-ID-DELETE");
			// COB_CODE: MOVE 'ERROR CLEARING KEY REPLACEMENT UMT RECORDS.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR CLEARING KEY REPLACEMENT UMT RECORDS.");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0610-CLEAR-MSG-ID-DELETE-X
			return;
		}
	}

	/**Original name: 0700-DELETE-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PROCESS DELETE  FUNCTION                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void deleteFunc() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		// COB_CODE: SET HALOUKRP-OKAY TO TRUE.
		ws.getHallukrp().getReturnCode().setOkay();
		// COB_CODE: MOVE UBOC-MSG-ID               TO WS-HALFUKRP-UBOC-MSG-ID.
		ws.setWsHalfukrpUbocMsgId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE SPACES                    TO WS-HALFUKRP-BUS-OBJ-NM.
		ws.setWsHalfukrpBusObjNm("");
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-LABEL   TO WS-HALFUKRP-KEY-REPL-LABEL.
		ws.setWsHalfukrpKeyReplLabel(ws.getHallukrp().getKeyReplLabel());
		// COB_CODE: EXEC CICS DELETE
		//                FILE      (UBOC-UOW-KEY-REPLACE-STORE)
		//                RIDFLD    (WS-HALFUKRP-KEY)
		//                KEYLENGTH (LENGTH OF WS-HALFUKRP-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowKeyReplaceStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getWsHalfukrpKeyBytes());
			iRowDAO.delete(iRowData, HaloukrpData.Len.WS_HALFUKRP_KEY);
		}
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//               WHEN DFHRESP(NOTFND)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0700-DELETE-FUNC-X
		//           END-EVALUATE.
		if ((TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL)
				|| (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NOTFND)) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: MOVE UBOC-UOW-KEY-REPLACE-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore());
			// COB_CODE: MOVE '0700-DELETE-FUNC'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0700-DELETE-FUNC");
			// COB_CODE: MOVE 'ERROR DELETING REC FROM KEY REPLACEMENT UMT.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR DELETING REC FROM KEY REPLACEMENT UMT.");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0700-DELETE-FUNC-X
			return;
		}
	}

	/**Original name: 0900-FINALISE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  FINALISATION PROCESSING                                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void finalise() {
		// COB_CODE: MOVE HALOUKRP-LINKAGE TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.setAppDataBuffer(ws.getHaloukrpLinkageFormatted());
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsWorkFields().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsWorkFields().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsWorkFields().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsWorkFields().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsWorkFields().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsWorkFields().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALOUKRP", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsWorkFields().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibrespDsply(ws.getWsWorkFields().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibresp2Dsply(ws.getWsWorkFields().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
