/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-INVALID-INPUT<br>
 * Variable: EA-02-INVALID-INPUT from program FNC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea02InvalidInputFnc02090 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-INVALID-INPUT
	private String flr1 = "INVALID INPUT.";
	//Original name: FILLER-EA-02-INVALID-INPUT-1
	private String flr2 = "INPUT TYPE:";
	//Original name: EA-02-INPUT-MATCH-TYPE
	private String matchType = DefaultValues.stringVal(Len.MATCH_TYPE);
	//Original name: FILLER-EA-02-INVALID-INPUT-2
	private String flr3 = " INPUT STRING:";
	//Original name: EA-02-INPUT-STRING
	private String stringFld = DefaultValues.stringVal(Len.STRING_FLD);

	//==== METHODS ====
	public String getEa02InvalidInputFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidInputBytes());
	}

	public byte[] getEa02InvalidInputBytes() {
		byte[] buffer = new byte[Len.EA02_INVALID_INPUT];
		return getEa02InvalidInputBytes(buffer, 1);
	}

	public byte[] getEa02InvalidInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, matchType, Len.MATCH_TYPE);
		position += Len.MATCH_TYPE;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, stringFld, Len.STRING_FLD);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setMatchType(String matchType) {
		this.matchType = Functions.subString(matchType, Len.MATCH_TYPE);
	}

	public String getMatchType() {
		return this.matchType;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setStringFld(String stringFld) {
		this.stringFld = Functions.subString(stringFld, Len.STRING_FLD);
	}

	public String getStringFld() {
		return this.stringFld;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MATCH_TYPE = 30;
		public static final int STRING_FLD = 256;
		public static final int FLR1 = 15;
		public static final int FLR2 = 12;
		public static final int EA02_INVALID_INPUT = MATCH_TYPE + STRING_FLD + 2 * FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
