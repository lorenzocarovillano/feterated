/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLWC-NOT-RPT<br>
 * Variable: DCLWC-NOT-RPT from copybook XZH00029<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclwcNotRpt {

	//==== PROPERTIES ====
	//Original name: POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: STA-MDF-TS
	private String staMdfTs = DefaultValues.stringVal(Len.STA_MDF_TS);
	//Original name: CMP-CD
	private String cmpCd = DefaultValues.stringVal(Len.CMP_CD);
	//Original name: NOT-TYP-CD
	private String notTypCd = DefaultValues.stringVal(Len.NOT_TYP_CD);
	//Original name: CNC-REN-TYP-CD
	private String cncRenTypCd = DefaultValues.stringVal(Len.CNC_REN_TYP_CD);
	//Original name: REA-CNC-REN-CD
	private String reaCncRenCd = DefaultValues.stringVal(Len.REA_CNC_REN_CD);
	//Original name: INSD-NM
	private String insdNm = DefaultValues.stringVal(Len.INSD_NM);
	//Original name: INSD-ADR
	private String insdAdr = DefaultValues.stringVal(Len.INSD_ADR);
	//Original name: LEG-ETY-DES
	private String legEtyDes = DefaultValues.stringVal(Len.LEG_ETY_DES);
	//Original name: CNC-MAL-INSD-DT
	private String cncMalInsdDt = DefaultValues.stringVal(Len.CNC_MAL_INSD_DT);
	//Original name: NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);

	//==== METHODS ====
	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public String getPolEffDtFormatted() {
		return Functions.padBlanks(getPolEffDt(), Len.POL_EFF_DT);
	}

	public void setStaMdfTs(String staMdfTs) {
		this.staMdfTs = Functions.subString(staMdfTs, Len.STA_MDF_TS);
	}

	public String getStaMdfTs() {
		return this.staMdfTs;
	}

	public String getStaMdfTsFormatted() {
		return Functions.padBlanks(getStaMdfTs(), Len.STA_MDF_TS);
	}

	public void setCmpCd(String cmpCd) {
		this.cmpCd = Functions.subString(cmpCd, Len.CMP_CD);
	}

	public String getCmpCd() {
		return this.cmpCd;
	}

	public void setNotTypCd(String notTypCd) {
		this.notTypCd = Functions.subString(notTypCd, Len.NOT_TYP_CD);
	}

	public String getNotTypCd() {
		return this.notTypCd;
	}

	public void setCncRenTypCd(String cncRenTypCd) {
		this.cncRenTypCd = Functions.subString(cncRenTypCd, Len.CNC_REN_TYP_CD);
	}

	public String getCncRenTypCd() {
		return this.cncRenTypCd;
	}

	public void setReaCncRenCd(String reaCncRenCd) {
		this.reaCncRenCd = Functions.subString(reaCncRenCd, Len.REA_CNC_REN_CD);
	}

	public String getReaCncRenCd() {
		return this.reaCncRenCd;
	}

	public void setInsdNm(String insdNm) {
		this.insdNm = Functions.subString(insdNm, Len.INSD_NM);
	}

	public String getInsdNm() {
		return this.insdNm;
	}

	public void setInsdAdr(String insdAdr) {
		this.insdAdr = Functions.subString(insdAdr, Len.INSD_ADR);
	}

	public String getInsdAdr() {
		return this.insdAdr;
	}

	public void setLegEtyDes(String legEtyDes) {
		this.legEtyDes = Functions.subString(legEtyDes, Len.LEG_ETY_DES);
	}

	public String getLegEtyDes() {
		return this.legEtyDes;
	}

	public void setCncMalInsdDt(String cncMalInsdDt) {
		this.cncMalInsdDt = Functions.subString(cncMalInsdDt, Len.CNC_MAL_INSD_DT);
	}

	public String getCncMalInsdDt() {
		return this.cncMalInsdDt;
	}

	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	public String getNotEffDt() {
		return this.notEffDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 7;
		public static final int POL_EFF_DT = 10;
		public static final int STA_MDF_TS = 26;
		public static final int NOT_TYP_CD = 5;
		public static final int NOT_EFF_DT = 10;
		public static final int CMP_CD = 2;
		public static final int CNC_REN_TYP_CD = 2;
		public static final int REA_CNC_REN_CD = 2;
		public static final int INSD_NM = 120;
		public static final int INSD_ADR = 120;
		public static final int LEG_ETY_DES = 40;
		public static final int CNC_MAL_INSD_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
