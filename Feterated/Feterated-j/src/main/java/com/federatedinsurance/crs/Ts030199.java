/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.config.ConfigSettings;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.dao.FedRmtPrtTabDao;
import com.federatedinsurance.crs.commons.data.dao.Sysdummy1Dao;
import com.federatedinsurance.crs.copy.SqlcaTs030199;
import com.federatedinsurance.crs.ws.LReportDbParameters;
import com.federatedinsurance.crs.ws.Ts030199Data;
import com.federatedinsurance.crs.ws.enums.LRdActionIndicator;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;

/**Original name: TS030199<br>
 * <pre>AUTHOR.         ROB LIVENGOOD.
 * DATE-WRITTEN.   NOVEMBER  2007.
 *        TITLE - REPORT INFORMATION RETRIEVAL SUBPROGRAM
 * **************************************************************
 *  NOTE: SINCE THIS IS A DB2 PROGRAM, IT MUST BE COMPILED      *
 *        THRU ENDEVOR WHEN TESTING.  THAT MEANS THE MODULE     *
 *        BECOMES AVAILABLE FOR ANYONE ELSE TESTING A PROGRAM   *
 *        WHOSE LOAD MODULE IS PUT INTO AN ENDEVOR TESTLIB.  IT *
 *        MAY BE NECESSARY TO CHANGE THE NAME OF THIS MODULE    *
 *        UNTIL TESTING IS COMPLETE.                            *
 *                                                              *
 * **************************************************************
 *        INPUT - ACTION INDICATOR
 *              - REPORT NUMBER
 *       OUTPUT - STATUS CODE
 *              - TABLE DATA
 *              - ERROR MESSAGES
 *      PROCESS - DETERMINE ACTION TYPE
 *              - IF CONNECT, TEST FOR A CONNECTION CURRENTLY ACTIVE
 *              - IF RETRIEVE, FIND AND RETURN REPORT INFORMATION
 *              - RETURN APPROPRIATE STATUS CODE, TABLE INFORMATION
 *                (IF NEEDED), AND ERROR MESSAGES (IF NEEDED)
 *        NOTES - THIS PROGRAM WORKS UNDER THE ASSUMPTION THAT THE
 *                CALLING PROGRAM HAS ESTABLISHED A CONNECTION TO
 *                DB2.  THE CONNECT OPTION WILL ONLY VERIFY THAT
 *                THERE IS A CONNECTION.  IF NOT, TS030299 SHOULD
 *                BE USED TO MAKE THE CONNECTION AND RETRIEVE ALL
 *                REPORT INFORMATION.
 *              - CHANGES TO THIS PROGRAM MAY ALSO CAUSE CHANGES TO
 *                TS030299.
 *     MAINTENANCE HISTORY
 *     *******************
 *      INFO   CHANGE
 *       NBR    DATE     III   DESCRIPTION
 *     *****  ********   ***   *************************************
 *    TO07600 11/29/07   RKL   NEW PROGRAM
 *    TO07600 11/30/07   DMA   STANDARDIZING PROGRAM
 *   TL000197 04/30/10   DMA   RETUN ONLY THE 'ACTIVE' RECORD.</pre>*/
public class Ts030199 extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private SqlcaTs030199 sqlca = new SqlcaTs030199();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private Sysdummy1Dao sysdummy1Dao = new Sysdummy1Dao(dbAccessStatus);
	private FedRmtPrtTabDao fedRmtPrtTabDao = new FedRmtPrtTabDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Ts030199Data ws = new Ts030199Data();
	//Original name: WHEN-COMPILED
	private static final String WHEN_COMPILED = ConfigSettings.whenCompiled(true);
	//Original name: L-REPORT-DB-PARAMETERS
	private LReportDbParameters lReportDbParameters;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LReportDbParameters lReportDbParameters) {
		this.lReportDbParameters = lReportDbParameters;
		retrieveReportData();
		programExit();
		return 0;
	}

	public static Ts030199 getInstance() {
		return (Programs.getInstance(Ts030199.class));
	}

	/**Original name: 1000-RETRIEVE-REPORT-DATA<br>
	 * <pre>    INITIALIZE OUTPUT AREAS</pre>*/
	private void retrieveReportData() {
		// COB_CODE: SET L-RD-STATUS-SUCCESSFUL  TO TRUE.
		lReportDbParameters.getStatus().setSuccessful();
		// COB_CODE: MOVE SPACES                 TO L-RD-TABLE-DATA
		//                                          L-RD-ERROR-MESSAGES.
		lReportDbParameters.setTableData("");
		lReportDbParameters.initErrorMessagesSpaces();
		//    IF ACTION IS TO DISCONNECT, DO NOT DO ANYTHING SINCE CALLING
		//    PROGRAM ALREADY HAD THE CURRENT CONNECTION.  IT IS
		//    RESPONSIBLE FOR DISCONNECTION IN ITS OWN PROGRAM WHEN
		//    COMPLETED.
		// COB_CODE: EVALUATE TRUE
		//               WHEN L-RD-AI-CONNECT
		//                      THRU 2000-EXIT
		//               WHEN L-RD-AI-DISCONNECT
		//                   CONTINUE
		//               WHEN OTHER
		//                      THRU 3000-EXIT
		//           END-EVALUATE.
		switch (lReportDbParameters.getActionIndicator().getActionIndicator()) {

		case LRdActionIndicator.CONNECT:// COB_CODE: PERFORM 2000-CHECK-DB-CONNECTION
			//              THRU 2000-EXIT
			checkDbConnection();
			break;

		case LRdActionIndicator.DISCONNECT:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: PERFORM 3000-RETRIEVE-REPORT-DATA
			//              THRU 3000-EXIT
			retrieveReportData1();
			break;
		}
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 2000-CHECK-DB-CONNECTION<br>
	 * <pre>    THIS PARAGRAPH DETERMINES IF THERE IS AN EXISTING CONNECTION
	 *     TO THE DATABASE.</pre>*/
	private void checkDbConnection() {
		// COB_CODE: EXEC SQL
		//               SELECT 1 INTO :SA-COUNT
		//                FROM SYSIBM.SYSDUMMY1
		//           END-EXEC.
		ws.setSaCount(sysdummy1Dao.selectRec(ws.getSaCount()));
		//    IF SUCCESSFUL, THEN ALREADY CONNECTED TO DB.  SEND
		//    'SUCCESSFUL' STATUS BACK.  THIS PROGRAM WILL BE USED FOR
		//    ALL FUTURE CALLS.
		//    IF NOT CONNECTED TO DB, SEND 'NOT SUCCESSFUL' STATUS BACK
		//    SO CALLING PROGRAM KNOWS TO USE TS030299 FOR ALL FUTURE
		//    CALLS.
		//    IF FATAL ERROR ENCOUNTERED, GIVE ERROR MESSAGE
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SC-SUCCESSFUL-RUN
		//                   CONTINUE
		//               WHEN SQLCODE = CF-SC-NOT-CONNECTED
		//                                       TO TRUE
		//               WHEN OTHER
		//                      THRU 9000-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getCfScSuccessfulRun()) {
			// COB_CODE: CONTINUE
			//continue
		} else if (sqlca.getSqlcode() == ws.getCfScNotConnected()) {
			// COB_CODE: SET L-RD-STATUS-NOT-CONNECTED
			//                               TO TRUE
			lReportDbParameters.getStatus().setNotConnected();
		} else {
			// COB_CODE: PERFORM 9000-FILL-SQL-ERROR-MESSAGE
			//              THRU 9000-EXIT
			fillSqlErrorMessage();
		}
	}

	/**Original name: 3000-RETRIEVE-REPORT-DATA<br>
	 * <pre>    THIS PARAGRAPH RETRIEVES THE REPORT DATA BASED ON THE
	 *     PROVIDED REPORT NUMBER.
	 *     ONLY PULL THE REPORT RECORD WHERE THE RECORD STATUS CODE
	 *     IS ACTIVE (RATHER THAN PENDING OR HISTORICAL).</pre>*/
	private void retrieveReportData1() {
		// COB_CODE: MOVE L-RD-REPORT-NBR        TO SA-REPORT-NBR.
		ws.setSaReportNbr(lReportDbParameters.getReportNbr());
		// COB_CODE: EXEC SQL
		//               SELECT
		//                   RPT_NBR,
		//                   RPT_DESC,
		//                   RPT_ACT_FLG,
		//                   RPT_BAL_FLG,
		//                   OFF_LOC_1,
		//                   OFF_LOC_FLG_1,
		//                   OFF_LOC_2,
		//                   OFF_LOC_FLG_2,
		//                   OFF_LOC_3,
		//                   OFF_LOC_FLG_3,
		//                   OFF_LOC_4,
		//                   OFF_LOC_FLG_4,
		//                   OFF_LOC_5,
		//                   OFF_LOC_FLG_5,
		//                   OFF_LOC_6,
		//                   OFF_LOC_FLG_6,
		//                   OFF_LOC_7,
		//                   OFF_LOC_FLG_7,
		//                   OFF_LOC_8,
		//                   OFF_LOC_FLG_8,
		//                   OFF_LOC_9,
		//                   OFF_LOC_FLG_9,
		//                   OFF_LOC_10,
		//                   OFF_LOC_FLG_10,
		//                   OFF_LOC_11,
		//                   OFF_LOC_FLG_11,
		//                   OFF_LOC_12,
		//                   OFF_LOC_FLG_12,
		//                   OFF_LOC_13,
		//                   OFF_LOC_FLG_13,
		//                   OFF_LOC_14,
		//                   OFF_LOC_FLG_14,
		//                   OFF_LOC_15,
		//                   OFF_LOC_FLG_15,
		//                   OFF_LOC_DEFLT_FLG
		//               INTO   :RPT-NBR                            ,
		//                      :RPT-DESC                           ,
		//                      :RPT-ACT-FLG                        ,
		//                      :RPT-BAL-FLG                        ,
		//                      :OFF-LOC-1                          ,
		//                      :OFF-LOC-FLG-1                      ,
		//                      :OFF-LOC-2                          ,
		//                      :OFF-LOC-FLG-2                      ,
		//                      :OFF-LOC-3                          ,
		//                      :OFF-LOC-FLG-3                      ,
		//                      :OFF-LOC-4                          ,
		//                      :OFF-LOC-FLG-4                      ,
		//                      :OFF-LOC-5                          ,
		//                      :OFF-LOC-FLG-5                      ,
		//                      :OFF-LOC-6                          ,
		//                      :OFF-LOC-FLG-6                      ,
		//                      :OFF-LOC-7                          ,
		//                      :OFF-LOC-FLG-7                      ,
		//                      :OFF-LOC-8                          ,
		//                      :OFF-LOC-FLG-8                      ,
		//                      :OFF-LOC-9                          ,
		//                      :OFF-LOC-FLG-9                      ,
		//                      :OFF-LOC-10                         ,
		//                      :OFF-LOC-FLG-10                     ,
		//                      :OFF-LOC-11                         ,
		//                      :OFF-LOC-FLG-11                     ,
		//                      :OFF-LOC-12                         ,
		//                      :OFF-LOC-FLG-12                     ,
		//                      :OFF-LOC-13                         ,
		//                      :OFF-LOC-FLG-13                     ,
		//                      :OFF-LOC-14                         ,
		//                      :OFF-LOC-FLG-14                     ,
		//                      :OFF-LOC-15                         ,
		//                      :OFF-LOC-FLG-15                     ,
		//                      :OFF-LOC-DEFLT-FLG
		//                   FROM  FED_RMT_PRT_TAB
		//                   WHERE RPT_NBR = :SA-REPORT-NBR
		//                     AND RPT_RCD_STA_CD = '0'
		//           END-EXEC.
		fedRmtPrtTabDao.selectBySaReportNbr(ws.getSaReportNbr(), ws.getDclfedRmtPrtTab());
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SC-SUCCESSFUL-RUN
		//                                       TO L-RD-TABLE-DATA
		//               WHEN SQLCODE = CF-SC-NO-RECORD-FOUND
		//                                       TO TRUE
		//               WHEN SQLCODE = CF-SC-NOT-CONNECTED
		//                                       TO TRUE
		//               WHEN OTHER
		//                   GO TO 3000-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getCfScSuccessfulRun()) {
			// COB_CODE: MOVE DCLFED-RMT-PRT-TAB
			//                               TO L-RD-TABLE-DATA
			lReportDbParameters.setTableData(ws.getDclfedRmtPrtTab().getDclfedRmtPrtTabFormatted());
		} else if (sqlca.getSqlcode() == ws.getCfScNoRecordFound()) {
			// COB_CODE: SET L-RD-STATUS-REPORT-NOT-FOUND
			//                               TO TRUE
			lReportDbParameters.getStatus().setReportNotFound();
		} else if (sqlca.getSqlcode() == ws.getCfScNotConnected()) {
			// COB_CODE: SET L-RD-STATUS-NOT-CONNECTED
			//                               TO TRUE
			lReportDbParameters.getStatus().setNotConnected();
		} else {
			// COB_CODE: PERFORM 9000-FILL-SQL-ERROR-MESSAGE
			//              THRU 9000-EXIT
			fillSqlErrorMessage();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 9000-FILL-SQL-ERROR-MESSAGE<br>*/
	private void fillSqlErrorMessage() {
		// COB_CODE: SET L-RD-STATUS-FATAL-ERROR TO TRUE.
		lReportDbParameters.getStatus().setFatalError();
		//    FILL IN PROGRAM COMPILE MESSAGE
		// COB_CODE: MOVE WHEN-COMPILED          TO EA-00-DATE
		//                                          EA-00-TIME.
		ws.getEa00ProgramMsg().setDateFld(WHEN_COMPILED);
		ws.getEa00ProgramMsg().setTimeFldFormatted(getWhenCompiledFormatted());
		// COB_CODE: MOVE EA-00-PROGRAM-MSG      TO L-RD-EM-MESSAGE1.
		lReportDbParameters.setEmMessage1(ws.getEa00ProgramMsg().getEa00ProgramMsgFormatted());
		//    CAPTURE THE SQL ERROR CODE AND MESSAGE
		// COB_CODE: MOVE SQLCODE                TO EA-01-SQL-CODE.
		ws.getEa01FatalErrorMsg().setCode(sqlca.getSqlcode());
		// COB_CODE: MOVE SQLERRMC               TO EA-01-SQL-MESSAGE.
		ws.getEa01FatalErrorMsg().setMessage(sqlca.getSqlerrmc());
		// COB_CODE: MOVE EA-01-FATAL-ERROR-MSG  TO L-RD-EM-MESSAGE2.
		lReportDbParameters.setEmMessage2(ws.getEa01FatalErrorMsg().getEa01FatalErrorMsgFormatted());
	}

	public String getWhenCompiledFormatted() {
		return Functions.padBlanks(WHEN_COMPILED, Len.WHEN_COMPILED);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WHEN_COMPILED = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
