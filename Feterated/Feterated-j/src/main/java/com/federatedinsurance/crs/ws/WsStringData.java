/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsFirstNameFoundSw;
import com.federatedinsurance.crs.ws.enums.WsNameParsedSw;

/**Original name: WS-STRING-DATA<br>
 * Variable: WS-STRING-DATA from program CIWBNSRB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsStringData {

	//==== PROPERTIES ====
	//Original name: STRG-FORMAT-OUT-LAST
	private String strgFormatOutLast = "";
	//Original name: STRG-FORMAT-OUT-LAST-END
	private char strgFormatOutLastEnd = Types.SPACE_CHAR;
	//Original name: STRG-FORMAT-OUT-MID
	private String strgFormatOutMid = "";
	//Original name: STRG-FORMAT-OUT-MID-END
	private char strgFormatOutMidEnd = Types.SPACE_CHAR;
	//Original name: STRG-FORMAT-OUT-MID2
	private String strgFormatOutMid2 = "";
	//Original name: STRG-FORMAT-OUT-MID2-END
	private char strgFormatOutMid2End = Types.SPACE_CHAR;
	//Original name: STRG-PARSED-NAME
	private String strgParsedName = "";
	//Original name: STRG-PARSED-NAME-END
	private char strgParsedNameEnd = Types.SPACE_CHAR;
	//Original name: STRG-SPACE1
	private char strgSpace1 = ' ';
	//Original name: STRG-SPACE1-END
	private char strgSpace1End = Types.SPACE_CHAR;
	/**Original name: HOLD-PARSE-MIN-IDX<br>
	 * <pre>***********************************************************
	 * ** THE FOLLOWING WORKING STORAGE IS FOR THE CODE THAT WAS
	 * ** PULLED FROM THE FEDERATED VERSION OF CIWBNSRB. WE ARE
	 * ** KEEPING THIS SEPARATE FOR EASE OF COMPARING SUBSEQUENT
	 * ** VERSIONS OF THIS CODE.
	 * ***********************************************************</pre>*/
	private short holdParseMinIdx = ((short) 0);
	//Original name: HOLD-PARSE-MAX-IDX
	private short holdParseMaxIdx = ((short) 0);
	//Original name: HOLD-PARSE-TARGET-IDX
	private short holdParseTargetIdx = ((short) 0);
	//Original name: CF-NAME-DELIMITER
	private char cfNameDelimiter = ';';
	//Original name: WS-NAME-PARSED-SW
	private WsNameParsedSw wsNameParsedSw = new WsNameParsedSw();
	//Original name: WS-FIRST-NAME-FOUND-SW
	private WsFirstNameFoundSw wsFirstNameFoundSw = new WsFirstNameFoundSw();

	//==== METHODS ====
	public void initWsStringDataSpaces() {
		strgFormatOutLast = "";
		strgFormatOutLastEnd = Types.SPACE_CHAR;
		strgFormatOutMid = "";
		strgFormatOutMidEnd = Types.SPACE_CHAR;
		strgFormatOutMid2 = "";
		strgFormatOutMid2End = Types.SPACE_CHAR;
		strgParsedName = "";
		strgParsedNameEnd = Types.SPACE_CHAR;
		strgSpace1 = Types.SPACE_CHAR;
		strgSpace1End = Types.SPACE_CHAR;
		holdParseMinIdx = Types.INVALID_SHORT_VAL;
		holdParseMaxIdx = Types.INVALID_SHORT_VAL;
		holdParseTargetIdx = Types.INVALID_SHORT_VAL;
		cfNameDelimiter = Types.SPACE_CHAR;
		wsNameParsedSw.setWsNameParsedSw(Types.SPACE_CHAR);
		wsFirstNameFoundSw.setWsFirstNameFoundSw(Types.SPACE_CHAR);
	}

	public void setStrgFormatOutMid(String strgFormatOutMid) {
		this.strgFormatOutMid = Functions.subString(strgFormatOutMid, Len.STRG_FORMAT_OUT_MID);
	}

	public String getStrgFormatOutMid() {
		return this.strgFormatOutMid;
	}

	public void setHoldParseMinIdx(short holdParseMinIdx) {
		this.holdParseMinIdx = holdParseMinIdx;
	}

	public short getHoldParseMinIdx() {
		return this.holdParseMinIdx;
	}

	public void setHoldParseMaxIdx(short holdParseMaxIdx) {
		this.holdParseMaxIdx = holdParseMaxIdx;
	}

	public short getHoldParseMaxIdx() {
		return this.holdParseMaxIdx;
	}

	public void setHoldParseTargetIdx(short holdParseTargetIdx) {
		this.holdParseTargetIdx = holdParseTargetIdx;
	}

	public short getHoldParseTargetIdx() {
		return this.holdParseTargetIdx;
	}

	public char getCfNameDelimiter() {
		return this.cfNameDelimiter;
	}

	public WsFirstNameFoundSw getWsFirstNameFoundSw() {
		return wsFirstNameFoundSw;
	}

	public WsNameParsedSw getWsNameParsedSw() {
		return wsNameParsedSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STRG_FORMAT_OUT_MID = 45;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
