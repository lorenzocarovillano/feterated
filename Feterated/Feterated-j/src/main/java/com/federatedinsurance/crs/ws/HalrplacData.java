/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALRPLAC<br>
 * Generated as a class for rule WS.<br>*/
public class HalrplacData {

	//==== PROPERTIES ====
	//Original name: WS-WORK-FIELDS
	private WsWorkFields wsWorkFields = new WsWorkFields();
	//Original name: ASSEM-IND
	private int assemInd = 1;
	//Original name: PLAC-IND
	private int placInd = 1;
	//Original name: MESS-IND
	private int messInd = 1;

	//==== METHODS ====
	public void setAssemInd(int assemInd) {
		this.assemInd = assemInd;
	}

	public int getAssemInd() {
		return this.assemInd;
	}

	public void setPlacInd(int placInd) {
		this.placInd = placInd;
	}

	public int getPlacInd() {
		return this.placInd;
	}

	public void setMessInd(int messInd) {
		this.messInd = messInd;
	}

	public int getMessInd() {
		return this.messInd;
	}

	public WsWorkFields getWsWorkFields() {
		return wsWorkFields;
	}
}
