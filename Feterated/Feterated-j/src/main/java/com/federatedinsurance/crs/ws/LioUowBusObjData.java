/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LIO-UOW-BUS-OBJ-DATA<br>
 * Variable: LIO-UOW-BUS-OBJ-DATA from program HALRRESP<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LioUowBusObjData extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int FLR1_MAXOCCURS = 32000;
	private int fillerLioUowBusObjDataListenerSize = FLR1_MAXOCCURS;
	private IValueChangeListener fillerLioUowBusObjDataListener = new FillerLioUowBusObjDataListener();
	//Original name: FILLER-LIO-UOW-BUS-OBJ-DATA
	private char[] flr1 = new char[FLR1_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public LioUowBusObjData() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return getLioUowBusObjDataSize();
	}

	@Override
	public void deserialize(byte[] buf) {
		setLioUowBusObjDataBytes(buf);
	}

	public void init() {
		for (int flr1Idx = 1; flr1Idx <= FLR1_MAXOCCURS; flr1Idx++) {
			setFlr1(flr1Idx, DefaultValues.CHAR_VAL);
		}
	}

	public int getLioUowBusObjDataSize() {
		return Types.CHAR_SIZE * fillerLioUowBusObjDataListenerSize;
	}

	public void setLioUowBusObjDataFormatted(String data) {
		byte[] buffer = new byte[getLioUowBusObjDataSize()];
		MarshalByte.writeString(buffer, 1, data, getLioUowBusObjDataSize());
		setLioUowBusObjDataBytes(buffer, 1);
	}

	public String getLioUowBusObjDataFormatted() {
		return MarshalByteExt.bufferToStr(getLioUowBusObjDataBytes());
	}

	public void setLioUowBusObjDataBytes(byte[] buffer) {
		setLioUowBusObjDataBytes(buffer, 1);
	}

	public byte[] getLioUowBusObjDataBytes() {
		byte[] buffer = new byte[getLioUowBusObjDataSize()];
		return getLioUowBusObjDataBytes(buffer, 1);
	}

	public void setLioUowBusObjDataBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= fillerLioUowBusObjDataListenerSize; idx++) {
			if (position <= buffer.length) {
				setFlr1(idx, MarshalByte.readChar(buffer, position));
				position += Types.CHAR_SIZE;
			} else {
				setFlr1(idx, Types.SPACE_CHAR);
				position += Types.CHAR_SIZE;
			}
		}
	}

	public byte[] getLioUowBusObjDataBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= fillerLioUowBusObjDataListenerSize; idx++) {
			MarshalByte.writeChar(buffer, position, getFlr1(idx));
			position += Types.CHAR_SIZE;
		}
		return buffer;
	}

	public void setFlr1(int flr1Idx, char flr1) {
		this.flr1[flr1Idx - 1] = flr1;
	}

	public char getFlr1(int flr1Idx) {
		return this.flr1[flr1Idx - 1];
	}

	public IValueChangeListener getFillerLioUowBusObjDataListener() {
		return fillerLioUowBusObjDataListener;
	}

	@Override
	public byte[] serialize() {
		return getLioUowBusObjDataBytes();
	}

	//==== INNER CLASSES ====
	/**Original name: FILLER-LIO-UOW-BUS-OBJ-DATA<br>*/
	public class FillerLioUowBusObjDataListener implements IValueChangeListener {

		//==== METHODS ====
		@Override
		public void change() {
			fillerLioUowBusObjDataListenerSize = FLR1_MAXOCCURS;
		}

		@Override
		public void change(int value) {
			fillerLioUowBusObjDataListenerSize = value < 1 ? 0 : (value > FLR1_MAXOCCURS ? FLR1_MAXOCCURS : value);
		}
	}
}
