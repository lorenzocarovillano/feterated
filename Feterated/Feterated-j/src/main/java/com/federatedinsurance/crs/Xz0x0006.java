/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.copy.DriverSpecificData;
import com.federatedinsurance.crs.ws.ConstantFieldsXz0x0006;
import com.federatedinsurance.crs.ws.DfhcommareaTs020000;
import com.federatedinsurance.crs.ws.WorkingStorageAreaXz0x0006;
import com.federatedinsurance.crs.ws.enums.WsOperationNameXz0x0006;
import com.federatedinsurance.crs.ws.ptr.DfhcommareaMu0x0004;
import com.federatedinsurance.crs.ws.ptr.LContractLocation;
import com.federatedinsurance.crs.ws.ptr.LFrameworkRequestAreaXz0x0005;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0006;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0X0006<br>
 * <pre>AUTHOR.       GARY LIEDTKE.
 * DATE-WRITTEN. 03 OCT 2008.
 * ***************************************************************
 *   PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
 *                       UOW        : XZ_MAINTAIN_ACT_NOT        *
 *                       OPERATIONS : AddAccountNotification     *
 *                                    UpdateAccountNotification  *
 *                                    DeleteAccountNotification  *
 *                                                               *
 *   PLATFORM - HOST CICS                                        *
 *                                                               *
 *   PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
 *              EXECUTION OF FRAMEWORK MAIN DRIVER               *
 *                                                               *
 *   PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
 *                        PROGRAM OR FROM AN IVORY WRAPPER       *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
 *                         AND SHARED MEMORY                     *
 *                         OUTPUT RETURNED VIA DFHCOMMAREA       *
 *                         AND SHARED MEMORY                     *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #    DATE      EMP ID              DESCRIPTION          *
 *  -------- ---------  -------   ------------------------------ *
 *  TO07614  09/30/2008 E404GCL   INITIAL PROGRAM                *
 *  TO07614  02/04/2009 E404DLP   ADDED ADD-CNC-DAY FIELD        *
 *  TO07614  03/09/2009 E404DLP   CHANGE ADR-SEQNBR TO ADR-ID    *
 *  TO07614  13/16/2009 E404KXS   CHANGE PRT_CPL_TS TO STA_MDF_TS*
 *  TL000143 02/18/2010 E404DMA   UPDATED TO DYNAMIC PROXY LOGIC *
 *  PP02500  08/09/2012 E404BPO   CHANGED CER-TRM-CD TO          *
 *                                CER-HLD-NOT-IND                *
 * ***************************************************************</pre>*/
public class Xz0x0006 extends Program {

	//==== PROPERTIES ====
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0x0006 constantFields = new ConstantFieldsXz0x0006();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0x0006 workingStorageArea = new WorkingStorageAreaXz0x0006();
	//Original name: MAIN-DRIVER-DATA
	private DfhcommareaTs020000 mainDriverData = new DfhcommareaTs020000();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaMu0x0004 dfhcommarea = new DfhcommareaMu0x0004(null);
	/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
	 * <pre>* "FRAMEWORK" REQUEST AREA FOR THIS OPERATION
	 * * INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE REQUEST TO
	 * * THE BUSINESS FRAMEWORK.</pre>*/
	private LFrameworkRequestAreaXz0x0005 lFrameworkRequestArea = new LFrameworkRequestAreaXz0x0005(null);
	/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
	 * <pre>* "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION
	 * * INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE RESPONSE FROM
	 * * THE BUSINESS FRAMEWORK.</pre>*/
	private LFrameworkRequestAreaXz0x0005 lFrameworkResponseArea = new LFrameworkRequestAreaXz0x0005(null);
	/**Original name: L-SERVICE-CONTRACT-AREA<br>
	 * <pre>* SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
	 * * BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK</pre>*/
	private LServiceContractAreaXz0x0006 lServiceContractArea = new LServiceContractAreaXz0x0006(null);
	/**Original name: L-CONTRACT-LOCATION<br>
	 * <pre>***************************************************************
	 *  TSC21PRO - PROXY COMMON LOGIC                                *
	 * ***************************************************************
	 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR COMMON LOGIC      *
	 *         COPYBOOK VERSIONING.                                  *
	 *         APPLICATION CODING.                                   *
	 *                                                               *
	 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
	 *                                                               *
	 *    WR #    DATE     EMP ID              DESCRIPTION           *
	 *  -------- --------- -------   ------------------------------- *
	 *  TS129    06MAR06   E404LJL   INITIAL VERSION                 *
	 *  TL000113 30JUN09   E404DMA   EXPAND FUNCTIONALITY SUCH THAT  *
	 *                               DATA CAN BE PASSED VIA CHANNEL  *
	 *                               & CONTAINERS, COMMAREA ONLY, OR *
	 *                               REFERENCE STORAGE (POINTERS).   *
	 * ***************************************************************</pre>*/
	private LContractLocation lContractLocation = new LContractLocation(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaMu0x0004 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea.assignBc(dfhcommarea);
		mainline();
		exit();
		return 0;
	}

	public static Xz0x0006 getInstance() {
		return (Programs.getInstance(Xz0x0006.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-CALL-FRAMEWORK
		//              THRU 3000-EXIT.
		callFramework();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: PERFORM 8000-ENDING-HOUSEKEEPING
		//              THRU 8000-EXIT.
		endingHousekeeping();
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM INITIALIZATION AND OTHER SETUP TASKS                 *
	 *  DETERMINE METHOD OF CAPTURING INPUT CONTRACTS TO GET         *
	 *  ADDRESSABILITY TO THEM.                                      *
	 * ***************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO WS-PROGRAM-NAME.
		workingStorageArea.setWsProgramName(constantFields.getProgramName());
		// COB_CODE: PERFORM 2500-CAPTURE-INPUT-CONTRACTS
		//              THRU 2500-EXIT.
		captureInputContracts();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: INITIALIZE DSD-ERROR-HANDLING-PARMS
		//                      PPC-ERROR-HANDLING-PARMS.
		initDsdErrorHandlingParms();
		initPpcErrorHandlingParms();
		//***************************************************************
		// FRAMEWORK FORMAT MEMORY NEEDS TO BE ALLOCATED BY THIS        *
		// PROGRAM BEFORE CALLING THE MAIN DRIVER.                      *
		//***************************************************************
		// COB_CODE: PERFORM 2100-ALLOCATE-FW-MEMORY
		//              THRU 2100-EXIT.
		allocateFwMemory();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-GET-USERID
		//              THRU 2200-EXIT.
		getUserid();
	}

	/**Original name: 2100-ALLOCATE-FW-MEMORY<br>
	 * <pre>***************************************************************
	 *  ALLOCATE THE FRAMEWORK REQUEST AND RESPONSE AREAS            *
	 *  FOR THIS OPERATION.                                          *
	 * ***************************************************************</pre>*/
	private void allocateFwMemory() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF L-FRAMEWORK-REQUEST-AREA
		//                                       TO MA-INPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(LFrameworkRequestAreaXz0x0005.Len.L_FRAMEWORK_REQUEST_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaInputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2100-ALLOCATE-FW-MEMORY'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2100-ALLOCATE-FW-MEMORY", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-FRAMEWORK-REQUEST-AREA
		//                                       TO MA-INPUT-POINTER.
		lFrameworkRequestArea = ((pointerManager
				.resolve(mainDriverData.getCommunicationShellCommon().getMaInputPointer(), LFrameworkRequestAreaXz0x0005.class)));
		// COB_CODE: INITIALIZE L-FRAMEWORK-REQUEST-AREA.
		initLFrameworkRequestArea();
		// COB_CODE: MOVE LENGTH OF L-FRAMEWORK-RESPONSE-AREA
		//                                       TO MA-OUTPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaOutputDataSize(LFrameworkRequestAreaXz0x0005.Len.L_FRAMEWORK_REQUEST_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//              SET(MA-OUTPUT-POINTER)
		//              FLENGTH(MA-OUTPUT-DATA-SIZE)
		//              INITIMG(CF-BLANK)
		//              RESP(WS-RESPONSE-CODE)
		//              RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaOutputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaOutputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2100-ALLOCATE-FW-MEMORY'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2100-ALLOCATE-FW-MEMORY", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-FRAMEWORK-RESPONSE-AREA
		//                                       TO MA-OUTPUT-POINTER.
		lFrameworkResponseArea = ((pointerManager
				.resolve(mainDriverData.getCommunicationShellCommon().getMaOutputPointer(), LFrameworkRequestAreaXz0x0005.class)));
		// COB_CODE: INITIALIZE L-FRAMEWORK-RESPONSE-AREA.
		initLFrameworkResponseArea();
	}

	/**Original name: 2200-GET-USERID<br>
	 * <pre>***************************************************************
	 *  GET THE USERID FOR THIS TRANSACTION FROM CICS AND POPULATE   *
	 *  THE FRAMEWORK LEVEL USERID FIELD.  THIS CAN BE OVERRIDEN BY  *
	 *  A USER-SUPPLIED USERID IF NECESSARY.                         *
	 * ***************************************************************</pre>*/
	private void getUserid() {
		// COB_CODE: INITIALIZE CSC-AUTH-USERID.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setAuthUserid("");
		// COB_CODE: EXEC CICS ASSIGN
		//               USERID(CSC-AUTH-USERID)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setAuthUserid(execContext.getUserId());
		execContext.clearStatus();
	}

	/**Original name: 2500-CAPTURE-INPUT-CONTRACTS<br>
	 * <pre>***************************************************************
	 *  DETERMINE IF THE DATA WAS PASSED USING CHANNELS AND          *
	 *  CONTAINERS, COMMAREA AND REFERENCE STORAGE, OR COMMARE ONLY. *
	 *  BASED ON THAT, CAPTURE THE PROXY COMMON CONTRACT AND SERVICE *
	 *  CONTRACT TO BE USED.                                         *
	 * ***************************************************************
	 *     IF THE DATA WAS PASSED USING CHANNELS AND CONTAINERS, THEN
	 *      THE COMMAREA WOULD NOT BE ALLOCATED, RESULTING IN A COMMAREA
	 *      LENGTH OF ZERO.  IF THE CONTRACT IS IN REFERENCE STORAGE,
	 *      THEN THE ONLY THING IN THE COMMAREA SHOULD BE THE PROXY
	 *      COMMON COPYBOOK.</pre>*/
	private void captureInputContracts() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EIBCALEN = +0
		//                      THRU 2510-EXIT
		//               WHEN EIBCALEN = LENGTH OF PROXY-PROGRAM-COMMON
		//                             + LENGTH OF L-SERVICE-CONTRACT-AREA
		//                      THRU 2520-EXIT
		//               WHEN OTHER
		//                      THRU 2530-EXIT
		//           END-EVALUATE.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 2510-CAPTURE-FROM-CHANNEL
			//              THRU 2510-EXIT
			captureFromChannel();
		} else if (execContext.getCommAreaLen() == DfhcommareaMu0x0004.Len.PROXY_PROGRAM_COMMON
				+ LServiceContractAreaXz0x0006.Len.L_SERVICE_CONTRACT_AREA) {
			// COB_CODE: PERFORM 2520-CAPTURE-FROM-COMMAREA
			//              THRU 2520-EXIT
			captureFromCommarea();
		} else {
			// COB_CODE: PERFORM 2530-CAPTURE-FROM-REF-STORAGE
			//              THRU 2530-EXIT
			captureFromRefStorage();
		}
	}

	/**Original name: 2510-CAPTURE-FROM-CHANNEL<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING CHANNELS AND CONTAINERS.  ALLOCATE AND *
	 *  CAPTURE THE PROXY COMMON CONTRACT FOLLOWED BY THE SERVICE    *
	 *  CONTRACT.                                                    *
	 * ***************************************************************</pre>*/
	private void captureFromChannel() {
		// COB_CODE: PERFORM 2511-ALLOCATE-PROXY-CNT
		//              THRU 2511-EXIT.
		allocateProxyCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2512-GET-PROXY-COMMON-CNT
		//              THRU 2512-EXIT.
		getProxyCommonCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2513-ALLOCATE-SERVICE-CONTRACT
		//              THRU 2513-EXIT.
		allocateServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		//    HAVE THE DEVELOPER SPECIFY THE LOCATION AND LENGTHS OF THE
		//     SERVICE CONTRACT INPUT AND OUTPUT AREAS.
		// COB_CODE: PERFORM 2514-CAPTURE-SVC-CTT-ATB
		//              THRU 2514-EXIT.
		captureSvcCttAtb();
		// COB_CODE: PERFORM 2515-GET-SERVICE-CONTRACT
		//              THRU 2515-EXIT.
		getServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
	}

	/**Original name: 2511-ALLOCATE-PROXY-CNT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE PROXY COMMON CONTRACT
	 * ***************************************************************</pre>*/
	private void allocateProxyCnt() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF DFHCOMMAREA  TO MA-INPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(DfhcommareaMu0x0004.Len.DFHCOMMAREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaInputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		//!   IF AN ISSUE IS ENCOUNTERED HERE, AN ABEND WILL LIKELY OCCUR.
		//!    ALTHOUGH NOT DESIRED, THERE IS NOTHING ELSE THAT CAN BE DONE
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2511-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2511-ALLOCATE-PROXY-CNT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2511-ALLOCATE-PROXY-CNT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2511-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF DFHCOMMAREA  TO MA-INPUT-POINTER.
		dfhcommarea = ((pointerManager.resolve(mainDriverData.getCommunicationShellCommon().getMaInputPointer(),
				DfhcommareaMu0x0004.class)));
		// COB_CODE: INITIALIZE DFHCOMMAREA.
		initDfhcommarea();
		//    WITH THE PROXY COMMON CONTRACT ALLOCATED, SPECIFY BOTH THE
		//     SIZE AND LOCATION OF THE INPUT AND OUTPUT AREAS.
		// COB_CODE: SET WS-PC-INPUT-PTR         TO ADDRESS OF PPC-INPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setInputPtr(pointerManager.addressOf(dfhcommarea));
		// COB_CODE: SET WS-PC-OUTPUT-PTR        TO ADDRESS OF PPC-OUTPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setOutputPtr(pointerManager.addressOf(dfhcommarea, DfhcommareaMu0x0004.Pos.PPC_OUTPUT_PARMS));
		// COB_CODE: COMPUTE WS-PC-INPUT-LEN = LENGTH OF PPC-INPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setInputLen(DfhcommareaMu0x0004.Len.PPC_INPUT_PARMS);
		// COB_CODE: COMPUTE WS-PC-OUTPUT-LEN = LENGTH OF PPC-OUTPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setOutputLen(DfhcommareaMu0x0004.Len.PPC_OUTPUT_PARMS);
	}

	/**Original name: 2512-GET-PROXY-COMMON-CNT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE PROXY COMMON CONTRACT       *
	 * ***************************************************************</pre>*/
	private void getProxyCommonCnt() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-PC-INPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsProxyContractAtb().getInputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-PROXY-CBK-INP-NM)
		//               INTO       (L-CONTRACT-LOCATION (1: WS-PC-INPUT-LEN))
		//               FLENGTH    (WS-PC-INPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsProxyContractAtb().getInputLen());
		Channel.read(execContext, "", constantFields.getCopybookNames().getProxyCbkInpNmFormatted(), tsOutputData);
		lContractLocation.setlContractLocationFromBuffer(tsOutputData.getData());
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2512-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2512-GET-PROXY-COMMON-CNT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2512-GET-PROXY-COMMON-CNT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2512-EXIT
			return;
		}
	}

	/**Original name: 2513-ALLOCATE-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT  *
	 * ***************************************************************</pre>*/
	private void allocateServiceContract() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF L-SERVICE-CONTRACT-AREA
		//                                       TO MA-INPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(LServiceContractAreaXz0x0006.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaInputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2513-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2513-ALLOCATE-SERVICE-CONTRACT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2513-ALLOCATE-SERVICE-CONTRACT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2513-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO MA-INPUT-POINTER.
		lServiceContractArea = ((pointerManager
				.resolve(mainDriverData.getCommunicationShellCommon().getMaInputPointer(), LServiceContractAreaXz0x0006.class)));
		// COB_CODE: INITIALIZE L-SERVICE-CONTRACT-AREA.
		initLServiceContractArea();
	}

	/**Original name: 2514-CAPTURE-SVC-CTT-ATB<br>
	 * <pre>***************************************************************
	 *  CAPTURE BOTH THE SIZE AND LOCATION OF THE INPUT AND OUTPUT   *
	 *  AREAS OF THE SERVICE CONTRACT AS SPECIFIED BY THE DEVELOPER  *
	 * ***************************************************************</pre>*/
	private void captureSvcCttAtb() {
		// COB_CODE: PERFORM 12514-SET-SVC-CTT-ATB
		//              THRU 12514-EXIT.
		setSvcCttAtb();
	}

	/**Original name: 2515-GET-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE SERVICE CONTRACT            *
	 * ***************************************************************</pre>*/
	private void getServiceContract() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-SC-INPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsServiceContractAtb().getInputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-SERVICE-CBK-INP-NM)
		//               INTO       (L-CONTRACT-LOCATION (1: WS-SC-INPUT-LEN))
		//               FLENGTH    (WS-SC-INPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsServiceContractAtb().getInputLen());
		Channel.read(execContext, "", constantFields.getCopybookNames().getServiceCbkInpNmFormatted(), tsOutputData);
		lContractLocation.setlContractLocationFromBuffer(tsOutputData.getData());
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2515-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2515-GET-SERVICE-CONTRACT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2515-GET-SERVICE-CONTRACT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2515-EXIT
			return;
		}
	}

	/**Original name: 2520-CAPTURE-FROM-COMMAREA<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING THE COMMAREA ONLY.  SINCE THE          *
	 *  ENVIRONMENT WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY,   *
	 *  SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT.              *
	 * ***************************************************************
	 *   SINCE THE SERVICE CONTRACT IS APPENDED TO THE PROXY CONTRACT,
	 *    SET THE LOCATION OF THE SERVICE CONTRACT TO BE IMMEDIATELY
	 *    AFTER THE PROXY CONTRACT.</pre>*/
	private void captureFromCommarea() {
		// COB_CODE: SET WS-CA-PTR               TO
		//                                        ADDRESS OF PROXY-PROGRAM-COMMON.
		workingStorageArea.getWsCaPtr().setWsCaPtr(pointerManager.addressOf(dfhcommarea));
		// COB_CODE: ADD LENGTH OF PROXY-PROGRAM-COMMON
		//                                       TO WS-CA-PTR-VAL.
		workingStorageArea.getWsCaPtr().setWsCaPtrVal(DfhcommareaMu0x0004.Len.PROXY_PROGRAM_COMMON + workingStorageArea.getWsCaPtr().getWsCaPtrVal());
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO WS-CA-PTR.
		lServiceContractArea = ((pointerManager.resolve(workingStorageArea.getWsCaPtr().getWsCaPtr(),
				LServiceContractAreaXz0x0006.class)));
	}

	/**Original name: 2530-CAPTURE-FROM-REF-STORAGE<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING REFERENCE STORAGE.  SINCE THE CALLER   *
	 *  WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY, SET THE       *
	 *  ADDRESSIBILITY OF THE SERVICE CONTRACT.                      *
	 * ***************************************************************</pre>*/
	private void captureFromRefStorage() {
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO PPC-SERVICE-DATA-POINTER.
		lServiceContractArea = ((pointerManager.resolve(dfhcommarea.getPpcServiceDataPointer(),
				LServiceContractAreaXz0x0006.class)));
	}

	/**Original name: 3000-CALL-FRAMEWORK<br>
	 * <pre>***************************************************************
	 *  PERFORM SET UP FOR AND MAKE THE CALL TO THE FRAMEWORK MAIN   *
	 *  DRIVER.                                                      *
	 * ***************************************************************</pre>*/
	private void callFramework() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 3100-SET-UP-INPUT
		//              THRU 3100-EXIT.
		setUpInput();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CF-MAIN-DRIVER)
		//               COMMAREA   (MAIN-DRIVER-DATA)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0X0006", execContext).commarea(mainDriverData).length(DfhcommareaTs020000.Len.DFHCOMMAREA)
				.link(constantFields.getMainDriver(), new Ts020000());
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '3000-CALL-FRAMEWORK'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"3000-CALL-FRAMEWORK", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: IF NOT DSD-NO-ERROR-CODE
		//                                       TO PPC-ERROR-HANDLING-PARMS
		//           END-IF.
		if (!mainDriverData.getDriverSpecificData().getErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: MOVE DSD-ERROR-HANDLING-PARMS
			//                                   TO PPC-ERROR-HANDLING-PARMS
			dfhcommarea.setPpcErrorHandlingParmsBytes(mainDriverData.getDriverSpecificData().getDsdErrorHandlingParmsBytes());
		}
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3200-SET-UP-OUTPUT
		//              THRU 3200-EXIT.
		setUpOutput();
	}

	/**Original name: 3100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
	 *  INPUT AREA                                                   *
	 * ***************************************************************
	 *  FRAMEWORK PARAMETERS</pre>*/
	private void setUpInput() {
		// COB_CODE: MOVE PPC-OPERATION          TO CSC-OPERATION.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setOperation(dfhcommarea.getPpcOperation());
		// COB_CODE: MOVE CF-UNIT-OF-WORK        TO CSC-UNIT-OF-WORK.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setUnitOfWork(constantFields.getUnitOfWork());
		// COB_CODE: MOVE CF-REQUEST-MODULE      TO CSC-REQUEST-MODULE.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setRequestModule(constantFields.getRequestModule());
		// COB_CODE: MOVE CF-RESPONSE-MODULE     TO CSC-RESPONSE-MODULE.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setResponseModule(constantFields.getResponseModule());
		// COB_CODE: MOVE PPC-BYPASS-SYNCPOINT-MDRV-IND
		//                                       TO DSD-BYPASS-SYNCPOINT-MDRV-IND.
		mainDriverData.getDriverSpecificData().getBypassSyncpointMdrvInd().setBypassSyncpointMdrvInd(dfhcommarea.getPpcBypassSyncpointMdrvInd());
		// COB_CODE: PERFORM 13100-SET-UP-INPUT
		//              THRU 13100-EXIT.
		setUpInput1();
	}

	/**Original name: 3200-SET-UP-OUTPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
	 *  RESPONSE AREA.                                               *
	 * ***************************************************************</pre>*/
	private void setUpOutput() {
		// COB_CODE: PERFORM 13200-SET-UP-OUTPUT
		//              THRU 13200-EXIT.
		setUpOutput1();
		// COB_CODE: IF EIBCALEN = +0
		//                  THRU 3210-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 3210-OUTPUT-SVC-CONTRACT-CTA
			//              THRU 3210-EXIT
			outputSvcContractCta();
		}
	}

	/**Original name: 3210-OUTPUT-SVC-CONTRACT-CTA<br>
	 * <pre>***************************************************************
	 *  SINCE THE SERVICE CONTRACT WILL NOT BE UPDATED FURTHER FROM  *
	 *  THIS POINT, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************</pre>*/
	private void outputSvcContractCta() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-SC-OUTPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsServiceContractAtb().getOutputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-SERVICE-CBK-OUP-NM)
		//               FROM       (L-CONTRACT-LOCATION (1: WS-SC-OUTPUT-LEN))
		//               FLENGTH    (WS-SC-OUTPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsServiceContractAtb().getOutputLen());
		tsOutputData.setData(lContractLocation.getlContractLocationAsBuffer());
		Channel.write(execContext, "", constantFields.getCopybookNames().getServiceCbkOupNmFormatted(), tsOutputData);
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3210-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '3210-OUTPUT-SVC-CONTRACT-CTA'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"3210-OUTPUT-SVC-CONTRACT-CTA", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 3210-EXIT
			return;
		}
	}

	/**Original name: 8000-ENDING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM CLEANUP TASKS SUCH AS FREEING MEMORY.                *
	 * ***************************************************************</pre>*/
	private void endingHousekeeping() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(L-FRAMEWORK-REQUEST-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lFrameworkRequestArea));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-1'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-1", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(L-FRAMEWORK-RESPONSE-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lFrameworkResponseArea));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-2'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-2", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IF USING CHANNELS AND CONTAINERS, DEALLOCATE THE SERVICE
		//     CONTRACT AND THE PROXY CONTRACT.  OTHERWISE, THAT WILL BE
		//     THE RESPONSIBILITY OF THE CALLER AND/OR THE CICS ENVIRONMENT
		// COB_CODE: IF EIBCALEN = +0
		//               CONTINUE
		//           ELSE
		//               GO TO 8000-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
		//     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
		// COB_CODE: IF ADDRESS OF L-SERVICE-CONTRACT-AREA NOT = NULLS
		//               END-EXEC
		//           END-IF.
		if (!pointerManager.isNullPointer(pointerManager.addressOf(lServiceContractArea))) {
			// COB_CODE: EXEC CICS FREEMAIN
			//               DATA(L-SERVICE-CONTRACT-AREA)
			//               RESP(WS-RESPONSE-CODE)
			//               RESP2(WS-RESPONSE-CODE2)
			//           END-EXEC
			cicsStorageManager.freemain(execContext, pointerManager.addressOf(lServiceContractArea));
			workingStorageArea.setWsResponseCode(execContext.getResp());
			workingStorageArea.setWsResponseCode2(execContext.getResp2());
		}
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL
				&& !pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-3'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-3", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
		//              THRU 8100-EXIT.
		outputProxyCommonCta();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 8000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
		//     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
		// COB_CODE: IF ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               END-EXEC
		//           END-IF.
		if (!pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: EXEC CICS FREEMAIN
			//               DATA(DFHCOMMAREA)
			//               RESP(WS-RESPONSE-CODE)
			//               RESP2(WS-RESPONSE-CODE2)
			//           END-EXEC
			cicsStorageManager.freemain(execContext, pointerManager.addressOf(dfhcommarea));
			workingStorageArea.setWsResponseCode(execContext.getResp());
			workingStorageArea.setWsResponseCode2(execContext.getResp2());
		}
		//!   RECORD AN ERROR IF ONE OCCURS AND THE MEMORY IS STILL
		//!    ALLOCATED.  IF IT IS NOT ALLOCATED, THEN WE WOULD RUN INTO
		//!    ADDRESSING ISSUES AND MOST LIKELY ABEND.  EITHER WAY THOUGH,
		//!    THE ERROR WILL NOT GET BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL
				&& !pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-4'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-4", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
	}

	/**Original name: 8100-OUTPUT-PROXY-COMMON-CTA<br>
	 * <pre>***************************************************************
	 *  SINCE THE PROXY COMMON CONTRACT SHOULD NOT BE UPDATED ANY    *
	 *  FURTHER, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW    *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************
	 *     IT IS POSSIBLE TO GET INTO THIS PARAGRAPH WHEN USING A METHOD
	 *      OTHER THAN CHANNELS.  BECAUSE OF THIS, WE WILL MAKE SURE WE
	 *      ARE UTILIZING A CHANNEL BEFORE PLACING THE COPYBOOK INTO A
	 *      CONTAINER.</pre>*/
	private void outputProxyCommonCta() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT EIBCALEN = +0
		//               GO TO 8100-EXIT
		//           END-IF.
		if (!(execContext.getCommAreaLen() == 0)) {
			// COB_CODE: GO TO 8100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-PC-OUTPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsProxyContractAtb().getOutputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-PROXY-CBK-OUP-NM)
		//               FROM       (L-CONTRACT-LOCATION (1: WS-PC-OUTPUT-LEN))
		//               FLENGTH    (WS-PC-OUTPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsProxyContractAtb().getOutputLen());
		tsOutputData.setData(lContractLocation.getlContractLocationAsBuffer());
		Channel.write(execContext, "", constantFields.getCopybookNames().getProxyCbkOupNmFormatted(), tsOutputData);
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		//!   RECORD AN ERROR IF ONE OCCURS, BUT THE ERROR WILL NOT GET
		//!   BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8100-OUTPUT-PROXY-COMMON'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8100-OUTPUT-PROXY-COMMON", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 8100-EXIT
			return;
		}
	}

	/**Original name: 12514-SET-SVC-CTT-ATB<br>
	 * <pre>***************************************************************
	 *  SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
	 * ***************************************************************</pre>*/
	private void setSvcCttAtb() {
		// COB_CODE: SET WS-SC-INPUT-PTR         TO ADDRESS OF
		//                                             XZT006-SERVICE-INPUTS.
		workingStorageArea.getWsServiceContractAtb().setInputPtr(pointerManager.addressOf(lServiceContractArea));
		// COB_CODE: SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
		//                                             XZT006-SERVICE-OUTPUTS.
		workingStorageArea.getWsServiceContractAtb()
				.setOutputPtr(pointerManager.addressOf(lServiceContractArea, LServiceContractAreaXz0x0006.Pos.XZT006_SERVICE_OUTPUTS));
		// COB_CODE: MOVE LENGTH OF XZT006-SERVICE-INPUTS
		//                                       TO WS-SC-INPUT-LEN.
		workingStorageArea.getWsServiceContractAtb().setInputLen(LServiceContractAreaXz0x0006.Len.XZT006_SERVICE_INPUTS);
		// COB_CODE: MOVE LENGTH OF XZT006-SERVICE-OUTPUTS
		//                                       TO WS-SC-OUTPUT-LEN.
		workingStorageArea.getWsServiceContractAtb().setOutputLen(LServiceContractAreaXz0x0006.Len.XZT006_SERVICE_OUTPUTS);
	}

	/**Original name: 13100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
	 *  INPUT AREA                                                   *
	 * ***************************************************************</pre>*/
	private void setUpInput1() {
		// COB_CODE: MOVE PPC-OPERATION          TO WS-OPERATION-NAME.
		workingStorageArea.getWsOperationName().setWsOperationName(dfhcommarea.getPpcOperation());
		// IF A USERID WAS SUPPLIED, PASS IT TO THE FRAMEWORK
		// COB_CODE: IF XZT06I-USERID NOT = SPACES
		//             AND
		//              XZT06I-USERID NOT = LOW-VALUES
		//               MOVE XZT06I-USERID      TO CSC-AUTH-USERID
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getXzt06iUserid())
				&& !Characters.EQ_LOW.test(lServiceContractArea.getXzt06iUseridFormatted())) {
			// COB_CODE: MOVE XZT06I-USERID      TO CSC-AUTH-USERID
			mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setAuthUserid(lServiceContractArea.getXzt06iUserid());
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN WS-ADD-ACT-NOT
		//                   MOVE 'K'            TO XZC001Q-NOT-PRC-TS-CI
		//               WHEN WS-UPDATE-ACT-NOT
		//                                       TO XZC001Q-ACT-NOT-CSUM
		//               WHEN WS-DELETE-ACT-NOT
		//                   GO TO 13100-EXIT
		//           END-EVALUATE.
		switch (workingStorageArea.getWsOperationName().getWsOperationName()) {

		case WsOperationNameXz0x0006.ADD_ACT_NOT:// COB_CODE: MOVE XZT06I-CSR-ACT-NBR
			//                               TO XZC001Q-CSR-ACT-NBR
			lFrameworkRequestArea.setXzc001qCsrActNbr(lServiceContractArea.getXzt06iCsrActNbr());
			// move spaces to KCRE since the key is passed by the front-end
			// COB_CODE: MOVE SPACES         TO XZC001Q-CSR-ACT-NBR-KCRE
			lFrameworkRequestArea.setXzc001qCsrActNbrKcre("");
			// COB_CODE: MOVE 'K'            TO XZC001Q-CSR-ACT-NBR-CI
			lFrameworkRequestArea.setXzc001qCsrActNbrCiFormatted("K");
			// COB_CODE: MOVE SPACES         TO XZC001Q-NOT-PRC-TS
			lFrameworkRequestArea.setXzc001qNotPrcTs("");
			// move field name to KCRE since the key is created by the BDO
			// COB_CODE: MOVE 'XZC001-NOT-PRC-TS'
			//                               TO XZC001Q-NOT-PRC-TS-KCRE
			lFrameworkRequestArea.setXzc001qNotPrcTsKcre("XZC001-NOT-PRC-TS");
			// COB_CODE: MOVE 'K'            TO XZC001Q-NOT-PRC-TS-CI
			lFrameworkRequestArea.setXzc001qNotPrcTsCiFormatted("K");
			break;

		case WsOperationNameXz0x0006.UPDATE_ACT_NOT:// COB_CODE: MOVE XZT06I-CSR-ACT-NBR
			//                               TO XZC001Q-CSR-ACT-NBR
			lFrameworkRequestArea.setXzc001qCsrActNbr(lServiceContractArea.getXzt06iCsrActNbr());
			// COB_CODE: MOVE XZT06I-TK-NOT-PRC-TS
			//                               TO XZC001Q-NOT-PRC-TS
			lFrameworkRequestArea.setXzc001qNotPrcTs(lServiceContractArea.getXzt06iTkNotPrcTs());
			// COB_CODE: MOVE XZT06I-TK-ACT-NOT-CSUM
			//                               TO XZC001Q-ACT-NOT-CSUM
			lFrameworkRequestArea.setXzc001qActNotCsumFormatted(lServiceContractArea.getXzt06iTkActNotCsumFormatted());
			break;

		case WsOperationNameXz0x0006.DELETE_ACT_NOT:// COB_CODE: MOVE XZT06I-CSR-ACT-NBR
			//                               TO XZC001Q-CSR-ACT-NBR
			lFrameworkRequestArea.setXzc001qCsrActNbr(lServiceContractArea.getXzt06iCsrActNbr());
			// COB_CODE: MOVE XZT06I-TK-NOT-PRC-TS
			//                               TO XZC001Q-NOT-PRC-TS
			lFrameworkRequestArea.setXzc001qNotPrcTs(lServiceContractArea.getXzt06iTkNotPrcTs());
			// COB_CODE: MOVE XZT06I-TK-ACT-NOT-CSUM
			//                               TO XZC001Q-ACT-NOT-CSUM
			lFrameworkRequestArea.setXzc001qActNotCsumFormatted(lServiceContractArea.getXzt06iTkActNotCsumFormatted());
			// COB_CODE: GO TO 13100-EXIT
			return;

		default:
			break;
		}
		// code common between add and update
		// COB_CODE: MOVE XZT06I-ACT-NOT-TYP-CD  TO XZC001Q-ACT-NOT-TYP-CD.
		lFrameworkRequestArea.setXzc001qActNotTypCd(lServiceContractArea.getXzt06iActNotTypCd());
		// COB_CODE: MOVE XZT06I-NOT-DT          TO XZC001Q-NOT-DT.
		lFrameworkRequestArea.setXzc001qNotDt(lServiceContractArea.getXzt06iNotDt());
		// COB_CODE: MOVE XZT06I-TK-ACT-OWN-CLT-ID
		//                                       TO XZC001Q-ACT-OWN-CLT-ID.
		lFrameworkRequestArea.setXzc001qActOwnCltId(lServiceContractArea.getXzt06iTkActOwnCltId());
		// COB_CODE: MOVE XZT06I-TK-ACT-OWN-ADR-ID
		//                                       TO XZC001Q-ACT-OWN-ADR-ID.
		lFrameworkRequestArea.setXzc001qActOwnAdrId(lServiceContractArea.getXzt06iTkActOwnAdrId());
		// COB_CODE: MOVE XZT06I-TK-EMP-ID       TO XZC001Q-EMP-ID.
		lFrameworkRequestArea.setXzc001qEmpId(lServiceContractArea.getXzt06iTkEmpId());
		// COB_CODE: IF XZT06I-TK-EMP-ID NOT = SPACES
		//               MOVE CF-COL-IS-NOT-NULL TO XZC001Q-EMP-ID-NI
		//           ELSE
		//               MOVE CF-COL-IS-NULL     TO XZC001Q-EMP-ID-NI
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getXzt06iTkEmpId())) {
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-EMP-ID-NI
			lFrameworkRequestArea.setXzc001qEmpIdNi(constantFields.getColIsNotNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NULL     TO XZC001Q-EMP-ID-NI
			lFrameworkRequestArea.setXzc001qEmpIdNi(constantFields.getColIsNull());
		}
		// COB_CODE: MOVE XZT06I-TK-STA-MDF-TS   TO XZC001Q-STA-MDF-TS.
		lFrameworkRequestArea.setXzc001qStaMdfTs(lServiceContractArea.getXzt06iTkStaMdfTs());
		// COB_CODE: MOVE XZT06I-TK-ACT-NOT-STA-CD
		//                                       TO XZC001Q-ACT-NOT-STA-CD.
		lFrameworkRequestArea.setXzc001qActNotStaCd(lServiceContractArea.getXzt06iTkActNotStaCd());
		// COB_CODE: MOVE XZT06I-PDC-NBR         TO XZC001Q-PDC-NBR.
		lFrameworkRequestArea.setXzc001qPdcNbr(lServiceContractArea.getXzt06iPdcNbr());
		// COB_CODE: IF XZT06I-PDC-NBR NOT = SPACES
		//               MOVE CF-COL-IS-NOT-NULL TO XZC001Q-PDC-NBR-NI
		//           ELSE
		//               MOVE CF-COL-IS-NULL     TO XZC001Q-PDC-NBR-NI
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getXzt06iPdcNbr())) {
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-PDC-NBR-NI
			lFrameworkRequestArea.setXzc001qPdcNbrNi(constantFields.getColIsNotNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NULL     TO XZC001Q-PDC-NBR-NI
			lFrameworkRequestArea.setXzc001qPdcNbrNi(constantFields.getColIsNull());
		}
		// COB_CODE: MOVE XZT06I-PDC-NM          TO XZC001Q-PDC-NM.
		lFrameworkRequestArea.setXzc001qPdcNm(lServiceContractArea.getXzt06iPdcNm());
		// COB_CODE: IF XZT06I-PDC-NM NOT = SPACES
		//               MOVE CF-COL-IS-NOT-NULL TO XZC001Q-PDC-NM-NI
		//           ELSE
		//               MOVE CF-COL-IS-NULL     TO XZC001Q-PDC-NM-NI
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getXzt06iPdcNm())) {
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-PDC-NM-NI
			lFrameworkRequestArea.setXzc001qPdcNmNi(constantFields.getColIsNotNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NULL     TO XZC001Q-PDC-NM-NI
			lFrameworkRequestArea.setXzc001qPdcNmNi(constantFields.getColIsNull());
		}
		// COB_CODE: MOVE XZT06I-TK-SEG-CD       TO XZC001Q-SEG-CD.
		lFrameworkRequestArea.setXzc001qSegCd(lServiceContractArea.getXzt06iTkSegCd());
		// COB_CODE: IF XZT06I-TK-SEG-CD NOT = SPACES
		//               MOVE CF-COL-IS-NOT-NULL TO XZC001Q-SEG-CD-NI
		//           ELSE
		//               MOVE CF-COL-IS-NULL     TO XZC001Q-SEG-CD-NI
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getXzt06iTkSegCd())) {
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-SEG-CD-NI
			lFrameworkRequestArea.setXzc001qSegCdNi(constantFields.getColIsNotNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NULL     TO XZC001Q-SEG-CD-NI
			lFrameworkRequestArea.setXzc001qSegCdNi(constantFields.getColIsNull());
		}
		// COB_CODE: MOVE XZT06I-TK-ACT-TYP-CD   TO XZC001Q-ACT-TYP-CD.
		lFrameworkRequestArea.setXzc001qActTypCd(lServiceContractArea.getXzt06iTkActTypCd());
		// COB_CODE: IF XZT06I-TK-ACT-TYP-CD NOT = SPACES
		//               MOVE CF-COL-IS-NOT-NULL TO XZC001Q-ACT-TYP-CD-NI
		//           ELSE
		//               MOVE CF-COL-IS-NULL     TO XZC001Q-ACT-TYP-CD-NI
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getXzt06iTkActTypCd())) {
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-ACT-TYP-CD-NI
			lFrameworkRequestArea.setXzc001qActTypCdNi(constantFields.getColIsNotNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NULL     TO XZC001Q-ACT-TYP-CD-NI
			lFrameworkRequestArea.setXzc001qActTypCdNi(constantFields.getColIsNull());
		}
		// COB_CODE: MOVE XZT06I-TOT-FEE-AMT     TO XZC001Q-TOT-FEE-AMT.
		lFrameworkRequestArea.setXzc001qTotFeeAmt(TruncAbs.toDecimal(lServiceContractArea.getXzt06iTotFeeAmt(), 10, 2));
		// COB_CODE: IF XZT06I-TOT-FEE-AMT > +0
		//               MOVE CF-COL-IS-NOT-NULL TO XZC001Q-TOT-FEE-AMT-NI
		//           ELSE
		//               END-IF
		//           END-IF.
		if (lServiceContractArea.getXzt06iTotFeeAmt().compareTo(0) > 0) {
			// COB_CODE: MOVE CF-POSITIVE        TO XZC001Q-TOT-FEE-AMT-SIGN
			lFrameworkRequestArea.setXzc001qTotFeeAmtSign(constantFields.getPositive());
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-TOT-FEE-AMT-NI
			lFrameworkRequestArea.setXzc001qTotFeeAmtNi(constantFields.getColIsNotNull());
		} else if (lServiceContractArea.getXzt06iTotFeeAmt().compareTo(0) < 0) {
			// COB_CODE: IF XZT06I-TOT-FEE-AMT < +0
			//                                   TO XZC001Q-TOT-FEE-AMT-NI
			//           ELSE
			//               MOVE CF-COL-IS-NULL TO XZC001Q-TOT-FEE-AMT-NI
			//           END-IF
			// COB_CODE: MOVE CF-NEGATIVE    TO XZC001Q-TOT-FEE-AMT-SIGN
			lFrameworkRequestArea.setXzc001qTotFeeAmtSign(constantFields.getNegative());
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL
			//                               TO XZC001Q-TOT-FEE-AMT-NI
			lFrameworkRequestArea.setXzc001qTotFeeAmtNi(constantFields.getColIsNotNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NULL TO XZC001Q-TOT-FEE-AMT-NI
			lFrameworkRequestArea.setXzc001qTotFeeAmtNi(constantFields.getColIsNull());
		}
		// COB_CODE: MOVE XZT06I-ST-ABB          TO XZC001Q-ST-ABB.
		lFrameworkRequestArea.setXzc001qStAbb(lServiceContractArea.getXzt06iStAbb());
		// COB_CODE: IF XZT06I-ST-ABB NOT = SPACES
		//               MOVE CF-COL-IS-NOT-NULL TO XZC001Q-ST-ABB-NI
		//           ELSE
		//               MOVE CF-COL-IS-NULL     TO XZC001Q-ST-ABB-NI
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getXzt06iStAbb())) {
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-ST-ABB-NI
			lFrameworkRequestArea.setXzc001qStAbbNi(constantFields.getColIsNotNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NULL     TO XZC001Q-ST-ABB-NI
			lFrameworkRequestArea.setXzc001qStAbbNi(constantFields.getColIsNull());
		}
		// COB_CODE: MOVE XZT06I-CER-HLD-NOT-IND TO XZC001Q-CER-HLD-NOT-IND.
		lFrameworkRequestArea.setXzc001qCerHldNotInd(lServiceContractArea.getXzt06iCerHldNotInd());
		// COB_CODE: IF XZT06I-CER-HLD-NOT-IND NOT = SPACES
		//               MOVE CF-COL-IS-NOT-NULL TO XZC001Q-CER-HLD-NOT-IND-NI
		//           ELSE
		//               MOVE CF-COL-IS-NULL     TO XZC001Q-CER-HLD-NOT-IND-NI
		//           END-IF.
		if (!Conditions.eq(lServiceContractArea.getXzt06iCerHldNotInd(), Types.SPACE_CHAR)) {
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-CER-HLD-NOT-IND-NI
			lFrameworkRequestArea.setXzc001qCerHldNotIndNi(constantFields.getColIsNotNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NULL     TO XZC001Q-CER-HLD-NOT-IND-NI
			lFrameworkRequestArea.setXzc001qCerHldNotIndNi(constantFields.getColIsNull());
		}
		// COB_CODE: MOVE XZT06I-ADD-CNC-DAY     TO XZC001Q-ADD-CNC-DAY.
		lFrameworkRequestArea.setXzc001qAddCncDayFormatted(Trunc.removeSign(lServiceContractArea.getXzt06iAddCncDayFormatted()));
		// COB_CODE: IF XZT06I-ADD-CNC-DAY  = +0
		//               MOVE CF-COL-IS-NULL     TO XZC001Q-ADD-CNC-DAY-NI
		//           ELSE
		//               END-IF
		//           END-IF.
		if (lServiceContractArea.getXzt06iAddCncDay() == 0) {
			// COB_CODE: MOVE CF-POSITIVE        TO XZC001Q-ADD-CNC-DAY-SIGN
			lFrameworkRequestArea.setXzc001qAddCncDaySign(constantFields.getPositive());
			// COB_CODE: MOVE CF-COL-IS-NULL     TO XZC001Q-ADD-CNC-DAY-NI
			lFrameworkRequestArea.setXzc001qAddCncDayNi(constantFields.getColIsNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-ADD-CNC-DAY-NI
			lFrameworkRequestArea.setXzc001qAddCncDayNi(constantFields.getColIsNotNull());
			// COB_CODE: IF XZT06I-ADD-CNC-DAY > +0
			//               MOVE CF-POSITIVE    TO XZC001Q-ADD-CNC-DAY-SIGN
			//           ELSE
			//               MOVE CF-NEGATIVE    TO XZC001Q-ADD-CNC-DAY-SIGN
			//           END-IF
			if (lServiceContractArea.getXzt06iAddCncDay() > 0) {
				// COB_CODE: MOVE CF-POSITIVE    TO XZC001Q-ADD-CNC-DAY-SIGN
				lFrameworkRequestArea.setXzc001qAddCncDaySign(constantFields.getPositive());
			} else {
				// COB_CODE: MOVE CF-NEGATIVE    TO XZC001Q-ADD-CNC-DAY-SIGN
				lFrameworkRequestArea.setXzc001qAddCncDaySign(constantFields.getNegative());
			}
		}
		// COB_CODE: MOVE XZT06I-REA-DES         TO XZC001Q-REA-DES.
		lFrameworkRequestArea.setXzc001qReaDes(lServiceContractArea.getXzt06iReaDes());
		// COB_CODE: IF XZT06I-REA-DES NOT = SPACES
		//               MOVE CF-COL-IS-NOT-NULL TO XZC001Q-REA-DES-NI
		//           ELSE
		//               MOVE CF-COL-IS-NULL     TO XZC001Q-REA-DES-NI
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getXzt06iReaDes())) {
			// COB_CODE: MOVE CF-COL-IS-NOT-NULL TO XZC001Q-REA-DES-NI
			lFrameworkRequestArea.setXzc001qReaDesNi(constantFields.getColIsNotNull());
		} else {
			// COB_CODE: MOVE CF-COL-IS-NULL     TO XZC001Q-REA-DES-NI
			lFrameworkRequestArea.setXzc001qReaDesNi(constantFields.getColIsNull());
		}
	}

	/**Original name: 13200-SET-UP-OUTPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
	 *  RESPONSE AREA.                                               *
	 * ***************************************************************</pre>*/
	private void setUpOutput1() {
		// COB_CODE: IF WS-DELETE-ACT-NOT
		//               GO TO 13200-EXIT
		//           END-IF.
		if (workingStorageArea.getWsOperationName().isDeleteActNot()) {
			// COB_CODE: GO TO 13200-EXIT
			return;
		}
		// COB_CODE: MOVE XZC001R-CSR-ACT-NBR    TO XZT06O-CSR-ACT-NBR.
		lServiceContractArea.setXzt06oCsrActNbr(lFrameworkResponseArea.getXzc001qCsrActNbr());
		// COB_CODE: MOVE XZC001R-NOT-PRC-TS     TO XZT06O-TK-NOT-PRC-TS.
		lServiceContractArea.setXzt06oTkNotPrcTs(lFrameworkResponseArea.getXzc001qNotPrcTs());
		// COB_CODE: MOVE XZC001R-ACT-NOT-TYP-CD TO XZT06O-ACT-NOT-TYP-CD.
		lServiceContractArea.setXzt06oActNotTypCd(lFrameworkResponseArea.getXzc001qActNotTypCd());
		// COB_CODE: MOVE XZC001R-ACT-NOT-TYP-DESC
		//                                       TO XZT06O-ACT-NOT-TYP-DESC.
		lServiceContractArea.setXzt06oActNotTypDesc(lFrameworkResponseArea.getXzc001qActNotTypDesc());
		// COB_CODE: MOVE XZC001R-NOT-DT         TO XZT06O-NOT-DT.
		lServiceContractArea.setXzt06oNotDt(lFrameworkResponseArea.getXzc001qNotDt());
		// COB_CODE: MOVE XZC001R-ACT-OWN-CLT-ID TO XZT06O-TK-ACT-OWN-CLT-ID.
		lServiceContractArea.setXzt06oTkActOwnCltId(lFrameworkResponseArea.getXzc001qActOwnCltId());
		// COB_CODE: MOVE XZC001R-ACT-OWN-ADR-ID TO XZT06O-TK-ACT-OWN-ADR-ID.
		lServiceContractArea.setXzt06oTkActOwnAdrId(lFrameworkResponseArea.getXzc001qActOwnAdrId());
		// COB_CODE: MOVE XZC001R-EMP-ID         TO XZT06O-TK-EMP-ID.
		lServiceContractArea.setXzt06oTkEmpId(lFrameworkResponseArea.getXzc001qEmpId());
		// COB_CODE: MOVE XZC001R-STA-MDF-TS     TO XZT06O-TK-STA-MDF-TS.
		lServiceContractArea.setXzt06oTkStaMdfTs(lFrameworkResponseArea.getXzc001qStaMdfTs());
		// COB_CODE: MOVE XZC001R-ACT-NOT-STA-CD TO XZT06O-TK-ACT-NOT-STA-CD.
		lServiceContractArea.setXzt06oTkActNotStaCd(lFrameworkResponseArea.getXzc001qActNotStaCd());
		// COB_CODE: MOVE XZC001R-PDC-NBR        TO XZT06O-PDC-NBR.
		lServiceContractArea.setXzt06oPdcNbr(lFrameworkResponseArea.getXzc001qPdcNbr());
		// COB_CODE: MOVE XZC001R-PDC-NM         TO XZT06O-PDC-NM.
		lServiceContractArea.setXzt06oPdcNm(lFrameworkResponseArea.getXzc001qPdcNm());
		// COB_CODE: MOVE XZC001R-SEG-CD         TO XZT06O-TK-SEG-CD.
		lServiceContractArea.setXzt06oTkSegCd(lFrameworkResponseArea.getXzc001qSegCd());
		// COB_CODE: MOVE XZC001R-ACT-TYP-CD     TO XZT06O-TK-ACT-TYP-CD.
		lServiceContractArea.setXzt06oTkActTypCd(lFrameworkResponseArea.getXzc001qActTypCd());
		// COB_CODE: MOVE XZC001R-TOT-FEE-AMT    TO XZT06O-TOT-FEE-AMT.
		lServiceContractArea.setXzt06oTotFeeAmt(Trunc.toDecimal(lFrameworkResponseArea.getXzc001qTotFeeAmt(), 10, 2));
		// COB_CODE: IF XZC001R-TOT-FEE-AMT-SIGN = CF-NEGATIVE
		//                                          * CF-NEGATIVE-ONE
		//           END-IF.
		if (lFrameworkResponseArea.getXzc001qTotFeeAmtSign() == constantFields.getNegative()) {
			// COB_CODE: COMPUTE XZT06O-TOT-FEE-AMT = XZT06O-TOT-FEE-AMT
			//                                      * CF-NEGATIVE-ONE
			lServiceContractArea
					.setXzt06oTotFeeAmt(Trunc.toDecimal(lServiceContractArea.getXzt06oTotFeeAmt().multiply(constantFields.getNegativeOne()), 10, 2));
		}
		// COB_CODE: MOVE XZC001R-ST-ABB         TO XZT06O-ST-ABB.
		lServiceContractArea.setXzt06oStAbb(lFrameworkResponseArea.getXzc001qStAbb());
		// COB_CODE: MOVE XZC001R-CER-HLD-NOT-IND
		//                                       TO XZT06O-CER-HLD-NOT-IND.
		lServiceContractArea.setXzt06oCerHldNotInd(lFrameworkResponseArea.getXzc001qCerHldNotInd());
		// COB_CODE: MOVE XZC001R-ADD-CNC-DAY    TO XZT06O-ADD-CNC-DAY.
		lServiceContractArea.setXzt06oAddCncDayFormatted(Trunc.addPlusSign(lFrameworkResponseArea.getXzc001rAddCncDayFormatted()));
		// COB_CODE: IF XZC001R-ADD-CNC-DAY-SIGN = CF-NEGATIVE
		//                       CF-NEGATIVE-ONE
		//           END-IF.
		if (lFrameworkResponseArea.getXzc001qAddCncDaySign() == constantFields.getNegative()) {
			// COB_CODE: COMPUTE XZT06O-ADD-CNC-DAY =
			//                   XZT06O-ADD-CNC-DAY *
			//                   CF-NEGATIVE-ONE
			lServiceContractArea.setXzt06oAddCncDay(Trunc.toInt(lServiceContractArea.getXzt06oAddCncDay() * constantFields.getNegativeOne(), 5));
		}
		// COB_CODE: MOVE XZC001R-REA-DES        TO XZT06O-REA-DES.
		lServiceContractArea.setXzt06oReaDes(lFrameworkResponseArea.getXzc001qReaDes());
		// COB_CODE: MOVE XZC001R-ACT-NOT-CSUM   TO XZT06O-TK-ACT-NOT-CSUM.
		lServiceContractArea.setXzt06oTkActNotCsumFormatted(lFrameworkResponseArea.getXzc001rActNotCsumFormatted());
	}

	public void initDsdErrorHandlingParms() {
		mainDriverData.getDriverSpecificData().getErrorReturnCode().setErrorReturnCodeFormatted("0000");
		mainDriverData.getDriverSpecificData().setFatalErrorMessage("");
		mainDriverData.getDriverSpecificData().setNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			mainDriverData.getDriverSpecificData().getNonLoggableErrors(idx0).setDsdNonLogErrMsg("");
		}
		mainDriverData.getDriverSpecificData().setWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.WARNINGS_MAXOCCURS; idx0++) {
			mainDriverData.getDriverSpecificData().getWarnings(idx0).setDsdWarnMsg("");
		}
	}

	public void initPpcErrorHandlingParms() {
		dfhcommarea.setPpcErrorReturnCodeFormatted("0000");
		dfhcommarea.setPpcFatalErrorMessage("");
		dfhcommarea.setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcNonLogErrMsg(idx0, "");
		}
		dfhcommarea.setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcWarnMsg(idx0, "");
		}
	}

	public void initMemoryAllocationParms() {
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(0);
		mainDriverData.getCommunicationShellCommon().setMaOutputDataSize(0);
		mainDriverData.getCommunicationShellCommon().getMaReturnCode().setMaReturnCodeFormatted("0000");
		mainDriverData.getCommunicationShellCommon().setMaErrorMessage("");
	}

	public void initLFrameworkRequestArea() {
		lFrameworkRequestArea.setXzc001qActNotCsumFormatted("000000000");
		lFrameworkRequestArea.setXzc001qCsrActNbrKcre("");
		lFrameworkRequestArea.setXzc001qNotPrcTsKcre("");
		lFrameworkRequestArea.setXzc001qTransProcessDt("");
		lFrameworkRequestArea.setXzc001qCsrActNbr("");
		lFrameworkRequestArea.setXzc001qNotPrcTs("");
		lFrameworkRequestArea.setXzc001qCsrActNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qNotPrcTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qActNotTypCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qActNotTypCd("");
		lFrameworkRequestArea.setXzc001qNotDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qNotDt("");
		lFrameworkRequestArea.setXzc001qActOwnCltIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qActOwnCltId("");
		lFrameworkRequestArea.setXzc001qActOwnAdrIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qActOwnAdrId("");
		lFrameworkRequestArea.setXzc001qEmpIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qEmpIdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qEmpId("");
		lFrameworkRequestArea.setXzc001qStaMdfTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qStaMdfTs("");
		lFrameworkRequestArea.setXzc001qActNotStaCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qActNotStaCd("");
		lFrameworkRequestArea.setXzc001qPdcNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qPdcNbrNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qPdcNbr("");
		lFrameworkRequestArea.setXzc001qPdcNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qPdcNmNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qPdcNm("");
		lFrameworkRequestArea.setXzc001qSegCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qSegCdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qSegCd("");
		lFrameworkRequestArea.setXzc001qActTypCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qActTypCdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qActTypCd("");
		lFrameworkRequestArea.setXzc001qTotFeeAmtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qTotFeeAmtNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qTotFeeAmtSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qTotFeeAmt(new AfDecimal(0, 10, 2));
		lFrameworkRequestArea.setXzc001qStAbbCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qStAbbNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qStAbb("");
		lFrameworkRequestArea.setXzc001qCerHldNotIndCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qCerHldNotIndNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qCerHldNotInd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qAddCncDayCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qAddCncDayNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qAddCncDaySign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qAddCncDayFormatted("00000");
		lFrameworkRequestArea.setXzc001qReaDesCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qReaDesNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc001qReaDes("");
		lFrameworkRequestArea.setXzc001qActNotTypDesc("");
	}

	public void initLFrameworkResponseArea() {
		lFrameworkResponseArea.setXzc001qActNotCsumFormatted("000000000");
		lFrameworkResponseArea.setXzc001qCsrActNbrKcre("");
		lFrameworkResponseArea.setXzc001qNotPrcTsKcre("");
		lFrameworkResponseArea.setXzc001qTransProcessDt("");
		lFrameworkResponseArea.setXzc001qCsrActNbr("");
		lFrameworkResponseArea.setXzc001qNotPrcTs("");
		lFrameworkResponseArea.setXzc001qCsrActNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qNotPrcTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qActNotTypCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qActNotTypCd("");
		lFrameworkResponseArea.setXzc001qNotDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qNotDt("");
		lFrameworkResponseArea.setXzc001qActOwnCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qActOwnCltId("");
		lFrameworkResponseArea.setXzc001qActOwnAdrIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qActOwnAdrId("");
		lFrameworkResponseArea.setXzc001qEmpIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qEmpIdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qEmpId("");
		lFrameworkResponseArea.setXzc001qStaMdfTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qStaMdfTs("");
		lFrameworkResponseArea.setXzc001qActNotStaCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qActNotStaCd("");
		lFrameworkResponseArea.setXzc001qPdcNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qPdcNbrNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qPdcNbr("");
		lFrameworkResponseArea.setXzc001qPdcNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qPdcNmNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qPdcNm("");
		lFrameworkResponseArea.setXzc001qSegCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qSegCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qSegCd("");
		lFrameworkResponseArea.setXzc001qActTypCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qActTypCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qActTypCd("");
		lFrameworkResponseArea.setXzc001qTotFeeAmtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qTotFeeAmtNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qTotFeeAmtSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qTotFeeAmt(new AfDecimal(0, 10, 2));
		lFrameworkResponseArea.setXzc001qStAbbCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qStAbbNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qStAbb("");
		lFrameworkResponseArea.setXzc001qCerHldNotIndCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qCerHldNotIndNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qCerHldNotInd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qAddCncDayCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qAddCncDayNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qAddCncDaySign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qAddCncDayFormatted("00000");
		lFrameworkResponseArea.setXzc001qReaDesCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qReaDesNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc001qReaDes("");
		lFrameworkResponseArea.setXzc001qActNotTypDesc("");
	}

	public void initDfhcommarea() {
		dfhcommarea.setPpcServiceDataSize(0);
		dfhcommarea.setPpcBypassSyncpointMdrvInd(Types.SPACE_CHAR);
		dfhcommarea.setPpcOperation("");
		dfhcommarea.setPpcErrorReturnCodeFormatted("0000");
		dfhcommarea.setPpcFatalErrorMessage("");
		dfhcommarea.setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcNonLogErrMsg(idx0, "");
		}
		dfhcommarea.setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcWarnMsg(idx0, "");
		}
	}

	public void initLServiceContractArea() {
		lServiceContractArea.setXzt06iTkNotPrcTs("");
		lServiceContractArea.setXzt06iTkActOwnCltId("");
		lServiceContractArea.setXzt06iTkActOwnAdrId("");
		lServiceContractArea.setXzt06iTkEmpId("");
		lServiceContractArea.setXzt06iTkStaMdfTs("");
		lServiceContractArea.setXzt06iTkActNotStaCd("");
		lServiceContractArea.setXzt06iTkSegCd("");
		lServiceContractArea.setXzt06iTkActTypCd("");
		lServiceContractArea.setXzt06iTkActNotCsumFormatted("000000000");
		lServiceContractArea.setXzt06iCsrActNbr("");
		lServiceContractArea.setXzt06iActNotTypCd("");
		lServiceContractArea.setXzt06iNotDt("");
		lServiceContractArea.setXzt06iPdcNbr("");
		lServiceContractArea.setXzt06iPdcNm("");
		lServiceContractArea.setXzt06iTotFeeAmt(new AfDecimal(0, 10, 2));
		lServiceContractArea.setXzt06iStAbb("");
		lServiceContractArea.setXzt06iCerHldNotInd(Types.SPACE_CHAR);
		lServiceContractArea.setXzt06iAddCncDay(0);
		lServiceContractArea.setXzt06iReaDes("");
		lServiceContractArea.setXzt06iUserid("");
		lServiceContractArea.setXzt06oTkNotPrcTs("");
		lServiceContractArea.setXzt06oTkActOwnCltId("");
		lServiceContractArea.setXzt06oTkActOwnAdrId("");
		lServiceContractArea.setXzt06oTkEmpId("");
		lServiceContractArea.setXzt06oTkStaMdfTs("");
		lServiceContractArea.setXzt06oTkActNotStaCd("");
		lServiceContractArea.setXzt06oTkSegCd("");
		lServiceContractArea.setXzt06oTkActTypCd("");
		lServiceContractArea.setXzt06oTkActNotCsumFormatted("000000000");
		lServiceContractArea.setXzt06oCsrActNbr("");
		lServiceContractArea.setXzt06oActNotTypCd("");
		lServiceContractArea.setXzt06oActNotTypDesc("");
		lServiceContractArea.setXzt06oNotDt("");
		lServiceContractArea.setXzt06oPdcNbr("");
		lServiceContractArea.setXzt06oPdcNm("");
		lServiceContractArea.setXzt06oTotFeeAmt(new AfDecimal(0, 10, 2));
		lServiceContractArea.setXzt06oStAbb("");
		lServiceContractArea.setXzt06oCerHldNotInd(Types.SPACE_CHAR);
		lServiceContractArea.setXzt06oAddCncDay(0);
		lServiceContractArea.setXzt06oReaDes("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
