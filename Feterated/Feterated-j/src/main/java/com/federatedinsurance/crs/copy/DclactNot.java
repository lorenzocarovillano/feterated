/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNot;
import com.federatedinsurance.crs.commons.data.to.IActNotCltDtMgtDtl;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRecAtcTyp;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: DCLACT-NOT<br>
 * Variable: DCLACT-NOT from copybook XZH00002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclactNot implements IActNot, IActNotFrmRecAtcTyp, IActNotCltDtMgtDtl {

	//==== PROPERTIES ====
	//Original name: CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: NOT-DT
	private String notDt = DefaultValues.stringVal(Len.NOT_DT);
	//Original name: ACT-OWN-CLT-ID
	private String actOwnCltId = DefaultValues.stringVal(Len.ACT_OWN_CLT_ID);
	//Original name: ACT-OWN-ADR-ID
	private String actOwnAdrId = DefaultValues.stringVal(Len.ACT_OWN_ADR_ID);
	//Original name: EMP-ID
	private String empId = DefaultValues.stringVal(Len.EMP_ID);
	//Original name: STA-MDF-TS
	private String staMdfTs = DefaultValues.stringVal(Len.STA_MDF_TS);
	//Original name: ACT-NOT-STA-CD
	private String actNotStaCd = DefaultValues.stringVal(Len.ACT_NOT_STA_CD);
	//Original name: PDC-NBR
	private String pdcNbr = DefaultValues.stringVal(Len.PDC_NBR);
	//Original name: PDC-NM-LEN
	private short pdcNmLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: PDC-NM-TEXT
	private String pdcNmText = DefaultValues.stringVal(Len.PDC_NM_TEXT);
	//Original name: SEG-CD
	private String segCd = DefaultValues.stringVal(Len.SEG_CD);
	//Original name: ACT-TYP-CD
	private String actTypCd = DefaultValues.stringVal(Len.ACT_TYP_CD);
	//Original name: TOT-FEE-AMT
	private AfDecimal totFeeAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: ADD-CNC-DAY
	private short addCncDay = DefaultValues.BIN_SHORT_VAL;
	//Original name: REA-DES-LEN
	private short reaDesLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: REA-DES-TEXT
	private String reaDesText = DefaultValues.stringVal(Len.REA_DES_TEXT);
	//Original name: CER-HLD-NOT-IND
	private char cerHldNotInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	@Override
	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getActNotTypCdFormatted() {
		return Functions.padBlanks(getActNotTypCd(), Len.ACT_NOT_TYP_CD);
	}

	@Override
	public void setNotDt(String notDt) {
		this.notDt = Functions.subString(notDt, Len.NOT_DT);
	}

	@Override
	public String getNotDt() {
		return this.notDt;
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		this.actOwnCltId = Functions.subString(actOwnCltId, Len.ACT_OWN_CLT_ID);
	}

	@Override
	public String getActOwnCltId() {
		return this.actOwnCltId;
	}

	@Override
	public void setActOwnAdrId(String actOwnAdrId) {
		this.actOwnAdrId = Functions.subString(actOwnAdrId, Len.ACT_OWN_ADR_ID);
	}

	@Override
	public String getActOwnAdrId() {
		return this.actOwnAdrId;
	}

	@Override
	public void setEmpId(String empId) {
		this.empId = Functions.subString(empId, Len.EMP_ID);
	}

	@Override
	public String getEmpId() {
		return this.empId;
	}

	@Override
	public void setStaMdfTs(String staMdfTs) {
		this.staMdfTs = Functions.subString(staMdfTs, Len.STA_MDF_TS);
	}

	@Override
	public String getStaMdfTs() {
		return this.staMdfTs;
	}

	public String getStaMdfTsFormatted() {
		return Functions.padBlanks(getStaMdfTs(), Len.STA_MDF_TS);
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		this.actNotStaCd = Functions.subString(actNotStaCd, Len.ACT_NOT_STA_CD);
	}

	@Override
	public String getActNotStaCd() {
		return this.actNotStaCd;
	}

	public String getActNotStaCdFormatted() {
		return Functions.padBlanks(getActNotStaCd(), Len.ACT_NOT_STA_CD);
	}

	@Override
	public void setPdcNbr(String pdcNbr) {
		this.pdcNbr = Functions.subString(pdcNbr, Len.PDC_NBR);
	}

	@Override
	public String getPdcNbr() {
		return this.pdcNbr;
	}

	public void setPdcNmLen(short pdcNmLen) {
		this.pdcNmLen = pdcNmLen;
	}

	public short getPdcNmLen() {
		return this.pdcNmLen;
	}

	public void setPdcNmText(String pdcNmText) {
		this.pdcNmText = Functions.subString(pdcNmText, Len.PDC_NM_TEXT);
	}

	public String getPdcNmText() {
		return this.pdcNmText;
	}

	@Override
	public void setSegCd(String segCd) {
		this.segCd = Functions.subString(segCd, Len.SEG_CD);
	}

	@Override
	public String getSegCd() {
		return this.segCd;
	}

	@Override
	public void setActTypCd(String actTypCd) {
		this.actTypCd = Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	@Override
	public String getActTypCd() {
		return this.actTypCd;
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		this.totFeeAmt.assign(totFeeAmt);
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		return this.totFeeAmt.copy();
	}

	@Override
	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	@Override
	public String getStAbb() {
		return this.stAbb;
	}

	@Override
	public void setAddCncDay(short addCncDay) {
		this.addCncDay = addCncDay;
	}

	@Override
	public short getAddCncDay() {
		return this.addCncDay;
	}

	public void setReaDesLen(short reaDesLen) {
		this.reaDesLen = reaDesLen;
	}

	public short getReaDesLen() {
		return this.reaDesLen;
	}

	public void setReaDesText(String reaDesText) {
		this.reaDesText = Functions.subString(reaDesText, Len.REA_DES_TEXT);
	}

	public String getReaDesText() {
		return this.reaDesText;
	}

	@Override
	public void setCerHldNotInd(char cerHldNotInd) {
		this.cerHldNotInd = cerHldNotInd;
	}

	@Override
	public char getCerHldNotInd() {
		return this.cerHldNotInd;
	}

	@Override
	public String getActTypCdObj() {
		return getActTypCd();
	}

	@Override
	public void setActTypCdObj(String actTypCdObj) {
		setActTypCd(actTypCdObj);
	}

	@Override
	public Short getAddCncDayObj() {
		return (getAddCncDay());
	}

	@Override
	public void setAddCncDayObj(Short addCncDayObj) {
		setAddCncDay((addCncDayObj));
	}

	@Override
	public Character getCerHldNotIndObj() {
		return (getCerHldNotInd());
	}

	@Override
	public void setCerHldNotIndObj(Character cerHldNotIndObj) {
		setCerHldNotInd((cerHldNotIndObj));
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	@Override
	public String getNotTypCd() {
		return getActNotTypCd();
	}

	@Override
	public void setNotTypCd(String notTypCd) {
		this.setActNotTypCd(notTypCd);
	}

	@Override
	public String getOwnAdrId() {
		return getActOwnAdrId();
	}

	@Override
	public void setOwnAdrId(String ownAdrId) {
		this.setActOwnAdrId(ownAdrId);
	}

	@Override
	public String getOwnCltId() {
		return getActOwnCltId();
	}

	@Override
	public void setOwnCltId(String ownCltId) {
		this.setActOwnCltId(ownCltId);
	}

	@Override
	public String getPdcNbrObj() {
		return getPdcNbr();
	}

	@Override
	public void setPdcNbrObj(String pdcNbrObj) {
		setPdcNbr(pdcNbrObj);
	}

	@Override
	public String getPdcNm() {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public void setPdcNm(String pdcNm) {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public String getPdcNmObj() {
		return getPdcNm();
	}

	@Override
	public void setPdcNmObj(String pdcNmObj) {
		setPdcNm(pdcNmObj);
	}

	@Override
	public String getReaDes() {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public void setReaDes(String reaDes) {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public String getReaDesObj() {
		return getReaDes();
	}

	@Override
	public void setReaDesObj(String reaDesObj) {
		setReaDes(reaDesObj);
	}

	@Override
	public String getSaNotPrcTsTime() {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	@Override
	public void setSaNotPrcTsTime(String saNotPrcTsTime) {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	@Override
	public String getSegCdObj() {
		return getSegCd();
	}

	@Override
	public void setSegCdObj(String segCdObj) {
		setSegCd(segCdObj);
	}

	@Override
	public String getStAbbObj() {
		return getStAbb();
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		setStAbb(stAbbObj);
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		return getTotFeeAmt().toString();
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int NOT_DT = 10;
		public static final int EMP_ID = 6;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int ACT_OWN_CLT_ID = 64;
		public static final int ACT_OWN_ADR_ID = 64;
		public static final int STA_MDF_TS = 26;
		public static final int ACT_NOT_STA_CD = 2;
		public static final int PDC_NBR = 5;
		public static final int PDC_NM_TEXT = 120;
		public static final int SEG_CD = 3;
		public static final int ACT_TYP_CD = 2;
		public static final int ST_ABB = 2;
		public static final int REA_DES_TEXT = 500;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
