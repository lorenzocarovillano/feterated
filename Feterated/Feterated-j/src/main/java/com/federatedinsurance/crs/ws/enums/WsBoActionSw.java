/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-BO-ACTION-SW<br>
 * Variable: WS-BO-ACTION-SW from program HALOUIEH<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsBoActionSw {

	//==== PROPERTIES ====
	private String value = "0";
	public static final String BYPASS_SE_STEP = "0";
	public static final String SE_AND_PRIM_KEYS = "1";

	//==== METHODS ====
	public void setWsBoActionSw(short wsBoActionSw) {
		this.value = NumericDisplay.asString(wsBoActionSw, Len.WS_BO_ACTION_SW);
	}

	public void setWsBoActionSwFormatted(String wsBoActionSw) {
		this.value = Trunc.toUnsignedNumeric(wsBoActionSw, Len.WS_BO_ACTION_SW);
	}

	public short getWsBoActionSw() {
		return NumericDisplay.asShort(this.value);
	}

	public String getWsBoActionSwFormatted() {
		return this.value;
	}

	public boolean isBypassSeStep() {
		return getWsBoActionSwFormatted().equals(BYPASS_SE_STEP);
	}

	public void setBypassSeStep() {
		setWsBoActionSwFormatted(BYPASS_SE_STEP);
	}

	public void setSeAndPrimKeys() {
		setWsBoActionSwFormatted(SE_AND_PRIM_KEYS);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_BO_ACTION_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
