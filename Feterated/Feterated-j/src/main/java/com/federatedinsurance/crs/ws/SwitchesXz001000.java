/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz001000 {

	//==== PROPERTIES ====
	//Original name: SW-ROW-FOUND-FLAG
	private boolean rowFoundFlag = true;
	//Original name: SW-CREATE-WORKFLOW-FLAG
	private boolean createWorkflowFlag = true;
	//Original name: SW-FIRST-TIME-FLAG
	private boolean firstTimeFlag = true;

	//==== METHODS ====
	public void setRowFoundFlag(boolean rowFoundFlag) {
		this.rowFoundFlag = rowFoundFlag;
	}

	public boolean isRowFoundFlag() {
		return this.rowFoundFlag;
	}

	public void setCreateWorkflowFlag(boolean createWorkflowFlag) {
		this.createWorkflowFlag = createWorkflowFlag;
	}

	public boolean isCreateWorkflowFlag() {
		return this.createWorkflowFlag;
	}

	public void setFirstTimeFlag(boolean firstTimeFlag) {
		this.firstTimeFlag = firstTimeFlag;
	}

	public boolean isFirstTimeFlag() {
		return this.firstTimeFlag;
	}
}
