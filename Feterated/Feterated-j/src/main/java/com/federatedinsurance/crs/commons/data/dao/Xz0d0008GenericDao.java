/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IXz0d0008Generic;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [XZ0D0008_Generic]
 * 
 */
public class Xz0d0008GenericDao extends BaseSqlDao<IXz0d0008Generic> {

	private Cursor actNotPolFrmCsr1;
	private final IRowMapper<IXz0d0008Generic> fetchRecRm = buildNamedRowMapper(IXz0d0008Generic.class, "csrActNbr", "notPrcTs", "polNbr",
			"recSeqNbr");

	public Xz0d0008GenericDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IXz0d0008Generic> getToClass() {
		return IXz0d0008Generic.class;
	}

	public IXz0d0008Generic fetchRec(IXz0d0008Generic iXz0d0008Generic) {
		return fetch(actNotPolFrmCsr1, iXz0d0008Generic, fetchRecRm);
	}
}
