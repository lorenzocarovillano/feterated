/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: UBOC-SEC-AUT-NBR<br>
 * Variable: UBOC-SEC-AUT-NBR from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocSecAutNbr {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.SEC_AUT_NBR);
	public static final String AUT_NBR_UNKNOWN = "00000";
	public static final String SUPER_USER = "00100";

	//==== METHODS ====
	public void setUbocSecAutNbr(int ubocSecAutNbr) {
		this.value = NumericDisplay.asString(ubocSecAutNbr, Len.SEC_AUT_NBR);
	}

	public void setUbocSecAutNbrFormatted(String ubocSecAutNbr) {
		this.value = Trunc.toUnsignedNumeric(ubocSecAutNbr, Len.SEC_AUT_NBR);
	}

	public int getUbocSecAutNbr() {
		return NumericDisplay.asInt(this.value);
	}

	public void setUbocSuperUser() {
		setUbocSecAutNbrFormatted(SUPER_USER);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SEC_AUT_NBR = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
