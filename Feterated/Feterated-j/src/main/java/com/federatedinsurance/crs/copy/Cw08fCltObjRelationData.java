/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CW08F-CLT-OBJ-RELATION-DATA<br>
 * Variable: CW08F-CLT-OBJ-RELATION-DATA from copybook CAWLF008<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw08fCltObjRelationData {

	//==== PROPERTIES ====
	/**Original name: CW08F-CLIENT-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char clientIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: CW08F-RLT-TYP-CD-CI
	private char rltTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-RLT-TYP-CD
	private String rltTypCd = DefaultValues.stringVal(Len.RLT_TYP_CD);
	//Original name: CW08F-OBJ-CD-CI
	private char objCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-OBJ-CD
	private String objCd = DefaultValues.stringVal(Len.OBJ_CD);
	//Original name: CW08F-CIOR-SHW-OBJ-KEY-CI
	private char ciorShwObjKeyCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-CIOR-SHW-OBJ-KEY
	private String ciorShwObjKey = DefaultValues.stringVal(Len.CIOR_SHW_OBJ_KEY);
	//Original name: CW08F-ADR-SEQ-NBR-CI
	private char adrSeqNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-ADR-SEQ-NBR-SIGN
	private char adrSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: CW08F-ADR-SEQ-NBR
	private String adrSeqNbr = DefaultValues.stringVal(Len.ADR_SEQ_NBR);
	//Original name: CW08F-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW08F-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW08F-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW08F-CIOR-EXP-DT-CI
	private char ciorExpDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-CIOR-EXP-DT
	private String ciorExpDt = DefaultValues.stringVal(Len.CIOR_EXP_DT);
	//Original name: CW08F-CIOR-EFF-ACY-TS-CI
	private char ciorEffAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-CIOR-EFF-ACY-TS
	private String ciorEffAcyTs = DefaultValues.stringVal(Len.CIOR_EFF_ACY_TS);
	//Original name: CW08F-CIOR-EXP-ACY-TS-CI
	private char ciorExpAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-CIOR-EXP-ACY-TS
	private String ciorExpAcyTs = DefaultValues.stringVal(Len.CIOR_EXP_ACY_TS);
	//Original name: CW08F-APP-TYPE
	private char appType = DefaultValues.CHAR_VAL;
	//Original name: CW08F-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: CW08F-OBJ-DESC
	private String objDesc = DefaultValues.stringVal(Len.OBJ_DESC);

	//==== METHODS ====
	public void setCltObjRelationDataBytes(byte[] buffer, int offset) {
		int position = offset;
		clientIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		clientId = MarshalByte.readString(buffer, position, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		rltTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rltTypCd = MarshalByte.readString(buffer, position, Len.RLT_TYP_CD);
		position += Len.RLT_TYP_CD;
		objCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		objCd = MarshalByte.readString(buffer, position, Len.OBJ_CD);
		position += Len.OBJ_CD;
		ciorShwObjKeyCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorShwObjKey = MarshalByte.readString(buffer, position, Len.CIOR_SHW_OBJ_KEY);
		position += Len.CIOR_SHW_OBJ_KEY;
		adrSeqNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		adrSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		adrSeqNbr = MarshalByte.readFixedString(buffer, position, Len.ADR_SEQ_NBR);
		position += Len.ADR_SEQ_NBR;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		ciorExpDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorExpDt = MarshalByte.readString(buffer, position, Len.CIOR_EXP_DT);
		position += Len.CIOR_EXP_DT;
		ciorEffAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorEffAcyTs = MarshalByte.readString(buffer, position, Len.CIOR_EFF_ACY_TS);
		position += Len.CIOR_EFF_ACY_TS;
		ciorExpAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorExpAcyTs = MarshalByte.readString(buffer, position, Len.CIOR_EXP_ACY_TS);
		position += Len.CIOR_EXP_ACY_TS;
		appType = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		objDesc = MarshalByte.readString(buffer, position, Len.OBJ_DESC);
	}

	public byte[] getCltObjRelationDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, clientIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeChar(buffer, position, rltTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rltTypCd, Len.RLT_TYP_CD);
		position += Len.RLT_TYP_CD;
		MarshalByte.writeChar(buffer, position, objCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, objCd, Len.OBJ_CD);
		position += Len.OBJ_CD;
		MarshalByte.writeChar(buffer, position, ciorShwObjKeyCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorShwObjKey, Len.CIOR_SHW_OBJ_KEY);
		position += Len.CIOR_SHW_OBJ_KEY;
		MarshalByte.writeChar(buffer, position, adrSeqNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, adrSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, adrSeqNbr, Len.ADR_SEQ_NBR);
		position += Len.ADR_SEQ_NBR;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, ciorExpDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorExpDt, Len.CIOR_EXP_DT);
		position += Len.CIOR_EXP_DT;
		MarshalByte.writeChar(buffer, position, ciorEffAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorEffAcyTs, Len.CIOR_EFF_ACY_TS);
		position += Len.CIOR_EFF_ACY_TS;
		MarshalByte.writeChar(buffer, position, ciorExpAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorExpAcyTs, Len.CIOR_EXP_ACY_TS);
		position += Len.CIOR_EXP_ACY_TS;
		MarshalByte.writeChar(buffer, position, appType);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, objDesc, Len.OBJ_DESC);
		return buffer;
	}

	public void setClientIdCi(char clientIdCi) {
		this.clientIdCi = clientIdCi;
	}

	public char getClientIdCi() {
		return this.clientIdCi;
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setRltTypCdCi(char rltTypCdCi) {
		this.rltTypCdCi = rltTypCdCi;
	}

	public char getRltTypCdCi() {
		return this.rltTypCdCi;
	}

	public void setRltTypCd(String rltTypCd) {
		this.rltTypCd = Functions.subString(rltTypCd, Len.RLT_TYP_CD);
	}

	public String getRltTypCd() {
		return this.rltTypCd;
	}

	public void setObjCdCi(char objCdCi) {
		this.objCdCi = objCdCi;
	}

	public char getObjCdCi() {
		return this.objCdCi;
	}

	public void setObjCd(String objCd) {
		this.objCd = Functions.subString(objCd, Len.OBJ_CD);
	}

	public String getObjCd() {
		return this.objCd;
	}

	public void setCiorShwObjKeyCi(char ciorShwObjKeyCi) {
		this.ciorShwObjKeyCi = ciorShwObjKeyCi;
	}

	public char getCiorShwObjKeyCi() {
		return this.ciorShwObjKeyCi;
	}

	public void setCiorShwObjKey(String ciorShwObjKey) {
		this.ciorShwObjKey = Functions.subString(ciorShwObjKey, Len.CIOR_SHW_OBJ_KEY);
	}

	public String getCiorShwObjKey() {
		return this.ciorShwObjKey;
	}

	public void setAdrSeqNbrCi(char adrSeqNbrCi) {
		this.adrSeqNbrCi = adrSeqNbrCi;
	}

	public char getAdrSeqNbrCi() {
		return this.adrSeqNbrCi;
	}

	public void setAdrSeqNbrSign(char adrSeqNbrSign) {
		this.adrSeqNbrSign = adrSeqNbrSign;
	}

	public char getAdrSeqNbrSign() {
		return this.adrSeqNbrSign;
	}

	public void setCw08fAdrSeqNbrFormatted(String cw08fAdrSeqNbr) {
		this.adrSeqNbr = Trunc.toUnsignedNumeric(cw08fAdrSeqNbr, Len.ADR_SEQ_NBR);
	}

	public int getCw08fAdrSeqNbr() {
		return NumericDisplay.asInt(this.adrSeqNbr);
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setCiorExpDtCi(char ciorExpDtCi) {
		this.ciorExpDtCi = ciorExpDtCi;
	}

	public char getCiorExpDtCi() {
		return this.ciorExpDtCi;
	}

	public void setCiorExpDt(String ciorExpDt) {
		this.ciorExpDt = Functions.subString(ciorExpDt, Len.CIOR_EXP_DT);
	}

	public String getCiorExpDt() {
		return this.ciorExpDt;
	}

	public void setCiorEffAcyTsCi(char ciorEffAcyTsCi) {
		this.ciorEffAcyTsCi = ciorEffAcyTsCi;
	}

	public char getCiorEffAcyTsCi() {
		return this.ciorEffAcyTsCi;
	}

	public void setCiorEffAcyTs(String ciorEffAcyTs) {
		this.ciorEffAcyTs = Functions.subString(ciorEffAcyTs, Len.CIOR_EFF_ACY_TS);
	}

	public String getCiorEffAcyTs() {
		return this.ciorEffAcyTs;
	}

	public void setCiorExpAcyTsCi(char ciorExpAcyTsCi) {
		this.ciorExpAcyTsCi = ciorExpAcyTsCi;
	}

	public char getCiorExpAcyTsCi() {
		return this.ciorExpAcyTsCi;
	}

	public void setCiorExpAcyTs(String ciorExpAcyTs) {
		this.ciorExpAcyTs = Functions.subString(ciorExpAcyTs, Len.CIOR_EXP_ACY_TS);
	}

	public String getCiorExpAcyTs() {
		return this.ciorExpAcyTs;
	}

	public void setAppType(char appType) {
		this.appType = appType;
	}

	public char getAppType() {
		return this.appType;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setObjDesc(String objDesc) {
		this.objDesc = Functions.subString(objDesc, Len.OBJ_DESC);
	}

	public String getObjDesc() {
		return this.objDesc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ID = 20;
		public static final int RLT_TYP_CD = 4;
		public static final int OBJ_CD = 4;
		public static final int CIOR_SHW_OBJ_KEY = 30;
		public static final int ADR_SEQ_NBR = 5;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CIOR_EXP_DT = 10;
		public static final int CIOR_EFF_ACY_TS = 26;
		public static final int CIOR_EXP_ACY_TS = 26;
		public static final int BUS_OBJ_NM = 32;
		public static final int OBJ_DESC = 40;
		public static final int CLIENT_ID_CI = 1;
		public static final int RLT_TYP_CD_CI = 1;
		public static final int OBJ_CD_CI = 1;
		public static final int CIOR_SHW_OBJ_KEY_CI = 1;
		public static final int ADR_SEQ_NBR_CI = 1;
		public static final int ADR_SEQ_NBR_SIGN = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int CIOR_EXP_DT_CI = 1;
		public static final int CIOR_EFF_ACY_TS_CI = 1;
		public static final int CIOR_EXP_ACY_TS_CI = 1;
		public static final int APP_TYPE = 1;
		public static final int CLT_OBJ_RELATION_DATA = CLIENT_ID_CI + CLIENT_ID + RLT_TYP_CD_CI + RLT_TYP_CD + OBJ_CD_CI + OBJ_CD
				+ CIOR_SHW_OBJ_KEY_CI + CIOR_SHW_OBJ_KEY + ADR_SEQ_NBR_CI + ADR_SEQ_NBR_SIGN + ADR_SEQ_NBR + USER_ID_CI + USER_ID + STATUS_CD_CI
				+ STATUS_CD + TERMINAL_ID_CI + TERMINAL_ID + CIOR_EXP_DT_CI + CIOR_EXP_DT + CIOR_EFF_ACY_TS_CI + CIOR_EFF_ACY_TS + CIOR_EXP_ACY_TS_CI
				+ CIOR_EXP_ACY_TS + APP_TYPE + BUS_OBJ_NM + OBJ_DESC;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
