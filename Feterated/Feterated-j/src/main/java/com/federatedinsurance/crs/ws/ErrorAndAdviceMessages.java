/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program TS030299<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessages {

	//==== PROPERTIES ====
	//Original name: EA-00-PROGRAM-MSG
	private Ea00ProgramMsgTs030299 ea00ProgramMsg = new Ea00ProgramMsgTs030299();
	//Original name: EA-01-CONNECT-ERROR-MSG
	private Ea01ConnectErrorMsg ea01ConnectErrorMsg = new Ea01ConnectErrorMsg();
	//Original name: EA-02-FETCH-ERROR-MSG
	private Ea02FetchErrorMsg ea02FetchErrorMsg = new Ea02FetchErrorMsg();

	//==== METHODS ====
	public Ea00ProgramMsgTs030299 getEa00ProgramMsg() {
		return ea00ProgramMsg;
	}

	public Ea01ConnectErrorMsg getEa01ConnectErrorMsg() {
		return ea01ConnectErrorMsg;
	}

	public Ea02FetchErrorMsg getEa02FetchErrorMsg() {
		return ea02FetchErrorMsg;
	}
}
