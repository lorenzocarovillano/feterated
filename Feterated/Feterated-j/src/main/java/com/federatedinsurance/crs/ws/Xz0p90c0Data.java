/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IActNotRec;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclactNotRec;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xzc050c1;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.occurs.TrRecipients;
import com.federatedinsurance.crs.ws.occurs.TsCertInfo;
import com.federatedinsurance.crs.ws.occurs.TuCertInfo;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0P90C0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0p90c0Data implements IActNotRec {

	//==== PROPERTIES ====
	public static final int TR_RECIPIENTS_MAXOCCURS = 2501;
	public static final int TS_CERT_INFO_MAXOCCURS = 2501;
	public static final int TU_CERT_INFO_MAXOCCURS = 2501;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0p90c0 constantFields = new ConstantFieldsXz0p90c0();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXz0p90c0 errorAndAdviceMessages = new ErrorAndAdviceMessagesXz0p90c0();
	//Original name: ES-01-FATAL-ERROR-MSG
	private Es01FatalErrorMsg es01FatalErrorMsg = new Es01FatalErrorMsg();
	/**Original name: NI-CER-HLD-NOT-IND<br>
	 * <pre>*  NULL-INDICATORS.</pre>*/
	private short niCerHldNotInd = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-CIT-NM
	private short niCitNm = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-LIN-1-ADR
	private short niLin1Adr = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-REC-CLT-ID
	private short niRecCltId = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-REC-ADR-ID
	private short niRecAdrId = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-ST-ABB
	private short niStAbb = DefaultValues.BIN_SHORT_VAL;
	//Original name: SUBSCRIPTS
	private SubscriptsXz0p0022 subscripts = new SubscriptsXz0p0022();
	//Original name: SWITCHES
	private SwitchesXz0p90c0 switches = new SwitchesXz0p90c0();
	//Original name: TR-RECIPIENTS
	private LazyArrayCopy<TrRecipients> trRecipients = new LazyArrayCopy<>(new TrRecipients(), 1, TR_RECIPIENTS_MAXOCCURS);
	//Original name: IX-TR
	private int ixTr = 1;
	//Original name: TS-CERT-INFO
	private LazyArrayCopy<TsCertInfo> tsCertInfo = new LazyArrayCopy<>(new TsCertInfo(), 1, TS_CERT_INFO_MAXOCCURS);
	//Original name: IX-TS
	private int ixTs = 1;
	//Original name: TU-CERT-INFO
	private LazyArrayCopy<TuCertInfo> tuCertInfo = new LazyArrayCopy<>(new TuCertInfo(), 1, TU_CERT_INFO_MAXOCCURS);
	//Original name: IX-TU
	private int ixTu = 1;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0p90c0 workingStorageArea = new WorkingStorageAreaXz0p90c0();
	//Original name: WS-XZ0Y90C0-ROW
	private WsXz0y90a0Row wsXz0y90c0Row = new WsXz0y90a0Row();
	//Original name: XZC050C1
	private Xzc050c1 xzc050c1 = new Xzc050c1();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: DCLACT-NOT-REC
	private DclactNotRec dclactNotRec = new DclactNotRec();
	//Original name: WS-PROXY-PROGRAM-AREA
	private WsProxyProgramArea wsProxyProgramArea = new WsProxyProgramArea();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== CONSTRUCTORS ====
	public Xz0p90c0Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		initTableOfRecipientsHighValues();
		initTableOfSortedCerHighValues();
		initTableOfUnsortedCerHighValues();
	}

	public short getNiCerHldNotInd() {
		return this.niCerHldNotInd;
	}

	public void setNiCitNm(short niCitNm) {
		this.niCitNm = niCitNm;
	}

	public short getNiCitNm() {
		return this.niCitNm;
	}

	public void setNiLin1Adr(short niLin1Adr) {
		this.niLin1Adr = niLin1Adr;
	}

	public short getNiLin1Adr() {
		return this.niLin1Adr;
	}

	public void setNiRecCltId(short niRecCltId) {
		this.niRecCltId = niRecCltId;
	}

	public short getNiRecCltId() {
		return this.niRecCltId;
	}

	public void setNiRecAdrId(short niRecAdrId) {
		this.niRecAdrId = niRecAdrId;
	}

	public short getNiRecAdrId() {
		return this.niRecAdrId;
	}

	public void setNiStAbb(short niStAbb) {
		this.niStAbb = niStAbb;
	}

	public short getNiStAbb() {
		return this.niStAbb;
	}

	public void initTableOfRecipientsHighValues() {
		getTrRecipientsObj().fill(new TrRecipients().initTrRecipientsHighValues());
	}

	public void setIxTr(int ixTr) {
		this.ixTr = ixTr;
	}

	public int getIxTr() {
		return this.ixTr;
	}

	public void initTableOfSortedCerHighValues() {
		getTsCertInfoObj().fill(new TsCertInfo().initTsCertInfoHighValues());
	}

	public void setIxTs(int ixTs) {
		this.ixTs = ixTs;
	}

	public int getIxTs() {
		return this.ixTs;
	}

	public void initTableOfUnsortedCerHighValues() {
		getTuCertInfoObj().fill(new TuCertInfo().initTuCertInfoHighValues());
	}

	public void setIxTu(int ixTu) {
		this.ixTu = ixTu;
	}

	public int getIxTu() {
		return this.ixTu;
	}

	public void setWsXzc050c1RowBytes(byte[] buffer) {
		setWsXzc050c1RowBytes(buffer, 1);
	}

	/**Original name: WS-XZC050C1-ROW<br>
	 * <pre>**  LAYOUT FOR CALLING GetCertificateList.</pre>*/
	public byte[] getWsXzc050c1RowBytes() {
		byte[] buffer = new byte[Len.WS_XZC050C1_ROW];
		return getWsXzc050c1RowBytes(buffer, 1);
	}

	public void setWsXzc050c1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc050c1.setXzc050ProgramInputBytes(buffer, position);
		position += Xzc050c1.Len.XZC050_PROGRAM_INPUT;
		xzc050c1.setXzc050ProgramOutputBytes(buffer, position);
	}

	public byte[] getWsXzc050c1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc050c1.getXzc050ProgramInputBytes(buffer, position);
		position += Xzc050c1.Len.XZC050_PROGRAM_INPUT;
		xzc050c1.getXzc050ProgramOutputBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getAdrId() {
		throw new FieldNotMappedException("adrId");
	}

	@Override
	public void setAdrId(String adrId) {
		throw new FieldNotMappedException("adrId");
	}

	@Override
	public String getAdrIdObj() {
		return getAdrId();
	}

	@Override
	public void setAdrIdObj(String adrIdObj) {
		setAdrId(adrIdObj);
	}

	@Override
	public String getCerNbr() {
		throw new FieldNotMappedException("cerNbr");
	}

	@Override
	public void setCerNbr(String cerNbr) {
		throw new FieldNotMappedException("cerNbr");
	}

	@Override
	public String getCerNbrObj() {
		return getCerNbr();
	}

	@Override
	public void setCerNbrObj(String cerNbrObj) {
		setCerNbr(cerNbrObj);
	}

	@Override
	public String getCitNm() {
		return dclactNotRec.getCitNm();
	}

	@Override
	public void setCitNm(String citNm) {
		this.dclactNotRec.setCitNm(citNm);
	}

	@Override
	public String getCitNmObj() {
		if (getNiCitNm() >= 0) {
			return getCitNm();
		} else {
			return null;
		}
	}

	@Override
	public void setCitNmObj(String citNmObj) {
		if (citNmObj != null) {
			setCitNm(citNmObj);
			setNiCitNm(((short) 0));
		} else {
			setNiCitNm(((short) -1));
		}
	}

	@Override
	public String getCltId() {
		throw new FieldNotMappedException("cltId");
	}

	@Override
	public void setCltId(String cltId) {
		throw new FieldNotMappedException("cltId");
	}

	@Override
	public String getCltIdObj() {
		return getCltId();
	}

	@Override
	public void setCltIdObj(String cltIdObj) {
		setCltId(cltIdObj);
	}

	public ConstantFieldsXz0p90c0 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclactNotRec getDclactNotRec() {
		return dclactNotRec;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public ErrorAndAdviceMessagesXz0p90c0 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public Es01FatalErrorMsg getEs01FatalErrorMsg() {
		return es01FatalErrorMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	@Override
	public String getLin1Adr() {
		return dclactNotRec.getLin1Adr();
	}

	@Override
	public void setLin1Adr(String lin1Adr) {
		this.dclactNotRec.setLin1Adr(lin1Adr);
	}

	@Override
	public String getLin1AdrObj() {
		if (getNiLin1Adr() >= 0) {
			return getLin1Adr();
		} else {
			return null;
		}
	}

	@Override
	public void setLin1AdrObj(String lin1AdrObj) {
		if (lin1AdrObj != null) {
			setLin1Adr(lin1AdrObj);
			setNiLin1Adr(((short) 0));
		} else {
			setNiLin1Adr(((short) -1));
		}
	}

	@Override
	public String getLin2Adr() {
		throw new FieldNotMappedException("lin2Adr");
	}

	@Override
	public void setLin2Adr(String lin2Adr) {
		throw new FieldNotMappedException("lin2Adr");
	}

	@Override
	public String getLin2AdrObj() {
		return getLin2Adr();
	}

	@Override
	public void setLin2AdrObj(String lin2AdrObj) {
		setLin2Adr(lin2AdrObj);
	}

	@Override
	public char getMnlInd() {
		throw new FieldNotMappedException("mnlInd");
	}

	@Override
	public void setMnlInd(char mnlInd) {
		throw new FieldNotMappedException("mnlInd");
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getPstCd() {
		throw new FieldNotMappedException("pstCd");
	}

	@Override
	public void setPstCd(String pstCd) {
		throw new FieldNotMappedException("pstCd");
	}

	@Override
	public String getPstCdObj() {
		return getPstCd();
	}

	@Override
	public void setPstCdObj(String pstCdObj) {
		setPstCd(pstCdObj);
	}

	@Override
	public String getRecAdrId() {
		return dclactNotRec.getRecAdrId();
	}

	@Override
	public void setRecAdrId(String recAdrId) {
		this.dclactNotRec.setRecAdrId(recAdrId);
	}

	@Override
	public String getRecAdrIdObj() {
		if (getNiRecAdrId() >= 0) {
			return getRecAdrId();
		} else {
			return null;
		}
	}

	@Override
	public void setRecAdrIdObj(String recAdrIdObj) {
		if (recAdrIdObj != null) {
			setRecAdrId(recAdrIdObj);
			setNiRecAdrId(((short) 0));
		} else {
			setNiRecAdrId(((short) -1));
		}
	}

	@Override
	public String getRecCltId() {
		return dclactNotRec.getRecCltId();
	}

	@Override
	public void setRecCltId(String recCltId) {
		this.dclactNotRec.setRecCltId(recCltId);
	}

	@Override
	public String getRecCltIdObj() {
		if (getNiRecCltId() >= 0) {
			return getRecCltId();
		} else {
			return null;
		}
	}

	@Override
	public void setRecCltIdObj(String recCltIdObj) {
		if (recCltIdObj != null) {
			setRecCltId(recCltIdObj);
			setNiRecCltId(((short) 0));
		} else {
			setNiRecCltId(((short) -1));
		}
	}

	@Override
	public String getRecNm() {
		return FixedStrings.get(dclactNotRec.getRecNmText(), dclactNotRec.getRecNmLen());
	}

	@Override
	public void setRecNm(String recNm) {
		this.dclactNotRec.setRecNmText(recNm);
		this.dclactNotRec.setRecNmLen((((short) strLen(recNm))));
	}

	@Override
	public String getRecNmObj() {
		return getRecNm();
	}

	@Override
	public void setRecNmObj(String recNmObj) {
		setRecNm(recNmObj);
	}

	@Override
	public short getRecSeqNbr() {
		throw new FieldNotMappedException("recSeqNbr");
	}

	@Override
	public void setRecSeqNbr(short recSeqNbr) {
		throw new FieldNotMappedException("recSeqNbr");
	}

	@Override
	public String getRecTypCd() {
		throw new FieldNotMappedException("recTypCd");
	}

	@Override
	public void setRecTypCd(String recTypCd) {
		throw new FieldNotMappedException("recTypCd");
	}

	@Override
	public String getStAbb() {
		return dclactNotRec.getStAbb();
	}

	@Override
	public void setStAbb(String stAbb) {
		this.dclactNotRec.setStAbb(stAbb);
	}

	@Override
	public String getStAbbObj() {
		if (getNiStAbb() >= 0) {
			return getStAbb();
		} else {
			return null;
		}
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		if (stAbbObj != null) {
			setStAbb(stAbbObj);
			setNiStAbb(((short) 0));
		} else {
			setNiStAbb(((short) -1));
		}
	}

	public SubscriptsXz0p0022 getSubscripts() {
		return subscripts;
	}

	public SwitchesXz0p90c0 getSwitches() {
		return switches;
	}

	public TrRecipients getTrRecipients(int idx) {
		return trRecipients.get(idx - 1);
	}

	public LazyArrayCopy<TrRecipients> getTrRecipientsObj() {
		return trRecipients;
	}

	public TsCertInfo getTsCertInfo(int idx) {
		return tsCertInfo.get(idx - 1);
	}

	public LazyArrayCopy<TsCertInfo> getTsCertInfoObj() {
		return tsCertInfo;
	}

	public TuCertInfo getTuCertInfo(int idx) {
		return tuCertInfo.get(idx - 1);
	}

	public LazyArrayCopy<TuCertInfo> getTuCertInfoObj() {
		return tuCertInfo;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0p90c0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsProxyProgramArea getWsProxyProgramArea() {
		return wsProxyProgramArea;
	}

	public WsXz0y90a0Row getWsXz0y90c0Row() {
		return wsXz0y90c0Row;
	}

	public Xzc050c1 getXzc050c1() {
		return xzc050c1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_XZC050C1_ROW = Xzc050c1.Len.XZC050_PROGRAM_INPUT + Xzc050c1.Len.XZC050_PROGRAM_OUTPUT;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
