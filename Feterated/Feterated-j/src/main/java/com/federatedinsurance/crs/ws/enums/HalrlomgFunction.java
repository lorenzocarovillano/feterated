/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: HALRLOMG-FUNCTION<br>
 * Variable: HALRLOMG-FUNCTION from copybook HALLLOMG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HalrlomgFunction {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.FUNCTION);
	public static final String ADD_LOCK_ENTRY = "ADD_LOCK_ENTRY";
	public static final String UPDATE_LOCK_ENTRY = "UPDATE_LOCK_ENTRY";
	public static final String UPGRADE_LOCK_ENTRY = "UPGRADE_LOCK_ENTRY";
	public static final String CHECK_FETCH_LOCK = "CHECK_LOCK_FOR_FETCH";
	public static final String CHECK_INSRET_LOCK = "CHECK_LOCK_FOR_INSRET";
	public static final String VERIFY_INSERT_ACCESS = "VERIFY_INSERT_ACCESS";
	public static final String CHECK_UPDATE_LOCK = "CHECK_LOCK_FOR_UPDATE";
	public static final String LIST_SESSION_LOCKS = "LIST_LOCKS_FOR_SESSION";
	public static final String DELETE_LOCK_ENTRY = "DELETE_LOCK_ENTRY";
	public static final String PURGE_LOCK_ENTRY = "PURGE_LOCK_ENTRY";
	public static final String ZAP_ALL_LOCK_ENTRIES = "ZAP_ALL_LOCK_ENTRIES";

	//==== METHODS ====
	public void setFunction(String function) {
		this.value = Functions.subString(function, Len.FUNCTION);
	}

	public String getFunction() {
		return this.value;
	}

	public boolean isHalrlomgAddLockEntry() {
		return value.equals(ADD_LOCK_ENTRY);
	}

	public void setHalrlomgAddLockEntry() {
		value = ADD_LOCK_ENTRY;
	}

	public void setHalrlomgUpdateLockEntry() {
		value = UPDATE_LOCK_ENTRY;
	}

	public boolean isHalrlomgUpgradeLockEntry() {
		return value.equals(UPGRADE_LOCK_ENTRY);
	}

	public void setHalrlomgUpgradeLockEntry() {
		value = UPGRADE_LOCK_ENTRY;
	}

	public void setHalrlomgCheckFetchLock() {
		value = CHECK_FETCH_LOCK;
	}

	public boolean isCheckInsretLock() {
		return value.equals(CHECK_INSRET_LOCK);
	}

	public void setHalrlomgCheckInsretLock() {
		value = CHECK_INSRET_LOCK;
	}

	public boolean isVerifyInsertAccess() {
		return value.equals(VERIFY_INSERT_ACCESS);
	}

	public void setHalrlomgVerifyInsertAccess() {
		value = VERIFY_INSERT_ACCESS;
	}

	public void setHalrlomgCheckUpdateLock() {
		value = CHECK_UPDATE_LOCK;
	}

	public boolean isDeleteLockEntry() {
		return value.equals(DELETE_LOCK_ENTRY);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FUNCTION = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
