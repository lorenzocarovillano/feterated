/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Cw04fMoreRowsSw;

/**Original name: CW04F-CLIENT-PHONE-DATA<br>
 * Variable: CW04F-CLIENT-PHONE-DATA from copybook CAWLF004<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw04fClientPhoneData {

	//==== PROPERTIES ====
	/**Original name: CW04F-CIPH-PHN-NBR-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char ciphPhnNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-CIPH-PHN-NBR
	private String ciphPhnNbr = DefaultValues.stringVal(Len.CIPH_PHN_NBR);
	//Original name: CW04F-PHN-TYP-CD-CI
	private char phnTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-PHN-TYP-CD
	private String phnTypCd = DefaultValues.stringVal(Len.PHN_TYP_CD);
	//Original name: CW04F-CIPH-XRF-ID-CI
	private char ciphXrfIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-CIPH-XRF-ID-NI
	private char ciphXrfIdNi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-CIPH-XRF-ID
	private String ciphXrfId = DefaultValues.stringVal(Len.CIPH_XRF_ID);
	//Original name: CW04F-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW04F-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW04F-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW04F-CIPH-EXP-DT-CI
	private char ciphExpDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-CIPH-EXP-DT
	private String ciphExpDt = DefaultValues.stringVal(Len.CIPH_EXP_DT);
	//Original name: CW04F-CIPH-EFF-ACY-TS-CI
	private char ciphEffAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-CIPH-EFF-ACY-TS
	private String ciphEffAcyTs = DefaultValues.stringVal(Len.CIPH_EFF_ACY_TS);
	//Original name: CW04F-CIPH-EXP-ACY-TS-CI
	private char ciphExpAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW04F-CIPH-EXP-ACY-TS
	private String ciphExpAcyTs = DefaultValues.stringVal(Len.CIPH_EXP_ACY_TS);
	/**Original name: CW04F-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	private Cw04fMoreRowsSw moreRowsSw = new Cw04fMoreRowsSw();
	//Original name: CW04F-PHN-TYP-DESC
	private String phnTypDesc = DefaultValues.stringVal(Len.PHN_TYP_DESC);

	//==== METHODS ====
	public void setClientPhoneDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ciphPhnNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciphPhnNbr = MarshalByte.readString(buffer, position, Len.CIPH_PHN_NBR);
		position += Len.CIPH_PHN_NBR;
		phnTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		phnTypCd = MarshalByte.readString(buffer, position, Len.PHN_TYP_CD);
		position += Len.PHN_TYP_CD;
		ciphXrfIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciphXrfIdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciphXrfId = MarshalByte.readString(buffer, position, Len.CIPH_XRF_ID);
		position += Len.CIPH_XRF_ID;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		ciphExpDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciphExpDt = MarshalByte.readString(buffer, position, Len.CIPH_EXP_DT);
		position += Len.CIPH_EXP_DT;
		ciphEffAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciphEffAcyTs = MarshalByte.readString(buffer, position, Len.CIPH_EFF_ACY_TS);
		position += Len.CIPH_EFF_ACY_TS;
		ciphExpAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciphExpAcyTs = MarshalByte.readString(buffer, position, Len.CIPH_EXP_ACY_TS);
		position += Len.CIPH_EXP_ACY_TS;
		moreRowsSw.setMoreRowsSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		phnTypDesc = MarshalByte.readString(buffer, position, Len.PHN_TYP_DESC);
	}

	public byte[] getClientPhoneDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, ciphPhnNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciphPhnNbr, Len.CIPH_PHN_NBR);
		position += Len.CIPH_PHN_NBR;
		MarshalByte.writeChar(buffer, position, phnTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, phnTypCd, Len.PHN_TYP_CD);
		position += Len.PHN_TYP_CD;
		MarshalByte.writeChar(buffer, position, ciphXrfIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciphXrfIdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciphXrfId, Len.CIPH_XRF_ID);
		position += Len.CIPH_XRF_ID;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, ciphExpDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciphExpDt, Len.CIPH_EXP_DT);
		position += Len.CIPH_EXP_DT;
		MarshalByte.writeChar(buffer, position, ciphEffAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciphEffAcyTs, Len.CIPH_EFF_ACY_TS);
		position += Len.CIPH_EFF_ACY_TS;
		MarshalByte.writeChar(buffer, position, ciphExpAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciphExpAcyTs, Len.CIPH_EXP_ACY_TS);
		position += Len.CIPH_EXP_ACY_TS;
		MarshalByte.writeChar(buffer, position, moreRowsSw.getMoreRowsSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, phnTypDesc, Len.PHN_TYP_DESC);
		return buffer;
	}

	public void setCiphPhnNbrCi(char ciphPhnNbrCi) {
		this.ciphPhnNbrCi = ciphPhnNbrCi;
	}

	public char getCiphPhnNbrCi() {
		return this.ciphPhnNbrCi;
	}

	public void setCiphPhnNbr(String ciphPhnNbr) {
		this.ciphPhnNbr = Functions.subString(ciphPhnNbr, Len.CIPH_PHN_NBR);
	}

	public String getCiphPhnNbr() {
		return this.ciphPhnNbr;
	}

	public void setPhnTypCdCi(char phnTypCdCi) {
		this.phnTypCdCi = phnTypCdCi;
	}

	public char getPhnTypCdCi() {
		return this.phnTypCdCi;
	}

	public void setPhnTypCd(String phnTypCd) {
		this.phnTypCd = Functions.subString(phnTypCd, Len.PHN_TYP_CD);
	}

	public String getPhnTypCd() {
		return this.phnTypCd;
	}

	public void setCiphXrfIdCi(char ciphXrfIdCi) {
		this.ciphXrfIdCi = ciphXrfIdCi;
	}

	public char getCiphXrfIdCi() {
		return this.ciphXrfIdCi;
	}

	public void setCiphXrfIdNi(char ciphXrfIdNi) {
		this.ciphXrfIdNi = ciphXrfIdNi;
	}

	public char getCiphXrfIdNi() {
		return this.ciphXrfIdNi;
	}

	public void setCiphXrfId(String ciphXrfId) {
		this.ciphXrfId = Functions.subString(ciphXrfId, Len.CIPH_XRF_ID);
	}

	public String getCiphXrfId() {
		return this.ciphXrfId;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setCiphExpDtCi(char ciphExpDtCi) {
		this.ciphExpDtCi = ciphExpDtCi;
	}

	public char getCiphExpDtCi() {
		return this.ciphExpDtCi;
	}

	public void setCiphExpDt(String ciphExpDt) {
		this.ciphExpDt = Functions.subString(ciphExpDt, Len.CIPH_EXP_DT);
	}

	public String getCiphExpDt() {
		return this.ciphExpDt;
	}

	public void setCiphEffAcyTsCi(char ciphEffAcyTsCi) {
		this.ciphEffAcyTsCi = ciphEffAcyTsCi;
	}

	public char getCiphEffAcyTsCi() {
		return this.ciphEffAcyTsCi;
	}

	public void setCiphEffAcyTs(String ciphEffAcyTs) {
		this.ciphEffAcyTs = Functions.subString(ciphEffAcyTs, Len.CIPH_EFF_ACY_TS);
	}

	public String getCiphEffAcyTs() {
		return this.ciphEffAcyTs;
	}

	public void setCiphExpAcyTsCi(char ciphExpAcyTsCi) {
		this.ciphExpAcyTsCi = ciphExpAcyTsCi;
	}

	public char getCiphExpAcyTsCi() {
		return this.ciphExpAcyTsCi;
	}

	public void setCiphExpAcyTs(String ciphExpAcyTs) {
		this.ciphExpAcyTs = Functions.subString(ciphExpAcyTs, Len.CIPH_EXP_ACY_TS);
	}

	public String getCiphExpAcyTs() {
		return this.ciphExpAcyTs;
	}

	public void setPhnTypDesc(String phnTypDesc) {
		this.phnTypDesc = Functions.subString(phnTypDesc, Len.PHN_TYP_DESC);
	}

	public String getPhnTypDesc() {
		return this.phnTypDesc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CIPH_PHN_NBR = 21;
		public static final int PHN_TYP_CD = 2;
		public static final int CIPH_XRF_ID = 20;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CIPH_EXP_DT = 10;
		public static final int CIPH_EFF_ACY_TS = 26;
		public static final int CIPH_EXP_ACY_TS = 26;
		public static final int PHN_TYP_DESC = 40;
		public static final int CIPH_PHN_NBR_CI = 1;
		public static final int PHN_TYP_CD_CI = 1;
		public static final int CIPH_XRF_ID_CI = 1;
		public static final int CIPH_XRF_ID_NI = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int CIPH_EXP_DT_CI = 1;
		public static final int CIPH_EFF_ACY_TS_CI = 1;
		public static final int CIPH_EXP_ACY_TS_CI = 1;
		public static final int CLIENT_PHONE_DATA = CIPH_PHN_NBR_CI + CIPH_PHN_NBR + PHN_TYP_CD_CI + PHN_TYP_CD + CIPH_XRF_ID_CI + CIPH_XRF_ID_NI
				+ CIPH_XRF_ID + USER_ID_CI + USER_ID + STATUS_CD_CI + STATUS_CD + TERMINAL_ID_CI + TERMINAL_ID + CIPH_EXP_DT_CI + CIPH_EXP_DT
				+ CIPH_EFF_ACY_TS_CI + CIPH_EFF_ACY_TS + CIPH_EXP_ACY_TS_CI + CIPH_EXP_ACY_TS + Cw04fMoreRowsSw.Len.MORE_ROWS_SW + PHN_TYP_DESC;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
