/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: MDRV-RETURN-WARNINGS-IND<br>
 * Variable: MDRV-RETURN-WARNINGS-IND from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvReturnWarningsInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char RETURN_WARNINGS = Types.SPACE_CHAR;
	public static final char DO_NOT_RETURN_WARNINGS = 'N';

	//==== METHODS ====
	public void setReturnWarningsInd(char returnWarningsInd) {
		this.value = returnWarningsInd;
	}

	public void setReturnWarningsIndFormatted(String returnWarningsInd) {
		setReturnWarningsInd(Functions.charAt(returnWarningsInd, Types.CHAR_SIZE));
	}

	public char getReturnWarningsInd() {
		return this.value;
	}

	public void setReturnWarnings() {
		setReturnWarningsIndFormatted(String.valueOf(RETURN_WARNINGS));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURN_WARNINGS_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
