/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZC09090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXzc09090 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-ERR
	private short maxErr = ((short) 10);
	//Original name: CF-SOAP-SERVICE
	private String soapService = "CRSTranInformationCallable";
	//Original name: CF-SOAP-OPERATION
	private String soapOperation = "getTransactionDetail";
	//Original name: CF-CALLABLE-SVC-PROGRAM
	private String callableSvcProgram = "GIICALS";
	//Original name: CF-TRACE-STOR-VALUE
	private String traceStorValue = "GWAO";
	//Original name: CF-PARAGRAPH-NAMES
	private CfParagraphNamesXzc09090 paragraphNames = new CfParagraphNamesXzc09090();
	//Original name: CF-WEB-SVC-ID
	private String webSvcId = "PPBKRTRSINF";
	//Original name: CF-WEB-SVC-LKU-PGM
	private String webSvcLkuPgm = "TS571099";
	//Original name: CF-LV-ZLINUX-SRV
	private String lvZlinuxSrv = "03";
	//Original name: CF-CONTAINER-INFO
	private CfContainerInfoXzc09090 containerInfo = new CfContainerInfoXzc09090();

	//==== METHODS ====
	public short getMaxErr() {
		return this.maxErr;
	}

	public String getSoapService() {
		return this.soapService;
	}

	public String getSoapOperation() {
		return this.soapOperation;
	}

	public String getCallableSvcProgram() {
		return this.callableSvcProgram;
	}

	public String getTraceStorValue() {
		return this.traceStorValue;
	}

	public String getWebSvcId() {
		return this.webSvcId;
	}

	public String getWebSvcLkuPgm() {
		return this.webSvcLkuPgm;
	}

	public String getLvZlinuxSrv() {
		return this.lvZlinuxSrv;
	}

	public CfContainerInfoXzc09090 getContainerInfo() {
		return containerInfo;
	}

	public CfParagraphNamesXzc09090 getParagraphNames() {
		return paragraphNames;
	}
}
