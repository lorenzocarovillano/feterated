/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.federatedinsurance.crs.ws.enums.WReadVsamInd;

/**Original name: W-WORKFIELDS<br>
 * Variable: W-WORKFIELDS from program HALOESTO<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WWorkfieldsHaloesto {

	//==== PROPERTIES ====
	//Original name: W-RESPONSE
	private int response = DefaultValues.BIN_INT_VAL;
	//Original name: W-RESPONSE2
	private int response2 = DefaultValues.BIN_INT_VAL;
	//Original name: W-READ-VSAM-IND
	private WReadVsamInd readVsamInd = new WReadVsamInd();

	//==== METHODS ====
	public void setResponse(int response) {
		this.response = response;
	}

	public int getResponse() {
		return this.response;
	}

	public void setResponse2(int response2) {
		this.response2 = response2;
	}

	public int getResponse2() {
		return this.response2;
	}

	public WReadVsamInd getReadVsamInd() {
		return readVsamInd;
	}
}
