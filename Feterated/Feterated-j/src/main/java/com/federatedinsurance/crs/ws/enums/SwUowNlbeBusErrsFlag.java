/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-UOW-NLBE-BUS-ERRS-FLAG<br>
 * Variable: SW-UOW-NLBE-BUS-ERRS-FLAG from program TS020000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwUowNlbeBusErrsFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char START_OF_UOW_NLBE_BUS_ERRS = '0';
	public static final char NO_MORE_UOW_NLBE_BUS_ERRS = '1';

	//==== METHODS ====
	public void setUowNlbeBusErrsFlag(char uowNlbeBusErrsFlag) {
		this.value = uowNlbeBusErrsFlag;
	}

	public char getUowNlbeBusErrsFlag() {
		return this.value;
	}

	public void setStartOfUowNlbeBusErrs() {
		value = START_OF_UOW_NLBE_BUS_ERRS;
	}

	public boolean isNoMoreUowNlbeBusErrs() {
		return value == NO_MORE_UOW_NLBE_BUS_ERRS;
	}

	public void setNoMoreUowNlbeBusErrs() {
		value = NO_MORE_UOW_NLBE_BUS_ERRS;
	}
}
