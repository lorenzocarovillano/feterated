/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZA970-ACY-FRM-INFO<br>
 * Variable: XZA970-ACY-FRM-INFO from copybook XZ0A9070<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xza970AcyFrmInfo {

	//==== PROPERTIES ====
	//Original name: XZA970-FRM-NBR
	private String frmNbr = DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: XZA970-FRM-EDT-DT
	private String frmEdtDt = DefaultValues.stringVal(Len.FRM_EDT_DT);
	//Original name: XZA970-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZA970-EDL-FRM-NM
	private String edlFrmNm = DefaultValues.stringVal(Len.EDL_FRM_NM);
	//Original name: XZA970-FRM-DES
	private String frmDes = DefaultValues.stringVal(Len.FRM_DES);
	//Original name: XZA970-SPE-PRC-CD
	private String spePrcCd = DefaultValues.stringVal(Len.SPE_PRC_CD);
	//Original name: XZA970-DTN-CD
	private int dtnCd = DefaultValues.INT_VAL;

	//==== METHODS ====
	public void setAcyFrmInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		frmNbr = MarshalByte.readString(buffer, position, Len.FRM_NBR);
		position += Len.FRM_NBR;
		frmEdtDt = MarshalByte.readString(buffer, position, Len.FRM_EDT_DT);
		position += Len.FRM_EDT_DT;
		actNotTypCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		edlFrmNm = MarshalByte.readString(buffer, position, Len.EDL_FRM_NM);
		position += Len.EDL_FRM_NM;
		frmDes = MarshalByte.readString(buffer, position, Len.FRM_DES);
		position += Len.FRM_DES;
		spePrcCd = MarshalByte.readString(buffer, position, Len.SPE_PRC_CD);
		position += Len.SPE_PRC_CD;
		dtnCd = MarshalByte.readInt(buffer, position, Len.DTN_CD);
	}

	public byte[] getAcyFrmInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, frmNbr, Len.FRM_NBR);
		position += Len.FRM_NBR;
		MarshalByte.writeString(buffer, position, frmEdtDt, Len.FRM_EDT_DT);
		position += Len.FRM_EDT_DT;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, edlFrmNm, Len.EDL_FRM_NM);
		position += Len.EDL_FRM_NM;
		MarshalByte.writeString(buffer, position, frmDes, Len.FRM_DES);
		position += Len.FRM_DES;
		MarshalByte.writeString(buffer, position, spePrcCd, Len.SPE_PRC_CD);
		position += Len.SPE_PRC_CD;
		MarshalByte.writeInt(buffer, position, dtnCd, Len.DTN_CD);
		return buffer;
	}

	public void setFrmNbr(String frmNbr) {
		this.frmNbr = Functions.subString(frmNbr, Len.FRM_NBR);
	}

	public String getFrmNbr() {
		return this.frmNbr;
	}

	public void setFrmEdtDt(String frmEdtDt) {
		this.frmEdtDt = Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	public String getFrmEdtDt() {
		return this.frmEdtDt;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setEdlFrmNm(String edlFrmNm) {
		this.edlFrmNm = Functions.subString(edlFrmNm, Len.EDL_FRM_NM);
	}

	public String getEdlFrmNm() {
		return this.edlFrmNm;
	}

	public void setFrmDes(String frmDes) {
		this.frmDes = Functions.subString(frmDes, Len.FRM_DES);
	}

	public String getFrmDes() {
		return this.frmDes;
	}

	public void setSpePrcCd(String spePrcCd) {
		this.spePrcCd = Functions.subString(spePrcCd, Len.SPE_PRC_CD);
	}

	public String getSpePrcCd() {
		return this.spePrcCd;
	}

	public void setDtnCd(int dtnCd) {
		this.dtnCd = dtnCd;
	}

	public int getDtnCd() {
		return this.dtnCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FRM_NBR = 30;
		public static final int FRM_EDT_DT = 10;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int EDL_FRM_NM = 30;
		public static final int FRM_DES = 20;
		public static final int SPE_PRC_CD = 8;
		public static final int DTN_CD = 5;
		public static final int ACY_FRM_INFO = FRM_NBR + FRM_EDT_DT + ACT_NOT_TYP_CD + EDL_FRM_NM + FRM_DES + SPE_PRC_CD + DTN_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
