/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.enums.PpcBypassSyncpointMdrvInd;
import com.federatedinsurance.crs.ws.occurs.PpcNonLoggableErrors;
import com.federatedinsurance.crs.ws.occurs.PpcWarnings;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-PROXY-PROGRAM-AREA<br>
 * Variable: WS-PROXY-PROGRAM-AREA from program XZ0B90B0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsProxyProgramArea extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int PPC_NON_LOGGABLE_ERRORS_MAXOCCURS = 10;
	public static final int PPC_WARNINGS_MAXOCCURS = 10;
	//Original name: PPC-SERVICE-DATA-SIZE
	private int ppcServiceDataSize = DefaultValues.BIN_INT_VAL;
	//Original name: PPC-SERVICE-DATA-POINTER
	private int ppcServiceDataPointer = DefaultValues.BIN_INT_VAL;
	/**Original name: PPC-BYPASS-SYNCPOINT-MDRV-IND<br>
	 * <pre>* FLAG TO INDICATE WHETHER OR NOT MAIN DRIVER DOES SYNCPOINT</pre>*/
	private PpcBypassSyncpointMdrvInd ppcBypassSyncpointMdrvInd = new PpcBypassSyncpointMdrvInd();
	//Original name: PPC-OPERATION
	private String ppcOperation = DefaultValues.stringVal(Len.PPC_OPERATION);
	//Original name: PPC-ERROR-RETURN-CODE
	private DsdErrorReturnCode ppcErrorReturnCode = new DsdErrorReturnCode();
	//Original name: PPC-FATAL-ERROR-MESSAGE
	private String ppcFatalErrorMessage = DefaultValues.stringVal(Len.PPC_FATAL_ERROR_MESSAGE);
	//Original name: PPC-NON-LOGGABLE-ERROR-CNT
	private String ppcNonLoggableErrorCnt = DefaultValues.stringVal(Len.PPC_NON_LOGGABLE_ERROR_CNT);
	//Original name: PPC-NON-LOGGABLE-ERRORS
	private PpcNonLoggableErrors[] ppcNonLoggableErrors = new PpcNonLoggableErrors[PPC_NON_LOGGABLE_ERRORS_MAXOCCURS];
	//Original name: PPC-WARNING-CNT
	private String ppcWarningCnt = DefaultValues.stringVal(Len.PPC_WARNING_CNT);
	//Original name: PPC-WARNINGS
	private PpcWarnings[] ppcWarnings = new PpcWarnings[PPC_WARNINGS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public WsProxyProgramArea() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_PROXY_PROGRAM_AREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsProxyProgramAreaBytes(buf);
	}

	public void init() {
		for (int ppcNonLoggableErrorsIdx = 1; ppcNonLoggableErrorsIdx <= PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; ppcNonLoggableErrorsIdx++) {
			ppcNonLoggableErrors[ppcNonLoggableErrorsIdx - 1] = new PpcNonLoggableErrors();
		}
		for (int ppcWarningsIdx = 1; ppcWarningsIdx <= PPC_WARNINGS_MAXOCCURS; ppcWarningsIdx++) {
			ppcWarnings[ppcWarningsIdx - 1] = new PpcWarnings();
		}
	}

	public void setWsProxyProgramAreaBytes(byte[] buffer) {
		setWsProxyProgramAreaBytes(buffer, 1);
	}

	public byte[] getWsProxyProgramAreaBytes() {
		byte[] buffer = new byte[Len.WS_PROXY_PROGRAM_AREA];
		return getWsProxyProgramAreaBytes(buffer, 1);
	}

	public void setWsProxyProgramAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		setProxyProgramCommonBytes(buffer, position);
	}

	public byte[] getWsProxyProgramAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		getProxyProgramCommonBytes(buffer, position);
		return buffer;
	}

	public void setProxyProgramCommonBytes(byte[] buffer) {
		setProxyProgramCommonBytes(buffer, 1);
	}

	public void setProxyProgramCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		setPpcInputParmsBytes(buffer, position);
		position += Len.PPC_INPUT_PARMS;
		setPpcOutputParmsBytes(buffer, position);
	}

	public byte[] getProxyProgramCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		getPpcInputParmsBytes(buffer, position);
		position += Len.PPC_INPUT_PARMS;
		getPpcOutputParmsBytes(buffer, position);
		return buffer;
	}

	public void setPpcInputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		setPpcMemoryAllocationParmsBytes(buffer, position);
		position += Len.PPC_MEMORY_ALLOCATION_PARMS;
		ppcBypassSyncpointMdrvInd.setPpcBypassSyncpointMdrvInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		ppcOperation = MarshalByte.readString(buffer, position, Len.PPC_OPERATION);
	}

	public byte[] getPpcInputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		getPpcMemoryAllocationParmsBytes(buffer, position);
		position += Len.PPC_MEMORY_ALLOCATION_PARMS;
		MarshalByte.writeChar(buffer, position, ppcBypassSyncpointMdrvInd.getPpcBypassSyncpointMdrvInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ppcOperation, Len.PPC_OPERATION);
		return buffer;
	}

	public void setPpcMemoryAllocationParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		ppcServiceDataSize = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		ppcServiceDataPointer = MarshalByte.readBinaryInt(buffer, position);
	}

	public byte[] getPpcMemoryAllocationParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryInt(buffer, position, ppcServiceDataSize);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, ppcServiceDataPointer);
		return buffer;
	}

	public void setPpcServiceDataSize(int ppcServiceDataSize) {
		this.ppcServiceDataSize = ppcServiceDataSize;
	}

	public int getPpcServiceDataSize() {
		return this.ppcServiceDataSize;
	}

	public void setPpcServiceDataPointer(int ppcServiceDataPointer) {
		this.ppcServiceDataPointer = ppcServiceDataPointer;
	}

	public int getPpcServiceDataPointer() {
		return this.ppcServiceDataPointer;
	}

	public void setPpcOperation(String ppcOperation) {
		this.ppcOperation = Functions.subString(ppcOperation, Len.PPC_OPERATION);
	}

	public String getPpcOperation() {
		return this.ppcOperation;
	}

	public void setPpcOutputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		setPpcErrorHandlingParmsBytes(buffer, position);
	}

	public byte[] getPpcOutputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		getPpcErrorHandlingParmsBytes(buffer, position);
		return buffer;
	}

	public void setPpcErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		ppcErrorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		ppcFatalErrorMessage = MarshalByte.readString(buffer, position, Len.PPC_FATAL_ERROR_MESSAGE);
		position += Len.PPC_FATAL_ERROR_MESSAGE;
		ppcNonLoggableErrorCnt = MarshalByte.readFixedString(buffer, position, Len.PPC_NON_LOGGABLE_ERROR_CNT);
		position += Len.PPC_NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				ppcNonLoggableErrors[idx - 1].setPpcNonLoggableErrorsBytes(buffer, position);
				position += PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS;
			} else {
				ppcNonLoggableErrors[idx - 1].initPpcNonLoggableErrorsSpaces();
				position += PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS;
			}
		}
		ppcWarningCnt = MarshalByte.readFixedString(buffer, position, Len.PPC_WARNING_CNT);
		position += Len.PPC_WARNING_CNT;
		for (int idx = 1; idx <= PPC_WARNINGS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				ppcWarnings[idx - 1].setPpcWarningsBytes(buffer, position);
				position += PpcWarnings.Len.PPC_WARNINGS;
			} else {
				ppcWarnings[idx - 1].initPpcWarningsSpaces();
				position += PpcWarnings.Len.PPC_WARNINGS;
			}
		}
	}

	public byte[] getPpcErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ppcErrorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, ppcFatalErrorMessage, Len.PPC_FATAL_ERROR_MESSAGE);
		position += Len.PPC_FATAL_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, ppcNonLoggableErrorCnt, Len.PPC_NON_LOGGABLE_ERROR_CNT);
		position += Len.PPC_NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			ppcNonLoggableErrors[idx - 1].getPpcNonLoggableErrorsBytes(buffer, position);
			position += PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS;
		}
		MarshalByte.writeString(buffer, position, ppcWarningCnt, Len.PPC_WARNING_CNT);
		position += Len.PPC_WARNING_CNT;
		for (int idx = 1; idx <= PPC_WARNINGS_MAXOCCURS; idx++) {
			ppcWarnings[idx - 1].getPpcWarningsBytes(buffer, position);
			position += PpcWarnings.Len.PPC_WARNINGS;
		}
		return buffer;
	}

	public void setPpcFatalErrorMessage(String ppcFatalErrorMessage) {
		this.ppcFatalErrorMessage = Functions.subString(ppcFatalErrorMessage, Len.PPC_FATAL_ERROR_MESSAGE);
	}

	public String getPpcFatalErrorMessage() {
		return this.ppcFatalErrorMessage;
	}

	public String getPpcFatalErrorMessageFormatted() {
		return Functions.padBlanks(getPpcFatalErrorMessage(), Len.PPC_FATAL_ERROR_MESSAGE);
	}

	public void setPpcNonLoggableErrorCntFormatted(String ppcNonLoggableErrorCnt) {
		this.ppcNonLoggableErrorCnt = Trunc.toUnsignedNumeric(ppcNonLoggableErrorCnt, Len.PPC_NON_LOGGABLE_ERROR_CNT);
	}

	public short getPpcNonLoggableErrorCnt() {
		return NumericDisplay.asShort(this.ppcNonLoggableErrorCnt);
	}

	public void setPpcWarningCntFormatted(String ppcWarningCnt) {
		this.ppcWarningCnt = Trunc.toUnsignedNumeric(ppcWarningCnt, Len.PPC_WARNING_CNT);
	}

	public short getPpcWarningCnt() {
		return NumericDisplay.asShort(this.ppcWarningCnt);
	}

	public PpcBypassSyncpointMdrvInd getPpcBypassSyncpointMdrvInd() {
		return ppcBypassSyncpointMdrvInd;
	}

	public DsdErrorReturnCode getPpcErrorReturnCode() {
		return ppcErrorReturnCode;
	}

	public PpcNonLoggableErrors getPpcNonLoggableErrors(int idx) {
		return ppcNonLoggableErrors[idx - 1];
	}

	public PpcWarnings getPpcWarnings(int idx) {
		return ppcWarnings[idx - 1];
	}

	@Override
	public byte[] serialize() {
		return getWsProxyProgramAreaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PPC_SERVICE_DATA_SIZE = 4;
		public static final int PPC_SERVICE_DATA_POINTER = 4;
		public static final int PPC_MEMORY_ALLOCATION_PARMS = PPC_SERVICE_DATA_SIZE + PPC_SERVICE_DATA_POINTER;
		public static final int PPC_OPERATION = 32;
		public static final int PPC_INPUT_PARMS = PPC_MEMORY_ALLOCATION_PARMS + PpcBypassSyncpointMdrvInd.Len.PPC_BYPASS_SYNCPOINT_MDRV_IND
				+ PPC_OPERATION;
		public static final int PPC_FATAL_ERROR_MESSAGE = 250;
		public static final int PPC_NON_LOGGABLE_ERROR_CNT = 4;
		public static final int PPC_WARNING_CNT = 4;
		public static final int PPC_ERROR_HANDLING_PARMS = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + PPC_FATAL_ERROR_MESSAGE
				+ PPC_NON_LOGGABLE_ERROR_CNT + WsProxyProgramArea.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS * PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS
				+ PPC_WARNING_CNT + WsProxyProgramArea.PPC_WARNINGS_MAXOCCURS * PpcWarnings.Len.PPC_WARNINGS;
		public static final int PPC_OUTPUT_PARMS = PPC_ERROR_HANDLING_PARMS;
		public static final int PROXY_PROGRAM_COMMON = PPC_INPUT_PARMS + PPC_OUTPUT_PARMS;
		public static final int WS_PROXY_PROGRAM_AREA = PROXY_PROGRAM_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
