/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0F0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0f0003 {

	//==== PROPERTIES ====
	//Original name: CF-YES
	private char yes = 'Y';
	//Original name: CF-POSITIVE
	private char positive = '+';
	//Original name: CF-NEGATIVE
	private char negative = '-';

	//==== METHODS ====
	public char getYes() {
		return this.yes;
	}

	public char getPositive() {
		return this.positive;
	}

	public char getNegative() {
		return this.negative;
	}
}
