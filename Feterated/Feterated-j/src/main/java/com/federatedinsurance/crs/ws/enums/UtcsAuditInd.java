/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UTCS-AUDIT-IND<br>
 * Variable: UTCS-AUDIT-IND from copybook HALLUTCS<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UtcsAuditInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char APPLY_AUDITS = 'Y';
	public static final char NO_AUDITS = 'N';

	//==== METHODS ====
	public void setAuditInd(char auditInd) {
		this.value = auditInd;
	}

	public char getAuditInd() {
		return this.value;
	}

	public boolean isApplyAudits() {
		return value == APPLY_AUDITS;
	}
}
