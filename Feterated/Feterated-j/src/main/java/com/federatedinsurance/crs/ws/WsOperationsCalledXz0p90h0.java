/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WS-OPERATIONS-CALLED<br>
 * Variable: WS-OPERATIONS-CALLED from program XZ0P90H0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsOperationsCalledXz0p90h0 {

	//==== PROPERTIES ====
	//Original name: WS-ADD-ACT-NOT-POL
	private String addActNotPol = "AddAccountNotificationPolicy";
	//Original name: WS-ADD-ACT-NOT-REC
	private String addActNotRec = "AddAccountNotificationRecipient";
	//Original name: WS-GET-ACT-CLT-LIST
	private String getActCltList = "GetActCltList";

	//==== METHODS ====
	public String getAddActNotPol() {
		return this.addActNotPol;
	}

	public String getAddActNotRec() {
		return this.addActNotRec;
	}

	public String getGetActCltList() {
		return this.getActCltList;
	}
}
