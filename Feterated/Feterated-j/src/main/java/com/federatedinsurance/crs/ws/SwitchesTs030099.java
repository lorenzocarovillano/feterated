/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.SwBalFileStatusFlag;
import com.federatedinsurance.crs.ws.enums.SwComFile131StatusFlag;
import com.federatedinsurance.crs.ws.enums.SwComFile132StatusFlag;
import com.federatedinsurance.crs.ws.enums.SwInHouseFileStatusFlag;
import com.federatedinsurance.crs.ws.enums.SwInHouseStruct132Flag;
import com.federatedinsurance.crs.ws.enums.SwInHouseStruct198Flag;
import com.federatedinsurance.crs.ws.enums.SwLaser198StatusFlag;
import com.federatedinsurance.crs.ws.enums.SwNewInhouseReptNmbrFlag;
import com.federatedinsurance.crs.ws.enums.SwNewReportNumberFlag;
import com.federatedinsurance.crs.ws.enums.SwNewVpsReportNumberFlag;
import com.federatedinsurance.crs.ws.enums.SwPrtrFileOpenFlag;
import com.federatedinsurance.crs.ws.enums.SwReportChangedFlag;
import com.federatedinsurance.crs.ws.enums.SwReportOnFileFlag;
import com.federatedinsurance.crs.ws.enums.SwSignOnAttemptedFlag;
import com.federatedinsurance.crs.ws.enums.SwTaskSignOnFlag;
import com.federatedinsurance.crs.ws.enums.SwVps198LaserStatusFlag;
import com.federatedinsurance.crs.ws.enums.SwVpsReportFileStatusFlag;
import com.federatedinsurance.crs.ws.enums.SwVpsStruct132Flag;
import com.federatedinsurance.crs.ws.enums.SwVpsStruct198Flag;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesTs030099 {

	//==== PROPERTIES ====
	//Original name: SW-SIGN-ON-ATTEMPTED-FLAG
	private SwSignOnAttemptedFlag signOnAttemptedFlag = new SwSignOnAttemptedFlag();
	//Original name: SW-NEW-REPORT-NUMBER-FLAG
	private SwNewReportNumberFlag newReportNumberFlag = new SwNewReportNumberFlag();
	/**Original name: SW-REPORT-CHANGED-FLAG<br>
	 * <pre>    THE FOLLOWING FLAG HAS THE SAME MEANING AS THE 'SW-NEW-
	 *     REPORT-NUMBER-FLAG' BUT WILL BE USED FOR THE BALANCING
	 *     REPORT FILE.</pre>*/
	private SwReportChangedFlag reportChangedFlag = new SwReportChangedFlag();
	//Original name: SW-NEW-VPS-REPORT-NUMBER-FLAG
	private SwNewVpsReportNumberFlag newVpsReportNumberFlag = new SwNewVpsReportNumberFlag();
	//Original name: SW-NEW-INHOUSE-REPT-NMBR-FLAG
	private SwNewInhouseReptNmbrFlag newInhouseReptNmbrFlag = new SwNewInhouseReptNmbrFlag();
	//Original name: SW-REPORT-ON-FILE-FLAG
	private SwReportOnFileFlag reportOnFileFlag = new SwReportOnFileFlag();
	//Original name: SW-COM-FILE-132-STATUS-FLAG
	private SwComFile132StatusFlag comFile132StatusFlag = new SwComFile132StatusFlag();
	//Original name: SW-COM-FILE-131-STATUS-FLAG
	private SwComFile131StatusFlag comFile131StatusFlag = new SwComFile131StatusFlag();
	//Original name: SW-BAL-FILE-STATUS-FLAG
	private SwBalFileStatusFlag balFileStatusFlag = new SwBalFileStatusFlag();
	//Original name: SW-IN-HOUSE-FILE-STATUS-FLAG
	private SwInHouseFileStatusFlag inHouseFileStatusFlag = new SwInHouseFileStatusFlag();
	//Original name: SW-IN-HOUSE-STRUCT-132-FLAG
	private SwInHouseStruct132Flag inHouseStruct132Flag = new SwInHouseStruct132Flag();
	//Original name: SW-IN-HOUSE-STRUCT-198-FLAG
	private SwInHouseStruct198Flag inHouseStruct198Flag = new SwInHouseStruct198Flag();
	//Original name: SW-VPS-STRUCT-132-FLAG
	private SwVpsStruct132Flag vpsStruct132Flag = new SwVpsStruct132Flag();
	//Original name: SW-VPS-STRUCT-198-FLAG
	private SwVpsStruct198Flag vpsStruct198Flag = new SwVpsStruct198Flag();
	//Original name: SW-LASER-198-STATUS-FLAG
	private SwLaser198StatusFlag laser198StatusFlag = new SwLaser198StatusFlag();
	//Original name: SW-VPS-REPORT-FILE-STATUS-FLAG
	private SwVpsReportFileStatusFlag vpsReportFileStatusFlag = new SwVpsReportFileStatusFlag();
	//Original name: SW-VPS-198-LASER-STATUS-FLAG
	private SwVps198LaserStatusFlag vps198LaserStatusFlag = new SwVps198LaserStatusFlag();
	//Original name: SW-TASK-SIGN-ON-FLAG
	private SwTaskSignOnFlag taskSignOnFlag = new SwTaskSignOnFlag();
	//Original name: SW-PRTR-FILE-OPEN-FLAG
	private SwPrtrFileOpenFlag prtrFileOpenFlag = new SwPrtrFileOpenFlag();

	//==== METHODS ====
	public SwBalFileStatusFlag getBalFileStatusFlag() {
		return balFileStatusFlag;
	}

	public SwComFile131StatusFlag getComFile131StatusFlag() {
		return comFile131StatusFlag;
	}

	public SwComFile132StatusFlag getComFile132StatusFlag() {
		return comFile132StatusFlag;
	}

	public SwInHouseFileStatusFlag getInHouseFileStatusFlag() {
		return inHouseFileStatusFlag;
	}

	public SwInHouseStruct132Flag getInHouseStruct132Flag() {
		return inHouseStruct132Flag;
	}

	public SwInHouseStruct198Flag getInHouseStruct198Flag() {
		return inHouseStruct198Flag;
	}

	public SwLaser198StatusFlag getLaser198StatusFlag() {
		return laser198StatusFlag;
	}

	public SwNewInhouseReptNmbrFlag getNewInhouseReptNmbrFlag() {
		return newInhouseReptNmbrFlag;
	}

	public SwNewReportNumberFlag getNewReportNumberFlag() {
		return newReportNumberFlag;
	}

	public SwNewVpsReportNumberFlag getNewVpsReportNumberFlag() {
		return newVpsReportNumberFlag;
	}

	public SwPrtrFileOpenFlag getPrtrFileOpenFlag() {
		return prtrFileOpenFlag;
	}

	public SwReportChangedFlag getReportChangedFlag() {
		return reportChangedFlag;
	}

	public SwReportOnFileFlag getReportOnFileFlag() {
		return reportOnFileFlag;
	}

	public SwSignOnAttemptedFlag getSignOnAttemptedFlag() {
		return signOnAttemptedFlag;
	}

	public SwTaskSignOnFlag getTaskSignOnFlag() {
		return taskSignOnFlag;
	}

	public SwVps198LaserStatusFlag getVps198LaserStatusFlag() {
		return vps198LaserStatusFlag;
	}

	public SwVpsReportFileStatusFlag getVpsReportFileStatusFlag() {
		return vpsReportFileStatusFlag;
	}

	public SwVpsStruct132Flag getVpsStruct132Flag() {
		return vpsStruct132Flag;
	}

	public SwVpsStruct198Flag getVpsStruct198Flag() {
		return vpsStruct198Flag;
	}
}
