/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0R90P0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0r90p0 {

	//==== PROPERTIES ====
	//Original name: CF-BUS-OBJ-NM-LIST-KEY
	private String busObjNmListKey = "XZ_GET_NOT_CER_POL_LIST_KEY";
	//Original name: CF-BUS-OBJ-NM-LIST-DTL
	private String busObjNmListDtl = "XZ_GET_NOT_CER_POL_LIST_DTL";
	//Original name: FILLER-CF-INVALID-OPERATION
	private String flr1 = "RESPONSE MODULE";
	//Original name: FILLER-CF-INVALID-OPERATION-1
	private String flr2 = " - INVALID";
	//Original name: FILLER-CF-INVALID-OPERATION-2
	private String flr3 = "OPERATION";
	//Original name: CF-MAX-POL-LIST-DTL
	private int maxPolListDtl = 50;

	//==== METHODS ====
	public String getBusObjNmListKey() {
		return this.busObjNmListKey;
	}

	public String getBusObjNmListDtl() {
		return this.busObjNmListDtl;
	}

	public String getInvalidOperationFormatted() {
		return MarshalByteExt.bufferToStr(getInvalidOperationBytes());
	}

	/**Original name: CF-INVALID-OPERATION<br>*/
	public byte[] getInvalidOperationBytes() {
		byte[] buffer = new byte[Len.INVALID_OPERATION];
		return getInvalidOperationBytes(buffer, 1);
	}

	public byte[] getInvalidOperationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public int getMaxPolListDtl() {
		return this.maxPolListDtl;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 15;
		public static final int FLR2 = 11;
		public static final int FLR3 = 9;
		public static final int INVALID_OPERATION = FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
