/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-HEADER-FOUND-SW<br>
 * Variable: WS-HEADER-FOUND-SW from program HALRLOMG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsHeaderFoundSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FND = 'Y';
	public static final char NOT_FND = 'N';

	//==== METHODS ====
	public void setHeaderFoundSw(char headerFoundSw) {
		this.value = headerFoundSw;
	}

	public char getHeaderFoundSw() {
		return this.value;
	}

	public boolean isFnd() {
		return value == FND;
	}

	public void setFnd() {
		value = FND;
	}

	public void setNotFnd() {
		value = NOT_FND;
	}
}
