/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-SERVER-MESSAGE<br>
 * Variable: L-SERVER-MESSAGE from program TS547099<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServerMessage extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServerMessage() {
	}

	public LServerMessage(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVER_MESSAGE;
	}

	/**Original name: L-SM-LENGTH<br>*/
	public short getLength2() {
		return readBinaryShort(Pos.LENGTH2);
	}

	/**Original name: L-SM-MESSAGE<br>*/
	public char getMessage() {
		return readChar(Pos.MESSAGE);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVER_MESSAGE = 1;
		public static final int LENGTH2 = L_SERVER_MESSAGE;
		public static final int FLR1 = LENGTH2 + Len.LENGTH2;
		public static final int MESSAGE = FLR1 + Len.FLR1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int LENGTH2 = 2;
		public static final int FLR1 = 2;
		public static final int MESSAGE = 1;
		public static final int L_SERVER_MESSAGE = LENGTH2 + MESSAGE + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
