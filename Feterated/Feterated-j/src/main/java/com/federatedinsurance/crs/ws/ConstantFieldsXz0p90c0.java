/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P90C0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p90c0 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-CERTIFICATES
	private short maxCertificates = ((short) 2500);
	//Original name: CF-NI-NULL
	private short niNull = ((short) -1);
	//Original name: CF-LINE-2-START
	private short line2Start = ((short) 46);
	//Original name: CF-LINE-3-START
	private short line3Start = ((short) 91);
	//Original name: CF-OTH-INF-DLM
	private String othInfDlm = "";
	//Original name: CF-CERT-REC-TYP-CD
	private String certRecTypCd = "CERT";
	//Original name: CF-MANUAL-IND
	private char manualInd = 'N';
	//Original name: CF-YES
	private char yes = 'Y';
	//Original name: CF-GET-CERT-LIS-PGM
	private String getCertLisPgm = "XZC05090";
	//Original name: CF-SP-ADD-ACT-NOT-REC-SVC
	private String spAddActNotRecSvc = "XZ0X0012";
	//Original name: CF-SC-ISSUED
	private char scIssued = 'S';
	//Original name: FILLER-CF-FLD-ISS-INS-COPY-CER
	private String flr1 = "@Field Issued/Insured Copy";
	//Original name: CF-CI-CER-LIS-CHANNEL
	private String ciCerLisChannel = "CERLISCHN";
	//Original name: CF-CI-CER-LIS-CONTAINER
	private String ciCerLisContainer = "CERLISCTA";
	//Original name: CF-OI-ATTENTION
	private String oiAttention = "ATT";
	//Original name: CF-OI-IN-CARE-OF
	private String oiInCareOf = "ICO";

	//==== METHODS ====
	public short getMaxCertificates() {
		return this.maxCertificates;
	}

	public short getNiNull() {
		return this.niNull;
	}

	public short getLine2Start() {
		return this.line2Start;
	}

	public short getLine3Start() {
		return this.line3Start;
	}

	public String getOthInfDlm() {
		return this.othInfDlm;
	}

	public String getOthInfDlmFormatted() {
		return Functions.padBlanks(getOthInfDlm(), Len.OTH_INF_DLM);
	}

	public String getCertRecTypCd() {
		return this.certRecTypCd;
	}

	public char getManualInd() {
		return this.manualInd;
	}

	public char getYes() {
		return this.yes;
	}

	public String getGetCertLisPgm() {
		return this.getCertLisPgm;
	}

	public String getSpAddActNotRecSvc() {
		return this.spAddActNotRecSvc;
	}

	public char getScIssued() {
		return this.scIssued;
	}

	/**Original name: CF-FLD-ISS-INS-COPY-CER<br>*/
	public byte[] getFldIssInsCopyCerBytes() {
		byte[] buffer = new byte[Len.FLD_ISS_INS_COPY_CER];
		return getFldIssInsCopyCerBytes(buffer, 1);
	}

	public byte[] getFldIssInsCopyCerBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getCiCerLisChannel() {
		return this.ciCerLisChannel;
	}

	public String getCiCerLisChannelFormatted() {
		return Functions.padBlanks(getCiCerLisChannel(), Len.CI_CER_LIS_CHANNEL);
	}

	public String getCiCerLisContainer() {
		return this.ciCerLisContainer;
	}

	public String getCiCerLisContainerFormatted() {
		return Functions.padBlanks(getCiCerLisContainer(), Len.CI_CER_LIS_CONTAINER);
	}

	public String getOiAttention() {
		return this.oiAttention;
	}

	public String getOiInCareOf() {
		return this.oiInCareOf;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CI_CER_LIS_CONTAINER = 16;
		public static final int CI_CER_LIS_CHANNEL = 16;
		public static final int FLR1 = 120;
		public static final int FLD_ISS_INS_COPY_CER = FLR1;
		public static final int OTH_INF_DLM = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
