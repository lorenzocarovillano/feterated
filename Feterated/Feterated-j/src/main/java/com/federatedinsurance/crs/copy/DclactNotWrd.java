/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotWrd;

/**Original name: DCLACT-NOT-WRD<br>
 * Variable: DCLACT-NOT-WRD from copybook XZH00008<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclactNotWrd implements IActNotWrd {

	//==== PROPERTIES ====
	//Original name: CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: ST-WRD-SEQ-CD
	private String stWrdSeqCd = DefaultValues.stringVal(Len.ST_WRD_SEQ_CD);

	//==== METHODS ====
	public String getXzh005ActNotWrdRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzh005ActNotWrdRowBytes());
	}

	public byte[] getXzh005ActNotWrdRowBytes() {
		byte[] buffer = new byte[Len.XZH005_ACT_NOT_WRD_ROW];
		return getXzh005ActNotWrdRowBytes(buffer, 1);
	}

	public byte[] getXzh005ActNotWrdRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, stWrdSeqCd, Len.ST_WRD_SEQ_CD);
		return buffer;
	}

	public void initXzh005ActNotWrdRowSpaces() {
		csrActNbr = "";
		notPrcTs = "";
		polNbr = "";
		stWrdSeqCd = "";
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	@Override
	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	@Override
	public void setStWrdSeqCd(String stWrdSeqCd) {
		this.stWrdSeqCd = Functions.subString(stWrdSeqCd, Len.ST_WRD_SEQ_CD);
	}

	@Override
	public String getStWrdSeqCd() {
		return this.stWrdSeqCd;
	}

	public String getStWrdSeqCdFormatted() {
		return Functions.padBlanks(getStWrdSeqCd(), Len.ST_WRD_SEQ_CD);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int POL_NBR = 25;
		public static final int ST_WRD_SEQ_CD = 5;
		public static final int XZH005_ACT_NOT_WRD_ROW = CSR_ACT_NBR + NOT_PRC_TS + POL_NBR + ST_WRD_SEQ_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
