/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.lang.collection.IAfSet;
import com.bphx.ctu.af.lang.collection.creation.CollectionCreator;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.commons.data.dao.Xz0b90s0GenericDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.copy.Xz0690co;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.DfhcommareaXzc01090;
import com.federatedinsurance.crs.ws.DfhcommareaXzc02090;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsXz0a90s1Row;
import com.federatedinsurance.crs.ws.Xz0b90s0Data;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.federatedinsurance.crs.ws.redefines.PpcErrorHandlingParms;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.SqlFunctions;
import com.modernsystems.programs.Programs;

/**Original name: XZ0B90S0<br>
 * <pre>AUTHOR.       ANTHONY LINDSAY.
 * DATE-WRITTEN. 16 MAR 2010.
 * *****************************************************************
 * *                                                              **
 * * PROGRAM TITLE - LOCATE ACTIVE PENDING CANCEL/TERMINATE       **
 * *                 POLICY LIST                                  **
 *                     XREF OBJ NM: XZ_ACY_PND_CNC_TMN_POL_LIST_BPO*
 *                     UOW        : XZ_ACY_PND_CNC_TMN_POL_LIST    *
 *                     OPERATIONS : GetAcyPndCncTmnPolListByAct    *
 * *                                                              **
 * * PURPOSE -  CALL THE LOCATE POLICY SERVICE AND RETURN POLICY  **
 * *            LIST, DETERMINE IF POLICY IS ACTIVE, IF ACTIVE    **
 * *            FIND POLICY PENDING CANCEL & TERMINATION STATUS.  **
 * *            CREATE OUTPUT ROW FOR ALL ACTIVE POLICIES.        **
 * *                                                              **
 * * PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS  **
 * *                       LINKED TO BY THE FRAMEWORK DRIVER.     **
 * *                                                              **
 * *                                                              **
 * * DATA ACCESS METHODS - UMT STORAGE RECORDS                    **
 * *                       DB2 DATABASE                           **
 * *                                                              **
 * *****************************************************************
 * *****************************************************************
 * * NOTE: THIS LOG FOR INFRASTRUCTURE USE ONLY FOR TEMPLATE      **
 * *       VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR      **
 * *       APPLICATION CODING.                                    **
 * *     T E M P L A T E   M A I N T E N A N C E   L O G          **
 * * CASE#     DATE       PROG       DESCRIPTION                  **
 * * --------  ---------  --------   -----------------------------**
 * * TS129     06/13/2006 E404LJL    TEMPLATE CREATED             **
 * * YJ249     04/27/2007 E404NEM    STDS CHGS                    **
 * * TS130     12/28/2007 E404JSP    CHANGED A FEW BUGS           **
 * * TMP09     07/16/2009 E404JSP    FIXED TYPOS, BETTER SQL      **
 * *****************************************************************
 * *****************************************************************
 * *               M A I N T E N A N C E    L O G                 **
 * *                                                              **
 * * SI #         DATE      PROG              DESCRIPTION         **
 * * -------   ----------  -------   -----------------------------**
 * *TO07602-22 03/16/2010  E404ABL   INITIAL PROGRAM              **
 * * BX000464  06/07/2010  E404ABL   SKIP QUOTES AND UNISSUED.    **
 * * BX000479  08/03/2010  E404ABL   RETURN PRIOR POLICY TERMS    **
 * *                                 UP TO 6 MONTHS OLD.          **
 * * PP02635   10/20/2010  E404ABL   CHANGE ERROR HANDLING FOR    **
 * *                                 PND CNC/TMN SERVICE CALLS    **
 * * BX000546  07/26/2011  E404GCL   FIX TERMINATION FLAG POLICY  **
 * *                                 LIST ERRORS.                 **
 * * 20163     08/24/2019  E404TJJ   FEDONE IMPLEMENTATION        **
 *                                                                **
 * *****************************************************************</pre>*/
public class Xz0b90s0 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private Xz0b90s0GenericDao xz0b90s0GenericDao = new Xz0b90s0GenericDao(dbAccessStatus);
	private IAfSet<String> channelSet = CollectionCreator.getStringFactory().createSet();
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0b90s0Data ws = new Xz0b90s0Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private Dfhcommarea dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, Dfhcommarea dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0b90s0 getInstance() {
		return (Programs.getInstance(Xz0b90s0.class));
	}

	/**Original name: 1000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   MAIN PROCESSING CONTROL                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-PRC-ACY-PND-CNC-TMN-POL.
		prcAcyPndCncTmnPol();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *  INITIALIZATION CONTROL                                        *
	 *                                                                *
	 * ****************************************************************
	 * * INITIALIZE ERROR/WARNING STORAGE</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//* VALIDATE UBOC IN COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2100-READ-REQ-UMT-ROW.
		readReqUmtRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-GET-CURRENT-DATE.
		getCurrentDate();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2300-GET-POL-CUTOFF-DATE.
		getPolCutoffDate();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-READ-REQ-UMT-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR THE BPO INPUT ROW                     *
	 * *****************************************************************</pre>*/
	private void readReqUmtRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: INITIALIZE WS-XZ0A90S0-ROW.
		initWsXz0a90s0Row();
		// COB_CODE: SET HALRURQA-READ-FUNC      TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM          TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNm());
		// COB_CODE: MOVE +1                     TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(1);
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-XZ0A90S0-ROW.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsXz0a90s0Row());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: IF HALRURQA-REC-NOT-FOUND
		//               GO TO 2100-EXIT
		//           END-IF.
		if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET ETRA-CICS-READ-UMT  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '2100-READ-REQ-UMT-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-READ-REQ-UMT-ROW");
			// COB_CODE: MOVE 'NO RECORD FOUND ON REQ MSG STORE'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO RECORD FOUND ON REQ MSG STORE");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HALRURQA-BUS-OBJ-NM='
			//                  HALRURQA-BUS-OBJ-NM
			//                  ';'
			//                  'HALRURQA-REC-SEQ='
			//                  HALRURQA-REC-SEQ
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HALRURQA-BUS-OBJ-NM=").append(ws.getWsHalrurqaLinkage().getBusObjNmFormatted())
							.append(";").append("HALRURQA-REC-SEQ=").append(ws.getWsHalrurqaLinkage().getRecSeqFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
	}

	/**Original name: 2200-GET-CURRENT-DATE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FIND THE CURRENT DATE                                          *
	 * *****************************************************************</pre>*/
	private void getCurrentDate() {
		// COB_CODE: EXEC SQL
		//               SET :WS-CURRENT-DATE = CURRENT DATE
		//           END-EXEC.
		ws.getWorkingStorageArea().setCurrentDate(SqlFunctions.currentDate());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 2200-EXIT
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'PND-CNC-TMN-POL-LIS'
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("PND-CNC-TMN-POL-LIS");
			// COB_CODE: MOVE '2200-GET-CURRENT-DATE'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2200-GET-CURRENT-DATE");
			// COB_CODE: MOVE 'CURRENT DATE SQL FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CURRENT DATE SQL FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2200-EXIT
			return;
		}
	}

	/**Original name: 2300-GET-POL-CUTOFF-DATE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FIND THE CUTOFF DATE THAT WILL BE USED TO DETERMINE WHETHER A  *
	 *  POLICY TERM WILL BE RETURNED.                                  *
	 * *****************************************************************</pre>*/
	private void getPolCutoffDate() {
		// COB_CODE: EXEC SQL
		//               SET :WS-POL-CUTOFF-DT = DATE(:WS-CURRENT-DATE) - 6 MONTHS
		//           END-EXEC.
		ws.getWorkingStorageArea().setPolCutoffDt(xz0b90s0GenericDao.setHostVarByWsCurrentDate(ws.getWorkingStorageArea().getCurrentDate(),
				ws.getWorkingStorageArea().getPolCutoffDt()));
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 2300-EXIT
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'PND-CNC-TMN-POL-LIS'
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("PND-CNC-TMN-POL-LIS");
			// COB_CODE: MOVE '2300-GET-POL-CUTOFF-DATE'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2300-GET-POL-CUTOFF-DATE");
			// COB_CODE: MOVE 'CALCULATE POLICY CUTOFF DATE SQL FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CALCULATE POLICY CUTOFF DATE SQL FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2300-EXIT
			return;
		}
	}

	/**Original name: 3000-PRC-ACY-PND-CNC-TMN-POL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE PASSED INFO AND BUILD THE PARMS AREA FOR THE          *
	 *  SAMPLE LOCATE PROCESSING.  PERFORM THE LOCATE AND              *
	 *  CREATE RESPONSE DATA FOR THE RETURNED OBJECTS.                 *
	 * *****************************************************************
	 * * FIND ALL POLICIES FOR ACCOUNT.</pre>*/
	private void prcAcyPndCncTmnPol() {
		// COB_CODE: PERFORM 3100-GET-POLICY-ROWS.
		getPolicyRows();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* A NON-LOGGABLE WARNING IS RETURNED IF THE MAXIMUM NUMBER
		//* OF ROWS ARE BEING RETURNED AND THERE ARE MORE ON THE
		//* DATABASE THAT ARE NOT BEING RETURNED.
		//* THE MAXIMUM NUMBER IS PASSED IN ON THE BPO COPYBOOK.
		// COB_CODE: IF WS-ROW-COUNT = XZA9S0-MAX-ROWS-RETURNED
		//               PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
		//           END-IF.
		if (ws.getWorkingStorageArea().getRowCount() == ws.getWsXz0a90s0Row().getMaxRowsReturned()) {
			// COB_CODE: SET WS-NON-LOGGABLE-WARNING
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setWarning();
			// COB_CODE: MOVE WS-BUS-OBJ-NM      TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getBusObjNm());
			// COB_CODE: MOVE 'PND-CNC-TMN-POL-LIS'
			//                                   TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("PND-CNC-TMN-POL-LIS");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO UWRN-WARNING-CODE
			ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE WS-ROW-COUNT       TO EA-01-MAX-ROWS-RETURNED
			ws.getEa01MaxRowsReturnedMsg().setEa01MaxRowsReturned(ws.getWorkingStorageArea().getRowCount());
			// COB_CODE: MOVE EA-01-MAX-ROWS-RETURNED-MSG
			//                                   TO WS-NONLOG-ERR-ALLTXT-TEXT
			ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getEa01MaxRowsReturnedMsg().getEa01MaxRowsReturnedMsgFormatted());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
		}
		//* A NON-LOGGABLE WARNING IS RETURNED IF SEARCH RETURNS NOTHING
		// COB_CODE: IF WS-NOTHING-FOUND
		//               PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
		//           END-IF.
		if (ws.getWorkingStorageArea().isNothingFound()) {
			// COB_CODE: SET WS-NON-LOGGABLE-WARNING
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setWarning();
			// COB_CODE: MOVE WS-BUS-OBJ-NM      TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getBusObjNm());
			// COB_CODE: MOVE 'PND-CNC-TMN-POL-LIS'
			//                                   TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("PND-CNC-TMN-POL-LIS");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO UWRN-WARNING-CODE
			ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE XZA9S0-ACT-NBR     TO EA-02-ACT-NBR
			ws.setEa02ActNbr(ws.getWsXz0a90s0Row().getActNbr());
			// COB_CODE: MOVE EA-02-NO-ACY-POL-FOUND-MSG
			//                                   TO WS-NONLOG-ERR-ALLTXT-TEXT
			ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getEa02NoAcyPolFoundMsgFormatted());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
		}
	}

	/**Original name: 3100-GET-POLICY-ROWS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL LOCATE POLICY SERVICE WHICH WILL DRIVE THE BPO.           *
	 * *****************************************************************</pre>*/
	private void getPolicyRows() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 3120-SET-LOC-POL-INPUT.
		setLocPolInput();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM   (CF-SN-GET-POL-TRM-LIS-BY-ACT)
		//               CHANNEL   (CF-CI-CICS-CHANNEL)
		//               RESP      (WS-RESPONSE-CODE)
		//               RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0B90S0", execContext).channel(ws.getConstantFields().getContainerInfo().getCicsChannelFormatted())
				.link(ws.getConstantFields().getSnGetPolTrmLisByAct(), new Xzc06090());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE 'PND-CNC-TMN-POL-LIS'
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("PND-CNC-TMN-POL-LIS");
			// COB_CODE: MOVE '3100-RETURN-LOCATE-POLICY-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3100-RETURN-LOCATE-POLICY-ROW");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'LOCATE_POLICY CICS CALL ERROR'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LOCATE_POLICY CICS CALL ERROR");
			// COB_CODE: STRING ' EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, " EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getEibrespCdAsString(), ".  EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(),
					".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: PERFORM 3130-GET-CONTAINER.
		getContainer();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SN-GET-POL-TRM-LIS-BY-ACT
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getSnGetPolTrmLisByAct());
		// COB_CODE: MOVE SPIO-RET-CODE          TO PPC-ERROR-RETURN-CODE.
		ws.getPpcErrorHandlingParms().setErrorReturnCode(TruncAbs.toShort(ws.getXz0690co().getRetCode().getRetCd(), 4));
		// COB_CODE: MOVE SPIO-ERR-LIS           TO PPC-FATAL-ERROR-MESSAGE.
		ws.getPpcErrorHandlingParms().setFatalErrorMessage(ws.getXz0690co().getErrLisFormatted());
		// COB_CODE: MOVE SPIO-WRN-LIS           TO PPC-GROUP-NON-LOG-MSG
		//                                          PPC-GROUP-WNG-MSG.
		ws.getPpcErrorHandlingParms().setGroupNonLogMsg(ws.getXz0690co().getWrnLisFormatted());
		ws.getPpcErrorHandlingParms().setGroupWngMsg(ws.getXz0690co().getWrnLisFormatted());
		// COB_CODE: MOVE '3100-RETURN-LOCATE-POLICY-ROW'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("3100-RETURN-LOCATE-POLICY-ROW");
		// COB_CODE: MOVE 'GET_POL_TRM_LIS_SVC'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("GET_POL_TRM_LIS_SVC");
		// COB_CODE: MOVE 'GET POLICY LIST'      TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("GET POLICY LIST");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: PERFORM 3180-PROCESS-POLICIES-OUTPUT.
		rng3180A();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
	}

	/**Original name: 3120-SET-LOC-POL-INPUT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  MOVE IN THE VALUES WE NEED TO CALL THE                         *
	 *  GetPolicyTermListByAccount service*
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setLocPolInput() {
		TpOutputData tsOutputData = null;
		// COB_CODE: INITIALIZE WS-XZC0690I-ROW.
		initWsXzc0690iRow();
		// COB_CODE: MOVE XZA9S0-ACT-NBR         TO SPII-ACT-NBR.
		ws.getXzc0690i().setActNbr(ws.getWsXz0a90s0Row().getActNbr());
		//    MOVE WS-CURRENT-DATE        TO FWBT10I-AS-OF-DT.
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER (CF-CI-SVC-IN-CONTAINER)
		//               CHANNEL   (CF-CI-CICS-CHANNEL)
		//               FROM      (WS-XZC0690I-ROW)
		//               FLENGTH   (LENGTH OF WS-XZC0690I-ROW)
		//               RESP      (WS-RESPONSE-CODE)
		//               RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xz0b90s0Data.Len.WS_XZC0690I_ROW);
		tsOutputData.setData(ws.getWsXzc0690iRowBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCicsChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getSvcInContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCicsChannelFormatted());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3120-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SN-GET-POL-TRM-LIS-BY-ACT
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSnGetPolTrmLisByAct());
			// COB_CODE: MOVE '3120-SET-LOC-POL-INPUT'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3120-SET-LOC-POL-INPUT");
			// COB_CODE: MOVE 'GETPOLTRMLISBYACT SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("GETPOLTRMLISBYACT SERVICE CALL FAILED.");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3120-SET-LOC-POL-INPUT'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("3120-SET-LOC-POL-INPUT").append(".  FAILED MODULE EIBRESP CODE IS ")
							.append(ws.getWorkingStorageArea().getEibrespCdFormatted()).append(".  FAILED MODULE EIBRESP2 CODE IS ")
							.append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3120-EXIT
			return;
		}
	}

	/**Original name: 3130-GET-CONTAINER_FIRST_SENTENCES<br>*/
	private void getContainer() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE WS-XZ0690CO-ROW.
		initWsXz0690coRow();
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CI-SVC-OUT-CONTAINER)
		//               CHANNEL    (CF-CI-CICS-CHANNEL)
		//               INTO       (WS-XZ0690CO-ROW)
		//               FLENGTH    (LENGTH OF WS-XZ0690CO-ROW)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xz0b90s0Data.Len.WS_XZ0690CO_ROW);
		Channel.read(execContext, ws.getConstantFields().getContainerInfo().getCicsChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getSvcOutContainerFormatted(), tsOutputData);
		ws.setWsXz0690coRowBytes(tsOutputData.getData());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             OR
		//              SPIO-RC-FAULT
		//               GO TO 3130-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL
				|| ws.getXz0690co().getRetCode().isSpioRcFault()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SN-GET-POL-TRM-LIS-BY-ACT
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSnGetPolTrmLisByAct());
			// COB_CODE: MOVE '3130-GET-CONTAINER'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3130-GET-CONTAINER");
			// COB_CODE: MOVE 'GETPOLTRMLISBYACT SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("GETPOLTRMLISBYACT SERVICE CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3130-GET-CONTAINER'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'              DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3130-GET-CONTAINER", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3130-EXIT
			return;
		}
	}

	/**Original name: 3180-A<br>
	 * <pre>*****************************************************************
	 *  FIND ALL ACTIVE POLICIES FOR THE ACCOUNT PASSED IN.  MOVE      *
	 *  ACTIVE POLICIES TO COMMUNICATION COPYBOOK. SKIP NON ACTIVE     *
	 *  POLICIES. THESE POLICIES ARE ORDERED BY OGN_EFF_DT,            *
	 *  QUOTE_SEQUENCE_NBR, LOB_CD, AND POLICY_ID. THE MAXIMUM THAT    *
	 *  CAN BE RETURNED IS 350.                                        *
	 * *****************************************************************
	 *  CHECK TO SEE IF WE HAVE REACHED THE END OF LOCATE-POLICY
	 *  OUTPUT.</pre>*/
	private String a() {
		// COB_CODE: IF SPIO-ACT-NBR(SS-LP) = SPACES
		//               GO TO 3180-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getActNbr())) {
			// COB_CODE: GO TO 3180-EXIT
			return "";
		}
		// CHECK IF POLICY IS ACTIVE, ISSUED, NON-QUOTE AND WITHIN THE
		// CUTOFF DATE. SKIP TO NEXT POLICY IF INACTIVE, UNISSED,
		// OR A QUOTE.
		// ONLY NEED TO SKIP IF IT IS A NEW BIZ OR RENEWAL TRANSACTION THAT
		// HAS NOT YET BEEN ISSUED.
		// SKIP IF POLICY EXPIRATION DAY IS PRIOR TO POLICY CUTOFF DATE.
		// SKIP IF POLICY ACTIVE INDICATOR IS NOT FILLED IN.
		// COB_CODE:      IF SPIO-CS-CD(SS-LP) = CF-CANCELLED-POL-STA-CD
		//                  OR
		//                   SPIO-CS-CD(SS-LP) = CF-TERMINATED-POL-STA-CD
		//                  OR
		//                   SPIO-QTE-NBR(SS-LP) NOT = +0
		//                  OR
		//           * ONLY NEED TO SKIP IF IT IS A NEW BIZ OR RENEWAL TRANSACTION THAT
		//           * HAS NOT YET BEEN ISSUED.
		//                   (SPIO-PND-FLG(SS-LP) = CF-YES
		//                  AND
		//                    (SPIO-TRS-TYP-CD(SS-LP) = CF-NEW-BIZ-TRS-TYP-CD
		//                  OR
		//                     SPIO-TRS-TYP-CD(SS-LP) = CF-RNL-TRS-TYP-CD))
		//           * SKIP IF POLICY EXPIRATION DAY IS PRIOR TO POLICY CUTOFF DATE.
		//                  OR
		//                   SPIO-EXP-DT(SS-LP) < WS-POL-CUTOFF-DT
		//                  OR
		//           * SKIP IF POLICY ACTIVE INDICATOR IS NOT FILLED IN.
		//                   SPIO-ACY-TRM-IND(SS-LP) = SPACE
		//                    END-IF
		//                END-IF.
		if (ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getCsCd() == ws.getConstantFields().getCancelledPolStaCd()
				|| ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getCsCd() == ws.getConstantFields().getTerminatedPolStaCd()
				|| ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getQteNbr() != 0
				|| ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTrsDtl().getPndFlg() == ws.getConstantFields().getYes() && (ws
						.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTrsDtl().getTrsTypCd() == ws.getConstantFields().getNewBizTrsTypCd()
						|| ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTrsDtl().getTrsTypCd() == ws.getConstantFields()
								.getRnlTrsTypCd())
				|| Conditions.lt(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getExpDt(), ws.getWorkingStorageArea().getPolCutoffDt())
				|| Conditions.eq(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getAcyTrmInd(), Types.SPACE_CHAR)) {
			// COB_CODE: IF SS-LP-MAX
			//               GO TO 3180-EXIT
			//           ELSE
			//               GO TO 3180-A
			//           END-IF
			if (ws.getSubscripts().isLpMax()) {
				// COB_CODE: GO TO 3180-EXIT
				return "";
			} else {
				// COB_CODE: ADD +1              TO SS-LP
				ws.getSubscripts().setLp(Trunc.toShort(1 + ws.getSubscripts().getLp(), 4));
				// COB_CODE: GO TO 3180-A
				return "3180-A";
			}
		}
		// COB_CODE: INITIALIZE XZA9S1-POLICY-ROW.
		initRow();
		// MOVE LOCATE_POLICY OUTPUT FIELDS.
		// COB_CODE: MOVE SPIO-POL-KEY(SS-LP)    TO XZA9S1-POLICY-ID.
		ws.getWsXz0a90s1Row().getRow().setPolicyId(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getPolKey());
		// COB_CODE: MOVE CF-NIN                 TO XZA9S1-RLT-TYP-CD.
		ws.getWsXz0a90s1Row().getRow().setRltTypCd(ws.getConstantFields().getNin());
		// COB_CODE: MOVE SPIO-POL-NBR(SS-LP)    TO XZA9S1-POL-NBR.
		ws.getWsXz0a90s1Row().getRow().setPolNbr(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getPolNbr());
		// COB_CODE: MOVE SPIO-EFF-DT(SS-LP)     TO XZA9S1-OGN-EFF-DT.
		ws.getWsXz0a90s1Row().getRow().setOgnEffDt(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getEffDt());
		// COB_CODE: MOVE SPIO-EXP-DT(SS-LP)     TO XZA9S1-PLN-EXP-DT.
		ws.getWsXz0a90s1Row().getRow().setPlnExpDt(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getExpDt());
		// COB_CODE: MOVE SPIO-QTE-NBR(SS-LP)    TO XZA9S1-QUOTE-SEQUENCE-NBR.
		ws.getWsXz0a90s1Row().getRow().setQuoteSequenceNbr(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getQteNbr());
		// COB_CODE: MOVE SPIO-ACT-NBR(SS-LP)    TO XZA9S1-ACT-NBR.
		ws.getWsXz0a90s1Row().getRow().setActNbr(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getActNbr());
		// COB_CODE: MOVE SPIO-ACT-NBR-FMT(SS-LP)
		//                                       TO XZA9S1-ACT-NBR-FMT.
		ws.getWsXz0a90s1Row().getRow().setActNbrFmt(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getActNbrFmt());
		// COB_CODE: MOVE SPIO-BSE-ST-ABB(SS-LP) TO XZA9S1-PRI-RSK-ST-ABB.
		ws.getWsXz0a90s1Row().getRow().setPriRskStAbb(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getBseStAbb());
		// COB_CODE: MOVE SPIO-BSE-ST-NM(SS-LP)  TO XZA9S1-PRI-RSK-ST-DES.
		ws.getWsXz0a90s1Row().getRow().setPriRskStDes(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getBseStNm());
		// COB_CODE: MOVE SPIO-PRD-CD(SS-LP)     TO XZA9S1-LOB-CD.
		ws.getWsXz0a90s1Row().getRow().setLobCd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getPrdCd());
		// COB_CODE: MOVE SPIO-PRD-DES(SS-LP)    TO XZA9S1-LOB-DES.
		ws.getWsXz0a90s1Row().getRow().setLobDes(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getPrdDes());
		// COB_CODE: MOVE SPIO-TOB-CD(SS-LP)     TO XZA9S1-TOB-CD.
		ws.getWsXz0a90s1Row().getRow().setTobCd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTobCd());
		// COB_CODE: MOVE SPIO-TOB-DES(SS-LP)    TO XZA9S1-TOB-DES.
		ws.getWsXz0a90s1Row().getRow().setTobDes(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTobDes());
		// COB_CODE: MOVE SPIO-SEG-CD(SS-LP)     TO XZA9S1-SEG-CD.
		ws.getWsXz0a90s1Row().getRow().setSegCd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getSegCd());
		// COB_CODE: MOVE SPIO-SEG-DES(SS-LP)    TO XZA9S1-SEG-DES.
		ws.getWsXz0a90s1Row().getRow().setSegDes(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getSegDes());
		// COB_CODE: MOVE SPIO-LA-IND(SS-LP)     TO XZA9S1-LGE-ACT-IND.
		ws.getWsXz0a90s1Row().getRow().setLgeActInd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getLaInd());
		// COB_CODE: MOVE SPIO-MNL-POL-IND(SS-LP)
		//                                       TO XZA9S1-MNL-POL-IND.
		ws.getWsXz0a90s1Row().getRow().setMnlPolInd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getMnlPolInd());
		// COB_CODE: MOVE SPIO-CS-CD(SS-LP)      TO XZA9S1-CUR-STA-CD.
		ws.getWsXz0a90s1Row().getRow().setCurStaCd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getCsCd());
		// COB_CODE: MOVE SPIO-CS-DES(SS-LP)     TO XZA9S1-CUR-STA-DES.
		ws.getWsXz0a90s1Row().getRow().setCurStaDes(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getCsDes());
		// COB_CODE: MOVE SPIO-TRS-TYP-CD(SS-LP) TO XZA9S1-CUR-TRS-TYP-CD.
		ws.getWsXz0a90s1Row().getRow().setCurTrsTypCd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTrsDtl().getTrsTypCd());
		// COB_CODE: MOVE SPIO-TRS-TYP-DES(SS-LP)
		//                                       TO XZA9S1-CUR-TRS-TYP-DES.
		ws.getWsXz0a90s1Row().getRow().setCurTrsTypDes(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTrsDtl().getTrsTypDes());
		// COB_CODE: MOVE SPIO-PND-FLG(SS-LP)    TO XZA9S1-CUR-PND-TRS-IND.
		ws.getWsXz0a90s1Row().getRow().setCurPndTrsInd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTrsDtl().getPndFlg());
		// COB_CODE: MOVE SPIO-TOS-CD(SS-LP)     TO XZA9S1-CUR-OGN-CD.
		ws.getWsXz0a90s1Row().getRow().setCurOgnCd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTrsDtl().getTrsOrtCd());
		// COB_CODE: MOVE SPIO-TOS-DES(SS-LP)    TO XZA9S1-CUR-OGN-DES.
		ws.getWsXz0a90s1Row().getRow().setCurOgnDes(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getTrsDtl().getTrsOrtDes());
		// COB_CODE: MOVE SPIO-LOCV-CDS(SS-LP)   TO XZA9S1-COVERAGE-PARTS.
		ws.getWsXz0a90s1Row().getRow().setCoverageParts(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getLocvCds());
		// COB_CODE: MOVE SPIO-BND-IND(SS-LP)    TO XZA9S1-BOND-IND.
		ws.getWsXz0a90s1Row().getRow().setBondInd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getBndInd());
		// COB_CODE: MOVE SPIO-OFC-CD(SS-LP)     TO XZA9S1-BRANCH-NBR.
		ws.getWsXz0a90s1Row().getRow().setBranchNbr(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getOfcCd());
		// COB_CODE: MOVE SPIO-OFC-DES(SS-LP)    TO XZA9S1-BRANCH-DESC.
		ws.getWsXz0a90s1Row().getRow().setBranchDesc(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getOfcDes());
		// COB_CODE: MOVE SPIO-WRT-PRM-AMT(SS-LP)
		//                                       TO XZA9S1-WRT-PREM-AMT.
		ws.getWsXz0a90s1Row().getRow().setWrtPremAmt(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getWrtPrmAmt());
		// COB_CODE: MOVE SPIO-ACY-TRM-IND(SS-LP)
		//                                       TO XZA9S1-POL-ACT-IND.
		ws.getWsXz0a90s1Row().getRow().setPolActInd(ws.getXz0690co().getPolTrm(ws.getSubscripts().getLp()).getAcyTrmInd());
		// POPULATE POLICY PENDING CANCEL/TERMINATE FIELDS
		// COB_CODE: PERFORM 3181-GET-POL-PND-CNC-INFO.
		getPolPndCncInfo();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3180-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3180-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3182-GET-POL-TMN-INFO.
		getPolTmnInfo();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3180-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3180-EXIT
			return "";
		}
		// WRITE OUTPUT ROW
		// COB_CODE: PERFORM 3183-WRITE-OUTPUT-ROW.
		writeOutputRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3180-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3180-EXIT
			return "";
		}
		// COB_CODE: IF SS-LP-MAX
		//             OR
		//              WS-ROW-COUNT = XZA9S0-MAX-ROWS-RETURNED
		//               GO TO 3180-EXIT
		//           END-IF.
		if (ws.getSubscripts().isLpMax() || ws.getWorkingStorageArea().getRowCount() == ws.getWsXz0a90s0Row().getMaxRowsReturned()) {
			// COB_CODE: GO TO 3180-EXIT
			return "";
		}
		// COB_CODE: code not available
		ws.setIntRegister1(((short) 1));
		ws.getSubscripts().setLp(Trunc.toShort(ws.getIntRegister1() + ws.getSubscripts().getLp(), 4));
		ws.getWorkingStorageArea().setRowCount(Trunc.toShort(ws.getIntRegister1() + ws.getWorkingStorageArea().getRowCount(), 4));
		// COB_CODE: GO TO 3180-A.
		return "3180-A";
	}

	/**Original name: 3181-GET-POL-PND-CNC-INFO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  RETRIEVE PENDING CANCELLATION STATUS INFO.                     *
	 * *****************************************************************
	 *  SET UP INPUT.</pre>*/
	private void getPolPndCncInfo() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE WS-XZC010C1-ROW.
		initWsXzc010c1Row();
		// COB_CODE: MOVE XZA9S1-POL-NBR         TO XZC01I-POL-NBR.
		ws.getWsXzc010c1Row().setiPolNbr(ws.getWsXz0a90s1Row().getRow().getPolNbr());
		// COB_CODE: MOVE XZA9S1-OGN-EFF-DT      TO XZC01I-POL-EFF-DT.
		ws.getWsXzc010c1Row().setiPolEffDt(ws.getWsXz0a90s1Row().getRow().getOgnEffDt());
		// COB_CODE: MOVE XZA9S1-PLN-EXP-DT      TO XZC01I-POL-EXP-DT.
		ws.getWsXzc010c1Row().setiPolExpDt(ws.getWsXz0a90s1Row().getRow().getPlnExpDt());
		// COB_CODE: MOVE XZA9S0-USERID          TO XZC01I-USERID.
		ws.getWsXzc010c1Row().setiUserid(ws.getWsXz0a90s0Row().getUserid());
		// CALL PROGRAM
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (CF-PN-GET-PND-CNC-STATUS)
		//               COMMAREA (WS-XZC010C1-ROW)
		//               LENGTH   (LENGTH OF WS-XZC010C1-ROW)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0B90S0", execContext).commarea(ws.getWsXzc010c1Row()).length(DfhcommareaXzc01090.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getPnGetPndCncStatus(), new Xzc01090());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// CHECK FOR CICS LINK ERROR
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3181-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE CF-PN-GET-PND-CNC-STATUS
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getPnGetPndCncStatus());
			// COB_CODE: MOVE '3181-GET-POL-PND-CNC-INFO'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3181-GET-POL-PND-CNC-INFO");
			// COB_CODE: MOVE 'LINK TO GET PENDING CANCEL STATUS PROGRAM FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO GET PENDING CANCEL STATUS PROGRAM FAILED");
			// COB_CODE: STRING 'POL-NBR='
			//                  XZA9S1-POL-NBR
			//                  '; POL-EFF-DT='
			//                  XZA9S1-OGN-EFF-DT
			//                  '; POL-EXP-DT='
			//                  XZA9S1-PLN-EXP-DT
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "POL-NBR=", ws.getWsXz0a90s1Row().getRow().getPolNbrFormatted(), "; POL-EFF-DT=",
							ws.getWsXz0a90s1Row().getRow().getOgnEffDtFormatted(), "; POL-EXP-DT=",
							ws.getWsXz0a90s1Row().getRow().getPlnExpDtFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3181-EXIT
			return;
		}
		// COB_CODE: MOVE CF-PN-GET-PND-CNC-STATUS
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getPnGetPndCncStatus());
		// COB_CODE: MOVE '3181-GET-POL-PND-CNC-INFO'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("3181-GET-POL-PND-CNC-INFO");
		// COB_CODE: MOVE 'GET PND CNC STA PGM'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("GET PND CNC STA PGM");
		// COB_CODE: MOVE 'CANCEL DATA'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("CANCEL DATA");
		// CHECK FOR ERROR FROM PROGRAM. THE ERROR HANDLING PARMS IN
		// THE PROGRAM'S COPYBOOK ARE THE SAME AS THOSE FOUND IN THE
		// TS020PRO COPYBOOK, SO CAN USE THE SAME ERROR HANDLING LOGIC.
		// COB_CODE: MOVE XZC010-ERROR-HANDLING-PARMS
		//                                       TO PPC-ERROR-HANDLING-PARMS.
		ws.getPpcErrorHandlingParms().setPpcErrorHandlingParmsBytes(ws.getWsXzc010c1Row().getXzc010ErrorHandlingParmsBytes());
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
		// MOVE TO OUTPUT.
		// COB_CODE: MOVE XZC01O-PND-CNC-IND     TO XZA9S1-PND-CNC-IND.
		ws.getWsXz0a90s1Row().getRow().setPndCncInd(ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getPndCncInd());
		// COB_CODE: MOVE XZC01O-SCH-CNC-DT      TO XZA9S1-SCH-CNC-DT.
		ws.getWsXz0a90s1Row().getRow().setSchCncDt(ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getSchCncDt());
		// COB_CODE: MOVE XZC01O-NOT-TYP-CD      TO XZA9S1-NOT-TYP-CD.
		ws.getWsXz0a90s1Row().getRow().setNotTypCd(ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getNotTypCd());
		// COB_CODE: MOVE XZC01O-NOT-TYP-DES     TO XZA9S1-NOT-TYP-DES.
		ws.getWsXz0a90s1Row().getRow().setNotTypDes(ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getNotTypDes());
		// COB_CODE: MOVE XZC01O-NOT-EMP-ID      TO XZA9S1-NOT-EMP-ID.
		ws.getWsXz0a90s1Row().getRow().setNotEmpId(ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getNotEmpId());
		// COB_CODE: MOVE XZC01O-NOT-EMP-NM      TO XZA9S1-NOT-EMP-NM.
		ws.getWsXz0a90s1Row().getRow().setNotEmpNm(ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getNotEmpNm());
		// COB_CODE: MOVE XZC01O-NOT-PRC-DT      TO XZA9S1-NOT-PRC-DT.
		ws.getWsXz0a90s1Row().getRow().setNotPrcDt(ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getNotPrcDt());
	}

	/**Original name: 3182-GET-POL-TMN-INFO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  RETRIEVE TERMINATION STATUS INFO.                              *
	 * *****************************************************************
	 *  SET UP INPUT.</pre>*/
	private void getPolTmnInfo() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE WS-XZC020C1-ROW.
		initWsXzc020c1Row();
		// COB_CODE: MOVE XZA9S1-POL-NBR         TO XZC02I-POL-NBR.
		ws.getWsXzc020c1Row().setiPolNbr(ws.getWsXz0a90s1Row().getRow().getPolNbr());
		// COB_CODE: MOVE XZA9S1-OGN-EFF-DT      TO XZC02I-POL-EFF-DT.
		ws.getWsXzc020c1Row().setiPolEffDt(ws.getWsXz0a90s1Row().getRow().getOgnEffDt());
		// COB_CODE: MOVE XZA9S1-PLN-EXP-DT      TO XZC02I-POL-EXP-DT.
		ws.getWsXzc020c1Row().setiPolExpDt(ws.getWsXz0a90s1Row().getRow().getPlnExpDt());
		// COB_CODE: MOVE XZA9S0-USERID          TO XZC02I-USERID.
		ws.getWsXzc020c1Row().setiUserid(ws.getWsXz0a90s0Row().getUserid());
		// CALL PROGRAM.
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (CF-PN-GET-POL-TMN-STATUS)
		//               COMMAREA (WS-XZC020C1-ROW)
		//               LENGTH   (LENGTH OF WS-XZC020C1-ROW)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0B90S0", execContext).commarea(ws.getWsXzc020c1Row()).length(DfhcommareaXzc02090.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getPnGetPolTmnStatus(), new Xzc02090());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// CHECK FOR CICS LINK ERROR
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3182-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE CF-PN-GET-POL-TMN-STATUS
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getPnGetPolTmnStatus());
			// COB_CODE: MOVE '3182-GET-POL-TMN-INFO'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3182-GET-POL-TMN-INFO");
			// COB_CODE: MOVE 'LINK TO GET TERMINATE STATUS PROGRAM FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO GET TERMINATE STATUS PROGRAM FAILED");
			// COB_CODE: STRING 'POL-NBR='
			//                  XZA9S1-POL-NBR
			//                  '; POL-EFF-DT='
			//                  XZA9S1-OGN-EFF-DT
			//                  '; POL-EXP-DT='
			//                  XZA9S1-PLN-EXP-DT
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "POL-NBR=", ws.getWsXz0a90s1Row().getRow().getPolNbrFormatted(), "; POL-EFF-DT=",
							ws.getWsXz0a90s1Row().getRow().getOgnEffDtFormatted(), "; POL-EXP-DT=",
							ws.getWsXz0a90s1Row().getRow().getPlnExpDtFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3182-EXIT
			return;
		}
		// CHECK FOR ERROR FROM PROGRAM
		// COB_CODE: MOVE CF-PN-GET-POL-TMN-STATUS
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getPnGetPolTmnStatus());
		// COB_CODE: MOVE '3182-GET-POL-TMN-INFO'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("3182-GET-POL-TMN-INFO");
		// COB_CODE: MOVE 'GET POL TMN STA PGM'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("GET POL TMN STA PGM");
		// COB_CODE: MOVE 'TERMINATE DATA'       TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("TERMINATE DATA");
		// CHECK FOR ERROR FROM PROGRAM. THE ERROR HANDLING PARMS IN
		// THE PROGRAM'S COPYBOOK ARE THE SAME AS THOSE FOUND IN THE
		// TS020PRO COPYBOOK, SO CAN USE THE SAME ERROR HANDLING LOGIC.
		// COB_CODE: MOVE XZC020-ERROR-HANDLING-PARMS
		//                                       TO PPC-ERROR-HANDLING-PARMS.
		ws.getPpcErrorHandlingParms().setPpcErrorHandlingParmsBytes(ws.getWsXzc020c1Row().getXzc020ErrorHandlingParmsBytes());
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
		// MOVE TO OUTPUT.
		// COB_CODE: MOVE XZC02O-UW-TMN-FLG      TO XZA9S1-TMN-IND.
		ws.getWsXz0a90s1Row().getRow().setTmnInd(ws.getWsXzc020c1Row().getoUwTmnFlg());
		// COB_CODE: MOVE XZC02O-TMN-EMP-ID      TO XZA9S1-TMN-EMP-ID.
		ws.getWsXz0a90s1Row().getRow().setTmnEmpId(ws.getWsXzc020c1Row().getoTmnEmpId());
		// COB_CODE: MOVE XZC02O-TMN-EMP-NM      TO XZA9S1-TMN-EMP-NM.
		ws.getWsXz0a90s1Row().getRow().setTmnEmpNm(ws.getWsXzc020c1Row().getoTmnEmpNm());
		// COB_CODE: MOVE XZC02O-TMN-PRC-DT      TO XZA9S1-TMN-PRC-DT.
		ws.getWsXz0a90s1Row().getRow().setTmnPrcDt(ws.getWsXzc020c1Row().getoTmnPrcDt());
	}

	/**Original name: 3183-WRITE-OUTPUT-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  WRITE ACTIVE PENDING CANCEL/TERMINATE POLICY LIST OUTPUT ROW.  *
	 * *****************************************************************</pre>*/
	private void writeOutputRow() {
		Halrresp halrresp = null;
		// COB_CODE: SET HALRRESP-WRITE-FUNC     TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM          TO HALRRESP-BUS-OBJ-NM.
		ws.getWsHalrrespLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNm());
		// COB_CODE: MOVE LENGTH OF WS-XZ0A90S1-ROW
		//                                       TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(((short) WsXz0a90s1Row.Len.WS_XZ0A90S1_ROW));
		// COB_CODE: CALL HALRRESP-HALRRESP-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRRESP-LINKAGE
		//                WS-XZ0A90S1-ROW.
		halrresp = Halrresp.getInstance();
		halrresp.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrrespLinkage(), ws.getWsXz0a90s1Row());
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWorkingStorageArea().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0B90S0", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getCommInfo().setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getCommInfo().setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWorkingStorageArea().getApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: 9900-CHECK-ERRORS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   CHECK FOR ERROR, NLBE, OR WARNING RETURNED FROM SERVICE      *
	 * ****************************************************************</pre>*/
	private void checkErrors() {
		// COB_CODE: IF PPC-NO-ERROR-CODE
		//               GO TO 9900-EXIT
		//           END-IF.
		if (ws.getPpcErrorHandlingParms().isPpcNoErrorCode()) {
			// COB_CODE: GO TO 9900-EXIT
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN PPC-FATAL-ERROR-CODE
		//                   PERFORM 9910-SET-FATAL-ERROR
		//               WHEN PPC-NLBE-CODE
		//                   PERFORM 9920-SET-NLBE
		//               WHEN PPC-WARNING-CODE
		//                   PERFORM 9930-SET-WARNING
		//           END-EVALUATE.
		switch (ws.getPpcErrorHandlingParms().getErrorReturnCodeFormatted()) {

		case PpcErrorHandlingParms.PPC_FATAL_ERROR_CODE:// COB_CODE: PERFORM 9910-SET-FATAL-ERROR
			setFatalError();
			break;

		case PpcErrorHandlingParms.PPC_NLBE_CODE:// COB_CODE: PERFORM 9920-SET-NLBE
			rng9920SetNlbe();
			break;

		case PpcErrorHandlingParms.PPC_WARNING_CODE:// COB_CODE: PERFORM 9930-SET-WARNING
			rng9930SetWarning();
			break;

		default:
			break;
		}
	}

	/**Original name: 9910-SET-FATAL-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET FOR FATAL ERROR                                           *
	 *   SAVE OFF ANY ERROR MESSAGE THAT WAS RECEIVED FROM THE CALLED  *
	 *   SERVICE IN ORDER TO PUT IT IN THE DISPLAY AREAS AFTER LOGGING *
	 *   THE BUSINESS PROCESS ERROR.                                   *
	 *   THIS WILL ALLOW US TO DISPLAY THE ACTUAL ERROR THAT OCCURRED  *
	 *   IN THE CALLED SERVICE TO THE USER.                            *
	 * *****************************************************************</pre>*/
	private void setFatalError() {
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET ETRA-CICS-WEB-RECEIVE   TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
		// COB_CODE: MOVE WS-EC-MODULE           TO EFAL-ERR-OBJECT-NAME.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkingStorageArea().getErrorCheckInfo().getModule());
		// COB_CODE: MOVE WS-EC-PARAGRAPH        TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph(ws.getWorkingStorageArea().getErrorCheckInfo().getParagraph());
		// COB_CODE: MOVE PPC-FATAL-ERROR-MESSAGE
		//                                       TO EFAL-ERR-COMMENT
		//                                          EFAL-OBJ-DATA-KEY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getPpcErrorHandlingParms().getFatalErrorMessage());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getPpcErrorHandlingParms().getFatalErrorMessage());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
		// COB_CODE: MOVE WS-EC-MODULE           TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
				.setFailedModule(ws.getWorkingStorageArea().getErrorCheckInfo().getModule());
		// COB_CODE: MOVE SPACES                 TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZEROS                  TO UBOC-SQLCODE-DISPLAY
		//                                          UBOC-EIBRESP-DISPLAY
		//                                          UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
	}

	/**Original name: 9920-SET-NLBE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR NLBE                                                 *
	 * ****************************************************************</pre>*/
	private void setNlbe() {
		// COB_CODE: MOVE +0                     TO SS-MSG.
		ws.getSubscripts().setMsg(((short) 0));
	}

	/**Original name: 9920-A<br>*/
	private String a1() {
		// COB_CODE: ADD +1                      TO SS-MSG.
		ws.getSubscripts().setMsg(Trunc.toShort(1 + ws.getSubscripts().getMsg(), 4));
		// COB_CODE: IF PPC-NON-LOG-ERR-MSG(SS-MSG) = SPACES
		//               GO TO 9920-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getPpcErrorHandlingParms().getNonLogErrMsg(ws.getSubscripts().getMsg()))) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// SET UBOC-HALT-AND-RETURN TO TRUE SO THAT THIS SERVICE ENDS
		// COB_CODE: SET UBOC-HALT-AND-RETURN    TO TRUE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setBusErr();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO NLBE-FAILED-TABLE-OR-FILE.
		ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO NLBE-FAILED-COLUMN-OR-FIELD.
		ws.getNlbeCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-NON-LOG-ERR-MSG (SS-MSG)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getPpcErrorHandlingParms().getNonLogErrMsg(ws.getSubscripts().getMsg()));
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-MSG-MAX
		//               GO TO 9920-EXIT
		//           END-IF.
		if (ws.getSubscripts().isMsgMax()) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// COB_CODE: GO TO 9920-A.
		return "9920-A";
	}

	/**Original name: 9930-SET-WARNING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR WARNING                                              *
	 * ****************************************************************</pre>*/
	private void setWarning() {
		// COB_CODE: MOVE +0                     TO SS-WNG.
		ws.getSubscripts().setWng(((short) 0));
	}

	/**Original name: 9930-A<br>*/
	private String a2() {
		// COB_CODE: ADD +1                      TO SS-WNG.
		ws.getSubscripts().setWng(Trunc.toShort(1 + ws.getSubscripts().getWng(), 4));
		// COB_CODE: IF PPC-WARN-MSG(SS-WNG) = SPACES
		//               GO TO 9930-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getPpcErrorHandlingParms().getWarnMsg(ws.getSubscripts().getWng()))) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: SET WS-NON-LOGGABLE-WARNING TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setWarning();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO UWRN-FAILED-TABLE-OR-FILE.
		ws.getUwrnCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO UWRN-FAILED-COLUMN-OR-FIELD.
		ws.getUwrnCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-WARN-MSG (SS-WNG)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getPpcErrorHandlingParms().getWarnMsg(ws.getSubscripts().getWng()));
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-WNG-MAX
		//               GO TO 9930-EXIT
		//           END-IF.
		if (ws.getSubscripts().isWngMax()) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: GO TO 9930-A.
		return "9930-A";
	}

	/**Original name: RNG_3180-A-_-3180-EXIT<br>*/
	private void rng3180A() {
		String retcode = "";
		boolean goto3180A = false;
		do {
			goto3180A = false;
			retcode = a();
		} while (retcode.equals("3180-A"));
	}

	/**Original name: RNG_9920-SET-NLBE_FIRST_SENTENCES-_-9920-EXIT<br>*/
	private void rng9920SetNlbe() {
		String retcode = "";
		boolean goto9920A = false;
		setNlbe();
		do {
			goto9920A = false;
			retcode = a1();
		} while (retcode.equals("9920-A"));
	}

	/**Original name: RNG_9930-SET-WARNING_FIRST_SENTENCES-_-9930-EXIT<br>*/
	private void rng9930SetWarning() {
		String retcode = "";
		boolean goto9930A = false;
		setWarning();
		do {
			goto9930A = false;
			retcode = a2();
		} while (retcode.equals("9930-A"));
	}

	public void initWsXz0a90s0Row() {
		ws.getWsXz0a90s0Row().setMaxRowsReturned(((short) 0));
		ws.getWsXz0a90s0Row().setPrimaryFunction("");
		ws.getWsXz0a90s0Row().setActNbr("");
		ws.getWsXz0a90s0Row().setUserid("");
	}

	public void initWsXzc0690iRow() {
		ws.getXzc0690i().setUri("");
		ws.getXzc0690i().setCallableUserid("");
		ws.getXzc0690i().setCallablePassword("");
		ws.getXzc0690i().setFedTarSys("");
		ws.getXzc0690i().setActNbr("");
		ws.getXzc0690i().setAsOfDt("");
	}

	public void initWsXz0690coRow() {
		ws.getXz0690co().getRetCode().setRetCd(((short) 0));
		for (int idx0 = 1; idx0 <= Xz0690co.ERR_MSG_MAXOCCURS; idx0++) {
			ws.getXz0690co().setErrMsg(idx0, "");
		}
		for (int idx0 = 1; idx0 <= Xz0690co.WNG_MSG_MAXOCCURS; idx0++) {
			ws.getXz0690co().setWngMsg(idx0, "");
		}
		for (int idx0 = 1; idx0 <= Xz0690co.POL_TRM_MAXOCCURS; idx0++) {
			ws.getXz0690co().getPolTrm(idx0).setPolNbr("");
			ws.getXz0690co().getPolTrm(idx0).setPolKey("");
			ws.getXz0690co().getPolTrm(idx0).setQteNbr(0);
			ws.getXz0690co().getPolTrm(idx0).setEffDt("");
			ws.getXz0690co().getPolTrm(idx0).setExpDt("");
			ws.getXz0690co().getPolTrm(idx0).setActNbr("");
			ws.getXz0690co().getPolTrm(idx0).setActNbrFmt("");
			ws.getXz0690co().getPolTrm(idx0).setBseStAbb("");
			ws.getXz0690co().getPolTrm(idx0).setBseStNm("");
			ws.getXz0690co().getPolTrm(idx0).setPrdCd("");
			ws.getXz0690co().getPolTrm(idx0).setPrdDes("");
			ws.getXz0690co().getPolTrm(idx0).setTobCd("");
			ws.getXz0690co().getPolTrm(idx0).setTobDes("");
			ws.getXz0690co().getPolTrm(idx0).setSegCd("");
			ws.getXz0690co().getPolTrm(idx0).setSegDes("");
			ws.getXz0690co().getPolTrm(idx0).setLaInd(Types.SPACE_CHAR);
			ws.getXz0690co().getPolTrm(idx0).setMnlPolInd(Types.SPACE_CHAR);
			ws.getXz0690co().getPolTrm(idx0).setCsCd(Types.SPACE_CHAR);
			ws.getXz0690co().getPolTrm(idx0).setCsDes("");
			ws.getXz0690co().getPolTrm(idx0).getTrsDtl().setTrsTypCd(Types.SPACE_CHAR);
			ws.getXz0690co().getPolTrm(idx0).getTrsDtl().setTrsTypDes("");
			ws.getXz0690co().getPolTrm(idx0).getTrsDtl().setPndFlg(Types.SPACE_CHAR);
			ws.getXz0690co().getPolTrm(idx0).getTrsDtl().setTrsOrtCd(Types.SPACE_CHAR);
			ws.getXz0690co().getPolTrm(idx0).getTrsDtl().setTrsOrtDes("");
			ws.getXz0690co().getPolTrm(idx0).setLocvCds("");
			ws.getXz0690co().getPolTrm(idx0).setBndInd(Types.SPACE_CHAR);
			ws.getXz0690co().getPolTrm(idx0).setOfcCd("");
			ws.getXz0690co().getPolTrm(idx0).setOfcDes("");
			ws.getXz0690co().getPolTrm(idx0).setWrtPrmAmt(0);
			ws.getXz0690co().getPolTrm(idx0).setAcyTrmInd(Types.SPACE_CHAR);
			ws.getXz0690co().getPolTrm(idx0).setEnTrsCd("");
			ws.getXz0690co().getPolTrm(idx0).setEnTrsEffDt("");
			ws.getXz0690co().getPolTrm(idx0).setSysOfRcdCd("");
		}
		ws.getXz0690co().setErrorIndicator("");
		ws.getXz0690co().setFaultCode("");
		ws.getXz0690co().setFaultString("");
		ws.getXz0690co().setFaultActor("");
		ws.getXz0690co().setFaultDetail("");
		ws.getXz0690co().setNonSoapFaultErrTxt("");
	}

	public void initRow() {
		ws.getWsXz0a90s1Row().getRow().setPolicyId("");
		ws.getWsXz0a90s1Row().getRow().setPolNbr("");
		ws.getWsXz0a90s1Row().getRow().setOgnEffDt("");
		ws.getWsXz0a90s1Row().getRow().setPlnExpDt("");
		ws.getWsXz0a90s1Row().getRow().setQuoteSequenceNbr(0);
		ws.getWsXz0a90s1Row().getRow().setActNbr("");
		ws.getWsXz0a90s1Row().getRow().setActNbrFmt("");
		ws.getWsXz0a90s1Row().getRow().setPriRskStAbb("");
		ws.getWsXz0a90s1Row().getRow().setPriRskStDes("");
		ws.getWsXz0a90s1Row().getRow().setLobCd("");
		ws.getWsXz0a90s1Row().getRow().setLobDes("");
		ws.getWsXz0a90s1Row().getRow().setTobCd("");
		ws.getWsXz0a90s1Row().getRow().setTobDes("");
		ws.getWsXz0a90s1Row().getRow().setSegCd("");
		ws.getWsXz0a90s1Row().getRow().setSegDes("");
		ws.getWsXz0a90s1Row().getRow().setLgeActInd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setStatusCd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setCurPndTrsInd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setCurStaCd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setCurStaDes("");
		ws.getWsXz0a90s1Row().getRow().setCurTrsTypCd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setCurTrsTypDes("");
		ws.getWsXz0a90s1Row().getRow().setCoverageParts("");
		ws.getWsXz0a90s1Row().getRow().setRltTypCd("");
		ws.getWsXz0a90s1Row().getRow().setMnlPolInd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setCurOgnCd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setCurOgnDes("");
		ws.getWsXz0a90s1Row().getRow().setBondInd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setBranchNbr("");
		ws.getWsXz0a90s1Row().getRow().setBranchDesc("");
		ws.getWsXz0a90s1Row().getRow().setWrtPremAmt(0);
		ws.getWsXz0a90s1Row().getRow().setPolActInd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setPndCncInd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setSchCncDt("");
		ws.getWsXz0a90s1Row().getRow().setNotTypCd("");
		ws.getWsXz0a90s1Row().getRow().setNotTypDes("");
		ws.getWsXz0a90s1Row().getRow().setNotEmpId("");
		ws.getWsXz0a90s1Row().getRow().setNotEmpNm("");
		ws.getWsXz0a90s1Row().getRow().setNotPrcDt("");
		ws.getWsXz0a90s1Row().getRow().setTmnInd(Types.SPACE_CHAR);
		ws.getWsXz0a90s1Row().getRow().setTmnEmpId("");
		ws.getWsXz0a90s1Row().getRow().setTmnEmpNm("");
		ws.getWsXz0a90s1Row().getRow().setTmnPrcDt("");
	}

	public void initWsXzc010c1Row() {
		ws.getWsXzc010c1Row().setiPolNbr("");
		ws.getWsXzc010c1Row().setiPolEffDt("");
		ws.getWsXzc010c1Row().setiPolExpDt("");
		ws.getWsXzc010c1Row().setiUserid("");
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getPolKeys().setNbr("");
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getPolKeys().setEffDt("");
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().getPolKeys().setExpDt("");
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().setPndCncInd(Types.SPACE_CHAR);
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().setSchCncDt("");
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().setNotTypCd("");
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().setNotTypDes("");
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().setNotEmpId("");
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().setNotEmpNm("");
		ws.getWsXzc010c1Row().getXzc010ServiceOutputs().setNotPrcDt("");
		ws.getWsXzc010c1Row().getXzc010ErrorReturnCode().setErrorReturnCodeFormatted("0000");
		ws.getWsXzc010c1Row().setXzc010FatalErrorMessage("");
		ws.getWsXzc010c1Row().setXzc010NonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaXzc01090.XZC010_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			ws.getWsXzc010c1Row().getXzc010NonLoggableErrors(idx0).setXzc010NonLogErrMsg("");
		}
		ws.getWsXzc010c1Row().setXzc010WarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaXzc01090.XZC010_WARNINGS_MAXOCCURS; idx0++) {
			ws.getWsXzc010c1Row().getXzc010Warnings(idx0).setXzc010WarnMsg("");
		}
	}

	public void initWsXzc020c1Row() {
		ws.getWsXzc020c1Row().setiPolNbr("");
		ws.getWsXzc020c1Row().setiPolEffDt("");
		ws.getWsXzc020c1Row().setiPolExpDt("");
		ws.getWsXzc020c1Row().setiUserid("");
		ws.getWsXzc020c1Row().getoPolKeys().setNbr("");
		ws.getWsXzc020c1Row().getoPolKeys().setEffDt("");
		ws.getWsXzc020c1Row().getoPolKeys().setExpDt("");
		ws.getWsXzc020c1Row().setoUwTmnFlg(Types.SPACE_CHAR);
		ws.getWsXzc020c1Row().setoTmnEmpId("");
		ws.getWsXzc020c1Row().setoTmnEmpNm("");
		ws.getWsXzc020c1Row().setoTmnPrcDt("");
		ws.getWsXzc020c1Row().getXzc020ErrorReturnCode().setErrorReturnCodeFormatted("0000");
		ws.getWsXzc020c1Row().setXzc020FatalErrorMessage("");
		ws.getWsXzc020c1Row().setXzc020NonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaXzc02090.XZC020_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			ws.getWsXzc020c1Row().getXzc020NonLoggableErrors(idx0).setXzc020NonLogErrMsg("");
		}
		ws.getWsXzc020c1Row().setXzc020WarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaXzc02090.XZC020_WARNINGS_MAXOCCURS; idx0++) {
			ws.getWsXzc020c1Row().getXzc020Warnings(idx0).setXzc020WarnMsg("");
		}
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
