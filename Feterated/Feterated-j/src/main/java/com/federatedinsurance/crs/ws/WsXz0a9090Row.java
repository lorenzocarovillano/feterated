/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9090-ROW<br>
 * Variable: WS-XZ0A9090-ROW from program XZ0B9090<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9090Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA990-MAX-NOT-CO-ROWS
	private short maxNotCoRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA990-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZA990-CO-CD
	private String coCd = DefaultValues.stringVal(Len.CO_CD);
	//Original name: XZA990-CO-DES
	private String coDes = DefaultValues.stringVal(Len.CO_DES);
	//Original name: XZA990-SPE-CRT-CD
	private String speCrtCd = DefaultValues.stringVal(Len.SPE_CRT_CD);
	//Original name: XZA990-CO-OFS-NBR
	private int coOfsNbr = DefaultValues.INT_VAL;
	//Original name: XZA990-DTB-CD
	private char dtbCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9090_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9090RowBytes(buf);
	}

	public String getWsXz0a9090RowFormatted() {
		return getNotCoInfoFormatted();
	}

	public void setWsXz0a9090RowBytes(byte[] buffer) {
		setWsXz0a9090RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9090RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9090_ROW];
		return getWsXz0a9090RowBytes(buffer, 1);
	}

	public void setWsXz0a9090RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setNotCoInfoBytes(buffer, position);
	}

	public byte[] getWsXz0a9090RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getNotCoInfoBytes(buffer, position);
		return buffer;
	}

	public String getNotCoInfoFormatted() {
		return MarshalByteExt.bufferToStr(getNotCoInfoBytes());
	}

	/**Original name: XZA990-NOT-CO-INFO<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0A9090 - BPO COPYBOOK FOR                                    *
	 *             UOW : XZ_GET_NOT_CO_LIST                            *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 01/30/2009  E404GCL   NEW                              *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getNotCoInfoBytes() {
		byte[] buffer = new byte[Len.NOT_CO_INFO];
		return getNotCoInfoBytes(buffer, 1);
	}

	public void setNotCoInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		maxNotCoRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		setNotCoRowBytes(buffer, position);
	}

	public byte[] getNotCoInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxNotCoRows);
		position += Types.SHORT_SIZE;
		getNotCoRowBytes(buffer, position);
		return buffer;
	}

	public void setMaxNotCoRows(short maxNotCoRows) {
		this.maxNotCoRows = maxNotCoRows;
	}

	public short getMaxNotCoRows() {
		return this.maxNotCoRows;
	}

	public void setNotCoRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotTypCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		coCd = MarshalByte.readString(buffer, position, Len.CO_CD);
		position += Len.CO_CD;
		coDes = MarshalByte.readString(buffer, position, Len.CO_DES);
		position += Len.CO_DES;
		speCrtCd = MarshalByte.readString(buffer, position, Len.SPE_CRT_CD);
		position += Len.SPE_CRT_CD;
		coOfsNbr = MarshalByte.readInt(buffer, position, Len.CO_OFS_NBR);
		position += Len.CO_OFS_NBR;
		dtbCd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getNotCoRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, coCd, Len.CO_CD);
		position += Len.CO_CD;
		MarshalByte.writeString(buffer, position, coDes, Len.CO_DES);
		position += Len.CO_DES;
		MarshalByte.writeString(buffer, position, speCrtCd, Len.SPE_CRT_CD);
		position += Len.SPE_CRT_CD;
		MarshalByte.writeInt(buffer, position, coOfsNbr, Len.CO_OFS_NBR);
		position += Len.CO_OFS_NBR;
		MarshalByte.writeChar(buffer, position, dtbCd);
		return buffer;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setCoCd(String coCd) {
		this.coCd = Functions.subString(coCd, Len.CO_CD);
	}

	public String getCoCd() {
		return this.coCd;
	}

	public void setCoDes(String coDes) {
		this.coDes = Functions.subString(coDes, Len.CO_DES);
	}

	public String getCoDes() {
		return this.coDes;
	}

	public void setSpeCrtCd(String speCrtCd) {
		this.speCrtCd = Functions.subString(speCrtCd, Len.SPE_CRT_CD);
	}

	public String getSpeCrtCd() {
		return this.speCrtCd;
	}

	public void setCoOfsNbr(int coOfsNbr) {
		this.coOfsNbr = coOfsNbr;
	}

	public int getCoOfsNbr() {
		return this.coOfsNbr;
	}

	public void setDtbCd(char dtbCd) {
		this.dtbCd = dtbCd;
	}

	public char getDtbCd() {
		return this.dtbCd;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9090RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_NOT_CO_ROWS = 2;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int CO_CD = 5;
		public static final int CO_DES = 30;
		public static final int SPE_CRT_CD = 5;
		public static final int CO_OFS_NBR = 5;
		public static final int DTB_CD = 1;
		public static final int NOT_CO_ROW = ACT_NOT_TYP_CD + CO_CD + CO_DES + SPE_CRT_CD + CO_OFS_NBR + DTB_CD;
		public static final int NOT_CO_INFO = MAX_NOT_CO_ROWS + NOT_CO_ROW;
		public static final int WS_XZ0A9090_ROW = NOT_CO_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
