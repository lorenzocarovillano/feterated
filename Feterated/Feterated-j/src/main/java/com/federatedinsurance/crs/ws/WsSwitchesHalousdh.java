/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.WsCallDataPrivSw;
import com.federatedinsurance.crs.ws.enums.WsReqUmtDfltRecSw;
import com.federatedinsurance.crs.ws.enums.WsRetFunRowSw;
import com.federatedinsurance.crs.ws.enums.WsScriptProcessing;
import com.federatedinsurance.crs.ws.enums.WsTsq1Sw;
import com.federatedinsurance.crs.ws.enums.WsTsq2Sw;
import com.federatedinsurance.crs.ws.enums.WsTsq3Sw;

/**Original name: WS-SWITCHES<br>
 * Variable: WS-SWITCHES from program HALOUSDH<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSwitchesHalousdh {

	//==== PROPERTIES ====
	//Original name: WS-SCRIPT-PROCESSING
	private WsScriptProcessing scriptProcessing = new WsScriptProcessing();
	//Original name: WS-CALL-DATA-PRIV-SW
	private WsCallDataPrivSw callDataPrivSw = new WsCallDataPrivSw();
	//Original name: WS-REQ-UMT-DFLT-REC-SW
	private WsReqUmtDfltRecSw reqUmtDfltRecSw = new WsReqUmtDfltRecSw();
	//Original name: WS-RET-FUN-ROW-SW
	private WsRetFunRowSw retFunRowSw = new WsRetFunRowSw();
	//Original name: WS-TSQ1-SW
	private WsTsq1Sw tsq1Sw = new WsTsq1Sw();
	//Original name: WS-TSQ2-SW
	private WsTsq2Sw tsq2Sw = new WsTsq2Sw();
	//Original name: WS-TSQ3-SW
	private WsTsq3Sw tsq3Sw = new WsTsq3Sw();

	//==== METHODS ====
	public WsCallDataPrivSw getCallDataPrivSw() {
		return callDataPrivSw;
	}

	public WsReqUmtDfltRecSw getReqUmtDfltRecSw() {
		return reqUmtDfltRecSw;
	}

	public WsRetFunRowSw getRetFunRowSw() {
		return retFunRowSw;
	}

	public WsScriptProcessing getScriptProcessing() {
		return scriptProcessing;
	}

	public WsTsq1Sw getTsq1Sw() {
		return tsq1Sw;
	}

	public WsTsq2Sw getTsq2Sw() {
		return tsq2Sw;
	}

	public WsTsq3Sw getTsq3Sw() {
		return tsq3Sw;
	}
}
