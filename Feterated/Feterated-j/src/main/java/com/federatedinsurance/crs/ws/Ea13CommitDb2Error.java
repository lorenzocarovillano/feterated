/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-13-COMMIT-DB2-ERROR<br>
 * Variable: EA-13-COMMIT-DB2-ERROR from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea13CommitDb2Error {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-13-COMMIT-DB2-ERROR
	private String flr1 = "XZ001000 -";
	//Original name: FILLER-EA-13-COMMIT-DB2-ERROR-1
	private String flr2 = "DB2 COMMIT";
	//Original name: FILLER-EA-13-COMMIT-DB2-ERROR-2
	private String flr3 = "ERROR OCCURRED:";
	//Original name: FILLER-EA-13-COMMIT-DB2-ERROR-3
	private String flr4 = " SQLCODE =";
	//Original name: EA-13-SQLCODE
	private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);
	//Original name: FILLER-EA-13-COMMIT-DB2-ERROR-4
	private String flr5 = ".  (PARAGRAPH #";
	//Original name: EA-13-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-13-COMMIT-DB2-ERROR-5
	private String flr6 = ").";

	//==== METHODS ====
	public String getEa13CommitDb2ErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa13CommitDb2ErrorBytes());
	}

	public byte[] getEa13CommitDb2ErrorBytes() {
		byte[] buffer = new byte[Len.EA13_COMMIT_DB2_ERROR];
		return getEa13CommitDb2ErrorBytes(buffer, 1);
	}

	public byte[] getEa13CommitDb2ErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
		position += Len.SQLCODE;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setSqlcode(long sqlcode) {
		this.sqlcode = PicFormatter.display("++(4)9").format(sqlcode).toString();
	}

	public long getSqlcode() {
		return PicParser.display("++(4)9").parseLong(this.sqlcode);
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SQLCODE = 6;
		public static final int PARAGRAPH_NBR = 5;
		public static final int FLR1 = 11;
		public static final int FLR3 = 15;
		public static final int FLR6 = 2;
		public static final int EA13_COMMIT_DB2_ERROR = SQLCODE + PARAGRAPH_NBR + 3 * FLR1 + 2 * FLR3 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
