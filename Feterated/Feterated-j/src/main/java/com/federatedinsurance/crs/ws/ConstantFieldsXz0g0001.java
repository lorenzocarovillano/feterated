/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0G0001<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class ConstantFieldsXz0g0001 {

	//==== PROPERTIES ====
	//Original name: CF-PN-2100
	private String pn2100 = "2100";
	//Original name: CF-PN-2200
	private String pn2200 = "2200";
	//Original name: CF-PN-3200
	private String pn3200 = "3200";
	//Original name: CF-PROGRAM-NAME
	private String programName = "XZ0G0001";
	//Original name: CF-RC-GOOD-RETURN
	private String rcGoodReturn = "0000";
	//Original name: CF-RC-MISSING-ACT
	private String rcMissingAct = "0300";
	//Original name: CF-RC-DB2-ERROR
	private String rcDb2Error = "0500";
	//Original name: CF-SC-GOOD-RETURN
	private String scGoodReturn = "00000";

	//==== METHODS ====
	public String getPn2100() {
		return this.pn2100;
	}

	public String getPn2200() {
		return this.pn2200;
	}

	public String getPn3200() {
		return this.pn3200;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getRcGoodReturnFormatted() {
		return this.rcGoodReturn;
	}

	public String getRcMissingActFormatted() {
		return this.rcMissingAct;
	}

	public String getRcDb2ErrorFormatted() {
		return this.rcDb2Error;
	}

	public String getScGoodReturn() {
		return this.scGoodReturn;
	}
}
