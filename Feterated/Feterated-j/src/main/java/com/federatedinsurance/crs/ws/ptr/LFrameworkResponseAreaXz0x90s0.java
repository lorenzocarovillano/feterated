/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0X90S0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0x90s0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_ACY_PND_CNC_TMN_POL_MAXOCCURS = 250;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0x90s0() {
	}

	public LFrameworkResponseAreaXz0x90s0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a90s1Formatted(int lFwRespXz0a90s1Idx) {
		int position = Pos.lFwRespAcyPndCncTmnPol(lFwRespXz0a90s1Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_ACY_PND_CNC_TMN_POL);
	}

	public void setlFwRespXz0a90s1Bytes(int lFwRespXz0a90s1Idx, byte[] buffer) {
		setlFwRespXz0a90s1Bytes(lFwRespXz0a90s1Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A90S1<br>*/
	public byte[] getlFwRespXz0a90s1Bytes(int lFwRespXz0a90s1Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_ACY_PND_CNC_TMN_POL];
		return getlFwRespXz0a90s1Bytes(lFwRespXz0a90s1Idx, buffer, 1);
	}

	public void setlFwRespXz0a90s1Bytes(int lFwRespXz0a90s1Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespAcyPndCncTmnPol(lFwRespXz0a90s1Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_ACY_PND_CNC_TMN_POL, position);
	}

	public byte[] getlFwRespXz0a90s1Bytes(int lFwRespXz0a90s1Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespAcyPndCncTmnPol(lFwRespXz0a90s1Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_ACY_PND_CNC_TMN_POL, position);
		return buffer;
	}

	public void setXza9s1rPolicyId(int xza9s1rPolicyIdIdx, String xza9s1rPolicyId) {
		int position = Pos.xza9s1rPolicyId(xza9s1rPolicyIdIdx - 1);
		writeString(position, xza9s1rPolicyId, Len.XZA9S1R_POLICY_ID);
	}

	/**Original name: XZA9S1R-POLICY-ID<br>*/
	public String getXza9s1rPolicyId(int xza9s1rPolicyIdIdx) {
		int position = Pos.xza9s1rPolicyId(xza9s1rPolicyIdIdx - 1);
		return readString(position, Len.XZA9S1R_POLICY_ID);
	}

	public void setXza9s1rPolNbr(int xza9s1rPolNbrIdx, String xza9s1rPolNbr) {
		int position = Pos.xza9s1rPolNbr(xza9s1rPolNbrIdx - 1);
		writeString(position, xza9s1rPolNbr, Len.XZA9S1R_POL_NBR);
	}

	/**Original name: XZA9S1R-POL-NBR<br>*/
	public String getXza9s1rPolNbr(int xza9s1rPolNbrIdx) {
		int position = Pos.xza9s1rPolNbr(xza9s1rPolNbrIdx - 1);
		return readString(position, Len.XZA9S1R_POL_NBR);
	}

	public void setXza9s1rOgnEffDt(int xza9s1rOgnEffDtIdx, String xza9s1rOgnEffDt) {
		int position = Pos.xza9s1rOgnEffDt(xza9s1rOgnEffDtIdx - 1);
		writeString(position, xza9s1rOgnEffDt, Len.XZA9S1R_OGN_EFF_DT);
	}

	/**Original name: XZA9S1R-OGN-EFF-DT<br>*/
	public String getXza9s1rOgnEffDt(int xza9s1rOgnEffDtIdx) {
		int position = Pos.xza9s1rOgnEffDt(xza9s1rOgnEffDtIdx - 1);
		return readString(position, Len.XZA9S1R_OGN_EFF_DT);
	}

	public void setXza9s1rPlnExpDt(int xza9s1rPlnExpDtIdx, String xza9s1rPlnExpDt) {
		int position = Pos.xza9s1rPlnExpDt(xza9s1rPlnExpDtIdx - 1);
		writeString(position, xza9s1rPlnExpDt, Len.XZA9S1R_PLN_EXP_DT);
	}

	/**Original name: XZA9S1R-PLN-EXP-DT<br>*/
	public String getXza9s1rPlnExpDt(int xza9s1rPlnExpDtIdx) {
		int position = Pos.xza9s1rPlnExpDt(xza9s1rPlnExpDtIdx - 1);
		return readString(position, Len.XZA9S1R_PLN_EXP_DT);
	}

	public void setXza9s1rQuoteSequenceNbr(int xza9s1rQuoteSequenceNbrIdx, int xza9s1rQuoteSequenceNbr) {
		int position = Pos.xza9s1rQuoteSequenceNbr(xza9s1rQuoteSequenceNbrIdx - 1);
		writeInt(position, xza9s1rQuoteSequenceNbr, Len.Int.XZA9S1R_QUOTE_SEQUENCE_NBR);
	}

	/**Original name: XZA9S1R-QUOTE-SEQUENCE-NBR<br>*/
	public int getXza9s1rQuoteSequenceNbr(int xza9s1rQuoteSequenceNbrIdx) {
		int position = Pos.xza9s1rQuoteSequenceNbr(xza9s1rQuoteSequenceNbrIdx - 1);
		return readNumDispInt(position, Len.XZA9S1R_QUOTE_SEQUENCE_NBR);
	}

	public void setXza9s1rActNbr(int xza9s1rActNbrIdx, String xza9s1rActNbr) {
		int position = Pos.xza9s1rActNbr(xza9s1rActNbrIdx - 1);
		writeString(position, xza9s1rActNbr, Len.XZA9S1R_ACT_NBR);
	}

	/**Original name: XZA9S1R-ACT-NBR<br>*/
	public String getXza9s1rActNbr(int xza9s1rActNbrIdx) {
		int position = Pos.xza9s1rActNbr(xza9s1rActNbrIdx - 1);
		return readString(position, Len.XZA9S1R_ACT_NBR);
	}

	public void setXza9s1rActNbrFmt(int xza9s1rActNbrFmtIdx, String xza9s1rActNbrFmt) {
		int position = Pos.xza9s1rActNbrFmt(xza9s1rActNbrFmtIdx - 1);
		writeString(position, xza9s1rActNbrFmt, Len.XZA9S1R_ACT_NBR_FMT);
	}

	/**Original name: XZA9S1R-ACT-NBR-FMT<br>*/
	public String getXza9s1rActNbrFmt(int xza9s1rActNbrFmtIdx) {
		int position = Pos.xza9s1rActNbrFmt(xza9s1rActNbrFmtIdx - 1);
		return readString(position, Len.XZA9S1R_ACT_NBR_FMT);
	}

	public void setXza9s1rPriRskStAbb(int xza9s1rPriRskStAbbIdx, String xza9s1rPriRskStAbb) {
		int position = Pos.xza9s1rPriRskStAbb(xza9s1rPriRskStAbbIdx - 1);
		writeString(position, xza9s1rPriRskStAbb, Len.XZA9S1R_PRI_RSK_ST_ABB);
	}

	/**Original name: XZA9S1R-PRI-RSK-ST-ABB<br>*/
	public String getXza9s1rPriRskStAbb(int xza9s1rPriRskStAbbIdx) {
		int position = Pos.xza9s1rPriRskStAbb(xza9s1rPriRskStAbbIdx - 1);
		return readString(position, Len.XZA9S1R_PRI_RSK_ST_ABB);
	}

	public void setXza9s1rPriRskStDes(int xza9s1rPriRskStDesIdx, String xza9s1rPriRskStDes) {
		int position = Pos.xza9s1rPriRskStDes(xza9s1rPriRskStDesIdx - 1);
		writeString(position, xza9s1rPriRskStDes, Len.XZA9S1R_PRI_RSK_ST_DES);
	}

	/**Original name: XZA9S1R-PRI-RSK-ST-DES<br>*/
	public String getXza9s1rPriRskStDes(int xza9s1rPriRskStDesIdx) {
		int position = Pos.xza9s1rPriRskStDes(xza9s1rPriRskStDesIdx - 1);
		return readString(position, Len.XZA9S1R_PRI_RSK_ST_DES);
	}

	public void setXza9s1rLobCd(int xza9s1rLobCdIdx, String xza9s1rLobCd) {
		int position = Pos.xza9s1rLobCd(xza9s1rLobCdIdx - 1);
		writeString(position, xza9s1rLobCd, Len.XZA9S1R_LOB_CD);
	}

	/**Original name: XZA9S1R-LOB-CD<br>*/
	public String getXza9s1rLobCd(int xza9s1rLobCdIdx) {
		int position = Pos.xza9s1rLobCd(xza9s1rLobCdIdx - 1);
		return readString(position, Len.XZA9S1R_LOB_CD);
	}

	public void setXza9s1rLobDes(int xza9s1rLobDesIdx, String xza9s1rLobDes) {
		int position = Pos.xza9s1rLobDes(xza9s1rLobDesIdx - 1);
		writeString(position, xza9s1rLobDes, Len.XZA9S1R_LOB_DES);
	}

	/**Original name: XZA9S1R-LOB-DES<br>*/
	public String getXza9s1rLobDes(int xza9s1rLobDesIdx) {
		int position = Pos.xza9s1rLobDes(xza9s1rLobDesIdx - 1);
		return readString(position, Len.XZA9S1R_LOB_DES);
	}

	public void setXza9s1rTobCd(int xza9s1rTobCdIdx, String xza9s1rTobCd) {
		int position = Pos.xza9s1rTobCd(xza9s1rTobCdIdx - 1);
		writeString(position, xza9s1rTobCd, Len.XZA9S1R_TOB_CD);
	}

	/**Original name: XZA9S1R-TOB-CD<br>*/
	public String getXza9s1rTobCd(int xza9s1rTobCdIdx) {
		int position = Pos.xza9s1rTobCd(xza9s1rTobCdIdx - 1);
		return readString(position, Len.XZA9S1R_TOB_CD);
	}

	public void setXza9s1rTobDes(int xza9s1rTobDesIdx, String xza9s1rTobDes) {
		int position = Pos.xza9s1rTobDes(xza9s1rTobDesIdx - 1);
		writeString(position, xza9s1rTobDes, Len.XZA9S1R_TOB_DES);
	}

	/**Original name: XZA9S1R-TOB-DES<br>*/
	public String getXza9s1rTobDes(int xza9s1rTobDesIdx) {
		int position = Pos.xza9s1rTobDes(xza9s1rTobDesIdx - 1);
		return readString(position, Len.XZA9S1R_TOB_DES);
	}

	public void setXza9s1rSegCd(int xza9s1rSegCdIdx, String xza9s1rSegCd) {
		int position = Pos.xza9s1rSegCd(xza9s1rSegCdIdx - 1);
		writeString(position, xza9s1rSegCd, Len.XZA9S1R_SEG_CD);
	}

	/**Original name: XZA9S1R-SEG-CD<br>*/
	public String getXza9s1rSegCd(int xza9s1rSegCdIdx) {
		int position = Pos.xza9s1rSegCd(xza9s1rSegCdIdx - 1);
		return readString(position, Len.XZA9S1R_SEG_CD);
	}

	public void setXza9s1rSegDes(int xza9s1rSegDesIdx, String xza9s1rSegDes) {
		int position = Pos.xza9s1rSegDes(xza9s1rSegDesIdx - 1);
		writeString(position, xza9s1rSegDes, Len.XZA9S1R_SEG_DES);
	}

	/**Original name: XZA9S1R-SEG-DES<br>*/
	public String getXza9s1rSegDes(int xza9s1rSegDesIdx) {
		int position = Pos.xza9s1rSegDes(xza9s1rSegDesIdx - 1);
		return readString(position, Len.XZA9S1R_SEG_DES);
	}

	public void setXza9s1rLgeActInd(int xza9s1rLgeActIndIdx, char xza9s1rLgeActInd) {
		int position = Pos.xza9s1rLgeActInd(xza9s1rLgeActIndIdx - 1);
		writeChar(position, xza9s1rLgeActInd);
	}

	/**Original name: XZA9S1R-LGE-ACT-IND<br>*/
	public char getXza9s1rLgeActInd(int xza9s1rLgeActIndIdx) {
		int position = Pos.xza9s1rLgeActInd(xza9s1rLgeActIndIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rStatusCd(int xza9s1rStatusCdIdx, char xza9s1rStatusCd) {
		int position = Pos.xza9s1rStatusCd(xza9s1rStatusCdIdx - 1);
		writeChar(position, xza9s1rStatusCd);
	}

	/**Original name: XZA9S1R-STATUS-CD<br>*/
	public char getXza9s1rStatusCd(int xza9s1rStatusCdIdx) {
		int position = Pos.xza9s1rStatusCd(xza9s1rStatusCdIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rCurPndTrsInd(int xza9s1rCurPndTrsIndIdx, char xza9s1rCurPndTrsInd) {
		int position = Pos.xza9s1rCurPndTrsInd(xza9s1rCurPndTrsIndIdx - 1);
		writeChar(position, xza9s1rCurPndTrsInd);
	}

	/**Original name: XZA9S1R-CUR-PND-TRS-IND<br>*/
	public char getXza9s1rCurPndTrsInd(int xza9s1rCurPndTrsIndIdx) {
		int position = Pos.xza9s1rCurPndTrsInd(xza9s1rCurPndTrsIndIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rCurStaCd(int xza9s1rCurStaCdIdx, char xza9s1rCurStaCd) {
		int position = Pos.xza9s1rCurStaCd(xza9s1rCurStaCdIdx - 1);
		writeChar(position, xza9s1rCurStaCd);
	}

	/**Original name: XZA9S1R-CUR-STA-CD<br>*/
	public char getXza9s1rCurStaCd(int xza9s1rCurStaCdIdx) {
		int position = Pos.xza9s1rCurStaCd(xza9s1rCurStaCdIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rCurStaDes(int xza9s1rCurStaDesIdx, String xza9s1rCurStaDes) {
		int position = Pos.xza9s1rCurStaDes(xza9s1rCurStaDesIdx - 1);
		writeString(position, xza9s1rCurStaDes, Len.XZA9S1R_CUR_STA_DES);
	}

	/**Original name: XZA9S1R-CUR-STA-DES<br>*/
	public String getXza9s1rCurStaDes(int xza9s1rCurStaDesIdx) {
		int position = Pos.xza9s1rCurStaDes(xza9s1rCurStaDesIdx - 1);
		return readString(position, Len.XZA9S1R_CUR_STA_DES);
	}

	public void setXza9s1rCurTrsTypCd(int xza9s1rCurTrsTypCdIdx, char xza9s1rCurTrsTypCd) {
		int position = Pos.xza9s1rCurTrsTypCd(xza9s1rCurTrsTypCdIdx - 1);
		writeChar(position, xza9s1rCurTrsTypCd);
	}

	/**Original name: XZA9S1R-CUR-TRS-TYP-CD<br>*/
	public char getXza9s1rCurTrsTypCd(int xza9s1rCurTrsTypCdIdx) {
		int position = Pos.xza9s1rCurTrsTypCd(xza9s1rCurTrsTypCdIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rCurTrsTypDes(int xza9s1rCurTrsTypDesIdx, String xza9s1rCurTrsTypDes) {
		int position = Pos.xza9s1rCurTrsTypDes(xza9s1rCurTrsTypDesIdx - 1);
		writeString(position, xza9s1rCurTrsTypDes, Len.XZA9S1R_CUR_TRS_TYP_DES);
	}

	/**Original name: XZA9S1R-CUR-TRS-TYP-DES<br>*/
	public String getXza9s1rCurTrsTypDes(int xza9s1rCurTrsTypDesIdx) {
		int position = Pos.xza9s1rCurTrsTypDes(xza9s1rCurTrsTypDesIdx - 1);
		return readString(position, Len.XZA9S1R_CUR_TRS_TYP_DES);
	}

	public void setXza9s1rCoverageParts(int xza9s1rCoveragePartsIdx, String xza9s1rCoverageParts) {
		int position = Pos.xza9s1rCoverageParts(xza9s1rCoveragePartsIdx - 1);
		writeString(position, xza9s1rCoverageParts, Len.XZA9S1R_COVERAGE_PARTS);
	}

	/**Original name: XZA9S1R-COVERAGE-PARTS<br>*/
	public String getXza9s1rCoverageParts(int xza9s1rCoveragePartsIdx) {
		int position = Pos.xza9s1rCoverageParts(xza9s1rCoveragePartsIdx - 1);
		return readString(position, Len.XZA9S1R_COVERAGE_PARTS);
	}

	public void setXza9s1rRltTypCd(int xza9s1rRltTypCdIdx, String xza9s1rRltTypCd) {
		int position = Pos.xza9s1rRltTypCd(xza9s1rRltTypCdIdx - 1);
		writeString(position, xza9s1rRltTypCd, Len.XZA9S1R_RLT_TYP_CD);
	}

	/**Original name: XZA9S1R-RLT-TYP-CD<br>*/
	public String getXza9s1rRltTypCd(int xza9s1rRltTypCdIdx) {
		int position = Pos.xza9s1rRltTypCd(xza9s1rRltTypCdIdx - 1);
		return readString(position, Len.XZA9S1R_RLT_TYP_CD);
	}

	public void setXza9s1rMnlPolInd(int xza9s1rMnlPolIndIdx, char xza9s1rMnlPolInd) {
		int position = Pos.xza9s1rMnlPolInd(xza9s1rMnlPolIndIdx - 1);
		writeChar(position, xza9s1rMnlPolInd);
	}

	/**Original name: XZA9S1R-MNL-POL-IND<br>*/
	public char getXza9s1rMnlPolInd(int xza9s1rMnlPolIndIdx) {
		int position = Pos.xza9s1rMnlPolInd(xza9s1rMnlPolIndIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rCurOgnCd(int xza9s1rCurOgnCdIdx, char xza9s1rCurOgnCd) {
		int position = Pos.xza9s1rCurOgnCd(xza9s1rCurOgnCdIdx - 1);
		writeChar(position, xza9s1rCurOgnCd);
	}

	/**Original name: XZA9S1R-CUR-OGN-CD<br>*/
	public char getXza9s1rCurOgnCd(int xza9s1rCurOgnCdIdx) {
		int position = Pos.xza9s1rCurOgnCd(xza9s1rCurOgnCdIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rCurOgnDes(int xza9s1rCurOgnDesIdx, String xza9s1rCurOgnDes) {
		int position = Pos.xza9s1rCurOgnDes(xza9s1rCurOgnDesIdx - 1);
		writeString(position, xza9s1rCurOgnDes, Len.XZA9S1R_CUR_OGN_DES);
	}

	/**Original name: XZA9S1R-CUR-OGN-DES<br>*/
	public String getXza9s1rCurOgnDes(int xza9s1rCurOgnDesIdx) {
		int position = Pos.xza9s1rCurOgnDes(xza9s1rCurOgnDesIdx - 1);
		return readString(position, Len.XZA9S1R_CUR_OGN_DES);
	}

	public void setXza9s1rBondInd(int xza9s1rBondIndIdx, char xza9s1rBondInd) {
		int position = Pos.xza9s1rBondInd(xza9s1rBondIndIdx - 1);
		writeChar(position, xza9s1rBondInd);
	}

	/**Original name: XZA9S1R-BOND-IND<br>*/
	public char getXza9s1rBondInd(int xza9s1rBondIndIdx) {
		int position = Pos.xza9s1rBondInd(xza9s1rBondIndIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rBranchNbr(int xza9s1rBranchNbrIdx, String xza9s1rBranchNbr) {
		int position = Pos.xza9s1rBranchNbr(xza9s1rBranchNbrIdx - 1);
		writeString(position, xza9s1rBranchNbr, Len.XZA9S1R_BRANCH_NBR);
	}

	/**Original name: XZA9S1R-BRANCH-NBR<br>*/
	public String getXza9s1rBranchNbr(int xza9s1rBranchNbrIdx) {
		int position = Pos.xza9s1rBranchNbr(xza9s1rBranchNbrIdx - 1);
		return readString(position, Len.XZA9S1R_BRANCH_NBR);
	}

	public void setXza9s1rBranchDesc(int xza9s1rBranchDescIdx, String xza9s1rBranchDesc) {
		int position = Pos.xza9s1rBranchDesc(xza9s1rBranchDescIdx - 1);
		writeString(position, xza9s1rBranchDesc, Len.XZA9S1R_BRANCH_DESC);
	}

	/**Original name: XZA9S1R-BRANCH-DESC<br>*/
	public String getXza9s1rBranchDesc(int xza9s1rBranchDescIdx) {
		int position = Pos.xza9s1rBranchDesc(xza9s1rBranchDescIdx - 1);
		return readString(position, Len.XZA9S1R_BRANCH_DESC);
	}

	public void setXza9s1rWrtPremAmt(int xza9s1rWrtPremAmtIdx, long xza9s1rWrtPremAmt) {
		int position = Pos.xza9s1rWrtPremAmt(xza9s1rWrtPremAmtIdx - 1);
		writeLong(position, xza9s1rWrtPremAmt, Len.Int.XZA9S1R_WRT_PREM_AMT);
	}

	/**Original name: XZA9S1R-WRT-PREM-AMT<br>*/
	public long getXza9s1rWrtPremAmt(int xza9s1rWrtPremAmtIdx) {
		int position = Pos.xza9s1rWrtPremAmt(xza9s1rWrtPremAmtIdx - 1);
		return readNumDispLong(position, Len.XZA9S1R_WRT_PREM_AMT);
	}

	public void setXza9s1rPolActInd(int xza9s1rPolActIndIdx, char xza9s1rPolActInd) {
		int position = Pos.xza9s1rPolActInd(xza9s1rPolActIndIdx - 1);
		writeChar(position, xza9s1rPolActInd);
	}

	/**Original name: XZA9S1R-POL-ACT-IND<br>*/
	public char getXza9s1rPolActInd(int xza9s1rPolActIndIdx) {
		int position = Pos.xza9s1rPolActInd(xza9s1rPolActIndIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rPndCncInd(int xza9s1rPndCncIndIdx, char xza9s1rPndCncInd) {
		int position = Pos.xza9s1rPndCncInd(xza9s1rPndCncIndIdx - 1);
		writeChar(position, xza9s1rPndCncInd);
	}

	/**Original name: XZA9S1R-PND-CNC-IND<br>*/
	public char getXza9s1rPndCncInd(int xza9s1rPndCncIndIdx) {
		int position = Pos.xza9s1rPndCncInd(xza9s1rPndCncIndIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rSchCncDt(int xza9s1rSchCncDtIdx, String xza9s1rSchCncDt) {
		int position = Pos.xza9s1rSchCncDt(xza9s1rSchCncDtIdx - 1);
		writeString(position, xza9s1rSchCncDt, Len.XZA9S1R_SCH_CNC_DT);
	}

	/**Original name: XZA9S1R-SCH-CNC-DT<br>*/
	public String getXza9s1rSchCncDt(int xza9s1rSchCncDtIdx) {
		int position = Pos.xza9s1rSchCncDt(xza9s1rSchCncDtIdx - 1);
		return readString(position, Len.XZA9S1R_SCH_CNC_DT);
	}

	public void setXza9s1rNotTypCd(int xza9s1rNotTypCdIdx, String xza9s1rNotTypCd) {
		int position = Pos.xza9s1rNotTypCd(xza9s1rNotTypCdIdx - 1);
		writeString(position, xza9s1rNotTypCd, Len.XZA9S1R_NOT_TYP_CD);
	}

	/**Original name: XZA9S1R-NOT-TYP-CD<br>*/
	public String getXza9s1rNotTypCd(int xza9s1rNotTypCdIdx) {
		int position = Pos.xza9s1rNotTypCd(xza9s1rNotTypCdIdx - 1);
		return readString(position, Len.XZA9S1R_NOT_TYP_CD);
	}

	public void setXza9s1rNotTypDes(int xza9s1rNotTypDesIdx, String xza9s1rNotTypDes) {
		int position = Pos.xza9s1rNotTypDes(xza9s1rNotTypDesIdx - 1);
		writeString(position, xza9s1rNotTypDes, Len.XZA9S1R_NOT_TYP_DES);
	}

	/**Original name: XZA9S1R-NOT-TYP-DES<br>*/
	public String getXza9s1rNotTypDes(int xza9s1rNotTypDesIdx) {
		int position = Pos.xza9s1rNotTypDes(xza9s1rNotTypDesIdx - 1);
		return readString(position, Len.XZA9S1R_NOT_TYP_DES);
	}

	public void setXza9s1rNotEmpId(int xza9s1rNotEmpIdIdx, String xza9s1rNotEmpId) {
		int position = Pos.xza9s1rNotEmpId(xza9s1rNotEmpIdIdx - 1);
		writeString(position, xza9s1rNotEmpId, Len.XZA9S1R_NOT_EMP_ID);
	}

	/**Original name: XZA9S1R-NOT-EMP-ID<br>*/
	public String getXza9s1rNotEmpId(int xza9s1rNotEmpIdIdx) {
		int position = Pos.xza9s1rNotEmpId(xza9s1rNotEmpIdIdx - 1);
		return readString(position, Len.XZA9S1R_NOT_EMP_ID);
	}

	public void setXza9s1rNotEmpNm(int xza9s1rNotEmpNmIdx, String xza9s1rNotEmpNm) {
		int position = Pos.xza9s1rNotEmpNm(xza9s1rNotEmpNmIdx - 1);
		writeString(position, xza9s1rNotEmpNm, Len.XZA9S1R_NOT_EMP_NM);
	}

	/**Original name: XZA9S1R-NOT-EMP-NM<br>*/
	public String getXza9s1rNotEmpNm(int xza9s1rNotEmpNmIdx) {
		int position = Pos.xza9s1rNotEmpNm(xza9s1rNotEmpNmIdx - 1);
		return readString(position, Len.XZA9S1R_NOT_EMP_NM);
	}

	public void setXza9s1rNotPrcDt(int xza9s1rNotPrcDtIdx, String xza9s1rNotPrcDt) {
		int position = Pos.xza9s1rNotPrcDt(xza9s1rNotPrcDtIdx - 1);
		writeString(position, xza9s1rNotPrcDt, Len.XZA9S1R_NOT_PRC_DT);
	}

	/**Original name: XZA9S1R-NOT-PRC-DT<br>*/
	public String getXza9s1rNotPrcDt(int xza9s1rNotPrcDtIdx) {
		int position = Pos.xza9s1rNotPrcDt(xza9s1rNotPrcDtIdx - 1);
		return readString(position, Len.XZA9S1R_NOT_PRC_DT);
	}

	public void setXza9s1rTmnInd(int xza9s1rTmnIndIdx, char xza9s1rTmnInd) {
		int position = Pos.xza9s1rTmnInd(xza9s1rTmnIndIdx - 1);
		writeChar(position, xza9s1rTmnInd);
	}

	/**Original name: XZA9S1R-TMN-IND<br>*/
	public char getXza9s1rTmnInd(int xza9s1rTmnIndIdx) {
		int position = Pos.xza9s1rTmnInd(xza9s1rTmnIndIdx - 1);
		return readChar(position);
	}

	public void setXza9s1rTmnEmpId(int xza9s1rTmnEmpIdIdx, String xza9s1rTmnEmpId) {
		int position = Pos.xza9s1rTmnEmpId(xza9s1rTmnEmpIdIdx - 1);
		writeString(position, xza9s1rTmnEmpId, Len.XZA9S1R_TMN_EMP_ID);
	}

	/**Original name: XZA9S1R-TMN-EMP-ID<br>*/
	public String getXza9s1rTmnEmpId(int xza9s1rTmnEmpIdIdx) {
		int position = Pos.xza9s1rTmnEmpId(xza9s1rTmnEmpIdIdx - 1);
		return readString(position, Len.XZA9S1R_TMN_EMP_ID);
	}

	public void setXza9s1rTmnEmpNm(int xza9s1rTmnEmpNmIdx, String xza9s1rTmnEmpNm) {
		int position = Pos.xza9s1rTmnEmpNm(xza9s1rTmnEmpNmIdx - 1);
		writeString(position, xza9s1rTmnEmpNm, Len.XZA9S1R_TMN_EMP_NM);
	}

	/**Original name: XZA9S1R-TMN-EMP-NM<br>*/
	public String getXza9s1rTmnEmpNm(int xza9s1rTmnEmpNmIdx) {
		int position = Pos.xza9s1rTmnEmpNm(xza9s1rTmnEmpNmIdx - 1);
		return readString(position, Len.XZA9S1R_TMN_EMP_NM);
	}

	public void setXza9s1rTmnPrcDt(int xza9s1rTmnPrcDtIdx, String xza9s1rTmnPrcDt) {
		int position = Pos.xza9s1rTmnPrcDt(xza9s1rTmnPrcDtIdx - 1);
		writeString(position, xza9s1rTmnPrcDt, Len.XZA9S1R_TMN_PRC_DT);
	}

	/**Original name: XZA9S1R-TMN-PRC-DT<br>*/
	public String getXza9s1rTmnPrcDt(int xza9s1rTmnPrcDtIdx) {
		int position = Pos.xza9s1rTmnPrcDt(xza9s1rTmnPrcDtIdx - 1);
		return readString(position, Len.XZA9S1R_TMN_PRC_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespAcyPndCncTmnPol(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_ACY_PND_CNC_TMN_POL;
		}

		public static int xza9s1rPolicyLocateRow(int idx) {
			return lFwRespAcyPndCncTmnPol(idx);
		}

		public static int xza9s1rPolicyRow(int idx) {
			return xza9s1rPolicyLocateRow(idx);
		}

		public static int xza9s1rPolicyId(int idx) {
			return xza9s1rPolicyRow(idx);
		}

		public static int xza9s1rPolNbr(int idx) {
			return xza9s1rPolicyId(idx) + Len.XZA9S1R_POLICY_ID;
		}

		public static int xza9s1rOgnEffDt(int idx) {
			return xza9s1rPolNbr(idx) + Len.XZA9S1R_POL_NBR;
		}

		public static int xza9s1rPlnExpDt(int idx) {
			return xza9s1rOgnEffDt(idx) + Len.XZA9S1R_OGN_EFF_DT;
		}

		public static int xza9s1rQuoteSequenceNbr(int idx) {
			return xza9s1rPlnExpDt(idx) + Len.XZA9S1R_PLN_EXP_DT;
		}

		public static int xza9s1rActNbr(int idx) {
			return xza9s1rQuoteSequenceNbr(idx) + Len.XZA9S1R_QUOTE_SEQUENCE_NBR;
		}

		public static int xza9s1rActNbrFmt(int idx) {
			return xza9s1rActNbr(idx) + Len.XZA9S1R_ACT_NBR;
		}

		public static int xza9s1rPriRskStAbb(int idx) {
			return xza9s1rActNbrFmt(idx) + Len.XZA9S1R_ACT_NBR_FMT;
		}

		public static int xza9s1rPriRskStDes(int idx) {
			return xza9s1rPriRskStAbb(idx) + Len.XZA9S1R_PRI_RSK_ST_ABB;
		}

		public static int xza9s1rLobCd(int idx) {
			return xza9s1rPriRskStDes(idx) + Len.XZA9S1R_PRI_RSK_ST_DES;
		}

		public static int xza9s1rLobDes(int idx) {
			return xza9s1rLobCd(idx) + Len.XZA9S1R_LOB_CD;
		}

		public static int xza9s1rTobCd(int idx) {
			return xza9s1rLobDes(idx) + Len.XZA9S1R_LOB_DES;
		}

		public static int xza9s1rTobDes(int idx) {
			return xza9s1rTobCd(idx) + Len.XZA9S1R_TOB_CD;
		}

		public static int xza9s1rSegCd(int idx) {
			return xza9s1rTobDes(idx) + Len.XZA9S1R_TOB_DES;
		}

		public static int xza9s1rSegDes(int idx) {
			return xza9s1rSegCd(idx) + Len.XZA9S1R_SEG_CD;
		}

		public static int xza9s1rLgeActInd(int idx) {
			return xza9s1rSegDes(idx) + Len.XZA9S1R_SEG_DES;
		}

		public static int xza9s1rStatusCd(int idx) {
			return xza9s1rLgeActInd(idx) + Len.XZA9S1R_LGE_ACT_IND;
		}

		public static int xza9s1rCurPndTrsInd(int idx) {
			return xza9s1rStatusCd(idx) + Len.XZA9S1R_STATUS_CD;
		}

		public static int xza9s1rCurStaCd(int idx) {
			return xza9s1rCurPndTrsInd(idx) + Len.XZA9S1R_CUR_PND_TRS_IND;
		}

		public static int xza9s1rCurStaDes(int idx) {
			return xza9s1rCurStaCd(idx) + Len.XZA9S1R_CUR_STA_CD;
		}

		public static int xza9s1rCurTrsTypCd(int idx) {
			return xza9s1rCurStaDes(idx) + Len.XZA9S1R_CUR_STA_DES;
		}

		public static int xza9s1rCurTrsTypDes(int idx) {
			return xza9s1rCurTrsTypCd(idx) + Len.XZA9S1R_CUR_TRS_TYP_CD;
		}

		public static int xza9s1rCoverageParts(int idx) {
			return xza9s1rCurTrsTypDes(idx) + Len.XZA9S1R_CUR_TRS_TYP_DES;
		}

		public static int xza9s1rRltTypCd(int idx) {
			return xza9s1rCoverageParts(idx) + Len.XZA9S1R_COVERAGE_PARTS;
		}

		public static int xza9s1rMnlPolInd(int idx) {
			return xza9s1rRltTypCd(idx) + Len.XZA9S1R_RLT_TYP_CD;
		}

		public static int xza9s1rCurOgnCd(int idx) {
			return xza9s1rMnlPolInd(idx) + Len.XZA9S1R_MNL_POL_IND;
		}

		public static int xza9s1rCurOgnDes(int idx) {
			return xza9s1rCurOgnCd(idx) + Len.XZA9S1R_CUR_OGN_CD;
		}

		public static int xza9s1rBondInd(int idx) {
			return xza9s1rCurOgnDes(idx) + Len.XZA9S1R_CUR_OGN_DES;
		}

		public static int xza9s1rBranchNbr(int idx) {
			return xza9s1rBondInd(idx) + Len.XZA9S1R_BOND_IND;
		}

		public static int xza9s1rBranchDesc(int idx) {
			return xza9s1rBranchNbr(idx) + Len.XZA9S1R_BRANCH_NBR;
		}

		public static int xza9s1rWrtPremAmt(int idx) {
			return xza9s1rBranchDesc(idx) + Len.XZA9S1R_BRANCH_DESC;
		}

		public static int xza9s1rPolActInd(int idx) {
			return xza9s1rWrtPremAmt(idx) + Len.XZA9S1R_WRT_PREM_AMT;
		}

		public static int xza9s1rPndCncInd(int idx) {
			return xza9s1rPolActInd(idx) + Len.XZA9S1R_POL_ACT_IND;
		}

		public static int xza9s1rSchCncDt(int idx) {
			return xza9s1rPndCncInd(idx) + Len.XZA9S1R_PND_CNC_IND;
		}

		public static int xza9s1rNotTypCd(int idx) {
			return xza9s1rSchCncDt(idx) + Len.XZA9S1R_SCH_CNC_DT;
		}

		public static int xza9s1rNotTypDes(int idx) {
			return xza9s1rNotTypCd(idx) + Len.XZA9S1R_NOT_TYP_CD;
		}

		public static int xza9s1rNotEmpId(int idx) {
			return xza9s1rNotTypDes(idx) + Len.XZA9S1R_NOT_TYP_DES;
		}

		public static int xza9s1rNotEmpNm(int idx) {
			return xza9s1rNotEmpId(idx) + Len.XZA9S1R_NOT_EMP_ID;
		}

		public static int xza9s1rNotPrcDt(int idx) {
			return xza9s1rNotEmpNm(idx) + Len.XZA9S1R_NOT_EMP_NM;
		}

		public static int xza9s1rTmnInd(int idx) {
			return xza9s1rNotPrcDt(idx) + Len.XZA9S1R_NOT_PRC_DT;
		}

		public static int xza9s1rTmnEmpId(int idx) {
			return xza9s1rTmnInd(idx) + Len.XZA9S1R_TMN_IND;
		}

		public static int xza9s1rTmnEmpNm(int idx) {
			return xza9s1rTmnEmpId(idx) + Len.XZA9S1R_TMN_EMP_ID;
		}

		public static int xza9s1rTmnPrcDt(int idx) {
			return xza9s1rTmnEmpNm(idx) + Len.XZA9S1R_TMN_EMP_NM;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9S1R_POLICY_ID = 16;
		public static final int XZA9S1R_POL_NBR = 25;
		public static final int XZA9S1R_OGN_EFF_DT = 10;
		public static final int XZA9S1R_PLN_EXP_DT = 10;
		public static final int XZA9S1R_QUOTE_SEQUENCE_NBR = 5;
		public static final int XZA9S1R_ACT_NBR = 9;
		public static final int XZA9S1R_ACT_NBR_FMT = 9;
		public static final int XZA9S1R_PRI_RSK_ST_ABB = 3;
		public static final int XZA9S1R_PRI_RSK_ST_DES = 25;
		public static final int XZA9S1R_LOB_CD = 3;
		public static final int XZA9S1R_LOB_DES = 45;
		public static final int XZA9S1R_TOB_CD = 3;
		public static final int XZA9S1R_TOB_DES = 40;
		public static final int XZA9S1R_SEG_CD = 3;
		public static final int XZA9S1R_SEG_DES = 40;
		public static final int XZA9S1R_LGE_ACT_IND = 1;
		public static final int XZA9S1R_STATUS_CD = 1;
		public static final int XZA9S1R_CUR_PND_TRS_IND = 1;
		public static final int XZA9S1R_CUR_STA_CD = 1;
		public static final int XZA9S1R_CUR_STA_DES = 40;
		public static final int XZA9S1R_CUR_TRS_TYP_CD = 1;
		public static final int XZA9S1R_CUR_TRS_TYP_DES = 35;
		public static final int XZA9S1R_COVERAGE_PARTS = 40;
		public static final int XZA9S1R_RLT_TYP_CD = 4;
		public static final int XZA9S1R_MNL_POL_IND = 1;
		public static final int XZA9S1R_CUR_OGN_CD = 1;
		public static final int XZA9S1R_CUR_OGN_DES = 20;
		public static final int XZA9S1R_BOND_IND = 1;
		public static final int XZA9S1R_BRANCH_NBR = 2;
		public static final int XZA9S1R_BRANCH_DESC = 45;
		public static final int XZA9S1R_WRT_PREM_AMT = 11;
		public static final int XZA9S1R_POL_ACT_IND = 1;
		public static final int XZA9S1R_PND_CNC_IND = 1;
		public static final int XZA9S1R_SCH_CNC_DT = 10;
		public static final int XZA9S1R_NOT_TYP_CD = 5;
		public static final int XZA9S1R_NOT_TYP_DES = 35;
		public static final int XZA9S1R_NOT_EMP_ID = 6;
		public static final int XZA9S1R_NOT_EMP_NM = 67;
		public static final int XZA9S1R_NOT_PRC_DT = 10;
		public static final int XZA9S1R_TMN_IND = 1;
		public static final int XZA9S1R_TMN_EMP_ID = 6;
		public static final int XZA9S1R_TMN_EMP_NM = 67;
		public static final int XZA9S1R_TMN_PRC_DT = 10;
		public static final int XZA9S1R_POLICY_ROW = XZA9S1R_POLICY_ID + XZA9S1R_POL_NBR + XZA9S1R_OGN_EFF_DT + XZA9S1R_PLN_EXP_DT
				+ XZA9S1R_QUOTE_SEQUENCE_NBR + XZA9S1R_ACT_NBR + XZA9S1R_ACT_NBR_FMT + XZA9S1R_PRI_RSK_ST_ABB + XZA9S1R_PRI_RSK_ST_DES
				+ XZA9S1R_LOB_CD + XZA9S1R_LOB_DES + XZA9S1R_TOB_CD + XZA9S1R_TOB_DES + XZA9S1R_SEG_CD + XZA9S1R_SEG_DES + XZA9S1R_LGE_ACT_IND
				+ XZA9S1R_STATUS_CD + XZA9S1R_CUR_PND_TRS_IND + XZA9S1R_CUR_STA_CD + XZA9S1R_CUR_STA_DES + XZA9S1R_CUR_TRS_TYP_CD
				+ XZA9S1R_CUR_TRS_TYP_DES + XZA9S1R_COVERAGE_PARTS + XZA9S1R_RLT_TYP_CD + XZA9S1R_MNL_POL_IND + XZA9S1R_CUR_OGN_CD
				+ XZA9S1R_CUR_OGN_DES + XZA9S1R_BOND_IND + XZA9S1R_BRANCH_NBR + XZA9S1R_BRANCH_DESC + XZA9S1R_WRT_PREM_AMT + XZA9S1R_POL_ACT_IND
				+ XZA9S1R_PND_CNC_IND + XZA9S1R_SCH_CNC_DT + XZA9S1R_NOT_TYP_CD + XZA9S1R_NOT_TYP_DES + XZA9S1R_NOT_EMP_ID + XZA9S1R_NOT_EMP_NM
				+ XZA9S1R_NOT_PRC_DT + XZA9S1R_TMN_IND + XZA9S1R_TMN_EMP_ID + XZA9S1R_TMN_EMP_NM + XZA9S1R_TMN_PRC_DT;
		public static final int XZA9S1R_POLICY_LOCATE_ROW = XZA9S1R_POLICY_ROW;
		public static final int L_FW_RESP_ACY_PND_CNC_TMN_POL = XZA9S1R_POLICY_LOCATE_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0x90s0.L_FW_RESP_ACY_PND_CNC_TMN_POL_MAXOCCURS
				* L_FW_RESP_ACY_PND_CNC_TMN_POL;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA9S1R_QUOTE_SEQUENCE_NBR = 5;
			public static final int XZA9S1R_WRT_PREM_AMT = 11;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
