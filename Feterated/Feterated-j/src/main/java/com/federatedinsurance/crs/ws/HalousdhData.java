/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalBoMduXrfVUowObjHierPrcSeq;
import com.federatedinsurance.crs.commons.data.to.IHalUowFunSecV;
import com.federatedinsurance.crs.copy.DclgenHalUniversalCt2;
import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalUowObjHierV;
import com.federatedinsurance.crs.copy.DclhalUowPrcSeq;
import com.federatedinsurance.crs.copy.Hallcctx;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Halluidg;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.HssvcHalScrnScriptRow;
import com.federatedinsurance.crs.copy.HssvhHalScrnScriptRow;
import com.federatedinsurance.crs.copy.HufscHalUowFunSecRow;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.redefines.CidpTableInfo;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALOUSDH<br>
 * Generated as a class for rule WS.<br>*/
public class HalousdhData implements IHalUowFunSecV, IHalBoMduXrfVUowObjHierPrcSeq {

	//==== PROPERTIES ====
	//Original name: WS-SPECIFIC-WORK-AREAS
	private WsSpecificWorkAreasHalousdh wsSpecificWorkAreas = new WsSpecificWorkAreasHalousdh();
	//Original name: WS-WORK-FIELDS
	private WsWorkFieldsHalousdh wsWorkFields = new WsWorkFieldsHalousdh();
	//Original name: WS-SWITCHES
	private WsSwitchesHalousdh wsSwitches = new WsSwitchesHalousdh();
	//Original name: FILLER-WS-TSQ1-QUEUE-NAME
	private String flr3 = "SD1";
	//Original name: WS-TASKNBR-TSQ1
	private String wsTasknbrTsq1 = DefaultValues.stringVal(Len.WS_TASKNBR_TSQ1);
	//Original name: WS-TSQ1-QUEUE-CNT
	private short wsTsq1QueueCnt = ((short) 0);
	//Original name: WS-TSQ1-BOBJ-NM
	private String wsTsq1BobjNm = DefaultValues.stringVal(Len.WS_TSQ1_BOBJ_NM);
	//Original name: WS-TSQ1-DFL-MDU-NM
	private String wsTsq1DflMduNm = DefaultValues.stringVal(Len.WS_TSQ1_DFL_MDU_NM);
	//Original name: FILLER-WS-TSQ2-QUEUE-NAME
	private String flr4 = "SD2";
	//Original name: WS-TASKNBR-TSQ2
	private String wsTasknbrTsq2 = DefaultValues.stringVal(Len.WS_TASKNBR_TSQ2);
	//Original name: WS-TSQ2-QUEUE-CNT
	private short wsTsq2QueueCnt = ((short) 0);
	//Original name: WS-TSQ2-DATA-AREA
	private WsTsq2DataArea wsTsq2DataArea = new WsTsq2DataArea();
	//Original name: FILLER-WS-TSQ3-QUEUE-NAME
	private String flr5 = "SD3";
	//Original name: WS-TASKNBR-TSQ3
	private String wsTasknbrTsq3 = DefaultValues.stringVal(Len.WS_TASKNBR_TSQ3);
	//Original name: WS-TSQ3-QUEUE-CNT
	private short wsTsq3QueueCnt = ((short) 0);
	//Original name: WS-BDO-WORK-FIELDS
	private WsBdoWorkFields wsBdoWorkFields = new WsBdoWorkFields();
	//Original name: WS-BDO-SWITCHES
	private WsBdoSwitchesHalousdh wsBdoSwitches = new WsBdoSwitchesHalousdh();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: HALLUSW
	private Hallusw hallusw = new Hallusw();
	//Original name: HALLUDAT
	private Halludat halludat = new Halludat();
	//Original name: HALLUHDR
	private Halluhdr halluhdr = new Halluhdr();
	//Original name: CIDP-TABLE-INFO
	private CidpTableInfo cidpTableInfo = new CidpTableInfo();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: DCLHAL-UOW-OBJ-HIER-V
	private DclhalUowObjHierV dclhalUowObjHierV = new DclhalUowObjHierV();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: USDH-BUS-OBJ-NM
	private String usdhBusObjNm = DefaultValues.stringVal(Len.USDH_BUS_OBJ_NM);
	//Original name: HUFSC-HAL-UOW-FUN-SEC-ROW
	private HufscHalUowFunSecRow hufscHalUowFunSecRow = new HufscHalUowFunSecRow();
	//Original name: HALLCCTX
	private Hallcctx hallcctx = new Hallcctx();
	//Original name: HSSVC-HAL-SCRN-SCRIPT-ROW
	private HssvcHalScrnScriptRow hssvcHalScrnScriptRow = new HssvcHalScrnScriptRow();
	//Original name: HUFSH-UOW-FUN-SEC-ROW
	private WsTsq2DataArea hufshUowFunSecRow = new WsTsq2DataArea();
	//Original name: DCLHAL-UOW-PRC-SEQ
	private DclhalUowPrcSeq dclhalUowPrcSeq = new DclhalUowPrcSeq();
	//Original name: HSSVH-HAL-SCRN-SCRIPT-ROW
	private HssvhHalScrnScriptRow hssvhHalScrnScriptRow = new HssvhHalScrnScriptRow();
	//Original name: DCLGEN-HAL-UNIVERSAL-CT2
	private DclgenHalUniversalCt2 dclgenHalUniversalCt2 = new DclgenHalUniversalCt2();

	//==== METHODS ====
	public String getWsTsq1QueueNameFormatted() {
		return MarshalByteExt.bufferToStr(getWsTsq1QueueNameBytes());
	}

	/**Original name: WS-TSQ1-QUEUE-NAME<br>*/
	public byte[] getWsTsq1QueueNameBytes() {
		byte[] buffer = new byte[Len.WS_TSQ1_QUEUE_NAME];
		return getWsTsq1QueueNameBytes(buffer, 1);
	}

	public byte[] getWsTsq1QueueNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, wsTasknbrTsq1, Len.WS_TASKNBR_TSQ1);
		return buffer;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setWsTasknbrTsq1(String wsTasknbrTsq1) {
		this.wsTasknbrTsq1 = Functions.subString(wsTasknbrTsq1, Len.WS_TASKNBR_TSQ1);
	}

	public String getWsTasknbrTsq1() {
		return this.wsTasknbrTsq1;
	}

	public String getWsTasknbrTsq1Formatted() {
		return Functions.padBlanks(getWsTasknbrTsq1(), Len.WS_TASKNBR_TSQ1);
	}

	public void setWsTsq1QueueCnt(short wsTsq1QueueCnt) {
		this.wsTsq1QueueCnt = wsTsq1QueueCnt;
	}

	public short getWsTsq1QueueCnt() {
		return this.wsTsq1QueueCnt;
	}

	public void setWsTsq1DataAreaBytes(byte[] buffer) {
		setWsTsq1DataAreaBytes(buffer, 1);
	}

	/**Original name: WS-TSQ1-DATA-AREA<br>*/
	public byte[] getWsTsq1DataAreaBytes() {
		byte[] buffer = new byte[Len.WS_TSQ1_DATA_AREA];
		return getWsTsq1DataAreaBytes(buffer, 1);
	}

	public void setWsTsq1DataAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		wsTsq1BobjNm = MarshalByte.readString(buffer, position, Len.WS_TSQ1_BOBJ_NM);
		position += Len.WS_TSQ1_BOBJ_NM;
		wsTsq1DflMduNm = MarshalByte.readString(buffer, position, Len.WS_TSQ1_DFL_MDU_NM);
	}

	public byte[] getWsTsq1DataAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, wsTsq1BobjNm, Len.WS_TSQ1_BOBJ_NM);
		position += Len.WS_TSQ1_BOBJ_NM;
		MarshalByte.writeString(buffer, position, wsTsq1DflMduNm, Len.WS_TSQ1_DFL_MDU_NM);
		return buffer;
	}

	public void initWsTsq1DataAreaSpaces() {
		wsTsq1BobjNm = "";
		wsTsq1DflMduNm = "";
	}

	public void setWsTsq1BobjNm(String wsTsq1BobjNm) {
		this.wsTsq1BobjNm = Functions.subString(wsTsq1BobjNm, Len.WS_TSQ1_BOBJ_NM);
	}

	public String getWsTsq1BobjNm() {
		return this.wsTsq1BobjNm;
	}

	public void setWsTsq1DflMduNm(String wsTsq1DflMduNm) {
		this.wsTsq1DflMduNm = Functions.subString(wsTsq1DflMduNm, Len.WS_TSQ1_DFL_MDU_NM);
	}

	public String getWsTsq1DflMduNm() {
		return this.wsTsq1DflMduNm;
	}

	public String getWsTsq2QueueNameFormatted() {
		return MarshalByteExt.bufferToStr(getWsTsq2QueueNameBytes());
	}

	/**Original name: WS-TSQ2-QUEUE-NAME<br>*/
	public byte[] getWsTsq2QueueNameBytes() {
		byte[] buffer = new byte[Len.WS_TSQ2_QUEUE_NAME];
		return getWsTsq2QueueNameBytes(buffer, 1);
	}

	public byte[] getWsTsq2QueueNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, wsTasknbrTsq2, Len.WS_TASKNBR_TSQ2);
		return buffer;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setWsTasknbrTsq2(String wsTasknbrTsq2) {
		this.wsTasknbrTsq2 = Functions.subString(wsTasknbrTsq2, Len.WS_TASKNBR_TSQ2);
	}

	public String getWsTasknbrTsq2() {
		return this.wsTasknbrTsq2;
	}

	public void setWsTsq2QueueCnt(short wsTsq2QueueCnt) {
		this.wsTsq2QueueCnt = wsTsq2QueueCnt;
	}

	public short getWsTsq2QueueCnt() {
		return this.wsTsq2QueueCnt;
	}

	public String getWsTsq3QueueNameFormatted() {
		return MarshalByteExt.bufferToStr(getWsTsq3QueueNameBytes());
	}

	/**Original name: WS-TSQ3-QUEUE-NAME<br>*/
	public byte[] getWsTsq3QueueNameBytes() {
		byte[] buffer = new byte[Len.WS_TSQ3_QUEUE_NAME];
		return getWsTsq3QueueNameBytes(buffer, 1);
	}

	public byte[] getWsTsq3QueueNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, wsTasknbrTsq3, Len.WS_TASKNBR_TSQ3);
		return buffer;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setWsTasknbrTsq3(String wsTasknbrTsq3) {
		this.wsTasknbrTsq3 = Functions.subString(wsTasknbrTsq3, Len.WS_TASKNBR_TSQ3);
	}

	public String getWsTasknbrTsq3() {
		return this.wsTasknbrTsq3;
	}

	public void setWsTsq3QueueCnt(short wsTsq3QueueCnt) {
		this.wsTsq3QueueCnt = wsTsq3QueueCnt;
	}

	public short getWsTsq3QueueCnt() {
		return this.wsTsq3QueueCnt;
	}

	public void setHalouidgLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.HALOUIDG_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.HALOUIDG_LINKAGE);
		setHalouidgLinkageBytes(buffer, 1);
	}

	public String getHalouidgLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHalouidgLinkageBytes());
	}

	/**Original name: HALOUIDG-LINKAGE<br>
	 * <pre>* HALOUIDG (UNIVERSAL ID GENERATOR) LINKAGE</pre>*/
	public byte[] getHalouidgLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUIDG_LINKAGE];
		return getHalouidgLinkageBytes(buffer, 1);
	}

	public void setHalouidgLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.setUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.setUidgCaOutputBytes(buffer, position);
	}

	public byte[] getHalouidgLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.getUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.getUidgCaOutputBytes(buffer, position);
		return buffer;
	}

	public void initWsDataUmtAreaSpaces() {
		halludat.initHalludatSpaces();
	}

	public void initWsHdrUmtAreaSpaces() {
		halluhdr.initHalluhdrSpaces();
	}

	public void setWsDataPrivacyInfoFormatted(String data) {
		byte[] buffer = new byte[Len.WS_DATA_PRIVACY_INFO];
		MarshalByte.writeString(buffer, 1, data, Len.WS_DATA_PRIVACY_INFO);
		setWsDataPrivacyInfoBytes(buffer, 1);
	}

	public String getWsDataPrivacyInfoFormatted() {
		return getCidpPassedInfoFormatted();
	}

	public void setWsDataPrivacyInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setCidpPassedInfoBytes(buffer, position);
	}

	public String getCidpPassedInfoFormatted() {
		return MarshalByteExt.bufferToStr(getCidpPassedInfoBytes());
	}

	/**Original name: CIDP-PASSED-INFO<br>
	 * <pre>****************************************************************
	 *  HALLCIDP                                                      *
	 *  COMMON INTERFACE TO DATA PRIVACY                              *
	 *  USED BY BUSINESS DATA OBJECTS TO PASS DATA ROWS FOR           *
	 *  DATA PRIVACY CHECKING.                                        *
	 * ****************************************************************
	 *  SI#       PRGRMR  DATE       DESCRIPTION                      *
	 *  --------- ------- ---------- ---------------------------------*
	 *  SAVANNAH  PM      31MAY2000  INITIAL CODING                   *
	 *  14969     18448   25JUN2001  MADE CHANGES REQUIRED FOR        *
	 *                               ENHANCED DATA PRIVACY AND SET    *
	 *                               DEFAULTS PROCESSING.             *
	 *                               (ARCHITECTURE 2.3)               *
	 * ****************************************************************</pre>*/
	public byte[] getCidpPassedInfoBytes() {
		byte[] buffer = new byte[Len.CIDP_PASSED_INFO];
		return getCidpPassedInfoBytes(buffer, 1);
	}

	public void setCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.setCidpTableInfoBytes(buffer, position);
	}

	public byte[] getCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.getCidpTableInfoBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public String getUsdhSetDefaultsRowFormatted() {
		return getUsdhBusObjNmFormatted();
	}

	public void setUsdhBusObjNm(String usdhBusObjNm) {
		this.usdhBusObjNm = Functions.subString(usdhBusObjNm, Len.USDH_BUS_OBJ_NM);
	}

	public String getUsdhBusObjNm() {
		return this.usdhBusObjNm;
	}

	public String getUsdhBusObjNmFormatted() {
		return Functions.padBlanks(getUsdhBusObjNm(), Len.USDH_BUS_OBJ_NM);
	}

	public String getWsScriptRowFormatted() {
		return hssvcHalScrnScriptRow.getHssvcHalScrnScriptRowFormatted();
	}

	public void setWsScriptRowBytes(byte[] buffer) {
		setWsScriptRowBytes(buffer, 1);
	}

	/**Original name: WS-SCRIPT-ROW<br>
	 * <pre>*  FRONT END/ BACK END COMMUNICATION LAYOUT FOR
	 * *  SCRIPTS RESPONSE MESSAGE</pre>*/
	public byte[] getWsScriptRowBytes() {
		byte[] buffer = new byte[Len.WS_SCRIPT_ROW];
		return getWsScriptRowBytes(buffer, 1);
	}

	public void setWsScriptRowBytes(byte[] buffer, int offset) {
		int position = offset;
		hssvcHalScrnScriptRow.setHssvcHalScrnScriptRowBytes(buffer, position);
	}

	public byte[] getWsScriptRowBytes(byte[] buffer, int offset) {
		int position = offset;
		hssvcHalScrnScriptRow.getHssvcHalScrnScriptRowBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getAppNm() {
		throw new FieldNotMappedException("appNm");
	}

	@Override
	public void setAppNm(String appNm) {
		throw new FieldNotMappedException("appNm");
	}

	@Override
	public String getAutActionCd() {
		throw new FieldNotMappedException("autActionCd");
	}

	@Override
	public void setAutActionCd(String autActionCd) {
		throw new FieldNotMappedException("autActionCd");
	}

	@Override
	public String getAutFunNm() {
		throw new FieldNotMappedException("autFunNm");
	}

	@Override
	public void setAutFunNm(String autFunNm) {
		throw new FieldNotMappedException("autFunNm");
	}

	public CidpTableInfo getCidpTableInfo() {
		return cidpTableInfo;
	}

	public DclgenHalUniversalCt2 getDclgenHalUniversalCt2() {
		return dclgenHalUniversalCt2;
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalUowObjHierV getDclhalUowObjHierV() {
		return dclhalUowObjHierV;
	}

	public DclhalUowPrcSeq getDclhalUowPrcSeq() {
		return dclhalUowPrcSeq;
	}

	@Override
	public short getFunSeqNbr() {
		throw new FieldNotMappedException("funSeqNbr");
	}

	@Override
	public void setFunSeqNbr(short funSeqNbr) {
		throw new FieldNotMappedException("funSeqNbr");
	}

	public Hallcctx getHallcctx() {
		return hallcctx;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Halluhdr getHalluhdr() {
		return halluhdr;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	@Override
	public String getHbmxDpDflMduNm() {
		return dclhalBoMduXrfV.getDpDflMduNm();
	}

	@Override
	public void setHbmxDpDflMduNm(String hbmxDpDflMduNm) {
		this.dclhalBoMduXrfV.setDpDflMduNm(hbmxDpDflMduNm);
	}

	public HssvcHalScrnScriptRow getHssvcHalScrnScriptRow() {
		return hssvcHalScrnScriptRow;
	}

	public HssvhHalScrnScriptRow getHssvhHalScrnScriptRow() {
		return hssvhHalScrnScriptRow;
	}

	public HufscHalUowFunSecRow getHufscHalUowFunSecRow() {
		return hufscHalUowFunSecRow;
	}

	@Override
	public String getHufshSecAscTyp() {
		return hufshUowFunSecRow.getSecAscTyp();
	}

	@Override
	public void setHufshSecAscTyp(String hufshSecAscTyp) {
		this.hufshUowFunSecRow.setSecAscTyp(hufshSecAscTyp);
	}

	@Override
	public String getHufshSecGrpNm() {
		return hufshUowFunSecRow.getSecGrpNm();
	}

	@Override
	public void setHufshSecGrpNm(String hufshSecGrpNm) {
		this.hufshUowFunSecRow.setSecGrpNm(hufshSecGrpNm);
	}

	public WsTsq2DataArea getHufshUowFunSecRow() {
		return hufshUowFunSecRow;
	}

	@Override
	public String getHufshUowNm() {
		return hufshUowFunSecRow.getUowNm();
	}

	@Override
	public void setHufshUowNm(String hufshUowNm) {
		this.hufshUowFunSecRow.setUowNm(hufshUowNm);
	}

	@Override
	public String getLnkToMduNm() {
		throw new FieldNotMappedException("lnkToMduNm");
	}

	@Override
	public void setLnkToMduNm(String lnkToMduNm) {
		throw new FieldNotMappedException("lnkToMduNm");
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	@Override
	public String getWsAuthUseridForCursor() {
		return wsWorkFields.getWsAuthUseridForCursor();
	}

	@Override
	public void setWsAuthUseridForCursor(String wsAuthUseridForCursor) {
		this.wsWorkFields.setWsAuthUseridForCursor(wsAuthUseridForCursor);
	}

	public WsBdoSwitchesHalousdh getWsBdoSwitches() {
		return wsBdoSwitches;
	}

	public WsBdoWorkFields getWsBdoWorkFields() {
		return wsBdoWorkFields;
	}

	@Override
	public String getWsContextFirst10ForCsr() {
		return wsWorkFields.getWsContextFirst10ForCsr();
	}

	@Override
	public void setWsContextFirst10ForCsr(String wsContextFirst10ForCsr) {
		this.wsWorkFields.setWsContextFirst10ForCsr(wsContextFirst10ForCsr);
	}

	@Override
	public String getWsCur1BobjNm() {
		return wsWorkFields.getWsCur1BobjNm();
	}

	@Override
	public void setWsCur1BobjNm(String wsCur1BobjNm) {
		this.wsWorkFields.setWsCur1BobjNm(wsCur1BobjNm);
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	@Override
	public String getWsFullContext() {
		return wsWorkFields.getWsContextRedef().getWsFullContext();
	}

	@Override
	public void setWsFullContext(String wsFullContext) {
		this.wsWorkFields.getWsContextRedef().setWsFullContext(wsFullContext);
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSpecificWorkAreasHalousdh getWsSpecificWorkAreas() {
		return wsSpecificWorkAreas;
	}

	public WsSwitchesHalousdh getWsSwitches() {
		return wsSwitches;
	}

	public WsTsq2DataArea getWsTsq2DataArea() {
		return wsTsq2DataArea;
	}

	public WsWorkFieldsHalousdh getWsWorkFields() {
		return wsWorkFields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_TASKNBR_TSQ1 = 13;
		public static final int WS_TSQ1_BOBJ_NM = 32;
		public static final int WS_TSQ1_DFL_MDU_NM = 32;
		public static final int WS_TASKNBR_TSQ2 = 13;
		public static final int WS_TASKNBR_TSQ3 = 13;
		public static final int WS_APPLID = 8;
		public static final int USDH_BUS_OBJ_NM = 32;
		public static final int HALOUIDG_LINKAGE = Halluidg.Len.UIDG_CA_INCOMING + Halluidg.Len.UIDG_CA_OUTPUT;
		public static final int FLR3 = 3;
		public static final int WS_TSQ1_QUEUE_NAME = WS_TASKNBR_TSQ1 + FLR3;
		public static final int WS_TSQ1_DATA_AREA = WS_TSQ1_BOBJ_NM + WS_TSQ1_DFL_MDU_NM;
		public static final int WS_TSQ2_QUEUE_NAME = WS_TASKNBR_TSQ2 + FLR3;
		public static final int WS_TSQ3_QUEUE_NAME = WS_TASKNBR_TSQ3 + FLR3;
		public static final int WS_SCRIPT_ROW = HssvcHalScrnScriptRow.Len.HSSVC_HAL_SCRN_SCRIPT_ROW;
		public static final int CIDP_PASSED_INFO = CidpTableInfo.Len.CIDP_TABLE_INFO;
		public static final int WS_DATA_PRIVACY_INFO = CIDP_PASSED_INFO;
		public static final int USDH_SET_DEFAULTS_ROW = USDH_BUS_OBJ_NM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
