/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-05-WEB-URI-FAULT<br>
 * Variable: EA-05-WEB-URI-FAULT from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea05WebUriFault {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-05-WEB-URI-FAULT
	private String flr1 = "UNABLE TO";
	//Original name: FILLER-EA-05-WEB-URI-FAULT-1
	private String flr2 = "FIND WEB URI";
	//Original name: FILLER-EA-05-WEB-URI-FAULT-2
	private String flr3 = "TO CALL WEB";
	//Original name: FILLER-EA-05-WEB-URI-FAULT-3
	private String flr4 = "SERVICE.";
	//Original name: FILLER-EA-05-WEB-URI-FAULT-4
	private String flr5 = "SERVICE ID:";
	//Original name: EA-05-WEB-SVC-ID
	private String ea05WebSvcId = DefaultValues.stringVal(Len.EA05_WEB_SVC_ID);

	//==== METHODS ====
	public String getEa05WebUriFaultFormatted() {
		return MarshalByteExt.bufferToStr(getEa05WebUriFaultBytes());
	}

	public byte[] getEa05WebUriFaultBytes() {
		byte[] buffer = new byte[Len.EA05_WEB_URI_FAULT];
		return getEa05WebUriFaultBytes(buffer, 1);
	}

	public byte[] getEa05WebUriFaultBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea05WebSvcId, Len.EA05_WEB_SVC_ID);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa05WebSvcId(String ea05WebSvcId) {
		this.ea05WebSvcId = Functions.subString(ea05WebSvcId, Len.EA05_WEB_SVC_ID);
	}

	public String getEa05WebSvcId() {
		return this.ea05WebSvcId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA05_WEB_SVC_ID = 12;
		public static final int FLR1 = 10;
		public static final int FLR2 = 13;
		public static final int FLR3 = 12;
		public static final int FLR4 = 9;
		public static final int EA05_WEB_URI_FAULT = EA05_WEB_SVC_ID + FLR1 + FLR2 + 2 * FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
