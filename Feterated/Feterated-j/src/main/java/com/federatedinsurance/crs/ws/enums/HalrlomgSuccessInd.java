/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: HALRLOMG-SUCCESS-IND<br>
 * Variable: HALRLOMG-SUCCESS-IND from copybook HALLLOMG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HalrlomgSuccessInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FOUND = 'F';
	public static final char NOT_FOUND = 'N';
	public static final char ZAPPED = 'Z';
	public static final char TYPE_MISMATCH = 'M';
	public static final char LOCK_TAKEOVER = 'T';

	//==== METHODS ====
	public void setSuccessInd(char successInd) {
		this.value = successInd;
	}

	public char getSuccessInd() {
		return this.value;
	}

	public boolean isFound() {
		return value == FOUND;
	}

	public void setFound() {
		value = FOUND;
	}

	public boolean isNotFound() {
		return value == NOT_FOUND;
	}

	public void setNotFound() {
		value = NOT_FOUND;
	}

	public boolean isZapped() {
		return value == ZAPPED;
	}

	public void setZapped() {
		value = ZAPPED;
	}

	public boolean isTypeMismatch() {
		return value == TYPE_MISMATCH;
	}

	public void setTypeMismatch() {
		value = TYPE_MISMATCH;
	}

	public boolean isHalrlomgLockTakeover() {
		return value == LOCK_TAKEOVER;
	}

	public void setHalrlomgLockTakeover() {
		value = LOCK_TAKEOVER;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SUCCESS_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
