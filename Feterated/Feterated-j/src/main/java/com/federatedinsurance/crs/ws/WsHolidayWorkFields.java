/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.federatedinsurance.crs.ws.enums.WsHolidayAdjustment;
import com.federatedinsurance.crs.ws.enums.WsHolidayFunc2;
import com.federatedinsurance.crs.ws.enums.WsHolidayTabResults;

/**Original name: WS-HOLIDAY-WORK-FIELDS<br>
 * Variable: WS-HOLIDAY-WORK-FIELDS from program XPIODAT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsHolidayWorkFields {

	//==== PROPERTIES ====
	//Original name: WS-HOLIDAY-FUNC-1
	private char wsHolidayFunc1 = DefaultValues.CHAR_VAL;
	//Original name: WS-HOLIDAY-FUNC-2
	private WsHolidayFunc2 wsHolidayFunc2 = new WsHolidayFunc2();
	//Original name: WS-HOLIDAY-DATE-RED
	private WsHolidayDateRed wsHolidayDateRed = new WsHolidayDateRed();
	//Original name: WS-HOLIDAY-TAB-RESULTS
	private WsHolidayTabResults wsHolidayTabResults = new WsHolidayTabResults();
	//Original name: WS-HOLIDAY-ADJUSTMENT
	private WsHolidayAdjustment wsHolidayAdjustment = new WsHolidayAdjustment();
	//Original name: WS-XCL-INDEX
	private int wsXclIndex = DefaultValues.BIN_INT_VAL;

	//==== METHODS ====
	public void setWsHolidayFuncFormatted(String data) {
		byte[] buffer = new byte[Len.WS_HOLIDAY_FUNC];
		MarshalByte.writeString(buffer, 1, data, Len.WS_HOLIDAY_FUNC);
		setWsHolidayFuncBytes(buffer, 1);
	}

	public void setWsHolidayFuncBytes(byte[] buffer, int offset) {
		int position = offset;
		wsHolidayFunc1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		wsHolidayFunc2.setWsHolidayFunc2(MarshalByte.readChar(buffer, position));
	}

	public void initWsHolidayFuncSpaces() {
		wsHolidayFunc1 = Types.SPACE_CHAR;
		wsHolidayFunc2.setWsHolidayFunc2(Types.SPACE_CHAR);
	}

	public void setWsHolidayFunc1(char wsHolidayFunc1) {
		this.wsHolidayFunc1 = wsHolidayFunc1;
	}

	public char getWsHolidayFunc1() {
		return this.wsHolidayFunc1;
	}

	public void setWsXclIndex(int wsXclIndex) {
		this.wsXclIndex = wsXclIndex;
	}

	public int getWsXclIndex() {
		return this.wsXclIndex;
	}

	public WsHolidayAdjustment getWsHolidayAdjustment() {
		return wsHolidayAdjustment;
	}

	public WsHolidayDateRed getWsHolidayDateRed() {
		return wsHolidayDateRed;
	}

	public WsHolidayFunc2 getWsHolidayFunc2() {
		return wsHolidayFunc2;
	}

	public WsHolidayTabResults getWsHolidayTabResults() {
		return wsHolidayTabResults;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_HOLIDAY_FUNC1 = 1;
		public static final int WS_HOLIDAY_FUNC = WS_HOLIDAY_FUNC1 + WsHolidayFunc2.Len.WS_HOLIDAY_FUNC2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
