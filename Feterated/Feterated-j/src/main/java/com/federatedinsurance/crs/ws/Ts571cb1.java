/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program TS571099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ts571cb1 extends SerializableParameter {

	//==== PROPERTIES ====
	/**Original name: UL-SERVICE-MNEMONIC<br>
	 * <pre> INPUT AND OUTPUT IS VIA THIS COPYBOOK.
	 * *************************************************************
	 *   COPYBOOK (TS571CB1)                                       *
	 * *************************************************************
	 *   COPYBOOK FOR COMMON URI LOOKUP PROGRAM (TS571099)         *
	 *                                                             *
	 *   INCLUDE THE FOLLOWING IN YOUR WORKING STORAGE SECTION:    *
	 *                                                             *
	 *       01  URI-LOOKUP-COMMAREA.                              *
	 *           COPY TS571CB1.                                    *
	 *                                                             *
	 *     MAINTENANCE HISTORY                                     *
	 *     *******************                                     *
	 *                                                             *
	 *     INFO    CHANGE                                          *
	 *     NMBR     DATE    III  DESCRIPTION                       *
	 *     *****  ********  ***  ********************************  *
	 *     TS087  03/10/05  LJL  CREATED NEW COPYBOOK              *
	 * *************************************************************
	 *  UL = URI LOOKUP</pre>*/
	private String serviceMnemonic = DefaultValues.stringVal(Len.SERVICE_MNEMONIC);
	//Original name: UL-URI
	private String uri = DefaultValues.stringVal(Len.URI);
	//Original name: UL-CICS-SYSID
	private String cicsSysid = DefaultValues.stringVal(Len.CICS_SYSID);
	//Original name: UL-SERVICE-PREFIX
	private String servicePrefix = DefaultValues.stringVal(Len.SERVICE_PREFIX);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.TS571CB1;
	}

	@Override
	public void deserialize(byte[] buf) {
		setTs571cb1Bytes(buf);
	}

	public void setTs571cb1Bytes(byte[] buffer) {
		setTs571cb1Bytes(buffer, 1);
	}

	public byte[] getTs571cb1Bytes() {
		byte[] buffer = new byte[Len.TS571CB1];
		return getTs571cb1Bytes(buffer, 1);
	}

	public void setTs571cb1Bytes(byte[] buffer, int offset) {
		int position = offset;
		serviceMnemonic = MarshalByte.readString(buffer, position, Len.SERVICE_MNEMONIC);
		position += Len.SERVICE_MNEMONIC;
		uri = MarshalByte.readString(buffer, position, Len.URI);
		position += Len.URI;
		cicsSysid = MarshalByte.readString(buffer, position, Len.CICS_SYSID);
		position += Len.CICS_SYSID;
		servicePrefix = MarshalByte.readString(buffer, position, Len.SERVICE_PREFIX);
	}

	public byte[] getTs571cb1Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, serviceMnemonic, Len.SERVICE_MNEMONIC);
		position += Len.SERVICE_MNEMONIC;
		MarshalByte.writeString(buffer, position, uri, Len.URI);
		position += Len.URI;
		MarshalByte.writeString(buffer, position, cicsSysid, Len.CICS_SYSID);
		position += Len.CICS_SYSID;
		MarshalByte.writeString(buffer, position, servicePrefix, Len.SERVICE_PREFIX);
		return buffer;
	}

	public void setServiceMnemonic(String serviceMnemonic) {
		this.serviceMnemonic = Functions.subString(serviceMnemonic, Len.SERVICE_MNEMONIC);
	}

	public String getServiceMnemonic() {
		return this.serviceMnemonic;
	}

	public void setUri(String uri) {
		this.uri = Functions.subString(uri, Len.URI);
	}

	public String getUri() {
		return this.uri;
	}

	public void setCicsSysid(String cicsSysid) {
		this.cicsSysid = Functions.subString(cicsSysid, Len.CICS_SYSID);
	}

	public String getCicsSysid() {
		return this.cicsSysid;
	}

	public String getCicsSysidFormatted() {
		return Functions.padBlanks(getCicsSysid(), Len.CICS_SYSID);
	}

	public void setServicePrefix(String servicePrefix) {
		this.servicePrefix = Functions.subString(servicePrefix, Len.SERVICE_PREFIX);
	}

	public String getServicePrefix() {
		return this.servicePrefix;
	}

	@Override
	public byte[] serialize() {
		return getTs571cb1Bytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SERVICE_MNEMONIC = 12;
		public static final int URI = 256;
		public static final int CICS_SYSID = 4;
		public static final int SERVICE_PREFIX = 4;
		public static final int TS571CB1 = SERVICE_MNEMONIC + URI + CICS_SYSID + SERVICE_PREFIX;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
