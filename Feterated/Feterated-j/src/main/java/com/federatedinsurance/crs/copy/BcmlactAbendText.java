/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: BCMLACT-ABEND-TEXT<br>
 * Variable: BCMLACT-ABEND-TEXT from copybook BXCMLACT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BcmlactAbendText {

	//==== PROPERTIES ====
	//Original name: BCMLACT-PROG-NAME
	private String progName = DefaultValues.stringVal(Len.PROG_NAME);
	//Original name: BCMLACT-PARA-NAME
	private String paraName = DefaultValues.stringVal(Len.PARA_NAME);
	/**Original name: BCMLACT-SQL-CD<br>
	 * <pre>          10 BCMLACT-SQL-CD          PIC 9(08).</pre>*/
	private int sqlCd = DefaultValues.INT_VAL;
	//Original name: BCMLACT-ERR-DESC
	private String errDesc = DefaultValues.stringVal(Len.ERR_DESC);
	//Original name: BCMLACT-ERR-KEY-ID
	private String errKeyId = DefaultValues.stringVal(Len.ERR_KEY_ID);

	//==== METHODS ====
	public void setAbendTextBytes(byte[] buffer, int offset) {
		int position = offset;
		progName = MarshalByte.readString(buffer, position, Len.PROG_NAME);
		position += Len.PROG_NAME;
		paraName = MarshalByte.readString(buffer, position, Len.PARA_NAME);
		position += Len.PARA_NAME;
		sqlCd = MarshalByte.readInt(buffer, position, Len.SQL_CD);
		position += Len.SQL_CD;
		errDesc = MarshalByte.readString(buffer, position, Len.ERR_DESC);
		position += Len.ERR_DESC;
		errKeyId = MarshalByte.readString(buffer, position, Len.ERR_KEY_ID);
	}

	public byte[] getAbendTextBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, progName, Len.PROG_NAME);
		position += Len.PROG_NAME;
		MarshalByte.writeString(buffer, position, paraName, Len.PARA_NAME);
		position += Len.PARA_NAME;
		MarshalByte.writeInt(buffer, position, sqlCd, Len.SQL_CD);
		position += Len.SQL_CD;
		MarshalByte.writeString(buffer, position, errDesc, Len.ERR_DESC);
		position += Len.ERR_DESC;
		MarshalByte.writeString(buffer, position, errKeyId, Len.ERR_KEY_ID);
		return buffer;
	}

	public void setProgName(String progName) {
		this.progName = Functions.subString(progName, Len.PROG_NAME);
	}

	public String getProgName() {
		return this.progName;
	}

	public void setParaName(String paraName) {
		this.paraName = Functions.subString(paraName, Len.PARA_NAME);
	}

	public String getParaName() {
		return this.paraName;
	}

	public String getParaNameFormatted() {
		return Functions.padBlanks(getParaName(), Len.PARA_NAME);
	}

	public void setSqlCd(int sqlCd) {
		this.sqlCd = sqlCd;
	}

	public int getSqlCd() {
		return this.sqlCd;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = Functions.subString(errDesc, Len.ERR_DESC);
	}

	public String getErrDesc() {
		return this.errDesc;
	}

	public String getErrDescFormatted() {
		return Functions.padBlanks(getErrDesc(), Len.ERR_DESC);
	}

	public void setErrKeyId(String errKeyId) {
		this.errKeyId = Functions.subString(errKeyId, Len.ERR_KEY_ID);
	}

	public String getErrKeyId() {
		return this.errKeyId;
	}

	public String getErrKeyIdFormatted() {
		return Functions.padBlanks(getErrKeyId(), Len.ERR_KEY_ID);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROG_NAME = 8;
		public static final int PARA_NAME = 30;
		public static final int SQL_CD = 8;
		public static final int ERR_DESC = 70;
		public static final int ERR_KEY_ID = 30;
		public static final int ABEND_TEXT = PROG_NAME + PARA_NAME + SQL_CD + ERR_DESC + ERR_KEY_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
