/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program HALOETRA<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaHaloetra extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int LNK_CA_DATA_MAXOCCURS = 32767;
	private int lnkCaDataListenerSize = LNK_CA_DATA_MAXOCCURS;
	private IValueChangeListener lnkCaDataListener = new LnkCaDataListener();
	//Original name: LNK-CA-DATA
	private char[] lnkCaData = new char[LNK_CA_DATA_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public DfhcommareaHaloetra() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return getDfhcommareaSize();
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void init() {
		for (int lnkCaDataIdx = 1; lnkCaDataIdx <= LNK_CA_DATA_MAXOCCURS; lnkCaDataIdx++) {
			setLnkCaData(lnkCaDataIdx, DefaultValues.CHAR_VAL);
		}
	}

	public int getDfhcommareaSize() {
		return Types.CHAR_SIZE * lnkCaDataListenerSize;
	}

	public String getDfhcommareaFormatted() {
		return MarshalByteExt.bufferToStr(getDfhcommareaBytes());
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[getDfhcommareaSize()];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= lnkCaDataListenerSize; idx++) {
			if (position <= buffer.length) {
				setLnkCaData(idx, MarshalByte.readChar(buffer, position));
				position += Types.CHAR_SIZE;
			} else {
				setLnkCaData(idx, Types.SPACE_CHAR);
				position += Types.CHAR_SIZE;
			}
		}
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= lnkCaDataListenerSize; idx++) {
			MarshalByte.writeChar(buffer, position, getLnkCaData(idx));
			position += Types.CHAR_SIZE;
		}
		return buffer;
	}

	public void initDfhcommareaSpaces() {
		for (int idx = 1; idx <= lnkCaDataListenerSize; idx++) {
			lnkCaData[idx - 1] = (Types.SPACE_CHAR);
		}
	}

	public void setLnkCaData(int lnkCaDataIdx, char lnkCaData) {
		this.lnkCaData[lnkCaDataIdx - 1] = lnkCaData;
	}

	public char getLnkCaData(int lnkCaDataIdx) {
		return this.lnkCaData[lnkCaDataIdx - 1];
	}

	public IValueChangeListener getLnkCaDataListener() {
		return lnkCaDataListener;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	/**Original name: LNK-CA-DATA<br>*/
	public class LnkCaDataListener implements IValueChangeListener {

		//==== METHODS ====
		@Override
		public void change() {
			lnkCaDataListenerSize = LNK_CA_DATA_MAXOCCURS;
		}

		@Override
		public void change(int value) {
			lnkCaDataListenerSize = value < 1 ? 0 : (value > LNK_CA_DATA_MAXOCCURS ? LNK_CA_DATA_MAXOCCURS : value);
		}
	}
}
