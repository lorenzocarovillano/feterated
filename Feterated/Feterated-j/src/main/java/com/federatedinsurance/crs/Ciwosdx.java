/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.inspect.InspectCharactersPattern;
import com.bphx.ctu.af.util.inspect.InspectMatcher;
import com.bphx.ctu.af.util.inspect.InspectPattern;
import com.federatedinsurance.crs.ws.CiwosdxData;
import com.federatedinsurance.crs.ws.LiErrContextText;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;

/**Original name: CIWOSDX<br>
 * <pre>AUTHOR.     PMSC.
 *     DATE-WRITTEN. SEPT 1994.
 * REMARKS.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - COBOL SOUNDEX ROUTINE                       **
 * *                                                             **
 * * PLATFORM - HOST                                             **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - IBM VS COBOL II                                  **
 * *                                                             **
 * * PURPOSE -  TO CREATE PHONETIC CODE USING LAST NAME          **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED BY ONE OF THE **
 * *                       FOLLOWING METHODS:                    **
 * *                                                             **
 * *                       1. IT IS DYNAMICALLY CALLED BY THE    **
 * *                          CLIENT API CIWOF08.                **
 * *                                                             **
 * * DATA ACCESS METHODS -                                       **
 * *                                                             **
 * ****************************************************************
 *     EJECT
 * ****************************************************************
 *              M A I N T E N A N C E   L O G                     *
 *                                                                *
 *    SI #    DATE    EMP ID              DESCRIPTION             *
 *   ------ --------  ------    --------------------------------- *
 *          10/03/94  5833      SOURCE CODE CREATED.              *
 *                                                                *
 *   042824 09/29/98  5833      REMOVED UNNEEDED COPYBOOKS.       *
 *   11397  10/27/16  E404DNF   RECOMPILED FOR STORCOPY WORK      *
 *                                                                *
 * ****************************************************************
 * ****************************************************************
 *                                                                *
 *      PROGRAM TO CREATE PHONETIC CODE                           *
 *                                                                *
 *   FORMAT OF GENERATED CODE WILL BE THE FOLLOWING:              *
 *                                                                *
 *   FIRST CHARACTER WILL BE THE FIRST CHARACTRR OF LAST NAME.    *
 *   EACH SUBSEQUENT CHARACTER IN THE STRING WILL BE TRANSLATED   *
 *   ACCORDING TO THE FOLLOWING TABLE:                            *
 *     A, E, I, O, U, Y, H, W  =>  ' '                            *
 *     B, F, P, V              =>  '1'                            *
 *     C, G, J, K, Q, S, X, Z  =>  '2'                            *
 *     D, T                    =>  '3'                            *
 *     L                       =>  '4'                            *
 *     M, N                    =>  '5'                            *
 *     R                       =>  '6'                            *
 *                                                                *
 *      IF A CHARACTER TRANSLATES TO THE SAME VALUE AS THE PRE-   *
 *   VIOUS CHARACTER, IT WILL NOT BE ADDED TO THE PHONETIC CODE.  *
 *      IF A CHARACTER TRANSLATES TO A SPACE, IT WILL NOT BE      *
 *   ADDED TO THE CODE.                                           *
 *      THE PHONETIC CODE WILL BE EXACTLY FOUR BYTES. IF AFTER    *
 *   THE PRIOR STEPS THE CODE IS TOO SHORT, PAD IT OUT WITH ZEROS.*
 *   IF TOO LONG, TRUNCATE TO FOUR BYTES.                         *
 * ****************************************************************
 * COPY XPXCABND.</pre>*/
public class Ciwosdx extends Program {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private CiwosdxData ws = new CiwosdxData();
	//Original name: DFHCOMMAREA
	private LiErrContextText dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LiErrContextText dfhcommarea) {
		this.dfhcommarea = dfhcommarea;
		main1();
		exitModule();
		return 0;
	}

	public static Ciwosdx getInstance() {
		return (Programs.getInstance(Ciwosdx.class));
	}

	/**Original name: 0100-MAIN<br>
	 * <pre>**********************************************************
	 *   THE 0100-MAIN PARAGRAPH IS RESPONSIBLE FOR CONTROLLING *
	 *   THE PROCESSING OF THE FUNCTION PASSED TO IT.           *
	 * **********************************************************</pre>*/
	private void main1() {
		// COB_CODE: MOVE DFHCOMMAREA TO WS-SDX-PASSED-PARMS.
		ws.getWsSdxPassedParms().setWsSdxPassedParmsFormatted(dfhcommarea.getLiErrContextTextFormatted());
		//                                                         *
		//**SET PROGRAM NAME IN USER AREA FOR DEBUGGING*************
		//                                                         *
		// COB_CODE: MOVE 'CIWOSDX' TO WS-PGM-ID.
		ws.getWsSdxPassedParms().setPgmId("CIWOSDX");
		//
		// COB_CODE: PERFORM 0200-GENERATE-SOUNDEX.
		generateSoundex();
	}

	/**Original name: EXIT-MODULE<br>
	 * <pre>**********************************************************
	 *     RESET THE COMMUNICATION AREA AND RETURN TO THE       *
	 *     PROGRAM THAT CALLED THIS ROUTINE.                    *
	 * **********************************************************</pre>*/
	private void exitModule() {
		// COB_CODE: IF SOUNDEX-COMPLETE
		//              MOVE 0 TO WS-SDX-RET-CD
		//           ELSE
		//              MOVE '93' TO WS-SDX-RET-CD.
		if (ws.getWsSoundexSw().isComplete()) {
			// COB_CODE: MOVE 0 TO WS-SDX-RET-CD
			ws.getWsSdxPassedParms().setSdxRetCd("0");
		} else {
			// COB_CODE: MOVE '93' TO WS-SDX-RET-CD.
			ws.getWsSdxPassedParms().setSdxRetCd("93");
		}
		//
		// COB_CODE: MOVE WS-SDX-PASSED-PARMS TO DFHCOMMAREA.
		dfhcommarea.setLiErrContextText(ws.getWsSdxPassedParms().getWsSdxPassedParmsFormatted());
		//
		//    COPY XPXCDONE.
		//
		// GOBACK REQUIRED BY COMPILER
		//
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 0200-GENERATE-SOUNDEX<br>*/
	private void generateSoundex() {
		// COB_CODE: MOVE 0 TO WS-SDX-CTR.
		ws.setWsSdxCtr(((short) 0));
		// COB_CODE: MOVE 1 TO WS-TMP-CTR.
		ws.setWsTmpCtr(((short) 1));
		// COB_CODE: MOVE ' ' TO WS-PREV-CHAR.
		ws.setWsPrevCharFormatted(" ");
		// COB_CODE: MOVE 'N' TO WS-SOUNDEX-SW WS-FIRST-CHAR-SW.
		ws.getWsSoundexSw().setWsSoundexSwFormatted("N");
		ws.getWsFirstCharSw().setWsFirstCharSwFormatted("N");
		// COB_CODE: MOVE WS-LAST-NAME TO WS-TMP-BUFFER.
		ws.setWsTmpBufferFormatted(ws.getWsSdxPassedParms().getLastNameFormatted());
		// COB_CODE: SET SDX-INDX BUF-INDX TO 1.
		ws.setSdxIndx(1);
		ws.setBufIndx(1);
		//
		// COB_CODE: PERFORM 0250-INSPECT-NAME THRU
		//                   0250-INSPECT-NAME-X VARYING WS-TMP-CTR FROM 1 BY 1
		//              UNTIL WS-TMP-CTR GREATER THAN LENGTH OF WS-TMP-BUFFER
		//              OR SOUNDEX-COMPLETE.
		ws.setWsTmpCtr(((short) 1));
		while (!(ws.getWsTmpCtr() > CiwosdxData.Len.WS_TMP_BUFFER || ws.getWsSoundexSw().isComplete())) {
			inspectName();
			ws.setWsTmpCtr(Trunc.toShort(ws.getWsTmpCtr() + 1, 4));
		}
		//
		// COB_CODE: IF SOUNDEX-NOT-COMPLETE
		//              END-PERFORM.
		if (ws.getWsSoundexSw().isNotComplete()) {
			// COB_CODE: PERFORM UNTIL SOUNDEX-COMPLETE
			//              END-IF
			//           END-PERFORM.
			while (!ws.getWsSoundexSw().isComplete()) {
				// COB_CODE: MOVE 0 TO WS-SNDX-CHAR (SDX-INDX)
				ws.getWsSdxPassedParms().getSoundex(ws.getSdxIndx()).setWsSndxCharFormatted("0");
				// COB_CODE: SET SDX-INDX UP BY 1
				ws.setSdxIndx(Trunc.toInt(ws.getSdxIndx() + 1, 9));
				// COB_CODE: ADD 1 TO WS-SDX-CTR
				ws.setWsSdxCtr(Trunc.toShort(1 + ws.getWsSdxCtr(), 4));
				// COB_CODE: IF WS-SDX-CTR = 4
				//              SET SOUNDEX-COMPLETE TO TRUE
				//           END-IF
				if (ws.getWsSdxCtr() == 4) {
					// COB_CODE: SET SOUNDEX-COMPLETE TO TRUE
					ws.getWsSoundexSw().setComplete();
				}
			}
		}
	}

	/**Original name: 0250-INSPECT-NAME<br>*/
	private void inspectName() {
		InspectPattern iPattern = null;
		InspectMatcher iMatcher = null;
		// COB_CODE: MOVE ZERO TO WS-TALLY.
		ws.setWsTally(0);
		// COB_CODE: MOVE WS-BUF-CHAR (BUF-INDX) TO WS-ONE-CHAR.
		ws.setWsOneChar(ws.getWsTmpBuff(ws.getBufIndx()).getWsBufChar());
		//
		// COB_CODE: IF FIRST-CHAR-NOT-LOADED
		//                 GO TO 0250-INSPECT-NAME-X.
		if (ws.getWsFirstCharSw().isNotLoaded()) {
			// COB_CODE: IF WS-ONE-CHAR > SPACES
			//              GO TO 0250-INSPECT-NAME-X.
			if (Conditions.gt(ws.getWsOneChar(), Types.SPACE_CHAR)) {
				// COB_CODE: MOVE WS-BUF-CHAR (BUF-INDX) TO WS-SNDX-CHAR (SDX-INDX)
				ws.getWsSdxPassedParms().getSoundex(ws.getSdxIndx()).setWsSndxChar(ws.getWsTmpBuff(ws.getBufIndx()).getWsBufChar());
				// COB_CODE: SET FIRST-CHAR-LOADED TO TRUE
				ws.getWsFirstCharSw().setLoaded();
				// COB_CODE: SET BUF-INDX SDX-INDX UP BY 1
				ws.setBufIndx(Trunc.toInt(ws.getBufIndx() + 1, 9));
				ws.setSdxIndx(Trunc.toInt(ws.getSdxIndx() + 1, 9));
				// COB_CODE: ADD 1 TO WS-SDX-CTR
				ws.setWsSdxCtr(Trunc.toShort(1 + ws.getWsSdxCtr(), 4));
				// COB_CODE: GO TO 0250-INSPECT-NAME-X.
				return;
			}
		}
		//
		// COB_CODE: INSPECT WS-ALPHABET TALLYING WS-TALLY
		//              FOR CHARACTERS BEFORE WS-ONE-CHAR.
		iPattern = new InspectCharactersPattern().before(String.valueOf(ws.getWsOneChar()));
		iMatcher = new InspectMatcher(ws.getWsAlphabetFormatted(), iPattern);
		ws.setWsTally(Trunc.toInt(ws.getWsTally() + iMatcher.count(), 2));
		//
		//**   TRANSLATE VALUE WILL BE TALLY + 1 ***
		// COB_CODE: IF WS-TALLY < 26
		//                    ADD 1 TO WS-SDX-CTR.
		if (ws.getWsTally() < 26) {
			// COB_CODE: IF WS-TRANSLATE-CHAR (WS-TALLY + 1) > SPACE
			//                 ADD 1 TO WS-SDX-CTR.
			if (Conditions.gt(ws.getWsTranslateValues().getCharFld(ws.getWsTally() + 1), Types.SPACE_CHAR)) {
				// COB_CODE: IF WS-TRANSLATE-CHAR (WS-TALLY + 1) NOT = WS-PREV-CHAR
				//              ADD 1 TO WS-SDX-CTR.
				if (ws.getWsTranslateValues().getCharFld(ws.getWsTally() + 1) != ws.getWsPrevChar()) {
					// COB_CODE: MOVE WS-TRANSLATE-CHAR (WS-TALLY + 1)
					//              TO WS-SNDX-CHAR (SDX-INDX)
					ws.getWsSdxPassedParms().getSoundex(ws.getSdxIndx()).setWsSndxChar(ws.getWsTranslateValues().getCharFld(ws.getWsTally() + 1));
					// COB_CODE: MOVE WS-TRANSLATE-CHAR (WS-TALLY + 1)
					//              TO WS-PREV-CHAR
					ws.setWsPrevChar(ws.getWsTranslateValues().getCharFld(ws.getWsTally() + 1));
					// COB_CODE: SET SDX-INDX UP BY 1
					ws.setSdxIndx(Trunc.toInt(ws.getSdxIndx() + 1, 9));
					// COB_CODE: ADD 1 TO WS-SDX-CTR.
					ws.setWsSdxCtr(Trunc.toShort(1 + ws.getWsSdxCtr(), 4));
				}
			}
		}
		//
		// COB_CODE: SET BUF-INDX UP BY 1.
		ws.setBufIndx(Trunc.toInt(ws.getBufIndx() + 1, 9));
		//
		// COB_CODE: IF WS-SDX-CTR = 4
		//              SET SOUNDEX-COMPLETE TO TRUE.
		if (ws.getWsSdxCtr() == 4) {
			// COB_CODE: SET SOUNDEX-COMPLETE TO TRUE.
			ws.getWsSoundexSw().setComplete();
		}
	}
}
