/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UBOC-APPLY-AUDITS-SW<br>
 * Variable: UBOC-APPLY-AUDITS-SW from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocApplyAuditsSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char APPLY_AUDITS = 'Y';
	public static final char NO_AUDITS = 'N';

	//==== METHODS ====
	public void setApplyAuditsSw(char applyAuditsSw) {
		this.value = applyAuditsSw;
	}

	public char getApplyAuditsSw() {
		return this.value;
	}

	public boolean isUbocApplyAudits() {
		return value == APPLY_AUDITS;
	}

	public void setUbocApplyAudits() {
		value = APPLY_AUDITS;
	}

	public void setUbocNoAudits() {
		value = NO_AUDITS;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int APPLY_AUDITS_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
