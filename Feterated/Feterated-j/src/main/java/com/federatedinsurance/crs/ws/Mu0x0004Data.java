/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program MU0X0004<br>
 * Generated as a class for rule WS.<br>*/
public class Mu0x0004Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsMu0x0004 constantFields = new ConstantFieldsMu0x0004();
	//Original name: SAVE-AREA
	private SaveAreaMu0x0004 saveArea = new SaveAreaMu0x0004();
	//Original name: SUBSCRIPTS
	private SubscriptsMu0x0004 subscripts = new SubscriptsMu0x0004();
	//Original name: SWITCHES
	private SwitchesMu0x0004 switches = new SwitchesMu0x0004();
	//Original name: WS-MISC-WORK-FLDS
	private WsMiscWorkFldsMu0x0004 wsMiscWorkFlds = new WsMiscWorkFldsMu0x0004();
	//Original name: MAIN-DRIVER-DATA
	private DfhcommareaTs020000 mainDriverData = new DfhcommareaTs020000();

	//==== METHODS ====
	public ConstantFieldsMu0x0004 getConstantFields() {
		return constantFields;
	}

	public DfhcommareaTs020000 getMainDriverData() {
		return mainDriverData;
	}

	public SaveAreaMu0x0004 getSaveArea() {
		return saveArea;
	}

	public SubscriptsMu0x0004 getSubscripts() {
		return subscripts;
	}

	public SwitchesMu0x0004 getSwitches() {
		return switches;
	}

	public WsMiscWorkFldsMu0x0004 getWsMiscWorkFlds() {
		return wsMiscWorkFlds;
	}
}
