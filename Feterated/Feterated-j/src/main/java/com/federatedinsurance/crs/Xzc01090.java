/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.federatedinsurance.crs.commons.data.dao.ActNotPolTypDtaExtDao;
import com.federatedinsurance.crs.commons.data.dao.PolDtaExtDao;
import com.federatedinsurance.crs.copy.SqlcaTs030199;
import com.federatedinsurance.crs.ws.DfhcommareaXzc01090;
import com.federatedinsurance.crs.ws.LSearchCriteria;
import com.federatedinsurance.crs.ws.Xzc01090Data;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZC01090<br>
 * <pre>AUTHOR.       Kristi Schettler.
 * DATE-WRITTEN. 15 MAR 2010.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - POLICY PENDING CANCELLATION STATUS CICS     **
 * *                 PROGRAM                                     **
 * *                 SERVICE NAME:XZGetPolPndCncStatus           **
 * *                                                             **
 * * PURPOSE -  FIND IF A POLICY TERM IS SET TO BE TERMINATED.   **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.        **
 * *                                                             **
 * ****************************************************************
 * *                                                             **
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * --------- ---------- --------   ----------------------------**
 * *TO07602-22 03-15-2010 E404KXS    NEW                         **
 * *TO07602-40 10-25-2010 E404KXS    GET MOST EFFECTIVE STATUS   **
 * *                                 IF NO DATE IS PASSED IN.    **
 * *                                                             **
 * ****************************************************************</pre>*/
public class Xzc01090 extends Program {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private SqlcaTs030199 sqlca = new SqlcaTs030199();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ExecContext execContext = null;
	private PolDtaExtDao polDtaExtDao = new PolDtaExtDao(dbAccessStatus);
	private ActNotPolTypDtaExtDao actNotPolTypDtaExtDao = new ActNotPolTypDtaExtDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xzc01090Data ws = new Xzc01090Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaXzc01090 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaXzc01090 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		programExit();
		return 0;
	}

	public static Xzc01090 getInstance() {
		return (Programs.getInstance(Xzc01090.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: PERFORM 3000-GET-POL-PND-CNC-STA
		//              THRU 3000-EXIT.
		getPolPndCncSta();
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  INITIALIZE OUTPUT AND VERIFY INPUT.                          *
	 * ***************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE XZC010-SERVICE-OUTPUTS
		//                      XZC010-ERROR-HANDLING-PARMS.
		initXzc010ServiceOutputs();
		initXzc010ErrorHandlingParms();
		// COB_CODE: SET XZC010-NO-ERROR-CODE    TO TRUE.
		dfhcommarea.getXzc010ErrorReturnCode().setNoErrorCode();
		// COB_CODE: MOVE +1                     TO SS-NLBE-IDX.
		ws.setSsNlbeIdx(((short) 1));
		// COB_CODE: PERFORM 2100-VERIFY-INPUT
		//              THRU 2100-EXIT.
		verifyInput();
	}

	/**Original name: 2100-VERIFY-INPUT<br>
	 * <pre>**********************************************************
	 *   VERIFY POLICY EXISTS AND COLLECT ANY MISSING INFO      *
	 * **********************************************************</pre>*/
	private void verifyInput() {
		// COB_CODE: INITIALIZE DCLPOL-DTA-EXT.
		initDclpolDtaExt();
		// BOTH EFFECTIVE AND EXPIRATION DATE PASSED IN
		// COB_CODE: IF XZC01I-POL-EFF-DT NOT = SPACES
		//             AND
		//              XZC01I-POL-EXP-DT NOT = SPACES
		//               GO TO 2100-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(dfhcommarea.getiPolEffDt()) && !Characters.EQ_SPACE.test(dfhcommarea.getiPolExpDt())) {
			// COB_CODE: PERFORM 2110-VERIFY-POLICY-TERM
			//              THRU 2110-EXIT
			verifyPolicyTerm();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// ONLY EFFECTIVE DATE PASSED IN
		// COB_CODE: IF XZC01I-POL-EFF-DT NOT = SPACES
		//               GO TO 2100-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(dfhcommarea.getiPolEffDt())) {
			// COB_CODE: PERFORM 2120-FIND-POL-EXP-DT
			//              THRU 2120-EXIT
			findPolExpDt();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// ONLY EXPIRATION DATE PASSED IN
		// COB_CODE: IF XZC01I-POL-EXP-DT NOT = SPACES
		//               GO TO 2100-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(dfhcommarea.getiPolExpDt())) {
			// COB_CODE: PERFORM 2130-FIND-POL-EFF-DT
			//              THRU 2130-EXIT
			findPolEffDt();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// NO DATES PASSED IN - GET MOST EFFECTIVE TERM
		// COB_CODE: PERFORM 2140-FIND-POL-DTS
		//              THRU 2140-EXIT.
		findPolDts();
	}

	/**Original name: 2110-VERIFY-POLICY-TERM<br>
	 * <pre>**********************************************************
	 *   VERIFY POLICY TERM DATES                               *
	 * **********************************************************</pre>*/
	private void verifyPolicyTerm() {
		// COB_CODE: MOVE XZC01I-POL-NBR         TO POL-NBR
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC01I-POL-EFF-DT      TO POL-EFF-DT
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: MOVE XZC01I-POL-EXP-DT      TO PLN-EXP-DT
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPlnExpDt(dfhcommarea.getiPolExpDt());
		// COB_CODE: EXEC SQL
		//               SELECT POL_ID
		//                    , ACT_NBR
		//                 INTO :DCLPOL-DTA-EXT.POL-ID
		//                    , :DCLPOL-DTA-EXT.ACT-NBR
		//                 FROM POL_DTA_EXT
		//                WHERE POL_NBR = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND POL_EFF_DT = :DCLPOL-DTA-EXT.POL-EFF-DT
		//                  AND PLN_EXP_DT = :DCLPOL-DTA-EXT.PLN-EXP-DT
		//           END-EXEC.
		polDtaExtDao.selectRec3(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt().getPolEffDt(), ws.getDclpolDtaExt().getPlnExpDt(),
				ws.getDclpolDtaExt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                   CONTINUE
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                      THRU 9920-EXIT
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: CONTINUE
			//continue
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE CF-PN-2110     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01InvalidInput().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2110());
			// COB_CODE: PERFORM 9920-INVALID-INPUT-NLBE
			//              THRU 9920-EXIT
			invalidInputNlbe();
		} else {
			// COB_CODE: MOVE CF-PN-2110     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2110());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 2120-FIND-POL-EXP-DT<br>
	 * <pre>**********************************************************
	 *   GET POLICY EXPIRATION DATE                             *
	 * **********************************************************</pre>*/
	private void findPolExpDt() {
		// COB_CODE: MOVE XZC01I-POL-NBR         TO POL-NBR
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC01I-POL-EFF-DT      TO POL-EFF-DT
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: EXEC SQL
		//               SELECT PLN_EXP_DT
		//                    , ACT_NBR
		//                 INTO :DCLPOL-DTA-EXT.PLN-EXP-DT
		//                    , :DCLPOL-DTA-EXT.ACT-NBR
		//                 FROM POL_DTA_EXT
		//                WHERE POL_NBR = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND POL_EFF_DT = :DCLPOL-DTA-EXT.POL-EFF-DT
		//           END-EXEC.
		polDtaExtDao.selectRec4(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt().getPolEffDt(), ws.getDclpolDtaExt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                                       TO XZC01I-POL-EXP-DT
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                      THRU 9920-EXIT
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: MOVE PLN-EXP-DT     OF DCLPOL-DTA-EXT
			//                               TO XZC01I-POL-EXP-DT
			dfhcommarea.setiPolExpDt(ws.getDclpolDtaExt().getPlnExpDt());
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE CF-PN-2120     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01InvalidInput().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2120());
			// COB_CODE: PERFORM 9920-INVALID-INPUT-NLBE
			//              THRU 9920-EXIT
			invalidInputNlbe();
		} else {
			// COB_CODE: MOVE CF-PN-2120     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2120());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 2130-FIND-POL-EFF-DT<br>
	 * <pre>**********************************************************
	 *   GET POLICY EFFECTIVE DATE                              *
	 * **********************************************************</pre>*/
	private void findPolEffDt() {
		// COB_CODE: MOVE XZC01I-POL-NBR         TO POL-NBR
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC01I-POL-EXP-DT      TO PLN-EXP-DT
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPlnExpDt(dfhcommarea.getiPolExpDt());
		// COB_CODE: EXEC SQL
		//               SELECT POL_EFF_DT
		//                    , ACT_NBR
		//                 INTO :DCLPOL-DTA-EXT.POL-EFF-DT
		//                    , :DCLPOL-DTA-EXT.ACT-NBR
		//                 FROM POL_DTA_EXT
		//                WHERE POL_NBR = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND PLN_EXP_DT = :DCLPOL-DTA-EXT.PLN-EXP-DT
		//           END-EXEC.
		polDtaExtDao.selectRec5(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt().getPlnExpDt(), ws.getDclpolDtaExt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                                       TO XZC01I-POL-EFF-DT
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                      THRU 9920-EXIT
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: MOVE POL-EFF-DT     OF DCLPOL-DTA-EXT
			//                               TO XZC01I-POL-EFF-DT
			dfhcommarea.setiPolEffDt(ws.getDclpolDtaExt().getPolEffDt());
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE CF-PN-2130     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01InvalidInput().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2130());
			// COB_CODE: PERFORM 9920-INVALID-INPUT-NLBE
			//              THRU 9920-EXIT
			invalidInputNlbe();
		} else {
			// COB_CODE: MOVE CF-PN-2130     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2130());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 2140-FIND-POL-DTS<br>
	 * <pre> T60240 START
	 * **********************************************************
	 *   GET THE MOST EFFECTIVE POLICY TERM DATES
	 * **********************************************************</pre>*/
	private void findPolDts() {
		// COB_CODE: MOVE XZC01I-POL-NBR         TO POL-NBR
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: EXEC SQL
		//               SELECT PD.POL_EFF_DT
		//                    , PD.PLN_EXP_DT
		//                    , PD.ACT_NBR
		//                 INTO :DCLPOL-DTA-EXT.POL-EFF-DT
		//                    , :DCLPOL-DTA-EXT.PLN-EXP-DT
		//                    , :DCLPOL-DTA-EXT.ACT-NBR
		//                 FROM POL_DTA_EXT PD
		//                WHERE PD.POL_NBR = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND PD.PLN_EXP_DT = (
		//                          SELECT MAX(PD2.PLN_EXP_DT)
		//                            FROM POL_DTA_EXT PD2
		//                           WHERE PD2.POL_NBR =
		//                                       :DCLPOL-DTA-EXT.POL-NBR)
		//           END-EXEC.
		polDtaExtDao.selectByR(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                                       TO XZC01I-POL-EXP-DT
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                      THRU 9920-EXIT
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: MOVE POL-EFF-DT     OF DCLPOL-DTA-EXT
			//                               TO XZC01I-POL-EFF-DT
			dfhcommarea.setiPolEffDt(ws.getDclpolDtaExt().getPolEffDt());
			// COB_CODE: MOVE PLN-EXP-DT     OF DCLPOL-DTA-EXT
			//                               TO XZC01I-POL-EXP-DT
			dfhcommarea.setiPolExpDt(ws.getDclpolDtaExt().getPlnExpDt());
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE CF-PN-2140     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01InvalidInput().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2140());
			// COB_CODE: PERFORM 9920-INVALID-INPUT-NLBE
			//              THRU 9920-EXIT
			invalidInputNlbe();
		} else {
			// COB_CODE: MOVE CF-PN-2140     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2140());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 3000-GET-POL-PND-CNC-STA<br>
	 * <pre> T60240 END
	 * **********************************************************
	 *  DETERMINE THE POLICY PENDING CANCELLATION STATUS.       *
	 * **********************************************************</pre>*/
	private void getPolPndCncSta() {
		// COB_CODE: PERFORM 3100-CHECK-POL-PND-CNC-STA
		//              THRU 3100-EXIT.
		checkPolPndCncSta();
		// COB_CODE: PERFORM 3200-SET-OUTPUT
		//              THRU 3200-EXIT.
		setOutput();
	}

	/**Original name: 3100-CHECK-POL-PND-CNC-STA<br>
	 * <pre>**********************************************************
	 *   CHECK TO SEE IF THE MOST RECENT NOTIFICATION WAS A     *
	 *   CANCELLATION AND THAT THE CANCELLATION IS STILL        *
	 *   PENDING. IF SO, SET THE PENDING CANCELLATION           *
	 *   INDICATOR TO 'Y'.                                      *
	 * **********************************************************</pre>*/
	private void checkPolPndCncSta() {
		// COB_CODE: PERFORM 3110-SET-CRS-INPUT
		//              THRU 3110-EXIT.
		setCrsInput();
		// COB_CODE:      EXEC SQL
		//                    SELECT A.NOT_EFF_DT
		//                         , B.ACT_NOT_TYP_CD
		//                         , B.NOT_DT
		//                         , B.EMP_ID
		//                         , C.ACT_NOT_DES
		//                      INTO :DCLACT-NOT-POL.NOT-EFF-DT
		//                         , :DCLACT-NOT.ACT-NOT-TYP-CD
		//                         , :DCLACT-NOT.NOT-DT
		//                         , :DCLACT-NOT.EMP-ID
		//                             :NI-EMP-ID
		//                         , :DCLACT-NOT-TYP.ACT-NOT-DES
		//                      FROM ACT_NOT_POL A
		//                         , ACT_NOT B
		//                         , ACT_NOT_TYP C
		//                     WHERE A.CSR_ACT_NBR = :DCLACT-NOT-POL.CSR-ACT-NBR
		//                       AND A.POL_NBR = :DCLACT-NOT-POL.POL-NBR
		//                       AND A.POL_EFF_DT = :DCLACT-NOT-POL.POL-EFF-DT
		//                       AND A.POL_EXP_DT = :DCLACT-NOT-POL.POL-EXP-DT
		//                       AND A.NOT_EFF_DT IS NOT NULL
		//                       AND A.NOT_PRC_TS =
		//                              (SELECT MAX(D.NOT_PRC_TS)
		//                                 FROM ACT_NOT_POL D
		//                                    , ACT_NOT E
		//                                WHERE D.CSR_ACT_NBR
		//                                          = :DCLACT-NOT-POL.CSR-ACT-NBR
		//                                  AND D.POL_NBR = :DCLACT-NOT-POL.POL-NBR
		//                                  AND D.POL_EFF_DT
		//                                          = :DCLACT-NOT-POL.POL-EFF-DT
		//                                  AND E.CSR_ACT_NBR
		//                                          = :DCLACT-NOT-POL.CSR-ACT-NBR
		//                                  AND E.NOT_PRC_TS = D.NOT_PRC_TS
		//                                  AND E.ACT_NOT_STA_CD = '70'
		//                                  AND E.ACT_OWN_CLT_ID
		//                                          <> 'NA                  '
		//                                  AND E.ACT_NOT_TYP_CD IN ('CNI','CNU','CRW',
		//                                                           'NPC','IMP','RES',
		//                                                           'RNS'))
		//                       AND A.CSR_ACT_NBR = B.CSR_ACT_NBR
		//                       AND A.NOT_PRC_TS = B.NOT_PRC_TS
		//                       AND B.ACT_NOT_TYP_CD IN ('CNI','CNU','CRW',
		//                                                'NPC','IMP')
		//                       AND C.ACT_NOT_TYP_CD = B.ACT_NOT_TYP_CD
		//           * MAKE SURE POLICY ISN'T ALREADY CANCELLED/TERMINATED/EXPIRED
		//                       AND A.POL_NBR =
		//                              (SELECT POL_NBR
		//                                 FROM POL_DTA_EXT F
		//                                WHERE F.ACT_NBR
		//                                          = :DCLACT-NOT-POL.CSR-ACT-NBR
		//                                  AND F.POL_NBR = :DCLACT-NOT-POL.POL-NBR
		//                                  AND F.POL_EFF_DT
		//                                          = :DCLACT-NOT-POL.POL-EFF-DT
		//                                  AND F.POL_PER_EN_TRS_CD IS NULL)
		//                END-EXEC.
		actNotPolTypDtaExtDao.selectRec(ws.getDclactNotPol().getCsrActNbr(), ws.getDclactNotPol().getPolNbr(), ws.getDclactNotPol().getPolEffDt(),
				ws.getDclactNotPol().getPolExpDt(), ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                   END-IF
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                                       OF DCLACT-NOT-POL
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: IF NI-EMP-ID = CF-DB2-IS-NULL
			//                               TO TRUE
			//           END-IF
			if (ws.getNiEmpId() == ws.getConstantFields().getDb2IsNull()) {
				// COB_CODE: MOVE SPACES     TO EMP-ID
				//                           OF DCLACT-NOT
				ws.getDclactNot().setEmpId("");
				// COB_CODE: SET SW-SYSTEM-NOTICE
				//                           TO TRUE
				ws.setSwSystemNoticeFlag(true);
			}
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE SPACES         TO NOT-EFF-DT
			//                               OF DCLACT-NOT-POL
			ws.getDclactNotPol().setNotEffDt("");
		} else {
			// COB_CODE: MOVE CF-PN-3100     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3100());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 3110-SET-CRS-INPUT<br>
	 * <pre>******************************************************
	 *  SET UP PARAMETERS NEEDED TO CHECK CRS TABLES        *
	 * ******************************************************</pre>*/
	private void setCrsInput() {
		// COB_CODE: INITIALIZE DCLACT-NOT-POL
		//                      DCLACT-NOT-TYP.
		initDclactNotPol();
		initDclactNotTyp();
		// COB_CODE: MOVE ACT-NBR                OF DCLPOL-DTA-EXT
		//                                       TO CSR-ACT-NBR OF DCLACT-NOT-POL.
		ws.getDclactNotPol().setCsrActNbr(ws.getDclpolDtaExt().getActNbr());
		// COB_CODE: MOVE POL-NBR                OF DCLPOL-DTA-EXT
		//                                       TO POL-NBR     OF DCLACT-NOT-POL.
		ws.getDclactNotPol().setPolNbr(ws.getDclpolDtaExt().getPolNbr());
		// COB_CODE: MOVE POL-EFF-DT             OF DCLPOL-DTA-EXT
		//                                       TO POL-EFF-DT  OF DCLACT-NOT-POL.
		ws.getDclactNotPol().setPolEffDt(ws.getDclpolDtaExt().getPolEffDt());
		// COB_CODE: MOVE PLN-EXP-DT             OF DCLPOL-DTA-EXT
		//                                       TO POL-EXP-DT  OF DCLACT-NOT-POL.
		ws.getDclactNotPol().setPolExpDt(ws.getDclpolDtaExt().getPlnExpDt());
	}

	/**Original name: 3200-SET-OUTPUT<br>
	 * <pre>**********************************************************
	 *  SET THE OUTPUT TO BE RETURNED                           *
	 * **********************************************************
	 *  SET POLICY KEYS</pre>*/
	private void setOutput() {
		// COB_CODE: MOVE POL-NBR                OF DCLPOL-DTA-EXT
		//                                       TO XZC01O-POL-NBR.
		dfhcommarea.getXzc010ServiceOutputs().getPolKeys().setNbr(ws.getDclpolDtaExt().getPolNbr());
		// COB_CODE: MOVE POL-EFF-DT             OF DCLPOL-DTA-EXT
		//                                       TO XZC01O-POL-EFF-DT.
		dfhcommarea.getXzc010ServiceOutputs().getPolKeys().setEffDt(ws.getDclpolDtaExt().getPolEffDt());
		// COB_CODE: MOVE PLN-EXP-DT             OF DCLPOL-DTA-EXT
		//                                       TO XZC01O-POL-EXP-DT.
		dfhcommarea.getXzc010ServiceOutputs().getPolKeys().setExpDt(ws.getDclpolDtaExt().getPlnExpDt());
		// COB_CODE: IF NOT-EFF-DT OF DCLACT-NOT-POL = SPACES
		//               GO TO 3200-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclactNotPol().getNotEffDt())) {
			// COB_CODE: MOVE CF-NO              TO XZC01O-PND-CNC-IND
			dfhcommarea.getXzc010ServiceOutputs().setPndCncInd(ws.getConstantFields().getNo());
			// COB_CODE: GO TO 3200-EXIT
			return;
		}
		// COB_CODE: MOVE CF-YES                 TO XZC01O-PND-CNC-IND.
		dfhcommarea.getXzc010ServiceOutputs().setPndCncInd(ws.getConstantFields().getYes());
		// RETURN NOTIFICATION DETAILS
		// COB_CODE: MOVE NOT-EFF-DT             OF DCLACT-NOT-POL
		//                                       TO XZC01O-SCH-CNC-DT.
		dfhcommarea.getXzc010ServiceOutputs().setSchCncDt(ws.getDclactNotPol().getNotEffDt());
		// COB_CODE: MOVE ACT-NOT-TYP-CD         OF DCLACT-NOT
		//                                       TO XZC01O-NOT-TYP-CD.
		dfhcommarea.getXzc010ServiceOutputs().setNotTypCd(ws.getDclactNot().getActNotTypCd());
		// COB_CODE: MOVE ACT-NOT-DES            OF DCLACT-NOT-TYP
		//                                       TO XZC01O-NOT-TYP-DES.
		dfhcommarea.getXzc010ServiceOutputs().setNotTypDes(ws.getDclactNotTyp().getActNotDes());
		// COB_CODE: MOVE EMP-ID                 OF DCLACT-NOT
		//                                       TO XZC01O-NOT-EMP-ID.
		dfhcommarea.getXzc010ServiceOutputs().setNotEmpId(ws.getDclactNot().getEmpId());
		//    FIND EMPLOYEE NAME
		// COB_CODE: IF EMP-ID OF DCLACT-NOT = SPACES
		//               END-IF
		//           ELSE
		//               MOVE L-OD-FUL-NM        TO XZC01O-NOT-EMP-NM
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclactNot().getEmpId())) {
			// COB_CODE: IF SW-SYSTEM-NOTICE
			//                                   TO XZC01O-NOT-EMP-NM
			//           ELSE
			//               MOVE SPACES         TO XZC01O-NOT-EMP-NM
			//           END-IF
			if (ws.isSwSystemNoticeFlag()) {
				// COB_CODE: MOVE CF-SYSTEM-NOTICE
				//                               TO XZC01O-NOT-EMP-NM
				dfhcommarea.getXzc010ServiceOutputs().setNotEmpNm(ws.getConstantFields().getSystemNotice());
			} else {
				// COB_CODE: MOVE SPACES         TO XZC01O-NOT-EMP-NM
				dfhcommarea.getXzc010ServiceOutputs().setNotEmpNm("");
			}
		} else {
			// COB_CODE: PERFORM 3210-GET-EMP-NM
			//              THRU 3210-EXIT
			getEmpNm();
			// COB_CODE: MOVE L-OD-FUL-NM        TO XZC01O-NOT-EMP-NM
			dfhcommarea.getXzc010ServiceOutputs().setNotEmpNm(ws.getTt008001().getOdPplData().getFulNm());
		}
		// COB_CODE: MOVE NOT-DT                 TO XZC01O-NOT-PRC-DT.
		dfhcommarea.getXzc010ServiceOutputs().setNotPrcDt(ws.getDclactNot().getNotDt());
	}

	/**Original name: 3210-GET-EMP-NM<br>
	 * <pre>**********************************************************
	 *  FIND EMPLOYEE NAME USING PEOPLE SEARCH SERVICE          *
	 * **********************************************************
	 *  SET PEOPLE SEARCH INPUTS.</pre>*/
	private void getEmpNm() {
		// COB_CODE: MOVE XZC01I-USERID          TO L-SC-USER.
		ws.getTt008001().getSearchCriteria().setUser(dfhcommarea.getiUserid());
		// EXPLANATION OF FLAGS SET:
		//    (1) EMPLOYEE SEARCH TYPE
		//    (2) ACTIVE EMPLOYEES ONLY
		//    (3) EMPLOYEES ONLY (EXCLUDE NON-EMPLOYEES)
		//    (4) SINGLE USER ID RETURNED
		// COB_CODE: SET L-SC-SER-TYP-EMP
		//               L-SC-STA-ACY
		//               L-SC-EMP-CD
		//               L-SC-SGL-USR-ID-CD      TO TRUE.
		ws.getTt008001().getSearchCriteria().getSerTyp().setEmp();
		ws.getTt008001().getSearchCriteria().getStaCd().setAcy();
		ws.getTt008001().getSearchCriteria().getEmpNonempCd().setEmpCd();
		ws.getTt008001().getSearchCriteria().getUsrIdCd().setSglUsrIdCd();
		//    USE "EQUAL TO" EMPLOYEE ID AS CRITERA #1
		// COB_CODE: MOVE EMP-ID                 OF DCLACT-NOT
		//                                       TO L-SC-CRT1-VAL.
		ws.getTt008001().getSearchCriteria().setCrt1Val(ws.getDclactNot().getEmpId());
		// COB_CODE: SET L-SC-CRT1-TYP-EMP-ID
		//               L-SC-CRT1-COP-EQU       TO TRUE.
		ws.getTt008001().getSearchCriteria().getCrt1Typ().setEmpId();
		ws.getTt008001().getSearchCriteria().getCrt1Cop().setEqu();
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM(CF-PEOPLE-SEARCH-MODULE)
		//                COMMAREA(L-SEARCH-CRITERIA)
		//                RESP    (WS-RESPONSE-CODE)
		//                RESP2   (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC01090", execContext).commarea(ws.getTt008001().getSearchCriteria()).length(LSearchCriteria.Len.SEARCH_CRITERIA)
				.link(ws.getConstantFields().getPeopleSearchModule(), new Tt008099());
		ws.setWsResponseCode(execContext.getResp());
		ws.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE TRUE
		//               WHEN WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                      THRU 9930-EXIT
		//               WHEN L-OD-ERR
		//                   GO TO 1000-PROGRAM-EXIT
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3210     TO EA-20-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3210());
			// COB_CODE: MOVE CF-PEOPLE-SEARCH-MODULE
			//                               TO EA-20-CICS-MODULE
			ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setCicsModule(ws.getConstantFields().getPeopleSearchModule());
			// COB_CODE: PERFORM 9930-CICS-LINK-ERROR
			//              THRU 9930-EXIT
			cicsLinkError();
		} else if (ws.getTt008001().getOdErrInd().isErr()) {
			// COB_CODE: SET XZC010-FATAL-ERROR-CODE
			//                               TO TRUE
			dfhcommarea.getXzc010ErrorReturnCode().setFatalErrorCode();
			// COB_CODE: MOVE CF-PROGRAM-NAME
			//                               TO EA-21-PROGRAM-NAME
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setProgramName(ws.getConstantFields().getProgramName());
			// COB_CODE: MOVE CF-PN-3210     TO EA-21-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3210());
			// COB_CODE: MOVE CF-PEOPLE-SEARCH-MODULE
			//                               TO EA-21-CICS-MODULE
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setCicsModule(ws.getConstantFields().getPeopleSearchModule());
			// COB_CODE: MOVE EMP-ID         OF DCLACT-NOT
			//                               TO EA-21-EMP-ID
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setEmpId(ws.getDclactNot().getEmpId());
			// COB_CODE: MOVE XZC01I-POL-NBR TO EA-21-POL-NBR
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setPolNbr(dfhcommarea.getiPolNbr());
			// COB_CODE: MOVE XZC01I-POL-EFF-DT
			//                               TO EA-21-POL-EFF-DT
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setPolEffDt(dfhcommarea.getiPolEffDt());
			// COB_CODE: MOVE XZC01I-POL-EXP-DT
			//                               TO EA-21-POL-EXP-DT
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setPolExpDt(dfhcommarea.getiPolExpDt());
			// COB_CODE: MOVE L-OD-MSG(1:54) TO EA-21-ERR-MSG
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setErrMsg(ws.getTt008001().getOdMsgFormatted().substring((1) - 1, 54));
			// COB_CODE: MOVE EA-21-PEOPLE-SEARCH-ERROR
			//                               TO XZC010-FATAL-ERROR-MESSAGE
			dfhcommarea.setXzc010FatalErrorMessage(ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().getEa21PeopleSearchErrorFormatted());
			// COB_CODE: INITIALIZE XZC010-SERVICE-OUTPUTS
			initXzc010ServiceOutputs();
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
	}

	/**Original name: 9910-SELECT-ERROR<br>
	 * <pre>******************************************************
	 *  SET UP ERROR ON THE SELECT                          *
	 * ******************************************************</pre>*/
	private void selectError() {
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO EA-10-PROGRAM-NAME.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setProgramName(ws.getConstantFields().getProgramName());
		// COB_CODE: MOVE SQLCODE                TO EA-10-SQLCODE.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setSqlcode(sqlca.getSqlcode());
		// COB_CODE: MOVE SQLSTATE               TO EA-10-SQLSTATE.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setSqlstate(sqlca.getSqlstate());
		// COB_CODE: MOVE XZC01I-POL-NBR         TO EA-10-POL-NBR.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC01I-POL-EFF-DT      TO EA-10-POL-EFF-DT.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: MOVE XZC01I-POL-EXP-DT      TO EA-10-POL-EXP-DT.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setPolExpDt(dfhcommarea.getiPolExpDt());
		// COB_CODE: SET XZC010-FATAL-ERROR-CODE TO TRUE.
		dfhcommarea.getXzc010ErrorReturnCode().setFatalErrorCode();
		// COB_CODE: MOVE EA-10-DB2-ERROR-ON-SELECT
		//                                       TO XZC010-FATAL-ERROR-MESSAGE.
		dfhcommarea.setXzc010FatalErrorMessage(ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().getEa10Db2ErrorOnSelectFormatted());
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9920-INVALID-INPUT-NLBE<br>
	 * <pre>******************************************************
	 *  SET UP ERROR ON THE SELECT                          *
	 * ******************************************************</pre>*/
	private void invalidInputNlbe() {
		// COB_CODE: SET XZC010-NLBE-CODE        TO TRUE.
		dfhcommarea.getXzc010ErrorReturnCode().setNlbeCode();
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO EA-01-PROGRAM-NAME.
		ws.getErrorAndAdviceMessages().getEa01InvalidInput().setProgramName(ws.getConstantFields().getProgramName());
		// COB_CODE: MOVE XZC01I-POL-NBR         TO EA-01-POL-NBR.
		ws.getErrorAndAdviceMessages().getEa01InvalidInput().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC01I-POL-EFF-DT      TO EA-01-POL-EFF-DT.
		ws.getErrorAndAdviceMessages().getEa01InvalidInput().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: MOVE XZC01I-POL-EXP-DT      TO EA-01-POL-EXP-DT.
		ws.getErrorAndAdviceMessages().getEa01InvalidInput().setPolExpDt(dfhcommarea.getiPolExpDt());
		// COB_CODE: MOVE EA-01-INVALID-INPUT    TO
		//                           XZC010-NON-LOG-ERR-MSG(SS-NLBE-IDX).
		dfhcommarea.getXzc010NonLoggableErrors(ws.getSsNlbeIdx())
				.setXzc010NonLogErrMsg(ws.getErrorAndAdviceMessages().getEa01InvalidInput().getEa01InvalidInputFormatted());
		// COB_CODE: INITIALIZE XZC010-SERVICE-OUTPUTS.
		initXzc010ServiceOutputs();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9930-CICS-LINK-ERROR<br>
	 * <pre>******************************************************
	 *  SET UP ERROR ON CICS LINK                           *
	 * ******************************************************</pre>*/
	private void cicsLinkError() {
		// COB_CODE: SET XZC010-FATAL-ERROR-CODE TO TRUE.
		dfhcommarea.getXzc010ErrorReturnCode().setFatalErrorCode();
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO EA-20-PROGRAM-NAME.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setProgramName(ws.getConstantFields().getProgramName());
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EA-20-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setResponseCode(ws.getWsResponseCode());
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EA-20-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setResponseCode2(ws.getWsResponseCode2());
		// COB_CODE: MOVE XZC01I-POL-NBR         TO EA-20-POL-NBR.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC01I-POL-EFF-DT      TO EA-20-POL-EFF-DT.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: MOVE XZC01I-POL-EXP-DT      TO EA-20-POL-EXP-DT.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setPolExpDt(dfhcommarea.getiPolExpDt());
		// COB_CODE: MOVE EA-20-CICS-LINK-ERROR  TO XZC010-FATAL-ERROR-MESSAGE.
		dfhcommarea.setXzc010FatalErrorMessage(ws.getErrorAndAdviceMessages().getEa20CicsLinkError().getEa20CicsLinkErrorFormatted());
		// COB_CODE: INITIALIZE XZC010-SERVICE-OUTPUTS.
		initXzc010ServiceOutputs();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	public void initXzc010ServiceOutputs() {
		dfhcommarea.getXzc010ServiceOutputs().getPolKeys().setNbr("");
		dfhcommarea.getXzc010ServiceOutputs().getPolKeys().setEffDt("");
		dfhcommarea.getXzc010ServiceOutputs().getPolKeys().setExpDt("");
		dfhcommarea.getXzc010ServiceOutputs().setPndCncInd(Types.SPACE_CHAR);
		dfhcommarea.getXzc010ServiceOutputs().setSchCncDt("");
		dfhcommarea.getXzc010ServiceOutputs().setNotTypCd("");
		dfhcommarea.getXzc010ServiceOutputs().setNotTypDes("");
		dfhcommarea.getXzc010ServiceOutputs().setNotEmpId("");
		dfhcommarea.getXzc010ServiceOutputs().setNotEmpNm("");
		dfhcommarea.getXzc010ServiceOutputs().setNotPrcDt("");
	}

	public void initXzc010ErrorHandlingParms() {
		dfhcommarea.getXzc010ErrorReturnCode().setErrorReturnCodeFormatted("0000");
		dfhcommarea.setXzc010FatalErrorMessage("");
		dfhcommarea.setXzc010NonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaXzc01090.XZC010_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.getXzc010NonLoggableErrors(idx0).setXzc010NonLogErrMsg("");
		}
		dfhcommarea.setXzc010WarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaXzc01090.XZC010_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.getXzc010Warnings(idx0).setXzc010WarnMsg("");
		}
	}

	public void initDclpolDtaExt() {
		ws.getDclpolDtaExt().setPolNbr("");
		ws.getDclpolDtaExt().setPolEffDt("");
		ws.getDclpolDtaExt().setPlnExpDt("");
		ws.getDclpolDtaExt().setActNbr("");
		ws.getDclpolDtaExt().setPolId("");
		ws.getDclpolDtaExt().setCmpCd("");
		ws.getDclpolDtaExt().setPolTypCd("");
		ws.getDclpolDtaExt().setStsPolTypCd("");
		ws.getDclpolDtaExt().setBndInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setMolCrPolInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setAudInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setLgePrdInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setSplBilPolInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setSirAmt(0);
		ws.getDclpolDtaExt().setPriRskStAbb("");
		ws.getDclpolDtaExt().setPriRskStCd("");
		ws.getDclpolDtaExt().setPriRskCtyCd("");
		ws.getDclpolDtaExt().setPriRskTwnCd("");
		ws.getDclpolDtaExt().setRewPolNbr("");
		ws.getDclpolDtaExt().setRnlQteInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setWhyCncCd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setTmnExpCncDt("");
		ws.getDclpolDtaExt().setPolPerEnTrsCd("");
		ws.getDclpolDtaExt().setAcyPolInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setWrtPrmAmt(0);
		ws.getDclpolDtaExt().setFstTrmInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setSrcSysCd("");
	}

	public void initDclactNotPol() {
		ws.getDclactNotPol().setCsrActNbr("");
		ws.getDclactNotPol().setNotPrcTs("");
		ws.getDclactNotPol().setPolNbr("");
		ws.getDclactNotPol().setPolTypCd("");
		ws.getDclactNotPol().setPolPriRskStAbb("");
		ws.getDclactNotPol().setNotEffDt("");
		ws.getDclactNotPol().setPolEffDt("");
		ws.getDclactNotPol().setPolExpDt("");
		ws.getDclactNotPol().setPolDueAmt(new AfDecimal(0, 10, 2));
		ws.getDclactNotPol().setNinCltId("");
		ws.getDclactNotPol().setNinAdrId("");
		ws.getDclactNotPol().setWfStartedInd(Types.SPACE_CHAR);
		ws.getDclactNotPol().setPolBilStaCd(Types.SPACE_CHAR);
	}

	public void initDclactNotTyp() {
		ws.getDclactNotTyp().setActNotTypCd("");
		ws.getDclactNotTyp().setActNotDes("");
		ws.getDclactNotTyp().setAcyInd(Types.SPACE_CHAR);
		ws.getDclactNotTyp().setDsyOrdNbr(((short) 0));
		ws.getDclactNotTyp().setDocDesLen(((short) 0));
		ws.getDclactNotTyp().setDocDesText("");
		ws.getDclactNotTyp().setActNotPthCd("");
		ws.getDclactNotTyp().setActNotPthDes("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
