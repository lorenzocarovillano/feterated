/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclgenHalUniversalCt2;
import com.federatedinsurance.crs.copy.DclhalBoLokExc;
import com.federatedinsurance.crs.copy.DclhalBoLokType;
import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclhalUowTransact;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallulli;
import com.federatedinsurance.crs.copy.HhsphHalSecProfileRow;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALRLODR<br>
 * Generated as a class for rule WS.<br>*/
public class HalrlodrData {

	//==== PROPERTIES ====
	//Original name: WS-SWITCHES
	private WsSwitches wsSwitches = new WsSwitches();
	//Original name: WS-WORK-FIELDS
	private WsWorkFieldsHalrlodr wsWorkFields = new WsWorkFieldsHalrlodr();
	//Original name: WS-HALRLOMG-LINKAGE
	private WsHalrlomgLinkage2 wsHalrlomgLinkage = new WsHalrlomgLinkage2();
	//Original name: HALLULLI
	private Hallulli hallulli = new Hallulli();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();
	//Original name: DCLHAL-BO-LOK-EXC
	private DclhalBoLokExc dclhalBoLokExc = new DclhalBoLokExc();
	//Original name: DCLHAL-BO-LOK-TYPE
	private DclhalBoLokType dclhalBoLokType = new DclhalBoLokType();
	//Original name: DCLGEN-HAL-UNIVERSAL-CT2
	private DclgenHalUniversalCt2 dclgenHalUniversalCt2 = new DclgenHalUniversalCt2();
	//Original name: DCLHAL-UOW-TRANSACT
	private DclhalUowTransact dclhalUowTransact = new DclhalUowTransact();
	//Original name: HHSPH-HAL-SEC-PROFILE-ROW
	private HhsphHalSecProfileRow hhsphHalSecProfileRow = new HhsphHalSecProfileRow();

	//==== METHODS ====
	public void setWsHallulliLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.WS_HALLULLI_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.WS_HALLULLI_LINKAGE);
		setWsHallulliLinkageBytes(buffer, 1);
	}

	public String getWsHallulliLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getWsHallulliLinkageBytes());
	}

	/**Original name: WS-HALLULLI-LINKAGE<br>
	 * <pre>* LEGACY LOCK MODULE LINKAGE</pre>*/
	public byte[] getWsHallulliLinkageBytes() {
		byte[] buffer = new byte[Len.WS_HALLULLI_LINKAGE];
		return getWsHallulliLinkageBytes(buffer, 1);
	}

	public void setWsHallulliLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallulli.setLockDrvrInputBytes(buffer, position);
		position += Hallulli.Len.LOCK_DRVR_INPUT;
		hallulli.setLegacyLockInfoBytes(buffer, position);
	}

	public byte[] getWsHallulliLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallulli.getLockDrvrInputBytes(buffer, position);
		position += Hallulli.Len.LOCK_DRVR_INPUT;
		hallulli.getLegacyLockInfoBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public DclgenHalUniversalCt2 getDclgenHalUniversalCt2() {
		return dclgenHalUniversalCt2;
	}

	public DclhalBoLokExc getDclhalBoLokExc() {
		return dclhalBoLokExc;
	}

	public DclhalBoLokType getDclhalBoLokType() {
		return dclhalBoLokType;
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclhalUowTransact getDclhalUowTransact() {
		return dclhalUowTransact;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallulli getHallulli() {
		return hallulli;
	}

	public HhsphHalSecProfileRow getHhsphHalSecProfileRow() {
		return hhsphHalSecProfileRow;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrlomgLinkage2 getWsHalrlomgLinkage() {
		return wsHalrlomgLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSwitches getWsSwitches() {
		return wsSwitches;
	}

	public WsWorkFieldsHalrlodr getWsWorkFields() {
		return wsWorkFields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_APPLID = 8;
		public static final int WS_HALLULLI_LINKAGE = Hallulli.Len.LOCK_DRVR_INPUT + Hallulli.Len.LEGACY_LOCK_INFO;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
