/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9040<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9040 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9041_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9040() {
	}

	public LFrameworkResponseAreaXz0r9040(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza940MaxFrmRows(short xza940MaxFrmRows) {
		writeBinaryShort(Pos.XZA940_MAX_FRM_ROWS, xza940MaxFrmRows);
	}

	/**Original name: XZA940-MAX-FRM-ROWS<br>*/
	public short getXza940MaxFrmRows() {
		return readBinaryShort(Pos.XZA940_MAX_FRM_ROWS);
	}

	public void setXza940CsrActNbr(String xza940CsrActNbr) {
		writeString(Pos.XZA940_CSR_ACT_NBR, xza940CsrActNbr, Len.XZA940_CSR_ACT_NBR);
	}

	/**Original name: XZA940-CSR-ACT-NBR<br>*/
	public String getXza940CsrActNbr() {
		return readString(Pos.XZA940_CSR_ACT_NBR, Len.XZA940_CSR_ACT_NBR);
	}

	public void setXza940NotPrcTs(String xza940NotPrcTs) {
		writeString(Pos.XZA940_NOT_PRC_TS, xza940NotPrcTs, Len.XZA940_NOT_PRC_TS);
	}

	/**Original name: XZA940-NOT-PRC-TS<br>*/
	public String getXza940NotPrcTs() {
		return readString(Pos.XZA940_NOT_PRC_TS, Len.XZA940_NOT_PRC_TS);
	}

	public void setXza940PiStartPoint(long xza940PiStartPoint) {
		writeLong(Pos.XZA940_PI_START_POINT, xza940PiStartPoint, Len.Int.XZA940_PI_START_POINT);
	}

	/**Original name: XZA940-PI-START-POINT<br>*/
	public long getXza940PiStartPoint() {
		return readNumDispLong(Pos.XZA940_PI_START_POINT, Len.XZA940_PI_START_POINT);
	}

	public String getlFwRespXz0a9041Formatted(int lFwRespXz0a9041Idx) {
		int position = Pos.lFwRespXz0a9041(lFwRespXz0a9041Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9041);
	}

	public void setXza941rFrmSeqNbr(int xza941rFrmSeqNbrIdx, int xza941rFrmSeqNbr) {
		int position = Pos.xza941FrmSeqNbr(xza941rFrmSeqNbrIdx - 1);
		writeInt(position, xza941rFrmSeqNbr, Len.Int.XZA941R_FRM_SEQ_NBR);
	}

	/**Original name: XZA941R-FRM-SEQ-NBR<br>*/
	public int getXza941rFrmSeqNbr(int xza941rFrmSeqNbrIdx) {
		int position = Pos.xza941FrmSeqNbr(xza941rFrmSeqNbrIdx - 1);
		return readNumDispInt(position, Len.XZA941_FRM_SEQ_NBR);
	}

	public void setXza941rFrmNbr(int xza941rFrmNbrIdx, String xza941rFrmNbr) {
		int position = Pos.xza941FrmNbr(xza941rFrmNbrIdx - 1);
		writeString(position, xza941rFrmNbr, Len.XZA941_FRM_NBR);
	}

	/**Original name: XZA941R-FRM-NBR<br>*/
	public String getXza941rFrmNbr(int xza941rFrmNbrIdx) {
		int position = Pos.xza941FrmNbr(xza941rFrmNbrIdx - 1);
		return readString(position, Len.XZA941_FRM_NBR);
	}

	public void setXza941rFrmEdtDt(int xza941rFrmEdtDtIdx, String xza941rFrmEdtDt) {
		int position = Pos.xza941FrmEdtDt(xza941rFrmEdtDtIdx - 1);
		writeString(position, xza941rFrmEdtDt, Len.XZA941_FRM_EDT_DT);
	}

	/**Original name: XZA941R-FRM-EDT-DT<br>*/
	public String getXza941rFrmEdtDt(int xza941rFrmEdtDtIdx) {
		int position = Pos.xza941FrmEdtDt(xza941rFrmEdtDtIdx - 1);
		return readString(position, Len.XZA941_FRM_EDT_DT);
	}

	public void setXza941rPoStartPoint(int xza941rPoStartPointIdx, long xza941rPoStartPoint) {
		int position = Pos.xza941PoStartPoint(xza941rPoStartPointIdx - 1);
		writeLong(position, xza941rPoStartPoint, Len.Int.XZA941R_PO_START_POINT);
	}

	/**Original name: XZA941R-PO-START-POINT<br>*/
	public long getXza941rPoStartPoint(int xza941rPoStartPointIdx) {
		int position = Pos.xza941PoStartPoint(xza941rPoStartPointIdx - 1);
		return readNumDispLong(position, Len.XZA941_PO_START_POINT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_XZ0A9040 = L_FRAMEWORK_RESPONSE_AREA;
		public static final int XZA940_GET_FRM_LIST_KEY = L_FW_RESP_XZ0A9040;
		public static final int XZA940_MAX_FRM_ROWS = XZA940_GET_FRM_LIST_KEY;
		public static final int XZA940_CSR_ACT_NBR = XZA940_MAX_FRM_ROWS + Len.XZA940_MAX_FRM_ROWS;
		public static final int XZA940_NOT_PRC_TS = XZA940_CSR_ACT_NBR + Len.XZA940_CSR_ACT_NBR;
		public static final int XZA940_PAGING_INPUTS = XZA940_NOT_PRC_TS + Len.XZA940_NOT_PRC_TS;
		public static final int XZA940_PI_START_POINT = XZA940_PAGING_INPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9041(int idx) {
			return XZA940_PI_START_POINT + Len.XZA940_PI_START_POINT + idx * Len.L_FW_RESP_XZ0A9041;
		}

		public static int xza941GetFrmListDetail(int idx) {
			return lFwRespXz0a9041(idx);
		}

		public static int xza941FrmSeqNbr(int idx) {
			return xza941GetFrmListDetail(idx);
		}

		public static int xza941FrmNbr(int idx) {
			return xza941FrmSeqNbr(idx) + Len.XZA941_FRM_SEQ_NBR;
		}

		public static int xza941FrmEdtDt(int idx) {
			return xza941FrmNbr(idx) + Len.XZA941_FRM_NBR;
		}

		public static int xza941PagingOutputs(int idx) {
			return xza941FrmEdtDt(idx) + Len.XZA941_FRM_EDT_DT;
		}

		public static int xza941PoStartPoint(int idx) {
			return xza941PagingOutputs(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA940_MAX_FRM_ROWS = 2;
		public static final int XZA940_CSR_ACT_NBR = 9;
		public static final int XZA940_NOT_PRC_TS = 26;
		public static final int XZA940_PI_START_POINT = 10;
		public static final int XZA941_FRM_SEQ_NBR = 5;
		public static final int XZA941_FRM_NBR = 30;
		public static final int XZA941_FRM_EDT_DT = 10;
		public static final int XZA941_PO_START_POINT = 10;
		public static final int XZA941_PAGING_OUTPUTS = XZA941_PO_START_POINT;
		public static final int XZA941_GET_FRM_LIST_DETAIL = XZA941_FRM_SEQ_NBR + XZA941_FRM_NBR + XZA941_FRM_EDT_DT + XZA941_PAGING_OUTPUTS;
		public static final int L_FW_RESP_XZ0A9041 = XZA941_GET_FRM_LIST_DETAIL;
		public static final int XZA940_PAGING_INPUTS = XZA940_PI_START_POINT;
		public static final int XZA940_GET_FRM_LIST_KEY = XZA940_MAX_FRM_ROWS + XZA940_CSR_ACT_NBR + XZA940_NOT_PRC_TS + XZA940_PAGING_INPUTS;
		public static final int L_FW_RESP_XZ0A9040 = XZA940_GET_FRM_LIST_KEY;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_XZ0A9040
				+ LFrameworkResponseAreaXz0r9040.L_FW_RESP_XZ0A9041_MAXOCCURS * L_FW_RESP_XZ0A9041;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA940_PI_START_POINT = 10;
			public static final int XZA941R_FRM_SEQ_NBR = 5;
			public static final int XZA941R_PO_START_POINT = 10;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
