/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsChildren;

/**Original name: WS-SPECIFIC-MISC<br>
 * Variable: WS-SPECIFIC-MISC from program XZ0D0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSpecificMiscXz0d0001 {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0D0001";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-APPLICATION-LOK-TAB
	private String applicationLokTab = "XXX";
	//Original name: WS-APPLICATION-LOK-NM
	private String applicationLokNm = "CRS";
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "ACT_NOT";
	//Original name: WS-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	/**Original name: WS-CHILDREN<br>
	 * <pre>* LIST ALL DIRECT AND FOREIGN CHILDREN OF THIS BDO HERE.</pre>*/
	private WsChildren children = new WsChildren();

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getApplicationLokTab() {
		return this.applicationLokTab;
	}

	public String getApplicationLokNm() {
		return this.applicationLokNm;
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public WsChildren getChildren() {
		return children;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NOT_PRC_TS = 26;
		public static final int BUS_OBJ_NM = 32;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
