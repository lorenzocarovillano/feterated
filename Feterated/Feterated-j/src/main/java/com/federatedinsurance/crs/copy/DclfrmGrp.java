/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLFRM-GRP<br>
 * Variable: DCLFRM-GRP from copybook XZH00026<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclfrmGrp {

	//==== PROPERTIES ====
	//Original name: EDL-FRM-NM
	private String edlFrmNm = DefaultValues.stringVal(Len.EDL_FRM_NM);
	//Original name: DTA-GRP-NM
	private String dtaGrpNm = DefaultValues.stringVal(Len.DTA_GRP_NM);
	//Original name: DTA-GRP-FLD-NBR
	private short dtaGrpFldNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: TAG-NM
	private String tagNm = DefaultValues.stringVal(Len.TAG_NM);
	//Original name: JUS-CD
	private char jusCd = DefaultValues.CHAR_VAL;
	//Original name: SPE-PRC-CD
	private String spePrcCd = DefaultValues.stringVal(Len.SPE_PRC_CD);
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setEdlFrmNm(String edlFrmNm) {
		this.edlFrmNm = Functions.subString(edlFrmNm, Len.EDL_FRM_NM);
	}

	public String getEdlFrmNm() {
		return this.edlFrmNm;
	}

	public void setDtaGrpNm(String dtaGrpNm) {
		this.dtaGrpNm = Functions.subString(dtaGrpNm, Len.DTA_GRP_NM);
	}

	public String getDtaGrpNm() {
		return this.dtaGrpNm;
	}

	public void setDtaGrpFldNbr(short dtaGrpFldNbr) {
		this.dtaGrpFldNbr = dtaGrpFldNbr;
	}

	public short getDtaGrpFldNbr() {
		return this.dtaGrpFldNbr;
	}

	public void setTagNm(String tagNm) {
		this.tagNm = Functions.subString(tagNm, Len.TAG_NM);
	}

	public String getTagNm() {
		return this.tagNm;
	}

	public void setJusCd(char jusCd) {
		this.jusCd = jusCd;
	}

	public char getJusCd() {
		return this.jusCd;
	}

	public void setSpePrcCd(String spePrcCd) {
		this.spePrcCd = Functions.subString(spePrcCd, Len.SPE_PRC_CD);
	}

	public String getSpePrcCd() {
		return this.spePrcCd;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EDL_FRM_NM = 30;
		public static final int DTA_GRP_NM = 8;
		public static final int TAG_NM = 30;
		public static final int SPE_PRC_CD = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
