/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: W-HALOUIDG-SEARCH-IND<br>
 * Variable: W-HALOUIDG-SEARCH-IND from program HALOETRA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WHalouidgSearchInd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.HALOUIDG_SEARCH_IND);
	public static final String DETERMINE_ERR_REF = "0";
	public static final String ERROR_CODE_OK = "1";
	public static final String ERROR_FOUND_NO_RETRY = "2";

	//==== METHODS ====
	public void setHalouidgSearchInd(short halouidgSearchInd) {
		this.value = NumericDisplay.asString(halouidgSearchInd, Len.HALOUIDG_SEARCH_IND);
	}

	public void setHalouidgSearchIndFormatted(String halouidgSearchInd) {
		this.value = Trunc.toUnsignedNumeric(halouidgSearchInd, Len.HALOUIDG_SEARCH_IND);
	}

	public short getHalouidgSearchInd() {
		return NumericDisplay.asShort(this.value);
	}

	public String getHalouidgSearchIndFormatted() {
		return this.value;
	}

	public void setDetermineErrRef() {
		setHalouidgSearchIndFormatted(DETERMINE_ERR_REF);
	}

	public boolean isErrorCodeOk() {
		return getHalouidgSearchIndFormatted().equals(ERROR_CODE_OK);
	}

	public void setErrorCodeOk() {
		setHalouidgSearchIndFormatted(ERROR_CODE_OK);
	}

	public boolean isErrorFoundNoRetry() {
		return getHalouidgSearchIndFormatted().equals(ERROR_FOUND_NO_RETRY);
	}

	public void setErrorFoundNoRetry() {
		setHalouidgSearchIndFormatted(ERROR_FOUND_NO_RETRY);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HALOUIDG_SEARCH_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
