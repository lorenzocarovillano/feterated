/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-CALL-DATA-PRIV-SW<br>
 * Variable: WS-CALL-DATA-PRIV-SW from program HALOUSDH<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsCallDataPrivSw {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char CALL_DATA_PRIV = 'Y';
	public static final char DONT_CALL_DATA_PRIV = 'N';

	//==== METHODS ====
	public void setCallDataPrivSw(char callDataPrivSw) {
		this.value = callDataPrivSw;
	}

	public char getCallDataPrivSw() {
		return this.value;
	}

	public boolean isCallDataPriv() {
		return value == CALL_DATA_PRIV;
	}

	public void setCallDataPriv() {
		value = CALL_DATA_PRIV;
	}

	public void setDontCallDataPriv() {
		value = DONT_CALL_DATA_PRIV;
	}
}
