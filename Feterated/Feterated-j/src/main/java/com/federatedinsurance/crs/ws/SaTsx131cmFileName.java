/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: SA-TSX131CM-FILE-NAME<br>
 * Variable: SA-TSX131CM-FILE-NAME from program TS030099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class SaTsx131cmFileName extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: SA-TSX131CM-REPORT-NUMBER
	private String reportNumber = DefaultValues.stringVal(Len.REPORT_NUMBER);
	//Original name: SA-TSX131CM-OFFICE-LOCATION
	private String officeLocation = DefaultValues.stringVal(Len.OFFICE_LOCATION);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.TSX131CM_FILE_NAME;
	}

	@Override
	public void deserialize(byte[] buf) {
		setTsx131cmFileNameBytes(buf);
	}

	public String getTsx131cmFileNameFormatted() {
		return MarshalByteExt.bufferToStr(getTsx131cmFileNameBytes());
	}

	public void setTsx131cmFileNameBytes(byte[] buffer) {
		setTsx131cmFileNameBytes(buffer, 1);
	}

	public byte[] getTsx131cmFileNameBytes() {
		byte[] buffer = new byte[Len.TSX131CM_FILE_NAME];
		return getTsx131cmFileNameBytes(buffer, 1);
	}

	public void setTsx131cmFileNameBytes(byte[] buffer, int offset) {
		int position = offset;
		reportNumber = MarshalByte.readString(buffer, position, Len.REPORT_NUMBER);
		position += Len.REPORT_NUMBER;
		officeLocation = MarshalByte.readString(buffer, position, Len.OFFICE_LOCATION);
	}

	public byte[] getTsx131cmFileNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, reportNumber, Len.REPORT_NUMBER);
		position += Len.REPORT_NUMBER;
		MarshalByte.writeString(buffer, position, officeLocation, Len.OFFICE_LOCATION);
		return buffer;
	}

	public void setReportNumber(String reportNumber) {
		this.reportNumber = Functions.subString(reportNumber, Len.REPORT_NUMBER);
	}

	public String getReportNumber() {
		return this.reportNumber;
	}

	public void setOfficeLocation(String officeLocation) {
		this.officeLocation = Functions.subString(officeLocation, Len.OFFICE_LOCATION);
	}

	public String getOfficeLocation() {
		return this.officeLocation;
	}

	@Override
	public byte[] serialize() {
		return getTsx131cmFileNameBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REPORT_NUMBER = 6;
		public static final int OFFICE_LOCATION = 2;
		public static final int TSX131CM_FILE_NAME = REPORT_NUMBER + OFFICE_LOCATION;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
