/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-TABLE-FORMATTER-MODULES<br>
 * Variable: CF-TABLE-FORMATTER-MODULES from program MU0Q0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfTableFormatterModules {

	//==== PROPERTIES ====
	//Original name: CF-TF-CLT-INQUIRE-KEY-FMT
	private String cltInquireKeyFmt = "CAWIGIK";
	//Original name: CF-TF-CLIENT-TAB-FMT
	private String clientTabFmt = "CAWI002";
	//Original name: CF-TF-CLT-CLT-REL-FMT
	private String cltCltRelFmt = "CAWI006";
	//Original name: CF-TF-CLT-OBJ-RELATION-FMT
	private String cltObjRelationFmt = "CAWI008";
	//Original name: CF-TF-CLT-ADDR-COMPOSITE-FMT
	private String cltAddrCompositeFmt = "CAWICAC";

	//==== METHODS ====
	public String getCltInquireKeyFmt() {
		return this.cltInquireKeyFmt;
	}

	public String getClientTabFmt() {
		return this.clientTabFmt;
	}

	public String getCltCltRelFmt() {
		return this.cltCltRelFmt;
	}

	public String getCltObjRelationFmt() {
		return this.cltObjRelationFmt;
	}

	public String getCltAddrCompositeFmt() {
		return this.cltAddrCompositeFmt;
	}
}
