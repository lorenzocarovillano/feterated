/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90O0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90o0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int OO_XML_REQ_ROW_MAXOCCURS = 1000;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90o0() {
	}

	public LServiceContractAreaXz0x90o0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setOiUserid(String oiUserid) {
		writeString(Pos.OI_USERID, oiUserid, Len.OI_USERID);
	}

	/**Original name: XZT9OI-USERID<br>*/
	public String getOiUserid() {
		return readString(Pos.OI_USERID, Len.OI_USERID);
	}

	public String getOiUseridFormatted() {
		return Functions.padBlanks(getOiUserid(), Len.OI_USERID);
	}

	public void setOoTkNotPrcTs(int ooTkNotPrcTsIdx, String ooTkNotPrcTs) {
		int position = Pos.xzt9ooTkNotPrcTs(ooTkNotPrcTsIdx - 1);
		writeString(position, ooTkNotPrcTs, Len.OO_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9OO-TK-NOT-PRC-TS<br>*/
	public String getOoTkNotPrcTs(int ooTkNotPrcTsIdx) {
		int position = Pos.xzt9ooTkNotPrcTs(ooTkNotPrcTsIdx - 1);
		return readString(position, Len.OO_TK_NOT_PRC_TS);
	}

	public void setOoCsrActNbr(int ooCsrActNbrIdx, String ooCsrActNbr) {
		int position = Pos.xzt9ooCsrActNbr(ooCsrActNbrIdx - 1);
		writeString(position, ooCsrActNbr, Len.OO_CSR_ACT_NBR);
	}

	/**Original name: XZT9OO-CSR-ACT-NBR<br>*/
	public String getOoCsrActNbr(int ooCsrActNbrIdx) {
		int position = Pos.xzt9ooCsrActNbr(ooCsrActNbrIdx - 1);
		return readString(position, Len.OO_CSR_ACT_NBR);
	}

	public void setOoActNotTypCd(int ooActNotTypCdIdx, String ooActNotTypCd) {
		int position = Pos.xzt9ooActNotTypCd(ooActNotTypCdIdx - 1);
		writeString(position, ooActNotTypCd, Len.OO_ACT_NOT_TYP_CD);
	}

	/**Original name: XZT9OO-ACT-NOT-TYP-CD<br>*/
	public String getOoActNotTypCd(int ooActNotTypCdIdx) {
		int position = Pos.xzt9ooActNotTypCd(ooActNotTypCdIdx - 1);
		return readString(position, Len.OO_ACT_NOT_TYP_CD);
	}

	public void setOoNotDt(int ooNotDtIdx, String ooNotDt) {
		int position = Pos.xzt9ooNotDt(ooNotDtIdx - 1);
		writeString(position, ooNotDt, Len.OO_NOT_DT);
	}

	/**Original name: XZT9OO-NOT-DT<br>*/
	public String getOoNotDt(int ooNotDtIdx) {
		int position = Pos.xzt9ooNotDt(ooNotDtIdx - 1);
		return readString(position, Len.OO_NOT_DT);
	}

	public void setOoEmpId(int ooEmpIdIdx, String ooEmpId) {
		int position = Pos.xzt9ooEmpId(ooEmpIdIdx - 1);
		writeString(position, ooEmpId, Len.OO_EMP_ID);
	}

	/**Original name: XZT9OO-EMP-ID<br>*/
	public String getOoEmpId(int ooEmpIdIdx) {
		int position = Pos.xzt9ooEmpId(ooEmpIdIdx - 1);
		return readString(position, Len.OO_EMP_ID);
	}

	public void setOoActTypCd(int ooActTypCdIdx, String ooActTypCd) {
		int position = Pos.xzt9ooActTypCd(ooActTypCdIdx - 1);
		writeString(position, ooActTypCd, Len.OO_ACT_TYP_CD);
	}

	/**Original name: XZT9OO-ACT-TYP-CD<br>*/
	public String getOoActTypCd(int ooActTypCdIdx) {
		int position = Pos.xzt9ooActTypCd(ooActTypCdIdx - 1);
		return readString(position, Len.OO_ACT_TYP_CD);
	}

	public void setOoDocDes(int ooDocDesIdx, String ooDocDes) {
		int position = Pos.xzt9ooDocDes(ooDocDesIdx - 1);
		writeString(position, ooDocDes, Len.OO_DOC_DES);
	}

	/**Original name: XZT9OO-DOC-DES<br>*/
	public String getOoDocDes(int ooDocDesIdx) {
		int position = Pos.xzt9ooDocDes(ooDocDesIdx - 1);
		return readString(position, Len.OO_DOC_DES);
	}

	public void setOoTtyCoInd(int ooTtyCoIndIdx, char ooTtyCoInd) {
		int position = Pos.xzt9ooTtyCoInd(ooTtyCoIndIdx - 1);
		writeChar(position, ooTtyCoInd);
	}

	/**Original name: XZT9OO-TTY-CO-IND<br>*/
	public char getOoTtyCoInd(int ooTtyCoIndIdx) {
		int position = Pos.xzt9ooTtyCoInd(ooTtyCoIndIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT90O_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int OI_USERID = XZT90O_SERVICE_INPUTS;
		public static final int XZT90O_SERVICE_OUTPUTS = OI_USERID + Len.OI_USERID;
		public static final int OO_TBL_OF_XML_REQ_ROWS = XZT90O_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt9ooXmlReqRow(int idx) {
			return OO_TBL_OF_XML_REQ_ROWS + idx * Len.OO_XML_REQ_ROW;
		}

		public static int xzt9ooTechnicalKey(int idx) {
			return xzt9ooXmlReqRow(idx);
		}

		public static int xzt9ooTkNotPrcTs(int idx) {
			return xzt9ooTechnicalKey(idx);
		}

		public static int xzt9ooCsrActNbr(int idx) {
			return xzt9ooTkNotPrcTs(idx) + Len.OO_TK_NOT_PRC_TS;
		}

		public static int xzt9ooActNotTypCd(int idx) {
			return xzt9ooCsrActNbr(idx) + Len.OO_CSR_ACT_NBR;
		}

		public static int xzt9ooNotDt(int idx) {
			return xzt9ooActNotTypCd(idx) + Len.OO_ACT_NOT_TYP_CD;
		}

		public static int xzt9ooEmpId(int idx) {
			return xzt9ooNotDt(idx) + Len.OO_NOT_DT;
		}

		public static int xzt9ooActTypCd(int idx) {
			return xzt9ooEmpId(idx) + Len.OO_EMP_ID;
		}

		public static int xzt9ooDocDes(int idx) {
			return xzt9ooActTypCd(idx) + Len.OO_ACT_TYP_CD;
		}

		public static int xzt9ooTtyCoInd(int idx) {
			return xzt9ooDocDes(idx) + Len.OO_DOC_DES;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int OI_USERID = 8;
		public static final int OO_TK_NOT_PRC_TS = 26;
		public static final int OO_TECHNICAL_KEY = OO_TK_NOT_PRC_TS;
		public static final int OO_CSR_ACT_NBR = 9;
		public static final int OO_ACT_NOT_TYP_CD = 5;
		public static final int OO_NOT_DT = 10;
		public static final int OO_EMP_ID = 6;
		public static final int OO_ACT_TYP_CD = 2;
		public static final int OO_DOC_DES = 240;
		public static final int OO_TTY_CO_IND = 1;
		public static final int OO_XML_REQ_ROW = OO_TECHNICAL_KEY + OO_CSR_ACT_NBR + OO_ACT_NOT_TYP_CD + OO_NOT_DT + OO_EMP_ID + OO_ACT_TYP_CD
				+ OO_DOC_DES + OO_TTY_CO_IND;
		public static final int XZT90O_SERVICE_INPUTS = OI_USERID;
		public static final int OO_TBL_OF_XML_REQ_ROWS = LServiceContractAreaXz0x90o0.OO_XML_REQ_ROW_MAXOCCURS * OO_XML_REQ_ROW;
		public static final int XZT90O_SERVICE_OUTPUTS = OO_TBL_OF_XML_REQ_ROWS;
		public static final int L_SERVICE_CONTRACT_AREA = XZT90O_SERVICE_INPUTS + XZT90O_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
