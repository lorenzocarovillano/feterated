/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SA-LP-DATA-SIZE<br>
 * Variable: SA-LP-DATA-SIZE from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaLpDataSize {

	//==== PROPERTIES ====
	private short value = DefaultValues.SHORT_VAL;
	public static final short LP131_BYTE_RECORD = ((short) 131);
	public static final short LP132_BYTE_RECORD = ((short) 132);
	public static final short LP198_BYTE_RECORD = ((short) 198);
	private static final short[] VALID_COM_FILE_SIZE = new short[] { ((short) 131), ((short) 132) };
	private static final short[] VALID_NON_COM_FILE_SIZE = new short[] { ((short) 132), ((short) 198) };

	//==== METHODS ====
	public void setLpDataSize(short lpDataSize) {
		this.value = lpDataSize;
	}

	public short getLpDataSize() {
		return this.value;
	}

	public boolean isLp131ByteRecord() {
		return value == LP131_BYTE_RECORD;
	}

	public boolean isLp132ByteRecord() {
		return value == LP132_BYTE_RECORD;
	}

	public void setLp132ByteRecord() {
		value = LP132_BYTE_RECORD;
	}

	public boolean isLp198ByteRecord() {
		return value == LP198_BYTE_RECORD;
	}

	public boolean isValidComFileSize() {
		return ArrayUtils.contains(VALID_COM_FILE_SIZE, value);
	}

	public boolean isValidNonComFileSize() {
		return ArrayUtils.contains(VALID_NON_COM_FILE_SIZE, value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LP_DATA_SIZE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int LP_DATA_SIZE = 3;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
