/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0X9020<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0x9020Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0x9020 constantFields = new ConstantFieldsXz0x9020();
	//Original name: SS-RL
	private short ssRl = DefaultValues.BIN_SHORT_VAL;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0x9020 workingStorageArea = new WorkingStorageAreaXz0x9020();
	//Original name: MAIN-DRIVER-DATA
	private DfhcommareaTs020000 mainDriverData = new DfhcommareaTs020000();
	//Original name: IX-RS
	private int ixRs = 1;

	//==== METHODS ====
	public void setSsRl(short ssRl) {
		this.ssRl = ssRl;
	}

	public short getSsRl() {
		return this.ssRl;
	}

	public void setIxRs(int ixRs) {
		this.ixRs = ixRs;
	}

	public int getIxRs() {
		return this.ixRs;
	}

	public ConstantFieldsXz0x9020 getConstantFields() {
		return constantFields;
	}

	public DfhcommareaTs020000 getMainDriverData() {
		return mainDriverData;
	}

	public WorkingStorageAreaXz0x9020 getWorkingStorageArea() {
		return workingStorageArea;
	}
}
