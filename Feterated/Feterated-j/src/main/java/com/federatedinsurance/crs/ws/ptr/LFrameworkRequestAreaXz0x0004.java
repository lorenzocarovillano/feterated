/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X0004<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x0004 extends BytesClass {

	//==== PROPERTIES ====
	public static final String XZA004Q_GET_RESCIND_IND = "GetRescindInd";

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x0004() {
	}

	public LFrameworkRequestAreaXz0x0004(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza004qPrimaryFunction(String xza004qPrimaryFunction) {
		writeString(Pos.XZA004Q_PRIMARY_FUNCTION, xza004qPrimaryFunction, Len.XZA004Q_PRIMARY_FUNCTION);
	}

	/**Original name: XZA004Q-PRIMARY-FUNCTION<br>*/
	public String getXza004qPrimaryFunction() {
		return readString(Pos.XZA004Q_PRIMARY_FUNCTION, Len.XZA004Q_PRIMARY_FUNCTION);
	}

	public void setXza004qAccountNumber(String xza004qAccountNumber) {
		writeString(Pos.XZA004Q_ACCOUNT_NUMBER, xza004qAccountNumber, Len.XZA004Q_ACCOUNT_NUMBER);
	}

	/**Original name: XZA004Q-ACCOUNT-NUMBER<br>*/
	public String getXza004qAccountNumber() {
		return readString(Pos.XZA004Q_ACCOUNT_NUMBER, Len.XZA004Q_ACCOUNT_NUMBER);
	}

	public void setXza004qOwnerState(String xza004qOwnerState) {
		writeString(Pos.XZA004Q_OWNER_STATE, xza004qOwnerState, Len.XZA004Q_OWNER_STATE);
	}

	/**Original name: XZA004Q-OWNER-STATE<br>*/
	public String getXza004qOwnerState() {
		return readString(Pos.XZA004Q_OWNER_STATE, Len.XZA004Q_OWNER_STATE);
	}

	public void setXza004qPolicyNumber(String xza004qPolicyNumber) {
		writeString(Pos.XZA004Q_POLICY_NUMBER, xza004qPolicyNumber, Len.XZA004Q_POLICY_NUMBER);
	}

	/**Original name: XZA004Q-POLICY-NUMBER<br>*/
	public String getXza004qPolicyNumber() {
		return readString(Pos.XZA004Q_POLICY_NUMBER, Len.XZA004Q_POLICY_NUMBER);
	}

	public void setXza004qPolicyEffDt(String xza004qPolicyEffDt) {
		writeString(Pos.XZA004Q_POLICY_EFF_DT, xza004qPolicyEffDt, Len.XZA004Q_POLICY_EFF_DT);
	}

	/**Original name: XZA004Q-POLICY-EFF-DT<br>*/
	public String getXza004qPolicyEffDt() {
		return readString(Pos.XZA004Q_POLICY_EFF_DT, Len.XZA004Q_POLICY_EFF_DT);
	}

	public void setXza004qPolicyExpDt(String xza004qPolicyExpDt) {
		writeString(Pos.XZA004Q_POLICY_EXP_DT, xza004qPolicyExpDt, Len.XZA004Q_POLICY_EXP_DT);
	}

	/**Original name: XZA004Q-POLICY-EXP-DT<br>*/
	public String getXza004qPolicyExpDt() {
		return readString(Pos.XZA004Q_POLICY_EXP_DT, Len.XZA004Q_POLICY_EXP_DT);
	}

	public void setXza004qPolicyType(String xza004qPolicyType) {
		writeString(Pos.XZA004Q_POLICY_TYPE, xza004qPolicyType, Len.XZA004Q_POLICY_TYPE);
	}

	/**Original name: XZA004Q-POLICY-TYPE<br>*/
	public String getXza004qPolicyType() {
		return readString(Pos.XZA004Q_POLICY_TYPE, Len.XZA004Q_POLICY_TYPE);
	}

	public void setXza004qPolicyCancDt(String xza004qPolicyCancDt) {
		writeString(Pos.XZA004Q_POLICY_CANC_DT, xza004qPolicyCancDt, Len.XZA004Q_POLICY_CANC_DT);
	}

	/**Original name: XZA004Q-POLICY-CANC-DT<br>*/
	public String getXza004qPolicyCancDt() {
		return readString(Pos.XZA004Q_POLICY_CANC_DT, Len.XZA004Q_POLICY_CANC_DT);
	}

	public void setXza004qRescindInd(char xza004qRescindInd) {
		writeChar(Pos.XZA004Q_RESCIND_IND, xza004qRescindInd);
	}

	/**Original name: XZA004Q-RESCIND-IND<br>*/
	public char getXza004qRescindInd() {
		return readChar(Pos.XZA004Q_RESCIND_IND);
	}

	public void setXza004qUserid(String xza004qUserid) {
		writeString(Pos.XZA004Q_USERID, xza004qUserid, Len.XZA004Q_USERID);
	}

	/**Original name: XZA004Q-USERID<br>*/
	public String getXza004qUserid() {
		return readString(Pos.XZA004Q_USERID, Len.XZA004Q_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_RESCIND = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA004Q_GET_RESCIND_IND_ROW = L_FW_REQ_RESCIND;
		public static final int XZA004Q_PRIMARY_FUNCTION = XZA004Q_GET_RESCIND_IND_ROW;
		public static final int XZA004Q_ACCOUNT_NUMBER = XZA004Q_PRIMARY_FUNCTION + Len.XZA004Q_PRIMARY_FUNCTION;
		public static final int XZA004Q_OWNER_STATE = XZA004Q_ACCOUNT_NUMBER + Len.XZA004Q_ACCOUNT_NUMBER;
		public static final int XZA004Q_POLICY_NUMBER = XZA004Q_OWNER_STATE + Len.XZA004Q_OWNER_STATE;
		public static final int XZA004Q_POLICY_EFF_DT = XZA004Q_POLICY_NUMBER + Len.XZA004Q_POLICY_NUMBER;
		public static final int XZA004Q_POLICY_EXP_DT = XZA004Q_POLICY_EFF_DT + Len.XZA004Q_POLICY_EFF_DT;
		public static final int XZA004Q_POLICY_TYPE = XZA004Q_POLICY_EXP_DT + Len.XZA004Q_POLICY_EXP_DT;
		public static final int XZA004Q_POLICY_CANC_DT = XZA004Q_POLICY_TYPE + Len.XZA004Q_POLICY_TYPE;
		public static final int XZA004Q_RESCIND_IND = XZA004Q_POLICY_CANC_DT + Len.XZA004Q_POLICY_CANC_DT;
		public static final int XZA004Q_USERID = XZA004Q_RESCIND_IND + Len.XZA004Q_RESCIND_IND;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA004Q_PRIMARY_FUNCTION = 32;
		public static final int XZA004Q_ACCOUNT_NUMBER = 9;
		public static final int XZA004Q_OWNER_STATE = 2;
		public static final int XZA004Q_POLICY_NUMBER = 25;
		public static final int XZA004Q_POLICY_EFF_DT = 10;
		public static final int XZA004Q_POLICY_EXP_DT = 10;
		public static final int XZA004Q_POLICY_TYPE = 3;
		public static final int XZA004Q_POLICY_CANC_DT = 10;
		public static final int XZA004Q_RESCIND_IND = 1;
		public static final int XZA004Q_USERID = 8;
		public static final int XZA004Q_GET_RESCIND_IND_ROW = XZA004Q_PRIMARY_FUNCTION + XZA004Q_ACCOUNT_NUMBER + XZA004Q_OWNER_STATE
				+ XZA004Q_POLICY_NUMBER + XZA004Q_POLICY_EFF_DT + XZA004Q_POLICY_EXP_DT + XZA004Q_POLICY_TYPE + XZA004Q_POLICY_CANC_DT
				+ XZA004Q_RESCIND_IND + XZA004Q_USERID;
		public static final int L_FW_REQ_RESCIND = XZA004Q_GET_RESCIND_IND_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_RESCIND;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
