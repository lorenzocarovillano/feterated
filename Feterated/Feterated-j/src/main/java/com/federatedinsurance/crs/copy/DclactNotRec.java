/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRec1;
import com.federatedinsurance.crs.commons.data.to.IActNotRec;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: DCLACT-NOT-REC<br>
 * Variable: DCLACT-NOT-REC from copybook XZH00006<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclactNotRec implements IActNotFrmRec1, IActNotRec {

	//==== PROPERTIES ====
	//Original name: CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: REC-SEQ-NBR
	private short recSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);
	//Original name: REC-CLT-ID
	private String recCltId = DefaultValues.stringVal(Len.REC_CLT_ID);
	//Original name: REC-NM-LEN
	private short recNmLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: REC-NM-TEXT
	private String recNmText = DefaultValues.stringVal(Len.REC_NM_TEXT);
	//Original name: REC-ADR-ID
	private String recAdrId = DefaultValues.stringVal(Len.REC_ADR_ID);
	//Original name: LIN-1-ADR
	private String lin1Adr = DefaultValues.stringVal(Len.LIN1_ADR);
	//Original name: LIN-2-ADR
	private String lin2Adr = DefaultValues.stringVal(Len.LIN2_ADR);
	//Original name: CIT-NM
	private String citNm = DefaultValues.stringVal(Len.CIT_NM);
	//Original name: ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: PST-CD
	private String pstCd = DefaultValues.stringVal(Len.PST_CD);
	//Original name: MNL-IND
	private char mnlInd = DefaultValues.CHAR_VAL;
	//Original name: CER-NBR
	private String cerNbr = DefaultValues.stringVal(Len.CER_NBR);

	//==== METHODS ====
	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	@Override
	public void setRecSeqNbr(short recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	@Override
	public short getRecSeqNbr() {
		return this.recSeqNbr;
	}

	@Override
	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	@Override
	public String getRecTypCd() {
		return this.recTypCd;
	}

	public String getRecTypCdFormatted() {
		return Functions.padBlanks(getRecTypCd(), Len.REC_TYP_CD);
	}

	@Override
	public void setRecCltId(String recCltId) {
		this.recCltId = Functions.subString(recCltId, Len.REC_CLT_ID);
	}

	@Override
	public String getRecCltId() {
		return this.recCltId;
	}

	public String getRecCltIdFormatted() {
		return Functions.padBlanks(getRecCltId(), Len.REC_CLT_ID);
	}

	public void setRecNmLen(short recNmLen) {
		this.recNmLen = recNmLen;
	}

	public short getRecNmLen() {
		return this.recNmLen;
	}

	public void setRecNmText(String recNmText) {
		this.recNmText = Functions.subString(recNmText, Len.REC_NM_TEXT);
	}

	public String getRecNmText() {
		return this.recNmText;
	}

	@Override
	public void setRecAdrId(String recAdrId) {
		this.recAdrId = Functions.subString(recAdrId, Len.REC_ADR_ID);
	}

	@Override
	public String getRecAdrId() {
		return this.recAdrId;
	}

	public String getRecAdrIdFormatted() {
		return Functions.padBlanks(getRecAdrId(), Len.REC_ADR_ID);
	}

	@Override
	public void setLin1Adr(String lin1Adr) {
		this.lin1Adr = Functions.subString(lin1Adr, Len.LIN1_ADR);
	}

	@Override
	public String getLin1Adr() {
		return this.lin1Adr;
	}

	public String getLin1AdrFormatted() {
		return Functions.padBlanks(getLin1Adr(), Len.LIN1_ADR);
	}

	@Override
	public void setLin2Adr(String lin2Adr) {
		this.lin2Adr = Functions.subString(lin2Adr, Len.LIN2_ADR);
	}

	@Override
	public String getLin2Adr() {
		return this.lin2Adr;
	}

	@Override
	public void setCitNm(String citNm) {
		this.citNm = Functions.subString(citNm, Len.CIT_NM);
	}

	@Override
	public String getCitNm() {
		return this.citNm;
	}

	public String getCitNmFormatted() {
		return Functions.padBlanks(getCitNm(), Len.CIT_NM);
	}

	@Override
	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	@Override
	public String getStAbb() {
		return this.stAbb;
	}

	@Override
	public void setPstCd(String pstCd) {
		this.pstCd = Functions.subString(pstCd, Len.PST_CD);
	}

	@Override
	public String getPstCd() {
		return this.pstCd;
	}

	@Override
	public void setMnlInd(char mnlInd) {
		this.mnlInd = mnlInd;
	}

	@Override
	public char getMnlInd() {
		return this.mnlInd;
	}

	@Override
	public void setCerNbr(String cerNbr) {
		this.cerNbr = Functions.subString(cerNbr, Len.CER_NBR);
	}

	@Override
	public String getCerNbr() {
		return this.cerNbr;
	}

	@Override
	public String getAdrId() {
		return getRecAdrId();
	}

	@Override
	public void setAdrId(String adrId) {
		this.setRecAdrId(adrId);
	}

	@Override
	public String getAdrIdObj() {
		return getAdrId();
	}

	@Override
	public void setAdrIdObj(String adrIdObj) {
		setAdrId(adrIdObj);
	}

	@Override
	public String getCerNbrObj() {
		return getCerNbr();
	}

	@Override
	public void setCerNbrObj(String cerNbrObj) {
		setCerNbr(cerNbrObj);
	}

	@Override
	public String getCitNmObj() {
		return getCitNm();
	}

	@Override
	public void setCitNmObj(String citNmObj) {
		setCitNm(citNmObj);
	}

	@Override
	public String getCltId() {
		return getRecCltId();
	}

	@Override
	public void setCltId(String cltId) {
		this.setRecCltId(cltId);
	}

	@Override
	public String getCltIdObj() {
		return getCltId();
	}

	@Override
	public void setCltIdObj(String cltIdObj) {
		setCltId(cltIdObj);
	}

	@Override
	public String getLin1AdrObj() {
		return getLin1Adr();
	}

	@Override
	public void setLin1AdrObj(String lin1AdrObj) {
		setLin1Adr(lin1AdrObj);
	}

	@Override
	public String getLin2AdrObj() {
		return getLin2Adr();
	}

	@Override
	public void setLin2AdrObj(String lin2AdrObj) {
		setLin2Adr(lin2AdrObj);
	}

	@Override
	public String getPstCdObj() {
		return getPstCd();
	}

	@Override
	public void setPstCdObj(String pstCdObj) {
		setPstCd(pstCdObj);
	}

	@Override
	public String getRecAdrIdObj() {
		return getRecAdrId();
	}

	@Override
	public void setRecAdrIdObj(String recAdrIdObj) {
		setRecAdrId(recAdrIdObj);
	}

	@Override
	public String getRecCltIdObj() {
		return getRecCltId();
	}

	@Override
	public void setRecCltIdObj(String recCltIdObj) {
		setRecCltId(recCltIdObj);
	}

	@Override
	public String getRecNm() {
		throw new FieldNotMappedException("recNm");
	}

	@Override
	public void setRecNm(String recNm) {
		throw new FieldNotMappedException("recNm");
	}

	@Override
	public String getRecNmObj() {
		return getRecNm();
	}

	@Override
	public void setRecNmObj(String recNmObj) {
		setRecNm(recNmObj);
	}

	@Override
	public String getStAbbObj() {
		return getStAbb();
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		setStAbb(stAbbObj);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_TYP_CD = 5;
		public static final int REC_NM_TEXT = 120;
		public static final int LIN1_ADR = 45;
		public static final int LIN2_ADR = 45;
		public static final int CIT_NM = 30;
		public static final int ST_ABB = 2;
		public static final int PST_CD = 13;
		public static final int CER_NBR = 25;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int REC_CLT_ID = 64;
		public static final int REC_ADR_ID = 64;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
