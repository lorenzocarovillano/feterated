/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-TAR-SYS<br>
 * Variable: SA-TAR-SYS from program FNC02090<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaTarSysFnc02090 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.SA_TAR_SYS);
	public static final String DEV1 = "DEV1";
	public static final String DEV2 = "DEV2";
	public static final String BETA1 = "BETA1";
	public static final String BETA2 = "BETA2";
	public static final String EDUC1 = "EDUC1";
	public static final String EDUC2 = "EDUC2";
	public static final String PROD = "PROD";

	//==== METHODS ====
	public void setSaTarSys(String saTarSys) {
		this.value = Functions.subString(saTarSys, Len.SA_TAR_SYS);
	}

	public String getSaTarSys() {
		return this.value;
	}

	public void setDev1() {
		value = DEV1;
	}

	public void setDev2() {
		value = DEV2;
	}

	public void setBeta1() {
		value = BETA1;
	}

	public void setBeta2() {
		value = BETA2;
	}

	public void setEduc1() {
		value = EDUC1;
	}

	public void setEduc2() {
		value = EDUC2;
	}

	public void setProd() {
		value = PROD;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_TAR_SYS = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
