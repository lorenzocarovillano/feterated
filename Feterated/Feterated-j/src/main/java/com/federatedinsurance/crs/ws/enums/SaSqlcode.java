/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SA-SQLCODE<br>
 * Variable: SA-SQLCODE from program XZ001000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaSqlcode {

	//==== PROPERTIES ====
	private int value = DefaultValues.BIN_INT_VAL;
	public static final int GOOD_RETURN = 0;
	public static final int ROW_NOT_FOUND = 100;
	public static final int DUPLICATE_VALUES = -803;
	public static final int CODE_DEADLOCK = -904;
	private static final int[] CONTENTION = new int[] { -904, -911, -913 };

	//==== METHODS ====
	public void setSqlcode(int sqlcode) {
		this.value = sqlcode;
	}

	public int getSqlcode() {
		return this.value;
	}

	public boolean isGoodReturn() {
		return value == GOOD_RETURN;
	}

	public boolean isContention() {
		return ArrayUtils.contains(CONTENTION, value);
	}
}
