/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ0P90E0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz0p90e0 {

	//==== PROPERTIES ====
	//Original name: SW-ACT-NOT-FRM-ALC-FLAG
	private boolean actNotFrmAlcFlag = false;
	//Original name: SW-ACT-NOT-FRM-REC-ALC-FLAG
	private boolean actNotFrmRecAlcFlag = false;
	//Original name: SW-ACT-NOT-POL-FRM-ALC-FLAG
	private boolean actNotPolFrmAlcFlag = false;
	//Original name: SW-END-OF-CURSOR-FLAG
	private boolean endOfCursorFlag = false;
	//Original name: SW-END-OF-REC-CURSOR-FLAG
	private boolean endOfRecCursorFlag = false;
	//Original name: SW-END-OF-POL-REC-CSR-FLAG
	private boolean endOfPolRecCsrFlag = false;
	//Original name: SW-FIRST-TIME-THRU-FLAG
	private boolean firstTimeThruFlag = true;
	//Original name: SW-FORMS-ATTACHED-FLAG
	private boolean formsAttachedFlag = false;
	//Original name: SW-PAYMENT-PLAN-RETRIEVED-FLAG
	private boolean paymentPlanRetrievedFlag = false;
	//Original name: SW-PCN-COUNT-RETRIEVED-FLAG
	private boolean pcnCountRetrievedFlag = false;
	//Original name: SW-POLICY-LIST-ALC-FLAG
	private boolean policyListAlcFlag = false;
	//Original name: SW-RECIPIENT-ADINS-FLAG
	private boolean recipientAdinsFlag = false;
	//Original name: SW-RECIPIENT-ANI-FLAG
	private boolean recipientAniFlag = false;
	//Original name: SW-RECIPIENT-CERT-FLAG
	private boolean recipientCertFlag = false;
	//Original name: SW-RECIPIENT-INS-FLAG
	private boolean recipientInsFlag = false;
	//Original name: SW-RECIPIENT-LPEMG-FLAG
	private boolean recipientLpemgFlag = false;
	//Original name: SW-MONTHLY-PAY-STA-CD-FLAG
	private boolean monthlyPayStaCdFlag = false;

	//==== METHODS ====
	public void setActNotFrmAlcFlag(boolean actNotFrmAlcFlag) {
		this.actNotFrmAlcFlag = actNotFrmAlcFlag;
	}

	public boolean isActNotFrmAlcFlag() {
		return this.actNotFrmAlcFlag;
	}

	public void setActNotFrmRecAlcFlag(boolean actNotFrmRecAlcFlag) {
		this.actNotFrmRecAlcFlag = actNotFrmRecAlcFlag;
	}

	public boolean isActNotFrmRecAlcFlag() {
		return this.actNotFrmRecAlcFlag;
	}

	public void setActNotPolFrmAlcFlag(boolean actNotPolFrmAlcFlag) {
		this.actNotPolFrmAlcFlag = actNotPolFrmAlcFlag;
	}

	public boolean isActNotPolFrmAlcFlag() {
		return this.actNotPolFrmAlcFlag;
	}

	public void setEndOfCursorFlag(boolean endOfCursorFlag) {
		this.endOfCursorFlag = endOfCursorFlag;
	}

	public boolean isEndOfCursorFlag() {
		return this.endOfCursorFlag;
	}

	public void setEndOfRecCursorFlag(boolean endOfRecCursorFlag) {
		this.endOfRecCursorFlag = endOfRecCursorFlag;
	}

	public boolean isEndOfRecCursorFlag() {
		return this.endOfRecCursorFlag;
	}

	public void setEndOfPolRecCsrFlag(boolean endOfPolRecCsrFlag) {
		this.endOfPolRecCsrFlag = endOfPolRecCsrFlag;
	}

	public boolean isEndOfPolRecCsrFlag() {
		return this.endOfPolRecCsrFlag;
	}

	public void setFirstTimeThruFlag(boolean firstTimeThruFlag) {
		this.firstTimeThruFlag = firstTimeThruFlag;
	}

	public boolean isFirstTimeThruFlag() {
		return this.firstTimeThruFlag;
	}

	public void setFormsAttachedFlag(boolean formsAttachedFlag) {
		this.formsAttachedFlag = formsAttachedFlag;
	}

	public boolean isFormsAttachedFlag() {
		return this.formsAttachedFlag;
	}

	public void setPaymentPlanRetrievedFlag(boolean paymentPlanRetrievedFlag) {
		this.paymentPlanRetrievedFlag = paymentPlanRetrievedFlag;
	}

	public boolean isPaymentPlanRetrievedFlag() {
		return this.paymentPlanRetrievedFlag;
	}

	public void setPcnCountRetrievedFlag(boolean pcnCountRetrievedFlag) {
		this.pcnCountRetrievedFlag = pcnCountRetrievedFlag;
	}

	public boolean isPcnCountRetrievedFlag() {
		return this.pcnCountRetrievedFlag;
	}

	public void setPolicyListAlcFlag(boolean policyListAlcFlag) {
		this.policyListAlcFlag = policyListAlcFlag;
	}

	public boolean isPolicyListAlcFlag() {
		return this.policyListAlcFlag;
	}

	public void setRecipientAdinsFlag(boolean recipientAdinsFlag) {
		this.recipientAdinsFlag = recipientAdinsFlag;
	}

	public boolean isRecipientAdinsFlag() {
		return this.recipientAdinsFlag;
	}

	public void setRecipientAniFlag(boolean recipientAniFlag) {
		this.recipientAniFlag = recipientAniFlag;
	}

	public boolean isRecipientAniFlag() {
		return this.recipientAniFlag;
	}

	public void setRecipientCertFlag(boolean recipientCertFlag) {
		this.recipientCertFlag = recipientCertFlag;
	}

	public boolean isRecipientCertFlag() {
		return this.recipientCertFlag;
	}

	public void setRecipientInsFlag(boolean recipientInsFlag) {
		this.recipientInsFlag = recipientInsFlag;
	}

	public boolean isRecipientInsFlag() {
		return this.recipientInsFlag;
	}

	public void setRecipientLpemgFlag(boolean recipientLpemgFlag) {
		this.recipientLpemgFlag = recipientLpemgFlag;
	}

	public boolean isRecipientLpemgFlag() {
		return this.recipientLpemgFlag;
	}

	public void setMonthlyPayStaCdFlag(boolean monthlyPayStaCdFlag) {
		this.monthlyPayStaCdFlag = monthlyPayStaCdFlag;
	}

	public boolean isMonthlyPayStaCdFlag() {
		return this.monthlyPayStaCdFlag;
	}
}
