/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UBOC-PROCESSING-STATUS-SW<br>
 * Variable: UBOC-PROCESSING-STATUS-SW from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocProcessingStatusSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CONTINUE_PROCESSING = ' ';
	public static final char HALT_AND_RETURN = 'H';

	//==== METHODS ====
	public void setUbocProcessingStatusSw(char ubocProcessingStatusSw) {
		this.value = ubocProcessingStatusSw;
	}

	public char getUbocProcessingStatusSw() {
		return this.value;
	}

	public void setUbocContinueProcessing() {
		value = CONTINUE_PROCESSING;
	}

	public boolean isUbocHaltAndReturn() {
		return value == HALT_AND_RETURN;
	}

	public void setHaltAndReturn() {
		value = HALT_AND_RETURN;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_PROCESSING_STATUS_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
