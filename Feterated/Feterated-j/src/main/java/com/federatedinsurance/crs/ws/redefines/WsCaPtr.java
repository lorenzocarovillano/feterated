/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-CA-PTR<br>
 * Variable: WS-CA-PTR from program MU0X0004<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsCaPtr extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WsCaPtr() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_CA_PTR;
	}

	public void setWsCaPtr(int wsCaPtr) {
		writeBinaryInt(Pos.WS_CA_PTR, wsCaPtr);
	}

	public int getWsCaPtr() {
		return readBinaryInt(Pos.WS_CA_PTR);
	}

	public void setWsCaPtrVal(int wsCaPtrVal) {
		writeBinaryInt(Pos.WS_CA_PTR_VAL, wsCaPtrVal);
	}

	/**Original name: WS-CA-PTR-VAL<br>*/
	public int getWsCaPtrVal() {
		return readBinaryInt(Pos.WS_CA_PTR_VAL);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_CA_PTR = 1;
		public static final int WS_CA_PTR_VAL = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_CA_PTR = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
