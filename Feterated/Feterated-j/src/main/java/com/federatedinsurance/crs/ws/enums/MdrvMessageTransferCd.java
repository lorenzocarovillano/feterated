/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: MDRV-MESSAGE-TRANSFER-CD<br>
 * Variable: MDRV-MESSAGE-TRANSFER-CD from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvMessageTransferCd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char PARTIAL_MESSAGE = 'P';
	public static final char MESSAGE_COMPLETE = 'C';
	public static final char RETRIEVE_RESPONSE = 'R';

	//==== METHODS ====
	public void setMessageTransferCd(char messageTransferCd) {
		this.value = messageTransferCd;
	}

	public char getMessageTransferCd() {
		return this.value;
	}

	public void setMessageComplete() {
		value = MESSAGE_COMPLETE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MESSAGE_TRANSFER_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
