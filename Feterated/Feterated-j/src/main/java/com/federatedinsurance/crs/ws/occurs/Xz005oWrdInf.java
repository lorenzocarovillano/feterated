/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: XZ005O-WRD-INF<br>
 * Variables: XZ005O-WRD-INF from copybook XZ05CL1O<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xz005oWrdInf implements ICopyable<Xz005oWrdInf> {

	//==== PROPERTIES ====
	//Original name: XZ005O-WRD-NM
	private String xz005oWrdNm = DefaultValues.stringVal(Len.XZ005O_WRD_NM);

	//==== CONSTRUCTORS ====
	public Xz005oWrdInf() {
	}

	public Xz005oWrdInf(Xz005oWrdInf wrdInf) {
		this();
		this.xz005oWrdNm = wrdInf.xz005oWrdNm;
	}

	//==== METHODS ====
	public void setWrdInfBytes(byte[] buffer, int offset) {
		int position = offset;
		xz005oWrdNm = MarshalByte.readString(buffer, position, Len.XZ005O_WRD_NM);
	}

	public byte[] getWrdInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xz005oWrdNm, Len.XZ005O_WRD_NM);
		return buffer;
	}

	public void initWrdInfLowValues() {
		xz005oWrdNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ005O_WRD_NM);
	}

	public void initWrdInfSpaces() {
		xz005oWrdNm = "";
	}

	public void setXz005oWrdNm(String xz005oWrdNm) {
		this.xz005oWrdNm = Functions.subString(xz005oWrdNm, Len.XZ005O_WRD_NM);
	}

	public String getXz005oWrdNm() {
		return this.xz005oWrdNm;
	}

	@Override
	public Xz005oWrdInf copy() {
		return new Xz005oWrdInf(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZ005O_WRD_NM = 10;
		public static final int WRD_INF = XZ005O_WRD_NM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
