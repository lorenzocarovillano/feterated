/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9041-ROW<br>
 * Variable: WS-XZ0A9041-ROW from program XZ0B9040<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9041Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA941-FRM-SEQ-NBR
	private int frmSeqNbr = DefaultValues.INT_VAL;
	//Original name: XZA941-FRM-NBR
	private String frmNbr = DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: XZA941-FRM-EDT-DT
	private String frmEdtDt = DefaultValues.stringVal(Len.FRM_EDT_DT);
	//Original name: XZA941-PO-START-POINT
	private long poStartPoint = DefaultValues.LONG_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9041_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9041RowBytes(buf);
	}

	public String getWsXz0a9041RowFormatted() {
		return getGetFrmListDetailFormatted();
	}

	public void setWsXz0a9041RowBytes(byte[] buffer) {
		setWsXz0a9041RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9041RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9041_ROW];
		return getWsXz0a9041RowBytes(buffer, 1);
	}

	public void setWsXz0a9041RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetFrmListDetailBytes(buffer, position);
	}

	public byte[] getWsXz0a9041RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetFrmListDetailBytes(buffer, position);
		return buffer;
	}

	public String getGetFrmListDetailFormatted() {
		return MarshalByteExt.bufferToStr(getGetFrmListDetailBytes());
	}

	/**Original name: XZA941-GET-FRM-LIST-DETAIL<br>
	 * <pre>*************************************************************
	 *  XZ0A9041 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_FRM_LIST                           *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  20JAN2009 E404DLP    NEW                          *
	 *  PP02756  04FEB2011 E404DLP    ADDED PAGING FIELDS          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetFrmListDetailBytes() {
		byte[] buffer = new byte[Len.GET_FRM_LIST_DETAIL];
		return getGetFrmListDetailBytes(buffer, 1);
	}

	public void setGetFrmListDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		frmSeqNbr = MarshalByte.readInt(buffer, position, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		frmNbr = MarshalByte.readString(buffer, position, Len.FRM_NBR);
		position += Len.FRM_NBR;
		frmEdtDt = MarshalByte.readString(buffer, position, Len.FRM_EDT_DT);
		position += Len.FRM_EDT_DT;
		setPagingOutputsBytes(buffer, position);
	}

	public byte[] getGetFrmListDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeInt(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		MarshalByte.writeString(buffer, position, frmNbr, Len.FRM_NBR);
		position += Len.FRM_NBR;
		MarshalByte.writeString(buffer, position, frmEdtDt, Len.FRM_EDT_DT);
		position += Len.FRM_EDT_DT;
		getPagingOutputsBytes(buffer, position);
		return buffer;
	}

	public void setFrmSeqNbr(int frmSeqNbr) {
		this.frmSeqNbr = frmSeqNbr;
	}

	public int getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	public void setFrmNbr(String frmNbr) {
		this.frmNbr = Functions.subString(frmNbr, Len.FRM_NBR);
	}

	public String getFrmNbr() {
		return this.frmNbr;
	}

	public void setFrmEdtDt(String frmEdtDt) {
		this.frmEdtDt = Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	public String getFrmEdtDt() {
		return this.frmEdtDt;
	}

	public void setPagingOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		poStartPoint = MarshalByte.readLong(buffer, position, Len.PO_START_POINT);
	}

	public byte[] getPagingOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeLong(buffer, position, poStartPoint, Len.PO_START_POINT);
		return buffer;
	}

	public void setPoStartPoint(long poStartPoint) {
		this.poStartPoint = poStartPoint;
	}

	public long getPoStartPoint() {
		return this.poStartPoint;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9041RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FRM_SEQ_NBR = 5;
		public static final int FRM_NBR = 30;
		public static final int FRM_EDT_DT = 10;
		public static final int PO_START_POINT = 10;
		public static final int PAGING_OUTPUTS = PO_START_POINT;
		public static final int GET_FRM_LIST_DETAIL = FRM_SEQ_NBR + FRM_NBR + FRM_EDT_DT + PAGING_OUTPUTS;
		public static final int WS_XZ0A9041_ROW = GET_FRM_LIST_DETAIL;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
