/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.SaCicsApplidFnc02090;
import com.federatedinsurance.crs.ws.enums.SaTarSysFnc02090;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program FNC02090<br>
 * Generated as a class for rule WS.<br>*/
public class Fnc02090Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsFnc02090 constantFields = new ConstantFieldsFnc02090();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesFnc02090 errorAndAdviceMessages = new ErrorAndAdviceMessagesFnc02090();
	//Original name: SA-CICS-APPLID
	private SaCicsApplidFnc02090 saCicsApplid = new SaCicsApplidFnc02090();
	//Original name: SA-TAR-SYS
	private SaTarSysFnc02090 saTarSys = new SaTarSysFnc02090();
	/**Original name: SS-IO<br>
	 * <pre>    SUBSCRIPT FOR INPUT/OUTPUT MATCH STRING ARRAY</pre>*/
	private short ssIo = ((short) 0);
	public static final short SS_IO_MAX = ((short) 4);
	//Original name: URI-LKU-LINKAGE
	private Ts571cb1 ts571cb1 = new Ts571cb1();
	//Original name: UID-LKU-LINKAGE
	private DfhcommareaTs571098 uidLkuLinkage = new DfhcommareaTs571098();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaFnc02090 workingStorageArea = new WorkingStorageAreaFnc02090();
	//Original name: IVORYH
	private IvoryhFnc02090 ivoryh = new IvoryhFnc02090();

	//==== METHODS ====
	public void setSsIo(short ssIo) {
		this.ssIo = ssIo;
	}

	public short getSsIo() {
		return this.ssIo;
	}

	public boolean isSsIoMax() {
		return ssIo == SS_IO_MAX;
	}

	public ConstantFieldsFnc02090 getConstantFields() {
		return constantFields;
	}

	public ErrorAndAdviceMessagesFnc02090 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public IvoryhFnc02090 getIvoryh() {
		return ivoryh;
	}

	public SaCicsApplidFnc02090 getSaCicsApplid() {
		return saCicsApplid;
	}

	public SaTarSysFnc02090 getSaTarSys() {
		return saTarSys;
	}

	public Ts571cb1 getTs571cb1() {
		return ts571cb1;
	}

	public DfhcommareaTs571098 getUidLkuLinkage() {
		return uidLkuLinkage;
	}

	public WorkingStorageAreaFnc02090 getWorkingStorageArea() {
		return workingStorageArea;
	}
}
