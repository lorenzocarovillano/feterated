/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-PHONE-NBR-CODE<br>
 * Variable: CF-PHONE-NBR-CODE from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfPhoneNbrCode {

	//==== PROPERTIES ====
	//Original name: CF-PN-BUS
	private String bus = "BS";
	//Original name: CF-PN-FAX-1
	private String fax1 = "FX";
	//Original name: CF-PN-FAX-2
	private String fax2 = "FY";
	//Original name: CF-PN-CTC
	private String ctc = "CP";

	//==== METHODS ====
	public String getBus() {
		return this.bus;
	}

	public String getFax1() {
		return this.fax1;
	}

	public String getFax2() {
		return this.fax2;
	}

	public String getCtc() {
		return this.ctc;
	}
}
