/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0U8000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageArea {

	//==== PROPERTIES ====
	//Original name: WS-DAYS-DIFFERENCE
	private int daysDifference = DefaultValues.BIN_INT_VAL;
	//Original name: WS-DAYS-DIFFERENCE-TXT
	private String daysDifferenceTxt = DefaultValues.stringVal(Len.DAYS_DIFFERENCE_TXT);
	//Original name: WS-MIN-POL-EFF-DT
	private String minPolEffDt = DefaultValues.stringVal(Len.MIN_POL_EFF_DT);
	//Original name: WS-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: WS-PRI-RSK-ST-ABB
	private String priRskStAbb = DefaultValues.stringVal(Len.PRI_RSK_ST_ABB);
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0U8000";

	//==== METHODS ====
	public void setDaysDifference(int daysDifference) {
		this.daysDifference = daysDifference;
	}

	public int getDaysDifference() {
		return this.daysDifference;
	}

	public void setDaysDifferenceTxt(long daysDifferenceTxt) {
		this.daysDifferenceTxt = PicFormatter.display("Z(8)9-").format(daysDifferenceTxt).toString();
	}

	public long getDaysDifferenceTxt() {
		return PicParser.display("Z(8)9-").parseLong(this.daysDifferenceTxt);
	}

	public String getDaysDifferenceTxtFormatted() {
		return this.daysDifferenceTxt;
	}

	public String getDaysDifferenceTxtAsString() {
		return getDaysDifferenceTxtFormatted();
	}

	public void setMinPolEffDt(String minPolEffDt) {
		this.minPolEffDt = Functions.subString(minPolEffDt, Len.MIN_POL_EFF_DT);
	}

	public String getMinPolEffDt() {
		return this.minPolEffDt;
	}

	public String getMinPolEffDtFormatted() {
		return Functions.padBlanks(getMinPolEffDt(), Len.MIN_POL_EFF_DT);
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPriRskStAbb(String priRskStAbb) {
		this.priRskStAbb = Functions.subString(priRskStAbb, Len.PRI_RSK_ST_ABB);
	}

	public String getPriRskStAbb() {
		return this.priRskStAbb;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DAYS_DIFFERENCE_TXT = 10;
		public static final int EC_MODULE = 8;
		public static final int EC_PARAGRAPH = 30;
		public static final int MIN_POL_EFF_DT = 10;
		public static final int POL_NBR = 25;
		public static final int PRI_RSK_ST_ABB = 2;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
