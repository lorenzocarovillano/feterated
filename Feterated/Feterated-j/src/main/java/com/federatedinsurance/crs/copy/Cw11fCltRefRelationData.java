/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Cw11fMoreRowsSw;

/**Original name: CW11F-CLT-REF-RELATION-DATA<br>
 * Variable: CW11F-CLT-REF-RELATION-DATA from copybook CAWLF011<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw11fCltRefRelationData {

	//==== PROPERTIES ====
	/**Original name: CW11F-CIRF-REF-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char cirfRefIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW11F-CIRF-REF-ID
	private String cirfRefId = DefaultValues.stringVal(Len.CIRF_REF_ID);
	//Original name: CW11F-REF-TYP-CD-CI
	private char refTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW11F-REF-TYP-CD
	private String refTypCd = DefaultValues.stringVal(Len.REF_TYP_CD);
	//Original name: CW11F-CIRF-EXP-DT-CI
	private char cirfExpDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW11F-CIRF-EXP-DT
	private String cirfExpDt = DefaultValues.stringVal(Len.CIRF_EXP_DT);
	//Original name: CW11F-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW11F-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW11F-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW11F-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW11F-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW11F-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW11F-CIRF-EFF-ACY-TS-CI
	private char cirfEffAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW11F-CIRF-EFF-ACY-TS
	private String cirfEffAcyTs = DefaultValues.stringVal(Len.CIRF_EFF_ACY_TS);
	//Original name: CW11F-CIRF-EXP-ACY-TS-CI
	private char cirfExpAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW11F-CIRF-EXP-ACY-TS
	private String cirfExpAcyTs = DefaultValues.stringVal(Len.CIRF_EXP_ACY_TS);
	/**Original name: CW11F-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	private Cw11fMoreRowsSw moreRowsSw = new Cw11fMoreRowsSw();

	//==== METHODS ====
	public void setCltRefRelationDataBytes(byte[] buffer, int offset) {
		int position = offset;
		cirfRefIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cirfRefId = MarshalByte.readString(buffer, position, Len.CIRF_REF_ID);
		position += Len.CIRF_REF_ID;
		refTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		refTypCd = MarshalByte.readString(buffer, position, Len.REF_TYP_CD);
		position += Len.REF_TYP_CD;
		cirfExpDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cirfExpDt = MarshalByte.readString(buffer, position, Len.CIRF_EXP_DT);
		position += Len.CIRF_EXP_DT;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		cirfEffAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cirfEffAcyTs = MarshalByte.readString(buffer, position, Len.CIRF_EFF_ACY_TS);
		position += Len.CIRF_EFF_ACY_TS;
		cirfExpAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cirfExpAcyTs = MarshalByte.readString(buffer, position, Len.CIRF_EXP_ACY_TS);
		position += Len.CIRF_EXP_ACY_TS;
		moreRowsSw.setMoreRowsSw(MarshalByte.readChar(buffer, position));
	}

	public byte[] getCltRefRelationDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, cirfRefIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cirfRefId, Len.CIRF_REF_ID);
		position += Len.CIRF_REF_ID;
		MarshalByte.writeChar(buffer, position, refTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, refTypCd, Len.REF_TYP_CD);
		position += Len.REF_TYP_CD;
		MarshalByte.writeChar(buffer, position, cirfExpDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cirfExpDt, Len.CIRF_EXP_DT);
		position += Len.CIRF_EXP_DT;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, cirfEffAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cirfEffAcyTs, Len.CIRF_EFF_ACY_TS);
		position += Len.CIRF_EFF_ACY_TS;
		MarshalByte.writeChar(buffer, position, cirfExpAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cirfExpAcyTs, Len.CIRF_EXP_ACY_TS);
		position += Len.CIRF_EXP_ACY_TS;
		MarshalByte.writeChar(buffer, position, moreRowsSw.getMoreRowsSw());
		return buffer;
	}

	public void setCirfRefIdCi(char cirfRefIdCi) {
		this.cirfRefIdCi = cirfRefIdCi;
	}

	public char getCirfRefIdCi() {
		return this.cirfRefIdCi;
	}

	public void setCirfRefId(String cirfRefId) {
		this.cirfRefId = Functions.subString(cirfRefId, Len.CIRF_REF_ID);
	}

	public String getCirfRefId() {
		return this.cirfRefId;
	}

	public void setRefTypCdCi(char refTypCdCi) {
		this.refTypCdCi = refTypCdCi;
	}

	public char getRefTypCdCi() {
		return this.refTypCdCi;
	}

	public void setRefTypCd(String refTypCd) {
		this.refTypCd = Functions.subString(refTypCd, Len.REF_TYP_CD);
	}

	public String getRefTypCd() {
		return this.refTypCd;
	}

	public void setCirfExpDtCi(char cirfExpDtCi) {
		this.cirfExpDtCi = cirfExpDtCi;
	}

	public char getCirfExpDtCi() {
		return this.cirfExpDtCi;
	}

	public void setCirfExpDt(String cirfExpDt) {
		this.cirfExpDt = Functions.subString(cirfExpDt, Len.CIRF_EXP_DT);
	}

	public String getCirfExpDt() {
		return this.cirfExpDt;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setCirfEffAcyTsCi(char cirfEffAcyTsCi) {
		this.cirfEffAcyTsCi = cirfEffAcyTsCi;
	}

	public char getCirfEffAcyTsCi() {
		return this.cirfEffAcyTsCi;
	}

	public void setCirfEffAcyTs(String cirfEffAcyTs) {
		this.cirfEffAcyTs = Functions.subString(cirfEffAcyTs, Len.CIRF_EFF_ACY_TS);
	}

	public String getCirfEffAcyTs() {
		return this.cirfEffAcyTs;
	}

	public void setCirfExpAcyTsCi(char cirfExpAcyTsCi) {
		this.cirfExpAcyTsCi = cirfExpAcyTsCi;
	}

	public char getCirfExpAcyTsCi() {
		return this.cirfExpAcyTsCi;
	}

	public void setCirfExpAcyTs(String cirfExpAcyTs) {
		this.cirfExpAcyTs = Functions.subString(cirfExpAcyTs, Len.CIRF_EXP_ACY_TS);
	}

	public String getCirfExpAcyTs() {
		return this.cirfExpAcyTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CIRF_REF_ID = 30;
		public static final int REF_TYP_CD = 4;
		public static final int CIRF_EXP_DT = 10;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CIRF_EFF_ACY_TS = 26;
		public static final int CIRF_EXP_ACY_TS = 26;
		public static final int CIRF_REF_ID_CI = 1;
		public static final int REF_TYP_CD_CI = 1;
		public static final int CIRF_EXP_DT_CI = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int CIRF_EFF_ACY_TS_CI = 1;
		public static final int CIRF_EXP_ACY_TS_CI = 1;
		public static final int CLT_REF_RELATION_DATA = CIRF_REF_ID_CI + CIRF_REF_ID + REF_TYP_CD_CI + REF_TYP_CD + CIRF_EXP_DT_CI + CIRF_EXP_DT
				+ USER_ID_CI + USER_ID + STATUS_CD_CI + STATUS_CD + TERMINAL_ID_CI + TERMINAL_ID + CIRF_EFF_ACY_TS_CI + CIRF_EFF_ACY_TS
				+ CIRF_EXP_ACY_TS_CI + CIRF_EXP_ACY_TS + Cw11fMoreRowsSw.Len.MORE_ROWS_SW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
