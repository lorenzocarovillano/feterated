/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZA981-TTY-CERT-LIST<br>
 * Variable: XZA981-TTY-CERT-LIST from copybook XZ0A9081<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xza981TtyCertList {

	//==== PROPERTIES ====
	//Original name: XZA981-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: XZA981-ADR-ID
	private String adrId = DefaultValues.stringVal(Len.ADR_ID);
	//Original name: XZA981-REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);
	//Original name: XZA981-REC-TYP-DES
	private String recTypDes = DefaultValues.stringVal(Len.REC_TYP_DES);
	//Original name: XZA981-NAME
	private String name = DefaultValues.stringVal(Len.NAME);
	//Original name: XZA981-ADR-LIN1
	private String adrLin1 = DefaultValues.stringVal(Len.ADR_LIN1);
	//Original name: XZA981-ADR-LIN2
	private String adrLin2 = DefaultValues.stringVal(Len.ADR_LIN2);
	//Original name: XZA981-CITY-NM
	private String cityNm = DefaultValues.stringVal(Len.CITY_NM);
	//Original name: XZA981-STATE-ABB
	private String stateAbb = DefaultValues.stringVal(Len.STATE_ABB);
	//Original name: XZA981-PST-CD
	private String pstCd = DefaultValues.stringVal(Len.PST_CD);
	//Original name: XZA981-CER-NBR
	private String cerNbr = DefaultValues.stringVal(Len.CER_NBR);

	//==== METHODS ====
	public void setTtyCertListBytes(byte[] buffer, int offset) {
		int position = offset;
		clientId = MarshalByte.readString(buffer, position, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		adrId = MarshalByte.readString(buffer, position, Len.ADR_ID);
		position += Len.ADR_ID;
		recTypCd = MarshalByte.readString(buffer, position, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		recTypDes = MarshalByte.readString(buffer, position, Len.REC_TYP_DES);
		position += Len.REC_TYP_DES;
		name = MarshalByte.readString(buffer, position, Len.NAME);
		position += Len.NAME;
		adrLin1 = MarshalByte.readString(buffer, position, Len.ADR_LIN1);
		position += Len.ADR_LIN1;
		adrLin2 = MarshalByte.readString(buffer, position, Len.ADR_LIN2);
		position += Len.ADR_LIN2;
		cityNm = MarshalByte.readString(buffer, position, Len.CITY_NM);
		position += Len.CITY_NM;
		stateAbb = MarshalByte.readString(buffer, position, Len.STATE_ABB);
		position += Len.STATE_ABB;
		pstCd = MarshalByte.readString(buffer, position, Len.PST_CD);
		position += Len.PST_CD;
		cerNbr = MarshalByte.readString(buffer, position, Len.CER_NBR);
	}

	public byte[] getTtyCertListBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeString(buffer, position, adrId, Len.ADR_ID);
		position += Len.ADR_ID;
		MarshalByte.writeString(buffer, position, recTypCd, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		MarshalByte.writeString(buffer, position, recTypDes, Len.REC_TYP_DES);
		position += Len.REC_TYP_DES;
		MarshalByte.writeString(buffer, position, name, Len.NAME);
		position += Len.NAME;
		MarshalByte.writeString(buffer, position, adrLin1, Len.ADR_LIN1);
		position += Len.ADR_LIN1;
		MarshalByte.writeString(buffer, position, adrLin2, Len.ADR_LIN2);
		position += Len.ADR_LIN2;
		MarshalByte.writeString(buffer, position, cityNm, Len.CITY_NM);
		position += Len.CITY_NM;
		MarshalByte.writeString(buffer, position, stateAbb, Len.STATE_ABB);
		position += Len.STATE_ABB;
		MarshalByte.writeString(buffer, position, pstCd, Len.PST_CD);
		position += Len.PST_CD;
		MarshalByte.writeString(buffer, position, cerNbr, Len.CER_NBR);
		return buffer;
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setAdrId(String adrId) {
		this.adrId = Functions.subString(adrId, Len.ADR_ID);
	}

	public String getAdrId() {
		return this.adrId;
	}

	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	public String getRecTypCd() {
		return this.recTypCd;
	}

	public void setRecTypDes(String recTypDes) {
		this.recTypDes = Functions.subString(recTypDes, Len.REC_TYP_DES);
	}

	public String getRecTypDes() {
		return this.recTypDes;
	}

	public void setName(String name) {
		this.name = Functions.subString(name, Len.NAME);
	}

	public void setNameSubstring(String replacement, int start, int length) {
		name = Functions.setSubstring(name, replacement, start, length);
	}

	public String getName() {
		return this.name;
	}

	public String getNameFormatted() {
		return Functions.padBlanks(getName(), Len.NAME);
	}

	public void setAdrLin1(String adrLin1) {
		this.adrLin1 = Functions.subString(adrLin1, Len.ADR_LIN1);
	}

	public String getAdrLin1() {
		return this.adrLin1;
	}

	public void setAdrLin2(String adrLin2) {
		this.adrLin2 = Functions.subString(adrLin2, Len.ADR_LIN2);
	}

	public String getAdrLin2() {
		return this.adrLin2;
	}

	public void setCityNm(String cityNm) {
		this.cityNm = Functions.subString(cityNm, Len.CITY_NM);
	}

	public String getCityNm() {
		return this.cityNm;
	}

	public void setStateAbb(String stateAbb) {
		this.stateAbb = Functions.subString(stateAbb, Len.STATE_ABB);
	}

	public String getStateAbb() {
		return this.stateAbb;
	}

	public void setPstCd(String pstCd) {
		this.pstCd = Functions.subString(pstCd, Len.PST_CD);
	}

	public String getPstCd() {
		return this.pstCd;
	}

	public void setCerNbr(String cerNbr) {
		this.cerNbr = Functions.subString(cerNbr, Len.CER_NBR);
	}

	public String getCerNbr() {
		return this.cerNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ID = 64;
		public static final int ADR_ID = 64;
		public static final int REC_TYP_CD = 5;
		public static final int REC_TYP_DES = 13;
		public static final int NAME = 120;
		public static final int ADR_LIN1 = 45;
		public static final int ADR_LIN2 = 45;
		public static final int CITY_NM = 30;
		public static final int STATE_ABB = 2;
		public static final int PST_CD = 13;
		public static final int CER_NBR = 25;
		public static final int TTY_CERT_LIST = CLIENT_ID + ADR_ID + REC_TYP_CD + REC_TYP_DES + NAME + ADR_LIN1 + ADR_LIN2 + CITY_NM + STATE_ABB
				+ PST_CD + CER_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
