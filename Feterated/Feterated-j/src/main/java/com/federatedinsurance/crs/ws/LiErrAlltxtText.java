/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LI-ERR-ALLTXT-TEXT<br>
 * Variable: LI-ERR-ALLTXT-TEXT from program HALRPLAC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class LiErrAlltxtText extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LI-ERR-ALLTXT-TEXT
	private String liErrAlltxtText = "";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.LI_ERR_ALLTXT_TEXT;
	}

	@Override
	public void deserialize(byte[] buf) {
		setLiErrAlltxtTextFromBuffer(buf);
	}

	public void setLiErrAlltxtText(String liErrAlltxtText) {
		this.liErrAlltxtText = Functions.subString(liErrAlltxtText, Len.LI_ERR_ALLTXT_TEXT);
	}

	public void setLiErrAlltxtTextFromBuffer(byte[] buffer, int offset) {
		setLiErrAlltxtText(MarshalByte.readString(buffer, offset, Len.LI_ERR_ALLTXT_TEXT));
	}

	public void setLiErrAlltxtTextFromBuffer(byte[] buffer) {
		setLiErrAlltxtTextFromBuffer(buffer, 1);
	}

	public String getLiErrAlltxtText() {
		return this.liErrAlltxtText;
	}

	public String getLiErrAlltxtTextFormatted() {
		return Functions.padBlanks(getLiErrAlltxtText(), Len.LI_ERR_ALLTXT_TEXT);
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.strToBuffer(getLiErrAlltxtText(), Len.LI_ERR_ALLTXT_TEXT);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LI_ERR_ALLTXT_TEXT = 500;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int LI_ERR_ALLTXT_TEXT = 500;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int LI_ERR_ALLTXT_TEXT = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
