/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-PARM1-ADDRESS<br>
 * Variable: L-PARM1-ADDRESS from program TS030099<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LParm1Address extends BytesClass {

	//==== CONSTRUCTORS ====
	public LParm1Address() {
	}

	public LParm1Address(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_PARM1_ADDRESS;
	}

	public void setlParm1Address(int lParm1Address) {
		writeBinaryInt(Pos.L_PARM1_ADDRESS, lParm1Address);
	}

	/**Original name: L-PARM1-ADDRESS<br>
	 * <pre>    WE WILL RECEIVE THE ADDRESS OF THE PARAMETERS IN L-PARM1
	 *     AND L-PARM2.  IF THE SECOND PARAMETER IS PASSED, WE'LL
	 *     MOVE IT TO A SAVE-AREA.  IF NOT, WE'LL SET THE SAVE-AREA
	 *     TO A DEFAULT SIZE OF 132.</pre>*/
	public int getlParm1Address() {
		return readBinaryInt(Pos.L_PARM1_ADDRESS);
	}

	public int getlParm1Pointer() {
		return readBinaryInt(Pos.L_PARM1_POINTER);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_PARM1_ADDRESS = 1;
		public static final int L_PARM1_POINTER = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int L_PARM1_ADDRESS = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
