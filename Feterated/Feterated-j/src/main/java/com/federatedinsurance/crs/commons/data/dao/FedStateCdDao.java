/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IFedStateCd;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [FED_STATE_CD]
 * 
 */
public class FedStateCdDao extends BaseSqlDao<IFedStateCd> {

	public FedStateCdDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IFedStateCd> getToClass() {
		return IFedStateCd.class;
	}

	public String selectByStCd(String stCd, String dft) {
		return buildQuery("selectByStCd").bind("stCd", stCd).scalarResultString(dft);
	}
}
