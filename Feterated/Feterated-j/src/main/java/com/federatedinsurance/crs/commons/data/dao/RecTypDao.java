/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IRecTyp;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [REC_TYP]
 * 
 */
public class RecTypDao extends BaseSqlDao<IRecTyp> {

	private final IRowMapper<IRecTyp> selectRec1Rm = buildNamedRowMapper(IRecTyp.class, "shtDes", "lngDes", "srOrdNbr");

	public RecTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IRecTyp> getToClass() {
		return IRecTyp.class;
	}

	public String selectByRecTypCd(String recTypCd, String dft) {
		return buildQuery("selectByRecTypCd").bind("recTypCd", recTypCd).scalarResultString(dft);
	}

	public String selectByRecTypCd1(String recTypCd, String dft) {
		return buildQuery("selectByRecTypCd1").bind("recTypCd", recTypCd).scalarResultString(dft);
	}

	public short selectRec(String xzh003RecTypCd, char cfYes, short dft) {
		return buildQuery("selectRec").bind("xzh003RecTypCd", xzh003RecTypCd).bind("cfYes", String.valueOf(cfYes)).scalarResultShort(dft);
	}

	public IRecTyp selectRec1(String recTypCd, char cfYes, IRecTyp iRecTyp) {
		return buildQuery("selectRec1").bind("recTypCd", recTypCd).bind("cfYes", String.valueOf(cfYes)).rowMapper(selectRec1Rm).singleResult(iRecTyp);
	}
}
