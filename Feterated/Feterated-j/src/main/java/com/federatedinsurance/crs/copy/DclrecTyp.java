/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IRecTyp;

/**Original name: DCLREC-TYP<br>
 * Variable: DCLREC-TYP from copybook XZH00020<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclrecTyp implements IRecTyp {

	//==== PROPERTIES ====
	//Original name: REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);
	//Original name: REC-SHT-DES
	private String recShtDes = DefaultValues.stringVal(Len.REC_SHT_DES);
	//Original name: REC-LNG-DES
	private String recLngDes = DefaultValues.stringVal(Len.REC_LNG_DES);
	//Original name: REC-SR-ORD-NBR
	private short recSrOrdNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	public String getRecTypCd() {
		return this.recTypCd;
	}

	public String getRecTypCdFormatted() {
		return Functions.padBlanks(getRecTypCd(), Len.REC_TYP_CD);
	}

	public void setRecShtDes(String recShtDes) {
		this.recShtDes = Functions.subString(recShtDes, Len.REC_SHT_DES);
	}

	public String getRecShtDes() {
		return this.recShtDes;
	}

	public void setRecLngDes(String recLngDes) {
		this.recLngDes = Functions.subString(recLngDes, Len.REC_LNG_DES);
	}

	public String getRecLngDes() {
		return this.recLngDes;
	}

	public void setRecSrOrdNbr(short recSrOrdNbr) {
		this.recSrOrdNbr = recSrOrdNbr;
	}

	public short getRecSrOrdNbr() {
		return this.recSrOrdNbr;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	@Override
	public String getLngDes() {
		return getRecLngDes();
	}

	@Override
	public void setLngDes(String lngDes) {
		this.setRecLngDes(lngDes);
	}

	@Override
	public String getShtDes() {
		return getRecShtDes();
	}

	@Override
	public void setShtDes(String shtDes) {
		this.setRecShtDes(shtDes);
	}

	@Override
	public short getSrOrdNbr() {
		return getRecSrOrdNbr();
	}

	@Override
	public void setSrOrdNbr(short srOrdNbr) {
		this.setRecSrOrdNbr(srOrdNbr);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_SHT_DES = 13;
		public static final int REC_LNG_DES = 30;
		public static final int REC_TYP_CD = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
