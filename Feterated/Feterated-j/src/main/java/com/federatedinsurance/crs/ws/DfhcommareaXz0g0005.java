/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Xzt005ServiceOutputs;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.enums.Xzt005BypassSyncpointInd;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program XZ0G0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaXz0g0005 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZT05I-CSR-ACT-NBR
	private String xzt05iCsrActNbr = DefaultValues.stringVal(Len.XZT05I_CSR_ACT_NBR);
	//Original name: XZT05I-NOT-PRC-TS
	private String xzt05iNotPrcTs = DefaultValues.stringVal(Len.XZT05I_NOT_PRC_TS);
	//Original name: XZT05I-USERID
	private String xzt05iUserid = DefaultValues.stringVal(Len.XZT05I_USERID);
	//Original name: FILLER-DFHCOMMAREA-XZT005-SERVICE-INPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: XZT005-SERVICE-OUTPUTS
	private Xzt005ServiceOutputs xzt005ServiceOutputs = new Xzt005ServiceOutputs();
	//Original name: XZT005-OPERATION
	private String xzt005Operation = DefaultValues.stringVal(Len.XZT005_OPERATION);
	public static final String XZT005_GET_ACT_NOT = "GetAccountNotificationDtlByActTS";
	//Original name: XZT005-BYPASS-SYNCPOINT-IND
	private Xzt005BypassSyncpointInd xzt005BypassSyncpointInd = new Xzt005BypassSyncpointInd();
	//Original name: XZT005-ERROR-RETURN-CODE
	private DsdErrorReturnCode xzt005ErrorReturnCode = new DsdErrorReturnCode();
	//Original name: XZT005-ERROR-MESSAGE
	private String xzt005ErrorMessage = DefaultValues.stringVal(Len.XZT005_ERROR_MESSAGE);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		setXzt005ServiceInputsBytes(buffer, position);
		position += Len.XZT005_SERVICE_INPUTS;
		xzt005ServiceOutputs.setXzt005ServiceOutputsBytes(buffer, position);
		position += Xzt005ServiceOutputs.Len.XZT005_SERVICE_OUTPUTS;
		setXzt005ServiceParametersBytes(buffer, position);
		position += Len.XZT005_SERVICE_PARAMETERS;
		setXzt005ServiceErrorInfoBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		getXzt005ServiceInputsBytes(buffer, position);
		position += Len.XZT005_SERVICE_INPUTS;
		xzt005ServiceOutputs.getXzt005ServiceOutputsBytes(buffer, position);
		position += Xzt005ServiceOutputs.Len.XZT005_SERVICE_OUTPUTS;
		getXzt005ServiceParametersBytes(buffer, position);
		position += Len.XZT005_SERVICE_PARAMETERS;
		getXzt005ServiceErrorInfoBytes(buffer, position);
		return buffer;
	}

	/**Original name: XZT005-SERVICE-INPUTS<br>
	 * <pre>*************************************************************
	 *  XZ0Z0005 - SERVICE CONTRACT COPYBOOK FOR UOW               *
	 *             XZ_GET_ACT_NOT                                  *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  17MAR2009 E404GCL    NEW                          *
	 *                                                             *
	 * *************************************************************
	 * ** BRING IN COPY OF SERVICE CONTRACT
	 * **  NOTE: THE MAX COPYBOOK SIZE ALLOWED IS 32,767 BYTES MINUS
	 * **        THE SIZE OF THE FIELDS DEFINED BELOW (287 BYTES)
	 * **  COPY XZ0T0005.
	 * *************************************************************
	 *  XZ0T0005 - SERVICE CONTRACT COPYBOOK FOR OPERATION         *
	 *             XZ_GET_ACT_NOT                                  *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#       DATE      PRGRMR    DESCRIPTION                  *
	 *  --------  --------- --------- -----------------------------*
	 *  TO07614   30SEP2008 E404GCL   NEW                          *
	 *  TO07614   04FEB2009 E404DLP   ADDED ADD-CNC-DAY FIELD      *
	 *  TO07614   09MAR2009 E404DLP   CHANGE ADR-SEQNBR TO ADR-ID  *
	 *  TO07614   16MAR2009 E404KXS   CHANGE PRT_CPL_TS TO         *
	 *                                STA_MDF_TS AND UPDATED PDC_NM*
	 *  TO0760222 31MAR2010 E404KXS   ADDED FORMS ATTACHED IND     *
	 *  PP02500   09AUG2012 E404BPO   CHANGED CER-TRM-CD TO        *
	 *                                CER-HLD-NOT-IND              *
	 *  20163.20  13JUL2018 E404DMW   UPDATED ADDRESS AND CLIENT   *
	 *                                IDS FROM 20 TO 64            *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getXzt005ServiceInputsBytes() {
		byte[] buffer = new byte[Len.XZT005_SERVICE_INPUTS];
		return getXzt005ServiceInputsBytes(buffer, 1);
	}

	public void setXzt005ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt05iCsrActNbr = MarshalByte.readString(buffer, position, Len.XZT05I_CSR_ACT_NBR);
		position += Len.XZT05I_CSR_ACT_NBR;
		xzt05iNotPrcTs = MarshalByte.readString(buffer, position, Len.XZT05I_NOT_PRC_TS);
		position += Len.XZT05I_NOT_PRC_TS;
		xzt05iUserid = MarshalByte.readString(buffer, position, Len.XZT05I_USERID);
		position += Len.XZT05I_USERID;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzt005ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt05iCsrActNbr, Len.XZT05I_CSR_ACT_NBR);
		position += Len.XZT05I_CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, xzt05iNotPrcTs, Len.XZT05I_NOT_PRC_TS);
		position += Len.XZT05I_NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, xzt05iUserid, Len.XZT05I_USERID);
		position += Len.XZT05I_USERID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setXzt05iCsrActNbr(String xzt05iCsrActNbr) {
		this.xzt05iCsrActNbr = Functions.subString(xzt05iCsrActNbr, Len.XZT05I_CSR_ACT_NBR);
	}

	public String getXzt05iCsrActNbr() {
		return this.xzt05iCsrActNbr;
	}

	public void setXzt05iNotPrcTs(String xzt05iNotPrcTs) {
		this.xzt05iNotPrcTs = Functions.subString(xzt05iNotPrcTs, Len.XZT05I_NOT_PRC_TS);
	}

	public String getXzt05iNotPrcTs() {
		return this.xzt05iNotPrcTs;
	}

	public void setXzt05iUserid(String xzt05iUserid) {
		this.xzt05iUserid = Functions.subString(xzt05iUserid, Len.XZT05I_USERID);
	}

	public String getXzt05iUserid() {
		return this.xzt05iUserid;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setXzt005ServiceParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt005Operation = MarshalByte.readString(buffer, position, Len.XZT005_OPERATION);
		position += Len.XZT005_OPERATION;
		xzt005BypassSyncpointInd.setXzt005BypassSyncpointInd(MarshalByte.readChar(buffer, position));
	}

	public byte[] getXzt005ServiceParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt005Operation, Len.XZT005_OPERATION);
		position += Len.XZT005_OPERATION;
		MarshalByte.writeChar(buffer, position, xzt005BypassSyncpointInd.getXzt005BypassSyncpointInd());
		return buffer;
	}

	public void setXzt005Operation(String xzt005Operation) {
		this.xzt005Operation = Functions.subString(xzt005Operation, Len.XZT005_OPERATION);
	}

	public String getXzt005Operation() {
		return this.xzt005Operation;
	}

	public void setXzt005ServiceErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt005ErrorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		xzt005ErrorMessage = MarshalByte.readString(buffer, position, Len.XZT005_ERROR_MESSAGE);
	}

	public byte[] getXzt005ServiceErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt005ErrorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, xzt005ErrorMessage, Len.XZT005_ERROR_MESSAGE);
		return buffer;
	}

	public void setXzt005ErrorMessage(String xzt005ErrorMessage) {
		this.xzt005ErrorMessage = Functions.subString(xzt005ErrorMessage, Len.XZT005_ERROR_MESSAGE);
	}

	public String getXzt005ErrorMessage() {
		return this.xzt005ErrorMessage;
	}

	public Xzt005BypassSyncpointInd getXzt005BypassSyncpointInd() {
		return xzt005BypassSyncpointInd;
	}

	public DsdErrorReturnCode getXzt005ErrorReturnCode() {
		return xzt005ErrorReturnCode;
	}

	public Xzt005ServiceOutputs getXzt005ServiceOutputs() {
		return xzt005ServiceOutputs;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT05I_CSR_ACT_NBR = 9;
		public static final int XZT05I_NOT_PRC_TS = 26;
		public static final int XZT05I_USERID = 8;
		public static final int FLR1 = 100;
		public static final int XZT005_SERVICE_INPUTS = XZT05I_CSR_ACT_NBR + XZT05I_NOT_PRC_TS + XZT05I_USERID + FLR1;
		public static final int XZT005_OPERATION = 32;
		public static final int XZT005_SERVICE_PARAMETERS = XZT005_OPERATION + Xzt005BypassSyncpointInd.Len.XZT005_BYPASS_SYNCPOINT_IND;
		public static final int XZT005_ERROR_MESSAGE = 250;
		public static final int XZT005_SERVICE_ERROR_INFO = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + XZT005_ERROR_MESSAGE;
		public static final int DFHCOMMAREA = XZT005_SERVICE_INPUTS + Xzt005ServiceOutputs.Len.XZT005_SERVICE_OUTPUTS + XZT005_SERVICE_PARAMETERS
				+ XZT005_SERVICE_ERROR_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
