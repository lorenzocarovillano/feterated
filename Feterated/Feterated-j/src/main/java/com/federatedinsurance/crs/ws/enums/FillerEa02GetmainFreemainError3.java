/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FILLER-EA-02-GETMAIN-FREEMAIN-ERROR-3<br>
 * Variable: FILLER-EA-02-GETMAIN-FREEMAIN-ERROR-3 from program XZ0G0005<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FillerEa02GetmainFreemainError3 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.FLR4);
	public static final String GETMAIN = "ALLOCATING";
	public static final String FREEMAIN = "RELEASING";

	//==== METHODS ====
	public void setFlr4(String flr4) {
		this.value = Functions.subString(flr4, Len.FLR4);
	}

	public String getFlr4() {
		return this.value;
	}

	public void setGetmain() {
		value = GETMAIN;
	}

	public void setFreemain() {
		value = FREEMAIN;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR4 = 11;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
