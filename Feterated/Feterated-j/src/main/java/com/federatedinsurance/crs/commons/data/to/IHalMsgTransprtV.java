/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HAL_MSG_TRANSPRT_V]
 * 
 */
public interface IHalMsgTransprtV extends BaseSqlTo {

	/**
	 * Host Variable HMTC-UOW-NM
	 * 
	 */
	String getHmtcUowNm();

	void setHmtcUowNm(String hmtcUowNm);

	/**
	 * Host Variable HMTC-MSG-DEL-TM
	 * 
	 */
	int getMsgDelTm();

	void setMsgDelTm(int msgDelTm);

	/**
	 * Host Variable HMTC-WNG-DEL-TM
	 * 
	 */
	int getWngDelTm();

	void setWngDelTm(int wngDelTm);

	/**
	 * Host Variable HMTC-PASS-LNK-IND
	 * 
	 */
	char getPassLnkInd();

	void setPassLnkInd(char passLnkInd);

	/**
	 * Host Variable HMTC-STG-TYP-CD
	 * 
	 */
	char getStgTypCd();

	void setStgTypCd(char stgTypCd);

	/**
	 * Host Variable HMTC-MDRV-REQ-STG
	 * 
	 */
	String getMdrvReqStg();

	void setMdrvReqStg(String mdrvReqStg);

	/**
	 * Host Variable HMTC-MDRV-RSP-STG
	 * 
	 */
	String getMdrvRspStg();

	void setMdrvRspStg(String mdrvRspStg);

	/**
	 * Host Variable HMTC-UOW-REQ-STG
	 * 
	 */
	String getUowReqStg();

	void setUowReqStg(String uowReqStg);

	/**
	 * Host Variable HMTC-UOW-SWI-STG
	 * 
	 */
	String getUowSwiStg();

	void setUowSwiStg(String uowSwiStg);

	/**
	 * Host Variable HMTC-UOW-HDR-STG
	 * 
	 */
	String getUowHdrStg();

	void setUowHdrStg(String uowHdrStg);

	/**
	 * Host Variable HMTC-UOW-DTA-STG
	 * 
	 */
	String getUowDtaStg();

	void setUowDtaStg(String uowDtaStg);

	/**
	 * Host Variable HMTC-UOW-WNG-STG
	 * 
	 */
	String getUowWngStg();

	void setUowWngStg(String uowWngStg);

	/**
	 * Host Variable HMTC-UOW-KRP-STG
	 * 
	 */
	String getUowKrpStg();

	void setUowKrpStg(String uowKrpStg);

	/**
	 * Host Variable HMTC-UOW-NLBE-STG
	 * 
	 */
	String getUowNlbeStg();

	void setUowNlbeStg(String uowNlbeStg);
};
