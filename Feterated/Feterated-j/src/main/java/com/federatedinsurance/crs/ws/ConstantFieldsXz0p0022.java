/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P0022<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p0022 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-POL-NOT
	private short maxPolNot = ((short) 50);
	//Original name: CF-MAX-ACT-POL
	private short maxActPol = ((short) 150);
	//Original name: CF-NO-FEE
	private short noFee = new AfDecimal("0.00", 3, 2).toShort();
	//Original name: CF-NO
	private char no = 'N';
	//Original name: CF-TMN-FLG
	private String tmnFlg = "UTM";
	//Original name: CF-ACT-NOT-STATUS-STARTED
	private String actNotStatusStarted = "10";
	//Original name: CF-TMN-CPL-STA-CD
	private String tmnCplStaCd = "70";
	//Original name: CF-BUS-OBJ-XZ-SET-TMN-FLG-BPO
	private String busObjXzSetTmnFlgBpo = "XZ_SET_TMN_FLG_BPO";
	//Original name: CF-SERVICE-PROXY
	private CfServiceProxy serviceProxy = new CfServiceProxy();
	//Original name: CF-PL-PRODUCER-NBR
	private String plProducerNbr = "4-000";
	//Original name: FILLER-CF-PL-PRODUCER-NM
	private String flr1 = "PERSONAL";
	//Original name: FILLER-CF-PL-PRODUCER-NM-1
	private String flr2 = "SERVICE";
	//Original name: FILLER-CF-PL-PRODUCER-NM-2
	private String flr3 = "REPRESENTATIVE";
	//Original name: FILLER-CF-PL-PRODUCER-NM-3
	private String flr4 = "";
	//Original name: CF-ATC-COMMERCIAL-LINES
	private String atcCommercialLines = "CL";
	//Original name: CF-ATC-PERSONAL-LINES
	private String atcPersonalLines = "PL";

	//==== METHODS ====
	public short getMaxPolNot() {
		return this.maxPolNot;
	}

	public short getMaxActPol() {
		return this.maxActPol;
	}

	public short getNoFee() {
		return this.noFee;
	}

	public char getNo() {
		return this.no;
	}

	public String getTmnFlg() {
		return this.tmnFlg;
	}

	public String getActNotStatusStarted() {
		return this.actNotStatusStarted;
	}

	public String getTmnCplStaCd() {
		return this.tmnCplStaCd;
	}

	public String getBusObjXzSetTmnFlgBpo() {
		return this.busObjXzSetTmnFlgBpo;
	}

	public String getPlProducerNbr() {
		return this.plProducerNbr;
	}

	public String getPlProducerNmFormatted() {
		return MarshalByteExt.bufferToStr(getPlProducerNmBytes());
	}

	/**Original name: CF-PL-PRODUCER-NM<br>*/
	public byte[] getPlProducerNmBytes() {
		byte[] buffer = new byte[Len.PL_PRODUCER_NM];
		return getPlProducerNmBytes(buffer, 1);
	}

	public byte[] getPlProducerNmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getAtcCommercialLines() {
		return this.atcCommercialLines;
	}

	public String getAtcPersonalLines() {
		return this.atcPersonalLines;
	}

	public CfServiceProxy getServiceProxy() {
		return serviceProxy;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 9;
		public static final int FLR2 = 8;
		public static final int FLR3 = 14;
		public static final int FLR4 = 89;
		public static final int PL_PRODUCER_NM = FLR1 + FLR2 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
