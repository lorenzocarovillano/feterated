/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalUowObjHierV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_UOW_OBJ_HIER_V]
 * 
 */
public class HalUowObjHierVDao extends BaseSqlDao<IHalUowObjHierV> {

	private Cursor boHierCurs0;
	private final IRowMapper<IHalUowObjHierV> fetchBoHierCurs0Rm = buildNamedRowMapper(IHalUowObjHierV.class, "hierSeqNbr", "pntBobjNm", "chdBobjNm");

	public HalUowObjHierVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalUowObjHierV> getToClass() {
		return IHalUowObjHierV.class;
	}

	public DbAccessStatus openBoHierCurs0(String uowNm, String rootBobjNm, String pntBobjNm) {
		boHierCurs0 = buildQuery("openBoHierCurs0").bind("uowNm", uowNm).bind("rootBobjNm", rootBobjNm).bind("pntBobjNm", pntBobjNm).open();
		return dbStatus;
	}

	public IHalUowObjHierV fetchBoHierCurs0(IHalUowObjHierV iHalUowObjHierV) {
		return fetch(boHierCurs0, iHalUowObjHierV, fetchBoHierCurs0Rm);
	}

	public DbAccessStatus closeBoHierCurs0() {
		return closeCursor(boHierCurs0);
	}
}
