/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.copy.Xza972AcyTagInfo;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9072-ROW<br>
 * Variable: WS-XZ0A9072-ROW from program XZ0B9072<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9072Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA972-MAX-TAG-ROWS
	private short maxTagRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA972-ACY-TAG-INFO
	private Xza972AcyTagInfo acyTagInfo = new Xza972AcyTagInfo();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9072_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9072RowBytes(buf);
	}

	public String getWsXz0a9072RowFormatted() {
		return getAcyTagRowFormatted();
	}

	public void setWsXz0a9072RowBytes(byte[] buffer) {
		setWsXz0a9072RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9072RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9072_ROW];
		return getWsXz0a9072RowBytes(buffer, 1);
	}

	public void setWsXz0a9072RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setAcyTagRowBytes(buffer, position);
	}

	public byte[] getWsXz0a9072RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getAcyTagRowBytes(buffer, position);
		return buffer;
	}

	public String getAcyTagRowFormatted() {
		return MarshalByteExt.bufferToStr(getAcyTagRowBytes());
	}

	/**Original name: XZA972-ACY-TAG-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0A9072 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_ACY_TAG_LIST                       *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  24MAR2009 E404DAP    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getAcyTagRowBytes() {
		byte[] buffer = new byte[Len.ACY_TAG_ROW];
		return getAcyTagRowBytes(buffer, 1);
	}

	public void setAcyTagRowBytes(byte[] buffer, int offset) {
		int position = offset;
		maxTagRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		acyTagInfo.setAcyTagInfoBytes(buffer, position);
	}

	public byte[] getAcyTagRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxTagRows);
		position += Types.SHORT_SIZE;
		acyTagInfo.getAcyTagInfoBytes(buffer, position);
		return buffer;
	}

	public void setMaxTagRows(short maxTagRows) {
		this.maxTagRows = maxTagRows;
	}

	public short getMaxTagRows() {
		return this.maxTagRows;
	}

	public Xza972AcyTagInfo getAcyTagInfo() {
		return acyTagInfo;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9072RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_TAG_ROWS = 2;
		public static final int ACY_TAG_ROW = MAX_TAG_ROWS + Xza972AcyTagInfo.Len.ACY_TAG_INFO;
		public static final int WS_XZ0A9072_ROW = ACY_TAG_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
