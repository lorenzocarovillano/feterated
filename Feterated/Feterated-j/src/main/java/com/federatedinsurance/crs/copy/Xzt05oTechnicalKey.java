/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XZT05O-TECHNICAL-KEY<br>
 * Variable: XZT05O-TECHNICAL-KEY from copybook XZ0Z0005<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzt05oTechnicalKey {

	//==== PROPERTIES ====
	//Original name: XZT05O-TK-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZT05O-TK-ACT-OWN-CLT-ID
	private String actOwnCltId = DefaultValues.stringVal(Len.ACT_OWN_CLT_ID);
	//Original name: XZT05O-TK-ACT-OWN-ADR-ID
	private String actOwnAdrId = DefaultValues.stringVal(Len.ACT_OWN_ADR_ID);
	//Original name: XZT05O-TK-EMP-ID
	private String empId = DefaultValues.stringVal(Len.EMP_ID);
	//Original name: XZT05O-TK-STA-MDF-TS
	private String staMdfTs = DefaultValues.stringVal(Len.STA_MDF_TS);
	//Original name: XZT05O-TK-ACT-NOT-STA-CD
	private String actNotStaCd = DefaultValues.stringVal(Len.ACT_NOT_STA_CD);
	//Original name: XZT05O-TK-SEG-CD
	private String segCd = DefaultValues.stringVal(Len.SEG_CD);
	//Original name: XZT05O-TK-ACT-TYP-CD
	private String actTypCd = DefaultValues.stringVal(Len.ACT_TYP_CD);
	//Original name: XZT05O-TK-ACT-NOT-CSUM
	private String actNotCsum = DefaultValues.stringVal(Len.ACT_NOT_CSUM);

	//==== METHODS ====
	public void setTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		actOwnCltId = MarshalByte.readString(buffer, position, Len.ACT_OWN_CLT_ID);
		position += Len.ACT_OWN_CLT_ID;
		actOwnAdrId = MarshalByte.readString(buffer, position, Len.ACT_OWN_ADR_ID);
		position += Len.ACT_OWN_ADR_ID;
		empId = MarshalByte.readString(buffer, position, Len.EMP_ID);
		position += Len.EMP_ID;
		staMdfTs = MarshalByte.readString(buffer, position, Len.STA_MDF_TS);
		position += Len.STA_MDF_TS;
		actNotStaCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_STA_CD);
		position += Len.ACT_NOT_STA_CD;
		segCd = MarshalByte.readString(buffer, position, Len.SEG_CD);
		position += Len.SEG_CD;
		actTypCd = MarshalByte.readString(buffer, position, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		actNotCsum = MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_CSUM);
	}

	public byte[] getTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, actOwnCltId, Len.ACT_OWN_CLT_ID);
		position += Len.ACT_OWN_CLT_ID;
		MarshalByte.writeString(buffer, position, actOwnAdrId, Len.ACT_OWN_ADR_ID);
		position += Len.ACT_OWN_ADR_ID;
		MarshalByte.writeString(buffer, position, empId, Len.EMP_ID);
		position += Len.EMP_ID;
		MarshalByte.writeString(buffer, position, staMdfTs, Len.STA_MDF_TS);
		position += Len.STA_MDF_TS;
		MarshalByte.writeString(buffer, position, actNotStaCd, Len.ACT_NOT_STA_CD);
		position += Len.ACT_NOT_STA_CD;
		MarshalByte.writeString(buffer, position, segCd, Len.SEG_CD);
		position += Len.SEG_CD;
		MarshalByte.writeString(buffer, position, actTypCd, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		MarshalByte.writeString(buffer, position, actNotCsum, Len.ACT_NOT_CSUM);
		return buffer;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public void setActOwnCltId(String actOwnCltId) {
		this.actOwnCltId = Functions.subString(actOwnCltId, Len.ACT_OWN_CLT_ID);
	}

	public String getActOwnCltId() {
		return this.actOwnCltId;
	}

	public void setActOwnAdrId(String actOwnAdrId) {
		this.actOwnAdrId = Functions.subString(actOwnAdrId, Len.ACT_OWN_ADR_ID);
	}

	public String getActOwnAdrId() {
		return this.actOwnAdrId;
	}

	public void setEmpId(String empId) {
		this.empId = Functions.subString(empId, Len.EMP_ID);
	}

	public String getEmpId() {
		return this.empId;
	}

	public void setStaMdfTs(String staMdfTs) {
		this.staMdfTs = Functions.subString(staMdfTs, Len.STA_MDF_TS);
	}

	public String getStaMdfTs() {
		return this.staMdfTs;
	}

	public void setActNotStaCd(String actNotStaCd) {
		this.actNotStaCd = Functions.subString(actNotStaCd, Len.ACT_NOT_STA_CD);
	}

	public String getActNotStaCd() {
		return this.actNotStaCd;
	}

	public void setSegCd(String segCd) {
		this.segCd = Functions.subString(segCd, Len.SEG_CD);
	}

	public String getSegCd() {
		return this.segCd;
	}

	public void setActTypCd(String actTypCd) {
		this.actTypCd = Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	public String getActTypCd() {
		return this.actTypCd;
	}

	public void setActNotCsumFormatted(String actNotCsum) {
		this.actNotCsum = Trunc.toUnsignedNumeric(actNotCsum, Len.ACT_NOT_CSUM);
	}

	public int getActNotCsum() {
		return NumericDisplay.asInt(this.actNotCsum);
	}

	public String getActNotCsumFormatted() {
		return this.actNotCsum;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NOT_PRC_TS = 26;
		public static final int ACT_OWN_CLT_ID = 64;
		public static final int ACT_OWN_ADR_ID = 64;
		public static final int EMP_ID = 6;
		public static final int STA_MDF_TS = 26;
		public static final int ACT_NOT_STA_CD = 2;
		public static final int SEG_CD = 3;
		public static final int ACT_TYP_CD = 2;
		public static final int ACT_NOT_CSUM = 9;
		public static final int TECHNICAL_KEY = NOT_PRC_TS + ACT_OWN_CLT_ID + ACT_OWN_ADR_ID + EMP_ID + STA_MDF_TS + ACT_NOT_STA_CD + SEG_CD
				+ ACT_TYP_CD + ACT_NOT_CSUM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
