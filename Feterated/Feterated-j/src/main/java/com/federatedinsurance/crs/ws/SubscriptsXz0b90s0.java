/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program XZ0B90S0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsXz0b90s0 {

	//==== PROPERTIES ====
	/**Original name: SS-LP<br>
	 * <pre> LOCATE POLICY INDEX</pre>*/
	private short lp = ((short) 1);
	public static final short LP_MAX = ((short) 350);
	/**Original name: SS-MSG<br>
	 * <pre> NLBE MESSAGE.</pre>*/
	private short msg = DefaultValues.BIN_SHORT_VAL;
	public static final short MSG_MAX = ((short) 10);
	/**Original name: SS-WNG<br>
	 * <pre> WARNING MESSAGE.</pre>*/
	private short wng = DefaultValues.BIN_SHORT_VAL;
	public static final short WNG_MAX = ((short) 10);

	//==== METHODS ====
	public void setLp(short lp) {
		this.lp = lp;
	}

	public short getLp() {
		return this.lp;
	}

	public boolean isLpMax() {
		return lp == LP_MAX;
	}

	public void setMsg(short msg) {
		this.msg = msg;
	}

	public short getMsg() {
		return this.msg;
	}

	public boolean isMsgMax() {
		return msg == MSG_MAX;
	}

	public void setWng(short wng) {
		this.wng = wng;
	}

	public short getWng() {
		return this.wng;
	}

	public boolean isWngMax() {
		return wng == WNG_MAX;
	}
}
