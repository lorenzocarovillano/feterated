/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: L-OD-ERR-IND<br>
 * Variable: L-OD-ERR-IND from copybook TT008001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LOdErrInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char ERR = 'Y';
	public static final char NO_ERR = 'N';

	//==== METHODS ====
	public char getOdErrInd() {
		return this.value;
	}

	public boolean isErr() {
		return value == ERR;
	}
}
