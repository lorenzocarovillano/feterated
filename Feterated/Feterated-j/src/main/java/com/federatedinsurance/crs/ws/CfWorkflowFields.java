/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CF-WORKFLOW-FIELDS<br>
 * Variable: CF-WORKFLOW-FIELDS from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfWorkflowFields {

	//==== PROPERTIES ====
	//Original name: FILLER-CF-WF-ACCOUNT-NUMBER
	private String flr1 = "accountNumber";
	//Original name: FILLER-CF-WF-CL-CANCEL-DATE
	private String flr2 = "cancellation";
	//Original name: FILLER-CF-WF-CL-CANCEL-DATE-1
	private String flr3 = "Date";
	//Original name: FILLER-CF-WF-CL-NONPAY-CANCEL
	private String flr4 = "PPNPC@Non Pay";
	//Original name: FILLER-CF-WF-CL-NONPAY-CANCEL-1
	private String flr5 = "Cancellation";
	//Original name: FILLER-CF-WF-OWN-CD
	private String flr6 = "CRS";
	//Original name: FILLER-CF-WF-POL-EFF-DATE
	private String flr7 = "policyEffective";
	//Original name: FILLER-CF-WF-POL-EFF-DATE-1
	private String flr8 = "Date";
	//Original name: FILLER-CF-WF-POLICY-NUMBER
	private String flr9 = "policyNumber";
	//Original name: FILLER-CF-WF-POLICY-NUMBER-LIST
	private String flr10 = "policyNumbers";
	//Original name: CF-WF-PL-CANCEL-DATE
	private CfWfPlCancelDate plCancelDate = new CfWfPlCancelDate();
	//Original name: FILLER-CF-WF-PL-NONPAY-CANCEL
	private String flr11 = "PCN@Cancel";
	//Original name: FILLER-CF-WF-PL-NONPAY-CANCEL-1
	private String flr12 = "NonPay";

	//==== METHODS ====
	public String getAccountNumberFormatted() {
		return getFlr1Formatted();
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr1Formatted() {
		return Functions.padBlanks(getFlr1(), Len.FLR1);
	}

	public String getClCancelDateFormatted() {
		return MarshalByteExt.bufferToStr(getClCancelDateBytes());
	}

	/**Original name: CF-WF-CL-CANCEL-DATE<br>*/
	public byte[] getClCancelDateBytes() {
		byte[] buffer = new byte[Len.CL_CANCEL_DATE];
		return getClCancelDateBytes(buffer, 1);
	}

	public byte[] getClCancelDateBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getClNonpayCancelFormatted() {
		return MarshalByteExt.bufferToStr(getClNonpayCancelBytes());
	}

	/**Original name: CF-WF-CL-NONPAY-CANCEL<br>*/
	public byte[] getClNonpayCancelBytes() {
		byte[] buffer = new byte[Len.CL_NONPAY_CANCEL];
		return getClNonpayCancelBytes(buffer, 1);
	}

	public byte[] getClNonpayCancelBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getOwnCdFormatted() {
		return getFlr6Formatted();
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr6Formatted() {
		return Functions.padBlanks(getFlr6(), Len.FLR6);
	}

	public String getPolEffDateFormatted() {
		return MarshalByteExt.bufferToStr(getPolEffDateBytes());
	}

	/**Original name: CF-WF-POL-EFF-DATE<br>*/
	public byte[] getPolEffDateBytes() {
		byte[] buffer = new byte[Len.POL_EFF_DATE];
		return getPolEffDateBytes(buffer, 1);
	}

	public byte[] getPolEffDateBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR3);
		return buffer;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public String getPolicyNumberFormatted() {
		return getFlr9Formatted();
	}

	public String getFlr9() {
		return this.flr9;
	}

	public String getFlr9Formatted() {
		return Functions.padBlanks(getFlr9(), Len.FLR5);
	}

	public String getPolicyNumberListFormatted() {
		return getFlr10Formatted();
	}

	public String getFlr10() {
		return this.flr10;
	}

	public String getFlr10Formatted() {
		return Functions.padBlanks(getFlr10(), Len.FLR1);
	}

	public String getPlNonpayCancelFormatted() {
		return MarshalByteExt.bufferToStr(getPlNonpayCancelBytes());
	}

	/**Original name: CF-WF-PL-NONPAY-CANCEL<br>*/
	public byte[] getPlNonpayCancelBytes() {
		byte[] buffer = new byte[Len.PL_NONPAY_CANCEL];
		return getPlNonpayCancelBytes(buffer, 1);
	}

	public byte[] getPlNonpayCancelBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr11, Len.FLR11);
		position += Len.FLR11;
		MarshalByte.writeString(buffer, position, flr12, Len.FLR12);
		return buffer;
	}

	public String getFlr11() {
		return this.flr11;
	}

	public String getFlr12() {
		return this.flr12;
	}

	public CfWfPlCancelDate getPlCancelDate() {
		return plCancelDate;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR6 = 3;
		public static final int FLR4 = 14;
		public static final int FLR5 = 12;
		public static final int CL_NONPAY_CANCEL = FLR4 + FLR5;
		public static final int FLR1 = 13;
		public static final int FLR3 = 4;
		public static final int CL_CANCEL_DATE = FLR3 + FLR5;
		public static final int FLR11 = 11;
		public static final int FLR12 = 6;
		public static final int PL_NONPAY_CANCEL = FLR11 + FLR12;
		public static final int FLR7 = 15;
		public static final int POL_EFF_DATE = FLR3 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
