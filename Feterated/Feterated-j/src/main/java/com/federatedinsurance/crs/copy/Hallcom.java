/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.Types;
import com.federatedinsurance.crs.ws.enums.ComOutgoingColIndVal;

/**Original name: HALLCOM<br>
 * Variable: HALLCOM from copybook HALLCOM<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Hallcom {

	//==== PROPERTIES ====
	//Original name: COM-CICS-COM-LENGTH
	private int cicsComLength = 32767;
	//Original name: COM-SEC-SYS-ID
	private int secSysId = 4;
	/**Original name: COM-COL-IS-NULL<br>
	 * <pre>* SEGMENT LAYOUT NULL INDICATOR VALUES:</pre>*/
	private char colIsNull = 'Y';
	//Original name: COM-COL-IS-NOT-NULL
	private char colIsNotNull = 'N';
	/**Original name: COM-RETAIN-VAL-COL-IND<br>
	 * <pre>* SEGMENT COLUMN INDICATOR VALUES (INCOMING):</pre>*/
	private char retainValColInd = 'R';
	//Original name: COM-USE-SUP-VAL-COL-IND
	private char useSupValColInd = Types.SPACE_CHAR;
	/**Original name: COM-IS-PRIM-KEY-COL-IND<br>
	 * <pre>* SEGMENT COLUMN INDICATOR VALUES (INCOMING & OUTGOING):</pre>*/
	private char isPrimKeyColInd = 'K';
	/**Original name: COM-OUTGOING-COL-IND-VAL<br>
	 * <pre>* SEGMENT COLUMN INDICATOR VALUES (OUTGOING):
	 * * THESE FLAGS ALLOW THE FRONT-END USER TO CHANGE THE
	 * * COLUMN DATA.</pre>*/
	private ComOutgoingColIndVal outgoingColIndVal = new ComOutgoingColIndVal();

	//==== METHODS ====
	public int getCicsComLength() {
		return this.cicsComLength;
	}

	public int getSecSysId() {
		return this.secSysId;
	}

	public char getColIsNull() {
		return this.colIsNull;
	}

	public char getColIsNotNull() {
		return this.colIsNotNull;
	}

	public char getRetainValColInd() {
		return this.retainValColInd;
	}

	public char getUseSupValColInd() {
		return this.useSupValColInd;
	}

	public char getIsPrimKeyColInd() {
		return this.isPrimKeyColInd;
	}

	public ComOutgoingColIndVal getOutgoingColIndVal() {
		return outgoingColIndVal;
	}
}
