/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.WsOperationName;
import com.federatedinsurance.crs.ws.redefines.WsCaPtr;

/**Original name: WS-MISC-WORK-FLDS<br>
 * Variable: WS-MISC-WORK-FLDS from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsMiscWorkFldsMu0x0004 {

	//==== PROPERTIES ====
	//Original name: WS-RESPONSE-CODE
	private int wsResponseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int wsResponseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-SERVICE-CONTRACT-ATB
	private WsServiceContractAtb wsServiceContractAtb = new WsServiceContractAtb();
	//Original name: WS-PROXY-CONTRACT-ATB
	private WsServiceContractAtb wsProxyContractAtb = new WsServiceContractAtb();
	//Original name: WS-CA-PTR
	private WsCaPtr wsCaPtr = new WsCaPtr();
	//Original name: WS-EIBRESP-DISPLAY
	private String wsEibrespDisplay = DefaultValues.stringVal(Len.WS_EIBRESP_DISPLAY);
	//Original name: WS-EIBRESP2-DISPLAY
	private String wsEibresp2Display = DefaultValues.stringVal(Len.WS_EIBRESP2_DISPLAY);
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "MU0X0004";
	//Original name: WS-OPERATION-NAME
	private WsOperationName wsOperationName = new WsOperationName();

	//==== METHODS ====
	public void setWsResponseCode(int wsResponseCode) {
		this.wsResponseCode = wsResponseCode;
	}

	public int getWsResponseCode() {
		return this.wsResponseCode;
	}

	public void setWsResponseCode2(int wsResponseCode2) {
		this.wsResponseCode2 = wsResponseCode2;
	}

	public int getWsResponseCode2() {
		return this.wsResponseCode2;
	}

	public void setWsEibrespDisplay(long wsEibrespDisplay) {
		this.wsEibrespDisplay = PicFormatter.display("-Z(8)9").format(wsEibrespDisplay).toString();
	}

	public long getWsEibrespDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.wsEibrespDisplay);
	}

	public String getWsEibrespDisplayFormatted() {
		return this.wsEibrespDisplay;
	}

	public String getWsEibrespDisplayAsString() {
		return getWsEibrespDisplayFormatted();
	}

	public void setWsEibresp2Display(long wsEibresp2Display) {
		this.wsEibresp2Display = PicFormatter.display("-Z(8)9").format(wsEibresp2Display).toString();
	}

	public long getWsEibresp2Display() {
		return PicParser.display("-Z(8)9").parseLong(this.wsEibresp2Display);
	}

	public String getWsEibresp2DisplayFormatted() {
		return this.wsEibresp2Display;
	}

	public String getWsEibresp2DisplayAsString() {
		return getWsEibresp2DisplayFormatted();
	}

	public void setWsProgramName(String wsProgramName) {
		this.wsProgramName = Functions.subString(wsProgramName, Len.WS_PROGRAM_NAME);
	}

	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public WsCaPtr getWsCaPtr() {
		return wsCaPtr;
	}

	public WsOperationName getWsOperationName() {
		return wsOperationName;
	}

	public WsServiceContractAtb getWsProxyContractAtb() {
		return wsProxyContractAtb;
	}

	public WsServiceContractAtb getWsServiceContractAtb() {
		return wsServiceContractAtb;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_EIBRESP_DISPLAY = 10;
		public static final int WS_EIBRESP2_DISPLAY = 10;
		public static final int WS_PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
