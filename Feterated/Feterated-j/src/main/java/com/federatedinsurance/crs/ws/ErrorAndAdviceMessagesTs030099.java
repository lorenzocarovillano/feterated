/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesTs030099 {

	//==== PROPERTIES ====
	//Original name: EA-01-PRTR-REPORT-MESSAGE
	private Ea01PrtrReportMessage ea01PrtrReportMessage = new Ea01PrtrReportMessage();
	//Original name: EA-02-REPORT-NOT-ON-FILE
	private Ea02ReportNotOnFile ea02ReportNotOnFile = new Ea02ReportNotOnFile();
	//Original name: EA-03-DBMS-ERROR
	private Ea03DbmsError ea03DbmsError = new Ea03DbmsError();
	//Original name: EA-04-DB2-NOT-ACTIVE
	private Ea04Db2NotActive ea04Db2NotActive = new Ea04Db2NotActive();
	//Original name: EA-05-COMPILE-PIC
	private Ea05CompilePic ea05CompilePic = new Ea05CompilePic();
	//Original name: EA-06-OPEN-MESSAGE
	private Ea06OpenMessage ea06OpenMessage = new Ea06OpenMessage();
	//Original name: EA-07-INVLD-RPT-OFFICE-DST-MSG
	private Ea07InvldRptOfficeDstMsg ea07InvldRptOfficeDstMsg = new Ea07InvldRptOfficeDstMsg();
	//Original name: EA-08-INACTIVE-REPORT-MSG
	private Ea08InactiveReportMsg ea08InactiveReportMsg = new Ea08InactiveReportMsg();
	//Original name: EA-09-CONTACT-CLS-MSG
	private Ea09ContactClsMsg ea09ContactClsMsg = new Ea09ContactClsMsg();
	//Original name: EA-10-INVALID-LENGTH-MSG
	private Ea10InvalidLengthMsg ea10InvalidLengthMsg = new Ea10InvalidLengthMsg();
	//Original name: EA-11-COM-FILE-TRUNCATION-MSG
	private Ea11ComFileTruncationMsg ea11ComFileTruncationMsg = new Ea11ComFileTruncationMsg();
	//Original name: EA-12-INQUIRY-MSG
	private Ea12InquiryMsg ea12InquiryMsg = new Ea12InquiryMsg();
	//Original name: EA-13-MULTIPLE-SIGN-ON-MSG
	private Ea13MultipleSignOnMsg ea13MultipleSignOnMsg = new Ea13MultipleSignOnMsg();
	//Original name: EA-14-ADD-RPT-NBR-MSG
	private Ea14AddRptNbrMsg ea14AddRptNbrMsg = new Ea14AddRptNbrMsg();

	//==== METHODS ====
	public Ea01PrtrReportMessage getEa01PrtrReportMessage() {
		return ea01PrtrReportMessage;
	}

	public Ea02ReportNotOnFile getEa02ReportNotOnFile() {
		return ea02ReportNotOnFile;
	}

	public Ea03DbmsError getEa03DbmsError() {
		return ea03DbmsError;
	}

	public Ea04Db2NotActive getEa04Db2NotActive() {
		return ea04Db2NotActive;
	}

	public Ea05CompilePic getEa05CompilePic() {
		return ea05CompilePic;
	}

	public Ea06OpenMessage getEa06OpenMessage() {
		return ea06OpenMessage;
	}

	public Ea07InvldRptOfficeDstMsg getEa07InvldRptOfficeDstMsg() {
		return ea07InvldRptOfficeDstMsg;
	}

	public Ea08InactiveReportMsg getEa08InactiveReportMsg() {
		return ea08InactiveReportMsg;
	}

	public Ea09ContactClsMsg getEa09ContactClsMsg() {
		return ea09ContactClsMsg;
	}

	public Ea10InvalidLengthMsg getEa10InvalidLengthMsg() {
		return ea10InvalidLengthMsg;
	}

	public Ea11ComFileTruncationMsg getEa11ComFileTruncationMsg() {
		return ea11ComFileTruncationMsg;
	}

	public Ea12InquiryMsg getEa12InquiryMsg() {
		return ea12InquiryMsg;
	}

	public Ea13MultipleSignOnMsg getEa13MultipleSignOnMsg() {
		return ea13MultipleSignOnMsg;
	}

	public Ea14AddRptNbrMsg getEa14AddRptNbrMsg() {
		return ea14AddRptNbrMsg;
	}
}
