/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRecTyp;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [ACT_NOT_FRM_REC, ACT_NOT_REC, REC_TYP]
 * 
 */
public class ActNotFrmRecTypDao extends BaseSqlDao<IActNotFrmRecTyp> {

	private Cursor recipientByFrmCsr;
	private final IRowMapper<IActNotFrmRecTyp> fetchRecipientByFrmCsrRm = buildNamedRowMapper(IActNotFrmRecTyp.class, "recSeqNbr", "recTypCd",
			"recNmObj", "lin1AdrObj", "lin2AdrObj", "citNmObj", "stAbbObj", "pstCdObj", "cerNbrObj", "recLngDes", "recSrOrdNbr");

	public ActNotFrmRecTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotFrmRecTyp> getToClass() {
		return IActNotFrmRecTyp.class;
	}

	public DbAccessStatus openRecipientByFrmCsr(String csrActNbr, String notPrcTs, short frmSeqNbr, short recSeqNbr) {
		recipientByFrmCsr = buildQuery("openRecipientByFrmCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr)
				.bind("recSeqNbr", recSeqNbr).open();
		return dbStatus;
	}

	public IActNotFrmRecTyp fetchRecipientByFrmCsr(IActNotFrmRecTyp iActNotFrmRecTyp) {
		return fetch(recipientByFrmCsr, iActNotFrmRecTyp, fetchRecipientByFrmCsrRm);
	}

	public DbAccessStatus closeRecipientByFrmCsr() {
		return closeCursor(recipientByFrmCsr);
	}
}
