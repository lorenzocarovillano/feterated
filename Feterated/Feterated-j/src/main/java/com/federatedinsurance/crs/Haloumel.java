/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TpSession;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalBoMduXrfVDao;
import com.federatedinsurance.crs.commons.data.dao.HalUowPrcSeqVDao;
import com.federatedinsurance.crs.copy.MdrvDriverInfo;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.DfhcommareaHaluidb;
import com.federatedinsurance.crs.ws.HaloumelData;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: HALOUMEL<br>
 * <pre>AUTHOR.  PMSC.
 * DATE-WRITTEN. JUNE 2000.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  MAIN DRIVER ERROR LOGGING INTERFACE MODULE **
 * *                                                             **
 * * PLATFORM - I-BASE                                           **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -                                                   **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED IN THE FOLLOW-**
 * *                       ING WAYS:                             **
 * *                                                             **
 * *                       1)LINK FROM THE MAIN DRIVER (HALOMDRV)**
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #       DATE     PROG    DESCRIPTION                     **
 * * --------  --------  ------  --------------------------------**
 * * SAVANNAH2 22JUN00   18448   NEW PROGRAM.
 * *
 * * C15776    14AUG2001 99361   REMOVE REFERENCES TO COLUMN
 * *                             BUS_OBJ_MDU_NM FROM THE TABLE
 * *                             HAL_UOW_PRC_SEQ_V. INSERTED CODE
 * *                             TO OBTAIN MODULE NAME VIA THE
 * *                             TABLE, HAL_BO_MDU_XRF, USING
 * *                             THE VALUE OBTAINED FROM COLUMN
 * *                             ROOT_BOBJ_NM.
 * * 15863     24AUG2001 18448   ACTIVATE CODE WHICH VALIDATES THE
 * *                             SESSION ID.  INCLUDED IN BUSINESS
 * *                             SERVICES INFRASTRUCTURE 2.2.
 * * 17241     13NOV01   03539   REPLACE REFERENCES TO IAP WITH
 * *                             COMPARABLE INFRASTRUCTURE CODE.
 * * 22246     23JUL02   18448   ADD FOR READ ONLY TO THE CURSOR.
 * * 26151     09SEP02   03539   FLOOD/EXTENDED ERROR PROCESSING
 * * 34504     09SEP03   18448   LOCKING FIX
 * ****************************************************************</pre>*/
public class Haloumel extends Program {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private HalUowPrcSeqVDao halUowPrcSeqVDao = new HalUowPrcSeqVDao(dbAccessStatus);
	private HalBoMduXrfVDao halBoMduXrfVDao = new HalBoMduXrfVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private HaloumelData ws = new HaloumelData();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaHaluidb dfhcommarea = new DfhcommareaHaluidb();

	//==== CONSTRUCTORS ====
	public Haloumel() {
		registerListeners();
	}

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaHaluidb dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		registerArgListeners();
		main1();
		exitModule();
		deleteArgListeners();
		return 0;
	}

	public static Haloumel getInstance() {
		return (Programs.getInstance(Haloumel.class));
	}

	/**Original name: 0000-MAIN<br>
	 * <pre>*****************************************************************
	 * * THE 0000-MAIN PARAGRAPH IS RESPONSIBLE FOR CONTROLLING THE   **
	 * * PROCESSING OF THE FUNCTION PASSED TO IT.                     **
	 * *****************************************************************</pre>*/
	private void main1() {
		TpOutputData parmData = null;
		// COB_CODE: INITIALIZE MDRV-DRIVER-INFO.
		initDriverInfo();
		// COB_CODE: INITIALIZE WS-ESTO-INFO.
		initWsEstoInfo();
		// COB_CODE: INITIALIZE EFAL-FAILURE-RECORD-INFO.
		initEfalFailureRecordInfo();
		// COB_CODE: MOVE 3 TO WS-NUM-ENTRIES.
		ws.getWsWorkFields().setNumEntries(((short) 3));
		//---*
		//   RETRIEVE LINKAGE FOR START REQUEST
		//---*
		// COB_CODE: EXEC CICS RETRIEVE
		//               INTO       (MDRV-DRIVER-INFO)
		//               LENGTH     (LENGTH OF MDRV-DRIVER-INFO)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		parmData = execContext.getParmData(MdrvDriverInfo.Len.DRIVER_INFO);
		if (parmData != null) {
			ws.getHallmdrv().getDriverInfo()
					.setDriverInfoBytes(MarshalByte.arrayCopyNoFill(parmData.getData(), ws.getHallmdrv().getDriverInfo().getDriverInfoBytes()));
		}
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO EXIT-MODULE
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: MOVE '0000-MAIN' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0000-MAIN");
			// COB_CODE: MOVE WS-PROGRAM-NAME
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsWorkFields().getProgramName());
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-RETRIEVE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsRetrieve();
			// COB_CODE: MOVE 'RETRIEVAL OF LINKAGE FROM START FAILED.'
			//              TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("RETRIEVAL OF LINKAGE FROM START FAILED.");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO EXIT-MODULE
			exitModule();
		}
		//* RESET COMMIT/ROLLBACK IND ETC FOR LATER USE
		// COB_CODE: SET MDRV-PERFORM-COMMIT TO TRUE.
		ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setCommit();
		// COB_CODE: SET MDRV-MAINDRVR-OK    TO TRUE.
		ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().setOk();
		// COB_CODE: SET MDRV-UOW-OK         TO TRUE.
		ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setOk();
		//* IF ERRORS OCCUR DURING VALIDATION, THE STORED ERRORS WILL
		//* BE RETAINED IN THE VSAM.  THE ALERT MODULE WILL STILL NEED
		//* TO BE INVOKED.  IN ORDER TO DO THIS, HALLUBOC MUST BE BUILT.
		// COB_CODE: PERFORM 0100-VALIDATION-AND-SETUP.
		validationAndSetup();
		// COB_CODE: PERFORM 0200-BUILD-UBOC-LINKAGE.
		buildUbocLinkage();
		// COB_CODE: IF MDRV-MAINDRVR-LOGGABLE-ERRS
		//               GO TO EXIT-MODULE
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().isLoggableErrs()) {
			// COB_CODE: GO TO EXIT-MODULE
			exitModule();
		}
		//* IF ERRORS OCCUR DURING PROCESSING, CLEANUP PROCESSING WILL
		//* PROCEED, AND THE ALERT MODULE WILL STILL BE INVOKED.
		// COB_CODE: PERFORM 0300-PROCESS-ERR-LOG-UOW.
		processErrLogUow();
		//*   PERFORM 0500-INVOKE-ALERT-MODULE.
		// COB_CODE: PERFORM 0400-INVOKE-ALERT-MODULE.
		invokeAlertModule();
		//*   PERFORM 0400-CLEANUP-ERROR-STORAGE.
		// COB_CODE: PERFORM 0500-CLEANUP-ERROR-STORAGE.
		cleanupErrorStorage();
	}

	/**Original name: EXIT-MODULE<br>
	 * <pre>*   PERFORM 0500-INVOKE-ALERT-MODULE.
	 *  GOBACK REQUIRED BY COMPILER</pre>*/
	private void exitModule() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 0100-VALIDATION-AND-SETUP_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  VALIDATE FIELDS OF THE MAINDRIVER COPYBOOK.  THIS COPYBOOK    *
	 *  DEFINED THE LINKAGE INTO THIS PROGRAM.                        *
	 *  ALSO, READ HAL_MSG_TRANSPRT_V USING PRESET UOW NAME TO FETCH  *
	 *  MESSAGE TRANSPORT INFO RELATED TO THIS ERROR LOGGING UOW.     *
	 * ****************************************************************
	 * * VALIDATE MDRV FIELDS.</pre>*/
	private void validationAndSetup() {
		// COB_CODE: IF (MDRV-AUTH-USERID EQUAL SPACES OR LOW-VALUES)
		//             OR (MDRV-MSG-ID EQUAL SPACES OR LOW-VALUES)
		//             OR (MDRV-SESSION-ID  EQUAL SPACES OR LOW-VALUES)
		//               SET EFAL-COMMAREA-FAILED TO TRUE
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getAuthUserid())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getAuthUserid(), MdrvDriverInfo.Len.AUTH_USERID)
				|| Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getMsgId())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getMsgId(), MdrvDriverInfo.Len.MSG_ID)
				|| Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getSessionId())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getSessionId(), MdrvDriverInfo.Len.SESSION_ID)) {
			// COB_CODE: MOVE '0100-VALIDATION-AND-SETUP' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-VALIDATION-AND-SETUP");
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
		}
		// COB_CODE: IF MDRV-MSG-ID EQUAL SPACES OR LOW-VALUES
		//               GO TO 0100-VALIDATION-AND-SETUP-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getMsgId())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getMsgId(), MdrvDriverInfo.Len.MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'HALLMDRV STORE ID IS EITHER SPACES OR LOW VALUES.'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("HALLMDRV STORE ID IS EITHER SPACES OR LOW VALUES.");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-VALIDATION-AND-SETUP-X
			return;
		}
		// COB_CODE: IF MDRV-AUTH-USERID EQUAL SPACES OR LOW-VALUES
		//               GO TO 0100-VALIDATION-AND-SETUP-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getAuthUserid())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getAuthUserid(), MdrvDriverInfo.Len.AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'HALLMDRV USERID IS EITHER SPACES OR LOW VALUES.'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("HALLMDRV USERID IS EITHER SPACES OR LOW VALUES.");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-VALIDATION-AND-SETUP-X
			return;
		}
		// COB_CODE: IF MDRV-SESSION-ID EQUAL SPACES OR LOW-VALUES
		//               GO TO 0100-VALIDATION-AND-SETUP-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getSessionId())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getSessionId(), MdrvDriverInfo.Len.SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'MAIN DRIVER SESSION ID WAS EMPTY. '
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MAIN DRIVER SESSION ID WAS EMPTY. ");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-VALIDATION-AND-SETUP-X
			return;
		}
	}

	/**Original name: 0200-BUILD-UBOC-LINKAGE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  BUILD THE HALLUBOC LINKAGE USED TO COMMUNICATE WITH THE ERROR *
	 *  LOGGING BDOS AND BPOS.                                        *
	 * ****************************************************************</pre>*/
	private void buildUbocLinkage() {
		// COB_CODE: INITIALIZE WS-UBOC-MSG.
		initWsUbocMsg();
		// COB_CODE: MOVE WS-SAV-HOST-ERR-LOG-UOW TO UBOC-UOW-NAME.
		ws.getWsUbocMsg().getCommInfo().setUbocUowName(ws.getWsWorkFields().getSavHostErrLogUow());
		// COB_CODE: MOVE MDRV-MSG-ID TO UBOC-MSG-ID.
		ws.getWsUbocMsg().getCommInfo().setUbocMsgId(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: MOVE MDRV-SESSION-ID TO UBOC-SESSION-ID.
		ws.getWsUbocMsg().getCommInfo().setUbocSessionId(ws.getHallmdrv().getDriverInfo().getSessionId());
		// COB_CODE: MOVE MDRV-AUTH-USERID TO UBOC-AUTH-USERID.
		ws.getWsUbocMsg().getCommInfo().setUbocAuthUserid(ws.getHallmdrv().getDriverInfo().getAuthUserid());
		// COB_CODE: MOVE WS-ERROR-LOGGING-CLT TO UBOC-AUTH-USER-CLIENTID.
		ws.getWsUbocMsg().getCommInfo().setUbocAuthUserClientid(ws.getWsWorkFields().getErrorLoggingClt());
		// COB_CODE: MOVE MDRV-TERMINAL-ID TO UBOC-TERMINAL-ID.
		ws.getWsUbocMsg().getCommInfo().setUbocTerminalId(ws.getHallmdrv().getDriverInfo().getTerminalId());
		// COB_CODE: MOVE MDRV-LOGGABLE-ERR-LOG-ONLY-SW
		//                                 TO UBOC-LOGGABLE-ERR-LOG-ONLY-SW.
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw()
				.setUbocLoggableErrLogOnlySw(ws.getHallmdrv().getDriverInfo().getLoggableErrLogOnlySw().getLoggableErrLogOnlySw());
		// COB_CODE: MOVE WS-MAINDRVR-ERR-MOD TO UBOC-PRIMARY-BUS-OBJ.
		ws.getWsUbocMsg().getCommInfo().setUbocPrimaryBusObj(ws.getWsWorkFields().getMaindrvrErrMod());
		// COB_CODE: SET UBOC-STORE-TYPE-VSAM TO TRUE.
		ws.getWsUbocMsg().getCommInfo().getUbocStoreTypeCd().setUbocStoreTypeVsam();
		// COB_CODE: MOVE WS-ERROR-STORAGE-VSAM TO UBOC-UOW-REQ-MSG-STORE.
		ws.getWsUbocMsg().getCommInfo().setUbocUowReqMsgStore(ws.getWsWorkFields().getErrorStorageVsam());
		// COB_CODE: MOVE 'NOT REQUIRED' TO UBOC-MDR-REQ-STORE
		//                                  UBOC-MDR-RSP-STORE
		//                                  UBOC-UOW-REQ-SWITCHES-STORE
		//                                  UBOC-UOW-RESP-HEADER-STORE
		//                                  UBOC-UOW-RESP-DATA-STORE
		//                                  UBOC-UOW-RESP-WARNINGS-STORE
		//                                  UBOC-UOW-KEY-REPLACE-STORE
		//                                  UBOC-UOW-RESP-NL-BL-ERRS-STORE.
		ws.getWsUbocMsg().getCommInfo().setUbocMdrReqStore("NOT REQUIRED");
		ws.getWsUbocMsg().getCommInfo().setUbocMdrRspStore("NOT REQUIRED");
		ws.getWsUbocMsg().getCommInfo().setUbocUowReqSwitchesStore("NOT REQUIRED");
		ws.getWsUbocMsg().getCommInfo().setUbocUowRespHeaderStore("NOT REQUIRED");
		ws.getWsUbocMsg().getCommInfo().setUbocUowRespDataStore("NOT REQUIRED");
		ws.getWsUbocMsg().getCommInfo().setUbocUowRespWarningsStore("NOT REQUIRED");
		ws.getWsUbocMsg().getCommInfo().setUbocUowKeyReplaceStore("NOT REQUIRED");
		ws.getWsUbocMsg().getCommInfo().setUbocUowRespNlBlErrsStore("NOT REQUIRED");
		// COB_CODE: SET INSERT-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE.
		ws.getWsUbocMsg().getCommInfo().getUbocPassThruAction().setInsertDataRequest();
		// COB_CODE: SET UBOC-UOW-NO-LOCK-NEEDED TO TRUE.
		ws.getWsUbocMsg().getCommInfo().getUbocUowLockStrategyCd().setUbocUowNoLockNeeded();
		// COB_CODE: SET UBOC-NO-DATA-PRIVACY TO TRUE.
		ws.getWsUbocMsg().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().setUbocNoDataPrivacy();
		// COB_CODE: SET UBOC-SUPER-USER TO TRUE.
		ws.getWsUbocMsg().getCommInfo().getUbocSecAndDataPrivInfo().getSecAutNbr().setUbocSuperUser();
		// COB_CODE: SET UBOC-NO-AUDITS TO TRUE.
		ws.getWsUbocMsg().getCommInfo().getUbocAuditProcessingInfo().getApplyAuditsSw().setUbocNoAudits();
		// COB_CODE: SET UBOC-CLEAR-UOW-STORAGE TO TRUE.
		ws.getWsUbocMsg().getCommInfo().getUbocKeepClearUowStorageSw().setUbocClearUowStorage();
		// COB_CODE: SET UBOC-UOW-OK TO TRUE.
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setUbocUowOk();
	}

	/**Original name: 0300-PROCESS-ERR-LOG-UOW_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  READ THE PROCESS SEQUENCE TABLE, RETRIEVE THE PRIMARY BUSINESS*
	 *  OBJECTS FOR THE UOW, AND CALL EACH IN THE PROPER SEQUENCE.    *
	 * ****************************************************************</pre>*/
	private void processErrLogUow() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE WS-SAV-HOST-ERR-LOG-UOW TO HUPS-UOW-NM.
		ws.getDclhalUowPrcSeq().setHupsUowNm(ws.getWsWorkFields().getSavHostErrLogUow());
		// COB_CODE:      EXEC SQL DECLARE PRIMARYBOCUR CURSOR FOR
		//                    SELECT UOW_NM
		//                         , UOW_SEQ_NBR
		//                         , ROOT_BOBJ_NM
		//           **            , BUS_OBJ_MDU_NM
		//                         , HUPS_BRN_PRC_CD
		//                      FROM HAL_UOW_PRC_SEQ_V
		//                      WHERE UOW_NM           = :HUPS-UOW-NM
		//                      ORDER BY UOW_SEQ_NBR
		//                      FOR READ ONLY
		//                END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
		// COB_CODE: EXEC SQL
		//               OPEN PRIMARYBOCUR
		//           END-EXEC.
		halUowPrcSeqVDao.openPrimarybocur(ws.getDclhalUowPrcSeq().getHupsUowNm());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 0300-PROCESS-ERR-LOG-UOW-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: STRING 'HUPS-UOW-NM      = '   HUPS-UOW-NM      ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HUPS-UOW-NM      = ",
					ws.getDclhalUowPrcSeq().getHupsUowNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE 'HAL_UOW_PRC_SEQ_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_PRC_SEQ_V");
			// COB_CODE: MOVE '0300-PROCESS-ERR-LOG-UOW' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0300-PROCESS-ERR-LOG-UOW");
			// COB_CODE: MOVE 'OPEN CURSOR FAILED - PRIMARYBOCUR'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CURSOR FAILED - PRIMARYBOCUR");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0300-PROCESS-ERR-LOG-UOW-X
			return;
		}
		// COB_CODE: SET FIRST-PRIMARY-BO TO TRUE.
		ws.getWsPrimaryBosSwitch().setFirstPrimaryBo();
		// COB_CODE: PERFORM 0305-RETRIEVE-PRIMARY-BOS
		//               UNTIL NO-MORE-PRIMARY-BOS
		//                 OR MDRV-UOW-LOGGABLE-ERRS.
		while (!(ws.getWsPrimaryBosSwitch().isNoMorePrimaryBos() || ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs())) {
			retrievePrimaryBos();
		}
		// COB_CODE: EXEC SQL
		//               CLOSE PRIMARYBOCUR
		//           END-EXEC.
		halUowPrcSeqVDao.closePrimarybocur();
		//* DO NOT INVOKE ROLLBACK IF CLOSE CURSOR FAILS!!!
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 0300-PROCESS-ERR-LOG-UOW-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-WARNS TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableWarns();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: STRING 'HUPS-UOW-NM      = '   HUPS-UOW-NM      ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HUPS-UOW-NM      = ",
					ws.getDclhalUowPrcSeq().getHupsUowNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE 'HAL_UOW_PRC_SEQ_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_PRC_SEQ_V");
			// COB_CODE: MOVE '0300-PROCESS-ERR-LOG-UOW' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0300-PROCESS-ERR-LOG-UOW");
			// COB_CODE: MOVE 'CLOSE CURSOR FAILED - PRIMARYBOCUR'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CURSOR FAILED - PRIMARYBOCUR");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0300-PROCESS-ERR-LOG-UOW-X
			return;
		}
	}

	/**Original name: 0305-RETRIEVE-PRIMARY-BOS_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  FETCH THE PRIMARY BUSINESS OBJECTS FOR THE CURRENT UOW. *
	 * **********************************************************</pre>*/
	private void retrievePrimaryBos() {
		ConcatUtil concatUtil = null;
		// COB_CODE:      EXEC SQL
		//                    FETCH PRIMARYBOCUR
		//                     INTO :HUPS-UOW-NM
		//                         ,:UOW-SEQ-NBR
		//                         ,:ROOT-BOBJ-NM
		//           **            ,:HUPS-BUS-OBJ-MDU-NM
		//                         ,:HUPS-BRN-PRC-CD
		//                END-EXEC.
		halUowPrcSeqVDao.fetchPrimarybocur(ws.getDclhalUowPrcSeq());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   END-IF
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0305-RETRIEVE-PRIMARY-BOS-X
		//               WHEN OTHER
		//                   GO TO 0305-RETRIEVE-PRIMARY-BOS-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET NEXT-PRIMARY-BO TO TRUE
			ws.getWsPrimaryBosSwitch().setNextPrimaryBo();
			// COB_CODE: MOVE DCLHAL-UOW-PRC-SEQ TO WS-UOW-PROCESS-SEQ-INFO
			ws.setWsUowProcessSeqInfoBytes(ws.getDclhalUowPrcSeq().getDclhalUowPrcSeqBytes());
			// COB_CODE: MOVE ROOT-BOBJ-NM TO WS-BUS-OBJ-NM
			ws.getWsWorkFields().setBusObjNm(ws.getDclhalUowPrcSeq().getRootBobjNm());
			// COB_CODE: PERFORM 0320-READ-BOBJ-MDU-XREF
			readBobjMduXref();
			// COB_CODE: IF NOT MDRV-UOW-LOGGABLE-ERRS
			//              PERFORM 0310-LINK-TO-PRIMARY-BOS
			//           END-IF
			if (!ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
				// COB_CODE: PERFORM 0310-LINK-TO-PRIMARY-BOS
				linkToPrimaryBos();
			}
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: IF FIRST-PRIMARY-BO
			//               PERFORM 9000-LOG-WARNING-OR-ERROR
			//           ELSE
			//               SET NO-MORE-PRIMARY-BOS TO TRUE
			//           END-IF
			if (ws.getWsPrimaryBosSwitch().isFirstPrimaryBo()) {
				// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS TO TRUE
				ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
				// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
				// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
				// COB_CODE: SET ETRA-DB2-FETCH-CSR TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
				// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
				// COB_CODE: STRING 'HUPS-UOW-NM      = '
				//                   HUPS-UOW-NM      ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HUPS-UOW-NM      = ",
						ws.getDclhalUowPrcSeq().getHupsUowNmFormatted(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: MOVE 'HAL_UOW_PRC_SEQ_V' TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_PRC_SEQ_V");
				// COB_CODE: MOVE '0305-RETRIEVE-PRIMARY-BOS'
				//             TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0305-RETRIEVE-PRIMARY-BOS");
				// COB_CODE: MOVE 'FETCH ROW NFND FRST TME - PRIMARYBOCUR'
				//             TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH ROW NFND FRST TME - PRIMARYBOCUR");
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
			} else {
				// COB_CODE: SET NO-MORE-PRIMARY-BOS TO TRUE
				ws.getWsPrimaryBosSwitch().setNoMorePrimaryBos();
			}
			// COB_CODE: GO TO 0305-RETRIEVE-PRIMARY-BOS-X
			return;

		default:// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: STRING 'HUPS-UOW-NM      = '   HUPS-UOW-NM      ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HUPS-UOW-NM      = ",
					ws.getDclhalUowPrcSeq().getHupsUowNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE 'HAL_UOW_PRC_SEQ_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_PRC_SEQ_V");
			// COB_CODE: MOVE '0305-RETRIEVE-PRIMARY-BOS'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0305-RETRIEVE-PRIMARY-BOS");
			// COB_CODE: MOVE 'FETCH CURSOR FAILED - PRIMARYBOCUR'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH CURSOR FAILED - PRIMARYBOCUR");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0305-RETRIEVE-PRIMARY-BOS-X
			return;
		}
	}

	/**Original name: 0310-LINK-TO-PRIMARY-BOS_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  LINK TO EACH PRIMARY BUSINESS OBJECT.                   *
	 * **********************************************************</pre>*/
	private void linkToPrimaryBos() {
		// COB_CODE: MOVE ROOT-BOBJ-NM     TO UBOC-PRIMARY-BUS-OBJ.
		ws.getWsUbocMsg().getCommInfo().setUbocPrimaryBusObj(ws.getDclhalUowPrcSeq().getRootBobjNm());
		// COB_CODE: MOVE SPACES           TO UBOC-APP-DATA-BUFFER.
		ws.getWsUbocMsg().setAppDataBuffer("");
		// COB_CODE: IF ROOT-BOBJ-NM = 'HAL_ERR_LOG_DTL_V'
		//                TO UBOC-UOW-REQ-MSG-STORE
		//           ELSE
		//                TO UBOC-UOW-REQ-MSG-STORE
		//           END-IF.
		if (Conditions.eq(ws.getDclhalUowPrcSeq().getRootBobjNm(), "HAL_ERR_LOG_DTL_V")) {
			// COB_CODE: MOVE WS-ERROR-STO-DTL-VSAM
			//             TO UBOC-UOW-REQ-MSG-STORE
			ws.getWsUbocMsg().getCommInfo().setUbocUowReqMsgStore(ws.getWsWorkFields().getErrorStoDtlVsam());
		} else {
			// COB_CODE: MOVE WS-ERROR-STORAGE-VSAM
			//             TO UBOC-UOW-REQ-MSG-STORE
			ws.getWsUbocMsg().getCommInfo().setUbocUowReqMsgStore(ws.getWsWorkFields().getErrorStorageVsam());
		}
		//        PROGRAM  (PST-OBJ-MODULE)
		// COB_CODE:      EXEC CICS LINK
		//           *        PROGRAM  (PST-OBJ-MODULE)
		//                    PROGRAM  (WS-BUS-OBJ-MDU)
		//                    COMMAREA (WS-UBOC-MSG)
		//                    LENGTH   (LENGTH OF WS-UBOC-MSG)
		//                    RESP     (WS-RESPONSE-CODE)
		//                    RESP2    (WS-RESPONSE-CODE2)
		//                END-EXEC.
		TpRunner.context("HALOUMEL", execContext).commarea(ws.getWsUbocMsg()).length(Dfhcommarea.Len.DFHCOMMAREA)
				.link(ws.getWsWorkFields().getBusObjMdu());
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0310-LINK-TO-PRIMARY-BOS-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE '0310-LINK-TO-PRIMARY-BOS' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0310-LINK-TO-PRIMARY-BOS");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM LINK'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM LINK");
			//            MOVE HUPS-BUS-OBJ-MDU-NM TO EFAL-ERR-OBJECT-NAME
			// COB_CODE: MOVE WS-BUS-OBJ-MDU  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsWorkFields().getBusObjMdu());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: SET UBOC-KEEP-UOW-STORAGE TO TRUE
			ws.getWsUbocMsg().getCommInfo().getUbocKeepClearUowStorageSw().setUbocKeepUowStorage();
			// COB_CODE: GO TO 0310-LINK-TO-PRIMARY-BOS-X
			return;
		}
		//* IF LOGGABLE FAILURES HAVE OCCURED, THEN STOP PROCESSING
		//* PRIMARY BUSINESS OBJECTS, ROLLBACK ANY DB2 WORK, AND DETERMINE
		//* IF INTERMEDIATE ERROR STORAGE SHOULD BE RETAINED OR CLEARED.
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 0310-LINK-TO-PRIMARY-BOS-X
		//           END-IF.
		if (ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: SET MDRV-PERFORM-ROLLBACK TO TRUE
			ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setRollback();
			// COB_CODE: GO TO 0310-LINK-TO-PRIMARY-BOS-X
			return;
		}
	}

	/**Original name: 0320-READ-BOBJ-MDU-XREF_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  RETRIEVE THE BUSINESS OBJECT MODULE NAME USING THE BUSINESS    *
	 *  OBJECT NAME.                                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readBobjMduXref() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE WS-BUS-OBJ-NM TO HBMX-BUS-OBJ-NM.
		ws.getDclhalBoMduXrfV().setBusObjNm(ws.getWsWorkFields().getBusObjNm());
		// COB_CODE: EXEC SQL
		//               SELECT HBMX_BOBJ_MDU_NM
		//                 INTO :HBMX-BOBJ-MDU-NM
		//                 FROM HAL_BO_MDU_XRF_V
		//                WHERE BUS_OBJ_NM = :HBMX-BUS-OBJ-NM
		//           END-EXEC.
		ws.getDclhalBoMduXrfV()
				.setBobjMduNm(halBoMduXrfVDao.selectByHbmxBusObjNm(ws.getDclhalBoMduXrfV().getBusObjNm(), ws.getDclhalBoMduXrfV().getBobjMduNm()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                    GO TO 0320-READ-BOBJ-MDU-XREF-X
		//               WHEN OTHER
		//                    GO TO 0320-READ-BOBJ-MDU-XREF-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE   OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED  OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQD-DATA-NOT-FOUND OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspReqdDataNotFound();
			// COB_CODE: MOVE '0320-READ-BOBJ-MDU-XREF'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0320-READ-BOBJ-MDU-XREF");
			// COB_CODE: MOVE 'EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ'
			//             TO EFAL-ERR-COMMENT   OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0320-READ-BOBJ-MDU-XREF-X
			return;

		default:// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED   OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT   OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF'
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF");
			// COB_CODE: MOVE '0320-READ-BOBJ-MDU-XREF'
			//             TO EFAL-ERR-PARAGRAPH   OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0320-READ-BOBJ-MDU-XREF");
			// COB_CODE: MOVE 'SELECT FROM OBJ XREF TABLE FAILED'
			//             TO EFAL-ERR-COMMENT     OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT FROM OBJ XREF TABLE FAILED");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0320-READ-BOBJ-MDU-XREF-X
			return;
		}
		//* CHECK MODULE NAME PRESENT IN OBJ XREF ROW RETURNED
		// COB_CODE: IF HBMX-BOBJ-MDU-NM = SPACES
		//              GO TO 0320-READ-BOBJ-MDU-XREF-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclhalBoMduXrfV().getBobjMduNm())) {
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE    OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED   OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUIRED-FIELD-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequiredFieldBlank();
			// COB_CODE: MOVE '0320-READ-BOBJ-MDU-XREF'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0320-READ-BOBJ-MDU-XREF");
			// COB_CODE: MOVE 'MODULE NAME ON OBJ XREF ROW IS BLANK'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MODULE NAME ON OBJ XREF ROW IS BLANK");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM='  HBMX-BUS-OBJ-NM ';'
			//                  'HBMX-BOBJ-MDU-NM=' HBMX-BOBJ-MDU-NM ';'
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";", "HBMX-BOBJ-MDU-NM=", ws.getDclhalBoMduXrfV().getBobjMduNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0320-READ-BOBJ-MDU-XREF-X
			return;
		}
		//* USE MODULE NAME FROM OBJ XREF ROW
		// COB_CODE: MOVE HBMX-BOBJ-MDU-NM TO WS-BUS-OBJ-MDU.
		ws.getWsWorkFields().setBusObjMdu(ws.getDclhalBoMduXrfV().getBobjMduNm());
	}

	/**Original name: 0400-INVOKE-ALERT-MODULE_FIRST_SENTENCES<br>
	 * <pre>***********************************************************
	 *  LINK TO THE ERROR ALERT MODULE THAT PERFORMS ADDITIONAL  *
	 *  ERROR NOTIFICATION.                                      *
	 * ***********************************************************</pre>*/
	private void invokeAlertModule() {
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (WS-ERROR-ALERT-MOD)
		//               COMMAREA (WS-UBOC-MSG)
		//               LENGTH   (LENGTH OF WS-UBOC-MSG)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALOUMEL", execContext).commarea(ws.getWsUbocMsg()).length(Dfhcommarea.Len.DFHCOMMAREA)
				.link(ws.getWsWorkFields().getErrorAlertMod(), new Haloepal());
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                  GO TO 0400-INVOKE-ALERT-MODULE-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE '0400-INVOKE-ALERT-MODULE' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0400-INVOKE-ALERT-MODULE");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM LINK'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM LINK");
			// COB_CODE: MOVE WS-ERROR-ALERT-MOD TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsWorkFields().getErrorAlertMod());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0400-INVOKE-ALERT-MODULE-X
			return;
		}
	}

	/**Original name: 0500-CLEANUP-ERROR-STORAGE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DETERMINE IF THE DB2 WORK SHOULD BE SYNCED OR ROLLED BACK.    *
	 *  DETERMINE IF INTERMEDIATE ERROR STORAGE SHOULD BE CLEARED OR  *
	 *  RETAINED FOR THIS STORE ID (GUID).                            *
	 * ****************************************************************</pre>*/
	private void cleanupErrorStorage() {
		// COB_CODE: IF MDRV-PERFORM-ROLLBACK
		//               END-EXEC
		//           ELSE
		//               END-EXEC
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().isRollback()) {
			// COB_CODE: EXEC CICS
			//               SYNCPOINT ROLLBACK
			//           END-EXEC
			TpSession.current().syncpoint(false);
		} else {
			// COB_CODE: EXEC CICS
			//               SYNCPOINT
			//           END-EXEC
			TpSession.current().syncpoint(true);
		}
		// COB_CODE: IF UBOC-CLEAR-UOW-STORAGE
		//                  UNTIL OBJECT-NUM GREATER THAN WS-NUM-ENTRIES
		//           END-IF.
		if (ws.getWsUbocMsg().getCommInfo().getUbocKeepClearUowStorageSw().isUbocClearUowStorage()) {
			// COB_CODE: PERFORM 0510-CLEAR-INT-ERR-STORE
			//             VARYING OBJECT-NUM FROM 1 BY 1
			//              UNTIL OBJECT-NUM GREATER THAN WS-NUM-ENTRIES
			ws.setObjectNum(1);
			while (!(ws.getObjectNum() > ws.getWsWorkFields().getNumEntries())) {
				clearIntErrStore();
				ws.setObjectNum(Trunc.toInt(ws.getObjectNum() + 1, 9));
			}
		}
	}

	/**Original name: 0510-CLEAR-INT-ERR-STORE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DELETE THE RECORDS ADDED TO THE INTERMEDIATE ERROR STORAGE    *
	 *  VSAM (I.E. THE UOW REQUEST MESSAGE).                          *
	 * ****************************************************************</pre>*/
	private void clearIntErrStore() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE MDRV-MSG-ID TO URQM-ID.
		ws.getUrqmCommon().setId(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: PERFORM 0515-BUILD-DELETE-KEY.
		buildDeleteKey();
		// COB_CODE: MOVE 0 TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(0);
		// COB_CODE: EXEC CICS
		//               DELETE FILE (WS-ERROR-VSAM-NAME)
		//               RIDFLD      (URQM-PARTIAL-KEY)
		//               KEYLENGTH   (LENGTH OF URQM-PARTIAL-KEY)
		//               GENERIC
		//               RESP        (WS-RESPONSE-CODE)
		//               RESP2       (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getWsWorkFields().getErrorVsamNameFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getPartialKeyBytes());
			iRowDAO.delete(iRowData, UrqmCommon.Len.PARTIAL_KEY);
		}
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//               WHEN DFHRESP(NOTFND)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0510-CLEAR-INT-ERR-STORE-X
		//           END-EVALUATE.
		if ((TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL)
				|| (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NOTFND)) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: STRING 'DELETE KEY ='   URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "DELETE KEY =", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0510-CLEAR-INT-ERR-STORE'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0510-CLEAR-INT-ERR-STORE");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE UMT");
			// COB_CODE: MOVE WS-ERROR-VSAM-NAME TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsWorkFields().getErrorVsamName());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0510-CLEAR-INT-ERR-STORE-X
			return;
		}
	}

	/**Original name: 0515-BUILD-DELETE-KEY_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  BUILD KEY FOR DELETION OF RECS IN INTERMEDIATE ERROR STORAGE
	 *  VSAM (I.E. THE UOW REQUEST MESSAGE).
	 * ****************************************************************</pre>*/
	private void buildDeleteKey() {
		// COB_CODE: MOVE WS-DELETE-OBJ-NAME(OBJECT-NUM) TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(ws.getWsDeleteObject().getDeleteObjName(ws.getObjectNum()));
		// COB_CODE: IF WS-DELETE-OBJ-NAME(OBJECT-NUM) = 'HAL_ERR_LOG_DTL_V'
		//              MOVE WS-ERROR-STO-DTL-VSAM TO WS-ERROR-VSAM-NAME
		//           ELSE
		//              MOVE WS-ERROR-STORAGE-VSAM TO WS-ERROR-VSAM-NAME
		//           END-IF.
		if (Conditions.eq(ws.getWsDeleteObject().getDeleteObjName(ws.getObjectNum()), "HAL_ERR_LOG_DTL_V")) {
			// COB_CODE: MOVE WS-ERROR-STO-DTL-VSAM TO WS-ERROR-VSAM-NAME
			ws.getWsWorkFields().setErrorVsamName(ws.getWsWorkFields().getErrorStoDtlVsam());
		} else {
			// COB_CODE: MOVE WS-ERROR-STORAGE-VSAM TO WS-ERROR-VSAM-NAME
			ws.getWsWorkFields().setErrorVsamName(ws.getWsWorkFields().getErrorStorageVsam());
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  CALL HALOESTO TO LOG FAILURES.                          *
	 * **********************************************************</pre>*/
	private void logWarningOrError() {
		// COB_CODE: SET MDRV-PERFORM-ROLLBACK TO TRUE.
		ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setRollback();
		// COB_CODE: SET MDRV-MAINDRVR-LOGGABLE-ERRS TO TRUE.
		ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().setMdrvMaindrvrLoggableErrs();
		// COB_CODE: MOVE '+'               TO EFAL-DB2-ERR-SQLCODE-SIGN
		//                                     EFAL-CICS-ERR-RESP-SIGN
		//                                     EFAL-CICS-ERR-RESP2-SIGN
		//                                     EFAL-SEC-SYS-ID-SIGN
		//                                     EFAL-ETRA-PRIORITY-LEVEL-SIGN.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: IF EFAL-DB2-FAILED
		//               END-IF
		//           END-IF.
		if (ws.getWsEstoInfo().getEstoDetailBuffer().isEfalDb2Failed()) {
			// COB_CODE: MOVE SQLCODE  TO EFAL-DB2-ERR-SQLCODE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC TO EFAL-DB2-ERR-SQLERRMC
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: IF SQLCODE IS POSITIVE
			//               MOVE '+'  TO EFAL-DB2-ERR-SQLCODE-SIGN
			//             ELSE
			//               MOVE '-'  TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'  TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'  TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
		}
		// COB_CODE: IF EFAL-CICS-FAILED
		//               END-IF
		//           END-IF.
		if (ws.getWsEstoInfo().getEstoDetailBuffer().isEfalCicsFailed()) {
			// COB_CODE: MOVE WS-RESPONSE-CODE  TO EFAL-CICS-ERR-RESP
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsWorkFields().getResponseCode(), 10));
			// COB_CODE: MOVE WS-RESPONSE-CODE2 TO EFAL-CICS-ERR-RESP2
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsWorkFields().getResponseCode2(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//               MOVE '+'  TO EFAL-CICS-ERR-RESP-SIGN
			//             ELSE
			//               MOVE '-'  TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWsWorkFields().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'  TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'  TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//               MOVE '+'  TO EFAL-CICS-ERR-RESP2-SIGN
			//             ELSE
			//               MOVE '-'  TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWsWorkFields().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'  TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'  TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
		}
		// COB_CODE: MOVE MDRV-MSG-ID          TO ESTO-STORE-ID
		//                                        EFAL-FAIL-LVL-GUID.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(ws.getHallmdrv().getDriverInfo().getMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: SET ESTO-FAILURE-LEVEL    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: SET EFAL-S3-SAVARCH       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		// COB_CODE: SET EFAL-MAINFRAME        TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: MOVE WS-PROGRAM-NAME      TO EFAL-FAILED-MODULE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsWorkFields().getProgramName());
		// COB_CODE: MOVE MDRV-UNIT-OF-WORK    TO EFAL-UNIT-OF-WORK.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(ws.getHallmdrv().getDriverInfo().getUnitOfWork());
		// COB_CODE: MOVE MDRV-AUTH-USERID     TO EFAL-LOGON-USERID.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(ws.getHallmdrv().getDriverInfo().getAuthUserid());
		// COB_CODE: MOVE COM-SEC-SYS-ID       TO EFAL-SEC-SYS-ID.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: EXEC CICS ASSIGN
		//               APPLID(WS-APPLID)
		//           END-EXEC.
		ws.getWsWorkFields().setApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID         TO EFAL-FAILED-LOCATION-ID.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsWorkFields().getApplid());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  ('HALOESTO')
		//               COMMAREA (WS-ESTO-INFO)
		//               LENGTH   (LENGTH OF WS-ESTO-INFO)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALOUMEL", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: SET UBOC-KEEP-UOW-STORAGE TO TRUE.
		ws.getWsUbocMsg().getCommInfo().getUbocKeepClearUowStorageSw().setUbocKeepUowStorage();
	}

	public void initDriverInfo() {
		ws.getHallmdrv().getDriverInfo().setMsgId("");
		ws.getHallmdrv().getDriverInfo().setMsgRecSeqFormatted("000");
		ws.getHallmdrv().getDriverInfo().getMessageTransferCd().setMessageTransferCd(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setUnitOfWork("");
		ws.getHallmdrv().getDriverInfo().setAuthUserid("");
		ws.getHallmdrv().getDriverInfo().setTerminalId("");
		ws.getHallmdrv().getDriverInfo().getDeleteUowStore().setDeleteUowStore(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getReturnWarningsInd().setReturnWarningsInd(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setTotalLengthFormatted("000000000");
		ws.getHallmdrv().getDriverInfo().setNumChunksFormatted("000");
		ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setCommitRollbackInd(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().setMaindrvLoggableProblems(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setMainDrvrErrcode("");
		ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setUowLoggableProblems(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setUnitOfWorkErrcode("");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setFailedModule("");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setFailedParagraph("");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setSqlcodeDisplayFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setEibrespDisplayFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setEibresp2DisplayFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().getErrorsLoggedSw().setErrorsLoggedSw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getErrorDetails().getErrorLoggingLvlSw().setErrorLoggingLvlSw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogSqlcodeDsplyFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogEibrespDsplyFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogEibresp2DsplyFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().setSessionId("");
		ws.getHallmdrv().getDriverInfo().getLoggableErrLogOnlySw().setLoggableErrLogOnlySw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getSyncpointLocation().setSyncpointLocation(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getRequestMsgProcessing().setRequestMsgProcessing(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getResponseMsgProcessing().setResponseMsgProcessing(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getMsgtranLoggableProblems().setMsgtranLoggableProblems(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setMsgtranErrcode("");
		ws.getHallmdrv().getDriverInfo().getTransElapsedTimeSw().setTransElapsedTimeSw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setTransElapsedTimeAmtFormatted("00000000");
	}

	public void initWsEstoInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initEfalFailureRecordInfo() {
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlErrTimestamp("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedApplication("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedPlatform("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailureType(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedActionType("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSign(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSign(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespFormatted("0000000000");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2Sign(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2Formatted("0000000000");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSign(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalActionBuffer("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSign(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelFormatted("00000");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraErrorRef("");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraErrorTxt("");
	}

	public void initWsUbocMsg() {
		ws.getWsUbocMsg().getCommInfo().setUbocUowName("");
		ws.getWsUbocMsg().getCommInfo().setUbocMsgId("");
		ws.getWsUbocMsg().getCommInfo().setUbocAuthUserid("");
		ws.getWsUbocMsg().getCommInfo().setUbocAuthUserClientid("");
		ws.getWsUbocMsg().getCommInfo().setUbocTerminalId("");
		ws.getWsUbocMsg().getCommInfo().setUbocPrimaryBusObj("");
		ws.getWsUbocMsg().getCommInfo().getUbocStoreTypeCd().setUbocStoreTypeCd(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().setUbocMdrReqStore("");
		ws.getWsUbocMsg().getCommInfo().setUbocMdrRspStore("");
		ws.getWsUbocMsg().getCommInfo().setUbocUowReqMsgStore("");
		ws.getWsUbocMsg().getCommInfo().setUbocUowReqSwitchesStore("");
		ws.getWsUbocMsg().getCommInfo().setUbocUowRespHeaderStore("");
		ws.getWsUbocMsg().getCommInfo().setUbocUowRespDataStore("");
		ws.getWsUbocMsg().getCommInfo().setUbocUowRespWarningsStore("");
		ws.getWsUbocMsg().getCommInfo().setUbocUowKeyReplaceStore("");
		ws.getWsUbocMsg().getCommInfo().setUbocUowRespNlBlErrsStore("");
		ws.getWsUbocMsg().getCommInfo().setUbocSessionId("");
		ws.getWsUbocMsg().getCommInfo().setUbocUowLockProcTsq("");
		ws.getWsUbocMsg().getCommInfo().setUbocUowReqSwitchesTsq("");
		ws.getWsUbocMsg().getCommInfo().getUbocPassThruAction().setUbocPassThruAction("");
		ws.getWsUbocMsg().getCommInfo().setUbocNbrHdrRowsFormatted("000000000");
		ws.getWsUbocMsg().getCommInfo().setUbocNbrDataRowsFormatted("000000000");
		ws.getWsUbocMsg().getCommInfo().setUbocNbrWarningsFormatted("000000000");
		ws.getWsUbocMsg().getCommInfo().setUbocNbrNonlogBlErrsFormatted("000000000");
		ws.getWsUbocMsg().getCommInfo().getUbocApplicationTsSw().setUbocApplicationTsSw(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().setUbocApplicationTs("");
		ws.getWsUbocMsg().getCommInfo().getUbocUowLockStrategyCd().setUbocUowLockStrategyCd(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().setUbocUowLockTimeoutInterval(0);
		ws.getWsUbocMsg().getCommInfo().getUbocUserInLockGrpSw().setUbocUserInLockGrpSw(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().setUbocLockGroup("");
		ws.getWsUbocMsg().getCommInfo().setFiller3("");
		ws.getWsUbocMsg().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().setApplyDataPrivacySw(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().getUbocSecAndDataPrivInfo().setSecurityContext("");
		ws.getWsUbocMsg().getCommInfo().getUbocSecAndDataPrivInfo().getSecAutNbr().setUbocSecAutNbrFormatted("00000");
		ws.getWsUbocMsg().getCommInfo().getUbocSecAndDataPrivInfo().setSecAssociationType("");
		ws.getWsUbocMsg().getCommInfo().getUbocDataPrivRetCode().setUbocDataPrivRetCode("");
		ws.getWsUbocMsg().getCommInfo().setUbocSecGroupName("");
		ws.getWsUbocMsg().getCommInfo().setFiller4("");
		ws.getWsUbocMsg().getCommInfo().getUbocAuditProcessingInfo().getApplyAuditsSw().setApplyAuditsSw(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().getUbocAuditProcessingInfo().setAudtBusObjNm("");
		ws.getWsUbocMsg().getCommInfo().getUbocAuditProcessingInfo().setAudtEventData("");
		ws.getWsUbocMsg().getCommInfo().setFiller5("");
		ws.getWsUbocMsg().getCommInfo().getUbocKeepClearUowStorageSw().setUbocKeepClearUowStorageSw(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setUbocObjectLoggableProblems(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setUbocProcessingStatusSw(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().setFiller6("");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setErrorCode("");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setUbocSqlcodeDisplayFormatted("0000000000");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setUbocEibrespDisplayFormatted("0000000000");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setUbocEibresp2DisplayFormatted("0000000000");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setUbocLoggableErrLogOnlySw(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().setFiller7("");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setErrorsLoggedSw(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setErrorLoggingLvlSw(Types.SPACE_CHAR);
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setUbocErrLogSqlcodeDsplyFormatted("0000000000");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setUbocErrLogEibrespDsplyFormatted("0000000000");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setUbocErrLogEibresp2DsplyFormatted("0000000000");
		ws.getWsUbocMsg().getCommInfo().getUbocErrorDetails().setFiller8("");
		ws.getWsUbocMsg().setAppDataBufferLengthFormatted("0000");
		ws.getWsUbocMsg().setAppDataBuffer("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}

	public void deleteArgListeners() {
		execContext.getCommAreaLenNotifier().deleteListener(dfhcommarea.getCaLnkDataListener());
	}

	public void registerArgListeners() {
		execContext.getCommAreaLenNotifier().addListener(dfhcommarea.getCaLnkDataListener());
		execContext.getCommAreaLenNotifier().notifyListeners();
	}

	public void registerListeners() {
		ws.getHallmdrv().getDriverInfo().getUowBufferLength().addListener(ws.getHallmdrv().getMdrvUowMessageByteListener());
	}
}
