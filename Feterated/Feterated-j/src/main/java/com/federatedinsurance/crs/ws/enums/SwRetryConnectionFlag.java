/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-RETRY-CONNECTION-FLAG<br>
 * Variable: SW-RETRY-CONNECTION-FLAG from program TS548099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwRetryConnectionFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char DO_NOT_RETRY_CONNECTION = '0';
	public static final char RETRY_CONNECTION = '1';

	//==== METHODS ====
	public void setSwRetryConnectionFlag(char swRetryConnectionFlag) {
		this.value = swRetryConnectionFlag;
	}

	public char getSwRetryConnectionFlag() {
		return this.value;
	}

	public void setDoNotRetryConnection() {
		value = DO_NOT_RETRY_CONNECTION;
	}

	public boolean isRetryConnection() {
		return value == RETRY_CONNECTION;
	}

	public void setRetryConnection() {
		value = RETRY_CONNECTION;
	}
}
