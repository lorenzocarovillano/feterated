/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: TP-PI-PIPE-STA-CD<br>
 * Variable: TP-PI-PIPE-STA-CD from program TS547099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TpPiPipeStaCd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.PIPE_STA_CD);
	public static final String OK = "0";
	public static final String UNUSABLE = "1";

	//==== METHODS ====
	public void setPipeStaCd(short pipeStaCd) {
		this.value = NumericDisplay.asString(pipeStaCd, Len.PIPE_STA_CD);
	}

	public void setPipeStaCdFormatted(String pipeStaCd) {
		this.value = Trunc.toUnsignedNumeric(pipeStaCd, Len.PIPE_STA_CD);
	}

	public short getPipeStaCd() {
		return NumericDisplay.asShort(this.value);
	}

	public String getPipeStaCdFormatted() {
		return this.value;
	}

	public boolean isOk() {
		return getPipeStaCdFormatted().equals(OK);
	}

	public void setOk() {
		setPipeStaCdFormatted(OK);
	}

	public boolean isUnusable() {
		return getPipeStaCdFormatted().equals(UNUSABLE);
	}

	public void setUnusable() {
		setPipeStaCdFormatted(UNUSABLE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PIPE_STA_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
