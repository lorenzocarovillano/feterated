/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HAL_SEC_DP_DFLT_V]
 * 
 */
public interface IHalSecDpDfltV extends BaseSqlTo {

	/**
	 * Host Variable WS-UOW-NM
	 * 
	 */
	String getUowNm();

	void setUowNm(String uowNm);

	/**
	 * Host Variable WS-ALL-UOWS-LIT
	 * 
	 */
	String getAllUowsLit();

	void setAllUowsLit(String allUowsLit);

	/**
	 * Host Variable WS-SEC-GRP-NM
	 * 
	 */
	String getSecGrpNm();

	void setSecGrpNm(String secGrpNm);

	/**
	 * Host Variable WS-AUTH-USERID
	 * 
	 */
	String getAuthUserid();

	void setAuthUserid(String authUserid);

	/**
	 * Host Variable WS-ALL-SEC-GRPS-LIT
	 * 
	 */
	String getAllSecGrpsLit();

	void setAllSecGrpsLit(String allSecGrpsLit);

	/**
	 * Host Variable WS-SEC-ASC-TYP
	 * 
	 */
	String getSecAscTyp();

	void setSecAscTyp(String secAscTyp);

	/**
	 * Host Variable WS-ALL-SEC-ASCS-LIT
	 * 
	 */
	String getAllSecAscsLit();

	void setAllSecAscsLit(String allSecAscsLit);

	/**
	 * Host Variable WS-DEFAULT-FLD-1
	 * 
	 */
	String getDefaultFld1();

	void setDefaultFld1(String defaultFld1);

	/**
	 * Host Variable WS-SUPPLIED-FLD-1
	 * 
	 */
	String getSuppliedFld1();

	void setSuppliedFld1(String suppliedFld1);

	/**
	 * Host Variable WS-DEFAULT-FLD-2
	 * 
	 */
	String getDefaultFld2();

	void setDefaultFld2(String defaultFld2);

	/**
	 * Host Variable WS-SUPPLIED-FLD-2
	 * 
	 */
	String getSuppliedFld2();

	void setSuppliedFld2(String suppliedFld2);

	/**
	 * Host Variable WS-DEFAULT-FLD-3
	 * 
	 */
	String getDefaultFld3();

	void setDefaultFld3(String defaultFld3);

	/**
	 * Host Variable WS-SUPPLIED-FLD-3
	 * 
	 */
	String getSuppliedFld3();

	void setSuppliedFld3(String suppliedFld3);

	/**
	 * Host Variable WS-DEFAULT-FLD-4
	 * 
	 */
	String getDefaultFld4();

	void setDefaultFld4(String defaultFld4);

	/**
	 * Host Variable WS-SUPPLIED-FLD-4
	 * 
	 */
	String getSuppliedFld4();

	void setSuppliedFld4(String suppliedFld4);

	/**
	 * Host Variable WS-DEFAULT-FLD-5
	 * 
	 */
	String getDefaultFld5();

	void setDefaultFld5(String defaultFld5);

	/**
	 * Host Variable WS-SUPPLIED-FLD-5
	 * 
	 */
	String getSuppliedFld5();

	void setSuppliedFld5(String suppliedFld5);

	/**
	 * Host Variable WS-ASSOC-BUS-OBJ-NM
	 * 
	 */
	String getAssocBusObjNm();

	void setAssocBusObjNm(String assocBusObjNm);

	/**
	 * Host Variable WS-SE3-CUR-ISO-DATE
	 * 
	 */
	String getSe3CurIsoDate();

	void setSe3CurIsoDate(String se3CurIsoDate);

	/**
	 * Host Variable HSDD-GEN-TXT-FLD-1
	 * 
	 */
	String getGenTxtFld1();

	void setGenTxtFld1(String genTxtFld1);

	/**
	 * Host Variable HSDD-GEN-TXT-FLD-2
	 * 
	 */
	String getGenTxtFld2();

	void setGenTxtFld2(String genTxtFld2);

	/**
	 * Host Variable HSDD-GEN-TXT-FLD-3
	 * 
	 */
	String getGenTxtFld3();

	void setGenTxtFld3(String genTxtFld3);

	/**
	 * Host Variable HSDD-GEN-TXT-FLD-4
	 * 
	 */
	String getGenTxtFld4();

	void setGenTxtFld4(String genTxtFld4);

	/**
	 * Host Variable HSDD-GEN-TXT-FLD-5
	 * 
	 */
	String getGenTxtFld5();

	void setGenTxtFld5(String genTxtFld5);

	/**
	 * Host Variable HSDD-BUS-OBJ-NM
	 * 
	 */
	String getBusObjNm();

	void setBusObjNm(String busObjNm);

	/**
	 * Host Variable HSDD-REQ-TYP-CD
	 * 
	 */
	String getReqTypCd();

	void setReqTypCd(String reqTypCd);

	/**
	 * Host Variable HSDD-EFFECTIVE-DT
	 * 
	 */
	String getEffectiveDt();

	void setEffectiveDt(String effectiveDt);

	/**
	 * Host Variable HSDD-EXPIRATION-DT
	 * 
	 */
	String getExpirationDt();

	void setExpirationDt(String expirationDt);

	/**
	 * Host Variable HSDD-SEQ-NBR
	 * 
	 */
	int getSeqNbr();

	void setSeqNbr(int seqNbr);

	/**
	 * Host Variable HSDD-CMN-NM
	 * 
	 */
	String getCmnNm();

	void setCmnNm(String cmnNm);

	/**
	 * Host Variable HSDD-CMN-ATR-IND
	 * 
	 */
	char getCmnAtrInd();

	void setCmnAtrInd(char cmnAtrInd);

	/**
	 * Host Variable HSDD-DFL-TYP-IND
	 * 
	 */
	char getDflTypInd();

	void setDflTypInd(char dflTypInd);

	/**
	 * Host Variable HSDD-DFL-DTA-AMT
	 * 
	 */
	AfDecimal getDflDtaAmt();

	void setDflDtaAmt(AfDecimal dflDtaAmt);

	/**
	 * Nullable property for HSDD-DFL-DTA-AMT
	 * 
	 */
	AfDecimal getDflDtaAmtObj();

	void setDflDtaAmtObj(AfDecimal dflDtaAmtObj);

	/**
	 * Host Variable HSDD-DFL-DTA-TXT
	 * 
	 */
	String getDflDtaTxt();

	void setDflDtaTxt(String dflDtaTxt);

	/**
	 * Host Variable HSDD-DFL-OFS-NBR
	 * 
	 */
	int getDflOfsNbr();

	void setDflOfsNbr(int dflOfsNbr);

	/**
	 * Nullable property for HSDD-DFL-OFS-NBR
	 * 
	 */
	Integer getDflOfsNbrObj();

	void setDflOfsNbrObj(Integer dflOfsNbrObj);

	/**
	 * Host Variable HSDD-DFL-OFS-PER
	 * 
	 */
	String getDflOfsPer();

	void setDflOfsPer(String dflOfsPer);

	/**
	 * Host Variable HSDD-CONTEXT
	 * 
	 */
	String getContext();

	void setContext(String context);
};
