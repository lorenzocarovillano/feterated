/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UBOC-LOGGABLE-ERR-LOG-ONLY-SW<br>
 * Variable: UBOC-LOGGABLE-ERR-LOG-ONLY-SW from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocLoggableErrLogOnlySw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char REQUIRED = 'Y';
	public static final char NOT_REQUIRED = 'N';
	public static final char NOT_SET = ' ';

	//==== METHODS ====
	public void setUbocLoggableErrLogOnlySw(char ubocLoggableErrLogOnlySw) {
		this.value = ubocLoggableErrLogOnlySw;
	}

	public char getUbocLoggableErrLogOnlySw() {
		return this.value;
	}

	public boolean isRequired() {
		return value == REQUIRED;
	}

	public void setRequired() {
		value = REQUIRED;
	}

	public boolean isNotRequired() {
		return value == NOT_REQUIRED;
	}

	public void setNotRequired() {
		value = NOT_REQUIRED;
	}

	public boolean isNotSet() {
		return value == NOT_SET;
	}

	public void setUbocLogOnlyNotSet() {
		value = NOT_SET;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_LOGGABLE_ERR_LOG_ONLY_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
