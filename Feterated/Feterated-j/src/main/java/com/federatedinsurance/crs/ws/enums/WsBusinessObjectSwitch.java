/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-BUSINESS-OBJECT-SWITCH<br>
 * Variable: WS-BUSINESS-OBJECT-SWITCH from program MU0Q0004<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsBusinessObjectSwitch {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.BUSINESS_OBJECT_SWITCH);
	public static final String RETURN_ALL = "RETURN ALL";
	public static final String CLIENT_TAB = "CLIENT_TAB_V";
	public static final String CLT_ADR_COMP = "CLIENT_ADDRESS_COMPOSITE";
	public static final String ADR_BEST_BSM = "CLIENT_ADDRESS_BEST_BSM";
	public static final String ADR_BEST_BSL = "CLIENT_ADDRESS_BEST_BSL";
	public static final String FED_BUS_TYP = "FED_BUSINESS_TYP_V";
	public static final String CLIENT_PHONE = "CLIENT_PHONE_V";
	public static final String CLIENT_EMAIL = "CLIENT_EMAIL_V";
	public static final String FED_SEG_INFO = "FED_SEG_INFO_V";

	//==== METHODS ====
	public void setBusinessObjectSwitch(String businessObjectSwitch) {
		this.value = Functions.subString(businessObjectSwitch, Len.BUSINESS_OBJECT_SWITCH);
	}

	public String getBusinessObjectSwitch() {
		return this.value;
	}

	public void setReturnAll() {
		value = RETURN_ALL;
	}

	public void setClientTab() {
		value = CLIENT_TAB;
	}

	public void setAdrBestBsm() {
		value = ADR_BEST_BSM;
	}

	public void setAdrBestBsl() {
		value = ADR_BEST_BSL;
	}

	public void setFedBusTyp() {
		value = FED_BUS_TYP;
	}

	public void setClientPhone() {
		value = CLIENT_PHONE;
	}

	public void setFedSegInfo() {
		value = FED_SEG_INFO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUSINESS_OBJECT_SWITCH = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
