/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ACT_NOT, ACT_NOT_POL]
 * 
 */
public interface IActNotPol1 extends BaseSqlTo {

	/**
	 * Host Variable NOT-PRC-TS
	 * 
	 */
	String getNotPrcTs();

	void setNotPrcTs(String notPrcTs);

	/**
	 * Host Variable CSR-ACT-NBR
	 * 
	 */
	String getCsrActNbr();

	void setCsrActNbr(String csrActNbr);

	/**
	 * Host Variable CF-ACT-NOT-TYP-CD-IMP
	 * 
	 */
	String getCfActNotTypCdImp();

	void setCfActNotTypCdImp(String cfActNotTypCdImp);

	/**
	 * Host Variable POL-NBR
	 * 
	 */
	String getPolNbr();

	void setPolNbr(String polNbr);

	/**
	 * Host Variable WS-POLICY-CANC-DT
	 * 
	 */
	String getWsPolicyCancDt();

	void setWsPolicyCancDt(String wsPolicyCancDt);

	/**
	 * Host Variable WS-NBR-OF-NOT-DAY
	 * 
	 */
	short getWsNbrOfNotDay();

	void setWsNbrOfNotDay(short wsNbrOfNotDay);

	/**
	 * Host Variable POL-TYP-CD
	 * 
	 */
	String getPolTypCd();

	void setPolTypCd(String polTypCd);

	/**
	 * Host Variable TOT-FEE-AMT
	 * 
	 */
	AfDecimal getTotFeeAmt();

	void setTotFeeAmt(AfDecimal totFeeAmt);

	/**
	 * Nullable property for TOT-FEE-AMT
	 * 
	 */
	AfDecimal getTotFeeAmtObj();

	void setTotFeeAmtObj(AfDecimal totFeeAmtObj);

	/**
	 * Host Variable POL-EXP-DT
	 * 
	 */
	String getPolExpDt();

	void setPolExpDt(String polExpDt);

	/**
	 * Host Variable POL-EFF-DT
	 * 
	 */
	String getPolEffDt();

	void setPolEffDt(String polEffDt);

	/**
	 * Host Variable CF-ACT-NOT-IMPENDING
	 * 
	 */
	String getCfActNotImpending();

	void setCfActNotImpending(String cfActNotImpending);

	/**
	 * Host Variable CF-ACT-NOT-NONPAY
	 * 
	 */
	String getCfActNotNonpay();

	void setCfActNotNonpay(String cfActNotNonpay);

	/**
	 * Host Variable CF-ACT-NOT-RESCIND
	 * 
	 */
	String getCfActNotRescind();

	void setCfActNotRescind(String cfActNotRescind);

	/**
	 * Host Variable ACT-NOT-TYP-CD
	 * 
	 */
	String getActNotTypCd();

	void setActNotTypCd(String actNotTypCd);

	/**
	 * Host Variable ACT-OWN-CLT-ID
	 * 
	 */
	String getActOwnCltId();

	void setActOwnCltId(String actOwnCltId);

	/**
	 * Host Variable ACT-NOT-STA-CD
	 * 
	 */
	String getActNotStaCd();

	void setActNotStaCd(String actNotStaCd);

	/**
	 * Host Variable NOT-EFF-DT
	 * 
	 */
	String getNotEffDt();

	void setNotEffDt(String notEffDt);

	/**
	 * Host Variable NOT-DT
	 * 
	 */
	String getNotDt();

	void setNotDt(String notDt);

	/**
	 * Host Variable EMP-ID
	 * 
	 */
	String getEmpId();

	void setEmpId(String empId);

	/**
	 * Nullable property for EMP-ID
	 * 
	 */
	String getEmpIdObj();

	void setEmpIdObj(String empIdObj);
};
