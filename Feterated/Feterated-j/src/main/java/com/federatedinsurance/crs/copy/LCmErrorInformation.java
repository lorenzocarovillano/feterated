/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.LCmEiErrorSeverity;
import com.federatedinsurance.crs.ws.enums.LCmEiErrorType;

/**Original name: L-CM-ERROR-INFORMATION<br>
 * Variable: L-CM-ERROR-INFORMATION from copybook TS54801<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class LCmErrorInformation {

	//==== PROPERTIES ====
	//Original name: L-CM-EI-ERROR-SEVERITY
	private LCmEiErrorSeverity errorSeverity = new LCmEiErrorSeverity();
	//Original name: L-CM-EI-ERROR-TYPE
	private LCmEiErrorType errorType = new LCmEiErrorType();
	//Original name: L-CM-EI-MESSAGE
	private String message = DefaultValues.stringVal(Len.MESSAGE);
	//Original name: L-CM-EI-EXCI-RESP
	private int exciResp = DefaultValues.BIN_INT_VAL;
	//Original name: L-CM-EI-EXCI-RESP2
	private int exciResp2 = DefaultValues.BIN_INT_VAL;
	//Original name: L-CM-EI-EXCI-RESP3
	private int exciResp3 = DefaultValues.BIN_INT_VAL;
	//Original name: L-CM-EI-DPL-RESP
	private int dplResp = DefaultValues.BIN_INT_VAL;
	//Original name: L-CM-EI-DPL-RESP2
	private int dplResp2 = DefaultValues.BIN_INT_VAL;

	//==== METHODS ====
	public void setErrorInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		setErrorCodeBytes(buffer, position);
		position += Len.ERROR_CODE;
		message = MarshalByte.readString(buffer, position, Len.MESSAGE);
		position += Len.MESSAGE;
		setCodesBytes(buffer, position);
	}

	public byte[] getErrorInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		getErrorCodeBytes(buffer, position);
		position += Len.ERROR_CODE;
		MarshalByte.writeString(buffer, position, message, Len.MESSAGE);
		position += Len.MESSAGE;
		getCodesBytes(buffer, position);
		return buffer;
	}

	public void setErrorCodeBytes(byte[] buffer, int offset) {
		int position = offset;
		errorSeverity.setErrorSeverity(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		errorType.setErrorType(MarshalByte.readChar(buffer, position));
	}

	public byte[] getErrorCodeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, errorSeverity.getErrorSeverity());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, errorType.getErrorType());
		return buffer;
	}

	public void setMessage(String message) {
		this.message = Functions.subString(message, Len.MESSAGE);
	}

	public String getMessage() {
		return this.message;
	}

	public void setCodesBytes(byte[] buffer, int offset) {
		int position = offset;
		exciResp = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		exciResp2 = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		exciResp3 = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		dplResp = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		dplResp2 = MarshalByte.readBinaryInt(buffer, position);
	}

	public byte[] getCodesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryInt(buffer, position, exciResp);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, exciResp2);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, exciResp3);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, dplResp);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, dplResp2);
		return buffer;
	}

	public void setExciResp(int exciResp) {
		this.exciResp = exciResp;
	}

	public int getExciResp() {
		return this.exciResp;
	}

	public void setExciResp2(int exciResp2) {
		this.exciResp2 = exciResp2;
	}

	public int getExciResp2() {
		return this.exciResp2;
	}

	public void setExciResp3(int exciResp3) {
		this.exciResp3 = exciResp3;
	}

	public int getExciResp3() {
		return this.exciResp3;
	}

	public void setDplResp(int dplResp) {
		this.dplResp = dplResp;
	}

	public int getDplResp() {
		return this.dplResp;
	}

	public void setDplResp2(int dplResp2) {
		this.dplResp2 = dplResp2;
	}

	public int getDplResp2() {
		return this.dplResp2;
	}

	public LCmEiErrorSeverity getErrorSeverity() {
		return errorSeverity;
	}

	public LCmEiErrorType getErrorType() {
		return errorType;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_CODE = LCmEiErrorSeverity.Len.ERROR_SEVERITY + LCmEiErrorType.Len.ERROR_TYPE;
		public static final int MESSAGE = 131;
		public static final int EXCI_RESP = 4;
		public static final int EXCI_RESP2 = 4;
		public static final int EXCI_RESP3 = 4;
		public static final int DPL_RESP = 4;
		public static final int DPL_RESP2 = 4;
		public static final int CODES = EXCI_RESP + EXCI_RESP2 + EXCI_RESP3 + DPL_RESP + DPL_RESP2;
		public static final int ERROR_INFORMATION = ERROR_CODE + MESSAGE + CODES;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
