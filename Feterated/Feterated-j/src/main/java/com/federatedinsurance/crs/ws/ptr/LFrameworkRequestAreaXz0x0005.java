/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X0005<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x0005 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x0005() {
	}

	public LFrameworkRequestAreaXz0x0005(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzc001ActNotRowFormatted(String data) {
		writeString(Pos.XZC001Q_ACT_NOT_ROW, data, Len.XZC001Q_ACT_NOT_ROW);
	}

	public String getXzc001ActNotRowFormatted() {
		return readFixedString(Pos.XZC001Q_ACT_NOT_ROW, Len.XZC001Q_ACT_NOT_ROW);
	}

	public void setXzc001qActNotCsumFormatted(String xzc001qActNotCsum) {
		writeString(Pos.XZC001Q_ACT_NOT_CSUM, Trunc.toUnsignedNumeric(xzc001qActNotCsum, Len.XZC001Q_ACT_NOT_CSUM), Len.XZC001Q_ACT_NOT_CSUM);
	}

	/**Original name: XZC001Q-ACT-NOT-CSUM<br>*/
	public int getXzc001qActNotCsum() {
		return readNumDispUnsignedInt(Pos.XZC001Q_ACT_NOT_CSUM, Len.XZC001Q_ACT_NOT_CSUM);
	}

	public String getXzc001rActNotCsumFormatted() {
		return readFixedString(Pos.XZC001Q_ACT_NOT_CSUM, Len.XZC001Q_ACT_NOT_CSUM);
	}

	public void setXzc001qCsrActNbrKcre(String xzc001qCsrActNbrKcre) {
		writeString(Pos.XZC001Q_CSR_ACT_NBR_KCRE, xzc001qCsrActNbrKcre, Len.XZC001Q_CSR_ACT_NBR_KCRE);
	}

	/**Original name: XZC001Q-CSR-ACT-NBR-KCRE<br>*/
	public String getXzc001qCsrActNbrKcre() {
		return readString(Pos.XZC001Q_CSR_ACT_NBR_KCRE, Len.XZC001Q_CSR_ACT_NBR_KCRE);
	}

	public void setXzc001qNotPrcTsKcre(String xzc001qNotPrcTsKcre) {
		writeString(Pos.XZC001Q_NOT_PRC_TS_KCRE, xzc001qNotPrcTsKcre, Len.XZC001Q_NOT_PRC_TS_KCRE);
	}

	/**Original name: XZC001Q-NOT-PRC-TS-KCRE<br>*/
	public String getXzc001qNotPrcTsKcre() {
		return readString(Pos.XZC001Q_NOT_PRC_TS_KCRE, Len.XZC001Q_NOT_PRC_TS_KCRE);
	}

	public void setXzc001qTransProcessDt(String xzc001qTransProcessDt) {
		writeString(Pos.XZC001Q_TRANS_PROCESS_DT, xzc001qTransProcessDt, Len.XZC001Q_TRANS_PROCESS_DT);
	}

	/**Original name: XZC001Q-TRANS-PROCESS-DT<br>*/
	public String getXzc001qTransProcessDt() {
		return readString(Pos.XZC001Q_TRANS_PROCESS_DT, Len.XZC001Q_TRANS_PROCESS_DT);
	}

	public void setXzc001qCsrActNbr(String xzc001qCsrActNbr) {
		writeString(Pos.XZC001Q_CSR_ACT_NBR, xzc001qCsrActNbr, Len.XZC001Q_CSR_ACT_NBR);
	}

	/**Original name: XZC001Q-CSR-ACT-NBR<br>*/
	public String getXzc001qCsrActNbr() {
		return readString(Pos.XZC001Q_CSR_ACT_NBR, Len.XZC001Q_CSR_ACT_NBR);
	}

	public void setXzc001qNotPrcTs(String xzc001qNotPrcTs) {
		writeString(Pos.XZC001Q_NOT_PRC_TS, xzc001qNotPrcTs, Len.XZC001Q_NOT_PRC_TS);
	}

	/**Original name: XZC001Q-NOT-PRC-TS<br>*/
	public String getXzc001qNotPrcTs() {
		return readString(Pos.XZC001Q_NOT_PRC_TS, Len.XZC001Q_NOT_PRC_TS);
	}

	public void setXzc001qCsrActNbrCi(char xzc001qCsrActNbrCi) {
		writeChar(Pos.XZC001Q_CSR_ACT_NBR_CI, xzc001qCsrActNbrCi);
	}

	public void setXzc001qCsrActNbrCiFormatted(String xzc001qCsrActNbrCi) {
		setXzc001qCsrActNbrCi(Functions.charAt(xzc001qCsrActNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC001Q-CSR-ACT-NBR-CI<br>*/
	public char getXzc001qCsrActNbrCi() {
		return readChar(Pos.XZC001Q_CSR_ACT_NBR_CI);
	}

	public void setXzc001qNotPrcTsCi(char xzc001qNotPrcTsCi) {
		writeChar(Pos.XZC001Q_NOT_PRC_TS_CI, xzc001qNotPrcTsCi);
	}

	public void setXzc001qNotPrcTsCiFormatted(String xzc001qNotPrcTsCi) {
		setXzc001qNotPrcTsCi(Functions.charAt(xzc001qNotPrcTsCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC001Q-NOT-PRC-TS-CI<br>*/
	public char getXzc001qNotPrcTsCi() {
		return readChar(Pos.XZC001Q_NOT_PRC_TS_CI);
	}

	public void setXzc001qActNotTypCdCi(char xzc001qActNotTypCdCi) {
		writeChar(Pos.XZC001Q_ACT_NOT_TYP_CD_CI, xzc001qActNotTypCdCi);
	}

	/**Original name: XZC001Q-ACT-NOT-TYP-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getXzc001qActNotTypCdCi() {
		return readChar(Pos.XZC001Q_ACT_NOT_TYP_CD_CI);
	}

	public void setXzc001qActNotTypCd(String xzc001qActNotTypCd) {
		writeString(Pos.XZC001Q_ACT_NOT_TYP_CD, xzc001qActNotTypCd, Len.XZC001Q_ACT_NOT_TYP_CD);
	}

	/**Original name: XZC001Q-ACT-NOT-TYP-CD<br>*/
	public String getXzc001qActNotTypCd() {
		return readString(Pos.XZC001Q_ACT_NOT_TYP_CD, Len.XZC001Q_ACT_NOT_TYP_CD);
	}

	public void setXzc001qNotDtCi(char xzc001qNotDtCi) {
		writeChar(Pos.XZC001Q_NOT_DT_CI, xzc001qNotDtCi);
	}

	/**Original name: XZC001Q-NOT-DT-CI<br>*/
	public char getXzc001qNotDtCi() {
		return readChar(Pos.XZC001Q_NOT_DT_CI);
	}

	public void setXzc001qNotDt(String xzc001qNotDt) {
		writeString(Pos.XZC001Q_NOT_DT, xzc001qNotDt, Len.XZC001Q_NOT_DT);
	}

	/**Original name: XZC001Q-NOT-DT<br>*/
	public String getXzc001qNotDt() {
		return readString(Pos.XZC001Q_NOT_DT, Len.XZC001Q_NOT_DT);
	}

	public void setXzc001qActOwnCltIdCi(char xzc001qActOwnCltIdCi) {
		writeChar(Pos.XZC001Q_ACT_OWN_CLT_ID_CI, xzc001qActOwnCltIdCi);
	}

	/**Original name: XZC001Q-ACT-OWN-CLT-ID-CI<br>*/
	public char getXzc001qActOwnCltIdCi() {
		return readChar(Pos.XZC001Q_ACT_OWN_CLT_ID_CI);
	}

	public void setXzc001qActOwnCltId(String xzc001qActOwnCltId) {
		writeString(Pos.XZC001Q_ACT_OWN_CLT_ID, xzc001qActOwnCltId, Len.XZC001Q_ACT_OWN_CLT_ID);
	}

	/**Original name: XZC001Q-ACT-OWN-CLT-ID<br>*/
	public String getXzc001qActOwnCltId() {
		return readString(Pos.XZC001Q_ACT_OWN_CLT_ID, Len.XZC001Q_ACT_OWN_CLT_ID);
	}

	public void setXzc001qActOwnAdrIdCi(char xzc001qActOwnAdrIdCi) {
		writeChar(Pos.XZC001Q_ACT_OWN_ADR_ID_CI, xzc001qActOwnAdrIdCi);
	}

	/**Original name: XZC001Q-ACT-OWN-ADR-ID-CI<br>*/
	public char getXzc001qActOwnAdrIdCi() {
		return readChar(Pos.XZC001Q_ACT_OWN_ADR_ID_CI);
	}

	public void setXzc001qActOwnAdrId(String xzc001qActOwnAdrId) {
		writeString(Pos.XZC001Q_ACT_OWN_ADR_ID, xzc001qActOwnAdrId, Len.XZC001Q_ACT_OWN_ADR_ID);
	}

	/**Original name: XZC001Q-ACT-OWN-ADR-ID<br>*/
	public String getXzc001qActOwnAdrId() {
		return readString(Pos.XZC001Q_ACT_OWN_ADR_ID, Len.XZC001Q_ACT_OWN_ADR_ID);
	}

	public void setXzc001qEmpIdCi(char xzc001qEmpIdCi) {
		writeChar(Pos.XZC001Q_EMP_ID_CI, xzc001qEmpIdCi);
	}

	/**Original name: XZC001Q-EMP-ID-CI<br>*/
	public char getXzc001qEmpIdCi() {
		return readChar(Pos.XZC001Q_EMP_ID_CI);
	}

	public void setXzc001qEmpIdNi(char xzc001qEmpIdNi) {
		writeChar(Pos.XZC001Q_EMP_ID_NI, xzc001qEmpIdNi);
	}

	/**Original name: XZC001Q-EMP-ID-NI<br>*/
	public char getXzc001qEmpIdNi() {
		return readChar(Pos.XZC001Q_EMP_ID_NI);
	}

	public void setXzc001qEmpId(String xzc001qEmpId) {
		writeString(Pos.XZC001Q_EMP_ID, xzc001qEmpId, Len.XZC001Q_EMP_ID);
	}

	/**Original name: XZC001Q-EMP-ID<br>*/
	public String getXzc001qEmpId() {
		return readString(Pos.XZC001Q_EMP_ID, Len.XZC001Q_EMP_ID);
	}

	public void setXzc001qStaMdfTsCi(char xzc001qStaMdfTsCi) {
		writeChar(Pos.XZC001Q_STA_MDF_TS_CI, xzc001qStaMdfTsCi);
	}

	/**Original name: XZC001Q-STA-MDF-TS-CI<br>*/
	public char getXzc001qStaMdfTsCi() {
		return readChar(Pos.XZC001Q_STA_MDF_TS_CI);
	}

	public void setXzc001qStaMdfTs(String xzc001qStaMdfTs) {
		writeString(Pos.XZC001Q_STA_MDF_TS, xzc001qStaMdfTs, Len.XZC001Q_STA_MDF_TS);
	}

	/**Original name: XZC001Q-STA-MDF-TS<br>*/
	public String getXzc001qStaMdfTs() {
		return readString(Pos.XZC001Q_STA_MDF_TS, Len.XZC001Q_STA_MDF_TS);
	}

	public void setXzc001qActNotStaCdCi(char xzc001qActNotStaCdCi) {
		writeChar(Pos.XZC001Q_ACT_NOT_STA_CD_CI, xzc001qActNotStaCdCi);
	}

	/**Original name: XZC001Q-ACT-NOT-STA-CD-CI<br>*/
	public char getXzc001qActNotStaCdCi() {
		return readChar(Pos.XZC001Q_ACT_NOT_STA_CD_CI);
	}

	public void setXzc001qActNotStaCd(String xzc001qActNotStaCd) {
		writeString(Pos.XZC001Q_ACT_NOT_STA_CD, xzc001qActNotStaCd, Len.XZC001Q_ACT_NOT_STA_CD);
	}

	/**Original name: XZC001Q-ACT-NOT-STA-CD<br>*/
	public String getXzc001qActNotStaCd() {
		return readString(Pos.XZC001Q_ACT_NOT_STA_CD, Len.XZC001Q_ACT_NOT_STA_CD);
	}

	public void setXzc001qPdcNbrCi(char xzc001qPdcNbrCi) {
		writeChar(Pos.XZC001Q_PDC_NBR_CI, xzc001qPdcNbrCi);
	}

	/**Original name: XZC001Q-PDC-NBR-CI<br>*/
	public char getXzc001qPdcNbrCi() {
		return readChar(Pos.XZC001Q_PDC_NBR_CI);
	}

	public void setXzc001qPdcNbrNi(char xzc001qPdcNbrNi) {
		writeChar(Pos.XZC001Q_PDC_NBR_NI, xzc001qPdcNbrNi);
	}

	/**Original name: XZC001Q-PDC-NBR-NI<br>*/
	public char getXzc001qPdcNbrNi() {
		return readChar(Pos.XZC001Q_PDC_NBR_NI);
	}

	public void setXzc001qPdcNbr(String xzc001qPdcNbr) {
		writeString(Pos.XZC001Q_PDC_NBR, xzc001qPdcNbr, Len.XZC001Q_PDC_NBR);
	}

	/**Original name: XZC001Q-PDC-NBR<br>*/
	public String getXzc001qPdcNbr() {
		return readString(Pos.XZC001Q_PDC_NBR, Len.XZC001Q_PDC_NBR);
	}

	public void setXzc001qPdcNmCi(char xzc001qPdcNmCi) {
		writeChar(Pos.XZC001Q_PDC_NM_CI, xzc001qPdcNmCi);
	}

	/**Original name: XZC001Q-PDC-NM-CI<br>*/
	public char getXzc001qPdcNmCi() {
		return readChar(Pos.XZC001Q_PDC_NM_CI);
	}

	public void setXzc001qPdcNmNi(char xzc001qPdcNmNi) {
		writeChar(Pos.XZC001Q_PDC_NM_NI, xzc001qPdcNmNi);
	}

	/**Original name: XZC001Q-PDC-NM-NI<br>*/
	public char getXzc001qPdcNmNi() {
		return readChar(Pos.XZC001Q_PDC_NM_NI);
	}

	public void setXzc001qPdcNm(String xzc001qPdcNm) {
		writeString(Pos.XZC001Q_PDC_NM, xzc001qPdcNm, Len.XZC001Q_PDC_NM);
	}

	/**Original name: XZC001Q-PDC-NM<br>*/
	public String getXzc001qPdcNm() {
		return readString(Pos.XZC001Q_PDC_NM, Len.XZC001Q_PDC_NM);
	}

	public void setXzc001qSegCdCi(char xzc001qSegCdCi) {
		writeChar(Pos.XZC001Q_SEG_CD_CI, xzc001qSegCdCi);
	}

	/**Original name: XZC001Q-SEG-CD-CI<br>*/
	public char getXzc001qSegCdCi() {
		return readChar(Pos.XZC001Q_SEG_CD_CI);
	}

	public void setXzc001qSegCdNi(char xzc001qSegCdNi) {
		writeChar(Pos.XZC001Q_SEG_CD_NI, xzc001qSegCdNi);
	}

	/**Original name: XZC001Q-SEG-CD-NI<br>*/
	public char getXzc001qSegCdNi() {
		return readChar(Pos.XZC001Q_SEG_CD_NI);
	}

	public void setXzc001qSegCd(String xzc001qSegCd) {
		writeString(Pos.XZC001Q_SEG_CD, xzc001qSegCd, Len.XZC001Q_SEG_CD);
	}

	/**Original name: XZC001Q-SEG-CD<br>*/
	public String getXzc001qSegCd() {
		return readString(Pos.XZC001Q_SEG_CD, Len.XZC001Q_SEG_CD);
	}

	public void setXzc001qActTypCdCi(char xzc001qActTypCdCi) {
		writeChar(Pos.XZC001Q_ACT_TYP_CD_CI, xzc001qActTypCdCi);
	}

	/**Original name: XZC001Q-ACT-TYP-CD-CI<br>*/
	public char getXzc001qActTypCdCi() {
		return readChar(Pos.XZC001Q_ACT_TYP_CD_CI);
	}

	public void setXzc001qActTypCdNi(char xzc001qActTypCdNi) {
		writeChar(Pos.XZC001Q_ACT_TYP_CD_NI, xzc001qActTypCdNi);
	}

	/**Original name: XZC001Q-ACT-TYP-CD-NI<br>*/
	public char getXzc001qActTypCdNi() {
		return readChar(Pos.XZC001Q_ACT_TYP_CD_NI);
	}

	public void setXzc001qActTypCd(String xzc001qActTypCd) {
		writeString(Pos.XZC001Q_ACT_TYP_CD, xzc001qActTypCd, Len.XZC001Q_ACT_TYP_CD);
	}

	/**Original name: XZC001Q-ACT-TYP-CD<br>*/
	public String getXzc001qActTypCd() {
		return readString(Pos.XZC001Q_ACT_TYP_CD, Len.XZC001Q_ACT_TYP_CD);
	}

	public void setXzc001qTotFeeAmtCi(char xzc001qTotFeeAmtCi) {
		writeChar(Pos.XZC001Q_TOT_FEE_AMT_CI, xzc001qTotFeeAmtCi);
	}

	/**Original name: XZC001Q-TOT-FEE-AMT-CI<br>*/
	public char getXzc001qTotFeeAmtCi() {
		return readChar(Pos.XZC001Q_TOT_FEE_AMT_CI);
	}

	public void setXzc001qTotFeeAmtNi(char xzc001qTotFeeAmtNi) {
		writeChar(Pos.XZC001Q_TOT_FEE_AMT_NI, xzc001qTotFeeAmtNi);
	}

	/**Original name: XZC001Q-TOT-FEE-AMT-NI<br>*/
	public char getXzc001qTotFeeAmtNi() {
		return readChar(Pos.XZC001Q_TOT_FEE_AMT_NI);
	}

	public void setXzc001qTotFeeAmtSign(char xzc001qTotFeeAmtSign) {
		writeChar(Pos.XZC001Q_TOT_FEE_AMT_SIGN, xzc001qTotFeeAmtSign);
	}

	/**Original name: XZC001Q-TOT-FEE-AMT-SIGN<br>*/
	public char getXzc001qTotFeeAmtSign() {
		return readChar(Pos.XZC001Q_TOT_FEE_AMT_SIGN);
	}

	public void setXzc001qTotFeeAmt(AfDecimal xzc001qTotFeeAmt) {
		writeDecimal(Pos.XZC001Q_TOT_FEE_AMT, xzc001qTotFeeAmt.copy(), SignType.NO_SIGN);
	}

	/**Original name: XZC001Q-TOT-FEE-AMT<br>*/
	public AfDecimal getXzc001qTotFeeAmt() {
		return readDecimal(Pos.XZC001Q_TOT_FEE_AMT, Len.Int.XZC001Q_TOT_FEE_AMT, Len.Fract.XZC001Q_TOT_FEE_AMT, SignType.NO_SIGN);
	}

	public void setXzc001qStAbbCi(char xzc001qStAbbCi) {
		writeChar(Pos.XZC001Q_ST_ABB_CI, xzc001qStAbbCi);
	}

	/**Original name: XZC001Q-ST-ABB-CI<br>*/
	public char getXzc001qStAbbCi() {
		return readChar(Pos.XZC001Q_ST_ABB_CI);
	}

	public void setXzc001qStAbbNi(char xzc001qStAbbNi) {
		writeChar(Pos.XZC001Q_ST_ABB_NI, xzc001qStAbbNi);
	}

	/**Original name: XZC001Q-ST-ABB-NI<br>*/
	public char getXzc001qStAbbNi() {
		return readChar(Pos.XZC001Q_ST_ABB_NI);
	}

	public void setXzc001qStAbb(String xzc001qStAbb) {
		writeString(Pos.XZC001Q_ST_ABB, xzc001qStAbb, Len.XZC001Q_ST_ABB);
	}

	/**Original name: XZC001Q-ST-ABB<br>*/
	public String getXzc001qStAbb() {
		return readString(Pos.XZC001Q_ST_ABB, Len.XZC001Q_ST_ABB);
	}

	public void setXzc001qCerHldNotIndCi(char xzc001qCerHldNotIndCi) {
		writeChar(Pos.XZC001Q_CER_HLD_NOT_IND_CI, xzc001qCerHldNotIndCi);
	}

	/**Original name: XZC001Q-CER-HLD-NOT-IND-CI<br>*/
	public char getXzc001qCerHldNotIndCi() {
		return readChar(Pos.XZC001Q_CER_HLD_NOT_IND_CI);
	}

	public void setXzc001qCerHldNotIndNi(char xzc001qCerHldNotIndNi) {
		writeChar(Pos.XZC001Q_CER_HLD_NOT_IND_NI, xzc001qCerHldNotIndNi);
	}

	/**Original name: XZC001Q-CER-HLD-NOT-IND-NI<br>*/
	public char getXzc001qCerHldNotIndNi() {
		return readChar(Pos.XZC001Q_CER_HLD_NOT_IND_NI);
	}

	public void setXzc001qCerHldNotInd(char xzc001qCerHldNotInd) {
		writeChar(Pos.XZC001Q_CER_HLD_NOT_IND, xzc001qCerHldNotInd);
	}

	/**Original name: XZC001Q-CER-HLD-NOT-IND<br>*/
	public char getXzc001qCerHldNotInd() {
		return readChar(Pos.XZC001Q_CER_HLD_NOT_IND);
	}

	public void setXzc001qAddCncDayCi(char xzc001qAddCncDayCi) {
		writeChar(Pos.XZC001Q_ADD_CNC_DAY_CI, xzc001qAddCncDayCi);
	}

	/**Original name: XZC001Q-ADD-CNC-DAY-CI<br>*/
	public char getXzc001qAddCncDayCi() {
		return readChar(Pos.XZC001Q_ADD_CNC_DAY_CI);
	}

	public void setXzc001qAddCncDayNi(char xzc001qAddCncDayNi) {
		writeChar(Pos.XZC001Q_ADD_CNC_DAY_NI, xzc001qAddCncDayNi);
	}

	/**Original name: XZC001Q-ADD-CNC-DAY-NI<br>*/
	public char getXzc001qAddCncDayNi() {
		return readChar(Pos.XZC001Q_ADD_CNC_DAY_NI);
	}

	public void setXzc001qAddCncDaySign(char xzc001qAddCncDaySign) {
		writeChar(Pos.XZC001Q_ADD_CNC_DAY_SIGN, xzc001qAddCncDaySign);
	}

	/**Original name: XZC001Q-ADD-CNC-DAY-SIGN<br>*/
	public char getXzc001qAddCncDaySign() {
		return readChar(Pos.XZC001Q_ADD_CNC_DAY_SIGN);
	}

	public void setXzc001qAddCncDayFormatted(String xzc001qAddCncDay) {
		writeString(Pos.XZC001Q_ADD_CNC_DAY, Trunc.toUnsignedNumeric(xzc001qAddCncDay, Len.XZC001Q_ADD_CNC_DAY), Len.XZC001Q_ADD_CNC_DAY);
	}

	/**Original name: XZC001Q-ADD-CNC-DAY<br>*/
	public int getXzc001qAddCncDay() {
		return readNumDispUnsignedInt(Pos.XZC001Q_ADD_CNC_DAY, Len.XZC001Q_ADD_CNC_DAY);
	}

	public String getXzc001rAddCncDayFormatted() {
		return readFixedString(Pos.XZC001Q_ADD_CNC_DAY, Len.XZC001Q_ADD_CNC_DAY);
	}

	public void setXzc001qReaDesCi(char xzc001qReaDesCi) {
		writeChar(Pos.XZC001Q_REA_DES_CI, xzc001qReaDesCi);
	}

	/**Original name: XZC001Q-REA-DES-CI<br>*/
	public char getXzc001qReaDesCi() {
		return readChar(Pos.XZC001Q_REA_DES_CI);
	}

	public void setXzc001qReaDesNi(char xzc001qReaDesNi) {
		writeChar(Pos.XZC001Q_REA_DES_NI, xzc001qReaDesNi);
	}

	/**Original name: XZC001Q-REA-DES-NI<br>*/
	public char getXzc001qReaDesNi() {
		return readChar(Pos.XZC001Q_REA_DES_NI);
	}

	public void setXzc001qReaDes(String xzc001qReaDes) {
		writeString(Pos.XZC001Q_REA_DES, xzc001qReaDes, Len.XZC001Q_REA_DES);
	}

	/**Original name: XZC001Q-REA-DES<br>*/
	public String getXzc001qReaDes() {
		return readString(Pos.XZC001Q_REA_DES, Len.XZC001Q_REA_DES);
	}

	public void setXzc001qActNotTypDesc(String xzc001qActNotTypDesc) {
		writeString(Pos.XZC001Q_ACT_NOT_TYP_DESC, xzc001qActNotTypDesc, Len.XZC001Q_ACT_NOT_TYP_DESC);
	}

	/**Original name: XZC001Q-ACT-NOT-TYP-DESC<br>*/
	public String getXzc001qActNotTypDesc() {
		return readString(Pos.XZC001Q_ACT_NOT_TYP_DESC, Len.XZC001Q_ACT_NOT_TYP_DESC);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0C0001 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZC001Q_ACT_NOT_ROW = L_FW_REQ_XZ0C0001;
		public static final int XZC001Q_ACT_NOT_FIXED = XZC001Q_ACT_NOT_ROW;
		public static final int XZC001Q_ACT_NOT_CSUM = XZC001Q_ACT_NOT_FIXED;
		public static final int XZC001Q_CSR_ACT_NBR_KCRE = XZC001Q_ACT_NOT_CSUM + Len.XZC001Q_ACT_NOT_CSUM;
		public static final int XZC001Q_NOT_PRC_TS_KCRE = XZC001Q_CSR_ACT_NBR_KCRE + Len.XZC001Q_CSR_ACT_NBR_KCRE;
		public static final int XZC001Q_ACT_NOT_DATES = XZC001Q_NOT_PRC_TS_KCRE + Len.XZC001Q_NOT_PRC_TS_KCRE;
		public static final int XZC001Q_TRANS_PROCESS_DT = XZC001Q_ACT_NOT_DATES;
		public static final int XZC001Q_ACT_NOT_KEY = XZC001Q_TRANS_PROCESS_DT + Len.XZC001Q_TRANS_PROCESS_DT;
		public static final int XZC001Q_CSR_ACT_NBR = XZC001Q_ACT_NOT_KEY;
		public static final int XZC001Q_NOT_PRC_TS = XZC001Q_CSR_ACT_NBR + Len.XZC001Q_CSR_ACT_NBR;
		public static final int XZC001Q_ACT_NOT_KEY_CI = XZC001Q_NOT_PRC_TS + Len.XZC001Q_NOT_PRC_TS;
		public static final int XZC001Q_CSR_ACT_NBR_CI = XZC001Q_ACT_NOT_KEY_CI;
		public static final int XZC001Q_NOT_PRC_TS_CI = XZC001Q_CSR_ACT_NBR_CI + Len.XZC001Q_CSR_ACT_NBR_CI;
		public static final int XZC001Q_ACT_NOT_DATA = XZC001Q_NOT_PRC_TS_CI + Len.XZC001Q_NOT_PRC_TS_CI;
		public static final int XZC001Q_ACT_NOT_TYP_CD_CI = XZC001Q_ACT_NOT_DATA;
		public static final int XZC001Q_ACT_NOT_TYP_CD = XZC001Q_ACT_NOT_TYP_CD_CI + Len.XZC001Q_ACT_NOT_TYP_CD_CI;
		public static final int XZC001Q_NOT_DT_CI = XZC001Q_ACT_NOT_TYP_CD + Len.XZC001Q_ACT_NOT_TYP_CD;
		public static final int XZC001Q_NOT_DT = XZC001Q_NOT_DT_CI + Len.XZC001Q_NOT_DT_CI;
		public static final int XZC001Q_ACT_OWN_CLT_ID_CI = XZC001Q_NOT_DT + Len.XZC001Q_NOT_DT;
		public static final int XZC001Q_ACT_OWN_CLT_ID = XZC001Q_ACT_OWN_CLT_ID_CI + Len.XZC001Q_ACT_OWN_CLT_ID_CI;
		public static final int XZC001Q_ACT_OWN_ADR_ID_CI = XZC001Q_ACT_OWN_CLT_ID + Len.XZC001Q_ACT_OWN_CLT_ID;
		public static final int XZC001Q_ACT_OWN_ADR_ID = XZC001Q_ACT_OWN_ADR_ID_CI + Len.XZC001Q_ACT_OWN_ADR_ID_CI;
		public static final int XZC001Q_EMP_ID_CI = XZC001Q_ACT_OWN_ADR_ID + Len.XZC001Q_ACT_OWN_ADR_ID;
		public static final int XZC001Q_EMP_ID_NI = XZC001Q_EMP_ID_CI + Len.XZC001Q_EMP_ID_CI;
		public static final int XZC001Q_EMP_ID = XZC001Q_EMP_ID_NI + Len.XZC001Q_EMP_ID_NI;
		public static final int XZC001Q_STA_MDF_TS_CI = XZC001Q_EMP_ID + Len.XZC001Q_EMP_ID;
		public static final int XZC001Q_STA_MDF_TS = XZC001Q_STA_MDF_TS_CI + Len.XZC001Q_STA_MDF_TS_CI;
		public static final int XZC001Q_ACT_NOT_STA_CD_CI = XZC001Q_STA_MDF_TS + Len.XZC001Q_STA_MDF_TS;
		public static final int XZC001Q_ACT_NOT_STA_CD = XZC001Q_ACT_NOT_STA_CD_CI + Len.XZC001Q_ACT_NOT_STA_CD_CI;
		public static final int XZC001Q_PDC_NBR_CI = XZC001Q_ACT_NOT_STA_CD + Len.XZC001Q_ACT_NOT_STA_CD;
		public static final int XZC001Q_PDC_NBR_NI = XZC001Q_PDC_NBR_CI + Len.XZC001Q_PDC_NBR_CI;
		public static final int XZC001Q_PDC_NBR = XZC001Q_PDC_NBR_NI + Len.XZC001Q_PDC_NBR_NI;
		public static final int XZC001Q_PDC_NM_CI = XZC001Q_PDC_NBR + Len.XZC001Q_PDC_NBR;
		public static final int XZC001Q_PDC_NM_NI = XZC001Q_PDC_NM_CI + Len.XZC001Q_PDC_NM_CI;
		public static final int XZC001Q_PDC_NM = XZC001Q_PDC_NM_NI + Len.XZC001Q_PDC_NM_NI;
		public static final int XZC001Q_SEG_CD_CI = XZC001Q_PDC_NM + Len.XZC001Q_PDC_NM;
		public static final int XZC001Q_SEG_CD_NI = XZC001Q_SEG_CD_CI + Len.XZC001Q_SEG_CD_CI;
		public static final int XZC001Q_SEG_CD = XZC001Q_SEG_CD_NI + Len.XZC001Q_SEG_CD_NI;
		public static final int XZC001Q_ACT_TYP_CD_CI = XZC001Q_SEG_CD + Len.XZC001Q_SEG_CD;
		public static final int XZC001Q_ACT_TYP_CD_NI = XZC001Q_ACT_TYP_CD_CI + Len.XZC001Q_ACT_TYP_CD_CI;
		public static final int XZC001Q_ACT_TYP_CD = XZC001Q_ACT_TYP_CD_NI + Len.XZC001Q_ACT_TYP_CD_NI;
		public static final int XZC001Q_TOT_FEE_AMT_CI = XZC001Q_ACT_TYP_CD + Len.XZC001Q_ACT_TYP_CD;
		public static final int XZC001Q_TOT_FEE_AMT_NI = XZC001Q_TOT_FEE_AMT_CI + Len.XZC001Q_TOT_FEE_AMT_CI;
		public static final int XZC001Q_TOT_FEE_AMT_SIGN = XZC001Q_TOT_FEE_AMT_NI + Len.XZC001Q_TOT_FEE_AMT_NI;
		public static final int XZC001Q_TOT_FEE_AMT = XZC001Q_TOT_FEE_AMT_SIGN + Len.XZC001Q_TOT_FEE_AMT_SIGN;
		public static final int XZC001Q_ST_ABB_CI = XZC001Q_TOT_FEE_AMT + Len.XZC001Q_TOT_FEE_AMT;
		public static final int XZC001Q_ST_ABB_NI = XZC001Q_ST_ABB_CI + Len.XZC001Q_ST_ABB_CI;
		public static final int XZC001Q_ST_ABB = XZC001Q_ST_ABB_NI + Len.XZC001Q_ST_ABB_NI;
		public static final int XZC001Q_CER_HLD_NOT_IND_CI = XZC001Q_ST_ABB + Len.XZC001Q_ST_ABB;
		public static final int XZC001Q_CER_HLD_NOT_IND_NI = XZC001Q_CER_HLD_NOT_IND_CI + Len.XZC001Q_CER_HLD_NOT_IND_CI;
		public static final int XZC001Q_CER_HLD_NOT_IND = XZC001Q_CER_HLD_NOT_IND_NI + Len.XZC001Q_CER_HLD_NOT_IND_NI;
		public static final int XZC001Q_ADD_CNC_DAY_CI = XZC001Q_CER_HLD_NOT_IND + Len.XZC001Q_CER_HLD_NOT_IND;
		public static final int XZC001Q_ADD_CNC_DAY_NI = XZC001Q_ADD_CNC_DAY_CI + Len.XZC001Q_ADD_CNC_DAY_CI;
		public static final int XZC001Q_ADD_CNC_DAY_SIGN = XZC001Q_ADD_CNC_DAY_NI + Len.XZC001Q_ADD_CNC_DAY_NI;
		public static final int XZC001Q_ADD_CNC_DAY = XZC001Q_ADD_CNC_DAY_SIGN + Len.XZC001Q_ADD_CNC_DAY_SIGN;
		public static final int XZC001Q_REA_DES_CI = XZC001Q_ADD_CNC_DAY + Len.XZC001Q_ADD_CNC_DAY;
		public static final int XZC001Q_REA_DES_NI = XZC001Q_REA_DES_CI + Len.XZC001Q_REA_DES_CI;
		public static final int XZC001Q_REA_DES = XZC001Q_REA_DES_NI + Len.XZC001Q_REA_DES_NI;
		public static final int XZC001Q_EXTENSION_FIELDS = XZC001Q_REA_DES + Len.XZC001Q_REA_DES;
		public static final int XZC001Q_ACT_NOT_TYP_DESC = XZC001Q_EXTENSION_FIELDS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC001Q_ACT_NOT_CSUM = 9;
		public static final int XZC001Q_CSR_ACT_NBR_KCRE = 32;
		public static final int XZC001Q_NOT_PRC_TS_KCRE = 32;
		public static final int XZC001Q_TRANS_PROCESS_DT = 10;
		public static final int XZC001Q_CSR_ACT_NBR = 9;
		public static final int XZC001Q_NOT_PRC_TS = 26;
		public static final int XZC001Q_CSR_ACT_NBR_CI = 1;
		public static final int XZC001Q_NOT_PRC_TS_CI = 1;
		public static final int XZC001Q_ACT_NOT_TYP_CD_CI = 1;
		public static final int XZC001Q_ACT_NOT_TYP_CD = 5;
		public static final int XZC001Q_NOT_DT_CI = 1;
		public static final int XZC001Q_NOT_DT = 10;
		public static final int XZC001Q_ACT_OWN_CLT_ID_CI = 1;
		public static final int XZC001Q_ACT_OWN_CLT_ID = 64;
		public static final int XZC001Q_ACT_OWN_ADR_ID_CI = 1;
		public static final int XZC001Q_ACT_OWN_ADR_ID = 64;
		public static final int XZC001Q_EMP_ID_CI = 1;
		public static final int XZC001Q_EMP_ID_NI = 1;
		public static final int XZC001Q_EMP_ID = 6;
		public static final int XZC001Q_STA_MDF_TS_CI = 1;
		public static final int XZC001Q_STA_MDF_TS = 26;
		public static final int XZC001Q_ACT_NOT_STA_CD_CI = 1;
		public static final int XZC001Q_ACT_NOT_STA_CD = 2;
		public static final int XZC001Q_PDC_NBR_CI = 1;
		public static final int XZC001Q_PDC_NBR_NI = 1;
		public static final int XZC001Q_PDC_NBR = 5;
		public static final int XZC001Q_PDC_NM_CI = 1;
		public static final int XZC001Q_PDC_NM_NI = 1;
		public static final int XZC001Q_PDC_NM = 120;
		public static final int XZC001Q_SEG_CD_CI = 1;
		public static final int XZC001Q_SEG_CD_NI = 1;
		public static final int XZC001Q_SEG_CD = 3;
		public static final int XZC001Q_ACT_TYP_CD_CI = 1;
		public static final int XZC001Q_ACT_TYP_CD_NI = 1;
		public static final int XZC001Q_ACT_TYP_CD = 2;
		public static final int XZC001Q_TOT_FEE_AMT_CI = 1;
		public static final int XZC001Q_TOT_FEE_AMT_NI = 1;
		public static final int XZC001Q_TOT_FEE_AMT_SIGN = 1;
		public static final int XZC001Q_TOT_FEE_AMT = 10;
		public static final int XZC001Q_ST_ABB_CI = 1;
		public static final int XZC001Q_ST_ABB_NI = 1;
		public static final int XZC001Q_ST_ABB = 2;
		public static final int XZC001Q_CER_HLD_NOT_IND_CI = 1;
		public static final int XZC001Q_CER_HLD_NOT_IND_NI = 1;
		public static final int XZC001Q_CER_HLD_NOT_IND = 1;
		public static final int XZC001Q_ADD_CNC_DAY_CI = 1;
		public static final int XZC001Q_ADD_CNC_DAY_NI = 1;
		public static final int XZC001Q_ADD_CNC_DAY_SIGN = 1;
		public static final int XZC001Q_ADD_CNC_DAY = 5;
		public static final int XZC001Q_REA_DES_CI = 1;
		public static final int XZC001Q_REA_DES_NI = 1;
		public static final int XZC001Q_REA_DES = 500;
		public static final int XZC001Q_ACT_NOT_FIXED = XZC001Q_ACT_NOT_CSUM + XZC001Q_CSR_ACT_NBR_KCRE + XZC001Q_NOT_PRC_TS_KCRE;
		public static final int XZC001Q_ACT_NOT_DATES = XZC001Q_TRANS_PROCESS_DT;
		public static final int XZC001Q_ACT_NOT_KEY = XZC001Q_CSR_ACT_NBR + XZC001Q_NOT_PRC_TS;
		public static final int XZC001Q_ACT_NOT_KEY_CI = XZC001Q_CSR_ACT_NBR_CI + XZC001Q_NOT_PRC_TS_CI;
		public static final int XZC001Q_ACT_NOT_DATA = XZC001Q_ACT_NOT_TYP_CD_CI + XZC001Q_ACT_NOT_TYP_CD + XZC001Q_NOT_DT_CI + XZC001Q_NOT_DT
				+ XZC001Q_ACT_OWN_CLT_ID_CI + XZC001Q_ACT_OWN_CLT_ID + XZC001Q_ACT_OWN_ADR_ID_CI + XZC001Q_ACT_OWN_ADR_ID + XZC001Q_EMP_ID_CI
				+ XZC001Q_EMP_ID_NI + XZC001Q_EMP_ID + XZC001Q_STA_MDF_TS_CI + XZC001Q_STA_MDF_TS + XZC001Q_ACT_NOT_STA_CD_CI + XZC001Q_ACT_NOT_STA_CD
				+ XZC001Q_PDC_NBR_CI + XZC001Q_PDC_NBR_NI + XZC001Q_PDC_NBR + XZC001Q_PDC_NM_CI + XZC001Q_PDC_NM_NI + XZC001Q_PDC_NM
				+ XZC001Q_SEG_CD_CI + XZC001Q_SEG_CD_NI + XZC001Q_SEG_CD + XZC001Q_ACT_TYP_CD_CI + XZC001Q_ACT_TYP_CD_NI + XZC001Q_ACT_TYP_CD
				+ XZC001Q_TOT_FEE_AMT_CI + XZC001Q_TOT_FEE_AMT_NI + XZC001Q_TOT_FEE_AMT_SIGN + XZC001Q_TOT_FEE_AMT + XZC001Q_ST_ABB_CI
				+ XZC001Q_ST_ABB_NI + XZC001Q_ST_ABB + XZC001Q_CER_HLD_NOT_IND_CI + XZC001Q_CER_HLD_NOT_IND_NI + XZC001Q_CER_HLD_NOT_IND
				+ XZC001Q_ADD_CNC_DAY_CI + XZC001Q_ADD_CNC_DAY_NI + XZC001Q_ADD_CNC_DAY_SIGN + XZC001Q_ADD_CNC_DAY + XZC001Q_REA_DES_CI
				+ XZC001Q_REA_DES_NI + XZC001Q_REA_DES;
		public static final int XZC001Q_ACT_NOT_TYP_DESC = 35;
		public static final int XZC001Q_EXTENSION_FIELDS = XZC001Q_ACT_NOT_TYP_DESC;
		public static final int XZC001Q_ACT_NOT_ROW = XZC001Q_ACT_NOT_FIXED + XZC001Q_ACT_NOT_DATES + XZC001Q_ACT_NOT_KEY + XZC001Q_ACT_NOT_KEY_CI
				+ XZC001Q_ACT_NOT_DATA + XZC001Q_EXTENSION_FIELDS;
		public static final int L_FW_REQ_XZ0C0001 = XZC001Q_ACT_NOT_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0C0001;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZC001Q_TOT_FEE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZC001Q_TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
