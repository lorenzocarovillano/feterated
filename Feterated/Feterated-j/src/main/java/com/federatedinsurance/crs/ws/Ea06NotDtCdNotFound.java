/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-06-NOT-DT-CD-NOT-FOUND<br>
 * Variable: EA-06-NOT-DT-CD-NOT-FOUND from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea06NotDtCdNotFound {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-06-NOT-DT-CD-NOT-FOUND
	private String flr1 = " PGM = XZ004000";
	//Original name: FILLER-EA-06-NOT-DT-CD-NOT-FOUND-1
	private String flr2 = " -";
	//Original name: FILLER-EA-06-NOT-DT-CD-NOT-FOUND-2
	private String flr3 = "NOTIFICATION";
	//Original name: FILLER-EA-06-NOT-DT-CD-NOT-FOUND-3
	private String flr4 = "DATE OR CODE";
	//Original name: FILLER-EA-06-NOT-DT-CD-NOT-FOUND-4
	private String flr5 = "IS MISSING.";
	//Original name: FILLER-EA-06-NOT-DT-CD-NOT-FOUND-5
	private String flr6 = "POL-NBR=";
	//Original name: EA-06-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: FILLER-EA-06-NOT-DT-CD-NOT-FOUND-6
	private String flr7 = " POL-EFF-DT=";
	//Original name: EA-06-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: FILLER-EA-06-NOT-DT-CD-NOT-FOUND-7
	private String flr8 = " STA-MDF-TS=";
	//Original name: EA-06-STA-MDF-TS
	private String staMdfTs = DefaultValues.stringVal(Len.STA_MDF_TS);

	//==== METHODS ====
	public String getEa06NotDtCdNotFoundFormatted() {
		return MarshalByteExt.bufferToStr(getEa06NotDtCdNotFoundBytes());
	}

	public byte[] getEa06NotDtCdNotFoundBytes() {
		byte[] buffer = new byte[Len.EA06_NOT_DT_CD_NOT_FOUND];
		return getEa06NotDtCdNotFoundBytes(buffer, 1);
	}

	public byte[] getEa06NotDtCdNotFoundBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, staMdfTs, Len.STA_MDF_TS);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setStaMdfTs(String staMdfTs) {
		this.staMdfTs = Functions.subString(staMdfTs, Len.STA_MDF_TS);
	}

	public String getStaMdfTs() {
		return this.staMdfTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 16;
		public static final int POL_EFF_DT = 10;
		public static final int STA_MDF_TS = 26;
		public static final int FLR1 = 15;
		public static final int FLR2 = 3;
		public static final int FLR3 = 13;
		public static final int FLR5 = 12;
		public static final int FLR6 = 9;
		public static final int EA06_NOT_DT_CD_NOT_FOUND = POL_NBR + POL_EFF_DT + STA_MDF_TS + FLR1 + FLR2 + 4 * FLR3 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
