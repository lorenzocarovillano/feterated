/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalUniversalCtV;

/**Original name: DCLGEN-HAL-UNIVERSAL-CT2<br>
 * Variable: DCLGEN-HAL-UNIVERSAL-CT2 from copybook HALLGUC2<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclgenHalUniversalCt2 implements IHalUniversalCtV {

	//==== PROPERTIES ====
	//Original name: HUC2H-ENTRY-KEY-CD
	private String entryKeyCd = DefaultValues.stringVal(Len.ENTRY_KEY_CD);
	//Original name: HUC2H-TBL-LBL-TXT
	private String tblLblTxt = DefaultValues.stringVal(Len.TBL_LBL_TXT);
	//Original name: HUC2H-ENTRY-DTA-TXT
	private String entryDtaTxt = DefaultValues.stringVal(Len.ENTRY_DTA_TXT);

	//==== METHODS ====
	public void setEntryKeyCd(String entryKeyCd) {
		this.entryKeyCd = Functions.subString(entryKeyCd, Len.ENTRY_KEY_CD);
	}

	public String getEntryKeyCd() {
		return this.entryKeyCd;
	}

	public String getEntryKeyCdFormatted() {
		return Functions.padBlanks(getEntryKeyCd(), Len.ENTRY_KEY_CD);
	}

	public void setTblLblTxt(String tblLblTxt) {
		this.tblLblTxt = Functions.subString(tblLblTxt, Len.TBL_LBL_TXT);
	}

	public String getTblLblTxt() {
		return this.tblLblTxt;
	}

	public String getTblLblTxtFormatted() {
		return Functions.padBlanks(getTblLblTxt(), Len.TBL_LBL_TXT);
	}

	public void setEntryDtaTxt(String entryDtaTxt) {
		this.entryDtaTxt = Functions.subString(entryDtaTxt, Len.ENTRY_DTA_TXT);
	}

	public String getEntryDtaTxt() {
		return this.entryDtaTxt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ENTRY_KEY_CD = 150;
		public static final int TBL_LBL_TXT = 32;
		public static final int EFFECTIVE_DT = 10;
		public static final int ENTRY_DTA_TXT = 200;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
