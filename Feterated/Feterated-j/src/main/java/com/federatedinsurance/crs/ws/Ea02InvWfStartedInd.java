/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-INV-WF-STARTED-IND<br>
 * Variable: EA-02-INV-WF-STARTED-IND from program XZ0D0002<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02InvWfStartedInd {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND
	private String flr1 = "Workflow";
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-1
	private String flr2 = "started";
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-2
	private String flr3 = "indicator is";
	//Original name: EA-02-WF-STARTED-IND
	private char wfStartedInd = DefaultValues.CHAR_VAL;
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-3
	private String flr4 = ", but must be";
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-4
	private String flr5 = "Y or N for";
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-5
	private String flr6 = "account";
	//Original name: EA-02-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-6
	private String flr7 = ", process time";
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-7
	private String flr8 = "stamp date";
	//Original name: EA-02-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-8
	private String flr9 = ", policy number";
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-9
	private char flr10 = ' ';
	//Original name: EA-02-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: FILLER-EA-02-INV-WF-STARTED-IND-10
	private char flr11 = '.';

	//==== METHODS ====
	public String getEa02InvWfStartedIndFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvWfStartedIndBytes());
	}

	public byte[] getEa02InvWfStartedIndBytes() {
		byte[] buffer = new byte[Len.EA02_INV_WF_STARTED_IND];
		return getEa02InvWfStartedIndBytes(buffer, 1);
	}

	public byte[] getEa02InvWfStartedIndBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeChar(buffer, position, wfStartedInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR9);
		position += Len.FLR9;
		MarshalByte.writeChar(buffer, position, flr10);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeChar(buffer, position, flr11);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setWfStartedInd(char wfStartedInd) {
		this.wfStartedInd = wfStartedInd;
	}

	public char getWfStartedInd() {
		return this.wfStartedInd;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public char getFlr10() {
		return this.flr10;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public char getFlr11() {
		return this.flr11;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int POL_NBR = 25;
		public static final int FLR1 = 9;
		public static final int FLR2 = 8;
		public static final int FLR3 = 13;
		public static final int WF_STARTED_IND = 1;
		public static final int FLR4 = 14;
		public static final int FLR5 = 11;
		public static final int FLR9 = 15;
		public static final int FLR10 = 1;
		public static final int EA02_INV_WF_STARTED_IND = WF_STARTED_IND + CSR_ACT_NBR + NOT_PRC_TS + POL_NBR + 2 * FLR10 + FLR1 + 2 * FLR2 + FLR3
				+ 2 * FLR4 + 2 * FLR5 + FLR9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
