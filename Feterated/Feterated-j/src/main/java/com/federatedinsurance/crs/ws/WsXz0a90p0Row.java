/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90P0-ROW<br>
 * Variable: WS-XZ0A90P0-ROW from program XZ0B90P0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90p0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9P0-MAX-POL-ROWS
	private short maxPolRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA9P0-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA9P0-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA9P0-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90P0_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90p0RowBytes(buf);
	}

	public String getWsXz0a90p0RowFormatted() {
		return getGetCertPolListKeyFormatted();
	}

	public void setWsXz0a90p0RowBytes(byte[] buffer) {
		setWsXz0a90p0RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90p0RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90P0_ROW];
		return getWsXz0a90p0RowBytes(buffer, 1);
	}

	public void setWsXz0a90p0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetCertPolListKeyBytes(buffer, position);
	}

	public byte[] getWsXz0a90p0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetCertPolListKeyBytes(buffer, position);
		return buffer;
	}

	public String getGetCertPolListKeyFormatted() {
		return MarshalByteExt.bufferToStr(getGetCertPolListKeyBytes());
	}

	/**Original name: XZA9P0-GET-CERT-POL-LIST-KEY<br>
	 * <pre>*************************************************************
	 *  XZ0A90P0 - BPO COPYBOOK FOR                                *
	 *             UOW : XY_GET_NOT_CER_POL_LIST                   *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  14APR2009 E404KXS    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetCertPolListKeyBytes() {
		byte[] buffer = new byte[Len.GET_CERT_POL_LIST_KEY];
		return getGetCertPolListKeyBytes(buffer, 1);
	}

	public void setGetCertPolListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		maxPolRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
	}

	public byte[] getGetCertPolListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxPolRows);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setMaxPolRows(short maxPolRows) {
		this.maxPolRows = maxPolRows;
	}

	public short getMaxPolRows() {
		return this.maxPolRows;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90p0RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_POL_ROWS = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int USERID = 8;
		public static final int GET_CERT_POL_LIST_KEY = MAX_POL_ROWS + CSR_ACT_NBR + NOT_PRC_TS + USERID;
		public static final int WS_XZ0A90P0_ROW = GET_CERT_POL_LIST_KEY;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
