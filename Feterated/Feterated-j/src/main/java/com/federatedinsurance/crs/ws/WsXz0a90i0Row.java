/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90I0-ROW<br>
 * Variable: WS-XZ0A90I0-ROW from program XZ0B90I0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90i0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9I0-MAX-FRM-REC-ROWS
	private int maxFrmRecRows = DefaultValues.BIN_INT_VAL;
	//Original name: XZA9I0-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA9I0-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90I0_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90i0RowBytes(buf);
	}

	public String getWsXz0a90i0RowFormatted() {
		return getGetFrmRecListKeyFormatted();
	}

	public void setWsXz0a90i0RowBytes(byte[] buffer) {
		setWsXz0a90i0RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90i0RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90I0_ROW];
		return getWsXz0a90i0RowBytes(buffer, 1);
	}

	public void setWsXz0a90i0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetFrmRecListKeyBytes(buffer, position);
	}

	public byte[] getWsXz0a90i0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetFrmRecListKeyBytes(buffer, position);
		return buffer;
	}

	public String getGetFrmRecListKeyFormatted() {
		return MarshalByteExt.bufferToStr(getGetFrmRecListKeyBytes());
	}

	/**Original name: XZA9I0-GET-FRM-REC-LIST-KEY<br>
	 * <pre>*************************************************************
	 *  XZ0A90I0 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_FRM_REC_LIST                       *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  04MAR2009 E404KXS    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetFrmRecListKeyBytes() {
		byte[] buffer = new byte[Len.GET_FRM_REC_LIST_KEY];
		return getGetFrmRecListKeyBytes(buffer, 1);
	}

	public void setGetFrmRecListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		maxFrmRecRows = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
	}

	public byte[] getGetFrmRecListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryInt(buffer, position, maxFrmRecRows);
		position += Types.INT_SIZE;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		return buffer;
	}

	public void setMaxFrmRecRows(int maxFrmRecRows) {
		this.maxFrmRecRows = maxFrmRecRows;
	}

	public int getMaxFrmRecRows() {
		return this.maxFrmRecRows;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90i0RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_FRM_REC_ROWS = 4;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int GET_FRM_REC_LIST_KEY = MAX_FRM_REC_ROWS + CSR_ACT_NBR + NOT_PRC_TS;
		public static final int WS_XZ0A90I0_ROW = GET_FRM_REC_LIST_KEY;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
