/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WS-MISC-WORK-FLDS<br>
 * Variable: WS-MISC-WORK-FLDS from program XZ0G90M1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsMiscWorkFldsXz0g90m1 {

	//==== PROPERTIES ====
	//Original name: WS-LIST-MAX
	private short listMax = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-DATA-SIZE
	private int dataSize = DefaultValues.BIN_INT_VAL;
	//Original name: WS-POINTER
	private int pointer = DefaultValues.BIN_INT_VAL;

	//==== METHODS ====
	public void setListMax(short listMax) {
		this.listMax = listMax;
	}

	public short getListMax() {
		return this.listMax;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public String getResponseCode2Formatted() {
		return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.BINARY)).format(getResponseCode2()).toString();
	}

	public void setDataSize(int dataSize) {
		this.dataSize = dataSize;
	}

	public int getDataSize() {
		return this.dataSize;
	}

	public void setPointer(int pointer) {
		this.pointer = pointer;
	}

	public int getPointer() {
		return this.pointer;
	}
}
