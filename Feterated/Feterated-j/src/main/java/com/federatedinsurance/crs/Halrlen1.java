/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.Halrlen1Data;
import com.federatedinsurance.crs.ws.LiString;
import com.federatedinsurance.crs.ws.LiStringLen;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;

/**Original name: HALRLEN1<br>
 * <pre>AUTHOR.
 *                   J. A. FULCHER.
 * DATE-WRITTEN.
 *                   SEP 2000.
 * DATE-COMPILED.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  DETERMINE THE LENGTH OF A FIELD.           **
 * *                  FIELD LENGTH MUST BE BETWEEN 1 AND 999.    **
 * *                                                             **
 * * PLATFORM - IBM MAINFRAME                                    **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -        DETERMINE THE LENGTH OF A FIELD            **
 * *                  I.E. ESTABLISH THE POSITION OF THE LAST    **
 * *                  NON SPACE CHARACTER IN A STRING.           **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED AS FOLLOWS:   **
 * *                       1) STATICALLY CALLED BY A MODULE      **
 * *                                                             **
 * * DATA ACCESS METHODS - DATA PASSED IN LINKAGE                **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #     DATE       PRGMR   DESCRIPTION                     **
 * * -------- ---------  ------  --------------------------------**
 * * SAVANNAH 23SEP00    JAF     NEW ROUTINE.                    **
 * *                                                             **
 * ****************************************************************</pre>*/
public class Halrlen1 extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private Halrlen1Data ws = new Halrlen1Data();
	//Original name: LI-STRING
	private LiString liString;
	//Original name: LI-STRING-LEN
	private LiStringLen liStringLen;
	//Original name: LO-STRING-LEN
	private LiStringLen loStringLen;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LiString liString, LiStringLen liStringLen, LiStringLen loStringLen) {
		this.liString = liString;
		this.liStringLen = liStringLen;
		this.loStringLen = loStringLen;
		s0000Main();
		mainX();
		return 0;
	}

	public static Halrlen1 getInstance() {
		return (Programs.getInstance(Halrlen1.class));
	}

	/**Original name: S0000-MAIN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONTROLS PROCESSING                                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void s0000Main() {
		// COB_CODE: PERFORM 0100-INITIALIZATION.
		initialization();
		// COB_CODE: IF ERROR-FLAGGED
		//               GO TO 0000-MAIN-X
		//           END-IF.
		if (ws.getWsErrorFlag().isFlagged()) {
			// COB_CODE: GO TO 0000-MAIN-X
			mainX();
		}
		// COB_CODE: PERFORM 0200-DETERMINE-LENGTH.
		determineLength();
	}

	/**Original name: 0000-MAIN-X<br>*/
	private void mainX() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 0100-INITIALIZATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PERFORM INITIAL PROCESSING.                                    *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void initialization() {
		// COB_CODE: SET ERROR-NOT-FLAGGED TO TRUE.
		ws.getWsErrorFlag().setNotFlagged();
		//* VALIDATE INPUT STRING LENGTH.
		// COB_CODE: IF LI-STRING-LEN < 0
		//               GO TO 0100-INITIALIZATION-X
		//           END-IF.
		if (liStringLen.getLiStringLen() < 0) {
			// COB_CODE: SET ERROR-FLAGGED TO TRUE
			ws.getWsErrorFlag().setFlagged();
			// COB_CODE: MOVE 0 TO LO-STRING-LEN
			loStringLen.setLiStringLen(((short) 0));
			// COB_CODE: GO TO 0100-INITIALIZATION-X
			return;
		}
		//* DETERMINE INPUT STRING LENGTH.
		// COB_CODE: IF LI-STRING-LEN > 999
		//               MOVE 999 TO WS-STRING-LEN
		//           ELSE
		//               MOVE LI-STRING-LEN TO WS-STRING-LEN
		//           END-IF.
		if (liStringLen.getLiStringLen() > 999) {
			// COB_CODE: MOVE 999 TO WS-STRING-LEN
			ws.setWsStringLen(((short) 999));
		} else {
			// COB_CODE: MOVE LI-STRING-LEN TO WS-STRING-LEN
			ws.setWsStringLen(liStringLen.getLiStringLen());
		}
	}

	/**Original name: 0200-DETERMINE-LENGTH_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  DETERMINE LAST NON SPACE CHARACTER IN INPUT STRING.            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void determineLength() {
		// COB_CODE: SET STRING-X-IND TO WS-STRING-LEN.
		ws.setStringXInd(ws.getWsStringLen());
		// COB_CODE: PERFORM VARYING WS-POSN
		//                      FROM WS-STRING-LEN
		//                        BY -1
		//                     UNTIL WS-POSN < 1
		//                        OR LI-STRING-X(STRING-X-IND) NOT = SPACE
		//               SET STRING-X-IND DOWN BY 1
		//           END-PERFORM.
		ws.setWsPosn(ws.getWsStringLen());
		while (!(ws.getWsPosn() < 1 || !Conditions.eq(liString.getLiStringX(ws.getStringXInd()), Types.SPACE_CHAR))) {
			// COB_CODE: SET STRING-X-IND DOWN BY 1
			ws.setStringXInd(Trunc.toInt(ws.getStringXInd() - 1, 9));
			ws.setWsPosn(Trunc.toShort(ws.getWsPosn() + (-1), 4));
		}
		// COB_CODE: MOVE WS-POSN TO LO-STRING-LEN.
		loStringLen.setLiStringLen(ws.getWsPosn());
	}
}
