/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xz0690co;
import com.federatedinsurance.crs.copy.Xzc0690iofCallableInputs;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.redefines.PpcErrorHandlingParms;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B90S0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b90s0Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b90s0 constantFields = new ConstantFieldsXz0b90s0();
	//Original name: EA-01-MAX-ROWS-RETURNED-MSG
	private Ea01MaxRowsReturnedMsg ea01MaxRowsReturnedMsg = new Ea01MaxRowsReturnedMsg();
	//Original name: FILLER-EA-02-NO-ACY-POL-FOUND-MSG
	private String flr1 = "No active";
	//Original name: FILLER-EA-02-NO-ACY-POL-FOUND-MSG-1
	private String flr2 = "policies found";
	//Original name: FILLER-EA-02-NO-ACY-POL-FOUND-MSG-2
	private String flr3 = "for Account =";
	//Original name: EA-02-ACT-NBR
	private String ea02ActNbr = DefaultValues.stringVal(Len.EA02_ACT_NBR);
	//Original name: FILLER-EA-02-NO-ACY-POL-FOUND-MSG-3
	private char flr4 = '.';
	//Original name: SUBSCRIPTS
	private SubscriptsXz0b90s0 subscripts = new SubscriptsXz0b90s0();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b90s0 workingStorageArea = new WorkingStorageAreaXz0b90s0();
	//Original name: WS-XZC010C1-ROW
	private DfhcommareaXzc01090 wsXzc010c1Row = new DfhcommareaXzc01090();
	//Original name: WS-XZC020C1-ROW
	private DfhcommareaXzc02090 wsXzc020c1Row = new DfhcommareaXzc02090();
	//Original name: WS-XZ0A90S0-ROW
	private WsXz0a90s0Row wsXz0a90s0Row = new WsXz0a90s0Row();
	//Original name: WS-XZ0A90S1-ROW
	private WsXz0a90s1Row wsXz0a90s1Row = new WsXz0a90s1Row();
	/**Original name: PPC-ERROR-HANDLING-PARMS<br>
	 * <pre>* ERROR HANDLING</pre>*/
	private PpcErrorHandlingParms ppcErrorHandlingParms = new PpcErrorHandlingParms();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: XZC0690I
	private Xzc0690iofCallableInputs xzc0690i = new Xzc0690iofCallableInputs();
	//Original name: XZ0690CO
	private Xz0690co xz0690co = new Xz0690co();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();
	//Original name: INT-REGISTER1
	private short intRegister1 = DefaultValues.SHORT_VAL;

	//==== METHODS ====
	public String getEa02NoAcyPolFoundMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02NoAcyPolFoundMsgBytes());
	}

	/**Original name: EA-02-NO-ACY-POL-FOUND-MSG<br>*/
	public byte[] getEa02NoAcyPolFoundMsgBytes() {
		byte[] buffer = new byte[Len.EA02_NO_ACY_POL_FOUND_MSG];
		return getEa02NoAcyPolFoundMsgBytes(buffer, 1);
	}

	public byte[] getEa02NoAcyPolFoundMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea02ActNbr, Len.EA02_ACT_NBR);
		position += Len.EA02_ACT_NBR;
		MarshalByte.writeChar(buffer, position, flr4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEa02ActNbr(String ea02ActNbr) {
		this.ea02ActNbr = Functions.subString(ea02ActNbr, Len.EA02_ACT_NBR);
	}

	public String getEa02ActNbr() {
		return this.ea02ActNbr;
	}

	public char getFlr4() {
		return this.flr4;
	}

	/**Original name: WS-XZC0690I-ROW<br>
	 * <pre> POLICY INFORMATION SERVICE INPUT</pre>*/
	public byte[] getWsXzc0690iRowBytes() {
		byte[] buffer = new byte[Len.WS_XZC0690I_ROW];
		return getWsXzc0690iRowBytes(buffer, 1);
	}

	public byte[] getWsXzc0690iRowBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc0690i.getGetPolTrmLisByActBytes(buffer, position);
		return buffer;
	}

	public void setWsXz0690coRowBytes(byte[] buffer) {
		setWsXz0690coRowBytes(buffer, 1);
	}

	public void setWsXz0690coRowBytes(byte[] buffer, int offset) {
		int position = offset;
		xz0690co.setGetPolTrmLisByActBytes(buffer, position);
		position += Xz0690co.Len.GET_POL_TRM_LIS_BY_ACT;
		xz0690co.setErrorInformationBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public void setIntRegister1(short intRegister1) {
		this.intRegister1 = intRegister1;
	}

	public short getIntRegister1() {
		return this.intRegister1;
	}

	public ConstantFieldsXz0b90s0 getConstantFields() {
		return constantFields;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public Ea01MaxRowsReturnedMsg getEa01MaxRowsReturnedMsg() {
		return ea01MaxRowsReturnedMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public PpcErrorHandlingParms getPpcErrorHandlingParms() {
		return ppcErrorHandlingParms;
	}

	public SubscriptsXz0b90s0 getSubscripts() {
		return subscripts;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b90s0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a90s0Row getWsXz0a90s0Row() {
		return wsXz0a90s0Row;
	}

	public WsXz0a90s1Row getWsXz0a90s1Row() {
		return wsXz0a90s1Row;
	}

	public DfhcommareaXzc01090 getWsXzc010c1Row() {
		return wsXzc010c1Row;
	}

	public DfhcommareaXzc02090 getWsXzc020c1Row() {
		return wsXzc020c1Row;
	}

	public Xz0690co getXz0690co() {
		return xz0690co;
	}

	public Xzc0690iofCallableInputs getXzc0690i() {
		return xzc0690i;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA02_ACT_NBR = 9;
		public static final int PPC_OPERATION = 32;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int FLR1 = 10;
		public static final int FLR2 = 15;
		public static final int FLR3 = 14;
		public static final int FLR4 = 1;
		public static final int EA02_NO_ACY_POL_FOUND_MSG = EA02_ACT_NBR + FLR1 + FLR2 + FLR3 + FLR4;
		public static final int WS_XZC0690I_ROW = Xzc0690iofCallableInputs.Len.GET_POL_TRM_LIS_BY_ACT;
		public static final int WS_XZ0690CO_ROW = Xz0690co.Len.GET_POL_TRM_LIS_BY_ACT + Xz0690co.Len.ERROR_INFORMATION;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
