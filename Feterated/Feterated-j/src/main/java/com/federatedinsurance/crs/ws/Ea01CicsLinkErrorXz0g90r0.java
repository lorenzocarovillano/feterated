/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-01-CICS-LINK-ERROR<br>
 * Variable: EA-01-CICS-LINK-ERROR from program XZ0G90R0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01CicsLinkErrorXz0g90r0 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-CICS-LINK-ERROR
	private String flr1 = "PGM XZ0G90R0";
	//Original name: FILLER-EA-01-CICS-LINK-ERROR-1
	private String flr2 = "PARA =";
	//Original name: EA-01-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	public static final String PN3000 = "3000";
	//Original name: FILLER-EA-01-CICS-LINK-ERROR-2
	private String flr3 = " CICS ERROR";
	//Original name: FILLER-EA-01-CICS-LINK-ERROR-3
	private String flr4 = "LINKING TO PGM";
	//Original name: EA-01-LINK-PGM-NAME
	private String linkPgmName = DefaultValues.stringVal(Len.LINK_PGM_NAME);
	//Original name: FILLER-EA-01-CICS-LINK-ERROR-4
	private String flr5 = " RESP =";
	//Original name: EA-01-RESP
	private String resp = DefaultValues.stringVal(Len.RESP);
	//Original name: FILLER-EA-01-CICS-LINK-ERROR-5
	private String flr6 = " RESP2 =";
	//Original name: EA-01-RESP2
	private String resp2 = DefaultValues.stringVal(Len.RESP2);

	//==== METHODS ====
	public String getEa01CicsLinkErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa01CicsLinkErrorBytes());
	}

	public byte[] getEa01CicsLinkErrorBytes() {
		byte[] buffer = new byte[Len.EA01_CICS_LINK_ERROR];
		return getEa01CicsLinkErrorBytes(buffer, 1);
	}

	public byte[] getEa01CicsLinkErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, linkPgmName, Len.LINK_PGM_NAME);
		position += Len.LINK_PGM_NAME;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, resp, Len.RESP);
		position += Len.RESP;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, resp2, Len.RESP2);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public void setPn3000() {
		paragraphNbr = PN3000;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setLinkPgmName(String linkPgmName) {
		this.linkPgmName = Functions.subString(linkPgmName, Len.LINK_PGM_NAME);
	}

	public String getLinkPgmName() {
		return this.linkPgmName;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setResp(int resp) {
		this.resp = NumericDisplay.asString(resp, Len.RESP);
	}

	public int getResp() {
		return NumericDisplay.asInt(this.resp);
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setResp2(String resp2) {
		this.resp2 = Functions.subString(resp2, Len.RESP2);
	}

	public String getResp2() {
		return this.resp2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PARAGRAPH_NBR = 4;
		public static final int LINK_PGM_NAME = 8;
		public static final int RESP = 8;
		public static final int RESP2 = 5;
		public static final int FLR1 = 13;
		public static final int FLR2 = 7;
		public static final int FLR3 = 12;
		public static final int FLR4 = 15;
		public static final int FLR5 = 8;
		public static final int FLR6 = 9;
		public static final int EA01_CICS_LINK_ERROR = PARAGRAPH_NBR + LINK_PGM_NAME + RESP + RESP2 + FLR1 + FLR2 + FLR3 + FLR4 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
