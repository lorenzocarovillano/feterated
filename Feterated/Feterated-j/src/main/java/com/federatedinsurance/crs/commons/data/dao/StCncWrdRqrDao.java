/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IStCncWrdRqr;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ST_CNC_WRD_RQR]
 * 
 */
public class StCncWrdRqrDao extends BaseSqlDao<IStCncWrdRqr> {

	private Cursor stCncWrdRqrCsr;
	private final IRowMapper<IStCncWrdRqr> fetchStCncWrdRqrCsrRm = buildNamedRowMapper(IStCncWrdRqr.class, "stWrdSeqCd");

	public StCncWrdRqrDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IStCncWrdRqr> getToClass() {
		return IStCncWrdRqr.class;
	}

	public DbAccessStatus openStCncWrdRqrCsr(String stAbb, String actNotTypCd, String polCovCd) {
		stCncWrdRqrCsr = buildQuery("openStCncWrdRqrCsr").bind("stAbb", stAbb).bind("actNotTypCd", actNotTypCd).bind("polCovCd", polCovCd).open();
		return dbStatus;
	}

	public IStCncWrdRqr fetchStCncWrdRqrCsr(IStCncWrdRqr iStCncWrdRqr) {
		return fetch(stCncWrdRqrCsr, iStCncWrdRqr, fetchStCncWrdRqrCsrRm);
	}

	public DbAccessStatus closeStCncWrdRqrCsr() {
		return closeCursor(stCncWrdRqrCsr);
	}
}
