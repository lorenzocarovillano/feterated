/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.federatedinsurance.crs.commons.data.to.IActNotPol1;
import com.federatedinsurance.crs.ws.Xz0b9050Data;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: ActNotPol1Xz0b9050<br>*/
public class ActNotPol1Xz0b9050 implements IActNotPol1 {

	//==== PROPERTIES ====
	private Xz0b9050Data ws;

	//==== CONSTRUCTORS ====
	public ActNotPol1Xz0b9050(Xz0b9050Data ws) {
		this.ws = ws;
	}

	//==== METHODS ====
	@Override
	public String getActNotStaCd() {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public String getActNotTypCd() {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public String getActOwnCltId() {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public String getCfActNotImpending() {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public void setCfActNotImpending(String cfActNotImpending) {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public String getCfActNotNonpay() {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public void setCfActNotNonpay(String cfActNotNonpay) {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public String getCfActNotRescind() {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public void setCfActNotRescind(String cfActNotRescind) {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public String getCfActNotTypCdImp() {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	@Override
	public void setCfActNotTypCdImp(String cfActNotTypCdImp) {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public String getEmpId() {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public void setEmpId(String empId) {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	@Override
	public String getNotDt() {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public void setNotDt(String notDt) {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public String getNotEffDt() {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getPolEffDt() {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public String getPolExpDt() {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public String getPolNbr() {
		return ws.getDclactNotPol().getPolNbr();
	}

	@Override
	public void setPolNbr(String polNbr) {
		ws.getDclactNotPol().setPolNbr(polNbr);
	}

	@Override
	public String getPolTypCd() {
		return ws.getDclactNotPol().getPolTypCd();
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		ws.getDclactNotPol().setPolTypCd(polTypCd);
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		return ws.getDclactNot().getTotFeeAmt();
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		ws.getDclactNot().setTotFeeAmt(totFeeAmt.copy());
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		if (ws.getNiTotFeeAmt() >= 0) {
			return getTotFeeAmt().toString();
		} else {
			return null;
		}
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		if (totFeeAmtObj != null) {
			setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
			ws.setNiTotFeeAmt(((short) 0));
		} else {
			ws.setNiTotFeeAmt(((short) -1));
		}
	}

	@Override
	public short getWsNbrOfNotDay() {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	@Override
	public void setWsNbrOfNotDay(short wsNbrOfNotDay) {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	@Override
	public String getWsPolicyCancDt() {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}

	@Override
	public void setWsPolicyCancDt(String wsPolicyCancDt) {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}
}
