/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WS-TIME-ARRAY<br>
 * Variable: WS-TIME-ARRAY from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsTimeArray extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WsTimeArray() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_TIME_ARRAY;
	}

	public void setWsTimeArrayFormatted(String data) {
		writeString(Pos.WS_TIME_ARRAY, data, Len.WS_TIME_ARRAY);
	}

	public void setWsTimeArrayBytes(byte[] buffer) {
		setWsTimeArrayBytes(buffer, 1);
	}

	public void setWsTimeArrayBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WS_TIME_ARRAY, Pos.WS_TIME_ARRAY);
	}

	public void initWsTimeArrayZeroes() {
		fill(Pos.WS_TIME_ARRAY, Len.WS_TIME_ARRAY, '0');
	}

	public void setArrayHours(short arrayHours) {
		writeShort(Pos.ARRAY_HOURS, arrayHours, Len.Int.ARRAY_HOURS, SignType.NO_SIGN);
	}

	/**Original name: WS-TIME-ARRAY-HOURS<br>*/
	public short getArrayHours() {
		return readNumDispUnsignedShort(Pos.ARRAY_HOURS, Len.ARRAY_HOURS);
	}

	public String getArrayHoursFormatted() {
		return readFixedString(Pos.ARRAY_HOURS, Len.ARRAY_HOURS);
	}

	/**Original name: WS-TIME-ARRAY-HMSS<br>*/
	public char getArrayHmss() {
		return readChar(Pos.ARRAY_HMSS);
	}

	public void setArrayMinutes(short arrayMinutes) {
		writeShort(Pos.ARRAY_MINUTES, arrayMinutes, Len.Int.ARRAY_MINUTES, SignType.NO_SIGN);
	}

	/**Original name: WS-TIME-ARRAY-MINUTES<br>*/
	public short getArrayMinutes() {
		return readNumDispUnsignedShort(Pos.ARRAY_MINUTES, Len.ARRAY_MINUTES);
	}

	public String getArrayMinutesFormatted() {
		return readFixedString(Pos.ARRAY_MINUTES, Len.ARRAY_MINUTES);
	}

	/**Original name: WS-TIME-ARRAY-MSS<br>*/
	public char getArrayMss() {
		return readChar(Pos.ARRAY_MSS);
	}

	public void setArraySeconds(short arraySeconds) {
		writeShort(Pos.ARRAY_SECONDS, arraySeconds, Len.Int.ARRAY_SECONDS, SignType.NO_SIGN);
	}

	/**Original name: WS-TIME-ARRAY-SECONDS<br>*/
	public short getArraySeconds() {
		return readNumDispUnsignedShort(Pos.ARRAY_SECONDS, Len.ARRAY_SECONDS);
	}

	public String getArraySecondsFormatted() {
		return readFixedString(Pos.ARRAY_SECONDS, Len.ARRAY_SECONDS);
	}

	/**Original name: WS-TIME-ARRAY-SMS<br>*/
	public char getArraySms() {
		return readChar(Pos.ARRAY_SMS);
	}

	public String getArrayMsFormatted() {
		return readFixedString(Pos.ARRAY_MS, Len.ARRAY_MS);
	}

	public void setArrayMsAmpm(String arrayMsAmpm) {
		writeString(Pos.ARRAY_MS_AMPM, arrayMsAmpm, Len.ARRAY_MS_AMPM);
	}

	/**Original name: WS-TIME-ARRAY-MS-AMPM<br>*/
	public String getArrayMsAmpm() {
		return readString(Pos.ARRAY_MS_AMPM, Len.ARRAY_MS_AMPM);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_TIME_ARRAY = 1;
		public static final int ARRAY_IN = WS_TIME_ARRAY;
		public static final int WS_TIME_ARRAY_HMS = 1;
		public static final int ARRAY_HOURS = WS_TIME_ARRAY_HMS;
		public static final int ARRAY_HMSS = ARRAY_HOURS + Len.ARRAY_HOURS;
		public static final int ARRAY_MINUTES = ARRAY_HMSS + Len.ARRAY_HMSS;
		public static final int ARRAY_MSS = ARRAY_MINUTES + Len.ARRAY_MINUTES;
		public static final int ARRAY_SECONDS = ARRAY_MSS + Len.ARRAY_MSS;
		public static final int ARRAY_SMS = ARRAY_SECONDS + Len.ARRAY_SECONDS;
		public static final int ARRAY_MS = ARRAY_SMS + Len.ARRAY_SMS;
		public static final int ARRAY_MS_RED = ARRAY_MS;
		public static final int FLR1 = ARRAY_MS_RED;
		public static final int ARRAY_MS_AMPM = FLR1 + Len.FLR1;
		public static final int FLR2 = ARRAY_MS_AMPM + Len.ARRAY_MS_AMPM;
		public static final int FLR3 = ARRAY_MS + Len.ARRAY_MS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int ARRAY_HOURS = 2;
		public static final int ARRAY_HMSS = 1;
		public static final int ARRAY_MINUTES = 2;
		public static final int ARRAY_MSS = 1;
		public static final int ARRAY_SECONDS = 2;
		public static final int ARRAY_SMS = 1;
		public static final int FLR1 = 1;
		public static final int ARRAY_MS_AMPM = 2;
		public static final int ARRAY_MS = 6;
		public static final int ARRAY_IN = 20;
		public static final int WS_TIME_ARRAY = ARRAY_IN;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int ARRAY_HOURS = 2;
			public static final int ARRAY_MINUTES = 2;
			public static final int ARRAY_SECONDS = 2;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
