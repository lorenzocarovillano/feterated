/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xza950Operation;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9050-ROW<br>
 * Variable: WS-XZ0A9050-ROW from program XZ0B9050<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9050Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA950-MAX-POL-ROWS-RETURNED
	private short maxPolRowsReturned = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA950-OPERATION
	private Xza950Operation operation = new Xza950Operation();
	//Original name: XZA950-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA950-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA950-FRM-SEQ-NBR
	private int frmSeqNbr = DefaultValues.INT_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9050_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9050RowBytes(buf);
	}

	public String getWsXz0a9050RowFormatted() {
		return getPolicyListHeaderFormatted();
	}

	public void setWsXz0a9050RowBytes(byte[] buffer) {
		setWsXz0a9050RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9050RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9050_ROW];
		return getWsXz0a9050RowBytes(buffer, 1);
	}

	public void setWsXz0a9050RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setPolicyListHeaderBytes(buffer, position);
	}

	public byte[] getWsXz0a9050RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getPolicyListHeaderBytes(buffer, position);
		return buffer;
	}

	public String getPolicyListHeaderFormatted() {
		return MarshalByteExt.bufferToStr(getPolicyListHeaderBytes());
	}

	/**Original name: XZA950-POLICY-LIST-HEADER<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0A9050 - POLICY LOCATE BPO - HEADER                          *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *             XZ_GET_POL_LIST_HEADER                              *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 11/19/2008  E404GCL   NEW                              *
	 *  TO07614 01/28/2009  E404GCL   Add FRM-SEQ-NBR                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getPolicyListHeaderBytes() {
		byte[] buffer = new byte[Len.POLICY_LIST_HEADER];
		return getPolicyListHeaderBytes(buffer, 1);
	}

	public void setPolicyListHeaderBytes(byte[] buffer, int offset) {
		int position = offset;
		maxPolRowsReturned = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		operation.setOperation(MarshalByte.readString(buffer, position, Xza950Operation.Len.OPERATION));
		position += Xza950Operation.Len.OPERATION;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		frmSeqNbr = MarshalByte.readInt(buffer, position, Len.FRM_SEQ_NBR);
	}

	public byte[] getPolicyListHeaderBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxPolRowsReturned);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, operation.getOperation(), Xza950Operation.Len.OPERATION);
		position += Xza950Operation.Len.OPERATION;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeInt(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		return buffer;
	}

	public void setMaxPolRowsReturned(short maxPolRowsReturned) {
		this.maxPolRowsReturned = maxPolRowsReturned;
	}

	public short getMaxPolRowsReturned() {
		return this.maxPolRowsReturned;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setFrmSeqNbr(int frmSeqNbr) {
		this.frmSeqNbr = frmSeqNbr;
	}

	public int getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	public Xza950Operation getOperation() {
		return operation;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9050RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_POL_ROWS_RETURNED = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FRM_SEQ_NBR = 5;
		public static final int POLICY_LIST_HEADER = MAX_POL_ROWS_RETURNED + Xza950Operation.Len.OPERATION + CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR;
		public static final int WS_XZ0A9050_ROW = POLICY_LIST_HEADER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
