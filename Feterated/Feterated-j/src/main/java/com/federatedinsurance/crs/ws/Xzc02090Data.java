/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.federatedinsurance.crs.commons.data.to.IActNotPol1;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclpolDtaExt;
import com.federatedinsurance.crs.copy.Tt008001;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZC02090<br>
 * Generated as a class for rule WS.<br>*/
public class Xzc02090Data implements IActNotPol1 {

	//==== PROPERTIES ====
	//Original name: DCLPOL-DTA-EXT
	private DclpolDtaExt dclpolDtaExt = new DclpolDtaExt();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXzc02090 constantFields = new ConstantFieldsXzc02090();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXzc02090 errorAndAdviceMessages = new ErrorAndAdviceMessagesXzc02090();
	//Original name: NI-EMP-ID
	private short niEmpId = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-NLBE-IDX
	private short ssNlbeIdx = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-RESPONSE-CODE
	private int wsResponseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int wsResponseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: TT008001
	private Tt008001 tt008001 = new Tt008001();

	//==== METHODS ====
	public void setNiEmpId(short niEmpId) {
		this.niEmpId = niEmpId;
	}

	public short getNiEmpId() {
		return this.niEmpId;
	}

	public void setSsNlbeIdx(short ssNlbeIdx) {
		this.ssNlbeIdx = ssNlbeIdx;
	}

	public short getSsNlbeIdx() {
		return this.ssNlbeIdx;
	}

	public void setWsResponseCode(int wsResponseCode) {
		this.wsResponseCode = wsResponseCode;
	}

	public int getWsResponseCode() {
		return this.wsResponseCode;
	}

	public void setWsResponseCode2(int wsResponseCode2) {
		this.wsResponseCode2 = wsResponseCode2;
	}

	public int getWsResponseCode2() {
		return this.wsResponseCode2;
	}

	@Override
	public String getActNotStaCd() {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public String getActNotTypCd() {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public String getActOwnCltId() {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public String getCfActNotImpending() {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public void setCfActNotImpending(String cfActNotImpending) {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public String getCfActNotNonpay() {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public void setCfActNotNonpay(String cfActNotNonpay) {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public String getCfActNotRescind() {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public void setCfActNotRescind(String cfActNotRescind) {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public String getCfActNotTypCdImp() {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	@Override
	public void setCfActNotTypCdImp(String cfActNotTypCdImp) {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	public ConstantFieldsXzc02090 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclpolDtaExt getDclpolDtaExt() {
		return dclpolDtaExt;
	}

	@Override
	public String getEmpId() {
		return dclactNot.getEmpId();
	}

	@Override
	public void setEmpId(String empId) {
		this.dclactNot.setEmpId(empId);
	}

	@Override
	public String getEmpIdObj() {
		if (getNiEmpId() >= 0) {
			return getEmpId();
		} else {
			return null;
		}
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		if (empIdObj != null) {
			setEmpId(empIdObj);
			setNiEmpId(((short) 0));
		} else {
			setNiEmpId(((short) -1));
		}
	}

	public ErrorAndAdviceMessagesXzc02090 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	@Override
	public String getNotDt() {
		return dclactNot.getNotDt();
	}

	@Override
	public void setNotDt(String notDt) {
		this.dclactNot.setNotDt(notDt);
	}

	@Override
	public String getNotEffDt() {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getPolEffDt() {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public String getPolExpDt() {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public String getPolNbr() {
		throw new FieldNotMappedException("polNbr");
	}

	@Override
	public void setPolNbr(String polNbr) {
		throw new FieldNotMappedException("polNbr");
	}

	@Override
	public String getPolTypCd() {
		throw new FieldNotMappedException("polTypCd");
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		throw new FieldNotMappedException("polTypCd");
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		return getTotFeeAmt().toString();
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
	}

	public Tt008001 getTt008001() {
		return tt008001;
	}

	@Override
	public short getWsNbrOfNotDay() {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	@Override
	public void setWsNbrOfNotDay(short wsNbrOfNotDay) {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	@Override
	public String getWsPolicyCancDt() {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}

	@Override
	public void setWsPolicyCancDt(String wsPolicyCancDt) {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}
}
