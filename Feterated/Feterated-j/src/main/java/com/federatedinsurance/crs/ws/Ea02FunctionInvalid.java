/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-02-FUNCTION-INVALID<br>
 * Variable: EA-02-FUNCTION-INVALID from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea02FunctionInvalid {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-FUNCTION-INVALID
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-02-FUNCTION-INVALID-1
	private String flr2 = "FUNCTION PARM";
	//Original name: FILLER-EA-02-FUNCTION-INVALID-2
	private String flr3 = "IS INVALID:  (";
	//Original name: EA-02-FUNCTION-VAL
	private char ea02FunctionVal = DefaultValues.CHAR_VAL;
	//Original name: FILLER-EA-02-FUNCTION-INVALID-3
	private String flr4 = ").";

	//==== METHODS ====
	public String getEa02FunctionInvalidFormatted() {
		return MarshalByteExt.bufferToStr(getEa02FunctionInvalidBytes());
	}

	public byte[] getEa02FunctionInvalidBytes() {
		byte[] buffer = new byte[Len.EA02_FUNCTION_INVALID];
		return getEa02FunctionInvalidBytes(buffer, 1);
	}

	public byte[] getEa02FunctionInvalidBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeChar(buffer, position, ea02FunctionVal);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEa02FunctionVal(char ea02FunctionVal) {
		this.ea02FunctionVal = ea02FunctionVal;
	}

	public char getEa02FunctionVal() {
		return this.ea02FunctionVal;
	}

	public String getFlr4() {
		return this.flr4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 14;
		public static final int EA02_FUNCTION_VAL = 1;
		public static final int FLR4 = 2;
		public static final int EA02_FUNCTION_INVALID = EA02_FUNCTION_VAL + FLR1 + 2 * FLR2 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
