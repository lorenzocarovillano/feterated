/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.NameOut;
import com.federatedinsurance.crs.copy.Ws4DgrSubrtnErrorLine;
import com.federatedinsurance.crs.ws.enums.ReturnConjoinedPerson;
import com.federatedinsurance.crs.ws.enums.ReturnedErrorSw;
import com.federatedinsurance.crs.ws.enums.TypeCode;
import com.federatedinsurance.crs.ws.occurs.ReturnedData;
import com.federatedinsurance.crs.ws.redefines.FullNameIn;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: NAME-SCRUB-RECORD<br>
 * Variable: NAME-SCRUB-RECORD from copybook CISLNSRB<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class NameScrubRecord extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int RETURNED_DATA_MAXOCCURS = 5;
	//Original name: TYPE-CODE
	private TypeCode typeCode = new TypeCode();
	//Original name: FULL-NAME-IN
	private FullNameIn fullNameIn = new FullNameIn();
	//Original name: CLIENT-ID1-18
	private String clientId118 = "";
	//Original name: CLIENT-ID19-20
	private String clientId1920 = "";
	//Original name: CLIENT2-ID1-18
	private String client2Id118 = "";
	//Original name: CLIENT2-ID19-20
	private String client2Id1920 = "";
	//Original name: NAME-OUT
	private NameOut nameOut = new NameOut();
	//Original name: RETURNED-ERROR-SW
	private ReturnedErrorSw returnedErrorSw = new ReturnedErrorSw();
	//Original name: WS4-DGR-SUBRTN-ERROR-LINE
	private Ws4DgrSubrtnErrorLine ws4DgrSubrtnErrorLine = new Ws4DgrSubrtnErrorLine();
	//Original name: RETURN-CONJOINED-PERSON
	private ReturnConjoinedPerson returnConjoinedPerson = new ReturnConjoinedPerson();
	//Original name: RETURNED-DATA
	private ReturnedData[] returnedData = new ReturnedData[RETURNED_DATA_MAXOCCURS];
	//Original name: 5DIGIT-IN
	private String n5digitIn = "";
	//Original name: PLUS-4-IN
	private String plus4In = "";

	//==== CONSTRUCTORS ====
	public NameScrubRecord() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.NAME_SCRUB_RECORD;
	}

	@Override
	public void deserialize(byte[] buf) {
		setNameScrubRecordBytes(buf);
	}

	public void init() {
		for (int returnedDataIdx = 1; returnedDataIdx <= RETURNED_DATA_MAXOCCURS; returnedDataIdx++) {
			returnedData[returnedDataIdx - 1] = new ReturnedData();
		}
	}

	public void setNameScrubRecordBytes(byte[] buffer) {
		setNameScrubRecordBytes(buffer, 1);
	}

	public byte[] getNameScrubRecordBytes() {
		byte[] buffer = new byte[Len.NAME_SCRUB_RECORD];
		return getNameScrubRecordBytes(buffer, 1);
	}

	public void setNameScrubRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		typeCode.setTypeCode(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		setNameInBytes(buffer, position);
		position += Len.NAME_IN;
		setClientIdOutBytes(buffer, position);
		position += Len.CLIENT_ID_OUT;
		setClientIdOut2Bytes(buffer, position);
		position += Len.CLIENT_ID_OUT2;
		nameOut.setNameOutBytes(buffer, position);
		position += NameOut.Len.NAME_OUT;
		returnedErrorSw.setReturnedErrorSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		ws4DgrSubrtnErrorLine.setWs4DgrSubrtnErrorLineBytes(buffer, position);
		position += Ws4DgrSubrtnErrorLine.Len.WS4_DGR_SUBRTN_ERROR_LINE;
		returnConjoinedPerson.setReturnConjoinedPerson(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		for (int idx = 1; idx <= RETURNED_DATA_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				returnedData[idx - 1].setReturnedDataBytes(buffer, position);
				position += ReturnedData.Len.RETURNED_DATA;
			} else {
				returnedData[idx - 1].initReturnedDataSpaces();
				position += ReturnedData.Len.RETURNED_DATA;
			}
		}
		setZipCodeInBytes(buffer, position);
	}

	public byte[] getNameScrubRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, typeCode.getTypeCode());
		position += Types.CHAR_SIZE;
		getNameInBytes(buffer, position);
		position += Len.NAME_IN;
		getClientIdOutBytes(buffer, position);
		position += Len.CLIENT_ID_OUT;
		getClientIdOut2Bytes(buffer, position);
		position += Len.CLIENT_ID_OUT2;
		nameOut.getNameOutBytes(buffer, position);
		position += NameOut.Len.NAME_OUT;
		MarshalByte.writeChar(buffer, position, returnedErrorSw.getReturnedErrorSw());
		position += Types.CHAR_SIZE;
		ws4DgrSubrtnErrorLine.getWs4DgrSubrtnErrorLineBytes(buffer, position);
		position += Ws4DgrSubrtnErrorLine.Len.WS4_DGR_SUBRTN_ERROR_LINE;
		MarshalByte.writeChar(buffer, position, returnConjoinedPerson.getReturnConjoinedPerson());
		position += Types.CHAR_SIZE;
		for (int idx = 1; idx <= RETURNED_DATA_MAXOCCURS; idx++) {
			returnedData[idx - 1].getReturnedDataBytes(buffer, position);
			position += ReturnedData.Len.RETURNED_DATA;
		}
		getZipCodeInBytes(buffer, position);
		return buffer;
	}

	/**Original name: NAME-IN<br>*/
	public byte[] getNameInBytes() {
		byte[] buffer = new byte[Len.NAME_IN];
		return getNameInBytes(buffer, 1);
	}

	public void setNameInBytes(byte[] buffer, int offset) {
		int position = offset;
		fullNameIn.setFullNameInFromBuffer(buffer, position);
	}

	public byte[] getNameInBytes(byte[] buffer, int offset) {
		int position = offset;
		fullNameIn.getFullNameInAsBuffer(buffer, position);
		return buffer;
	}

	public void setClientIdOutBytes(byte[] buffer, int offset) {
		int position = offset;
		clientId118 = MarshalByte.readString(buffer, position, Len.CLIENT_ID118);
		position += Len.CLIENT_ID118;
		clientId1920 = MarshalByte.readString(buffer, position, Len.CLIENT_ID1920);
	}

	public byte[] getClientIdOutBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientId118, Len.CLIENT_ID118);
		position += Len.CLIENT_ID118;
		MarshalByte.writeString(buffer, position, clientId1920, Len.CLIENT_ID1920);
		return buffer;
	}

	public void setClientId118(String clientId118) {
		this.clientId118 = Functions.subString(clientId118, Len.CLIENT_ID118);
	}

	public String getClientId118() {
		return this.clientId118;
	}

	public void setClientId1920(String clientId1920) {
		this.clientId1920 = Functions.subString(clientId1920, Len.CLIENT_ID1920);
	}

	public String getClientId1920() {
		return this.clientId1920;
	}

	public void setClientIdOut2Bytes(byte[] buffer, int offset) {
		int position = offset;
		client2Id118 = MarshalByte.readString(buffer, position, Len.CLIENT2_ID118);
		position += Len.CLIENT2_ID118;
		client2Id1920 = MarshalByte.readString(buffer, position, Len.CLIENT2_ID1920);
	}

	public byte[] getClientIdOut2Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, client2Id118, Len.CLIENT2_ID118);
		position += Len.CLIENT2_ID118;
		MarshalByte.writeString(buffer, position, client2Id1920, Len.CLIENT2_ID1920);
		return buffer;
	}

	public void setClient2Id118(String client2Id118) {
		this.client2Id118 = Functions.subString(client2Id118, Len.CLIENT2_ID118);
	}

	public String getClient2Id118() {
		return this.client2Id118;
	}

	public void setClient2Id1920(String client2Id1920) {
		this.client2Id1920 = Functions.subString(client2Id1920, Len.CLIENT2_ID1920);
	}

	public String getClient2Id1920() {
		return this.client2Id1920;
	}

	public void setZipCodeInBytes(byte[] buffer, int offset) {
		int position = offset;
		n5digitIn = MarshalByte.readString(buffer, position, Len.N5DIGIT_IN);
		position += Len.N5DIGIT_IN;
		plus4In = MarshalByte.readString(buffer, position, Len.PLUS4_IN);
	}

	public byte[] getZipCodeInBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, n5digitIn, Len.N5DIGIT_IN);
		position += Len.N5DIGIT_IN;
		MarshalByte.writeString(buffer, position, plus4In, Len.PLUS4_IN);
		return buffer;
	}

	public void setN5digitIn(String n5digitIn) {
		this.n5digitIn = Functions.subString(n5digitIn, Len.N5DIGIT_IN);
	}

	public String getN5digitIn() {
		return this.n5digitIn;
	}

	public void setPlus4In(String plus4In) {
		this.plus4In = Functions.subString(plus4In, Len.PLUS4_IN);
	}

	public String getPlus4In() {
		return this.plus4In;
	}

	public FullNameIn getFullNameIn() {
		return fullNameIn;
	}

	public NameOut getNameOut() {
		return nameOut;
	}

	public ReturnConjoinedPerson getReturnConjoinedPerson() {
		return returnConjoinedPerson;
	}

	public ReturnedData getReturnedData(int idx) {
		return returnedData[idx - 1];
	}

	public ReturnedErrorSw getReturnedErrorSw() {
		return returnedErrorSw;
	}

	public TypeCode getTypeCode() {
		return typeCode;
	}

	@Override
	public byte[] serialize() {
		return getNameScrubRecordBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NAME_IN = FullNameIn.Len.FULL_NAME_IN;
		public static final int CLIENT_ID118 = 18;
		public static final int CLIENT_ID1920 = 2;
		public static final int CLIENT_ID_OUT = CLIENT_ID118 + CLIENT_ID1920;
		public static final int CLIENT2_ID118 = 18;
		public static final int CLIENT2_ID1920 = 2;
		public static final int CLIENT_ID_OUT2 = CLIENT2_ID118 + CLIENT2_ID1920;
		public static final int N5DIGIT_IN = 5;
		public static final int PLUS4_IN = 4;
		public static final int ZIP_CODE_IN = N5DIGIT_IN + PLUS4_IN;
		public static final int NAME_SCRUB_RECORD = TypeCode.Len.TYPE_CODE + NAME_IN + CLIENT_ID_OUT + CLIENT_ID_OUT2 + NameOut.Len.NAME_OUT
				+ ReturnedErrorSw.Len.RETURNED_ERROR_SW + Ws4DgrSubrtnErrorLine.Len.WS4_DGR_SUBRTN_ERROR_LINE
				+ ReturnConjoinedPerson.Len.RETURN_CONJOINED_PERSON + NameScrubRecord.RETURNED_DATA_MAXOCCURS * ReturnedData.Len.RETURNED_DATA
				+ ZIP_CODE_IN;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
