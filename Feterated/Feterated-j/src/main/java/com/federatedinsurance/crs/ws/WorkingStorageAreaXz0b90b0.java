/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0B90B0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0b90b0 {

	//==== PROPERTIES ====
	//Original name: WS-MAX-POL-WITH-DAYS-ROWS
	private short maxPolWithDaysRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0B90B0";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-EIBRESP-CD
	private short eibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-BUS-OBJ-NM-HEADER
	private String busObjNmHeader = "XZ_GET_NOT_DAYS_RQR_LIST_HDR";
	//Original name: WS-BUS-OBJ-NM-DETAIL
	private String busObjNmDetail = "XZ_GET_NOT_DAYS_RQR_LIST_DTL";
	//Original name: WS-GET-POLICY-LIST
	private String getPolicyList = "GetPolicyListByNotification";

	//==== METHODS ====
	public void setMaxPolWithDaysRows(short maxPolWithDaysRows) {
		this.maxPolWithDaysRows = maxPolWithDaysRows;
	}

	public short getMaxPolWithDaysRows() {
		return this.maxPolWithDaysRows;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd = eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public String getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd = eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public String getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public String getBusObjNmHeader() {
		return this.busObjNmHeader;
	}

	public String getBusObjNmDetail() {
		return this.busObjNmDetail;
	}

	public String getGetPolicyList() {
		return this.getPolicyList;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int EIBRESP_CD = 4;
		public static final int EIBRESP2_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
