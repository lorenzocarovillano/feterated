/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-CLEAR-UOW-REQUEST-SWITCH<br>
 * Variable: WS-CLEAR-UOW-REQUEST-SWITCH from program HALRURQA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsClearUowRequestSwitch {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char START_BROWSE_UOW_REQUEST = 'S';
	public static final char NO_MORE_UOW_REQUESTS = 'N';

	//==== METHODS ====
	public void setWsClearUowRequestSwitch(char wsClearUowRequestSwitch) {
		this.value = wsClearUowRequestSwitch;
	}

	public char getWsClearUowRequestSwitch() {
		return this.value;
	}

	public void setStartBrowseUowRequest() {
		value = START_BROWSE_UOW_REQUEST;
	}

	public boolean isNoMoreUowRequests() {
		return value == NO_MORE_UOW_REQUESTS;
	}

	public void setNoMoreUowRequests() {
		value = NO_MORE_UOW_REQUESTS;
	}
}
