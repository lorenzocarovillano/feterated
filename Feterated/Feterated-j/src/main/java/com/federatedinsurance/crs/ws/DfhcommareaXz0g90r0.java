/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Xzt9r0ServiceOutputs;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.enums.Xzz9r0BypassSyncpointInd;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program XZ0G90R0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaXz0g90r0 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZT9RI-POL-NBR
	private String xzt9riPolNbr = DefaultValues.stringVal(Len.XZT9RI_POL_NBR);
	//Original name: XZT9RI-POL-EFF-DT
	private String xzt9riPolEffDt = DefaultValues.stringVal(Len.XZT9RI_POL_EFF_DT);
	//Original name: XZT9RI-POL-EXP-DT
	private String xzt9riPolExpDt = DefaultValues.stringVal(Len.XZT9RI_POL_EXP_DT);
	//Original name: XZT9RI-USERID
	private String xzt9riUserid = DefaultValues.stringVal(Len.XZT9RI_USERID);
	//Original name: FILLER-DFHCOMMAREA-XZT9R0-SERVICE-INPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: XZT9R0-SERVICE-OUTPUTS
	private Xzt9r0ServiceOutputs xzt9r0ServiceOutputs = new Xzt9r0ServiceOutputs();
	//Original name: XZZ9R0-OPERATION
	private String xzz9r0Operation = DefaultValues.stringVal(Len.XZZ9R0_OPERATION);
	public static final String XZZ9R0_GET_ADD_POL_INF = "GetAdditionalPolicyInfo";
	//Original name: XZZ9R0-BYPASS-SYNCPOINT-IND
	private Xzz9r0BypassSyncpointInd xzz9r0BypassSyncpointInd = new Xzz9r0BypassSyncpointInd();
	//Original name: XZZ9R0-ERROR-RETURN-CODE
	private DsdErrorReturnCode xzz9r0ErrorReturnCode = new DsdErrorReturnCode();
	//Original name: XZZ9R0-ERROR-MESSAGE
	private String xzz9r0ErrorMessage = DefaultValues.stringVal(Len.XZZ9R0_ERROR_MESSAGE);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		setXzt9r0ServiceInputsBytes(buffer, position);
		position += Len.XZT9R0_SERVICE_INPUTS;
		xzt9r0ServiceOutputs.setXzt9r0ServiceOutputsBytes(buffer, position);
		position += Xzt9r0ServiceOutputs.Len.XZT9R0_SERVICE_OUTPUTS;
		setXzz9r0ServiceParametersBytes(buffer, position);
		position += Len.XZZ9R0_SERVICE_PARAMETERS;
		setXzz9r0ServiceErrorInfoBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		getXzt9r0ServiceInputsBytes(buffer, position);
		position += Len.XZT9R0_SERVICE_INPUTS;
		xzt9r0ServiceOutputs.getXzt9r0ServiceOutputsBytes(buffer, position);
		position += Xzt9r0ServiceOutputs.Len.XZT9R0_SERVICE_OUTPUTS;
		getXzz9r0ServiceParametersBytes(buffer, position);
		position += Len.XZZ9R0_SERVICE_PARAMETERS;
		getXzz9r0ServiceErrorInfoBytes(buffer, position);
		return buffer;
	}

	/**Original name: XZT9R0-SERVICE-INPUTS<br>
	 * <pre>*************************************************************
	 *  XZ0Z90R0 - CONTRACT COPYBOOK FOR SERVICE/UNIT OF WORK:     *
	 *     XZ_GET_ADDITIONAL_POLICY_INFO                           *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE       PRGRMR     DESCRIPTION                 *
	 * --------- ---------- ---------- ----------------------------*
	 * TO0760247 01/04/2010 E404ABL    NEW                         *
	 *                                                             *
	 * *************************************************************
	 *     COPY XZ0T90R0.
	 * ***************************************************************
	 *  XZ0T90R0 - SERVICE CONTRACT COPYBOOK FOR ADDITIONAL POLICY   *
	 *               INFORMATION WRAPPER                             *
	 *             UOW       : XZ_GET_ADDITIONAL_POLICY_INFO         *
	 * ***************************************************************
	 *  MAINTENANCE LOG                                              *
	 *                                                               *
	 *  SI#        DATE      PRGRMR    DESCRIPTION                   *
	 *  ---------  --------- --------- ------------------------------*
	 * TO07602-22  10MAR2010 E404ABL   NEW                           *
	 *                                                               *
	 * ***************************************************************</pre>*/
	public byte[] getXzt9r0ServiceInputsBytes() {
		byte[] buffer = new byte[Len.XZT9R0_SERVICE_INPUTS];
		return getXzt9r0ServiceInputsBytes(buffer, 1);
	}

	public void setXzt9r0ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt9riPolNbr = MarshalByte.readString(buffer, position, Len.XZT9RI_POL_NBR);
		position += Len.XZT9RI_POL_NBR;
		xzt9riPolEffDt = MarshalByte.readString(buffer, position, Len.XZT9RI_POL_EFF_DT);
		position += Len.XZT9RI_POL_EFF_DT;
		xzt9riPolExpDt = MarshalByte.readString(buffer, position, Len.XZT9RI_POL_EXP_DT);
		position += Len.XZT9RI_POL_EXP_DT;
		xzt9riUserid = MarshalByte.readString(buffer, position, Len.XZT9RI_USERID);
		position += Len.XZT9RI_USERID;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzt9r0ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt9riPolNbr, Len.XZT9RI_POL_NBR);
		position += Len.XZT9RI_POL_NBR;
		MarshalByte.writeString(buffer, position, xzt9riPolEffDt, Len.XZT9RI_POL_EFF_DT);
		position += Len.XZT9RI_POL_EFF_DT;
		MarshalByte.writeString(buffer, position, xzt9riPolExpDt, Len.XZT9RI_POL_EXP_DT);
		position += Len.XZT9RI_POL_EXP_DT;
		MarshalByte.writeString(buffer, position, xzt9riUserid, Len.XZT9RI_USERID);
		position += Len.XZT9RI_USERID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setXzt9riPolNbr(String xzt9riPolNbr) {
		this.xzt9riPolNbr = Functions.subString(xzt9riPolNbr, Len.XZT9RI_POL_NBR);
	}

	public String getXzt9riPolNbr() {
		return this.xzt9riPolNbr;
	}

	public void setXzt9riPolEffDt(String xzt9riPolEffDt) {
		this.xzt9riPolEffDt = Functions.subString(xzt9riPolEffDt, Len.XZT9RI_POL_EFF_DT);
	}

	public String getXzt9riPolEffDt() {
		return this.xzt9riPolEffDt;
	}

	public void setXzt9riPolExpDt(String xzt9riPolExpDt) {
		this.xzt9riPolExpDt = Functions.subString(xzt9riPolExpDt, Len.XZT9RI_POL_EXP_DT);
	}

	public String getXzt9riPolExpDt() {
		return this.xzt9riPolExpDt;
	}

	public void setXzt9riUserid(String xzt9riUserid) {
		this.xzt9riUserid = Functions.subString(xzt9riUserid, Len.XZT9RI_USERID);
	}

	public String getXzt9riUserid() {
		return this.xzt9riUserid;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setXzz9r0ServiceParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		xzz9r0Operation = MarshalByte.readString(buffer, position, Len.XZZ9R0_OPERATION);
		position += Len.XZZ9R0_OPERATION;
		xzz9r0BypassSyncpointInd.setXzz9r0BypassSyncpointInd(MarshalByte.readChar(buffer, position));
	}

	public byte[] getXzz9r0ServiceParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzz9r0Operation, Len.XZZ9R0_OPERATION);
		position += Len.XZZ9R0_OPERATION;
		MarshalByte.writeChar(buffer, position, xzz9r0BypassSyncpointInd.getXzz9r0BypassSyncpointInd());
		return buffer;
	}

	public void setXzz9r0Operation(String xzz9r0Operation) {
		this.xzz9r0Operation = Functions.subString(xzz9r0Operation, Len.XZZ9R0_OPERATION);
	}

	public String getXzz9r0Operation() {
		return this.xzz9r0Operation;
	}

	public void setXzz9r0ServiceErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		xzz9r0ErrorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		xzz9r0ErrorMessage = MarshalByte.readString(buffer, position, Len.XZZ9R0_ERROR_MESSAGE);
	}

	public byte[] getXzz9r0ServiceErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzz9r0ErrorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, xzz9r0ErrorMessage, Len.XZZ9R0_ERROR_MESSAGE);
		return buffer;
	}

	public void setXzz9r0ErrorMessage(String xzz9r0ErrorMessage) {
		this.xzz9r0ErrorMessage = Functions.subString(xzz9r0ErrorMessage, Len.XZZ9R0_ERROR_MESSAGE);
	}

	public String getXzz9r0ErrorMessage() {
		return this.xzz9r0ErrorMessage;
	}

	public Xzt9r0ServiceOutputs getXzt9r0ServiceOutputs() {
		return xzt9r0ServiceOutputs;
	}

	public Xzz9r0BypassSyncpointInd getXzz9r0BypassSyncpointInd() {
		return xzz9r0BypassSyncpointInd;
	}

	public DsdErrorReturnCode getXzz9r0ErrorReturnCode() {
		return xzz9r0ErrorReturnCode;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT9RI_POL_NBR = 9;
		public static final int XZT9RI_POL_EFF_DT = 10;
		public static final int XZT9RI_POL_EXP_DT = 10;
		public static final int XZT9RI_USERID = 8;
		public static final int FLR1 = 100;
		public static final int XZT9R0_SERVICE_INPUTS = XZT9RI_POL_NBR + XZT9RI_POL_EFF_DT + XZT9RI_POL_EXP_DT + XZT9RI_USERID + FLR1;
		public static final int XZZ9R0_OPERATION = 32;
		public static final int XZZ9R0_SERVICE_PARAMETERS = XZZ9R0_OPERATION + Xzz9r0BypassSyncpointInd.Len.XZZ9R0_BYPASS_SYNCPOINT_IND;
		public static final int XZZ9R0_ERROR_MESSAGE = 250;
		public static final int XZZ9R0_SERVICE_ERROR_INFO = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + XZZ9R0_ERROR_MESSAGE;
		public static final int DFHCOMMAREA = XZT9R0_SERVICE_INPUTS + Xzt9r0ServiceOutputs.Len.XZT9R0_SERVICE_OUTPUTS + XZZ9R0_SERVICE_PARAMETERS
				+ XZZ9R0_SERVICE_ERROR_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
