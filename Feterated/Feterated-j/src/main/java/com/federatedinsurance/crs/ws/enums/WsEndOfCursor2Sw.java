/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-END-OF-CURSOR2-SW<br>
 * Variable: WS-END-OF-CURSOR2-SW from program HALRLOMG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEndOfCursor2Sw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_OF_CURSOR2 = 'Y';
	public static final char NOT_END_OF_CURSOR2 = 'N';

	//==== METHODS ====
	public void setEndOfCursor2Sw(char endOfCursor2Sw) {
		this.value = endOfCursor2Sw;
	}

	public char getEndOfCursor2Sw() {
		return this.value;
	}

	public boolean isEndOfCursor2() {
		return value == END_OF_CURSOR2;
	}

	public void setEndOfCursor2() {
		value = END_OF_CURSOR2;
	}

	public void setNotEndOfCursor2() {
		value = NOT_END_OF_CURSOR2;
	}
}
