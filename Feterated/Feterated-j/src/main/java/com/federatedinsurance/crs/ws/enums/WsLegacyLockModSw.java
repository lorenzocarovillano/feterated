/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-LEGACY-LOCK-MOD-SW<br>
 * Variable: WS-LEGACY-LOCK-MOD-SW from program HALRLODR<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsLegacyLockModSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char RETRIEVE_LEGACY_LOCK_MODULE = 'R';
	public static final char LEGACY_LOCK_MODULE_FOUND = 'F';
	public static final char NO_LEGACY_LOCK_MODULE = 'N';

	//==== METHODS ====
	public void setLegacyLockModSw(char legacyLockModSw) {
		this.value = legacyLockModSw;
	}

	public char getLegacyLockModSw() {
		return this.value;
	}

	public boolean isRetrieveLegacyLockModule() {
		return value == RETRIEVE_LEGACY_LOCK_MODULE;
	}

	public void setRetrieveLegacyLockModule() {
		value = RETRIEVE_LEGACY_LOCK_MODULE;
	}

	public boolean isLegacyLockModuleFound() {
		return value == LEGACY_LOCK_MODULE_FOUND;
	}

	public void setLegacyLockModuleFound() {
		value = LEGACY_LOCK_MODULE_FOUND;
	}

	public boolean isNoLegacyLockModule() {
		return value == NO_LEGACY_LOCK_MODULE;
	}

	public void setNoLegacyLockModule() {
		value = NO_LEGACY_LOCK_MODULE;
	}
}
