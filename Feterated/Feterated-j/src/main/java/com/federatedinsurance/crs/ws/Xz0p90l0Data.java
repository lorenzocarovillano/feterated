/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPol1;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.TpPoliciesXz0p90l0;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0P90L0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0p90l0Data implements IActNotPol1 {

	//==== PROPERTIES ====
	public static final int TP_POLICIES_MAXOCCURS = 30;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0p90l0 constantFields = new ConstantFieldsXz0p90l0();
	//Original name: EA-01-NO-RESCINDS
	private Ea01NoRescinds ea01NoRescinds = new Ea01NoRescinds();
	//Original name: EA-02-IMP-IN-PROGRESS
	private Ea02ImpInProgress ea02ImpInProgress = new Ea02ImpInProgress();
	//Original name: ES-01-FATAL-ERROR-MSG
	private Es01FatalErrorMsg es01FatalErrorMsg = new Es01FatalErrorMsg();
	//Original name: SUBSCRIPTS
	private SubscriptsXz0p90d0 subscripts = new SubscriptsXz0p90d0();
	//Original name: SWITCHES
	private SwitchesXz0p90l0 switches = new SwitchesXz0p90l0();
	//Original name: TP-POLICIES
	private TpPoliciesXz0p90l0[] tpPolicies = new TpPoliciesXz0p90l0[TP_POLICIES_MAXOCCURS];
	//Original name: IX-TP
	private int ixTp = 1;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0p90l0 workingStorageArea = new WorkingStorageAreaXz0p90l0();
	//Original name: WS-XZ0Y90L0-ROW
	private WsXz0y90a0Row wsXz0y90l0Row = new WsXz0y90a0Row();
	//Original name: WS-PROXY-PROGRAM-AREA
	private WsProxyProgramArea wsProxyProgramArea = new WsProxyProgramArea();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-BILLING-LINKAGE
	private WsBillingLinkage wsBillingLinkage = new WsBillingLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== CONSTRUCTORS ====
	public Xz0p90l0Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int tpPoliciesIdx = 1; tpPoliciesIdx <= TP_POLICIES_MAXOCCURS; tpPoliciesIdx++) {
			tpPolicies[tpPoliciesIdx - 1] = new TpPoliciesXz0p90l0();
		}
		initTableOfPoliciesHighValues();
	}

	public void initTableOfPoliciesHighValues() {
		for (int idx = 1; idx <= TP_POLICIES_MAXOCCURS; idx++) {
			tpPolicies[idx - 1].initTpPoliciesHighValues();
		}
	}

	public void setIxTp(int ixTp) {
		this.ixTp = ixTp;
	}

	public int getIxTp() {
		return this.ixTp;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getActNotStaCd() {
		return dclactNot.getActNotStaCd();
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		this.dclactNot.setActNotStaCd(actNotStaCd);
	}

	@Override
	public String getActNotTypCd() {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public String getActOwnCltId() {
		return dclactNot.getActOwnCltId();
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		this.dclactNot.setActOwnCltId(actOwnCltId);
	}

	@Override
	public String getCfActNotImpending() {
		return constantFields.getActNotTypCd().getImpending();
	}

	@Override
	public void setCfActNotImpending(String cfActNotImpending) {
		this.constantFields.getActNotTypCd().setImpending(cfActNotImpending);
	}

	@Override
	public String getCfActNotNonpay() {
		return constantFields.getActNotTypCd().getNonpay();
	}

	@Override
	public void setCfActNotNonpay(String cfActNotNonpay) {
		this.constantFields.getActNotTypCd().setNonpay(cfActNotNonpay);
	}

	@Override
	public String getCfActNotRescind() {
		return constantFields.getActNotTypCd().getRescind();
	}

	@Override
	public void setCfActNotRescind(String cfActNotRescind) {
		this.constantFields.getActNotTypCd().setRescind(cfActNotRescind);
	}

	@Override
	public String getCfActNotTypCdImp() {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	@Override
	public void setCfActNotTypCdImp(String cfActNotTypCdImp) {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	public ConstantFieldsXz0p90l0 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		return dclactNot.getCsrActNbr();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.dclactNot.setCsrActNbr(csrActNbr);
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public Ea01NoRescinds getEa01NoRescinds() {
		return ea01NoRescinds;
	}

	public Ea02ImpInProgress getEa02ImpInProgress() {
		return ea02ImpInProgress;
	}

	@Override
	public String getEmpId() {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public void setEmpId(String empId) {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	public Es01FatalErrorMsg getEs01FatalErrorMsg() {
		return es01FatalErrorMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotDt() {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public void setNotDt(String notDt) {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public String getNotEffDt() {
		return dclactNotPol.getNotEffDt();
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		this.dclactNotPol.setNotEffDt(notEffDt);
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getPolEffDt() {
		return dclactNotPol.getPolEffDt();
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		this.dclactNotPol.setPolEffDt(polEffDt);
	}

	@Override
	public String getPolExpDt() {
		return dclactNotPol.getPolExpDt();
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		this.dclactNotPol.setPolExpDt(polExpDt);
	}

	@Override
	public String getPolNbr() {
		return dclactNotPol.getPolNbr();
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.dclactNotPol.setPolNbr(polNbr);
	}

	@Override
	public String getPolTypCd() {
		throw new FieldNotMappedException("polTypCd");
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		throw new FieldNotMappedException("polTypCd");
	}

	public SubscriptsXz0p90d0 getSubscripts() {
		return subscripts;
	}

	public SwitchesXz0p90l0 getSwitches() {
		return switches;
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		return getTotFeeAmt().toString();
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
	}

	public TpPoliciesXz0p90l0 getTpPolicies(int idx) {
		return tpPolicies[idx - 1];
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0p90l0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsBillingLinkage getWsBillingLinkage() {
		return wsBillingLinkage;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	@Override
	public short getWsNbrOfNotDay() {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	@Override
	public void setWsNbrOfNotDay(short wsNbrOfNotDay) {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	@Override
	public String getWsPolicyCancDt() {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}

	@Override
	public void setWsPolicyCancDt(String wsPolicyCancDt) {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}

	public WsProxyProgramArea getWsProxyProgramArea() {
		return wsProxyProgramArea;
	}

	public WsXz0y90a0Row getWsXz0y90l0Row() {
		return wsXz0y90l0Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
