/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program XZC06090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsXzc06090 {

	//==== PROPERTIES ====
	//Original name: SS-ER
	private short er = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-LC
	private short lc = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-PT
	private short pt = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setEr(short er) {
		this.er = er;
	}

	public short getEr() {
		return this.er;
	}

	public void setLc(short lc) {
		this.lc = lc;
	}

	public short getLc() {
		return this.lc;
	}

	public void setPt(short pt) {
		this.pt = pt;
	}

	public short getPt() {
		return this.pt;
	}
}
