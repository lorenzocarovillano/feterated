/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: PPC-ERROR-HANDLING-PARMS<br>
 * Variable: PPC-ERROR-HANDLING-PARMS from program XZ0B90S0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PpcErrorHandlingParms extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int NON_LOGGABLE_ERRORS_MAXOCCURS = 10;
	public static final int WARNINGS_MAXOCCURS = 10;
	public static final String PPC_FATAL_ERROR_CODE = "0300";
	public static final String PPC_NLBE_CODE = "0200";
	public static final String PPC_WARNING_CODE = "0100";
	public static final String PPC_NO_ERROR_CODE = "0000";

	//==== CONSTRUCTORS ====
	public PpcErrorHandlingParms() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.PPC_ERROR_HANDLING_PARMS;
	}

	public void setPpcErrorHandlingParmsBytes(byte[] buffer) {
		setPpcErrorHandlingParmsBytes(buffer, 1);
	}

	public void setPpcErrorHandlingParmsBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.PPC_ERROR_HANDLING_PARMS, Pos.PPC_ERROR_HANDLING_PARMS);
	}

	public void setErrorReturnCode(short errorReturnCode) {
		writeShort(Pos.ERROR_RETURN_CODE, errorReturnCode, Len.Int.ERROR_RETURN_CODE, SignType.NO_SIGN);
	}

	/**Original name: PPC-ERROR-RETURN-CODE<br>*/
	public short getErrorReturnCode() {
		return readNumDispUnsignedShort(Pos.ERROR_RETURN_CODE, Len.ERROR_RETURN_CODE);
	}

	public String getErrorReturnCodeFormatted() {
		return readFixedString(Pos.ERROR_RETURN_CODE, Len.ERROR_RETURN_CODE);
	}

	public boolean isPpcNoErrorCode() {
		return getErrorReturnCodeFormatted().equals(PPC_NO_ERROR_CODE);
	}

	public void setFatalErrorMessage(String fatalErrorMessage) {
		writeString(Pos.FATAL_ERROR_MESSAGE, fatalErrorMessage, Len.FATAL_ERROR_MESSAGE);
	}

	/**Original name: PPC-FATAL-ERROR-MESSAGE<br>*/
	public String getFatalErrorMessage() {
		return readString(Pos.FATAL_ERROR_MESSAGE, Len.FATAL_ERROR_MESSAGE);
	}

	/**Original name: PPC-NON-LOG-ERR-MSG<br>*/
	public String getNonLogErrMsg(int nonLogErrMsgIdx) {
		int position = Pos.ppcNonLogErrMsg(nonLogErrMsgIdx - 1);
		return readString(position, Len.NON_LOG_ERR_MSG);
	}

	/**Original name: PPC-WARN-MSG<br>*/
	public String getWarnMsg(int warnMsgIdx) {
		int position = Pos.ppcWarnMsg(warnMsgIdx - 1);
		return readString(position, Len.WARN_MSG);
	}

	public void setGroupNonLogMsg(String groupNonLogMsg) {
		writeString(Pos.GROUP_NON_LOG_MSG, groupNonLogMsg, Len.GROUP_NON_LOG_MSG);
	}

	/**Original name: PPC-GROUP-NON-LOG-MSG<br>*/
	public String getGroupNonLogMsg() {
		return readString(Pos.GROUP_NON_LOG_MSG, Len.GROUP_NON_LOG_MSG);
	}

	public void setGroupWngMsg(String groupWngMsg) {
		writeString(Pos.GROUP_WNG_MSG, groupWngMsg, Len.GROUP_WNG_MSG);
	}

	/**Original name: PPC-GROUP-WNG-MSG<br>*/
	public String getGroupWngMsg() {
		return readString(Pos.GROUP_WNG_MSG, Len.GROUP_WNG_MSG);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int PPC_ERROR_HANDLING_PARMS = 1;
		public static final int ERROR_RETURN_CODE = PPC_ERROR_HANDLING_PARMS;
		public static final int FATAL_ERROR_MESSAGE = ERROR_RETURN_CODE + Len.ERROR_RETURN_CODE;
		public static final int NON_LOGGABLE_ERROR_CNT = FATAL_ERROR_MESSAGE + Len.FATAL_ERROR_MESSAGE;
		public static final int WARNING_CNT = ppcNonLogErrMsg(NON_LOGGABLE_ERRORS_MAXOCCURS - 1) + Len.NON_LOG_ERR_MSG;
		public static final int FILLER_PPC_OUTPUT_PARMS = 1;
		public static final int FLR1 = FILLER_PPC_OUTPUT_PARMS;
		public static final int GROUP_NON_LOG_MSG = FLR1 + Len.FLR1;
		public static final int FLR2 = GROUP_NON_LOG_MSG + Len.GROUP_NON_LOG_MSG;
		public static final int GROUP_WNG_MSG = FLR2 + Len.FLR2;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int ppcNonLoggableErrors(int idx) {
			return NON_LOGGABLE_ERROR_CNT + Len.NON_LOGGABLE_ERROR_CNT + idx * Len.NON_LOGGABLE_ERRORS;
		}

		public static int ppcNonLogErrMsg(int idx) {
			return ppcNonLoggableErrors(idx);
		}

		public static int ppcWarnings(int idx) {
			return WARNING_CNT + Len.WARNING_CNT + idx * Len.WARNINGS;
		}

		public static int ppcWarnMsg(int idx) {
			return ppcWarnings(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_RETURN_CODE = 4;
		public static final int FATAL_ERROR_MESSAGE = 250;
		public static final int NON_LOGGABLE_ERROR_CNT = 4;
		public static final int NON_LOG_ERR_MSG = 500;
		public static final int NON_LOGGABLE_ERRORS = NON_LOG_ERR_MSG;
		public static final int WARNING_CNT = 4;
		public static final int WARN_MSG = 500;
		public static final int WARNINGS = WARN_MSG;
		public static final int FLR1 = 258;
		public static final int GROUP_NON_LOG_MSG = 5000;
		public static final int FLR2 = 4;
		public static final int PPC_ERROR_HANDLING_PARMS = ERROR_RETURN_CODE + FATAL_ERROR_MESSAGE + NON_LOGGABLE_ERROR_CNT
				+ PpcErrorHandlingParms.NON_LOGGABLE_ERRORS_MAXOCCURS * NON_LOGGABLE_ERRORS + WARNING_CNT
				+ PpcErrorHandlingParms.WARNINGS_MAXOCCURS * WARNINGS;
		public static final int GROUP_WNG_MSG = 5000;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int ERROR_RETURN_CODE = 4;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
