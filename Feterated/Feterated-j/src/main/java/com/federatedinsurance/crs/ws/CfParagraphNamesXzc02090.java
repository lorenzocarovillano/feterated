/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-PARAGRAPH-NAMES<br>
 * Variable: CF-PARAGRAPH-NAMES from program XZC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfParagraphNamesXzc02090 {

	//==== PROPERTIES ====
	//Original name: CF-PN-2110
	private String pn2110 = "2110";
	//Original name: CF-PN-2120
	private String pn2120 = "2120";
	//Original name: CF-PN-2130
	private String pn2130 = "2130";
	//Original name: CF-PN-2140
	private String pn2140 = "2140";
	//Original name: CF-PN-3200
	private String pn3200 = "3200";
	//Original name: CF-PN-3310
	private String pn3310 = "3310";

	//==== METHODS ====
	public String getPn2110() {
		return this.pn2110;
	}

	public String getPn2120() {
		return this.pn2120;
	}

	public String getPn2130() {
		return this.pn2130;
	}

	public String getPn2140() {
		return this.pn2140;
	}

	public String getPn3200() {
		return this.pn3200;
	}

	public String getPn3310() {
		return this.pn3310;
	}
}
