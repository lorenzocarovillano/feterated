/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZY022-WARNINGS<br>
 * Variables: XZY022-WARNINGS from copybook XZ0Y0022<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzy022Warnings {

	//==== PROPERTIES ====
	//Original name: XZY022-WARN-MSG
	private String xzy022WarnMsg = DefaultValues.stringVal(Len.XZY022_WARN_MSG);

	//==== METHODS ====
	public void setWarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzy022WarnMsg = MarshalByte.readString(buffer, position, Len.XZY022_WARN_MSG);
	}

	public byte[] getWarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzy022WarnMsg, Len.XZY022_WARN_MSG);
		return buffer;
	}

	public void initWarningsSpaces() {
		xzy022WarnMsg = "";
	}

	public void setXzy022WarnMsg(String xzy022WarnMsg) {
		this.xzy022WarnMsg = Functions.subString(xzy022WarnMsg, Len.XZY022_WARN_MSG);
	}

	public String getXzy022WarnMsg() {
		return this.xzy022WarnMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY022_WARN_MSG = 400;
		public static final int WARNINGS = XZY022_WARN_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
