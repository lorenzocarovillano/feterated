/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q9072<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q9072 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q9072() {
	}

	public LFrameworkRequestAreaXz0q9072(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza972qMaxTagRows(short xza972qMaxTagRows) {
		writeBinaryShort(Pos.XZA972_MAX_TAG_ROWS, xza972qMaxTagRows);
	}

	/**Original name: XZA972Q-MAX-TAG-ROWS<br>*/
	public short getXza972qMaxTagRows() {
		return readBinaryShort(Pos.XZA972_MAX_TAG_ROWS);
	}

	public void setXza972qEdlFrmNm(String xza972qEdlFrmNm) {
		writeString(Pos.XZA972_EDL_FRM_NM, xza972qEdlFrmNm, Len.XZA972_EDL_FRM_NM);
	}

	/**Original name: XZA972Q-EDL-FRM-NM<br>*/
	public String getXza972qEdlFrmNm() {
		return readString(Pos.XZA972_EDL_FRM_NM, Len.XZA972_EDL_FRM_NM);
	}

	public void setXza972qDtaGrpNm(String xza972qDtaGrpNm) {
		writeString(Pos.XZA972_DTA_GRP_NM, xza972qDtaGrpNm, Len.XZA972_DTA_GRP_NM);
	}

	/**Original name: XZA972Q-DTA-GRP-NM<br>*/
	public String getXza972qDtaGrpNm() {
		return readString(Pos.XZA972_DTA_GRP_NM, Len.XZA972_DTA_GRP_NM);
	}

	public void setXza972qDtaGrpFldNbr(int xza972qDtaGrpFldNbr) {
		writeInt(Pos.XZA972_DTA_GRP_FLD_NBR, xza972qDtaGrpFldNbr, Len.Int.XZA972Q_DTA_GRP_FLD_NBR);
	}

	/**Original name: XZA972Q-DTA-GRP-FLD-NBR<br>*/
	public int getXza972qDtaGrpFldNbr() {
		return readNumDispInt(Pos.XZA972_DTA_GRP_FLD_NBR, Len.XZA972_DTA_GRP_FLD_NBR);
	}

	public void setXza972qTagNm(String xza972qTagNm) {
		writeString(Pos.XZA972_TAG_NM, xza972qTagNm, Len.XZA972_TAG_NM);
	}

	/**Original name: XZA972Q-TAG-NM<br>*/
	public String getXza972qTagNm() {
		return readString(Pos.XZA972_TAG_NM, Len.XZA972_TAG_NM);
	}

	public void setXza972qJusCd(char xza972qJusCd) {
		writeChar(Pos.XZA972_JUS_CD, xza972qJusCd);
	}

	/**Original name: XZA972Q-JUS-CD<br>*/
	public char getXza972qJusCd() {
		return readChar(Pos.XZA972_JUS_CD);
	}

	public void setXza972qSpePrcCd(String xza972qSpePrcCd) {
		writeString(Pos.XZA972_SPE_PRC_CD, xza972qSpePrcCd, Len.XZA972_SPE_PRC_CD);
	}

	/**Original name: XZA972Q-SPE-PRC-CD<br>*/
	public String getXza972qSpePrcCd() {
		return readString(Pos.XZA972_SPE_PRC_CD, Len.XZA972_SPE_PRC_CD);
	}

	public void setXza972qLenNbr(int xza972qLenNbr) {
		writeInt(Pos.XZA972_LEN_NBR, xza972qLenNbr, Len.Int.XZA972Q_LEN_NBR);
	}

	/**Original name: XZA972Q-LEN-NBR<br>*/
	public int getXza972qLenNbr() {
		return readNumDispInt(Pos.XZA972_LEN_NBR, Len.XZA972_LEN_NBR);
	}

	public void setXza972qTagOccCnt(int xza972qTagOccCnt) {
		writeInt(Pos.XZA972_TAG_OCC_CNT, xza972qTagOccCnt, Len.Int.XZA972Q_TAG_OCC_CNT);
	}

	/**Original name: XZA972Q-TAG-OCC-CNT<br>*/
	public int getXza972qTagOccCnt() {
		return readNumDispInt(Pos.XZA972_TAG_OCC_CNT, Len.XZA972_TAG_OCC_CNT);
	}

	public void setXza972qDelInd(char xza972qDelInd) {
		writeChar(Pos.XZA972_DEL_IND, xza972qDelInd);
	}

	/**Original name: XZA972Q-DEL-IND<br>*/
	public char getXza972qDelInd() {
		return readChar(Pos.XZA972_DEL_IND);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A9072 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA972_ACY_TAG_ROW = L_FW_REQ_XZ0A9072;
		public static final int XZA972_MAX_TAG_ROWS = XZA972_ACY_TAG_ROW;
		public static final int XZA972_ACY_TAG_INFO = XZA972_MAX_TAG_ROWS + Len.XZA972_MAX_TAG_ROWS;
		public static final int XZA972_EDL_FRM_NM = XZA972_ACY_TAG_INFO;
		public static final int XZA972_DTA_GRP_NM = XZA972_EDL_FRM_NM + Len.XZA972_EDL_FRM_NM;
		public static final int XZA972_DTA_GRP_FLD_NBR = XZA972_DTA_GRP_NM + Len.XZA972_DTA_GRP_NM;
		public static final int XZA972_TAG_NM = XZA972_DTA_GRP_FLD_NBR + Len.XZA972_DTA_GRP_FLD_NBR;
		public static final int XZA972_JUS_CD = XZA972_TAG_NM + Len.XZA972_TAG_NM;
		public static final int XZA972_SPE_PRC_CD = XZA972_JUS_CD + Len.XZA972_JUS_CD;
		public static final int XZA972_LEN_NBR = XZA972_SPE_PRC_CD + Len.XZA972_SPE_PRC_CD;
		public static final int XZA972_TAG_OCC_CNT = XZA972_LEN_NBR + Len.XZA972_LEN_NBR;
		public static final int XZA972_DEL_IND = XZA972_TAG_OCC_CNT + Len.XZA972_TAG_OCC_CNT;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA972_MAX_TAG_ROWS = 2;
		public static final int XZA972_EDL_FRM_NM = 30;
		public static final int XZA972_DTA_GRP_NM = 8;
		public static final int XZA972_DTA_GRP_FLD_NBR = 5;
		public static final int XZA972_TAG_NM = 30;
		public static final int XZA972_JUS_CD = 1;
		public static final int XZA972_SPE_PRC_CD = 8;
		public static final int XZA972_LEN_NBR = 5;
		public static final int XZA972_TAG_OCC_CNT = 5;
		public static final int XZA972_DEL_IND = 1;
		public static final int XZA972_ACY_TAG_INFO = XZA972_EDL_FRM_NM + XZA972_DTA_GRP_NM + XZA972_DTA_GRP_FLD_NBR + XZA972_TAG_NM + XZA972_JUS_CD
				+ XZA972_SPE_PRC_CD + XZA972_LEN_NBR + XZA972_TAG_OCC_CNT + XZA972_DEL_IND;
		public static final int XZA972_ACY_TAG_ROW = XZA972_MAX_TAG_ROWS + XZA972_ACY_TAG_INFO;
		public static final int L_FW_REQ_XZ0A9072 = XZA972_ACY_TAG_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A9072;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA972Q_DTA_GRP_FLD_NBR = 5;
			public static final int XZA972Q_LEN_NBR = 5;
			public static final int XZA972Q_TAG_OCC_CNT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
