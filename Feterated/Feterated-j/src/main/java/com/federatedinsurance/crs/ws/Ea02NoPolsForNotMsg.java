/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-NO-POLS-FOR-NOT-MSG<br>
 * Variable: EA-02-NO-POLS-FOR-NOT-MSG from program XZ0P90H0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02NoPolsForNotMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-NO-POLS-FOR-NOT-MSG
	private String flr1 = "No policies";
	//Original name: FILLER-EA-02-NO-POLS-FOR-NOT-MSG-1
	private String flr2 = "qualified for";
	//Original name: FILLER-EA-02-NO-POLS-FOR-NOT-MSG-2
	private String flr3 = "notification";
	//Original name: FILLER-EA-02-NO-POLS-FOR-NOT-MSG-3
	private String flr4 = "type.";
	//Original name: FILLER-EA-02-NO-POLS-FOR-NOT-MSG-4
	private String flr5 = "Account =";
	//Original name: EA-02-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-02-NO-POLS-FOR-NOT-MSG-5
	private String flr6 = "; Notification";
	//Original name: FILLER-EA-02-NO-POLS-FOR-NOT-MSG-6
	private String flr7 = "TimeStamp =";
	//Original name: EA-02-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-02-NO-POLS-FOR-NOT-MSG-7
	private char flr8 = '.';

	//==== METHODS ====
	public String getEa02NoPolsForNotMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02NoPolsForNotMsgBytes());
	}

	public byte[] getEa02NoPolsForNotMsgBytes() {
		byte[] buffer = new byte[Len.EA02_NO_POLS_FOR_NOT_MSG];
		return getEa02NoPolsForNotMsgBytes(buffer, 1);
	}

	public byte[] getEa02NoPolsForNotMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr8);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 12;
		public static final int FLR2 = 14;
		public static final int FLR3 = 13;
		public static final int FLR4 = 6;
		public static final int FLR5 = 10;
		public static final int FLR6 = 15;
		public static final int FLR8 = 1;
		public static final int EA02_NO_POLS_FOR_NOT_MSG = CSR_ACT_NBR + NOT_PRC_TS + 2 * FLR1 + FLR2 + FLR3 + FLR4 + FLR5 + FLR6 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
