/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XZC001-ACT-NOT-DATA<br>
 * Variable: XZC001-ACT-NOT-DATA from copybook XZ0C0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzc001ActNotData {

	//==== PROPERTIES ====
	/**Original name: XZC001-ACT-NOT-TYP-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char actNotTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZC001-NOT-DT-CI
	private char notDtCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-NOT-DT
	private String notDt = DefaultValues.stringVal(Len.NOT_DT);
	//Original name: XZC001-ACT-OWN-CLT-ID-CI
	private char actOwnCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ACT-OWN-CLT-ID
	private String actOwnCltId = DefaultValues.stringVal(Len.ACT_OWN_CLT_ID);
	//Original name: XZC001-ACT-OWN-ADR-ID-CI
	private char actOwnAdrIdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ACT-OWN-ADR-ID
	private String actOwnAdrId = DefaultValues.stringVal(Len.ACT_OWN_ADR_ID);
	//Original name: XZC001-EMP-ID-CI
	private char empIdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-EMP-ID-NI
	private char empIdNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-EMP-ID
	private String empId = DefaultValues.stringVal(Len.EMP_ID);
	//Original name: XZC001-STA-MDF-TS-CI
	private char staMdfTsCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-STA-MDF-TS
	private String staMdfTs = DefaultValues.stringVal(Len.STA_MDF_TS);
	//Original name: XZC001-ACT-NOT-STA-CD-CI
	private char actNotStaCdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ACT-NOT-STA-CD
	private String actNotStaCd = DefaultValues.stringVal(Len.ACT_NOT_STA_CD);
	//Original name: XZC001-PDC-NBR-CI
	private char pdcNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-PDC-NBR-NI
	private char pdcNbrNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-PDC-NBR
	private String pdcNbr = DefaultValues.stringVal(Len.PDC_NBR);
	//Original name: XZC001-PDC-NM-CI
	private char pdcNmCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-PDC-NM-NI
	private char pdcNmNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-PDC-NM
	private String pdcNm = DefaultValues.stringVal(Len.PDC_NM);
	//Original name: XZC001-SEG-CD-CI
	private char segCdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-SEG-CD-NI
	private char segCdNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-SEG-CD
	private String segCd = DefaultValues.stringVal(Len.SEG_CD);
	//Original name: XZC001-ACT-TYP-CD-CI
	private char actTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ACT-TYP-CD-NI
	private char actTypCdNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ACT-TYP-CD
	private String actTypCd = DefaultValues.stringVal(Len.ACT_TYP_CD);
	//Original name: XZC001-TOT-FEE-AMT-CI
	private char totFeeAmtCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-TOT-FEE-AMT-NI
	private char totFeeAmtNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-TOT-FEE-AMT-SIGN
	private char totFeeAmtSign = DefaultValues.CHAR_VAL;
	//Original name: XZC001-TOT-FEE-AMT
	private AfDecimal totFeeAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: XZC001-ST-ABB-CI
	private char stAbbCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ST-ABB-NI
	private char stAbbNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZC001-CER-HLD-NOT-IND-CI
	private char cerHldNotIndCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-CER-HLD-NOT-IND-NI
	private char cerHldNotIndNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-CER-HLD-NOT-IND
	private char cerHldNotInd = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ADD-CNC-DAY-CI
	private char addCncDayCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ADD-CNC-DAY-NI
	private char addCncDayNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ADD-CNC-DAY-SIGN
	private char addCncDaySign = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ADD-CNC-DAY
	private String addCncDay = DefaultValues.stringVal(Len.ADD_CNC_DAY);
	//Original name: XZC001-REA-DES-CI
	private char reaDesCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-REA-DES-NI
	private char reaDesNi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-REA-DES
	private String reaDes = DefaultValues.stringVal(Len.REA_DES);

	//==== METHODS ====
	public void setActNotDataBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		actNotTypCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		notDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notDt = MarshalByte.readString(buffer, position, Len.NOT_DT);
		position += Len.NOT_DT;
		actOwnCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		actOwnCltId = MarshalByte.readString(buffer, position, Len.ACT_OWN_CLT_ID);
		position += Len.ACT_OWN_CLT_ID;
		actOwnAdrIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		actOwnAdrId = MarshalByte.readString(buffer, position, Len.ACT_OWN_ADR_ID);
		position += Len.ACT_OWN_ADR_ID;
		empIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		empIdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		empId = MarshalByte.readString(buffer, position, Len.EMP_ID);
		position += Len.EMP_ID;
		staMdfTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		staMdfTs = MarshalByte.readString(buffer, position, Len.STA_MDF_TS);
		position += Len.STA_MDF_TS;
		actNotStaCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		actNotStaCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_STA_CD);
		position += Len.ACT_NOT_STA_CD;
		pdcNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pdcNbrNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pdcNbr = MarshalByte.readString(buffer, position, Len.PDC_NBR);
		position += Len.PDC_NBR;
		pdcNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pdcNmNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pdcNm = MarshalByte.readString(buffer, position, Len.PDC_NM);
		position += Len.PDC_NM;
		segCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		segCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		segCd = MarshalByte.readString(buffer, position, Len.SEG_CD);
		position += Len.SEG_CD;
		actTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		actTypCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		actTypCd = MarshalByte.readString(buffer, position, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		totFeeAmtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		totFeeAmtNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		totFeeAmtSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		totFeeAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.TOT_FEE_AMT, Len.Fract.TOT_FEE_AMT, SignType.NO_SIGN));
		position += Len.TOT_FEE_AMT;
		stAbbCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		stAbbNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		stAbb = MarshalByte.readString(buffer, position, Len.ST_ABB);
		position += Len.ST_ABB;
		cerHldNotIndCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cerHldNotIndNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cerHldNotInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addCncDayCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addCncDayNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addCncDaySign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addCncDay = MarshalByte.readFixedString(buffer, position, Len.ADD_CNC_DAY);
		position += Len.ADD_CNC_DAY;
		reaDesCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		reaDesNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		reaDes = MarshalByte.readString(buffer, position, Len.REA_DES);
	}

	public byte[] getActNotDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, actNotTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeChar(buffer, position, notDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, notDt, Len.NOT_DT);
		position += Len.NOT_DT;
		MarshalByte.writeChar(buffer, position, actOwnCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, actOwnCltId, Len.ACT_OWN_CLT_ID);
		position += Len.ACT_OWN_CLT_ID;
		MarshalByte.writeChar(buffer, position, actOwnAdrIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, actOwnAdrId, Len.ACT_OWN_ADR_ID);
		position += Len.ACT_OWN_ADR_ID;
		MarshalByte.writeChar(buffer, position, empIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, empIdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, empId, Len.EMP_ID);
		position += Len.EMP_ID;
		MarshalByte.writeChar(buffer, position, staMdfTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, staMdfTs, Len.STA_MDF_TS);
		position += Len.STA_MDF_TS;
		MarshalByte.writeChar(buffer, position, actNotStaCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, actNotStaCd, Len.ACT_NOT_STA_CD);
		position += Len.ACT_NOT_STA_CD;
		MarshalByte.writeChar(buffer, position, pdcNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pdcNbrNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pdcNbr, Len.PDC_NBR);
		position += Len.PDC_NBR;
		MarshalByte.writeChar(buffer, position, pdcNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pdcNmNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pdcNm, Len.PDC_NM);
		position += Len.PDC_NM;
		MarshalByte.writeChar(buffer, position, segCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, segCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, segCd, Len.SEG_CD);
		position += Len.SEG_CD;
		MarshalByte.writeChar(buffer, position, actTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, actTypCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, actTypCd, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		MarshalByte.writeChar(buffer, position, totFeeAmtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, totFeeAmtNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, totFeeAmtSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeDecimal(buffer, position, totFeeAmt.copy(), SignType.NO_SIGN);
		position += Len.TOT_FEE_AMT;
		MarshalByte.writeChar(buffer, position, stAbbCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, stAbbNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeChar(buffer, position, cerHldNotIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cerHldNotIndNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cerHldNotInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, addCncDayCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, addCncDayNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, addCncDaySign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, addCncDay, Len.ADD_CNC_DAY);
		position += Len.ADD_CNC_DAY;
		MarshalByte.writeChar(buffer, position, reaDesCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, reaDesNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, reaDes, Len.REA_DES);
		return buffer;
	}

	public void initActNotDataSpaces() {
		actNotTypCdCi = Types.SPACE_CHAR;
		actNotTypCd = "";
		notDtCi = Types.SPACE_CHAR;
		notDt = "";
		actOwnCltIdCi = Types.SPACE_CHAR;
		actOwnCltId = "";
		actOwnAdrIdCi = Types.SPACE_CHAR;
		actOwnAdrId = "";
		empIdCi = Types.SPACE_CHAR;
		empIdNi = Types.SPACE_CHAR;
		empId = "";
		staMdfTsCi = Types.SPACE_CHAR;
		staMdfTs = "";
		actNotStaCdCi = Types.SPACE_CHAR;
		actNotStaCd = "";
		pdcNbrCi = Types.SPACE_CHAR;
		pdcNbrNi = Types.SPACE_CHAR;
		pdcNbr = "";
		pdcNmCi = Types.SPACE_CHAR;
		pdcNmNi = Types.SPACE_CHAR;
		pdcNm = "";
		segCdCi = Types.SPACE_CHAR;
		segCdNi = Types.SPACE_CHAR;
		segCd = "";
		actTypCdCi = Types.SPACE_CHAR;
		actTypCdNi = Types.SPACE_CHAR;
		actTypCd = "";
		totFeeAmtCi = Types.SPACE_CHAR;
		totFeeAmtNi = Types.SPACE_CHAR;
		totFeeAmtSign = Types.SPACE_CHAR;
		totFeeAmt.setNaN();
		stAbbCi = Types.SPACE_CHAR;
		stAbbNi = Types.SPACE_CHAR;
		stAbb = "";
		cerHldNotIndCi = Types.SPACE_CHAR;
		cerHldNotIndNi = Types.SPACE_CHAR;
		cerHldNotInd = Types.SPACE_CHAR;
		addCncDayCi = Types.SPACE_CHAR;
		addCncDayNi = Types.SPACE_CHAR;
		addCncDaySign = Types.SPACE_CHAR;
		addCncDay = "";
		reaDesCi = Types.SPACE_CHAR;
		reaDesNi = Types.SPACE_CHAR;
		reaDes = "";
	}

	public void setActNotTypCdCi(char actNotTypCdCi) {
		this.actNotTypCdCi = actNotTypCdCi;
	}

	public char getActNotTypCdCi() {
		return this.actNotTypCdCi;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getActNotTypCdFormatted() {
		return Functions.padBlanks(getActNotTypCd(), Len.ACT_NOT_TYP_CD);
	}

	public void setNotDtCi(char notDtCi) {
		this.notDtCi = notDtCi;
	}

	public char getNotDtCi() {
		return this.notDtCi;
	}

	public void setNotDt(String notDt) {
		this.notDt = Functions.subString(notDt, Len.NOT_DT);
	}

	public String getNotDt() {
		return this.notDt;
	}

	public String getNotDtFormatted() {
		return Functions.padBlanks(getNotDt(), Len.NOT_DT);
	}

	public void setActOwnCltIdCi(char actOwnCltIdCi) {
		this.actOwnCltIdCi = actOwnCltIdCi;
	}

	public char getActOwnCltIdCi() {
		return this.actOwnCltIdCi;
	}

	public void setActOwnCltId(String actOwnCltId) {
		this.actOwnCltId = Functions.subString(actOwnCltId, Len.ACT_OWN_CLT_ID);
	}

	public String getActOwnCltId() {
		return this.actOwnCltId;
	}

	public void setActOwnAdrIdCi(char actOwnAdrIdCi) {
		this.actOwnAdrIdCi = actOwnAdrIdCi;
	}

	public char getActOwnAdrIdCi() {
		return this.actOwnAdrIdCi;
	}

	public void setActOwnAdrId(String actOwnAdrId) {
		this.actOwnAdrId = Functions.subString(actOwnAdrId, Len.ACT_OWN_ADR_ID);
	}

	public String getActOwnAdrId() {
		return this.actOwnAdrId;
	}

	public void setEmpIdCi(char empIdCi) {
		this.empIdCi = empIdCi;
	}

	public char getEmpIdCi() {
		return this.empIdCi;
	}

	public void setEmpIdNi(char empIdNi) {
		this.empIdNi = empIdNi;
	}

	public void setEmpIdNiFormatted(String empIdNi) {
		setEmpIdNi(Functions.charAt(empIdNi, Types.CHAR_SIZE));
	}

	public char getEmpIdNi() {
		return this.empIdNi;
	}

	public void setEmpId(String empId) {
		this.empId = Functions.subString(empId, Len.EMP_ID);
	}

	public String getEmpId() {
		return this.empId;
	}

	public String getEmpIdFormatted() {
		return Functions.padBlanks(getEmpId(), Len.EMP_ID);
	}

	public void setStaMdfTsCi(char staMdfTsCi) {
		this.staMdfTsCi = staMdfTsCi;
	}

	public char getStaMdfTsCi() {
		return this.staMdfTsCi;
	}

	public void setStaMdfTs(String staMdfTs) {
		this.staMdfTs = Functions.subString(staMdfTs, Len.STA_MDF_TS);
	}

	public String getStaMdfTs() {
		return this.staMdfTs;
	}

	public void setActNotStaCdCi(char actNotStaCdCi) {
		this.actNotStaCdCi = actNotStaCdCi;
	}

	public char getActNotStaCdCi() {
		return this.actNotStaCdCi;
	}

	public void setActNotStaCd(String actNotStaCd) {
		this.actNotStaCd = Functions.subString(actNotStaCd, Len.ACT_NOT_STA_CD);
	}

	public String getActNotStaCd() {
		return this.actNotStaCd;
	}

	public void setPdcNbrCi(char pdcNbrCi) {
		this.pdcNbrCi = pdcNbrCi;
	}

	public char getPdcNbrCi() {
		return this.pdcNbrCi;
	}

	public void setPdcNbrNi(char pdcNbrNi) {
		this.pdcNbrNi = pdcNbrNi;
	}

	public void setPdcNbrNiFormatted(String pdcNbrNi) {
		setPdcNbrNi(Functions.charAt(pdcNbrNi, Types.CHAR_SIZE));
	}

	public char getPdcNbrNi() {
		return this.pdcNbrNi;
	}

	public void setPdcNbr(String pdcNbr) {
		this.pdcNbr = Functions.subString(pdcNbr, Len.PDC_NBR);
	}

	public String getPdcNbr() {
		return this.pdcNbr;
	}

	public String getPdcNbrFormatted() {
		return Functions.padBlanks(getPdcNbr(), Len.PDC_NBR);
	}

	public void setPdcNmCi(char pdcNmCi) {
		this.pdcNmCi = pdcNmCi;
	}

	public char getPdcNmCi() {
		return this.pdcNmCi;
	}

	public void setPdcNmNi(char pdcNmNi) {
		this.pdcNmNi = pdcNmNi;
	}

	public void setPdcNmNiFormatted(String pdcNmNi) {
		setPdcNmNi(Functions.charAt(pdcNmNi, Types.CHAR_SIZE));
	}

	public char getPdcNmNi() {
		return this.pdcNmNi;
	}

	public void setPdcNm(String pdcNm) {
		this.pdcNm = Functions.subString(pdcNm, Len.PDC_NM);
	}

	public String getPdcNm() {
		return this.pdcNm;
	}

	public String getPdcNmFormatted() {
		return Functions.padBlanks(getPdcNm(), Len.PDC_NM);
	}

	public void setSegCdCi(char segCdCi) {
		this.segCdCi = segCdCi;
	}

	public char getSegCdCi() {
		return this.segCdCi;
	}

	public void setSegCdNi(char segCdNi) {
		this.segCdNi = segCdNi;
	}

	public void setSegCdNiFormatted(String segCdNi) {
		setSegCdNi(Functions.charAt(segCdNi, Types.CHAR_SIZE));
	}

	public char getSegCdNi() {
		return this.segCdNi;
	}

	public void setSegCd(String segCd) {
		this.segCd = Functions.subString(segCd, Len.SEG_CD);
	}

	public String getSegCd() {
		return this.segCd;
	}

	public String getSegCdFormatted() {
		return Functions.padBlanks(getSegCd(), Len.SEG_CD);
	}

	public void setActTypCdCi(char actTypCdCi) {
		this.actTypCdCi = actTypCdCi;
	}

	public char getActTypCdCi() {
		return this.actTypCdCi;
	}

	public void setActTypCdNi(char actTypCdNi) {
		this.actTypCdNi = actTypCdNi;
	}

	public void setActTypCdNiFormatted(String actTypCdNi) {
		setActTypCdNi(Functions.charAt(actTypCdNi, Types.CHAR_SIZE));
	}

	public char getActTypCdNi() {
		return this.actTypCdNi;
	}

	public void setActTypCd(String actTypCd) {
		this.actTypCd = Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	public String getActTypCd() {
		return this.actTypCd;
	}

	public void setTotFeeAmtCi(char totFeeAmtCi) {
		this.totFeeAmtCi = totFeeAmtCi;
	}

	public char getTotFeeAmtCi() {
		return this.totFeeAmtCi;
	}

	public void setTotFeeAmtNi(char totFeeAmtNi) {
		this.totFeeAmtNi = totFeeAmtNi;
	}

	public void setTotFeeAmtNiFormatted(String totFeeAmtNi) {
		setTotFeeAmtNi(Functions.charAt(totFeeAmtNi, Types.CHAR_SIZE));
	}

	public char getTotFeeAmtNi() {
		return this.totFeeAmtNi;
	}

	public void setTotFeeAmtSign(char totFeeAmtSign) {
		this.totFeeAmtSign = totFeeAmtSign;
	}

	public void setTotFeeAmtSignFormatted(String totFeeAmtSign) {
		setTotFeeAmtSign(Functions.charAt(totFeeAmtSign, Types.CHAR_SIZE));
	}

	public char getTotFeeAmtSign() {
		return this.totFeeAmtSign;
	}

	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		this.totFeeAmt.assign(totFeeAmt);
	}

	public AfDecimal getTotFeeAmt() {
		return this.totFeeAmt.copy();
	}

	public void setStAbbCi(char stAbbCi) {
		this.stAbbCi = stAbbCi;
	}

	public char getStAbbCi() {
		return this.stAbbCi;
	}

	public void setStAbbNi(char stAbbNi) {
		this.stAbbNi = stAbbNi;
	}

	public void setStAbbNiFormatted(String stAbbNi) {
		setStAbbNi(Functions.charAt(stAbbNi, Types.CHAR_SIZE));
	}

	public char getStAbbNi() {
		return this.stAbbNi;
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public String getStAbbFormatted() {
		return Functions.padBlanks(getStAbb(), Len.ST_ABB);
	}

	public void setCerHldNotIndCi(char cerHldNotIndCi) {
		this.cerHldNotIndCi = cerHldNotIndCi;
	}

	public char getCerHldNotIndCi() {
		return this.cerHldNotIndCi;
	}

	public void setCerHldNotIndNi(char cerHldNotIndNi) {
		this.cerHldNotIndNi = cerHldNotIndNi;
	}

	public void setCerHldNotIndNiFormatted(String cerHldNotIndNi) {
		setCerHldNotIndNi(Functions.charAt(cerHldNotIndNi, Types.CHAR_SIZE));
	}

	public char getCerHldNotIndNi() {
		return this.cerHldNotIndNi;
	}

	public void setCerHldNotInd(char cerHldNotInd) {
		this.cerHldNotInd = cerHldNotInd;
	}

	public char getCerHldNotInd() {
		return this.cerHldNotInd;
	}

	public void setAddCncDayCi(char addCncDayCi) {
		this.addCncDayCi = addCncDayCi;
	}

	public char getAddCncDayCi() {
		return this.addCncDayCi;
	}

	public void setAddCncDayNi(char addCncDayNi) {
		this.addCncDayNi = addCncDayNi;
	}

	public void setAddCncDayNiFormatted(String addCncDayNi) {
		setAddCncDayNi(Functions.charAt(addCncDayNi, Types.CHAR_SIZE));
	}

	public char getAddCncDayNi() {
		return this.addCncDayNi;
	}

	public void setAddCncDaySign(char addCncDaySign) {
		this.addCncDaySign = addCncDaySign;
	}

	public void setAddCncDaySignFormatted(String addCncDaySign) {
		setAddCncDaySign(Functions.charAt(addCncDaySign, Types.CHAR_SIZE));
	}

	public char getAddCncDaySign() {
		return this.addCncDaySign;
	}

	public void setAddCncDay(int addCncDay) {
		this.addCncDay = NumericDisplay.asString(addCncDay, Len.ADD_CNC_DAY);
	}

	public void setAddCncDayFormatted(String addCncDay) {
		this.addCncDay = Trunc.toUnsignedNumeric(addCncDay, Len.ADD_CNC_DAY);
	}

	public int getAddCncDay() {
		return NumericDisplay.asInt(this.addCncDay);
	}

	public String getAddCncDayFormatted() {
		return this.addCncDay;
	}

	public void setReaDesCi(char reaDesCi) {
		this.reaDesCi = reaDesCi;
	}

	public char getReaDesCi() {
		return this.reaDesCi;
	}

	public void setReaDesNi(char reaDesNi) {
		this.reaDesNi = reaDesNi;
	}

	public void setReaDesNiFormatted(String reaDesNi) {
		setReaDesNi(Functions.charAt(reaDesNi, Types.CHAR_SIZE));
	}

	public char getReaDesNi() {
		return this.reaDesNi;
	}

	public void setReaDes(String reaDes) {
		this.reaDes = Functions.subString(reaDes, Len.REA_DES);
	}

	public String getReaDes() {
		return this.reaDes;
	}

	public String getReaDesFormatted() {
		return Functions.padBlanks(getReaDes(), Len.REA_DES);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_TYP_CD_CI = 1;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int NOT_DT_CI = 1;
		public static final int NOT_DT = 10;
		public static final int ACT_OWN_CLT_ID_CI = 1;
		public static final int ACT_OWN_CLT_ID = 64;
		public static final int ACT_OWN_ADR_ID_CI = 1;
		public static final int ACT_OWN_ADR_ID = 64;
		public static final int EMP_ID_CI = 1;
		public static final int EMP_ID_NI = 1;
		public static final int EMP_ID = 6;
		public static final int STA_MDF_TS_CI = 1;
		public static final int STA_MDF_TS = 26;
		public static final int ACT_NOT_STA_CD_CI = 1;
		public static final int ACT_NOT_STA_CD = 2;
		public static final int PDC_NBR_CI = 1;
		public static final int PDC_NBR_NI = 1;
		public static final int PDC_NBR = 5;
		public static final int PDC_NM_CI = 1;
		public static final int PDC_NM_NI = 1;
		public static final int PDC_NM = 120;
		public static final int SEG_CD_CI = 1;
		public static final int SEG_CD_NI = 1;
		public static final int SEG_CD = 3;
		public static final int ACT_TYP_CD_CI = 1;
		public static final int ACT_TYP_CD_NI = 1;
		public static final int ACT_TYP_CD = 2;
		public static final int TOT_FEE_AMT_CI = 1;
		public static final int TOT_FEE_AMT_NI = 1;
		public static final int TOT_FEE_AMT_SIGN = 1;
		public static final int TOT_FEE_AMT = 10;
		public static final int ST_ABB_CI = 1;
		public static final int ST_ABB_NI = 1;
		public static final int ST_ABB = 2;
		public static final int CER_HLD_NOT_IND_CI = 1;
		public static final int CER_HLD_NOT_IND_NI = 1;
		public static final int CER_HLD_NOT_IND = 1;
		public static final int ADD_CNC_DAY_CI = 1;
		public static final int ADD_CNC_DAY_NI = 1;
		public static final int ADD_CNC_DAY_SIGN = 1;
		public static final int ADD_CNC_DAY = 5;
		public static final int REA_DES_CI = 1;
		public static final int REA_DES_NI = 1;
		public static final int REA_DES = 500;
		public static final int ACT_NOT_DATA = ACT_NOT_TYP_CD_CI + ACT_NOT_TYP_CD + NOT_DT_CI + NOT_DT + ACT_OWN_CLT_ID_CI + ACT_OWN_CLT_ID
				+ ACT_OWN_ADR_ID_CI + ACT_OWN_ADR_ID + EMP_ID_CI + EMP_ID_NI + EMP_ID + STA_MDF_TS_CI + STA_MDF_TS + ACT_NOT_STA_CD_CI
				+ ACT_NOT_STA_CD + PDC_NBR_CI + PDC_NBR_NI + PDC_NBR + PDC_NM_CI + PDC_NM_NI + PDC_NM + SEG_CD_CI + SEG_CD_NI + SEG_CD + ACT_TYP_CD_CI
				+ ACT_TYP_CD_NI + ACT_TYP_CD + TOT_FEE_AMT_CI + TOT_FEE_AMT_NI + TOT_FEE_AMT_SIGN + TOT_FEE_AMT + ST_ABB_CI + ST_ABB_NI + ST_ABB
				+ CER_HLD_NOT_IND_CI + CER_HLD_NOT_IND_NI + CER_HLD_NOT_IND + ADD_CNC_DAY_CI + ADD_CNC_DAY_NI + ADD_CNC_DAY_SIGN + ADD_CNC_DAY
				+ REA_DES_CI + REA_DES_NI + REA_DES;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
