/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-REQ-UMT-READ-SW<br>
 * Variable: WS-REQ-UMT-READ-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsReqUmtReadSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FIRST_READ_OF_REQ_UMT = 'F';
	public static final char SUBSEQUENT_READ_OF_REQ_UMT = 'S';

	//==== METHODS ====
	public void setReqUmtReadSw(char reqUmtReadSw) {
		this.value = reqUmtReadSw;
	}

	public char getReqUmtReadSw() {
		return this.value;
	}

	public boolean isFirstReadOfReqUmt() {
		return value == FIRST_READ_OF_REQ_UMT;
	}

	public void setFirstReadOfReqUmt() {
		value = FIRST_READ_OF_REQ_UMT;
	}

	public void setSubsequentReadOfReqUmt() {
		value = SUBSEQUENT_READ_OF_REQ_UMT;
	}
}
