/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.SaActTypCd;
import com.federatedinsurance.crs.ws.enums.SaCerHldNotInd;
import com.federatedinsurance.crs.ws.enums.SaRecipientTypCd;
import com.federatedinsurance.crs.ws.enums.SaSegCd;
import com.federatedinsurance.crs.ws.enums.SaSpecialAttachment;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program XZ0P90E0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaveAreaXz0p90e0 {

	//==== PROPERTIES ====
	//Original name: SA-STA-CD-CNT
	private short staCdCnt = ((short) 0);
	//Original name: SA-PCN-CNT-PLS-TRN
	private String pcnCntPlsTrn = "000";
	//Original name: SA-FRM-SEQ-NBR
	private int frmSeqNbr = DefaultValues.INT_VAL;
	//Original name: SA-REC-SEQ-NBR
	private int recSeqNbr = DefaultValues.INT_VAL;
	//Original name: SA-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: SA-ACT-TYP-CD
	private SaActTypCd actTypCd = new SaActTypCd();
	/**Original name: SA-BILLING-METHOD<br>
	 * <pre>  FOR THIS REQUEST, CUSTOMER ACCOUNTS DOES NOT WANT THE
	 *   PAYMENT METHOD OF "FULL" TO BE INCLUDED IN THE FULL PAY.</pre>*/
	private String billingMethod = DefaultValues.stringVal(Len.BILLING_METHOD);
	private static final String[] BIL_MTH_FULL_PAY = new String[] { "UREQ", "IREQ", "PREQ" };
	//Original name: SA-CER-HLD-NOT-IND
	private SaCerHldNotInd cerHldNotInd = new SaCerHldNotInd();
	//Original name: SA-RECIPIENT-TYP-CD
	private SaRecipientTypCd recipientTypCd = new SaRecipientTypCd();
	//Original name: SA-SEG-CD
	private SaSegCd segCd = new SaSegCd();
	//Original name: SA-SPECIAL-ATTACHMENT
	private SaSpecialAttachment specialAttachment = new SaSpecialAttachment();

	//==== METHODS ====
	public short getStaCdCnt() {
		return this.staCdCnt;
	}

	public void setPcnCntPlsTrn(short pcnCntPlsTrn) {
		this.pcnCntPlsTrn = NumericDisplay.asString(pcnCntPlsTrn, Len.PCN_CNT_PLS_TRN);
	}

	public void setPcnCntPlsTrnFormatted(String pcnCntPlsTrn) {
		this.pcnCntPlsTrn = Trunc.toUnsignedNumeric(pcnCntPlsTrn, Len.PCN_CNT_PLS_TRN);
	}

	public short getPcnCntPlsTrn() {
		return NumericDisplay.asShort(this.pcnCntPlsTrn);
	}

	public void setFrmSeqNbr(int frmSeqNbr) {
		this.frmSeqNbr = frmSeqNbr;
	}

	public int getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	public void setRecSeqNbr(int recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	public int getRecSeqNbr() {
		return this.recSeqNbr;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setBillingMethod(String billingMethod) {
		this.billingMethod = Functions.subString(billingMethod, Len.BILLING_METHOD);
	}

	public String getBillingMethod() {
		return this.billingMethod;
	}

	public boolean isBilMthFullPay() {
		return ArrayUtils.contains(BIL_MTH_FULL_PAY, billingMethod);
	}

	public SaActTypCd getActTypCd() {
		return actTypCd;
	}

	public SaCerHldNotInd getCerHldNotInd() {
		return cerHldNotInd;
	}

	public SaRecipientTypCd getRecipientTypCd() {
		return recipientTypCd;
	}

	public SaSegCd getSegCd() {
		return segCd;
	}

	public SaSpecialAttachment getSpecialAttachment() {
		return specialAttachment;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int BILLING_METHOD = 4;
		public static final int PCN_CNT_PLS_TRN = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
