/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: EXCI-RETURN-CODE<br>
 * Variable: EXCI-RETURN-CODE from copybook DFHXCPLO<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ExciReturnCode extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: EXCI-RESPONSE
	private long response = DefaultValues.BIN_LONG_VAL;
	//Original name: EXCI-REASON
	private long reason = DefaultValues.BIN_LONG_VAL;
	//Original name: EXCI-SUB-REASON1
	private long subReason1 = DefaultValues.BIN_LONG_VAL;
	//Original name: EXCI-SUB-REASON2
	private long subReason2 = DefaultValues.BIN_LONG_VAL;
	//Original name: EXCI-MSG-PTR
	private int msgPtr = DefaultValues.BIN_INT_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.EXCI_RETURN_CODE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setExciReturnCodeBytes(buf);
	}

	public String getExciReturnCodeFormatted() {
		return MarshalByteExt.bufferToStr(getExciReturnCodeBytes());
	}

	public void setExciReturnCodeBytes(byte[] buffer) {
		setExciReturnCodeBytes(buffer, 1);
	}

	public byte[] getExciReturnCodeBytes() {
		byte[] buffer = new byte[Len.EXCI_RETURN_CODE];
		return getExciReturnCodeBytes(buffer, 1);
	}

	public void setExciReturnCodeBytes(byte[] buffer, int offset) {
		int position = offset;
		response = MarshalByte.readBinaryUnsignedInt(buffer, position);
		position += Types.INT_SIZE;
		reason = MarshalByte.readBinaryUnsignedInt(buffer, position);
		position += Types.INT_SIZE;
		subReason1 = MarshalByte.readBinaryUnsignedInt(buffer, position);
		position += Types.INT_SIZE;
		subReason2 = MarshalByte.readBinaryUnsignedInt(buffer, position);
		position += Types.INT_SIZE;
		msgPtr = MarshalByte.readBinaryInt(buffer, position);
	}

	public byte[] getExciReturnCodeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryUnsignedInt(buffer, position, response);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryUnsignedInt(buffer, position, reason);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryUnsignedInt(buffer, position, subReason1);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryUnsignedInt(buffer, position, subReason2);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, msgPtr);
		return buffer;
	}

	public void setResponse(long response) {
		this.response = response;
	}

	public long getResponse() {
		return this.response;
	}

	public void setReason(long reason) {
		this.reason = reason;
	}

	public long getReason() {
		return this.reason;
	}

	public void setSubReason1(long subReason1) {
		this.subReason1 = subReason1;
	}

	public long getSubReason1() {
		return this.subReason1;
	}

	public void setSubReason2(long subReason2) {
		this.subReason2 = subReason2;
	}

	public long getSubReason2() {
		return this.subReason2;
	}

	public void setMsgPtr(int msgPtr) {
		this.msgPtr = msgPtr;
	}

	public int getMsgPtr() {
		return this.msgPtr;
	}

	@Override
	public byte[] serialize() {
		return getExciReturnCodeBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESPONSE = 4;
		public static final int REASON = 4;
		public static final int SUB_REASON1 = 4;
		public static final int SUB_REASON2 = 4;
		public static final int MSG_PTR = 4;
		public static final int EXCI_RETURN_CODE = RESPONSE + REASON + SUB_REASON1 + SUB_REASON2 + MSG_PTR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
