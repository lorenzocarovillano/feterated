/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: XZ0N0002<br>
 * Variable: XZ0N0002 from copybook XZ0N0002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz0n0002 {

	//==== PROPERTIES ====
	//Original name: XZN002-ACT-NOT-POL-COLS
	private Xzn002ActNotPolCols actNotPolCols = new Xzn002ActNotPolCols();
	//Original name: FILLER-XZN002-PROCESSING-CONTEXT-TEXT
	private String flr1 = "the account,";
	//Original name: FILLER-XZN002-PROCESSING-CONTEXT-TEXT-1
	private String flr2 = " notification";
	//Original name: FILLER-XZN002-PROCESSING-CONTEXT-TEXT-2
	private String flr3 = "timestamp and";
	//Original name: FILLER-XZN002-PROCESSING-CONTEXT-TEXT-3
	private String flr4 = "policy number";
	//Original name: FILLER-XZN002-PROCESSING-CONTEXT-TEXT-4
	private String flr5 = "of:";

	//==== METHODS ====
	public String getProcessingContextTextFormatted() {
		return MarshalByteExt.bufferToStr(getProcessingContextTextBytes());
	}

	/**Original name: XZN002-PROCESSING-CONTEXT-TEXT<br>*/
	public byte[] getProcessingContextTextBytes() {
		byte[] buffer = new byte[Len.PROCESSING_CONTEXT_TEXT];
		return getProcessingContextTextBytes(buffer, 1);
	}

	public byte[] getProcessingContextTextBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public Xzn002ActNotPolCols getActNotPolCols() {
		return actNotPolCols;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 13;
		public static final int FLR2 = 14;
		public static final int FLR5 = 4;
		public static final int PROCESSING_CONTEXT_TEXT = FLR1 + 3 * FLR2 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
