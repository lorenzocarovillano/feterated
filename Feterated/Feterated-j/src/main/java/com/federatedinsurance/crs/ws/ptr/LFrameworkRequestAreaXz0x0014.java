/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X0014<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x0014 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x0014() {
	}

	public LFrameworkRequestAreaXz0x0014(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzc006ActNotFrmRowFormatted(String data) {
		writeString(Pos.XZC006Q_ACT_NOT_FRM_ROW, data, Len.XZC006Q_ACT_NOT_FRM_ROW);
	}

	public String getXzc006ActNotFrmRowFormatted() {
		return readFixedString(Pos.XZC006Q_ACT_NOT_FRM_ROW, Len.XZC006Q_ACT_NOT_FRM_ROW);
	}

	public void setXzc006qActNotFrmCsumFormatted(String xzc006qActNotFrmCsum) {
		writeString(Pos.XZC006Q_ACT_NOT_FRM_CSUM, Trunc.toUnsignedNumeric(xzc006qActNotFrmCsum, Len.XZC006Q_ACT_NOT_FRM_CSUM),
				Len.XZC006Q_ACT_NOT_FRM_CSUM);
	}

	/**Original name: XZC006Q-ACT-NOT-FRM-CSUM<br>*/
	public int getXzc006qActNotFrmCsum() {
		return readNumDispUnsignedInt(Pos.XZC006Q_ACT_NOT_FRM_CSUM, Len.XZC006Q_ACT_NOT_FRM_CSUM);
	}

	public String getXzc006rActNotFrmCsumFormatted() {
		return readFixedString(Pos.XZC006Q_ACT_NOT_FRM_CSUM, Len.XZC006Q_ACT_NOT_FRM_CSUM);
	}

	public void setXzc006qCsrActNbrKcre(String xzc006qCsrActNbrKcre) {
		writeString(Pos.XZC006Q_CSR_ACT_NBR_KCRE, xzc006qCsrActNbrKcre, Len.XZC006Q_CSR_ACT_NBR_KCRE);
	}

	/**Original name: XZC006Q-CSR-ACT-NBR-KCRE<br>*/
	public String getXzc006qCsrActNbrKcre() {
		return readString(Pos.XZC006Q_CSR_ACT_NBR_KCRE, Len.XZC006Q_CSR_ACT_NBR_KCRE);
	}

	public void setXzc006qNotPrcTsKcre(String xzc006qNotPrcTsKcre) {
		writeString(Pos.XZC006Q_NOT_PRC_TS_KCRE, xzc006qNotPrcTsKcre, Len.XZC006Q_NOT_PRC_TS_KCRE);
	}

	/**Original name: XZC006Q-NOT-PRC-TS-KCRE<br>*/
	public String getXzc006qNotPrcTsKcre() {
		return readString(Pos.XZC006Q_NOT_PRC_TS_KCRE, Len.XZC006Q_NOT_PRC_TS_KCRE);
	}

	public void setXzc006qFrmSeqNbrKcre(String xzc006qFrmSeqNbrKcre) {
		writeString(Pos.XZC006Q_FRM_SEQ_NBR_KCRE, xzc006qFrmSeqNbrKcre, Len.XZC006Q_FRM_SEQ_NBR_KCRE);
	}

	/**Original name: XZC006Q-FRM-SEQ-NBR-KCRE<br>*/
	public String getXzc006qFrmSeqNbrKcre() {
		return readString(Pos.XZC006Q_FRM_SEQ_NBR_KCRE, Len.XZC006Q_FRM_SEQ_NBR_KCRE);
	}

	public void setXzc006qTransProcessDt(String xzc006qTransProcessDt) {
		writeString(Pos.XZC006Q_TRANS_PROCESS_DT, xzc006qTransProcessDt, Len.XZC006Q_TRANS_PROCESS_DT);
	}

	/**Original name: XZC006Q-TRANS-PROCESS-DT<br>*/
	public String getXzc006qTransProcessDt() {
		return readString(Pos.XZC006Q_TRANS_PROCESS_DT, Len.XZC006Q_TRANS_PROCESS_DT);
	}

	public void setXzc006qCsrActNbr(String xzc006qCsrActNbr) {
		writeString(Pos.XZC006Q_CSR_ACT_NBR, xzc006qCsrActNbr, Len.XZC006Q_CSR_ACT_NBR);
	}

	/**Original name: XZC006Q-CSR-ACT-NBR<br>*/
	public String getXzc006qCsrActNbr() {
		return readString(Pos.XZC006Q_CSR_ACT_NBR, Len.XZC006Q_CSR_ACT_NBR);
	}

	public void setXzc006qNotPrcTs(String xzc006qNotPrcTs) {
		writeString(Pos.XZC006Q_NOT_PRC_TS, xzc006qNotPrcTs, Len.XZC006Q_NOT_PRC_TS);
	}

	/**Original name: XZC006Q-NOT-PRC-TS<br>*/
	public String getXzc006qNotPrcTs() {
		return readString(Pos.XZC006Q_NOT_PRC_TS, Len.XZC006Q_NOT_PRC_TS);
	}

	public void setXzc006qFrmSeqNbrSign(char xzc006qFrmSeqNbrSign) {
		writeChar(Pos.XZC006Q_FRM_SEQ_NBR_SIGN, xzc006qFrmSeqNbrSign);
	}

	/**Original name: XZC006Q-FRM-SEQ-NBR-SIGN<br>*/
	public char getXzc006qFrmSeqNbrSign() {
		return readChar(Pos.XZC006Q_FRM_SEQ_NBR_SIGN);
	}

	public void setXzc006qFrmSeqNbrFormatted(String xzc006qFrmSeqNbr) {
		writeString(Pos.XZC006Q_FRM_SEQ_NBR, Trunc.toUnsignedNumeric(xzc006qFrmSeqNbr, Len.XZC006Q_FRM_SEQ_NBR), Len.XZC006Q_FRM_SEQ_NBR);
	}

	/**Original name: XZC006Q-FRM-SEQ-NBR<br>*/
	public int getXzc006qFrmSeqNbr() {
		return readNumDispUnsignedInt(Pos.XZC006Q_FRM_SEQ_NBR, Len.XZC006Q_FRM_SEQ_NBR);
	}

	public String getXzc006rFrmSeqNbrFormatted() {
		return readFixedString(Pos.XZC006Q_FRM_SEQ_NBR, Len.XZC006Q_FRM_SEQ_NBR);
	}

	public void setXzc006qCsrActNbrCi(char xzc006qCsrActNbrCi) {
		writeChar(Pos.XZC006Q_CSR_ACT_NBR_CI, xzc006qCsrActNbrCi);
	}

	public void setXzc006qCsrActNbrCiFormatted(String xzc006qCsrActNbrCi) {
		setXzc006qCsrActNbrCi(Functions.charAt(xzc006qCsrActNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC006Q-CSR-ACT-NBR-CI<br>*/
	public char getXzc006qCsrActNbrCi() {
		return readChar(Pos.XZC006Q_CSR_ACT_NBR_CI);
	}

	public void setXzc006qNotPrcTsCi(char xzc006qNotPrcTsCi) {
		writeChar(Pos.XZC006Q_NOT_PRC_TS_CI, xzc006qNotPrcTsCi);
	}

	public void setXzc006qNotPrcTsCiFormatted(String xzc006qNotPrcTsCi) {
		setXzc006qNotPrcTsCi(Functions.charAt(xzc006qNotPrcTsCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC006Q-NOT-PRC-TS-CI<br>*/
	public char getXzc006qNotPrcTsCi() {
		return readChar(Pos.XZC006Q_NOT_PRC_TS_CI);
	}

	public void setXzc006qFrmSeqNbrCi(char xzc006qFrmSeqNbrCi) {
		writeChar(Pos.XZC006Q_FRM_SEQ_NBR_CI, xzc006qFrmSeqNbrCi);
	}

	public void setXzc006qFrmSeqNbrCiFormatted(String xzc006qFrmSeqNbrCi) {
		setXzc006qFrmSeqNbrCi(Functions.charAt(xzc006qFrmSeqNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC006Q-FRM-SEQ-NBR-CI<br>*/
	public char getXzc006qFrmSeqNbrCi() {
		return readChar(Pos.XZC006Q_FRM_SEQ_NBR_CI);
	}

	public void setXzc006qFrmNbrCi(char xzc006qFrmNbrCi) {
		writeChar(Pos.XZC006Q_FRM_NBR_CI, xzc006qFrmNbrCi);
	}

	/**Original name: XZC006Q-FRM-NBR-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getXzc006qFrmNbrCi() {
		return readChar(Pos.XZC006Q_FRM_NBR_CI);
	}

	public void setXzc006qFrmNbr(String xzc006qFrmNbr) {
		writeString(Pos.XZC006Q_FRM_NBR, xzc006qFrmNbr, Len.XZC006Q_FRM_NBR);
	}

	/**Original name: XZC006Q-FRM-NBR<br>*/
	public String getXzc006qFrmNbr() {
		return readString(Pos.XZC006Q_FRM_NBR, Len.XZC006Q_FRM_NBR);
	}

	public void setXzc006qFrmEdtDtCi(char xzc006qFrmEdtDtCi) {
		writeChar(Pos.XZC006Q_FRM_EDT_DT_CI, xzc006qFrmEdtDtCi);
	}

	/**Original name: XZC006Q-FRM-EDT-DT-CI<br>*/
	public char getXzc006qFrmEdtDtCi() {
		return readChar(Pos.XZC006Q_FRM_EDT_DT_CI);
	}

	public void setXzc006qFrmEdtDt(String xzc006qFrmEdtDt) {
		writeString(Pos.XZC006Q_FRM_EDT_DT, xzc006qFrmEdtDt, Len.XZC006Q_FRM_EDT_DT);
	}

	/**Original name: XZC006Q-FRM-EDT-DT<br>*/
	public String getXzc006qFrmEdtDt() {
		return readString(Pos.XZC006Q_FRM_EDT_DT, Len.XZC006Q_FRM_EDT_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0C0006 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZC006Q_ACT_NOT_FRM_ROW = L_FW_REQ_XZ0C0006;
		public static final int XZC006Q_ACT_NOT_FRM_FIXED = XZC006Q_ACT_NOT_FRM_ROW;
		public static final int XZC006Q_ACT_NOT_FRM_CSUM = XZC006Q_ACT_NOT_FRM_FIXED;
		public static final int XZC006Q_CSR_ACT_NBR_KCRE = XZC006Q_ACT_NOT_FRM_CSUM + Len.XZC006Q_ACT_NOT_FRM_CSUM;
		public static final int XZC006Q_NOT_PRC_TS_KCRE = XZC006Q_CSR_ACT_NBR_KCRE + Len.XZC006Q_CSR_ACT_NBR_KCRE;
		public static final int XZC006Q_FRM_SEQ_NBR_KCRE = XZC006Q_NOT_PRC_TS_KCRE + Len.XZC006Q_NOT_PRC_TS_KCRE;
		public static final int XZC006Q_ACT_NOT_FRM_DATES = XZC006Q_FRM_SEQ_NBR_KCRE + Len.XZC006Q_FRM_SEQ_NBR_KCRE;
		public static final int XZC006Q_TRANS_PROCESS_DT = XZC006Q_ACT_NOT_FRM_DATES;
		public static final int XZC006Q_ACT_NOT_FRM_KEY = XZC006Q_TRANS_PROCESS_DT + Len.XZC006Q_TRANS_PROCESS_DT;
		public static final int XZC006Q_CSR_ACT_NBR = XZC006Q_ACT_NOT_FRM_KEY;
		public static final int XZC006Q_NOT_PRC_TS = XZC006Q_CSR_ACT_NBR + Len.XZC006Q_CSR_ACT_NBR;
		public static final int XZC006Q_FRM_SEQ_NBR_SIGN = XZC006Q_NOT_PRC_TS + Len.XZC006Q_NOT_PRC_TS;
		public static final int XZC006Q_FRM_SEQ_NBR = XZC006Q_FRM_SEQ_NBR_SIGN + Len.XZC006Q_FRM_SEQ_NBR_SIGN;
		public static final int XZC006Q_ACT_NOT_FRM_KEY_CI = XZC006Q_FRM_SEQ_NBR + Len.XZC006Q_FRM_SEQ_NBR;
		public static final int XZC006Q_CSR_ACT_NBR_CI = XZC006Q_ACT_NOT_FRM_KEY_CI;
		public static final int XZC006Q_NOT_PRC_TS_CI = XZC006Q_CSR_ACT_NBR_CI + Len.XZC006Q_CSR_ACT_NBR_CI;
		public static final int XZC006Q_FRM_SEQ_NBR_CI = XZC006Q_NOT_PRC_TS_CI + Len.XZC006Q_NOT_PRC_TS_CI;
		public static final int XZC006Q_ACT_NOT_FRM_DATA = XZC006Q_FRM_SEQ_NBR_CI + Len.XZC006Q_FRM_SEQ_NBR_CI;
		public static final int XZC006Q_FRM_NBR_CI = XZC006Q_ACT_NOT_FRM_DATA;
		public static final int XZC006Q_FRM_NBR = XZC006Q_FRM_NBR_CI + Len.XZC006Q_FRM_NBR_CI;
		public static final int XZC006Q_FRM_EDT_DT_CI = XZC006Q_FRM_NBR + Len.XZC006Q_FRM_NBR;
		public static final int XZC006Q_FRM_EDT_DT = XZC006Q_FRM_EDT_DT_CI + Len.XZC006Q_FRM_EDT_DT_CI;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC006Q_ACT_NOT_FRM_CSUM = 9;
		public static final int XZC006Q_CSR_ACT_NBR_KCRE = 32;
		public static final int XZC006Q_NOT_PRC_TS_KCRE = 32;
		public static final int XZC006Q_FRM_SEQ_NBR_KCRE = 32;
		public static final int XZC006Q_TRANS_PROCESS_DT = 10;
		public static final int XZC006Q_CSR_ACT_NBR = 9;
		public static final int XZC006Q_NOT_PRC_TS = 26;
		public static final int XZC006Q_FRM_SEQ_NBR_SIGN = 1;
		public static final int XZC006Q_FRM_SEQ_NBR = 5;
		public static final int XZC006Q_CSR_ACT_NBR_CI = 1;
		public static final int XZC006Q_NOT_PRC_TS_CI = 1;
		public static final int XZC006Q_FRM_SEQ_NBR_CI = 1;
		public static final int XZC006Q_FRM_NBR_CI = 1;
		public static final int XZC006Q_FRM_NBR = 30;
		public static final int XZC006Q_FRM_EDT_DT_CI = 1;
		public static final int XZC006Q_ACT_NOT_FRM_FIXED = XZC006Q_ACT_NOT_FRM_CSUM + XZC006Q_CSR_ACT_NBR_KCRE + XZC006Q_NOT_PRC_TS_KCRE
				+ XZC006Q_FRM_SEQ_NBR_KCRE;
		public static final int XZC006Q_ACT_NOT_FRM_DATES = XZC006Q_TRANS_PROCESS_DT;
		public static final int XZC006Q_ACT_NOT_FRM_KEY = XZC006Q_CSR_ACT_NBR + XZC006Q_NOT_PRC_TS + XZC006Q_FRM_SEQ_NBR_SIGN + XZC006Q_FRM_SEQ_NBR;
		public static final int XZC006Q_ACT_NOT_FRM_KEY_CI = XZC006Q_CSR_ACT_NBR_CI + XZC006Q_NOT_PRC_TS_CI + XZC006Q_FRM_SEQ_NBR_CI;
		public static final int XZC006Q_FRM_EDT_DT = 10;
		public static final int XZC006Q_ACT_NOT_FRM_DATA = XZC006Q_FRM_NBR_CI + XZC006Q_FRM_NBR + XZC006Q_FRM_EDT_DT_CI + XZC006Q_FRM_EDT_DT;
		public static final int XZC006Q_ACT_NOT_FRM_ROW = XZC006Q_ACT_NOT_FRM_FIXED + XZC006Q_ACT_NOT_FRM_DATES + XZC006Q_ACT_NOT_FRM_KEY
				+ XZC006Q_ACT_NOT_FRM_KEY_CI + XZC006Q_ACT_NOT_FRM_DATA;
		public static final int L_FW_REQ_XZ0C0006 = XZC006Q_ACT_NOT_FRM_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0C0006;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
