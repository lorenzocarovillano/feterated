/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90P0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90p0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int PO_POL_INFO_MAXOCCURS = 50;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90p0() {
	}

	public LServiceContractAreaXz0x90p0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setPiTkNotPrcTs(String piTkNotPrcTs) {
		writeString(Pos.PI_TK_NOT_PRC_TS, piTkNotPrcTs, Len.PI_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9PI-TK-NOT-PRC-TS<br>*/
	public String getPiTkNotPrcTs() {
		return readString(Pos.PI_TK_NOT_PRC_TS, Len.PI_TK_NOT_PRC_TS);
	}

	public void setPiCsrActNbr(String piCsrActNbr) {
		writeString(Pos.PI_CSR_ACT_NBR, piCsrActNbr, Len.PI_CSR_ACT_NBR);
	}

	/**Original name: XZT9PI-CSR-ACT-NBR<br>*/
	public String getPiCsrActNbr() {
		return readString(Pos.PI_CSR_ACT_NBR, Len.PI_CSR_ACT_NBR);
	}

	public void setPiUserid(String piUserid) {
		writeString(Pos.PI_USERID, piUserid, Len.PI_USERID);
	}

	/**Original name: XZT9PI-USERID<br>*/
	public String getPiUserid() {
		return readString(Pos.PI_USERID, Len.PI_USERID);
	}

	public String getPiUseridFormatted() {
		return Functions.padBlanks(getPiUserid(), Len.PI_USERID);
	}

	public void setPoTkNotPrcTs(String poTkNotPrcTs) {
		writeString(Pos.PO_TK_NOT_PRC_TS, poTkNotPrcTs, Len.PO_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9PO-TK-NOT-PRC-TS<br>*/
	public String getPoTkNotPrcTs() {
		return readString(Pos.PO_TK_NOT_PRC_TS, Len.PO_TK_NOT_PRC_TS);
	}

	public void setPoCsrActNbr(String poCsrActNbr) {
		writeString(Pos.PO_CSR_ACT_NBR, poCsrActNbr, Len.PO_CSR_ACT_NBR);
	}

	/**Original name: XZT9PO-CSR-ACT-NBR<br>*/
	public String getPoCsrActNbr() {
		return readString(Pos.PO_CSR_ACT_NBR, Len.PO_CSR_ACT_NBR);
	}

	public void setPoPolNbr(int poPolNbrIdx, String poPolNbr) {
		int position = Pos.xzt9poPolNbr(poPolNbrIdx - 1);
		writeString(position, poPolNbr, Len.PO_POL_NBR);
	}

	/**Original name: XZT9PO-POL-NBR<br>*/
	public String getPoPolNbr(int poPolNbrIdx) {
		int position = Pos.xzt9poPolNbr(poPolNbrIdx - 1);
		return readString(position, Len.PO_POL_NBR);
	}

	public void setPoPolTyp(int poPolTypIdx, String poPolTyp) {
		int position = Pos.xzt9poPolTyp(poPolTypIdx - 1);
		writeString(position, poPolTyp, Len.PO_POL_TYP);
	}

	/**Original name: XZT9PO-POL-TYP<br>*/
	public String getPoPolTyp(int poPolTypIdx) {
		int position = Pos.xzt9poPolTyp(poPolTypIdx - 1);
		return readString(position, Len.PO_POL_TYP);
	}

	public void setPoPolEffDt(int poPolEffDtIdx, String poPolEffDt) {
		int position = Pos.xzt9poPolEffDt(poPolEffDtIdx - 1);
		writeString(position, poPolEffDt, Len.PO_POL_EFF_DT);
	}

	/**Original name: XZT9PO-POL-EFF-DT<br>*/
	public String getPoPolEffDt(int poPolEffDtIdx) {
		int position = Pos.xzt9poPolEffDt(poPolEffDtIdx - 1);
		return readString(position, Len.PO_POL_EFF_DT);
	}

	public void setPoPolExpDt(int poPolExpDtIdx, String poPolExpDt) {
		int position = Pos.xzt9poPolExpDt(poPolExpDtIdx - 1);
		writeString(position, poPolExpDt, Len.PO_POL_EXP_DT);
	}

	/**Original name: XZT9PO-POL-EXP-DT<br>*/
	public String getPoPolExpDt(int poPolExpDtIdx) {
		int position = Pos.xzt9poPolExpDt(poPolExpDtIdx - 1);
		return readString(position, Len.PO_POL_EXP_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT90P_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int PI_TECHNICAL_KEY = XZT90P_SERVICE_INPUTS;
		public static final int PI_TK_NOT_PRC_TS = PI_TECHNICAL_KEY;
		public static final int PI_CSR_ACT_NBR = PI_TK_NOT_PRC_TS + Len.PI_TK_NOT_PRC_TS;
		public static final int PI_USERID = PI_CSR_ACT_NBR + Len.PI_CSR_ACT_NBR;
		public static final int XZT90P_SERVICE_OUTPUTS = PI_USERID + Len.PI_USERID;
		public static final int PO_TECHNICAL_KEY = XZT90P_SERVICE_OUTPUTS;
		public static final int PO_TK_NOT_PRC_TS = PO_TECHNICAL_KEY;
		public static final int PO_CSR_ACT_NBR = PO_TK_NOT_PRC_TS + Len.PO_TK_NOT_PRC_TS;
		public static final int PO_TABLE_OF_POLS = PO_CSR_ACT_NBR + Len.PO_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt9poPolInfo(int idx) {
			return PO_TABLE_OF_POLS + idx * Len.PO_POL_INFO;
		}

		public static int xzt9poPolNbr(int idx) {
			return xzt9poPolInfo(idx);
		}

		public static int xzt9poPolTyp(int idx) {
			return xzt9poPolNbr(idx) + Len.PO_POL_NBR;
		}

		public static int xzt9poPolEffDt(int idx) {
			return xzt9poPolTyp(idx) + Len.PO_POL_TYP;
		}

		public static int xzt9poPolExpDt(int idx) {
			return xzt9poPolEffDt(idx) + Len.PO_POL_EFF_DT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int PI_TK_NOT_PRC_TS = 26;
		public static final int PI_CSR_ACT_NBR = 9;
		public static final int PI_USERID = 8;
		public static final int PO_TK_NOT_PRC_TS = 26;
		public static final int PO_CSR_ACT_NBR = 9;
		public static final int PO_POL_NBR = 25;
		public static final int PO_POL_TYP = 4;
		public static final int PO_POL_EFF_DT = 10;
		public static final int PO_POL_EXP_DT = 10;
		public static final int PO_POL_INFO = PO_POL_NBR + PO_POL_TYP + PO_POL_EFF_DT + PO_POL_EXP_DT;
		public static final int PI_TECHNICAL_KEY = PI_TK_NOT_PRC_TS;
		public static final int XZT90P_SERVICE_INPUTS = PI_TECHNICAL_KEY + PI_CSR_ACT_NBR + PI_USERID;
		public static final int PO_TECHNICAL_KEY = PO_TK_NOT_PRC_TS;
		public static final int PO_TABLE_OF_POLS = LServiceContractAreaXz0x90p0.PO_POL_INFO_MAXOCCURS * PO_POL_INFO;
		public static final int XZT90P_SERVICE_OUTPUTS = PO_TECHNICAL_KEY + PO_CSR_ACT_NBR + PO_TABLE_OF_POLS;
		public static final int L_SERVICE_CONTRACT_AREA = XZT90P_SERVICE_INPUTS + XZT90P_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
