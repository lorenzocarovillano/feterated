/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xza920Operation;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WS-XZ0A9020-ROW<br>
 * Variable: WS-XZ0A9020-ROW from program XZ0B9020<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9020Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA920-MAX-REC-ROWS
	private short maxRecRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA920-OPERATION
	private Xza920Operation operation = new Xza920Operation();
	//Original name: XZA920-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA920-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA920-FRM-SEQ-NBR
	private int frmSeqNbr = DefaultValues.INT_VAL;
	//Original name: XZA920-RECALL-REC-SEQ-NBR
	private int recallRecSeqNbr = DefaultValues.INT_VAL;
	//Original name: XZA920-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9020_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9020RowBytes(buf);
	}

	public String getWsXz0a9020RowFormatted() {
		return getGetRecListKeyFormatted();
	}

	public void setWsXz0a9020RowBytes(byte[] buffer) {
		setWsXz0a9020RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9020RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9020_ROW];
		return getWsXz0a9020RowBytes(buffer, 1);
	}

	public void setWsXz0a9020RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetRecListKeyBytes(buffer, position);
	}

	public byte[] getWsXz0a9020RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetRecListKeyBytes(buffer, position);
		return buffer;
	}

	public String getGetRecListKeyFormatted() {
		return MarshalByteExt.bufferToStr(getGetRecListKeyBytes());
	}

	/**Original name: XZA920-GET-REC-LIST-KEY<br>
	 * <pre>*************************************************************
	 *  XZ0A9020 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_REC_LIST                           *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  12JAN2009 E404DLP    NEW                          *
	 *  TO07614  04MAR2009 E404GCL    Add new operation -          *
	 *                                GetRecipientListByNotification *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetRecListKeyBytes() {
		byte[] buffer = new byte[Len.GET_REC_LIST_KEY];
		return getGetRecListKeyBytes(buffer, 1);
	}

	public void setGetRecListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		maxRecRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		operation.setOperation(MarshalByte.readString(buffer, position, Xza920Operation.Len.OPERATION));
		position += Xza920Operation.Len.OPERATION;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		frmSeqNbr = MarshalByte.readInt(buffer, position, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		recallRecSeqNbr = MarshalByte.readInt(buffer, position, Len.RECALL_REC_SEQ_NBR);
		position += Len.RECALL_REC_SEQ_NBR;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
	}

	public byte[] getGetRecListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxRecRows);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, operation.getOperation(), Xza920Operation.Len.OPERATION);
		position += Xza920Operation.Len.OPERATION;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeInt(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		MarshalByte.writeInt(buffer, position, recallRecSeqNbr, Len.RECALL_REC_SEQ_NBR);
		position += Len.RECALL_REC_SEQ_NBR;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setMaxRecRows(short maxRecRows) {
		this.maxRecRows = maxRecRows;
	}

	public short getMaxRecRows() {
		return this.maxRecRows;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setFrmSeqNbr(int frmSeqNbr) {
		this.frmSeqNbr = frmSeqNbr;
	}

	public int getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	public String getFrmSeqNbrFormatted() {
		return NumericDisplaySigned.asString(getFrmSeqNbr(), Len.FRM_SEQ_NBR);
	}

	public void setRecallRecSeqNbr(int recallRecSeqNbr) {
		this.recallRecSeqNbr = recallRecSeqNbr;
	}

	public int getRecallRecSeqNbr() {
		return this.recallRecSeqNbr;
	}

	public String getRecallRecSeqNbrFormatted() {
		return NumericDisplaySigned.asString(getRecallRecSeqNbr(), Len.RECALL_REC_SEQ_NBR);
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	public Xza920Operation getOperation() {
		return operation;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9020RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_REC_ROWS = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FRM_SEQ_NBR = 5;
		public static final int RECALL_REC_SEQ_NBR = 5;
		public static final int USERID = 8;
		public static final int GET_REC_LIST_KEY = MAX_REC_ROWS + Xza920Operation.Len.OPERATION + CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR
				+ RECALL_REC_SEQ_NBR + USERID;
		public static final int WS_XZ0A9020_ROW = GET_REC_LIST_KEY;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
