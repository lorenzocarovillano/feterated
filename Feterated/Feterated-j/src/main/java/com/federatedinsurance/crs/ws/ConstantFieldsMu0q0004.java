/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program MU0Q0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsMu0q0004 {

	//==== PROPERTIES ====
	//Original name: CF-BO-REL-CLT-OBJ-REL-BPO
	private String boRelCltObjRelBpo = "CLT_OBJECT_RELATED_CLIENT_BPO";
	//Original name: CF-INVALID-OPERATION
	private CfInvalidOperation invalidOperation = new CfInvalidOperation();
	//Original name: CF-TABLE-FORMATTER-MODULES
	private CfTableFormatterModules tableFormatterModules = new CfTableFormatterModules();
	//Original name: CF-OWNER
	private String owner = "OWN";
	//Original name: CF-COMPANY
	private String company = "CMP";
	//Original name: CF-INDIVIDUAL
	private String individual = "INDI";
	//Original name: CF-TERRITORY
	private String territory = "TERR";
	//Original name: CF-UNDERWRITER
	private String underwriter = "UND";
	//Original name: CF-RSKA
	private String rska = "RSKA";
	//Original name: CF-FPU
	private String fpu = "FPU";
	//Original name: CF-LPRA
	private String lpra = "LPRA";

	//==== METHODS ====
	public String getBoRelCltObjRelBpo() {
		return this.boRelCltObjRelBpo;
	}

	public String getOwner() {
		return this.owner;
	}

	public String getCompany() {
		return this.company;
	}

	public String getIndividual() {
		return this.individual;
	}

	public String getTerritory() {
		return this.territory;
	}

	public String getUnderwriter() {
		return this.underwriter;
	}

	public String getRska() {
		return this.rska;
	}

	public String getFpu() {
		return this.fpu;
	}

	public String getLpra() {
		return this.lpra;
	}

	public CfInvalidOperation getInvalidOperation() {
		return invalidOperation;
	}

	public CfTableFormatterModules getTableFormatterModules() {
		return tableFormatterModules;
	}
}
