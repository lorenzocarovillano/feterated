/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalErrLogFailV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [HAL_ERR_LOG_FAIL_V]
 * 
 */
public class HalErrLogFailVDao extends BaseSqlDao<IHalErrLogFailV> {

	public HalErrLogFailVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalErrLogFailV> getToClass() {
		return IHalErrLogFailV.class;
	}

	public long selectByHelfhHelfErrRfrNbr(String helfhHelfErrRfrNbr, long dft) {
		return buildQuery("selectByHelfhHelfErrRfrNbr").bind("helfhHelfErrRfrNbr", helfhHelfErrRfrNbr).scalarResultLong(dft);
	}
}
