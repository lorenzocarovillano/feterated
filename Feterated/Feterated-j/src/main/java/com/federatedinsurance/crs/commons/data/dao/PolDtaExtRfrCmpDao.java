/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IPolDtaExtRfrCmp;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [POL_DTA_EXT, RFR_CMP]
 * 
 */
public class PolDtaExtRfrCmpDao extends BaseSqlDao<IPolDtaExtRfrCmp> {

	private final IRowMapper<IPolDtaExtRfrCmp> selectByPolNbrRm = buildNamedRowMapper(IPolDtaExtRfrCmp.class, "cd", "des");

	public PolDtaExtRfrCmpDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IPolDtaExtRfrCmp> getToClass() {
		return IPolDtaExtRfrCmp.class;
	}

	public IPolDtaExtRfrCmp selectByPolNbr(String polNbr, IPolDtaExtRfrCmp iPolDtaExtRfrCmp) {
		return buildQuery("selectByPolNbr").bind("polNbr", polNbr).rowMapper(selectByPolNbrRm).singleResult(iPolDtaExtRfrCmp);
	}
}
