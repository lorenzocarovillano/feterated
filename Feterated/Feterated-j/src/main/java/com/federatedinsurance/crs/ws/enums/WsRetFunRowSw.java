/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-RET-FUN-ROW-SW<br>
 * Variable: WS-RET-FUN-ROW-SW from program HALOUSDH<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsRetFunRowSw {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char OK_TO_RET_FUN_ROW = 'Y';
	public static final char DO_NOT_RET_FUN_ROW = 'N';

	//==== METHODS ====
	public void setRetFunRowSw(char retFunRowSw) {
		this.value = retFunRowSw;
	}

	public char getRetFunRowSw() {
		return this.value;
	}

	public void setOkToRetFunRow() {
		value = OK_TO_RET_FUN_ROW;
	}

	public boolean isDoNotRetFunRow() {
		return value == DO_NOT_RET_FUN_ROW;
	}

	public void setDoNotRetFunRow() {
		value = DO_NOT_RET_FUN_ROW;
	}
}
