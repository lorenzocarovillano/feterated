/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CAWLF027<br>
 * Variable: CAWLF027 from copybook CAWLF027<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cawlf027 {

	//==== PROPERTIES ====
	//Original name: CW27F-CLIENT-TAX-CHK-SUM
	private String clientTaxChkSum = DefaultValues.stringVal(Len.CLIENT_TAX_CHK_SUM);
	//Original name: CW27F-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CW27F-CITX-TAX-SEQ-NBR-KCRE
	private String citxTaxSeqNbrKcre = DefaultValues.stringVal(Len.CITX_TAX_SEQ_NBR_KCRE);
	//Original name: CW27F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW27F-CLIENT-TAX-KEY
	private Cw01fBusinessClientKey clientTaxKey = new Cw01fBusinessClientKey();
	//Original name: CW27F-CLIENT-TAX-DATA
	private Cw27fClientTaxData clientTaxData = new Cw27fClientTaxData();

	//==== METHODS ====
	public void setClientTaxRowFormatted(String data) {
		byte[] buffer = new byte[Len.CLIENT_TAX_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CLIENT_TAX_ROW);
		setClientTaxRowBytes(buffer, 1);
	}

	public String getClientTaxRowFormatted() {
		return MarshalByteExt.bufferToStr(getClientTaxRowBytes());
	}

	/**Original name: CW27F-CLIENT-TAX-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CAWLF027 - CLIENT_TAX TABLE                                    *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  PP00015  03/07/2007 E404ASW   NEW COPYBOOK                     *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getClientTaxRowBytes() {
		byte[] buffer = new byte[Len.CLIENT_TAX_ROW];
		return getClientTaxRowBytes(buffer, 1);
	}

	public void setClientTaxRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setClientTaxFixedBytes(buffer, position);
		position += Len.CLIENT_TAX_FIXED;
		setClientTaxDatesBytes(buffer, position);
		position += Len.CLIENT_TAX_DATES;
		clientTaxKey.setBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		clientTaxData.setClientTaxDataBytes(buffer, position);
	}

	public byte[] getClientTaxRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getClientTaxFixedBytes(buffer, position);
		position += Len.CLIENT_TAX_FIXED;
		getClientTaxDatesBytes(buffer, position);
		position += Len.CLIENT_TAX_DATES;
		clientTaxKey.getBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		clientTaxData.getClientTaxDataBytes(buffer, position);
		return buffer;
	}

	public void setClientTaxFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		clientTaxChkSum = MarshalByte.readFixedString(buffer, position, Len.CLIENT_TAX_CHK_SUM);
		position += Len.CLIENT_TAX_CHK_SUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		citxTaxSeqNbrKcre = MarshalByte.readString(buffer, position, Len.CITX_TAX_SEQ_NBR_KCRE);
	}

	public byte[] getClientTaxFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientTaxChkSum, Len.CLIENT_TAX_CHK_SUM);
		position += Len.CLIENT_TAX_CHK_SUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		MarshalByte.writeString(buffer, position, citxTaxSeqNbrKcre, Len.CITX_TAX_SEQ_NBR_KCRE);
		return buffer;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setCitxTaxSeqNbrKcre(String citxTaxSeqNbrKcre) {
		this.citxTaxSeqNbrKcre = Functions.subString(citxTaxSeqNbrKcre, Len.CITX_TAX_SEQ_NBR_KCRE);
	}

	public String getCitxTaxSeqNbrKcre() {
		return this.citxTaxSeqNbrKcre;
	}

	public void setClientTaxDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getClientTaxDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public Cw27fClientTaxData getClientTaxData() {
		return clientTaxData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_TAX_CHK_SUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int CITX_TAX_SEQ_NBR_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLIENT_TAX_FIXED = CLIENT_TAX_CHK_SUM + CLIENT_ID_KCRE + CITX_TAX_SEQ_NBR_KCRE;
		public static final int CLIENT_TAX_DATES = TRANS_PROCESS_DT;
		public static final int CLIENT_TAX_ROW = CLIENT_TAX_FIXED + CLIENT_TAX_DATES + Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY
				+ Cw27fClientTaxData.Len.CLIENT_TAX_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
