/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.federatedinsurance.crs.copy.Ivoryh;
import com.federatedinsurance.crs.copy.Xz08ci1i;
import com.federatedinsurance.crs.copy.Xz08coServiceOutputs;
import com.federatedinsurance.crs.copy.Xzc080ci;
import com.federatedinsurance.crs.copy.Xzc080co;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZC08090<br>
 * Generated as a class for rule WS.<br>*/
public class Xzc08090Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXzc08090 constantFields = new ConstantFieldsXzc08090();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXzc08090 errorAndAdviceMessages = new ErrorAndAdviceMessagesXzc08090();
	//Original name: URI-LKU-LINKAGE
	private Ts571cb1 ts571cb1 = new Ts571cb1();
	//Original name: SAVE-AREA
	private SaveAreaXzc08090 saveArea = new SaveAreaXzc08090();
	//Original name: SS-AI
	private short ssAi = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-ER
	private short ssEr = DefaultValues.BIN_SHORT_VAL;
	//Original name: IVORYH
	private Ivoryh ivoryh = new Ivoryh();
	//Original name: XZ08CI1I
	private Xz08ci1i xz08ci1i = new Xz08ci1i();
	//Original name: XZ08CO-SERVICE-OUTPUTS
	private Xz08coServiceOutputs xz08coServiceOutputs = new Xz08coServiceOutputs();
	//Original name: XZC080CI
	private Xzc080ci xzc080ci = new Xzc080ci();
	//Original name: XZC080CO
	private Xzc080co xzc080co = new Xzc080co();

	//==== METHODS ====
	public void setSsAi(short ssAi) {
		this.ssAi = ssAi;
	}

	public short getSsAi() {
		return this.ssAi;
	}

	public void setSsEr(short ssEr) {
		this.ssEr = ssEr;
	}

	public short getSsEr() {
		return this.ssEr;
	}

	/**Original name: CALLABLE-INPUTS<br>*/
	public byte[] getCallableInputsBytes() {
		byte[] buffer = new byte[Len.CALLABLE_INPUTS];
		return getCallableInputsBytes(buffer, 1);
	}

	public byte[] getCallableInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xz08ci1i.getServiceInputsBytes(buffer, position);
		return buffer;
	}

	public void initCallableInputsLowValues() {
		xz08ci1i.initXz08ci1iLowValues();
	}

	public void setCallableOutputsBytes(byte[] buffer) {
		setCallableOutputsBytes(buffer, 1);
	}

	/**Original name: CALLABLE-OUTPUTS<br>*/
	public byte[] getCallableOutputsBytes() {
		byte[] buffer = new byte[Len.CALLABLE_OUTPUTS];
		return getCallableOutputsBytes(buffer, 1);
	}

	public void setCallableOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xz08coServiceOutputs.setXz08coServiceOutputsBytes(buffer, position);
	}

	public byte[] getCallableOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xz08coServiceOutputs.getXz08coServiceOutputsBytes(buffer, position);
		return buffer;
	}

	public void initCallableOutputsLowValues() {
		xz08coServiceOutputs.initXz08coServiceOutputsLowValues();
	}

	public void setServiceInputsBytes(byte[] buffer) {
		setServiceInputsBytes(buffer, 1);
	}

	public void setServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc080ci.setServiceInputsBytes(buffer, position);
	}

	/**Original name: SERVICE-OUTPUTS<br>*/
	public byte[] getServiceOutputsBytes() {
		byte[] buffer = new byte[Len.SERVICE_OUTPUTS];
		return getServiceOutputsBytes(buffer, 1);
	}

	public byte[] getServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc080co.getServiceOutputsBytes(buffer, position);
		return buffer;
	}

	public ConstantFieldsXzc08090 getConstantFields() {
		return constantFields;
	}

	public ErrorAndAdviceMessagesXzc08090 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public Ivoryh getIvoryh() {
		return ivoryh;
	}

	public SaveAreaXzc08090 getSaveArea() {
		return saveArea;
	}

	public Ts571cb1 getTs571cb1() {
		return ts571cb1;
	}

	public Xz08ci1i getXz08ci1i() {
		return xz08ci1i;
	}

	public Xz08coServiceOutputs getXz08coServiceOutputs() {
		return xz08coServiceOutputs;
	}

	public Xzc080ci getXzc080ci() {
		return xzc080ci;
	}

	public Xzc080co getXzc080co() {
		return xzc080co;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SERVICE_INPUTS = Xzc080ci.Len.SERVICE_INPUTS;
		public static final int CALLABLE_INPUTS = Xz08ci1i.Len.SERVICE_INPUTS;
		public static final int CALLABLE_OUTPUTS = Xz08coServiceOutputs.Len.XZ08CO_SERVICE_OUTPUTS;
		public static final int SERVICE_OUTPUTS = Xzc080co.Len.SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
