/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P90D0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p90d0 {

	//==== PROPERTIES ====
	//Original name: CF-NOTIFY-BILLING-PGM
	private String notifyBillingPgm = "BXC08099";
	//Original name: CF-ACT-API-TYPE
	private String actApiType = "ACT";
	//Original name: CF-STA-CD-NOT-BIL-DONE
	private String staCdNotBilDone = "40";
	//Original name: CF-SP-GET-POL-LST-SVC
	private String spGetPolLstSvc = "XZ0X9050";
	//Original name: CF-SP-UPD-NOT-STA-SVC
	private String spUpdNotStaSvc = "XZ0X90M0";

	//==== METHODS ====
	public String getNotifyBillingPgm() {
		return this.notifyBillingPgm;
	}

	public String getActApiType() {
		return this.actApiType;
	}

	public String getStaCdNotBilDone() {
		return this.staCdNotBilDone;
	}

	public String getSpGetPolLstSvc() {
		return this.spGetPolLstSvc;
	}

	public String getSpUpdNotStaSvc() {
		return this.spUpdNotStaSvc;
	}
}
