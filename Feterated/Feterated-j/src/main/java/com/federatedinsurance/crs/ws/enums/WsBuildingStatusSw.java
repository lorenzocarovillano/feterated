/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-BUILDING-STATUS-SW<br>
 * Variable: WS-BUILDING-STATUS-SW from program HALUIDB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsBuildingStatusSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char INCOMPLETE = 'I';
	public static final char ERRORED = 'E';
	public static final char DUPED = 'D';
	public static final char SUCCESSFUL = 'S';
	private static final char[] COMPLETE = new char[] { 'S', 'D', 'E' };

	//==== METHODS ====
	public void setBuildingStatusSw(char buildingStatusSw) {
		this.value = buildingStatusSw;
	}

	public char getBuildingStatusSw() {
		return this.value;
	}

	public void setIncomplete() {
		value = INCOMPLETE;
	}

	public void setErrored() {
		value = ERRORED;
	}

	public void setDuped() {
		value = DUPED;
	}

	public void setSuccessful() {
		value = SUCCESSFUL;
	}

	public boolean isComplete() {
		return ArrayUtils.contains(COMPLETE, value);
	}
}
