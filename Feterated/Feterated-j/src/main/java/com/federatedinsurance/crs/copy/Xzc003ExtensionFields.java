/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XZC003-EXTENSION-FIELDS<br>
 * Variable: XZC003-EXTENSION-FIELDS from copybook XZ0C0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzc003ExtensionFields {

	//==== PROPERTIES ====
	//Original name: XZC003-REC-TYP-SHT-DES
	private String typShtDes = DefaultValues.stringVal(Len.TYP_SHT_DES);
	//Original name: XZC003-REC-TYP-LNG-DES
	private String typLngDes = DefaultValues.stringVal(Len.TYP_LNG_DES);
	//Original name: XZC003-REC-SR-ORD-NBR-SIGN
	private char srOrdNbrSign = DefaultValues.CHAR_VAL;
	//Original name: XZC003-REC-SR-ORD-NBR
	private String srOrdNbr = DefaultValues.stringVal(Len.SR_ORD_NBR);

	//==== METHODS ====
	public void setExtensionFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		typShtDes = MarshalByte.readString(buffer, position, Len.TYP_SHT_DES);
		position += Len.TYP_SHT_DES;
		typLngDes = MarshalByte.readString(buffer, position, Len.TYP_LNG_DES);
		position += Len.TYP_LNG_DES;
		srOrdNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		srOrdNbr = MarshalByte.readFixedString(buffer, position, Len.SR_ORD_NBR);
	}

	public byte[] getExtensionFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, typShtDes, Len.TYP_SHT_DES);
		position += Len.TYP_SHT_DES;
		MarshalByte.writeString(buffer, position, typLngDes, Len.TYP_LNG_DES);
		position += Len.TYP_LNG_DES;
		MarshalByte.writeChar(buffer, position, srOrdNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, srOrdNbr, Len.SR_ORD_NBR);
		return buffer;
	}

	public void setTypShtDes(String typShtDes) {
		this.typShtDes = Functions.subString(typShtDes, Len.TYP_SHT_DES);
	}

	public String getTypShtDes() {
		return this.typShtDes;
	}

	public void setTypLngDes(String typLngDes) {
		this.typLngDes = Functions.subString(typLngDes, Len.TYP_LNG_DES);
	}

	public String getTypLngDes() {
		return this.typLngDes;
	}

	public void setSrOrdNbrSign(char srOrdNbrSign) {
		this.srOrdNbrSign = srOrdNbrSign;
	}

	public char getSrOrdNbrSign() {
		return this.srOrdNbrSign;
	}

	public void setSrOrdNbr(int srOrdNbr) {
		this.srOrdNbr = NumericDisplay.asString(srOrdNbr, Len.SR_ORD_NBR);
	}

	public void setSrOrdNbrFormatted(String srOrdNbr) {
		this.srOrdNbr = Trunc.toUnsignedNumeric(srOrdNbr, Len.SR_ORD_NBR);
	}

	public int getSrOrdNbr() {
		return NumericDisplay.asInt(this.srOrdNbr);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TYP_SHT_DES = 13;
		public static final int TYP_LNG_DES = 30;
		public static final int SR_ORD_NBR_SIGN = 1;
		public static final int SR_ORD_NBR = 5;
		public static final int EXTENSION_FIELDS = TYP_SHT_DES + TYP_LNG_DES + SR_ORD_NBR_SIGN + SR_ORD_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
