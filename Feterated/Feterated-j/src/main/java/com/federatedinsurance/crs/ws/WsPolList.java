/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-POL-LIST<br>
 * Variable: WS-POL-LIST from program XZ0B9010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsPolList {

	//==== PROPERTIES ====
	//Original name: WS-POL-NBR1
	private String nbr1 = DefaultValues.stringVal(Len.NBR1);
	//Original name: WS-POL-NBR2
	private String nbr2 = DefaultValues.stringVal(Len.NBR2);
	//Original name: WS-POL-NBR3
	private String nbr3 = DefaultValues.stringVal(Len.NBR3);
	//Original name: WS-POL-NBR4
	private String nbr4 = DefaultValues.stringVal(Len.NBR4);
	//Original name: WS-POL-NBR5
	private String nbr5 = DefaultValues.stringVal(Len.NBR5);
	//Original name: WS-POL-NBR6
	private String nbr6 = DefaultValues.stringVal(Len.NBR6);
	//Original name: WS-POL-NBR7
	private String nbr7 = DefaultValues.stringVal(Len.NBR7);
	//Original name: WS-POL-NBR8
	private String nbr8 = DefaultValues.stringVal(Len.NBR8);
	//Original name: WS-POL-NBR9
	private String nbr9 = DefaultValues.stringVal(Len.NBR9);
	//Original name: WS-POL-NBR10
	private String nbr10 = DefaultValues.stringVal(Len.NBR10);
	//Original name: WS-POL-NBR11
	private String nbr11 = DefaultValues.stringVal(Len.NBR11);
	//Original name: WS-POL-NBR12
	private String nbr12 = DefaultValues.stringVal(Len.NBR12);
	//Original name: WS-POL-NBR13
	private String nbr13 = DefaultValues.stringVal(Len.NBR13);
	//Original name: WS-POL-NBR14
	private String nbr14 = DefaultValues.stringVal(Len.NBR14);
	//Original name: WS-POL-NBR15
	private String nbr15 = DefaultValues.stringVal(Len.NBR15);
	//Original name: WS-POL-NBR16
	private String nbr16 = DefaultValues.stringVal(Len.NBR16);
	//Original name: WS-POL-NBR17
	private String nbr17 = DefaultValues.stringVal(Len.NBR17);
	//Original name: WS-POL-NBR18
	private String nbr18 = DefaultValues.stringVal(Len.NBR18);
	//Original name: WS-POL-NBR19
	private String nbr19 = DefaultValues.stringVal(Len.NBR19);
	//Original name: WS-POL-NBR20
	private String nbr20 = DefaultValues.stringVal(Len.NBR20);
	//Original name: WS-POL-NBR21
	private String nbr21 = DefaultValues.stringVal(Len.NBR21);
	//Original name: WS-POL-NBR22
	private String nbr22 = DefaultValues.stringVal(Len.NBR22);
	//Original name: WS-POL-NBR23
	private String nbr23 = DefaultValues.stringVal(Len.NBR23);
	//Original name: WS-POL-NBR24
	private String nbr24 = DefaultValues.stringVal(Len.NBR24);
	//Original name: WS-POL-NBR25
	private String nbr25 = DefaultValues.stringVal(Len.NBR25);
	//Original name: WS-POL-NBR26
	private String nbr26 = DefaultValues.stringVal(Len.NBR26);
	//Original name: WS-POL-NBR27
	private String nbr27 = DefaultValues.stringVal(Len.NBR27);
	//Original name: WS-POL-NBR28
	private String nbr28 = DefaultValues.stringVal(Len.NBR28);
	//Original name: WS-POL-NBR29
	private String nbr29 = DefaultValues.stringVal(Len.NBR29);
	//Original name: WS-POL-NBR30
	private String nbr30 = DefaultValues.stringVal(Len.NBR30);
	//Original name: WS-POL-NBR31
	private String nbr31 = DefaultValues.stringVal(Len.NBR31);
	//Original name: WS-POL-NBR32
	private String nbr32 = DefaultValues.stringVal(Len.NBR32);
	//Original name: WS-POL-NBR33
	private String nbr33 = DefaultValues.stringVal(Len.NBR33);
	//Original name: WS-POL-NBR34
	private String nbr34 = DefaultValues.stringVal(Len.NBR34);
	//Original name: WS-POL-NBR35
	private String nbr35 = DefaultValues.stringVal(Len.NBR35);
	//Original name: WS-POL-NBR36
	private String nbr36 = DefaultValues.stringVal(Len.NBR36);
	//Original name: WS-POL-NBR37
	private String nbr37 = DefaultValues.stringVal(Len.NBR37);
	//Original name: WS-POL-NBR38
	private String nbr38 = DefaultValues.stringVal(Len.NBR38);
	//Original name: WS-POL-NBR39
	private String nbr39 = DefaultValues.stringVal(Len.NBR39);
	//Original name: WS-POL-NBR40
	private String nbr40 = DefaultValues.stringVal(Len.NBR40);
	//Original name: WS-POL-NBR41
	private String nbr41 = DefaultValues.stringVal(Len.NBR41);
	//Original name: WS-POL-NBR42
	private String nbr42 = DefaultValues.stringVal(Len.NBR42);
	//Original name: WS-POL-NBR43
	private String nbr43 = DefaultValues.stringVal(Len.NBR43);
	//Original name: WS-POL-NBR44
	private String nbr44 = DefaultValues.stringVal(Len.NBR44);
	//Original name: WS-POL-NBR45
	private String nbr45 = DefaultValues.stringVal(Len.NBR45);
	//Original name: WS-POL-NBR46
	private String nbr46 = DefaultValues.stringVal(Len.NBR46);
	//Original name: WS-POL-NBR47
	private String nbr47 = DefaultValues.stringVal(Len.NBR47);
	//Original name: WS-POL-NBR48
	private String nbr48 = DefaultValues.stringVal(Len.NBR48);
	//Original name: WS-POL-NBR49
	private String nbr49 = DefaultValues.stringVal(Len.NBR49);
	//Original name: WS-POL-NBR50
	private String nbr50 = DefaultValues.stringVal(Len.NBR50);
	//Original name: WS-POL-NBR51
	private String nbr51 = DefaultValues.stringVal(Len.NBR51);
	//Original name: WS-POL-NBR52
	private String nbr52 = DefaultValues.stringVal(Len.NBR52);
	//Original name: WS-POL-NBR53
	private String nbr53 = DefaultValues.stringVal(Len.NBR53);
	//Original name: WS-POL-NBR54
	private String nbr54 = DefaultValues.stringVal(Len.NBR54);
	//Original name: WS-POL-NBR55
	private String nbr55 = DefaultValues.stringVal(Len.NBR55);
	//Original name: WS-POL-NBR56
	private String nbr56 = DefaultValues.stringVal(Len.NBR56);
	//Original name: WS-POL-NBR57
	private String nbr57 = DefaultValues.stringVal(Len.NBR57);
	//Original name: WS-POL-NBR58
	private String nbr58 = DefaultValues.stringVal(Len.NBR58);
	//Original name: WS-POL-NBR59
	private String nbr59 = DefaultValues.stringVal(Len.NBR59);
	//Original name: WS-POL-NBR60
	private String nbr60 = DefaultValues.stringVal(Len.NBR60);
	//Original name: WS-POL-NBR61
	private String nbr61 = DefaultValues.stringVal(Len.NBR61);
	//Original name: WS-POL-NBR62
	private String nbr62 = DefaultValues.stringVal(Len.NBR62);
	//Original name: WS-POL-NBR63
	private String nbr63 = DefaultValues.stringVal(Len.NBR63);
	//Original name: WS-POL-NBR64
	private String nbr64 = DefaultValues.stringVal(Len.NBR64);
	//Original name: WS-POL-NBR65
	private String nbr65 = DefaultValues.stringVal(Len.NBR65);
	//Original name: WS-POL-NBR66
	private String nbr66 = DefaultValues.stringVal(Len.NBR66);
	//Original name: WS-POL-NBR67
	private String nbr67 = DefaultValues.stringVal(Len.NBR67);
	//Original name: WS-POL-NBR68
	private String nbr68 = DefaultValues.stringVal(Len.NBR68);
	//Original name: WS-POL-NBR69
	private String nbr69 = DefaultValues.stringVal(Len.NBR69);
	//Original name: WS-POL-NBR70
	private String nbr70 = DefaultValues.stringVal(Len.NBR70);
	//Original name: WS-POL-NBR71
	private String nbr71 = DefaultValues.stringVal(Len.NBR71);
	//Original name: WS-POL-NBR72
	private String nbr72 = DefaultValues.stringVal(Len.NBR72);
	//Original name: WS-POL-NBR73
	private String nbr73 = DefaultValues.stringVal(Len.NBR73);
	//Original name: WS-POL-NBR74
	private String nbr74 = DefaultValues.stringVal(Len.NBR74);
	//Original name: WS-POL-NBR75
	private String nbr75 = DefaultValues.stringVal(Len.NBR75);
	//Original name: WS-POL-NBR76
	private String nbr76 = DefaultValues.stringVal(Len.NBR76);
	//Original name: WS-POL-NBR77
	private String nbr77 = DefaultValues.stringVal(Len.NBR77);
	//Original name: WS-POL-NBR78
	private String nbr78 = DefaultValues.stringVal(Len.NBR78);
	//Original name: WS-POL-NBR79
	private String nbr79 = DefaultValues.stringVal(Len.NBR79);
	//Original name: WS-POL-NBR80
	private String nbr80 = DefaultValues.stringVal(Len.NBR80);
	//Original name: WS-POL-NBR81
	private String nbr81 = DefaultValues.stringVal(Len.NBR81);
	//Original name: WS-POL-NBR82
	private String nbr82 = DefaultValues.stringVal(Len.NBR82);
	//Original name: WS-POL-NBR83
	private String nbr83 = DefaultValues.stringVal(Len.NBR83);
	//Original name: WS-POL-NBR84
	private String nbr84 = DefaultValues.stringVal(Len.NBR84);
	//Original name: WS-POL-NBR85
	private String nbr85 = DefaultValues.stringVal(Len.NBR85);
	//Original name: WS-POL-NBR86
	private String nbr86 = DefaultValues.stringVal(Len.NBR86);
	//Original name: WS-POL-NBR87
	private String nbr87 = DefaultValues.stringVal(Len.NBR87);
	//Original name: WS-POL-NBR88
	private String nbr88 = DefaultValues.stringVal(Len.NBR88);
	//Original name: WS-POL-NBR89
	private String nbr89 = DefaultValues.stringVal(Len.NBR89);
	//Original name: WS-POL-NBR90
	private String nbr90 = DefaultValues.stringVal(Len.NBR90);
	//Original name: WS-POL-NBR91
	private String nbr91 = DefaultValues.stringVal(Len.NBR91);
	//Original name: WS-POL-NBR92
	private String nbr92 = DefaultValues.stringVal(Len.NBR92);
	//Original name: WS-POL-NBR93
	private String nbr93 = DefaultValues.stringVal(Len.NBR93);
	//Original name: WS-POL-NBR94
	private String nbr94 = DefaultValues.stringVal(Len.NBR94);
	//Original name: WS-POL-NBR95
	private String nbr95 = DefaultValues.stringVal(Len.NBR95);
	//Original name: WS-POL-NBR96
	private String nbr96 = DefaultValues.stringVal(Len.NBR96);
	//Original name: WS-POL-NBR97
	private String nbr97 = DefaultValues.stringVal(Len.NBR97);
	//Original name: WS-POL-NBR98
	private String nbr98 = DefaultValues.stringVal(Len.NBR98);
	//Original name: WS-POL-NBR99
	private String nbr99 = DefaultValues.stringVal(Len.NBR99);
	//Original name: WS-POL-NBR100
	private String nbr100 = DefaultValues.stringVal(Len.NBR100);

	//==== METHODS ====
	public String getPolListFormatted() {
		return MarshalByteExt.bufferToStr(getPolListBytes());
	}

	public void setPolListBytes(byte[] buffer) {
		setPolListBytes(buffer, 1);
	}

	public byte[] getPolListBytes() {
		byte[] buffer = new byte[Len.POL_LIST];
		return getPolListBytes(buffer, 1);
	}

	public void setPolListBytes(byte[] buffer, int offset) {
		int position = offset;
		nbr1 = MarshalByte.readString(buffer, position, Len.NBR1);
		position += Len.NBR1;
		nbr2 = MarshalByte.readString(buffer, position, Len.NBR2);
		position += Len.NBR2;
		nbr3 = MarshalByte.readString(buffer, position, Len.NBR3);
		position += Len.NBR3;
		nbr4 = MarshalByte.readString(buffer, position, Len.NBR4);
		position += Len.NBR4;
		nbr5 = MarshalByte.readString(buffer, position, Len.NBR5);
		position += Len.NBR5;
		nbr6 = MarshalByte.readString(buffer, position, Len.NBR6);
		position += Len.NBR6;
		nbr7 = MarshalByte.readString(buffer, position, Len.NBR7);
		position += Len.NBR7;
		nbr8 = MarshalByte.readString(buffer, position, Len.NBR8);
		position += Len.NBR8;
		nbr9 = MarshalByte.readString(buffer, position, Len.NBR9);
		position += Len.NBR9;
		nbr10 = MarshalByte.readString(buffer, position, Len.NBR10);
		position += Len.NBR10;
		nbr11 = MarshalByte.readString(buffer, position, Len.NBR11);
		position += Len.NBR11;
		nbr12 = MarshalByte.readString(buffer, position, Len.NBR12);
		position += Len.NBR12;
		nbr13 = MarshalByte.readString(buffer, position, Len.NBR13);
		position += Len.NBR13;
		nbr14 = MarshalByte.readString(buffer, position, Len.NBR14);
		position += Len.NBR14;
		nbr15 = MarshalByte.readString(buffer, position, Len.NBR15);
		position += Len.NBR15;
		nbr16 = MarshalByte.readString(buffer, position, Len.NBR16);
		position += Len.NBR16;
		nbr17 = MarshalByte.readString(buffer, position, Len.NBR17);
		position += Len.NBR17;
		nbr18 = MarshalByte.readString(buffer, position, Len.NBR18);
		position += Len.NBR18;
		nbr19 = MarshalByte.readString(buffer, position, Len.NBR19);
		position += Len.NBR19;
		nbr20 = MarshalByte.readString(buffer, position, Len.NBR20);
		position += Len.NBR20;
		nbr21 = MarshalByte.readString(buffer, position, Len.NBR21);
		position += Len.NBR21;
		nbr22 = MarshalByte.readString(buffer, position, Len.NBR22);
		position += Len.NBR22;
		nbr23 = MarshalByte.readString(buffer, position, Len.NBR23);
		position += Len.NBR23;
		nbr24 = MarshalByte.readString(buffer, position, Len.NBR24);
		position += Len.NBR24;
		nbr25 = MarshalByte.readString(buffer, position, Len.NBR25);
		position += Len.NBR25;
		nbr26 = MarshalByte.readString(buffer, position, Len.NBR26);
		position += Len.NBR26;
		nbr27 = MarshalByte.readString(buffer, position, Len.NBR27);
		position += Len.NBR27;
		nbr28 = MarshalByte.readString(buffer, position, Len.NBR28);
		position += Len.NBR28;
		nbr29 = MarshalByte.readString(buffer, position, Len.NBR29);
		position += Len.NBR29;
		nbr30 = MarshalByte.readString(buffer, position, Len.NBR30);
		position += Len.NBR30;
		nbr31 = MarshalByte.readString(buffer, position, Len.NBR31);
		position += Len.NBR31;
		nbr32 = MarshalByte.readString(buffer, position, Len.NBR32);
		position += Len.NBR32;
		nbr33 = MarshalByte.readString(buffer, position, Len.NBR33);
		position += Len.NBR33;
		nbr34 = MarshalByte.readString(buffer, position, Len.NBR34);
		position += Len.NBR34;
		nbr35 = MarshalByte.readString(buffer, position, Len.NBR35);
		position += Len.NBR35;
		nbr36 = MarshalByte.readString(buffer, position, Len.NBR36);
		position += Len.NBR36;
		nbr37 = MarshalByte.readString(buffer, position, Len.NBR37);
		position += Len.NBR37;
		nbr38 = MarshalByte.readString(buffer, position, Len.NBR38);
		position += Len.NBR38;
		nbr39 = MarshalByte.readString(buffer, position, Len.NBR39);
		position += Len.NBR39;
		nbr40 = MarshalByte.readString(buffer, position, Len.NBR40);
		position += Len.NBR40;
		nbr41 = MarshalByte.readString(buffer, position, Len.NBR41);
		position += Len.NBR41;
		nbr42 = MarshalByte.readString(buffer, position, Len.NBR42);
		position += Len.NBR42;
		nbr43 = MarshalByte.readString(buffer, position, Len.NBR43);
		position += Len.NBR43;
		nbr44 = MarshalByte.readString(buffer, position, Len.NBR44);
		position += Len.NBR44;
		nbr45 = MarshalByte.readString(buffer, position, Len.NBR45);
		position += Len.NBR45;
		nbr46 = MarshalByte.readString(buffer, position, Len.NBR46);
		position += Len.NBR46;
		nbr47 = MarshalByte.readString(buffer, position, Len.NBR47);
		position += Len.NBR47;
		nbr48 = MarshalByte.readString(buffer, position, Len.NBR48);
		position += Len.NBR48;
		nbr49 = MarshalByte.readString(buffer, position, Len.NBR49);
		position += Len.NBR49;
		nbr50 = MarshalByte.readString(buffer, position, Len.NBR50);
		position += Len.NBR50;
		nbr51 = MarshalByte.readString(buffer, position, Len.NBR51);
		position += Len.NBR51;
		nbr52 = MarshalByte.readString(buffer, position, Len.NBR52);
		position += Len.NBR52;
		nbr53 = MarshalByte.readString(buffer, position, Len.NBR53);
		position += Len.NBR53;
		nbr54 = MarshalByte.readString(buffer, position, Len.NBR54);
		position += Len.NBR54;
		nbr55 = MarshalByte.readString(buffer, position, Len.NBR55);
		position += Len.NBR55;
		nbr56 = MarshalByte.readString(buffer, position, Len.NBR56);
		position += Len.NBR56;
		nbr57 = MarshalByte.readString(buffer, position, Len.NBR57);
		position += Len.NBR57;
		nbr58 = MarshalByte.readString(buffer, position, Len.NBR58);
		position += Len.NBR58;
		nbr59 = MarshalByte.readString(buffer, position, Len.NBR59);
		position += Len.NBR59;
		nbr60 = MarshalByte.readString(buffer, position, Len.NBR60);
		position += Len.NBR60;
		nbr61 = MarshalByte.readString(buffer, position, Len.NBR61);
		position += Len.NBR61;
		nbr62 = MarshalByte.readString(buffer, position, Len.NBR62);
		position += Len.NBR62;
		nbr63 = MarshalByte.readString(buffer, position, Len.NBR63);
		position += Len.NBR63;
		nbr64 = MarshalByte.readString(buffer, position, Len.NBR64);
		position += Len.NBR64;
		nbr65 = MarshalByte.readString(buffer, position, Len.NBR65);
		position += Len.NBR65;
		nbr66 = MarshalByte.readString(buffer, position, Len.NBR66);
		position += Len.NBR66;
		nbr67 = MarshalByte.readString(buffer, position, Len.NBR67);
		position += Len.NBR67;
		nbr68 = MarshalByte.readString(buffer, position, Len.NBR68);
		position += Len.NBR68;
		nbr69 = MarshalByte.readString(buffer, position, Len.NBR69);
		position += Len.NBR69;
		nbr70 = MarshalByte.readString(buffer, position, Len.NBR70);
		position += Len.NBR70;
		nbr71 = MarshalByte.readString(buffer, position, Len.NBR71);
		position += Len.NBR71;
		nbr72 = MarshalByte.readString(buffer, position, Len.NBR72);
		position += Len.NBR72;
		nbr73 = MarshalByte.readString(buffer, position, Len.NBR73);
		position += Len.NBR73;
		nbr74 = MarshalByte.readString(buffer, position, Len.NBR74);
		position += Len.NBR74;
		nbr75 = MarshalByte.readString(buffer, position, Len.NBR75);
		position += Len.NBR75;
		nbr76 = MarshalByte.readString(buffer, position, Len.NBR76);
		position += Len.NBR76;
		nbr77 = MarshalByte.readString(buffer, position, Len.NBR77);
		position += Len.NBR77;
		nbr78 = MarshalByte.readString(buffer, position, Len.NBR78);
		position += Len.NBR78;
		nbr79 = MarshalByte.readString(buffer, position, Len.NBR79);
		position += Len.NBR79;
		nbr80 = MarshalByte.readString(buffer, position, Len.NBR80);
		position += Len.NBR80;
		nbr81 = MarshalByte.readString(buffer, position, Len.NBR81);
		position += Len.NBR81;
		nbr82 = MarshalByte.readString(buffer, position, Len.NBR82);
		position += Len.NBR82;
		nbr83 = MarshalByte.readString(buffer, position, Len.NBR83);
		position += Len.NBR83;
		nbr84 = MarshalByte.readString(buffer, position, Len.NBR84);
		position += Len.NBR84;
		nbr85 = MarshalByte.readString(buffer, position, Len.NBR85);
		position += Len.NBR85;
		nbr86 = MarshalByte.readString(buffer, position, Len.NBR86);
		position += Len.NBR86;
		nbr87 = MarshalByte.readString(buffer, position, Len.NBR87);
		position += Len.NBR87;
		nbr88 = MarshalByte.readString(buffer, position, Len.NBR88);
		position += Len.NBR88;
		nbr89 = MarshalByte.readString(buffer, position, Len.NBR89);
		position += Len.NBR89;
		nbr90 = MarshalByte.readString(buffer, position, Len.NBR90);
		position += Len.NBR90;
		nbr91 = MarshalByte.readString(buffer, position, Len.NBR91);
		position += Len.NBR91;
		nbr92 = MarshalByte.readString(buffer, position, Len.NBR92);
		position += Len.NBR92;
		nbr93 = MarshalByte.readString(buffer, position, Len.NBR93);
		position += Len.NBR93;
		nbr94 = MarshalByte.readString(buffer, position, Len.NBR94);
		position += Len.NBR94;
		nbr95 = MarshalByte.readString(buffer, position, Len.NBR95);
		position += Len.NBR95;
		nbr96 = MarshalByte.readString(buffer, position, Len.NBR96);
		position += Len.NBR96;
		nbr97 = MarshalByte.readString(buffer, position, Len.NBR97);
		position += Len.NBR97;
		nbr98 = MarshalByte.readString(buffer, position, Len.NBR98);
		position += Len.NBR98;
		nbr99 = MarshalByte.readString(buffer, position, Len.NBR99);
		position += Len.NBR99;
		nbr100 = MarshalByte.readString(buffer, position, Len.NBR100);
	}

	public byte[] getPolListBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, nbr1, Len.NBR1);
		position += Len.NBR1;
		MarshalByte.writeString(buffer, position, nbr2, Len.NBR2);
		position += Len.NBR2;
		MarshalByte.writeString(buffer, position, nbr3, Len.NBR3);
		position += Len.NBR3;
		MarshalByte.writeString(buffer, position, nbr4, Len.NBR4);
		position += Len.NBR4;
		MarshalByte.writeString(buffer, position, nbr5, Len.NBR5);
		position += Len.NBR5;
		MarshalByte.writeString(buffer, position, nbr6, Len.NBR6);
		position += Len.NBR6;
		MarshalByte.writeString(buffer, position, nbr7, Len.NBR7);
		position += Len.NBR7;
		MarshalByte.writeString(buffer, position, nbr8, Len.NBR8);
		position += Len.NBR8;
		MarshalByte.writeString(buffer, position, nbr9, Len.NBR9);
		position += Len.NBR9;
		MarshalByte.writeString(buffer, position, nbr10, Len.NBR10);
		position += Len.NBR10;
		MarshalByte.writeString(buffer, position, nbr11, Len.NBR11);
		position += Len.NBR11;
		MarshalByte.writeString(buffer, position, nbr12, Len.NBR12);
		position += Len.NBR12;
		MarshalByte.writeString(buffer, position, nbr13, Len.NBR13);
		position += Len.NBR13;
		MarshalByte.writeString(buffer, position, nbr14, Len.NBR14);
		position += Len.NBR14;
		MarshalByte.writeString(buffer, position, nbr15, Len.NBR15);
		position += Len.NBR15;
		MarshalByte.writeString(buffer, position, nbr16, Len.NBR16);
		position += Len.NBR16;
		MarshalByte.writeString(buffer, position, nbr17, Len.NBR17);
		position += Len.NBR17;
		MarshalByte.writeString(buffer, position, nbr18, Len.NBR18);
		position += Len.NBR18;
		MarshalByte.writeString(buffer, position, nbr19, Len.NBR19);
		position += Len.NBR19;
		MarshalByte.writeString(buffer, position, nbr20, Len.NBR20);
		position += Len.NBR20;
		MarshalByte.writeString(buffer, position, nbr21, Len.NBR21);
		position += Len.NBR21;
		MarshalByte.writeString(buffer, position, nbr22, Len.NBR22);
		position += Len.NBR22;
		MarshalByte.writeString(buffer, position, nbr23, Len.NBR23);
		position += Len.NBR23;
		MarshalByte.writeString(buffer, position, nbr24, Len.NBR24);
		position += Len.NBR24;
		MarshalByte.writeString(buffer, position, nbr25, Len.NBR25);
		position += Len.NBR25;
		MarshalByte.writeString(buffer, position, nbr26, Len.NBR26);
		position += Len.NBR26;
		MarshalByte.writeString(buffer, position, nbr27, Len.NBR27);
		position += Len.NBR27;
		MarshalByte.writeString(buffer, position, nbr28, Len.NBR28);
		position += Len.NBR28;
		MarshalByte.writeString(buffer, position, nbr29, Len.NBR29);
		position += Len.NBR29;
		MarshalByte.writeString(buffer, position, nbr30, Len.NBR30);
		position += Len.NBR30;
		MarshalByte.writeString(buffer, position, nbr31, Len.NBR31);
		position += Len.NBR31;
		MarshalByte.writeString(buffer, position, nbr32, Len.NBR32);
		position += Len.NBR32;
		MarshalByte.writeString(buffer, position, nbr33, Len.NBR33);
		position += Len.NBR33;
		MarshalByte.writeString(buffer, position, nbr34, Len.NBR34);
		position += Len.NBR34;
		MarshalByte.writeString(buffer, position, nbr35, Len.NBR35);
		position += Len.NBR35;
		MarshalByte.writeString(buffer, position, nbr36, Len.NBR36);
		position += Len.NBR36;
		MarshalByte.writeString(buffer, position, nbr37, Len.NBR37);
		position += Len.NBR37;
		MarshalByte.writeString(buffer, position, nbr38, Len.NBR38);
		position += Len.NBR38;
		MarshalByte.writeString(buffer, position, nbr39, Len.NBR39);
		position += Len.NBR39;
		MarshalByte.writeString(buffer, position, nbr40, Len.NBR40);
		position += Len.NBR40;
		MarshalByte.writeString(buffer, position, nbr41, Len.NBR41);
		position += Len.NBR41;
		MarshalByte.writeString(buffer, position, nbr42, Len.NBR42);
		position += Len.NBR42;
		MarshalByte.writeString(buffer, position, nbr43, Len.NBR43);
		position += Len.NBR43;
		MarshalByte.writeString(buffer, position, nbr44, Len.NBR44);
		position += Len.NBR44;
		MarshalByte.writeString(buffer, position, nbr45, Len.NBR45);
		position += Len.NBR45;
		MarshalByte.writeString(buffer, position, nbr46, Len.NBR46);
		position += Len.NBR46;
		MarshalByte.writeString(buffer, position, nbr47, Len.NBR47);
		position += Len.NBR47;
		MarshalByte.writeString(buffer, position, nbr48, Len.NBR48);
		position += Len.NBR48;
		MarshalByte.writeString(buffer, position, nbr49, Len.NBR49);
		position += Len.NBR49;
		MarshalByte.writeString(buffer, position, nbr50, Len.NBR50);
		position += Len.NBR50;
		MarshalByte.writeString(buffer, position, nbr51, Len.NBR51);
		position += Len.NBR51;
		MarshalByte.writeString(buffer, position, nbr52, Len.NBR52);
		position += Len.NBR52;
		MarshalByte.writeString(buffer, position, nbr53, Len.NBR53);
		position += Len.NBR53;
		MarshalByte.writeString(buffer, position, nbr54, Len.NBR54);
		position += Len.NBR54;
		MarshalByte.writeString(buffer, position, nbr55, Len.NBR55);
		position += Len.NBR55;
		MarshalByte.writeString(buffer, position, nbr56, Len.NBR56);
		position += Len.NBR56;
		MarshalByte.writeString(buffer, position, nbr57, Len.NBR57);
		position += Len.NBR57;
		MarshalByte.writeString(buffer, position, nbr58, Len.NBR58);
		position += Len.NBR58;
		MarshalByte.writeString(buffer, position, nbr59, Len.NBR59);
		position += Len.NBR59;
		MarshalByte.writeString(buffer, position, nbr60, Len.NBR60);
		position += Len.NBR60;
		MarshalByte.writeString(buffer, position, nbr61, Len.NBR61);
		position += Len.NBR61;
		MarshalByte.writeString(buffer, position, nbr62, Len.NBR62);
		position += Len.NBR62;
		MarshalByte.writeString(buffer, position, nbr63, Len.NBR63);
		position += Len.NBR63;
		MarshalByte.writeString(buffer, position, nbr64, Len.NBR64);
		position += Len.NBR64;
		MarshalByte.writeString(buffer, position, nbr65, Len.NBR65);
		position += Len.NBR65;
		MarshalByte.writeString(buffer, position, nbr66, Len.NBR66);
		position += Len.NBR66;
		MarshalByte.writeString(buffer, position, nbr67, Len.NBR67);
		position += Len.NBR67;
		MarshalByte.writeString(buffer, position, nbr68, Len.NBR68);
		position += Len.NBR68;
		MarshalByte.writeString(buffer, position, nbr69, Len.NBR69);
		position += Len.NBR69;
		MarshalByte.writeString(buffer, position, nbr70, Len.NBR70);
		position += Len.NBR70;
		MarshalByte.writeString(buffer, position, nbr71, Len.NBR71);
		position += Len.NBR71;
		MarshalByte.writeString(buffer, position, nbr72, Len.NBR72);
		position += Len.NBR72;
		MarshalByte.writeString(buffer, position, nbr73, Len.NBR73);
		position += Len.NBR73;
		MarshalByte.writeString(buffer, position, nbr74, Len.NBR74);
		position += Len.NBR74;
		MarshalByte.writeString(buffer, position, nbr75, Len.NBR75);
		position += Len.NBR75;
		MarshalByte.writeString(buffer, position, nbr76, Len.NBR76);
		position += Len.NBR76;
		MarshalByte.writeString(buffer, position, nbr77, Len.NBR77);
		position += Len.NBR77;
		MarshalByte.writeString(buffer, position, nbr78, Len.NBR78);
		position += Len.NBR78;
		MarshalByte.writeString(buffer, position, nbr79, Len.NBR79);
		position += Len.NBR79;
		MarshalByte.writeString(buffer, position, nbr80, Len.NBR80);
		position += Len.NBR80;
		MarshalByte.writeString(buffer, position, nbr81, Len.NBR81);
		position += Len.NBR81;
		MarshalByte.writeString(buffer, position, nbr82, Len.NBR82);
		position += Len.NBR82;
		MarshalByte.writeString(buffer, position, nbr83, Len.NBR83);
		position += Len.NBR83;
		MarshalByte.writeString(buffer, position, nbr84, Len.NBR84);
		position += Len.NBR84;
		MarshalByte.writeString(buffer, position, nbr85, Len.NBR85);
		position += Len.NBR85;
		MarshalByte.writeString(buffer, position, nbr86, Len.NBR86);
		position += Len.NBR86;
		MarshalByte.writeString(buffer, position, nbr87, Len.NBR87);
		position += Len.NBR87;
		MarshalByte.writeString(buffer, position, nbr88, Len.NBR88);
		position += Len.NBR88;
		MarshalByte.writeString(buffer, position, nbr89, Len.NBR89);
		position += Len.NBR89;
		MarshalByte.writeString(buffer, position, nbr90, Len.NBR90);
		position += Len.NBR90;
		MarshalByte.writeString(buffer, position, nbr91, Len.NBR91);
		position += Len.NBR91;
		MarshalByte.writeString(buffer, position, nbr92, Len.NBR92);
		position += Len.NBR92;
		MarshalByte.writeString(buffer, position, nbr93, Len.NBR93);
		position += Len.NBR93;
		MarshalByte.writeString(buffer, position, nbr94, Len.NBR94);
		position += Len.NBR94;
		MarshalByte.writeString(buffer, position, nbr95, Len.NBR95);
		position += Len.NBR95;
		MarshalByte.writeString(buffer, position, nbr96, Len.NBR96);
		position += Len.NBR96;
		MarshalByte.writeString(buffer, position, nbr97, Len.NBR97);
		position += Len.NBR97;
		MarshalByte.writeString(buffer, position, nbr98, Len.NBR98);
		position += Len.NBR98;
		MarshalByte.writeString(buffer, position, nbr99, Len.NBR99);
		position += Len.NBR99;
		MarshalByte.writeString(buffer, position, nbr100, Len.NBR100);
		return buffer;
	}

	public void setNbr1(String nbr1) {
		this.nbr1 = Functions.subString(nbr1, Len.NBR1);
	}

	public String getNbr1() {
		return this.nbr1;
	}

	public void setNbr2(String nbr2) {
		this.nbr2 = Functions.subString(nbr2, Len.NBR2);
	}

	public String getNbr2() {
		return this.nbr2;
	}

	public void setNbr3(String nbr3) {
		this.nbr3 = Functions.subString(nbr3, Len.NBR3);
	}

	public String getNbr3() {
		return this.nbr3;
	}

	public void setNbr4(String nbr4) {
		this.nbr4 = Functions.subString(nbr4, Len.NBR4);
	}

	public String getNbr4() {
		return this.nbr4;
	}

	public void setNbr5(String nbr5) {
		this.nbr5 = Functions.subString(nbr5, Len.NBR5);
	}

	public String getNbr5() {
		return this.nbr5;
	}

	public void setNbr6(String nbr6) {
		this.nbr6 = Functions.subString(nbr6, Len.NBR6);
	}

	public String getNbr6() {
		return this.nbr6;
	}

	public void setNbr7(String nbr7) {
		this.nbr7 = Functions.subString(nbr7, Len.NBR7);
	}

	public String getNbr7() {
		return this.nbr7;
	}

	public void setNbr8(String nbr8) {
		this.nbr8 = Functions.subString(nbr8, Len.NBR8);
	}

	public String getNbr8() {
		return this.nbr8;
	}

	public void setNbr9(String nbr9) {
		this.nbr9 = Functions.subString(nbr9, Len.NBR9);
	}

	public String getNbr9() {
		return this.nbr9;
	}

	public void setNbr10(String nbr10) {
		this.nbr10 = Functions.subString(nbr10, Len.NBR10);
	}

	public String getNbr10() {
		return this.nbr10;
	}

	public void setNbr11(String nbr11) {
		this.nbr11 = Functions.subString(nbr11, Len.NBR11);
	}

	public String getNbr11() {
		return this.nbr11;
	}

	public void setNbr12(String nbr12) {
		this.nbr12 = Functions.subString(nbr12, Len.NBR12);
	}

	public String getNbr12() {
		return this.nbr12;
	}

	public void setNbr13(String nbr13) {
		this.nbr13 = Functions.subString(nbr13, Len.NBR13);
	}

	public String getNbr13() {
		return this.nbr13;
	}

	public void setNbr14(String nbr14) {
		this.nbr14 = Functions.subString(nbr14, Len.NBR14);
	}

	public String getNbr14() {
		return this.nbr14;
	}

	public void setNbr15(String nbr15) {
		this.nbr15 = Functions.subString(nbr15, Len.NBR15);
	}

	public String getNbr15() {
		return this.nbr15;
	}

	public void setNbr16(String nbr16) {
		this.nbr16 = Functions.subString(nbr16, Len.NBR16);
	}

	public String getNbr16() {
		return this.nbr16;
	}

	public void setNbr17(String nbr17) {
		this.nbr17 = Functions.subString(nbr17, Len.NBR17);
	}

	public String getNbr17() {
		return this.nbr17;
	}

	public void setNbr18(String nbr18) {
		this.nbr18 = Functions.subString(nbr18, Len.NBR18);
	}

	public String getNbr18() {
		return this.nbr18;
	}

	public void setNbr19(String nbr19) {
		this.nbr19 = Functions.subString(nbr19, Len.NBR19);
	}

	public String getNbr19() {
		return this.nbr19;
	}

	public void setNbr20(String nbr20) {
		this.nbr20 = Functions.subString(nbr20, Len.NBR20);
	}

	public String getNbr20() {
		return this.nbr20;
	}

	public void setNbr21(String nbr21) {
		this.nbr21 = Functions.subString(nbr21, Len.NBR21);
	}

	public String getNbr21() {
		return this.nbr21;
	}

	public void setNbr22(String nbr22) {
		this.nbr22 = Functions.subString(nbr22, Len.NBR22);
	}

	public String getNbr22() {
		return this.nbr22;
	}

	public void setNbr23(String nbr23) {
		this.nbr23 = Functions.subString(nbr23, Len.NBR23);
	}

	public String getNbr23() {
		return this.nbr23;
	}

	public void setNbr24(String nbr24) {
		this.nbr24 = Functions.subString(nbr24, Len.NBR24);
	}

	public String getNbr24() {
		return this.nbr24;
	}

	public void setNbr25(String nbr25) {
		this.nbr25 = Functions.subString(nbr25, Len.NBR25);
	}

	public String getNbr25() {
		return this.nbr25;
	}

	public void setNbr26(String nbr26) {
		this.nbr26 = Functions.subString(nbr26, Len.NBR26);
	}

	public String getNbr26() {
		return this.nbr26;
	}

	public void setNbr27(String nbr27) {
		this.nbr27 = Functions.subString(nbr27, Len.NBR27);
	}

	public String getNbr27() {
		return this.nbr27;
	}

	public void setNbr28(String nbr28) {
		this.nbr28 = Functions.subString(nbr28, Len.NBR28);
	}

	public String getNbr28() {
		return this.nbr28;
	}

	public void setNbr29(String nbr29) {
		this.nbr29 = Functions.subString(nbr29, Len.NBR29);
	}

	public String getNbr29() {
		return this.nbr29;
	}

	public void setNbr30(String nbr30) {
		this.nbr30 = Functions.subString(nbr30, Len.NBR30);
	}

	public String getNbr30() {
		return this.nbr30;
	}

	public void setNbr31(String nbr31) {
		this.nbr31 = Functions.subString(nbr31, Len.NBR31);
	}

	public String getNbr31() {
		return this.nbr31;
	}

	public void setNbr32(String nbr32) {
		this.nbr32 = Functions.subString(nbr32, Len.NBR32);
	}

	public String getNbr32() {
		return this.nbr32;
	}

	public void setNbr33(String nbr33) {
		this.nbr33 = Functions.subString(nbr33, Len.NBR33);
	}

	public String getNbr33() {
		return this.nbr33;
	}

	public void setNbr34(String nbr34) {
		this.nbr34 = Functions.subString(nbr34, Len.NBR34);
	}

	public String getNbr34() {
		return this.nbr34;
	}

	public void setNbr35(String nbr35) {
		this.nbr35 = Functions.subString(nbr35, Len.NBR35);
	}

	public String getNbr35() {
		return this.nbr35;
	}

	public void setNbr36(String nbr36) {
		this.nbr36 = Functions.subString(nbr36, Len.NBR36);
	}

	public String getNbr36() {
		return this.nbr36;
	}

	public void setNbr37(String nbr37) {
		this.nbr37 = Functions.subString(nbr37, Len.NBR37);
	}

	public String getNbr37() {
		return this.nbr37;
	}

	public void setNbr38(String nbr38) {
		this.nbr38 = Functions.subString(nbr38, Len.NBR38);
	}

	public String getNbr38() {
		return this.nbr38;
	}

	public void setNbr39(String nbr39) {
		this.nbr39 = Functions.subString(nbr39, Len.NBR39);
	}

	public String getNbr39() {
		return this.nbr39;
	}

	public void setNbr40(String nbr40) {
		this.nbr40 = Functions.subString(nbr40, Len.NBR40);
	}

	public String getNbr40() {
		return this.nbr40;
	}

	public void setNbr41(String nbr41) {
		this.nbr41 = Functions.subString(nbr41, Len.NBR41);
	}

	public String getNbr41() {
		return this.nbr41;
	}

	public void setNbr42(String nbr42) {
		this.nbr42 = Functions.subString(nbr42, Len.NBR42);
	}

	public String getNbr42() {
		return this.nbr42;
	}

	public void setNbr43(String nbr43) {
		this.nbr43 = Functions.subString(nbr43, Len.NBR43);
	}

	public String getNbr43() {
		return this.nbr43;
	}

	public void setNbr44(String nbr44) {
		this.nbr44 = Functions.subString(nbr44, Len.NBR44);
	}

	public String getNbr44() {
		return this.nbr44;
	}

	public void setNbr45(String nbr45) {
		this.nbr45 = Functions.subString(nbr45, Len.NBR45);
	}

	public String getNbr45() {
		return this.nbr45;
	}

	public void setNbr46(String nbr46) {
		this.nbr46 = Functions.subString(nbr46, Len.NBR46);
	}

	public String getNbr46() {
		return this.nbr46;
	}

	public void setNbr47(String nbr47) {
		this.nbr47 = Functions.subString(nbr47, Len.NBR47);
	}

	public String getNbr47() {
		return this.nbr47;
	}

	public void setNbr48(String nbr48) {
		this.nbr48 = Functions.subString(nbr48, Len.NBR48);
	}

	public String getNbr48() {
		return this.nbr48;
	}

	public void setNbr49(String nbr49) {
		this.nbr49 = Functions.subString(nbr49, Len.NBR49);
	}

	public String getNbr49() {
		return this.nbr49;
	}

	public void setNbr50(String nbr50) {
		this.nbr50 = Functions.subString(nbr50, Len.NBR50);
	}

	public String getNbr50() {
		return this.nbr50;
	}

	public void setNbr51(String nbr51) {
		this.nbr51 = Functions.subString(nbr51, Len.NBR51);
	}

	public String getNbr51() {
		return this.nbr51;
	}

	public void setNbr52(String nbr52) {
		this.nbr52 = Functions.subString(nbr52, Len.NBR52);
	}

	public String getNbr52() {
		return this.nbr52;
	}

	public void setNbr53(String nbr53) {
		this.nbr53 = Functions.subString(nbr53, Len.NBR53);
	}

	public String getNbr53() {
		return this.nbr53;
	}

	public void setNbr54(String nbr54) {
		this.nbr54 = Functions.subString(nbr54, Len.NBR54);
	}

	public String getNbr54() {
		return this.nbr54;
	}

	public void setNbr55(String nbr55) {
		this.nbr55 = Functions.subString(nbr55, Len.NBR55);
	}

	public String getNbr55() {
		return this.nbr55;
	}

	public void setNbr56(String nbr56) {
		this.nbr56 = Functions.subString(nbr56, Len.NBR56);
	}

	public String getNbr56() {
		return this.nbr56;
	}

	public void setNbr57(String nbr57) {
		this.nbr57 = Functions.subString(nbr57, Len.NBR57);
	}

	public String getNbr57() {
		return this.nbr57;
	}

	public void setNbr58(String nbr58) {
		this.nbr58 = Functions.subString(nbr58, Len.NBR58);
	}

	public String getNbr58() {
		return this.nbr58;
	}

	public void setNbr59(String nbr59) {
		this.nbr59 = Functions.subString(nbr59, Len.NBR59);
	}

	public String getNbr59() {
		return this.nbr59;
	}

	public void setNbr60(String nbr60) {
		this.nbr60 = Functions.subString(nbr60, Len.NBR60);
	}

	public String getNbr60() {
		return this.nbr60;
	}

	public void setNbr61(String nbr61) {
		this.nbr61 = Functions.subString(nbr61, Len.NBR61);
	}

	public String getNbr61() {
		return this.nbr61;
	}

	public void setNbr62(String nbr62) {
		this.nbr62 = Functions.subString(nbr62, Len.NBR62);
	}

	public String getNbr62() {
		return this.nbr62;
	}

	public void setNbr63(String nbr63) {
		this.nbr63 = Functions.subString(nbr63, Len.NBR63);
	}

	public String getNbr63() {
		return this.nbr63;
	}

	public void setNbr64(String nbr64) {
		this.nbr64 = Functions.subString(nbr64, Len.NBR64);
	}

	public String getNbr64() {
		return this.nbr64;
	}

	public void setNbr65(String nbr65) {
		this.nbr65 = Functions.subString(nbr65, Len.NBR65);
	}

	public String getNbr65() {
		return this.nbr65;
	}

	public void setNbr66(String nbr66) {
		this.nbr66 = Functions.subString(nbr66, Len.NBR66);
	}

	public String getNbr66() {
		return this.nbr66;
	}

	public void setNbr67(String nbr67) {
		this.nbr67 = Functions.subString(nbr67, Len.NBR67);
	}

	public String getNbr67() {
		return this.nbr67;
	}

	public void setNbr68(String nbr68) {
		this.nbr68 = Functions.subString(nbr68, Len.NBR68);
	}

	public String getNbr68() {
		return this.nbr68;
	}

	public void setNbr69(String nbr69) {
		this.nbr69 = Functions.subString(nbr69, Len.NBR69);
	}

	public String getNbr69() {
		return this.nbr69;
	}

	public void setNbr70(String nbr70) {
		this.nbr70 = Functions.subString(nbr70, Len.NBR70);
	}

	public String getNbr70() {
		return this.nbr70;
	}

	public void setNbr71(String nbr71) {
		this.nbr71 = Functions.subString(nbr71, Len.NBR71);
	}

	public String getNbr71() {
		return this.nbr71;
	}

	public void setNbr72(String nbr72) {
		this.nbr72 = Functions.subString(nbr72, Len.NBR72);
	}

	public String getNbr72() {
		return this.nbr72;
	}

	public void setNbr73(String nbr73) {
		this.nbr73 = Functions.subString(nbr73, Len.NBR73);
	}

	public String getNbr73() {
		return this.nbr73;
	}

	public void setNbr74(String nbr74) {
		this.nbr74 = Functions.subString(nbr74, Len.NBR74);
	}

	public String getNbr74() {
		return this.nbr74;
	}

	public void setNbr75(String nbr75) {
		this.nbr75 = Functions.subString(nbr75, Len.NBR75);
	}

	public String getNbr75() {
		return this.nbr75;
	}

	public void setNbr76(String nbr76) {
		this.nbr76 = Functions.subString(nbr76, Len.NBR76);
	}

	public String getNbr76() {
		return this.nbr76;
	}

	public void setNbr77(String nbr77) {
		this.nbr77 = Functions.subString(nbr77, Len.NBR77);
	}

	public String getNbr77() {
		return this.nbr77;
	}

	public void setNbr78(String nbr78) {
		this.nbr78 = Functions.subString(nbr78, Len.NBR78);
	}

	public String getNbr78() {
		return this.nbr78;
	}

	public void setNbr79(String nbr79) {
		this.nbr79 = Functions.subString(nbr79, Len.NBR79);
	}

	public String getNbr79() {
		return this.nbr79;
	}

	public void setNbr80(String nbr80) {
		this.nbr80 = Functions.subString(nbr80, Len.NBR80);
	}

	public String getNbr80() {
		return this.nbr80;
	}

	public void setNbr81(String nbr81) {
		this.nbr81 = Functions.subString(nbr81, Len.NBR81);
	}

	public String getNbr81() {
		return this.nbr81;
	}

	public void setNbr82(String nbr82) {
		this.nbr82 = Functions.subString(nbr82, Len.NBR82);
	}

	public String getNbr82() {
		return this.nbr82;
	}

	public void setNbr83(String nbr83) {
		this.nbr83 = Functions.subString(nbr83, Len.NBR83);
	}

	public String getNbr83() {
		return this.nbr83;
	}

	public void setNbr84(String nbr84) {
		this.nbr84 = Functions.subString(nbr84, Len.NBR84);
	}

	public String getNbr84() {
		return this.nbr84;
	}

	public void setNbr85(String nbr85) {
		this.nbr85 = Functions.subString(nbr85, Len.NBR85);
	}

	public String getNbr85() {
		return this.nbr85;
	}

	public void setNbr86(String nbr86) {
		this.nbr86 = Functions.subString(nbr86, Len.NBR86);
	}

	public String getNbr86() {
		return this.nbr86;
	}

	public void setNbr87(String nbr87) {
		this.nbr87 = Functions.subString(nbr87, Len.NBR87);
	}

	public String getNbr87() {
		return this.nbr87;
	}

	public void setNbr88(String nbr88) {
		this.nbr88 = Functions.subString(nbr88, Len.NBR88);
	}

	public String getNbr88() {
		return this.nbr88;
	}

	public void setNbr89(String nbr89) {
		this.nbr89 = Functions.subString(nbr89, Len.NBR89);
	}

	public String getNbr89() {
		return this.nbr89;
	}

	public void setNbr90(String nbr90) {
		this.nbr90 = Functions.subString(nbr90, Len.NBR90);
	}

	public String getNbr90() {
		return this.nbr90;
	}

	public void setNbr91(String nbr91) {
		this.nbr91 = Functions.subString(nbr91, Len.NBR91);
	}

	public String getNbr91() {
		return this.nbr91;
	}

	public void setNbr92(String nbr92) {
		this.nbr92 = Functions.subString(nbr92, Len.NBR92);
	}

	public String getNbr92() {
		return this.nbr92;
	}

	public void setNbr93(String nbr93) {
		this.nbr93 = Functions.subString(nbr93, Len.NBR93);
	}

	public String getNbr93() {
		return this.nbr93;
	}

	public void setNbr94(String nbr94) {
		this.nbr94 = Functions.subString(nbr94, Len.NBR94);
	}

	public String getNbr94() {
		return this.nbr94;
	}

	public void setNbr95(String nbr95) {
		this.nbr95 = Functions.subString(nbr95, Len.NBR95);
	}

	public String getNbr95() {
		return this.nbr95;
	}

	public void setNbr96(String nbr96) {
		this.nbr96 = Functions.subString(nbr96, Len.NBR96);
	}

	public String getNbr96() {
		return this.nbr96;
	}

	public void setNbr97(String nbr97) {
		this.nbr97 = Functions.subString(nbr97, Len.NBR97);
	}

	public String getNbr97() {
		return this.nbr97;
	}

	public void setNbr98(String nbr98) {
		this.nbr98 = Functions.subString(nbr98, Len.NBR98);
	}

	public String getNbr98() {
		return this.nbr98;
	}

	public void setNbr99(String nbr99) {
		this.nbr99 = Functions.subString(nbr99, Len.NBR99);
	}

	public String getNbr99() {
		return this.nbr99;
	}

	public void setNbr100(String nbr100) {
		this.nbr100 = Functions.subString(nbr100, Len.NBR100);
	}

	public String getNbr100() {
		return this.nbr100;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NBR1 = 25;
		public static final int NBR2 = 25;
		public static final int NBR3 = 25;
		public static final int NBR4 = 25;
		public static final int NBR5 = 25;
		public static final int NBR6 = 25;
		public static final int NBR7 = 25;
		public static final int NBR8 = 25;
		public static final int NBR9 = 25;
		public static final int NBR10 = 25;
		public static final int NBR11 = 25;
		public static final int NBR12 = 25;
		public static final int NBR13 = 25;
		public static final int NBR14 = 25;
		public static final int NBR15 = 25;
		public static final int NBR16 = 25;
		public static final int NBR17 = 25;
		public static final int NBR18 = 25;
		public static final int NBR19 = 25;
		public static final int NBR20 = 25;
		public static final int NBR21 = 25;
		public static final int NBR22 = 25;
		public static final int NBR23 = 25;
		public static final int NBR24 = 25;
		public static final int NBR25 = 25;
		public static final int NBR26 = 25;
		public static final int NBR27 = 25;
		public static final int NBR28 = 25;
		public static final int NBR29 = 25;
		public static final int NBR30 = 25;
		public static final int NBR31 = 25;
		public static final int NBR32 = 25;
		public static final int NBR33 = 25;
		public static final int NBR34 = 25;
		public static final int NBR35 = 25;
		public static final int NBR36 = 25;
		public static final int NBR37 = 25;
		public static final int NBR38 = 25;
		public static final int NBR39 = 25;
		public static final int NBR40 = 25;
		public static final int NBR41 = 25;
		public static final int NBR42 = 25;
		public static final int NBR43 = 25;
		public static final int NBR44 = 25;
		public static final int NBR45 = 25;
		public static final int NBR46 = 25;
		public static final int NBR47 = 25;
		public static final int NBR48 = 25;
		public static final int NBR49 = 25;
		public static final int NBR50 = 25;
		public static final int NBR51 = 25;
		public static final int NBR52 = 25;
		public static final int NBR53 = 25;
		public static final int NBR54 = 25;
		public static final int NBR55 = 25;
		public static final int NBR56 = 25;
		public static final int NBR57 = 25;
		public static final int NBR58 = 25;
		public static final int NBR59 = 25;
		public static final int NBR60 = 25;
		public static final int NBR61 = 25;
		public static final int NBR62 = 25;
		public static final int NBR63 = 25;
		public static final int NBR64 = 25;
		public static final int NBR65 = 25;
		public static final int NBR66 = 25;
		public static final int NBR67 = 25;
		public static final int NBR68 = 25;
		public static final int NBR69 = 25;
		public static final int NBR70 = 25;
		public static final int NBR71 = 25;
		public static final int NBR72 = 25;
		public static final int NBR73 = 25;
		public static final int NBR74 = 25;
		public static final int NBR75 = 25;
		public static final int NBR76 = 25;
		public static final int NBR77 = 25;
		public static final int NBR78 = 25;
		public static final int NBR79 = 25;
		public static final int NBR80 = 25;
		public static final int NBR81 = 25;
		public static final int NBR82 = 25;
		public static final int NBR83 = 25;
		public static final int NBR84 = 25;
		public static final int NBR85 = 25;
		public static final int NBR86 = 25;
		public static final int NBR87 = 25;
		public static final int NBR88 = 25;
		public static final int NBR89 = 25;
		public static final int NBR90 = 25;
		public static final int NBR91 = 25;
		public static final int NBR92 = 25;
		public static final int NBR93 = 25;
		public static final int NBR94 = 25;
		public static final int NBR95 = 25;
		public static final int NBR96 = 25;
		public static final int NBR97 = 25;
		public static final int NBR98 = 25;
		public static final int NBR99 = 25;
		public static final int NBR100 = 25;
		public static final int POL_LIST = NBR1 + NBR2 + NBR3 + NBR4 + NBR5 + NBR6 + NBR7 + NBR8 + NBR9 + NBR10 + NBR11 + NBR12 + NBR13 + NBR14
				+ NBR15 + NBR16 + NBR17 + NBR18 + NBR19 + NBR20 + NBR21 + NBR22 + NBR23 + NBR24 + NBR25 + NBR26 + NBR27 + NBR28 + NBR29 + NBR30
				+ NBR31 + NBR32 + NBR33 + NBR34 + NBR35 + NBR36 + NBR37 + NBR38 + NBR39 + NBR40 + NBR41 + NBR42 + NBR43 + NBR44 + NBR45 + NBR46
				+ NBR47 + NBR48 + NBR49 + NBR50 + NBR51 + NBR52 + NBR53 + NBR54 + NBR55 + NBR56 + NBR57 + NBR58 + NBR59 + NBR60 + NBR61 + NBR62
				+ NBR63 + NBR64 + NBR65 + NBR66 + NBR67 + NBR68 + NBR69 + NBR70 + NBR71 + NBR72 + NBR73 + NBR74 + NBR75 + NBR76 + NBR77 + NBR78
				+ NBR79 + NBR80 + NBR81 + NBR82 + NBR83 + NBR84 + NBR85 + NBR86 + NBR87 + NBR88 + NBR89 + NBR90 + NBR91 + NBR92 + NBR93 + NBR94
				+ NBR95 + NBR96 + NBR97 + NBR98 + NBR99 + NBR100;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
