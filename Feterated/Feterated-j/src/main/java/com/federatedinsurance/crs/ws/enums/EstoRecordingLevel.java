/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ESTO-RECORDING-LEVEL<br>
 * Variable: ESTO-RECORDING-LEVEL from copybook HALLESTO<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class EstoRecordingLevel {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.RECORDING_LEVEL);
	public static final String MAIN_DRVR_LEVEL = "HAL_ERR_LOG_MDRV_V";
	public static final String MSG_CNTL_LEVEL = "HAL_ERR_LOG_MCM_V";
	public static final String FAILURE_LEVEL = "HAL_ERR_LOG_FAIL_V";

	//==== METHODS ====
	public void setRecordingLevel(String recordingLevel) {
		this.value = Functions.subString(recordingLevel, Len.RECORDING_LEVEL);
	}

	public String getRecordingLevel() {
		return this.value;
	}

	public boolean isEstoFailureLevel() {
		return value.equals(FAILURE_LEVEL);
	}

	public void setFailureLevel() {
		value = FAILURE_LEVEL;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RECORDING_LEVEL = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
