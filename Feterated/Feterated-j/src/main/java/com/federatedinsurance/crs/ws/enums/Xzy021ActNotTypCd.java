/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZY021-ACT-NOT-TYP-CD<br>
 * Variable: XZY021-ACT-NOT-TYP-CD from copybook XZ0Y0021<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xzy021ActNotTypCd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	public static final String IMP_TMN_TYP = "IMP";
	public static final String UW_TMN_REQ_TYP = "TRU";
	public static final String INSD_TMN_REQ_TYP = "TRI";
	public static final String NON_PAY_CNC = "NPC";
	public static final String INSD_CNC = "CNI";
	public static final String UW_CNC = "CNU";
	private static final String[] VLD_TMN_REQ_TYP = new String[] { "IMP", "TRU" };

	//==== METHODS ====
	public void setActNotTypCd(String actNotTypCd) {
		this.value = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_TYP_CD = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
