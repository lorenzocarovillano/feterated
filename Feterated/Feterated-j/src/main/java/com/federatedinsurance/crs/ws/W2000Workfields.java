/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: W2000-WORKFIELDS<br>
 * Variable: W2000-WORKFIELDS from program HALOUIDG<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class W2000Workfields {

	//==== PROPERTIES ====
	//Original name: W2000-INPUT-BASE10
	private String inputBase10 = DefaultValues.stringVal(Len.INPUT_BASE10);
	/**Original name: W2000-INDEX<br>
	 * <pre>*     05     W2000-INPUT-BASE10  PIC  9(12).</pre>*/
	private short index2 = DefaultValues.SHORT_VAL;
	//Original name: W2000-OUTPUT-BASE36
	private String outputBase36 = DefaultValues.stringVal(Len.OUTPUT_BASE36);
	//Original name: W2000-REMAINDER
	private long remainder = DefaultValues.LONG_VAL;
	//Original name: W2000-QUOTIENT
	private long quotient = DefaultValues.LONG_VAL;
	/**Original name: W2000-REF-DIGITS<br>
	 * <pre>*   03       W2000-QUOTIENT      PIC  9(12)    PACKED-DECIMAL.</pre>*/
	private String refDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	//==== METHODS ====
	public void setInputBase10Formatted(String inputBase10) {
		this.inputBase10 = Trunc.toUnsignedNumeric(inputBase10, Len.INPUT_BASE10);
	}

	public long getInputBase10() {
		return NumericDisplay.asLong(this.inputBase10);
	}

	public void setIndex2(short index2) {
		this.index2 = index2;
	}

	public short getIndex2() {
		return this.index2;
	}

	public void setOutputBase36(String outputBase36) {
		this.outputBase36 = Functions.subString(outputBase36, Len.OUTPUT_BASE36);
	}

	public String getOutputBase36() {
		return this.outputBase36;
	}

	public String getOutputBase36Formatted() {
		return Functions.padBlanks(getOutputBase36(), Len.OUTPUT_BASE36);
	}

	public void setRemainder(long remainder) {
		this.remainder = remainder;
	}

	public long getRemainder() {
		return this.remainder;
	}

	public void setQuotient(long quotient) {
		this.quotient = quotient;
	}

	public long getQuotient() {
		return this.quotient;
	}

	public String getRefDigits() {
		return this.refDigits;
	}

	public String getRefDigitsFormatted() {
		return Functions.padBlanks(getRefDigits(), Len.REF_DIGITS);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int INPUT_BASE10 = 16;
		public static final int INDEX2 = 2;
		public static final int OUTPUT_BASE36 = 10;
		public static final int REF_DIGITS = 36;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
