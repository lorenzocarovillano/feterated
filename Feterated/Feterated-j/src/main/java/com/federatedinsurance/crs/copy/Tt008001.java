/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.LSearchCriteria;
import com.federatedinsurance.crs.ws.enums.LOdErrInd;

/**Original name: TT008001<br>
 * Variable: TT008001 from copybook TT008001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Tt008001 {

	//==== PROPERTIES ====
	//Original name: L-SEARCH-CRITERIA
	private LSearchCriteria searchCriteria = new LSearchCriteria();
	//Original name: L-OD-PPL-DATA
	private LOdPplData odPplData = new LOdPplData();
	//Original name: L-OD-ERR-IND
	private LOdErrInd odErrInd = new LOdErrInd();
	//Original name: L-OD-MSG
	private String odMsg = DefaultValues.stringVal(Len.OD_MSG);

	//==== METHODS ====
	public String getOdMsg() {
		return this.odMsg;
	}

	public String getOdMsgFormatted() {
		return Functions.padBlanks(getOdMsg(), Len.OD_MSG);
	}

	public LOdErrInd getOdErrInd() {
		return odErrInd;
	}

	public LOdPplData getOdPplData() {
		return odPplData;
	}

	public LSearchCriteria getSearchCriteria() {
		return searchCriteria;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OD_MSG = 132;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
