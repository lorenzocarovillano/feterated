/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.Ts020tbl;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program MU0Q0004<br>
 * Generated as a class for rule WS.<br>*/
public class Mu0q0004Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsMu0q0004 constantFields = new ConstantFieldsMu0q0004();
	//Original name: WS-MISC-WORK-FLDS
	private WsMiscWorkFldsMu0q0004 wsMiscWorkFlds = new WsMiscWorkFldsMu0q0004();
	//Original name: WS-FILTER-COPYBOOKS
	private WsFilterCopybooks wsFilterCopybooks = new WsFilterCopybooks();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: HALLUSW
	private Hallusw hallusw = new Hallusw();
	//Original name: TS020TBL
	private Ts020tbl ts020tbl = new Ts020tbl();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	/**Original name: WS-USW-MSG<br>*/
	public byte[] getWsUswMsgBytes() {
		byte[] buffer = new byte[Len.WS_USW_MSG];
		return getWsUswMsgBytes(buffer, 1);
	}

	public byte[] getWsUswMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		hallusw.getCommonBytes(buffer, position);
		return buffer;
	}

	public String getTableFormatterDataFormatted() {
		return ts020tbl.getTableFormatterParmsFormatted();
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public ConstantFieldsMu0q0004 getConstantFields() {
		return constantFields;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	public Ts020tbl getTs020tbl() {
		return ts020tbl;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsFilterCopybooks getWsFilterCopybooks() {
		return wsFilterCopybooks;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsMiscWorkFldsMu0q0004 getWsMiscWorkFlds() {
		return wsMiscWorkFlds;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_USW_MSG = Hallusw.Len.COMMON;
		public static final int TABLE_FORMATTER_DATA = Ts020tbl.Len.TABLE_FORMATTER_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
