/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: XZT005-BYPASS-SYNCPOINT-IND<br>
 * Variable: XZT005-BYPASS-SYNCPOINT-IND from copybook XZ0Z0005<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xzt005BypassSyncpointInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char BYPASS_SYNCPOINT = 'Y';
	public static final char DO_NOT_BYPASS_SYNCPOINT = 'N';

	//==== METHODS ====
	public void setXzt005BypassSyncpointInd(char xzt005BypassSyncpointInd) {
		this.value = xzt005BypassSyncpointInd;
	}

	public char getXzt005BypassSyncpointInd() {
		return this.value;
	}

	public boolean isBypassSyncpoint() {
		return value == BYPASS_SYNCPOINT;
	}

	public void setBypassSyncpoint() {
		value = BYPASS_SYNCPOINT;
	}

	public void setXzt005DoNotBypassSyncpoint() {
		value = DO_NOT_BYPASS_SYNCPOINT;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT005_BYPASS_SYNCPOINT_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
