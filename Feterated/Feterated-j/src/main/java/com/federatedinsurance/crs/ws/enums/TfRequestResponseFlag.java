/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TF-REQUEST-RESPONSE-FLAG<br>
 * Variable: TF-REQUEST-RESPONSE-FLAG from copybook TS020TBL<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TfRequestResponseFlag {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.TF_REQUEST_RESPONSE_FLAG);
	public static final String REQUEST_FORMATTER_CALL = "REQUEST";
	public static final String RESPONSE_FORMATTER_CALL = "RESPONSE";

	//==== METHODS ====
	public void setTfRequestResponseFlag(String tfRequestResponseFlag) {
		this.value = Functions.subString(tfRequestResponseFlag, Len.TF_REQUEST_RESPONSE_FLAG);
	}

	public String getTfRequestResponseFlag() {
		return this.value;
	}

	public boolean isRequestFormatterCall() {
		return value.equals(REQUEST_FORMATTER_CALL);
	}

	public void setTfRequestFormatterCall() {
		value = REQUEST_FORMATTER_CALL;
	}

	public boolean isResponseFormatterCall() {
		return value.equals(RESPONSE_FORMATTER_CALL);
	}

	public void setTfResponseFormatterCall() {
		value = RESPONSE_FORMATTER_CALL;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TF_REQUEST_RESPONSE_FLAG = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
