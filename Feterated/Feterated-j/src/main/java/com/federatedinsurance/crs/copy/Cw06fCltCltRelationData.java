/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW06F-CLT-CLT-RELATION-DATA<br>
 * Variable: CW06F-CLT-CLT-RELATION-DATA from copybook CAWLF006<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw06fCltCltRelationData {

	//==== PROPERTIES ====
	/**Original name: CW06F-CICR-EXP-DT-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char cicrExpDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-CICR-EXP-DT
	private String cicrExpDt = DefaultValues.stringVal(Len.CICR_EXP_DT);
	//Original name: CW06F-CICR-NBR-OPR-STRS-CI
	private char cicrNbrOprStrsCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-CICR-NBR-OPR-STRS-NI
	private char cicrNbrOprStrsNi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-CICR-NBR-OPR-STRS
	private String cicrNbrOprStrs = DefaultValues.stringVal(Len.CICR_NBR_OPR_STRS);
	//Original name: CW06F-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW06F-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW06F-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW06F-CICR-EFF-ACY-TS-CI
	private char cicrEffAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-CICR-EFF-ACY-TS
	private String cicrEffAcyTs = DefaultValues.stringVal(Len.CICR_EFF_ACY_TS);
	//Original name: CW06F-CICR-EXP-ACY-TS-CI
	private char cicrExpAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-CICR-EXP-ACY-TS
	private String cicrExpAcyTs = DefaultValues.stringVal(Len.CICR_EXP_ACY_TS);

	//==== METHODS ====
	public void setCltCltRelationDataBytes(byte[] buffer, int offset) {
		int position = offset;
		cicrExpDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicrExpDt = MarshalByte.readString(buffer, position, Len.CICR_EXP_DT);
		position += Len.CICR_EXP_DT;
		cicrNbrOprStrsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicrNbrOprStrsNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicrNbrOprStrs = MarshalByte.readString(buffer, position, Len.CICR_NBR_OPR_STRS);
		position += Len.CICR_NBR_OPR_STRS;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		cicrEffAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicrEffAcyTs = MarshalByte.readString(buffer, position, Len.CICR_EFF_ACY_TS);
		position += Len.CICR_EFF_ACY_TS;
		cicrExpAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicrExpAcyTs = MarshalByte.readString(buffer, position, Len.CICR_EXP_ACY_TS);
	}

	public byte[] getCltCltRelationDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, cicrExpDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicrExpDt, Len.CICR_EXP_DT);
		position += Len.CICR_EXP_DT;
		MarshalByte.writeChar(buffer, position, cicrNbrOprStrsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cicrNbrOprStrsNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicrNbrOprStrs, Len.CICR_NBR_OPR_STRS);
		position += Len.CICR_NBR_OPR_STRS;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, cicrEffAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicrEffAcyTs, Len.CICR_EFF_ACY_TS);
		position += Len.CICR_EFF_ACY_TS;
		MarshalByte.writeChar(buffer, position, cicrExpAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicrExpAcyTs, Len.CICR_EXP_ACY_TS);
		return buffer;
	}

	public void setCicrExpDtCi(char cicrExpDtCi) {
		this.cicrExpDtCi = cicrExpDtCi;
	}

	public char getCicrExpDtCi() {
		return this.cicrExpDtCi;
	}

	public void setCicrExpDt(String cicrExpDt) {
		this.cicrExpDt = Functions.subString(cicrExpDt, Len.CICR_EXP_DT);
	}

	public String getCicrExpDt() {
		return this.cicrExpDt;
	}

	public void setCicrNbrOprStrsCi(char cicrNbrOprStrsCi) {
		this.cicrNbrOprStrsCi = cicrNbrOprStrsCi;
	}

	public char getCicrNbrOprStrsCi() {
		return this.cicrNbrOprStrsCi;
	}

	public void setCicrNbrOprStrsNi(char cicrNbrOprStrsNi) {
		this.cicrNbrOprStrsNi = cicrNbrOprStrsNi;
	}

	public char getCicrNbrOprStrsNi() {
		return this.cicrNbrOprStrsNi;
	}

	public void setCicrNbrOprStrs(String cicrNbrOprStrs) {
		this.cicrNbrOprStrs = Functions.subString(cicrNbrOprStrs, Len.CICR_NBR_OPR_STRS);
	}

	public String getCicrNbrOprStrs() {
		return this.cicrNbrOprStrs;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setCicrEffAcyTsCi(char cicrEffAcyTsCi) {
		this.cicrEffAcyTsCi = cicrEffAcyTsCi;
	}

	public char getCicrEffAcyTsCi() {
		return this.cicrEffAcyTsCi;
	}

	public void setCicrEffAcyTs(String cicrEffAcyTs) {
		this.cicrEffAcyTs = Functions.subString(cicrEffAcyTs, Len.CICR_EFF_ACY_TS);
	}

	public String getCicrEffAcyTs() {
		return this.cicrEffAcyTs;
	}

	public void setCicrExpAcyTsCi(char cicrExpAcyTsCi) {
		this.cicrExpAcyTsCi = cicrExpAcyTsCi;
	}

	public char getCicrExpAcyTsCi() {
		return this.cicrExpAcyTsCi;
	}

	public void setCicrExpAcyTs(String cicrExpAcyTs) {
		this.cicrExpAcyTs = Functions.subString(cicrExpAcyTs, Len.CICR_EXP_ACY_TS);
	}

	public String getCicrExpAcyTs() {
		return this.cicrExpAcyTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICR_EXP_DT = 10;
		public static final int CICR_NBR_OPR_STRS = 3;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CICR_EFF_ACY_TS = 26;
		public static final int CICR_EXP_ACY_TS = 26;
		public static final int CICR_EXP_DT_CI = 1;
		public static final int CICR_NBR_OPR_STRS_CI = 1;
		public static final int CICR_NBR_OPR_STRS_NI = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int CICR_EFF_ACY_TS_CI = 1;
		public static final int CICR_EXP_ACY_TS_CI = 1;
		public static final int CLT_CLT_RELATION_DATA = CICR_EXP_DT_CI + CICR_EXP_DT + CICR_NBR_OPR_STRS_CI + CICR_NBR_OPR_STRS_NI + CICR_NBR_OPR_STRS
				+ USER_ID_CI + USER_ID + STATUS_CD_CI + STATUS_CD + TERMINAL_ID_CI + TERMINAL_ID + CICR_EFF_ACY_TS_CI + CICR_EFF_ACY_TS
				+ CICR_EXP_ACY_TS_CI + CICR_EXP_ACY_TS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
