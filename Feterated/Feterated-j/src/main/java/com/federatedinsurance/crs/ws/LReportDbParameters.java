/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.LRdActionIndicator;
import com.federatedinsurance.crs.ws.enums.LRdStatus;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-REPORT-DB-PARAMETERS<br>
 * Variable: L-REPORT-DB-PARAMETERS from program TS030199<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LReportDbParameters extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: L-RD-ACTION-INDICATOR
	private LRdActionIndicator actionIndicator = new LRdActionIndicator();
	//Original name: L-RD-REPORT-NBR
	private String reportNbr = DefaultValues.stringVal(Len.REPORT_NBR);
	//Original name: L-RD-STATUS
	private LRdStatus status = new LRdStatus();
	//Original name: L-RD-TABLE-DATA
	private String tableData = DefaultValues.stringVal(Len.TABLE_DATA);
	//Original name: L-RD-EM-MESSAGE1
	private String emMessage1 = DefaultValues.stringVal(Len.EM_MESSAGE1);
	//Original name: L-RD-EM-MESSAGE2
	private String emMessage2 = DefaultValues.stringVal(Len.EM_MESSAGE2);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_REPORT_DB_PARAMETERS;
	}

	@Override
	public void deserialize(byte[] buf) {
		setlReportDbParametersBytes(buf);
	}

	public String getReportDbParametersFormatted() {
		return MarshalByteExt.bufferToStr(getlReportDbParametersBytes());
	}

	public void setlReportDbParametersBytes(byte[] buffer) {
		setlReportDbParametersBytes(buffer, 1);
	}

	public byte[] getlReportDbParametersBytes() {
		byte[] buffer = new byte[Len.L_REPORT_DB_PARAMETERS];
		return getlReportDbParametersBytes(buffer, 1);
	}

	public void setlReportDbParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		actionIndicator.setActionIndicator(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		reportNbr = MarshalByte.readString(buffer, position, Len.REPORT_NBR);
		position += Len.REPORT_NBR;
		status.setStatus(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		tableData = MarshalByte.readString(buffer, position, Len.TABLE_DATA);
		position += Len.TABLE_DATA;
		setErrorMessagesBytes(buffer, position);
	}

	public byte[] getlReportDbParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, actionIndicator.getActionIndicator());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, reportNbr, Len.REPORT_NBR);
		position += Len.REPORT_NBR;
		MarshalByte.writeChar(buffer, position, status.getStatus());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tableData, Len.TABLE_DATA);
		position += Len.TABLE_DATA;
		getErrorMessagesBytes(buffer, position);
		return buffer;
	}

	public void setReportNbr(String reportNbr) {
		this.reportNbr = Functions.subString(reportNbr, Len.REPORT_NBR);
	}

	public String getReportNbr() {
		return this.reportNbr;
	}

	public void setTableData(String tableData) {
		this.tableData = Functions.subString(tableData, Len.TABLE_DATA);
	}

	public String getTableData() {
		return this.tableData;
	}

	public String getTableDataFormatted() {
		return Functions.padBlanks(getTableData(), Len.TABLE_DATA);
	}

	public void setErrorMessagesBytes(byte[] buffer, int offset) {
		int position = offset;
		emMessage1 = MarshalByte.readString(buffer, position, Len.EM_MESSAGE1);
		position += Len.EM_MESSAGE1;
		emMessage2 = MarshalByte.readString(buffer, position, Len.EM_MESSAGE2);
	}

	public byte[] getErrorMessagesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, emMessage1, Len.EM_MESSAGE1);
		position += Len.EM_MESSAGE1;
		MarshalByte.writeString(buffer, position, emMessage2, Len.EM_MESSAGE2);
		return buffer;
	}

	public void initErrorMessagesSpaces() {
		emMessage1 = "";
		emMessage2 = "";
	}

	public void setEmMessage1(String emMessage1) {
		this.emMessage1 = Functions.subString(emMessage1, Len.EM_MESSAGE1);
	}

	public String getEmMessage1() {
		return this.emMessage1;
	}

	public void setEmMessage2(String emMessage2) {
		this.emMessage2 = Functions.subString(emMessage2, Len.EM_MESSAGE2);
	}

	public String getEmMessage2() {
		return this.emMessage2;
	}

	public LRdActionIndicator getActionIndicator() {
		return actionIndicator;
	}

	public LRdStatus getStatus() {
		return status;
	}

	@Override
	public byte[] serialize() {
		return getlReportDbParametersBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REPORT_NBR = 6;
		public static final int TABLE_DATA = 200;
		public static final int EM_MESSAGE1 = 132;
		public static final int EM_MESSAGE2 = 132;
		public static final int ERROR_MESSAGES = EM_MESSAGE1 + EM_MESSAGE2;
		public static final int L_REPORT_DB_PARAMETERS = LRdActionIndicator.Len.ACTION_INDICATOR + REPORT_NBR + LRdStatus.Len.STATUS + TABLE_DATA
				+ ERROR_MESSAGES;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
