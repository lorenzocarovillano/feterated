/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-NEW-INHOUSE-REPT-NMBR-FLAG<br>
 * Variable: SW-NEW-INHOUSE-REPT-NMBR-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwNewInhouseReptNmbrFlag {

	//==== PROPERTIES ====
	private char value = '0';
	public static final char SAME_INHOUSE_REPT_NMBR = '0';
	public static final char NEW_INHOUSE_REPT_NMBR = '1';

	//==== METHODS ====
	public void setNewInhouseReptNmbrFlag(char newInhouseReptNmbrFlag) {
		this.value = newInhouseReptNmbrFlag;
	}

	public char getNewInhouseReptNmbrFlag() {
		return this.value;
	}

	public void setSameInhouseReptNmbr() {
		value = SAME_INHOUSE_REPT_NMBR;
	}

	public boolean isNewInhouseReptNmbr() {
		return value == NEW_INHOUSE_REPT_NMBR;
	}

	public void setNewInhouseReptNmbr() {
		value = NEW_INHOUSE_REPT_NMBR;
	}
}
