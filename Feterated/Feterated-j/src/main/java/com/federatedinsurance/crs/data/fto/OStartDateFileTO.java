/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;

/**Original name: O-START-DATE-FILE<br>
 * File: O-START-DATE-FILE from program XZ004000<br>
 * Generated as a class for rule FTO.<br>*/
public class OStartDateFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: SDO-TIMESTAMP
	private String sdoTimestamp = DefaultValues.stringVal(Len.SDO_TIMESTAMP);
	//Original name: FILLER-O-START-DATE-RECORD
	private String flr1 = "";

	//==== METHODS ====
	public void setoStartDateRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		sdoTimestamp = MarshalByte.readString(buffer, position, Len.SDO_TIMESTAMP);
		position += Len.SDO_TIMESTAMP;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getoStartDateRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, sdoTimestamp, Len.SDO_TIMESTAMP);
		position += Len.SDO_TIMESTAMP;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setSdoTimestamp(String sdoTimestamp) {
		this.sdoTimestamp = Functions.subString(sdoTimestamp, Len.SDO_TIMESTAMP);
	}

	public String getSdoTimestamp() {
		return this.sdoTimestamp;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoStartDateRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoStartDateRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_START_DATE_RECORD;
	}

	@Override
	public IBuffer copy() {
		OStartDateFileTO copyTO = new OStartDateFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SDO_TIMESTAMP = 26;
		public static final int FLR1 = 54;
		public static final int O_START_DATE_RECORD = SDO_TIMESTAMP + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
