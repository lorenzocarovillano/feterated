/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesTs547099 {

	//==== PROPERTIES ====
	//Original name: EA-01-PARM-NOT-FOUND
	private Ea01ParmNotFoundTs547099 ea01ParmNotFound = new Ea01ParmNotFoundTs547099();
	//Original name: EA-02-FUNCTION-INVALID
	private Ea02FunctionInvalidTs547099 ea02FunctionInvalid = new Ea02FunctionInvalidTs547099();
	//Original name: EA-04-DATA-LENGTH-INVALID
	private Ea04DataLengthInvalid ea04DataLengthInvalid = new Ea04DataLengthInvalid();
	//Original name: EA-05-NO-DATA-TO-PASS
	private Ea05NoDataToPass ea05NoDataToPass = new Ea05NoDataToPass();
	//Original name: EA-10-TOO-MANY-PIPES-OPEN
	private Ea10TooManyPipesOpen ea10TooManyPipesOpen = new Ea10TooManyPipesOpen();
	//Original name: EA-11-REGION-NOT-OPEN
	private Ea11RegionNotOpen ea11RegionNotOpen = new Ea11RegionNotOpen();
	//Original name: EA-12-TOO-MANY-PIPES
	private Ea12TooManyPipes ea12TooManyPipes = new Ea12TooManyPipes();
	//Original name: EA-13-PIPE-ALREADY-CLOSED
	private Ea13PipeAlreadyClosed ea13PipeAlreadyClosed = new Ea13PipeAlreadyClosed();
	//Original name: EA-14-REGION-SWAPPED
	private Ea14RegionSwapped ea14RegionSwapped = new Ea14RegionSwapped();
	//Original name: EA-15-DPL-RESPONSE-GIVEN
	private Ea15DplResponseGiven ea15DplResponseGiven = new Ea15DplResponseGiven();
	//Original name: EA-17-FATAL-DPL-RESP-FOUND
	private Ea17FatalDplRespFound ea17FatalDplRespFound = new Ea17FatalDplRespFound();
	//Original name: EA-18-PIPE-IS-UNUSABLE
	private Ea18PipeIsUnusable ea18PipeIsUnusable = new Ea18PipeIsUnusable();
	//Original name: EA-20-GEN-WARNING-MSG
	private Ea20GenWarningMsgTs547099 ea20GenWarningMsg = new Ea20GenWarningMsgTs547099();
	//Original name: EA-40-GEN-RETRYABLE-MSG
	private Ea40GenRetryableMsgTs547099 ea40GenRetryableMsg = new Ea40GenRetryableMsgTs547099();
	//Original name: EA-41-RTY-NO-CONNECTION-MSG
	private Ea41RtyNoConnectionMsg ea41RtyNoConnectionMsg = new Ea41RtyNoConnectionMsg();
	//Original name: EA-60-GEN-USER-ERROR-MSG
	private Ea60GenUserErrorMsgTs547099 ea60GenUserErrorMsg = new Ea60GenUserErrorMsgTs547099();
	//Original name: EA-61-USR-INVALID-USR-TKN-MSG
	private Ea61UsrInvalidUsrTknMsg ea61UsrInvalidUsrTknMsg = new Ea61UsrInvalidUsrTknMsg();
	//Original name: EA-62-USR-PIPE-NOT-OPEN-MSG
	private Ea62UsrPipeNotOpenMsg ea62UsrPipeNotOpenMsg = new Ea62UsrPipeNotOpenMsg();
	//Original name: EA-63-USR-INVALID-TRAN-ID-MSG
	private Ea63UsrInvalidTranIdMsg ea63UsrInvalidTranIdMsg = new Ea63UsrInvalidTranIdMsg();
	//Original name: EA-64-USR-CICS-SERVER-ERROR
	private Ea64UsrCicsServerError ea64UsrCicsServerError = new Ea64UsrCicsServerError();
	//Original name: EA-65-USR-INVALID-PIP-TKN-MSG
	private Ea65UsrInvalidPipTknMsgTs547099 ea65UsrInvalidPipTknMsg = new Ea65UsrInvalidPipTknMsgTs547099();
	//Original name: EA-66-USR-CICS-PGM-ABENDED-MSG
	private Ea66UsrCicsPgmAbendedMsg ea66UsrCicsPgmAbendedMsg = new Ea66UsrCicsPgmAbendedMsg();
	//Original name: EA-67-USR-DFHXCOPT-LOAD-MSG
	private Ea67UsrDfhxcoptLoadMsg ea67UsrDfhxcoptLoadMsg = new Ea67UsrDfhxcoptLoadMsg();
	//Original name: EA-80-GEN-SYSTEM-ERR-MSG
	private Ea80GenSystemErrMsgTs547099 ea80GenSystemErrMsg = new Ea80GenSystemErrMsgTs547099();
	//Original name: EA-81-CICS-PROGRAM-TIMEOUT-MSG
	private Ea81CicsProgramTimeoutMsg ea81CicsProgramTimeoutMsg = new Ea81CicsProgramTimeoutMsg();

	//==== METHODS ====
	public Ea01ParmNotFoundTs547099 getEa01ParmNotFound() {
		return ea01ParmNotFound;
	}

	public Ea02FunctionInvalidTs547099 getEa02FunctionInvalid() {
		return ea02FunctionInvalid;
	}

	public Ea04DataLengthInvalid getEa04DataLengthInvalid() {
		return ea04DataLengthInvalid;
	}

	public Ea05NoDataToPass getEa05NoDataToPass() {
		return ea05NoDataToPass;
	}

	public Ea10TooManyPipesOpen getEa10TooManyPipesOpen() {
		return ea10TooManyPipesOpen;
	}

	public Ea11RegionNotOpen getEa11RegionNotOpen() {
		return ea11RegionNotOpen;
	}

	public Ea12TooManyPipes getEa12TooManyPipes() {
		return ea12TooManyPipes;
	}

	public Ea13PipeAlreadyClosed getEa13PipeAlreadyClosed() {
		return ea13PipeAlreadyClosed;
	}

	public Ea14RegionSwapped getEa14RegionSwapped() {
		return ea14RegionSwapped;
	}

	public Ea15DplResponseGiven getEa15DplResponseGiven() {
		return ea15DplResponseGiven;
	}

	public Ea17FatalDplRespFound getEa17FatalDplRespFound() {
		return ea17FatalDplRespFound;
	}

	public Ea18PipeIsUnusable getEa18PipeIsUnusable() {
		return ea18PipeIsUnusable;
	}

	public Ea20GenWarningMsgTs547099 getEa20GenWarningMsg() {
		return ea20GenWarningMsg;
	}

	public Ea40GenRetryableMsgTs547099 getEa40GenRetryableMsg() {
		return ea40GenRetryableMsg;
	}

	public Ea41RtyNoConnectionMsg getEa41RtyNoConnectionMsg() {
		return ea41RtyNoConnectionMsg;
	}

	public Ea60GenUserErrorMsgTs547099 getEa60GenUserErrorMsg() {
		return ea60GenUserErrorMsg;
	}

	public Ea61UsrInvalidUsrTknMsg getEa61UsrInvalidUsrTknMsg() {
		return ea61UsrInvalidUsrTknMsg;
	}

	public Ea62UsrPipeNotOpenMsg getEa62UsrPipeNotOpenMsg() {
		return ea62UsrPipeNotOpenMsg;
	}

	public Ea63UsrInvalidTranIdMsg getEa63UsrInvalidTranIdMsg() {
		return ea63UsrInvalidTranIdMsg;
	}

	public Ea64UsrCicsServerError getEa64UsrCicsServerError() {
		return ea64UsrCicsServerError;
	}

	public Ea65UsrInvalidPipTknMsgTs547099 getEa65UsrInvalidPipTknMsg() {
		return ea65UsrInvalidPipTknMsg;
	}

	public Ea66UsrCicsPgmAbendedMsg getEa66UsrCicsPgmAbendedMsg() {
		return ea66UsrCicsPgmAbendedMsg;
	}

	public Ea67UsrDfhxcoptLoadMsg getEa67UsrDfhxcoptLoadMsg() {
		return ea67UsrDfhxcoptLoadMsg;
	}

	public Ea80GenSystemErrMsgTs547099 getEa80GenSystemErrMsg() {
		return ea80GenSystemErrMsg;
	}

	public Ea81CicsProgramTimeoutMsg getEa81CicsProgramTimeoutMsg() {
		return ea81CicsProgramTimeoutMsg;
	}
}
