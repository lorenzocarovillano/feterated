/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-10-TOO-MANY-PIPES-OPEN<br>
 * Variable: EA-10-TOO-MANY-PIPES-OPEN from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea10TooManyPipesOpen {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-10-TOO-MANY-PIPES-OPEN
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-10-TOO-MANY-PIPES-OPEN-1
	private String flr2 = "TOO MANY OPEN";
	//Original name: FILLER-EA-10-TOO-MANY-PIPES-OPEN-2
	private String flr3 = "PIPES.  MAX";
	//Original name: FILLER-EA-10-TOO-MANY-PIPES-OPEN-3
	private String flr4 = "ALLOWED IS";
	//Original name: EA-10-MAX-PIPES
	private String ea10MaxPipes = DefaultValues.stringVal(Len.EA10_MAX_PIPES);
	//Original name: FILLER-EA-10-TOO-MANY-PIPES-OPEN-4
	private String flr5 = " PIPES.";

	//==== METHODS ====
	public String getEa10TooManyPipesOpenFormatted() {
		return MarshalByteExt.bufferToStr(getEa10TooManyPipesOpenBytes());
	}

	public byte[] getEa10TooManyPipesOpenBytes() {
		byte[] buffer = new byte[Len.EA10_TOO_MANY_PIPES_OPEN];
		return getEa10TooManyPipesOpenBytes(buffer, 1);
	}

	public byte[] getEa10TooManyPipesOpenBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, ea10MaxPipes, Len.EA10_MAX_PIPES);
		position += Len.EA10_MAX_PIPES;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setEa10MaxPipes(long ea10MaxPipes) {
		this.ea10MaxPipes = PicFormatter.display("Z(2)9").format(ea10MaxPipes).toString();
	}

	public long getEa10MaxPipes() {
		return PicParser.display("Z(2)9").parseLong(this.ea10MaxPipes);
	}

	public String getFlr5() {
		return this.flr5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA10_MAX_PIPES = 3;
		public static final int FLR1 = 11;
		public static final int FLR2 = 14;
		public static final int FLR3 = 12;
		public static final int FLR5 = 7;
		public static final int EA10_TOO_MANY_PIPES_OPEN = EA10_MAX_PIPES + 2 * FLR1 + FLR2 + FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
