/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90Q0-ROW<br>
 * Variable: WS-XZ0A90Q0-ROW from program XZ0B90Q0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90q0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9Q0-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA9Q0-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);
	//Original name: XZA9Q0-ACT-OWN-CLT-ID
	private String actOwnCltId = DefaultValues.stringVal(Len.ACT_OWN_CLT_ID);
	//Original name: XZA9Q0-ACT-OWN-ADR-ID
	private String actOwnAdrId = DefaultValues.stringVal(Len.ACT_OWN_ADR_ID);
	//Original name: XZA9Q0-SEG-CD
	private String segCd = DefaultValues.stringVal(Len.SEG_CD);
	//Original name: XZA9Q0-ACT-TYP-CD
	private String actTypCd = DefaultValues.stringVal(Len.ACT_TYP_CD);
	//Original name: XZA9Q0-PDC-NBR
	private String pdcNbr = DefaultValues.stringVal(Len.PDC_NBR);
	//Original name: XZA9Q0-PDC-NM
	private String pdcNm = DefaultValues.stringVal(Len.PDC_NM);
	//Original name: XZA9Q0-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZA9Q0-LAST-NAME
	private String lastName = DefaultValues.stringVal(Len.LAST_NAME);
	//Original name: XZA9Q0-NM-ADR-LIN-1
	private String nmAdrLin1 = DefaultValues.stringVal(Len.NM_ADR_LIN1);
	//Original name: XZA9Q0-NM-ADR-LIN-2
	private String nmAdrLin2 = DefaultValues.stringVal(Len.NM_ADR_LIN2);
	//Original name: XZA9Q0-NM-ADR-LIN-3
	private String nmAdrLin3 = DefaultValues.stringVal(Len.NM_ADR_LIN3);
	//Original name: XZA9Q0-NM-ADR-LIN-4
	private String nmAdrLin4 = DefaultValues.stringVal(Len.NM_ADR_LIN4);
	//Original name: XZA9Q0-NM-ADR-LIN-5
	private String nmAdrLin5 = DefaultValues.stringVal(Len.NM_ADR_LIN5);
	//Original name: XZA9Q0-NM-ADR-LIN-6
	private String nmAdrLin6 = DefaultValues.stringVal(Len.NM_ADR_LIN6);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90Q0_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90q0RowBytes(buf);
	}

	public String getWsXz0a90q0RowFormatted() {
		return getInsuredInfoFormatted();
	}

	public void setWsXz0a90q0RowBytes(byte[] buffer) {
		setWsXz0a90q0RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90q0RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90Q0_ROW];
		return getWsXz0a90q0RowBytes(buffer, 1);
	}

	public void setWsXz0a90q0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setInsuredInfoBytes(buffer, position);
	}

	public byte[] getWsXz0a90q0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getInsuredInfoBytes(buffer, position);
		return buffer;
	}

	public String getInsuredInfoFormatted() {
		return MarshalByteExt.bufferToStr(getInsuredInfoBytes());
	}

	/**Original name: XZA9Q0-INSURED-INFO<br>
	 * <pre>*************************************************************
	 *  XZ0A90Q0 - SERVICE CONTRACT COPYBOOK FOR                   *
	 *             UOW : XZ_GET_NOT_INS_DTL                        *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  04MAY2009 E404KXS    NEW                          *
	 *  20163.20 13JUL2018 E404DMW    UPDATED ADDRESS AND CLIENT   *
	 *                                IDS FROM 20 TO 64            *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getInsuredInfoBytes() {
		byte[] buffer = new byte[Len.INSURED_INFO];
		return getInsuredInfoBytes(buffer, 1);
	}

	public void setInsuredInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
		position += Len.USERID;
		actOwnCltId = MarshalByte.readString(buffer, position, Len.ACT_OWN_CLT_ID);
		position += Len.ACT_OWN_CLT_ID;
		actOwnAdrId = MarshalByte.readString(buffer, position, Len.ACT_OWN_ADR_ID);
		position += Len.ACT_OWN_ADR_ID;
		segCd = MarshalByte.readString(buffer, position, Len.SEG_CD);
		position += Len.SEG_CD;
		actTypCd = MarshalByte.readString(buffer, position, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		pdcNbr = MarshalByte.readString(buffer, position, Len.PDC_NBR);
		position += Len.PDC_NBR;
		pdcNm = MarshalByte.readString(buffer, position, Len.PDC_NM);
		position += Len.PDC_NM;
		stAbb = MarshalByte.readString(buffer, position, Len.ST_ABB);
		position += Len.ST_ABB;
		lastName = MarshalByte.readString(buffer, position, Len.LAST_NAME);
		position += Len.LAST_NAME;
		nmAdrLin1 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN1);
		position += Len.NM_ADR_LIN1;
		nmAdrLin2 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN2);
		position += Len.NM_ADR_LIN2;
		nmAdrLin3 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN3);
		position += Len.NM_ADR_LIN3;
		nmAdrLin4 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN4);
		position += Len.NM_ADR_LIN4;
		nmAdrLin5 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN5);
		position += Len.NM_ADR_LIN5;
		nmAdrLin6 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN6);
	}

	public byte[] getInsuredInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position += Len.USERID;
		MarshalByte.writeString(buffer, position, actOwnCltId, Len.ACT_OWN_CLT_ID);
		position += Len.ACT_OWN_CLT_ID;
		MarshalByte.writeString(buffer, position, actOwnAdrId, Len.ACT_OWN_ADR_ID);
		position += Len.ACT_OWN_ADR_ID;
		MarshalByte.writeString(buffer, position, segCd, Len.SEG_CD);
		position += Len.SEG_CD;
		MarshalByte.writeString(buffer, position, actTypCd, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		MarshalByte.writeString(buffer, position, pdcNbr, Len.PDC_NBR);
		position += Len.PDC_NBR;
		MarshalByte.writeString(buffer, position, pdcNm, Len.PDC_NM);
		position += Len.PDC_NM;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeString(buffer, position, lastName, Len.LAST_NAME);
		position += Len.LAST_NAME;
		MarshalByte.writeString(buffer, position, nmAdrLin1, Len.NM_ADR_LIN1);
		position += Len.NM_ADR_LIN1;
		MarshalByte.writeString(buffer, position, nmAdrLin2, Len.NM_ADR_LIN2);
		position += Len.NM_ADR_LIN2;
		MarshalByte.writeString(buffer, position, nmAdrLin3, Len.NM_ADR_LIN3);
		position += Len.NM_ADR_LIN3;
		MarshalByte.writeString(buffer, position, nmAdrLin4, Len.NM_ADR_LIN4);
		position += Len.NM_ADR_LIN4;
		MarshalByte.writeString(buffer, position, nmAdrLin5, Len.NM_ADR_LIN5);
		position += Len.NM_ADR_LIN5;
		MarshalByte.writeString(buffer, position, nmAdrLin6, Len.NM_ADR_LIN6);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	public void setActOwnCltId(String actOwnCltId) {
		this.actOwnCltId = Functions.subString(actOwnCltId, Len.ACT_OWN_CLT_ID);
	}

	public String getActOwnCltId() {
		return this.actOwnCltId;
	}

	public void setActOwnAdrId(String actOwnAdrId) {
		this.actOwnAdrId = Functions.subString(actOwnAdrId, Len.ACT_OWN_ADR_ID);
	}

	public String getActOwnAdrId() {
		return this.actOwnAdrId;
	}

	public void setSegCd(String segCd) {
		this.segCd = Functions.subString(segCd, Len.SEG_CD);
	}

	public String getSegCd() {
		return this.segCd;
	}

	public void setActTypCd(String actTypCd) {
		this.actTypCd = Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	public String getActTypCd() {
		return this.actTypCd;
	}

	public void setPdcNbr(String pdcNbr) {
		this.pdcNbr = Functions.subString(pdcNbr, Len.PDC_NBR);
	}

	public String getPdcNbr() {
		return this.pdcNbr;
	}

	public void setPdcNm(String pdcNm) {
		this.pdcNm = Functions.subString(pdcNm, Len.PDC_NM);
	}

	public String getPdcNm() {
		return this.pdcNm;
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public void setLastName(String lastName) {
		this.lastName = Functions.subString(lastName, Len.LAST_NAME);
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setNmAdrLin1(String nmAdrLin1) {
		this.nmAdrLin1 = Functions.subString(nmAdrLin1, Len.NM_ADR_LIN1);
	}

	public String getNmAdrLin1() {
		return this.nmAdrLin1;
	}

	public void setNmAdrLin2(String nmAdrLin2) {
		this.nmAdrLin2 = Functions.subString(nmAdrLin2, Len.NM_ADR_LIN2);
	}

	public String getNmAdrLin2() {
		return this.nmAdrLin2;
	}

	public void setNmAdrLin3(String nmAdrLin3) {
		this.nmAdrLin3 = Functions.subString(nmAdrLin3, Len.NM_ADR_LIN3);
	}

	public String getNmAdrLin3() {
		return this.nmAdrLin3;
	}

	public void setNmAdrLin4(String nmAdrLin4) {
		this.nmAdrLin4 = Functions.subString(nmAdrLin4, Len.NM_ADR_LIN4);
	}

	public String getNmAdrLin4() {
		return this.nmAdrLin4;
	}

	public void setNmAdrLin5(String nmAdrLin5) {
		this.nmAdrLin5 = Functions.subString(nmAdrLin5, Len.NM_ADR_LIN5);
	}

	public String getNmAdrLin5() {
		return this.nmAdrLin5;
	}

	public void setNmAdrLin6(String nmAdrLin6) {
		this.nmAdrLin6 = Functions.subString(nmAdrLin6, Len.NM_ADR_LIN6);
	}

	public String getNmAdrLin6() {
		return this.nmAdrLin6;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90q0RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int USERID = 8;
		public static final int ACT_OWN_CLT_ID = 64;
		public static final int ACT_OWN_ADR_ID = 64;
		public static final int SEG_CD = 3;
		public static final int ACT_TYP_CD = 2;
		public static final int PDC_NBR = 5;
		public static final int PDC_NM = 120;
		public static final int ST_ABB = 2;
		public static final int LAST_NAME = 60;
		public static final int NM_ADR_LIN1 = 45;
		public static final int NM_ADR_LIN2 = 45;
		public static final int NM_ADR_LIN3 = 45;
		public static final int NM_ADR_LIN4 = 45;
		public static final int NM_ADR_LIN5 = 45;
		public static final int NM_ADR_LIN6 = 45;
		public static final int INSURED_INFO = CSR_ACT_NBR + USERID + ACT_OWN_CLT_ID + ACT_OWN_ADR_ID + SEG_CD + ACT_TYP_CD + PDC_NBR + PDC_NM
				+ ST_ABB + LAST_NAME + NM_ADR_LIN1 + NM_ADR_LIN2 + NM_ADR_LIN3 + NM_ADR_LIN4 + NM_ADR_LIN5 + NM_ADR_LIN6;
		public static final int WS_XZ0A90Q0_ROW = INSURED_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
