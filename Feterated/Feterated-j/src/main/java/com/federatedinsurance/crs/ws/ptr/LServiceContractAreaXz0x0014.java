/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0014<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0014 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0014() {
	}

	public LServiceContractAreaXz0x0014(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt14iTkNotPrcTs(String xzt14iTkNotPrcTs) {
		writeString(Pos.XZT14I_TK_NOT_PRC_TS, xzt14iTkNotPrcTs, Len.XZT14I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT14I-TK-NOT-PRC-TS<br>*/
	public String getXzt14iTkNotPrcTs() {
		return readString(Pos.XZT14I_TK_NOT_PRC_TS, Len.XZT14I_TK_NOT_PRC_TS);
	}

	public void setXzt14iTkFrmSeqNbr(int xzt14iTkFrmSeqNbr) {
		writeInt(Pos.XZT14I_TK_FRM_SEQ_NBR, xzt14iTkFrmSeqNbr, Len.Int.XZT14I_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT14I-TK-FRM-SEQ-NBR<br>*/
	public int getXzt14iTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT14I_TK_FRM_SEQ_NBR, Len.XZT14I_TK_FRM_SEQ_NBR);
	}

	public String getXzt14iTkFrmSeqNbrFormatted() {
		return readFixedString(Pos.XZT14I_TK_FRM_SEQ_NBR, Len.XZT14I_TK_FRM_SEQ_NBR);
	}

	public void setXzt14iTkActNotFrmCsumFormatted(String xzt14iTkActNotFrmCsum) {
		writeString(Pos.XZT14I_TK_ACT_NOT_FRM_CSUM, Trunc.toUnsignedNumeric(xzt14iTkActNotFrmCsum, Len.XZT14I_TK_ACT_NOT_FRM_CSUM),
				Len.XZT14I_TK_ACT_NOT_FRM_CSUM);
	}

	/**Original name: XZT14I-TK-ACT-NOT-FRM-CSUM<br>*/
	public int getXzt14iTkActNotFrmCsum() {
		return readNumDispUnsignedInt(Pos.XZT14I_TK_ACT_NOT_FRM_CSUM, Len.XZT14I_TK_ACT_NOT_FRM_CSUM);
	}

	public String getXzt14iTkActNotFrmCsumFormatted() {
		return readFixedString(Pos.XZT14I_TK_ACT_NOT_FRM_CSUM, Len.XZT14I_TK_ACT_NOT_FRM_CSUM);
	}

	public void setXzt14iCsrActNbr(String xzt14iCsrActNbr) {
		writeString(Pos.XZT14I_CSR_ACT_NBR, xzt14iCsrActNbr, Len.XZT14I_CSR_ACT_NBR);
	}

	/**Original name: XZT14I-CSR-ACT-NBR<br>*/
	public String getXzt14iCsrActNbr() {
		return readString(Pos.XZT14I_CSR_ACT_NBR, Len.XZT14I_CSR_ACT_NBR);
	}

	public void setXzt14iFrmNbr(String xzt14iFrmNbr) {
		writeString(Pos.XZT14I_FRM_NBR, xzt14iFrmNbr, Len.XZT14I_FRM_NBR);
	}

	/**Original name: XZT14I-FRM-NBR<br>*/
	public String getXzt14iFrmNbr() {
		return readString(Pos.XZT14I_FRM_NBR, Len.XZT14I_FRM_NBR);
	}

	public void setXzt14iFrmEdtDt(String xzt14iFrmEdtDt) {
		writeString(Pos.XZT14I_FRM_EDT_DT, xzt14iFrmEdtDt, Len.XZT14I_FRM_EDT_DT);
	}

	/**Original name: XZT14I-FRM-EDT-DT<br>*/
	public String getXzt14iFrmEdtDt() {
		return readString(Pos.XZT14I_FRM_EDT_DT, Len.XZT14I_FRM_EDT_DT);
	}

	public void setXzt14iUserid(String xzt14iUserid) {
		writeString(Pos.XZT14I_USERID, xzt14iUserid, Len.XZT14I_USERID);
	}

	/**Original name: XZT14I-USERID<br>*/
	public String getXzt14iUserid() {
		return readString(Pos.XZT14I_USERID, Len.XZT14I_USERID);
	}

	public String getXzt14iUseridFormatted() {
		return Functions.padBlanks(getXzt14iUserid(), Len.XZT14I_USERID);
	}

	public void setXzt14oTkNotPrcTs(String xzt14oTkNotPrcTs) {
		writeString(Pos.XZT14O_TK_NOT_PRC_TS, xzt14oTkNotPrcTs, Len.XZT14O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT14O-TK-NOT-PRC-TS<br>*/
	public String getXzt14oTkNotPrcTs() {
		return readString(Pos.XZT14O_TK_NOT_PRC_TS, Len.XZT14O_TK_NOT_PRC_TS);
	}

	public void setXzt14oTkFrmSeqNbr(int xzt14oTkFrmSeqNbr) {
		writeInt(Pos.XZT14O_TK_FRM_SEQ_NBR, xzt14oTkFrmSeqNbr, Len.Int.XZT14O_TK_FRM_SEQ_NBR);
	}

	public void setXzt14oTkFrmSeqNbrFormatted(String xzt14oTkFrmSeqNbr) {
		writeString(Pos.XZT14O_TK_FRM_SEQ_NBR, Trunc.toUnsignedNumeric(xzt14oTkFrmSeqNbr, Len.XZT14O_TK_FRM_SEQ_NBR), Len.XZT14O_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT14O-TK-FRM-SEQ-NBR<br>*/
	public int getXzt14oTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT14O_TK_FRM_SEQ_NBR, Len.XZT14O_TK_FRM_SEQ_NBR);
	}

	public void setXzt14oTkActNotFrmCsumFormatted(String xzt14oTkActNotFrmCsum) {
		writeString(Pos.XZT14O_TK_ACT_NOT_FRM_CSUM, Trunc.toUnsignedNumeric(xzt14oTkActNotFrmCsum, Len.XZT14O_TK_ACT_NOT_FRM_CSUM),
				Len.XZT14O_TK_ACT_NOT_FRM_CSUM);
	}

	/**Original name: XZT14O-TK-ACT-NOT-FRM-CSUM<br>*/
	public int getXzt14oTkActNotFrmCsum() {
		return readNumDispUnsignedInt(Pos.XZT14O_TK_ACT_NOT_FRM_CSUM, Len.XZT14O_TK_ACT_NOT_FRM_CSUM);
	}

	public void setXzt14oCsrActNbr(String xzt14oCsrActNbr) {
		writeString(Pos.XZT14O_CSR_ACT_NBR, xzt14oCsrActNbr, Len.XZT14O_CSR_ACT_NBR);
	}

	/**Original name: XZT14O-CSR-ACT-NBR<br>*/
	public String getXzt14oCsrActNbr() {
		return readString(Pos.XZT14O_CSR_ACT_NBR, Len.XZT14O_CSR_ACT_NBR);
	}

	public void setXzt14oFrmNbr(String xzt14oFrmNbr) {
		writeString(Pos.XZT14O_FRM_NBR, xzt14oFrmNbr, Len.XZT14O_FRM_NBR);
	}

	/**Original name: XZT14O-FRM-NBR<br>*/
	public String getXzt14oFrmNbr() {
		return readString(Pos.XZT14O_FRM_NBR, Len.XZT14O_FRM_NBR);
	}

	public void setXzt14oFrmEdtDt(String xzt14oFrmEdtDt) {
		writeString(Pos.XZT14O_FRM_EDT_DT, xzt14oFrmEdtDt, Len.XZT14O_FRM_EDT_DT);
	}

	/**Original name: XZT14O-FRM-EDT-DT<br>*/
	public String getXzt14oFrmEdtDt() {
		return readString(Pos.XZT14O_FRM_EDT_DT, Len.XZT14O_FRM_EDT_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT014_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT14I_TECHNICAL_KEY = XZT014_SERVICE_INPUTS;
		public static final int XZT14I_TK_NOT_PRC_TS = XZT14I_TECHNICAL_KEY;
		public static final int XZT14I_TK_FRM_SEQ_NBR = XZT14I_TK_NOT_PRC_TS + Len.XZT14I_TK_NOT_PRC_TS;
		public static final int XZT14I_TK_ACT_NOT_FRM_CSUM = XZT14I_TK_FRM_SEQ_NBR + Len.XZT14I_TK_FRM_SEQ_NBR;
		public static final int XZT14I_CSR_ACT_NBR = XZT14I_TK_ACT_NOT_FRM_CSUM + Len.XZT14I_TK_ACT_NOT_FRM_CSUM;
		public static final int XZT14I_FRM_NBR = XZT14I_CSR_ACT_NBR + Len.XZT14I_CSR_ACT_NBR;
		public static final int XZT14I_FRM_EDT_DT = XZT14I_FRM_NBR + Len.XZT14I_FRM_NBR;
		public static final int XZT14I_USERID = XZT14I_FRM_EDT_DT + Len.XZT14I_FRM_EDT_DT;
		public static final int XZT014_SERVICE_OUTPUTS = XZT14I_USERID + Len.XZT14I_USERID;
		public static final int XZT14O_TECHNICAL_KEY = XZT014_SERVICE_OUTPUTS;
		public static final int XZT14O_TK_NOT_PRC_TS = XZT14O_TECHNICAL_KEY;
		public static final int XZT14O_TK_FRM_SEQ_NBR = XZT14O_TK_NOT_PRC_TS + Len.XZT14O_TK_NOT_PRC_TS;
		public static final int XZT14O_TK_ACT_NOT_FRM_CSUM = XZT14O_TK_FRM_SEQ_NBR + Len.XZT14O_TK_FRM_SEQ_NBR;
		public static final int XZT14O_CSR_ACT_NBR = XZT14O_TK_ACT_NOT_FRM_CSUM + Len.XZT14O_TK_ACT_NOT_FRM_CSUM;
		public static final int XZT14O_FRM_NBR = XZT14O_CSR_ACT_NBR + Len.XZT14O_CSR_ACT_NBR;
		public static final int XZT14O_FRM_EDT_DT = XZT14O_FRM_NBR + Len.XZT14O_FRM_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT14I_TK_NOT_PRC_TS = 26;
		public static final int XZT14I_TK_FRM_SEQ_NBR = 5;
		public static final int XZT14I_TK_ACT_NOT_FRM_CSUM = 9;
		public static final int XZT14I_CSR_ACT_NBR = 9;
		public static final int XZT14I_FRM_NBR = 30;
		public static final int XZT14I_FRM_EDT_DT = 10;
		public static final int XZT14I_USERID = 8;
		public static final int XZT14O_TK_NOT_PRC_TS = 26;
		public static final int XZT14O_TK_FRM_SEQ_NBR = 5;
		public static final int XZT14O_TK_ACT_NOT_FRM_CSUM = 9;
		public static final int XZT14O_CSR_ACT_NBR = 9;
		public static final int XZT14O_FRM_NBR = 30;
		public static final int XZT14I_TECHNICAL_KEY = XZT14I_TK_NOT_PRC_TS + XZT14I_TK_FRM_SEQ_NBR + XZT14I_TK_ACT_NOT_FRM_CSUM;
		public static final int XZT014_SERVICE_INPUTS = XZT14I_TECHNICAL_KEY + XZT14I_CSR_ACT_NBR + XZT14I_FRM_NBR + XZT14I_FRM_EDT_DT
				+ XZT14I_USERID;
		public static final int XZT14O_TECHNICAL_KEY = XZT14O_TK_NOT_PRC_TS + XZT14O_TK_FRM_SEQ_NBR + XZT14O_TK_ACT_NOT_FRM_CSUM;
		public static final int XZT14O_FRM_EDT_DT = 10;
		public static final int XZT014_SERVICE_OUTPUTS = XZT14O_TECHNICAL_KEY + XZT14O_CSR_ACT_NBR + XZT14O_FRM_NBR + XZT14O_FRM_EDT_DT;
		public static final int L_SERVICE_CONTRACT_AREA = XZT014_SERVICE_INPUTS + XZT014_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT14I_TK_FRM_SEQ_NBR = 5;
			public static final int XZT14O_TK_FRM_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
