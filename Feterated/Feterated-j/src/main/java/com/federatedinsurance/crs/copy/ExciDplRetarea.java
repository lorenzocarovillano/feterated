/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: EXCI-DPL-RETAREA<br>
 * Variable: EXCI-DPL-RETAREA from copybook DFHXCPLO<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ExciDplRetarea {

	//==== PROPERTIES ====
	//Original name: EXCI-DPL-RESP
	private long resp = DefaultValues.BIN_LONG_VAL;
	//Original name: EXCI-DPL-RESP2
	private long resp2 = DefaultValues.BIN_LONG_VAL;

	//==== METHODS ====
	public long getResp() {
		return this.resp;
	}

	public long getResp2() {
		return this.resp2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ABCODE = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
