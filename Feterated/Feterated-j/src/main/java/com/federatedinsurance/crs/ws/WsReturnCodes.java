/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WS-RETURN-CODES<br>
 * Variable: WS-RETURN-CODES from program XPIODAT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsReturnCodes {

	//==== PROPERTIES ====
	//Original name: WS-DATE-OK
	private String dateOk = "OK";
	//Original name: WS-SQL-ERROR
	private String sqlError = "SQ";
	//Original name: WS-INVALID-DATE
	private String invalidDate = "ID";
	//Original name: WS-INVALID-CODE
	private String invalidCode = "IC";
	//Original name: WS-INVALID-UNSUPPORT
	private String invalidUnsupport = "UC";

	//==== METHODS ====
	public String getDateOk() {
		return this.dateOk;
	}

	public String getSqlError() {
		return this.sqlError;
	}

	public String getInvalidDate() {
		return this.invalidDate;
	}

	public String getInvalidCode() {
		return this.invalidCode;
	}

	public String getInvalidUnsupport() {
		return this.invalidUnsupport;
	}
}
