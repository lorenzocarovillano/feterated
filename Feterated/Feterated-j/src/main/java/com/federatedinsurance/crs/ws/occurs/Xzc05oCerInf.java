/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: XZC05O-CER-INF<br>
 * Variables: XZC05O-CER-INF from copybook XZC050C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzc05oCerInf implements ICopyable<Xzc05oCerInf> {

	//==== PROPERTIES ====
	public static final int WRD_INF_MAXOCCURS = 20;
	//Original name: XZC05O-STA-CD
	private char staCd = DefaultValues.CHAR_VAL;
	//Original name: XZC05O-CER-NBR
	private String cerNbr = DefaultValues.stringVal(Len.CER_NBR);
	//Original name: XZC05O-ACY-TYP-CD
	private char acyTypCd = DefaultValues.CHAR_VAL;
	//Original name: XZC05O-CER-HLD-ID
	private String cerHldId = DefaultValues.stringVal(Len.CER_HLD_ID);
	//Original name: XZC05O-CER-HLD-NM
	private String cerHldNm = DefaultValues.stringVal(Len.CER_HLD_NM);
	//Original name: XZC05O-OTH-INF-TYP-CD
	private String othInfTypCd = DefaultValues.stringVal(Len.OTH_INF_TYP_CD);
	//Original name: XZC05O-OTH-INF-PFX
	private String othInfPfx = DefaultValues.stringVal(Len.OTH_INF_PFX);
	//Original name: XZC05O-OTH-INF-TXT
	private String othInfTxt = DefaultValues.stringVal(Len.OTH_INF_TXT);
	//Original name: XZC05O-CER-HLD-ADR-ID
	private String cerHldAdrId = DefaultValues.stringVal(Len.CER_HLD_ADR_ID);
	//Original name: XZC05O-CER-HLD-ADR1
	private String cerHldAdr1 = DefaultValues.stringVal(Len.CER_HLD_ADR1);
	//Original name: XZC05O-CER-HLD-ADR2
	private String cerHldAdr2 = DefaultValues.stringVal(Len.CER_HLD_ADR2);
	//Original name: XZC05O-CER-HLD-CIT
	private String cerHldCit = DefaultValues.stringVal(Len.CER_HLD_CIT);
	//Original name: XZC05O-CER-HLD-ST-ABB
	private String cerHldStAbb = DefaultValues.stringVal(Len.CER_HLD_ST_ABB);
	//Original name: XZC05O-CER-HLD-PST-CD
	private String cerHldPstCd = DefaultValues.stringVal(Len.CER_HLD_PST_CD);
	//Original name: XZC05O-CER-HLD-ADD-INS-IND
	private char cerHldAddInsInd = DefaultValues.CHAR_VAL;
	//Original name: XZC05O-WRD-INF
	private Xzc05oWrdInf[] wrdInf = new Xzc05oWrdInf[WRD_INF_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Xzc05oCerInf() {
		init();
	}

	public Xzc05oCerInf(Xzc05oCerInf oCerInf) {
		this();
		this.staCd = oCerInf.staCd;
		this.cerNbr = oCerInf.cerNbr;
		this.acyTypCd = oCerInf.acyTypCd;
		this.cerHldId = oCerInf.cerHldId;
		this.cerHldNm = oCerInf.cerHldNm;
		this.othInfTypCd = oCerInf.othInfTypCd;
		this.othInfPfx = oCerInf.othInfPfx;
		this.othInfTxt = oCerInf.othInfTxt;
		this.cerHldAdrId = oCerInf.cerHldAdrId;
		this.cerHldAdr1 = oCerInf.cerHldAdr1;
		this.cerHldAdr2 = oCerInf.cerHldAdr2;
		this.cerHldCit = oCerInf.cerHldCit;
		this.cerHldStAbb = oCerInf.cerHldStAbb;
		this.cerHldPstCd = oCerInf.cerHldPstCd;
		this.cerHldAddInsInd = oCerInf.cerHldAddInsInd;
		for (int wrdInfIdx = 1; wrdInfIdx <= Xzc05oCerInf.WRD_INF_MAXOCCURS; wrdInfIdx++) {
			this.wrdInf[wrdInfIdx - 1] = oCerInf.wrdInf[wrdInfIdx - 1].copy();
		}
	}

	//==== METHODS ====
	public void init() {
		for (int wrdInfIdx = 1; wrdInfIdx <= WRD_INF_MAXOCCURS; wrdInfIdx++) {
			wrdInf[wrdInfIdx - 1] = new Xzc05oWrdInf();
		}
	}

	public void setoCerInfBytes(byte[] buffer, int offset) {
		int position = offset;
		staCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cerNbr = MarshalByte.readString(buffer, position, Len.CER_NBR);
		position += Len.CER_NBR;
		acyTypCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cerHldId = MarshalByte.readString(buffer, position, Len.CER_HLD_ID);
		position += Len.CER_HLD_ID;
		cerHldNm = MarshalByte.readString(buffer, position, Len.CER_HLD_NM);
		position += Len.CER_HLD_NM;
		othInfTypCd = MarshalByte.readString(buffer, position, Len.OTH_INF_TYP_CD);
		position += Len.OTH_INF_TYP_CD;
		othInfPfx = MarshalByte.readString(buffer, position, Len.OTH_INF_PFX);
		position += Len.OTH_INF_PFX;
		othInfTxt = MarshalByte.readString(buffer, position, Len.OTH_INF_TXT);
		position += Len.OTH_INF_TXT;
		cerHldAdrId = MarshalByte.readString(buffer, position, Len.CER_HLD_ADR_ID);
		position += Len.CER_HLD_ADR_ID;
		cerHldAdr1 = MarshalByte.readString(buffer, position, Len.CER_HLD_ADR1);
		position += Len.CER_HLD_ADR1;
		cerHldAdr2 = MarshalByte.readString(buffer, position, Len.CER_HLD_ADR2);
		position += Len.CER_HLD_ADR2;
		cerHldCit = MarshalByte.readString(buffer, position, Len.CER_HLD_CIT);
		position += Len.CER_HLD_CIT;
		cerHldStAbb = MarshalByte.readString(buffer, position, Len.CER_HLD_ST_ABB);
		position += Len.CER_HLD_ST_ABB;
		cerHldPstCd = MarshalByte.readString(buffer, position, Len.CER_HLD_PST_CD);
		position += Len.CER_HLD_PST_CD;
		cerHldAddInsInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		for (int idx = 1; idx <= WRD_INF_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				wrdInf[idx - 1].setWrdInfBytes(buffer, position);
				position += Xzc05oWrdInf.Len.WRD_INF;
			} else {
				wrdInf[idx - 1].initWrdInfSpaces();
				position += Xzc05oWrdInf.Len.WRD_INF;
			}
		}
	}

	public byte[] getoCerInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, staCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cerNbr, Len.CER_NBR);
		position += Len.CER_NBR;
		MarshalByte.writeChar(buffer, position, acyTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cerHldId, Len.CER_HLD_ID);
		position += Len.CER_HLD_ID;
		MarshalByte.writeString(buffer, position, cerHldNm, Len.CER_HLD_NM);
		position += Len.CER_HLD_NM;
		MarshalByte.writeString(buffer, position, othInfTypCd, Len.OTH_INF_TYP_CD);
		position += Len.OTH_INF_TYP_CD;
		MarshalByte.writeString(buffer, position, othInfPfx, Len.OTH_INF_PFX);
		position += Len.OTH_INF_PFX;
		MarshalByte.writeString(buffer, position, othInfTxt, Len.OTH_INF_TXT);
		position += Len.OTH_INF_TXT;
		MarshalByte.writeString(buffer, position, cerHldAdrId, Len.CER_HLD_ADR_ID);
		position += Len.CER_HLD_ADR_ID;
		MarshalByte.writeString(buffer, position, cerHldAdr1, Len.CER_HLD_ADR1);
		position += Len.CER_HLD_ADR1;
		MarshalByte.writeString(buffer, position, cerHldAdr2, Len.CER_HLD_ADR2);
		position += Len.CER_HLD_ADR2;
		MarshalByte.writeString(buffer, position, cerHldCit, Len.CER_HLD_CIT);
		position += Len.CER_HLD_CIT;
		MarshalByte.writeString(buffer, position, cerHldStAbb, Len.CER_HLD_ST_ABB);
		position += Len.CER_HLD_ST_ABB;
		MarshalByte.writeString(buffer, position, cerHldPstCd, Len.CER_HLD_PST_CD);
		position += Len.CER_HLD_PST_CD;
		MarshalByte.writeChar(buffer, position, cerHldAddInsInd);
		position += Types.CHAR_SIZE;
		for (int idx = 1; idx <= WRD_INF_MAXOCCURS; idx++) {
			wrdInf[idx - 1].getWrdInfBytes(buffer, position);
			position += Xzc05oWrdInf.Len.WRD_INF;
		}
		return buffer;
	}

	public Xzc05oCerInf initoCerInfSpaces() {
		staCd = Types.SPACE_CHAR;
		cerNbr = "";
		acyTypCd = Types.SPACE_CHAR;
		cerHldId = "";
		cerHldNm = "";
		othInfTypCd = "";
		othInfPfx = "";
		othInfTxt = "";
		cerHldAdrId = "";
		cerHldAdr1 = "";
		cerHldAdr2 = "";
		cerHldCit = "";
		cerHldStAbb = "";
		cerHldPstCd = "";
		cerHldAddInsInd = Types.SPACE_CHAR;
		for (int idx = 1; idx <= WRD_INF_MAXOCCURS; idx++) {
			wrdInf[idx - 1].initWrdInfSpaces();
		}
		return this;
	}

	public void setStaCd(char staCd) {
		this.staCd = staCd;
	}

	public char getStaCd() {
		return this.staCd;
	}

	public void setCerNbr(String cerNbr) {
		this.cerNbr = Functions.subString(cerNbr, Len.CER_NBR);
	}

	public String getCerNbr() {
		return this.cerNbr;
	}

	public String getXzc05oCerNbrFormatted() {
		return Functions.padBlanks(getCerNbr(), Len.CER_NBR);
	}

	public void setAcyTypCd(char acyTypCd) {
		this.acyTypCd = acyTypCd;
	}

	public char getAcyTypCd() {
		return this.acyTypCd;
	}

	public void setCerHldId(String cerHldId) {
		this.cerHldId = Functions.subString(cerHldId, Len.CER_HLD_ID);
	}

	public String getCerHldId() {
		return this.cerHldId;
	}

	public void setCerHldNm(String cerHldNm) {
		this.cerHldNm = Functions.subString(cerHldNm, Len.CER_HLD_NM);
	}

	public String getCerHldNm() {
		return this.cerHldNm;
	}

	public void setOthInfTypCd(String othInfTypCd) {
		this.othInfTypCd = Functions.subString(othInfTypCd, Len.OTH_INF_TYP_CD);
	}

	public String getOthInfTypCd() {
		return this.othInfTypCd;
	}

	public void setOthInfPfx(String othInfPfx) {
		this.othInfPfx = Functions.subString(othInfPfx, Len.OTH_INF_PFX);
	}

	public String getOthInfPfx() {
		return this.othInfPfx;
	}

	public void setOthInfTxt(String othInfTxt) {
		this.othInfTxt = Functions.subString(othInfTxt, Len.OTH_INF_TXT);
	}

	public String getOthInfTxt() {
		return this.othInfTxt;
	}

	public void setCerHldAdrId(String cerHldAdrId) {
		this.cerHldAdrId = Functions.subString(cerHldAdrId, Len.CER_HLD_ADR_ID);
	}

	public String getCerHldAdrId() {
		return this.cerHldAdrId;
	}

	public void setCerHldAdr1(String cerHldAdr1) {
		this.cerHldAdr1 = Functions.subString(cerHldAdr1, Len.CER_HLD_ADR1);
	}

	public String getCerHldAdr1() {
		return this.cerHldAdr1;
	}

	public void setCerHldAdr2(String cerHldAdr2) {
		this.cerHldAdr2 = Functions.subString(cerHldAdr2, Len.CER_HLD_ADR2);
	}

	public String getCerHldAdr2() {
		return this.cerHldAdr2;
	}

	public void setCerHldCit(String cerHldCit) {
		this.cerHldCit = Functions.subString(cerHldCit, Len.CER_HLD_CIT);
	}

	public String getCerHldCit() {
		return this.cerHldCit;
	}

	public void setCerHldStAbb(String cerHldStAbb) {
		this.cerHldStAbb = Functions.subString(cerHldStAbb, Len.CER_HLD_ST_ABB);
	}

	public String getCerHldStAbb() {
		return this.cerHldStAbb;
	}

	public void setCerHldPstCd(String cerHldPstCd) {
		this.cerHldPstCd = Functions.subString(cerHldPstCd, Len.CER_HLD_PST_CD);
	}

	public String getCerHldPstCd() {
		return this.cerHldPstCd;
	}

	public void setCerHldAddInsInd(char cerHldAddInsInd) {
		this.cerHldAddInsInd = cerHldAddInsInd;
	}

	public char getCerHldAddInsInd() {
		return this.cerHldAddInsInd;
	}

	public Xzc05oWrdInf getWrdInf(int idx) {
		return wrdInf[idx - 1];
	}

	@Override
	public Xzc05oCerInf copy() {
		return new Xzc05oCerInf(this);
	}

	public Xzc05oCerInf initXzc05oCerInf() {
		staCd = Types.SPACE_CHAR;
		cerNbr = "";
		acyTypCd = Types.SPACE_CHAR;
		cerHldId = "";
		cerHldNm = "";
		othInfTypCd = "";
		othInfPfx = "";
		othInfTxt = "";
		cerHldAdrId = "";
		cerHldAdr1 = "";
		cerHldAdr2 = "";
		cerHldCit = "";
		cerHldStAbb = "";
		cerHldPstCd = "";
		cerHldAddInsInd = Types.SPACE_CHAR;
		for (int idx0 = 1; idx0 <= Xzc05oCerInf.WRD_INF_MAXOCCURS; idx0++) {
			wrdInf[idx0 - 1].setXzc05oWrdNm("");
		}
		return this;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CER_NBR = 25;
		public static final int CER_HLD_ID = 64;
		public static final int CER_HLD_NM = 120;
		public static final int OTH_INF_TYP_CD = 3;
		public static final int OTH_INF_PFX = 10;
		public static final int OTH_INF_TXT = 45;
		public static final int CER_HLD_ADR_ID = 64;
		public static final int CER_HLD_ADR1 = 45;
		public static final int CER_HLD_ADR2 = 45;
		public static final int CER_HLD_CIT = 30;
		public static final int CER_HLD_ST_ABB = 2;
		public static final int CER_HLD_PST_CD = 13;
		public static final int STA_CD = 1;
		public static final int ACY_TYP_CD = 1;
		public static final int CER_HLD_ADD_INS_IND = 1;
		public static final int O_CER_INF = STA_CD + CER_NBR + ACY_TYP_CD + CER_HLD_ID + CER_HLD_NM + OTH_INF_TYP_CD + OTH_INF_PFX + OTH_INF_TXT
				+ CER_HLD_ADR_ID + CER_HLD_ADR1 + CER_HLD_ADR2 + CER_HLD_CIT + CER_HLD_ST_ABB + CER_HLD_PST_CD + CER_HLD_ADD_INS_IND
				+ Xzc05oCerInf.WRD_INF_MAXOCCURS * Xzc05oWrdInf.Len.WRD_INF;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
