/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.DpColl;
import com.federatedinsurance.crs.ws.enums.DpFunction;
import com.federatedinsurance.crs.ws.enums.DpSsid;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DSNRLI-PARMS<br>
 * Variable: DSNRLI-PARMS from program TS030299<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DsnrliParms extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: DP-FUNCTION
	private DpFunction function = new DpFunction();
	//Original name: DP-SSID
	private DpSsid ssid = new DpSsid();
	//Original name: DP-RETURN
	private String returnFld = DefaultValues.stringVal(Len.RETURN_FLD);
	//Original name: DP-REASON
	private String reason = DefaultValues.stringVal(Len.REASON);
	//Original name: DP-PLAN
	private String plan = DefaultValues.stringVal(Len.PLAN);
	public static final String PLAN_FEDPRINT = "FEDPRINT";
	//Original name: DP-TECB
	private String tecb = DefaultValues.stringVal(Len.TECB);
	//Original name: DP-SECB
	private String secb = DefaultValues.stringVal(Len.SECB);
	//Original name: DP-RIBPTR
	private String ribptr = DefaultValues.stringVal(Len.RIBPTR);
	//Original name: DP-SRDURA
	private String srdura = DefaultValues.stringVal(Len.SRDURA);
	//Original name: DP-EIBPTR
	private String eibptr = DefaultValues.stringVal(Len.EIBPTR);
	//Original name: DP-GROUP
	private String group = DefaultValues.stringVal(Len.GROUP);
	//Original name: DP-CORR
	private String corr = DefaultValues.stringVal(Len.CORR);
	public static final String CORR_FEDPRINT = "FEDPRINT";
	//Original name: DP-ACCTT
	private String acctt = DefaultValues.stringVal(Len.ACCTT);
	public static final String ACCTT_PRINT_ROUTINE = "FED PRINT ROUTINE";
	//Original name: DP-ACCTI
	private String accti = DefaultValues.stringVal(Len.ACCTI);
	//Original name: DP-COLL
	private DpColl coll = new DpColl();
	//Original name: DP-REUSE
	private String reuse = DefaultValues.stringVal(Len.REUSE);
	public static final String REUSE_RESET = "RESET";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DSNRLI_PARMS;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDsnrliParmsBytes(buf);
	}

	public String getDsnrliParmsFormatted() {
		return MarshalByteExt.bufferToStr(getDsnrliParmsBytes());
	}

	public void setDsnrliParmsBytes(byte[] buffer) {
		setDsnrliParmsBytes(buffer, 1);
	}

	public byte[] getDsnrliParmsBytes() {
		byte[] buffer = new byte[Len.DSNRLI_PARMS];
		return getDsnrliParmsBytes(buffer, 1);
	}

	public void setDsnrliParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		function.setFunction(MarshalByte.readString(buffer, position, DpFunction.Len.FUNCTION));
		position += DpFunction.Len.FUNCTION;
		ssid.setSsid(MarshalByte.readString(buffer, position, DpSsid.Len.SSID));
		position += DpSsid.Len.SSID;
		returnFld = MarshalByte.readString(buffer, position, Len.RETURN_FLD);
		position += Len.RETURN_FLD;
		reason = MarshalByte.readString(buffer, position, Len.REASON);
		position += Len.REASON;
		plan = MarshalByte.readString(buffer, position, Len.PLAN);
		position += Len.PLAN;
		tecb = MarshalByte.readString(buffer, position, Len.TECB);
		position += Len.TECB;
		secb = MarshalByte.readString(buffer, position, Len.SECB);
		position += Len.SECB;
		ribptr = MarshalByte.readString(buffer, position, Len.RIBPTR);
		position += Len.RIBPTR;
		srdura = MarshalByte.readString(buffer, position, Len.SRDURA);
		position += Len.SRDURA;
		eibptr = MarshalByte.readString(buffer, position, Len.EIBPTR);
		position += Len.EIBPTR;
		group = MarshalByte.readString(buffer, position, Len.GROUP);
		position += Len.GROUP;
		corr = MarshalByte.readString(buffer, position, Len.CORR);
		position += Len.CORR;
		acctt = MarshalByte.readString(buffer, position, Len.ACCTT);
		position += Len.ACCTT;
		accti = MarshalByte.readString(buffer, position, Len.ACCTI);
		position += Len.ACCTI;
		coll.setColl(MarshalByte.readString(buffer, position, DpColl.Len.COLL));
		position += DpColl.Len.COLL;
		reuse = MarshalByte.readString(buffer, position, Len.REUSE);
	}

	public byte[] getDsnrliParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, function.getFunction(), DpFunction.Len.FUNCTION);
		position += DpFunction.Len.FUNCTION;
		MarshalByte.writeString(buffer, position, ssid.getSsid(), DpSsid.Len.SSID);
		position += DpSsid.Len.SSID;
		MarshalByte.writeString(buffer, position, returnFld, Len.RETURN_FLD);
		position += Len.RETURN_FLD;
		MarshalByte.writeString(buffer, position, reason, Len.REASON);
		position += Len.REASON;
		MarshalByte.writeString(buffer, position, plan, Len.PLAN);
		position += Len.PLAN;
		MarshalByte.writeString(buffer, position, tecb, Len.TECB);
		position += Len.TECB;
		MarshalByte.writeString(buffer, position, secb, Len.SECB);
		position += Len.SECB;
		MarshalByte.writeString(buffer, position, ribptr, Len.RIBPTR);
		position += Len.RIBPTR;
		MarshalByte.writeString(buffer, position, srdura, Len.SRDURA);
		position += Len.SRDURA;
		MarshalByte.writeString(buffer, position, eibptr, Len.EIBPTR);
		position += Len.EIBPTR;
		MarshalByte.writeString(buffer, position, group, Len.GROUP);
		position += Len.GROUP;
		MarshalByte.writeString(buffer, position, corr, Len.CORR);
		position += Len.CORR;
		MarshalByte.writeString(buffer, position, acctt, Len.ACCTT);
		position += Len.ACCTT;
		MarshalByte.writeString(buffer, position, accti, Len.ACCTI);
		position += Len.ACCTI;
		MarshalByte.writeString(buffer, position, coll.getColl(), DpColl.Len.COLL);
		position += DpColl.Len.COLL;
		MarshalByte.writeString(buffer, position, reuse, Len.REUSE);
		return buffer;
	}

	public void setReturnFld(String returnFld) {
		this.returnFld = Functions.subString(returnFld, Len.RETURN_FLD);
	}

	public String getReturnFld() {
		return this.returnFld;
	}

	public String getReturnFldFormatted() {
		return Functions.padBlanks(getReturnFld(), Len.RETURN_FLD);
	}

	public void setReason(String reason) {
		this.reason = Functions.subString(reason, Len.REASON);
	}

	public String getReason() {
		return this.reason;
	}

	public String getReasonFormatted() {
		return Functions.padBlanks(getReason(), Len.REASON);
	}

	public void setPlan(String plan) {
		this.plan = Functions.subString(plan, Len.PLAN);
	}

	public String getPlan() {
		return this.plan;
	}

	public String getPlanFormatted() {
		return Functions.padBlanks(getPlan(), Len.PLAN);
	}

	public void setPlanFedprint() {
		plan = PLAN_FEDPRINT;
	}

	public void setTecb(String tecb) {
		this.tecb = Functions.subString(tecb, Len.TECB);
	}

	public String getTecb() {
		return this.tecb;
	}

	public String getTecbFormatted() {
		return Functions.padBlanks(getTecb(), Len.TECB);
	}

	public void setSecb(String secb) {
		this.secb = Functions.subString(secb, Len.SECB);
	}

	public String getSecb() {
		return this.secb;
	}

	public String getSecbFormatted() {
		return Functions.padBlanks(getSecb(), Len.SECB);
	}

	public void setRibptr(String ribptr) {
		this.ribptr = Functions.subString(ribptr, Len.RIBPTR);
	}

	public String getRibptr() {
		return this.ribptr;
	}

	public String getRibptrFormatted() {
		return Functions.padBlanks(getRibptr(), Len.RIBPTR);
	}

	public void setSrdura(String srdura) {
		this.srdura = Functions.subString(srdura, Len.SRDURA);
	}

	public String getSrdura() {
		return this.srdura;
	}

	public void setEibptr(String eibptr) {
		this.eibptr = Functions.subString(eibptr, Len.EIBPTR);
	}

	public String getEibptr() {
		return this.eibptr;
	}

	public String getEibptrFormatted() {
		return Functions.padBlanks(getEibptr(), Len.EIBPTR);
	}

	public void setGroup(String group) {
		this.group = Functions.subString(group, Len.GROUP);
	}

	public String getGroup() {
		return this.group;
	}

	public String getGroupFormatted() {
		return Functions.padBlanks(getGroup(), Len.GROUP);
	}

	public void setCorr(String corr) {
		this.corr = Functions.subString(corr, Len.CORR);
	}

	public String getCorr() {
		return this.corr;
	}

	public String getCorrFormatted() {
		return Functions.padBlanks(getCorr(), Len.CORR);
	}

	public void setCorrFedprint() {
		corr = CORR_FEDPRINT;
	}

	public void setAcctt(String acctt) {
		this.acctt = Functions.subString(acctt, Len.ACCTT);
	}

	public String getAcctt() {
		return this.acctt;
	}

	public String getAccttFormatted() {
		return Functions.padBlanks(getAcctt(), Len.ACCTT);
	}

	public void setAccttPrintRoutine() {
		acctt = ACCTT_PRINT_ROUTINE;
	}

	public void setAccti(String accti) {
		this.accti = Functions.subString(accti, Len.ACCTI);
	}

	public String getAccti() {
		return this.accti;
	}

	public String getAcctiFormatted() {
		return Functions.padBlanks(getAccti(), Len.ACCTI);
	}

	public void setReuse(String reuse) {
		this.reuse = Functions.subString(reuse, Len.REUSE);
	}

	public String getReuse() {
		return this.reuse;
	}

	public String getReuseFormatted() {
		return Functions.padBlanks(getReuse(), Len.REUSE);
	}

	public void setReuseReset() {
		reuse = REUSE_RESET;
	}

	public DpColl getColl() {
		return coll;
	}

	public DpFunction getFunction() {
		return function;
	}

	public DpSsid getSsid() {
		return ssid;
	}

	@Override
	public byte[] serialize() {
		return getDsnrliParmsBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURN_FLD = 4;
		public static final int REASON = 4;
		public static final int PLAN = 8;
		public static final int TECB = 4;
		public static final int SECB = 4;
		public static final int RIBPTR = 4;
		public static final int SRDURA = 4;
		public static final int EIBPTR = 8;
		public static final int GROUP = 8;
		public static final int CORR = 12;
		public static final int ACCTT = 22;
		public static final int ACCTI = 6;
		public static final int REUSE = 8;
		public static final int DSNRLI_PARMS = DpFunction.Len.FUNCTION + DpSsid.Len.SSID + RETURN_FLD + REASON + PLAN + TECB + SECB + RIBPTR + SRDURA
				+ EIBPTR + GROUP + CORR + ACCTT + ACCTI + DpColl.Len.COLL + REUSE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
