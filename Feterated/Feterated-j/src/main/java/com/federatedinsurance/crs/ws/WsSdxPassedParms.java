/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.occurs.WsSoundex;

/**Original name: WS-SDX-PASSED-PARMS<br>
 * Variable: WS-SDX-PASSED-PARMS from program CIWOSDX<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSdxPassedParms {

	//==== PROPERTIES ====
	public static final int SOUNDEX_MAXOCCURS = 4;
	//Original name: WS-LAST-NAME
	private String lastName = DefaultValues.stringVal(Len.LAST_NAME);
	//Original name: WS-SOUNDEX
	private WsSoundex[] soundex = new WsSoundex[SOUNDEX_MAXOCCURS];
	//Original name: WS-PGM-ID
	private String pgmId = DefaultValues.stringVal(Len.PGM_ID);
	//Original name: WS-SDX-RET-CD
	private String sdxRetCd = DefaultValues.stringVal(Len.SDX_RET_CD);

	//==== CONSTRUCTORS ====
	public WsSdxPassedParms() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int soundexIdx = 1; soundexIdx <= SOUNDEX_MAXOCCURS; soundexIdx++) {
			soundex[soundexIdx - 1] = new WsSoundex();
		}
	}

	public void setWsSdxPassedParmsFormatted(String data) {
		byte[] buffer = new byte[Len.WS_SDX_PASSED_PARMS];
		MarshalByte.writeString(buffer, 1, data, Len.WS_SDX_PASSED_PARMS);
		setWsSdxPassedParmsBytes(buffer, 1);
	}

	public String getWsSdxPassedParmsFormatted() {
		return MarshalByteExt.bufferToStr(getWsSdxPassedParmsBytes());
	}

	public byte[] getWsSdxPassedParmsBytes() {
		byte[] buffer = new byte[Len.WS_SDX_PASSED_PARMS];
		return getWsSdxPassedParmsBytes(buffer, 1);
	}

	public void setWsSdxPassedParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		lastName = MarshalByte.readString(buffer, position, Len.LAST_NAME);
		position += Len.LAST_NAME;
		setSoundexCdBytes(buffer, position);
		position += Len.SOUNDEX_CD;
		pgmId = MarshalByte.readString(buffer, position, Len.PGM_ID);
		position += Len.PGM_ID;
		sdxRetCd = MarshalByte.readString(buffer, position, Len.SDX_RET_CD);
	}

	public byte[] getWsSdxPassedParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, lastName, Len.LAST_NAME);
		position += Len.LAST_NAME;
		getSoundexCdBytes(buffer, position);
		position += Len.SOUNDEX_CD;
		MarshalByte.writeString(buffer, position, pgmId, Len.PGM_ID);
		position += Len.PGM_ID;
		MarshalByte.writeString(buffer, position, sdxRetCd, Len.SDX_RET_CD);
		return buffer;
	}

	public void setLastName(String lastName) {
		this.lastName = Functions.subString(lastName, Len.LAST_NAME);
	}

	public String getLastName() {
		return this.lastName;
	}

	public String getLastNameFormatted() {
		return Functions.padBlanks(getLastName(), Len.LAST_NAME);
	}

	public void setSoundexCdBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= SOUNDEX_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				soundex[idx - 1].setSoundexBytes(buffer, position);
				position += WsSoundex.Len.SOUNDEX;
			} else {
				soundex[idx - 1].initSoundexSpaces();
				position += WsSoundex.Len.SOUNDEX;
			}
		}
	}

	public byte[] getSoundexCdBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= SOUNDEX_MAXOCCURS; idx++) {
			soundex[idx - 1].getSoundexBytes(buffer, position);
			position += WsSoundex.Len.SOUNDEX;
		}
		return buffer;
	}

	public void setPgmId(String pgmId) {
		this.pgmId = Functions.subString(pgmId, Len.PGM_ID);
	}

	public String getPgmId() {
		return this.pgmId;
	}

	public void setSdxRetCd(String sdxRetCd) {
		this.sdxRetCd = Functions.subString(sdxRetCd, Len.SDX_RET_CD);
	}

	public String getSdxRetCd() {
		return this.sdxRetCd;
	}

	public WsSoundex getSoundex(int idx) {
		return soundex[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LAST_NAME = 60;
		public static final int PGM_ID = 8;
		public static final int SDX_RET_CD = 2;
		public static final int SOUNDEX_CD = WsSdxPassedParms.SOUNDEX_MAXOCCURS * WsSoundex.Len.SOUNDEX;
		public static final int WS_SDX_PASSED_PARMS = LAST_NAME + SOUNDEX_CD + PGM_ID + SDX_RET_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
