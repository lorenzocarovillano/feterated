/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-RESP-DATA-ROWS-WRIT-SW<br>
 * Variable: WS-RESP-DATA-ROWS-WRIT-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsRespDataRowsWritSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char WRIT = 'Y';
	public static final char NOT_WRIT = 'N';

	//==== METHODS ====
	public void setRespDataRowsWritSw(char respDataRowsWritSw) {
		this.value = respDataRowsWritSw;
	}

	public char getRespDataRowsWritSw() {
		return this.value;
	}

	public boolean isWsRespDataRowsWrit() {
		return value == WRIT;
	}

	public void setWsRespDataRowsWrit() {
		value = WRIT;
	}

	public void setWsRespDataRowsNotWrit() {
		value = NOT_WRIT;
	}
}
