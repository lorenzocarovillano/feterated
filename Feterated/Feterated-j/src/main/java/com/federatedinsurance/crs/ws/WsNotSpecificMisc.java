/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.WsProcessChildSw;

/**Original name: WS-NOT-SPECIFIC-MISC<br>
 * Variable: WS-NOT-SPECIFIC-MISC from program TS020100<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsNotSpecificMisc {

	//==== PROPERTIES ====
	//Original name: WS-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-DATA-KEY-DISP1
	private String dataKeyDisp1 = DefaultValues.stringVal(Len.DATA_KEY_DISP1);
	//Original name: WS-DATA-KEY-DISP2
	private String dataKeyDisp2 = DefaultValues.stringVal(Len.DATA_KEY_DISP2);
	//Original name: WS-PROCESS-CHILD-SW
	private WsProcessChildSw processChildSw = new WsProcessChildSw();
	//Original name: WS-CHILD-BUS-OBJ-NM
	private String childBusObjNm = DefaultValues.stringVal(Len.CHILD_BUS_OBJ_NM);
	//Original name: WS-CHILD-BUS-OBJ-MDU
	private String childBusObjMdu = DefaultValues.stringVal(Len.CHILD_BUS_OBJ_MDU);

	//==== METHODS ====
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public void setDataKeyDisp1(long dataKeyDisp1) {
		this.dataKeyDisp1 = PicFormatter.display("-Z(8)9").format(dataKeyDisp1).toString();
	}

	public long getDataKeyDisp1() {
		return PicParser.display("-Z(8)9").parseLong(this.dataKeyDisp1);
	}

	public String getDataKeyDisp1Formatted() {
		return this.dataKeyDisp1;
	}

	public String getDataKeyDisp1AsString() {
		return getDataKeyDisp1Formatted();
	}

	public void setDataKeyDisp2(long dataKeyDisp2) {
		this.dataKeyDisp2 = PicFormatter.display("-Z(8)9").format(dataKeyDisp2).toString();
	}

	public long getDataKeyDisp2() {
		return PicParser.display("-Z(8)9").parseLong(this.dataKeyDisp2);
	}

	public String getDataKeyDisp2Formatted() {
		return this.dataKeyDisp2;
	}

	public String getDataKeyDisp2AsString() {
		return getDataKeyDisp2Formatted();
	}

	public void setChildBusObjNm(String childBusObjNm) {
		this.childBusObjNm = Functions.subString(childBusObjNm, Len.CHILD_BUS_OBJ_NM);
	}

	public String getChildBusObjNm() {
		return this.childBusObjNm;
	}

	public String getChildBusObjNmFormatted() {
		return Functions.padBlanks(getChildBusObjNm(), Len.CHILD_BUS_OBJ_NM);
	}

	public void setChildBusObjMdu(String childBusObjMdu) {
		this.childBusObjMdu = Functions.subString(childBusObjMdu, Len.CHILD_BUS_OBJ_MDU);
	}

	public String getChildBusObjMdu() {
		return this.childBusObjMdu;
	}

	public WsProcessChildSw getProcessChildSw() {
		return processChildSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DATA_KEY_DISP1 = 10;
		public static final int DATA_KEY_DISP2 = 10;
		public static final int DATA_KEY_DISP3 = 10;
		public static final int DATA_KEY_DISP4 = 10;
		public static final int DATA_KEY_DISP5 = 10;
		public static final int DATA_KEY_DISP6 = 10;
		public static final int DATA_KEY_DISP7 = 10;
		public static final int DATA_KEY_DISP8 = 10;
		public static final int DATA_KEY_DISP9 = 10;
		public static final int DATA_KEY_DISP10 = 10;
		public static final int DATA_DEC_DISP1 = 11;
		public static final int CHILD_BUS_OBJ_NM = 32;
		public static final int CHILD_BUS_OBJ_MDU = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
