/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ0B9081<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz0b9081 {

	//==== PROPERTIES ====
	//Original name: SW-TTY-FOUND-FLAG
	private boolean ttyFoundFlag = false;
	//Original name: SW-PRI-POL-FOUND-FLAG
	private boolean priPolFoundFlag = false;

	//==== METHODS ====
	public void setTtyFoundFlag(boolean ttyFoundFlag) {
		this.ttyFoundFlag = ttyFoundFlag;
	}

	public boolean isTtyFoundFlag() {
		return this.ttyFoundFlag;
	}

	public void setPriPolFoundFlag(boolean priPolFoundFlag) {
		this.priPolFoundFlag = priPolFoundFlag;
	}

	public boolean isPriPolFoundFlag() {
		return this.priPolFoundFlag;
	}
}
