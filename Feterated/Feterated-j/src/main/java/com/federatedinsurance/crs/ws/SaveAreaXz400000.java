/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.SaParagraphXz400000;
import com.federatedinsurance.crs.ws.enums.SaReturnCodeXz400000;
import com.federatedinsurance.crs.ws.enums.SaSqlcodeXz003000;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program XZ400000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaveAreaXz400000 {

	//==== PROPERTIES ====
	//Original name: SA-SQLCODE
	private SaSqlcodeXz003000 sqlcode = new SaSqlcodeXz003000();
	//Original name: SA-RETURN-CODE
	private SaReturnCodeXz400000 returnCode = new SaReturnCodeXz400000();
	//Original name: SA-PARAGRAPH
	private SaParagraphXz400000 paragraph = new SaParagraphXz400000();
	//Original name: SA-REGION-TO-RUN-IN
	private String regionToRunIn = DefaultValues.stringVal(Len.REGION_TO_RUN_IN);
	//Original name: SA-COPY-PGE-DT
	private String copyPgeDt = DefaultValues.stringVal(Len.COPY_PGE_DT);

	//==== METHODS ====
	public void setRegionToRunIn(String regionToRunIn) {
		this.regionToRunIn = Functions.subString(regionToRunIn, Len.REGION_TO_RUN_IN);
	}

	public String getRegionToRunIn() {
		return this.regionToRunIn;
	}

	public String getCopyPgeDt() {
		return this.copyPgeDt;
	}

	public SaParagraphXz400000 getParagraph() {
		return paragraph;
	}

	public SaReturnCodeXz400000 getReturnCode() {
		return returnCode;
	}

	public SaSqlcodeXz003000 getSqlcode() {
		return sqlcode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REGION_TO_RUN_IN = 8;
		public static final int COPY_PGE_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
