/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-INVALID-POL-EFF-DT<br>
 * Variable: EA-02-INVALID-POL-EFF-DT from program XZC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea02InvalidPolEffDt {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-INVALID-POL-EFF-DT
	private String flr1 = "PGM =";
	//Original name: EA-02-PROGRAM-NAME
	private String programName = DefaultValues.stringVal(Len.PROGRAM_NAME);
	//Original name: FILLER-EA-02-INVALID-POL-EFF-DT-1
	private String flr2 = " PARA =";
	//Original name: EA-02-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-02-INVALID-POL-EFF-DT-2
	private String flr3 = " INVALID INPUT";
	//Original name: FILLER-EA-02-INVALID-POL-EFF-DT-3
	private String flr4 = "POLICY";
	//Original name: FILLER-EA-02-INVALID-POL-EFF-DT-4
	private String flr5 = "EFFECTIVE DATE.";
	//Original name: FILLER-EA-02-INVALID-POL-EFF-DT-5
	private String flr6 = " POL NBR =";
	//Original name: EA-02-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: FILLER-EA-02-INVALID-POL-EFF-DT-6
	private String flr7 = ", POL EFF DT =";
	//Original name: EA-02-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);

	//==== METHODS ====
	public String getEa02InvalidPolEffDtFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidPolEffDtBytes());
	}

	public byte[] getEa02InvalidPolEffDtBytes() {
		byte[] buffer = new byte[Len.EA02_INVALID_POL_EFF_DT];
		return getEa02InvalidPolEffDtBytes(buffer, 1);
	}

	public byte[] getEa02InvalidPolEffDtBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, programName, Len.PROGRAM_NAME);
		position += Len.PROGRAM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setProgramName(String programName) {
		this.programName = Functions.subString(programName, Len.PROGRAM_NAME);
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int PARAGRAPH_NBR = 5;
		public static final int POL_NBR = 9;
		public static final int POL_EFF_DT = 10;
		public static final int FLR1 = 6;
		public static final int FLR2 = 8;
		public static final int FLR3 = 15;
		public static final int FLR4 = 7;
		public static final int FLR6 = 11;
		public static final int EA02_INVALID_POL_EFF_DT = PROGRAM_NAME + PARAGRAPH_NBR + POL_NBR + POL_EFF_DT + FLR1 + FLR2 + 3 * FLR3 + FLR4 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
