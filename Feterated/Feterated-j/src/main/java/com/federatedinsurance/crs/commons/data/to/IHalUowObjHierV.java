/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HAL_UOW_OBJ_HIER_V]
 * 
 */
public interface IHalUowObjHierV extends BaseSqlTo {

	/**
	 * Host Variable HUOH-HIER-SEQ-NBR
	 * 
	 */
	short getHierSeqNbr();

	void setHierSeqNbr(short hierSeqNbr);

	/**
	 * Host Variable HUOH-PNT-BOBJ-NM
	 * 
	 */
	String getPntBobjNm();

	void setPntBobjNm(String pntBobjNm);

	/**
	 * Host Variable HUOH-CHD-BOBJ-NM
	 * 
	 */
	String getChdBobjNm();

	void setChdBobjNm(String chdBobjNm);
};
