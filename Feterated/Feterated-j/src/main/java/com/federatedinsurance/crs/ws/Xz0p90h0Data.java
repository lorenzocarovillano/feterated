/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotRec;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclpolDtaExt;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.TpPolicyInfo;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0P90H0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0p90h0Data {

	//==== PROPERTIES ====
	public static final int TP_POLICY_INFO_MAXOCCURS = 50;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0p90h0 constantFields = new ConstantFieldsXz0p90h0();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXz0p90h0 errorAndAdviceMessages = new ErrorAndAdviceMessagesXz0p90h0();
	//Original name: ES-01-FATAL-ERROR-MSG
	private Es01FatalErrorMsg es01FatalErrorMsg = new Es01FatalErrorMsg();
	//Original name: SUBSCRIPTS
	private SubscriptsXz0p90h0 subscripts = new SubscriptsXz0p90h0();
	//Original name: SWITCHES
	private SwitchesXz0p90h0 switches = new SwitchesXz0p90h0();
	//Original name: TP-POLICY-INFO
	private TpPolicyInfo[] tpPolicyInfo = new TpPolicyInfo[TP_POLICY_INFO_MAXOCCURS];
	//Original name: IX-TP
	private int ixTp = 1;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0p90h0 workingStorageArea = new WorkingStorageAreaXz0p90h0();
	//Original name: WS-FRAMEWORK-REQUEST-AREA
	private WsFrameworkRequestArea wsFrameworkRequestArea = new WsFrameworkRequestArea();
	//Original name: IX-RQ
	private int ixRq = 1;
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-REC
	private DclactNotRec dclactNotRec = new DclactNotRec();
	//Original name: DCLPOL-DTA-EXT
	private DclpolDtaExt dclpolDtaExt = new DclpolDtaExt();
	//Original name: WS-PROXY-PROGRAM-AREA
	private WsProxyProgramArea wsProxyProgramArea = new WsProxyProgramArea();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-XZC090C1-ROW
	private DfhcommareaXzc09090 wsXzc090c1Row = new DfhcommareaXzc09090();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== CONSTRUCTORS ====
	public Xz0p90h0Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int tpPolicyInfoIdx = 1; tpPolicyInfoIdx <= TP_POLICY_INFO_MAXOCCURS; tpPolicyInfoIdx++) {
			tpPolicyInfo[tpPolicyInfoIdx - 1] = new TpPolicyInfo();
		}
	}

	public void initTableOfPolicyInfoHighValues() {
		for (int idx = 1; idx <= TP_POLICY_INFO_MAXOCCURS; idx++) {
			tpPolicyInfo[idx - 1].initTpPolicyInfoHighValues();
		}
	}

	public void setIxTp(int ixTp) {
		this.ixTp = ixTp;
	}

	public int getIxTp() {
		return this.ixTp;
	}

	public void setIxRq(int ixRq) {
		this.ixRq = ixRq;
	}

	public int getIxRq() {
		return this.ixRq;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public ConstantFieldsXz0p90h0 getConstantFields() {
		return constantFields;
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotRec getDclactNotRec() {
		return dclactNotRec;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclpolDtaExt getDclpolDtaExt() {
		return dclpolDtaExt;
	}

	public ErrorAndAdviceMessagesXz0p90h0 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public Es01FatalErrorMsg getEs01FatalErrorMsg() {
		return es01FatalErrorMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public SubscriptsXz0p90h0 getSubscripts() {
		return subscripts;
	}

	public SwitchesXz0p90h0 getSwitches() {
		return switches;
	}

	public TpPolicyInfo getTpPolicyInfo(int idx) {
		return tpPolicyInfo[idx - 1];
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0p90h0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsFrameworkRequestArea getWsFrameworkRequestArea() {
		return wsFrameworkRequestArea;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsProxyProgramArea getWsProxyProgramArea() {
		return wsProxyProgramArea;
	}

	public DfhcommareaXzc09090 getWsXzc090c1Row() {
		return wsXzc090c1Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
