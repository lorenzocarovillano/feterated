/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;

/**Original name: O-TSXXXXBL-BALANCE-FILE<br>
 * File: O-TSXXXXBL-BALANCE-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OTsxxxxblBalanceFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: TBO-CARRIAGE-CONTROL
	private char tboCarriageControl = DefaultValues.CHAR_VAL;
	//Original name: TBO-DATA
	private String tboData = DefaultValues.stringVal(Len.TBO_DATA);

	//==== METHODS ====
	public void setoTsxxxxblBalanceRecordBytes(byte[] buffer) {
		setoTsxxxxblBalanceRecordBytes(buffer, 1);
	}

	public void setoTsxxxxblBalanceRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		setTboHeadingBytes(buffer, position);
		position += Len.TBO_HEADING;
		tboData = MarshalByte.readString(buffer, position, Len.TBO_DATA);
	}

	public byte[] getoTsxxxxblBalanceRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		getTboHeadingBytes(buffer, position);
		position += Len.TBO_HEADING;
		MarshalByte.writeString(buffer, position, tboData, Len.TBO_DATA);
		return buffer;
	}

	public void setTboHeadingBytes(byte[] buffer, int offset) {
		int position = offset;
		tboCarriageControl = MarshalByte.readChar(buffer, position);
	}

	public byte[] getTboHeadingBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, tboCarriageControl);
		return buffer;
	}

	public void setTboCarriageControl(char tboCarriageControl) {
		this.tboCarriageControl = tboCarriageControl;
	}

	public char getTboCarriageControl() {
		return this.tboCarriageControl;
	}

	public void setTboData(String tboData) {
		this.tboData = Functions.subString(tboData, Len.TBO_DATA);
	}

	public String getTboData() {
		return this.tboData;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoTsxxxxblBalanceRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoTsxxxxblBalanceRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_TSXXXXBL_BALANCE_RECORD;
	}

	@Override
	public IBuffer copy() {
		OTsxxxxblBalanceFileTO copyTO = new OTsxxxxblBalanceFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TBO_CARRIAGE_CONTROL = 1;
		public static final int TBO_HEADING = TBO_CARRIAGE_CONTROL;
		public static final int TBO_DATA = 198;
		public static final int O_TSXXXXBL_BALANCE_RECORD = TBO_HEADING + TBO_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
