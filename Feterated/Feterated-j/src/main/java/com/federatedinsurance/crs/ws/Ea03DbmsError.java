/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-03-DBMS-ERROR<br>
 * Variable: EA-03-DBMS-ERROR from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea03DbmsError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-03-DBMS-ERROR
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-03-DBMS-ERROR-1
	private String flr2 = "TS030099 -";
	//Original name: FILLER-EA-03-DBMS-ERROR-2
	private String flr3 = "A FATAL";
	//Original name: FILLER-EA-03-DBMS-ERROR-3
	private String flr4 = "DATABASE ERROR";
	//Original name: FILLER-EA-03-DBMS-ERROR-4
	private String flr5 = "HAS OCCURRED.";

	//==== METHODS ====
	public String getEa03DbmsErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa03DbmsErrorBytes());
	}

	public byte[] getEa03DbmsErrorBytes() {
		byte[] buffer = new byte[Len.EA03_DBMS_ERROR];
		return getEa03DbmsErrorBytes(buffer, 1);
	}

	public byte[] getEa03DbmsErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 8;
		public static final int FLR4 = 15;
		public static final int FLR5 = 13;
		public static final int EA03_DBMS_ERROR = FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
