/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.WVsamEndOfRecords;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-WORK-AREAS<br>
 * Variable: WS-WORK-AREAS from program HALOESTO<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkAreasHaloesto {

	//==== PROPERTIES ====
	/**Original name: WS-LAST-SEQ-NUM<br>
	 * <pre>*   03  WS-LAST-SEQ-NUM                 PIC  9(03).</pre>*/
	private String wsLastSeqNum = DefaultValues.stringVal(Len.WS_LAST_SEQ_NUM);
	//Original name: WS-RECORDING-LEVEL
	private String wsRecordingLevel = DefaultValues.stringVal(Len.WS_RECORDING_LEVEL);
	//Original name: WS-VSAM-FILE-NM
	private String wsVsamFileNm = DefaultValues.stringVal(Len.WS_VSAM_FILE_NM);
	//Original name: WS-SEARCH-KEY
	private WsSearchKey wsSearchKey = new WsSearchKey();
	/**Original name: W-VSAM-END-OF-RECORDS<br>
	 * <pre>*     05 WS-SEARCH-SEQ-NUM              PIC  9(03).</pre>*/
	private WVsamEndOfRecords wVsamEndOfRecords = new WVsamEndOfRecords();
	//Original name: WS-ABSTIME
	private long wsAbstime = DefaultValues.BIN_LONG_VAL;
	//Original name: WS-ABSTIME-MSECS
	private int wsAbstimeMsecs = DefaultValues.BIN_INT_VAL;
	//Original name: WS-MILLISECONDS
	private String wsMilliseconds = DefaultValues.stringVal(Len.WS_MILLISECONDS);
	//Original name: WS-ERR-TIMESTAMP
	private WsErrTimestamp wsErrTimestamp = new WsErrTimestamp();

	//==== METHODS ====
	public void setWsLastSeqNum(int wsLastSeqNum) {
		this.wsLastSeqNum = NumericDisplay.asString(wsLastSeqNum, Len.WS_LAST_SEQ_NUM);
	}

	public void setWsLastSeqNumFormatted(String wsLastSeqNum) {
		this.wsLastSeqNum = Trunc.toUnsignedNumeric(wsLastSeqNum, Len.WS_LAST_SEQ_NUM);
	}

	public int getWsLastSeqNum() {
		return NumericDisplay.asInt(this.wsLastSeqNum);
	}

	public String getWsLastSeqNumFormatted() {
		return this.wsLastSeqNum;
	}

	public void setWsRecordingLevel(String wsRecordingLevel) {
		this.wsRecordingLevel = Functions.subString(wsRecordingLevel, Len.WS_RECORDING_LEVEL);
	}

	public String getWsRecordingLevel() {
		return this.wsRecordingLevel;
	}

	public void setWsVsamFileNm(String wsVsamFileNm) {
		this.wsVsamFileNm = Functions.subString(wsVsamFileNm, Len.WS_VSAM_FILE_NM);
	}

	public String getWsVsamFileNm() {
		return this.wsVsamFileNm;
	}

	public String getWsVsamFileNmFormatted() {
		return Functions.padBlanks(getWsVsamFileNm(), Len.WS_VSAM_FILE_NM);
	}

	public void setWsAbstime(long wsAbstime) {
		this.wsAbstime = wsAbstime;
	}

	public long getWsAbstime() {
		return this.wsAbstime;
	}

	public void setWsAbstimeMsecs(int wsAbstimeMsecs) {
		this.wsAbstimeMsecs = wsAbstimeMsecs;
	}

	public int getWsAbstimeMsecs() {
		return this.wsAbstimeMsecs;
	}

	public void setWsMilliseconds(int wsMilliseconds) {
		this.wsMilliseconds = NumericDisplay.asString(wsMilliseconds, Len.WS_MILLISECONDS);
	}

	public int getWsMilliseconds() {
		return NumericDisplay.asInt(this.wsMilliseconds);
	}

	public String getWsMillisecondsFormatted() {
		return this.wsMilliseconds;
	}

	public WsErrTimestamp getWsErrTimestamp() {
		return wsErrTimestamp;
	}

	public WsSearchKey getWsSearchKey() {
		return wsSearchKey;
	}

	public WVsamEndOfRecords getwVsamEndOfRecords() {
		return wVsamEndOfRecords;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_LAST_SEQ_NUM = 5;
		public static final int WS_RECORDING_LEVEL = 32;
		public static final int WS_VSAM_FILE_NM = 8;
		public static final int WS_MILLISECONDS = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
