/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TS-CERT-INFO<br>
 * Variables: TS-CERT-INFO from program XZ0P90C0<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TsCertInfo implements ICopyable<TsCertInfo> {

	//==== PROPERTIES ====
	//Original name: TS-CER-HLD-ID
	private String cerHldId = DefaultValues.stringVal(Len.CER_HLD_ID);
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ID);
	//Original name: TS-CER-NBR
	private String cerNbr = DefaultValues.stringVal(Len.CER_NBR);
	//Original name: TS-CER-HLD-NM
	private String cerHldNm = DefaultValues.stringVal(Len.CER_HLD_NM);
	//Original name: TS-CER-HLD-ADR-ID
	private String cerHldAdrId = DefaultValues.stringVal(Len.CER_HLD_ADR_ID);
	//Original name: TS-CER-HLD-ADR1
	private String cerHldAdr1 = DefaultValues.stringVal(Len.CER_HLD_ADR1);
	//Original name: TS-CER-HLD-ADR2
	private String cerHldAdr2 = DefaultValues.stringVal(Len.CER_HLD_ADR2);
	//Original name: TS-CER-HLD-CIT
	private String cerHldCit = DefaultValues.stringVal(Len.CER_HLD_CIT);
	//Original name: TS-CER-HLD-ST-ABB
	private String cerHldStAbb = DefaultValues.stringVal(Len.CER_HLD_ST_ABB);
	//Original name: TS-CER-HLD-PST-CD
	private String cerHldPstCd = DefaultValues.stringVal(Len.CER_HLD_PST_CD);

	//==== CONSTRUCTORS ====
	public TsCertInfo() {
	}

	public TsCertInfo(TsCertInfo tsCertInfo) {
		this();
		this.cerHldId = tsCertInfo.cerHldId;
		this.cerNbr = tsCertInfo.cerNbr;
		this.cerHldNm = tsCertInfo.cerHldNm;
		this.cerHldAdrId = tsCertInfo.cerHldAdrId;
		this.cerHldAdr1 = tsCertInfo.cerHldAdr1;
		this.cerHldAdr2 = tsCertInfo.cerHldAdr2;
		this.cerHldCit = tsCertInfo.cerHldCit;
		this.cerHldStAbb = tsCertInfo.cerHldStAbb;
		this.cerHldPstCd = tsCertInfo.cerHldPstCd;
	}

	//==== METHODS ====
	public TsCertInfo initTsCertInfoHighValues() {
		cerHldId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ID);
		cerNbr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_NBR);
		cerHldNm = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_NM);
		cerHldAdrId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ADR_ID);
		cerHldAdr1 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ADR1);
		cerHldAdr2 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ADR2);
		cerHldCit = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_CIT);
		cerHldStAbb = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ST_ABB);
		cerHldPstCd = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_PST_CD);
		return this;
	}

	public void setCerHldId(String cerHldId) {
		this.cerHldId = Functions.subString(cerHldId, Len.CER_HLD_ID);
	}

	public String getCerHldId() {
		return this.cerHldId;
	}

	public boolean isEndOfTable() {
		return cerHldId.equals(END_OF_TABLE);
	}

	public void setCerNbr(String cerNbr) {
		this.cerNbr = Functions.subString(cerNbr, Len.CER_NBR);
	}

	public String getCerNbr() {
		return this.cerNbr;
	}

	public void setCerHldNm(String cerHldNm) {
		this.cerHldNm = Functions.subString(cerHldNm, Len.CER_HLD_NM);
	}

	public String getCerHldNm() {
		return this.cerHldNm;
	}

	public void setCerHldAdrId(String cerHldAdrId) {
		this.cerHldAdrId = Functions.subString(cerHldAdrId, Len.CER_HLD_ADR_ID);
	}

	public String getCerHldAdrId() {
		return this.cerHldAdrId;
	}

	public void setCerHldAdr1(String cerHldAdr1) {
		this.cerHldAdr1 = Functions.subString(cerHldAdr1, Len.CER_HLD_ADR1);
	}

	public String getCerHldAdr1() {
		return this.cerHldAdr1;
	}

	public void setCerHldAdr2(String cerHldAdr2) {
		this.cerHldAdr2 = Functions.subString(cerHldAdr2, Len.CER_HLD_ADR2);
	}

	public String getCerHldAdr2() {
		return this.cerHldAdr2;
	}

	public void setCerHldCit(String cerHldCit) {
		this.cerHldCit = Functions.subString(cerHldCit, Len.CER_HLD_CIT);
	}

	public String getCerHldCit() {
		return this.cerHldCit;
	}

	public void setCerHldStAbb(String cerHldStAbb) {
		this.cerHldStAbb = Functions.subString(cerHldStAbb, Len.CER_HLD_ST_ABB);
	}

	public String getCerHldStAbb() {
		return this.cerHldStAbb;
	}

	public void setCerHldPstCd(String cerHldPstCd) {
		this.cerHldPstCd = Functions.subString(cerHldPstCd, Len.CER_HLD_PST_CD);
	}

	public String getCerHldPstCd() {
		return this.cerHldPstCd;
	}

	@Override
	public TsCertInfo copy() {
		return new TsCertInfo(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CER_HLD_ID = 64;
		public static final int CER_NBR = 25;
		public static final int CER_HLD_NM = 120;
		public static final int CER_HLD_ADR_ID = 64;
		public static final int CER_HLD_ADR1 = 45;
		public static final int CER_HLD_ADR2 = 45;
		public static final int CER_HLD_CIT = 30;
		public static final int CER_HLD_ST_ABB = 2;
		public static final int CER_HLD_PST_CD = 13;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
