/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ009O-TRS-DTL-RSP<br>
 * Variable: XZ009O-TRS-DTL-RSP from copybook XZC090CO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz009oTrsDtlRsp {

	//==== PROPERTIES ====
	//Original name: XZ009O-RET-MSG
	private Xz009oRetMsg retMsg = new Xz009oRetMsg();
	//Original name: XZ009O-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZ009O-PT-EFF-DT
	private String ptEffDt = DefaultValues.stringVal(Len.PT_EFF_DT);
	//Original name: XZ009O-PT-EXP-DT
	private String ptExpDt = DefaultValues.stringVal(Len.PT_EXP_DT);
	//Original name: XZ009O-QTE-NBR
	private int qteNbr = DefaultValues.INT_VAL;
	//Original name: XZ009O-ACT-NBR
	private String actNbr = DefaultValues.stringVal(Len.ACT_NBR);
	//Original name: XZ009O-NAMED-INSURED
	private Xz009oNamedInsured namedInsured = new Xz009oNamedInsured();

	//==== METHODS ====
	public void setXzc09oServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		retMsg.setRetMsgBytes(buffer, position);
		position += Xz009oRetMsg.Len.RET_MSG;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		setPolTrmBytes(buffer, position);
		position += Len.POL_TRM;
		qteNbr = MarshalByte.readInt(buffer, position, Len.QTE_NBR);
		position += Len.QTE_NBR;
		actNbr = MarshalByte.readString(buffer, position, Len.ACT_NBR);
		position += Len.ACT_NBR;
		namedInsured.setNamedInsuredBytes(buffer, position);
	}

	public byte[] getXzc09oServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		retMsg.getRetMsgBytes(buffer, position);
		position += Xz009oRetMsg.Len.RET_MSG;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		getPolTrmBytes(buffer, position);
		position += Len.POL_TRM;
		MarshalByte.writeInt(buffer, position, qteNbr, Len.QTE_NBR);
		position += Len.QTE_NBR;
		MarshalByte.writeString(buffer, position, actNbr, Len.ACT_NBR);
		position += Len.ACT_NBR;
		namedInsured.getNamedInsuredBytes(buffer, position);
		return buffer;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolTrmBytes(byte[] buffer, int offset) {
		int position = offset;
		ptEffDt = MarshalByte.readString(buffer, position, Len.PT_EFF_DT);
		position += Len.PT_EFF_DT;
		ptExpDt = MarshalByte.readString(buffer, position, Len.PT_EXP_DT);
	}

	public byte[] getPolTrmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ptEffDt, Len.PT_EFF_DT);
		position += Len.PT_EFF_DT;
		MarshalByte.writeString(buffer, position, ptExpDt, Len.PT_EXP_DT);
		return buffer;
	}

	public void setPtEffDt(String ptEffDt) {
		this.ptEffDt = Functions.subString(ptEffDt, Len.PT_EFF_DT);
	}

	public String getPtEffDt() {
		return this.ptEffDt;
	}

	public void setPtExpDt(String ptExpDt) {
		this.ptExpDt = Functions.subString(ptExpDt, Len.PT_EXP_DT);
	}

	public String getPtExpDt() {
		return this.ptExpDt;
	}

	public void setQteNbr(int qteNbr) {
		this.qteNbr = qteNbr;
	}

	public int getQteNbr() {
		return this.qteNbr;
	}

	public void setActNbr(String actNbr) {
		this.actNbr = Functions.subString(actNbr, Len.ACT_NBR);
	}

	public String getActNbr() {
		return this.actNbr;
	}

	public Xz009oNamedInsured getNamedInsured() {
		return namedInsured;
	}

	public Xz009oRetMsg getRetMsg() {
		return retMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int PT_EFF_DT = 10;
		public static final int PT_EXP_DT = 10;
		public static final int POL_TRM = PT_EFF_DT + PT_EXP_DT;
		public static final int QTE_NBR = 5;
		public static final int ACT_NBR = 9;
		public static final int XZC09O_SERVICE_OUTPUTS = Xz009oRetMsg.Len.RET_MSG + POL_NBR + POL_TRM + QTE_NBR + ACT_NBR
				+ Xz009oNamedInsured.Len.NAMED_INSURED;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
