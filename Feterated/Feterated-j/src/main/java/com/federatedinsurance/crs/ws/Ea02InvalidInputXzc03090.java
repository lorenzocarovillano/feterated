/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-INVALID-INPUT<br>
 * Variable: EA-02-INVALID-INPUT from program XZC03090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea02InvalidInputXzc03090 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-INVALID-INPUT
	private String flr1 = "INVALID INPUT.";
	//Original name: FILLER-EA-02-INVALID-INPUT-1
	private String flr2 = " ACCOUNT";
	//Original name: FILLER-EA-02-INVALID-INPUT-2
	private String flr3 = "NUMBER:";
	//Original name: EA-02-ACCOUNT-NUMBER
	private String ea02AccountNumber = DefaultValues.stringVal(Len.EA02_ACCOUNT_NUMBER);

	//==== METHODS ====
	public String getEa02InvalidInputFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidInputBytes());
	}

	public byte[] getEa02InvalidInputBytes() {
		byte[] buffer = new byte[Len.EA02_INVALID_INPUT];
		return getEa02InvalidInputBytes(buffer, 1);
	}

	public byte[] getEa02InvalidInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea02AccountNumber, Len.EA02_ACCOUNT_NUMBER);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEa02AccountNumber(String ea02AccountNumber) {
		this.ea02AccountNumber = Functions.subString(ea02AccountNumber, Len.EA02_ACCOUNT_NUMBER);
	}

	public String getEa02AccountNumber() {
		return this.ea02AccountNumber;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA02_ACCOUNT_NUMBER = 9;
		public static final int FLR1 = 15;
		public static final int FLR2 = 9;
		public static final int FLR3 = 8;
		public static final int EA02_INVALID_INPUT = EA02_ACCOUNT_NUMBER + FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
