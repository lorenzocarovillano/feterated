/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HSSVC-HAL-SCRN-SCRIPT-ROW<br>
 * Variable: HSSVC-HAL-SCRN-SCRIPT-ROW from copybook HALLCSSV<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class HssvcHalScrnScriptRow {

	//==== PROPERTIES ====
	//Original name: HSSVC-HAL-SCRN-SCRIPT-CSUM
	private String halScrnScriptCsum = DefaultValues.stringVal(Len.HAL_SCRN_SCRIPT_CSUM);
	//Original name: HSSVC-UOW-NM-KCRE
	private String uowNmKcre = DefaultValues.stringVal(Len.UOW_NM_KCRE);
	//Original name: HSSVC-SEC-GRP-NM-KCRE
	private String secGrpNmKcre = DefaultValues.stringVal(Len.SEC_GRP_NM_KCRE);
	//Original name: HSSVC-HSSV-CONTEXT-KCRE
	private String hssvContextKcre = DefaultValues.stringVal(Len.HSSV_CONTEXT_KCRE);
	//Original name: HSSVC-HSSV-SCRN-FLD-ID-KCRE
	private String hssvScrnFldIdKcre = DefaultValues.stringVal(Len.HSSV_SCRN_FLD_ID_KCRE);
	//Original name: HSSVC-HSSV-SEQ-NBR-KCRE
	private String hssvSeqNbrKcre = DefaultValues.stringVal(Len.HSSV_SEQ_NBR_KCRE);
	//Original name: HSSVC-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: HSSVC-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HSSVC-SEC-GRP-NM
	private String secGrpNm = DefaultValues.stringVal(Len.SEC_GRP_NM);
	//Original name: HSSVC-HSSV-CONTEXT
	private String hssvContext = DefaultValues.stringVal(Len.HSSV_CONTEXT);
	//Original name: HSSVC-HSSV-SCRN-FLD-ID
	private String hssvScrnFldId = DefaultValues.stringVal(Len.HSSV_SCRN_FLD_ID);
	//Original name: HSSVC-HSSV-SEQ-NBR-SIGN
	private char hssvSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: HSSVC-HSSV-SEQ-NBR
	private String hssvSeqNbr = DefaultValues.stringVal(Len.HSSV_SEQ_NBR);
	//Original name: HSSVC-UOW-NM-CI
	private char uowNmCi = DefaultValues.CHAR_VAL;
	//Original name: HSSVC-SEC-GRP-NM-CI
	private char secGrpNmCi = DefaultValues.CHAR_VAL;
	//Original name: HSSVC-HSSV-CONTEXT-CI
	private char hssvContextCi = DefaultValues.CHAR_VAL;
	//Original name: HSSVC-HSSV-SCRN-FLD-ID-CI
	private char hssvScrnFldIdCi = DefaultValues.CHAR_VAL;
	//Original name: HSSVC-HSSV-SEQ-NBR-CI
	private char hssvSeqNbrCi = DefaultValues.CHAR_VAL;
	/**Original name: HSSVC-HSSV-NEW-LIN-IND-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char hssvNewLinIndCi = DefaultValues.CHAR_VAL;
	//Original name: HSSVC-HSSV-NEW-LIN-IND
	private char hssvNewLinInd = DefaultValues.CHAR_VAL;
	//Original name: HSSVC-HSSV-SCRIPT-TXT-CI
	private char hssvScriptTxtCi = DefaultValues.CHAR_VAL;
	//Original name: HSSVC-HSSV-SCRIPT-TXT
	private String hssvScriptTxt = DefaultValues.stringVal(Len.HSSV_SCRIPT_TXT);

	//==== METHODS ====
	public String getHssvcHalScrnScriptRowFormatted() {
		return MarshalByteExt.bufferToStr(getHssvcHalScrnScriptRowBytes());
	}

	public byte[] getHssvcHalScrnScriptRowBytes() {
		byte[] buffer = new byte[Len.HSSVC_HAL_SCRN_SCRIPT_ROW];
		return getHssvcHalScrnScriptRowBytes(buffer, 1);
	}

	public void setHssvcHalScrnScriptRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setHalScrnScriptFixedBytes(buffer, position);
		position += Len.HAL_SCRN_SCRIPT_FIXED;
		setHalScrnScriptDatesBytes(buffer, position);
		position += Len.HAL_SCRN_SCRIPT_DATES;
		setHalScrnScriptKeyBytes(buffer, position);
		position += Len.HAL_SCRN_SCRIPT_KEY;
		setHalScrnScriptKeyCiBytes(buffer, position);
		position += Len.HAL_SCRN_SCRIPT_KEY_CI;
		setHalScrnScriptDataBytes(buffer, position);
	}

	public byte[] getHssvcHalScrnScriptRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getHalScrnScriptFixedBytes(buffer, position);
		position += Len.HAL_SCRN_SCRIPT_FIXED;
		getHalScrnScriptDatesBytes(buffer, position);
		position += Len.HAL_SCRN_SCRIPT_DATES;
		getHalScrnScriptKeyBytes(buffer, position);
		position += Len.HAL_SCRN_SCRIPT_KEY;
		getHalScrnScriptKeyCiBytes(buffer, position);
		position += Len.HAL_SCRN_SCRIPT_KEY_CI;
		getHalScrnScriptDataBytes(buffer, position);
		return buffer;
	}

	public void setHalScrnScriptFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		halScrnScriptCsum = MarshalByte.readFixedString(buffer, position, Len.HAL_SCRN_SCRIPT_CSUM);
		position += Len.HAL_SCRN_SCRIPT_CSUM;
		uowNmKcre = MarshalByte.readString(buffer, position, Len.UOW_NM_KCRE);
		position += Len.UOW_NM_KCRE;
		secGrpNmKcre = MarshalByte.readString(buffer, position, Len.SEC_GRP_NM_KCRE);
		position += Len.SEC_GRP_NM_KCRE;
		hssvContextKcre = MarshalByte.readString(buffer, position, Len.HSSV_CONTEXT_KCRE);
		position += Len.HSSV_CONTEXT_KCRE;
		hssvScrnFldIdKcre = MarshalByte.readString(buffer, position, Len.HSSV_SCRN_FLD_ID_KCRE);
		position += Len.HSSV_SCRN_FLD_ID_KCRE;
		hssvSeqNbrKcre = MarshalByte.readString(buffer, position, Len.HSSV_SEQ_NBR_KCRE);
	}

	public byte[] getHalScrnScriptFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, halScrnScriptCsum, Len.HAL_SCRN_SCRIPT_CSUM);
		position += Len.HAL_SCRN_SCRIPT_CSUM;
		MarshalByte.writeString(buffer, position, uowNmKcre, Len.UOW_NM_KCRE);
		position += Len.UOW_NM_KCRE;
		MarshalByte.writeString(buffer, position, secGrpNmKcre, Len.SEC_GRP_NM_KCRE);
		position += Len.SEC_GRP_NM_KCRE;
		MarshalByte.writeString(buffer, position, hssvContextKcre, Len.HSSV_CONTEXT_KCRE);
		position += Len.HSSV_CONTEXT_KCRE;
		MarshalByte.writeString(buffer, position, hssvScrnFldIdKcre, Len.HSSV_SCRN_FLD_ID_KCRE);
		position += Len.HSSV_SCRN_FLD_ID_KCRE;
		MarshalByte.writeString(buffer, position, hssvSeqNbrKcre, Len.HSSV_SEQ_NBR_KCRE);
		return buffer;
	}

	public void setHalScrnScriptCsumFormatted(String halScrnScriptCsum) {
		this.halScrnScriptCsum = Trunc.toUnsignedNumeric(halScrnScriptCsum, Len.HAL_SCRN_SCRIPT_CSUM);
	}

	public int getHalScrnScriptCsum() {
		return NumericDisplay.asInt(this.halScrnScriptCsum);
	}

	public void setUowNmKcre(String uowNmKcre) {
		this.uowNmKcre = Functions.subString(uowNmKcre, Len.UOW_NM_KCRE);
	}

	public String getUowNmKcre() {
		return this.uowNmKcre;
	}

	public void setSecGrpNmKcre(String secGrpNmKcre) {
		this.secGrpNmKcre = Functions.subString(secGrpNmKcre, Len.SEC_GRP_NM_KCRE);
	}

	public String getSecGrpNmKcre() {
		return this.secGrpNmKcre;
	}

	public void setHssvContextKcre(String hssvContextKcre) {
		this.hssvContextKcre = Functions.subString(hssvContextKcre, Len.HSSV_CONTEXT_KCRE);
	}

	public String getHssvContextKcre() {
		return this.hssvContextKcre;
	}

	public void setHssvScrnFldIdKcre(String hssvScrnFldIdKcre) {
		this.hssvScrnFldIdKcre = Functions.subString(hssvScrnFldIdKcre, Len.HSSV_SCRN_FLD_ID_KCRE);
	}

	public String getHssvScrnFldIdKcre() {
		return this.hssvScrnFldIdKcre;
	}

	public void setHssvSeqNbrKcre(String hssvSeqNbrKcre) {
		this.hssvSeqNbrKcre = Functions.subString(hssvSeqNbrKcre, Len.HSSV_SEQ_NBR_KCRE);
	}

	public String getHssvSeqNbrKcre() {
		return this.hssvSeqNbrKcre;
	}

	public void setHalScrnScriptDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getHalScrnScriptDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setHalScrnScriptKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		uowNm = MarshalByte.readString(buffer, position, Len.UOW_NM);
		position += Len.UOW_NM;
		secGrpNm = MarshalByte.readString(buffer, position, Len.SEC_GRP_NM);
		position += Len.SEC_GRP_NM;
		hssvContext = MarshalByte.readString(buffer, position, Len.HSSV_CONTEXT);
		position += Len.HSSV_CONTEXT;
		hssvScrnFldId = MarshalByte.readString(buffer, position, Len.HSSV_SCRN_FLD_ID);
		position += Len.HSSV_SCRN_FLD_ID;
		hssvSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hssvSeqNbr = MarshalByte.readFixedString(buffer, position, Len.HSSV_SEQ_NBR);
	}

	public byte[] getHalScrnScriptKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, uowNm, Len.UOW_NM);
		position += Len.UOW_NM;
		MarshalByte.writeString(buffer, position, secGrpNm, Len.SEC_GRP_NM);
		position += Len.SEC_GRP_NM;
		MarshalByte.writeString(buffer, position, hssvContext, Len.HSSV_CONTEXT);
		position += Len.HSSV_CONTEXT;
		MarshalByte.writeString(buffer, position, hssvScrnFldId, Len.HSSV_SCRN_FLD_ID);
		position += Len.HSSV_SCRN_FLD_ID;
		MarshalByte.writeChar(buffer, position, hssvSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hssvSeqNbr, Len.HSSV_SEQ_NBR);
		return buffer;
	}

	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public void setSecGrpNm(String secGrpNm) {
		this.secGrpNm = Functions.subString(secGrpNm, Len.SEC_GRP_NM);
	}

	public String getSecGrpNm() {
		return this.secGrpNm;
	}

	public void setHssvContext(String hssvContext) {
		this.hssvContext = Functions.subString(hssvContext, Len.HSSV_CONTEXT);
	}

	public String getHssvContext() {
		return this.hssvContext;
	}

	public void setHssvScrnFldId(String hssvScrnFldId) {
		this.hssvScrnFldId = Functions.subString(hssvScrnFldId, Len.HSSV_SCRN_FLD_ID);
	}

	public String getHssvScrnFldId() {
		return this.hssvScrnFldId;
	}

	public void setHssvSeqNbrSign(char hssvSeqNbrSign) {
		this.hssvSeqNbrSign = hssvSeqNbrSign;
	}

	public void setHssvSeqNbrSignFormatted(String hssvSeqNbrSign) {
		setHssvSeqNbrSign(Functions.charAt(hssvSeqNbrSign, Types.CHAR_SIZE));
	}

	public char getHssvSeqNbrSign() {
		return this.hssvSeqNbrSign;
	}

	public void setHssvSeqNbr(long hssvSeqNbr) {
		this.hssvSeqNbr = NumericDisplay.asString(hssvSeqNbr, Len.HSSV_SEQ_NBR);
	}

	public void setHssvSeqNbrFormatted(String hssvSeqNbr) {
		this.hssvSeqNbr = Trunc.toUnsignedNumeric(hssvSeqNbr, Len.HSSV_SEQ_NBR);
	}

	public long getHssvSeqNbr() {
		return NumericDisplay.asLong(this.hssvSeqNbr);
	}

	public void setHalScrnScriptKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		uowNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		secGrpNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hssvContextCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hssvScrnFldIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hssvSeqNbrCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getHalScrnScriptKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, uowNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, secGrpNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hssvContextCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hssvScrnFldIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hssvSeqNbrCi);
		return buffer;
	}

	public void setUowNmCi(char uowNmCi) {
		this.uowNmCi = uowNmCi;
	}

	public char getUowNmCi() {
		return this.uowNmCi;
	}

	public void setSecGrpNmCi(char secGrpNmCi) {
		this.secGrpNmCi = secGrpNmCi;
	}

	public char getSecGrpNmCi() {
		return this.secGrpNmCi;
	}

	public void setHssvContextCi(char hssvContextCi) {
		this.hssvContextCi = hssvContextCi;
	}

	public char getHssvContextCi() {
		return this.hssvContextCi;
	}

	public void setHssvScrnFldIdCi(char hssvScrnFldIdCi) {
		this.hssvScrnFldIdCi = hssvScrnFldIdCi;
	}

	public char getHssvScrnFldIdCi() {
		return this.hssvScrnFldIdCi;
	}

	public void setHssvSeqNbrCi(char hssvSeqNbrCi) {
		this.hssvSeqNbrCi = hssvSeqNbrCi;
	}

	public char getHssvSeqNbrCi() {
		return this.hssvSeqNbrCi;
	}

	public void setHalScrnScriptDataBytes(byte[] buffer, int offset) {
		int position = offset;
		hssvNewLinIndCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hssvNewLinInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hssvScriptTxtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hssvScriptTxt = MarshalByte.readString(buffer, position, Len.HSSV_SCRIPT_TXT);
	}

	public byte[] getHalScrnScriptDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, hssvNewLinIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hssvNewLinInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hssvScriptTxtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hssvScriptTxt, Len.HSSV_SCRIPT_TXT);
		return buffer;
	}

	public void setHssvNewLinIndCi(char hssvNewLinIndCi) {
		this.hssvNewLinIndCi = hssvNewLinIndCi;
	}

	public char getHssvNewLinIndCi() {
		return this.hssvNewLinIndCi;
	}

	public void setHssvNewLinInd(char hssvNewLinInd) {
		this.hssvNewLinInd = hssvNewLinInd;
	}

	public char getHssvNewLinInd() {
		return this.hssvNewLinInd;
	}

	public void setHssvScriptTxtCi(char hssvScriptTxtCi) {
		this.hssvScriptTxtCi = hssvScriptTxtCi;
	}

	public char getHssvScriptTxtCi() {
		return this.hssvScriptTxtCi;
	}

	public void setHssvScriptTxt(String hssvScriptTxt) {
		this.hssvScriptTxt = Functions.subString(hssvScriptTxt, Len.HSSV_SCRIPT_TXT);
	}

	public String getHssvScriptTxt() {
		return this.hssvScriptTxt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HAL_SCRN_SCRIPT_CSUM = 9;
		public static final int UOW_NM_KCRE = 32;
		public static final int SEC_GRP_NM_KCRE = 32;
		public static final int HSSV_CONTEXT_KCRE = 32;
		public static final int HSSV_SCRN_FLD_ID_KCRE = 32;
		public static final int HSSV_SEQ_NBR_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int UOW_NM = 32;
		public static final int SEC_GRP_NM = 8;
		public static final int HSSV_CONTEXT = 20;
		public static final int HSSV_SCRN_FLD_ID = 32;
		public static final int HSSV_SEQ_NBR = 10;
		public static final int HSSV_SCRIPT_TXT = 100;
		public static final int HAL_SCRN_SCRIPT_FIXED = HAL_SCRN_SCRIPT_CSUM + UOW_NM_KCRE + SEC_GRP_NM_KCRE + HSSV_CONTEXT_KCRE
				+ HSSV_SCRN_FLD_ID_KCRE + HSSV_SEQ_NBR_KCRE;
		public static final int HAL_SCRN_SCRIPT_DATES = TRANS_PROCESS_DT;
		public static final int HSSV_SEQ_NBR_SIGN = 1;
		public static final int HAL_SCRN_SCRIPT_KEY = UOW_NM + SEC_GRP_NM + HSSV_CONTEXT + HSSV_SCRN_FLD_ID + HSSV_SEQ_NBR_SIGN + HSSV_SEQ_NBR;
		public static final int UOW_NM_CI = 1;
		public static final int SEC_GRP_NM_CI = 1;
		public static final int HSSV_CONTEXT_CI = 1;
		public static final int HSSV_SCRN_FLD_ID_CI = 1;
		public static final int HSSV_SEQ_NBR_CI = 1;
		public static final int HAL_SCRN_SCRIPT_KEY_CI = UOW_NM_CI + SEC_GRP_NM_CI + HSSV_CONTEXT_CI + HSSV_SCRN_FLD_ID_CI + HSSV_SEQ_NBR_CI;
		public static final int HSSV_NEW_LIN_IND_CI = 1;
		public static final int HSSV_NEW_LIN_IND = 1;
		public static final int HSSV_SCRIPT_TXT_CI = 1;
		public static final int HAL_SCRN_SCRIPT_DATA = HSSV_NEW_LIN_IND_CI + HSSV_NEW_LIN_IND + HSSV_SCRIPT_TXT_CI + HSSV_SCRIPT_TXT;
		public static final int HSSVC_HAL_SCRN_SCRIPT_ROW = HAL_SCRN_SCRIPT_FIXED + HAL_SCRN_SCRIPT_DATES + HAL_SCRN_SCRIPT_KEY
				+ HAL_SCRN_SCRIPT_KEY_CI + HAL_SCRN_SCRIPT_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
