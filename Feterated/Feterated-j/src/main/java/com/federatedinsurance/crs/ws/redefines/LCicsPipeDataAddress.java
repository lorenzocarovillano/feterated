/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-CICS-PIPE-DATA-ADDRESS<br>
 * Variable: L-CICS-PIPE-DATA-ADDRESS from program TS547099<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LCicsPipeDataAddress extends BytesClass {

	//==== CONSTRUCTORS ====
	public LCicsPipeDataAddress() {
	}

	public LCicsPipeDataAddress(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_CICS_PIPE_DATA_ADDRESS;
	}

	public void setlCicsPipeDataAddress(int lCicsPipeDataAddress) {
		writeBinaryInt(Pos.L_CICS_PIPE_DATA_ADDRESS, lCicsPipeDataAddress);
	}

	/**Original name: L-CICS-PIPE-DATA-ADDRESS<br>*/
	public int getlCicsPipeDataAddress() {
		return readBinaryInt(Pos.L_CICS_PIPE_DATA_ADDRESS);
	}

	public int getlCicsPipeDataPointer() {
		return readBinaryInt(Pos.L_CICS_PIPE_DATA_POINTER);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_CICS_PIPE_DATA_ADDRESS = 1;
		public static final int L_CICS_PIPE_DATA_POINTER = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int L_CICS_PIPE_DATA_ADDRESS = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
