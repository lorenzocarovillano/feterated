/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLST-WRD-DES<br>
 * Variable: DCLST-WRD-DES from copybook XZH00015<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclstWrdDes {

	//==== PROPERTIES ====
	//Original name: ST-WRD-SEQ-CD
	private String stWrdSeqCd = DefaultValues.stringVal(Len.ST_WRD_SEQ_CD);
	//Original name: ST-WRD-FONT-TYP
	private String stWrdFontTyp = DefaultValues.stringVal(Len.ST_WRD_FONT_TYP);
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;
	//Original name: ST-WRD-TXT-LEN
	private short stWrdTxtLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: ST-WRD-TXT-TEXT
	private String stWrdTxtText = DefaultValues.stringVal(Len.ST_WRD_TXT_TEXT);

	//==== METHODS ====
	public void setStWrdSeqCd(String stWrdSeqCd) {
		this.stWrdSeqCd = Functions.subString(stWrdSeqCd, Len.ST_WRD_SEQ_CD);
	}

	public String getStWrdSeqCd() {
		return this.stWrdSeqCd;
	}

	public void setStWrdFontTyp(String stWrdFontTyp) {
		this.stWrdFontTyp = Functions.subString(stWrdFontTyp, Len.ST_WRD_FONT_TYP);
	}

	public String getStWrdFontTyp() {
		return this.stWrdFontTyp;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	public void setStWrdTxtLen(short stWrdTxtLen) {
		this.stWrdTxtLen = stWrdTxtLen;
	}

	public short getStWrdTxtLen() {
		return this.stWrdTxtLen;
	}

	public void setStWrdTxtText(String stWrdTxtText) {
		this.stWrdTxtText = Functions.subString(stWrdTxtText, Len.ST_WRD_TXT_TEXT);
	}

	public String getStWrdTxtText() {
		return this.stWrdTxtText;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ST_WRD_SEQ_CD = 5;
		public static final int ST_WRD_FONT_TYP = 6;
		public static final int ST_WRD_TXT_TEXT = 4500;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
