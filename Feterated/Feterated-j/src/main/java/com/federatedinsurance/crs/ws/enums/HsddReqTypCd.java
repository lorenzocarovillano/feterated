/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: HSDD-REQ-TYP-CD<br>
 * Variable: HSDD-REQ-TYP-CD from copybook HALLGSDD<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HsddReqTypCd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.REQ_TYP_CD);
	public static final String DEFAULT_FLD = "DFLT";
	public static final String DATA_PRIVACY = "DPRV";
	public static final String BOTH = "BOTH";

	//==== METHODS ====
	public void setReqTypCd(String reqTypCd) {
		this.value = Functions.subString(reqTypCd, Len.REQ_TYP_CD);
	}

	public String getReqTypCd() {
		return this.value;
	}

	public boolean isDefaultFld() {
		return value.equals(DEFAULT_FLD);
	}

	public boolean isDataPrivacy() {
		return value.equals(DATA_PRIVACY);
	}

	public boolean isBoth() {
		return value.equals(BOTH);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REQ_TYP_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
