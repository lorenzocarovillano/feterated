/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: RI-ACTIVE-FLAG<br>
 * Variable: RI-ACTIVE-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class RiActiveFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char ACTIVE = 'Y';
	public static final char INACTIVE = 'N';

	//==== METHODS ====
	public void setActiveFlag(char activeFlag) {
		this.value = activeFlag;
	}

	public char getActiveFlag() {
		return this.value;
	}

	public boolean isInactive() {
		return value == INACTIVE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACTIVE_FLAG = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
