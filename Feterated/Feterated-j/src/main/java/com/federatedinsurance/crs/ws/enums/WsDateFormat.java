/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-DATE-FORMAT<br>
 * Variable: WS-DATE-FORMAT from program XPIODAT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsDateFormat {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.WS_DATE_FORMAT);
	private static final String[] VALID_DATE_FORMAT = new String[] { "J", "G", "G1", "G7", "M", "M1", "M7", "D", "D1", "D7", "BU", "GU", "TI", "TU" };
	private static final String[] VALID_DATE_FORMATO = new String[] { "J", "G", "M", "D", "W", "AM", "AD", "AS", "B", "W", "BU", "GU", "TI", "TU" };
	private static final String[] VALID_HOLIDAY_FORMATO = new String[] { "HT", "H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9" };
	private static final String[] VALID_DATE_FORMAT_CONV = new String[] { "B", "SY", "S3" };
	public static final String DATE_FORMAT_JULIAN = "J";
	public static final String DATE_FORMAT_ASCII_D = "AD";
	public static final String DATE_FORMAT_ASCII_M = "AM";
	public static final String DATE_FORMAT_ASCII_S = "AS";
	private static final String[] DATE_FORMAT_GREGORIAN = new String[] { "G", "G1", "G7" };
	public static final String DATE_FORMAT_GREGORIAN1 = "G1";
	public static final String DATE_FORMAT_GREGORIAN7 = "G7";
	public static final String DATE_FORMAT_GREG_U = "GU";
	private static final String[] DATE_FORMAT_MDY = new String[] { "M", "M1", "M7" };
	public static final String DATE_FORMAT_MDY1 = "M1";
	public static final String DATE_FORMAT_MDY7 = "M7";
	private static final String[] DATE_FORMAT_DMY = new String[] { "D", "D1", "D7" };
	public static final String DATE_FORMAT_DMY1 = "D1";
	public static final String DATE_FORMAT_DMY7 = "D7";
	public static final String DATE_FORMAT_YMDHMSM = "B";
	public static final String DATE_FORMAT_YMDHMSM_TU = "BU";
	public static final String DATE_FORMAT_SYSTEM = "SY";
	public static final String DATE_FORMAT_SE3 = "S3";
	public static final String DATE_FORMAT_TIME = "TI";
	public static final String DATE_FORMAT_TIME_U = "TU";
	public static final String DATE_FORMAT_DAYOFWEEK = "W";
	private static final String[] DATE_FORMAT_YEAR4_REQ = new String[] { "G1", "G7", "M1", "M7", "D1", "D7" };
	private static final String[] DATE_FORMAT_DMY_REQ = new String[] { "G7", "M7", "D7" };

	//==== METHODS ====
	public void setWsDateFormat(String wsDateFormat) {
		this.value = Functions.subString(wsDateFormat, Len.WS_DATE_FORMAT);
	}

	public String getWsDateFormat() {
		return this.value;
	}

	public String getWsDateFormatFormatted() {
		return Functions.padBlanks(getWsDateFormat(), Len.WS_DATE_FORMAT);
	}

	public boolean isValidDateFormat() {
		return ArrayUtils.contains(VALID_DATE_FORMAT, value);
	}

	public boolean isValidDateFormato() {
		return ArrayUtils.contains(VALID_DATE_FORMATO, value);
	}

	public boolean isValidHolidayFormato() {
		return ArrayUtils.contains(VALID_HOLIDAY_FORMATO, value);
	}

	public boolean isValidDateFormatConv() {
		return ArrayUtils.contains(VALID_DATE_FORMAT_CONV, value);
	}

	public boolean isDateFormatJulian() {
		return value.equals(DATE_FORMAT_JULIAN);
	}

	public boolean isDateFormatAsciiD() {
		return value.equals(DATE_FORMAT_ASCII_D);
	}

	public boolean isDateFormatAsciiM() {
		return value.equals(DATE_FORMAT_ASCII_M);
	}

	public boolean isDateFormatAsciiS() {
		return value.equals(DATE_FORMAT_ASCII_S);
	}

	public boolean isDateFormatGregorian() {
		return ArrayUtils.contains(DATE_FORMAT_GREGORIAN, value);
	}

	public boolean isDateFormatGregorian1() {
		return value.equals(DATE_FORMAT_GREGORIAN1);
	}

	public boolean isDateFormatGregorian7() {
		return value.equals(DATE_FORMAT_GREGORIAN7);
	}

	public boolean isDateFormatGregU() {
		return value.equals(DATE_FORMAT_GREG_U);
	}

	public boolean isDateFormatMdy() {
		return ArrayUtils.contains(DATE_FORMAT_MDY, value);
	}

	public boolean isDateFormatMdy1() {
		return value.equals(DATE_FORMAT_MDY1);
	}

	public boolean isDateFormatMdy7() {
		return value.equals(DATE_FORMAT_MDY7);
	}

	public boolean isDateFormatDmy() {
		return ArrayUtils.contains(DATE_FORMAT_DMY, value);
	}

	public boolean isDateFormatDmy1() {
		return value.equals(DATE_FORMAT_DMY1);
	}

	public boolean isDateFormatDmy7() {
		return value.equals(DATE_FORMAT_DMY7);
	}

	public boolean isDateFormatYmdhmsm() {
		return value.equals(DATE_FORMAT_YMDHMSM);
	}

	public boolean isDateFormatYmdhmsmTu() {
		return value.equals(DATE_FORMAT_YMDHMSM_TU);
	}

	public boolean isDateFormatSystem() {
		return value.equals(DATE_FORMAT_SYSTEM);
	}

	public boolean isDateFormatSe3() {
		return value.equals(DATE_FORMAT_SE3);
	}

	public boolean isDateFormatTime() {
		return value.equals(DATE_FORMAT_TIME);
	}

	public boolean isDateFormatTimeU() {
		return value.equals(DATE_FORMAT_TIME_U);
	}

	public boolean isDateFormatDayofweek() {
		return value.equals(DATE_FORMAT_DAYOFWEEK);
	}

	public boolean isDateFormatYear4Req() {
		return ArrayUtils.contains(DATE_FORMAT_YEAR4_REQ, value);
	}

	public boolean isDateFormatDmyReq() {
		return ArrayUtils.contains(DATE_FORMAT_DMY_REQ, value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_DATE_FORMAT = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
