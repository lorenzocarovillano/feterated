/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-41-RTY-NO-FREE-SESSIONS-MSG<br>
 * Variable: EA-41-RTY-NO-FREE-SESSIONS-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea41RtyNoFreeSessionsMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-41-RTY-NO-FREE-SESSIONS-MSG
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-41-RTY-NO-FREE-SESSIONS-MSG-1
	private String flr2 = "TARGETED CICS";
	//Original name: FILLER-EA-41-RTY-NO-FREE-SESSIONS-MSG-2
	private String flr3 = "REGIONS HAVE NO";
	//Original name: FILLER-EA-41-RTY-NO-FREE-SESSIONS-MSG-3
	private String flr4 = " FREE SESSIONS.";
	//Original name: FILLER-EA-41-RTY-NO-FREE-SESSIONS-MSG-4
	private String flr5 = "REGIONS TRIED:";
	//Original name: EA-41-REGIONS-ATTEMPTED
	private String ea41RegionsAttempted = DefaultValues.stringVal(Len.EA41_REGIONS_ATTEMPTED);

	//==== METHODS ====
	public String getEa41RtyNoFreeSessionsMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa41RtyNoFreeSessionsMsgBytes());
	}

	public byte[] getEa41RtyNoFreeSessionsMsgBytes() {
		byte[] buffer = new byte[Len.EA41_RTY_NO_FREE_SESSIONS_MSG];
		return getEa41RtyNoFreeSessionsMsgBytes(buffer, 1);
	}

	public byte[] getEa41RtyNoFreeSessionsMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea41RegionsAttempted, Len.EA41_REGIONS_ATTEMPTED);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa41RegionsAttempted(String ea41RegionsAttempted) {
		this.ea41RegionsAttempted = Functions.subString(ea41RegionsAttempted, Len.EA41_REGIONS_ATTEMPTED);
	}

	public String getEa41RegionsAttempted() {
		return this.ea41RegionsAttempted;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA41_REGIONS_ATTEMPTED = 30;
		public static final int FLR1 = 11;
		public static final int FLR2 = 14;
		public static final int FLR3 = 15;
		public static final int FLR4 = 17;
		public static final int EA41_RTY_NO_FREE_SESSIONS_MSG = EA41_REGIONS_ATTEMPTED + FLR1 + FLR2 + 2 * FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
