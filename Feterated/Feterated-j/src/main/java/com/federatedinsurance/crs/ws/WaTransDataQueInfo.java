/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WA-TRANS-DATA-QUE-INFO<br>
 * Variable: WA-TRANS-DATA-QUE-INFO from program TS020000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WaTransDataQueInfo {

	//==== PROPERTIES ====
	//Original name: WA-TD-OPERATION-NAME
	private String operationName = DefaultValues.stringVal(Len.OPERATION_NAME);
	//Original name: WA-TD-CURRENT-DATE-YYYYMMDD
	private String currentDateYyyymmdd = DefaultValues.stringVal(Len.CURRENT_DATE_YYYYMMDD);
	//Original name: WA-TD-BROKER-BEGIN
	private String brokerBegin = DefaultValues.stringVal(Len.BROKER_BEGIN);
	//Original name: WA-TD-BROKER-END
	private String brokerEnd = DefaultValues.stringVal(Len.BROKER_END);
	//Original name: WA-TD-BROKER-ELAPSED
	private String brokerElapsed = DefaultValues.stringVal(Len.BROKER_ELAPSED);
	//Original name: WA-TD-REQHUB-ELAPSED
	private String reqhubElapsed = DefaultValues.stringVal(Len.REQHUB_ELAPSED);
	//Original name: WA-TD-UOW-ELAPSED
	private String uowElapsed = DefaultValues.stringVal(Len.UOW_ELAPSED);
	//Original name: WA-TD-RESHUB-ELAPSED
	private String reshubElapsed = DefaultValues.stringVal(Len.RESHUB_ELAPSED);

	//==== METHODS ====
	public void setOperationName(String operationName) {
		this.operationName = Functions.subString(operationName, Len.OPERATION_NAME);
	}

	public String getOperationName() {
		return this.operationName;
	}

	public void setCurrentDateYyyymmdd(String currentDateYyyymmdd) {
		this.currentDateYyyymmdd = Functions.subString(currentDateYyyymmdd, Len.CURRENT_DATE_YYYYMMDD);
	}

	public String getCurrentDateYyyymmdd() {
		return this.currentDateYyyymmdd;
	}

	public void setBrokerBeginFormatted(String brokerBegin) {
		this.brokerBegin = Trunc.toUnsignedNumeric(brokerBegin, Len.BROKER_BEGIN);
	}

	public int getBrokerBegin() {
		return NumericDisplay.asInt(this.brokerBegin);
	}

	public void setBrokerEndFormatted(String brokerEnd) {
		this.brokerEnd = Trunc.toUnsignedNumeric(brokerEnd, Len.BROKER_END);
	}

	public int getBrokerEnd() {
		return NumericDisplay.asInt(this.brokerEnd);
	}

	public void setBrokerElapsed(AfDecimal brokerElapsed) {
		this.brokerElapsed = Functions.replaceAll(PicFormatter.display("Z(2)9.9(2)").format(brokerElapsed.copy()).toString(), ",", ".");
	}

	public AfDecimal getBrokerElapsed() {
		return PicParser.display("Z(2)9.9(2)").parseDecimal(Len.Int.BROKER_ELAPSED + Len.Fract.BROKER_ELAPSED, Len.Fract.BROKER_ELAPSED,
				Functions.replaceAll(this.brokerElapsed, ".", ","));
	}

	public void setReqhubElapsed(AfDecimal reqhubElapsed) {
		this.reqhubElapsed = Functions.replaceAll(PicFormatter.display("Z(2)9.9(2)").format(reqhubElapsed.copy()).toString(), ",", ".");
	}

	public AfDecimal getReqhubElapsed() {
		return PicParser.display("Z(2)9.9(2)").parseDecimal(Len.Int.REQHUB_ELAPSED + Len.Fract.REQHUB_ELAPSED, Len.Fract.REQHUB_ELAPSED,
				Functions.replaceAll(this.reqhubElapsed, ".", ","));
	}

	public void setUowElapsed(AfDecimal uowElapsed) {
		this.uowElapsed = Functions.replaceAll(PicFormatter.display("Z(2)9.9(2)").format(uowElapsed.copy()).toString(), ",", ".");
	}

	public AfDecimal getUowElapsed() {
		return PicParser.display("Z(2)9.9(2)").parseDecimal(Len.Int.UOW_ELAPSED + Len.Fract.UOW_ELAPSED, Len.Fract.UOW_ELAPSED,
				Functions.replaceAll(this.uowElapsed, ".", ","));
	}

	public void setReshubElapsed(AfDecimal reshubElapsed) {
		this.reshubElapsed = Functions.replaceAll(PicFormatter.display("Z(2)9.9(2)").format(reshubElapsed.copy()).toString(), ",", ".");
	}

	public AfDecimal getReshubElapsed() {
		return PicParser.display("Z(2)9.9(2)").parseDecimal(Len.Int.RESHUB_ELAPSED + Len.Fract.RESHUB_ELAPSED, Len.Fract.RESHUB_ELAPSED,
				Functions.replaceAll(this.reshubElapsed, ".", ","));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPERATION_NAME = 32;
		public static final int CURRENT_DATE_YYYYMMDD = 10;
		public static final int BROKER_BEGIN = 8;
		public static final int BROKER_END = 8;
		public static final int BROKER_ELAPSED = 6;
		public static final int REQHUB_ELAPSED = 6;
		public static final int UOW_ELAPSED = 6;
		public static final int RESHUB_ELAPSED = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int BROKER_ELAPSED = 3;
			public static final int REQHUB_ELAPSED = 3;
			public static final int UOW_ELAPSED = 3;
			public static final int RESHUB_ELAPSED = 3;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int BROKER_ELAPSED = 2;
			public static final int REQHUB_ELAPSED = 2;
			public static final int UOW_ELAPSED = 2;
			public static final int RESHUB_ELAPSED = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
