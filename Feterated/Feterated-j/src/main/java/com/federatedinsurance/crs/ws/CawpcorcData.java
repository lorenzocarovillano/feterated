/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IClientTabV;
import com.federatedinsurance.crs.copy.Cawlc008;
import com.federatedinsurance.crs.copy.Cawlcorc;
import com.federatedinsurance.crs.copy.Cw02hClientTabRow;
import com.federatedinsurance.crs.copy.Cw08hCltObjRelationRow;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program CAWPCORC<br>
 * Generated as a class for rule WS.<br>*/
public class CawpcorcData implements IClientTabV {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsCawpcorc constantFields = new ConstantFieldsCawpcorc();
	//Original name: WS-SPECIFIC-WORK-AREAS
	private WsSpecificWorkAreasCawpcorc wsSpecificWorkAreas = new WsSpecificWorkAreasCawpcorc();
	//Original name: WS-WORK-AREA
	private WsWorkArea wsWorkArea = new WsWorkArea();
	//Original name: WS-CLIENT-PROCESSING
	private WsClientProcessing wsClientProcessing = new WsClientProcessing();
	//Original name: WS-SPECIFIC-SWITCHES
	private WsSpecificSwitches wsSpecificSwitches = new WsSpecificSwitches();
	//Original name: SWITCHES
	private SwitchesCawpcorc switches = new SwitchesCawpcorc();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: CAWLC008
	private Cawlc008 cawlc008 = new Cawlc008();
	//Original name: CAWLCORC
	private Cawlcorc cawlcorc = new Cawlcorc();
	//Original name: CW02H-CLIENT-TAB-ROW
	private Cw02hClientTabRow cw02hClientTabRow = new Cw02hClientTabRow();
	//Original name: CW08H-CLT-OBJ-RELATION-ROW
	private Cw08hCltObjRelationRow cw08hCltObjRelationRow = new Cw08hCltObjRelationRow();
	//Original name: WS-HALLUBOC-LINKAGE
	private Dfhcommarea wsHallubocLinkage = new Dfhcommarea();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: DATE-STRUCTURE
	private DateStructureCawpcorc dateStructure = new DateStructureCawpcorc();
	//Original name: WS-SE3-CUR-ISO-DATE
	private String wsSe3CurIsoDate = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_DATE);
	//Original name: WS-SE3-CUR-ISO-TIME
	private String wsSe3CurIsoTime = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_TIME);
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public void setWsSe3CurIsoDateTimeFormatted(String data) {
		byte[] buffer = new byte[Len.WS_SE3_CUR_ISO_DATE_TIME];
		MarshalByte.writeString(buffer, 1, data, Len.WS_SE3_CUR_ISO_DATE_TIME);
		setWsSe3CurIsoDateTimeBytes(buffer, 1);
	}

	public void setWsSe3CurIsoDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		wsSe3CurIsoDate = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_DATE);
		position += Len.WS_SE3_CUR_ISO_DATE;
		wsSe3CurIsoTime = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_TIME);
	}

	public void setWsSe3CurIsoDate(String wsSe3CurIsoDate) {
		this.wsSe3CurIsoDate = Functions.subString(wsSe3CurIsoDate, Len.WS_SE3_CUR_ISO_DATE);
	}

	public String getWsSe3CurIsoDate() {
		return this.wsSe3CurIsoDate;
	}

	public void setWsSe3CurIsoTime(String wsSe3CurIsoTime) {
		this.wsSe3CurIsoTime = Functions.subString(wsSe3CurIsoTime, Len.WS_SE3_CUR_ISO_TIME);
	}

	public String getWsSe3CurIsoTime() {
		return this.wsSe3CurIsoTime;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public Cawlc008 getCawlc008() {
		return cawlc008;
	}

	public Cawlcorc getCawlcorc() {
		return cawlcorc;
	}

	@Override
	public char getCiclAddNmInd() {
		return cw02hClientTabRow.getCiclAddNmInd();
	}

	@Override
	public void setCiclAddNmInd(char ciclAddNmInd) {
		this.cw02hClientTabRow.setCiclAddNmInd(ciclAddNmInd);
	}

	@Override
	public Character getCiclAddNmIndObj() {
		if (cw02hClientTabRow.getCiclAddNmIndNi() >= 0) {
			return (getCiclAddNmInd());
		} else {
			return null;
		}
	}

	@Override
	public void setCiclAddNmIndObj(Character ciclAddNmIndObj) {
		if (ciclAddNmIndObj != null) {
			setCiclAddNmInd((ciclAddNmIndObj));
			cw02hClientTabRow.setCiclAddNmIndNi(((short) 0));
		} else {
			cw02hClientTabRow.setCiclAddNmIndNi(((short) -1));
		}
	}

	@Override
	public String getCiclBirStCd() {
		return cw02hClientTabRow.getCiclBirStCd();
	}

	@Override
	public void setCiclBirStCd(String ciclBirStCd) {
		this.cw02hClientTabRow.setCiclBirStCd(ciclBirStCd);
	}

	@Override
	public String getCiclDobDt() {
		return cw02hClientTabRow.getCiclDobDt();
	}

	@Override
	public void setCiclDobDt(String ciclDobDt) {
		this.cw02hClientTabRow.setCiclDobDt(ciclDobDt);
	}

	@Override
	public String getCiclEffAcyTs() {
		return cw02hClientTabRow.getCiclEffAcyTs();
	}

	@Override
	public void setCiclEffAcyTs(String ciclEffAcyTs) {
		this.cw02hClientTabRow.setCiclEffAcyTs(ciclEffAcyTs);
	}

	@Override
	public String getCiclEffDt() {
		return cw02hClientTabRow.getCiclEffDt();
	}

	@Override
	public void setCiclEffDt(String ciclEffDt) {
		this.cw02hClientTabRow.setCiclEffDt(ciclEffDt);
	}

	@Override
	public String getCiclExpAcyTs() {
		return cw02hClientTabRow.getCiclExpAcyTs();
	}

	@Override
	public void setCiclExpAcyTs(String ciclExpAcyTs) {
		this.cw02hClientTabRow.setCiclExpAcyTs(ciclExpAcyTs);
	}

	@Override
	public String getCiclExpDt() {
		return cw02hClientTabRow.getCiclExpDt();
	}

	@Override
	public void setCiclExpDt(String ciclExpDt) {
		this.cw02hClientTabRow.setCiclExpDt(ciclExpDt);
	}

	@Override
	public String getCiclFstNm() {
		return cw02hClientTabRow.getCiclFstNm();
	}

	@Override
	public void setCiclFstNm(String ciclFstNm) {
		this.cw02hClientTabRow.setCiclFstNm(ciclFstNm);
	}

	@Override
	public String getCiclLngNm() {
		return FixedStrings.get(cw02hClientTabRow.getCiclLngNmText(), cw02hClientTabRow.getCiclLngNmLen());
	}

	@Override
	public void setCiclLngNm(String ciclLngNm) {
		this.cw02hClientTabRow.setCiclLngNmText(ciclLngNm);
		this.cw02hClientTabRow.setCiclLngNmLen((((short) strLen(ciclLngNm))));
	}

	@Override
	public String getCiclLstNm() {
		return cw02hClientTabRow.getCiclLstNm();
	}

	@Override
	public void setCiclLstNm(String ciclLstNm) {
		this.cw02hClientTabRow.setCiclLstNm(ciclLstNm);
	}

	@Override
	public String getCiclMdlNm() {
		return cw02hClientTabRow.getCiclMdlNm();
	}

	@Override
	public void setCiclMdlNm(String ciclMdlNm) {
		this.cw02hClientTabRow.setCiclMdlNm(ciclMdlNm);
	}

	@Override
	public String getCiclOgnInceptDt() {
		return cw02hClientTabRow.getCiclOgnInceptDt();
	}

	@Override
	public void setCiclOgnInceptDt(String ciclOgnInceptDt) {
		this.cw02hClientTabRow.setCiclOgnInceptDt(ciclOgnInceptDt);
	}

	@Override
	public String getCiclOgnInceptDtObj() {
		if (cw02hClientTabRow.getCiclOgnInceptDtNi() >= 0) {
			return getCiclOgnInceptDt();
		} else {
			return null;
		}
	}

	@Override
	public void setCiclOgnInceptDtObj(String ciclOgnInceptDtObj) {
		if (ciclOgnInceptDtObj != null) {
			setCiclOgnInceptDt(ciclOgnInceptDtObj);
			cw02hClientTabRow.setCiclOgnInceptDtNi(((short) 0));
		} else {
			cw02hClientTabRow.setCiclOgnInceptDtNi(((short) -1));
		}
	}

	@Override
	public String getCiclPriSubCd() {
		return cw02hClientTabRow.getCiclPriSubCd();
	}

	@Override
	public void setCiclPriSubCd(String ciclPriSubCd) {
		this.cw02hClientTabRow.setCiclPriSubCd(ciclPriSubCd);
	}

	@Override
	public String getCiclSdxCd() {
		return cw02hClientTabRow.getCiclSdxCd();
	}

	@Override
	public void setCiclSdxCd(String ciclSdxCd) {
		this.cw02hClientTabRow.setCiclSdxCd(ciclSdxCd);
	}

	@Override
	public String getClientId() {
		return cw02hClientTabRow.getClientId();
	}

	@Override
	public void setClientId(String clientId) {
		this.cw02hClientTabRow.setClientId(clientId);
	}

	public ConstantFieldsCawpcorc getConstantFields() {
		return constantFields;
	}

	public Cw02hClientTabRow getCw02hClientTabRow() {
		return cw02hClientTabRow;
	}

	public Cw08hCltObjRelationRow getCw08hCltObjRelationRow() {
		return cw08hCltObjRelationRow;
	}

	public DateStructureCawpcorc getDateStructure() {
		return dateStructure;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	@Override
	public char getGenderCd() {
		return cw02hClientTabRow.getGenderCd();
	}

	@Override
	public void setGenderCd(char genderCd) {
		this.cw02hClientTabRow.setGenderCd(genderCd);
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	@Override
	public short getHistoryVldNbr() {
		return cw02hClientTabRow.getHistoryVldNbr();
	}

	@Override
	public void setHistoryVldNbr(short historyVldNbr) {
		this.cw02hClientTabRow.setHistoryVldNbr(historyVldNbr);
	}

	@Override
	public String getLegEntCd() {
		return cw02hClientTabRow.getLegEntCd();
	}

	@Override
	public void setLegEntCd(String legEntCd) {
		this.cw02hClientTabRow.setLegEntCd(legEntCd);
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNmPfx() {
		return cw02hClientTabRow.getNmPfx();
	}

	@Override
	public void setNmPfx(String nmPfx) {
		this.cw02hClientTabRow.setNmPfx(nmPfx);
	}

	@Override
	public String getNmSfx() {
		return cw02hClientTabRow.getNmSfx();
	}

	@Override
	public void setNmSfx(String nmSfx) {
		this.cw02hClientTabRow.setNmSfx(nmSfx);
	}

	@Override
	public String getPriLggCd() {
		return cw02hClientTabRow.getPriLggCd();
	}

	@Override
	public void setPriLggCd(String priLggCd) {
		this.cw02hClientTabRow.setPriLggCd(priLggCd);
	}

	@Override
	public String getPrimaryProDsnCd() {
		return cw02hClientTabRow.getPrimaryProDsnCd();
	}

	@Override
	public void setPrimaryProDsnCd(String primaryProDsnCd) {
		this.cw02hClientTabRow.setPrimaryProDsnCd(primaryProDsnCd);
	}

	@Override
	public String getPrimaryProDsnCdObj() {
		if (cw02hClientTabRow.getPrimaryProDsnCdNi() >= 0) {
			return getPrimaryProDsnCd();
		} else {
			return null;
		}
	}

	@Override
	public void setPrimaryProDsnCdObj(String primaryProDsnCdObj) {
		if (primaryProDsnCdObj != null) {
			setPrimaryProDsnCd(primaryProDsnCdObj);
			cw02hClientTabRow.setPrimaryProDsnCdNi(((short) 0));
		} else {
			cw02hClientTabRow.setPrimaryProDsnCdNi(((short) -1));
		}
	}

	@Override
	public char getStatusCd() {
		return cw02hClientTabRow.getStatusCd();
	}

	@Override
	public void setStatusCd(char statusCd) {
		this.cw02hClientTabRow.setStatusCd(statusCd);
	}

	@Override
	public String getStatutoryTleCd() {
		return cw02hClientTabRow.getStatutoryTleCd();
	}

	@Override
	public void setStatutoryTleCd(String statutoryTleCd) {
		this.cw02hClientTabRow.setStatutoryTleCd(statutoryTleCd);
	}

	public SwitchesCawpcorc getSwitches() {
		return switches;
	}

	@Override
	public String getTerminalId() {
		return cw02hClientTabRow.getTerminalId();
	}

	@Override
	public void setTerminalId(String terminalId) {
		this.cw02hClientTabRow.setTerminalId(terminalId);
	}

	@Override
	public String getUserId() {
		return cw02hClientTabRow.getUserId();
	}

	@Override
	public void setUserId(String userId) {
		this.cw02hClientTabRow.setUserId(userId);
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WsClientProcessing getWsClientProcessing() {
		return wsClientProcessing;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public Dfhcommarea getWsHallubocLinkage() {
		return wsHallubocLinkage;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSpecificSwitches getWsSpecificSwitches() {
		return wsSpecificSwitches;
	}

	public WsSpecificWorkAreasCawpcorc getWsSpecificWorkAreas() {
		return wsSpecificWorkAreas;
	}

	public WsWorkArea getWsWorkArea() {
		return wsWorkArea;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_SE3_CUR_ISO_DATE_TIME = WS_SE3_CUR_ISO_DATE + WS_SE3_CUR_ISO_TIME;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
