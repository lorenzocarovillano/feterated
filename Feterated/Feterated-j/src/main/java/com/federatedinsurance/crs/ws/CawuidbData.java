/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Cw02hClientTabRow;
import com.federatedinsurance.crs.copy.Cw03hClientAddressRow;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Halluidg;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program CAWUIDB<br>
 * Generated as a class for rule WS.<br>*/
public class CawuidbData {

	//==== PROPERTIES ====
	//Original name: CF-OVR-PREFIX
	private String cfOvrPrefix = "OVR";
	//Original name: WS-STANDARD-BUSOBJ-FLDS
	private WsStandardBusobjFldsCawuidb wsStandardBusobjFlds = new WsStandardBusobjFldsCawuidb();
	//Original name: WS-GENERAL-WORKFIELDS
	private WsGeneralWorkfieldsCawuidb wsGeneralWorkfields = new WsGeneralWorkfieldsCawuidb();
	//Original name: WS-UBOC-LINKAGE
	private Dfhcommarea wsUbocLinkage = new Dfhcommarea();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: CW02H-CLIENT-TAB-ROW
	private Cw02hClientTabRow cw02hClientTabRow = new Cw02hClientTabRow();
	//Original name: CW03H-CLIENT-ADDRESS-ROW
	private Cw03hClientAddressRow cw03hClientAddressRow = new Cw03hClientAddressRow();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public String getCfOvrPrefix() {
		return this.cfOvrPrefix;
	}

	public void setWsHalouidgLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.WS_HALOUIDG_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.WS_HALOUIDG_LINKAGE);
		setWsHalouidgLinkageBytes(buffer, 1);
	}

	public String getWsHalouidgLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getWsHalouidgLinkageBytes());
	}

	/**Original name: WS-HALOUIDG-LINKAGE<br>*/
	public byte[] getWsHalouidgLinkageBytes() {
		byte[] buffer = new byte[Len.WS_HALOUIDG_LINKAGE];
		return getWsHalouidgLinkageBytes(buffer, 1);
	}

	public void setWsHalouidgLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.setUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.setUidgCaOutputBytes(buffer, position);
	}

	public byte[] getWsHalouidgLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.getUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.getUidgCaOutputBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public Cw02hClientTabRow getCw02hClientTabRow() {
		return cw02hClientTabRow;
	}

	public Cw03hClientAddressRow getCw03hClientAddressRow() {
		return cw03hClientAddressRow;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsGeneralWorkfieldsCawuidb getWsGeneralWorkfields() {
		return wsGeneralWorkfields;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsStandardBusobjFldsCawuidb getWsStandardBusobjFlds() {
		return wsStandardBusobjFlds;
	}

	public Dfhcommarea getWsUbocLinkage() {
		return wsUbocLinkage;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_APPLID = 8;
		public static final int WS_HALOUIDG_LINKAGE = Halluidg.Len.UIDG_CA_INCOMING + Halluidg.Len.UIDG_CA_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
