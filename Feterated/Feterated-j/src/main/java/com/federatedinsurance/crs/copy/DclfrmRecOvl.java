/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLFRM-REC-OVL<br>
 * Variable: DCLFRM-REC-OVL from copybook XZH00022<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclfrmRecOvl {

	//==== PROPERTIES ====
	//Original name: FRM-NBR
	private String frmNbr = DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: FRM-EDT-DT
	private String frmEdtDt = DefaultValues.stringVal(Len.FRM_EDT_DT);
	//Original name: REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);
	//Original name: OVL-EDL-FRM-NM
	private String ovlEdlFrmNm = DefaultValues.stringVal(Len.OVL_EDL_FRM_NM);
	//Original name: ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: OVL-SR-CD
	private String ovlSrCd = DefaultValues.stringVal(Len.OVL_SR_CD);
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;
	//Original name: SPE-PRC-CD
	private String spePrcCd = DefaultValues.stringVal(Len.SPE_PRC_CD);

	//==== METHODS ====
	public void setFrmNbr(String frmNbr) {
		this.frmNbr = Functions.subString(frmNbr, Len.FRM_NBR);
	}

	public String getFrmNbr() {
		return this.frmNbr;
	}

	public void setFrmEdtDt(String frmEdtDt) {
		this.frmEdtDt = Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	public String getFrmEdtDt() {
		return this.frmEdtDt;
	}

	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	public String getRecTypCd() {
		return this.recTypCd;
	}

	public void setOvlEdlFrmNm(String ovlEdlFrmNm) {
		this.ovlEdlFrmNm = Functions.subString(ovlEdlFrmNm, Len.OVL_EDL_FRM_NM);
	}

	public String getOvlEdlFrmNm() {
		return this.ovlEdlFrmNm;
	}

	public String getOvlEdlFrmNmFormatted() {
		return Functions.padBlanks(getOvlEdlFrmNm(), Len.OVL_EDL_FRM_NM);
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public void setOvlSrCd(String ovlSrCd) {
		this.ovlSrCd = Functions.subString(ovlSrCd, Len.OVL_SR_CD);
	}

	public String getOvlSrCd() {
		return this.ovlSrCd;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	public void setSpePrcCd(String spePrcCd) {
		this.spePrcCd = Functions.subString(spePrcCd, Len.SPE_PRC_CD);
	}

	public String getSpePrcCd() {
		return this.spePrcCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FRM_NBR = 30;
		public static final int FRM_EDT_DT = 10;
		public static final int REC_TYP_CD = 5;
		public static final int OVL_EDL_FRM_NM = 30;
		public static final int ST_ABB = 2;
		public static final int OVL_SR_CD = 5;
		public static final int SPE_PRC_CD = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
