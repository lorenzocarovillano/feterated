/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ08C0-ERR-IND<br>
 * Variable: XZ08C0-ERR-IND from copybook XZ08CI1O<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xz08c0ErrInd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.XZ08C0_ERR_IND);
	public static final String NO_ERR = "NoError";
	public static final String SOAP_FAULT = "SOAPFault";

	//==== METHODS ====
	public void setXz08c0ErrInd(String xz08c0ErrInd) {
		this.value = Functions.subString(xz08c0ErrInd, Len.XZ08C0_ERR_IND);
	}

	public String getXz08c0ErrInd() {
		return this.value;
	}

	public boolean isNoErr() {
		return value.equals(NO_ERR);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZ08C0_ERR_IND = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
