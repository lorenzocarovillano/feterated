/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IRfrSeg;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [RFR_SEG]
 * 
 */
public class RfrSegDao extends BaseSqlDao<IRfrSeg> {

	public RfrSegDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IRfrSeg> getToClass() {
		return IRfrSeg.class;
	}

	public short selectRec(String xzh001SegCd, char cfYes, short dft) {
		return buildQuery("selectRec").bind("xzh001SegCd", xzh001SegCd).bind("cfYes", String.valueOf(cfYes)).scalarResultShort(dft);
	}
}
