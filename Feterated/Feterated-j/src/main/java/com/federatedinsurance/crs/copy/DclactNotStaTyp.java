/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLACT-NOT-STA-TYP<br>
 * Variable: DCLACT-NOT-STA-TYP from copybook XZH00018<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclactNotStaTyp {

	//==== PROPERTIES ====
	//Original name: ACT-NOT-STA-CD
	private String actNotStaCd = DefaultValues.stringVal(Len.ACT_NOT_STA_CD);
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setActNotStaCd(String actNotStaCd) {
		this.actNotStaCd = Functions.subString(actNotStaCd, Len.ACT_NOT_STA_CD);
	}

	public String getActNotStaCd() {
		return this.actNotStaCd;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_STA_CD = 2;
		public static final int ACT_NOT_STA_DES = 35;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
