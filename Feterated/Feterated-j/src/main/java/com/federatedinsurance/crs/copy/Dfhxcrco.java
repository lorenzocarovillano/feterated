/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

/**Original name: DFHXCRCO<br>
 * Variable: DFHXCRCO from copybook DFHXCRCO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Dfhxcrco {

	//==== PROPERTIES ====
	/**Original name: EXCI-NORMAL<br>
	 * <pre>* COPYBOOK OF IBM'S CICS EXTERNAL INTERFACE RETURN CODE LISTING
	 *  *************************************************************** 00002000
	 *                                                                  00003000
	 *  CONTROL BLOCK NAME = DFHXCRCO (GENERATED FROM DFHXCRCC)         00004000
	 *                                                                  00005000
	 *                                                                  00006000
	 *  DESCRIPTIVE NAME = CICS TS  External CICS Interface Return code 00007000
	 *                                                                  00008000
	 *                                                                  00009000
	 *                                                                  00010000
	 *                                                                  00011000
	 *       Licensed Materials - Property of IBM                       00012000
	 *                                                                  00013000
	 *       "Restricted Materials of IBM"                              00014000
	 *                                                                  00015000
	 *       5655-Y04                                                   00016000
	 *                                                                  00017000
	 *       (C) Copyright IBM Corp. 1992, 2008"                        00018000
	 *                                                                  00019000
	 *                                                                  00020000
	 *                                                                  00021000
	 *                                                                  00022000
	 *  STATUS = 6.9.0                                                  00023000
	 *                                                                  00024000
	 *                                                                  00025000
	 *  FUNCTION =                                                      00026000
	 *       Copy book to be used by applications using the External    00027000
	 *       CICS Interface CALL API, or EXEC API.                      00028000
	 *       It provide equates for the return code values.             00029000
	 *                                                                  00030000
	 *                                                                  00031000
	 *  NOTES :                                                         00032000
	 *   DEPENDENCIES = S/370                                           00033000
	 *   RESTRICTIONS = none                                            00034000
	 *   MODULE TYPE = N/A                                              00035000
	 *   PROCESSOR = COBOL                                              00036000
	 *                                                                  00037000
	 *  --------------------------------------------------------------- 00038000
	 *                                                                  00039000
	 *  CHANGE ACTIVITY :                                               00040000
	 *       $SEG(DFHXCRCO),COMP(IRC),PROD(CICS TS ):                   00041000
	 *                                                                  00042000
	 *    PN= REASON REL YYMMDD HDXIII : REMARKS                        00043000
	 *   $02= A87365 510 960918 HDBGNRB : Migrate PN87365 from SPA R510 00044000
	 *   $03= A38644 610 000720 HDBGNRB : Migrate PQ38644 from SPA R530 00045000
	 *   $L0= 660    410 921006 HD2GJST : Create EXCI return code equat 00046000
	 *   $L1= 641    410 930225 HDAHSCG : DCR 6174 - DFHIRP level inqui 00047000
	 *   $L2= 724    530 970908 HD2TAHI : Transactional EXCI            00048000
	 *   $P1= M82684 410 930603 HDELDPG : Traced exit for Server Abende 00049000
	 *   $P2= M83185 410 930718 HD2GJST : Add SYSIDERR to DPL responses 00050000
	 *   $P3= M84399 410 931103 HD2GJST : Put back parm list equates    00051000
	 *   $P4= M84689 410 931126 HD5KASR : Add RESP2 value for TERMERR   00052000
	 *   $P5= M84673 410 931129 HD5KASR : Add SERVER_TERMINATED_ERROR   00053000
	 *   $P6= M95900 510 951205 HD2JPEH : Add surrogate-user check      00054000
	 *   $P8= M28123 530 980518 HD2TAHI : Reject TEXCI DPL across XCF   00055000
	 *   $P9= M28721 530 980610 HD2TAHI : Disallow UOWID on TEXCI DPL   00056000
	 *   $PB= D15521 650 070209 HD2GJST: FIN apar PK21399               00057000
	 *   $PC= D21292 660 080711 HD2GJST: Cope with RESUNAVAIL on DPL    00058000
	 *                                                                  00059000
	 *  *************************************************************** 00060000
	 *  =*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*= 00061000
	 *                                                                  00062000
	 *        Return Code and Reason codes for the CALL API             00063000
	 *                                                                  00064000
	 *  =*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*= 00065000
	 *  =*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*= 00066000
	 *                                                                  00067000
	 *        Values for EXCI_RESPONSE                                  00068000
	 *                                                                  00069000
	 *  =*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*= 00070000</pre>*/
	private long exciNormal = 0L;
	//Original name: EXCI-WARNING
	private long exciWarning = 4L;
	//Original name: EXCI-RETRYABLE
	private long exciRetryable = 8L;
	//Original name: EXCI-USER-ERROR
	private long exciUserError = 12L;
	//Original name: EXCI-SYSTEM-ERROR
	private long exciSystemError = 16L;
	/**Original name: PIPE-ALREADY-OPEN<br>
	 * <pre> =*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*= 00076000
	 *                                                                  00077000
	 *        Values of EXCI_REASON when EXCI_RESPONSE=EXCI_WARNING     00078000
	 *                                                                  00079000
	 *  =*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*= 00080000</pre>*/
	private long pipeAlreadyOpen = 1L;
	//Original name: PIPE-ALREADY-CLOSED
	private long pipeAlreadyClosed = 2L;
	//Original name: NO-PIPE
	private long noPipe = 202L;
	//Original name: NO-CICS
	private long noCics = 203L;
	//Original name: INVALID-APPL-NAME
	private long invalidApplName = 403L;
	//Original name: INVALID-USER-TOKEN
	private long invalidUserToken = 404L;
	//Original name: PIPE-NOT-CLOSED
	private long pipeNotClosed = 405L;
	//Original name: PIPE-NOT-OPEN
	private long pipeNotOpen = 406L;
	//Original name: INVALID-TRANSID
	private long invalidTransid = 409L;
	//Original name: IRP-ABORT-RECEIVED
	private long irpAbortReceived = 414L;
	//Original name: INVALID-PIPE-TOKEN
	private long invalidPipeToken = 418L;
	//Original name: DFHXCOPT-LOAD-FAILED
	private long dfhxcoptLoadFailed = 420L;
	//Original name: SERVER-ABENDED
	private long serverAbended = 422L;
	//Original name: CICS-SVC-CALL-FAILURE
	private long cicsSvcCallFailure = 607L;
	//Original name: IRC-CONNECT-FAILURE
	private long ircConnectFailure = 609L;
	//Original name: SERVER-TIMEDOUT
	private long serverTimedout = 624L;
	/**Original name: EXEC-NORMAL<br>
	 * <pre> =*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*= 00170000
	 *                                                                  00171000
	 *        Resp and RESP2 values for the EXEC API                    00172000
	 *                                                                  00173000
	 *  =*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*= 00174000
	 *  *************************************************************** 00175000
	 *                                                                  00176000
	 *        Values of EXEC RESP                                       00177000
	 *                                                                  00178000
	 *  *************************************************************** 00179000</pre>*/
	private long execNormal = 0L;
	//Original name: EXEC-LENGERR
	private long execLengerr = 22L;
	//Original name: EXEC-PGMIDERR
	private long execPgmiderr = 27L;
	//Original name: EXEC-SYSIDERR
	private long execSysiderr = 53L;
	//Original name: EXEC-NOTAUTH
	private long execNotauth = 70L;
	//Original name: EXEC-TERMERR
	private long execTermerr = 81L;
	//Original name: EXEC-ROLLEDBACK
	private long execRolledback = 82L;

	//==== METHODS ====
	public long getExciNormal() {
		return this.exciNormal;
	}

	public long getExciWarning() {
		return this.exciWarning;
	}

	public long getExciRetryable() {
		return this.exciRetryable;
	}

	public long getExciUserError() {
		return this.exciUserError;
	}

	public long getExciSystemError() {
		return this.exciSystemError;
	}

	public long getPipeAlreadyOpen() {
		return this.pipeAlreadyOpen;
	}

	public long getPipeAlreadyClosed() {
		return this.pipeAlreadyClosed;
	}

	public long getNoPipe() {
		return this.noPipe;
	}

	public long getNoCics() {
		return this.noCics;
	}

	public long getInvalidApplName() {
		return this.invalidApplName;
	}

	public long getInvalidUserToken() {
		return this.invalidUserToken;
	}

	public long getPipeNotClosed() {
		return this.pipeNotClosed;
	}

	public long getPipeNotOpen() {
		return this.pipeNotOpen;
	}

	public long getInvalidTransid() {
		return this.invalidTransid;
	}

	public long getIrpAbortReceived() {
		return this.irpAbortReceived;
	}

	public long getInvalidPipeToken() {
		return this.invalidPipeToken;
	}

	public long getDfhxcoptLoadFailed() {
		return this.dfhxcoptLoadFailed;
	}

	public long getServerAbended() {
		return this.serverAbended;
	}

	public long getCicsSvcCallFailure() {
		return this.cicsSvcCallFailure;
	}

	public long getIrcConnectFailure() {
		return this.ircConnectFailure;
	}

	public long getServerTimedout() {
		return this.serverTimedout;
	}

	public long getExecNormal() {
		return this.execNormal;
	}

	public long getExecLengerr() {
		return this.execLengerr;
	}

	public long getExecPgmiderr() {
		return this.execPgmiderr;
	}

	public long getExecSysiderr() {
		return this.execSysiderr;
	}

	public long getExecNotauth() {
		return this.execNotauth;
	}

	public long getExecTermerr() {
		return this.execTermerr;
	}

	public long getExecRolledback() {
		return this.execRolledback;
	}
}
