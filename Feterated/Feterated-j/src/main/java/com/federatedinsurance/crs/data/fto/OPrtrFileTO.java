/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: O-PRTR-FILE<br>
 * File: O-PRTR-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OPrtrFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: PRO-CARRIAGE-CONTROL
	private String proCarriageControl = DefaultValues.stringVal(Len.PRO_CARRIAGE_CONTROL);
	//Original name: PRO-PRTR-DATA
	private String proPrtrData = DefaultValues.stringVal(Len.PRO_PRTR_DATA);

	//==== METHODS ====
	public void setoPrtrRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		proCarriageControl = MarshalByte.readFixedString(buffer, position, Len.PRO_CARRIAGE_CONTROL);
		position += Len.PRO_CARRIAGE_CONTROL;
		proPrtrData = MarshalByte.readString(buffer, position, Len.PRO_PRTR_DATA);
	}

	public byte[] getoPrtrRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, proCarriageControl, Len.PRO_CARRIAGE_CONTROL);
		position += Len.PRO_CARRIAGE_CONTROL;
		MarshalByte.writeString(buffer, position, proPrtrData, Len.PRO_PRTR_DATA);
		return buffer;
	}

	public void setProCarriageControlFormatted(String proCarriageControl) {
		this.proCarriageControl = Trunc.toUnsignedNumeric(proCarriageControl, Len.PRO_CARRIAGE_CONTROL);
	}

	public short getProCarriageControl() {
		return NumericDisplay.asShort(this.proCarriageControl);
	}

	public void setProPrtrData(String proPrtrData) {
		this.proPrtrData = Functions.subString(proPrtrData, Len.PRO_PRTR_DATA);
	}

	public String getProPrtrData() {
		return this.proPrtrData;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoPrtrRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoPrtrRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_PRTR_RECORD;
	}

	@Override
	public IBuffer copy() {
		OPrtrFileTO copyTO = new OPrtrFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PRO_CARRIAGE_CONTROL = 1;
		public static final int PRO_PRTR_DATA = 132;
		public static final int O_PRTR_RECORD = PRO_CARRIAGE_CONTROL + PRO_PRTR_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
