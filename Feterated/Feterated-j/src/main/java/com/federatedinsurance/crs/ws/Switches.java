/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.SwClearUowRequestFlag;
import com.federatedinsurance.crs.ws.enums.SwPrimaryBosFlag;
import com.federatedinsurance.crs.ws.enums.SwUowDataFlag;
import com.federatedinsurance.crs.ws.enums.SwUowHeaderFlag;
import com.federatedinsurance.crs.ws.enums.SwUowNlbeBusErrsFlag;
import com.federatedinsurance.crs.ws.enums.SwUowWarningsFlag;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program TS020000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Switches {

	//==== PROPERTIES ====
	/**Original name: SW-PRIMARY-BOS-FLAG<br>
	 * <pre>* SWITCHES USED TO INVOKE THE REQUESTED UOW.</pre>*/
	private SwPrimaryBosFlag primaryBosFlag = new SwPrimaryBosFlag();
	/**Original name: SW-UOW-HEADER-FLAG<br>
	 * <pre>* SWITCHES USED TO READ THE UOW LEVEL RESPONSE UMTS.</pre>*/
	private SwUowHeaderFlag uowHeaderFlag = new SwUowHeaderFlag();
	//Original name: SW-UOW-DATA-FLAG
	private SwUowDataFlag uowDataFlag = new SwUowDataFlag();
	//Original name: SW-UOW-WARNINGS-FLAG
	private SwUowWarningsFlag uowWarningsFlag = new SwUowWarningsFlag();
	//Original name: SW-UOW-NLBE-BUS-ERRS-FLAG
	private SwUowNlbeBusErrsFlag uowNlbeBusErrsFlag = new SwUowNlbeBusErrsFlag();
	/**Original name: SW-CLEAR-UOW-REQUEST-FLAG<br>
	 * <pre>* SWITCHES USED TO READ THE UOW LEVEL RESPONSE UMTS, BUILD THE
	 * * MAINDRIVER RESPONSE UMT, AND CLEANUP THE MAINDRIVER REQUEST
	 * * UMT, UOW LEVEL REQUEST UMTS, AND UOW LEVEL RESPONSE UMTS.</pre>*/
	private SwClearUowRequestFlag clearUowRequestFlag = new SwClearUowRequestFlag();
	/**Original name: SW-NONLOG-BUS-ERR-FLAG<br>
	 * <pre>* SWITCH USED TO CHECK FOR NON-LOGGABLE BUSINESS ERRORS
	 * * PRODUCED DURING THE PROCESSING OF EACH BRANCH OF THE UOW.
	 * * THESE ERRORS CAUSE FURTHER PROCESSING OF THE UOW TO HALT.</pre>*/
	private boolean nonlogBusErrFlag = false;
	//Original name: SW-ERROR-OCCURED-FLAG
	private boolean errorOccuredFlag = false;
	/**Original name: SW-UBOC-LINKAGE-CHECK-FLAG<br>
	 * <pre>* SWITCH USED DURING CHECK OF UBOC LINKAGE UPON RETURN FROM UOW.</pre>*/
	private boolean ubocLinkageCheckFlag = false;

	//==== METHODS ====
	public void setNonlogBusErrFlag(boolean nonlogBusErrFlag) {
		this.nonlogBusErrFlag = nonlogBusErrFlag;
	}

	public boolean isNonlogBusErrFlag() {
		return this.nonlogBusErrFlag;
	}

	public void setErrorOccuredFlag(boolean errorOccuredFlag) {
		this.errorOccuredFlag = errorOccuredFlag;
	}

	public boolean isErrorOccuredFlag() {
		return this.errorOccuredFlag;
	}

	public void setUbocLinkageCheckFlag(boolean ubocLinkageCheckFlag) {
		this.ubocLinkageCheckFlag = ubocLinkageCheckFlag;
	}

	public boolean isUbocLinkageCheckFlag() {
		return this.ubocLinkageCheckFlag;
	}

	public SwClearUowRequestFlag getClearUowRequestFlag() {
		return clearUowRequestFlag;
	}

	public SwPrimaryBosFlag getPrimaryBosFlag() {
		return primaryBosFlag;
	}

	public SwUowDataFlag getUowDataFlag() {
		return uowDataFlag;
	}

	public SwUowHeaderFlag getUowHeaderFlag() {
		return uowHeaderFlag;
	}

	public SwUowNlbeBusErrsFlag getUowNlbeBusErrsFlag() {
		return uowNlbeBusErrsFlag;
	}

	public SwUowWarningsFlag getUowWarningsFlag() {
		return uowWarningsFlag;
	}
}
