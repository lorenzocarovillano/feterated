/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-41-RTY-NO-CONNECTION-MSG<br>
 * Variable: EA-41-RTY-NO-CONNECTION-MSG from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea41RtyNoConnectionMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-41-RTY-NO-CONNECTION-MSG
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-41-RTY-NO-CONNECTION-MSG-1
	private String flr2 = "UNABLE TO";
	//Original name: FILLER-EA-41-RTY-NO-CONNECTION-MSG-2
	private String flr3 = "CONNECT TO";
	//Original name: FILLER-EA-41-RTY-NO-CONNECTION-MSG-3
	private String flr4 = "TARGETED CICS";
	//Original name: FILLER-EA-41-RTY-NO-CONNECTION-MSG-4
	private String flr5 = "SESSION (";
	//Original name: EA-41-REGION-ATTEMPTED
	private String ea41RegionAttempted = DefaultValues.stringVal(Len.EA41_REGION_ATTEMPTED);
	//Original name: FILLER-EA-41-RTY-NO-CONNECTION-MSG-5
	private char flr6 = ')';

	//==== METHODS ====
	public String getEa41RtyNoConnectionMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa41RtyNoConnectionMsgBytes());
	}

	public byte[] getEa41RtyNoConnectionMsgBytes() {
		byte[] buffer = new byte[Len.EA41_RTY_NO_CONNECTION_MSG];
		return getEa41RtyNoConnectionMsgBytes(buffer, 1);
	}

	public byte[] getEa41RtyNoConnectionMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea41RegionAttempted, Len.EA41_REGION_ATTEMPTED);
		position += Len.EA41_REGION_ATTEMPTED;
		MarshalByte.writeChar(buffer, position, flr6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa41RegionAttempted(String ea41RegionAttempted) {
		this.ea41RegionAttempted = Functions.subString(ea41RegionAttempted, Len.EA41_REGION_ATTEMPTED);
	}

	public String getEa41RegionAttempted() {
		return this.ea41RegionAttempted;
	}

	public char getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA41_REGION_ATTEMPTED = 8;
		public static final int FLR1 = 11;
		public static final int FLR2 = 10;
		public static final int FLR4 = 14;
		public static final int FLR5 = 9;
		public static final int FLR6 = 1;
		public static final int EA41_RTY_NO_CONNECTION_MSG = EA41_REGION_ATTEMPTED + 2 * FLR1 + FLR2 + FLR4 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
