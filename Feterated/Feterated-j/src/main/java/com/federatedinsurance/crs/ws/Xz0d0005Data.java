/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IStWrdDes;
import com.federatedinsurance.crs.copy.DclactNotWrd;
import com.federatedinsurance.crs.copy.DclhalBoAudTgrV;
import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclhalUowObjHierV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Halluchs;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Halluidg;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xz0n0005;
import com.federatedinsurance.crs.copy.Xzc005ActNotWrdRow;
import com.federatedinsurance.crs.copy.Xzh002ActNotPolRow;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.redefines.CidpTableInfo;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0D0005<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0d0005Data implements IStWrdDes {

	//==== PROPERTIES ====
	//Original name: CF-YES
	private char cfYes = 'Y';
	//Original name: WS-SPECIFIC-MISC
	private WsSpecificMiscXz0d0005 wsSpecificMisc = new WsSpecificMiscXz0d0005();
	//Original name: XZC005-ACT-NOT-WRD-ROW
	private Xzc005ActNotWrdRow xzc005ActNotWrdRow = new Xzc005ActNotWrdRow();
	//Original name: XZ0N0005
	private Xz0n0005 xz0n0005 = new Xz0n0005();
	//Original name: XZH005-ACT-NOT-WRD-ROW
	private DclactNotWrd xzh005ActNotWrdRow = new DclactNotWrd();
	//Original name: XZH002-ACT-NOT-POL-ROW
	private Xzh002ActNotPolRow xzh002ActNotPolRow = new Xzh002ActNotPolRow();
	//Original name: WS-BDO-WORK-FIELDS
	private WsBdoWorkFields wsBdoWorkFields = new WsBdoWorkFields();
	//Original name: WS-BDO-SWITCHES
	private WsBdoSwitches wsBdoSwitches = new WsBdoSwitches();
	//Original name: DCLHAL-BO-AUD-TGR-V
	private DclhalBoAudTgrV dclhalBoAudTgrV = new DclhalBoAudTgrV();
	//Original name: HALLUCHS
	private Halluchs halluchs = new Halluchs();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();
	//Original name: HALRLODR-LOCK-DRIVER-STORAGE
	private WsHalrlodrLinkage halrlodrLockDriverStorage = new WsHalrlodrLinkage();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: HALLUSW
	private Hallusw hallusw = new Hallusw();
	//Original name: HALLUDAT
	private Halludat halludat = new Halludat();
	//Original name: HALLUHDR
	private Halluhdr halluhdr = new Halluhdr();
	//Original name: CIDP-TABLE-INFO
	private CidpTableInfo cidpTableInfo = new CidpTableInfo();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: HALRMON-PERF-MONITOR-STORAGE
	private HalrmonPerfMonitorStorage halrmonPerfMonitorStorage = new HalrmonPerfMonitorStorage();
	//Original name: DATE-STRUCTURE
	private DateStructureCawpcorc dateStructure = new DateStructureCawpcorc();
	//Original name: WS-SE3-CUR-ISO-DATE
	private String wsSe3CurIsoDate = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_DATE);
	//Original name: WS-SE3-CUR-ISO-TIME
	private String wsSe3CurIsoTime = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_TIME);
	//Original name: DCLHAL-UOW-OBJ-HIER-V
	private DclhalUowObjHierV dclhalUowObjHierV = new DclhalUowObjHierV();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public char getCfYes() {
		return this.cfYes;
	}

	public void setHalouchsLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.HALOUCHS_LINKAGE);
		setHalouchsLinkageBytes(buffer, 1);
	}

	public String getHalouchsLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHalouchsLinkageBytes());
	}

	/**Original name: HALOUCHS-LINKAGE<br>*/
	public byte[] getHalouchsLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		return getHalouchsLinkageBytes(buffer, 1);
	}

	public void setHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluchs.setLength2(MarshalByte.readBinaryShort(buffer, position));
		position += Types.SHORT_SIZE;
		halluchs.setStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		halluchs.checkSum = MarshalByte.readFixedString(buffer, position, Halluchs.Len.CHECK_SUM);
	}

	public byte[] getHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, halluchs.getLength2());
		position += Types.SHORT_SIZE;
		halluchs.getStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		MarshalByte.writeString(buffer, position, halluchs.checkSum, Halluchs.Len.CHECK_SUM);
		return buffer;
	}

	/**Original name: WS-DATA-UMT-AREA<br>
	 * <pre>* DATA RESPONSE UMT AREA</pre>*/
	public byte[] getWsDataUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_DATA_UMT_AREA];
		return getWsDataUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsDataUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		halludat.getCommonBytes(buffer, position);
		return buffer;
	}

	public void initWsDataUmtAreaSpaces() {
		halludat.initHalludatSpaces();
	}

	public void initWsHdrUmtAreaSpaces() {
		halluhdr.initHalluhdrSpaces();
	}

	public void setWsDataPrivacyInfoFormatted(String data) {
		byte[] buffer = new byte[Len.WS_DATA_PRIVACY_INFO];
		MarshalByte.writeString(buffer, 1, data, Len.WS_DATA_PRIVACY_INFO);
		setWsDataPrivacyInfoBytes(buffer, 1);
	}

	public String getWsDataPrivacyInfoFormatted() {
		return getCidpPassedInfoFormatted();
	}

	public void setWsDataPrivacyInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setCidpPassedInfoBytes(buffer, position);
	}

	public String getCidpPassedInfoFormatted() {
		return MarshalByteExt.bufferToStr(getCidpPassedInfoBytes());
	}

	/**Original name: CIDP-PASSED-INFO<br>
	 * <pre>****************************************************************
	 *  HALLCIDP                                                      *
	 *  COMMON INTERFACE TO DATA PRIVACY                              *
	 *  USED BY BUSINESS DATA OBJECTS TO PASS DATA ROWS FOR           *
	 *  DATA PRIVACY CHECKING.                                        *
	 * ****************************************************************
	 *  SI#       PRGRMR  DATE       DESCRIPTION                      *
	 *  --------- ------- ---------- ---------------------------------*
	 *  SAVANNAH  PM      31MAY2000  INITIAL CODING                   *
	 *  14969     18448   25JUN2001  MADE CHANGES REQUIRED FOR        *
	 *                               ENHANCED DATA PRIVACY AND SET    *
	 *                               DEFAULTS PROCESSING.             *
	 *                               (ARCHITECTURE 2.3)               *
	 * ****************************************************************</pre>*/
	public byte[] getCidpPassedInfoBytes() {
		byte[] buffer = new byte[Len.CIDP_PASSED_INFO];
		return getCidpPassedInfoBytes(buffer, 1);
	}

	public void setCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.setCidpTableInfoBytes(buffer, position);
	}

	public byte[] getCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.getCidpTableInfoBytes(buffer, position);
		return buffer;
	}

	public void setWsSe3CurIsoDateTimeFormatted(String data) {
		byte[] buffer = new byte[Len.WS_SE3_CUR_ISO_DATE_TIME];
		MarshalByte.writeString(buffer, 1, data, Len.WS_SE3_CUR_ISO_DATE_TIME);
		setWsSe3CurIsoDateTimeBytes(buffer, 1);
	}

	public void setWsSe3CurIsoDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		wsSe3CurIsoDate = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_DATE);
		position += Len.WS_SE3_CUR_ISO_DATE;
		wsSe3CurIsoTime = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_TIME);
	}

	public void setWsSe3CurIsoDate(String wsSe3CurIsoDate) {
		this.wsSe3CurIsoDate = Functions.subString(wsSe3CurIsoDate, Len.WS_SE3_CUR_ISO_DATE);
	}

	public String getWsSe3CurIsoDate() {
		return this.wsSe3CurIsoDate;
	}

	public void setWsSe3CurIsoTime(String wsSe3CurIsoTime) {
		this.wsSe3CurIsoTime = Functions.subString(wsSe3CurIsoTime, Len.WS_SE3_CUR_ISO_TIME);
	}

	public String getWsSe3CurIsoTime() {
		return this.wsSe3CurIsoTime;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public CidpTableInfo getCidpTableInfo() {
		return cidpTableInfo;
	}

	public DateStructureCawpcorc getDateStructure() {
		return dateStructure;
	}

	public DclhalBoAudTgrV getDclhalBoAudTgrV() {
		return dclhalBoAudTgrV;
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclhalUowObjHierV getDclhalUowObjHierV() {
		return dclhalUowObjHierV;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Halluchs getHalluchs() {
		return halluchs;
	}

	public Halludat getHalludat() {
		return halludat;
	}

	public Halluhdr getHalluhdr() {
		return halluhdr;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	public WsHalrlodrLinkage getHalrlodrLockDriverStorage() {
		return halrlodrLockDriverStorage;
	}

	public HalrmonPerfMonitorStorage getHalrmonPerfMonitorStorage() {
		return halrmonPerfMonitorStorage;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WsBdoSwitches getWsBdoSwitches() {
		return wsBdoSwitches;
	}

	public WsBdoWorkFields getWsBdoWorkFields() {
		return wsBdoWorkFields;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSpecificMiscXz0d0005 getWsSpecificMisc() {
		return wsSpecificMisc;
	}

	public Xz0n0005 getXz0n0005() {
		return xz0n0005;
	}

	public Xzc005ActNotWrdRow getXzc005ActNotWrdRow() {
		return xzc005ActNotWrdRow;
	}

	public Xzh002ActNotPolRow getXzh002ActNotPolRow() {
		return xzh002ActNotPolRow;
	}

	public DclactNotWrd getXzh005ActNotWrdRow() {
		return xzh005ActNotWrdRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_SE3_CUR_ISO_DATE_TIME = WS_SE3_CUR_ISO_DATE + WS_SE3_CUR_ISO_TIME;
		public static final int WS_DATA_UMT_AREA = Halludat.Len.COMMON;
		public static final int CIDP_PASSED_INFO = CidpTableInfo.Len.CIDP_TABLE_INFO;
		public static final int WS_DATA_PRIVACY_INFO = CIDP_PASSED_INFO;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;
		public static final int HALOUCHS_LINKAGE = Halluchs.Len.LENGTH2 + Halluchs.Len.STRING_FLD + Halluchs.Len.CHECK_SUM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
