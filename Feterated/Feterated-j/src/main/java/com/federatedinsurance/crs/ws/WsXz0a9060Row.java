/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9060-ROW<br>
 * Variable: WS-XZ0A9060-ROW from program XZ0B9060<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9060Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA960-MAX-POL-ROWS
	private short maxPolRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA960-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA960-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA960-NOT-DT
	private String notDt = DefaultValues.stringVal(Len.NOT_DT);
	//Original name: XZA960-TOT-FEE-AMT
	private AfDecimal totFeeAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: XZA960-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZA960-DSY-PRT-GRP
	private String dsyPrtGrp = DefaultValues.stringVal(Len.DSY_PRT_GRP);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9060_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9060RowBytes(buf);
	}

	public String getWsXz0a9060RowFormatted() {
		return getGetActNotDetailFormatted();
	}

	public void setWsXz0a9060RowBytes(byte[] buffer) {
		setWsXz0a9060RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9060RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9060_ROW];
		return getWsXz0a9060RowBytes(buffer, 1);
	}

	public void setWsXz0a9060RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetActNotDetailBytes(buffer, position);
	}

	public byte[] getWsXz0a9060RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetActNotDetailBytes(buffer, position);
		return buffer;
	}

	public String getGetActNotDetailFormatted() {
		return MarshalByteExt.bufferToStr(getGetActNotDetailBytes());
	}

	/**Original name: XZA960-GET-ACT-NOT-DETAIL<br>
	 * <pre>*************************************************************
	 *  XZ0A9060 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_LAST_IMP_CN                        *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  26JAN2009 E404DLP    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetActNotDetailBytes() {
		byte[] buffer = new byte[Len.GET_ACT_NOT_DETAIL];
		return getGetActNotDetailBytes(buffer, 1);
	}

	public void setGetActNotDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		maxPolRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		notDt = MarshalByte.readString(buffer, position, Len.NOT_DT);
		position += Len.NOT_DT;
		totFeeAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.TOT_FEE_AMT, Len.Fract.TOT_FEE_AMT));
		position += Len.TOT_FEE_AMT;
		stAbb = MarshalByte.readString(buffer, position, Len.ST_ABB);
		position += Len.ST_ABB;
		dsyPrtGrp = MarshalByte.readString(buffer, position, Len.DSY_PRT_GRP);
	}

	public byte[] getGetActNotDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxPolRows);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, notDt, Len.NOT_DT);
		position += Len.NOT_DT;
		MarshalByte.writeDecimal(buffer, position, totFeeAmt.copy());
		position += Len.TOT_FEE_AMT;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeString(buffer, position, dsyPrtGrp, Len.DSY_PRT_GRP);
		return buffer;
	}

	public void setMaxPolRows(short maxPolRows) {
		this.maxPolRows = maxPolRows;
	}

	public short getMaxPolRows() {
		return this.maxPolRows;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setNotDt(String notDt) {
		this.notDt = Functions.subString(notDt, Len.NOT_DT);
	}

	public String getNotDt() {
		return this.notDt;
	}

	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		this.totFeeAmt.assign(totFeeAmt);
	}

	public AfDecimal getTotFeeAmt() {
		return this.totFeeAmt.copy();
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public void setDsyPrtGrp(String dsyPrtGrp) {
		this.dsyPrtGrp = Functions.subString(dsyPrtGrp, Len.DSY_PRT_GRP);
	}

	public String getDsyPrtGrp() {
		return this.dsyPrtGrp;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9060RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_POL_ROWS = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int NOT_DT = 10;
		public static final int TOT_FEE_AMT = 10;
		public static final int ST_ABB = 2;
		public static final int DSY_PRT_GRP = 5;
		public static final int GET_ACT_NOT_DETAIL = MAX_POL_ROWS + CSR_ACT_NBR + NOT_PRC_TS + NOT_DT + TOT_FEE_AMT + ST_ABB + DSY_PRT_GRP;
		public static final int WS_XZ0A9060_ROW = GET_ACT_NOT_DETAIL;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
