/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Xzt006ServiceInputs;
import com.federatedinsurance.crs.copy.Xzt006ServiceOutputs;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.enums.Xzt006BypassSyncpointInd;
import com.federatedinsurance.crs.ws.enums.Xzt006Operation;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program XZ0G0006<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaXz0g0006 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZT006-SERVICE-INPUTS
	private Xzt006ServiceInputs serviceInputs = new Xzt006ServiceInputs();
	//Original name: XZT006-SERVICE-OUTPUTS
	private Xzt006ServiceOutputs serviceOutputs = new Xzt006ServiceOutputs();
	//Original name: XZT006-OPERATION
	private Xzt006Operation operation = new Xzt006Operation();
	//Original name: XZT006-BYPASS-SYNCPOINT-IND
	private Xzt006BypassSyncpointInd bypassSyncpointInd = new Xzt006BypassSyncpointInd();
	//Original name: XZT006-ERROR-RETURN-CODE
	private DsdErrorReturnCode errorReturnCode = new DsdErrorReturnCode();
	//Original name: XZT006-ERROR-MESSAGE
	private String errorMessage = DefaultValues.stringVal(Len.ERROR_MESSAGE);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		serviceInputs.setServiceInputsBytes(buffer, position);
		position += Xzt006ServiceInputs.Len.SERVICE_INPUTS;
		serviceOutputs.setServiceOutputsBytes(buffer, position);
		position += Xzt006ServiceOutputs.Len.SERVICE_OUTPUTS;
		setServiceParametersBytes(buffer, position);
		position += Len.SERVICE_PARAMETERS;
		setServiceErrorInfoBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		serviceInputs.getServiceInputsBytes(buffer, position);
		position += Xzt006ServiceInputs.Len.SERVICE_INPUTS;
		serviceOutputs.getServiceOutputsBytes(buffer, position);
		position += Xzt006ServiceOutputs.Len.SERVICE_OUTPUTS;
		getServiceParametersBytes(buffer, position);
		position += Len.SERVICE_PARAMETERS;
		getServiceErrorInfoBytes(buffer, position);
		return buffer;
	}

	public String getServiceParametersFormatted() {
		return MarshalByteExt.bufferToStr(getServiceParametersBytes());
	}

	/**Original name: XZT006-SERVICE-PARAMETERS<br>
	 * <pre>** PASS/CAPTURE REST OF SERVICE INFORMATION</pre>*/
	public byte[] getServiceParametersBytes() {
		byte[] buffer = new byte[Len.SERVICE_PARAMETERS];
		return getServiceParametersBytes(buffer, 1);
	}

	public void setServiceParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		operation.setOperation(MarshalByte.readString(buffer, position, Xzt006Operation.Len.OPERATION));
		position += Xzt006Operation.Len.OPERATION;
		bypassSyncpointInd.setBypassSyncpointInd(MarshalByte.readChar(buffer, position));
	}

	public byte[] getServiceParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, operation.getOperation(), Xzt006Operation.Len.OPERATION);
		position += Xzt006Operation.Len.OPERATION;
		MarshalByte.writeChar(buffer, position, bypassSyncpointInd.getBypassSyncpointInd());
		return buffer;
	}

	public void setServiceErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		errorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		errorMessage = MarshalByte.readString(buffer, position, Len.ERROR_MESSAGE);
	}

	public byte[] getServiceErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, errorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, errorMessage, Len.ERROR_MESSAGE);
		return buffer;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = Functions.subString(errorMessage, Len.ERROR_MESSAGE);
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public Xzt006BypassSyncpointInd getBypassSyncpointInd() {
		return bypassSyncpointInd;
	}

	public DsdErrorReturnCode getErrorReturnCode() {
		return errorReturnCode;
	}

	public Xzt006Operation getOperation() {
		return operation;
	}

	public Xzt006ServiceInputs getServiceInputs() {
		return serviceInputs;
	}

	public Xzt006ServiceOutputs getServiceOutputs() {
		return serviceOutputs;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SERVICE_PARAMETERS = Xzt006Operation.Len.OPERATION + Xzt006BypassSyncpointInd.Len.BYPASS_SYNCPOINT_IND;
		public static final int ERROR_MESSAGE = 250;
		public static final int SERVICE_ERROR_INFO = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + ERROR_MESSAGE;
		public static final int DFHCOMMAREA = Xzt006ServiceInputs.Len.SERVICE_INPUTS + Xzt006ServiceOutputs.Len.SERVICE_OUTPUTS + SERVICE_PARAMETERS
				+ SERVICE_ERROR_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
