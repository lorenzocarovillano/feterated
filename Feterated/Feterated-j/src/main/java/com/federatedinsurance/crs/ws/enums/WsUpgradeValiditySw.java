/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-UPGRADE-VALIDITY-SW<br>
 * Variable: WS-UPGRADE-VALIDITY-SW from program HALRLODR<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsUpgradeValiditySw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char VALID = 'V';
	public static final char INVALID = 'I';

	//==== METHODS ====
	public void setUpgradeValiditySw(char upgradeValiditySw) {
		this.value = upgradeValiditySw;
	}

	public char getUpgradeValiditySw() {
		return this.value;
	}

	public boolean isValid() {
		return value == VALID;
	}

	public void setValid() {
		value = VALID;
	}

	public void setInvalid() {
		value = INVALID;
	}
}
