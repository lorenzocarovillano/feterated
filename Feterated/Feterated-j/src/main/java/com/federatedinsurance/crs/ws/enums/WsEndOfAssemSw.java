/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-END-OF-ASSEM-SW<br>
 * Variable: WS-END-OF-ASSEM-SW from program HALRPLAC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEndOfAssemSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_OF_ASSEM = 'Y';
	public static final char NOT_END_OF_ASSEM = 'N';

	//==== METHODS ====
	public void setEndOfAssemSw(char endOfAssemSw) {
		this.value = endOfAssemSw;
	}

	public char getEndOfAssemSw() {
		return this.value;
	}

	public boolean isEndOfAssem() {
		return value == END_OF_ASSEM;
	}

	public void setEndOfAssem() {
		value = END_OF_ASSEM;
	}

	public void setNotEndOfAssem() {
		value = NOT_END_OF_ASSEM;
	}
}
