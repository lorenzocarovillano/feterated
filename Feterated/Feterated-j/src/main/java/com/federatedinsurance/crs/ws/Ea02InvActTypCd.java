/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-INV-ACT-TYP-CD<br>
 * Variable: EA-02-INV-ACT-TYP-CD from program XZ0D0001<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02InvActTypCd {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-INV-ACT-TYP-CD
	private String flr1 = "Account type";
	//Original name: FILLER-EA-02-INV-ACT-TYP-CD-1
	private String flr2 = "code is";
	//Original name: EA-02-ACT-TYP-CD
	private String actTypCd = DefaultValues.stringVal(Len.ACT_TYP_CD);
	//Original name: FILLER-EA-02-INV-ACT-TYP-CD-2
	private String flr3 = ", but must be";
	//Original name: FILLER-EA-02-INV-ACT-TYP-CD-3
	private String flr4 = "CL or PL for";
	//Original name: FILLER-EA-02-INV-ACT-TYP-CD-4
	private String flr5 = "account";
	//Original name: EA-02-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-02-INV-ACT-TYP-CD-5
	private String flr6 = ", process time";
	//Original name: FILLER-EA-02-INV-ACT-TYP-CD-6
	private String flr7 = "stamp date";
	//Original name: EA-02-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-02-INV-ACT-TYP-CD-7
	private char flr8 = '.';

	//==== METHODS ====
	public String getEa02InvActTypCdFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvActTypCdBytes());
	}

	public byte[] getEa02InvActTypCdBytes() {
		byte[] buffer = new byte[Len.EA02_INV_ACT_TYP_CD];
		return getEa02InvActTypCdBytes(buffer, 1);
	}

	public byte[] getEa02InvActTypCdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, actTypCd, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr8);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setActTypCd(String actTypCd) {
		this.actTypCd = Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	public String getActTypCd() {
		return this.actTypCd;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_TYP_CD = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 13;
		public static final int FLR2 = 8;
		public static final int FLR3 = 14;
		public static final int FLR7 = 11;
		public static final int FLR8 = 1;
		public static final int EA02_INV_ACT_TYP_CD = ACT_TYP_CD + CSR_ACT_NBR + NOT_PRC_TS + 2 * FLR1 + 2 * FLR2 + 2 * FLR3 + FLR7 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
