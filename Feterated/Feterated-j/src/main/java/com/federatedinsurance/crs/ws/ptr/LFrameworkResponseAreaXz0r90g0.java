/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R90G0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r90g0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A90G1_MAXOCCURS = 1000;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r90g0() {
	}

	public LFrameworkResponseAreaXz0r90g0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza9g0MaxRecRows(short xza9g0MaxRecRows) {
		writeBinaryShort(Pos.XZA9G0_MAX_REC_ROWS, xza9g0MaxRecRows);
	}

	/**Original name: XZA9G0-MAX-REC-ROWS<br>*/
	public short getXza9g0MaxRecRows() {
		return readBinaryShort(Pos.XZA9G0_MAX_REC_ROWS);
	}

	public void setXza9g0CsrActNbr(String xza9g0CsrActNbr) {
		writeString(Pos.XZA9G0_CSR_ACT_NBR, xza9g0CsrActNbr, Len.XZA9G0_CSR_ACT_NBR);
	}

	/**Original name: XZA9G0-CSR-ACT-NBR<br>*/
	public String getXza9g0CsrActNbr() {
		return readString(Pos.XZA9G0_CSR_ACT_NBR, Len.XZA9G0_CSR_ACT_NBR);
	}

	public void setXza9g0NotPrcTs(String xza9g0NotPrcTs) {
		writeString(Pos.XZA9G0_NOT_PRC_TS, xza9g0NotPrcTs, Len.XZA9G0_NOT_PRC_TS);
	}

	/**Original name: XZA9G0-NOT-PRC-TS<br>*/
	public String getXza9g0NotPrcTs() {
		return readString(Pos.XZA9G0_NOT_PRC_TS, Len.XZA9G0_NOT_PRC_TS);
	}

	public void setXza9g0RecallCertNbr(String xza9g0RecallCertNbr) {
		writeString(Pos.XZA9G0_RECALL_CERT_NBR, xza9g0RecallCertNbr, Len.XZA9G0_RECALL_CERT_NBR);
	}

	/**Original name: XZA9G0-RECALL-CERT-NBR<br>*/
	public String getXza9g0RecallCertNbr() {
		return readString(Pos.XZA9G0_RECALL_CERT_NBR, Len.XZA9G0_RECALL_CERT_NBR);
	}

	public void setXza9g0Userid(String xza9g0Userid) {
		writeString(Pos.XZA9G0_USERID, xza9g0Userid, Len.XZA9G0_USERID);
	}

	/**Original name: XZA9G0-USERID<br>*/
	public String getXza9g0Userid() {
		return readString(Pos.XZA9G0_USERID, Len.XZA9G0_USERID);
	}

	public String getlFwRespXz0a90g1Formatted(int lFwRespXz0a90g1Idx) {
		int position = Pos.lFwRespXz0a90g1(lFwRespXz0a90g1Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A90G1);
	}

	public void setXza9g1rCertNbr(int xza9g1rCertNbrIdx, String xza9g1rCertNbr) {
		int position = Pos.xza9g1CertNbr(xza9g1rCertNbrIdx - 1);
		writeString(position, xza9g1rCertNbr, Len.XZA9G1_CERT_NBR);
	}

	/**Original name: XZA9G1R-CERT-NBR<br>*/
	public String getXza9g1rCertNbr(int xza9g1rCertNbrIdx) {
		int position = Pos.xza9g1CertNbr(xza9g1rCertNbrIdx - 1);
		return readString(position, Len.XZA9G1_CERT_NBR);
	}

	public void setXza9g1rNmAdrLin1(int xza9g1rNmAdrLin1Idx, String xza9g1rNmAdrLin1) {
		int position = Pos.xza9g1NmAdrLin1(xza9g1rNmAdrLin1Idx - 1);
		writeString(position, xza9g1rNmAdrLin1, Len.XZA9G1_NM_ADR_LIN1);
	}

	/**Original name: XZA9G1R-NM-ADR-LIN-1<br>*/
	public String getXza9g1rNmAdrLin1(int xza9g1rNmAdrLin1Idx) {
		int position = Pos.xza9g1NmAdrLin1(xza9g1rNmAdrLin1Idx - 1);
		return readString(position, Len.XZA9G1_NM_ADR_LIN1);
	}

	public void setXza9g1rNmAdrLin2(int xza9g1rNmAdrLin2Idx, String xza9g1rNmAdrLin2) {
		int position = Pos.xza9g1NmAdrLin2(xza9g1rNmAdrLin2Idx - 1);
		writeString(position, xza9g1rNmAdrLin2, Len.XZA9G1_NM_ADR_LIN2);
	}

	/**Original name: XZA9G1R-NM-ADR-LIN-2<br>*/
	public String getXza9g1rNmAdrLin2(int xza9g1rNmAdrLin2Idx) {
		int position = Pos.xza9g1NmAdrLin2(xza9g1rNmAdrLin2Idx - 1);
		return readString(position, Len.XZA9G1_NM_ADR_LIN2);
	}

	public void setXza9g1rNmAdrLin3(int xza9g1rNmAdrLin3Idx, String xza9g1rNmAdrLin3) {
		int position = Pos.xza9g1NmAdrLin3(xza9g1rNmAdrLin3Idx - 1);
		writeString(position, xza9g1rNmAdrLin3, Len.XZA9G1_NM_ADR_LIN3);
	}

	/**Original name: XZA9G1R-NM-ADR-LIN-3<br>*/
	public String getXza9g1rNmAdrLin3(int xza9g1rNmAdrLin3Idx) {
		int position = Pos.xza9g1NmAdrLin3(xza9g1rNmAdrLin3Idx - 1);
		return readString(position, Len.XZA9G1_NM_ADR_LIN3);
	}

	public void setXza9g1rNmAdrLin4(int xza9g1rNmAdrLin4Idx, String xza9g1rNmAdrLin4) {
		int position = Pos.xza9g1NmAdrLin4(xza9g1rNmAdrLin4Idx - 1);
		writeString(position, xza9g1rNmAdrLin4, Len.XZA9G1_NM_ADR_LIN4);
	}

	/**Original name: XZA9G1R-NM-ADR-LIN-4<br>*/
	public String getXza9g1rNmAdrLin4(int xza9g1rNmAdrLin4Idx) {
		int position = Pos.xza9g1NmAdrLin4(xza9g1rNmAdrLin4Idx - 1);
		return readString(position, Len.XZA9G1_NM_ADR_LIN4);
	}

	public void setXza9g1rNmAdrLin5(int xza9g1rNmAdrLin5Idx, String xza9g1rNmAdrLin5) {
		int position = Pos.xza9g1NmAdrLin5(xza9g1rNmAdrLin5Idx - 1);
		writeString(position, xza9g1rNmAdrLin5, Len.XZA9G1_NM_ADR_LIN5);
	}

	/**Original name: XZA9G1R-NM-ADR-LIN-5<br>*/
	public String getXza9g1rNmAdrLin5(int xza9g1rNmAdrLin5Idx) {
		int position = Pos.xza9g1NmAdrLin5(xza9g1rNmAdrLin5Idx - 1);
		return readString(position, Len.XZA9G1_NM_ADR_LIN5);
	}

	public void setXza9g1rNmAdrLin6(int xza9g1rNmAdrLin6Idx, String xza9g1rNmAdrLin6) {
		int position = Pos.xza9g1NmAdrLin6(xza9g1rNmAdrLin6Idx - 1);
		writeString(position, xza9g1rNmAdrLin6, Len.XZA9G1_NM_ADR_LIN6);
	}

	/**Original name: XZA9G1R-NM-ADR-LIN-6<br>*/
	public String getXza9g1rNmAdrLin6(int xza9g1rNmAdrLin6Idx) {
		int position = Pos.xza9g1NmAdrLin6(xza9g1rNmAdrLin6Idx - 1);
		return readString(position, Len.XZA9G1_NM_ADR_LIN6);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_XZ0A90G0 = L_FRAMEWORK_RESPONSE_AREA;
		public static final int XZA9G0_GET_CERT_REC_LIST_KEY = L_FW_RESP_XZ0A90G0;
		public static final int XZA9G0_MAX_REC_ROWS = XZA9G0_GET_CERT_REC_LIST_KEY;
		public static final int XZA9G0_CSR_ACT_NBR = XZA9G0_MAX_REC_ROWS + Len.XZA9G0_MAX_REC_ROWS;
		public static final int XZA9G0_NOT_PRC_TS = XZA9G0_CSR_ACT_NBR + Len.XZA9G0_CSR_ACT_NBR;
		public static final int XZA9G0_RECALL_CERT_NBR = XZA9G0_NOT_PRC_TS + Len.XZA9G0_NOT_PRC_TS;
		public static final int XZA9G0_USERID = XZA9G0_RECALL_CERT_NBR + Len.XZA9G0_RECALL_CERT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a90g1(int idx) {
			return XZA9G0_USERID + Len.XZA9G0_USERID + idx * Len.L_FW_RESP_XZ0A90G1;
		}

		public static int xza9g1GetCertRecDetail(int idx) {
			return lFwRespXz0a90g1(idx);
		}

		public static int xza9g1CertNbr(int idx) {
			return xza9g1GetCertRecDetail(idx);
		}

		public static int xza9g1NmAdrLin1(int idx) {
			return xza9g1CertNbr(idx) + Len.XZA9G1_CERT_NBR;
		}

		public static int xza9g1NmAdrLin2(int idx) {
			return xza9g1NmAdrLin1(idx) + Len.XZA9G1_NM_ADR_LIN1;
		}

		public static int xza9g1NmAdrLin3(int idx) {
			return xza9g1NmAdrLin2(idx) + Len.XZA9G1_NM_ADR_LIN2;
		}

		public static int xza9g1NmAdrLin4(int idx) {
			return xza9g1NmAdrLin3(idx) + Len.XZA9G1_NM_ADR_LIN3;
		}

		public static int xza9g1NmAdrLin5(int idx) {
			return xza9g1NmAdrLin4(idx) + Len.XZA9G1_NM_ADR_LIN4;
		}

		public static int xza9g1NmAdrLin6(int idx) {
			return xza9g1NmAdrLin5(idx) + Len.XZA9G1_NM_ADR_LIN5;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9G0_MAX_REC_ROWS = 2;
		public static final int XZA9G0_CSR_ACT_NBR = 9;
		public static final int XZA9G0_NOT_PRC_TS = 26;
		public static final int XZA9G0_RECALL_CERT_NBR = 25;
		public static final int XZA9G0_USERID = 8;
		public static final int XZA9G1_CERT_NBR = 25;
		public static final int XZA9G1_NM_ADR_LIN1 = 30;
		public static final int XZA9G1_NM_ADR_LIN2 = 30;
		public static final int XZA9G1_NM_ADR_LIN3 = 30;
		public static final int XZA9G1_NM_ADR_LIN4 = 30;
		public static final int XZA9G1_NM_ADR_LIN5 = 30;
		public static final int XZA9G1_NM_ADR_LIN6 = 30;
		public static final int XZA9G1_GET_CERT_REC_DETAIL = XZA9G1_CERT_NBR + XZA9G1_NM_ADR_LIN1 + XZA9G1_NM_ADR_LIN2 + XZA9G1_NM_ADR_LIN3
				+ XZA9G1_NM_ADR_LIN4 + XZA9G1_NM_ADR_LIN5 + XZA9G1_NM_ADR_LIN6;
		public static final int L_FW_RESP_XZ0A90G1 = XZA9G1_GET_CERT_REC_DETAIL;
		public static final int XZA9G0_GET_CERT_REC_LIST_KEY = XZA9G0_MAX_REC_ROWS + XZA9G0_CSR_ACT_NBR + XZA9G0_NOT_PRC_TS + XZA9G0_RECALL_CERT_NBR
				+ XZA9G0_USERID;
		public static final int L_FW_RESP_XZ0A90G0 = XZA9G0_GET_CERT_REC_LIST_KEY;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_XZ0A90G0
				+ LFrameworkResponseAreaXz0r90g0.L_FW_RESP_XZ0A90G1_MAXOCCURS * L_FW_RESP_XZ0A90G1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
