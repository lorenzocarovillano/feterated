/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.occurs.Xz003oExtMsgStaLis;
import com.federatedinsurance.crs.ws.occurs.Xz003oPolCovInf;

/**Original name: XZ03CI1O<br>
 * Variable: XZ03CI1O from copybook XZ03CI1O<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz03ci1o {

	//==== PROPERTIES ====
	public static final int POL_COV_INF_MAXOCCURS = 50;
	public static final int EXT_MSG_STA_LIS_MAXOCCURS = 50;
	//Original name: XZ003O-POL-COV-INF
	private Xz003oPolCovInf[] polCovInf = new Xz003oPolCovInf[POL_COV_INF_MAXOCCURS];
	//Original name: XZ003O-MSG-STA-CD
	private String msgStaCd = DefaultValues.stringVal(Len.MSG_STA_CD);
	//Original name: XZ003O-MSG-ERR-CD
	private String msgErrCd = DefaultValues.stringVal(Len.MSG_ERR_CD);
	//Original name: XZ003O-MSG-STA-DES
	private String msgStaDes = DefaultValues.stringVal(Len.MSG_STA_DES);
	//Original name: XZ003O-EXT-MSG-STA-LIS
	private Xz003oExtMsgStaLis[] extMsgStaLis = new Xz003oExtMsgStaLis[EXT_MSG_STA_LIS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Xz03ci1o() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int polCovInfIdx = 1; polCovInfIdx <= POL_COV_INF_MAXOCCURS; polCovInfIdx++) {
			polCovInf[polCovInfIdx - 1] = new Xz003oPolCovInf();
		}
		for (int extMsgStaLisIdx = 1; extMsgStaLisIdx <= EXT_MSG_STA_LIS_MAXOCCURS; extMsgStaLisIdx++) {
			extMsgStaLis[extMsgStaLisIdx - 1] = new Xz003oExtMsgStaLis();
		}
	}

	public void initXz03ci1oLowValues() {
		initGetCerPolLisLowValues();
	}

	public void setGetCerPolLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POL_COV_INF_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				polCovInf[idx - 1].setPolCovInfBytes(buffer, position);
				position += Xz003oPolCovInf.Len.POL_COV_INF;
			} else {
				polCovInf[idx - 1].initPolCovInfSpaces();
				position += Xz003oPolCovInf.Len.POL_COV_INF;
			}
		}
		setMsgInfBytes(buffer, position);
	}

	public byte[] getGetCerPolLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POL_COV_INF_MAXOCCURS; idx++) {
			polCovInf[idx - 1].getPolCovInfBytes(buffer, position);
			position += Xz003oPolCovInf.Len.POL_COV_INF;
		}
		getMsgInfBytes(buffer, position);
		return buffer;
	}

	public void initGetCerPolLisLowValues() {
		for (int idx = 1; idx <= POL_COV_INF_MAXOCCURS; idx++) {
			polCovInf[idx - 1].initPolCovInfLowValues();
		}
		initMsgInfLowValues();
	}

	public void setMsgInfBytes(byte[] buffer, int offset) {
		int position = offset;
		msgStaCd = MarshalByte.readString(buffer, position, Len.MSG_STA_CD);
		position += Len.MSG_STA_CD;
		msgErrCd = MarshalByte.readString(buffer, position, Len.MSG_ERR_CD);
		position += Len.MSG_ERR_CD;
		msgStaDes = MarshalByte.readString(buffer, position, Len.MSG_STA_DES);
		position += Len.MSG_STA_DES;
		for (int idx = 1; idx <= EXT_MSG_STA_LIS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				extMsgStaLis[idx - 1].setExtMsgStaLisBytes(buffer, position);
				position += Xz003oExtMsgStaLis.Len.EXT_MSG_STA_LIS;
			} else {
				extMsgStaLis[idx - 1].initExtMsgStaLisSpaces();
				position += Xz003oExtMsgStaLis.Len.EXT_MSG_STA_LIS;
			}
		}
	}

	public byte[] getMsgInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, msgStaCd, Len.MSG_STA_CD);
		position += Len.MSG_STA_CD;
		MarshalByte.writeString(buffer, position, msgErrCd, Len.MSG_ERR_CD);
		position += Len.MSG_ERR_CD;
		MarshalByte.writeString(buffer, position, msgStaDes, Len.MSG_STA_DES);
		position += Len.MSG_STA_DES;
		for (int idx = 1; idx <= EXT_MSG_STA_LIS_MAXOCCURS; idx++) {
			extMsgStaLis[idx - 1].getExtMsgStaLisBytes(buffer, position);
			position += Xz003oExtMsgStaLis.Len.EXT_MSG_STA_LIS;
		}
		return buffer;
	}

	public void initMsgInfLowValues() {
		msgStaCd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.MSG_STA_CD);
		msgErrCd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.MSG_ERR_CD);
		msgStaDes = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.MSG_STA_DES);
		for (int idx = 1; idx <= EXT_MSG_STA_LIS_MAXOCCURS; idx++) {
			extMsgStaLis[idx - 1].initExtMsgStaLisLowValues();
		}
	}

	public void setMsgStaCd(String msgStaCd) {
		this.msgStaCd = Functions.subString(msgStaCd, Len.MSG_STA_CD);
	}

	public String getMsgStaCd() {
		return this.msgStaCd;
	}

	public void setMsgErrCd(String msgErrCd) {
		this.msgErrCd = Functions.subString(msgErrCd, Len.MSG_ERR_CD);
	}

	public String getMsgErrCd() {
		return this.msgErrCd;
	}

	public void setMsgStaDes(String msgStaDes) {
		this.msgStaDes = Functions.subString(msgStaDes, Len.MSG_STA_DES);
	}

	public String getMsgStaDes() {
		return this.msgStaDes;
	}

	public Xz003oExtMsgStaLis getExtMsgStaLis(int idx) {
		return extMsgStaLis[idx - 1];
	}

	public Xz003oPolCovInf getPolCovInf(int idx) {
		return polCovInf[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MSG_STA_CD = 25;
		public static final int MSG_ERR_CD = 30;
		public static final int MSG_STA_DES = 255;
		public static final int MSG_INF = MSG_STA_CD + MSG_ERR_CD + MSG_STA_DES
				+ Xz03ci1o.EXT_MSG_STA_LIS_MAXOCCURS * Xz003oExtMsgStaLis.Len.EXT_MSG_STA_LIS;
		public static final int GET_CER_POL_LIS = Xz03ci1o.POL_COV_INF_MAXOCCURS * Xz003oPolCovInf.Len.POL_COV_INF + MSG_INF;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
