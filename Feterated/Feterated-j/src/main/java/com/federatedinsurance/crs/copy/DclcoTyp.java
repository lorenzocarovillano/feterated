/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLCO-TYP<br>
 * Variable: DCLCO-TYP from copybook XZH00024<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclcoTyp {

	//==== PROPERTIES ====
	//Original name: CO-OFS-NBR
	private short coOfsNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: CO-DES
	private String coDes = DefaultValues.stringVal(Len.CO_DES);

	//==== METHODS ====
	public void setCoOfsNbr(short coOfsNbr) {
		this.coOfsNbr = coOfsNbr;
	}

	public short getCoOfsNbr() {
		return this.coOfsNbr;
	}

	public void setCoDes(String coDes) {
		this.coDes = Functions.subString(coDes, Len.CO_DES);
	}

	public String getCoDes() {
		return this.coDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CO_DES = 30;
		public static final int CO_CD = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
