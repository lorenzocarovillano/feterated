/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZA971-ACY-OVL-INFO<br>
 * Variable: XZA971-ACY-OVL-INFO from copybook XZ0A9071<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xza971AcyOvlInfo {

	//==== PROPERTIES ====
	//Original name: XZA971-FRM-NBR
	private String frmNbr = DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: XZA971-FRM-EDT-DT
	private String frmEdtDt = DefaultValues.stringVal(Len.FRM_EDT_DT);
	//Original name: XZA971-REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);
	//Original name: XZA971-OVL-EDL-FRM-NM
	private String ovlEdlFrmNm = DefaultValues.stringVal(Len.OVL_EDL_FRM_NM);
	//Original name: XZA971-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZA971-SPE-PRC-CD
	private String spePrcCd = DefaultValues.stringVal(Len.SPE_PRC_CD);
	//Original name: XZA971-OVL-SR-CD
	private String ovlSrCd = DefaultValues.stringVal(Len.OVL_SR_CD);
	//Original name: XZA971-OVL-DES
	private String ovlDes = DefaultValues.stringVal(Len.OVL_DES);
	//Original name: XZA971-XCLV-IND
	private char xclvInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setAcyOvlInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		frmNbr = MarshalByte.readString(buffer, position, Len.FRM_NBR);
		position += Len.FRM_NBR;
		frmEdtDt = MarshalByte.readString(buffer, position, Len.FRM_EDT_DT);
		position += Len.FRM_EDT_DT;
		recTypCd = MarshalByte.readString(buffer, position, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		ovlEdlFrmNm = MarshalByte.readString(buffer, position, Len.OVL_EDL_FRM_NM);
		position += Len.OVL_EDL_FRM_NM;
		stAbb = MarshalByte.readString(buffer, position, Len.ST_ABB);
		position += Len.ST_ABB;
		spePrcCd = MarshalByte.readString(buffer, position, Len.SPE_PRC_CD);
		position += Len.SPE_PRC_CD;
		ovlSrCd = MarshalByte.readString(buffer, position, Len.OVL_SR_CD);
		position += Len.OVL_SR_CD;
		ovlDes = MarshalByte.readString(buffer, position, Len.OVL_DES);
		position += Len.OVL_DES;
		xclvInd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getAcyOvlInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, frmNbr, Len.FRM_NBR);
		position += Len.FRM_NBR;
		MarshalByte.writeString(buffer, position, frmEdtDt, Len.FRM_EDT_DT);
		position += Len.FRM_EDT_DT;
		MarshalByte.writeString(buffer, position, recTypCd, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		MarshalByte.writeString(buffer, position, ovlEdlFrmNm, Len.OVL_EDL_FRM_NM);
		position += Len.OVL_EDL_FRM_NM;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeString(buffer, position, spePrcCd, Len.SPE_PRC_CD);
		position += Len.SPE_PRC_CD;
		MarshalByte.writeString(buffer, position, ovlSrCd, Len.OVL_SR_CD);
		position += Len.OVL_SR_CD;
		MarshalByte.writeString(buffer, position, ovlDes, Len.OVL_DES);
		position += Len.OVL_DES;
		MarshalByte.writeChar(buffer, position, xclvInd);
		return buffer;
	}

	public void setFrmNbr(String frmNbr) {
		this.frmNbr = Functions.subString(frmNbr, Len.FRM_NBR);
	}

	public String getFrmNbr() {
		return this.frmNbr;
	}

	public void setFrmEdtDt(String frmEdtDt) {
		this.frmEdtDt = Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	public String getFrmEdtDt() {
		return this.frmEdtDt;
	}

	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	public String getRecTypCd() {
		return this.recTypCd;
	}

	public void setOvlEdlFrmNm(String ovlEdlFrmNm) {
		this.ovlEdlFrmNm = Functions.subString(ovlEdlFrmNm, Len.OVL_EDL_FRM_NM);
	}

	public String getOvlEdlFrmNm() {
		return this.ovlEdlFrmNm;
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public void setSpePrcCd(String spePrcCd) {
		this.spePrcCd = Functions.subString(spePrcCd, Len.SPE_PRC_CD);
	}

	public String getSpePrcCd() {
		return this.spePrcCd;
	}

	public void setOvlSrCd(String ovlSrCd) {
		this.ovlSrCd = Functions.subString(ovlSrCd, Len.OVL_SR_CD);
	}

	public String getOvlSrCd() {
		return this.ovlSrCd;
	}

	public void setOvlDes(String ovlDes) {
		this.ovlDes = Functions.subString(ovlDes, Len.OVL_DES);
	}

	public String getOvlDes() {
		return this.ovlDes;
	}

	public void setXclvInd(char xclvInd) {
		this.xclvInd = xclvInd;
	}

	public char getXclvInd() {
		return this.xclvInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FRM_NBR = 30;
		public static final int FRM_EDT_DT = 10;
		public static final int REC_TYP_CD = 5;
		public static final int OVL_EDL_FRM_NM = 30;
		public static final int ST_ABB = 2;
		public static final int SPE_PRC_CD = 8;
		public static final int OVL_SR_CD = 8;
		public static final int OVL_DES = 30;
		public static final int XCLV_IND = 1;
		public static final int ACY_OVL_INFO = FRM_NBR + FRM_EDT_DT + REC_TYP_CD + OVL_EDL_FRM_NM + ST_ABB + SPE_PRC_CD + OVL_SR_CD + OVL_DES
				+ XCLV_IND;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
