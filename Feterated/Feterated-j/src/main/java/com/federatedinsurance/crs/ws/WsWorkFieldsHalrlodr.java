/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-WORK-FIELDS<br>
 * Variable: WS-WORK-FIELDS from program HALRLODR<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkFieldsHalrlodr {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "HALRLODR";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "SAV_ARCH";
	//Original name: WS-UCT2-LEGACY-LOCK-VIEW
	private String uct2LegacyLockView = "APP_LEGACY_LOCK_INTERFACE";
	//Original name: WS-EXPIRY-TS
	private String expiryTs = "";
	//Original name: WS-LEGACY-LOCK-MODULE
	private String legacyLockModule = "";
	//Original name: WS-UBOC-AUTH-USERID
	private String ubocAuthUserid = "";
	//Original name: WS-LOCK-GROUP-LABEL
	private String lockGroupLabel = "LOCK_GROUP_ASSOCIATION";

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getUct2LegacyLockView() {
		return this.uct2LegacyLockView;
	}

	public void setExpiryTs(String expiryTs) {
		this.expiryTs = Functions.subString(expiryTs, Len.EXPIRY_TS);
	}

	public String getExpiryTs() {
		return this.expiryTs;
	}

	public void setLegacyLockModule(String legacyLockModule) {
		this.legacyLockModule = Functions.subString(legacyLockModule, Len.LEGACY_LOCK_MODULE);
	}

	public String getLegacyLockModule() {
		return this.legacyLockModule;
	}

	public String getLegacyLockModuleFormatted() {
		return Functions.padBlanks(getLegacyLockModule(), Len.LEGACY_LOCK_MODULE);
	}

	public void setUbocAuthUserid(String ubocAuthUserid) {
		this.ubocAuthUserid = Functions.subString(ubocAuthUserid, Len.UBOC_AUTH_USERID);
	}

	public String getUbocAuthUserid() {
		return this.ubocAuthUserid;
	}

	public String getLockGroupLabel() {
		return this.lockGroupLabel;
	}

	public String getLockGroupLabelFormatted() {
		return Functions.padBlanks(getLockGroupLabel(), Len.LOCK_GROUP_LABEL);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EXPIRY_TS = 26;
		public static final int LEGACY_LOCK_MODULE = 32;
		public static final int UBOC_AUTH_USERID = 32;
		public static final int LOCK_GROUP_LABEL = 32;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
