/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalouidgGeneric;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [HALOUIDG_Generic]
 * 
 */
public class HalouidgGenericDao extends BaseSqlDao<IHalouidgGeneric> {

	public HalouidgGenericDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalouidgGeneric> getToClass() {
		return IHalouidgGeneric.class;
	}

	public int setHostVarRec(int dft) {
		return buildQuery("setHostVarRec").scalarResultInt(dft);
	}
}
