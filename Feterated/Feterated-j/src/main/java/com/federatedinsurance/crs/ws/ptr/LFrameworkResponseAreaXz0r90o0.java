/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R90O0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r90o0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A90O0_MAXOCCURS = 1000;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r90o0() {
	}

	public LFrameworkResponseAreaXz0r90o0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a90o0Formatted(int lFwRespXz0a90o0Idx) {
		int position = Pos.lFwRespXz0a90o0(lFwRespXz0a90o0Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A90O0);
	}

	public void setlFwRespXz0a90o0Bytes(int lFwRespXz0a90o0Idx, byte[] buffer) {
		setlFwRespXz0a90o0Bytes(lFwRespXz0a90o0Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A90O0<br>*/
	public byte[] getlFwRespXz0a90o0Bytes(int lFwRespXz0a90o0Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_XZ0A90O0];
		return getlFwRespXz0a90o0Bytes(lFwRespXz0a90o0Idx, buffer, 1);
	}

	public void setlFwRespXz0a90o0Bytes(int lFwRespXz0a90o0Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a90o0(lFwRespXz0a90o0Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A90O0, position);
	}

	public byte[] getlFwRespXz0a90o0Bytes(int lFwRespXz0a90o0Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a90o0(lFwRespXz0a90o0Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A90O0, position);
		return buffer;
	}

	public void setXza9o0rMaxXmlReqRows(int xza9o0rMaxXmlReqRowsIdx, short xza9o0rMaxXmlReqRows) {
		int position = Pos.xza9o0MaxXmlReqRows(xza9o0rMaxXmlReqRowsIdx - 1);
		writeBinaryShort(position, xza9o0rMaxXmlReqRows);
	}

	/**Original name: XZA9O0R-MAX-XML-REQ-ROWS<br>*/
	public short getXza9o0rMaxXmlReqRows(int xza9o0rMaxXmlReqRowsIdx) {
		int position = Pos.xza9o0MaxXmlReqRows(xza9o0rMaxXmlReqRowsIdx - 1);
		return readBinaryShort(position);
	}

	public void setXza9o0rCsrActNbr(int xza9o0rCsrActNbrIdx, String xza9o0rCsrActNbr) {
		int position = Pos.xza9o0CsrActNbr(xza9o0rCsrActNbrIdx - 1);
		writeString(position, xza9o0rCsrActNbr, Len.XZA9O0_CSR_ACT_NBR);
	}

	/**Original name: XZA9O0R-CSR-ACT-NBR<br>*/
	public String getXza9o0rCsrActNbr(int xza9o0rCsrActNbrIdx) {
		int position = Pos.xza9o0CsrActNbr(xza9o0rCsrActNbrIdx - 1);
		return readString(position, Len.XZA9O0_CSR_ACT_NBR);
	}

	public void setXza9o0rNotPrcTs(int xza9o0rNotPrcTsIdx, String xza9o0rNotPrcTs) {
		int position = Pos.xza9o0NotPrcTs(xza9o0rNotPrcTsIdx - 1);
		writeString(position, xza9o0rNotPrcTs, Len.XZA9O0_NOT_PRC_TS);
	}

	/**Original name: XZA9O0R-NOT-PRC-TS<br>*/
	public String getXza9o0rNotPrcTs(int xza9o0rNotPrcTsIdx) {
		int position = Pos.xza9o0NotPrcTs(xza9o0rNotPrcTsIdx - 1);
		return readString(position, Len.XZA9O0_NOT_PRC_TS);
	}

	public void setXza9o0rActNotTypCd(int xza9o0rActNotTypCdIdx, String xza9o0rActNotTypCd) {
		int position = Pos.xza9o0ActNotTypCd(xza9o0rActNotTypCdIdx - 1);
		writeString(position, xza9o0rActNotTypCd, Len.XZA9O0_ACT_NOT_TYP_CD);
	}

	/**Original name: XZA9O0R-ACT-NOT-TYP-CD<br>*/
	public String getXza9o0rActNotTypCd(int xza9o0rActNotTypCdIdx) {
		int position = Pos.xza9o0ActNotTypCd(xza9o0rActNotTypCdIdx - 1);
		return readString(position, Len.XZA9O0_ACT_NOT_TYP_CD);
	}

	public void setXza9o0rNotDt(int xza9o0rNotDtIdx, String xza9o0rNotDt) {
		int position = Pos.xza9o0NotDt(xza9o0rNotDtIdx - 1);
		writeString(position, xza9o0rNotDt, Len.XZA9O0_NOT_DT);
	}

	/**Original name: XZA9O0R-NOT-DT<br>*/
	public String getXza9o0rNotDt(int xza9o0rNotDtIdx) {
		int position = Pos.xza9o0NotDt(xza9o0rNotDtIdx - 1);
		return readString(position, Len.XZA9O0_NOT_DT);
	}

	public void setXza9o0rEmpId(int xza9o0rEmpIdIdx, String xza9o0rEmpId) {
		int position = Pos.xza9o0EmpId(xza9o0rEmpIdIdx - 1);
		writeString(position, xza9o0rEmpId, Len.XZA9O0_EMP_ID);
	}

	/**Original name: XZA9O0R-EMP-ID<br>*/
	public String getXza9o0rEmpId(int xza9o0rEmpIdIdx) {
		int position = Pos.xza9o0EmpId(xza9o0rEmpIdIdx - 1);
		return readString(position, Len.XZA9O0_EMP_ID);
	}

	public void setXza9o0rActTypCd(int xza9o0rActTypCdIdx, String xza9o0rActTypCd) {
		int position = Pos.xza9o0ActTypCd(xza9o0rActTypCdIdx - 1);
		writeString(position, xza9o0rActTypCd, Len.XZA9O0_ACT_TYP_CD);
	}

	/**Original name: XZA9O0R-ACT-TYP-CD<br>*/
	public String getXza9o0rActTypCd(int xza9o0rActTypCdIdx) {
		int position = Pos.xza9o0ActTypCd(xza9o0rActTypCdIdx - 1);
		return readString(position, Len.XZA9O0_ACT_TYP_CD);
	}

	public void setXza9o0rDocDes(int xza9o0rDocDesIdx, String xza9o0rDocDes) {
		int position = Pos.xza9o0DocDes(xza9o0rDocDesIdx - 1);
		writeString(position, xza9o0rDocDes, Len.XZA9O0_DOC_DES);
	}

	/**Original name: XZA9O0R-DOC-DES<br>*/
	public String getXza9o0rDocDes(int xza9o0rDocDesIdx) {
		int position = Pos.xza9o0DocDes(xza9o0rDocDesIdx - 1);
		return readString(position, Len.XZA9O0_DOC_DES);
	}

	public void setXza9o0rTtyCoInd(int xza9o0rTtyCoIndIdx, char xza9o0rTtyCoInd) {
		int position = Pos.xza9o0TtyCoInd(xza9o0rTtyCoIndIdx - 1);
		writeChar(position, xza9o0rTtyCoInd);
	}

	/**Original name: XZA9O0R-TTY-CO-IND<br>*/
	public char getXza9o0rTtyCoInd(int xza9o0rTtyCoIndIdx) {
		int position = Pos.xza9o0TtyCoInd(xza9o0rTtyCoIndIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a90o0(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A90O0;
		}

		public static int xza9o0XmlReqRow(int idx) {
			return lFwRespXz0a90o0(idx);
		}

		public static int xza9o0MaxXmlReqRows(int idx) {
			return xza9o0XmlReqRow(idx);
		}

		public static int xza9o0XmlReqInfo(int idx) {
			return xza9o0MaxXmlReqRows(idx) + Len.XZA9O0_MAX_XML_REQ_ROWS;
		}

		public static int xza9o0CsrActNbr(int idx) {
			return xza9o0XmlReqInfo(idx);
		}

		public static int xza9o0NotPrcTs(int idx) {
			return xza9o0CsrActNbr(idx) + Len.XZA9O0_CSR_ACT_NBR;
		}

		public static int xza9o0ActNotTypCd(int idx) {
			return xza9o0NotPrcTs(idx) + Len.XZA9O0_NOT_PRC_TS;
		}

		public static int xza9o0NotDt(int idx) {
			return xza9o0ActNotTypCd(idx) + Len.XZA9O0_ACT_NOT_TYP_CD;
		}

		public static int xza9o0EmpId(int idx) {
			return xza9o0NotDt(idx) + Len.XZA9O0_NOT_DT;
		}

		public static int xza9o0ActTypCd(int idx) {
			return xza9o0EmpId(idx) + Len.XZA9O0_EMP_ID;
		}

		public static int xza9o0DocDes(int idx) {
			return xza9o0ActTypCd(idx) + Len.XZA9O0_ACT_TYP_CD;
		}

		public static int xza9o0TtyCoInd(int idx) {
			return xza9o0DocDes(idx) + Len.XZA9O0_DOC_DES;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9O0_MAX_XML_REQ_ROWS = 2;
		public static final int XZA9O0_CSR_ACT_NBR = 9;
		public static final int XZA9O0_NOT_PRC_TS = 26;
		public static final int XZA9O0_ACT_NOT_TYP_CD = 5;
		public static final int XZA9O0_NOT_DT = 10;
		public static final int XZA9O0_EMP_ID = 6;
		public static final int XZA9O0_ACT_TYP_CD = 2;
		public static final int XZA9O0_DOC_DES = 240;
		public static final int XZA9O0_TTY_CO_IND = 1;
		public static final int XZA9O0_XML_REQ_INFO = XZA9O0_CSR_ACT_NBR + XZA9O0_NOT_PRC_TS + XZA9O0_ACT_NOT_TYP_CD + XZA9O0_NOT_DT + XZA9O0_EMP_ID
				+ XZA9O0_ACT_TYP_CD + XZA9O0_DOC_DES + XZA9O0_TTY_CO_IND;
		public static final int XZA9O0_XML_REQ_ROW = XZA9O0_MAX_XML_REQ_ROWS + XZA9O0_XML_REQ_INFO;
		public static final int L_FW_RESP_XZ0A90O0 = XZA9O0_XML_REQ_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0r90o0.L_FW_RESP_XZ0A90O0_MAXOCCURS * L_FW_RESP_XZ0A90O0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
