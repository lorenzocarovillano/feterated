/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.CommunicationShellCommon;
import com.federatedinsurance.crs.copy.DriverSpecificData;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program TS020000<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaTs020000 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: COMMUNICATION-SHELL-COMMON
	private CommunicationShellCommon communicationShellCommon = new CommunicationShellCommon();
	//Original name: DRIVER-SPECIFIC-DATA
	private DriverSpecificData driverSpecificData = new DriverSpecificData();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		communicationShellCommon.setCommunicationShellCommonBytes(buffer, position);
		position += CommunicationShellCommon.Len.COMMUNICATION_SHELL_COMMON;
		driverSpecificData.setDriverSpecificDataBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		communicationShellCommon.getCommunicationShellCommonBytes(buffer, position);
		position += CommunicationShellCommon.Len.COMMUNICATION_SHELL_COMMON;
		driverSpecificData.getDriverSpecificDataBytes(buffer, position);
		return buffer;
	}

	public CommunicationShellCommon getCommunicationShellCommon() {
		return communicationShellCommon;
	}

	public DriverSpecificData getDriverSpecificData() {
		return driverSpecificData;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DFHCOMMAREA = CommunicationShellCommon.Len.COMMUNICATION_SHELL_COMMON + DriverSpecificData.Len.DRIVER_SPECIFIC_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
