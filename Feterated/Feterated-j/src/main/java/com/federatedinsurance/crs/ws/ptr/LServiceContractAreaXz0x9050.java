/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9050<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9050 extends BytesClass {

	//==== PROPERTIES ====
	public static final int O_POLICY_ROW_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9050() {
	}

	public LServiceContractAreaXz0x9050(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiTkNotPrcTs(String iTkNotPrcTs) {
		writeString(Pos.I_TK_NOT_PRC_TS, iTkNotPrcTs, Len.I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT95I-TK-NOT-PRC-TS<br>*/
	public String getiTkNotPrcTs() {
		return readString(Pos.I_TK_NOT_PRC_TS, Len.I_TK_NOT_PRC_TS);
	}

	public void setiTkFrmSeqNbr(int iTkFrmSeqNbr) {
		writeInt(Pos.I_TK_FRM_SEQ_NBR, iTkFrmSeqNbr, Len.Int.I_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT95I-TK-FRM-SEQ-NBR<br>*/
	public int getiTkFrmSeqNbr() {
		return readNumDispInt(Pos.I_TK_FRM_SEQ_NBR, Len.I_TK_FRM_SEQ_NBR);
	}

	public void setiCsrActNbr(String iCsrActNbr) {
		writeString(Pos.I_CSR_ACT_NBR, iCsrActNbr, Len.I_CSR_ACT_NBR);
	}

	/**Original name: XZT95I-CSR-ACT-NBR<br>*/
	public String getiCsrActNbr() {
		return readString(Pos.I_CSR_ACT_NBR, Len.I_CSR_ACT_NBR);
	}

	public void setiUserid(String iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT95I-USERID<br>*/
	public String getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	public String getiUseridFormatted() {
		return Functions.padBlanks(getiUserid(), Len.I_USERID);
	}

	public void setoTkNotPrcTs(String oTkNotPrcTs) {
		writeString(Pos.O_TK_NOT_PRC_TS, oTkNotPrcTs, Len.O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT95O-TK-NOT-PRC-TS<br>*/
	public String getoTkNotPrcTs() {
		return readString(Pos.O_TK_NOT_PRC_TS, Len.O_TK_NOT_PRC_TS);
	}

	public String getoTkNotPrcTsFormatted() {
		return Functions.padBlanks(getoTkNotPrcTs(), Len.O_TK_NOT_PRC_TS);
	}

	public void setoCsrActNbr(String oCsrActNbr) {
		writeString(Pos.O_CSR_ACT_NBR, oCsrActNbr, Len.O_CSR_ACT_NBR);
	}

	/**Original name: XZT95O-CSR-ACT-NBR<br>*/
	public String getoCsrActNbr() {
		return readString(Pos.O_CSR_ACT_NBR, Len.O_CSR_ACT_NBR);
	}

	public String getoCsrActNbrFormatted() {
		return Functions.padBlanks(getoCsrActNbr(), Len.O_CSR_ACT_NBR);
	}

	public void setoTkNinCltId(int oTkNinCltIdIdx, String oTkNinCltId) {
		int position = Pos.xzt95oTkNinCltId(oTkNinCltIdIdx - 1);
		writeString(position, oTkNinCltId, Len.O_TK_NIN_CLT_ID);
	}

	/**Original name: XZT95O-TK-NIN-CLT-ID<br>*/
	public String getoTkNinCltId(int oTkNinCltIdIdx) {
		int position = Pos.xzt95oTkNinCltId(oTkNinCltIdIdx - 1);
		return readString(position, Len.O_TK_NIN_CLT_ID);
	}

	public void setoTkNinAdrId(int oTkNinAdrIdIdx, String oTkNinAdrId) {
		int position = Pos.xzt95oTkNinAdrId(oTkNinAdrIdIdx - 1);
		writeString(position, oTkNinAdrId, Len.O_TK_NIN_ADR_ID);
	}

	/**Original name: XZT95O-TK-NIN-ADR-ID<br>*/
	public String getoTkNinAdrId(int oTkNinAdrIdIdx) {
		int position = Pos.xzt95oTkNinAdrId(oTkNinAdrIdIdx - 1);
		return readString(position, Len.O_TK_NIN_ADR_ID);
	}

	public void setoTkWfStartedInd(int oTkWfStartedIndIdx, char oTkWfStartedInd) {
		int position = Pos.xzt95oTkWfStartedInd(oTkWfStartedIndIdx - 1);
		writeChar(position, oTkWfStartedInd);
	}

	/**Original name: XZT95O-TK-WF-STARTED-IND<br>*/
	public char getoTkWfStartedInd(int oTkWfStartedIndIdx) {
		int position = Pos.xzt95oTkWfStartedInd(oTkWfStartedIndIdx - 1);
		return readChar(position);
	}

	public void setoTkPolBilStaCd(int oTkPolBilStaCdIdx, char oTkPolBilStaCd) {
		int position = Pos.xzt95oTkPolBilStaCd(oTkPolBilStaCdIdx - 1);
		writeChar(position, oTkPolBilStaCd);
	}

	/**Original name: XZT95O-TK-POL-BIL-STA-CD<br>*/
	public char getoTkPolBilStaCd(int oTkPolBilStaCdIdx) {
		int position = Pos.xzt95oTkPolBilStaCd(oTkPolBilStaCdIdx - 1);
		return readChar(position);
	}

	public void setoPolNbr(int oPolNbrIdx, String oPolNbr) {
		int position = Pos.xzt95oPolNbr(oPolNbrIdx - 1);
		writeString(position, oPolNbr, Len.O_POL_NBR);
	}

	/**Original name: XZT95O-POL-NBR<br>*/
	public String getoPolNbr(int oPolNbrIdx) {
		int position = Pos.xzt95oPolNbr(oPolNbrIdx - 1);
		return readString(position, Len.O_POL_NBR);
	}

	public String getoPolNbrFormatted(int oPolNbrIdx) {
		return Functions.padBlanks(getoPolNbr(oPolNbrIdx), Len.O_POL_NBR);
	}

	public void setoPolTypCd(int oPolTypCdIdx, String oPolTypCd) {
		int position = Pos.xzt95oPolTypCd(oPolTypCdIdx - 1);
		writeString(position, oPolTypCd, Len.O_POL_TYP_CD);
	}

	/**Original name: XZT95O-POL-TYP-CD<br>*/
	public String getoPolTypCd(int oPolTypCdIdx) {
		int position = Pos.xzt95oPolTypCd(oPolTypCdIdx - 1);
		return readString(position, Len.O_POL_TYP_CD);
	}

	public void setoPolTypDes(int oPolTypDesIdx, String oPolTypDes) {
		int position = Pos.xzt95oPolTypDes(oPolTypDesIdx - 1);
		writeString(position, oPolTypDes, Len.O_POL_TYP_DES);
	}

	/**Original name: XZT95O-POL-TYP-DES<br>*/
	public String getoPolTypDes(int oPolTypDesIdx) {
		int position = Pos.xzt95oPolTypDes(oPolTypDesIdx - 1);
		return readString(position, Len.O_POL_TYP_DES);
	}

	public void setoPolPriRskStAbb(int oPolPriRskStAbbIdx, String oPolPriRskStAbb) {
		int position = Pos.xzt95oPolPriRskStAbb(oPolPriRskStAbbIdx - 1);
		writeString(position, oPolPriRskStAbb, Len.O_POL_PRI_RSK_ST_ABB);
	}

	/**Original name: XZT95O-POL-PRI-RSK-ST-ABB<br>*/
	public String getoPolPriRskStAbb(int oPolPriRskStAbbIdx) {
		int position = Pos.xzt95oPolPriRskStAbb(oPolPriRskStAbbIdx - 1);
		return readString(position, Len.O_POL_PRI_RSK_ST_ABB);
	}

	public String getoPolPriRskStAbbFormatted(int oPolPriRskStAbbIdx) {
		return Functions.padBlanks(getoPolPriRskStAbb(oPolPriRskStAbbIdx), Len.O_POL_PRI_RSK_ST_ABB);
	}

	public void setoNotEffDt(int oNotEffDtIdx, String oNotEffDt) {
		int position = Pos.xzt95oNotEffDt(oNotEffDtIdx - 1);
		writeString(position, oNotEffDt, Len.O_NOT_EFF_DT);
	}

	/**Original name: XZT95O-NOT-EFF-DT<br>*/
	public String getoNotEffDt(int oNotEffDtIdx) {
		int position = Pos.xzt95oNotEffDt(oNotEffDtIdx - 1);
		return readString(position, Len.O_NOT_EFF_DT);
	}

	public void setoPolEffDt(int oPolEffDtIdx, String oPolEffDt) {
		int position = Pos.xzt95oPolEffDt(oPolEffDtIdx - 1);
		writeString(position, oPolEffDt, Len.O_POL_EFF_DT);
	}

	/**Original name: XZT95O-POL-EFF-DT<br>*/
	public String getoPolEffDt(int oPolEffDtIdx) {
		int position = Pos.xzt95oPolEffDt(oPolEffDtIdx - 1);
		return readString(position, Len.O_POL_EFF_DT);
	}

	public String getoPolEffDtFormatted(int oPolEffDtIdx) {
		return Functions.padBlanks(getoPolEffDt(oPolEffDtIdx), Len.O_POL_EFF_DT);
	}

	public void setoPolExpDt(int oPolExpDtIdx, String oPolExpDt) {
		int position = Pos.xzt95oPolExpDt(oPolExpDtIdx - 1);
		writeString(position, oPolExpDt, Len.O_POL_EXP_DT);
	}

	/**Original name: XZT95O-POL-EXP-DT<br>*/
	public String getoPolExpDt(int oPolExpDtIdx) {
		int position = Pos.xzt95oPolExpDt(oPolExpDtIdx - 1);
		return readString(position, Len.O_POL_EXP_DT);
	}

	public void setoPolDueAmt(int oPolDueAmtIdx, AfDecimal oPolDueAmt) {
		int position = Pos.xzt95oPolDueAmt(oPolDueAmtIdx - 1);
		writeDecimal(position, oPolDueAmt.copy());
	}

	/**Original name: XZT95O-POL-DUE-AMT<br>*/
	public AfDecimal getoPolDueAmt(int oPolDueAmtIdx) {
		int position = Pos.xzt95oPolDueAmt(oPolDueAmtIdx - 1);
		return readDecimal(position, Len.Int.O_POL_DUE_AMT, Len.Fract.O_POL_DUE_AMT);
	}

	public void setoMasterCompanyNbr(int oMasterCompanyNbrIdx, String oMasterCompanyNbr) {
		int position = Pos.xzt95oMasterCompanyNbr(oMasterCompanyNbrIdx - 1);
		writeString(position, oMasterCompanyNbr, Len.O_MASTER_COMPANY_NBR);
	}

	/**Original name: XZT95O-MASTER-COMPANY-NBR<br>*/
	public String getoMasterCompanyNbr(int oMasterCompanyNbrIdx) {
		int position = Pos.xzt95oMasterCompanyNbr(oMasterCompanyNbrIdx - 1);
		return readString(position, Len.O_MASTER_COMPANY_NBR);
	}

	public void setoMasterCompanyDes(int oMasterCompanyDesIdx, String oMasterCompanyDes) {
		int position = Pos.xzt95oMasterCompanyDes(oMasterCompanyDesIdx - 1);
		writeString(position, oMasterCompanyDes, Len.O_MASTER_COMPANY_DES);
	}

	/**Original name: XZT95O-MASTER-COMPANY-DES<br>*/
	public String getoMasterCompanyDes(int oMasterCompanyDesIdx) {
		int position = Pos.xzt95oMasterCompanyDes(oMasterCompanyDesIdx - 1);
		return readString(position, Len.O_MASTER_COMPANY_DES);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT950_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_TECHNICAL_KEY = XZT950_SERVICE_INPUTS;
		public static final int I_TK_NOT_PRC_TS = I_TECHNICAL_KEY;
		public static final int I_TK_FRM_SEQ_NBR = I_TK_NOT_PRC_TS + Len.I_TK_NOT_PRC_TS;
		public static final int I_CSR_ACT_NBR = I_TK_FRM_SEQ_NBR + Len.I_TK_FRM_SEQ_NBR;
		public static final int I_USERID = I_CSR_ACT_NBR + Len.I_CSR_ACT_NBR;
		public static final int XZT950_SERVICE_OUTPUTS = I_USERID + Len.I_USERID;
		public static final int O_TECHNICAL_KEY = XZT950_SERVICE_OUTPUTS;
		public static final int O_TK_NOT_PRC_TS = O_TECHNICAL_KEY;
		public static final int O_CSR_ACT_NBR = O_TK_NOT_PRC_TS + Len.O_TK_NOT_PRC_TS;
		public static final int O_POLICY_ROW_TBL = O_CSR_ACT_NBR + Len.O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt95oPolicyRow(int idx) {
			return O_POLICY_ROW_TBL + idx * Len.O_POLICY_ROW;
		}

		public static int xzt95oPolicyTechnicalKey(int idx) {
			return xzt95oPolicyRow(idx);
		}

		public static int xzt95oTkNinCltId(int idx) {
			return xzt95oPolicyTechnicalKey(idx);
		}

		public static int xzt95oTkNinAdrId(int idx) {
			return xzt95oTkNinCltId(idx) + Len.O_TK_NIN_CLT_ID;
		}

		public static int xzt95oTkWfStartedInd(int idx) {
			return xzt95oTkNinAdrId(idx) + Len.O_TK_NIN_ADR_ID;
		}

		public static int xzt95oTkPolBilStaCd(int idx) {
			return xzt95oTkWfStartedInd(idx) + Len.O_TK_WF_STARTED_IND;
		}

		public static int xzt95oPolNbr(int idx) {
			return xzt95oTkPolBilStaCd(idx) + Len.O_TK_POL_BIL_STA_CD;
		}

		public static int xzt95oPolicyType(int idx) {
			return xzt95oPolNbr(idx) + Len.O_POL_NBR;
		}

		public static int xzt95oPolTypCd(int idx) {
			return xzt95oPolicyType(idx);
		}

		public static int xzt95oPolTypDes(int idx) {
			return xzt95oPolTypCd(idx) + Len.O_POL_TYP_CD;
		}

		public static int xzt95oPolPriRskStAbb(int idx) {
			return xzt95oPolTypDes(idx) + Len.O_POL_TYP_DES;
		}

		public static int xzt95oNotEffDt(int idx) {
			return xzt95oPolPriRskStAbb(idx) + Len.O_POL_PRI_RSK_ST_ABB;
		}

		public static int xzt95oPolEffDt(int idx) {
			return xzt95oNotEffDt(idx) + Len.O_NOT_EFF_DT;
		}

		public static int xzt95oPolExpDt(int idx) {
			return xzt95oPolEffDt(idx) + Len.O_POL_EFF_DT;
		}

		public static int xzt95oPolDueAmt(int idx) {
			return xzt95oPolExpDt(idx) + Len.O_POL_EXP_DT;
		}

		public static int xzt95oMasterCompany(int idx) {
			return xzt95oPolDueAmt(idx) + Len.O_POL_DUE_AMT;
		}

		public static int xzt95oMasterCompanyNbr(int idx) {
			return xzt95oMasterCompany(idx);
		}

		public static int xzt95oMasterCompanyDes(int idx) {
			return xzt95oMasterCompanyNbr(idx) + Len.O_MASTER_COMPANY_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_TK_NOT_PRC_TS = 26;
		public static final int I_TK_FRM_SEQ_NBR = 5;
		public static final int I_CSR_ACT_NBR = 9;
		public static final int I_USERID = 8;
		public static final int O_TK_NOT_PRC_TS = 26;
		public static final int O_CSR_ACT_NBR = 9;
		public static final int O_TK_NIN_CLT_ID = 64;
		public static final int O_TK_NIN_ADR_ID = 64;
		public static final int O_TK_WF_STARTED_IND = 1;
		public static final int O_TK_POL_BIL_STA_CD = 1;
		public static final int O_POLICY_TECHNICAL_KEY = O_TK_NIN_CLT_ID + O_TK_NIN_ADR_ID + O_TK_WF_STARTED_IND + O_TK_POL_BIL_STA_CD;
		public static final int O_POL_NBR = 25;
		public static final int O_POL_TYP_CD = 3;
		public static final int O_POL_TYP_DES = 30;
		public static final int O_POLICY_TYPE = O_POL_TYP_CD + O_POL_TYP_DES;
		public static final int O_POL_PRI_RSK_ST_ABB = 2;
		public static final int O_NOT_EFF_DT = 10;
		public static final int O_POL_EFF_DT = 10;
		public static final int O_POL_EXP_DT = 10;
		public static final int O_POL_DUE_AMT = 10;
		public static final int O_MASTER_COMPANY_NBR = 2;
		public static final int O_MASTER_COMPANY_DES = 40;
		public static final int O_MASTER_COMPANY = O_MASTER_COMPANY_NBR + O_MASTER_COMPANY_DES;
		public static final int O_POLICY_ROW = O_POLICY_TECHNICAL_KEY + O_POL_NBR + O_POLICY_TYPE + O_POL_PRI_RSK_ST_ABB + O_NOT_EFF_DT + O_POL_EFF_DT
				+ O_POL_EXP_DT + O_POL_DUE_AMT + O_MASTER_COMPANY;
		public static final int I_TECHNICAL_KEY = I_TK_NOT_PRC_TS + I_TK_FRM_SEQ_NBR;
		public static final int XZT950_SERVICE_INPUTS = I_TECHNICAL_KEY + I_CSR_ACT_NBR + I_USERID;
		public static final int O_TECHNICAL_KEY = O_TK_NOT_PRC_TS;
		public static final int O_POLICY_ROW_TBL = LServiceContractAreaXz0x9050.O_POLICY_ROW_MAXOCCURS * O_POLICY_ROW;
		public static final int XZT950_SERVICE_OUTPUTS = O_TECHNICAL_KEY + O_CSR_ACT_NBR + O_POLICY_ROW_TBL;
		public static final int L_SERVICE_CONTRACT_AREA = XZT950_SERVICE_INPUTS + XZT950_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int I_TK_FRM_SEQ_NBR = 5;
			public static final int O_POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int O_POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
