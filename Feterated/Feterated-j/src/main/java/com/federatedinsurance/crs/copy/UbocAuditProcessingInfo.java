/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.UbocApplyAuditsSw;

/**Original name: UBOC-AUDIT-PROCESSING-INFO<br>
 * Variable: UBOC-AUDIT-PROCESSING-INFO from copybook HALLUBOC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class UbocAuditProcessingInfo {

	//==== PROPERTIES ====
	//Original name: UBOC-APPLY-AUDITS-SW
	private UbocApplyAuditsSw applyAuditsSw = new UbocApplyAuditsSw();
	//Original name: UBOC-AUDT-BUS-OBJ-NM
	private String audtBusObjNm = DefaultValues.stringVal(Len.AUDT_BUS_OBJ_NM);
	//Original name: UBOC-AUDT-EVENT-DATA
	private String audtEventData = DefaultValues.stringVal(Len.AUDT_EVENT_DATA);

	//==== METHODS ====
	public void setUbocAuditProcessingInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		applyAuditsSw.setApplyAuditsSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		audtBusObjNm = MarshalByte.readString(buffer, position, Len.AUDT_BUS_OBJ_NM);
		position += Len.AUDT_BUS_OBJ_NM;
		audtEventData = MarshalByte.readString(buffer, position, Len.AUDT_EVENT_DATA);
	}

	public byte[] getUbocAuditProcessingInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, applyAuditsSw.getApplyAuditsSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, audtBusObjNm, Len.AUDT_BUS_OBJ_NM);
		position += Len.AUDT_BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, audtEventData, Len.AUDT_EVENT_DATA);
		return buffer;
	}

	public void setAudtBusObjNm(String audtBusObjNm) {
		this.audtBusObjNm = Functions.subString(audtBusObjNm, Len.AUDT_BUS_OBJ_NM);
	}

	public String getAudtBusObjNm() {
		return this.audtBusObjNm;
	}

	public void setAudtEventData(String audtEventData) {
		this.audtEventData = Functions.subString(audtEventData, Len.AUDT_EVENT_DATA);
	}

	public String getAudtEventData() {
		return this.audtEventData;
	}

	public UbocApplyAuditsSw getApplyAuditsSw() {
		return applyAuditsSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int AUDT_BUS_OBJ_NM = 8;
		public static final int AUDT_EVENT_DATA = 100;
		public static final int UBOC_AUDIT_PROCESSING_INFO = UbocApplyAuditsSw.Len.APPLY_AUDITS_SW + AUDT_BUS_OBJ_NM + AUDT_EVENT_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
