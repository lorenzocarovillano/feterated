/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.WsBillingRegion;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P90L0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p90l0 {

	//==== PROPERTIES ====
	//Original name: WS-MAX-POL-ROWS
	private short wsMaxPolRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-MAX-ROWS-X
	private String wsMaxRowsX = DefaultValues.stringVal(Len.WS_MAX_ROWS_X);
	//Original name: WS-EIBRESP-CD
	private short wsEibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short wsEibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-CURRENT-TIMESTAMP-X
	private WsCurrentTimestampX wsCurrentTimestampX = new WsCurrentTimestampX();
	/**Original name: WS-BILLING-REGION<br>
	 * <pre>* !WARNING! DEV AND BETA BILLING REGIONS MAY CHANGE!
	 * * VERIFY THE FOLLOWING BEFORE TESTING!</pre>*/
	private WsBillingRegion wsBillingRegion = new WsBillingRegion();
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo wsErrorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-PRODUCER-NBR
	private WsProducerNbr wsProducerNbr = new WsProducerNbr();
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "XZ0P90L0";
	//Original name: WS-APPLICATION-NM
	private String wsApplicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-PREPARE-RESC-NOT
	private String wsBusObjNmPrepareRescNot = "XZ_PREPARE_RES_NOTIFICATION";
	//Original name: WS-OPERATIONS-CALLED
	private WsOperationsCalledXz0p90k0 wsOperationsCalled = new WsOperationsCalledXz0p90k0();

	//==== METHODS ====
	public void setWsMaxPolRows(short wsMaxPolRows) {
		this.wsMaxPolRows = wsMaxPolRows;
	}

	public short getWsMaxPolRows() {
		return this.wsMaxPolRows;
	}

	public void setWsMaxRowsX(long wsMaxRowsX) {
		this.wsMaxRowsX = PicFormatter.display("Z(3)9").format(wsMaxRowsX).toString();
	}

	public long getWsMaxRowsX() {
		return PicParser.display("Z(3)9").parseLong(this.wsMaxRowsX);
	}

	public String getWsMaxRowsXFormatted() {
		return this.wsMaxRowsX;
	}

	public String getWsMaxRowsXAsString() {
		return getWsMaxRowsXFormatted();
	}

	public void setWsEibrespCd(short wsEibrespCd) {
		this.wsEibrespCd = wsEibrespCd;
	}

	public short getWsEibrespCd() {
		return this.wsEibrespCd;
	}

	public String getWsEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getWsEibrespCd(), Len.WS_EIBRESP_CD);
	}

	public String getWsEibrespCdAsString() {
		return getWsEibrespCdFormatted();
	}

	public void setWsEibresp2Cd(short wsEibresp2Cd) {
		this.wsEibresp2Cd = wsEibresp2Cd;
	}

	public short getWsEibresp2Cd() {
		return this.wsEibresp2Cd;
	}

	public String getWsEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getWsEibresp2Cd(), Len.WS_EIBRESP2_CD);
	}

	public String getWsEibresp2CdAsString() {
		return getWsEibresp2CdFormatted();
	}

	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public String getWsApplicationNm() {
		return this.wsApplicationNm;
	}

	public String getWsBusObjNmPrepareRescNot() {
		return this.wsBusObjNmPrepareRescNot;
	}

	public WsBillingRegion getWsBillingRegion() {
		return wsBillingRegion;
	}

	public WsCurrentTimestampX getWsCurrentTimestampX() {
		return wsCurrentTimestampX;
	}

	public WsErrorCheckInfo getWsErrorCheckInfo() {
		return wsErrorCheckInfo;
	}

	public WsOperationsCalledXz0p90k0 getWsOperationsCalled() {
		return wsOperationsCalled;
	}

	public WsProducerNbr getWsProducerNbr() {
		return wsProducerNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_MAX_ROWS_X = 4;
		public static final int WS_PROGRAM_NAME = 8;
		public static final int WS_EIBRESP_CD = 4;
		public static final int WS_EIBRESP2_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
