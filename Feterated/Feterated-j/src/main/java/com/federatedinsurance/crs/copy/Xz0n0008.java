/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: XZ0N0008<br>
 * Variable: XZ0N0008 from copybook XZ0N0008<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz0n0008 {

	//==== PROPERTIES ====
	//Original name: XZN008-TRANS-PROCESS-DT
	private String transProcessDt = "Transaction Processing Date";
	//Original name: FILLER-XZN008-PROCESSING-CONTEXT-TEXT
	private String flr1 = "The account,";
	//Original name: FILLER-XZN008-PROCESSING-CONTEXT-TEXT-1
	private String flr2 = "notification";
	//Original name: FILLER-XZN008-PROCESSING-CONTEXT-TEXT-2
	private String flr3 = "timestamp,";
	//Original name: FILLER-XZN008-PROCESSING-CONTEXT-TEXT-3
	private String flr4 = "rec seq nbr,";
	//Original name: FILLER-XZN008-PROCESSING-CONTEXT-TEXT-4
	private String flr5 = "and policy nbr";
	//Original name: FILLER-XZN008-PROCESSING-CONTEXT-TEXT-5
	private String flr6 = "of:";

	//==== METHODS ====
	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public String getProcessingContextTextFormatted() {
		return MarshalByteExt.bufferToStr(getProcessingContextTextBytes());
	}

	/**Original name: XZN008-PROCESSING-CONTEXT-TEXT<br>*/
	public byte[] getProcessingContextTextBytes() {
		byte[] buffer = new byte[Len.PROCESSING_CONTEXT_TEXT];
		return getProcessingContextTextBytes(buffer, 1);
	}

	public byte[] getProcessingContextTextBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 13;
		public static final int FLR3 = 11;
		public static final int FLR5 = 15;
		public static final int FLR6 = 4;
		public static final int PROCESSING_CONTEXT_TEXT = 3 * FLR1 + FLR3 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
