/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-UOW-LOCK-TSQ-SW<br>
 * Variable: WS-UOW-LOCK-TSQ-SW from program HALRLOMG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsUowLockTsqSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char START_OF_UOW_LOCK_TSQ = 'S';
	public static final char END_OF_UOW_LOCK_TSQ = 'E';

	//==== METHODS ====
	public void setUowLockTsqSw(char uowLockTsqSw) {
		this.value = uowLockTsqSw;
	}

	public char getUowLockTsqSw() {
		return this.value;
	}

	public void setStartOfUowLockTsq() {
		value = START_OF_UOW_LOCK_TSQ;
	}

	public boolean isEndOfUowLockTsq() {
		return value == END_OF_UOW_LOCK_TSQ;
	}

	public void setEndOfUowLockTsq() {
		value = END_OF_UOW_LOCK_TSQ;
	}
}
