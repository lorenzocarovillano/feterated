/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WS-SYSTEM-YEAR-RED<br>
 * Variable: WS-SYSTEM-YEAR-RED from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsSystemYearRed {

	//==== PROPERTIES ====
	//Original name: WS-SYSTEM-YEAR-CENTURY
	private String yearCentury = DefaultValues.stringVal(Len.YEAR_CENTURY);
	//Original name: WS-SYSTEM-YEAR-YEAR
	private String yearYear = DefaultValues.stringVal(Len.YEAR_YEAR);

	//==== METHODS ====
	public void setWsSystemYear(short wsSystemYear) {
		int position = 1;
		byte[] buffer = getWsSystemYearRedBytes();
		MarshalByte.writeShort(buffer, position, wsSystemYear, Len.Int.WS_SYSTEM_YEAR, SignType.NO_SIGN);
		setWsSystemYearRedBytes(buffer);
	}

	/**Original name: WS-SYSTEM-YEAR<br>*/
	public short getWsSystemYear() {
		int position = 1;
		return MarshalByte.readShort(getWsSystemYearRedBytes(), position, Len.Int.WS_SYSTEM_YEAR, SignType.NO_SIGN);
	}

	public String getWsSystemYearFormatted() {
		int position = 1;
		return MarshalByte.readFixedString(getWsSystemYearRedBytes(), position, Len.WS_SYSTEM_YEAR);
	}

	public void setWsSystemYearRedBytes(byte[] buffer) {
		setWsSystemYearRedBytes(buffer, 1);
	}

	/**Original name: WS-SYSTEM-YEAR-RED<br>*/
	public byte[] getWsSystemYearRedBytes() {
		byte[] buffer = new byte[Len.WS_SYSTEM_YEAR_RED];
		return getWsSystemYearRedBytes(buffer, 1);
	}

	public void setWsSystemYearRedBytes(byte[] buffer, int offset) {
		int position = offset;
		yearCentury = MarshalByte.readFixedString(buffer, position, Len.YEAR_CENTURY);
		position += Len.YEAR_CENTURY;
		yearYear = MarshalByte.readFixedString(buffer, position, Len.YEAR_YEAR);
	}

	public byte[] getWsSystemYearRedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, yearCentury, Len.YEAR_CENTURY);
		position += Len.YEAR_CENTURY;
		MarshalByte.writeString(buffer, position, yearYear, Len.YEAR_YEAR);
		return buffer;
	}

	public String getYearCenturyFormatted() {
		return this.yearCentury;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YEAR_CENTURY = 2;
		public static final int YEAR_YEAR = 2;
		public static final int WS_SYSTEM_YEAR_RED = YEAR_CENTURY + YEAR_YEAR;
		public static final int WS_SYSTEM_YEAR = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_SYSTEM_YEAR = 4;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
