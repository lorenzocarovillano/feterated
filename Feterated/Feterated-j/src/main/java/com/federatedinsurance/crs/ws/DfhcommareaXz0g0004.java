/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.Xz0t0004;
import com.federatedinsurance.crs.copy.Xzt004ServiceInputs;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program XZ0G0004<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaXz0g0004 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: PROXY-PROGRAM-COMMON
	private ProxyProgramCommonXz0g0004 proxyProgramCommon = new ProxyProgramCommonXz0g0004();
	//Original name: XZ0T0004
	private Xz0t0004 xz0t0004 = new Xz0t0004();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		proxyProgramCommon.setProxyProgramCommonBytes(buffer, position);
		position += ProxyProgramCommonXz0g0004.Len.PROXY_PROGRAM_COMMON;
		xz0t0004.getXzt004ServiceInputs().setXzt004ServiceInputsBytes(buffer, position);
		position += Xzt004ServiceInputs.Len.XZT004_SERVICE_INPUTS;
		xz0t0004.setXzt004ServiceOutputsBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		proxyProgramCommon.getProxyProgramCommonBytes(buffer, position);
		position += ProxyProgramCommonXz0g0004.Len.PROXY_PROGRAM_COMMON;
		xz0t0004.getXzt004ServiceInputs().getXzt004ServiceInputsBytes(buffer, position);
		position += Xzt004ServiceInputs.Len.XZT004_SERVICE_INPUTS;
		xz0t0004.getXzt004ServiceOutputsBytes(buffer, position);
		return buffer;
	}

	public ProxyProgramCommonXz0g0004 getProxyProgramCommon() {
		return proxyProgramCommon;
	}

	public Xz0t0004 getXz0t0004() {
		return xz0t0004;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DFHCOMMAREA = ProxyProgramCommonXz0g0004.Len.PROXY_PROGRAM_COMMON + Xzt004ServiceInputs.Len.XZT004_SERVICE_INPUTS
				+ Xz0t0004.Len.XZT004_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
