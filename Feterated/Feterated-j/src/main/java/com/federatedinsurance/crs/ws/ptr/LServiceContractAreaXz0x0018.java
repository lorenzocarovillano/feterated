/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0018<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0018 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0018() {
	}

	public LServiceContractAreaXz0x0018(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt18iTkNotPrcTs(String xzt18iTkNotPrcTs) {
		writeString(Pos.XZT18I_TK_NOT_PRC_TS, xzt18iTkNotPrcTs, Len.XZT18I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT18I-TK-NOT-PRC-TS<br>*/
	public String getXzt18iTkNotPrcTs() {
		return readString(Pos.XZT18I_TK_NOT_PRC_TS, Len.XZT18I_TK_NOT_PRC_TS);
	}

	public void setXzt18iTkFrmSeqNbr(int xzt18iTkFrmSeqNbr) {
		writeInt(Pos.XZT18I_TK_FRM_SEQ_NBR, xzt18iTkFrmSeqNbr, Len.Int.XZT18I_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT18I-TK-FRM-SEQ-NBR<br>*/
	public int getXzt18iTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT18I_TK_FRM_SEQ_NBR, Len.XZT18I_TK_FRM_SEQ_NBR);
	}

	public String getXzt18iTkFrmSeqNbrFormatted() {
		return readFixedString(Pos.XZT18I_TK_FRM_SEQ_NBR, Len.XZT18I_TK_FRM_SEQ_NBR);
	}

	public void setXzt18iTkRecSeqNbr(int xzt18iTkRecSeqNbr) {
		writeInt(Pos.XZT18I_TK_REC_SEQ_NBR, xzt18iTkRecSeqNbr, Len.Int.XZT18I_TK_REC_SEQ_NBR);
	}

	/**Original name: XZT18I-TK-REC-SEQ-NBR<br>*/
	public int getXzt18iTkRecSeqNbr() {
		return readNumDispInt(Pos.XZT18I_TK_REC_SEQ_NBR, Len.XZT18I_TK_REC_SEQ_NBR);
	}

	public String getXzt18iTkRecSeqNbrFormatted() {
		return readFixedString(Pos.XZT18I_TK_REC_SEQ_NBR, Len.XZT18I_TK_REC_SEQ_NBR);
	}

	public void setXzt18iCsrActNbr(String xzt18iCsrActNbr) {
		writeString(Pos.XZT18I_CSR_ACT_NBR, xzt18iCsrActNbr, Len.XZT18I_CSR_ACT_NBR);
	}

	/**Original name: XZT18I-CSR-ACT-NBR<br>*/
	public String getXzt18iCsrActNbr() {
		return readString(Pos.XZT18I_CSR_ACT_NBR, Len.XZT18I_CSR_ACT_NBR);
	}

	public void setXzt18iUserid(String xzt18iUserid) {
		writeString(Pos.XZT18I_USERID, xzt18iUserid, Len.XZT18I_USERID);
	}

	/**Original name: XZT18I-USERID<br>*/
	public String getXzt18iUserid() {
		return readString(Pos.XZT18I_USERID, Len.XZT18I_USERID);
	}

	public String getXzt18iUseridFormatted() {
		return Functions.padBlanks(getXzt18iUserid(), Len.XZT18I_USERID);
	}

	public void setXzt18oTkNotPrcTs(String xzt18oTkNotPrcTs) {
		writeString(Pos.XZT18O_TK_NOT_PRC_TS, xzt18oTkNotPrcTs, Len.XZT18O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT18O-TK-NOT-PRC-TS<br>*/
	public String getXzt18oTkNotPrcTs() {
		return readString(Pos.XZT18O_TK_NOT_PRC_TS, Len.XZT18O_TK_NOT_PRC_TS);
	}

	public void setXzt18oTkFrmSeqNbr(int xzt18oTkFrmSeqNbr) {
		writeInt(Pos.XZT18O_TK_FRM_SEQ_NBR, xzt18oTkFrmSeqNbr, Len.Int.XZT18O_TK_FRM_SEQ_NBR);
	}

	public void setXzt18oTkFrmSeqNbrFormatted(String xzt18oTkFrmSeqNbr) {
		writeString(Pos.XZT18O_TK_FRM_SEQ_NBR, Trunc.toUnsignedNumeric(xzt18oTkFrmSeqNbr, Len.XZT18O_TK_FRM_SEQ_NBR), Len.XZT18O_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT18O-TK-FRM-SEQ-NBR<br>*/
	public int getXzt18oTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT18O_TK_FRM_SEQ_NBR, Len.XZT18O_TK_FRM_SEQ_NBR);
	}

	public void setXzt18oTkRecSeqNbr(int xzt18oTkRecSeqNbr) {
		writeInt(Pos.XZT18O_TK_REC_SEQ_NBR, xzt18oTkRecSeqNbr, Len.Int.XZT18O_TK_REC_SEQ_NBR);
	}

	public void setXzt18oTkRecSeqNbrFormatted(String xzt18oTkRecSeqNbr) {
		writeString(Pos.XZT18O_TK_REC_SEQ_NBR, Trunc.toUnsignedNumeric(xzt18oTkRecSeqNbr, Len.XZT18O_TK_REC_SEQ_NBR), Len.XZT18O_TK_REC_SEQ_NBR);
	}

	/**Original name: XZT18O-TK-REC-SEQ-NBR<br>*/
	public int getXzt18oTkRecSeqNbr() {
		return readNumDispInt(Pos.XZT18O_TK_REC_SEQ_NBR, Len.XZT18O_TK_REC_SEQ_NBR);
	}

	public void setXzt18oTkFrmRecCsumFormatted(String xzt18oTkFrmRecCsum) {
		writeString(Pos.XZT18O_TK_FRM_REC_CSUM, Trunc.toUnsignedNumeric(xzt18oTkFrmRecCsum, Len.XZT18O_TK_FRM_REC_CSUM), Len.XZT18O_TK_FRM_REC_CSUM);
	}

	/**Original name: XZT18O-TK-FRM-REC-CSUM<br>*/
	public int getXzt18oTkFrmRecCsum() {
		return readNumDispUnsignedInt(Pos.XZT18O_TK_FRM_REC_CSUM, Len.XZT18O_TK_FRM_REC_CSUM);
	}

	public void setXzt18oCsrActNbr(String xzt18oCsrActNbr) {
		writeString(Pos.XZT18O_CSR_ACT_NBR, xzt18oCsrActNbr, Len.XZT18O_CSR_ACT_NBR);
	}

	/**Original name: XZT18O-CSR-ACT-NBR<br>*/
	public String getXzt18oCsrActNbr() {
		return readString(Pos.XZT18O_CSR_ACT_NBR, Len.XZT18O_CSR_ACT_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT018_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT18I_TECHNICAL_KEY = XZT018_SERVICE_INPUTS;
		public static final int XZT18I_TK_NOT_PRC_TS = XZT18I_TECHNICAL_KEY;
		public static final int XZT18I_TK_FRM_SEQ_NBR = XZT18I_TK_NOT_PRC_TS + Len.XZT18I_TK_NOT_PRC_TS;
		public static final int XZT18I_TK_REC_SEQ_NBR = XZT18I_TK_FRM_SEQ_NBR + Len.XZT18I_TK_FRM_SEQ_NBR;
		public static final int XZT18I_CSR_ACT_NBR = XZT18I_TK_REC_SEQ_NBR + Len.XZT18I_TK_REC_SEQ_NBR;
		public static final int XZT18I_USERID = XZT18I_CSR_ACT_NBR + Len.XZT18I_CSR_ACT_NBR;
		public static final int XZT018_SERVICE_OUTPUTS = XZT18I_USERID + Len.XZT18I_USERID;
		public static final int XZT18O_TECHNICAL_KEY = XZT018_SERVICE_OUTPUTS;
		public static final int XZT18O_TK_NOT_PRC_TS = XZT18O_TECHNICAL_KEY;
		public static final int XZT18O_TK_FRM_SEQ_NBR = XZT18O_TK_NOT_PRC_TS + Len.XZT18O_TK_NOT_PRC_TS;
		public static final int XZT18O_TK_REC_SEQ_NBR = XZT18O_TK_FRM_SEQ_NBR + Len.XZT18O_TK_FRM_SEQ_NBR;
		public static final int XZT18O_TK_FRM_REC_CSUM = XZT18O_TK_REC_SEQ_NBR + Len.XZT18O_TK_REC_SEQ_NBR;
		public static final int XZT18O_CSR_ACT_NBR = XZT18O_TK_FRM_REC_CSUM + Len.XZT18O_TK_FRM_REC_CSUM;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT18I_TK_NOT_PRC_TS = 26;
		public static final int XZT18I_TK_FRM_SEQ_NBR = 5;
		public static final int XZT18I_TK_REC_SEQ_NBR = 5;
		public static final int XZT18I_CSR_ACT_NBR = 9;
		public static final int XZT18I_USERID = 8;
		public static final int XZT18O_TK_NOT_PRC_TS = 26;
		public static final int XZT18O_TK_FRM_SEQ_NBR = 5;
		public static final int XZT18O_TK_REC_SEQ_NBR = 5;
		public static final int XZT18O_TK_FRM_REC_CSUM = 9;
		public static final int XZT18I_TECHNICAL_KEY = XZT18I_TK_NOT_PRC_TS + XZT18I_TK_FRM_SEQ_NBR + XZT18I_TK_REC_SEQ_NBR;
		public static final int XZT018_SERVICE_INPUTS = XZT18I_TECHNICAL_KEY + XZT18I_CSR_ACT_NBR + XZT18I_USERID;
		public static final int XZT18O_TECHNICAL_KEY = XZT18O_TK_NOT_PRC_TS + XZT18O_TK_FRM_SEQ_NBR + XZT18O_TK_REC_SEQ_NBR + XZT18O_TK_FRM_REC_CSUM;
		public static final int XZT18O_CSR_ACT_NBR = 9;
		public static final int XZT018_SERVICE_OUTPUTS = XZT18O_TECHNICAL_KEY + XZT18O_CSR_ACT_NBR;
		public static final int L_SERVICE_CONTRACT_AREA = XZT018_SERVICE_INPUTS + XZT018_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT18I_TK_FRM_SEQ_NBR = 5;
			public static final int XZT18I_TK_REC_SEQ_NBR = 5;
			public static final int XZT18O_TK_FRM_SEQ_NBR = 5;
			public static final int XZT18O_TK_REC_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
