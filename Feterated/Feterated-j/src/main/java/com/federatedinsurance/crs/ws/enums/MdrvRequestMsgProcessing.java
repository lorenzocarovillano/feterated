/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;

/**Original name: MDRV-REQUEST-MSG-PROCESSING<br>
 * Variable: MDRV-REQUEST-MSG-PROCESSING from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvRequestMsgProcessing {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char BUILD_REQ_FROM_CHUNKS = Types.SPACE_CHAR;
	public static final char REQ_IN_MRQM_UMT = 'M';
	public static final char REQ_IN_URQM_UMT = 'U';

	//==== METHODS ====
	public void setRequestMsgProcessing(char requestMsgProcessing) {
		this.value = requestMsgProcessing;
	}

	public char getRequestMsgProcessing() {
		return this.value;
	}

	public void setReqInMrqmUmt() {
		value = REQ_IN_MRQM_UMT;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REQUEST_MSG_PROCESSING = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
