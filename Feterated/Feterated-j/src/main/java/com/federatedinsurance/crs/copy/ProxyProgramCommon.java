/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.enums.PpcBypassSyncpointMdrvInd;
import com.federatedinsurance.crs.ws.occurs.PpcNonLoggableErrors;
import com.federatedinsurance.crs.ws.occurs.PpcWarnings;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: PROXY-PROGRAM-COMMON<br>
 * Variable: PROXY-PROGRAM-COMMON from copybook TS020PRO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class ProxyProgramCommon {

	//==== PROPERTIES ====
	public static final int NON_LOGGABLE_ERRORS_MAXOCCURS = 10;
	public static final int WARNINGS_MAXOCCURS = 10;
	//Original name: PPC-SERVICE-DATA-SIZE
	private int serviceDataSize = DefaultValues.BIN_INT_VAL;
	//Original name: PPC-SERVICE-DATA-POINTER
	private int serviceDataPointer = DefaultValues.BIN_INT_VAL;
	/**Original name: PPC-BYPASS-SYNCPOINT-MDRV-IND<br>
	 * <pre>* FLAG TO INDICATE WHETHER OR NOT MAIN DRIVER DOES SYNCPOINT</pre>*/
	private PpcBypassSyncpointMdrvInd bypassSyncpointMdrvInd = new PpcBypassSyncpointMdrvInd();
	//Original name: PPC-OPERATION
	private String operation = DefaultValues.stringVal(Len.OPERATION);
	//Original name: PPC-ERROR-RETURN-CODE
	private DsdErrorReturnCode errorReturnCode = new DsdErrorReturnCode();
	//Original name: PPC-FATAL-ERROR-MESSAGE
	private String fatalErrorMessage = DefaultValues.stringVal(Len.FATAL_ERROR_MESSAGE);
	//Original name: PPC-NON-LOGGABLE-ERROR-CNT
	private String nonLoggableErrorCnt = DefaultValues.stringVal(Len.NON_LOGGABLE_ERROR_CNT);
	//Original name: PPC-NON-LOGGABLE-ERRORS
	private PpcNonLoggableErrors[] nonLoggableErrors = new PpcNonLoggableErrors[NON_LOGGABLE_ERRORS_MAXOCCURS];
	//Original name: PPC-WARNING-CNT
	private String warningCnt = DefaultValues.stringVal(Len.WARNING_CNT);
	//Original name: PPC-WARNINGS
	private PpcWarnings[] warnings = new PpcWarnings[WARNINGS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public ProxyProgramCommon() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int nonLoggableErrorsIdx = 1; nonLoggableErrorsIdx <= NON_LOGGABLE_ERRORS_MAXOCCURS; nonLoggableErrorsIdx++) {
			nonLoggableErrors[nonLoggableErrorsIdx - 1] = new PpcNonLoggableErrors();
		}
		for (int warningsIdx = 1; warningsIdx <= WARNINGS_MAXOCCURS; warningsIdx++) {
			warnings[warningsIdx - 1] = new PpcWarnings();
		}
	}

	public byte[] getProxyProgramCommonBytes() {
		byte[] buffer = new byte[Len.PROXY_PROGRAM_COMMON];
		return getProxyProgramCommonBytes(buffer, 1);
	}

	public void setProxyProgramCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		setInputParmsBytes(buffer, position);
		position += Len.INPUT_PARMS;
		setOutputParmsBytes(buffer, position);
	}

	public byte[] getProxyProgramCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		getInputParmsBytes(buffer, position);
		position += Len.INPUT_PARMS;
		getOutputParmsBytes(buffer, position);
		return buffer;
	}

	public void setInputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		setMemoryAllocationParmsBytes(buffer, position);
		position += Len.MEMORY_ALLOCATION_PARMS;
		bypassSyncpointMdrvInd.setPpcBypassSyncpointMdrvInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		operation = MarshalByte.readString(buffer, position, Len.OPERATION);
	}

	public byte[] getInputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		getMemoryAllocationParmsBytes(buffer, position);
		position += Len.MEMORY_ALLOCATION_PARMS;
		MarshalByte.writeChar(buffer, position, bypassSyncpointMdrvInd.getPpcBypassSyncpointMdrvInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, operation, Len.OPERATION);
		return buffer;
	}

	public void setMemoryAllocationParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		serviceDataSize = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		serviceDataPointer = MarshalByte.readBinaryInt(buffer, position);
	}

	public byte[] getMemoryAllocationParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryInt(buffer, position, serviceDataSize);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, serviceDataPointer);
		return buffer;
	}

	public void setServiceDataSize(int serviceDataSize) {
		this.serviceDataSize = serviceDataSize;
	}

	public int getServiceDataSize() {
		return this.serviceDataSize;
	}

	public void setServiceDataPointer(int serviceDataPointer) {
		this.serviceDataPointer = serviceDataPointer;
	}

	public int getServiceDataPointer() {
		return this.serviceDataPointer;
	}

	public void setOperation(String operation) {
		this.operation = Functions.subString(operation, Len.OPERATION);
	}

	public String getOperation() {
		return this.operation;
	}

	public void setOutputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		setErrorHandlingParmsBytes(buffer, position);
	}

	public byte[] getOutputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		getErrorHandlingParmsBytes(buffer, position);
		return buffer;
	}

	public void setErrorHandlingParmsBytes(byte[] buffer) {
		setErrorHandlingParmsBytes(buffer, 1);
	}

	public void setErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		errorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		fatalErrorMessage = MarshalByte.readString(buffer, position, Len.FATAL_ERROR_MESSAGE);
		position += Len.FATAL_ERROR_MESSAGE;
		nonLoggableErrorCnt = MarshalByte.readFixedString(buffer, position, Len.NON_LOGGABLE_ERROR_CNT);
		position += Len.NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				nonLoggableErrors[idx - 1].setPpcNonLoggableErrorsBytes(buffer, position);
				position += PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS;
			} else {
				nonLoggableErrors[idx - 1].initPpcNonLoggableErrorsSpaces();
				position += PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS;
			}
		}
		warningCnt = MarshalByte.readFixedString(buffer, position, Len.WARNING_CNT);
		position += Len.WARNING_CNT;
		for (int idx = 1; idx <= WARNINGS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				warnings[idx - 1].setPpcWarningsBytes(buffer, position);
				position += PpcWarnings.Len.PPC_WARNINGS;
			} else {
				warnings[idx - 1].initPpcWarningsSpaces();
				position += PpcWarnings.Len.PPC_WARNINGS;
			}
		}
	}

	public byte[] getErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, errorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, fatalErrorMessage, Len.FATAL_ERROR_MESSAGE);
		position += Len.FATAL_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, nonLoggableErrorCnt, Len.NON_LOGGABLE_ERROR_CNT);
		position += Len.NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			nonLoggableErrors[idx - 1].getPpcNonLoggableErrorsBytes(buffer, position);
			position += PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS;
		}
		MarshalByte.writeString(buffer, position, warningCnt, Len.WARNING_CNT);
		position += Len.WARNING_CNT;
		for (int idx = 1; idx <= WARNINGS_MAXOCCURS; idx++) {
			warnings[idx - 1].getPpcWarningsBytes(buffer, position);
			position += PpcWarnings.Len.PPC_WARNINGS;
		}
		return buffer;
	}

	public void setFatalErrorMessage(String fatalErrorMessage) {
		this.fatalErrorMessage = Functions.subString(fatalErrorMessage, Len.FATAL_ERROR_MESSAGE);
	}

	public String getFatalErrorMessage() {
		return this.fatalErrorMessage;
	}

	public void setNonLoggableErrorCntFormatted(String nonLoggableErrorCnt) {
		this.nonLoggableErrorCnt = Trunc.toUnsignedNumeric(nonLoggableErrorCnt, Len.NON_LOGGABLE_ERROR_CNT);
	}

	public short getNonLoggableErrorCnt() {
		return NumericDisplay.asShort(this.nonLoggableErrorCnt);
	}

	public void setWarningCntFormatted(String warningCnt) {
		this.warningCnt = Trunc.toUnsignedNumeric(warningCnt, Len.WARNING_CNT);
	}

	public short getWarningCnt() {
		return NumericDisplay.asShort(this.warningCnt);
	}

	public PpcBypassSyncpointMdrvInd getBypassSyncpointMdrvInd() {
		return bypassSyncpointMdrvInd;
	}

	public DsdErrorReturnCode getErrorReturnCode() {
		return errorReturnCode;
	}

	public PpcNonLoggableErrors getNonLoggableErrors(int idx) {
		return nonLoggableErrors[idx - 1];
	}

	public PpcWarnings getWarnings(int idx) {
		return warnings[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPERATION = 32;
		public static final int FATAL_ERROR_MESSAGE = 250;
		public static final int NON_LOGGABLE_ERROR_CNT = 4;
		public static final int WARNING_CNT = 4;
		public static final int SERVICE_DATA_SIZE = 4;
		public static final int SERVICE_DATA_POINTER = 4;
		public static final int MEMORY_ALLOCATION_PARMS = SERVICE_DATA_SIZE + SERVICE_DATA_POINTER;
		public static final int INPUT_PARMS = MEMORY_ALLOCATION_PARMS + PpcBypassSyncpointMdrvInd.Len.PPC_BYPASS_SYNCPOINT_MDRV_IND + OPERATION;
		public static final int ERROR_HANDLING_PARMS = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + FATAL_ERROR_MESSAGE + NON_LOGGABLE_ERROR_CNT
				+ ProxyProgramCommon.NON_LOGGABLE_ERRORS_MAXOCCURS * PpcNonLoggableErrors.Len.PPC_NON_LOGGABLE_ERRORS + WARNING_CNT
				+ ProxyProgramCommon.WARNINGS_MAXOCCURS * PpcWarnings.Len.PPC_WARNINGS;
		public static final int OUTPUT_PARMS = ERROR_HANDLING_PARMS;
		public static final int PROXY_PROGRAM_COMMON = INPUT_PARMS + OUTPUT_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
