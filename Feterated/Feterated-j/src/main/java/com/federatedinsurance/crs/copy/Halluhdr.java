/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HALLUHDR<br>
 * Variable: HALLUHDR from copybook HALLUHDR<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Halluhdr {

	//==== PROPERTIES ====
	//Original name: UHDR-ID
	private String id = DefaultValues.stringVal(Len.ID);
	//Original name: UHDR-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: UHDR-UOW-BUFFER-LENGTH
	private String uowBufferLength = DefaultValues.stringVal(Len.UOW_BUFFER_LENGTH);
	//Original name: UHDR-MSG-BUS-OBJ-NM
	private String msgBusObjNm = DefaultValues.stringVal(Len.MSG_BUS_OBJ_NM);
	//Original name: UHDR-MSG-BUS-OBJ-COUNT
	private String msgBusObjCount = DefaultValues.stringVal(Len.MSG_BUS_OBJ_COUNT);
	//Original name: UHDR-MSG-BUS-OBJ-DATA-LEN
	private String msgBusObjDataLen = DefaultValues.stringVal(Len.MSG_BUS_OBJ_DATA_LEN);

	//==== METHODS ====
	public void initHalluhdrSpaces() {
		initCommonSpaces();
	}

	public void setCommonBytes(byte[] buffer) {
		setCommonBytes(buffer, 1);
	}

	/**Original name: UHDR-COMMON<br>
	 * <pre>*************************************************************
	 *  HALLUHDR                                                   *
	 *  UNIT OF WORK RESPONSE MESSAGE HEADER DATA UMT              *
	 *  THIS COPYBOOK IS USED BY HALOMCM AND THE BUSINESS OBJECTS  *
	 *  TO READ/WRITE FROM/TO THE UOW RESPONSE HEADER UMT          *
	 * *************************************************************
	 *  MAINTENANCE LOG
	 *  SI#      DATE      PROG#     DESCRIPTION
	 *  -------- --------- --------- -------------------------------
	 *  SAVANNAH 11JAN00   18448     CREATED.
	 * *************************************************************</pre>*/
	public byte[] getCommonBytes() {
		byte[] buffer = new byte[Len.COMMON];
		return getCommonBytes(buffer, 1);
	}

	public void setCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		setKeyBytes(buffer, position);
		position += Len.KEY;
		uowBufferLength = MarshalByte.readFixedString(buffer, position, Len.UOW_BUFFER_LENGTH);
		position += Len.UOW_BUFFER_LENGTH;
		setUowMessageBufferBytes(buffer, position);
	}

	public byte[] getCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		getKeyBytes(buffer, position);
		position += Len.KEY;
		MarshalByte.writeString(buffer, position, uowBufferLength, Len.UOW_BUFFER_LENGTH);
		position += Len.UOW_BUFFER_LENGTH;
		getUowMessageBufferBytes(buffer, position);
		return buffer;
	}

	public void initCommonSpaces() {
		initKeySpaces();
		uowBufferLength = "";
		initUowMessageBufferSpaces();
	}

	public String getKeyFormatted() {
		return MarshalByteExt.bufferToStr(getKeyBytes());
	}

	public void setKeyBytes(byte[] buffer) {
		setKeyBytes(buffer, 1);
	}

	/**Original name: UHDR-KEY<br>*/
	public byte[] getKeyBytes() {
		byte[] buffer = new byte[Len.KEY];
		return getKeyBytes(buffer, 1);
	}

	public void setKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		id = MarshalByte.readString(buffer, position, Len.ID);
		position += Len.ID;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
	}

	public byte[] getKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, id, Len.ID);
		position += Len.ID;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		return buffer;
	}

	public void initKeySpaces() {
		id = "";
		busObjNm = "";
	}

	public void setId(String id) {
		this.id = Functions.subString(id, Len.ID);
	}

	public String getId() {
		return this.id;
	}

	public String getIdFormatted() {
		return Functions.padBlanks(getId(), Len.ID);
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public String getBusObjNmFormatted() {
		return Functions.padBlanks(getBusObjNm(), Len.BUS_OBJ_NM);
	}

	public void setUowBufferLength(short uowBufferLength) {
		this.uowBufferLength = NumericDisplay.asString(uowBufferLength, Len.UOW_BUFFER_LENGTH);
	}

	public short getUowBufferLength() {
		return NumericDisplay.asShort(this.uowBufferLength);
	}

	public void setUowMessageBufferBytes(byte[] buffer, int offset) {
		int position = offset;
		msgBusObjNm = MarshalByte.readString(buffer, position, Len.MSG_BUS_OBJ_NM);
		position += Len.MSG_BUS_OBJ_NM;
		msgBusObjCount = MarshalByte.readFixedString(buffer, position, Len.MSG_BUS_OBJ_COUNT);
		position += Len.MSG_BUS_OBJ_COUNT;
		msgBusObjDataLen = MarshalByte.readFixedString(buffer, position, Len.MSG_BUS_OBJ_DATA_LEN);
	}

	public byte[] getUowMessageBufferBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, msgBusObjNm, Len.MSG_BUS_OBJ_NM);
		position += Len.MSG_BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, msgBusObjCount, Len.MSG_BUS_OBJ_COUNT);
		position += Len.MSG_BUS_OBJ_COUNT;
		MarshalByte.writeString(buffer, position, msgBusObjDataLen, Len.MSG_BUS_OBJ_DATA_LEN);
		return buffer;
	}

	public void initUowMessageBufferSpaces() {
		msgBusObjNm = "";
		msgBusObjCount = "";
		msgBusObjDataLen = "";
	}

	public void setMsgBusObjNm(String msgBusObjNm) {
		this.msgBusObjNm = Functions.subString(msgBusObjNm, Len.MSG_BUS_OBJ_NM);
	}

	public String getMsgBusObjNm() {
		return this.msgBusObjNm;
	}

	public void setMsgBusObjCount(int msgBusObjCount) {
		this.msgBusObjCount = NumericDisplay.asString(msgBusObjCount, Len.MSG_BUS_OBJ_COUNT);
	}

	public void setMsgBusObjCountFormatted(String msgBusObjCount) {
		this.msgBusObjCount = Trunc.toUnsignedNumeric(msgBusObjCount, Len.MSG_BUS_OBJ_COUNT);
	}

	public int getMsgBusObjCount() {
		return NumericDisplay.asInt(this.msgBusObjCount);
	}

	public String getMsgBusObjCountFormatted() {
		return this.msgBusObjCount;
	}

	public void setMsgBusObjDataLen(short msgBusObjDataLen) {
		this.msgBusObjDataLen = NumericDisplay.asString(msgBusObjDataLen, Len.MSG_BUS_OBJ_DATA_LEN);
	}

	public short getMsgBusObjDataLen() {
		return NumericDisplay.asShort(this.msgBusObjDataLen);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID = 32;
		public static final int BUS_OBJ_NM = 32;
		public static final int UOW_BUFFER_LENGTH = 2;
		public static final int MSG_BUS_OBJ_NM = 32;
		public static final int MSG_BUS_OBJ_COUNT = 5;
		public static final int MSG_BUS_OBJ_DATA_LEN = 4;
		public static final int KEY = ID + BUS_OBJ_NM;
		public static final int UOW_MESSAGE_BUFFER = MSG_BUS_OBJ_NM + MSG_BUS_OBJ_COUNT + MSG_BUS_OBJ_DATA_LEN;
		public static final int COMMON = KEY + UOW_BUFFER_LENGTH + UOW_MESSAGE_BUFFER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
