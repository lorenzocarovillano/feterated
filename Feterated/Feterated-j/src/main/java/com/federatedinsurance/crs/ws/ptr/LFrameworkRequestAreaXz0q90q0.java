/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q90Q0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q90q0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q90q0() {
	}

	public LFrameworkRequestAreaXz0q90q0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza9q0qCsrActNbr(String xza9q0qCsrActNbr) {
		writeString(Pos.XZA9Q0_CSR_ACT_NBR, xza9q0qCsrActNbr, Len.XZA9Q0_CSR_ACT_NBR);
	}

	/**Original name: XZA9Q0Q-CSR-ACT-NBR<br>*/
	public String getXza9q0qCsrActNbr() {
		return readString(Pos.XZA9Q0_CSR_ACT_NBR, Len.XZA9Q0_CSR_ACT_NBR);
	}

	public void setXza9q0qUserid(String xza9q0qUserid) {
		writeString(Pos.XZA9Q0_USERID, xza9q0qUserid, Len.XZA9Q0_USERID);
	}

	/**Original name: XZA9Q0Q-USERID<br>*/
	public String getXza9q0qUserid() {
		return readString(Pos.XZA9Q0_USERID, Len.XZA9Q0_USERID);
	}

	public void setXza9q0qActOwnCltId(String xza9q0qActOwnCltId) {
		writeString(Pos.XZA9Q0_ACT_OWN_CLT_ID, xza9q0qActOwnCltId, Len.XZA9Q0_ACT_OWN_CLT_ID);
	}

	/**Original name: XZA9Q0Q-ACT-OWN-CLT-ID<br>*/
	public String getXza9q0qActOwnCltId() {
		return readString(Pos.XZA9Q0_ACT_OWN_CLT_ID, Len.XZA9Q0_ACT_OWN_CLT_ID);
	}

	public void setXza9q0qActOwnAdrId(String xza9q0qActOwnAdrId) {
		writeString(Pos.XZA9Q0_ACT_OWN_ADR_ID, xza9q0qActOwnAdrId, Len.XZA9Q0_ACT_OWN_ADR_ID);
	}

	/**Original name: XZA9Q0Q-ACT-OWN-ADR-ID<br>*/
	public String getXza9q0qActOwnAdrId() {
		return readString(Pos.XZA9Q0_ACT_OWN_ADR_ID, Len.XZA9Q0_ACT_OWN_ADR_ID);
	}

	public void setXza9q0qSegCd(String xza9q0qSegCd) {
		writeString(Pos.XZA9Q0_SEG_CD, xza9q0qSegCd, Len.XZA9Q0_SEG_CD);
	}

	/**Original name: XZA9Q0Q-SEG-CD<br>*/
	public String getXza9q0qSegCd() {
		return readString(Pos.XZA9Q0_SEG_CD, Len.XZA9Q0_SEG_CD);
	}

	public void setXza9q0qActTypCd(String xza9q0qActTypCd) {
		writeString(Pos.XZA9Q0_ACT_TYP_CD, xza9q0qActTypCd, Len.XZA9Q0_ACT_TYP_CD);
	}

	/**Original name: XZA9Q0Q-ACT-TYP-CD<br>*/
	public String getXza9q0qActTypCd() {
		return readString(Pos.XZA9Q0_ACT_TYP_CD, Len.XZA9Q0_ACT_TYP_CD);
	}

	public void setXza9q0qPdcNbr(String xza9q0qPdcNbr) {
		writeString(Pos.XZA9Q0_PDC_NBR, xza9q0qPdcNbr, Len.XZA9Q0_PDC_NBR);
	}

	/**Original name: XZA9Q0Q-PDC-NBR<br>*/
	public String getXza9q0qPdcNbr() {
		return readString(Pos.XZA9Q0_PDC_NBR, Len.XZA9Q0_PDC_NBR);
	}

	public void setXza9q0qPdcNm(String xza9q0qPdcNm) {
		writeString(Pos.XZA9Q0_PDC_NM, xza9q0qPdcNm, Len.XZA9Q0_PDC_NM);
	}

	/**Original name: XZA9Q0Q-PDC-NM<br>*/
	public String getXza9q0qPdcNm() {
		return readString(Pos.XZA9Q0_PDC_NM, Len.XZA9Q0_PDC_NM);
	}

	public void setXza9q0qStAbb(String xza9q0qStAbb) {
		writeString(Pos.XZA9Q0_ST_ABB, xza9q0qStAbb, Len.XZA9Q0_ST_ABB);
	}

	/**Original name: XZA9Q0Q-ST-ABB<br>*/
	public String getXza9q0qStAbb() {
		return readString(Pos.XZA9Q0_ST_ABB, Len.XZA9Q0_ST_ABB);
	}

	public void setXza9q0qLastName(String xza9q0qLastName) {
		writeString(Pos.XZA9Q0_LAST_NAME, xza9q0qLastName, Len.XZA9Q0_LAST_NAME);
	}

	/**Original name: XZA9Q0Q-LAST-NAME<br>*/
	public String getXza9q0qLastName() {
		return readString(Pos.XZA9Q0_LAST_NAME, Len.XZA9Q0_LAST_NAME);
	}

	public void setXza9q0qNmAdrLin1(String xza9q0qNmAdrLin1) {
		writeString(Pos.XZA9Q0_NM_ADR_LIN1, xza9q0qNmAdrLin1, Len.XZA9Q0_NM_ADR_LIN1);
	}

	/**Original name: XZA9Q0Q-NM-ADR-LIN-1<br>*/
	public String getXza9q0qNmAdrLin1() {
		return readString(Pos.XZA9Q0_NM_ADR_LIN1, Len.XZA9Q0_NM_ADR_LIN1);
	}

	public void setXza9q0qNmAdrLin2(String xza9q0qNmAdrLin2) {
		writeString(Pos.XZA9Q0_NM_ADR_LIN2, xza9q0qNmAdrLin2, Len.XZA9Q0_NM_ADR_LIN2);
	}

	/**Original name: XZA9Q0Q-NM-ADR-LIN-2<br>*/
	public String getXza9q0qNmAdrLin2() {
		return readString(Pos.XZA9Q0_NM_ADR_LIN2, Len.XZA9Q0_NM_ADR_LIN2);
	}

	public void setXza9q0qNmAdrLin3(String xza9q0qNmAdrLin3) {
		writeString(Pos.XZA9Q0_NM_ADR_LIN3, xza9q0qNmAdrLin3, Len.XZA9Q0_NM_ADR_LIN3);
	}

	/**Original name: XZA9Q0Q-NM-ADR-LIN-3<br>*/
	public String getXza9q0qNmAdrLin3() {
		return readString(Pos.XZA9Q0_NM_ADR_LIN3, Len.XZA9Q0_NM_ADR_LIN3);
	}

	public void setXza9q0qNmAdrLin4(String xza9q0qNmAdrLin4) {
		writeString(Pos.XZA9Q0_NM_ADR_LIN4, xza9q0qNmAdrLin4, Len.XZA9Q0_NM_ADR_LIN4);
	}

	/**Original name: XZA9Q0Q-NM-ADR-LIN-4<br>*/
	public String getXza9q0qNmAdrLin4() {
		return readString(Pos.XZA9Q0_NM_ADR_LIN4, Len.XZA9Q0_NM_ADR_LIN4);
	}

	public void setXza9q0qNmAdrLin5(String xza9q0qNmAdrLin5) {
		writeString(Pos.XZA9Q0_NM_ADR_LIN5, xza9q0qNmAdrLin5, Len.XZA9Q0_NM_ADR_LIN5);
	}

	/**Original name: XZA9Q0Q-NM-ADR-LIN-5<br>*/
	public String getXza9q0qNmAdrLin5() {
		return readString(Pos.XZA9Q0_NM_ADR_LIN5, Len.XZA9Q0_NM_ADR_LIN5);
	}

	public void setXza9q0qNmAdrLin6(String xza9q0qNmAdrLin6) {
		writeString(Pos.XZA9Q0_NM_ADR_LIN6, xza9q0qNmAdrLin6, Len.XZA9Q0_NM_ADR_LIN6);
	}

	/**Original name: XZA9Q0Q-NM-ADR-LIN-6<br>*/
	public String getXza9q0qNmAdrLin6() {
		return readString(Pos.XZA9Q0_NM_ADR_LIN6, Len.XZA9Q0_NM_ADR_LIN6);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A90Q0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA9Q0_INSURED_INFO = L_FW_REQ_XZ0A90Q0;
		public static final int XZA9Q0_CSR_ACT_NBR = XZA9Q0_INSURED_INFO;
		public static final int XZA9Q0_USERID = XZA9Q0_CSR_ACT_NBR + Len.XZA9Q0_CSR_ACT_NBR;
		public static final int XZA9Q0_ACT_OWN_CLT_ID = XZA9Q0_USERID + Len.XZA9Q0_USERID;
		public static final int XZA9Q0_ACT_OWN_ADR_ID = XZA9Q0_ACT_OWN_CLT_ID + Len.XZA9Q0_ACT_OWN_CLT_ID;
		public static final int XZA9Q0_SEG_CD = XZA9Q0_ACT_OWN_ADR_ID + Len.XZA9Q0_ACT_OWN_ADR_ID;
		public static final int XZA9Q0_ACT_TYP_CD = XZA9Q0_SEG_CD + Len.XZA9Q0_SEG_CD;
		public static final int XZA9Q0_PDC_NBR = XZA9Q0_ACT_TYP_CD + Len.XZA9Q0_ACT_TYP_CD;
		public static final int XZA9Q0_PDC_NM = XZA9Q0_PDC_NBR + Len.XZA9Q0_PDC_NBR;
		public static final int XZA9Q0_ST_ABB = XZA9Q0_PDC_NM + Len.XZA9Q0_PDC_NM;
		public static final int XZA9Q0_LAST_NAME = XZA9Q0_ST_ABB + Len.XZA9Q0_ST_ABB;
		public static final int XZA9Q0_NM_ADR_LIN1 = XZA9Q0_LAST_NAME + Len.XZA9Q0_LAST_NAME;
		public static final int XZA9Q0_NM_ADR_LIN2 = XZA9Q0_NM_ADR_LIN1 + Len.XZA9Q0_NM_ADR_LIN1;
		public static final int XZA9Q0_NM_ADR_LIN3 = XZA9Q0_NM_ADR_LIN2 + Len.XZA9Q0_NM_ADR_LIN2;
		public static final int XZA9Q0_NM_ADR_LIN4 = XZA9Q0_NM_ADR_LIN3 + Len.XZA9Q0_NM_ADR_LIN3;
		public static final int XZA9Q0_NM_ADR_LIN5 = XZA9Q0_NM_ADR_LIN4 + Len.XZA9Q0_NM_ADR_LIN4;
		public static final int XZA9Q0_NM_ADR_LIN6 = XZA9Q0_NM_ADR_LIN5 + Len.XZA9Q0_NM_ADR_LIN5;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9Q0_CSR_ACT_NBR = 9;
		public static final int XZA9Q0_USERID = 8;
		public static final int XZA9Q0_ACT_OWN_CLT_ID = 64;
		public static final int XZA9Q0_ACT_OWN_ADR_ID = 64;
		public static final int XZA9Q0_SEG_CD = 3;
		public static final int XZA9Q0_ACT_TYP_CD = 2;
		public static final int XZA9Q0_PDC_NBR = 5;
		public static final int XZA9Q0_PDC_NM = 120;
		public static final int XZA9Q0_ST_ABB = 2;
		public static final int XZA9Q0_LAST_NAME = 60;
		public static final int XZA9Q0_NM_ADR_LIN1 = 45;
		public static final int XZA9Q0_NM_ADR_LIN2 = 45;
		public static final int XZA9Q0_NM_ADR_LIN3 = 45;
		public static final int XZA9Q0_NM_ADR_LIN4 = 45;
		public static final int XZA9Q0_NM_ADR_LIN5 = 45;
		public static final int XZA9Q0_NM_ADR_LIN6 = 45;
		public static final int XZA9Q0_INSURED_INFO = XZA9Q0_CSR_ACT_NBR + XZA9Q0_USERID + XZA9Q0_ACT_OWN_CLT_ID + XZA9Q0_ACT_OWN_ADR_ID
				+ XZA9Q0_SEG_CD + XZA9Q0_ACT_TYP_CD + XZA9Q0_PDC_NBR + XZA9Q0_PDC_NM + XZA9Q0_ST_ABB + XZA9Q0_LAST_NAME + XZA9Q0_NM_ADR_LIN1
				+ XZA9Q0_NM_ADR_LIN2 + XZA9Q0_NM_ADR_LIN3 + XZA9Q0_NM_ADR_LIN4 + XZA9Q0_NM_ADR_LIN5 + XZA9Q0_NM_ADR_LIN6;
		public static final int L_FW_REQ_XZ0A90Q0 = XZA9Q0_INSURED_INFO;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A90Q0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
