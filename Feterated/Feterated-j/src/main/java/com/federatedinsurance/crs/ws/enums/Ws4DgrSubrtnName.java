/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS4-DGR-SUBRTN-NAME<br>
 * Variable: WS4-DGR-SUBRTN-NAME from copybook CISLNSRB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ws4DgrSubrtnName {

	//==== PROPERTIES ====
	private String value = "";
	private static final String[] SCRUB_CALLED_SUBRTN_IN_ERROR = new String[] { "DGRUSTR", "DGRSTRG" };

	//==== METHODS ====
	public void setSubrtnName(String subrtnName) {
		this.value = Functions.subString(subrtnName, Len.SUBRTN_NAME);
	}

	public String getSubrtnName() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SUBRTN_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
