/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: TU-UNIQUE-POLICY-NINS<br>
 * Variable: TU-UNIQUE-POLICY-NINS from program XZ0P90E0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TuUniquePolicyNins {

	//==== PROPERTIES ====
	public static final int POLICY_NBRS_MAXOCCURS = 100;
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TU_UNIQUE_POLICY_NINS);
	//Original name: TU-NIN-CLIENT-ID
	private String ninClientId = DefaultValues.stringVal(Len.NIN_CLIENT_ID);
	//Original name: TU-NIN-ADR-ID
	private String ninAdrId = DefaultValues.stringVal(Len.NIN_ADR_ID);
	//Original name: TU-POLICY-NBRS
	private TuPolicyNbrs[] policyNbrs = new TuPolicyNbrs[POLICY_NBRS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public TuUniquePolicyNins() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int policyNbrsIdx = 1; policyNbrsIdx <= POLICY_NBRS_MAXOCCURS; policyNbrsIdx++) {
			policyNbrs[policyNbrsIdx - 1] = new TuPolicyNbrs();
		}
	}

	public String getTuUniquePolicyNinsFormatted() {
		return MarshalByteExt.bufferToStr(getTuUniquePolicyNinsBytes());
	}

	public byte[] getTuUniquePolicyNinsBytes() {
		byte[] buffer = new byte[Len.TU_UNIQUE_POLICY_NINS];
		return getTuUniquePolicyNinsBytes(buffer, 1);
	}

	public byte[] getTuUniquePolicyNinsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ninClientId, Len.NIN_CLIENT_ID);
		position += Len.NIN_CLIENT_ID;
		MarshalByte.writeString(buffer, position, ninAdrId, Len.NIN_ADR_ID);
		position += Len.NIN_ADR_ID;
		for (int idx = 1; idx <= POLICY_NBRS_MAXOCCURS; idx++) {
			policyNbrs[idx - 1].getPolicyNbrsBytes(buffer, position);
			position += TuPolicyNbrs.Len.POLICY_NBRS;
		}
		return buffer;
	}

	public void initTuUniquePolicyNinsHighValues() {
		ninClientId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NIN_CLIENT_ID);
		ninAdrId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NIN_ADR_ID);
		for (int idx = 1; idx <= POLICY_NBRS_MAXOCCURS; idx++) {
			policyNbrs[idx - 1].initPolicyNbrsHighValues();
		}
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTuUniquePolicyNinsFormatted()).equals(END_OF_TABLE);
	}

	public void setNinClientId(String ninClientId) {
		this.ninClientId = Functions.subString(ninClientId, Len.NIN_CLIENT_ID);
	}

	public String getNinClientId() {
		return this.ninClientId;
	}

	public void setNinAdrId(String ninAdrId) {
		this.ninAdrId = Functions.subString(ninAdrId, Len.NIN_ADR_ID);
	}

	public String getNinAdrId() {
		return this.ninAdrId;
	}

	public TuPolicyNbrs getPolicyNbrs(int idx) {
		return policyNbrs[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NIN_CLIENT_ID = 64;
		public static final int NIN_ADR_ID = 64;
		public static final int TU_UNIQUE_POLICY_NINS = NIN_CLIENT_ID + NIN_ADR_ID
				+ TuUniquePolicyNins.POLICY_NBRS_MAXOCCURS * TuPolicyNbrs.Len.POLICY_NBRS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
