/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XZC007-ACT-NOT-FRM-REC-ROW<br>
 * Variable: XZC007-ACT-NOT-FRM-REC-ROW from copybook XZ0C0007<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc007ActNotFrmRecRow {

	//==== PROPERTIES ====
	//Original name: XZC007-ACT-NOT-FRM-REC-CSUM
	private String actNotFrmRecCsum = DefaultValues.stringVal(Len.ACT_NOT_FRM_REC_CSUM);
	//Original name: XZC007-CSR-ACT-NBR-KCRE
	private String csrActNbrKcre = DefaultValues.stringVal(Len.CSR_ACT_NBR_KCRE);
	//Original name: XZC007-NOT-PRC-TS-KCRE
	private String notPrcTsKcre = DefaultValues.stringVal(Len.NOT_PRC_TS_KCRE);
	//Original name: XZC007-FRM-SEQ-NBR-KCRE
	private String frmSeqNbrKcre = DefaultValues.stringVal(Len.FRM_SEQ_NBR_KCRE);
	//Original name: XZC007-REC-SEQ-NBR-KCRE
	private String recSeqNbrKcre = DefaultValues.stringVal(Len.REC_SEQ_NBR_KCRE);
	//Original name: XZC007-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC007-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC007-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC007-FRM-SEQ-NBR-SIGN
	private char frmSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: XZC007-FRM-SEQ-NBR
	private String frmSeqNbr = DefaultValues.stringVal(Len.FRM_SEQ_NBR);
	//Original name: XZC007-REC-SEQ-NBR-SIGN
	private char recSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: XZC007-REC-SEQ-NBR
	private String recSeqNbr = DefaultValues.stringVal(Len.REC_SEQ_NBR);
	//Original name: XZC007-CSR-ACT-NBR-CI
	private char csrActNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC007-NOT-PRC-TS-CI
	private char notPrcTsCi = DefaultValues.CHAR_VAL;
	//Original name: XZC007-FRM-SEQ-NBR-CI
	private char frmSeqNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC007-REC-SEQ-NBR-CI
	private char recSeqNbrCi = DefaultValues.CHAR_VAL;
	/**Original name: FILLER-XZC007-ACT-NOT-FRM-REC-DATA<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:
	 * *  NO DATA OTHER THAN KEY FIELDS - BUT NEED TO LEAVE THE
	 * *  07 DATA ELEMENT IN ORDER FOR THE COMPILE TO WORK.</pre>*/
	private char flr1 = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setXzc007ActNotFrmRecRowFormatted(String data) {
		byte[] buffer = new byte[Len.XZC007_ACT_NOT_FRM_REC_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.XZC007_ACT_NOT_FRM_REC_ROW);
		setXzc007ActNotFrmRecRowBytes(buffer, 1);
	}

	public String getXzc007ActNotFrmRecRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzc007ActNotFrmRecRowBytes());
	}

	public byte[] getXzc007ActNotFrmRecRowBytes() {
		byte[] buffer = new byte[Len.XZC007_ACT_NOT_FRM_REC_ROW];
		return getXzc007ActNotFrmRecRowBytes(buffer, 1);
	}

	public void setXzc007ActNotFrmRecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotFrmRecFixedBytes(buffer, position);
		position += Len.ACT_NOT_FRM_REC_FIXED;
		setActNotFrmRecDatesBytes(buffer, position);
		position += Len.ACT_NOT_FRM_REC_DATES;
		setActNotFrmRecKeyBytes(buffer, position);
		position += Len.ACT_NOT_FRM_REC_KEY;
		setActNotFrmRecKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_FRM_REC_KEY_CI;
		setActNotFrmRecDataBytes(buffer, position);
	}

	public byte[] getXzc007ActNotFrmRecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotFrmRecFixedBytes(buffer, position);
		position += Len.ACT_NOT_FRM_REC_FIXED;
		getActNotFrmRecDatesBytes(buffer, position);
		position += Len.ACT_NOT_FRM_REC_DATES;
		getActNotFrmRecKeyBytes(buffer, position);
		position += Len.ACT_NOT_FRM_REC_KEY;
		getActNotFrmRecKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_FRM_REC_KEY_CI;
		getActNotFrmRecDataBytes(buffer, position);
		return buffer;
	}

	public void setActNotFrmRecFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotFrmRecCsum = MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_FRM_REC_CSUM);
		position += Len.ACT_NOT_FRM_REC_CSUM;
		csrActNbrKcre = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		notPrcTsKcre = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		frmSeqNbrKcre = MarshalByte.readString(buffer, position, Len.FRM_SEQ_NBR_KCRE);
		position += Len.FRM_SEQ_NBR_KCRE;
		recSeqNbrKcre = MarshalByte.readString(buffer, position, Len.REC_SEQ_NBR_KCRE);
	}

	public byte[] getActNotFrmRecFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotFrmRecCsum, Len.ACT_NOT_FRM_REC_CSUM);
		position += Len.ACT_NOT_FRM_REC_CSUM;
		MarshalByte.writeString(buffer, position, csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		MarshalByte.writeString(buffer, position, notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		MarshalByte.writeString(buffer, position, frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
		position += Len.FRM_SEQ_NBR_KCRE;
		MarshalByte.writeString(buffer, position, recSeqNbrKcre, Len.REC_SEQ_NBR_KCRE);
		return buffer;
	}

	public void initActNotFrmRecFixedSpaces() {
		actNotFrmRecCsum = "";
		csrActNbrKcre = "";
		notPrcTsKcre = "";
		frmSeqNbrKcre = "";
		recSeqNbrKcre = "";
	}

	public void setActNotFrmRecCsumFormatted(String actNotFrmRecCsum) {
		this.actNotFrmRecCsum = Trunc.toUnsignedNumeric(actNotFrmRecCsum, Len.ACT_NOT_FRM_REC_CSUM);
	}

	public int getActNotFrmRecCsum() {
		return NumericDisplay.asInt(this.actNotFrmRecCsum);
	}

	public String getActNotFrmRecCsumFormatted() {
		return this.actNotFrmRecCsum;
	}

	public String getActNotFrmRecCsumAsString() {
		return getActNotFrmRecCsumFormatted();
	}

	public void setCsrActNbrKcre(String csrActNbrKcre) {
		this.csrActNbrKcre = Functions.subString(csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
	}

	public String getCsrActNbrKcre() {
		return this.csrActNbrKcre;
	}

	public String getCsrActNbrKcreFormatted() {
		return Functions.padBlanks(getCsrActNbrKcre(), Len.CSR_ACT_NBR_KCRE);
	}

	public void setNotPrcTsKcre(String notPrcTsKcre) {
		this.notPrcTsKcre = Functions.subString(notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
	}

	public String getNotPrcTsKcre() {
		return this.notPrcTsKcre;
	}

	public String getNotPrcTsKcreFormatted() {
		return Functions.padBlanks(getNotPrcTsKcre(), Len.NOT_PRC_TS_KCRE);
	}

	public void setFrmSeqNbrKcre(String frmSeqNbrKcre) {
		this.frmSeqNbrKcre = Functions.subString(frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
	}

	public String getFrmSeqNbrKcre() {
		return this.frmSeqNbrKcre;
	}

	public String getFrmSeqNbrKcreFormatted() {
		return Functions.padBlanks(getFrmSeqNbrKcre(), Len.FRM_SEQ_NBR_KCRE);
	}

	public void setRecSeqNbrKcre(String recSeqNbrKcre) {
		this.recSeqNbrKcre = Functions.subString(recSeqNbrKcre, Len.REC_SEQ_NBR_KCRE);
	}

	public String getRecSeqNbrKcre() {
		return this.recSeqNbrKcre;
	}

	public String getRecSeqNbrKcreFormatted() {
		return Functions.padBlanks(getRecSeqNbrKcre(), Len.REC_SEQ_NBR_KCRE);
	}

	public void setActNotFrmRecDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getActNotFrmRecDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public String getTransProcessDtFormatted() {
		return Functions.padBlanks(getTransProcessDt(), Len.TRANS_PROCESS_DT);
	}

	public String getActNotFrmRecKeyFormatted() {
		return MarshalByteExt.bufferToStr(getActNotFrmRecKeyBytes());
	}

	/**Original name: XZC007-ACT-NOT-FRM-REC-KEY<br>*/
	public byte[] getActNotFrmRecKeyBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_FRM_REC_KEY];
		return getActNotFrmRecKeyBytes(buffer, 1);
	}

	public void setActNotFrmRecKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		frmSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		frmSeqNbr = MarshalByte.readFixedString(buffer, position, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		recSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recSeqNbr = MarshalByte.readFixedString(buffer, position, Len.REC_SEQ_NBR);
	}

	public byte[] getActNotFrmRecKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, frmSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		MarshalByte.writeChar(buffer, position, recSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, recSeqNbr, Len.REC_SEQ_NBR);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setFrmSeqNbrSign(char frmSeqNbrSign) {
		this.frmSeqNbrSign = frmSeqNbrSign;
	}

	public void setFrmSeqNbrSignFormatted(String frmSeqNbrSign) {
		setFrmSeqNbrSign(Functions.charAt(frmSeqNbrSign, Types.CHAR_SIZE));
	}

	public char getFrmSeqNbrSign() {
		return this.frmSeqNbrSign;
	}

	public void setFrmSeqNbr(int frmSeqNbr) {
		this.frmSeqNbr = NumericDisplay.asString(frmSeqNbr, Len.FRM_SEQ_NBR);
	}

	public void setFrmSeqNbrFormatted(String frmSeqNbr) {
		this.frmSeqNbr = Trunc.toUnsignedNumeric(frmSeqNbr, Len.FRM_SEQ_NBR);
	}

	public int getFrmSeqNbr() {
		return NumericDisplay.asInt(this.frmSeqNbr);
	}

	public String getFrmSeqNbrFormatted() {
		return this.frmSeqNbr;
	}

	public String getFrmSeqNbrAsString() {
		return getFrmSeqNbrFormatted();
	}

	public void setRecSeqNbrSign(char recSeqNbrSign) {
		this.recSeqNbrSign = recSeqNbrSign;
	}

	public void setRecSeqNbrSignFormatted(String recSeqNbrSign) {
		setRecSeqNbrSign(Functions.charAt(recSeqNbrSign, Types.CHAR_SIZE));
	}

	public char getRecSeqNbrSign() {
		return this.recSeqNbrSign;
	}

	public void setRecSeqNbr(int recSeqNbr) {
		this.recSeqNbr = NumericDisplay.asString(recSeqNbr, Len.REC_SEQ_NBR);
	}

	public void setRecSeqNbrFormatted(String recSeqNbr) {
		this.recSeqNbr = Trunc.toUnsignedNumeric(recSeqNbr, Len.REC_SEQ_NBR);
	}

	public int getRecSeqNbr() {
		return NumericDisplay.asInt(this.recSeqNbr);
	}

	public String getRecSeqNbrFormatted() {
		return this.recSeqNbr;
	}

	public String getRecSeqNbrAsString() {
		return getRecSeqNbrFormatted();
	}

	public void setActNotFrmRecKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notPrcTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		frmSeqNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recSeqNbrCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotFrmRecKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, frmSeqNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, recSeqNbrCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi = csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi = notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setFrmSeqNbrCi(char frmSeqNbrCi) {
		this.frmSeqNbrCi = frmSeqNbrCi;
	}

	public char getFrmSeqNbrCi() {
		return this.frmSeqNbrCi;
	}

	public void setRecSeqNbrCi(char recSeqNbrCi) {
		this.recSeqNbrCi = recSeqNbrCi;
	}

	public char getRecSeqNbrCi() {
		return this.recSeqNbrCi;
	}

	public void setActNotFrmRecDataBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotFrmRecDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		return buffer;
	}

	public void initActNotFrmRecDataSpaces() {
		flr1 = Types.SPACE_CHAR;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_FRM_REC_CSUM = 9;
		public static final int CSR_ACT_NBR_KCRE = 32;
		public static final int NOT_PRC_TS_KCRE = 32;
		public static final int FRM_SEQ_NBR_KCRE = 32;
		public static final int REC_SEQ_NBR_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FRM_SEQ_NBR = 5;
		public static final int REC_SEQ_NBR = 5;
		public static final int ACT_NOT_FRM_REC_FIXED = ACT_NOT_FRM_REC_CSUM + CSR_ACT_NBR_KCRE + NOT_PRC_TS_KCRE + FRM_SEQ_NBR_KCRE
				+ REC_SEQ_NBR_KCRE;
		public static final int ACT_NOT_FRM_REC_DATES = TRANS_PROCESS_DT;
		public static final int FRM_SEQ_NBR_SIGN = 1;
		public static final int REC_SEQ_NBR_SIGN = 1;
		public static final int ACT_NOT_FRM_REC_KEY = CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR_SIGN + FRM_SEQ_NBR + REC_SEQ_NBR_SIGN + REC_SEQ_NBR;
		public static final int CSR_ACT_NBR_CI = 1;
		public static final int NOT_PRC_TS_CI = 1;
		public static final int FRM_SEQ_NBR_CI = 1;
		public static final int REC_SEQ_NBR_CI = 1;
		public static final int ACT_NOT_FRM_REC_KEY_CI = CSR_ACT_NBR_CI + NOT_PRC_TS_CI + FRM_SEQ_NBR_CI + REC_SEQ_NBR_CI;
		public static final int FLR1 = 1;
		public static final int ACT_NOT_FRM_REC_DATA = FLR1;
		public static final int XZC007_ACT_NOT_FRM_REC_ROW = ACT_NOT_FRM_REC_FIXED + ACT_NOT_FRM_REC_DATES + ACT_NOT_FRM_REC_KEY
				+ ACT_NOT_FRM_REC_KEY_CI + ACT_NOT_FRM_REC_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
