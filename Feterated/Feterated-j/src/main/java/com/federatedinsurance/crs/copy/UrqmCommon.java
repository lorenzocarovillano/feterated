/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.UbocPassThruAction;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: URQM-COMMON<br>
 * Variable: URQM-COMMON from copybook HALLURQM<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class UrqmCommon {

	//==== PROPERTIES ====
	//Original name: URQM-ID
	private String id = DefaultValues.stringVal(Len.ID);
	//Original name: URQM-BUS-OBJ
	private String busObj = DefaultValues.stringVal(Len.BUS_OBJ);
	//Original name: URQM-REC-SEQ
	private String recSeq = DefaultValues.stringVal(Len.REC_SEQ);
	/**Original name: URQM-UOW-BUFFER-LENGTH<br>
	 * <pre>**       10  URQM-REC-SEQ                   PIC 9(03).</pre>*/
	private String uowBufferLength = DefaultValues.stringVal(Len.UOW_BUFFER_LENGTH);
	/**Original name: URQM-MSG-BUS-OBJ-NM<br>
	 * <pre>** UOW MESSAGE DATA HEADER FOR DATA ROWS IN THE INCOMING
	 * ** MESSAGE AND THE UOW MESSAGE UMT.</pre>*/
	private String msgBusObjNm = DefaultValues.stringVal(Len.MSG_BUS_OBJ_NM);
	//Original name: URQM-ACTION-CODE
	private UbocPassThruAction actionCode = new UbocPassThruAction();
	//Original name: URQM-BUS-OBJ-DATA-LENGTH
	private String busObjDataLength = DefaultValues.stringVal(Len.BUS_OBJ_DATA_LENGTH);
	//Original name: URQM-MSG-DATA
	private String msgData = DefaultValues.stringVal(Len.MSG_DATA);

	//==== METHODS ====
	public void setUrqmCommonBytes(byte[] buffer) {
		setUrqmCommonBytes(buffer, 1);
	}

	public byte[] getUrqmCommonBytes() {
		byte[] buffer = new byte[Len.URQM_COMMON];
		return getUrqmCommonBytes(buffer, 1);
	}

	public void setUrqmCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		setKeyBytes(buffer, position);
		position += Len.KEY;
		uowBufferLength = MarshalByte.readFixedString(buffer, position, Len.UOW_BUFFER_LENGTH);
		position += Len.UOW_BUFFER_LENGTH;
		setUowMessageBufferBytes(buffer, position);
	}

	public byte[] getUrqmCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		getKeyBytes(buffer, position);
		position += Len.KEY;
		MarshalByte.writeString(buffer, position, uowBufferLength, Len.UOW_BUFFER_LENGTH);
		position += Len.UOW_BUFFER_LENGTH;
		getUowMessageBufferBytes(buffer, position);
		return buffer;
	}

	public String getKeyFormatted() {
		return MarshalByteExt.bufferToStr(getKeyBytes());
	}

	public void setKeyBytes(byte[] buffer) {
		setKeyBytes(buffer, 1);
	}

	/**Original name: URQM-KEY<br>*/
	public byte[] getKeyBytes() {
		byte[] buffer = new byte[Len.KEY];
		return getKeyBytes(buffer, 1);
	}

	public void setKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		setPartialKeyBytes(buffer, position);
		position += Len.PARTIAL_KEY;
		recSeq = MarshalByte.readFixedString(buffer, position, Len.REC_SEQ);
	}

	public byte[] getKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		getPartialKeyBytes(buffer, position);
		position += Len.PARTIAL_KEY;
		MarshalByte.writeString(buffer, position, recSeq, Len.REC_SEQ);
		return buffer;
	}

	/**Original name: URQM-PARTIAL-KEY<br>*/
	public byte[] getPartialKeyBytes() {
		byte[] buffer = new byte[Len.PARTIAL_KEY];
		return getPartialKeyBytes(buffer, 1);
	}

	public void setPartialKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		id = MarshalByte.readString(buffer, position, Len.ID);
		position += Len.ID;
		busObj = MarshalByte.readString(buffer, position, Len.BUS_OBJ);
	}

	public byte[] getPartialKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, id, Len.ID);
		position += Len.ID;
		MarshalByte.writeString(buffer, position, busObj, Len.BUS_OBJ);
		return buffer;
	}

	public void setId(String id) {
		this.id = Functions.subString(id, Len.ID);
	}

	public String getId() {
		return this.id;
	}

	public String getIdFormatted() {
		return Functions.padBlanks(getId(), Len.ID);
	}

	public void setBusObj(String busObj) {
		this.busObj = Functions.subString(busObj, Len.BUS_OBJ);
	}

	public String getBusObj() {
		return this.busObj;
	}

	public String getBusObjFormatted() {
		return Functions.padBlanks(getBusObj(), Len.BUS_OBJ);
	}

	public void setRecSeq(int recSeq) {
		this.recSeq = NumericDisplay.asString(recSeq, Len.REC_SEQ);
	}

	public void setRecSeqFormatted(String recSeq) {
		this.recSeq = Trunc.toUnsignedNumeric(recSeq, Len.REC_SEQ);
	}

	public int getRecSeq() {
		return NumericDisplay.asInt(this.recSeq);
	}

	public String getRecSeqFormatted() {
		return this.recSeq;
	}

	public String getRecSeqAsString() {
		return getRecSeqFormatted();
	}

	public void setUowBufferLength(short uowBufferLength) {
		this.uowBufferLength = NumericDisplay.asString(uowBufferLength, Len.UOW_BUFFER_LENGTH);
	}

	public void setUowBufferLengthFormatted(String uowBufferLength) {
		this.uowBufferLength = Trunc.toUnsignedNumeric(uowBufferLength, Len.UOW_BUFFER_LENGTH);
	}

	public short getUowBufferLength() {
		return NumericDisplay.asShort(this.uowBufferLength);
	}

	public void setUowMessageBufferBytes(byte[] buffer, int offset) {
		int position = offset;
		setMsgHdrBytes(buffer, position);
		position += Len.MSG_HDR;
		msgData = MarshalByte.readString(buffer, position, Len.MSG_DATA);
	}

	public byte[] getUowMessageBufferBytes(byte[] buffer, int offset) {
		int position = offset;
		getMsgHdrBytes(buffer, position);
		position += Len.MSG_HDR;
		MarshalByte.writeString(buffer, position, msgData, Len.MSG_DATA);
		return buffer;
	}

	public void initUowMessageBufferSpaces() {
		initMsgHdrSpaces();
		msgData = "";
	}

	public void setMsgHdrBytes(byte[] buffer, int offset) {
		int position = offset;
		msgBusObjNm = MarshalByte.readString(buffer, position, Len.MSG_BUS_OBJ_NM);
		position += Len.MSG_BUS_OBJ_NM;
		actionCode.setUbocPassThruAction(MarshalByte.readString(buffer, position, UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION));
		position += UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION;
		busObjDataLength = MarshalByte.readFixedString(buffer, position, Len.BUS_OBJ_DATA_LENGTH);
	}

	public byte[] getMsgHdrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, msgBusObjNm, Len.MSG_BUS_OBJ_NM);
		position += Len.MSG_BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, actionCode.getUbocPassThruAction(), UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION);
		position += UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION;
		MarshalByte.writeString(buffer, position, busObjDataLength, Len.BUS_OBJ_DATA_LENGTH);
		return buffer;
	}

	public void initMsgHdrSpaces() {
		msgBusObjNm = "";
		actionCode.setUbocPassThruAction("");
		busObjDataLength = "";
	}

	public void setMsgBusObjNm(String msgBusObjNm) {
		this.msgBusObjNm = Functions.subString(msgBusObjNm, Len.MSG_BUS_OBJ_NM);
	}

	public String getMsgBusObjNm() {
		return this.msgBusObjNm;
	}

	public void setBusObjDataLength(short busObjDataLength) {
		this.busObjDataLength = NumericDisplay.asString(busObjDataLength, Len.BUS_OBJ_DATA_LENGTH);
	}

	public void setBusObjDataLengthFormatted(String busObjDataLength) {
		this.busObjDataLength = Trunc.toUnsignedNumeric(busObjDataLength, Len.BUS_OBJ_DATA_LENGTH);
	}

	public short getBusObjDataLength() {
		return NumericDisplay.asShort(this.busObjDataLength);
	}

	public void setMsgData(String msgData) {
		this.msgData = Functions.subString(msgData, Len.MSG_DATA);
	}

	public void setMsgDataSubstring(String replacement, int start, int length) {
		msgData = Functions.setSubstring(msgData, replacement, start, length);
	}

	public String getMsgData() {
		return this.msgData;
	}

	public String getMsgDataFormatted() {
		return Functions.padBlanks(getMsgData(), Len.MSG_DATA);
	}

	public UbocPassThruAction getActionCode() {
		return actionCode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID = 32;
		public static final int BUS_OBJ = 32;
		public static final int REC_SEQ = 5;
		public static final int UOW_BUFFER_LENGTH = 4;
		public static final int MSG_BUS_OBJ_NM = 32;
		public static final int BUS_OBJ_DATA_LENGTH = 4;
		public static final int MSG_DATA = 5000;
		public static final int PARTIAL_KEY = ID + BUS_OBJ;
		public static final int KEY = PARTIAL_KEY + REC_SEQ;
		public static final int MSG_HDR = MSG_BUS_OBJ_NM + UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION + BUS_OBJ_DATA_LENGTH;
		public static final int UOW_MESSAGE_BUFFER = MSG_HDR + MSG_DATA;
		public static final int URQM_COMMON = KEY + UOW_BUFFER_LENGTH + UOW_MESSAGE_BUFFER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
