/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ009I-SERVICE-INPUTS<br>
 * Variable: XZ009I-SERVICE-INPUTS from copybook XZC090CI<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz009iServiceInputs {

	//==== PROPERTIES ====
	//Original name: XZ009I-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZ009I-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZ009I-QTE-NBR
	private int qteNbr = DefaultValues.INT_VAL;
	//Original name: XZ009I-URI
	private String uri = DefaultValues.stringVal(Len.URI);
	//Original name: XZ009I-USR-ID
	private String usrId = DefaultValues.stringVal(Len.USR_ID);
	//Original name: XZ009I-PWD
	private String pwd = DefaultValues.stringVal(Len.PWD);
	//Original name: XZ009I-TAR-SYS
	private String tarSys = DefaultValues.stringVal(Len.TAR_SYS);

	//==== METHODS ====
	public void setXzc09iServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		polExpDt = MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		qteNbr = MarshalByte.readInt(buffer, position, Len.QTE_NBR);
		position += Len.QTE_NBR;
		uri = MarshalByte.readString(buffer, position, Len.URI);
		position += Len.URI;
		usrId = MarshalByte.readString(buffer, position, Len.USR_ID);
		position += Len.USR_ID;
		pwd = MarshalByte.readString(buffer, position, Len.PWD);
		position += Len.PWD;
		tarSys = MarshalByte.readString(buffer, position, Len.TAR_SYS);
	}

	public byte[] getXzc09iServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeInt(buffer, position, qteNbr, Len.QTE_NBR);
		position += Len.QTE_NBR;
		MarshalByte.writeString(buffer, position, uri, Len.URI);
		position += Len.URI;
		MarshalByte.writeString(buffer, position, usrId, Len.USR_ID);
		position += Len.USR_ID;
		MarshalByte.writeString(buffer, position, pwd, Len.PWD);
		position += Len.PWD;
		MarshalByte.writeString(buffer, position, tarSys, Len.TAR_SYS);
		return buffer;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public String getPolExpDtFormatted() {
		return Functions.padBlanks(getPolExpDt(), Len.POL_EXP_DT);
	}

	public void setQteNbr(int qteNbr) {
		this.qteNbr = qteNbr;
	}

	public int getQteNbr() {
		return this.qteNbr;
	}

	public void setUri(String uri) {
		this.uri = Functions.subString(uri, Len.URI);
	}

	public String getUri() {
		return this.uri;
	}

	public void setUsrId(String usrId) {
		this.usrId = Functions.subString(usrId, Len.USR_ID);
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setPwd(String pwd) {
		this.pwd = Functions.subString(pwd, Len.PWD);
	}

	public String getPwd() {
		return this.pwd;
	}

	public void setTarSys(String tarSys) {
		this.tarSys = Functions.subString(tarSys, Len.TAR_SYS);
	}

	public String getTarSys() {
		return this.tarSys;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int POL_EXP_DT = 10;
		public static final int QTE_NBR = 5;
		public static final int URI = 256;
		public static final int USR_ID = 8;
		public static final int PWD = 8;
		public static final int TAR_SYS = 5;
		public static final int XZC09I_SERVICE_INPUTS = POL_NBR + POL_EXP_DT + QTE_NBR + URI + USR_ID + PWD + TAR_SYS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
