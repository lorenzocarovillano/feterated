/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-HOLIDAY-ADJUSTMENT<br>
 * Variable: WS-HOLIDAY-ADJUSTMENT from program XPIODAT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsHolidayAdjustment {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char HOLIDAY_ADJUSTMENT_MADE = 'Y';
	public static final char NO_HOLIDAY_ADJUSTMENT_MADE = 'N';

	//==== METHODS ====
	public void setWsHolidayAdjustment(char wsHolidayAdjustment) {
		this.value = wsHolidayAdjustment;
	}

	public char getWsHolidayAdjustment() {
		return this.value;
	}

	public boolean isHolidayAdjustmentMade() {
		return value == HOLIDAY_ADJUSTMENT_MADE;
	}

	public void setHolidayAdjustmentMade() {
		value = HOLIDAY_ADJUSTMENT_MADE;
	}

	public void setNoHolidayAdjustmentMade() {
		value = NO_HOLIDAY_ADJUSTMENT_MADE;
	}
}
