/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-FA-ADDRESS-INPUTS<br>
 * Variable: L-FA-ADDRESS-INPUTS from copybook TS52901<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class LFaAddressInputs {

	//==== PROPERTIES ====
	//Original name: L-FA-AI-DISPLAY-NAME
	private String displayName = DefaultValues.stringVal(Len.DISPLAY_NAME);
	//Original name: L-FA-AI-NAME-LINE-1
	private String nameLine1 = DefaultValues.stringVal(Len.NAME_LINE1);
	//Original name: L-FA-AI-NAME-LINE-2
	private String nameLine2 = DefaultValues.stringVal(Len.NAME_LINE2);
	//Original name: L-FA-AI-ADR-LINE-1
	private String adrLine1 = DefaultValues.stringVal(Len.ADR_LINE1);
	//Original name: L-FA-AI-ADR-LINE-2
	private String adrLine2 = DefaultValues.stringVal(Len.ADR_LINE2);
	//Original name: L-FA-AI-CITY
	private String city = DefaultValues.stringVal(Len.CITY);
	//Original name: L-FA-AI-STATE-ABB
	private String stateAbb = DefaultValues.stringVal(Len.STATE_ABB);
	//Original name: L-FA-AI-POSTAL-CODE
	private String postalCode = DefaultValues.stringVal(Len.POSTAL_CODE);
	//Original name: FILLER-L-FA-ADDRESS-INPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setAddressInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		displayName = MarshalByte.readString(buffer, position, Len.DISPLAY_NAME);
		position += Len.DISPLAY_NAME;
		nameLine1 = MarshalByte.readString(buffer, position, Len.NAME_LINE1);
		position += Len.NAME_LINE1;
		nameLine2 = MarshalByte.readString(buffer, position, Len.NAME_LINE2);
		position += Len.NAME_LINE2;
		adrLine1 = MarshalByte.readString(buffer, position, Len.ADR_LINE1);
		position += Len.ADR_LINE1;
		adrLine2 = MarshalByte.readString(buffer, position, Len.ADR_LINE2);
		position += Len.ADR_LINE2;
		city = MarshalByte.readString(buffer, position, Len.CITY);
		position += Len.CITY;
		stateAbb = MarshalByte.readString(buffer, position, Len.STATE_ABB);
		position += Len.STATE_ABB;
		postalCode = MarshalByte.readString(buffer, position, Len.POSTAL_CODE);
		position += Len.POSTAL_CODE;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getAddressInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, displayName, Len.DISPLAY_NAME);
		position += Len.DISPLAY_NAME;
		MarshalByte.writeString(buffer, position, nameLine1, Len.NAME_LINE1);
		position += Len.NAME_LINE1;
		MarshalByte.writeString(buffer, position, nameLine2, Len.NAME_LINE2);
		position += Len.NAME_LINE2;
		MarshalByte.writeString(buffer, position, adrLine1, Len.ADR_LINE1);
		position += Len.ADR_LINE1;
		MarshalByte.writeString(buffer, position, adrLine2, Len.ADR_LINE2);
		position += Len.ADR_LINE2;
		MarshalByte.writeString(buffer, position, city, Len.CITY);
		position += Len.CITY;
		MarshalByte.writeString(buffer, position, stateAbb, Len.STATE_ABB);
		position += Len.STATE_ABB;
		MarshalByte.writeString(buffer, position, postalCode, Len.POSTAL_CODE);
		position += Len.POSTAL_CODE;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setDisplayName(String displayName) {
		this.displayName = Functions.subString(displayName, Len.DISPLAY_NAME);
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setNameLine1(String nameLine1) {
		this.nameLine1 = Functions.subString(nameLine1, Len.NAME_LINE1);
	}

	public String getNameLine1() {
		return this.nameLine1;
	}

	public void setNameLine2(String nameLine2) {
		this.nameLine2 = Functions.subString(nameLine2, Len.NAME_LINE2);
	}

	public String getNameLine2() {
		return this.nameLine2;
	}

	public void setAdrLine1(String adrLine1) {
		this.adrLine1 = Functions.subString(adrLine1, Len.ADR_LINE1);
	}

	public String getAdrLine1() {
		return this.adrLine1;
	}

	public void setAdrLine2(String adrLine2) {
		this.adrLine2 = Functions.subString(adrLine2, Len.ADR_LINE2);
	}

	public String getAdrLine2() {
		return this.adrLine2;
	}

	public void setCity(String city) {
		this.city = Functions.subString(city, Len.CITY);
	}

	public String getCity() {
		return this.city;
	}

	public void setStateAbb(String stateAbb) {
		this.stateAbb = Functions.subString(stateAbb, Len.STATE_ABB);
	}

	public String getStateAbb() {
		return this.stateAbb;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = Functions.subString(postalCode, Len.POSTAL_CODE);
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DISPLAY_NAME = 120;
		public static final int NAME_LINE1 = 45;
		public static final int NAME_LINE2 = 45;
		public static final int ADR_LINE1 = 45;
		public static final int ADR_LINE2 = 45;
		public static final int CITY = 30;
		public static final int STATE_ABB = 3;
		public static final int POSTAL_CODE = 13;
		public static final int FLR1 = 100;
		public static final int ADDRESS_INPUTS = DISPLAY_NAME + NAME_LINE1 + NAME_LINE2 + ADR_LINE1 + ADR_LINE2 + CITY + STATE_ABB + POSTAL_CODE
				+ FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
