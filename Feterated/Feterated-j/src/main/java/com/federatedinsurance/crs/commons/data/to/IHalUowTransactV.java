/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HAL_UOW_TRANSACT_V]
 * 
 */
public interface IHalUowTransactV extends BaseSqlTo {

	/**
	 * Host Variable HUTC-UOW-NM
	 * 
	 */
	String getHutcUowNm();

	void setHutcUowNm(String hutcUowNm);

	/**
	 * Host Variable HUTC-MCM-MDU-NM
	 * 
	 */
	String getMcmMduNm();

	void setMcmMduNm(String mcmMduNm);

	/**
	 * Host Variable HUTC-LOK-TMO-ITV
	 * 
	 */
	int getLokTmoItv();

	void setLokTmoItv(int lokTmoItv);

	/**
	 * Host Variable HUTC-LOK-SGY-CD
	 * 
	 */
	char getLokSgyCd();

	void setLokSgyCd(char lokSgyCd);

	/**
	 * Host Variable HUTC-SEC-IND
	 * 
	 */
	char getSecInd();

	void setSecInd(char secInd);

	/**
	 * Host Variable HUTC-SEC-MDU-NM
	 * 
	 */
	String getSecMduNm();

	void setSecMduNm(String secMduNm);

	/**
	 * Nullable property for HUTC-SEC-MDU-NM
	 * 
	 */
	String getSecMduNmObj();

	void setSecMduNmObj(String secMduNmObj);

	/**
	 * Host Variable HUTC-DTA-PVC-IND
	 * 
	 */
	char getDtaPvcInd();

	void setDtaPvcInd(char dtaPvcInd);

	/**
	 * Host Variable HUTC-AUDIT-IND
	 * 
	 */
	char getAuditInd();

	void setAuditInd(char auditInd);
};
