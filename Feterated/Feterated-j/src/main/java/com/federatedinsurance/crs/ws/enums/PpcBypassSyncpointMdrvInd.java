/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PPC-BYPASS-SYNCPOINT-MDRV-IND<br>
 * Variable: PPC-BYPASS-SYNCPOINT-MDRV-IND from copybook TS020PRO<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class PpcBypassSyncpointMdrvInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char BYPASS_SYNCPOINT_IN_MDRV = 'B';
	private static final char[] DO_NOT_BYPASS_SYNCPOINT = new char[] { Types.SPACE_CHAR, Types.LOW_CHAR_VAL, Types.HIGH_CHAR_VAL };

	//==== METHODS ====
	public void setPpcBypassSyncpointMdrvInd(char ppcBypassSyncpointMdrvInd) {
		this.value = ppcBypassSyncpointMdrvInd;
	}

	public void setPpcBypassSyncpointMdrvIndFormatted(String ppcBypassSyncpointMdrvInd) {
		setPpcBypassSyncpointMdrvInd(Functions.charAt(ppcBypassSyncpointMdrvInd, Types.CHAR_SIZE));
	}

	public char getPpcBypassSyncpointMdrvInd() {
		return this.value;
	}

	public void setBypassSyncpointInMdrv() {
		value = BYPASS_SYNCPOINT_IN_MDRV;
	}

	public void setPpcDoNotBypassSyncpoint() {
		setPpcBypassSyncpointMdrvIndFormatted(String.valueOf(DO_NOT_BYPASS_SYNCPOINT[0]));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PPC_BYPASS_SYNCPOINT_MDRV_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
