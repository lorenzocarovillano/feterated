/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: DSD-PERF-MONITORING-FLAG<br>
 * Variable: DSD-PERF-MONITORING-FLAG from copybook TS020DRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class DsdPerfMonitoringFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char OFF = '0';
	public static final char ON = '1';

	//==== METHODS ====
	public void setPerfMonitoringFlag(char perfMonitoringFlag) {
		this.value = perfMonitoringFlag;
	}

	public char getPerfMonitoringFlag() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PERF_MONITORING_FLAG = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
