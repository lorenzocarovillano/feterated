/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.DsdPerfMonitoringFlag;

/**Original name: DSD-PERF-MONITORING-PARMS<br>
 * Variable: DSD-PERF-MONITORING-PARMS from copybook TS020DRV<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class DsdPerfMonitoringParms {

	//==== PROPERTIES ====
	//Original name: DSD-PERF-MONITORING-FLAG
	private DsdPerfMonitoringFlag perfMonitoringFlag = new DsdPerfMonitoringFlag();
	//Original name: DSD-PM-BROKER-BEGIN
	private String pmBrokerBegin = DefaultValues.stringVal(Len.PM_BROKER_BEGIN);
	//Original name: DSD-PM-BROKER-END
	private String pmBrokerEnd = DefaultValues.stringVal(Len.PM_BROKER_END);
	//Original name: DSD-PM-BROKER-ELAPSED
	private String pmBrokerElapsed = DefaultValues.stringVal(Len.PM_BROKER_ELAPSED);
	//Original name: DSD-PM-REQHUB-ELAPSED
	private String pmReqhubElapsed = DefaultValues.stringVal(Len.PM_REQHUB_ELAPSED);
	//Original name: DSD-PM-UOW-ELAPSED
	private String pmUowElapsed = DefaultValues.stringVal(Len.PM_UOW_ELAPSED);
	//Original name: DSD-PM-RESHUB-ELAPSED
	private String pmReshubElapsed = DefaultValues.stringVal(Len.PM_RESHUB_ELAPSED);

	//==== METHODS ====
	public void setPerfMonitoringParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		perfMonitoringFlag.setPerfMonitoringFlag(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		pmBrokerBegin = MarshalByte.readFixedString(buffer, position, Len.PM_BROKER_BEGIN);
		position += Len.PM_BROKER_BEGIN;
		pmBrokerEnd = MarshalByte.readFixedString(buffer, position, Len.PM_BROKER_END);
		position += Len.PM_BROKER_END;
		pmBrokerElapsed = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.PM_BROKER_ELAPSED), Len.PM_BROKER_ELAPSED);
		position += Len.PM_BROKER_ELAPSED;
		pmReqhubElapsed = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.PM_REQHUB_ELAPSED), Len.PM_REQHUB_ELAPSED);
		position += Len.PM_REQHUB_ELAPSED;
		pmUowElapsed = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.PM_UOW_ELAPSED), Len.PM_UOW_ELAPSED);
		position += Len.PM_UOW_ELAPSED;
		pmReshubElapsed = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.PM_RESHUB_ELAPSED), Len.PM_RESHUB_ELAPSED);
	}

	public byte[] getPerfMonitoringParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, perfMonitoringFlag.getPerfMonitoringFlag());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pmBrokerBegin, Len.PM_BROKER_BEGIN);
		position += Len.PM_BROKER_BEGIN;
		MarshalByte.writeString(buffer, position, pmBrokerEnd, Len.PM_BROKER_END);
		position += Len.PM_BROKER_END;
		MarshalByte.writeString(buffer, position, pmBrokerElapsed, Len.PM_BROKER_ELAPSED);
		position += Len.PM_BROKER_ELAPSED;
		MarshalByte.writeString(buffer, position, pmReqhubElapsed, Len.PM_REQHUB_ELAPSED);
		position += Len.PM_REQHUB_ELAPSED;
		MarshalByte.writeString(buffer, position, pmUowElapsed, Len.PM_UOW_ELAPSED);
		position += Len.PM_UOW_ELAPSED;
		MarshalByte.writeString(buffer, position, pmReshubElapsed, Len.PM_RESHUB_ELAPSED);
		return buffer;
	}

	public void setPmBrokerBeginFromBuffer(byte[] buffer) {
		pmBrokerBegin = MarshalByte.readFixedString(buffer, 1, Len.PM_BROKER_BEGIN);
	}

	public String getPmBrokerBeginFormatted() {
		return this.pmBrokerBegin;
	}

	public void setPmBrokerEndFromBuffer(byte[] buffer) {
		pmBrokerEnd = MarshalByte.readFixedString(buffer, 1, Len.PM_BROKER_END);
	}

	public String getPmBrokerEndFormatted() {
		return this.pmBrokerEnd;
	}

	public void setPmBrokerElapsed(AfDecimal pmBrokerElapsed) {
		this.pmBrokerElapsed = Functions.replaceAll(PicFormatter.display("Z(2)9.9(2)").format(pmBrokerElapsed.copy()).toString(), ",", ".");
	}

	public AfDecimal getPmBrokerElapsed() {
		return PicParser.display("Z(2)9.9(2)").parseDecimal(Len.Int.PM_BROKER_ELAPSED + Len.Fract.PM_BROKER_ELAPSED, Len.Fract.PM_BROKER_ELAPSED,
				Functions.replaceAll(this.pmBrokerElapsed, ".", ","));
	}

	public void setPmReqhubElapsed(AfDecimal pmReqhubElapsed) {
		this.pmReqhubElapsed = Functions.replaceAll(PicFormatter.display("Z(2)9.9(2)").format(pmReqhubElapsed.copy()).toString(), ",", ".");
	}

	public AfDecimal getPmReqhubElapsed() {
		return PicParser.display("Z(2)9.9(2)").parseDecimal(Len.Int.PM_REQHUB_ELAPSED + Len.Fract.PM_REQHUB_ELAPSED, Len.Fract.PM_REQHUB_ELAPSED,
				Functions.replaceAll(this.pmReqhubElapsed, ".", ","));
	}

	public void setPmUowElapsed(AfDecimal pmUowElapsed) {
		this.pmUowElapsed = Functions.replaceAll(PicFormatter.display("Z(2)9.9(2)").format(pmUowElapsed.copy()).toString(), ",", ".");
	}

	public AfDecimal getPmUowElapsed() {
		return PicParser.display("Z(2)9.9(2)").parseDecimal(Len.Int.PM_UOW_ELAPSED + Len.Fract.PM_UOW_ELAPSED, Len.Fract.PM_UOW_ELAPSED,
				Functions.replaceAll(this.pmUowElapsed, ".", ","));
	}

	public void setPmReshubElapsed(AfDecimal pmReshubElapsed) {
		this.pmReshubElapsed = Functions.replaceAll(PicFormatter.display("Z(2)9.9(2)").format(pmReshubElapsed.copy()).toString(), ",", ".");
	}

	public AfDecimal getPmReshubElapsed() {
		return PicParser.display("Z(2)9.9(2)").parseDecimal(Len.Int.PM_RESHUB_ELAPSED + Len.Fract.PM_RESHUB_ELAPSED, Len.Fract.PM_RESHUB_ELAPSED,
				Functions.replaceAll(this.pmReshubElapsed, ".", ","));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PM_BROKER_BEGIN = 8;
		public static final int PM_BROKER_END = 8;
		public static final int PM_BROKER_ELAPSED = 6;
		public static final int PM_REQHUB_ELAPSED = 6;
		public static final int PM_UOW_ELAPSED = 6;
		public static final int PM_RESHUB_ELAPSED = 6;
		public static final int PERF_MONITORING_PARMS = DsdPerfMonitoringFlag.Len.PERF_MONITORING_FLAG + PM_BROKER_BEGIN + PM_BROKER_END
				+ PM_BROKER_ELAPSED + PM_REQHUB_ELAPSED + PM_UOW_ELAPSED + PM_RESHUB_ELAPSED;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int PM_BROKER_ELAPSED = 3;
			public static final int PM_REQHUB_ELAPSED = 3;
			public static final int PM_UOW_ELAPSED = 3;
			public static final int PM_RESHUB_ELAPSED = 3;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int PM_BROKER_ELAPSED = 2;
			public static final int PM_REQHUB_ELAPSED = 2;
			public static final int PM_UOW_ELAPSED = 2;
			public static final int PM_RESHUB_ELAPSED = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
