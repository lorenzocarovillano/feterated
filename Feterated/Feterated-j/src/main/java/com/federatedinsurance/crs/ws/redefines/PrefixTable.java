/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: PREFIX-TABLE<br>
 * Variable: PREFIX-TABLE from program CIWBNSRB<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PrefixTable extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int PREFIX_FLD_MAXOCCURS = 21;

	//==== CONSTRUCTORS ====
	public PrefixTable() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.PREFIX_TABLE;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "ADMI", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "BR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "COL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "DR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "JUDG", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "JUST", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "LIEU", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MISS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MRS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MAJ", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MYR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "PRES", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "PROF", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "REV", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "SGT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "SEN", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "SIS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "ATT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CPA", Len.FLR1);
	}

	/**Original name: PREFIX-STD<br>*/
	public String getPrefixStd(int prefixStdIdx) {
		int position = Pos.prefixStd(prefixStdIdx - 1);
		return readString(position, Len.PREFIX_STD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int PREFIX_TABLE = 1;
		public static final int FLR1 = PREFIX_TABLE;
		public static final int FLR2 = FLR1 + Len.FLR1;
		public static final int FLR3 = FLR2 + Len.FLR1;
		public static final int FLR4 = FLR3 + Len.FLR1;
		public static final int FLR5 = FLR4 + Len.FLR1;
		public static final int FLR6 = FLR5 + Len.FLR1;
		public static final int FLR7 = FLR6 + Len.FLR1;
		public static final int FLR8 = FLR7 + Len.FLR1;
		public static final int FLR9 = FLR8 + Len.FLR1;
		public static final int FLR10 = FLR9 + Len.FLR1;
		public static final int FLR11 = FLR10 + Len.FLR1;
		public static final int FLR12 = FLR11 + Len.FLR1;
		public static final int FLR13 = FLR12 + Len.FLR1;
		public static final int FLR14 = FLR13 + Len.FLR1;
		public static final int FLR15 = FLR14 + Len.FLR1;
		public static final int FLR16 = FLR15 + Len.FLR1;
		public static final int FLR17 = FLR16 + Len.FLR1;
		public static final int FLR18 = FLR17 + Len.FLR1;
		public static final int FLR19 = FLR18 + Len.FLR1;
		public static final int FLR20 = FLR19 + Len.FLR1;
		public static final int FLR21 = FLR20 + Len.FLR1;
		public static final int PREF_TABLE_REDEF = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int prefixFld(int idx) {
			return PREF_TABLE_REDEF + idx * Len.PREFIX_FLD;
		}

		public static int prefixStd(int idx) {
			return prefixFld(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 4;
		public static final int PREFIX_STD = 4;
		public static final int PREFIX_FLD = PREFIX_STD;
		public static final int PREFIX_TABLE = 21 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
