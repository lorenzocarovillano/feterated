/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X9080<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x9080 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x9080() {
	}

	public LFrameworkRequestAreaXz0x9080(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza980qMaxTtyRows(short xza980qMaxTtyRows) {
		writeBinaryShort(Pos.XZA980Q_MAX_TTY_ROWS, xza980qMaxTtyRows);
	}

	/**Original name: XZA980Q-MAX-TTY-ROWS<br>*/
	public short getXza980qMaxTtyRows() {
		return readBinaryShort(Pos.XZA980Q_MAX_TTY_ROWS);
	}

	public void setXza980qCsrActNbr(String xza980qCsrActNbr) {
		writeString(Pos.XZA980Q_CSR_ACT_NBR, xza980qCsrActNbr, Len.XZA980Q_CSR_ACT_NBR);
	}

	/**Original name: XZA980Q-CSR-ACT-NBR<br>*/
	public String getXza980qCsrActNbr() {
		return readString(Pos.XZA980Q_CSR_ACT_NBR, Len.XZA980Q_CSR_ACT_NBR);
	}

	public void setXza980qNotPrcTs(String xza980qNotPrcTs) {
		writeString(Pos.XZA980Q_NOT_PRC_TS, xza980qNotPrcTs, Len.XZA980Q_NOT_PRC_TS);
	}

	/**Original name: XZA980Q-NOT-PRC-TS<br>*/
	public String getXza980qNotPrcTs() {
		return readString(Pos.XZA980Q_NOT_PRC_TS, Len.XZA980Q_NOT_PRC_TS);
	}

	public void setXza980qUserid(String xza980qUserid) {
		writeString(Pos.XZA980Q_USERID, xza980qUserid, Len.XZA980Q_USERID);
	}

	/**Original name: XZA980Q-USERID<br>*/
	public String getXza980qUserid() {
		return readString(Pos.XZA980Q_USERID, Len.XZA980Q_USERID);
	}

	public void setXza980qClientId(String xza980qClientId) {
		writeString(Pos.XZA980Q_CLIENT_ID, xza980qClientId, Len.XZA980Q_CLIENT_ID);
	}

	/**Original name: XZA980Q-CLIENT-ID<br>*/
	public String getXza980qClientId() {
		return readString(Pos.XZA980Q_CLIENT_ID, Len.XZA980Q_CLIENT_ID);
	}

	public void setXza980qAdrId(String xza980qAdrId) {
		writeString(Pos.XZA980Q_ADR_ID, xza980qAdrId, Len.XZA980Q_ADR_ID);
	}

	/**Original name: XZA980Q-ADR-ID<br>*/
	public String getXza980qAdrId() {
		return readString(Pos.XZA980Q_ADR_ID, Len.XZA980Q_ADR_ID);
	}

	public void setXza980qRecTypCd(String xza980qRecTypCd) {
		writeString(Pos.XZA980Q_REC_TYP_CD, xza980qRecTypCd, Len.XZA980Q_REC_TYP_CD);
	}

	/**Original name: XZA980Q-REC-TYP-CD<br>*/
	public String getXza980qRecTypCd() {
		return readString(Pos.XZA980Q_REC_TYP_CD, Len.XZA980Q_REC_TYP_CD);
	}

	public void setXza980qRecTypDes(String xza980qRecTypDes) {
		writeString(Pos.XZA980Q_REC_TYP_DES, xza980qRecTypDes, Len.XZA980Q_REC_TYP_DES);
	}

	/**Original name: XZA980Q-REC-TYP-DES<br>*/
	public String getXza980qRecTypDes() {
		return readString(Pos.XZA980Q_REC_TYP_DES, Len.XZA980Q_REC_TYP_DES);
	}

	public void setXza980qName(String xza980qName) {
		writeString(Pos.XZA980Q_NAME, xza980qName, Len.XZA980Q_NAME);
	}

	/**Original name: XZA980Q-NAME<br>*/
	public String getXza980qName() {
		return readString(Pos.XZA980Q_NAME, Len.XZA980Q_NAME);
	}

	public void setXza980qAdrLin1(String xza980qAdrLin1) {
		writeString(Pos.XZA980Q_ADR_LIN1, xza980qAdrLin1, Len.XZA980Q_ADR_LIN1);
	}

	/**Original name: XZA980Q-ADR-LIN1<br>*/
	public String getXza980qAdrLin1() {
		return readString(Pos.XZA980Q_ADR_LIN1, Len.XZA980Q_ADR_LIN1);
	}

	public void setXza980qAdrLin2(String xza980qAdrLin2) {
		writeString(Pos.XZA980Q_ADR_LIN2, xza980qAdrLin2, Len.XZA980Q_ADR_LIN2);
	}

	/**Original name: XZA980Q-ADR-LIN2<br>*/
	public String getXza980qAdrLin2() {
		return readString(Pos.XZA980Q_ADR_LIN2, Len.XZA980Q_ADR_LIN2);
	}

	public void setXza980qCityNm(String xza980qCityNm) {
		writeString(Pos.XZA980Q_CITY_NM, xza980qCityNm, Len.XZA980Q_CITY_NM);
	}

	/**Original name: XZA980Q-CITY-NM<br>*/
	public String getXza980qCityNm() {
		return readString(Pos.XZA980Q_CITY_NM, Len.XZA980Q_CITY_NM);
	}

	public void setXza980qStateAbb(String xza980qStateAbb) {
		writeString(Pos.XZA980Q_STATE_ABB, xza980qStateAbb, Len.XZA980Q_STATE_ABB);
	}

	/**Original name: XZA980Q-STATE-ABB<br>*/
	public String getXza980qStateAbb() {
		return readString(Pos.XZA980Q_STATE_ABB, Len.XZA980Q_STATE_ABB);
	}

	public void setXza980qPstCd(String xza980qPstCd) {
		writeString(Pos.XZA980Q_PST_CD, xza980qPstCd, Len.XZA980Q_PST_CD);
	}

	/**Original name: XZA980Q-PST-CD<br>*/
	public String getXza980qPstCd() {
		return readString(Pos.XZA980Q_PST_CD, Len.XZA980Q_PST_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A9080 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA980Q_TTY_INFO_ROW = L_FW_REQ_XZ0A9080;
		public static final int XZA980Q_MAX_TTY_ROWS = XZA980Q_TTY_INFO_ROW;
		public static final int XZA980Q_CSR_ACT_NBR = XZA980Q_MAX_TTY_ROWS + Len.XZA980Q_MAX_TTY_ROWS;
		public static final int XZA980Q_NOT_PRC_TS = XZA980Q_CSR_ACT_NBR + Len.XZA980Q_CSR_ACT_NBR;
		public static final int XZA980Q_USERID = XZA980Q_NOT_PRC_TS + Len.XZA980Q_NOT_PRC_TS;
		public static final int XZA980Q_TTY_LIST = XZA980Q_USERID + Len.XZA980Q_USERID;
		public static final int XZA980Q_CLIENT_ID = XZA980Q_TTY_LIST;
		public static final int XZA980Q_ADR_ID = XZA980Q_CLIENT_ID + Len.XZA980Q_CLIENT_ID;
		public static final int XZA980Q_REC_TYP_CD = XZA980Q_ADR_ID + Len.XZA980Q_ADR_ID;
		public static final int XZA980Q_REC_TYP_DES = XZA980Q_REC_TYP_CD + Len.XZA980Q_REC_TYP_CD;
		public static final int XZA980Q_NAME = XZA980Q_REC_TYP_DES + Len.XZA980Q_REC_TYP_DES;
		public static final int XZA980Q_ADR_LIN1 = XZA980Q_NAME + Len.XZA980Q_NAME;
		public static final int XZA980Q_ADR_LIN2 = XZA980Q_ADR_LIN1 + Len.XZA980Q_ADR_LIN1;
		public static final int XZA980Q_CITY_NM = XZA980Q_ADR_LIN2 + Len.XZA980Q_ADR_LIN2;
		public static final int XZA980Q_STATE_ABB = XZA980Q_CITY_NM + Len.XZA980Q_CITY_NM;
		public static final int XZA980Q_PST_CD = XZA980Q_STATE_ABB + Len.XZA980Q_STATE_ABB;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA980Q_MAX_TTY_ROWS = 2;
		public static final int XZA980Q_CSR_ACT_NBR = 9;
		public static final int XZA980Q_NOT_PRC_TS = 26;
		public static final int XZA980Q_USERID = 8;
		public static final int XZA980Q_CLIENT_ID = 64;
		public static final int XZA980Q_ADR_ID = 64;
		public static final int XZA980Q_REC_TYP_CD = 5;
		public static final int XZA980Q_REC_TYP_DES = 13;
		public static final int XZA980Q_NAME = 120;
		public static final int XZA980Q_ADR_LIN1 = 45;
		public static final int XZA980Q_ADR_LIN2 = 45;
		public static final int XZA980Q_CITY_NM = 30;
		public static final int XZA980Q_STATE_ABB = 2;
		public static final int XZA980Q_PST_CD = 13;
		public static final int XZA980Q_TTY_LIST = XZA980Q_CLIENT_ID + XZA980Q_ADR_ID + XZA980Q_REC_TYP_CD + XZA980Q_REC_TYP_DES + XZA980Q_NAME
				+ XZA980Q_ADR_LIN1 + XZA980Q_ADR_LIN2 + XZA980Q_CITY_NM + XZA980Q_STATE_ABB + XZA980Q_PST_CD;
		public static final int XZA980Q_TTY_INFO_ROW = XZA980Q_MAX_TTY_ROWS + XZA980Q_CSR_ACT_NBR + XZA980Q_NOT_PRC_TS + XZA980Q_USERID
				+ XZA980Q_TTY_LIST;
		public static final int L_FW_REQ_XZ0A9080 = XZA980Q_TTY_INFO_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A9080;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
