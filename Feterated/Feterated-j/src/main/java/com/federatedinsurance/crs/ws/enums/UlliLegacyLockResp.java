/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ULLI-LEGACY-LOCK-RESP<br>
 * Variable: ULLI-LEGACY-LOCK-RESP from copybook HALLULLI<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UlliLegacyLockResp {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char UNKNOWN_OR_ERROR = 'X';
	public static final char USER_LOCK_FOUND = 'F';
	public static final char OTHER_LOCK_FOUND = 'O';
	public static final char LOCK_NOT_FOUND = 'N';
	public static final char LOCK_CREATED = 'C';
	public static final char LOCK_UPDATED = 'U';
	public static final char LOCK_DELETED = 'D';

	//==== METHODS ====
	public void setLegacyLockResp(char legacyLockResp) {
		this.value = legacyLockResp;
	}

	public char getLegacyLockResp() {
		return this.value;
	}

	public void setUnknownOrError() {
		value = UNKNOWN_OR_ERROR;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LEGACY_LOCK_RESP = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
