/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0R0005<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0r0005 {

	//==== PROPERTIES ====
	//Original name: CF-TF-ACT-NOT
	private String tfActNot = "XZ0F0001";
	//Original name: FILLER-CF-INVALID-OPERATION
	private String flr1 = "RESPONSE MODULE";
	//Original name: FILLER-CF-INVALID-OPERATION-1
	private String flr2 = " - INVALID";
	//Original name: FILLER-CF-INVALID-OPERATION-2
	private String flr3 = "OPERATION";
	//Original name: CF-UP-ATC-FRM-IND-PGM
	private String upAtcFrmIndPgm = "XZ0U8010";

	//==== METHODS ====
	public String getTfActNot() {
		return this.tfActNot;
	}

	public String getInvalidOperationFormatted() {
		return MarshalByteExt.bufferToStr(getInvalidOperationBytes());
	}

	/**Original name: CF-INVALID-OPERATION<br>*/
	public byte[] getInvalidOperationBytes() {
		byte[] buffer = new byte[Len.INVALID_OPERATION];
		return getInvalidOperationBytes(buffer, 1);
	}

	public byte[] getInvalidOperationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getUpAtcFrmIndPgm() {
		return this.upAtcFrmIndPgm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 15;
		public static final int FLR2 = 11;
		public static final int FLR3 = 9;
		public static final int INVALID_OPERATION = FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
