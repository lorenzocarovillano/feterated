/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.ActNotDao;
import com.federatedinsurance.crs.commons.data.dao.ActNotWrdDao;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.commons.data.dao.StCncWrdRqrDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.WorkingStorageAreaXz0p9000;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsProxyProgramArea;
import com.federatedinsurance.crs.ws.Xz0p9000Data;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0010;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x9050;
import com.federatedinsurance.crs.ws.ptr.WsFwbt0011Row;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0P9000<br>
 * <pre>AUTHOR.       FEDERATED INSURANCE.
 * DATE-WRITTEN. 05 JAN 2009.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - PREPARE CANCELLATION WORDINGS PROCESSING BPO**
 *                   XREF OBJ NM: XZ_PREPARE_CANCELLATION_WRD_BPO**
 *                   UOW        : XZ_PREPARE_CANCELLATION_WORDING**
 *                   OPERATIONS : PrepareCancellationWording     **
 * *                                                             **
 * * PURPOSE -  Add entries to the ACT_NOT_WRD table based on    **
 * *            policies from the ACT_NOT_POL table with the     **
 * *            correct account number and notification process  **
 * *            timestamp date.                                  **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS **
 * *                       LINKED TO BY THE FRAMEWORK DRIVER.    **
 * *                                                             **
 * *                                                             **
 * * DATA ACCESS METHODS - UMT STORAGE RECORDS                   **
 * *                       DB2 DATABASE                          **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * * NOTE: THIS LOG FOR INFRASTRUCTURE USE ONLY FOR TEMPLATE     **
 * *       VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     **
 * *       APPLICATION CODING.                                   **
 * *     T E M P L A T E   M A I N T E N A N C E   L O G         **
 * * CASE#     DATE       PROG       DESCRIPTION                 **
 * * --------  ---------  --------   ----------------------------**
 * * TS129     06/13/2006 E404LJL    TEMPLATE CREATED            **
 * * YJ249     04/27/2007 E404NEM    STDS CHGS                   **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * --------  ---------- --------   ----------------------------**
 * * TO07614   01/05/2009 E404GCL    NEW                         **
 * * TO07614   03/09/2009 E404GCL    CHANGE INDEX AND ADD MAX    **
 * *                                 CALC.                       **
 * * OOS       08/24/2009 E404GRK    CHANGE TO GET THE BEST      **
 * *                                 POSSIBLE LIST OF ACTIVE     **
 * *                                 INSURANCE LINES WHEN THE    **
 * **                                POLICY IS IN THE MIDDLE OF  **
 * **                                OF AN OOS.                  **
 * * PP03294   10/12/2012 E404DNF    RECOMPILE FOR FWBT0011 CHG  **
 * * PP03824   04/02/2013 E404DNF    RECOMPILE FOR XZ0T9050 CHGS **
 * * PP03933   11/18/2013 E404KXS    RECOMPILE                   **
 * * 25046     09/09/2019 E404JAL    RECOMPILE FOR FW0T0011 CHGS **
 * ****************************************************************</pre>*/
public class Xz0p9000 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS
	 * *****************************************************************
	 *  THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *
	 * *****************************************************************</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ActNotDao actNotDao = new ActNotDao(dbAccessStatus);
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	private StCncWrdRqrDao stCncWrdRqrDao = new StCncWrdRqrDao(dbAccessStatus);
	private ActNotWrdDao actNotWrdDao = new ActNotWrdDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0p9000Data ws = new Xz0p9000Data();
	private ExecContext execContext = null;
	/**Original name: WS-XZ0T9050-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE GET POL LST SERVICE</pre>*/
	private LServiceContractAreaXz0x9050 wsXz0t9050Row = new LServiceContractAreaXz0x9050(null);
	/**Original name: WS-XZ0T0010-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE ADD ACT NOT WRD SERVICE</pre>*/
	private LServiceContractAreaXz0x0010 wsXz0t0010Row = new LServiceContractAreaXz0x0010(null);
	/**Original name: WS-FWBT0011-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE GET POL DTL SERVICE</pre>*/
	private WsFwbt0011Row wsFwbt0011Row = new WsFwbt0011Row(null);
	//Original name: DFHCOMMAREA
	private Dfhcommarea dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, Dfhcommarea dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0p9000 getInstance() {
		return (Programs.getInstance(Xz0p9000.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>*****************************************************************
	 *   CONTROLS MAINLINE PROGRAM PROCESSING.                         *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-FILL-ACT-NOT-WRD.
		fillActNotWrd();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//                RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *  INITIALIZATION CONTROL                                        *
	 *                                                                *
	 * ****************************************************************
	 * * INITIALIZE ERROR/WARNING STORAGE</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//* VALIDATE UBOC IN COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//**  INITIALIZE WARNING SUBSCRIPT.
		// COB_CODE: MOVE +0                     TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(((short) 0));
		// COB_CODE: PERFORM 2100-READ-PRP-CNC-WRD-ROW.
		readPrpCncWrdRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-VALIDATE-ACT-NOT-TYP-CD.
		validateActNotTypCd();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-READ-PRP-CNC-WRD-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR PREPARE CANCELLATION WORDING BPO ROW. *
	 * *****************************************************************</pre>*/
	private void readPrpCncWrdRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: INITIALIZE WS-PRP-CNC-WRD-BPO-ROW.
		initWsPrpCncWrdBpoRow();
		// COB_CODE: SET HALRURQA-READ-FUNC      TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE WS-BO-PRP-CNC-WRD-BPO  TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWorkingStorageArea().getBoPrpCncWrdBpo());
		// COB_CODE: MOVE +1                     TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(1);
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-PRP-CNC-WRD-BPO-ROW.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsPrpCncWrdBpoRow());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: IF HALRURQA-REC-NOT-FOUND
		//               GO TO 2100-EXIT
		//           END-IF.
		if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUEST-MSG-MISSING
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequestMsgMissing();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '2100-READ-REQ-UMT-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-READ-REQ-UMT-ROW");
			// COB_CODE: MOVE 'NO RECORD FOUND ON REQ MSG STORE'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO RECORD FOUND ON REQ MSG STORE");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HALRURQA-BUS-OBJ-NM='
			//                  HALRURQA-BUS-OBJ-NM
			//                  '; HALRURQA-REC-SEQ='
			//                  HALRURQA-REC-SEQ
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HALRURQA-BUS-OBJ-NM=").append(ws.getWsHalrurqaLinkage().getBusObjNmFormatted())
							.append("; HALRURQA-REC-SEQ=").append(ws.getWsHalrurqaLinkage().getRecSeqFormatted()).toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
	}

	/**Original name: 2200-VALIDATE-ACT-NOT-TYP-CD_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  VALIDATE THE ACT-NOT-TYP-CD ENTERED AGAINST THE ACT_NOT TABLE. *
	 * *****************************************************************</pre>*/
	private void validateActNotTypCd() {
		// COB_CODE: MOVE XZY900-CSR-ACT-NBR     TO CSR-ACT-NBR
		//                                       OF DCLACT-NOT.
		ws.getDclactNot().setCsrActNbr(ws.getWsPrpCncWrdBpoRow().getCsrActNbr());
		// COB_CODE: MOVE XZY900-NOT-PRC-TS      TO NOT-PRC-TS
		//                                       OF DCLACT-NOT.
		ws.getDclactNot().setNotPrcTs(ws.getWsPrpCncWrdBpoRow().getNotPrcTs());
		// COB_CODE: EXEC SQL
		//              SELECT ACT_NOT_TYP_CD
		//                INTO :DCLACT-NOT.ACT-NOT-TYP-CD
		//                FROM ACT_NOT
		//               WHERE CSR_ACT_NBR = :DCLACT-NOT.CSR-ACT-NBR
		//                 AND NOT_PRC_TS  = :DCLACT-NOT.NOT-PRC-TS
		//           END-EXEC.
		ws.getDclactNot().setActNotTypCd(
				actNotDao.selectRec8(ws.getDclactNot().getCsrActNbr(), ws.getDclactNot().getNotPrcTs(), ws.getDclactNot().getActNotTypCd()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   END-IF
		//               WHEN ERD-SQL-NOT-FOUND
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: IF ACT-NOT-TYP-CD OF DCLACT-NOT
			//                             NOT = XZY900-ACT-NOT-TYP-CD
			//               PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			//           END-IF
			if (!Conditions.eq(ws.getDclactNot().getActNotTypCd(), ws.getWsPrpCncWrdBpoRow().getActNotTypCd())) {
				// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR
				//                           TO TRUE
				ws.getWsNonLoggableWarnOrErrSw().setBusErr();
				// COB_CODE: MOVE 'ACT_NOT'  TO NLBE-FAILED-TABLE-OR-FILE
				ws.getNlbeCommon().setFailedTableOrFile("ACT_NOT");
				// COB_CODE: MOVE 'ACT-NOT-TYP-CD'
				//                           TO NLBE-FAILED-COLUMN-OR-FIELD
				ws.getNlbeCommon().setFailedColumnOrField("ACT-NOT-TYP-CD");
				// COB_CODE: MOVE 'GEN_ALLTXT'
				//                           TO NLBE-ERROR-CODE
				ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
				// COB_CODE: MOVE SPACES     TO WS-NONLOG-PLACEHOLDER-VALUES
				ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
				// COB_CODE: MOVE XZY900-ACT-NOT-TYP-CD
				//                           TO EA-02-ACT-NOT-TYP-CD
				ws.getErrorAndAdviceMessages().getEa02InvActNotTypCd().setActNotTypCd(ws.getWsPrpCncWrdBpoRow().getActNotTypCd());
				// COB_CODE: MOVE ACT-NOT-TYP-CD OF DCLACT-NOT
				//                           TO EA-02-ACT-NOT-TYP-CD-FROM-TBL
				ws.getErrorAndAdviceMessages().getEa02InvActNotTypCd().setActNotTypCdFromTbl(ws.getDclactNot().getActNotTypCd());
				// COB_CODE: MOVE XZY900-CSR-ACT-NBR
				//                           TO EA-02-CSR-ACT-NBR
				ws.getErrorAndAdviceMessages().getEa02InvActNotTypCd().setCsrActNbr(ws.getWsPrpCncWrdBpoRow().getCsrActNbr());
				// COB_CODE: MOVE XZY900-NOT-PRC-TS
				//                           TO EA-02-NOT-PRC-TS
				ws.getErrorAndAdviceMessages().getEa02InvActNotTypCd().setNotPrcTs(ws.getWsPrpCncWrdBpoRow().getNotPrcTs());
				// COB_CODE: MOVE EA-02-INV-ACT-NOT-TYP-CD
				//                           TO WS-NONLOG-ERR-ALLTXT-TEXT
				ws.getWsNonlogPlaceholderValues()
						.setNonlogErrAlltxtText(ws.getErrorAndAdviceMessages().getEa02InvActNotTypCd().getEa01InvActNotTypCdFormatted());
				// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
				procNonLogWrnOrErr();
			}
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-PARENT-MISSING
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspParentMissing();
			// COB_CODE: MOVE '2200-VALIDATE-ACT-NOT-TYP-CD'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2200-VALIDATE-ACT-NOT-TYP-CD");
			// COB_CODE: MOVE 'ACT_NOT ENTRY NOT FOUND'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ACT_NOT ENTRY NOT FOUND");
			// COB_CODE: MOVE SPACES         TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'XZY900-CSR-ACT-NBR='
			//                   XZY900-CSR-ACT-NBR ';'
			//                  'XZY900-NOT-PRC-TS='
			//                   XZY900-NOT-PRC-TS ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(
					new StringBuffer(256).append("XZY900-CSR-ACT-NBR=").append(ws.getWsPrpCncWrdBpoRow().getCsrActNbrFormatted()).append(";")
							.append("XZY900-NOT-PRC-TS=").append(ws.getWsPrpCncWrdBpoRow().getNotPrcTsFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT'      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT");
			// COB_CODE: MOVE '2200-VALIDATE-ACT-NOT-TYP-CD'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2200-VALIDATE-ACT-NOT-TYP-CD");
			// COB_CODE: MOVE 'SELECT ACT_NOT PARENT FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT ACT_NOT PARENT FAILED");
			// COB_CODE: MOVE SPACES         TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'XZY900-CSR-ACT-NBR='
			//                   XZY900-CSR-ACT-NBR ';'
			//                  'XZY900-NOT-PRC-TS='
			//                   XZY900-NOT-PRC-TS ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(
					new StringBuffer(256).append("XZY900-CSR-ACT-NBR=").append(ws.getWsPrpCncWrdBpoRow().getCsrActNbrFormatted()).append(";")
							.append("XZY900-NOT-PRC-TS=").append(ws.getWsPrpCncWrdBpoRow().getNotPrcTsFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 3000-FILL-ACT-NOT-WRD_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  GET THE LIST OF POLICIES FOR AN ACCOUNT NUMBER AND NOTIFICATION*
	 *  PROCESS TIMESTAMP DATE AND ADD ENTRIES TO THE ACT_NOT_WRD TABLE*
	 * *****************************************************************
	 *  GET THE LIST OF POLICIES FROM GET-POL-LST</pre>*/
	private void fillActNotWrd() {
		// COB_CODE: PERFORM 3100-CALL-GET-POL-LST-SVC.
		callGetPolLstSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// PROCESS ALL POLICIES RETURNED AND ADD ENTRIES TO ACT_NOT_WORD
		// COB_CODE: PERFORM 3200-PROCESS-POLICY-ROWS.
		rng3200ProcessPolicyRows();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 3300-FREE-MEM-FOR-SERVICES
			freeMemForServices();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// FREE MEMORY FOR SERVICES
		// COB_CODE: PERFORM 3300-FREE-MEM-FOR-SERVICES.
		freeMemForServices();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3100-CALL-GET-POL-LST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PARAGRAPH TO CALL THE GET POLICY LIST SERVICE.                 *
	 * *****************************************************************</pre>*/
	private void callGetPolLstSvc() {
		// COB_CODE: PERFORM 3110-ALC-GET-POL-LST-SVC-MEM.
		alcGetPolLstSvcMem();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		//*  SET INPUT FOR GET-POL-LST SERVICE.
		//*  BE SURE TO SET OPERATION IN THIS PARAGRAPH AS WELL.
		// COB_CODE: PERFORM 3120-SET-GET-POL-LST-SVC-INPUT.
		setGetPolLstSvcInput();
		//*  CALL GET-POL-LST SERVICE.
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-GET-POL-LST-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P9000", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getSpGetPolLstSvc(), new Xz0x9050());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-POL-LST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetPolLstSvc());
			// COB_CODE: MOVE '3100-CALL-GET-POL-LST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3100-CALL-GET-POL-LST-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'CALL TO GET POLICY LIST SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CALL TO GET POLICY LIST SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3100-CALL-GET-POL-LST-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("3100-CALL-GET-POL-LST-SVC").append(".  FAILED MODULE EIBRESP CODE IS ")
							.append(ws.getWorkingStorageArea().getEibrespCdFormatted()).append(".  FAILED MODULE EIBRESP2 CODE IS ")
							.append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-GET-POL-LST-SVC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getSpGetPolLstSvc());
		// COB_CODE: MOVE '3100-CALL-GET-POL-LST-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("3100-CALL-GET-POL-LST-SVC");
		// COB_CODE: MOVE 'GET-POL-LST INFO'     TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("GET-POL-LST INFO");
		// COB_CODE: MOVE 'GET-POL-LST SVC'      TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("GET-POL-LST SVC");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
	}

	/**Original name: 3110-ALC-GET-POL-LST-SVC-MEM_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE GET-POL-LST SERVICE.               *
	 * *****************************************************************</pre>*/
	private void alcGetPolLstSvcMem() {
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T9050-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x9050.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-POL-LST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetPolLstSvc());
			// COB_CODE: MOVE '3110-ALC-GET-POL-LST-SVC-MEM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3110-ALC-GET-POL-LST-SVC-MEM");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'ALLOCATE GET POLICY LIST SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ALLOCATE GET POLICY LIST SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3110-ALC-GET-POL-LST-SVC-MEM'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("3110-ALC-GET-POL-LST-SVC-MEM").append(".  FAILED MODULE EIBRESP CODE IS ")
							.append(ws.getWorkingStorageArea().getEibrespCdFormatted()).append(".  FAILED MODULE EIBRESP2 CODE IS ")
							.append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3110-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T9050-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t9050Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x9050.class)));
		// COB_CODE: INITIALIZE WS-XZ0T9050-ROW.
		initWsXz0t9050Row();
	}

	/**Original name: 3120-SET-GET-POL-LST-SVC-INPUT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PARAGRAPH TO FILL IN GET-POL-LST SERVICE INPUT.                *
	 * *****************************************************************
	 * **  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * **  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setGetPolLstSvcInput() {
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-GET-POLICY-LIST     TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getGetPolicyList());
		// COB_CODE: MOVE XZY900-USERID          TO XZT95I-USERID.
		wsXz0t9050Row.setiUserid(ws.getWsPrpCncWrdBpoRow().getUserid());
		// COB_CODE: MOVE XZY900-CSR-ACT-NBR     TO XZT95I-CSR-ACT-NBR.
		wsXz0t9050Row.setiCsrActNbr(ws.getWsPrpCncWrdBpoRow().getCsrActNbr());
		// COB_CODE: MOVE XZY900-NOT-PRC-TS      TO XZT95I-TK-NOT-PRC-TS.
		wsXz0t9050Row.setiTkNotPrcTs(ws.getWsPrpCncWrdBpoRow().getNotPrcTs());
	}

	/**Original name: 3200-PROCESS-POLICY-ROWS_FIRST_SENTENCES<br>*/
	private void processPolicyRows() {
		// COB_CODE: MOVE +1                     TO SS-PL.
		ws.getSubscripts().setPl(((short) 1));
		// COB_CODE: COMPUTE WS-MAX-POLICY-ROWS = LENGTH OF XZT95O-POLICY-ROW-TBL
		//                                      / LENGTH OF XZT95O-POLICY-ROW.
		ws.getWorkingStorageArea()
				.setMaxPolicyRows((new AfDecimal(
						((((double) LServiceContractAreaXz0x9050.Len.O_POLICY_ROW_TBL)) / LServiceContractAreaXz0x9050.Len.O_POLICY_ROW), 9, 0))
								.toShort());
	}

	/**Original name: 3200-A<br>*/
	private String a() {
		// COB_CODE: IF SS-PL > WS-MAX-POLICY-ROWS
		//             OR
		//              XZT95O-POL-NBR (SS-PL) = SPACES
		//               GO TO 3200-EXIT
		//           END-IF.
		if (ws.getSubscripts().getPl() > ws.getWorkingStorageArea().getMaxPolicyRows()
				|| Characters.EQ_SPACE.test(wsXz0t9050Row.getoPolNbr(ws.getSubscripts().getPl()))) {
			// COB_CODE: GO TO 3200-EXIT
			return "";
		}
		// COB_CODE: IF XZT95O-POL-TYP-CD (SS-PL) = CF-COMMERCIAL-PACKAGE-POLICY
		//               END-IF
		//           END-IF.
		if (Conditions.eq(wsXz0t9050Row.getoPolTypCd(ws.getSubscripts().getPl()), ws.getConstantFields().getCommercialPackagePolicy())) {
			// COB_CODE: SET SW-NOT-OOS-AS-OF-DATE-RQR
			//                                   TO TRUE
			ws.getSwitches().setOosAsOfDateRqrFlag(false);
			// COB_CODE: PERFORM 3210-CALL-GET-POL-DTL-SVC
			callGetPolDtlSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3200-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3200-EXIT
				return "";
			}
			//**  IF THE POLICY IS IN THE MIDDLE OF AN OOS WE NEED TO CALL
			//**  GetPolDtlBasic AGAIN WITH THE 'AS-OF-DT' SET TO THE DATE OF
			//**  THE LAST ISSUED TRANSACTION IN ORDER TO GET THE BEST POSSIBLE
			//**  LIST OF ACTIVE INSURANCE LINES.
			// COB_CODE: IF FWBT11O-OOS-STR-IND = CF-YES
			//               END-IF
			//           END-IF
			if (wsFwbt0011Row.getoOosStrInd() == ws.getConstantFields().getYes()) {
				// COB_CODE: SET SW-OOS-AS-OF-DATE-RQR
				//                               TO TRUE
				ws.getSwitches().setOosAsOfDateRqrFlag(true);
				// COB_CODE: MOVE FWBT11O-EFF-DT TO SA-OOS-AS-OF-DATE
				ws.setSaOosAsOfDate(wsFwbt0011Row.getoEffDt());
				// COB_CODE: PERFORM 3210-CALL-GET-POL-DTL-SVC
				callGetPolDtlSvc();
				// COB_CODE: IF UBOC-HALT-AND-RETURN
				//               GO TO 3200-EXIT
				//           END-IF
				if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
					// COB_CODE: GO TO 3200-EXIT
					return "";
				}
			}
			// COB_CODE: PERFORM 3220-GET-CPP-ST-WRD-SEQ-CD
			getCppStWrdSeqCd();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3200-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3200-EXIT
				return "";
			}
		}
		// CALL 3230 REGARDLESS OF WHETHER IT IS A CPP OR NOT
		// COB_CODE: PERFORM 3230-GET-ST-WRD-SEQ-CD.
		getStWrdSeqCd();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3200-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3200-EXIT
			return "";
		}
		// COB_CODE: ADD +1                      TO SS-PL.
		ws.getSubscripts().setPl(Trunc.toShort(1 + ws.getSubscripts().getPl(), 4));
		// COB_CODE: GO TO 3200-A.
		return "3200-A";
	}

	/**Original name: 3210-CALL-GET-POL-DTL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PARAGRAPH TO CALL THE GET POLICY DETAIL SERVICE                *
	 * *****************************************************************</pre>*/
	private void callGetPolDtlSvc() {
		// COB_CODE: IF SW-NOT-GET-POL-DTL-SVC-CALLED
		//               END-IF
		//           END-IF.
		if (!ws.getSwitches().isGetPolDtlSvcCalledFlag()) {
			// COB_CODE: SET SW-GET-POL-DTL-SVC-CALLED
			//                                   TO TRUE
			ws.getSwitches().setGetPolDtlSvcCalledFlag(true);
			// COB_CODE: PERFORM 3211-ALC-GET-POL-DTL-SVC-MEM
			alcGetPolDtlSvcMem();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3210-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3210-EXIT
				return;
			}
		}
		//*  SET INPUT FOR GET-POL-DTL SERVICE.
		//*  BE SURE TO SET OPERATION IN THIS PARAGRAPH AS WELL.
		// COB_CODE: PERFORM 3212-SET-GET-POL-DTL-SVC-INPUT.
		setGetPolDtlSvcInput();
		//*  CALL GET-POL-DTL SERVICE.
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-GET-POL-DTL-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P9000", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getSpGetPolDtlSvc(), new Fwbx0011());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3210-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-POL-DTL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetPolDtlSvc());
			// COB_CODE: MOVE '3210-CALL-GET-POL-DTL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3210-CALL-GET-POL-DTL-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'CALL TO GET POLICY DETAIL SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CALL TO GET POLICY DETAIL SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3210-CALL-GET-POL-DTL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("3210-CALL-GET-POL-DTL-SVC").append(".  FAILED MODULE EIBRESP CODE IS ")
							.append(ws.getWorkingStorageArea().getEibrespCdFormatted()).append(".  FAILED MODULE EIBRESP2 CODE IS ")
							.append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3210-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-GET-POL-DTL-SVC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getSpGetPolDtlSvc());
		// COB_CODE: MOVE '3210-CALL-GET-POL-DTL-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("3210-CALL-GET-POL-DTL-SVC");
		// COB_CODE: MOVE 'GET-POL-DTL INFO'     TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("GET-POL-DTL INFO");
		// COB_CODE: MOVE 'GET-POL-DTL SVC'      TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("GET-POL-DTL SVC");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3210-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3210-EXIT
			return;
		}
		//*  MOVE INFO INTO BDO COPYBOOK FOR THIS BPO'S PROCESSING.
		// COB_CODE: PERFORM 3218-GET-POL-DTL-SVC-OUTPUT.
		getPolDtlSvcOutput();
	}

	/**Original name: 3211-ALC-GET-POL-DTL-SVC-MEM_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE GET-POL-DTL SERVICE.               *
	 * *****************************************************************</pre>*/
	private void alcGetPolDtlSvcMem() {
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-FWBT0011-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(WsFwbt0011Row.Len.WS_FWBT0011_ROW);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3211-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-POL-DTL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetPolDtlSvc());
			// COB_CODE: MOVE '3211-ALC-GET-POL-DTL-SVC-MEM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3211-ALC-GET-POL-DTL-SVC-MEM");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'ALLOCATE GET POLICY LIST SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ALLOCATE GET POLICY LIST SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3211-ALC-GET-POL-DTL-SVC-MEM'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("3211-ALC-GET-POL-DTL-SVC-MEM").append(".  FAILED MODULE EIBRESP CODE IS ")
							.append(ws.getWorkingStorageArea().getEibrespCdFormatted()).append(".  FAILED MODULE EIBRESP2 CODE IS ")
							.append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3211-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-FWBT0011-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsFwbt0011Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(), WsFwbt0011Row.class)));
		// COB_CODE: INITIALIZE WS-FWBT0011-ROW.
		initWsFwbt0011Row();
	}

	/**Original name: 3212-SET-GET-POL-DTL-SVC-INPUT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PARAGRAPH TO FILL IN GET-POL-DTL SERVICE INPUT.                *
	 * *****************************************************************
	 * **  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * **  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setGetPolDtlSvcInput() {
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE LENGTH OF WS-FWBT0011-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(WsFwbt0011Row.Len.WS_FWBT0011_ROW);
		// COB_CODE: SET PPC-SERVICE-DATA-POINTER
		//                                       TO ADDRESS OF WS-FWBT0011-ROW.
		ws.getWsProxyProgramArea().setPpcServiceDataPointer(pointerManager.addressOf(wsFwbt0011Row));
		// COB_CODE: INITIALIZE WS-FWBT0011-ROW.
		initWsFwbt0011Row();
		// COB_CODE: MOVE WS-GET-POLICY-DETAIL-BASIC
		//                                       TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getGetPolicyDetailBasic());
		// COB_CODE: MOVE XZY900-USERID          TO FWBT11I-USR-ID.
		wsFwbt0011Row.setiUsrId(ws.getWsPrpCncWrdBpoRow().getUserid());
		// COB_CODE: MOVE XZT95O-CSR-ACT-NBR     TO FWBT11I-TK-ACT-NBR.
		wsFwbt0011Row.setiTkActNbr(wsXz0t9050Row.getoCsrActNbr());
		// COB_CODE: MOVE XZT95O-POL-NBR (SS-PL) TO FWBT11I-TK-POL-NBR
		//                                          FWBT11I-POL-NBR.
		wsFwbt0011Row.setiTkPolNbr(wsXz0t9050Row.getoPolNbr(ws.getSubscripts().getPl()));
		wsFwbt0011Row.setiPolNbr(wsXz0t9050Row.getoPolNbr(ws.getSubscripts().getPl()));
		//**  IF THE POLICY IS IN THE MIDDLE OF AN OOS CALL GetPolDtlBasic
		//**  WITH THE 'AS OF DATE' SET TO THE EFFECTIVE DATE OF THE LAST
		//**  ISSUED TRANSACTION.
		// COB_CODE: IF SW-OOS-AS-OF-DATE-RQR
		//               MOVE SA-OOS-AS-OF-DATE  TO FWBT11I-AS-OF-DT
		//           END-IF.
		if (ws.getSwitches().isOosAsOfDateRqrFlag()) {
			// COB_CODE: MOVE SA-OOS-AS-OF-DATE  TO FWBT11I-AS-OF-DT
			wsFwbt0011Row.setiAsOfDt(ws.getSaOosAsOfDate());
		}
		// COB_CODE: MOVE XZT95O-POL-EFF-DT (SS-PL)
		//                                       TO FWBT11I-TK-POL-EFF-DT
		//                                          FWBT11I-POL-EFF-DT.
		wsFwbt0011Row.setiTkPolEffDt(wsXz0t9050Row.getoPolEffDt(ws.getSubscripts().getPl()));
		wsFwbt0011Row.setiPolEffDt(wsXz0t9050Row.getoPolEffDt(ws.getSubscripts().getPl()));
		// COB_CODE: MOVE XZT95O-POL-EXP-DT (SS-PL)
		//                                       TO FWBT11I-TK-POL-EXP-DT
		//                                          FWBT11I-POL-EXP-DT.
		wsFwbt0011Row.setiTkPolExpDt(wsXz0t9050Row.getoPolExpDt(ws.getSubscripts().getPl()));
		wsFwbt0011Row.setiPolExpDt(wsXz0t9050Row.getoPolExpDt(ws.getSubscripts().getPl()));
	}

	/**Original name: 3220-GET-CPP-ST-WRD-SEQ-CD_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PARAGRAPH THAT GOES THROUGH AUTOMATED AND MANUAL INSURANCE
	 *  LINE CODE TABLES TO SEE IF THEY SHOULD BE ADDED TO ACT_NOT_WRD
	 * ***************************************************************</pre>*/
	private void getCppStWrdSeqCd() {
		// COB_CODE: PERFORM 3221-GET-ATM-CPP-ST-WRD-SEQ-CD.
		rng3221GetAtmCppStWrdSeqCd();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3220-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3220-EXIT
			return;
		}
		// COB_CODE: PERFORM 3222-GET-MNL-CPP-ST-WRD-SEQ-CD.
		rng3222GetMnlCppStWrdSeqCd();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3220-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3220-EXIT
			return;
		}
	}

	/**Original name: 3221-GET-ATM-CPP-ST-WRD-SEQ-CD_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  GET ST-WRD-SEQ-CD USING POL-TYP-CD FROM AUTOMATED INSURANCE
	 *  LINE CODES.
	 * ***************************************************************</pre>*/
	private void getAtmCppStWrdSeqCd() {
		// COB_CODE: INITIALIZE DCLST-CNC-WRD-RQR.
		initDclstCncWrdRqr();
		// COB_CODE: MOVE XZT95O-POL-PRI-RSK-ST-ABB (SS-PL)
		//                                       TO ST-ABB
		//                                       OF DCLST-CNC-WRD-RQR.
		ws.getDclstCncWrdRqr().setStAbb(wsXz0t9050Row.getoPolPriRskStAbb(ws.getSubscripts().getPl()));
		// COB_CODE: MOVE XZY900-ACT-NOT-TYP-CD  TO ACT-NOT-TYP-CD
		//                                       OF DCLST-CNC-WRD-RQR.
		ws.getDclstCncWrdRqr().setActNotTypCd(ws.getWsPrpCncWrdBpoRow().getActNotTypCd());
		// COB_CODE: SET IX-AL                   TO +1.
		ws.setIxAl(1);
	}

	/**Original name: 3221-A<br>*/
	private String a1() {
		// COB_CODE: IF IX-AL > CF-MAX-INS-LIN-CD
		//             OR
		//              WS-IL-AL-INS-LIN-CD (IX-AL) = SPACES
		//               GO TO 3221-EXIT
		//           ELSE
		//                                       TO SA-PL-POL-TYP-CD
		//           END-IF.
		if (ws.getIxAl() > ws.getConstantFields().getMaxInsLinCd()
				|| Characters.EQ_SPACE.test(ws.getWorkingStorageArea().getIlAutomatedLines(ws.getIxAl()).getWsIlAlInsLinCd())) {
			// COB_CODE: GO TO 3221-EXIT
			return "";
		} else {
			// COB_CODE: MOVE WS-IL-AL-INS-LIN-CD (IX-AL)
			//                                   TO SA-PL-POL-TYP-CD
			ws.setSaPlPolTypCd(ws.getWorkingStorageArea().getIlAutomatedLines(ws.getIxAl()).getWsIlAlInsLinCd());
		}
		// CHECK TO SEE IF AN ENTRY EXISTS IN ACT_NOT_WRD, IF NOT THEN CALL
		// THE ADDACCOUNTNOTIFICATIONWORDING SERVICE TO ADD IT.
		// COB_CODE: PERFORM 9800-PRC-ST-CNC-WRD-RQR-CSR.
		prcStCncWrdRqrCsr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3221-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3221-EXIT
			return "";
		}
		// COB_CODE: SET IX-AL                   UP BY +1.
		ws.setIxAl(Trunc.toInt(ws.getIxAl() + 1, 9));
		// COB_CODE: GO TO 3221-A.
		return "3221-A";
	}

	/**Original name: 3222-GET-MNL-CPP-ST-WRD-SEQ-CD_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  GET ST-WRD-SEQ-CD USING POL-TYP-CD FROM MANUAL INSURANCE LINE
	 *  CODES.
	 * ***************************************************************</pre>*/
	private void getMnlCppStWrdSeqCd() {
		// COB_CODE: INITIALIZE DCLST-CNC-WRD-RQR.
		initDclstCncWrdRqr();
		// COB_CODE: MOVE XZT95O-POL-PRI-RSK-ST-ABB (SS-PL)
		//                                       TO ST-ABB
		//                                       OF DCLST-CNC-WRD-RQR.
		ws.getDclstCncWrdRqr().setStAbb(wsXz0t9050Row.getoPolPriRskStAbb(ws.getSubscripts().getPl()));
		// COB_CODE: MOVE XZY900-ACT-NOT-TYP-CD  TO ACT-NOT-TYP-CD
		//                                       OF DCLST-CNC-WRD-RQR.
		ws.getDclstCncWrdRqr().setActNotTypCd(ws.getWsPrpCncWrdBpoRow().getActNotTypCd());
		// COB_CODE: SET IX-ML                   TO +1.
		ws.setIxMl(1);
	}

	/**Original name: 3222-A<br>*/
	private String a2() {
		// COB_CODE: IF IX-ML > CF-MAX-INS-LIN-CD
		//             OR
		//              WS-IL-ML-INS-LIN-CD (IX-ML) = SPACES
		//               GO TO 3222-EXIT
		//           ELSE
		//                                       TO SA-PL-POL-TYP-CD
		//           END-IF.
		if (ws.getIxMl() > ws.getConstantFields().getMaxInsLinCd()
				|| Characters.EQ_SPACE.test(ws.getWorkingStorageArea().getIlManualLines(ws.getIxMl()).getWsIlMlInsLinCd())) {
			// COB_CODE: GO TO 3222-EXIT
			return "";
		} else {
			// COB_CODE: MOVE WS-IL-ML-INS-LIN-CD (IX-ML)
			//                                   TO SA-PL-POL-TYP-CD
			ws.setSaPlPolTypCd(ws.getWorkingStorageArea().getIlManualLines(ws.getIxMl()).getWsIlMlInsLinCd());
		}
		// CHECK TO SEE IF AN ENTRY EXISTS IN ACT_NOT_WRD, IF NOT THEN CALL
		// THE ADDACCOUNTNOTIFICATIONWORDING SERVICE TO ADD IT.
		// COB_CODE: PERFORM 9800-PRC-ST-CNC-WRD-RQR-CSR.
		prcStCncWrdRqrCsr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3222-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3222-EXIT
			return "";
		}
		// COB_CODE: SET IX-ML                   UP BY +1.
		ws.setIxMl(Trunc.toInt(ws.getIxMl() + 1, 9));
		// COB_CODE: GO TO 3222-A.
		return "3222-A";
	}

	/**Original name: 3230-GET-ST-WRD-SEQ-CD_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  GET ST-WRD-SEQ-CD USING POL-TYP-CD FROM ACT_NOT_POL
	 * ***************************************************************</pre>*/
	private void getStWrdSeqCd() {
		// COB_CODE: INITIALIZE DCLST-CNC-WRD-RQR.
		initDclstCncWrdRqr();
		// COB_CODE: MOVE XZT95O-POL-PRI-RSK-ST-ABB (SS-PL)
		//                                       TO ST-ABB
		//                                       OF DCLST-CNC-WRD-RQR.
		ws.getDclstCncWrdRqr().setStAbb(wsXz0t9050Row.getoPolPriRskStAbb(ws.getSubscripts().getPl()));
		// COB_CODE: MOVE XZY900-ACT-NOT-TYP-CD  TO ACT-NOT-TYP-CD
		//                                       OF DCLST-CNC-WRD-RQR.
		ws.getDclstCncWrdRqr().setActNotTypCd(ws.getWsPrpCncWrdBpoRow().getActNotTypCd());
		// COB_CODE: MOVE XZT95O-POL-TYP-CD (SS-PL)
		//                                       TO SA-PL-POL-TYP-CD.
		ws.setSaPlPolTypCd(wsXz0t9050Row.getoPolTypCd(ws.getSubscripts().getPl()));
		// CHECK TO SEE IF AN ENTRY EXISTS IN ACT_NOT_WRD, IF NOT THEN CALL
		// THE ADDACCOUNTNOTIFICATIONWORDING SERVICE TO ADD IT.
		// COB_CODE: PERFORM 9800-PRC-ST-CNC-WRD-RQR-CSR.
		prcStCncWrdRqrCsr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3230-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3230-EXIT
			return;
		}
	}

	/**Original name: 3218-GET-POL-DTL-SVC-OUTPUT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PARAGRAPH TO FILL IN GET-POL-DTL SERVICE OUTPUT.               *
	 * *****************************************************************</pre>*/
	private void getPolDtlSvcOutput() {
		// COB_CODE: INITIALIZE WS-INSURANCE-LINES.
		initInsuranceLines();
		// PUT AUTOMATED INSURANCE LINE CODES INTO A WORKING STORAGE TABLE
		// IN ORDER TO PROCESS THEM LATER.
		// COB_CODE: PERFORM 3218-1-FILL-IN-ATM-INS-LIN-CD.
		rng32181FillInAtmInsLinCd();
		// PUT MANUAL INSURANCE LINE CODES INTO A WORKING STORAGE TABLE
		// IN ORDER TO PROCESS THEM LATER.
		// COB_CODE: PERFORM 3218-2-FILL-IN-MNL-INS-LIN-CD.
		rng32182FillInMnlInsLinCd();
	}

	/**Original name: 3218-1-FILL-IN-ATM-INS-LIN-CD_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FILL IN A TABLE WITH AUTOMATED INSURANCE LINE CODES BROUGHT    *
	 *  BACK FROM THE GET POLICY DETAIL SERVICE.                       *
	 * *****************************************************************</pre>*/
	private void fillInAtmInsLinCd() {
		// COB_CODE: MOVE +0                     TO SS-AL.
		ws.getSubscripts().setAl(((short) 0));
	}

	/**Original name: 3218-1-A<br>*/
	private String a3() {
		// COB_CODE: ADD +1                      TO SS-AL.
		ws.getSubscripts().setAl(Trunc.toShort(1 + ws.getSubscripts().getAl(), 4));
		// COB_CODE: IF FWBT11O-INS-LIN-CD (SS-AL) NOT = SPACES
		//                                       TO WS-IL-AL-INS-LIN-CD (SS-AL)
		//           ELSE
		//               GO TO 3218-1-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(wsFwbt0011Row.getoInsLinCd(ws.getSubscripts().getAl()))) {
			// COB_CODE: MOVE FWBT11O-INS-LIN-CD (SS-AL)
			//                                   TO WS-IL-AL-INS-LIN-CD (SS-AL)
			ws.getWorkingStorageArea().getIlAutomatedLines(ws.getSubscripts().getAl())
					.setWsIlAlInsLinCd(wsFwbt0011Row.getoInsLinCd(ws.getSubscripts().getAl()));
		} else {
			// COB_CODE: GO TO 3218-1-EXIT
			return "";
		}
		// COB_CODE: IF SS-AL < CF-MAX-INS-LIN-CD
		//               GO TO 3218-1-A
		//           END-IF.
		if (ws.getSubscripts().getAl() < ws.getConstantFields().getMaxInsLinCd()) {
			// COB_CODE: GO TO 3218-1-A
			return "3218-1-A";
		}
		return "";
	}

	/**Original name: 3218-2-FILL-IN-MNL-INS-LIN-CD_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FILL IN A TABLE WITH MANUAL INSURANCE LINE CODES BROUGHT BACK  *
	 *  FROM THE GET POLICY DETAIL SERVICE.                            *
	 * *****************************************************************</pre>*/
	private void fillInMnlInsLinCd() {
		// COB_CODE: MOVE +0                     TO SS-ML.
		ws.getSubscripts().setMl(((short) 0));
	}

	/**Original name: 3218-2-A<br>*/
	private String a4() {
		// COB_CODE: ADD +1                      TO SS-ML.
		ws.getSubscripts().setMl(Trunc.toShort(1 + ws.getSubscripts().getMl(), 4));
		// COB_CODE: IF FWBT11O-MNL-INS-LIN-CD (SS-ML) NOT = SPACES
		//                                       TO WS-IL-ML-INS-LIN-CD (SS-ML)
		//           ELSE
		//               GO TO 3218-2-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(wsFwbt0011Row.getoMnlInsLinCd(ws.getSubscripts().getMl()))) {
			// COB_CODE: MOVE FWBT11O-MNL-INS-LIN-CD (SS-ML)
			//                                   TO WS-IL-ML-INS-LIN-CD (SS-ML)
			ws.getWorkingStorageArea().getIlManualLines(ws.getSubscripts().getMl())
					.setWsIlMlInsLinCd(wsFwbt0011Row.getoMnlInsLinCd(ws.getSubscripts().getMl()));
		} else {
			// COB_CODE: GO TO 3218-2-EXIT
			return "";
		}
		// COB_CODE: IF SS-ML < CF-MAX-INS-LIN-CD
		//               GO TO 3218-2-A
		//           END-IF.
		if (ws.getSubscripts().getMl() < ws.getConstantFields().getMaxInsLinCd()) {
			// COB_CODE: GO TO 3218-2-A
			return "3218-2-A";
		}
		return "";
	}

	/**Original name: 3300-FREE-MEM-FOR-SERVICES_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  FREE THE MEMORY FOR THE SERVICES
	 * ***************************************************************</pre>*/
	private void freeMemForServices() {
		// COB_CODE: PERFORM 3310-FREE-GET-POL-LST-SVC-MEM.
		freeGetPolLstSvcMem();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3300-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3300-EXIT
			return;
		}
		//***************************************************************
		// ONLY FREE IF THE MEMORY HAS BEEN ALLOCATED FOR THIS SERVICE.
		//***************************************************************
		// COB_CODE: IF SW-GET-POL-DTL-SVC-CALLED
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isGetPolDtlSvcCalledFlag()) {
			// COB_CODE: PERFORM 3320-FREE-GET-POL-DTL-SVC-MEM
			freeGetPolDtlSvcMem();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3300-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3300-EXIT
				return;
			}
		}
		//***************************************************************
		// ONLY FREE IF THE MEMORY HAS BEEN ALLOCATED FOR THIS SERVICE.
		//***************************************************************
		// COB_CODE: IF SW-ACT-NOT-WRD-SVC-CALLED
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isActNotWrdSvcCalledFlag()) {
			// COB_CODE: PERFORM 3330-FREE-ADD-ACT-NOT-WRD-MEM
			freeAddActNotWrdMem();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3300-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3300-EXIT
				return;
			}
		}
	}

	/**Original name: 3310-FREE-GET-POL-LST-SVC-MEM_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY OF THE GET-POL-LST SERVICE.                    *
	 * *****************************************************************</pre>*/
	private void freeGetPolLstSvcMem() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T9050-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t9050Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3310-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-POL-LST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetPolLstSvc());
			// COB_CODE: MOVE '3310-FREE-GET-POL-LST-SVC-MEM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3310-FREE-GET-POL-LST-SVC-MEM");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FREE GET POLICY LIST SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FREE GET POLICY LIST SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3310-FREE-GET-POL-LST-SVC-MEM'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("3310-FREE-GET-POL-LST-SVC-MEM").append(".  FAILED MODULE EIBRESP CODE IS ")
							.append(ws.getWorkingStorageArea().getEibrespCdFormatted()).append(".  FAILED MODULE EIBRESP2 CODE IS ")
							.append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3310-EXIT
			return;
		}
	}

	/**Original name: 3320-FREE-GET-POL-DTL-SVC-MEM_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY OF THE GET-POL-DTL SERVICE.                    *
	 * *****************************************************************</pre>*/
	private void freeGetPolDtlSvcMem() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-FWBT0011-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsFwbt0011Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3320-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-POL-DTL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetPolDtlSvc());
			// COB_CODE: MOVE '3320-FREE-GET-POL-DTL-SVC-MEM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3320-FREE-GET-POL-DTL-SVC-MEM");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FREE GET POLICY DETAIL SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FREE GET POLICY DETAIL SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3320-FREE-GET-POL-DTL-SVC-MEM'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("3320-FREE-GET-POL-DTL-SVC-MEM").append(".  FAILED MODULE EIBRESP CODE IS ")
							.append(ws.getWorkingStorageArea().getEibrespCdFormatted()).append(".  FAILED MODULE EIBRESP2 CODE IS ")
							.append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3320-EXIT
			return;
		}
	}

	/**Original name: 3330-FREE-ADD-ACT-NOT-WRD-MEM_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY OF THE ADD-ACT-NOT-WRD SERVICE.                *
	 * *****************************************************************</pre>*/
	private void freeAddActNotWrdMem() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T0010-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t0010Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-WRD-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpAddActNotWrdSvc());
			// COB_CODE: MOVE '3330-FREE-ADD-ACT-NOT-WRD-MEM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3330-FREE-ADD-ACT-NOT-WRD-MEM");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FREE ADD ACT NOT WORD SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FREE ADD ACT NOT WORD SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3330-FREE-ADD-ACT-NOT-WRD-MEM'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("3330-FREE-ADD-ACT-NOT-WRD-MEM").append(".  FAILED MODULE EIBRESP CODE IS ")
							.append(ws.getWorkingStorageArea().getEibrespCdFormatted()).append(".  FAILED MODULE EIBRESP2 CODE IS ")
							.append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWorkingStorageArea().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P9000", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getCommInfo().setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getCommInfo().setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWorkingStorageArea().getApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: 9800-PRC-ST-CNC-WRD-RQR-CSR_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  CURSOR OPERATIONS FOR ST_CNC_WRD_RQR TABLE
	 * ***************************************************************
	 * * OPEN REQUIRED CURSOR</pre>*/
	private void prcStCncWrdRqrCsr() {
		// COB_CODE: PERFORM 9810-OPEN-ST-CNC-WRD-RQR-CSR.
		openStCncWrdRqrCsr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9800-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9800-EXIT
			return;
		}
		//* FETCH ALL DATA FROM REQUIRED CURSOR
		// COB_CODE: PERFORM 9820-PROCESS-ST-WRD-SEQ-CD.
		rng9820ProcessStWrdSeqCd();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9800-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9800-EXIT
			return;
		}
		//* CLOSE CURSOR
		// COB_CODE: PERFORM 9830-CLOSE-ST-CNC-WRD-RQR-CSR.
		closeStCncWrdRqrCsr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9800-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9800-EXIT
			return;
		}
	}

	/**Original name: 9810-OPEN-ST-CNC-WRD-RQR-CSR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET UP AND EXECUTE CURSOR OPEN.                                *
	 * *****************************************************************</pre>*/
	private void openStCncWrdRqrCsr() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE DCLST-CNC-WRD-RQR.
		initDclstCncWrdRqr();
		// COB_CODE: MOVE XZT95O-POL-PRI-RSK-ST-ABB (SS-PL)
		//                                       TO ST-ABB
		//                                       OF DCLST-CNC-WRD-RQR.
		ws.getDclstCncWrdRqr().setStAbb(wsXz0t9050Row.getoPolPriRskStAbb(ws.getSubscripts().getPl()));
		// COB_CODE: MOVE XZY900-ACT-NOT-TYP-CD  TO ACT-NOT-TYP-CD
		//                                       OF DCLST-CNC-WRD-RQR.
		ws.getDclstCncWrdRqr().setActNotTypCd(ws.getWsPrpCncWrdBpoRow().getActNotTypCd());
		// COB_CODE: MOVE SA-PL-POL-TYP-CD       TO POL-COV-CD
		//                                       OF DCLST-CNC-WRD-RQR.
		ws.getDclstCncWrdRqr().setPolCovCd(ws.getSaPlPolTypCd());
		// COB_CODE: EXEC SQL
		//               OPEN ST_CNC_WRD_RQR_CSR
		//           END-EXEC.
		stCncWrdRqrDao.openStCncWrdRqrCsr(ws.getDclstCncWrdRqr().getStAbb(), ws.getDclstCncWrdRqr().getActNotTypCd(),
				ws.getDclstCncWrdRqr().getPolCovCd());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'ST_CNC_WRD_RQR'   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ST_CNC_WRD_RQR");
			// COB_CODE: MOVE '9810-OPEN-ST-CNC-WRD-RQR-CSR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9810-OPEN-ST-CNC-WRD-RQR-CSR");
			// COB_CODE: MOVE 'OPEN CF-ST-CNC-WRD-RQR-CSR FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CF-ST-CNC-WRD-RQR-CSR FAILED");
			// COB_CODE: STRING 'XZT95O-POL-PRI-RSK-ST-ABB='
			//                  XZT95O-POL-PRI-RSK-ST-ABB (SS-PL)
			//                  'XZY900-ACT-NOT-TYP-CD='
			//                  XZY900-ACT-NOT-TYP-CD
			//                  'XZT95O-POL-EFF-DT='
			//                  XZT95O-POL-EFF-DT (SS-PL)
			//                  'SA-PL-POL-TYP-CD='
			//                  SA-PL-POL-TYP-CD
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZT95O-POL-PRI-RSK-ST-ABB=", wsXz0t9050Row.getoPolPriRskStAbbFormatted(ws.getSubscripts().getPl()),
							"XZY900-ACT-NOT-TYP-CD=", ws.getWsPrpCncWrdBpoRow().getActNotTypCdFormatted(), "XZT95O-POL-EFF-DT=",
							wsXz0t9050Row.getoPolEffDtFormatted(ws.getSubscripts().getPl()), "SA-PL-POL-TYP-CD=", ws.getSaPlPolTypCdFormatted(),
							";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9820-PROCESS-ST-WRD-SEQ-CD_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PROCESS ST-WRD-SEQ-CD'S TO BE ADDED TO ACT_NOT_WRD             *
	 * *****************************************************************</pre>*/
	private void processStWrdSeqCd() {
		// COB_CODE: SET SW-NOT-END-ST-CNC-WRD-RQR-CSR
		//                                       TO TRUE.
		ws.getSwitches().getEndStCncWrdRqrCsrFlag().setNotEndStCncWrdRqrCsr();
	}

	/**Original name: 9820-A<br>*/
	private String a5() {
		// COB_CODE: SET SW-NO-CALL-ADD-ACT-NOT-WRD-SVC
		//                                       TO TRUE.
		ws.getSwitches().getCallAddActNotWrdSvcFg().setNoCallAddActNotWrdSvc();
		// COB_CODE: PERFORM 9821-FETCH-ST-CNC-WRD-RQR-CSR.
		fetchStCncWrdRqrCsr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//             OR
		//              SW-END-ST-CNC-WRD-RQR-CSR
		//               GO TO 9820-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getSwitches().getEndStCncWrdRqrCsrFlag().isEndStCncWrdRqrCsr()) {
			// COB_CODE: GO TO 9820-EXIT
			return "";
		}
		// COB_CODE: PERFORM 9822-CHECK-FOR-DUP-ACT-NOT-WRD.
		checkForDupActNotWrd();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9820-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9820-EXIT
			return "";
		}
		// COB_CODE: IF SW-CALL-ADD-ACT-NOT-WRD-SVC
		//               END-IF
		//           END-IF
		if (ws.getSwitches().getCallAddActNotWrdSvcFg().isCallAddActNotWrdSvc()) {
			// COB_CODE: PERFORM 9823-CALL-ADD-ACT-NOT-WRD-SVC
			callAddActNotWrdSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9820-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9820-EXIT
				return "";
			}
		}
		// COB_CODE: GO TO 9820-A.
		return "9820-A";
	}

	/**Original name: 9821-FETCH-ST-CNC-WRD-RQR-CSR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FETCH CURSOR.                                                  *
	 * *****************************************************************</pre>*/
	private void fetchStCncWrdRqrCsr() {
		// COB_CODE: EXEC SQL
		//               FETCH ST_CNC_WRD_RQR_CSR
		//                 INTO :DCLST-CNC-WRD-RQR.ST-WRD-SEQ-CD
		//           END-EXEC.
		stCncWrdRqrDao.fetchStCncWrdRqrCsr(ws.getDclstCncWrdRqr());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                                       TO TRUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET SW-END-ST-CNC-WRD-RQR-CSR
			//                               TO TRUE
			ws.getSwitches().getEndStCncWrdRqrCsrFlag().setEndStCncWrdRqrCsr();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED    OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'ST_CNC_WRD_RQR'
			//                               TO EFAL-ERR-OBJECT-NAME
			//                               OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ST_CNC_WRD_RQR");
			// COB_CODE: MOVE '9821-FETCH-ST-CNC-WRD-RQR-CSR'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9821-FETCH-ST-CNC-WRD-RQR-CSR");
			// COB_CODE: MOVE 'FETCH CF-ST-CNC-WRD-RQR-CSR FAILED'
			//                               TO EFAL-ERR-COMMENT
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH CF-ST-CNC-WRD-RQR-CSR FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 9822-CHECK-FOR-DUP-ACT-NOT-WRD_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  BEFORE ADDING A ROW TO ACT_NOT_WRD, MAKE SURE IT DOESN'T EXIST *
	 * *****************************************************************</pre>*/
	private void checkForDupActNotWrd() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE XZT95O-CSR-ACT-NBR     TO CSR-ACT-NBR
		//                                       OF DCLACT-NOT-WRD.
		ws.getDclactNotWrd().setCsrActNbr(wsXz0t9050Row.getoCsrActNbr());
		// COB_CODE: MOVE XZT95O-TK-NOT-PRC-TS   TO NOT-PRC-TS
		//                                       OF DCLACT-NOT-WRD.
		ws.getDclactNotWrd().setNotPrcTs(wsXz0t9050Row.getoTkNotPrcTs());
		// COB_CODE: MOVE XZT95O-POL-NBR (SS-PL) TO POL-NBR
		//                                       OF DCLACT-NOT-WRD.
		ws.getDclactNotWrd().setPolNbr(wsXz0t9050Row.getoPolNbr(ws.getSubscripts().getPl()));
		// COB_CODE: MOVE ST-WRD-SEQ-CD OF DCLST-CNC-WRD-RQR
		//                                       TO ST-WRD-SEQ-CD
		//                                       OF DCLACT-NOT-WRD.
		ws.getDclactNotWrd().setStWrdSeqCd(ws.getDclstCncWrdRqr().getStWrdSeqCd());
		// COB_CODE: EXEC SQL
		//              SELECT ST_WRD_SEQ_CD
		//                INTO :DCLACT-NOT-WRD.ST-WRD-SEQ-CD
		//                FROM ACT_NOT_WRD
		//               WHERE CSR_ACT_NBR   = :DCLACT-NOT-WRD.CSR-ACT-NBR
		//                 AND NOT_PRC_TS    = :DCLACT-NOT-WRD.NOT-PRC-TS
		//                 AND POL_NBR       = :DCLACT-NOT-WRD.POL-NBR
		//                 AND ST_WRD_SEQ_CD = :DCLACT-NOT-WRD.ST-WRD-SEQ-CD
		//           END-EXEC.
		ws.getDclactNotWrd().setStWrdSeqCd(actNotWrdDao.selectRec1(ws.getDclactNotWrd().getCsrActNbr(), ws.getDclactNotWrd().getNotPrcTs(),
				ws.getDclactNotWrd().getPolNbr(), ws.getDclactNotWrd().getStWrdSeqCd(), ws.getDclactNotWrd().getStWrdSeqCd()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   GO TO 9822-EXIT
		//               WHEN ERD-SQL-NOT-FOUND
		//                                       TO TRUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: GO TO 9822-EXIT
			return;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET SW-CALL-ADD-ACT-NOT-WRD-SVC
			//                               TO TRUE
			ws.getSwitches().getCallAddActNotWrdSvcFg().setCallAddActNotWrdSvc();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT_WRD'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_WRD");
			// COB_CODE: MOVE '9822-CHECK-FOR-DUP-ACT-NOT-WRD'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9822-CHECK-FOR-DUP-ACT-NOT-WRD");
			// COB_CODE: MOVE 'SELECT ACT_NOT PARENT FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT ACT_NOT PARENT FAILED");
			// COB_CODE: STRING 'XZT95O-CSR-ACT-NBR='
			//                  XZT95O-CSR-ACT-NBR
			//                  'XZT95O-TK-NOT-PRC-TS='
			//                  XZT95O-TK-NOT-PRC-TS
			//                  'XZT95O-POL-NBR='
			//                  XZT95O-POL-NBR (SS-PL)
			//                  'DCLST-CNC-WRD-RQR.ST-WRD-SEQ-CD='
			//                  ST-WRD-SEQ-CD OF DCLST-CNC-WRD-RQR
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZT95O-CSR-ACT-NBR=", wsXz0t9050Row.getoCsrActNbrFormatted(), "XZT95O-TK-NOT-PRC-TS=",
							wsXz0t9050Row.getoTkNotPrcTsFormatted(), "XZT95O-POL-NBR=", wsXz0t9050Row.getoPolNbrFormatted(ws.getSubscripts().getPl()),
							"DCLST-CNC-WRD-RQR.ST-WRD-SEQ-CD=", ws.getDclstCncWrdRqr().getStWrdSeqCdFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 9823-CALL-ADD-ACT-NOT-WRD-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PARAGRAPH TO CALL THE ADD ACCOUNT NOTIFICATION WORDING SERVICE *
	 * *****************************************************************
	 * *****************************************************************
	 *  THE FIRST TIME INTO THIS PARAGRAPH, THE ROW HAS ALREADY BEEN   *
	 *  INITIALIZED.                                                   *
	 * *****************************************************************</pre>*/
	private void callAddActNotWrdSvc() {
		// COB_CODE: IF SW-NOT-ACT-NOT-WRD-SVC-CALLED
		//               END-IF
		//           END-IF.
		if (!ws.getSwitches().isActNotWrdSvcCalledFlag()) {
			// COB_CODE: SET SW-ACT-NOT-WRD-SVC-CALLED
			//                                   TO TRUE
			ws.getSwitches().setActNotWrdSvcCalledFlag(true);
			// COB_CODE: PERFORM 9823-1-ALC-ADD-ACT-NOT-WRD-MEM
			alcAddActNotWrdMem();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9823-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9823-EXIT
				return;
			}
		}
		//*  SET INPUT FOR ADD-ACT-NOT-WRD SERVICE.
		//*  BE SURE TO SET OPERATION IN THIS PARAGRAPH AS WELL.
		// COB_CODE: PERFORM 9823-2-ADD-ACT-NOT-WRD-SVC-INP.
		addActNotWrdSvcInp();
		//*  CALL ADD-ACT-NOT-WRD SERVICE.
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-ADD-ACT-NOT-WRD-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P9000", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getSpAddActNotWrdSvc(), new Xz0x0010());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 9823-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-WRD-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpAddActNotWrdSvc());
			// COB_CODE: MOVE '9823-CALL-ADD-ACT-NOT-WRD-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9823-CALL-ADD-ACT-NOT-WRD-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'CALL TO ADD ACT NOT WRD SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CALL TO ADD ACT NOT WRD SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9823-CALL-ADD-ACT-NOT-WRD-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("9823-CALL-ADD-ACT-NOT-WRD-SVC").append(".  FAILED MODULE EIBRESP CODE IS ")
							.append(ws.getWorkingStorageArea().getEibrespCdFormatted()).append(".  FAILED MODULE EIBRESP2 CODE IS ")
							.append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9823-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-WRD-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getSpAddActNotWrdSvc());
		// COB_CODE: MOVE '9823-CALL-ADD-ACT-NOT-WRD-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("9823-CALL-ADD-ACT-NOT-WRD-SVC");
		// COB_CODE: MOVE 'ADD-ACT-NOT-WRD INFO' TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("ADD-ACT-NOT-WRD INFO");
		// COB_CODE: MOVE 'ADD-ACT-NOT-WRD SVC'  TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("ADD-ACT-NOT-WRD SVC");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9823-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9823-EXIT
			return;
		}
	}

	/**Original name: 9823-1-ALC-ADD-ACT-NOT-WRD-MEM_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE ADD-ACT-NOT-WRD SERVICE.           *
	 * *****************************************************************</pre>*/
	private void alcAddActNotWrdMem() {
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T0010-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x0010.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 9823-1-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-WRD-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpAddActNotWrdSvc());
			// COB_CODE: MOVE '9823-1-ALC-ADD-ACT-NOT-WRD-SVC-MEM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9823-1-ALC-ADD-ACT-NOT-WRD-SVC-MEM");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'ALLOCATE ADD ACT NOT WRD SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ALLOCATE ADD ACT NOT WRD SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9823-1-ALC-ADD-ACT-NOT-WRD-SVC-MEM'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("FAILED MODULE IS ").append(ws.getWorkingStorageArea().getProgramNameFormatted())
							.append(".  FAILED PARAGRAPH IS ").append("9823-1-ALC-ADD-ACT-NOT-WRD-SVC-MEM")
							.append(".  FAILED MODULE EIBRESP CODE IS ").append(ws.getWorkingStorageArea().getEibrespCdFormatted())
							.append(".  FAILED MODULE EIBRESP2 CODE IS ").append(ws.getWorkingStorageArea().getEibresp2CdFormatted()).append(".")
							.toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9823-1-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T0010-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t0010Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x0010.class)));
		// COB_CODE: INITIALIZE WS-XZ0T0010-ROW.
		initWsXz0t0010Row();
	}

	/**Original name: 9823-2-ADD-ACT-NOT-WRD-SVC-INP_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PARAGRAPH TO FILL IN ADD-ACT-NOT-WRD SERVICE INPUT.            *
	 * *****************************************************************
	 * **  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * **  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void addActNotWrdSvcInp() {
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T0010-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x0010.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: SET PPC-SERVICE-DATA-POINTER
		//                                       TO ADDRESS OF WS-XZ0T0010-ROW.
		ws.getWsProxyProgramArea().setPpcServiceDataPointer(pointerManager.addressOf(wsXz0t0010Row));
		// COB_CODE: INITIALIZE WS-XZ0T0010-ROW.
		initWsXz0t0010Row();
		// COB_CODE: MOVE WS-ADD-ACT-NOT-WRD     TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getAddActNotWrd());
		// COB_CODE: MOVE XZY900-USERID          TO XZT95I-USERID.
		wsXz0t9050Row.setiUserid(ws.getWsPrpCncWrdBpoRow().getUserid());
		// COB_CODE: MOVE XZY900-CSR-ACT-NBR     TO XZT10I-CSR-ACT-NBR.
		wsXz0t0010Row.setXzt10iCsrActNbr(ws.getWsPrpCncWrdBpoRow().getCsrActNbr());
		// COB_CODE: MOVE XZY900-NOT-PRC-TS      TO XZT10I-TK-NOT-PRC-TS.
		wsXz0t0010Row.setXzt10iTkNotPrcTs(ws.getWsPrpCncWrdBpoRow().getNotPrcTs());
		// COB_CODE: MOVE XZT95O-POL-NBR (SS-PL) TO XZT10I-POL-NBR.
		wsXz0t0010Row.setXzt10iPolNbr(wsXz0t9050Row.getoPolNbr(ws.getSubscripts().getPl()));
		// COB_CODE: MOVE ST-WRD-SEQ-CD OF DCLST-CNC-WRD-RQR
		//                                       TO XZT10I-TK-ST-WRD-SEQ-CD.
		wsXz0t0010Row.setXzt10iTkStWrdSeqCd(ws.getDclstCncWrdRqr().getStWrdSeqCd());
	}

	/**Original name: 9830-CLOSE-ST-CNC-WRD-RQR-CSR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CLOSE ST_CNC_WRD_RQR CURSOR                                    *
	 * *****************************************************************</pre>*/
	private void closeStCncWrdRqrCsr() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//               CLOSE ST_CNC_WRD_RQR_CSR
		//           END-EXEC.
		stCncWrdRqrDao.closeStCncWrdRqrCsr();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'ST_CNC_WRD_RQR'   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ST_CNC_WRD_RQR");
			// COB_CODE: MOVE '9830-CLOSE-ST-CNC-WRD-RQR-CSR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9830-CLOSE-ST-CNC-WRD-RQR-CSR");
			// COB_CODE: MOVE 'CLOSE CF-ST-CNC-WRD-RQR-CSR FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CF-ST-CNC-WRD-RQR-CSR FAILED");
			// COB_CODE: STRING 'XZT95O-POL-PRI-RSK-ST-ABB='
			//                  XZT95O-POL-PRI-RSK-ST-ABB (SS-PL)
			//                  'XZY900-ACT-NOT-TYP-CD='
			//                  XZY900-ACT-NOT-TYP-CD
			//                  'XZT95O-POL-EFF-DT='
			//                  XZT95O-POL-EFF-DT (SS-PL)
			//                  'SA-PL-POL-TYP-CD='
			//                  SA-PL-POL-TYP-CD
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZT95O-POL-PRI-RSK-ST-ABB=", wsXz0t9050Row.getoPolPriRskStAbbFormatted(ws.getSubscripts().getPl()),
							"XZY900-ACT-NOT-TYP-CD=", ws.getWsPrpCncWrdBpoRow().getActNotTypCdFormatted(), "XZT95O-POL-EFF-DT=",
							wsXz0t9050Row.getoPolEffDtFormatted(ws.getSubscripts().getPl()), "SA-PL-POL-TYP-CD=", ws.getSaPlPolTypCdFormatted(),
							";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9900-CHECK-ERRORS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   CHECK FOR ERROR, NLBE, OR WARNING RETURNED FROM SERVICE      *
	 * ****************************************************************</pre>*/
	private void checkErrors() {
		// COB_CODE: IF PPC-NO-ERROR-CODE
		//               GO TO 9900-EXIT
		//           END-IF.
		if (ws.getWsProxyProgramArea().getPpcErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: GO TO 9900-EXIT
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN PPC-FATAL-ERROR-CODE
		//                   PERFORM 9910-SET-FATAL-ERROR
		//               WHEN PPC-NLBE-CODE
		//                   PERFORM 9920-SET-NLBE
		//               WHEN PPC-WARNING-CODE
		//                   PERFORM 9930-SET-WARNING
		//           END-EVALUATE.
		switch (ws.getWsProxyProgramArea().getPpcErrorReturnCode().getDsdErrorReturnCodeFormatted()) {

		case DsdErrorReturnCode.FATAL_ERROR_CODE:// COB_CODE: PERFORM 9910-SET-FATAL-ERROR
			setFatalError();
			break;

		case DsdErrorReturnCode.NLBE_CODE:// COB_CODE: PERFORM 9920-SET-NLBE
			rng9920SetNlbe();
			break;

		case DsdErrorReturnCode.WARNING_CODE:// COB_CODE: PERFORM 9930-SET-WARNING
			rng9930SetWarning();
			break;

		default:
			break;
		}
	}

	/**Original name: 9910-SET-FATAL-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET FOR FATAL ERROR                                           *
	 *   SAVE OFF ANY ERROR MESSAGE THAT WAS RECEIVED FROM THE CALLED  *
	 *   SERVICE IN ORDER TO PUT IT IN THE DISPLAY AREAS AFTER LOGGING *
	 *   THE BUSINESS PROCESS ERROR.                                   *
	 *   THIS WILL ALLOW US TO DISPLAY THE ACTUAL ERROR THAT OCCURRED  *
	 *   IN THE CALLED SERVICE TO THE USER.                            *
	 * *****************************************************************</pre>*/
	private void setFatalError() {
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET ETRA-CICS-WEB-RECEIVE   TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
		// COB_CODE: MOVE WS-EC-MODULE           TO EFAL-ERR-OBJECT-NAME.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkingStorageArea().getErrorCheckInfo().getModule());
		// COB_CODE: MOVE WS-EC-PARAGRAPH        TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph(ws.getWorkingStorageArea().getErrorCheckInfo().getParagraph());
		// COB_CODE: MOVE PPC-FATAL-ERROR-MESSAGE
		//                                       TO EFAL-ERR-COMMENT
		//                                          EFAL-OBJ-DATA-KEY
		//                                          ES-01-FATAL-ERROR-MSG.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		ws.getEs01FatalErrorMsg().setEs01FatalErrorMsgFormatted(ws.getWsProxyProgramArea().getPpcFatalErrorMessageFormatted());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
		// COB_CODE: MOVE ES-01-FAILED-MODULE    TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule(ws.getEs01FatalErrorMsg().getFailedModule());
		// COB_CODE: MOVE ES-01-FAILED-PARAGRAPH TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph(ws.getEs01FatalErrorMsg().getFailedParagraph());
		// COB_CODE: MOVE ES-01-SQLCODE-DISPLAY  TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(ws.getEs01FatalErrorMsg().getSqlcodeDisplay());
		// COB_CODE: MOVE ES-01-EIBRESP-DISPLAY  TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(ws.getEs01FatalErrorMsg().getEibrespDisplay());
		// COB_CODE: MOVE ES-01-EIBRESP2-DISPLAY TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(ws.getEs01FatalErrorMsg().getEibresp2Display());
	}

	/**Original name: 9920-SET-NLBE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR NLBE                                                 *
	 * ****************************************************************</pre>*/
	private void setNlbe() {
		// COB_CODE: MOVE +0                     TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(((short) 0));
	}

	/**Original name: 9920-A<br>*/
	private String a6() {
		// COB_CODE: ADD +1                      TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(Trunc.toShort(1 + ws.getSubscripts().getMsgIdx(), 4));
		// COB_CODE: IF PPC-NON-LOG-ERR-MSG(SS-MSG-IDX) = SPACES
		//               GO TO 9920-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg())) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// we are setting UBOC-HALT-AND-RETURN to true so that if a service
		// gets an NLBE, the service ends.
		// COB_CODE: SET UBOC-HALT-AND-RETURN    TO TRUE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setBusErr();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO NLBE-FAILED-TABLE-OR-FILE.
		ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO NLBE-FAILED-COLUMN-OR-FIELD.
		ws.getNlbeCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-NON-LOG-ERR-MSG (SS-MSG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-MSG-IDX-MAX
		//               GO TO 9920-EXIT
		//           END-IF.
		if (ws.getSubscripts().isMsgIdxMax()) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// COB_CODE: GO TO 9920-A.
		return "9920-A";
	}

	/**Original name: 9930-SET-WARNING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR WARNING                                              *
	 * ****************************************************************</pre>*/
	private void setWarning() {
		// COB_CODE: MOVE +0                     TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(((short) 0));
	}

	/**Original name: 9930-A<br>*/
	private String a7() {
		// COB_CODE: ADD +1                      TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(Trunc.toShort(1 + ws.getSubscripts().getWngIdx(), 4));
		// COB_CODE: IF PPC-WARN-MSG(SS-WNG-IDX) = SPACES
		//               GO TO 9930-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg())) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: SET WS-NON-LOGGABLE-WARNING TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setWarning();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO UWRN-FAILED-TABLE-OR-FILE.
		ws.getUwrnCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO UWRN-FAILED-COLUMN-OR-FIELD.
		ws.getUwrnCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-WARN-MSG (SS-WNG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-WNG-IDX-MAX
		//               GO TO 9930-EXIT
		//           END-IF.
		if (ws.getSubscripts().isWngIdxMax()) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: GO TO 9930-A.
		return "9930-A";
	}

	/**Original name: RNG_3200-PROCESS-POLICY-ROWS_FIRST_SENTENCES-_-3200-EXIT<br>*/
	private void rng3200ProcessPolicyRows() {
		String retcode = "";
		boolean goto3200A = false;
		processPolicyRows();
		do {
			goto3200A = false;
			retcode = a();
		} while (retcode.equals("3200-A"));
	}

	/**Original name: RNG_3221-GET-ATM-CPP-ST-WRD-SEQ-CD_FIRST_SENTENCES-_-3221-EXIT<br>*/
	private void rng3221GetAtmCppStWrdSeqCd() {
		String retcode = "";
		boolean goto3221A = false;
		getAtmCppStWrdSeqCd();
		do {
			goto3221A = false;
			retcode = a1();
		} while (retcode.equals("3221-A"));
	}

	/**Original name: RNG_3222-GET-MNL-CPP-ST-WRD-SEQ-CD_FIRST_SENTENCES-_-3222-EXIT<br>*/
	private void rng3222GetMnlCppStWrdSeqCd() {
		String retcode = "";
		boolean goto3222A = false;
		getMnlCppStWrdSeqCd();
		do {
			goto3222A = false;
			retcode = a2();
		} while (retcode.equals("3222-A"));
	}

	/**Original name: RNG_3218-1-FILL-IN-ATM-INS-LIN-CD_FIRST_SENTENCES-_-3218-1-EXIT<br>*/
	private void rng32181FillInAtmInsLinCd() {
		String retcode = "";
		boolean goto32181A = false;
		fillInAtmInsLinCd();
		do {
			goto32181A = false;
			retcode = a3();
		} while (retcode.equals("3218-1-A"));
	}

	/**Original name: RNG_3218-2-FILL-IN-MNL-INS-LIN-CD_FIRST_SENTENCES-_-3218-2-EXIT<br>*/
	private void rng32182FillInMnlInsLinCd() {
		String retcode = "";
		boolean goto32182A = false;
		fillInMnlInsLinCd();
		do {
			goto32182A = false;
			retcode = a4();
		} while (retcode.equals("3218-2-A"));
	}

	/**Original name: RNG_9820-PROCESS-ST-WRD-SEQ-CD_FIRST_SENTENCES-_-9820-EXIT<br>*/
	private void rng9820ProcessStWrdSeqCd() {
		String retcode = "";
		boolean goto9820A = false;
		processStWrdSeqCd();
		do {
			goto9820A = false;
			retcode = a5();
		} while (retcode.equals("9820-A"));
	}

	/**Original name: RNG_9920-SET-NLBE_FIRST_SENTENCES-_-9920-EXIT<br>*/
	private void rng9920SetNlbe() {
		String retcode = "";
		boolean goto9920A = false;
		setNlbe();
		do {
			goto9920A = false;
			retcode = a6();
		} while (retcode.equals("9920-A"));
	}

	/**Original name: RNG_9930-SET-WARNING_FIRST_SENTENCES-_-9930-EXIT<br>*/
	private void rng9930SetWarning() {
		String retcode = "";
		boolean goto9930A = false;
		setWarning();
		do {
			goto9930A = false;
			retcode = a7();
		} while (retcode.equals("9930-A"));
	}

	public void initWsPrpCncWrdBpoRow() {
		ws.getWsPrpCncWrdBpoRow().setCsrActNbr("");
		ws.getWsPrpCncWrdBpoRow().setNotPrcTs("");
		ws.getWsPrpCncWrdBpoRow().setActNotTypCd("");
		ws.getWsPrpCncWrdBpoRow().setUserid("");
	}

	public void initPpcMemoryAllocationParms() {
		ws.getWsProxyProgramArea().setPpcServiceDataSize(0);
	}

	public void initWsXz0t9050Row() {
		wsXz0t9050Row.setiTkNotPrcTs("");
		wsXz0t9050Row.setiTkFrmSeqNbr(0);
		wsXz0t9050Row.setiCsrActNbr("");
		wsXz0t9050Row.setiUserid("");
		wsXz0t9050Row.setoTkNotPrcTs("");
		wsXz0t9050Row.setoCsrActNbr("");
		for (int idx0 = 1; idx0 <= LServiceContractAreaXz0x9050.O_POLICY_ROW_MAXOCCURS; idx0++) {
			wsXz0t9050Row.setoTkNinCltId(idx0, "");
			wsXz0t9050Row.setoTkNinAdrId(idx0, "");
			wsXz0t9050Row.setoTkWfStartedInd(idx0, Types.SPACE_CHAR);
			wsXz0t9050Row.setoTkPolBilStaCd(idx0, Types.SPACE_CHAR);
			wsXz0t9050Row.setoPolNbr(idx0, "");
			wsXz0t9050Row.setoPolTypCd(idx0, "");
			wsXz0t9050Row.setoPolTypDes(idx0, "");
			wsXz0t9050Row.setoPolPriRskStAbb(idx0, "");
			wsXz0t9050Row.setoNotEffDt(idx0, "");
			wsXz0t9050Row.setoPolEffDt(idx0, "");
			wsXz0t9050Row.setoPolExpDt(idx0, "");
			wsXz0t9050Row.setoPolDueAmt(idx0, new AfDecimal(0, 10, 2));
			wsXz0t9050Row.setoMasterCompanyNbr(idx0, "");
			wsXz0t9050Row.setoMasterCompanyDes(idx0, "");
		}
	}

	public void initWsFwbt0011Row() {
		wsFwbt0011Row.setiTkPolId("");
		wsFwbt0011Row.setiTkPolNbr("");
		wsFwbt0011Row.setiTkPolEffDt("");
		wsFwbt0011Row.setiTkPolExpDt("");
		wsFwbt0011Row.setiTkQteNbr(0);
		wsFwbt0011Row.setiTkActNbr("");
		wsFwbt0011Row.setiTkWrtDt("");
		wsFwbt0011Row.setiTkPriRskStAbb("");
		wsFwbt0011Row.setiTkLobCd("");
		wsFwbt0011Row.setiTkCmpNbr("");
		wsFwbt0011Row.setiTkTobCd("");
		wsFwbt0011Row.setiTkSegCd("");
		wsFwbt0011Row.setiTkLgeActInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setiTkAlAdacInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setiPolNbr("");
		wsFwbt0011Row.setiAsOfDt("");
		wsFwbt0011Row.setiPolEffDt("");
		wsFwbt0011Row.setiPolExpDt("");
		wsFwbt0011Row.setiQteNbr(0);
		wsFwbt0011Row.setiUsrId("");
		wsFwbt0011Row.setoTkPolId("");
		wsFwbt0011Row.setoTkPolNbr("");
		wsFwbt0011Row.setoTkPolEffDt("");
		wsFwbt0011Row.setoTkPolExpDt("");
		wsFwbt0011Row.setoTkQteNbr(0);
		wsFwbt0011Row.setoTkActNbr("");
		wsFwbt0011Row.setoTkWrtDt("");
		wsFwbt0011Row.setoTkPriRskStAbb("");
		wsFwbt0011Row.setoTkLobCd("");
		wsFwbt0011Row.setoTkCmpNbr("");
		wsFwbt0011Row.setoTkTobCd("");
		wsFwbt0011Row.setoTkSegCd("");
		wsFwbt0011Row.setoTkLgeActInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoTkAlAdacInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoPolNbr("");
		wsFwbt0011Row.setoActNbr("");
		wsFwbt0011Row.setoActNbrFmt("");
		wsFwbt0011Row.setoOvlOgnCd("");
		wsFwbt0011Row.setoPolEffDt("");
		wsFwbt0011Row.setoPolExpDt("");
		wsFwbt0011Row.setoLobCd("");
		wsFwbt0011Row.setoLobDes("");
		wsFwbt0011Row.setoPolSymCd("");
		wsFwbt0011Row.setoPolSymDes("");
		wsFwbt0011Row.setoTobCd("");
		wsFwbt0011Row.setoTobDes("");
		wsFwbt0011Row.setoSegCd("");
		wsFwbt0011Row.setoSegDes("");
		wsFwbt0011Row.setoLgeActInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoCmpNbr("");
		wsFwbt0011Row.setoCmpDes("");
		wsFwbt0011Row.setoBrnCd("");
		wsFwbt0011Row.setoBrnDes("");
		wsFwbt0011Row.setoPriRskStAbb("");
		wsFwbt0011Row.setoPriRskStDes("");
		wsFwbt0011Row.setoPolTrmCd("");
		wsFwbt0011Row.setoPolTrmDes("");
		wsFwbt0011Row.setoLegEtyCd("");
		wsFwbt0011Row.setoLegEtyDes("");
		wsFwbt0011Row.setoCurStaCd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoCurStaDes("");
		wsFwbt0011Row.setoRewInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoPrePolNbr("");
		wsFwbt0011Row.setoPrePolSymCd("");
		wsFwbt0011Row.setoPrePolSymDes("");
		wsFwbt0011Row.setoDsaDedAmt(0);
		wsFwbt0011Row.setoDsaDedOvrInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoSysBknDedAmt(0);
		wsFwbt0011Row.setoSysBknDedOvrInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoRtnAmt(0);
		wsFwbt0011Row.setoSirDsc(new AfDecimal(0, 5, 3));
		wsFwbt0011Row.setoSirRmdAmt(0);
		wsFwbt0011Row.setoAudInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoAudStrCd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoPolFutCncDt("");
		wsFwbt0011Row.setoAceQteSeqNbr(0);
		wsFwbt0011Row.setoQteStaCd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoQteStaDes("");
		wsFwbt0011Row.setoQteGuaDt("");
		wsFwbt0011Row.setoAthLossRqrInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoAthLossRecDt("");
		wsFwbt0011Row.setoUwRspDt("");
		wsFwbt0011Row.setoWrtDt("");
		wsFwbt0011Row.setoInsScoreCd("");
		wsFwbt0011Row.setoNotCncDay(0);
		wsFwbt0011Row.setoPolAppRecDt("");
		wsFwbt0011Row.setoSplitBilledInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoAlAdacInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoPmaCd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoPmaDes("");
		wsFwbt0011Row.setoReaAmdCd("");
		wsFwbt0011Row.setoReaAmdTrsDes("");
		wsFwbt0011Row.setoReaAmdCdDes("");
		wsFwbt0011Row.setoIssAcyTs("");
		wsFwbt0011Row.setoEffDt("");
		wsFwbt0011Row.setoLstMdfAcyTs("");
		wsFwbt0011Row.setoBondInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoWrtPrmAmt(0);
		wsFwbt0011Row.setoAcyPolInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoOgnOgnCd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoOgnTrsTypCd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoOgnTrsTypDes("");
		wsFwbt0011Row.setoOgnTrsIssInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoCurTrsTypCd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoCurTrsTypDes("");
		wsFwbt0011Row.setoCurTrsDt("");
		wsFwbt0011Row.setoCurTrsPndInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoCurOgnCd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoWrapUpDt("");
		wsFwbt0011Row.setoPolCncInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoPndCncInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoOosStrInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoPolGoneMnlInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoInsLinGoneMnlInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoTmnInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoExpRewInd(Types.SPACE_CHAR);
		for (int idx0 = 1; idx0 <= WsFwbt0011Row.O_WNG_MSG_MAXOCCURS; idx0++) {
			wsFwbt0011Row.setoWngMsg(idx0, "");
		}
		wsFwbt0011Row.setoCncDt("");
		wsFwbt0011Row.setoCncReaCd("");
		wsFwbt0011Row.setoCncReaDes("");
		wsFwbt0011Row.setoWcCncCdFormatted("000");
		wsFwbt0011Row.setoWcCncDes("");
		wsFwbt0011Row.setoNinCltId("");
		wsFwbt0011Row.setoNinCrmId("");
		wsFwbt0011Row.setoNinAdrSeqNbr(0);
		wsFwbt0011Row.setoNmNbr(0);
		wsFwbt0011Row.setoNmSeqNbr(0);
		wsFwbt0011Row.setoWcLinkNbr(0);
		wsFwbt0011Row.setoFreFrmSeqNbr(0);
		wsFwbt0011Row.setoNinIdvNmInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoNinDsyNm("");
		wsFwbt0011Row.setoNinSrNm("");
		wsFwbt0011Row.setoNinLstNm("");
		wsFwbt0011Row.setoNinFstNm("");
		wsFwbt0011Row.setoNinMdlNm("");
		wsFwbt0011Row.setoNinSfx("");
		wsFwbt0011Row.setoNinPfx("");
		wsFwbt0011Row.setoNinAdr1("");
		wsFwbt0011Row.setoNinAdr2("");
		wsFwbt0011Row.setoNinCit("");
		wsFwbt0011Row.setoNinStAbb("");
		wsFwbt0011Row.setoNinStDes("");
		wsFwbt0011Row.setoNinPstCd("");
		wsFwbt0011Row.setoNinCty("");
		wsFwbt0011Row.setoNinAdrId("");
		wsFwbt0011Row.setoNinAdrTypCd("");
		wsFwbt0011Row.setoNinAdrTypDes("");
		wsFwbt0011Row.setoNinCitxTaxId("");
		wsFwbt0011Row.setoNinTaxTypeCd("");
		wsFwbt0011Row.setoNinTaxTypeDes("");
		wsFwbt0011Row.setoOwnCltId("");
		wsFwbt0011Row.setoOwnCrmId("");
		wsFwbt0011Row.setoOwnAdrSeqNbr(0);
		wsFwbt0011Row.setoOwnIdvNmInd(Types.SPACE_CHAR);
		wsFwbt0011Row.setoOwnDsyNm("");
		wsFwbt0011Row.setoOwnSrNm("");
		wsFwbt0011Row.setoOwnLstNm("");
		wsFwbt0011Row.setoOwnFstNm("");
		wsFwbt0011Row.setoOwnMdlNm("");
		wsFwbt0011Row.setoOwnSfx("");
		wsFwbt0011Row.setoOwnPfx("");
		wsFwbt0011Row.setoOwnAdr1("");
		wsFwbt0011Row.setoOwnAdr2("");
		wsFwbt0011Row.setoOwnCit("");
		wsFwbt0011Row.setoOwnStAbb("");
		wsFwbt0011Row.setoOwnStDes("");
		wsFwbt0011Row.setoOwnPstCd("");
		wsFwbt0011Row.setoOwnCty("");
		wsFwbt0011Row.setoOwnAdrId("");
		for (int idx0 = 1; idx0 <= WsFwbt0011Row.O_AUTOMATED_LINES_MAXOCCURS; idx0++) {
			wsFwbt0011Row.setoInsLinCd(idx0, "");
			wsFwbt0011Row.setoInsLinDes(idx0, "");
			wsFwbt0011Row.setoOgnEffDt(idx0, "");
			wsFwbt0011Row.setoExpDt(idx0, "");
			wsFwbt0011Row.setoGoneMnlInd(idx0, Types.SPACE_CHAR);
			wsFwbt0011Row.setoRtInd(idx0, Types.SPACE_CHAR);
		}
		for (int idx0 = 1; idx0 <= WsFwbt0011Row.O_MANUAL_LINES_MAXOCCURS; idx0++) {
			wsFwbt0011Row.setoMnlInsLinCd(idx0, "");
			wsFwbt0011Row.setoMnlInsLinDes(idx0, "");
			wsFwbt0011Row.setoMnlOgnEffDt(idx0, "");
			wsFwbt0011Row.setoMnlExpDt(idx0, "");
		}
		wsFwbt0011Row.setoBondAmt(0);
		wsFwbt0011Row.setoBondCmt("");
		wsFwbt0011Row.setoBondTypCd("");
		wsFwbt0011Row.setoBondTypDes("");
		wsFwbt0011Row.setoOgnTobCd("");
		wsFwbt0011Row.setoPolPriRskStCd("");
		wsFwbt0011Row.setoPolPriRskCtyCd("");
		wsFwbt0011Row.setoPolPriRskTwnCd("");
	}

	public void initDclstCncWrdRqr() {
		ws.getDclstCncWrdRqr().setStAbb("");
		ws.getDclstCncWrdRqr().setActNotTypCd("");
		ws.getDclstCncWrdRqr().setPolCovCd("");
		ws.getDclstCncWrdRqr().setStWrdSeqCd("");
		ws.getDclstCncWrdRqr().setStWrdEffDt("");
		ws.getDclstCncWrdRqr().setStWrdExpDt("");
	}

	public void initInsuranceLines() {
		for (int idx0 = 1; idx0 <= WorkingStorageAreaXz0p9000.IL_AUTOMATED_LINES_MAXOCCURS; idx0++) {
			ws.getWorkingStorageArea().getIlAutomatedLines(idx0).setWsIlAlInsLinCd("");
		}
		for (int idx0 = 1; idx0 <= WorkingStorageAreaXz0p9000.IL_MANUAL_LINES_MAXOCCURS; idx0++) {
			ws.getWorkingStorageArea().getIlManualLines(idx0).setWsIlMlInsLinCd("");
		}
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public void initWsXz0t0010Row() {
		wsXz0t0010Row.setXzt10iTkNotPrcTs("");
		wsXz0t0010Row.setXzt10iTkStWrdSeqCd("");
		wsXz0t0010Row.setXzt10iCsrActNbr("");
		wsXz0t0010Row.setXzt10iPolNbr("");
		wsXz0t0010Row.setXzt10iUserid("");
		wsXz0t0010Row.setXzt10oTkNotPrcTs("");
		wsXz0t0010Row.setXzt10oTkStWrdSeqCd("");
		wsXz0t0010Row.setXzt10oTkActNotWrdCsumFormatted("000000000");
		wsXz0t0010Row.setXzt10oCsrActNbr("");
		wsXz0t0010Row.setXzt10oPolNbr("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
