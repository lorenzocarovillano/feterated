/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IActNotRecTyp;
import com.federatedinsurance.crs.ws.Xz0b9020Data;

/**Original name: ActNotRecTypXz0b9020<br>*/
public class ActNotRecTypXz0b9020 implements IActNotRecTyp {

	//==== PROPERTIES ====
	private Xz0b9020Data ws;

	//==== CONSTRUCTORS ====
	public ActNotRecTypXz0b9020(Xz0b9020Data ws) {
		this.ws = ws;
	}

	//==== METHODS ====
	@Override
	public String getCerNbr() {
		return ws.getDclactNotRec().getCerNbr();
	}

	@Override
	public void setCerNbr(String cerNbr) {
		ws.getDclactNotRec().setCerNbr(cerNbr);
	}

	@Override
	public String getCerNbrObj() {
		if (ws.getNiCerNbr() >= 0) {
			return getCerNbr();
		} else {
			return null;
		}
	}

	@Override
	public void setCerNbrObj(String cerNbrObj) {
		if (cerNbrObj != null) {
			setCerNbr(cerNbrObj);
			ws.setNiCerNbr(((short) 0));
		} else {
			ws.setNiCerNbr(((short) -1));
		}
	}

	@Override
	public String getCitNm() {
		return ws.getDclactNotRec().getCitNm();
	}

	@Override
	public void setCitNm(String citNm) {
		ws.getDclactNotRec().setCitNm(citNm);
	}

	@Override
	public String getCitNmObj() {
		if (ws.getNiCitNm() >= 0) {
			return getCitNm();
		} else {
			return null;
		}
	}

	@Override
	public void setCitNmObj(String citNmObj) {
		if (citNmObj != null) {
			setCitNm(citNmObj);
			ws.setNiCitNm(((short) 0));
		} else {
			ws.setNiCitNm(((short) -1));
		}
	}

	@Override
	public String getLin1Adr() {
		return ws.getDclactNotRec().getLin1Adr();
	}

	@Override
	public void setLin1Adr(String lin1Adr) {
		ws.getDclactNotRec().setLin1Adr(lin1Adr);
	}

	@Override
	public String getLin1AdrObj() {
		if (ws.getNiLin1Adr() >= 0) {
			return getLin1Adr();
		} else {
			return null;
		}
	}

	@Override
	public void setLin1AdrObj(String lin1AdrObj) {
		if (lin1AdrObj != null) {
			setLin1Adr(lin1AdrObj);
			ws.setNiLin1Adr(((short) 0));
		} else {
			ws.setNiLin1Adr(((short) -1));
		}
	}

	@Override
	public String getLin2Adr() {
		return ws.getDclactNotRec().getLin2Adr();
	}

	@Override
	public void setLin2Adr(String lin2Adr) {
		ws.getDclactNotRec().setLin2Adr(lin2Adr);
	}

	@Override
	public String getLin2AdrObj() {
		if (ws.getNiLin2Adr() >= 0) {
			return getLin2Adr();
		} else {
			return null;
		}
	}

	@Override
	public void setLin2AdrObj(String lin2AdrObj) {
		if (lin2AdrObj != null) {
			setLin2Adr(lin2AdrObj);
			ws.setNiLin2Adr(((short) 0));
		} else {
			ws.setNiLin2Adr(((short) -1));
		}
	}

	@Override
	public String getPstCd() {
		return ws.getDclactNotRec().getPstCd();
	}

	@Override
	public void setPstCd(String pstCd) {
		ws.getDclactNotRec().setPstCd(pstCd);
	}

	@Override
	public String getPstCdObj() {
		if (ws.getNiPstCd() >= 0) {
			return getPstCd();
		} else {
			return null;
		}
	}

	@Override
	public void setPstCdObj(String pstCdObj) {
		if (pstCdObj != null) {
			setPstCd(pstCdObj);
			ws.setNiPstCd(((short) 0));
		} else {
			ws.setNiPstCd(((short) -1));
		}
	}

	@Override
	public String getRecLngDes() {
		return ws.getDclrecTyp().getRecLngDes();
	}

	@Override
	public void setRecLngDes(String recLngDes) {
		ws.getDclrecTyp().setRecLngDes(recLngDes);
	}

	@Override
	public String getRecNm() {
		return FixedStrings.get(ws.getDclactNotRec().getRecNmText(), ws.getDclactNotRec().getRecNmLen());
	}

	@Override
	public void setRecNm(String recNm) {
		this.ws.getDclactNotRec().setRecNmText(recNm);
		this.ws.getDclactNotRec().setRecNmLen((((short) strLen(recNm))));
	}

	@Override
	public short getRecSeqNbr() {
		return ws.getDclactNotRec().getRecSeqNbr();
	}

	@Override
	public void setRecSeqNbr(short recSeqNbr) {
		ws.getDclactNotRec().setRecSeqNbr(recSeqNbr);
	}

	@Override
	public short getRecSrOrdNbr() {
		return ws.getDclrecTyp().getRecSrOrdNbr();
	}

	@Override
	public void setRecSrOrdNbr(short recSrOrdNbr) {
		ws.getDclrecTyp().setRecSrOrdNbr(recSrOrdNbr);
	}

	@Override
	public String getRecTypCd() {
		return ws.getDclactNotRec().getRecTypCd();
	}

	@Override
	public void setRecTypCd(String recTypCd) {
		ws.getDclactNotRec().setRecTypCd(recTypCd);
	}

	@Override
	public String getStAbb() {
		return ws.getDclactNotRec().getStAbb();
	}

	@Override
	public void setStAbb(String stAbb) {
		ws.getDclactNotRec().setStAbb(stAbb);
	}

	@Override
	public String getStAbbObj() {
		if (ws.getNiStAbb() >= 0) {
			return getStAbb();
		} else {
			return null;
		}
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		if (stAbbObj != null) {
			setStAbb(stAbbObj);
			ws.setNiStAbb(((short) 0));
		} else {
			ws.setNiStAbb(((short) -1));
		}
	}
}
