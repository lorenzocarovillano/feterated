/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LI-STRING-LEN<br>
 * Variable: LI-STRING-LEN from program HALRLEN1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class LiStringLen extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LI-STRING-LEN
	private short liStringLen;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.LI_STRING_LEN;
	}

	@Override
	public void deserialize(byte[] buf) {
		setLiStringLenFromBuffer(buf);
	}

	public void setLiStringLen(short liStringLen) {
		this.liStringLen = liStringLen;
	}

	public void setLiStringLenFromBuffer(byte[] buffer, int offset) {
		setLiStringLen(MarshalByte.readBinaryShort(buffer, offset));
	}

	public void setLiStringLenFromBuffer(byte[] buffer) {
		setLiStringLenFromBuffer(buffer, 1);
	}

	public short getLiStringLen() {
		return this.liStringLen;
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.binShortToBuffer(getLiStringLen());
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LI_STRING_LEN = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int LI_STRING_LEN = 4;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int LI_STRING_LEN = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
