/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: RETURN-ERROR-STATUS<br>
 * Variable: RETURN-ERROR-STATUS from copybook CISLNSRB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ReturnErrorStatus {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char WARNING = 'W';
	public static final char ERROR = 'E';

	//==== METHODS ====
	public void setReturnErrorStatus(char returnErrorStatus) {
		this.value = returnErrorStatus;
	}

	public char getReturnErrorStatus() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURN_ERROR_STATUS = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
