/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.enums.Xzt005BypassSyncpointInd;

/**Original name: XZ0Z0005<br>
 * Variable: XZ0Z0005 from copybook XZ0Z0005<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz0z0005 {

	//==== PROPERTIES ====
	//Original name: XZT05I-CSR-ACT-NBR
	private String xzt05iCsrActNbr = DefaultValues.stringVal(Len.XZT05I_CSR_ACT_NBR);
	//Original name: XZT05I-NOT-PRC-TS
	private String xzt05iNotPrcTs = DefaultValues.stringVal(Len.XZT05I_NOT_PRC_TS);
	//Original name: XZT05I-USERID
	private String xzt05iUserid = DefaultValues.stringVal(Len.XZT05I_USERID);
	//Original name: FILLER-XZT005-SERVICE-INPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: XZT005-SERVICE-OUTPUTS
	private Xzt005ServiceOutputs xzt005ServiceOutputs = new Xzt005ServiceOutputs();
	//Original name: XZT005-OPERATION
	private String xzt005Operation = DefaultValues.stringVal(Len.XZT005_OPERATION);
	public static final String XZT005_GET_ACT_NOT = "GetAccountNotificationDtlByActTS";
	//Original name: XZT005-BYPASS-SYNCPOINT-IND
	private Xzt005BypassSyncpointInd xzt005BypassSyncpointInd = new Xzt005BypassSyncpointInd();
	//Original name: XZT005-ERROR-RETURN-CODE
	private DsdErrorReturnCode xzt005ErrorReturnCode = new DsdErrorReturnCode();
	//Original name: XZT005-ERROR-MESSAGE
	private String xzt005ErrorMessage = DefaultValues.stringVal(Len.XZT005_ERROR_MESSAGE);

	//==== METHODS ====
	public void setXzt005ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt05iCsrActNbr = MarshalByte.readString(buffer, position, Len.XZT05I_CSR_ACT_NBR);
		position += Len.XZT05I_CSR_ACT_NBR;
		xzt05iNotPrcTs = MarshalByte.readString(buffer, position, Len.XZT05I_NOT_PRC_TS);
		position += Len.XZT05I_NOT_PRC_TS;
		xzt05iUserid = MarshalByte.readString(buffer, position, Len.XZT05I_USERID);
		position += Len.XZT05I_USERID;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzt005ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt05iCsrActNbr, Len.XZT05I_CSR_ACT_NBR);
		position += Len.XZT05I_CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, xzt05iNotPrcTs, Len.XZT05I_NOT_PRC_TS);
		position += Len.XZT05I_NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, xzt05iUserid, Len.XZT05I_USERID);
		position += Len.XZT05I_USERID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setXzt05iCsrActNbr(String xzt05iCsrActNbr) {
		this.xzt05iCsrActNbr = Functions.subString(xzt05iCsrActNbr, Len.XZT05I_CSR_ACT_NBR);
	}

	public String getXzt05iCsrActNbr() {
		return this.xzt05iCsrActNbr;
	}

	public void setXzt05iNotPrcTs(String xzt05iNotPrcTs) {
		this.xzt05iNotPrcTs = Functions.subString(xzt05iNotPrcTs, Len.XZT05I_NOT_PRC_TS);
	}

	public String getXzt05iNotPrcTs() {
		return this.xzt05iNotPrcTs;
	}

	public void setXzt05iUserid(String xzt05iUserid) {
		this.xzt05iUserid = Functions.subString(xzt05iUserid, Len.XZT05I_USERID);
	}

	public String getXzt05iUserid() {
		return this.xzt05iUserid;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setXzt005ServiceParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt005Operation = MarshalByte.readString(buffer, position, Len.XZT005_OPERATION);
		position += Len.XZT005_OPERATION;
		xzt005BypassSyncpointInd.setXzt005BypassSyncpointInd(MarshalByte.readChar(buffer, position));
	}

	public byte[] getXzt005ServiceParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt005Operation, Len.XZT005_OPERATION);
		position += Len.XZT005_OPERATION;
		MarshalByte.writeChar(buffer, position, xzt005BypassSyncpointInd.getXzt005BypassSyncpointInd());
		return buffer;
	}

	public void setXzt005Operation(String xzt005Operation) {
		this.xzt005Operation = Functions.subString(xzt005Operation, Len.XZT005_OPERATION);
	}

	public String getXzt005Operation() {
		return this.xzt005Operation;
	}

	public void setXzt005GetActNot() {
		xzt005Operation = XZT005_GET_ACT_NOT;
	}

	public void setXzt005ServiceErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt005ErrorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		xzt005ErrorMessage = MarshalByte.readString(buffer, position, Len.XZT005_ERROR_MESSAGE);
	}

	public byte[] getXzt005ServiceErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt005ErrorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, xzt005ErrorMessage, Len.XZT005_ERROR_MESSAGE);
		return buffer;
	}

	public void setXzt005ErrorMessage(String xzt005ErrorMessage) {
		this.xzt005ErrorMessage = Functions.subString(xzt005ErrorMessage, Len.XZT005_ERROR_MESSAGE);
	}

	public String getXzt005ErrorMessage() {
		return this.xzt005ErrorMessage;
	}

	public Xzt005BypassSyncpointInd getXzt005BypassSyncpointInd() {
		return xzt005BypassSyncpointInd;
	}

	public DsdErrorReturnCode getXzt005ErrorReturnCode() {
		return xzt005ErrorReturnCode;
	}

	public Xzt005ServiceOutputs getXzt005ServiceOutputs() {
		return xzt005ServiceOutputs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT05I_CSR_ACT_NBR = 9;
		public static final int XZT05I_NOT_PRC_TS = 26;
		public static final int XZT05I_USERID = 8;
		public static final int FLR1 = 100;
		public static final int XZT005_OPERATION = 32;
		public static final int XZT005_ERROR_MESSAGE = 250;
		public static final int XZT005_SERVICE_INPUTS = XZT05I_CSR_ACT_NBR + XZT05I_NOT_PRC_TS + XZT05I_USERID + FLR1;
		public static final int XZT005_SERVICE_PARAMETERS = XZT005_OPERATION + Xzt005BypassSyncpointInd.Len.XZT005_BYPASS_SYNCPOINT_IND;
		public static final int XZT005_SERVICE_ERROR_INFO = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + XZT005_ERROR_MESSAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
