/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ003000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz003000 {

	//==== PROPERTIES ====
	//Original name: CF-SC-GOOD-RETURN
	private short scGoodReturn = ((short) 0);
	//Original name: FILLER-CF-CN-ACT-NOT-CSR
	private String flr1 = "ACT_NOT_CSR";
	//Original name: CF-GET-ACT-NOT-SVC-INTF
	private String getActNotSvcIntf = "XZ0G0005";
	//Original name: CF-MTN-ACT-NOT-SVC-INTF
	private String mtnActNotSvcIntf = "XZ0G0006";
	//Original name: CF-CICS-TARGET-TRAN-ID
	private String cicsTargetTranId = "KXZ1";
	//Original name: CF-INCOMPLETE-STATUS
	private String incompleteStatus = "10";

	//==== METHODS ====
	public short getScGoodReturn() {
		return this.scGoodReturn;
	}

	public String getCnActNotCsrFormatted() {
		return getFlr1Formatted();
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr1Formatted() {
		return Functions.padBlanks(getFlr1(), Len.FLR1);
	}

	public String getGetActNotSvcIntf() {
		return this.getActNotSvcIntf;
	}

	public String getMtnActNotSvcIntf() {
		return this.mtnActNotSvcIntf;
	}

	public String getCicsTargetTranId() {
		return this.cicsTargetTranId;
	}

	public String getIncompleteStatus() {
		return this.incompleteStatus;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 18;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
