/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HALLUDAT<br>
 * Variable: HALLUDAT from copybook HALLUDAT<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Halludat {

	//==== PROPERTIES ====
	//Original name: UDAT-ID
	private String id = DefaultValues.stringVal(Len.ID);
	//Original name: UDAT-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: UDAT-REC-SEQ
	private String recSeq = DefaultValues.stringVal(Len.REC_SEQ);
	/**Original name: UDAT-UOW-BUFFER-LENGTH<br>
	 * <pre>**         10  UDAT-REC-SEQ                   PIC 9(03).</pre>*/
	private String uowBufferLength = DefaultValues.stringVal(Len.UOW_BUFFER_LENGTH);
	//Original name: UDAT-UOW-BUS-OBJ-NM
	private String uowBusObjNm = DefaultValues.stringVal(Len.UOW_BUS_OBJ_NM);
	//Original name: UDAT-UOW-BUS-OBJ-DATA
	private String uowBusObjData = DefaultValues.stringVal(Len.UOW_BUS_OBJ_DATA);

	//==== METHODS ====
	public void initHalludatSpaces() {
		initCommonSpaces();
	}

	public void setCommonBytes(byte[] buffer) {
		setCommonBytes(buffer, 1);
	}

	/**Original name: UDAT-COMMON<br>
	 * <pre>*************************************************************
	 *  HALLUDAT                                                   *
	 *  UNIT OF WORK RESPONSE DATA UMT                             *
	 *  THIS COPYBOOK IS USED BY HALOMCM AND THE BUSINESS OBJECTS  *
	 *  TO READ/WRITE FROM/TO THE UOW RESPONSE DATA UMT.           *
	 * *************************************************************
	 *  MAINTENANCE LOG
	 *  SI#      DATE      PRGMR     DESCRIPTION
	 *  -------- --------- --------- -------------------------------
	 *  SAVANNAH 11JAN00   18448     CREATED.
	 *  28068    21NOV02   18448     INCREASE REC SEQ NUMBER.
	 * *************************************************************</pre>*/
	public byte[] getCommonBytes() {
		byte[] buffer = new byte[Len.COMMON];
		return getCommonBytes(buffer, 1);
	}

	public void setCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		setKeyBytes(buffer, position);
		position += Len.KEY;
		uowBufferLength = MarshalByte.readFixedString(buffer, position, Len.UOW_BUFFER_LENGTH);
		position += Len.UOW_BUFFER_LENGTH;
		setUowMessageBufferBytes(buffer, position);
	}

	public byte[] getCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		getKeyBytes(buffer, position);
		position += Len.KEY;
		MarshalByte.writeString(buffer, position, uowBufferLength, Len.UOW_BUFFER_LENGTH);
		position += Len.UOW_BUFFER_LENGTH;
		getUowMessageBufferBytes(buffer, position);
		return buffer;
	}

	public void initCommonSpaces() {
		initKeySpaces();
		uowBufferLength = "";
		initUowMessageBufferSpaces();
	}

	public String getKeyFormatted() {
		return MarshalByteExt.bufferToStr(getKeyBytes());
	}

	public void setKeyBytes(byte[] buffer) {
		setKeyBytes(buffer, 1);
	}

	/**Original name: UDAT-KEY<br>*/
	public byte[] getKeyBytes() {
		byte[] buffer = new byte[Len.KEY];
		return getKeyBytes(buffer, 1);
	}

	public void setKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		id = MarshalByte.readString(buffer, position, Len.ID);
		position += Len.ID;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		recSeq = MarshalByte.readFixedString(buffer, position, Len.REC_SEQ);
	}

	public byte[] getKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, id, Len.ID);
		position += Len.ID;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, recSeq, Len.REC_SEQ);
		return buffer;
	}

	public void initKeySpaces() {
		id = "";
		busObjNm = "";
		recSeq = "";
	}

	public void setId(String id) {
		this.id = Functions.subString(id, Len.ID);
	}

	public String getId() {
		return this.id;
	}

	public String getIdFormatted() {
		return Functions.padBlanks(getId(), Len.ID);
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public String getBusObjNmFormatted() {
		return Functions.padBlanks(getBusObjNm(), Len.BUS_OBJ_NM);
	}

	public void setRecSeq(int recSeq) {
		this.recSeq = NumericDisplay.asString(recSeq, Len.REC_SEQ);
	}

	public void setRecSeqFormatted(String recSeq) {
		this.recSeq = Trunc.toUnsignedNumeric(recSeq, Len.REC_SEQ);
	}

	public int getRecSeq() {
		return NumericDisplay.asInt(this.recSeq);
	}

	public String getRecSeqFormatted() {
		return this.recSeq;
	}

	public String getRecSeqAsString() {
		return getRecSeqFormatted();
	}

	public void setUowBufferLength(short uowBufferLength) {
		this.uowBufferLength = NumericDisplay.asString(uowBufferLength, Len.UOW_BUFFER_LENGTH);
	}

	public short getUowBufferLength() {
		return NumericDisplay.asShort(this.uowBufferLength);
	}

	public void setUowMessageBufferBytes(byte[] buffer, int offset) {
		int position = offset;
		uowBusObjNm = MarshalByte.readString(buffer, position, Len.UOW_BUS_OBJ_NM);
		position += Len.UOW_BUS_OBJ_NM;
		uowBusObjData = MarshalByte.readString(buffer, position, Len.UOW_BUS_OBJ_DATA);
	}

	public byte[] getUowMessageBufferBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, uowBusObjNm, Len.UOW_BUS_OBJ_NM);
		position += Len.UOW_BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, uowBusObjData, Len.UOW_BUS_OBJ_DATA);
		return buffer;
	}

	public void initUowMessageBufferSpaces() {
		uowBusObjNm = "";
		uowBusObjData = "";
	}

	public void setUowBusObjNm(String uowBusObjNm) {
		this.uowBusObjNm = Functions.subString(uowBusObjNm, Len.UOW_BUS_OBJ_NM);
	}

	public String getUowBusObjNm() {
		return this.uowBusObjNm;
	}

	public void setUowBusObjData(String uowBusObjData) {
		this.uowBusObjData = Functions.subString(uowBusObjData, Len.UOW_BUS_OBJ_DATA);
	}

	public String getUowBusObjData() {
		return this.uowBusObjData;
	}

	public String getUowBusObjDataFormatted() {
		return Functions.padBlanks(getUowBusObjData(), Len.UOW_BUS_OBJ_DATA);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID = 32;
		public static final int BUS_OBJ_NM = 32;
		public static final int REC_SEQ = 5;
		public static final int UOW_BUFFER_LENGTH = 4;
		public static final int UOW_BUS_OBJ_NM = 32;
		public static final int UOW_BUS_OBJ_DATA = 5000;
		public static final int KEY = ID + BUS_OBJ_NM + REC_SEQ;
		public static final int UOW_MESSAGE_BUFFER = UOW_BUS_OBJ_NM + UOW_BUS_OBJ_DATA;
		public static final int COMMON = KEY + UOW_BUFFER_LENGTH + UOW_MESSAGE_BUFFER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
