/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-TSQ3-SW<br>
 * Variable: WS-TSQ3-SW from program HALOUSDH<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTsq3Sw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char START_OF_TSQ3 = 'S';
	public static final char END_OF_TSQ3 = 'E';

	//==== METHODS ====
	public void setTsq3Sw(char tsq3Sw) {
		this.value = tsq3Sw;
	}

	public char getTsq3Sw() {
		return this.value;
	}

	public void setStartOfTsq3() {
		value = START_OF_TSQ3;
	}

	public boolean isEndOfTsq3() {
		return value == END_OF_TSQ3;
	}

	public void setEndOfTsq3() {
		value = END_OF_TSQ3;
	}
}
