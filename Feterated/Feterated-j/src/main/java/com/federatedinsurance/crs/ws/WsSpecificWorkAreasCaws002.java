/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsInboundReqSw;
import com.federatedinsurance.crs.ws.enums.WsRecordProcessedSw;

/**Original name: WS-SPECIFIC-WORK-AREAS<br>
 * Variable: WS-SPECIFIC-WORK-AREAS from program CAWS002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSpecificWorkAreasCaws002 {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "CAWS002";
	//Original name: WS-APPLICATION-NM
	private String wsApplicationNm = "CIWS";
	//Original name: WS-ASSOC-BUS-OBJ-NM
	private String wsAssocBusObjNm = "CLIENT_TAB_V";
	//Original name: WS-RECORD-PROCESSED-SW
	private WsRecordProcessedSw wsRecordProcessedSw = new WsRecordProcessedSw();
	//Original name: WS-INBOUND-REQ-SW
	private WsInboundReqSw wsInboundReqSw = new WsInboundReqSw();
	/**Original name: WS-NUM-TBL-ENTRIES<br>
	 * <pre>** THE VALID INBOUND AND OUTBOUND COLUMN INDICATOR VALUES
	 * ** CAN BE FOUND IN COPYBOOK HALLCOM.</pre>*/
	private short wsNumTblEntries = DefaultValues.SHORT_VAL;
	//Original name: WS-DFL-DTA-AMT-NI
	private short wsDflDtaAmtNi = ((short) 0);
	//Original name: WS-DFL-OFS-NBR-NI
	private short wsDflOfsNbrNi = ((short) 0);
	//Original name: WS-HOLD-INITIAL-AMT
	private AfDecimal wsHoldInitialAmt = new AfDecimal("-999999999", 14, 2);
	//Original name: WS-HOLD-AREA
	private WsHoldArea wsHoldArea = new WsHoldArea();
	//Original name: WS-DATA-PRIV-CTX-LIT
	private String wsDataPrivCtxLit = "DATA_PRIVACY_CONTEXT";
	//Original name: WS-CONTEXT-REDEF
	private WsContextRedef wsContextRedef = new WsContextRedef();

	//==== METHODS ====
	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public String getWsApplicationNm() {
		return this.wsApplicationNm;
	}

	public String getWsApplicationNmFormatted() {
		return Functions.padBlanks(getWsApplicationNm(), Len.WS_APPLICATION_NM);
	}

	public void setWsAssocBusObjNm(String wsAssocBusObjNm) {
		this.wsAssocBusObjNm = Functions.subString(wsAssocBusObjNm, Len.WS_ASSOC_BUS_OBJ_NM);
	}

	public String getWsAssocBusObjNm() {
		return this.wsAssocBusObjNm;
	}

	public void setWsNumTblEntries(short wsNumTblEntries) {
		this.wsNumTblEntries = wsNumTblEntries;
	}

	public short getWsNumTblEntries() {
		return this.wsNumTblEntries;
	}

	public void setWsDflDtaAmtNi(short wsDflDtaAmtNi) {
		this.wsDflDtaAmtNi = wsDflDtaAmtNi;
	}

	public short getWsDflDtaAmtNi() {
		return this.wsDflDtaAmtNi;
	}

	public void setWsDflOfsNbrNi(short wsDflOfsNbrNi) {
		this.wsDflOfsNbrNi = wsDflOfsNbrNi;
	}

	public short getWsDflOfsNbrNi() {
		return this.wsDflOfsNbrNi;
	}

	public void setWsHoldInitialAmt(AfDecimal wsHoldInitialAmt) {
		this.wsHoldInitialAmt.assign(wsHoldInitialAmt);
	}

	public AfDecimal getWsHoldInitialAmt() {
		return this.wsHoldInitialAmt.copy();
	}

	public String getWsDataPrivCtxLit() {
		return this.wsDataPrivCtxLit;
	}

	public WsContextRedef getWsContextRedef() {
		return wsContextRedef;
	}

	public WsHoldArea getWsHoldArea() {
		return wsHoldArea;
	}

	public WsInboundReqSw getWsInboundReqSw() {
		return wsInboundReqSw;
	}

	public WsRecordProcessedSw getWsRecordProcessedSw() {
		return wsRecordProcessedSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_ASSOC_BUS_OBJ_NM = 32;
		public static final int WS_NUM_TBL_ENTRIES = 3;
		public static final int WS_APPLICATION_NM = 8;
		public static final int WS_PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
