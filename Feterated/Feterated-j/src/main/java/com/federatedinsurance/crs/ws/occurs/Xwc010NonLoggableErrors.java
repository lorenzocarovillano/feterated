/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XWC010-NON-LOGGABLE-ERRORS<br>
 * Variables: XWC010-NON-LOGGABLE-ERRORS from copybook XWC010C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xwc010NonLoggableErrors {

	//==== PROPERTIES ====
	//Original name: XWC010-NON-LOG-ERR-MSG
	private String xwc010NonLogErrMsg = DefaultValues.stringVal(Len.XWC010_NON_LOG_ERR_MSG);

	//==== METHODS ====
	public void setXwc010NonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		xwc010NonLogErrMsg = MarshalByte.readString(buffer, position, Len.XWC010_NON_LOG_ERR_MSG);
	}

	public byte[] getXwc010NonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xwc010NonLogErrMsg, Len.XWC010_NON_LOG_ERR_MSG);
		return buffer;
	}

	public void initXwc010NonLoggableErrorsSpaces() {
		xwc010NonLogErrMsg = "";
	}

	public void setXwc010NonLogErrMsg(String xwc010NonLogErrMsg) {
		this.xwc010NonLogErrMsg = Functions.subString(xwc010NonLogErrMsg, Len.XWC010_NON_LOG_ERR_MSG);
	}

	public String getXwc010NonLogErrMsg() {
		return this.xwc010NonLogErrMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XWC010_NON_LOG_ERR_MSG = 500;
		public static final int XWC010_NON_LOGGABLE_ERRORS = XWC010_NON_LOG_ERR_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
