/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P90H0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p90h0 {

	//==== PROPERTIES ====
	//Original name: CF-LINE-2-START
	private short line2Start = ((short) 46);
	//Original name: CF-MAX-TBL-ENTRIES
	private short maxTblEntries = ((short) 50);
	//Original name: CF-ATC-PERSONAL-LINES
	private String atcPersonalLines = "PL";
	//Original name: CF-INSURED-RECIPIENT
	private String insuredRecipient = "INS";
	//Original name: CF-NO
	private char no = 'N';
	//Original name: CF-RLT-TYP-CD-COWN
	private String rltTypCdCown = "COWN";
	//Original name: CF-SERVICE-PROXY
	private CfServiceProxyXz0p90h0 serviceProxy = new CfServiceProxyXz0p90h0();
	//Original name: CF-NO-TTY-FON-WNG-NOT-TYP
	private CfNoTtyFonWngNotTyp noTtyFonWngNotTyp = new CfNoTtyFonWngNotTyp();
	//Original name: CF-SO-MATCH-ACT-ADR-ID
	private char soMatchActAdrId = '1';
	//Original name: CF-SO-NO-MATCH-ACT-ADR-ID
	private char soNoMatchActAdrId = '2';

	//==== METHODS ====
	public short getLine2Start() {
		return this.line2Start;
	}

	public short getMaxTblEntries() {
		return this.maxTblEntries;
	}

	public String getAtcPersonalLines() {
		return this.atcPersonalLines;
	}

	public String getInsuredRecipient() {
		return this.insuredRecipient;
	}

	public char getNo() {
		return this.no;
	}

	public String getRltTypCdCown() {
		return this.rltTypCdCown;
	}

	public char getSoMatchActAdrId() {
		return this.soMatchActAdrId;
	}

	public char getSoNoMatchActAdrId() {
		return this.soNoMatchActAdrId;
	}

	public CfNoTtyFonWngNotTyp getNoTtyFonWngNotTyp() {
		return noTtyFonWngNotTyp;
	}

	public CfServiceProxyXz0p90h0 getServiceProxy() {
		return serviceProxy;
	}
}
