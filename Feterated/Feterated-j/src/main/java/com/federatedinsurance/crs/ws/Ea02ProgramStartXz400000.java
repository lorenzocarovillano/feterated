/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-02-PROGRAM-START<br>
 * Variable: EA-02-PROGRAM-START from program XZ400000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea02ProgramStartXz400000 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-PROGRAM-START
	private String flr1 = "XZ400000 -";
	//Original name: FILLER-EA-02-PROGRAM-START-1
	private String flr2 = "START OF";
	//Original name: FILLER-EA-02-PROGRAM-START-2
	private String flr3 = "PROGRAM";

	//==== METHODS ====
	public String getEa02ProgramStartFormatted() {
		return MarshalByteExt.bufferToStr(getEa02ProgramStartBytes());
	}

	public byte[] getEa02ProgramStartBytes() {
		byte[] buffer = new byte[Len.EA02_PROGRAM_START];
		return getEa02ProgramStartBytes(buffer, 1);
	}

	public byte[] getEa02ProgramStartBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 9;
		public static final int FLR3 = 7;
		public static final int EA02_PROGRAM_START = FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
