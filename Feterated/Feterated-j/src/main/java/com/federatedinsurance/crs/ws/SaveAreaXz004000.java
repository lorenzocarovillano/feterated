/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.SaNotTypCd;
import com.federatedinsurance.crs.ws.enums.SaReturnCodeXz004000;
import com.federatedinsurance.crs.ws.enums.SaSqlcode;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaveAreaXz004000 {

	//==== PROPERTIES ====
	//Original name: SA-SQLCODE
	private SaSqlcode saSqlcode = new SaSqlcode();
	//Original name: SA-RETURN-CODE
	private SaReturnCodeXz004000 saReturnCode = new SaReturnCodeXz004000();
	//Original name: SA-CURRENT-TIMESTAMP
	private String saCurrentTimestamp = DefaultValues.stringVal(Len.SA_CURRENT_TIMESTAMP);
	//Original name: SA-CURRENT-DATE
	private SaCurrentDate saCurrentDate = new SaCurrentDate();
	//Original name: SA-ACT-NBR
	private String saActNbr = DefaultValues.stringVal(Len.SA_ACT_NBR);
	//Original name: SA-POL-NBR
	private String saPolNbr = DefaultValues.stringVal(Len.SA_POL_NBR);
	//Original name: SA-POL-EFF-DT
	private String saPolEffDt = DefaultValues.stringVal(Len.SA_POL_EFF_DT);
	//Original name: SA-STA-MDF-TS
	private String saStaMdfTs = DefaultValues.stringVal(Len.SA_STA_MDF_TS);
	//Original name: SA-NOT-PRC-TS-DT
	private String saNotPrcTsDt = DefaultValues.stringVal(Len.SA_NOT_PRC_TS_DT);
	//Original name: FILLER-SA-NOT-PRC-TS-COMPARE
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: SA-STA-MDF-TS-DT
	private String saStaMdfTsDt = DefaultValues.stringVal(Len.SA_STA_MDF_TS_DT);
	//Original name: FILLER-SA-STA-MDF-TS-COMPARE
	private String flr2 = DefaultValues.stringVal(Len.FLR1);
	//Original name: SA-VLD-TS
	private String saVldTs = DefaultValues.stringVal(Len.SA_VLD_TS);
	//Original name: SA-NOT-EFF-DT
	private String saNotEffDt = DefaultValues.stringVal(Len.SA_NOT_EFF_DT);
	//Original name: SA-ACCOUNT-MM-DD-YYYY
	private SaAccountMmDdYyyy saAccountMmDdYyyy = new SaAccountMmDdYyyy();
	//Original name: SA-NOT-TYP-CD
	private SaNotTypCd saNotTypCd = new SaNotTypCd();

	//==== METHODS ====
	public void setSaCurrentTimestamp(String saCurrentTimestamp) {
		this.saCurrentTimestamp = Functions.subString(saCurrentTimestamp, Len.SA_CURRENT_TIMESTAMP);
	}

	public String getSaCurrentTimestamp() {
		return this.saCurrentTimestamp;
	}

	public void setSaActNbr(String saActNbr) {
		this.saActNbr = Functions.subString(saActNbr, Len.SA_ACT_NBR);
	}

	public String getSaActNbr() {
		return this.saActNbr;
	}

	public void setSaPolNbr(String saPolNbr) {
		this.saPolNbr = Functions.subString(saPolNbr, Len.SA_POL_NBR);
	}

	public String getSaPolNbr() {
		return this.saPolNbr;
	}

	public String getSaPolNbrFormatted() {
		return Functions.padBlanks(getSaPolNbr(), Len.SA_POL_NBR);
	}

	public void setSaPolEffDt(String saPolEffDt) {
		this.saPolEffDt = Functions.subString(saPolEffDt, Len.SA_POL_EFF_DT);
	}

	public String getSaPolEffDt() {
		return this.saPolEffDt;
	}

	public String getSaPolEffDtFormatted() {
		return Functions.padBlanks(getSaPolEffDt(), Len.SA_POL_EFF_DT);
	}

	public void setSaStaMdfTs(String saStaMdfTs) {
		this.saStaMdfTs = Functions.subString(saStaMdfTs, Len.SA_STA_MDF_TS);
	}

	public String getSaStaMdfTs() {
		return this.saStaMdfTs;
	}

	public void setSaNotPrcTsCompareFormatted(String data) {
		byte[] buffer = new byte[Len.SA_NOT_PRC_TS_COMPARE];
		MarshalByte.writeString(buffer, 1, data, Len.SA_NOT_PRC_TS_COMPARE);
		setSaNotPrcTsCompareBytes(buffer, 1);
	}

	public void setSaNotPrcTsCompareBytes(byte[] buffer, int offset) {
		int position = offset;
		saNotPrcTsDt = MarshalByte.readString(buffer, position, Len.SA_NOT_PRC_TS_DT);
		position += Len.SA_NOT_PRC_TS_DT;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public void setSaNotPrcTsDt(String saNotPrcTsDt) {
		this.saNotPrcTsDt = Functions.subString(saNotPrcTsDt, Len.SA_NOT_PRC_TS_DT);
	}

	public String getSaNotPrcTsDt() {
		return this.saNotPrcTsDt;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setSaStaMdfTsCompareFormatted(String data) {
		byte[] buffer = new byte[Len.SA_STA_MDF_TS_COMPARE];
		MarshalByte.writeString(buffer, 1, data, Len.SA_STA_MDF_TS_COMPARE);
		setSaStaMdfTsCompareBytes(buffer, 1);
	}

	public void setSaStaMdfTsCompareBytes(byte[] buffer, int offset) {
		int position = offset;
		saStaMdfTsDt = MarshalByte.readString(buffer, position, Len.SA_STA_MDF_TS_DT);
		position += Len.SA_STA_MDF_TS_DT;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public void setSaStaMdfTsDt(String saStaMdfTsDt) {
		this.saStaMdfTsDt = Functions.subString(saStaMdfTsDt, Len.SA_STA_MDF_TS_DT);
	}

	public String getSaStaMdfTsDt() {
		return this.saStaMdfTsDt;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR1);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setSaVldTs(String saVldTs) {
		this.saVldTs = Functions.subString(saVldTs, Len.SA_VLD_TS);
	}

	public String getSaVldTs() {
		return this.saVldTs;
	}

	public void setSaNotEffDt(String saNotEffDt) {
		this.saNotEffDt = Functions.subString(saNotEffDt, Len.SA_NOT_EFF_DT);
	}

	public String getSaNotEffDt() {
		return this.saNotEffDt;
	}

	public String getSaNotEffDtFormatted() {
		return Functions.padBlanks(getSaNotEffDt(), Len.SA_NOT_EFF_DT);
	}

	public SaAccountMmDdYyyy getSaAccountMmDdYyyy() {
		return saAccountMmDdYyyy;
	}

	public SaCurrentDate getSaCurrentDate() {
		return saCurrentDate;
	}

	public SaNotTypCd getSaNotTypCd() {
		return saNotTypCd;
	}

	public SaReturnCodeXz004000 getSaReturnCode() {
		return saReturnCode;
	}

	public SaSqlcode getSaSqlcode() {
		return saSqlcode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_PARAGRAPH_NBR = 4;
		public static final int SA_CURRENT_TIMESTAMP = 26;
		public static final int SA_ACT_NBR = 9;
		public static final int SA_POL_NBR = 7;
		public static final int SA_POL_EFF_DT = 10;
		public static final int SA_STA_MDF_TS = 26;
		public static final int SA_IMP_STA_MDF_TS = 26;
		public static final int SA_NOT_PRC_TS_DT = 10;
		public static final int FLR1 = 16;
		public static final int SA_STA_MDF_TS_DT = 10;
		public static final int SA_VLD_TS = 26;
		public static final int SA_NOT_EFF_DT = 10;
		public static final int SA_NOT_PRC_TS_COMPARE = SA_NOT_PRC_TS_DT + FLR1;
		public static final int SA_STA_MDF_TS_COMPARE = SA_STA_MDF_TS_DT + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
