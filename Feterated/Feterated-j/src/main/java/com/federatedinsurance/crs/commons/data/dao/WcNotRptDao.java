/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IWcNotRpt;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [WC_NOT_RPT]
 * 
 */
public class WcNotRptDao extends BaseSqlDao<IWcNotRpt> {

	private Cursor wcNotRptCrsr;
	private final IRowMapper<IWcNotRpt> fetchWcNotRptCrsrRm = buildNamedRowMapper(IWcNotRpt.class, "polNbr", "polEffDt", "staMdfTs", "notEffDtObj",
			"notTypCdObj");

	public WcNotRptDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IWcNotRpt> getToClass() {
		return IWcNotRpt.class;
	}

	public IWcNotRpt fetchWcNotRptCrsr(IWcNotRpt iWcNotRpt) {
		return fetch(wcNotRptCrsr, iWcNotRpt, fetchWcNotRptCrsrRm);
	}

	public DbAccessStatus openWcNotRptCrsr(String aCurrentTimestamp, String dStartTimestamp) {
		wcNotRptCrsr = buildQuery("openWcNotRptCrsr").bind("aCurrentTimestamp", aCurrentTimestamp).bind("dStartTimestamp", dStartTimestamp)
				.withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeWcNotRptCrsr() {
		return closeCursor(wcNotRptCrsr);
	}

	public IWcNotRpt selectRec(String polNbr, String polEffDt, String staMdfTs, IWcNotRpt iWcNotRpt) {
		return buildQuery("selectRec").bind("polNbr", polNbr).bind("polEffDt", polEffDt).bind("staMdfTs", staMdfTs).singleResult(iWcNotRpt);
	}

	public String selectRec1(String polNbr, String polEffDt, String staMdfTs, String dft) {
		return buildQuery("selectRec1").bind("polNbr", polNbr).bind("polEffDt", polEffDt).bind("staMdfTs", staMdfTs).scalarResultString(dft);
	}
}
