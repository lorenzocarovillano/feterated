/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.federatedinsurance.crs.ws.enums.WsErrorFlag;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALRLEN1<br>
 * Generated as a class for rule WS.<br>*/
public class Halrlen1Data {

	//==== PROPERTIES ====
	//Original name: WS-ERROR-FLAG
	private WsErrorFlag wsErrorFlag = new WsErrorFlag();
	//Original name: WS-STRING-LEN
	private short wsStringLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-POSN
	private short wsPosn = DefaultValues.BIN_SHORT_VAL;
	//Original name: STRING-X-IND
	private int stringXInd = 1;

	//==== METHODS ====
	public void setWsStringLen(short wsStringLen) {
		this.wsStringLen = wsStringLen;
	}

	public short getWsStringLen() {
		return this.wsStringLen;
	}

	public void setWsPosn(short wsPosn) {
		this.wsPosn = wsPosn;
	}

	public short getWsPosn() {
		return this.wsPosn;
	}

	public void setStringXInd(int stringXInd) {
		this.stringXInd = stringXInd;
	}

	public int getStringXInd() {
		return this.stringXInd;
	}

	public WsErrorFlag getWsErrorFlag() {
		return wsErrorFlag;
	}
}
