/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: FNC02I-MATCH-STRING-ARRAY<br>
 * Variables: FNC02I-MATCH-STRING-ARRAY from copybook FNC020C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Fnc02iMatchStringArray {

	//==== PROPERTIES ====
	//Original name: FNC02I-INPUT-STRING
	private String stringFld = DefaultValues.stringVal(Len.STRING_FLD);
	/**Original name: FNC02I-INPUT-MATCH-TYPE<br>
	 * <pre>***************************************************************
	 *  NOTE: AT THIS TIME, THE FOLLOWING MATCH TYPES ARE AVAILABLE:
	 *                       PERSONAL_NAME
	 *                       COMPANY_NAME
	 *                       CITY
	 *                       ID_NUMBER
	 * ***************************************************************</pre>*/
	private String matchType = DefaultValues.stringVal(Len.MATCH_TYPE);

	//==== METHODS ====
	public void setiMatchStringArrayBytes(byte[] buffer, int offset) {
		int position = offset;
		stringFld = MarshalByte.readString(buffer, position, Len.STRING_FLD);
		position += Len.STRING_FLD;
		matchType = MarshalByte.readString(buffer, position, Len.MATCH_TYPE);
	}

	public byte[] getiMatchStringArrayBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, stringFld, Len.STRING_FLD);
		position += Len.STRING_FLD;
		MarshalByte.writeString(buffer, position, matchType, Len.MATCH_TYPE);
		return buffer;
	}

	public void initiMatchStringArraySpaces() {
		stringFld = "";
		matchType = "";
	}

	public void setStringFld(String stringFld) {
		this.stringFld = Functions.subString(stringFld, Len.STRING_FLD);
	}

	public String getStringFld() {
		return this.stringFld;
	}

	public void setMatchType(String matchType) {
		this.matchType = Functions.subString(matchType, Len.MATCH_TYPE);
	}

	public String getMatchType() {
		return this.matchType;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STRING_FLD = 256;
		public static final int MATCH_TYPE = 30;
		public static final int I_MATCH_STRING_ARRAY = STRING_FLD + MATCH_TYPE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
