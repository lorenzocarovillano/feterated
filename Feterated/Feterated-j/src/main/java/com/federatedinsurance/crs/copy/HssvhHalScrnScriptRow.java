/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IHalScrnScriptV;

/**Original name: HSSVH-HAL-SCRN-SCRIPT-ROW<br>
 * Variable: HSSVH-HAL-SCRN-SCRIPT-ROW from copybook HALLHSSV<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class HssvhHalScrnScriptRow implements IHalScrnScriptV {

	//==== PROPERTIES ====
	//Original name: HSSVH-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HSSVH-SEC-GRP-NM
	private String secGrpNm = DefaultValues.stringVal(Len.SEC_GRP_NM);
	//Original name: HSSVH-HSSV-CONTEXT
	private String hssvContext = DefaultValues.stringVal(Len.HSSV_CONTEXT);
	//Original name: HSSVH-HSSV-SCRN-FLD-ID
	private String hssvScrnFldId = DefaultValues.stringVal(Len.HSSV_SCRN_FLD_ID);
	//Original name: HSSVH-HSSV-SEQ-NBR
	private int hssvSeqNbr = DefaultValues.BIN_INT_VAL;
	//Original name: HSSVH-HSSV-NEW-LIN-IND
	private char hssvNewLinInd = DefaultValues.CHAR_VAL;
	//Original name: HSSVH-HSSV-SCRIPT-TXT-LEN
	private short hssvScriptTxtLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: HSSVH-HSSV-SCRIPT-TXT-TEXT
	private String hssvScriptTxtText = DefaultValues.stringVal(Len.HSSV_SCRIPT_TXT_TEXT);

	//==== METHODS ====
	@Override
	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	@Override
	public String getUowNm() {
		return this.uowNm;
	}

	public String getUowNmFormatted() {
		return Functions.padBlanks(getUowNm(), Len.UOW_NM);
	}

	@Override
	public void setSecGrpNm(String secGrpNm) {
		this.secGrpNm = Functions.subString(secGrpNm, Len.SEC_GRP_NM);
	}

	@Override
	public String getSecGrpNm() {
		return this.secGrpNm;
	}

	public String getSecGrpNmFormatted() {
		return Functions.padBlanks(getSecGrpNm(), Len.SEC_GRP_NM);
	}

	@Override
	public void setHssvContext(String hssvContext) {
		this.hssvContext = Functions.subString(hssvContext, Len.HSSV_CONTEXT);
	}

	@Override
	public String getHssvContext() {
		return this.hssvContext;
	}

	public String getHssvContextFormatted() {
		return Functions.padBlanks(getHssvContext(), Len.HSSV_CONTEXT);
	}

	@Override
	public void setHssvScrnFldId(String hssvScrnFldId) {
		this.hssvScrnFldId = Functions.subString(hssvScrnFldId, Len.HSSV_SCRN_FLD_ID);
	}

	@Override
	public String getHssvScrnFldId() {
		return this.hssvScrnFldId;
	}

	@Override
	public void setHssvSeqNbr(int hssvSeqNbr) {
		this.hssvSeqNbr = hssvSeqNbr;
	}

	@Override
	public int getHssvSeqNbr() {
		return this.hssvSeqNbr;
	}

	@Override
	public void setHssvNewLinInd(char hssvNewLinInd) {
		this.hssvNewLinInd = hssvNewLinInd;
	}

	@Override
	public char getHssvNewLinInd() {
		return this.hssvNewLinInd;
	}

	public void setHssvScriptTxtLen(short hssvScriptTxtLen) {
		this.hssvScriptTxtLen = hssvScriptTxtLen;
	}

	public short getHssvScriptTxtLen() {
		return this.hssvScriptTxtLen;
	}

	public void setHssvScriptTxtText(String hssvScriptTxtText) {
		this.hssvScriptTxtText = Functions.subString(hssvScriptTxtText, Len.HSSV_SCRIPT_TXT_TEXT);
	}

	public String getHssvScriptTxtText() {
		return this.hssvScriptTxtText;
	}

	public String getHssvScriptTxtTextFormatted() {
		return Functions.padBlanks(getHssvScriptTxtText(), Len.HSSV_SCRIPT_TXT_TEXT);
	}

	@Override
	public String getHssvScriptTxt() {
		return FixedStrings.get(getHssvScriptTxtText(), getHssvScriptTxtLen());
	}

	@Override
	public void setHssvScriptTxt(String hssvScriptTxt) {
		this.setHssvScriptTxtText(hssvScriptTxt);
		this.setHssvScriptTxtLen((((short) strLen(hssvScriptTxt))));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NM = 32;
		public static final int SEC_GRP_NM = 8;
		public static final int HSSV_CONTEXT = 20;
		public static final int HSSV_SCRN_FLD_ID = 32;
		public static final int HSSV_SCRIPT_TXT_TEXT = 100;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
