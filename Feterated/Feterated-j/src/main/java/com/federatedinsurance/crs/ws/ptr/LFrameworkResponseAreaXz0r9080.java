/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9080<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9080 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9080_MAXOCCURS = 1000;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9080() {
	}

	public LFrameworkResponseAreaXz0r9080(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a9080Formatted(int lFwRespXz0a9080Idx) {
		int position = Pos.lFwRespXz0a9080(lFwRespXz0a9080Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9080);
	}

	public void setlFwRespXz0a9080Bytes(int lFwRespXz0a9080Idx, byte[] buffer) {
		setlFwRespXz0a9080Bytes(lFwRespXz0a9080Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A9080<br>*/
	public byte[] getlFwRespXz0a9080Bytes(int lFwRespXz0a9080Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_XZ0A9080];
		return getlFwRespXz0a9080Bytes(lFwRespXz0a9080Idx, buffer, 1);
	}

	public void setlFwRespXz0a9080Bytes(int lFwRespXz0a9080Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9080(lFwRespXz0a9080Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A9080, position);
	}

	public byte[] getlFwRespXz0a9080Bytes(int lFwRespXz0a9080Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9080(lFwRespXz0a9080Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A9080, position);
		return buffer;
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9080(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9080;
		}

		public static int xza980TtyInfoRow(int idx) {
			return lFwRespXz0a9080(idx);
		}

		public static int xza980MaxTtyRows(int idx) {
			return xza980TtyInfoRow(idx);
		}

		public static int xza980CsrActNbr(int idx) {
			return xza980MaxTtyRows(idx) + Len.XZA980_MAX_TTY_ROWS;
		}

		public static int xza980NotPrcTs(int idx) {
			return xza980CsrActNbr(idx) + Len.XZA980_CSR_ACT_NBR;
		}

		public static int xza980Userid(int idx) {
			return xza980NotPrcTs(idx) + Len.XZA980_NOT_PRC_TS;
		}

		public static int xza980TtyList(int idx) {
			return xza980Userid(idx) + Len.XZA980_USERID;
		}

		public static int xza980ClientId(int idx) {
			return xza980TtyList(idx);
		}

		public static int xza980AdrId(int idx) {
			return xza980ClientId(idx) + Len.XZA980_CLIENT_ID;
		}

		public static int xza980RecTypCd(int idx) {
			return xza980AdrId(idx) + Len.XZA980_ADR_ID;
		}

		public static int xza980RecTypDes(int idx) {
			return xza980RecTypCd(idx) + Len.XZA980_REC_TYP_CD;
		}

		public static int xza980Name(int idx) {
			return xza980RecTypDes(idx) + Len.XZA980_REC_TYP_DES;
		}

		public static int xza980AdrLin1(int idx) {
			return xza980Name(idx) + Len.XZA980_NAME;
		}

		public static int xza980AdrLin2(int idx) {
			return xza980AdrLin1(idx) + Len.XZA980_ADR_LIN1;
		}

		public static int xza980CityNm(int idx) {
			return xza980AdrLin2(idx) + Len.XZA980_ADR_LIN2;
		}

		public static int xza980StateAbb(int idx) {
			return xza980CityNm(idx) + Len.XZA980_CITY_NM;
		}

		public static int xza980PstCd(int idx) {
			return xza980StateAbb(idx) + Len.XZA980_STATE_ABB;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA980_MAX_TTY_ROWS = 2;
		public static final int XZA980_CSR_ACT_NBR = 9;
		public static final int XZA980_NOT_PRC_TS = 26;
		public static final int XZA980_USERID = 8;
		public static final int XZA980_CLIENT_ID = 64;
		public static final int XZA980_ADR_ID = 64;
		public static final int XZA980_REC_TYP_CD = 5;
		public static final int XZA980_REC_TYP_DES = 13;
		public static final int XZA980_NAME = 120;
		public static final int XZA980_ADR_LIN1 = 45;
		public static final int XZA980_ADR_LIN2 = 45;
		public static final int XZA980_CITY_NM = 30;
		public static final int XZA980_STATE_ABB = 2;
		public static final int XZA980_PST_CD = 13;
		public static final int XZA980_TTY_LIST = XZA980_CLIENT_ID + XZA980_ADR_ID + XZA980_REC_TYP_CD + XZA980_REC_TYP_DES + XZA980_NAME
				+ XZA980_ADR_LIN1 + XZA980_ADR_LIN2 + XZA980_CITY_NM + XZA980_STATE_ABB + XZA980_PST_CD;
		public static final int XZA980_TTY_INFO_ROW = XZA980_MAX_TTY_ROWS + XZA980_CSR_ACT_NBR + XZA980_NOT_PRC_TS + XZA980_USERID + XZA980_TTY_LIST;
		public static final int L_FW_RESP_XZ0A9080 = XZA980_TTY_INFO_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0r9080.L_FW_RESP_XZ0A9080_MAXOCCURS * L_FW_RESP_XZ0A9080;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
