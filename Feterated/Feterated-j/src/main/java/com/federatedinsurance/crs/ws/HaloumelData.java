/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalUowPrcSeq;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallmdrv;
import com.federatedinsurance.crs.copy.Hallpst;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.ws.enums.WsPrimaryBosSwitch;
import com.federatedinsurance.crs.ws.redefines.WsDeleteObject;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALOUMEL<br>
 * Generated as a class for rule WS.<br>*/
public class HaloumelData {

	//==== PROPERTIES ====
	/**Original name: WS-PRIMARY-BOS-SWITCH<br>
	 * <pre>* SWITCHES USED TO PROCESS THE REQUESTED UOW.</pre>*/
	private WsPrimaryBosSwitch wsPrimaryBosSwitch = new WsPrimaryBosSwitch();
	//Original name: WS-WORK-FIELDS
	private WsWorkFieldsHaloumel wsWorkFields = new WsWorkFieldsHaloumel();
	//Original name: WS-DELETE-OBJECT
	private WsDeleteObject wsDeleteObject = new WsDeleteObject();
	//Original name: OBJECT-NUM
	private int objectNum = 1;
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: HALLMDRV
	private Hallmdrv hallmdrv = new Hallmdrv();
	//Original name: HALLPST
	private Hallpst hallpst = new Hallpst();
	//Original name: WS-UBOC-MSG
	private Dfhcommarea wsUbocMsg = new Dfhcommarea();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: DCLHAL-UOW-PRC-SEQ
	private DclhalUowPrcSeq dclhalUowPrcSeq = new DclhalUowPrcSeq();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();

	//==== METHODS ====
	public void setObjectNum(int objectNum) {
		this.objectNum = objectNum;
	}

	public int getObjectNum() {
		return this.objectNum;
	}

	public void setWsUowProcessSeqInfoBytes(byte[] buffer) {
		setWsUowProcessSeqInfoBytes(buffer, 1);
	}

	public void setWsUowProcessSeqInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		hallpst.setUowPrcSeqBytes(buffer, position);
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalUowPrcSeq getDclhalUowPrcSeq() {
		return dclhalUowPrcSeq;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallmdrv getHallmdrv() {
		return hallmdrv;
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public WsDeleteObject getWsDeleteObject() {
		return wsDeleteObject;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsPrimaryBosSwitch getWsPrimaryBosSwitch() {
		return wsPrimaryBosSwitch;
	}

	public Dfhcommarea getWsUbocMsg() {
		return wsUbocMsg;
	}

	public WsWorkFieldsHaloumel getWsWorkFields() {
		return wsWorkFields;
	}
}
