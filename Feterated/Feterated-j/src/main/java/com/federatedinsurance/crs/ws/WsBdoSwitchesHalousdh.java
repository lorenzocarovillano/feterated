/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.WsEndOfCursor1Sw;
import com.federatedinsurance.crs.ws.enums.WsEndOfCursor2Sw;
import com.federatedinsurance.crs.ws.enums.WsFirstFetchSw;
import com.federatedinsurance.crs.ws.enums.WsMasterSwitchInd;
import com.federatedinsurance.crs.ws.enums.WsMatchingSwitchSw;
import com.federatedinsurance.crs.ws.enums.WsReadForMstrSwtInd;
import com.federatedinsurance.crs.ws.enums.WsSwitchesTsqSw;
import com.federatedinsurance.crs.ws.enums.WsSwitchesTypeSw;
import com.federatedinsurance.crs.ws.enums.WsUmtHdrSw;

/**Original name: WS-BDO-SWITCHES<br>
 * Variable: WS-BDO-SWITCHES from program HALOUSDH<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsBdoSwitchesHalousdh {

	//==== PROPERTIES ====
	//Original name: WS-MASTER-SWITCH-IND
	private WsMasterSwitchInd masterSwitchInd = new WsMasterSwitchInd();
	//Original name: WS-READ-FOR-MSTR-SWT-IND
	private WsReadForMstrSwtInd readForMstrSwtInd = new WsReadForMstrSwtInd();
	//Original name: WS-MATCHING-SWITCH-SW
	private WsMatchingSwitchSw matchingSwitchSw = new WsMatchingSwitchSw();
	//Original name: WS-END-OF-CURSOR1-SW
	private WsEndOfCursor1Sw endOfCursor1Sw = new WsEndOfCursor1Sw();
	//Original name: WS-END-OF-CURSOR2-SW
	private WsEndOfCursor2Sw endOfCursor2Sw = new WsEndOfCursor2Sw();
	//Original name: WS-UMT-HDR-SW
	private WsUmtHdrSw umtHdrSw = new WsUmtHdrSw();
	//Original name: WS-FIRST-FETCH-SW
	private WsFirstFetchSw firstFetchSw = new WsFirstFetchSw();
	//Original name: WS-SWITCHES-TSQ-SW
	private WsSwitchesTsqSw switchesTsqSw = new WsSwitchesTsqSw();
	//Original name: WS-SWITCHES-TYPE-SW
	private WsSwitchesTypeSw switchesTypeSw = new WsSwitchesTypeSw();

	//==== METHODS ====
	public WsEndOfCursor1Sw getEndOfCursor1Sw() {
		return endOfCursor1Sw;
	}

	public WsEndOfCursor2Sw getEndOfCursor2Sw() {
		return endOfCursor2Sw;
	}

	public WsFirstFetchSw getFirstFetchSw() {
		return firstFetchSw;
	}

	public WsMasterSwitchInd getMasterSwitchInd() {
		return masterSwitchInd;
	}

	public WsMatchingSwitchSw getMatchingSwitchSw() {
		return matchingSwitchSw;
	}

	public WsReadForMstrSwtInd getReadForMstrSwtInd() {
		return readForMstrSwtInd;
	}

	public WsSwitchesTsqSw getSwitchesTsqSw() {
		return switchesTsqSw;
	}

	public WsSwitchesTypeSw getSwitchesTypeSw() {
		return switchesTypeSw;
	}

	public WsUmtHdrSw getUmtHdrSw() {
		return umtHdrSw;
	}
}
