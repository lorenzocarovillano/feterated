/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPol;
import com.federatedinsurance.crs.copy.DclhalBoAudTgrV;
import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclhalUowObjHierV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Halluchs;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Halluidg;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xz0c0002;
import com.federatedinsurance.crs.copy.Xz0n0002;
import com.federatedinsurance.crs.copy.Xzc004ActNotPolFrmRow;
import com.federatedinsurance.crs.copy.Xzc005ActNotWrdRow;
import com.federatedinsurance.crs.copy.Xzc008ActNotPolRecRow;
import com.federatedinsurance.crs.copy.Xzh001ActNotRow;
import com.federatedinsurance.crs.copy.Xzh002ActNotPolRow;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.redefines.CidpTableInfo;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0D0002<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0d0002Data implements IActNotPol {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0d0002 constantFields = new ConstantFieldsXz0d0002();
	//Original name: EA-01-INV-POL-NBR
	private Ea01InvPolNbr ea01InvPolNbr = new Ea01InvPolNbr();
	//Original name: EA-02-INV-WF-STARTED-IND
	private Ea02InvWfStartedInd ea02InvWfStartedInd = new Ea02InvWfStartedInd();
	//Original name: WS-SPECIFIC-MISC
	private WsSpecificMiscXz0d0002 wsSpecificMisc = new WsSpecificMiscXz0d0002();
	//Original name: XZ0C0002
	private Xz0c0002 xz0c0002 = new Xz0c0002();
	//Original name: XZC004-ACT-NOT-POL-FRM-ROW
	private Xzc004ActNotPolFrmRow xzc004ActNotPolFrmRow = new Xzc004ActNotPolFrmRow();
	//Original name: XZC005-ACT-NOT-WRD-ROW
	private Xzc005ActNotWrdRow xzc005ActNotWrdRow = new Xzc005ActNotWrdRow();
	//Original name: XZC008-ACT-NOT-POL-REC-ROW
	private Xzc008ActNotPolRecRow xzc008ActNotPolRecRow = new Xzc008ActNotPolRecRow();
	//Original name: XZ0N0002
	private Xz0n0002 xz0n0002 = new Xz0n0002();
	//Original name: XZH002-ACT-NOT-POL-ROW
	private Xzh002ActNotPolRow xzh002ActNotPolRow = new Xzh002ActNotPolRow();
	//Original name: XZH001-ACT-NOT-ROW
	private Xzh001ActNotRow xzh001ActNotRow = new Xzh001ActNotRow();
	//Original name: WS-BDO-WORK-FIELDS
	private WsBdoWorkFields wsBdoWorkFields = new WsBdoWorkFields();
	//Original name: WS-BDO-SWITCHES
	private WsBdoSwitches wsBdoSwitches = new WsBdoSwitches();
	//Original name: DCLHAL-BO-AUD-TGR-V
	private DclhalBoAudTgrV dclhalBoAudTgrV = new DclhalBoAudTgrV();
	//Original name: HALLUCHS
	private Halluchs halluchs = new Halluchs();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();
	//Original name: HALRLODR-LOCK-DRIVER-STORAGE
	private WsHalrlodrLinkage halrlodrLockDriverStorage = new WsHalrlodrLinkage();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: HALLUSW
	private Hallusw hallusw = new Hallusw();
	//Original name: HALLUDAT
	private Halludat halludat = new Halludat();
	//Original name: HALLUHDR
	private Halluhdr halluhdr = new Halluhdr();
	//Original name: CIDP-TABLE-INFO
	private CidpTableInfo cidpTableInfo = new CidpTableInfo();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: HALRMON-PERF-MONITOR-STORAGE
	private HalrmonPerfMonitorStorage halrmonPerfMonitorStorage = new HalrmonPerfMonitorStorage();
	//Original name: DATE-STRUCTURE
	private DateStructureCawpcorc dateStructure = new DateStructureCawpcorc();
	//Original name: WS-SE3-CUR-ISO-DATE
	private String wsSe3CurIsoDate = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_DATE);
	//Original name: WS-SE3-CUR-ISO-TIME
	private String wsSe3CurIsoTime = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_TIME);
	//Original name: DCLHAL-UOW-OBJ-HIER-V
	private DclhalUowObjHierV dclhalUowObjHierV = new DclhalUowObjHierV();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public String getWsActNotPolFrmCopybookFormatted() {
		return xzc004ActNotPolFrmRow.getXzc004ActNotPolFrmRowFormatted();
	}

	public String getWsActNotWrdCommCopybookFormatted() {
		return xzc005ActNotWrdRow.getXzc005ActNotWrdRowFormatted();
	}

	public String getWsActNotPolRecCopybookFormatted() {
		return xzc008ActNotPolRecRow.getXzc008ActNotPolRecRowFormatted();
	}

	public void setHalouchsLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.HALOUCHS_LINKAGE);
		setHalouchsLinkageBytes(buffer, 1);
	}

	public String getHalouchsLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHalouchsLinkageBytes());
	}

	/**Original name: HALOUCHS-LINKAGE<br>*/
	public byte[] getHalouchsLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		return getHalouchsLinkageBytes(buffer, 1);
	}

	public void setHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluchs.setLength2(MarshalByte.readBinaryShort(buffer, position));
		position += Types.SHORT_SIZE;
		halluchs.setStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		halluchs.checkSum = MarshalByte.readFixedString(buffer, position, Halluchs.Len.CHECK_SUM);
	}

	public byte[] getHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, halluchs.getLength2());
		position += Types.SHORT_SIZE;
		halluchs.getStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		MarshalByte.writeString(buffer, position, halluchs.checkSum, Halluchs.Len.CHECK_SUM);
		return buffer;
	}

	/**Original name: WS-DATA-UMT-AREA<br>
	 * <pre>* DATA RESPONSE UMT AREA</pre>*/
	public byte[] getWsDataUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_DATA_UMT_AREA];
		return getWsDataUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsDataUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		halludat.getCommonBytes(buffer, position);
		return buffer;
	}

	public void initWsDataUmtAreaSpaces() {
		halludat.initHalludatSpaces();
	}

	public void initWsHdrUmtAreaSpaces() {
		halluhdr.initHalluhdrSpaces();
	}

	public void setWsDataPrivacyInfoFormatted(String data) {
		byte[] buffer = new byte[Len.WS_DATA_PRIVACY_INFO];
		MarshalByte.writeString(buffer, 1, data, Len.WS_DATA_PRIVACY_INFO);
		setWsDataPrivacyInfoBytes(buffer, 1);
	}

	public String getWsDataPrivacyInfoFormatted() {
		return getCidpPassedInfoFormatted();
	}

	public void setWsDataPrivacyInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setCidpPassedInfoBytes(buffer, position);
	}

	public String getCidpPassedInfoFormatted() {
		return MarshalByteExt.bufferToStr(getCidpPassedInfoBytes());
	}

	/**Original name: CIDP-PASSED-INFO<br>
	 * <pre>****************************************************************
	 *  HALLCIDP                                                      *
	 *  COMMON INTERFACE TO DATA PRIVACY                              *
	 *  USED BY BUSINESS DATA OBJECTS TO PASS DATA ROWS FOR           *
	 *  DATA PRIVACY CHECKING.                                        *
	 * ****************************************************************
	 *  SI#       PRGRMR  DATE       DESCRIPTION                      *
	 *  --------- ------- ---------- ---------------------------------*
	 *  SAVANNAH  PM      31MAY2000  INITIAL CODING                   *
	 *  14969     18448   25JUN2001  MADE CHANGES REQUIRED FOR        *
	 *                               ENHANCED DATA PRIVACY AND SET    *
	 *                               DEFAULTS PROCESSING.             *
	 *                               (ARCHITECTURE 2.3)               *
	 * ****************************************************************</pre>*/
	public byte[] getCidpPassedInfoBytes() {
		byte[] buffer = new byte[Len.CIDP_PASSED_INFO];
		return getCidpPassedInfoBytes(buffer, 1);
	}

	public void setCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.setCidpTableInfoBytes(buffer, position);
	}

	public byte[] getCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.getCidpTableInfoBytes(buffer, position);
		return buffer;
	}

	public void setWsSe3CurIsoDateTimeFormatted(String data) {
		byte[] buffer = new byte[Len.WS_SE3_CUR_ISO_DATE_TIME];
		MarshalByte.writeString(buffer, 1, data, Len.WS_SE3_CUR_ISO_DATE_TIME);
		setWsSe3CurIsoDateTimeBytes(buffer, 1);
	}

	public void setWsSe3CurIsoDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		wsSe3CurIsoDate = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_DATE);
		position += Len.WS_SE3_CUR_ISO_DATE;
		wsSe3CurIsoTime = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_TIME);
	}

	public void setWsSe3CurIsoDate(String wsSe3CurIsoDate) {
		this.wsSe3CurIsoDate = Functions.subString(wsSe3CurIsoDate, Len.WS_SE3_CUR_ISO_DATE);
	}

	public String getWsSe3CurIsoDate() {
		return this.wsSe3CurIsoDate;
	}

	public void setWsSe3CurIsoTime(String wsSe3CurIsoTime) {
		this.wsSe3CurIsoTime = Functions.subString(wsSe3CurIsoTime, Len.WS_SE3_CUR_ISO_TIME);
	}

	public String getWsSe3CurIsoTime() {
		return this.wsSe3CurIsoTime;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public CidpTableInfo getCidpTableInfo() {
		return cidpTableInfo;
	}

	public ConstantFieldsXz0d0002 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		return xzh002ActNotPolRow.getCsrActNbr();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.xzh002ActNotPolRow.setCsrActNbr(csrActNbr);
	}

	public DateStructureCawpcorc getDateStructure() {
		return dateStructure;
	}

	public DclhalBoAudTgrV getDclhalBoAudTgrV() {
		return dclhalBoAudTgrV;
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclhalUowObjHierV getDclhalUowObjHierV() {
		return dclhalUowObjHierV;
	}

	public Ea01InvPolNbr getEa01InvPolNbr() {
		return ea01InvPolNbr;
	}

	public Ea02InvWfStartedInd getEa02InvWfStartedInd() {
		return ea02InvWfStartedInd;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Halluchs getHalluchs() {
		return halluchs;
	}

	public Halludat getHalludat() {
		return halludat;
	}

	public Halluhdr getHalluhdr() {
		return halluhdr;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	public WsHalrlodrLinkage getHalrlodrLockDriverStorage() {
		return halrlodrLockDriverStorage;
	}

	public HalrmonPerfMonitorStorage getHalrmonPerfMonitorStorage() {
		return halrmonPerfMonitorStorage;
	}

	@Override
	public String getNinAdrId() {
		return xzh002ActNotPolRow.getNinAdrId();
	}

	@Override
	public void setNinAdrId(String ninAdrId) {
		this.xzh002ActNotPolRow.setNinAdrId(ninAdrId);
	}

	@Override
	public String getNinCltId() {
		return xzh002ActNotPolRow.getNinCltId();
	}

	@Override
	public void setNinCltId(String ninCltId) {
		this.xzh002ActNotPolRow.setNinCltId(ninCltId);
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotEffDt() {
		return xzh002ActNotPolRow.getNotEffDt();
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		this.xzh002ActNotPolRow.setNotEffDt(notEffDt);
	}

	@Override
	public String getNotEffDtObj() {
		if (xzh002ActNotPolRow.getNotEffDtNi() >= 0) {
			return getNotEffDt();
		} else {
			return null;
		}
	}

	@Override
	public void setNotEffDtObj(String notEffDtObj) {
		if (notEffDtObj != null) {
			setNotEffDt(notEffDtObj);
			xzh002ActNotPolRow.setNotEffDtNi(((short) 0));
		} else {
			xzh002ActNotPolRow.setNotEffDtNi(((short) -1));
		}
	}

	@Override
	public String getNotPrcTs() {
		return xzh002ActNotPolRow.getNotPrcTs();
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.xzh002ActNotPolRow.setNotPrcTs(notPrcTs);
	}

	@Override
	public char getPolBilStaCd() {
		return xzh002ActNotPolRow.getPolBilStaCd();
	}

	@Override
	public void setPolBilStaCd(char polBilStaCd) {
		this.xzh002ActNotPolRow.setPolBilStaCd(polBilStaCd);
	}

	@Override
	public Character getPolBilStaCdObj() {
		if (xzh002ActNotPolRow.getPolBilStaCdNi() >= 0) {
			return (getPolBilStaCd());
		} else {
			return null;
		}
	}

	@Override
	public void setPolBilStaCdObj(Character polBilStaCdObj) {
		if (polBilStaCdObj != null) {
			setPolBilStaCd((polBilStaCdObj));
			xzh002ActNotPolRow.setPolBilStaCdNi(((short) 0));
		} else {
			xzh002ActNotPolRow.setPolBilStaCdNi(((short) -1));
		}
	}

	@Override
	public AfDecimal getPolDueAmt() {
		return xzh002ActNotPolRow.getPolDueAmt();
	}

	@Override
	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.xzh002ActNotPolRow.setPolDueAmt(polDueAmt.copy());
	}

	@Override
	public AfDecimal getPolDueAmtObj() {
		if (xzh002ActNotPolRow.getPolDueAmtNi() >= 0) {
			return getPolDueAmt().toString();
		} else {
			return null;
		}
	}

	@Override
	public void setPolDueAmtObj(AfDecimal polDueAmtObj) {
		if (polDueAmtObj != null) {
			setPolDueAmt(new AfDecimal(polDueAmtObj, 10, 2));
			xzh002ActNotPolRow.setPolDueAmtNi(((short) 0));
		} else {
			xzh002ActNotPolRow.setPolDueAmtNi(((short) -1));
		}
	}

	@Override
	public String getPolEffDt() {
		return xzh002ActNotPolRow.getPolEffDt();
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		this.xzh002ActNotPolRow.setPolEffDt(polEffDt);
	}

	@Override
	public String getPolExpDt() {
		return xzh002ActNotPolRow.getPolExpDt();
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		this.xzh002ActNotPolRow.setPolExpDt(polExpDt);
	}

	@Override
	public String getPolNbr() {
		return xzh002ActNotPolRow.getPolNbr();
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.xzh002ActNotPolRow.setPolNbr(polNbr);
	}

	@Override
	public String getPolPriRskStAbb() {
		return xzh002ActNotPolRow.getPolPriRskStAbb();
	}

	@Override
	public void setPolPriRskStAbb(String polPriRskStAbb) {
		this.xzh002ActNotPolRow.setPolPriRskStAbb(polPriRskStAbb);
	}

	@Override
	public String getPolTypCd() {
		return xzh002ActNotPolRow.getPolTypCd();
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		this.xzh002ActNotPolRow.setPolTypCd(polTypCd);
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	@Override
	public char getWfStartedInd() {
		return xzh002ActNotPolRow.getWfStartedInd();
	}

	@Override
	public void setWfStartedInd(char wfStartedInd) {
		this.xzh002ActNotPolRow.setWfStartedInd(wfStartedInd);
	}

	@Override
	public Character getWfStartedIndObj() {
		if (xzh002ActNotPolRow.getWfStartedIndNi() >= 0) {
			return (getWfStartedInd());
		} else {
			return null;
		}
	}

	@Override
	public void setWfStartedIndObj(Character wfStartedIndObj) {
		if (wfStartedIndObj != null) {
			setWfStartedInd((wfStartedIndObj));
			xzh002ActNotPolRow.setWfStartedIndNi(((short) 0));
		} else {
			xzh002ActNotPolRow.setWfStartedIndNi(((short) -1));
		}
	}

	public WsBdoSwitches getWsBdoSwitches() {
		return wsBdoSwitches;
	}

	public WsBdoWorkFields getWsBdoWorkFields() {
		return wsBdoWorkFields;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSpecificMiscXz0d0002 getWsSpecificMisc() {
		return wsSpecificMisc;
	}

	public Xz0c0002 getXz0c0002() {
		return xz0c0002;
	}

	public Xz0n0002 getXz0n0002() {
		return xz0n0002;
	}

	public Xzc004ActNotPolFrmRow getXzc004ActNotPolFrmRow() {
		return xzc004ActNotPolFrmRow;
	}

	public Xzc005ActNotWrdRow getXzc005ActNotWrdRow() {
		return xzc005ActNotWrdRow;
	}

	public Xzc008ActNotPolRecRow getXzc008ActNotPolRecRow() {
		return xzc008ActNotPolRecRow;
	}

	public Xzh001ActNotRow getXzh001ActNotRow() {
		return xzh001ActNotRow;
	}

	public Xzh002ActNotPolRow getXzh002ActNotPolRow() {
		return xzh002ActNotPolRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_SE3_CUR_ISO_DATE_TIME = WS_SE3_CUR_ISO_DATE + WS_SE3_CUR_ISO_TIME;
		public static final int WS_DATA_UMT_AREA = Halludat.Len.COMMON;
		public static final int CIDP_PASSED_INFO = CidpTableInfo.Len.CIDP_TABLE_INFO;
		public static final int WS_DATA_PRIVACY_INFO = CIDP_PASSED_INFO;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;
		public static final int HALOUCHS_LINKAGE = Halluchs.Len.LENGTH2 + Halluchs.Len.STRING_FLD + Halluchs.Len.CHECK_SUM;
		public static final int WS_ACT_NOT_WRD_COMM_COPYBOOK = Xzc005ActNotWrdRow.Len.XZC005_ACT_NOT_WRD_ROW;
		public static final int WS_ACT_NOT_POL_FRM_COPYBOOK = Xzc004ActNotPolFrmRow.Len.XZC004_ACT_NOT_POL_FRM_ROW;
		public static final int WS_ACT_NOT_POL_REC_COPYBOOK = Xzc008ActNotPolRecRow.Len.XZC008_ACT_NOT_POL_REC_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
