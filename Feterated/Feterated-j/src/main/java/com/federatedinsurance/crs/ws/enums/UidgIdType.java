/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: UIDG-ID-TYPE<br>
 * Variable: UIDG-ID-TYPE from copybook HALLUIDG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UidgIdType {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.UIDG_ID_TYPE);
	public static final String ADDRESS_ID = "A1";
	public static final String AGENCY_ID = "A2";
	public static final String DSBBATCH_ID = "B1";
	public static final String COMMENT_ID = "C1";
	public static final String CONTRACT_ID = "C2";
	public static final String DWELLING_ID = "D1";
	public static final String ERROR_ID = "E1";
	public static final String RANDOM_GLOBAL_ID = "G1";
	public static final String CLIENT_ID = "K1";
	public static final String CLAIM_ID = "L1";
	public static final String POLICY_ID = "P1";
	public static final String PRODUCER_ID = "P2";
	public static final String SCHEME_ID = "S1";
	public static final String VEHICLE_ID = "V1";
	public static final String WIP_DIARY_ID = "W1";

	//==== METHODS ====
	public void setUidgIdType(String uidgIdType) {
		this.value = Functions.subString(uidgIdType, Len.UIDG_ID_TYPE);
	}

	public String getUidgIdType() {
		return this.value;
	}

	public String getUidgIdTypeFormatted() {
		return Functions.padBlanks(getUidgIdType(), Len.UIDG_ID_TYPE);
	}

	public boolean isErrorId() {
		return value.equals(ERROR_ID);
	}

	public void setUidgErrorId() {
		value = ERROR_ID;
	}

	public void setUidgRandomGlobalId() {
		value = RANDOM_GLOBAL_ID;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UIDG_ID_TYPE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
