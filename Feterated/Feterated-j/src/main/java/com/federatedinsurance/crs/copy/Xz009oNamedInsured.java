/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ009O-NAMED-INSURED<br>
 * Variable: XZ009O-NAMED-INSURED from copybook XZC090CO<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xz009oNamedInsured {

	//==== PROPERTIES ====
	//Original name: XZ009O-CTC-ID
	private String ctcId = DefaultValues.stringVal(Len.XZC09O_CTC_ID);
	//Original name: XZ009O-DSY-NM
	private String dsyNm = DefaultValues.stringVal(Len.XZC09O_DSY_NM);
	//Original name: XZ009O-ADR-ID
	private String adrId = DefaultValues.stringVal(Len.XZC09O_ADR_ID);
	//Original name: XZ009O-ADR-1
	private String adr1 = DefaultValues.stringVal(Len.XZC09O_ADR1);
	//Original name: XZ009O-ADR-2
	private String adr2 = DefaultValues.stringVal(Len.XZC09O_ADR2);
	//Original name: XZ009O-CIT-NM
	private String citNm = DefaultValues.stringVal(Len.XZC09O_CIT_NM);
	//Original name: XZ009O-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.XZC09O_ST_ABB);
	//Original name: XZ009O-PST-CD
	private String pstCd = DefaultValues.stringVal(Len.XZC09O_PST_CD);
	//Original name: XZ009O-CTY-NM
	private String ctyNm = DefaultValues.stringVal(Len.XZC09O_CTY_NM);

	//==== METHODS ====
	public void setNamedInsuredBytes(byte[] buffer, int offset) {
		int position = offset;
		ctcId = MarshalByte.readString(buffer, position, Len.XZC09O_CTC_ID);
		position += Len.XZC09O_CTC_ID;
		dsyNm = MarshalByte.readString(buffer, position, Len.XZC09O_DSY_NM);
		position += Len.XZC09O_DSY_NM;
		adrId = MarshalByte.readString(buffer, position, Len.XZC09O_ADR_ID);
		position += Len.XZC09O_ADR_ID;
		adr1 = MarshalByte.readString(buffer, position, Len.XZC09O_ADR1);
		position += Len.XZC09O_ADR1;
		adr2 = MarshalByte.readString(buffer, position, Len.XZC09O_ADR2);
		position += Len.XZC09O_ADR2;
		citNm = MarshalByte.readString(buffer, position, Len.XZC09O_CIT_NM);
		position += Len.XZC09O_CIT_NM;
		stAbb = MarshalByte.readString(buffer, position, Len.XZC09O_ST_ABB);
		position += Len.XZC09O_ST_ABB;
		pstCd = MarshalByte.readString(buffer, position, Len.XZC09O_PST_CD);
		position += Len.XZC09O_PST_CD;
		ctyNm = MarshalByte.readString(buffer, position, Len.XZC09O_CTY_NM);
	}

	public byte[] getNamedInsuredBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ctcId, Len.XZC09O_CTC_ID);
		position += Len.XZC09O_CTC_ID;
		MarshalByte.writeString(buffer, position, dsyNm, Len.XZC09O_DSY_NM);
		position += Len.XZC09O_DSY_NM;
		MarshalByte.writeString(buffer, position, adrId, Len.XZC09O_ADR_ID);
		position += Len.XZC09O_ADR_ID;
		MarshalByte.writeString(buffer, position, adr1, Len.XZC09O_ADR1);
		position += Len.XZC09O_ADR1;
		MarshalByte.writeString(buffer, position, adr2, Len.XZC09O_ADR2);
		position += Len.XZC09O_ADR2;
		MarshalByte.writeString(buffer, position, citNm, Len.XZC09O_CIT_NM);
		position += Len.XZC09O_CIT_NM;
		MarshalByte.writeString(buffer, position, stAbb, Len.XZC09O_ST_ABB);
		position += Len.XZC09O_ST_ABB;
		MarshalByte.writeString(buffer, position, pstCd, Len.XZC09O_PST_CD);
		position += Len.XZC09O_PST_CD;
		MarshalByte.writeString(buffer, position, ctyNm, Len.XZC09O_CTY_NM);
		return buffer;
	}

	public void setXzc09oCtcId(String xzc09oCtcId) {
		this.ctcId = Functions.subString(xzc09oCtcId, Len.XZC09O_CTC_ID);
	}

	public String getXzc09oCtcId() {
		return this.ctcId;
	}

	public void setXzc09oDsyNm(String xzc09oDsyNm) {
		this.dsyNm = Functions.subString(xzc09oDsyNm, Len.XZC09O_DSY_NM);
	}

	public String getXzc09oDsyNm() {
		return this.dsyNm;
	}

	public void setXzc09oAdrId(String xzc09oAdrId) {
		this.adrId = Functions.subString(xzc09oAdrId, Len.XZC09O_ADR_ID);
	}

	public String getXzc09oAdrId() {
		return this.adrId;
	}

	public void setXzc09oAdr1(String xzc09oAdr1) {
		this.adr1 = Functions.subString(xzc09oAdr1, Len.XZC09O_ADR1);
	}

	public String getXzc09oAdr1() {
		return this.adr1;
	}

	public void setXzc09oAdr2(String xzc09oAdr2) {
		this.adr2 = Functions.subString(xzc09oAdr2, Len.XZC09O_ADR2);
	}

	public String getXzc09oAdr2() {
		return this.adr2;
	}

	public void setXzc09oCitNm(String xzc09oCitNm) {
		this.citNm = Functions.subString(xzc09oCitNm, Len.XZC09O_CIT_NM);
	}

	public String getXzc09oCitNm() {
		return this.citNm;
	}

	public void setXzc09oStAbb(String xzc09oStAbb) {
		this.stAbb = Functions.subString(xzc09oStAbb, Len.XZC09O_ST_ABB);
	}

	public String getXzc09oStAbb() {
		return this.stAbb;
	}

	public void setXzc09oPstCd(String xzc09oPstCd) {
		this.pstCd = Functions.subString(xzc09oPstCd, Len.XZC09O_PST_CD);
	}

	public String getXzc09oPstCd() {
		return this.pstCd;
	}

	public void setXzc09oCtyNm(String xzc09oCtyNm) {
		this.ctyNm = Functions.subString(xzc09oCtyNm, Len.XZC09O_CTY_NM);
	}

	public String getXzc09oCtyNm() {
		return this.ctyNm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC09O_CTC_ID = 64;
		public static final int XZC09O_DSY_NM = 120;
		public static final int XZC09O_ADR_ID = 64;
		public static final int XZC09O_ADR1 = 45;
		public static final int XZC09O_ADR2 = 45;
		public static final int XZC09O_CIT_NM = 30;
		public static final int XZC09O_ST_ABB = 3;
		public static final int XZC09O_PST_CD = 13;
		public static final int XZC09O_CTY_NM = 30;
		public static final int NAMED_INSURED = XZC09O_CTC_ID + XZC09O_DSY_NM + XZC09O_ADR_ID + XZC09O_ADR1 + XZC09O_ADR2 + XZC09O_CIT_NM
				+ XZC09O_ST_ABB + XZC09O_PST_CD + XZC09O_CTY_NM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
