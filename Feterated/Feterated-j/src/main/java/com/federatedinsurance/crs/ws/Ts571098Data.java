/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS571098<br>
 * Generated as a class for rule WS.<br>*/
public class Ts571098Data {

	//==== PROPERTIES ====
	//Original name: EA-01-TSQ-ERROR
	private Ea01TsqError ea01TsqError = new Ea01TsqError();
	//Original name: FILLER-EA-02-EMPTY-TSQ
	private String flr1 = "TEMP QUEUE";
	//Original name: FILLER-EA-02-EMPTY-TSQ-1
	private String flr2 = "CONTAINS NO";
	//Original name: FILLER-EA-02-EMPTY-TSQ-2
	private String flr3 = "CREDENTIALS.";
	//Original name: SA-RESP-CODE
	private int saRespCode = DefaultValues.BIN_INT_VAL;
	//Original name: SA-RESP2-CODE
	private int saResp2Code = DefaultValues.BIN_INT_VAL;
	//Original name: SA-ITEMNUM
	private int saItemnum = 1;
	//Original name: SA-NUM-ITEMS
	private int saNumItems = 0;

	//==== METHODS ====
	public String getEa02EmptyTsqFormatted() {
		return MarshalByteExt.bufferToStr(getEa02EmptyTsqBytes());
	}

	/**Original name: EA-02-EMPTY-TSQ<br>*/
	public byte[] getEa02EmptyTsqBytes() {
		byte[] buffer = new byte[Len.EA02_EMPTY_TSQ];
		return getEa02EmptyTsqBytes(buffer, 1);
	}

	public byte[] getEa02EmptyTsqBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setSaRespCode(int saRespCode) {
		this.saRespCode = saRespCode;
	}

	public int getSaRespCode() {
		return this.saRespCode;
	}

	public String getSaRespCodeFormatted() {
		return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.BINARY)).format(getSaRespCode()).toString();
	}

	public void setSaResp2Code(int saResp2Code) {
		this.saResp2Code = saResp2Code;
	}

	public int getSaResp2Code() {
		return this.saResp2Code;
	}

	public String getSaResp2CodeFormatted() {
		return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.BINARY)).format(getSaResp2Code()).toString();
	}

	public int getSaItemnum() {
		return this.saItemnum;
	}

	public void setSaNumItems(int saNumItems) {
		this.saNumItems = saNumItems;
	}

	public int getSaNumItems() {
		return this.saNumItems;
	}

	public Ea01TsqError getEa01TsqError() {
		return ea01TsqError;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 12;
		public static final int FLR3 = 13;
		public static final int EA02_EMPTY_TSQ = FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
