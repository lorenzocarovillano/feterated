/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLFRM-PRC-TYP<br>
 * Variable: DCLFRM-PRC-TYP from copybook XZH00025<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclfrmPrcTyp {

	//==== PROPERTIES ====
	//Original name: FRM-NBR
	private String frmNbr = DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: FRM-EDT-DT
	private String frmEdtDt = DefaultValues.stringVal(Len.FRM_EDT_DT);
	//Original name: ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: EDL-FRM-NM
	private String edlFrmNm = DefaultValues.stringVal(Len.EDL_FRM_NM);
	//Original name: FRM-DES
	private String frmDes = DefaultValues.stringVal(Len.FRM_DES);
	//Original name: SPE-PRC-CD
	private String spePrcCd = DefaultValues.stringVal(Len.SPE_PRC_CD);
	//Original name: DTN-CD
	private short dtnCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setFrmNbr(String frmNbr) {
		this.frmNbr = Functions.subString(frmNbr, Len.FRM_NBR);
	}

	public String getFrmNbr() {
		return this.frmNbr;
	}

	public void setFrmEdtDt(String frmEdtDt) {
		this.frmEdtDt = Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	public String getFrmEdtDt() {
		return this.frmEdtDt;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setEdlFrmNm(String edlFrmNm) {
		this.edlFrmNm = Functions.subString(edlFrmNm, Len.EDL_FRM_NM);
	}

	public String getEdlFrmNm() {
		return this.edlFrmNm;
	}

	public void setFrmDes(String frmDes) {
		this.frmDes = Functions.subString(frmDes, Len.FRM_DES);
	}

	public String getFrmDes() {
		return this.frmDes;
	}

	public void setSpePrcCd(String spePrcCd) {
		this.spePrcCd = Functions.subString(spePrcCd, Len.SPE_PRC_CD);
	}

	public String getSpePrcCd() {
		return this.spePrcCd;
	}

	public void setDtnCd(short dtnCd) {
		this.dtnCd = dtnCd;
	}

	public short getDtnCd() {
		return this.dtnCd;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FRM_NBR = 30;
		public static final int FRM_EDT_DT = 10;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int EDL_FRM_NM = 30;
		public static final int FRM_DES = 20;
		public static final int SPE_PRC_CD = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
