/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0U8010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0u8010 {

	//==== PROPERTIES ====
	//Original name: CF-YES
	private char yes = 'Y';
	//Original name: CF-NO
	private char no = 'N';
	//Original name: CF-PN-2100
	private String pn2100 = "2100";

	//==== METHODS ====
	public char getYes() {
		return this.yes;
	}

	public char getNo() {
		return this.no;
	}

	public String getPn2100() {
		return this.pn2100;
	}
}
