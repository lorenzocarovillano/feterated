/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program CAWPCORC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesCawpcorc {

	//==== PROPERTIES ====
	//Original name: SW-INPUT-FROM-UMT-FLAG
	private boolean inputFromUmtFlag = false;
	//Original name: SW-INPUT-FROM-LINKAGE-FLAG
	private boolean inputFromLinkageFlag = false;
	//Original name: SW-RETURN-CLIENT-FLAG
	private boolean returnClientFlag = false;

	//==== METHODS ====
	public void setInputFromUmtFlag(boolean inputFromUmtFlag) {
		this.inputFromUmtFlag = inputFromUmtFlag;
	}

	public boolean isInputFromUmtFlag() {
		return this.inputFromUmtFlag;
	}

	public void setInputFromLinkageFlag(boolean inputFromLinkageFlag) {
		this.inputFromLinkageFlag = inputFromLinkageFlag;
	}

	public boolean isInputFromLinkageFlag() {
		return this.inputFromLinkageFlag;
	}

	public void setReturnClientFlag(boolean returnClientFlag) {
		this.returnClientFlag = returnClientFlag;
	}

	public boolean isReturnClientFlag() {
		return this.returnClientFlag;
	}
}
