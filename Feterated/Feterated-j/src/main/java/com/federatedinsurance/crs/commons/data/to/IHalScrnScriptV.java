/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HAL_SCRN_SCRIPT_V]
 * 
 */
public interface IHalScrnScriptV extends BaseSqlTo {

	/**
	 * Host Variable HSSVH-UOW-NM
	 * 
	 */
	String getUowNm();

	void setUowNm(String uowNm);

	/**
	 * Host Variable HSSVH-SEC-GRP-NM
	 * 
	 */
	String getSecGrpNm();

	void setSecGrpNm(String secGrpNm);

	/**
	 * Host Variable HSSVH-HSSV-CONTEXT
	 * 
	 */
	String getHssvContext();

	void setHssvContext(String hssvContext);

	/**
	 * Host Variable HSSVH-HSSV-SCRN-FLD-ID
	 * 
	 */
	String getHssvScrnFldId();

	void setHssvScrnFldId(String hssvScrnFldId);

	/**
	 * Host Variable HSSVH-HSSV-SEQ-NBR
	 * 
	 */
	int getHssvSeqNbr();

	void setHssvSeqNbr(int hssvSeqNbr);

	/**
	 * Host Variable HSSVH-HSSV-NEW-LIN-IND
	 * 
	 */
	char getHssvNewLinInd();

	void setHssvNewLinInd(char hssvNewLinInd);

	/**
	 * Host Variable HSSVH-HSSV-SCRIPT-TXT
	 * 
	 */
	String getHssvScriptTxt();

	void setHssvScriptTxt(String hssvScriptTxt);
};
