/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.ActNotPol1Dao;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.commons.data.dao.PolDtaExtDao;
import com.federatedinsurance.crs.commons.data.dao.Sysdummy1Dao;
import com.federatedinsurance.crs.copy.Bxt03oAccountInfo;
import com.federatedinsurance.crs.copy.ProxyProgramCommon;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.copy.Xzy800GetNotDayRqrRow;
import com.federatedinsurance.crs.ws.UbocRecord;
import com.federatedinsurance.crs.ws.WsBillingLinkage;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsHalrlomgLinkage1;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsProxyProgramArea;
import com.federatedinsurance.crs.ws.WsXz0y90a0Row;
import com.federatedinsurance.crs.ws.Xz0p90k0Data;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.occurs.Bxt03oFeeInf;
import com.federatedinsurance.crs.ws.occurs.Bxt03oPolicyInf;
import com.federatedinsurance.crs.ws.ptr.LServiceContractArea;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0006;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x90c0;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x90h0;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x90j0;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x90m0;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0P90K0<br>
 * <pre>AUTHOR.       DAWN POSSEHL.
 * DATE-WRITTEN. 19 MAR 2009.
 * *****************************************************************
 *                                                                 *
 *   PROGRAM TITLE - PREPARE IMPENDING NOTIFICATION                *
 *                   XREF OBJ NM : XZ_PREPARE_IMP_NOTIFICATION_BPO *
 *                   UOW         : XZ_PREPARE_IMP_NOTIFICATION     *
 *                   OPERATION   : PrepareImpendingNotification    *
 *                                                                 *
 *   PURPOSE -  THIS BPO WILL CALL THE GET INSURED DETAIL AND THE  *
 *              GET BILLING DETAIL; AND HIT POL_DTA_EXT.  THEN     *
 *              FOR EVERY POLICY WITH THE PCN INDICATION OF YES,   *
 *             (THAT HASN'T BEEN PUT ON A PRIOR NOTIFICATION)      *
 *              WE WILL GET THE NUMBER OF NOTIFICATION DAYS.       *
 *              USING THAT, WE WILL DETERMINE THE NOTIFICATION     *
 *              EFFECTIVE DATE.  IF ANY POLICY HAS A NOTIFICATION  *
 *              DATE GREATER THAN OR EQUAL TO THE POLICY'S         *
 *              EXPIRATION DATE - THEN WE WILL KICK OUT A          *
 *              SPECIFIC ERROR MESSAGE THAT BUSINESSWORKS          *
 *              WILL KEY ON TO KICK OUT A WORKFLOW.                *
 *              OTHERWISE WE WILL ADD OUT THE NOTICE TO THE        *
 *              CANCELLATION DATA BASE.                            *
 *                                                                 *
 *   PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS   *
 *                         LINKED TO BY THE FRAMEWORK DRIVER.      *
 *                                                                 *
 *   DATA ACCESS METHODS - UMT STORAGE RECORDS                     *
 *                         DB2 DATABASE                            *
 *                                                                 *
 * *****************************************************************
 * ****************************************************************
 * * NOTE: THIS LOG FOR INFRASTRUCTURE USE ONLY FOR TEMPLATE     **
 * *       VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     **
 * *       APPLICATION CODING.                                   **
 * *     T E M P L A T E   M A I N T E N A N C E   L O G         **
 * * CASE#     DATE       PROG       DESCRIPTION                 **
 * * --------  ---------  --------   ----------------------------**
 * * TS129     06/13/2006 E404LJL    TEMPLATE CREATED            **
 * * YJ249     04/27/2007 E404NEM    STDS CHGS                   **
 * * TS130     12/28/2007 E404JSP    Changed a few bugs          **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #          DATE      PROG            DESCRIPTION         **
 * * -------   ----------  -------   ----------------------------**
 * * TO07614   03/19/2009  E404DLP   NEW                         **
 * * TO07614   04/09/2009  E404DLP   MAKE USE OF THE BILLING     **
 * *                                 PROPOSED CANCEL DATE.       **
 * * TO07614   05/12/2009  E404GCL   Change beta region to BB    **
 * * TO07614   05/15/2009  E404KXS   Change how tot-amt-due is   **
 * *                                 calculated.                 **
 * * TO07614   07/22/2009  E404GRK   Change to not list a policy **
 * *                                 on a notice if it already   **
 * *                                 went out on a prior notice. **
 * *                                 Also don't include fees if  **
 * *                                 a prior notice was sent with**
 * *                                 policies that are still in  **
 * *                                 PCN.                        **
 * * TO0760222 03/31/2009  E404KXS   Add call to Update          **
 * *                                 Notification Status.        **
 * * BX000397  07/20/2010  E404GCL   Bypass date of 9999-12-31 in**
 * *                                 calc of Billing Cancel date **
 * * BX000484  11/12/2010  E404GCL   Change to only look for IMP,**
 * *                                 RES, and NPC transactions   **
 * *                                 when looking to see if a pol**
 * *                                 has an imp notification sent**
 * * BX000477  01/27/2011  E404DLP   Use the policy effective    **
 * *                                 passed from billing, in our **
 * *                                 valid policy check.         **
 * * PP03294   10/12/2012  E404DNF   Recompile for FWBT0011 chg  **
 * * CP081-01  10/12/2012  E404AKW   REMOVE ORIGINAL INCEPTION DT**
 * *                                 AND USE THE MINIMUM POLICY  **
 * *                                 EFFECTIVE DATE INSTEAD      **
 * * PP02500   09/07/2012  E404BPO   RECOMPILE FOR XZ0T0006 CHG  **
 * * PP03933   11/18/2013  E404KXS   RECOMPILE                   **
 * * 20163     01/10/2019  E404DLP   ADDED FEDONE REGIONS        **
 * * 20163     01/10/2019  E404DLP   REMOVE CALL TO THE POLICY   **
 * *                                 DETAIL SERVICE AND HIT      **
 * *                                 POL_DTA_EXT INSTEAD         **
 * ****************************************************************</pre>*/
public class Xz0p90k0 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private Sysdummy1Dao sysdummy1Dao = new Sysdummy1Dao(dbAccessStatus);
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	private ActNotPol1Dao actNotPol1Dao = new ActNotPol1Dao(dbAccessStatus);
	private PolDtaExtDao polDtaExtDao = new PolDtaExtDao(dbAccessStatus);
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0p90k0Data ws = new Xz0p90k0Data();
	private ExecContext execContext = null;
	/**Original name: WS-MU0T0004-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE INSURED DETAIL SERVICE</pre>*/
	private LServiceContractArea wsMu0t0004Row = new LServiceContractArea(null);
	/**Original name: WS-XZ0T0006-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE MAINTAIN ACT NOT SERVICE</pre>*/
	private LServiceContractAreaXz0x0006 wsXz0t0006Row = new LServiceContractAreaXz0x0006(null);
	/**Original name: WS-XZ0T90A0-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE PREPARE THIRD PARTY SERVICE</pre>*/
	private LServiceContractAreaXz0x90c0 wsXz0t90a0Row = new LServiceContractAreaXz0x90c0(null);
	/**Original name: WS-XZ0T90H0-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE PREPARE INSURED POLICY SERVICE</pre>*/
	private LServiceContractAreaXz0x90h0 wsXz0t90h0Row = new LServiceContractAreaXz0x90h0(null);
	/**Original name: WS-XZ0T90J0-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE PREPARE NOTIFICATION SERVICE</pre>*/
	private LServiceContractAreaXz0x90j0 wsXz0t90j0Row = new LServiceContractAreaXz0x90j0(null);
	/**Original name: WS-XZ0T90M0-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE UPDATE NOTIFICATION STATUS SERVICE</pre>*/
	private LServiceContractAreaXz0x90m0 wsXz0t90m0Row = new LServiceContractAreaXz0x90m0(null);
	//Original name: DFHCOMMAREA
	private WsHalrlomgLinkage1 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, WsHalrlomgLinkage1 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0p90k0 getInstance() {
		return (Programs.getInstance(Xz0p90k0.class));
	}

	/**Original name: 1000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   MAIN PROCESSING CONTROL                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-PREPARE-IMP-NOT.
		prepareImpNot();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *  INITIALIZATION CONTROL                                        *
	 *                                                                *
	 * ****************************************************************
	 * * INITIALIZE ERROR/WARNING STORAGE</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//* VALIDATE UBOC IN COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2100-READ-REQ-UMT-ROW.
		readReqUmtRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-GET-CURRENT-DATE-TIME.
		getCurrentDateTime();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2300-INIT-BILLING-RGN.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0P90K0.CBL:line=693, because the code is unreachable.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-READ-REQ-UMT-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR THE BPO INPUT ROW                     *
	 * *****************************************************************</pre>*/
	private void readReqUmtRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: SET HALRURQA-READ-FUNC      TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM-PREPARE-IMP-NOT
		//                                       TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWorkingStorageArea().getWsBusObjNmPrepareImpNot());
		// COB_CODE: MOVE +1                     TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(1);
		// COB_CODE: INITIALIZE                  WS-XZ0Y90K0-ROW.
		initWsXz0y90k0Row();
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-XZ0Y90K0-ROW.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsXz0y90k0Row());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: IF HALRURQA-REC-NOT-FOUND
		//               GO TO 2100-EXIT
		//           END-IF.
		if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUEST-MSG-MISSING
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequestMsgMissing();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '2100-READ-REQ-UMT-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-READ-REQ-UMT-ROW");
			// COB_CODE: MOVE 'NO RECORD FOUND ON REQ MSG STORE'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO RECORD FOUND ON REQ MSG STORE");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HALRURQA-BUS-OBJ-NM='
			//                  HALRURQA-BUS-OBJ-NM
			//                  '; HALRURQA-REC-SEQ='
			//                  HALRURQA-REC-SEQ
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HALRURQA-BUS-OBJ-NM=").append(ws.getWsHalrurqaLinkage().getBusObjNmFormatted())
							.append("; HALRURQA-REC-SEQ=").append(ws.getWsHalrurqaLinkage().getRecSeqFormatted()).toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
	}

	/**Original name: 2200-GET-CURRENT-DATE-TIME_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  GET THE CURRENT DATE TIME                                      *
	 * *****************************************************************</pre>*/
	private void getCurrentDateTime() {
		// COB_CODE: EXEC SQL
		//               SELECT CURRENT TIMESTAMP
		//                 INTO :WS-CURRENT-TIMESTAMP
		//                 FROM SYSIBM.SYSDUMMY1
		//           END-EXEC.
		ws.getWorkingStorageArea().getWsCurrentTimestampX()
				.setWsCurrentTimestamp(sysdummy1Dao.selectRec2(ws.getWorkingStorageArea().getWsCurrentTimestampX().getWsCurrentTimestamp()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   GO TO 2200-EXIT
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: GO TO 2200-EXIT
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'SYSIBM DATE'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("SYSIBM DATE");
			// COB_CODE: MOVE '2200-GET-CURRENT-DATE-TIME'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2200-GET-CURRENT-DATE-TIME");
			// COB_CODE: MOVE SQLCODE        TO EFAL-DB2-ERR-SQLCODE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: IF SQLCODE < 0
			//                MOVE '-'       TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           ELSE
			//                MOVE '+'       TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           END-IF
			if (sqlca.getSqlcode() < 0) {
				// COB_CODE: MOVE '-'       TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'       TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			}
			// COB_CODE: MOVE 'DB2 ERROR READING SYSIBM TABLE'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DB2 ERROR READING SYSIBM TABLE");
			// COB_CODE: MOVE SQLERRMC       TO EFAL-DB2-ERR-SQLERRMC
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE SPACES         TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 3000-PREPARE-IMP-NOT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  GET INSURED AND BILLING INFORMATION FOR THE ACCOUNT NUMBER     *
	 *  PASSED IN.  ALSO GET THE POLICY DETAIL INFORMATION AND         *
	 *  CALCULATE THE POLICY NOTIFICATION EFFECTIVE DATE.              *
	 *  THEN DETERMINE IF WE CAN ADD OUT THE NOTICE OR IF BUSINESSWORKS*
	 *  WILL NEED TO CREATE AN INSUFFICIENT DAYS TO EXPIRE WORKFLOW.   *
	 * *****************************************************************
	 * * GET THE INSURED DETAIL</pre>*/
	private void prepareImpNot() {
		// COB_CODE: PERFORM 3100-GET-INSURED-DETAIL.
		getInsuredDetail();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-FREE-MEM-FOR-SERVICES
			freeMemForServices();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* GET THE BILLING DETAIL
		// COB_CODE: PERFORM 3200-GET-BILLING-DETAIL.
		getBillingDetail();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-FREE-MEM-FOR-SERVICES
			freeMemForServices();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* GET EACH POLICIES DETAIL INFORMATION
		// COB_CODE: PERFORM 3300-GET-POLICY-DETAIL.
		rng3300GetPolicyDetail();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-FREE-MEM-FOR-SERVICES
			freeMemForServices();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* DETERMINE EACH POLICIES NOTIFICATION EFFECTIVE DATE
		// COB_CODE: PERFORM 3400-DET-POLICY-NOT-EFF-DT.
		rng3400DetPolicyNotEffDt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-FREE-MEM-FOR-SERVICES
			freeMemForServices();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* CREATE THE NOTICE  --  OR PRODUCE ERROR FOR WORKFLOW TRIGGER
		// COB_CODE:      IF SW-ADD-NOTIFICATION
		//                    END-IF
		//                ELSE
		//           **        ***
		//           **  NOTE  ***
		//           **        ***
		//           **    DO NOT CHANGE THIS ERROR MESSAGE WITHOUT
		//           **    CONTACTING THE BUSINESSWORKS TEAM - THEY
		//           **    ARE KEYING OFF IT TO CREATE A WORKFLOW
		//           **
		//                    PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
		//                END-IF.
		if (ws.getSwitches().isAddNotificationFlag()) {
			// COB_CODE: PERFORM 3500-CREATE-NOTICE
			createNotice();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3000-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: PERFORM 9100-FREE-MEM-FOR-SERVICES
				freeMemForServices();
				// COB_CODE: GO TO 3000-EXIT
				return;
			}
			// COB_CODE: PERFORM 3600-WRITE-OUT-RESPONSE
			writeOutResponse();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3000-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: PERFORM 9100-FREE-MEM-FOR-SERVICES
				freeMemForServices();
				// COB_CODE: GO TO 3000-EXIT
				return;
			}
		} else {
			//*        ***
			//*  NOTE  ***
			//*        ***
			//*    DO NOT CHANGE THIS ERROR MESSAGE WITHOUT
			//*    CONTACTING THE BUSINESSWORKS TEAM - THEY
			//*    ARE KEYING OFF IT TO CREATE A WORKFLOW
			//*
			// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			// COB_CODE: MOVE WS-BUS-OBJ-NM-PREPARE-IMP-NOT
			//                                   TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getWsBusObjNmPrepareImpNot());
			// COB_CODE: MOVE 'ACT_NBR'          TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("ACT_NBR");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE XZY9K0-CSR-ACT-NBR TO EA-01-CSR-ACT-NBR
			ws.getEa01InsufficientDays().setCsrActNbr(ws.getWsXz0y90k0Row().getCsrActNbr());
			// COB_CODE: MOVE WS-NOT-EFF-DT-MM   TO EA-01-NOT-EFF-DT-MM
			ws.getEa01InsufficientDays().setNotEffDtMm(ws.getWorkingStorageArea().getWsNotEffDtMax().getMm());
			// COB_CODE: MOVE WS-NOT-EFF-DT-DD   TO EA-01-NOT-EFF-DT-DD
			ws.getEa01InsufficientDays().setNotEffDtDd(ws.getWorkingStorageArea().getWsNotEffDtMax().getDd());
			// COB_CODE: MOVE WS-NOT-EFF-DT-YYYY TO EA-01-NOT-EFF-DT-YYYY
			ws.getEa01InsufficientDays().setNotEffDtYyyy(ws.getWorkingStorageArea().getWsNotEffDtMax().getYyyy());
			// COB_CODE: MOVE EA-01-INSUFFICIENT-DAYS
			//                                   TO WS-NONLOG-ERR-ALLTXT-TEXT
			ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getEa01InsufficientDays().getEa01InsufficientDaysFormatted());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
		}
		// COB_CODE: PERFORM 9100-FREE-MEM-FOR-SERVICES.
		freeMemForServices();
	}

	/**Original name: 3100-GET-INSURED-DETAIL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE SERVICE TO GET THE INSURED DETAIL                     *
	 * *****************************************************************</pre>*/
	private void getInsuredDetail() {
		// COB_CODE: PERFORM 3110-ALC-MEM-INSURED-DTL-SVC.
		alcMemInsuredDtlSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: SET SW-INSURED-DETAIL-ALC   TO TRUE.
		ws.getSwitches().setInsuredDetailAlcFlag(true);
		// COB_CODE: PERFORM 3120-CALL-INSURED-DTL-SVC.
		callInsuredDtlSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
	}

	/**Original name: 3110-ALC-MEM-INSURED-DTL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE GET INSURED DETAIL SERVICE         *
	 * *****************************************************************</pre>*/
	private void alcMemInsuredDtlSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS
		//                                       OF WS-PROXY-PROGRAM-AREA.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-MU0T0004-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractArea.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER OF WS-PROXY-PROGRAM-AREA)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE OF WS-PROXY-PROGRAM-AREA)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-INSURED-DETAIL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc());
			// COB_CODE: MOVE '3110-ALC-MEM-INSURED-DTL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3110-ALC-MEM-INSURED-DTL-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3110-ALC-MEM-INSURED-DTL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3110-ALC-MEM-INSURED-DTL-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3110-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-MU0T0004-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER
		//                                       OF WS-PROXY-PROGRAM-AREA.
		wsMu0t0004Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractArea.class)));
		// COB_CODE: INITIALIZE WS-MU0T0004-ROW.
		initWsMu0t0004Row();
	}

	/**Original name: 3120-CALL-INSURED-DTL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS NEEDED TO CALL THE INSURED DETAIL SERVICE.      *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void callInsuredDtlSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       OF WS-PROXY-PROGRAM-AREA
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-GET-INSURED-DETAIL  TO PPC-OPERATION
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getGetInsuredDetail());
		// COB_CODE: MOVE XZY9K0-USERID          TO MUT04I-USR-ID.
		wsMu0t0004Row.setMut04iUsrId(ws.getWsXz0y90k0Row().getUserid());
		// COB_CODE: MOVE XZY9K0-CSR-ACT-NBR     TO MUT04I-ACT-NBR.
		wsMu0t0004Row.setMut04iActNbr(ws.getWsXz0y90k0Row().getCsrActNbr());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-GET-INSURED-DETAIL-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90K0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc(), new Mu0x0004());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3120-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-INSURED-DETAIL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc());
			// COB_CODE: MOVE '3120-CALL-INSURED-DTL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3120-CALL-INSURED-DTL-SVC");
			// COB_CODE: MOVE 'INSURED DETAIL SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INSURED DETAIL SERVICE CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3120-CALL-INSURED-DTL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3120-CALL-INSURED-DTL-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3120-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-GET-INSURED-DETAIL-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc());
		// COB_CODE: MOVE '3120-GET-INSURED-DTL-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3120-GET-INSURED-DTL-SVC");
		// COB_CODE: MOVE 'GET INSURED DTL SVC'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("GET INSURED DTL SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3200-GET-BILLING-DETAIL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE SERVICE INTERFACE TO GET THE BILLING DETAIL           *
	 * *****************************************************************</pre>*/
	private void getBillingDetail() {
		// COB_CODE: PERFORM 3210-CALL-BILLING-DTL-SVC.
		callBillingDtlSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3200-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3200-EXIT
			return;
		}
		// COB_CODE: PERFORM 3220-LOAD-PCN-POLICIES.
		rng3220LoadPcnPolicies();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3200-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3200-EXIT
			return;
		}
		// COB_CODE: IF SW-DO-NOT-APPLY-FEES
		//               MOVE +0                 TO AA-TOT-FEE-AMT
		//           ELSE
		//               END-IF
		//           END-IF.
		if (!ws.getSwitches().isApplyFeesFlag()) {
			// COB_CODE: MOVE +0                 TO AA-TOT-FEE-AMT
			ws.setAaTotFeeAmt(Trunc.toDecimal(0, 10, 2));
		} else {
			// COB_CODE: PERFORM 3230-CALCULATE-FEE-AMT
			rng3230CalculateFeeAmt();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3200-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3200-EXIT
				return;
			}
		}
	}

	/**Original name: 3210-CALL-BILLING-DTL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS NEEDED AND CALL THE BILLING DETAIL SERVICE.     *
	 *   SINCE THE BILLING PROGRAM RESIDES IN A DIFFERENT CICS,        *
	 *   WE NEED TO CALL THEIR SERVICE INTERFACE PROGRAM AND PASS      *
	 *   THE CICS REGION AND TRANSID.                                  *
	 * *****************************************************************</pre>*/
	private void callBillingDtlSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE WS-BILLING-LINKAGE.
		initWsBillingLinkage();
		//*  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
		//*  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       OF WS-BILLING-LINKAGE
		//                                       TO TRUE.
		ws.getWsBillingLinkage().getProxyProgramCommon().getBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-GET-BILLING-DETAIL  TO PPC-OPERATION
		//                                       OF WS-BILLING-LINKAGE.
		ws.getWsBillingLinkage().getProxyProgramCommon().setOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getGetBillingDetail());
		// COB_CODE: MOVE CF-BILLING-USERID      TO BXT03I-USERID.
		ws.getWsBillingLinkage().getBx0t0003().setBxt03iUserid(ws.getConstantFields().getBillingUserid());
		// COB_CODE: MOVE XZY9K0-CSR-ACT-NBR     TO BXT03I-ACCOUNT-NBR.
		ws.getWsBillingLinkage().getBx0t0003().setBxt03iAccountNbr(ws.getWsXz0y90k0Row().getCsrActNbr());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-GET-BILLING-DETAIL-SVC-INTF)
		//               SYSID(WS-BILLING-REGION)
		//               TRANSID(CF-GET-BILLING-DETAIL-TRANSID)
		//               COMMAREA(WS-BILLING-LINKAGE)
		//               LENGTH(LENGTH OF WS-BILLING-LINKAGE)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90K0", execContext).commarea(ws.getWsBillingLinkage()).length(WsBillingLinkage.Len.WS_BILLING_LINKAGE)
				.sysid(ws.getWorkingStorageArea().getWsBillingRegion().getBillingRegion())
				.link(ws.getConstantFields().getGetBillingDetailSvcIntf(), new Bx0g0003());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3210-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-GET-BILLING-DETAIL-SVC-INTF
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getGetBillingDetailSvcIntf());
			// COB_CODE: MOVE '3210-CALL-BILLING-DTL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3210-CALL-BILLING-DTL-SVC");
			// COB_CODE: MOVE 'BILLING DETAIL SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BILLING DETAIL SERVICE CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3210-CALL-BILLING-DTL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3210-CALL-BILLING-DTL-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3210-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-GET-BILLING-DETAIL-SVC-INTF
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getGetBillingDetailSvcIntf());
		// COB_CODE: MOVE '3210-CALL-BILLING-DTL-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3210-CALL-BILLING-DTL-SVC");
		// COB_CODE: MOVE 'GET BILLING DTL SVC'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("GET BILLING DTL SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		//  MOVE THE BILLING PPC AREA TO THIS PROGRAM PPC AREA
		//  SO THE COMMON ERROR ROUTINE CAN REFER TO THE SAME PPC AREA
		// COB_CODE: MOVE PROXY-PROGRAM-COMMON   OF WS-BILLING-LINKAGE
		//                                       TO PROXY-PROGRAM-COMMON
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setProxyProgramCommonBytes(ws.getWsBillingLinkage().getProxyProgramCommon().getProxyProgramCommonBytes());
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3220-LOAD-PCN-POLICIES_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LOAD THE POLICY TABLE                                          *
	 * *****************************************************************</pre>*/
	private void loadPcnPolicies() {
		// COB_CODE: SET SW-APPLY-FEES           TO TRUE.
		ws.getSwitches().setApplyFeesFlag(true);
		// COB_CODE: SET IX-TP                   TO +1.
		ws.setIxTp(1);
		// COB_CODE: MOVE +1                     TO SS-PL.
		ws.getSubscripts().setCl(((short) 1));
		// COB_CODE: COMPUTE WS-MAX-POL-ROWS = LENGTH OF BXT03O-POLICY-LIST
		//                                   / LENGTH OF BXT03O-POLICY-INF.
		ws.getWorkingStorageArea()
				.setWsMaxPolRows((new AfDecimal(((((double) Bxt03oAccountInfo.Len.POLICY_LIST)) / Bxt03oPolicyInf.Len.POLICY_INF), 9, 0)).toShort());
	}

	/**Original name: 3220-A<br>
	 * <pre>* WHEN WE HAVE REACHED THE END OF THE BILLING POLICY LIST - EXIT
	 * * KICK OUT A ERROR IF NO POLICY RETURNED FROM BILLING SERVICE</pre>*/
	private String a() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF SS-PL > WS-MAX-POL-ROWS
		//            OR
		//              BXT03O-POLICY-NBR(SS-PL) = SPACES
		//               GO TO 3220-EXIT
		//           END-IF.
		if (ws.getSubscripts().getCl() > ws.getWorkingStorageArea().getWsMaxPolRows() || Characters.EQ_SPACE
				.test(ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPolicyNbr())) {
			// COB_CODE: IF SS-PL = +1
			//               PERFORM 9000-LOG-WARNING-OR-ERROR
			//           END-IF
			if (ws.getSubscripts().getCl() == 1) {
				// COB_CODE: SET WS-LOG-ERROR    TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
				// COB_CODE: SET BUSP-REQD-DATA-NOT-FOUND
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspReqdDataNotFound();
				// COB_CODE: MOVE '3220-LOAD-PCN-POLICIES'
				//                               TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3220-LOAD-PCN-POLICIES");
				// COB_CODE: MOVE 'NO POLICY RETURNED FROM BILLING SERVICE'
				//                               TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO POLICY RETURNED FROM BILLING SERVICE");
				// COB_CODE: STRING 'CSR-ACT-NBR='
				//                  XZY9K0-CSR-ACT-NBR
				//                  ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CSR-ACT-NBR=",
						ws.getWsXz0y90k0Row().getCsrActNbrFormatted(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
			}
			// COB_CODE: GO TO 3220-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3221-CHECK-VALID-POLICY
		checkValidPolicy();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3220-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3220-EXIT
			return "";
		}
		//* ADD POLICY TO TABLE
		// COB_CODE: IF SW-ADD-POLICY
		//               SET IX-TP               UP BY +1
		//           END-IF.
		if (ws.getSwitches().isAddPolicyFlag()) {
			// COB_CODE: IF IX-TP > CF-MAX-TBL-POLICIES
			//               GO TO 3220-EXIT
			//           END-IF
			if (ws.getIxTp() > ws.getConstantFields().getMaxTblPolicies()) {
				// COB_CODE: SET WS-LOG-ERROR    TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
				// COB_CODE: SET BUSP-LIMIT-EXCEEDED
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspLimitExceeded();
				// COB_CODE: MOVE '3220-LOAD-PCN-POLICIES'
				//                               TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3220-LOAD-PCN-POLICIES");
				// COB_CODE: MOVE 'TABLE-OF-POLICIES HIT MAX'
				//                               TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("TABLE-OF-POLICIES HIT MAX");
				// COB_CODE: MOVE CF-MAX-TBL-POLICIES
				//                               TO WS-MAX-ROWS
				ws.getWorkingStorageArea().setWsMaxRows(ws.getConstantFields().getMaxTblPolicies());
				// COB_CODE: STRING 'CSR-ACT-NBR='
				//                  XZY9K0-CSR-ACT-NBR
				//                  ' MAX ROWS = '
				//                  WS-MAX-ROWS
				//                  ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CSR-ACT-NBR=",
						ws.getWsXz0y90k0Row().getCsrActNbrFormatted(), " MAX ROWS = ", ws.getWorkingStorageArea().getWsMaxRowsAsString(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 3220-EXIT
				return "";
			}
			// COB_CODE: MOVE BXT03O-POLICY-NBR(SS-PL)
			//                                   TO TP-POL-NBR(IX-TP)
			ws.getTpPolicies(ws.getIxTp())
					.setPolNbr(ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPolicyNbr());
			// COB_CODE: MOVE BXT03O-POL-EFF-DT(SS-PL)
			//                                   TO TP-POL-EFF-DT(IX-TP)
			ws.getTpPolicies(ws.getIxTp()).setPolEffDt(
					ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPolEffDt());
			// COB_CODE: MOVE BXT03O-PROP-CAN-DT(SS-PL)
			//                                   TO TP-PROP-CAN-DT(IX-TP)
			ws.getTpPolicies(ws.getIxTp()).setPropCanDt(
					ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPropCanDt());
			// COB_CODE: MOVE BXT03O-POL-STATUS-CD(SS-PL)
			//                                   TO TP-POL-BIL-STA-CD(IX-TP)
			ws.getTpPolicies(ws.getIxTp()).setPolBilStaCd(
					ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPolStatusCd());
			// COB_CODE: MOVE BXT03O-POL-AMT-DUE(SS-PL)
			//                                   TO TP-POL-DUE-AMT(IX-TP)
			ws.getTpPolicies(ws.getIxTp()).setPolDueAmt(Trunc.toDecimal(
					ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPolAmtDue(), 10, 2));
			// COB_CODE: SET IX-TP               UP BY +1
			ws.setIxTp(Trunc.toInt(ws.getIxTp() + 1, 9));
		}
		// COB_CODE: ADD +1                      TO SS-PL.
		ws.getSubscripts().setCl(Trunc.toShort(1 + ws.getSubscripts().getCl(), 4));
		// COB_CODE: GO TO 3220-A.
		return "3220-A";
	}

	/**Original name: 3221-CHECK-VALID-POLICY_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CHECK TO SEE IF THE POLICY QUALIFIES TO BE PUT ON THE IMPENDING*
	 *  NOTIFICATION. IN ORDER TO DO SO, IT MUST A) HAVE PCN IND = Y,  *
	 *  B) NOT HAVE AN IMPENDING NOTIFICATION IN THE CANCELLATION      *
	 *  TABLES AS THE MOST RECENT PRIOR NOTIFICATION FOR THE POLICY.   *
	 * *****************************************************************</pre>*/
	private void checkValidPolicy() {
		// COB_CODE: SET SW-ADD-POLICY           TO TRUE.
		ws.getSwitches().setAddPolicyFlag(true);
		//* CHECK PCN IND = Y
		// COB_CODE: IF BXT03O-PCN-IND(SS-PL) NOT = CF-YES
		//               GO TO 3221-EXIT
		//           END-IF.
		if (ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPcnInd() != ws
				.getConstantFields().getYes()) {
			// COB_CODE: SET SW-DO-NOT-ADD-POLICY
			//                                   TO TRUE
			ws.getSwitches().setAddPolicyFlag(false);
			// COB_CODE: GO TO 3221-EXIT
			return;
		}
		//* CHECK IMPENDING NOTIFICATION DONE
		// SET CONSTANT FIELDS. THESE ONLY NEED TO BE SET THE FIRST
		// TIME THROUGH.
		// COB_CODE: IF SW-FIRST-SELECT
		//                                       OF DCLACT-NOT
		//           END-IF.
		if (ws.getSwitches().isFirstSelectFlag()) {
			// COB_CODE: SET SW-NOT-FIRST-SELECT TO TRUE
			ws.getSwitches().setFirstSelectFlag(false);
			// COB_CODE: MOVE XZY9K0-CSR-ACT-NBR TO CSR-ACT-NBR
			//                                   OF DCLACT-NOT
			ws.getDclactNot().setCsrActNbr(ws.getWsXz0y90k0Row().getCsrActNbr());
			// COB_CODE: MOVE CF-ACT-NOT-IMPENDING
			//                                   TO ACT-NOT-TYP-CD
			//                                   OF DCLACT-NOT
			ws.getDclactNot().setActNotTypCd(ws.getConstantFields().getActNotImpending());
			// COB_CODE: MOVE CF-ADDED-BY-BUSINESS-WORKS
			//                                   TO ACT-OWN-CLT-ID
			//                                   OF DCLACT-NOT
			ws.getDclactNot().setActOwnCltId(ws.getConstantFields().getAddedByBusinessWorks());
			// COB_CODE: MOVE CF-ACT-NOT-STATUS-COMPLETE
			//                                   TO ACT-NOT-STA-CD
			//                                   OF DCLACT-NOT
			ws.getDclactNot().setActNotStaCd(ws.getConstantFields().getActNotStaCd().getComplete());
		}
		// COB_CODE: MOVE BXT03O-POLICY-NBR(SS-PL)
		//                                       TO POL-NBR
		//                                       OF DCLACT-NOT-POL.
		ws.getDclactNotPol()
				.setPolNbr(ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPolicyNbr());
		// COB_CODE: MOVE BXT03O-POL-EFF-DT(SS-PL)
		//                                       TO POL-EFF-DT
		//                                       OF DCLACT-NOT-POL.
		ws.getDclactNotPol()
				.setPolEffDt(ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPolEffDt());
		// INNER QUERY SELECTS THE MOST RECENT TIMESTAMP ASSOCIATED
		// WITH THE POLICY. OUTER QUERY DETERMINES IF THE MOST RECENT
		// NOTIFICATION IS 1) OF TYPE IMPENDING 2) OF STATUS COMPLETE
		// 3) ADDED BY CANCELLATION SYSTEM. IF SO, THE POLICY EXPIRATION
		// DATE IS RETRIEVED.
		// COB_CODE:      EXEC SQL
		//                    SELECT B.POL_EXP_DT
		//                      INTO :DCLACT-NOT-POL.POL-EXP-DT
		//                      FROM ACT_NOT A, ACT_NOT_POL B
		//                     WHERE A.NOT_PRC_TS      = B.NOT_PRC_TS
		//                       AND A.CSR_ACT_NBR     = B.CSR_ACT_NBR
		//                       AND A.NOT_PRC_TS      =
		//                               (SELECT MAX(C.NOT_PRC_TS)
		//                                  FROM ACT_NOT_POL C
		//                                     , ACT_NOT D
		//                                 WHERE C.CSR_ACT_NBR
		//                                         = :DCLACT-NOT.CSR-ACT-NBR
		//                                   AND C.POL_NBR
		//                                         = :DCLACT-NOT-POL.POL-NBR
		//                                   AND C.POL_EFF_DT
		//           *                             = B.POL_EFF_DT
		//                                         = :DCLACT-NOT-POL.POL-EFF-DT
		//                                   AND D.CSR_ACT_NBR
		//                                         = :DCLACT-NOT.CSR-ACT-NBR
		//                                   AND D.NOT_PRC_TS
		//                                         = C.NOT_PRC_TS
		//                                   AND D.ACT_NOT_TYP_CD
		//                                         IN (:CF-ACT-NOT-IMPENDING
		//                                            ,:CF-ACT-NOT-NONPAY
		//                                            ,:CF-ACT-NOT-RESCIND) )
		//                       AND A.CSR_ACT_NBR     = :DCLACT-NOT.CSR-ACT-NBR
		//                       AND A.ACT_NOT_TYP_CD  = :DCLACT-NOT.ACT-NOT-TYP-CD
		//                       AND A.ACT_OWN_CLT_ID <> :DCLACT-NOT.ACT-OWN-CLT-ID
		//                       AND A.ACT_NOT_STA_CD  = :DCLACT-NOT.ACT-NOT-STA-CD
		//                       AND B.POL_NBR         = :DCLACT-NOT-POL.POL-NBR
		//                       AND B.POL_EFF_DT      = :DCLACT-NOT-POL.POL-EFF-DT
		//                END-EXEC.
		ws.getDclactNotPol().setPolExpDt(actNotPol1Dao.selectRec2(ws, ws.getDclactNotPol().getPolExpDt()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                                       TO TRUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 3221-EXIT
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET SW-DO-NOT-ADD-POLICY
			//                               TO TRUE
			ws.getSwitches().setAddPolicyFlag(false);
			//            Do not apply fees to this notice if any policy
			//            currently in PCN was on a prior notice because the
			//            fees were included on the prior one and they cannot
			//            be applied to both.
			// COB_CODE: SET SW-DO-NOT-APPLY-FEES
			//                               TO TRUE
			ws.getSwitches().setApplyFeesFlag(false);
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT'      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT");
			// COB_CODE: MOVE '3221-CHECK-VALID-POLICY'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3221-CHECK-VALID-POLICY");
			// COB_CODE: MOVE 'SELECT ACT_NOT FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT ACT_NOT FAILED");
			// COB_CODE: MOVE SPACES         TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'ACT_NBR: '
			//                  XZY9K0-CSR-ACT-NBR
			//                  'POL_NBR: '
			//                  BXT03O-POLICY-NBR(SS-PL)
			//              DELIMITED BY SIZE
			//              INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(new StringBuffer(256).append("ACT_NBR: ")
					.append(ws.getWsXz0y90k0Row().getCsrActNbrFormatted()).append("POL_NBR: ")
					.append(ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(ws.getSubscripts().getCl()).getPolicyNbr())
					.toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3221-EXIT
			return;
		}
	}

	/**Original name: 3230-CALCULATE-FEE-AMT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALCULATE THE TOTAL FEE AMOUNT ASSOCIATED WITH THE ACCOUNT.    *
	 * *****************************************************************</pre>*/
	private void calculateFeeAmt() {
		// COB_CODE: MOVE +1                     TO SS-FL.
		ws.getSubscripts().setTl(((short) 1));
		// COB_CODE: COMPUTE WS-MAX-FEE-ROWS = LENGTH OF BXT03O-FEE-LIST
		//                                   / LENGTH OF BXT03O-FEE-INF.
		ws.getWorkingStorageArea()
				.setWsMaxFeeRows((new AfDecimal(((((double) Bxt03oAccountInfo.Len.FEE_LIST)) / Bxt03oFeeInf.Len.FEE_INF), 9, 0)).toShort());
	}

	/**Original name: 3230-A<br>*/
	private String a1() {
		// COB_CODE: IF SS-FL > WS-MAX-FEE-ROWS
		//            OR
		//              BXT03O-FEE-TYPE(SS-FL) = SPACES
		//               GO TO 3230-EXIT
		//           END-IF.
		if (ws.getSubscripts().getTl() > ws.getWorkingStorageArea().getWsMaxFeeRows() || Characters.EQ_SPACE
				.test(ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getFeeInf(ws.getSubscripts().getTl()).getType())) {
			// COB_CODE: GO TO 3230-EXIT
			return "";
		}
		// ADD FEE AMOUNT TO TOTAL
		// COB_CODE: COMPUTE AA-TOT-FEE-AMT = AA-TOT-FEE-AMT
		//                                  + BXT03O-FEE-AMT(SS-FL).
		ws.setAaTotFeeAmt(Trunc.toDecimal(
				ws.getAaTotFeeAmt().add(ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getFeeInf(ws.getSubscripts().getTl()).getAmt()),
				10, 2));
		// COB_CODE: ADD +1                      TO SS-FL.
		ws.getSubscripts().setTl(Trunc.toShort(1 + ws.getSubscripts().getTl(), 4));
		// COB_CODE: GO TO 3230-A.
		return "3230-A";
	}

	/**Original name: 3300-GET-POLICY-DETAIL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FOR EVERY POLICY IN THE TABLE -- GET THE POLICY DETAIL         *
	 * *****************************************************************</pre>*/
	private void getPolicyDetail() {
		// COB_CODE: SET IX-TP                   TO +1.
		ws.setIxTp(1);
	}

	/**Original name: 3300-A<br>
	 * <pre>* WHEN WE HAVE REACHED THE END OF THE TABLE - EXIT
	 * * KICK OUT A ERROR IF THE TABLE IS EMPTY BECAUSE
	 * * NO PCN POLICY WAS RETURNED FROM BILLING.</pre>*/
	private String a2() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF TP-END-OF-TABLE(IX-TP)
		//               GO TO 3300-EXIT
		//           END-IF.
		if (ws.getTpPolicies(ws.getIxTp()).isEndOfTable()) {
			// COB_CODE: IF IX-TP = +1
			//               PERFORM 9000-LOG-WARNING-OR-ERROR
			//           END-IF
			if (ws.getIxTp() == 1) {
				// COB_CODE: SET WS-LOG-ERROR    TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
				// COB_CODE: SET BUSP-REQD-DATA-NOT-FOUND
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspReqdDataNotFound();
				// COB_CODE: MOVE '3300-GET-POLICY-DETAIL'
				//                               TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3300-GET-POLICY-DETAIL");
				// COB_CODE: MOVE 'NO PCN POLICY RETURNED FROM BILLING SERVICE'
				//                               TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO PCN POLICY RETURNED FROM BILLING SERVICE");
				// COB_CODE: STRING 'CSR-ACT-NBR='
				//                  XZY9K0-CSR-ACT-NBR
				//                  ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CSR-ACT-NBR=",
						ws.getWsXz0y90k0Row().getCsrActNbrFormatted(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
			}
			// COB_CODE: GO TO 3300-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3320-GET-POLICY-DTL.
		getPolicyDtl();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3300-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3300-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3340-GET-MIN-POL-EFF-DT.
		getMinPolEffDt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3300-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3300-EXIT
			return "";
		}
		// COB_CODE: SET IX-TP                   UP BY +1.
		ws.setIxTp(Trunc.toInt(ws.getIxTp() + 1, 9));
		// COB_CODE: GO TO 3300-A.
		return "3300-A";
	}

	/**Original name: 3320-GET-POLICY-DTL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   GET THE POLICY DETAIL AND UPDATE THE POLICY TABLE             *
	 *   WITH THE INFO RETURNED.                                       *
	 * *****************************************************************</pre>*/
	private void getPolicyDtl() {
		// COB_CODE: MOVE TP-POL-NBR(IX-TP)      TO POL-NBR OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(ws.getTpPolicies(ws.getIxTp()).getPolNbr());
		// COB_CODE: MOVE TP-POL-EFF-DT(IX-TP)   TO POL-EFF-DT OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolEffDt(ws.getTpPolicies(ws.getIxTp()).getPolEffDt());
		// COB_CODE: EXEC SQL
		//               SELECT POL_TYP_CD
		//                     ,PRI_RSK_ST_ABB
		//                     ,PLN_EXP_DT
		//                 INTO :DCLPOL-DTA-EXT.POL-TYP-CD
		//                     ,:DCLPOL-DTA-EXT.PRI-RSK-ST-ABB
		//                     ,:DCLPOL-DTA-EXT.PLN-EXP-DT
		//                 FROM POL_DTA_EXT
		//                WHERE POL_NBR    = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND POL_EFF_DT = :DCLPOL-DTA-EXT.POL-EFF-DT
		//           END-EXEC.
		polDtaExtDao.selectRec2(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt().getPolEffDt(), ws.getDclpolDtaExt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'POL_DTA_EXT'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("POL_DTA_EXT");
			// COB_CODE: MOVE '3320-GET-POLICY-DTL'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3320-GET-POLICY-DTL");
			// COB_CODE: MOVE SQLCODE        TO EFAL-DB2-ERR-SQLCODE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE 'DB2 ERROR READING POL_DTA_EXT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DB2 ERROR READING POL_DTA_EXT");
			// COB_CODE: MOVE SPACES         TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'POL_NBR= '
			//                  TP-POL-NBR(IX-TP)
			//                  '; POL_EFF_DT='
			//                  TP-POL-EFF-DT(IX-TP)
			//              DELIMITED BY SIZE
			//              INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("POL_NBR= ").append(ws.getTpPolicies(ws.getIxTp()).getPolNbrFormatted())
							.append("; POL_EFF_DT=").append(ws.getTpPolicies(ws.getIxTp()).getPolEffDt()).toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
		// UPDATE THE POLICY TABLE
		// COB_CODE: MOVE PLN-EXP-DT OF DCLPOL-DTA-EXT
		//                                       TO TP-POL-EXP-DT(IX-TP).
		ws.getTpPolicies(ws.getIxTp()).setPolExpDt(ws.getDclpolDtaExt().getPlnExpDt());
		// COB_CODE: MOVE POL-TYP-CD OF DCLPOL-DTA-EXT
		//                                       TO TP-POL-TYP-CD(IX-TP).
		ws.getTpPolicies(ws.getIxTp()).setPolTypCd(ws.getDclpolDtaExt().getPolTypCd());
		// COB_CODE: MOVE PRI-RSK-ST-ABB OF DCLPOL-DTA-EXT
		//                                       TO TP-POL-PRI-RSK-ST-ABB(IX-TP).
		ws.getTpPolicies(ws.getIxTp()).setPolPriRskStAbb(ws.getDclpolDtaExt().getPriRskStAbb());
	}

	/**Original name: 3340-GET-MIN-POL-EFF-DT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PREVIOUSLY WE WERE SENDING THE POLICY ORIGINAL INCEPTION DATE
	 *  WHICH WILL NO LONGER BE USED IN SERIES III.  SO THAT WE DON'T
	 *  HAVE TO REWORK HOW THE NOT_DAY_RQR TABLE IS SET UP, WE WILL
	 *  GET THE MINIMUM POLICY EFFECTIVE DATE OF THE POLICY.
	 *  THE DATE RANGE IN NOT_DAY_RQR IS JUST IDENTIFYING 'ARE WE
	 *  CANCELLING THE POLICY WITHIN IT'S FIRST YEAR, OR NOT?'.  THE MIN
	 *  POLICY EFFECTIVE DATE WILL BASICALLY DO THE SAME JOB THAT THE
	 *  ORIGINAL INCEPTION DATE WAS DOING.
	 * *****************************************************************</pre>*/
	private void getMinPolEffDt() {
		// COB_CODE: MOVE TP-POL-NBR(IX-TP)      TO WS-POL-NBR.
		ws.getWorkingStorageArea().setWsPolNbr(ws.getTpPolicies(ws.getIxTp()).getPolNbr());
		// COB_CODE: EXEC SQL
		//               SELECT MIN(POL_EFF_DT)
		//                 INTO :WS-MIN-POL-EFF-DT
		//                 FROM POL_DTA_EXT
		//                WHERE POL_NBR = :WS-POL-NBR
		//           END-EXEC.
		ws.getWorkingStorageArea().setWsMinPolEffDt(
				polDtaExtDao.selectByWsPolNbr1(ws.getWorkingStorageArea().getWsPolNbr(), ws.getWorkingStorageArea().getWsMinPolEffDt()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'POL_DTA_EXT'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("POL_DTA_EXT");
			// COB_CODE: MOVE '3340-GET-MIN-POL-EFF-DT'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3340-GET-MIN-POL-EFF-DT");
			// COB_CODE: MOVE SQLCODE        TO EFAL-DB2-ERR-SQLCODE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE 'DB2 ERROR READING POL_DTA_EXT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DB2 ERROR READING POL_DTA_EXT");
			// COB_CODE: MOVE SPACES         TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'POL_NBR: '
			//                  TP-POL-NBR(IX-TP)
			//              DELIMITED BY SIZE
			//              INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("POL_NBR: " + ws.getTpPolicies(ws.getIxTp()).getPolNbr());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
		// COB_CODE: MOVE WS-MIN-POL-EFF-DT      TO TP-MIN-POL-EFF-DT(IX-TP).
		ws.getTpPolicies(ws.getIxTp()).setMinPolEffDt(ws.getWorkingStorageArea().getWsMinPolEffDt());
	}

	/**Original name: 3400-DET-POLICY-NOT-EFF-DT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FOR EVERY POLICY IN THE TABLE -- DETERMINE WHAT THE            *
	 *  NOTIFICATION EFFECTIVE DATE SHOULD BE.                         *
	 *  IF ANY POLICY HAS A NOTIFCATION EFFECTIVE DATE GREATER THAN    *
	 *  OR EQUAL TO THAT POLICY'S EXPIRATION DATE -- THEN SET THE      *
	 *  DO NOT ADD NOTIFICATION SWITCH TO TRUE.                        *
	 * *****************************************************************</pre>*/
	private void detPolicyNotEffDt() {
		// COB_CODE: SET SW-ADD-NOTIFICATION     TO TRUE.
		ws.getSwitches().setAddNotificationFlag(true);
		// COB_CODE: SET IX-TP                   TO +1.
		ws.setIxTp(1);
		// COB_CODE: MOVE SPACES                 TO WS-NOT-EFF-DT-MAX.
		ws.getWorkingStorageArea().getWsNotEffDtMax().initWsNotEffDtMaxSpaces();
	}

	/**Original name: 3400-A<br>*/
	private String a3() {
		// COB_CODE: IF TP-END-OF-TABLE(IX-TP)
		//               GO TO 3400-EXIT
		//           END-IF.
		if (ws.getTpPolicies(ws.getIxTp()).isEndOfTable()) {
			// COB_CODE: GO TO 3400-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3410-GET-NBR-OF-NOT-DAY.
		getNbrOfNotDay();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3400-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3400-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3420-CALCULATE-NOT-EFF-DT.
		calculateNotEffDt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3400-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3400-EXIT
			return "";
		}
		// IF THE BILLING PROPOSED CANCEL DATE IS GREATER THAN THE
		// CALCULATED NOTIFICATION DATE THEN USE THE BILLING CANCEL DATE.
		// IF THE PROPOSED CANCEL DATE IS 9999-12-31, USE THE CALCULATED
		// NOTIFICATION DATE FROM CRS.
		// COB_CODE: IF TP-PROP-CAN-DT(IX-TP) > WS-NOT-EFF-DT
		//             AND
		//              TP-PROP-CAN-DT(IX-TP) NOT = CF-MAX-DATE
		//                                       TO TP-NOT-EFF-DT(IX-TP)
		//           ELSE
		//               MOVE WS-NOT-EFF-DT      TO TP-NOT-EFF-DT(IX-TP)
		//           END-IF.
		if (Conditions.gt(ws.getTpPolicies(ws.getIxTp()).getPropCanDt(), ws.getWorkingStorageArea().getWsNotEffDt())
				&& !Conditions.eq(ws.getTpPolicies(ws.getIxTp()).getPropCanDt(), ws.getConstantFields().getMaxDate())) {
			// COB_CODE: MOVE TP-PROP-CAN-DT(IX-TP)
			//                                   TO TP-NOT-EFF-DT(IX-TP)
			ws.getTpPolicies(ws.getIxTp()).setNotEffDt(ws.getTpPolicies(ws.getIxTp()).getPropCanDt());
		} else {
			// COB_CODE: MOVE WS-NOT-EFF-DT      TO TP-NOT-EFF-DT(IX-TP)
			ws.getTpPolicies(ws.getIxTp()).setNotEffDt(ws.getWorkingStorageArea().getWsNotEffDt());
		}
		// SINCE THE INSUFFICIENT DAYS WORKFLOW NEEDS TO BE PASSED
		// THE MAX NOTIFICATION EFFECTIVE DATE, WE NEED TO LOOP
		// THROUGH ALL OF THE POLICIES.
		// COB_CODE: IF TP-NOT-EFF-DT(IX-TP) >= TP-POL-EXP-DT(IX-TP)
		//               END-IF
		//           END-IF.
		if (Conditions.gte(ws.getTpPolicies(ws.getIxTp()).getNotEffDt(), ws.getTpPolicies(ws.getIxTp()).getPolExpDt())) {
			// COB_CODE: SET SW-DO-NOT-ADD-NOTIFICATION
			//                                   TO TRUE
			ws.getSwitches().setAddNotificationFlag(false);
			// COB_CODE: IF TP-NOT-EFF-DT(IX-TP) > WS-NOT-EFF-DT-MAX
			//                                   TO WS-NOT-EFF-DT-MAX
			//           END-IF
			if (Conditions.gt(ws.getTpPolicies(ws.getIxTp()).getNotEffDt(), ws.getWorkingStorageArea().getWsNotEffDtMax().getWsNotEffDtMaxBytes())) {
				// COB_CODE: MOVE TP-NOT-EFF-DT(IX-TP)
				//                               TO WS-NOT-EFF-DT-MAX
				ws.getWorkingStorageArea().getWsNotEffDtMax().setWsNotEffDtMaxFormatted(ws.getTpPolicies(ws.getIxTp()).getNotEffDtFormatted());
			}
		}
		// COB_CODE: SET IX-TP                   UP BY +1.
		ws.setIxTp(Trunc.toInt(ws.getIxTp() + 1, 9));
		// COB_CODE: GO TO 3400-A.
		return "3400-A";
	}

	/**Original name: 3410-GET-NBR-OF-NOT-DAY_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE SUBROUTINE TO GET THE NUMBER OF NOTIFICATION DAYS.    *
	 * *****************************************************************</pre>*/
	private void getNbrOfNotDay() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE WS-XZ0Y8000-ROW.
		initWsXz0y8000Row();
		// COB_CODE: MOVE TP-POL-PRI-RSK-ST-ABB(IX-TP)
		//                                       TO XZY800-POL-PRI-RSK-ST-ABB.
		ws.getXzy800GetNotDayRqrRow().setPolPriRskStAbb(ws.getTpPolicies(ws.getIxTp()).getPolPriRskStAbb());
		// COB_CODE: MOVE CF-ACT-NOT-IMPENDING   TO XZY800-ACT-NOT-TYP-CD.
		ws.getXzy800GetNotDayRqrRow().setActNotTypCd(ws.getConstantFields().getActNotImpending());
		// COB_CODE: MOVE TP-POL-TYP-CD(IX-TP)   TO XZY800-POL-TYP-CD.
		ws.getXzy800GetNotDayRqrRow().setPolTypCd(ws.getTpPolicies(ws.getIxTp()).getPolTypCd());
		// COB_CODE: MOVE TP-MIN-POL-EFF-DT(IX-TP)
		//                                       TO XZY800-MIN-POL-EFF-DT.
		ws.getXzy800GetNotDayRqrRow().setMinPolEffDt(ws.getTpPolicies(ws.getIxTp()).getMinPolEffDt());
		// COB_CODE: MOVE XZY9K0-USERID          TO XZY800-USERID.
		ws.getXzy800GetNotDayRqrRow().setUserid(ws.getWsXz0y90k0Row().getUserid());
		// COB_CODE: MOVE SPACES                 TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE LENGTH OF WS-XZ0Y8000-ROW
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) Xz0p90k0Data.Len.WS_XZ0Y8000_ROW));
		// COB_CODE: MOVE WS-XZ0Y8000-ROW        TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getWsXz0y8000RowFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-GET-NOT-DAY-RQR-UTY-PGM)
		//               COMMAREA(UBOC-RECORD)
		//               LENGTH(LENGTH OF UBOC-RECORD)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90K0", execContext).commarea(dfhcommarea.getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD)
				.link(ws.getConstantFields().getGetNotDayRqrUtyPgm(), new Xz0u8000());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3410-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE CF-GET-NOT-DAY-RQR-UTY-PGM
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getGetNotDayRqrUtyPgm());
			// COB_CODE: MOVE '3410-GET-NBR-OF-NOT-DAY'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3410-GET-NBR-OF-NOT-DAY");
			// COB_CODE: MOVE 'LINK TO GET NOT DAYS REQUIRED UTILITY FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO GET NOT DAYS REQUIRED UTILITY FAILED");
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZY9K0-CSR-ACT-NBR
			//                  '; ACT-NOT-TYP-CD='
			//                  XZY800-ACT-NOT-TYP-CD
			//                  '; POL-NBR='
			//                  TP-POL-NBR(IX-TP)
			//                  '; POL-PRI-RSK-ST-ABB='
			//                  XZY800-POL-PRI-RSK-ST-ABB
			//                  '; POL-TYP-CD='
			//                  XZY800-POL-TYP-CD
			//                  '; MIN-POL-EFF-DT='
			//                  XZY800-MIN-POL-EFF-DT
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "CSR-ACT-NBR=",
					ws.getWsXz0y90k0Row().getCsrActNbrFormatted(), "; ACT-NOT-TYP-CD=", ws.getXzy800GetNotDayRqrRow().getActNotTypCdFormatted(),
					"; POL-NBR=", ws.getTpPolicies(ws.getIxTp()).getPolNbrFormatted(), "; POL-PRI-RSK-ST-ABB=",
					ws.getXzy800GetNotDayRqrRow().getPolPriRskStAbbFormatted(), "; POL-TYP-CD=", ws.getXzy800GetNotDayRqrRow().getPolTypCdFormatted(),
					"; MIN-POL-EFF-DT=", ws.getXzy800GetNotDayRqrRow().getMinPolEffDtFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3410-EXIT
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3410-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3410-EXIT
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER   TO WS-XZ0Y8000-ROW.
		ws.setWsXz0y8000RowFormatted(dfhcommarea.getUbocRecord().getAppDataBufferFormatted());
		// COB_CODE: IF XZY800-NLBE-OCC
		//               GO TO 3410-EXIT
		//           END-IF.
		if (ws.getXzy800GetNotDayRqrRow().getNlbeInd().isNlbeOcc()) {
			// COB_CODE: SET UBOC-HALT-AND-RETURN
			//                                   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			// COB_CODE: MOVE 'NOT_DAY_REQ'      TO NLBE-FAILED-TABLE-OR-FILE
			//                                      NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedTableOrFile("NOT_DAY_REQ");
			ws.getNlbeCommon().setFailedColumnOrField("NOT_DAY_REQ");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE XZY800-NON-LOG-ERR-MSG
			//                                   TO WS-NONLOG-ERR-ALLTXT-TEXT
			ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getXzy800GetNotDayRqrRow().getNonLogErrMsg());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 3410-EXIT
			return;
		}
		// COB_CODE: IF XZY800-WARNINGS-OCC
		//               PERFORM 3411-OUTPUT-WARNINGS
		//           END-IF.
		if (ws.getXzy800GetNotDayRqrRow().getWarningInd().isXzy800WarningsOcc()) {
			// COB_CODE: PERFORM 3411-OUTPUT-WARNINGS
			rng3411OutputWarnings();
		}
	}

	/**Original name: 3411-OUTPUT-WARNINGS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LOOP THROUGH ALL OF THE WARNINGS THAT WERE PASSED BACK FROM    *
	 *  THE UTILITY AND CREATE WARNING MESSAGES.                       *
	 *      NOTE:  THE UTILITY'S WARNING AREA IS A DIFFERENT SIZE THAN *
	 *  THE PPC'S WARNING AREA, THUS THE DIFFERENT WARNING SECTION.    *
	 * *****************************************************************</pre>*/
	private void outputWarnings() {
		// COB_CODE: MOVE +0                     TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(((short) 0));
	}

	/**Original name: 3411-A<br>*/
	private String a4() {
		// COB_CODE: ADD +1                      TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(Trunc.toShort(1 + ws.getSubscripts().getWngIdx(), 4));
		// COB_CODE: IF XZY800-WARN-MSG(SS-WNG-IDX) = SPACES
		//               GO TO 3411-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzy800GetNotDayRqrRow().getWarnings(ws.getSubscripts().getWngIdx()).getXzy800WarnMsg())) {
			// COB_CODE: GO TO 3411-EXIT
			return "";
		}
		// COB_CODE: SET WS-NON-LOGGABLE-WARNING TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setWarning();
		// COB_CODE: MOVE 'NOT_DAY_REQ'          TO NLBE-FAILED-TABLE-OR-FILE
		//                                          NLBE-FAILED-COLUMN-OR-FIELD.
		ws.getNlbeCommon().setFailedTableOrFile("NOT_DAY_REQ");
		ws.getNlbeCommon().setFailedColumnOrField("NOT_DAY_REQ");
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE XZY800-WARN-MSG(SS-WNG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getXzy800GetNotDayRqrRow().getWarnings(ws.getSubscripts().getWngIdx()).getXzy800WarnMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-WNG-IDX-MAX
		//               GO TO 3411-EXIT
		//           END-IF.
		if (ws.getSubscripts().isWngIdxMax()) {
			// COB_CODE: GO TO 3411-EXIT
			return "";
		}
		// COB_CODE: GO TO 3411-A.
		return "3411-A";
	}

	/**Original name: 3420-CALCULATE-NOT-EFF-DT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALCULATE THE NOTIFICATION EFFECTIVE DATE FOR THAT POLICY      *
	 * *****************************************************************</pre>*/
	private void calculateNotEffDt() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE XZY800-NBR-OF-NOT-DAY  TO WS-NBR-OF-NOT-DAY.
		ws.getWorkingStorageArea().setWsNbrOfNotDay(Trunc.toShort(ws.getXzy800GetNotDayRqrRow().getNbrOfNotDay(), 4));
		// COB_CODE: EXEC SQL
		//               SELECT CURRENT DATE + :WS-NBR-OF-NOT-DAY DAYS
		//                 INTO :WS-NOT-EFF-DT
		//                 FROM SYSIBM.SYSDUMMY1
		//           END-EXEC.
		ws.getWorkingStorageArea().setWsNotEffDt(
				sysdummy1Dao.selectByWsNbrOfNotDay(ws.getWorkingStorageArea().getWsNbrOfNotDay(), ws.getWorkingStorageArea().getWsNotEffDt()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   GO TO 3420-EXIT
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: GO TO 3420-EXIT
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'SYSIBM DATE'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("SYSIBM DATE");
			// COB_CODE: MOVE '3420-CALCULATE-NOT-EFF-DT'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3420-CALCULATE-NOT-EFF-DT");
			// COB_CODE: MOVE SQLCODE        TO EFAL-DB2-ERR-SQLCODE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: IF SQLCODE < 0
			//                MOVE '-'       TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           ELSE
			//                MOVE '+'       TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           END-IF
			if (sqlca.getSqlcode() < 0) {
				// COB_CODE: MOVE '-'       TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'       TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			}
			// COB_CODE: MOVE 'DB2 ERROR READING SYSIBM TABLE'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DB2 ERROR READING SYSIBM TABLE");
			// COB_CODE: MOVE SQLERRMC       TO EFAL-DB2-ERR-SQLERRMC
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE WS-NBR-OF-NOT-DAY
			//                               TO WS-NBR-OF-NOT-DAY-X
			ws.getWorkingStorageArea().setWsNbrOfNotDayX(ws.getWorkingStorageArea().getWsNbrOfNotDay());
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZY9K0-CSR-ACT-NBR
			//                  ' NBR OF NOT DAY = '
			//                  WS-NBR-OF-NOT-DAY-X
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CSR-ACT-NBR=", ws.getWsXz0y90k0Row().getCsrActNbrFormatted(),
					" NBR OF NOT DAY = ", ws.getWorkingStorageArea().getWsNbrOfNotDayXAsString(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 3500-CREATE-NOTICE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CREATE THE IMPENDING NOTICE FOR THIS ACCOUNT                   *
	 * *****************************************************************
	 * * ADD THE ACT_NOT ROW</pre>*/
	private void createNotice() {
		// COB_CODE: PERFORM 3510-ADD-ACT-NOT.
		addActNot();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3500-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3500-EXIT
			return;
		}
		//* USE THE PREPARE INSURED POLICY SERVICE
		//* TO ADD OUT THE ACT_NOT_POL AND ACT_NOT_REC ROWS
		// COB_CODE: PERFORM 3520-PREPARE-INS-POL.
		prepareInsPol();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3500-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3500-EXIT
			return;
		}
		//* FOR PERSONAL LINES ACCOUNT ADD THE THIRD PARTIES TO ACT_NOT_REC
		// COB_CODE: IF SW-PERSONAL-LINES-ACT
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isPersonalLinesActFlag()) {
			// COB_CODE: PERFORM 3530-PREPARE-TTY
			prepareTty();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3500-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3500-EXIT
				return;
			}
		}
		//* PREPARE THE NOTICE (ATTACHES FORMS AND THE WORDINGS)
		// COB_CODE: PERFORM 3540-PREPARE-NOTICE.
		prepareNotice();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3500-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3500-EXIT
			return;
		}
		//* UPDATE THE STATUS TO 'PREPARED'
		// COB_CODE: PERFORM 3550-UPDATE-STATUS.
		updateStatus();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3500-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3500-EXIT
			return;
		}
	}

	/**Original name: 3510-ADD-ACT-NOT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE SERVICE TO ADD OUT THE ACT_NOT ROW                    *
	 * *****************************************************************</pre>*/
	private void addActNot() {
		// COB_CODE: PERFORM 3511-ALC-MEM-ACT-NOT-SVC.
		alcMemActNotSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3510-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3510-EXIT
			return;
		}
		// COB_CODE: SET SW-ACT-NOT-SVC-ALC      TO TRUE.
		ws.getSwitches().setActNotSvcAlcFlag(true);
		// COB_CODE: PERFORM 3512-SET-INPUT-ACT-NOT.
		setInputActNot();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3510-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3510-EXIT
			return;
		}
		// COB_CODE: PERFORM 3513-CALL-ADD-ACT-NOT-SVC.
		callAddActNotSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3510-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3510-EXIT
			return;
		}
	}

	/**Original name: 3511-ALC-MEM-ACT-NOT-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE ADD ACT NOT SERVICE                *
	 * *****************************************************************</pre>*/
	private void alcMemActNotSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS
		//                                       OF WS-PROXY-PROGRAM-AREA.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T0006-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x0006.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER OF WS-PROXY-PROGRAM-AREA)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE OF WS-PROXY-PROGRAM-AREA)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3511-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotSvc());
			// COB_CODE: MOVE '3511-ALC-MEM-ACT-NOT-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3511-ALC-MEM-ACT-NOT-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3511-ALC-MEM-ACT-NOT-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3511-ALC-MEM-ACT-NOT-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3511-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T0006-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER
		//                                       OF WS-PROXY-PROGRAM-AREA.
		wsXz0t0006Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x0006.class)));
		// COB_CODE: INITIALIZE WS-XZ0T0006-ROW.
		initWsXz0t0006Row();
	}

	/**Original name: 3512-SET-INPUT-ACT-NOT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS NEEDED FOR THE ADD ACT-NOT SERVICE              *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setInputActNot() {
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       OF WS-PROXY-PROGRAM-AREA
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-ADD-ACT-NOT         TO PPC-OPERATION
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getAddActNot());
		// COB_CODE: MOVE XZY9K0-USERID          TO XZT06I-USERID.
		wsXz0t0006Row.setXzt06iUserid(ws.getWsXz0y90k0Row().getUserid());
		// COB_CODE: MOVE XZY9K0-CSR-ACT-NBR     TO XZT06I-CSR-ACT-NBR.
		wsXz0t0006Row.setXzt06iCsrActNbr(ws.getWsXz0y90k0Row().getCsrActNbr());
		// COB_CODE: MOVE CF-ACT-NOT-IMPENDING   TO XZT06I-ACT-NOT-TYP-CD.
		wsXz0t0006Row.setXzt06iActNotTypCd(ws.getConstantFields().getActNotImpending());
		// COB_CODE: MOVE WS-CURRENT-DATE        TO XZT06I-NOT-DT.
		wsXz0t0006Row.setXzt06iNotDt(ws.getWorkingStorageArea().getWsCurrentTimestampX().getDateFld());
		// COB_CODE: MOVE MUT04O-TK-CLT-ID       TO XZT06I-TK-ACT-OWN-CLT-ID.
		wsXz0t0006Row.setXzt06iTkActOwnCltId(wsMu0t0004Row.getMut04oTkCltId());
		// COB_CODE: MOVE MUT04O-BSM-ADR-ID      TO XZT06I-TK-ACT-OWN-ADR-ID.
		wsXz0t0006Row.setXzt06iTkActOwnAdrId(wsMu0t0004Row.getMut04oBsmAdrId());
		// COB_CODE: MOVE WS-CURRENT-TIMESTAMP   TO XZT06I-TK-STA-MDF-TS.
		wsXz0t0006Row.setXzt06iTkStaMdfTs(ws.getWorkingStorageArea().getWsCurrentTimestampX().getWsCurrentTimestamp());
		// COB_CODE: MOVE CF-ACT-NOT-STATUS-STARTED
		//                                       TO XZT06I-TK-ACT-NOT-STA-CD.
		wsXz0t0006Row.setXzt06iTkActNotStaCd(ws.getConstantFields().getActNotStaCd().getStarted());
		// COB_CODE: MOVE MUT04O-SEG-CD          TO XZT06I-TK-SEG-CD.
		wsXz0t0006Row.setXzt06iTkSegCd(wsMu0t0004Row.getMut04oSegCd());
		// COB_CODE: MOVE AA-TOT-FEE-AMT         TO XZT06I-TOT-FEE-AMT.
		wsXz0t0006Row.setXzt06iTotFeeAmt(Trunc.toDecimal(ws.getAaTotFeeAmt(), 10, 2));
		// COB_CODE: MOVE MUT04O-BSM-ST-ABB      TO XZT06I-ST-ABB.
		wsXz0t0006Row.setXzt06iStAbb(wsMu0t0004Row.getMut04oBsmStAbb());
		// COB_CODE:      IF MUT04O-PRS-LIN-ACT-NBR NOT = SPACES
		//                                            TO XZT06I-TK-ACT-TYP-CD
		//                ELSE
		//           **  THE TERR NBR NEEDS TO BE FORMATTED LIKE THIS X-XXX
		//                                            TO XZT06I-TK-ACT-TYP-CD
		//                END-IF.
		if (!Characters.EQ_SPACE.test(wsMu0t0004Row.getMut04oPrsLinActNbr())) {
			// COB_CODE: SET SW-PERSONAL-LINES-ACT
			//                                   TO TRUE
			ws.getSwitches().setPersonalLinesActFlag(true);
			// COB_CODE: MOVE CF-PL-PRODUCER-NBR TO XZT06I-PDC-NBR
			wsXz0t0006Row.setXzt06iPdcNbr(ws.getConstantFields().getPlProducerNbr());
			// COB_CODE: MOVE CF-PL-PRODUCER-NM  TO XZT06I-PDC-NM
			wsXz0t0006Row.setXzt06iPdcNm(ws.getConstantFields().getPlProducerNmFormatted());
			// COB_CODE: MOVE CF-ATC-PERSONAL-LINES
			//                                   TO XZT06I-TK-ACT-TYP-CD
			wsXz0t0006Row.setXzt06iTkActTypCd(ws.getConstantFields().getAtcPersonalLines());
		} else {
			//*  THE TERR NBR NEEDS TO BE FORMATTED LIKE THIS X-XXX
			// COB_CODE: MOVE MUT04O-MKT-TER-NBR(1:1)
			//                                   TO WS-PRODUCER-NBR-START
			ws.getWorkingStorageArea().getWsProducerNbr().setStart2Formatted(wsMu0t0004Row.getMut04oMktTerNbrFormatted().substring((1) - 1, 1));
			// COB_CODE: MOVE MUT04O-MKT-TER-NBR(2:3)
			//                                   TO WS-PRODUCER-NBR-END
			ws.getWorkingStorageArea().getWsProducerNbr().setEnd(wsMu0t0004Row.getMut04oMktTerNbrFormatted().substring((2) - 1, 4));
			// COB_CODE: MOVE WS-PRODUCER-NBR    TO XZT06I-PDC-NBR
			wsXz0t0006Row.setXzt06iPdcNbr(ws.getWorkingStorageArea().getWsProducerNbr().getWsProducerNbrFormatted());
			// COB_CODE: MOVE MUT04O-MKT-SVC-DSY-NM
			//                                   TO XZT06I-PDC-NM
			wsXz0t0006Row.setXzt06iPdcNm(wsMu0t0004Row.getMut04oMktSvcDsyNm());
			// COB_CODE: MOVE CF-ATC-COMMERCIAL-LINES
			//                                   TO XZT06I-TK-ACT-TYP-CD
			wsXz0t0006Row.setXzt06iTkActTypCd(ws.getConstantFields().getAtcCommercialLines());
		}
	}

	/**Original name: 3513-CALL-ADD-ACT-NOT-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   CALL THE ADD ACT-NOT SERVICE                                  *
	 * *****************************************************************</pre>*/
	private void callAddActNotSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-ADD-ACT-NOT-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90K0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getAddActNotSvc(), new Xz0x0006());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3513-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotSvc());
			// COB_CODE: MOVE '3513-CALL-ADD-ACT-NOT-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3513-CALL-ADD-ACT-NOT-SVC");
			// COB_CODE: MOVE 'ADD ACT-NOT SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ADD ACT-NOT SERVICE CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3513-CALL-ADD-ACT-NOT-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3513-CALL-ADD-ACT-NOT-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3513-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-SVC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getAddActNotSvc());
		// COB_CODE: MOVE '3513-CALL-ADD-ACT-NOT-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3513-CALL-ADD-ACT-NOT-SVC");
		// COB_CODE: MOVE 'ADD ACT-NOT SERVICE'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("ADD ACT-NOT SERVICE");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3520-PREPARE-INS-POL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE SERVICE TO ADD THE POLICY INFORMATION TO ACT_NOT_POL  *
	 *  AND THE INSURED INFORMATION TO ACT_NOT_REC.                    *
	 * *****************************************************************</pre>*/
	private void prepareInsPol() {
		// COB_CODE: PERFORM 3521-ALC-MEM-PRP-INS-POL-SVC.
		alcMemPrpInsPolSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3520-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3520-EXIT
			return;
		}
		// COB_CODE: SET SW-PREPARE-INS-POL-ALC  TO TRUE.
		ws.getSwitches().setPrepareInsPolFlag(true);
		// COB_CODE: PERFORM 3522-SET-INPUT-PRP-INS-POL.
		rng3522SetInputPrpInsPol();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3520-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3520-EXIT
			return;
		}
		// COB_CODE: PERFORM 3523-CALL-PRP-INS-POL-SVC.
		callPrpInsPolSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3520-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3520-EXIT
			return;
		}
	}

	/**Original name: 3521-ALC-MEM-PRP-INS-POL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE PREPARE INSURED POLICY SERVICE     *
	 * *****************************************************************</pre>*/
	private void alcMemPrpInsPolSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS
		//                                       OF WS-PROXY-PROGRAM-AREA.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T90H0-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x90h0.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER OF WS-PROXY-PROGRAM-AREA)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE OF WS-PROXY-PROGRAM-AREA)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3521-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
			// COB_CODE: MOVE '3521-ALC-MEM-PRP-INS-POL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3521-ALC-MEM-PRP-INS-POL-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3521-ALC-MEM-PRP-INS-POL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3521-ALC-MEM-PRP-INS-POL-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3521-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T90H0-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER
		//                                       OF WS-PROXY-PROGRAM-AREA.
		wsXz0t90h0Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x90h0.class)));
		// COB_CODE: INITIALIZE WS-XZ0T90H0-ROW.
		initWsXz0t90h0Row();
	}

	/**Original name: 3522-SET-INPUT-PRP-INS-POL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS NEEDED FOR THE PREPARE INSURED POLICY SERVICE   *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setInputPrpInsPol() {
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       OF WS-PROXY-PROGRAM-AREA
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-PREPARE-INS-POL     TO PPC-OPERATION
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getPrepareInsPol());
		// COB_CODE: MOVE XZY9K0-USERID          TO XZT9HI-USERID.
		wsXz0t90h0Row.setiUserid(ws.getWsXz0y90k0Row().getUserid());
		// COB_CODE: MOVE XZY9K0-CSR-ACT-NBR     TO XZT9HI-CSR-ACT-NBR.
		wsXz0t90h0Row.setiCsrActNbr(ws.getWsXz0y90k0Row().getCsrActNbr());
		// COB_CODE: MOVE XZT06O-TK-NOT-PRC-TS   TO XZT9HI-TK-NOT-PRC-TS.
		wsXz0t90h0Row.setiTkNotPrcTs(wsXz0t0006Row.getXzt06oTkNotPrcTs());
		// COB_CODE: SET IX-TP                   TO +1.
		ws.setIxTp(1);
		// COB_CODE: MOVE +1                     TO SS-PL.
		ws.getSubscripts().setCl(((short) 1));
		// COB_CODE: COMPUTE WS-MAX-POL-ROWS = LENGTH OF XZT9HI-POLICY-LIST-TBL
		//                                   / LENGTH OF XZT9HI-POLICY-LIST.
		ws.getWorkingStorageArea()
				.setWsMaxPolRows((new AfDecimal(
						((((double) LServiceContractAreaXz0x90h0.Len.I_POLICY_LIST_TBL)) / LServiceContractAreaXz0x90h0.Len.I_POLICY_LIST), 9, 0))
								.toShort());
	}

	/**Original name: 3522-A<br>
	 * <pre>* IF WE HAVE REACHED THE END OF THE POLICY TABLE
	 * * THEN WE ARE DONE LOADING THE SERVICE INPUT POLICY LIST</pre>*/
	private String a5() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF TP-END-OF-TABLE(IX-TP)
		//               GO TO 3522-EXIT
		//           END-IF.
		if (ws.getTpPolicies(ws.getIxTp()).isEndOfTable()) {
			// COB_CODE: GO TO 3522-EXIT
			return "";
		}
		//* IF REACHED THE POLICY LIST MAX -- KICK OUT AN ERROR
		// COB_CODE: IF SS-PL > WS-MAX-POL-ROWS
		//               GO TO 3522-EXIT
		//           END-IF.
		if (ws.getSubscripts().getCl() > ws.getWorkingStorageArea().getWsMaxPolRows()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-LIMIT-EXCEEDED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspLimitExceeded();
			// COB_CODE: MOVE '3522-SET-INPUT-PRP-INS-POL'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3522-SET-INPUT-PRP-INS-POL");
			// COB_CODE: MOVE 'XZT9HI-POLICY-LIST-TBL HIT MAX'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("XZT9HI-POLICY-LIST-TBL HIT MAX");
			// COB_CODE: MOVE WS-MAX-POL-ROWS    TO WS-MAX-ROWS
			ws.getWorkingStorageArea().setWsMaxRows(ws.getWorkingStorageArea().getWsMaxPolRows());
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZY9K0-CSR-ACT-NBR
			//                  ' MAX ROWS = '
			//                  WS-MAX-ROWS
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CSR-ACT-NBR=", ws.getWsXz0y90k0Row().getCsrActNbrFormatted(),
					" MAX ROWS = ", ws.getWorkingStorageArea().getWsMaxRowsAsString(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3522-EXIT
			return "";
		}
		// COB_CODE: MOVE TP-POL-NBR(IX-TP)      TO XZT9HI-POL-NBR(SS-PL).
		wsXz0t90h0Row.setiPolNbr(ws.getSubscripts().getCl(), ws.getTpPolicies(ws.getIxTp()).getPolNbr());
		// COB_CODE: MOVE TP-POL-EFF-DT(IX-TP)   TO XZT9HI-POL-EFF-DT(SS-PL).
		wsXz0t90h0Row.setiPolEffDt(ws.getSubscripts().getCl(), ws.getTpPolicies(ws.getIxTp()).getPolEffDt());
		// COB_CODE: MOVE TP-POL-EXP-DT(IX-TP)   TO XZT9HI-POL-EXP-DT(SS-PL).
		wsXz0t90h0Row.setiPolExpDt(ws.getSubscripts().getCl(), ws.getTpPolicies(ws.getIxTp()).getPolExpDt());
		// COB_CODE: MOVE TP-NOT-EFF-DT(IX-TP)   TO XZT9HI-NOT-EFF-DT(SS-PL).
		wsXz0t90h0Row.setiNotEffDt(ws.getSubscripts().getCl(), ws.getTpPolicies(ws.getIxTp()).getNotEffDt());
		// COB_CODE: MOVE TP-POL-DUE-AMT(IX-TP)  TO XZT9HI-POL-DUE-AMT(SS-PL).
		wsXz0t90h0Row.setiPolDueAmt(ws.getSubscripts().getCl(), Trunc.toDecimal(ws.getTpPolicies(ws.getIxTp()).getPolDueAmt(), 10, 2));
		// COB_CODE: MOVE TP-POL-BIL-STA-CD(IX-TP)
		//                                       TO XZT9HI-POL-BIL-STA-CD(SS-PL).
		wsXz0t90h0Row.setiPolBilStaCd(ws.getSubscripts().getCl(), ws.getTpPolicies(ws.getIxTp()).getPolBilStaCd());
		// COB_CODE: ADD +1                      TO SS-PL.
		ws.getSubscripts().setCl(Trunc.toShort(1 + ws.getSubscripts().getCl(), 4));
		// COB_CODE: SET IX-TP                   UP BY +1.
		ws.setIxTp(Trunc.toInt(ws.getIxTp() + 1, 9));
		// COB_CODE: GO TO 3522-A.
		return "3522-A";
	}

	/**Original name: 3523-CALL-PRP-INS-POL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   CALL THE PREPARE INSURED POLICY SERVICE                       *
	 * *****************************************************************</pre>*/
	private void callPrpInsPolSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-PREPARE-INS-POL-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90K0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc(), new Xz0x90h0());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3523-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
			// COB_CODE: MOVE '3523-CALL-PRP-INS-POL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3523-CALL-PRP-INS-POL-SVC");
			// COB_CODE: MOVE 'PREPARE INS POL SVC CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("PREPARE INS POL SVC CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3523-CALL-PRP-INS-POL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3523-CALL-PRP-INS-POL-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3523-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
		// COB_CODE: MOVE '3523-CALL-PRP-INS-POL-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3523-CALL-PRP-INS-POL-SVC");
		// COB_CODE: MOVE 'PREPARE INS POL SVC'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("PREPARE INS POL SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3530-PREPARE-TTY_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ** NOTE **  ONLY IF IT IS A PERSONAL LINES ACCOUNT             *
	 *  CALL THE SERVICE TO ADD THE THIRD PARTIES TO THE ACT_NOT_REC   *
	 * *****************************************************************</pre>*/
	private void prepareTty() {
		// COB_CODE: PERFORM 3531-ALC-MEM-PREPARE-TTY-SVC.
		alcMemPrepareTtySvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3530-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3530-EXIT
			return;
		}
		// COB_CODE: SET SW-PREPARE-TTY-ALC      TO TRUE.
		ws.getSwitches().setPrepareTtyFlag(true);
		// COB_CODE: PERFORM 3532-SET-INPUT-PREPARE-TTY.
		setInputPrepareTty();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3530-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3530-EXIT
			return;
		}
		// COB_CODE: PERFORM 3533-CALL-PREPARE-TTY-SVC.
		callPrepareTtySvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3530-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3530-EXIT
			return;
		}
	}

	/**Original name: 3531-ALC-MEM-PREPARE-TTY-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE PREPARE TTY SERVICE                *
	 * *****************************************************************</pre>*/
	private void alcMemPrepareTtySvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS
		//                                       OF WS-PROXY-PROGRAM-AREA.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T90A0-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x90c0.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER OF WS-PROXY-PROGRAM-AREA)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE OF WS-PROXY-PROGRAM-AREA)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3531-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-TTY-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareTtySvc());
			// COB_CODE: MOVE '3531-ALC-MEM-PREPARE-TTY-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3531-ALC-MEM-PREPARE-TTY-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3531-ALC-MEM-PREPARE-TTY-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3531-ALC-MEM-PREPARE-TTY-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3531-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T90A0-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER
		//                                       OF WS-PROXY-PROGRAM-AREA.
		wsXz0t90a0Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x90c0.class)));
		// COB_CODE: INITIALIZE WS-XZ0T90A0-ROW.
		initWsXz0t90a0Row();
	}

	/**Original name: 3532-SET-INPUT-PREPARE-TTY_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS NEEDED FOR THE PREPARE TTY SERVICE              *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setInputPrepareTty() {
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       OF WS-PROXY-PROGRAM-AREA
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-PREPARE-TTY         TO PPC-OPERATION
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getPrepareTty());
		// COB_CODE: MOVE XZY9K0-USERID          TO XZT9AI-USERID.
		wsXz0t90a0Row.setiUserid(ws.getWsXz0y90k0Row().getUserid());
		// COB_CODE: MOVE XZY9K0-CSR-ACT-NBR     TO XZT9AI-CSR-ACT-NBR.
		wsXz0t90a0Row.setiCsrActNbr(ws.getWsXz0y90k0Row().getCsrActNbr());
		// COB_CODE: MOVE XZT06O-TK-NOT-PRC-TS   TO XZT9AI-TK-NOT-PRC-TS.
		wsXz0t90a0Row.setiTkNotPrcTs(wsXz0t0006Row.getXzt06oTkNotPrcTs());
	}

	/**Original name: 3533-CALL-PREPARE-TTY-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   CALL THE PREPARE THIRD PARTY SERVICE                          *
	 * *****************************************************************</pre>*/
	private void callPrepareTtySvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-PREPARE-TTY-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90K0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getPrepareTtySvc(), new Xz0x90a0());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3533-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-TTY-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareTtySvc());
			// COB_CODE: MOVE '3533-CALL-PREPARE-TTY-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3533-CALL-PREPARE-TTY-SVC");
			// COB_CODE: MOVE 'PREPARE TTY SVC CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("PREPARE TTY SVC CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3533-CALL-PREPARE-TTY-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3533-CALL-PREPARE-TTY-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3533-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-PREPARE-TTY-SVC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getPrepareTtySvc());
		// COB_CODE: MOVE '3533-CALL-PREPARE-TTY-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3533-CALL-PREPARE-TTY-SVC");
		// COB_CODE: MOVE 'PREPARE TTY SERVICE'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("PREPARE TTY SERVICE");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3540-PREPARE-NOTICE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE SERVICE PREPARE THE NOTICE                            *
	 *    WHICH WILL:                                                  *
	 *  (1) ATTACH THE FORMS -- WHICH WILL ADD THE APPROPRIATE ROWS TO *
	 *      ACT_NOT_FRM, ACT_NOT_POL_FRM, AND ACT_NOT_FRM_REC.         *
	 *  (1) ADD THE WORDINGS -- WHICH WILL ADD THE APPROPRIATE ROWS TO *
	 *      ACT_NOT_WRD.                                               *
	 * *****************************************************************</pre>*/
	private void prepareNotice() {
		// COB_CODE: PERFORM 3541-ALC-MEM-PRP-NOTICE-SVC.
		alcMemPrpNoticeSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3540-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3540-EXIT
			return;
		}
		// COB_CODE: SET SW-PREPARE-NOT-ALC      TO TRUE.
		ws.getSwitches().setPrepareNotFlag(true);
		// COB_CODE: PERFORM 3542-SET-INPUT-PRP-NOTICE.
		setInputPrpNotice();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3540-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3540-EXIT
			return;
		}
		// COB_CODE: PERFORM 3543-CALL-PRP-NOTICE-SVC.
		callPrpNoticeSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3540-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3540-EXIT
			return;
		}
	}

	/**Original name: 3541-ALC-MEM-PRP-NOTICE-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE PREPARE NOTICE SERVICE             *
	 * *****************************************************************</pre>*/
	private void alcMemPrpNoticeSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS
		//                                       OF WS-PROXY-PROGRAM-AREA.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T90J0-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x90j0.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER OF WS-PROXY-PROGRAM-AREA)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE OF WS-PROXY-PROGRAM-AREA)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3541-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-NOT-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareNotSvc());
			// COB_CODE: MOVE '3541-ALC-MEM-PRP-NOTICE-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3541-ALC-MEM-PRP-NOTICE-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3541-ALC-MEM-PRP-NOTICE-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3541-ALC-MEM-PRP-NOTICE-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3541-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T90J0-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER
		//                                       OF WS-PROXY-PROGRAM-AREA.
		wsXz0t90j0Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x90j0.class)));
		// COB_CODE: INITIALIZE WS-XZ0T90J0-ROW.
		initWsXz0t90j0Row();
	}

	/**Original name: 3542-SET-INPUT-PRP-NOTICE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS NEEDED FOR THE PREPARE NOTICE SERVICE           *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setInputPrpNotice() {
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       OF WS-PROXY-PROGRAM-AREA
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-PREPARE-NOT         TO PPC-OPERATION
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getPrepareNot());
		// COB_CODE: MOVE XZY9K0-USERID          TO XZT9JI-USERID.
		wsXz0t90j0Row.setiUserid(ws.getWsXz0y90k0Row().getUserid());
		// COB_CODE: MOVE XZY9K0-CSR-ACT-NBR     TO XZT9JI-CSR-ACT-NBR.
		wsXz0t90j0Row.setiCsrActNbr(ws.getWsXz0y90k0Row().getCsrActNbr());
		// COB_CODE: MOVE XZT06O-TK-NOT-PRC-TS   TO XZT9JI-TK-NOT-PRC-TS.
		wsXz0t90j0Row.setiTkNotPrcTs(wsXz0t0006Row.getXzt06oTkNotPrcTs());
	}

	/**Original name: 3543-CALL-PRP-NOTICE-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   CALL THE PREPARE NOTICE SERVICE                               *
	 * *****************************************************************</pre>*/
	private void callPrpNoticeSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-PREPARE-NOT-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90K0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getPrepareNotSvc(), new Xz0x90j0());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3543-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-NOT-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareNotSvc());
			// COB_CODE: MOVE '3543-CALL-PRP-NOTICE-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3543-CALL-PRP-NOTICE-SVC");
			// COB_CODE: MOVE 'PREPARE NOTICE SVC CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("PREPARE NOTICE SVC CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3543-CALL-PRP-NOTICE-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3543-CALL-PRP-NOTICE-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3543-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-PREPARE-NOT-SVC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getPrepareNotSvc());
		// COB_CODE: MOVE '3543-CALL-PRP-NOTICE-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3543-CALL-PRP-NOTICE-SVC");
		// COB_CODE: MOVE 'PREPARE NOTICE SVC'   TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("PREPARE NOTICE SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3550-UPDATE-STATUS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE SERVICE UPDATE NOTIFICATION STATUS TO MARK NOTICE AS  *
	 *  'PREPARED'.                                                    *
	 * *****************************************************************</pre>*/
	private void updateStatus() {
		// COB_CODE: PERFORM 3551-ALC-MEM-UPD-STATUS-SVC.
		alcMemUpdStatusSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3550-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3550-EXIT
			return;
		}
		// COB_CODE: SET SW-UPD-NOT-STA-ALC      TO TRUE.
		ws.getSwitches().setUpdNotStaFlag(true);
		// COB_CODE: PERFORM 3552-SET-INPUT-UPD-STATUS.
		setInputUpdStatus();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3550-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3550-EXIT
			return;
		}
		// COB_CODE: PERFORM 3553-CALL-UPD-STATUS-SVC.
		callUpdStatusSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3550-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3550-EXIT
			return;
		}
	}

	/**Original name: 3551-ALC-MEM-UPD-STATUS-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE UPDATE NOTIFICATION STATUS SERVICE *
	 * *****************************************************************</pre>*/
	private void alcMemUpdStatusSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS
		//                                       OF WS-PROXY-PROGRAM-AREA.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T90M0-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x90m0.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER OF WS-PROXY-PROGRAM-AREA)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE OF WS-PROXY-PROGRAM-AREA)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3551-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-UPD-NOT-STA-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getUpdNotStaSvc());
			// COB_CODE: MOVE '3551-ALC-MEM-UPD-STATUS-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3551-ALC-MEM-UPD-STATUS-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3551-ALC-MEM-UPD-STATUS-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3551-ALC-MEM-UPD-STATUS-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3551-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T90M0-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER
		//                                       OF WS-PROXY-PROGRAM-AREA.
		wsXz0t90m0Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x90m0.class)));
		// COB_CODE: INITIALIZE WS-XZ0T90M0-ROW.
		initWsXz0t90m0Row();
	}

	/**Original name: 3552-SET-INPUT-UPD-STATUS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS FOR THE UPDATE NOTIFICATION STATUS SERVICE      *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setInputUpdStatus() {
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       OF WS-PROXY-PROGRAM-AREA
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-UPD-NOT-STA         TO PPC-OPERATION
		//                                       OF WS-PROXY-PROGRAM-AREA.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getUpdNotSta());
		// COB_CODE: MOVE XZY9K0-USERID          TO XZT9MI-USERID.
		wsXz0t90m0Row.setiUserid(ws.getWsXz0y90k0Row().getUserid());
		// COB_CODE: MOVE XZY9K0-CSR-ACT-NBR     TO XZT9MI-CSR-ACT-NBR.
		wsXz0t90m0Row.setiCsrActNbr(ws.getWsXz0y90k0Row().getCsrActNbr());
		// COB_CODE: MOVE XZT06O-TK-NOT-PRC-TS   TO XZT9MI-TK-NOT-PRC-TS.
		wsXz0t90m0Row.setiTkNotPrcTs(wsXz0t0006Row.getXzt06oTkNotPrcTs());
		// COB_CODE: MOVE CF-ACT-NOT-STATUS-PREPARED
		//                                       TO XZT9MI-TK-ACT-NOT-STA-CD.
		wsXz0t90m0Row.setiTkActNotStaCd(ws.getConstantFields().getActNotStaCd().getPrepared());
	}

	/**Original name: 3553-CALL-UPD-STATUS-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   CALL THE UPDATE NOTIFICATION STATUS SERVICE                   *
	 * *****************************************************************</pre>*/
	private void callUpdStatusSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-UPD-NOT-STA-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90K0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getUpdNotStaSvc(), new Xz0x90m0());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3553-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-UPD-NOT-STA-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getUpdNotStaSvc());
			// COB_CODE: MOVE '3553-CALL-UPD-STATUS-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3553-CALL-UPD-STATUS-SVC");
			// COB_CODE: MOVE 'UPDATE NOTIFICATION STATUS SVC CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE NOTIFICATION STATUS SVC CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3553-CALL-UPD-STATUS-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3553-CALL-UPD-STATUS-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3553-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-UPD-NOT-STA-SVC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getUpdNotStaSvc());
		// COB_CODE: MOVE '3553-CALL-UPD-STATUS-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3553-CALL-UPD-STATUS-SVC");
		// COB_CODE: MOVE 'UPDATE NOT STA SVC'   TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("UPDATE NOT STA SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3600-WRITE-OUT-RESPONSE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  WRITE OUT THE RESPONSE                                         *
	 * *****************************************************************
	 *  MOVE OUT THE NOTIFICATION PROCESS TIMESTAMP</pre>*/
	private void writeOutResponse() {
		Halrresp halrresp = null;
		// COB_CODE: MOVE XZT06O-TK-NOT-PRC-TS   TO XZY9K0-NOT-PRC-TS.
		ws.getWsXz0y90k0Row().setNotPrcTs(wsXz0t0006Row.getXzt06oTkNotPrcTs());
		// COB_CODE: SET HALRRESP-WRITE-FUNC     TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM-PREPARE-IMP-NOT
		//                                       TO HALRRESP-BUS-OBJ-NM.
		ws.getWsHalrrespLinkage().setBusObjNm(ws.getWorkingStorageArea().getWsBusObjNmPrepareImpNot());
		// COB_CODE: MOVE LENGTH OF WS-XZ0Y90K0-ROW
		//                                       TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(((short) WsXz0y90a0Row.Len.WS_XZ0Y90A0_ROW));
		// COB_CODE: CALL HALRRESP-HALRRESP-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRRESP-LINKAGE
		//                WS-XZ0Y90K0-ROW.
		halrresp = Halrresp.getInstance();
		halrresp.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrrespLinkage(), ws.getWsXz0y90k0Row());
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowKeyReplaceStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning()
				&& dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWorkingStorageArea().getWsProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkingStorageArea().getWsProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90K0", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWorkingStorageArea().getWsProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getUbocRecord().getCommInfo()
				.setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getWsProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getUbocRecord().getCommInfo()
				.setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWorkingStorageArea().getWsApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: 9100-FREE-MEM-FOR-SERVICES_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE SERVICES  - IF ALLOCATED               *
	 * *****************************************************************</pre>*/
	private void freeMemForServices() {
		// COB_CODE: IF SW-INSURED-DETAIL-ALC
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isInsuredDetailAlcFlag()) {
			// COB_CODE: PERFORM 9110-FREE-MEM-INSURED-DTL-SVC
			freeMemInsuredDtlSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9100-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9100-EXIT
				return;
			}
		}
		// COB_CODE: IF SW-ACT-NOT-SVC-ALC
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isActNotSvcAlcFlag()) {
			// COB_CODE: PERFORM 9130-FREE-MEM-ACT-NOT-SVC
			freeMemActNotSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9100-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9100-EXIT
				return;
			}
		}
		// COB_CODE: IF SW-PREPARE-INS-POL-ALC
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isPrepareInsPolFlag()) {
			// COB_CODE: PERFORM 9140-FREE-MEM-PRP-INS-POL-SVC
			freeMemPrpInsPolSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9100-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9100-EXIT
				return;
			}
		}
		// COB_CODE: IF SW-PREPARE-NOT-ALC
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isPrepareNotFlag()) {
			// COB_CODE: PERFORM 9150-FREE-MEM-PRP-NOTICE-SVC
			freeMemPrpNoticeSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9100-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9100-EXIT
				return;
			}
		}
		// COB_CODE: IF SW-PREPARE-TTY-ALC
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isPrepareTtyFlag()) {
			// COB_CODE: PERFORM 9160-FREE-MEM-PREPARE-TTY-SVC
			freeMemPrepareTtySvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9100-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9100-EXIT
				return;
			}
		}
		// COB_CODE: IF SW-UPD-NOT-STA-ALC
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isUpdNotStaFlag()) {
			// COB_CODE: PERFORM 9170-FREE-MEM-UPD-STATUS-SVC
			freeMemUpdStatusSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9100-EXIT
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9100-EXIT
				return;
			}
		}
	}

	/**Original name: 9110-FREE-MEM-INSURED-DTL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE INSURED DETAIL SERVICE                 *
	 * *****************************************************************</pre>*/
	private void freeMemInsuredDtlSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-MU0T0004-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsMu0t0004Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-INSURED-DETAIL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc());
			// COB_CODE: MOVE '9110-FREE-MEM-INSURED-DTL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9110-FREE-MEM-INSURED-DTL-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9110-FREE-MEM-INSURED-DTL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9110-FREE-MEM-INSURED-DTL-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9130-FREE-MEM-ACT-NOT-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE MAINTAIN ACT_NOT SERVICE               *
	 * *****************************************************************</pre>*/
	private void freeMemActNotSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T0006-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t0006Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotSvc());
			// COB_CODE: MOVE '9130-FREE-MEM-ACT-NOT-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9130-FREE-MEM-ACT-NOT-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9130-FREE-MEM-ACT-NOT-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9130-FREE-MEM-ACT-NOT-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9140-FREE-MEM-PRP-INS-POL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE PREPARE INSURED POLICY SERVICE         *
	 * *****************************************************************</pre>*/
	private void freeMemPrpInsPolSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T90H0-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t90h0Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
			// COB_CODE: MOVE '9140-FREE-MEM-PRP-INS-POL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9140-FREE-MEM-PRP-INS-POL-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9140-FREE-MEM-PRP-INS-POL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9140-FREE-MEM-PRP-INS-POL-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9150-FREE-MEM-PRP-NOTICE-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE PREPARE NOTICE SERVICE                 *
	 * *****************************************************************</pre>*/
	private void freeMemPrpNoticeSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T90J0-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t90j0Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-NOT-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareNotSvc());
			// COB_CODE: MOVE '9150-FREE-MEM-PRP-NOTICE-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9150-FREE-MEM-PRP-NOTICE-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9150-FREE-MEM-PRP-NOTICE-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9150-FREE-MEM-PRP-NOTICE-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9160-FREE-MEM-PREPARE-TTY-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE PREPARE THIRD PARTY SERVICE            *
	 * *****************************************************************</pre>*/
	private void freeMemPrepareTtySvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T90A0-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t90a0Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-TTY-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareTtySvc());
			// COB_CODE: MOVE '9160-FREE-MEM-PREPARE-TTY-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9160-FREE-MEM-PREPARE-TTY-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9160-FREE-MEM-PREPARE-TTY-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9160-FREE-MEM-PREPARE-TTY-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9170-FREE-MEM-UPD-STATUS-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE UPDATE NOTIFICATION STATUS SERVICE     *
	 * *****************************************************************</pre>*/
	private void freeMemUpdStatusSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T90M0-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t90m0Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-UPD-NOT-STA-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getUpdNotStaSvc());
			// COB_CODE: MOVE '9170-FREE-MEM-UPD-STATUS-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9170-FREE-MEM-UPD-STATUS-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9170-FREE-MEM-UPD-STATUS-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getWsProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9170-FREE-MEM-UPD-STATUS-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getWsEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getWsEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9900-CHECK-ERRORS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   CHECK FOR ERROR, NLBE, OR WARNING RETURNED FROM SERVICE      *
	 * ****************************************************************</pre>*/
	private void checkErrors() {
		// COB_CODE: IF PPC-NO-ERROR-CODE OF WS-PROXY-PROGRAM-AREA
		//               GO TO 9900-EXIT
		//           END-IF.
		if (ws.getWsProxyProgramArea().getPpcErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: GO TO 9900-EXIT
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN PPC-FATAL-ERROR-CODE OF WS-PROXY-PROGRAM-AREA
		//                   PERFORM 9910-SET-FATAL-ERROR
		//               WHEN PPC-NLBE-CODE OF WS-PROXY-PROGRAM-AREA
		//                   PERFORM 9920-SET-NLBE
		//               WHEN PPC-WARNING-CODE OF WS-PROXY-PROGRAM-AREA
		//                   PERFORM 9930-SET-WARNING
		//           END-EVALUATE.
		switch (ws.getWsProxyProgramArea().getPpcErrorReturnCode().getDsdErrorReturnCodeFormatted()) {

		case DsdErrorReturnCode.FATAL_ERROR_CODE:// COB_CODE: PERFORM 9910-SET-FATAL-ERROR
			setFatalError();
			break;

		case DsdErrorReturnCode.NLBE_CODE:// COB_CODE: PERFORM 9920-SET-NLBE
			rng9920SetNlbe();
			break;

		case DsdErrorReturnCode.WARNING_CODE:// COB_CODE: PERFORM 9930-SET-WARNING
			rng9930SetWarning();
			break;

		default:
			break;
		}
	}

	/**Original name: 9910-SET-FATAL-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET FOR FATAL ERROR                                           *
	 *   SAVE OFF ANY ERROR MESSAGE THAT WAS RECEIVED FROM THE CALLED  *
	 *   SERVICE IN ORDER TO PUT IT IN THE DISPLAY AREAS AFTER LOGGING *
	 *   THE BUSINESS PROCESS ERROR.                                   *
	 *   THIS WILL ALLOW US TO DISPLAY THE ACTUAL ERROR THAT OCCURRED  *
	 *   IN THE CALLED SERVICE TO THE USER.                            *
	 * *****************************************************************</pre>*/
	private void setFatalError() {
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET ETRA-CICS-WEB-RECEIVE   TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
		// COB_CODE: MOVE WS-EC-MODULE           TO EFAL-ERR-OBJECT-NAME.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkingStorageArea().getWsErrorCheckInfo().getModule());
		// COB_CODE: MOVE WS-EC-PARAGRAPH        TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph(ws.getWorkingStorageArea().getWsErrorCheckInfo().getParagraph());
		// COB_CODE: MOVE PPC-FATAL-ERROR-MESSAGE
		//                                       OF WS-PROXY-PROGRAM-AREA
		//                                       TO EFAL-ERR-COMMENT
		//                                          EFAL-OBJ-DATA-KEY
		//                                          ES-01-FATAL-ERROR-MSG.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		ws.getEs01FatalErrorMsg().setEs01FatalErrorMsgFormatted(ws.getWsProxyProgramArea().getPpcFatalErrorMessageFormatted());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
		// COB_CODE: MOVE ES-01-FAILED-MODULE    TO UBOC-FAILED-MODULE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
				.setFailedModule(ws.getEs01FatalErrorMsg().getFailedModule());
		// COB_CODE: MOVE ES-01-FAILED-PARAGRAPH TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
				.setFailedParagraph(ws.getEs01FatalErrorMsg().getFailedParagraph());
		// COB_CODE: MOVE ES-01-SQLCODE-DISPLAY  TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
				.setSqlcodeDisplay(ws.getEs01FatalErrorMsg().getSqlcodeDisplay());
		// COB_CODE: MOVE ES-01-EIBRESP-DISPLAY  TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
				.setEibrespDisplay(ws.getEs01FatalErrorMsg().getEibrespDisplay());
		// COB_CODE: MOVE ES-01-EIBRESP2-DISPLAY TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
				.setEibresp2Display(ws.getEs01FatalErrorMsg().getEibresp2Display());
	}

	/**Original name: 9920-SET-NLBE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR NLBE                                                 *
	 * ****************************************************************</pre>*/
	private void setNlbe() {
		// COB_CODE: MOVE +0                     TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(((short) 0));
	}

	/**Original name: 9920-A<br>*/
	private String a6() {
		// COB_CODE: ADD +1                      TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(Trunc.toShort(1 + ws.getSubscripts().getMsgIdx(), 4));
		// COB_CODE: IF PPC-NON-LOG-ERR-MSG OF WS-PROXY-PROGRAM-AREA(SS-MSG-IDX)
		//                                       = SPACES
		//               GO TO 9920-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg())) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// we are setting UBOC-HALT-AND-RETURN to true so that if a service
		// gets an NLBE, the service ends.
		// COB_CODE: SET UBOC-HALT-AND-RETURN    TO TRUE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setBusErr();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO NLBE-FAILED-TABLE-OR-FILE.
		ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getWsErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO NLBE-FAILED-COLUMN-OR-FIELD.
		ws.getNlbeCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getWsErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-NON-LOG-ERR-MSG OF WS-PROXY-PROGRAM-AREA(SS-MSG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-MSG-IDX-MAX
		//               GO TO 9920-EXIT
		//           END-IF.
		if (ws.getSubscripts().isMsgIdxMax()) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// COB_CODE: GO TO 9920-A.
		return "9920-A";
	}

	/**Original name: 9930-SET-WARNING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR WARNING                                              *
	 * ****************************************************************</pre>*/
	private void setWarning() {
		// COB_CODE: MOVE +0                     TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(((short) 0));
	}

	/**Original name: 9930-A<br>*/
	private String a7() {
		// COB_CODE: ADD +1                      TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(Trunc.toShort(1 + ws.getSubscripts().getWngIdx(), 4));
		// COB_CODE: IF PPC-WARN-MSG OF WS-PROXY-PROGRAM-AREA(SS-WNG-IDX)
		//                                       = SPACES
		//               GO TO 9930-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg())) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: SET WS-NON-LOGGABLE-WARNING TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setWarning();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO UWRN-FAILED-TABLE-OR-FILE.
		ws.getUwrnCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getWsErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO UWRN-FAILED-COLUMN-OR-FIELD.
		ws.getUwrnCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getWsErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-WARN-MSG OF WS-PROXY-PROGRAM-AREA(SS-WNG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-WNG-IDX-MAX
		//               GO TO 9930-EXIT
		//           END-IF.
		if (ws.getSubscripts().isWngIdxMax()) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: GO TO 9930-A.
		return "9930-A";
	}

	/**Original name: RNG_3220-LOAD-PCN-POLICIES_FIRST_SENTENCES-_-3220-EXIT<br>*/
	private void rng3220LoadPcnPolicies() {
		String retcode = "";
		boolean goto3220A = false;
		loadPcnPolicies();
		do {
			goto3220A = false;
			retcode = a();
		} while (retcode.equals("3220-A"));
	}

	/**Original name: RNG_3230-CALCULATE-FEE-AMT_FIRST_SENTENCES-_-3230-EXIT<br>*/
	private void rng3230CalculateFeeAmt() {
		String retcode = "";
		boolean goto3230A = false;
		calculateFeeAmt();
		do {
			goto3230A = false;
			retcode = a1();
		} while (retcode.equals("3230-A"));
	}

	/**Original name: RNG_3300-GET-POLICY-DETAIL_FIRST_SENTENCES-_-3300-EXIT<br>*/
	private void rng3300GetPolicyDetail() {
		String retcode = "";
		boolean goto3300A = false;
		getPolicyDetail();
		do {
			goto3300A = false;
			retcode = a2();
		} while (retcode.equals("3300-A"));
	}

	/**Original name: RNG_3400-DET-POLICY-NOT-EFF-DT_FIRST_SENTENCES-_-3400-EXIT<br>*/
	private void rng3400DetPolicyNotEffDt() {
		String retcode = "";
		boolean goto3400A = false;
		detPolicyNotEffDt();
		do {
			goto3400A = false;
			retcode = a3();
		} while (retcode.equals("3400-A"));
	}

	/**Original name: RNG_3411-OUTPUT-WARNINGS_FIRST_SENTENCES-_-3411-EXIT<br>*/
	private void rng3411OutputWarnings() {
		String retcode = "";
		boolean goto3411A = false;
		outputWarnings();
		do {
			goto3411A = false;
			retcode = a4();
		} while (retcode.equals("3411-A"));
	}

	/**Original name: RNG_3522-SET-INPUT-PRP-INS-POL_FIRST_SENTENCES-_-3522-EXIT<br>*/
	private void rng3522SetInputPrpInsPol() {
		String retcode = "";
		boolean goto3522A = false;
		setInputPrpInsPol();
		do {
			goto3522A = false;
			retcode = a5();
		} while (retcode.equals("3522-A"));
	}

	/**Original name: RNG_9920-SET-NLBE_FIRST_SENTENCES-_-9920-EXIT<br>*/
	private void rng9920SetNlbe() {
		String retcode = "";
		boolean goto9920A = false;
		setNlbe();
		do {
			goto9920A = false;
			retcode = a6();
		} while (retcode.equals("9920-A"));
	}

	/**Original name: RNG_9930-SET-WARNING_FIRST_SENTENCES-_-9930-EXIT<br>*/
	private void rng9930SetWarning() {
		String retcode = "";
		boolean goto9930A = false;
		setWarning();
		do {
			goto9930A = false;
			retcode = a7();
		} while (retcode.equals("9930-A"));
	}

	public void initWsXz0y90k0Row() {
		ws.getWsXz0y90k0Row().setCsrActNbr("");
		ws.getWsXz0y90k0Row().setNotPrcTs("");
		ws.getWsXz0y90k0Row().setUserid("");
	}

	public void initPpcMemoryAllocationParms() {
		ws.getWsProxyProgramArea().setPpcServiceDataSize(0);
	}

	public void initWsMu0t0004Row() {
		wsMu0t0004Row.setMut04iTkCltId("");
		wsMu0t0004Row.setMut04iTkCiorShwObjKey("");
		wsMu0t0004Row.setMut04iTkAdrSeqNbr(0);
		wsMu0t0004Row.setMut04iUsrId("");
		wsMu0t0004Row.setMut04iActNbr("");
		wsMu0t0004Row.setMut04iTerNbr("");
		wsMu0t0004Row.setMut04iAsOfDt("");
		wsMu0t0004Row.setMut04iFein("");
		wsMu0t0004Row.setMut04iSsn("");
		wsMu0t0004Row.setMut04iPhnAcd("");
		wsMu0t0004Row.setMut04iPhnPfxNbr("");
		wsMu0t0004Row.setMut04iPhnLinNbr("");
		wsMu0t0004Row.setMut04oTkCltId("");
		wsMu0t0004Row.setMut04oCnIdvNmInd(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oCnDsyNm("");
		wsMu0t0004Row.setMut04oCnSrNm("");
		wsMu0t0004Row.setMut04oCnFstNm("");
		wsMu0t0004Row.setMut04oCnMdlNm("");
		wsMu0t0004Row.setMut04oCnLstNm("");
		wsMu0t0004Row.setMut04oCnSfx("");
		wsMu0t0004Row.setMut04oCnPfx("");
		wsMu0t0004Row.setMut04oCiClientId("");
		wsMu0t0004Row.setMut04oCiIdvNmInd(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oCiDsyNm("");
		wsMu0t0004Row.setMut04oCiSrNm("");
		wsMu0t0004Row.setMut04oCiFstNm("");
		wsMu0t0004Row.setMut04oCiMdlNm("");
		wsMu0t0004Row.setMut04oCiLstNm("");
		wsMu0t0004Row.setMut04oCiSfx("");
		wsMu0t0004Row.setMut04oCiPfx("");
		wsMu0t0004Row.setMut04oBsmAdrSeqNbr(0);
		wsMu0t0004Row.setMut04oBsmAdrId("");
		wsMu0t0004Row.setMut04oBsmLin1Adr("");
		wsMu0t0004Row.setMut04oBsmLin2Adr("");
		wsMu0t0004Row.setMut04oBsmCit("");
		wsMu0t0004Row.setMut04oBsmStAbb("");
		wsMu0t0004Row.setMut04oBsmPstCd("");
		wsMu0t0004Row.setMut04oBsmCty("");
		wsMu0t0004Row.setMut04oBsmCtrCd("");
		wsMu0t0004Row.setMut04oBslAdrSeqNbr(0);
		wsMu0t0004Row.setMut04oBslAdrId("");
		wsMu0t0004Row.setMut04oBslLin1Adr("");
		wsMu0t0004Row.setMut04oBslLin2Adr("");
		wsMu0t0004Row.setMut04oBslCit("");
		wsMu0t0004Row.setMut04oBslStAbb("");
		wsMu0t0004Row.setMut04oBslPstCd("");
		wsMu0t0004Row.setMut04oBslCty("");
		wsMu0t0004Row.setMut04oBslCtrCd("");
		wsMu0t0004Row.setMut04oBusPhnAcd("");
		wsMu0t0004Row.setMut04oBusPhnPfxNbr("");
		wsMu0t0004Row.setMut04oBusPhnLinNbr("");
		wsMu0t0004Row.setMut04oBusPhnExtNbr("");
		wsMu0t0004Row.setMut04oBusPhnNbrFmt("");
		wsMu0t0004Row.setMut04oFaxPhnAcd("");
		wsMu0t0004Row.setMut04oFaxPhnPfxNbr("");
		wsMu0t0004Row.setMut04oFaxPhnLinNbr("");
		wsMu0t0004Row.setMut04oFaxPhnExtNbr("");
		wsMu0t0004Row.setMut04oFaxPhnNbrFmt("");
		wsMu0t0004Row.setMut04oCtcPhnAcd("");
		wsMu0t0004Row.setMut04oCtcPhnPfxNbr("");
		wsMu0t0004Row.setMut04oCtcPhnLinNbr("");
		wsMu0t0004Row.setMut04oCtcPhnExtNbr("");
		wsMu0t0004Row.setMut04oCtcPhnNbrFmt("");
		wsMu0t0004Row.setMut04oFein("");
		wsMu0t0004Row.setMut04oSsn("");
		wsMu0t0004Row.setMut04oFeinFmt("");
		wsMu0t0004Row.setMut04oSsnFmt("");
		wsMu0t0004Row.setMut04oLegEtyCd("");
		wsMu0t0004Row.setMut04oLegEtyDes("");
		wsMu0t0004Row.setMut04oTobCd("");
		wsMu0t0004Row.setMut04oTobDes("");
		wsMu0t0004Row.setMut04oSicCd("");
		wsMu0t0004Row.setMut04oSicDes("");
		wsMu0t0004Row.setMut04oSegCd("");
		wsMu0t0004Row.setMut04oSegDes("");
		wsMu0t0004Row.setMut04oPcPsptActInd(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oPcActNbr("");
		wsMu0t0004Row.setMut04oPcActNbr2("");
		wsMu0t0004Row.setMut04oPcActNbrFmt("");
		wsMu0t0004Row.setMut04oPcActNbr2Fmt("");
		wsMu0t0004Row.setMut04oPsptActNbr("");
		wsMu0t0004Row.setMut04oPsptActNbrFmt("");
		wsMu0t0004Row.setMut04oActNmId(0);
		wsMu0t0004Row.setMut04oAct2NmId(0);
		wsMu0t0004Row.setMut04oObjCd("");
		wsMu0t0004Row.setMut04oObjCd2("");
		wsMu0t0004Row.setMut04oObjDes("");
		wsMu0t0004Row.setMut04oObjDes2("");
		wsMu0t0004Row.setMut04oSinWcActNbr("");
		wsMu0t0004Row.setMut04oSinWcActNbrFmt("");
		wsMu0t0004Row.setMut04oSinWcActNmId(0);
		wsMu0t0004Row.setMut04oSinWcObjCd("");
		wsMu0t0004Row.setMut04oSinWcObjDes("");
		wsMu0t0004Row.setMut04oSplBillActNbr("");
		wsMu0t0004Row.setMut04oSplBillActNbrFmt("");
		wsMu0t0004Row.setMut04oSplBillActNmId(0);
		wsMu0t0004Row.setMut04oSplBillObjCd("");
		wsMu0t0004Row.setMut04oSplBillObjDes("");
		wsMu0t0004Row.setMut04oPrsLinActNbr("");
		wsMu0t0004Row.setMut04oPrsLinActNbrFmt("");
		wsMu0t0004Row.setMut04oPrsLinActNmId(0);
		wsMu0t0004Row.setMut04oPrsLinObjCd("");
		wsMu0t0004Row.setMut04oPrsLinObjDes("");
		wsMu0t0004Row.setMut04oCstNbr("");
		wsMu0t0004Row.setMut04oMktCltId("");
		wsMu0t0004Row.setMut04oMktTerNbr("");
		wsMu0t0004Row.setMut04oMktSvcDsyNm("");
		wsMu0t0004Row.setMut04oMktTypeCd(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oMktTypeDes("");
		wsMu0t0004Row.setMut04oMtMktCltId("");
		wsMu0t0004Row.setMut04oMtMktTerNbr("");
		wsMu0t0004Row.setMut04oMtMktDsyNm("");
		wsMu0t0004Row.setMut04oMtMktFstNm("");
		wsMu0t0004Row.setMut04oMtMktMdlNm("");
		wsMu0t0004Row.setMut04oMtMktLstNm("");
		wsMu0t0004Row.setMut04oMtMktSfx("");
		wsMu0t0004Row.setMut04oMtMktPfx("");
		wsMu0t0004Row.setMut04oMtCsrCltId("");
		wsMu0t0004Row.setMut04oMtCsrTerNbr("");
		wsMu0t0004Row.setMut04oMtCsrDsyNm("");
		wsMu0t0004Row.setMut04oMtCsrFstNm("");
		wsMu0t0004Row.setMut04oMtCsrMdlNm("");
		wsMu0t0004Row.setMut04oMtCsrLstNm("");
		wsMu0t0004Row.setMut04oMtCsrSfx("");
		wsMu0t0004Row.setMut04oMtCsrPfx("");
		wsMu0t0004Row.setMut04oMtSmrCltId("");
		wsMu0t0004Row.setMut04oMtSmrTerNbr("");
		wsMu0t0004Row.setMut04oMtSmrDsyNm("");
		wsMu0t0004Row.setMut04oMtSmrFstNm("");
		wsMu0t0004Row.setMut04oMtSmrMdlNm("");
		wsMu0t0004Row.setMut04oMtSmrLstNm("");
		wsMu0t0004Row.setMut04oMtSmrSfx("");
		wsMu0t0004Row.setMut04oMtSmrPfx("");
		wsMu0t0004Row.setMut04oMtDmmCltId("");
		wsMu0t0004Row.setMut04oMtDmmTerNbr("");
		wsMu0t0004Row.setMut04oMtDmmDsyNm("");
		wsMu0t0004Row.setMut04oMtDmmFstNm("");
		wsMu0t0004Row.setMut04oMtDmmMdlNm("");
		wsMu0t0004Row.setMut04oMtDmmLstNm("");
		wsMu0t0004Row.setMut04oMtDmmSfx("");
		wsMu0t0004Row.setMut04oMtDmmPfx("");
		wsMu0t0004Row.setMut04oMtRmmCltId("");
		wsMu0t0004Row.setMut04oMtRmmTerNbr("");
		wsMu0t0004Row.setMut04oMtRmmDsyNm("");
		wsMu0t0004Row.setMut04oMtRmmFstNm("");
		wsMu0t0004Row.setMut04oMtRmmMdlNm("");
		wsMu0t0004Row.setMut04oMtRmmLstNm("");
		wsMu0t0004Row.setMut04oMtRmmSfx("");
		wsMu0t0004Row.setMut04oMtRmmPfx("");
		wsMu0t0004Row.setMut04oMtDfoCltId("");
		wsMu0t0004Row.setMut04oMtDfoTerNbr("");
		wsMu0t0004Row.setMut04oMtDfoDsyNm("");
		wsMu0t0004Row.setMut04oMtDfoFstNm("");
		wsMu0t0004Row.setMut04oMtDfoMdlNm("");
		wsMu0t0004Row.setMut04oMtDfoLstNm("");
		wsMu0t0004Row.setMut04oMtDfoSfx("");
		wsMu0t0004Row.setMut04oMtDfoPfx("");
		wsMu0t0004Row.setMut04oAtBrnCltId("");
		wsMu0t0004Row.setMut04oAtBrnTerNbr("");
		wsMu0t0004Row.setMut04oAtBrnNm("");
		wsMu0t0004Row.setMut04oAtAgcCltId("");
		wsMu0t0004Row.setMut04oAtAgcTerNbr("");
		wsMu0t0004Row.setMut04oAtAgcNm("");
		wsMu0t0004Row.setMut04oAtSmrCltId("");
		wsMu0t0004Row.setMut04oAtSmrTerNbr("");
		wsMu0t0004Row.setMut04oAtSmrNm("");
		wsMu0t0004Row.setMut04oAtAmmCltId("");
		wsMu0t0004Row.setMut04oAtAmmTerNbr("");
		wsMu0t0004Row.setMut04oAtAmmDsyNm("");
		wsMu0t0004Row.setMut04oAtAmmFstNm("");
		wsMu0t0004Row.setMut04oAtAmmMdlNm("");
		wsMu0t0004Row.setMut04oAtAmmLstNm("");
		wsMu0t0004Row.setMut04oAtAmmSfx("");
		wsMu0t0004Row.setMut04oAtAmmPfx("");
		wsMu0t0004Row.setMut04oAtAfmCltId("");
		wsMu0t0004Row.setMut04oAtAfmTerNbr("");
		wsMu0t0004Row.setMut04oAtAfmDsyNm("");
		wsMu0t0004Row.setMut04oAtAfmFstNm("");
		wsMu0t0004Row.setMut04oAtAfmMdlNm("");
		wsMu0t0004Row.setMut04oAtAfmLstNm("");
		wsMu0t0004Row.setMut04oAtAfmSfx("");
		wsMu0t0004Row.setMut04oAtAfmPfx("");
		wsMu0t0004Row.setMut04oAtAvpCltId("");
		wsMu0t0004Row.setMut04oAtAvpTerNbr("");
		wsMu0t0004Row.setMut04oAtAvpDsyNm("");
		wsMu0t0004Row.setMut04oAtAvpFstNm("");
		wsMu0t0004Row.setMut04oAtAvpMdlNm("");
		wsMu0t0004Row.setMut04oAtAvpLstNm("");
		wsMu0t0004Row.setMut04oAtAvpSfx("");
		wsMu0t0004Row.setMut04oAtAvpPfx("");
		wsMu0t0004Row.setMut04oSvcUwDsyNm("");
		wsMu0t0004Row.setMut04oSvcUwTypeCd("");
		wsMu0t0004Row.setMut04oSvcUwTypeDes("");
		wsMu0t0004Row.setMut04oUwCltId("");
		wsMu0t0004Row.setMut04oUwDsyNm("");
		wsMu0t0004Row.setMut04oUwFstNm("");
		wsMu0t0004Row.setMut04oUwMdlNm("");
		wsMu0t0004Row.setMut04oUwLstNm("");
		wsMu0t0004Row.setMut04oUwSfx("");
		wsMu0t0004Row.setMut04oUwPfx("");
		wsMu0t0004Row.setMut04oRskAlsCltId("");
		wsMu0t0004Row.setMut04oRskAlsDsyNm("");
		wsMu0t0004Row.setMut04oRskAlsFstNm("");
		wsMu0t0004Row.setMut04oRskAlsMdlNm("");
		wsMu0t0004Row.setMut04oRskAlsLstNm("");
		wsMu0t0004Row.setMut04oRskAlsSfx("");
		wsMu0t0004Row.setMut04oRskAlsPfx("");
		wsMu0t0004Row.setMut04oDumCltId("");
		wsMu0t0004Row.setMut04oDumDsyNm("");
		wsMu0t0004Row.setMut04oDumTer("");
		wsMu0t0004Row.setMut04oDumFstNm("");
		wsMu0t0004Row.setMut04oDumMdlNm("");
		wsMu0t0004Row.setMut04oDumLstNm("");
		wsMu0t0004Row.setMut04oDumSfx("");
		wsMu0t0004Row.setMut04oDumPfx("");
		wsMu0t0004Row.setMut04oRumCltId("");
		wsMu0t0004Row.setMut04oRumDsyNm("");
		wsMu0t0004Row.setMut04oRumTer("");
		wsMu0t0004Row.setMut04oRumFstNm("");
		wsMu0t0004Row.setMut04oRumMdlNm("");
		wsMu0t0004Row.setMut04oRumLstNm("");
		wsMu0t0004Row.setMut04oRumSfx("");
		wsMu0t0004Row.setMut04oRumPfx("");
		wsMu0t0004Row.setMut04oFpuCltId("");
		wsMu0t0004Row.setMut04oFpuDsyNm("");
		wsMu0t0004Row.setMut04oFpuFstNm("");
		wsMu0t0004Row.setMut04oFpuMdlNm("");
		wsMu0t0004Row.setMut04oFpuLstNm("");
		wsMu0t0004Row.setMut04oFpuSfx("");
		wsMu0t0004Row.setMut04oFpuPfx("");
		wsMu0t0004Row.setMut04oFpuDumCltId("");
		wsMu0t0004Row.setMut04oFpuDumDsyNm("");
		wsMu0t0004Row.setMut04oFpuDumTer("");
		wsMu0t0004Row.setMut04oFpuDumFstNm("");
		wsMu0t0004Row.setMut04oFpuDumMdlNm("");
		wsMu0t0004Row.setMut04oFpuDumLstNm("");
		wsMu0t0004Row.setMut04oFpuDumSfx("");
		wsMu0t0004Row.setMut04oFpuDumPfx("");
		wsMu0t0004Row.setMut04oFpuRumCltId("");
		wsMu0t0004Row.setMut04oFpuRumDsyNm("");
		wsMu0t0004Row.setMut04oFpuRumTer("");
		wsMu0t0004Row.setMut04oFpuRumFstNm("");
		wsMu0t0004Row.setMut04oFpuRumMdlNm("");
		wsMu0t0004Row.setMut04oFpuRumLstNm("");
		wsMu0t0004Row.setMut04oFpuRumSfx("");
		wsMu0t0004Row.setMut04oFpuRumPfx("");
		wsMu0t0004Row.setMut04oLpRskAlsCltId("");
		wsMu0t0004Row.setMut04oLpRskAlsDsyNm("");
		wsMu0t0004Row.setMut04oLpRskAlsFstNm("");
		wsMu0t0004Row.setMut04oLpRskAlsMdlNm("");
		wsMu0t0004Row.setMut04oLpRskAlsLstNm("");
		wsMu0t0004Row.setMut04oLpRskAlsSfx("");
		wsMu0t0004Row.setMut04oLpRskAlsPfx("");
		wsMu0t0004Row.setMut04oLpraDumCltId("");
		wsMu0t0004Row.setMut04oLpraDumDsyNm("");
		wsMu0t0004Row.setMut04oLpraDumTer("");
		wsMu0t0004Row.setMut04oLpraDumFstNm("");
		wsMu0t0004Row.setMut04oLpraDumMdlNm("");
		wsMu0t0004Row.setMut04oLpraDumLstNm("");
		wsMu0t0004Row.setMut04oLpraDumSfx("");
		wsMu0t0004Row.setMut04oLpraDumPfx("");
		wsMu0t0004Row.setMut04oLpraRumCltId("");
		wsMu0t0004Row.setMut04oLpraRumDsyNm("");
		wsMu0t0004Row.setMut04oLpraRumTer("");
		wsMu0t0004Row.setMut04oLpraRumFstNm("");
		wsMu0t0004Row.setMut04oLpraRumMdlNm("");
		wsMu0t0004Row.setMut04oLpraRumLstNm("");
		wsMu0t0004Row.setMut04oLpraRumSfx("");
		wsMu0t0004Row.setMut04oLpraRumPfx("");
		wsMu0t0004Row.setMut04oAlStateCd("");
		wsMu0t0004Row.setMut04oAlCountyCd("");
		wsMu0t0004Row.setMut04oAlTownCd("");
		wsMu0t0004Row.setMut04oNbrEmployees(0);
		for (int idx0 = 1; idx0 <= LServiceContractArea.MUT04O_SE_SIC_MAXOCCURS; idx0++) {
			wsMu0t0004Row.setMut04oSeSicCd(idx0, "");
			wsMu0t0004Row.setMut04oSeSicDes(idx0, "");
		}
		wsMu0t0004Row.setMut04oOlDivision(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oOlSubDivision(Types.SPACE_CHAR);
	}

	public void initWsBillingLinkage() {
		ws.getWsBillingLinkage().getProxyProgramCommon().setServiceDataSize(0);
		ws.getWsBillingLinkage().getProxyProgramCommon().getBypassSyncpointMdrvInd().setPpcBypassSyncpointMdrvInd(Types.SPACE_CHAR);
		ws.getWsBillingLinkage().getProxyProgramCommon().setOperation("");
		ws.getWsBillingLinkage().getProxyProgramCommon().getErrorReturnCode().setErrorReturnCodeFormatted("0000");
		ws.getWsBillingLinkage().getProxyProgramCommon().setFatalErrorMessage("");
		ws.getWsBillingLinkage().getProxyProgramCommon().setNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= ProxyProgramCommon.NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			ws.getWsBillingLinkage().getProxyProgramCommon().getNonLoggableErrors(idx0).setPpcNonLogErrMsg("");
		}
		ws.getWsBillingLinkage().getProxyProgramCommon().setWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= ProxyProgramCommon.WARNINGS_MAXOCCURS; idx0++) {
			ws.getWsBillingLinkage().getProxyProgramCommon().getWarnings(idx0).setPpcWarnMsg("");
		}
		ws.getWsBillingLinkage().getBx0t0003().setBxt03iAccountNbr("");
		ws.getWsBillingLinkage().getBx0t0003().setBxt03iUserid("");
		ws.getWsBillingLinkage().getBx0t0003().setBxt03oAccountNbr("");
		ws.getWsBillingLinkage().getBx0t0003().setBxt03oUserid("");
		ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().setBankruptcyInd(Types.SPACE_CHAR);
		for (int idx0 = 1; idx0 <= Bxt03oAccountInfo.FEE_INF_MAXOCCURS; idx0++) {
			ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getFeeInf(idx0).setType("");
			ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getFeeInf(idx0).setAmt(new AfDecimal(0, 17, 2));
		}
		for (int idx0 = 1; idx0 <= Bxt03oAccountInfo.POLICY_INF_MAXOCCURS; idx0++) {
			ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(idx0).setPolicyNbr("");
			ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(idx0).setPolEffDt("");
			ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(idx0).setPropCanDt("");
			ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(idx0).setPolAmtDue(new AfDecimal(0, 17, 2));
			ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(idx0).setPcnInd(Types.SPACE_CHAR);
			ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(idx0).setPolStatusCd(Types.SPACE_CHAR);
			ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().getPolicyInf(idx0).setPolStatusDes("");
		}
		ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().setPaymentMadeInd(Types.SPACE_CHAR);
		ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().setInvoiceSentInd(Types.SPACE_CHAR);
		ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().setPolCntCi(Types.SPACE_CHAR);
		ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().setPolCntFormatted("0000000");
		ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().setPcnPolCntCi(Types.SPACE_CHAR);
		ws.getWsBillingLinkage().getBx0t0003().getBxt03oAccountInfo().setPcnPolCntFormatted("0000000");
	}

	public void initWsXz0y8000Row() {
		ws.getXzy800GetNotDayRqrRow().setPolPriRskStAbb("");
		ws.getXzy800GetNotDayRqrRow().setActNotTypCd("");
		ws.getXzy800GetNotDayRqrRow().setPolTypCd("");
		ws.getXzy800GetNotDayRqrRow().setMinPolEffDt("");
		ws.getXzy800GetNotDayRqrRow().setPolNbr("");
		ws.getXzy800GetNotDayRqrRow().setPolEffDt("");
		ws.getXzy800GetNotDayRqrRow().setNbrOfNotDay(0);
		ws.getXzy800GetNotDayRqrRow().setUserid("");
		ws.getXzy800GetNotDayRqrRow().getNlbeInd().setNlbeInd(Types.SPACE_CHAR);
		ws.getXzy800GetNotDayRqrRow().getWarningInd().setWarningInd(Types.SPACE_CHAR);
		ws.getXzy800GetNotDayRqrRow().setNonLogErrMsg("");
		for (int idx0 = 1; idx0 <= Xzy800GetNotDayRqrRow.WARNINGS_MAXOCCURS; idx0++) {
			ws.getXzy800GetNotDayRqrRow().getWarnings(idx0).setXzy800WarnMsg("");
		}
	}

	public void initWsXz0t0006Row() {
		wsXz0t0006Row.setXzt06iTkNotPrcTs("");
		wsXz0t0006Row.setXzt06iTkActOwnCltId("");
		wsXz0t0006Row.setXzt06iTkActOwnAdrId("");
		wsXz0t0006Row.setXzt06iTkEmpId("");
		wsXz0t0006Row.setXzt06iTkStaMdfTs("");
		wsXz0t0006Row.setXzt06iTkActNotStaCd("");
		wsXz0t0006Row.setXzt06iTkSegCd("");
		wsXz0t0006Row.setXzt06iTkActTypCd("");
		wsXz0t0006Row.setXzt06iTkActNotCsumFormatted("000000000");
		wsXz0t0006Row.setXzt06iCsrActNbr("");
		wsXz0t0006Row.setXzt06iActNotTypCd("");
		wsXz0t0006Row.setXzt06iNotDt("");
		wsXz0t0006Row.setXzt06iPdcNbr("");
		wsXz0t0006Row.setXzt06iPdcNm("");
		wsXz0t0006Row.setXzt06iTotFeeAmt(new AfDecimal(0, 10, 2));
		wsXz0t0006Row.setXzt06iStAbb("");
		wsXz0t0006Row.setXzt06iCerHldNotInd(Types.SPACE_CHAR);
		wsXz0t0006Row.setXzt06iAddCncDay(0);
		wsXz0t0006Row.setXzt06iReaDes("");
		wsXz0t0006Row.setXzt06iUserid("");
		wsXz0t0006Row.setXzt06oTkNotPrcTs("");
		wsXz0t0006Row.setXzt06oTkActOwnCltId("");
		wsXz0t0006Row.setXzt06oTkActOwnAdrId("");
		wsXz0t0006Row.setXzt06oTkEmpId("");
		wsXz0t0006Row.setXzt06oTkStaMdfTs("");
		wsXz0t0006Row.setXzt06oTkActNotStaCd("");
		wsXz0t0006Row.setXzt06oTkSegCd("");
		wsXz0t0006Row.setXzt06oTkActTypCd("");
		wsXz0t0006Row.setXzt06oTkActNotCsumFormatted("000000000");
		wsXz0t0006Row.setXzt06oCsrActNbr("");
		wsXz0t0006Row.setXzt06oActNotTypCd("");
		wsXz0t0006Row.setXzt06oActNotTypDesc("");
		wsXz0t0006Row.setXzt06oNotDt("");
		wsXz0t0006Row.setXzt06oPdcNbr("");
		wsXz0t0006Row.setXzt06oPdcNm("");
		wsXz0t0006Row.setXzt06oTotFeeAmt(new AfDecimal(0, 10, 2));
		wsXz0t0006Row.setXzt06oStAbb("");
		wsXz0t0006Row.setXzt06oCerHldNotInd(Types.SPACE_CHAR);
		wsXz0t0006Row.setXzt06oAddCncDay(0);
		wsXz0t0006Row.setXzt06oReaDes("");
	}

	public void initWsXz0t90h0Row() {
		wsXz0t90h0Row.setiTkNotPrcTs("");
		wsXz0t90h0Row.setiCsrActNbr("");
		wsXz0t90h0Row.setiUserid("");
		for (int idx0 = 1; idx0 <= LServiceContractAreaXz0x90h0.I_POLICY_LIST_MAXOCCURS; idx0++) {
			wsXz0t90h0Row.setiPolNbr(idx0, "");
			wsXz0t90h0Row.setiPolEffDt(idx0, "");
			wsXz0t90h0Row.setiPolExpDt(idx0, "");
			wsXz0t90h0Row.setiNotEffDt(idx0, "");
			wsXz0t90h0Row.setiPolDueAmt(idx0, new AfDecimal(0, 10, 2));
			wsXz0t90h0Row.setiPolBilStaCd(idx0, Types.SPACE_CHAR);
		}
	}

	public void initWsXz0t90a0Row() {
		wsXz0t90a0Row.setiTkNotPrcTs("");
		wsXz0t90a0Row.setiCsrActNbr("");
		wsXz0t90a0Row.setiUserid("");
	}

	public void initWsXz0t90j0Row() {
		wsXz0t90j0Row.setiTkNotPrcTs("");
		wsXz0t90j0Row.setiCsrActNbr("");
		wsXz0t90j0Row.setiUserid("");
		wsXz0t90j0Row.setoFrmAtcInd(Types.SPACE_CHAR);
	}

	public void initWsXz0t90m0Row() {
		wsXz0t90m0Row.setiTkNotPrcTs("");
		wsXz0t90m0Row.setiTkActNotStaCd("");
		wsXz0t90m0Row.setiCsrActNbr("");
		wsXz0t90m0Row.setiUserid("");
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
