/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-PARAGRAPH-NAMES<br>
 * Variable: CF-PARAGRAPH-NAMES from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfParagraphNamesXz004000 {

	//==== PROPERTIES ====
	//Original name: CF-PN-2000
	private String pn2000 = "2000";
	//Original name: CF-PN-2100
	private String pn2100 = "2100";
	//Original name: CF-PN-3000
	private String pn3000 = "3000";
	//Original name: CF-PN-3100
	private String pn3100 = "3100";
	//Original name: CF-PN-3200
	private String pn3200 = "3200";
	//Original name: CF-PN-3300
	private String pn3300 = "3300";
	//Original name: CF-PN-3310
	private String pn3310 = "3310";
	//Original name: CF-PN-3400
	private String pn3400 = "3400";
	//Original name: CF-PN-3500
	private String pn3500 = "3500";

	//==== METHODS ====
	public String getPn2000() {
		return this.pn2000;
	}

	public String getPn2100() {
		return this.pn2100;
	}

	public String getPn3000() {
		return this.pn3000;
	}

	public String getPn3100() {
		return this.pn3100;
	}

	public String getPn3200() {
		return this.pn3200;
	}

	public String getPn3300() {
		return this.pn3300;
	}

	public String getPn3310() {
		return this.pn3310;
	}

	public String getPn3400() {
		return this.pn3400;
	}

	public String getPn3500() {
		return this.pn3500;
	}
}
