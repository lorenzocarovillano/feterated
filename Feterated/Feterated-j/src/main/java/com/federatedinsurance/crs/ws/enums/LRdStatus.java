/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: L-RD-STATUS<br>
 * Variable: L-RD-STATUS from program TS030199<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LRdStatus {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SUCCESSFUL = '*';
	public static final char NOT_CONNECTED = 'C';
	public static final char REPORT_NOT_FOUND = 'R';
	public static final char FATAL_ERROR = 'F';

	//==== METHODS ====
	public void setStatus(char status) {
		this.value = status;
	}

	public char getStatus() {
		return this.value;
	}

	public boolean isRdStatusSuccessful() {
		return value == SUCCESSFUL;
	}

	public void setSuccessful() {
		value = SUCCESSFUL;
	}

	public void setNotConnected() {
		value = NOT_CONNECTED;
	}

	public boolean isRdStatusReportNotFound() {
		return value == REPORT_NOT_FOUND;
	}

	public void setReportNotFound() {
		value = REPORT_NOT_FOUND;
	}

	public boolean isRdStatusFatalError() {
		return value == FATAL_ERROR;
	}

	public void setFatalError() {
		value = FATAL_ERROR;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STATUS = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
