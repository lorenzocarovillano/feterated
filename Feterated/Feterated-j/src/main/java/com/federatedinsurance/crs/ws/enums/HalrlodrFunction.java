/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: HALRLODR-FUNCTION<br>
 * Variable: HALRLODR-FUNCTION from copybook HALLLODR<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HalrlodrFunction {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.FUNCTION);
	public static final String CREATE_ACCESS_LOCK = "CREATE_ACCESS_LOCK";
	public static final String CREATE_INSRET_LOCK = "CREATE_INSRET_LOCK";
	public static final String VERIFY_INSERT_ACCESS = "VERIFY_INSERT_ACCESS";
	public static final String AUTH_ACCESS_LOCK = "AUTHENTICATE_ACCESS_LOCK";

	//==== METHODS ====
	public void setFunction(String function) {
		this.value = Functions.subString(function, Len.FUNCTION);
	}

	public String getFunction() {
		return this.value;
	}

	public void setHalrlodrCreateAccessLock() {
		value = CREATE_ACCESS_LOCK;
	}

	public void setHalrlodrCreateInsretLock() {
		value = CREATE_INSRET_LOCK;
	}

	public boolean isVerifyInsertAccess() {
		return value.equals(VERIFY_INSERT_ACCESS);
	}

	public void setHalrlodrVerifyInsertAccess() {
		value = VERIFY_INSERT_ACCESS;
	}

	public void setHalrlodrAuthAccessLock() {
		value = AUTH_ACCESS_LOCK;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FUNCTION = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
