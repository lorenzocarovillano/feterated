/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IHolidayPrc;

/**Original name: DCLHOLIDAY-PRC<br>
 * Variable: DCLHOLIDAY-PRC from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DclholidayPrc extends BytesAllocatingClass implements IHolidayPrc {

	//==== CONSTRUCTORS ====
	public DclholidayPrc() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DCLHOLIDAY_PRC;
	}

	public String getDclholidayPrcFormatted() {
		return readFixedString(Pos.DCLHOLIDAY_PRC, Len.DCLHOLIDAY_PRC);
	}

	public void setHolidayDt(String holidayDt) {
		writeString(Pos.HOLIDAY_DT, holidayDt, Len.HOLIDAY_DT);
	}

	/**Original name: HOLIDAY-DT<br>*/
	public String getHolidayDt() {
		return readString(Pos.HOLIDAY_DT, Len.HOLIDAY_DT);
	}

	@Override
	public void setCtrCd(String ctrCd) {
		writeString(Pos.CTR_CD, ctrCd, Len.CTR_CD);
	}

	/**Original name: CTR-CD<br>*/
	@Override
	public String getCtrCd() {
		return readString(Pos.CTR_CD, Len.CTR_CD);
	}

	@Override
	public void setStateCd(String stateCd) {
		writeString(Pos.STATE_CD, stateCd, Len.STATE_CD);
	}

	/**Original name: STATE-CD<br>*/
	@Override
	public String getStateCd() {
		return readString(Pos.STATE_CD, Len.STATE_CD);
	}

	@Override
	public void setApp01XclCd(String app01XclCd) {
		writeString(Pos.APP01_XCL_CD, app01XclCd, Len.APP01_XCL_CD);
	}

	/**Original name: APP-01-XCL-CD<br>*/
	@Override
	public String getApp01XclCd() {
		return readString(Pos.APP01_XCL_CD, Len.APP01_XCL_CD);
	}

	@Override
	public void setApp02XclCd(String app02XclCd) {
		writeString(Pos.APP02_XCL_CD, app02XclCd, Len.APP02_XCL_CD);
	}

	/**Original name: APP-02-XCL-CD<br>*/
	@Override
	public String getApp02XclCd() {
		return readString(Pos.APP02_XCL_CD, Len.APP02_XCL_CD);
	}

	@Override
	public void setApp03XclCd(String app03XclCd) {
		writeString(Pos.APP03_XCL_CD, app03XclCd, Len.APP03_XCL_CD);
	}

	/**Original name: APP-03-XCL-CD<br>*/
	@Override
	public String getApp03XclCd() {
		return readString(Pos.APP03_XCL_CD, Len.APP03_XCL_CD);
	}

	@Override
	public void setApp04XclCd(String app04XclCd) {
		writeString(Pos.APP04_XCL_CD, app04XclCd, Len.APP04_XCL_CD);
	}

	/**Original name: APP-04-XCL-CD<br>*/
	@Override
	public String getApp04XclCd() {
		return readString(Pos.APP04_XCL_CD, Len.APP04_XCL_CD);
	}

	@Override
	public void setApp05XclCd(String app05XclCd) {
		writeString(Pos.APP05_XCL_CD, app05XclCd, Len.APP05_XCL_CD);
	}

	/**Original name: APP-05-XCL-CD<br>*/
	@Override
	public String getApp05XclCd() {
		return readString(Pos.APP05_XCL_CD, Len.APP05_XCL_CD);
	}

	@Override
	public void setApp06XclCd(String app06XclCd) {
		writeString(Pos.APP06_XCL_CD, app06XclCd, Len.APP06_XCL_CD);
	}

	/**Original name: APP-06-XCL-CD<br>*/
	@Override
	public String getApp06XclCd() {
		return readString(Pos.APP06_XCL_CD, Len.APP06_XCL_CD);
	}

	@Override
	public void setApp07XclCd(String app07XclCd) {
		writeString(Pos.APP07_XCL_CD, app07XclCd, Len.APP07_XCL_CD);
	}

	/**Original name: APP-07-XCL-CD<br>*/
	@Override
	public String getApp07XclCd() {
		return readString(Pos.APP07_XCL_CD, Len.APP07_XCL_CD);
	}

	@Override
	public void setApp08XclCd(String app08XclCd) {
		writeString(Pos.APP08_XCL_CD, app08XclCd, Len.APP08_XCL_CD);
	}

	/**Original name: APP-08-XCL-CD<br>*/
	@Override
	public String getApp08XclCd() {
		return readString(Pos.APP08_XCL_CD, Len.APP08_XCL_CD);
	}

	@Override
	public void setApp09XclCd(String app09XclCd) {
		writeString(Pos.APP09_XCL_CD, app09XclCd, Len.APP09_XCL_CD);
	}

	/**Original name: APP-09-XCL-CD<br>*/
	@Override
	public String getApp09XclCd() {
		return readString(Pos.APP09_XCL_CD, Len.APP09_XCL_CD);
	}

	@Override
	public void setApp10XclCd(String app10XclCd) {
		writeString(Pos.APP10_XCL_CD, app10XclCd, Len.APP10_XCL_CD);
	}

	/**Original name: APP-10-XCL-CD<br>*/
	@Override
	public String getApp10XclCd() {
		return readString(Pos.APP10_XCL_CD, Len.APP10_XCL_CD);
	}

	@Override
	public void setApp11XclCd(String app11XclCd) {
		writeString(Pos.APP11_XCL_CD, app11XclCd, Len.APP11_XCL_CD);
	}

	/**Original name: APP-11-XCL-CD<br>*/
	@Override
	public String getApp11XclCd() {
		return readString(Pos.APP11_XCL_CD, Len.APP11_XCL_CD);
	}

	@Override
	public void setApp12XclCd(String app12XclCd) {
		writeString(Pos.APP12_XCL_CD, app12XclCd, Len.APP12_XCL_CD);
	}

	/**Original name: APP-12-XCL-CD<br>*/
	@Override
	public String getApp12XclCd() {
		return readString(Pos.APP12_XCL_CD, Len.APP12_XCL_CD);
	}

	@Override
	public void setApp13XclCd(String app13XclCd) {
		writeString(Pos.APP13_XCL_CD, app13XclCd, Len.APP13_XCL_CD);
	}

	/**Original name: APP-13-XCL-CD<br>*/
	@Override
	public String getApp13XclCd() {
		return readString(Pos.APP13_XCL_CD, Len.APP13_XCL_CD);
	}

	@Override
	public void setApp14XclCd(String app14XclCd) {
		writeString(Pos.APP14_XCL_CD, app14XclCd, Len.APP14_XCL_CD);
	}

	/**Original name: APP-14-XCL-CD<br>*/
	@Override
	public String getApp14XclCd() {
		return readString(Pos.APP14_XCL_CD, Len.APP14_XCL_CD);
	}

	@Override
	public void setApp15XclCd(String app15XclCd) {
		writeString(Pos.APP15_XCL_CD, app15XclCd, Len.APP15_XCL_CD);
	}

	/**Original name: APP-15-XCL-CD<br>*/
	@Override
	public String getApp15XclCd() {
		return readString(Pos.APP15_XCL_CD, Len.APP15_XCL_CD);
	}

	@Override
	public void setApp16XclCd(String app16XclCd) {
		writeString(Pos.APP16_XCL_CD, app16XclCd, Len.APP16_XCL_CD);
	}

	/**Original name: APP-16-XCL-CD<br>*/
	@Override
	public String getApp16XclCd() {
		return readString(Pos.APP16_XCL_CD, Len.APP16_XCL_CD);
	}

	@Override
	public void setApp17XclCd(String app17XclCd) {
		writeString(Pos.APP17_XCL_CD, app17XclCd, Len.APP17_XCL_CD);
	}

	/**Original name: APP-17-XCL-CD<br>*/
	@Override
	public String getApp17XclCd() {
		return readString(Pos.APP17_XCL_CD, Len.APP17_XCL_CD);
	}

	@Override
	public void setApp18XclCd(String app18XclCd) {
		writeString(Pos.APP18_XCL_CD, app18XclCd, Len.APP18_XCL_CD);
	}

	/**Original name: APP-18-XCL-CD<br>*/
	@Override
	public String getApp18XclCd() {
		return readString(Pos.APP18_XCL_CD, Len.APP18_XCL_CD);
	}

	@Override
	public void setApp19XclCd(String app19XclCd) {
		writeString(Pos.APP19_XCL_CD, app19XclCd, Len.APP19_XCL_CD);
	}

	/**Original name: APP-19-XCL-CD<br>*/
	@Override
	public String getApp19XclCd() {
		return readString(Pos.APP19_XCL_CD, Len.APP19_XCL_CD);
	}

	@Override
	public void setApp20XclCd(String app20XclCd) {
		writeString(Pos.APP20_XCL_CD, app20XclCd, Len.APP20_XCL_CD);
	}

	/**Original name: APP-20-XCL-CD<br>*/
	@Override
	public String getApp20XclCd() {
		return readString(Pos.APP20_XCL_CD, Len.APP20_XCL_CD);
	}

	public void setHolidayDesLen(short holidayDesLen) {
		writeBinaryShort(Pos.HOLIDAY_DES_LEN, holidayDesLen);
	}

	/**Original name: HOLIDAY-DES-LEN<br>*/
	public short getHolidayDesLen() {
		return readBinaryShort(Pos.HOLIDAY_DES_LEN);
	}

	public void setHolidayDesText(String holidayDesText) {
		writeString(Pos.HOLIDAY_DES_TEXT, holidayDesText, Len.HOLIDAY_DES_TEXT);
	}

	/**Original name: HOLIDAY-DES-TEXT<br>*/
	public String getHolidayDesText() {
		return readString(Pos.HOLIDAY_DES_TEXT, Len.HOLIDAY_DES_TEXT);
	}

	public String getHolidayDesTextFormatted() {
		return Functions.padBlanks(getHolidayDesText(), Len.HOLIDAY_DES_TEXT);
	}

	/**Original name: WS-APP-XCL-CODES<br>*/
	public String getWsAppXclCodes() {
		return readString(Pos.WS_APP_XCL_CODES, Len.WS_APP_XCL_CODES);
	}

	public String getWsAppXclCodesFormatted() {
		return Functions.padBlanks(getWsAppXclCodes(), Len.WS_APP_XCL_CODES);
	}

	@Override
	public String getHolidayDes() {
		return FixedStrings.get(getHolidayDesText(), getHolidayDesLen());
	}

	@Override
	public void setHolidayDes(String holidayDes) {
		this.setHolidayDesText(holidayDes);
		this.setHolidayDesLen((((short) strLen(holidayDes))));
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int DCLHOLIDAY_PRC = 1;
		public static final int HOLIDAY_DT = DCLHOLIDAY_PRC;
		public static final int CTR_CD = HOLIDAY_DT + Len.HOLIDAY_DT;
		public static final int STATE_CD = CTR_CD + Len.CTR_CD;
		public static final int APP01_XCL_CD = STATE_CD + Len.STATE_CD;
		public static final int APP02_XCL_CD = APP01_XCL_CD + Len.APP01_XCL_CD;
		public static final int APP03_XCL_CD = APP02_XCL_CD + Len.APP02_XCL_CD;
		public static final int APP04_XCL_CD = APP03_XCL_CD + Len.APP03_XCL_CD;
		public static final int APP05_XCL_CD = APP04_XCL_CD + Len.APP04_XCL_CD;
		public static final int APP06_XCL_CD = APP05_XCL_CD + Len.APP05_XCL_CD;
		public static final int APP07_XCL_CD = APP06_XCL_CD + Len.APP06_XCL_CD;
		public static final int APP08_XCL_CD = APP07_XCL_CD + Len.APP07_XCL_CD;
		public static final int APP09_XCL_CD = APP08_XCL_CD + Len.APP08_XCL_CD;
		public static final int APP10_XCL_CD = APP09_XCL_CD + Len.APP09_XCL_CD;
		public static final int APP11_XCL_CD = APP10_XCL_CD + Len.APP10_XCL_CD;
		public static final int APP12_XCL_CD = APP11_XCL_CD + Len.APP11_XCL_CD;
		public static final int APP13_XCL_CD = APP12_XCL_CD + Len.APP12_XCL_CD;
		public static final int APP14_XCL_CD = APP13_XCL_CD + Len.APP13_XCL_CD;
		public static final int APP15_XCL_CD = APP14_XCL_CD + Len.APP14_XCL_CD;
		public static final int APP16_XCL_CD = APP15_XCL_CD + Len.APP15_XCL_CD;
		public static final int APP17_XCL_CD = APP16_XCL_CD + Len.APP16_XCL_CD;
		public static final int APP18_XCL_CD = APP17_XCL_CD + Len.APP17_XCL_CD;
		public static final int APP19_XCL_CD = APP18_XCL_CD + Len.APP18_XCL_CD;
		public static final int APP20_XCL_CD = APP19_XCL_CD + Len.APP19_XCL_CD;
		public static final int HOLIDAY_DES = APP20_XCL_CD + Len.APP20_XCL_CD;
		public static final int HOLIDAY_DES_LEN = HOLIDAY_DES;
		public static final int HOLIDAY_DES_TEXT = HOLIDAY_DES_LEN + Len.HOLIDAY_DES_LEN;
		public static final int WS_HOLIDAY_TAB = 1;
		public static final int FLR1 = WS_HOLIDAY_TAB;
		public static final int WS_APP_XCL_CODES = FLR1 + Len.FLR1;
		public static final int FLR2 = WS_APP_XCL_CODES + Len.WS_APP_XCL_CODES;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int HOLIDAY_DT = 10;
		public static final int CTR_CD = 4;
		public static final int STATE_CD = 3;
		public static final int APP01_XCL_CD = 3;
		public static final int APP02_XCL_CD = 3;
		public static final int APP03_XCL_CD = 3;
		public static final int APP04_XCL_CD = 3;
		public static final int APP05_XCL_CD = 3;
		public static final int APP06_XCL_CD = 3;
		public static final int APP07_XCL_CD = 3;
		public static final int APP08_XCL_CD = 3;
		public static final int APP09_XCL_CD = 3;
		public static final int APP10_XCL_CD = 3;
		public static final int APP11_XCL_CD = 3;
		public static final int APP12_XCL_CD = 3;
		public static final int APP13_XCL_CD = 3;
		public static final int APP14_XCL_CD = 3;
		public static final int APP15_XCL_CD = 3;
		public static final int APP16_XCL_CD = 3;
		public static final int APP17_XCL_CD = 3;
		public static final int APP18_XCL_CD = 3;
		public static final int APP19_XCL_CD = 3;
		public static final int APP20_XCL_CD = 3;
		public static final int HOLIDAY_DES_LEN = 2;
		public static final int FLR1 = 17;
		public static final int WS_APP_XCL_CODES = 60;
		public static final int HOLIDAY_DES_TEXT = 40;
		public static final int HOLIDAY_DES = HOLIDAY_DES_LEN + HOLIDAY_DES_TEXT;
		public static final int DCLHOLIDAY_PRC = HOLIDAY_DT + CTR_CD + STATE_CD + APP01_XCL_CD + APP02_XCL_CD + APP03_XCL_CD + APP04_XCL_CD
				+ APP05_XCL_CD + APP06_XCL_CD + APP07_XCL_CD + APP08_XCL_CD + APP09_XCL_CD + APP10_XCL_CD + APP11_XCL_CD + APP12_XCL_CD + APP13_XCL_CD
				+ APP14_XCL_CD + APP15_XCL_CD + APP16_XCL_CD + APP17_XCL_CD + APP18_XCL_CD + APP19_XCL_CD + APP20_XCL_CD + HOLIDAY_DES;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
