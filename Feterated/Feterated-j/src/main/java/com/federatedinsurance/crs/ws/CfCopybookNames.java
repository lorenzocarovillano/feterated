/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: CF-COPYBOOK-NAMES<br>
 * Variable: CF-COPYBOOK-NAMES from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfCopybookNames {

	//==== PROPERTIES ====
	//Original name: CF-CN-PROXY-CBK-INP-NM
	private String proxyCbkInpNm = "FWPXYCOMONINP";
	//Original name: CF-CN-PROXY-CBK-OUP-NM
	private String proxyCbkOupNm = "FWPXYCOMONOUP";
	//Original name: CF-CN-SERVICE-CBK-INP-NM
	private String serviceCbkInpNm = "FWPXYSERVICEINP";
	//Original name: CF-CN-SERVICE-CBK-OUP-NM
	private String serviceCbkOupNm = "FWPXYSERVICEOUP";

	//==== METHODS ====
	public String getProxyCbkInpNm() {
		return this.proxyCbkInpNm;
	}

	public String getProxyCbkInpNmFormatted() {
		return Functions.padBlanks(getProxyCbkInpNm(), Len.PROXY_CBK_INP_NM);
	}

	public String getProxyCbkOupNm() {
		return this.proxyCbkOupNm;
	}

	public String getProxyCbkOupNmFormatted() {
		return Functions.padBlanks(getProxyCbkOupNm(), Len.PROXY_CBK_OUP_NM);
	}

	public String getServiceCbkInpNm() {
		return this.serviceCbkInpNm;
	}

	public String getServiceCbkInpNmFormatted() {
		return Functions.padBlanks(getServiceCbkInpNm(), Len.SERVICE_CBK_INP_NM);
	}

	public String getServiceCbkOupNm() {
		return this.serviceCbkOupNm;
	}

	public String getServiceCbkOupNmFormatted() {
		return Functions.padBlanks(getServiceCbkOupNm(), Len.SERVICE_CBK_OUP_NM);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROXY_CBK_INP_NM = 16;
		public static final int SERVICE_CBK_INP_NM = 16;
		public static final int SERVICE_CBK_OUP_NM = 16;
		public static final int PROXY_CBK_OUP_NM = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
