/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Cw27fMoreRowsSw;

/**Original name: CW27F-CLIENT-TAX-DATA<br>
 * Variable: CW27F-CLIENT-TAX-DATA from copybook CAWLF027<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw27fClientTaxData {

	//==== PROPERTIES ====
	/**Original name: CW27F-CITX-TAX-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char citxTaxIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-CITX-TAX-ID
	private String citxTaxId = DefaultValues.stringVal(Len.CITX_TAX_ID);
	//Original name: CW27F-TAX-TYPE-CD-CI
	private char taxTypeCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-TAX-TYPE-CD
	private String taxTypeCd = DefaultValues.stringVal(Len.TAX_TYPE_CD);
	//Original name: CW27F-CITX-TAX-ST-CD-CI
	private char citxTaxStCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-CITX-TAX-ST-CD-NI
	private char citxTaxStCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-CITX-TAX-ST-CD
	private String citxTaxStCd = DefaultValues.stringVal(Len.CITX_TAX_ST_CD);
	//Original name: CW27F-CITX-TAX-CTR-CD-CI
	private char citxTaxCtrCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-CITX-TAX-CTR-CD-NI
	private char citxTaxCtrCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-CITX-TAX-CTR-CD
	private String citxTaxCtrCd = DefaultValues.stringVal(Len.CITX_TAX_CTR_CD);
	//Original name: CW27F-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW27F-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW27F-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW27F-EXPIRATION-DT-CI
	private char expirationDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-EXPIRATION-DT
	private String expirationDt = DefaultValues.stringVal(Len.EXPIRATION_DT);
	//Original name: CW27F-EFFECTIVE-ACY-TS-CI
	private char effectiveAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-EFFECTIVE-ACY-TS
	private String effectiveAcyTs = DefaultValues.stringVal(Len.EFFECTIVE_ACY_TS);
	//Original name: CW27F-EXPIRATION-ACY-TS-CI
	private char expirationAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-EXPIRATION-ACY-TS-NI
	private char expirationAcyTsNi = DefaultValues.CHAR_VAL;
	//Original name: CW27F-EXPIRATION-ACY-TS
	private String expirationAcyTs = DefaultValues.stringVal(Len.EXPIRATION_ACY_TS);
	/**Original name: CW27F-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	private Cw27fMoreRowsSw moreRowsSw = new Cw27fMoreRowsSw();
	//Original name: CW27F-TAX-TYPE-DESC
	private String taxTypeDesc = DefaultValues.stringVal(Len.TAX_TYPE_DESC);

	//==== METHODS ====
	public void setClientTaxDataBytes(byte[] buffer, int offset) {
		int position = offset;
		citxTaxIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		citxTaxId = MarshalByte.readString(buffer, position, Len.CITX_TAX_ID);
		position += Len.CITX_TAX_ID;
		taxTypeCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		taxTypeCd = MarshalByte.readString(buffer, position, Len.TAX_TYPE_CD);
		position += Len.TAX_TYPE_CD;
		citxTaxStCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		citxTaxStCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		citxTaxStCd = MarshalByte.readString(buffer, position, Len.CITX_TAX_ST_CD);
		position += Len.CITX_TAX_ST_CD;
		citxTaxCtrCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		citxTaxCtrCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		citxTaxCtrCd = MarshalByte.readString(buffer, position, Len.CITX_TAX_CTR_CD);
		position += Len.CITX_TAX_CTR_CD;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		expirationDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		expirationDt = MarshalByte.readString(buffer, position, Len.EXPIRATION_DT);
		position += Len.EXPIRATION_DT;
		effectiveAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		effectiveAcyTs = MarshalByte.readString(buffer, position, Len.EFFECTIVE_ACY_TS);
		position += Len.EFFECTIVE_ACY_TS;
		expirationAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		expirationAcyTsNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		expirationAcyTs = MarshalByte.readString(buffer, position, Len.EXPIRATION_ACY_TS);
		position += Len.EXPIRATION_ACY_TS;
		moreRowsSw.setMoreRowsSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		taxTypeDesc = MarshalByte.readString(buffer, position, Len.TAX_TYPE_DESC);
	}

	public byte[] getClientTaxDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, citxTaxIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, citxTaxId, Len.CITX_TAX_ID);
		position += Len.CITX_TAX_ID;
		MarshalByte.writeChar(buffer, position, taxTypeCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, taxTypeCd, Len.TAX_TYPE_CD);
		position += Len.TAX_TYPE_CD;
		MarshalByte.writeChar(buffer, position, citxTaxStCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, citxTaxStCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, citxTaxStCd, Len.CITX_TAX_ST_CD);
		position += Len.CITX_TAX_ST_CD;
		MarshalByte.writeChar(buffer, position, citxTaxCtrCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, citxTaxCtrCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, citxTaxCtrCd, Len.CITX_TAX_CTR_CD);
		position += Len.CITX_TAX_CTR_CD;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, expirationDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationDt, Len.EXPIRATION_DT);
		position += Len.EXPIRATION_DT;
		MarshalByte.writeChar(buffer, position, effectiveAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
		position += Len.EFFECTIVE_ACY_TS;
		MarshalByte.writeChar(buffer, position, expirationAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, expirationAcyTsNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationAcyTs, Len.EXPIRATION_ACY_TS);
		position += Len.EXPIRATION_ACY_TS;
		MarshalByte.writeChar(buffer, position, moreRowsSw.getMoreRowsSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, taxTypeDesc, Len.TAX_TYPE_DESC);
		return buffer;
	}

	public void setCitxTaxIdCi(char citxTaxIdCi) {
		this.citxTaxIdCi = citxTaxIdCi;
	}

	public char getCitxTaxIdCi() {
		return this.citxTaxIdCi;
	}

	public void setCitxTaxId(String citxTaxId) {
		this.citxTaxId = Functions.subString(citxTaxId, Len.CITX_TAX_ID);
	}

	public String getCitxTaxId() {
		return this.citxTaxId;
	}

	public void setTaxTypeCdCi(char taxTypeCdCi) {
		this.taxTypeCdCi = taxTypeCdCi;
	}

	public char getTaxTypeCdCi() {
		return this.taxTypeCdCi;
	}

	public void setTaxTypeCd(String taxTypeCd) {
		this.taxTypeCd = Functions.subString(taxTypeCd, Len.TAX_TYPE_CD);
	}

	public String getTaxTypeCd() {
		return this.taxTypeCd;
	}

	public void setCitxTaxStCdCi(char citxTaxStCdCi) {
		this.citxTaxStCdCi = citxTaxStCdCi;
	}

	public char getCitxTaxStCdCi() {
		return this.citxTaxStCdCi;
	}

	public void setCitxTaxStCdNi(char citxTaxStCdNi) {
		this.citxTaxStCdNi = citxTaxStCdNi;
	}

	public char getCitxTaxStCdNi() {
		return this.citxTaxStCdNi;
	}

	public void setCitxTaxStCd(String citxTaxStCd) {
		this.citxTaxStCd = Functions.subString(citxTaxStCd, Len.CITX_TAX_ST_CD);
	}

	public String getCitxTaxStCd() {
		return this.citxTaxStCd;
	}

	public void setCitxTaxCtrCdCi(char citxTaxCtrCdCi) {
		this.citxTaxCtrCdCi = citxTaxCtrCdCi;
	}

	public char getCitxTaxCtrCdCi() {
		return this.citxTaxCtrCdCi;
	}

	public void setCitxTaxCtrCdNi(char citxTaxCtrCdNi) {
		this.citxTaxCtrCdNi = citxTaxCtrCdNi;
	}

	public char getCitxTaxCtrCdNi() {
		return this.citxTaxCtrCdNi;
	}

	public void setCitxTaxCtrCd(String citxTaxCtrCd) {
		this.citxTaxCtrCd = Functions.subString(citxTaxCtrCd, Len.CITX_TAX_CTR_CD);
	}

	public String getCitxTaxCtrCd() {
		return this.citxTaxCtrCd;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setExpirationDtCi(char expirationDtCi) {
		this.expirationDtCi = expirationDtCi;
	}

	public char getExpirationDtCi() {
		return this.expirationDtCi;
	}

	public void setExpirationDt(String expirationDt) {
		this.expirationDt = Functions.subString(expirationDt, Len.EXPIRATION_DT);
	}

	public String getExpirationDt() {
		return this.expirationDt;
	}

	public void setEffectiveAcyTsCi(char effectiveAcyTsCi) {
		this.effectiveAcyTsCi = effectiveAcyTsCi;
	}

	public char getEffectiveAcyTsCi() {
		return this.effectiveAcyTsCi;
	}

	public void setEffectiveAcyTs(String effectiveAcyTs) {
		this.effectiveAcyTs = Functions.subString(effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
	}

	public String getEffectiveAcyTs() {
		return this.effectiveAcyTs;
	}

	public void setExpirationAcyTsCi(char expirationAcyTsCi) {
		this.expirationAcyTsCi = expirationAcyTsCi;
	}

	public char getExpirationAcyTsCi() {
		return this.expirationAcyTsCi;
	}

	public void setExpirationAcyTsNi(char expirationAcyTsNi) {
		this.expirationAcyTsNi = expirationAcyTsNi;
	}

	public char getExpirationAcyTsNi() {
		return this.expirationAcyTsNi;
	}

	public void setExpirationAcyTs(String expirationAcyTs) {
		this.expirationAcyTs = Functions.subString(expirationAcyTs, Len.EXPIRATION_ACY_TS);
	}

	public String getExpirationAcyTs() {
		return this.expirationAcyTs;
	}

	public void setTaxTypeDesc(String taxTypeDesc) {
		this.taxTypeDesc = Functions.subString(taxTypeDesc, Len.TAX_TYPE_DESC);
	}

	public String getTaxTypeDesc() {
		return this.taxTypeDesc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CITX_TAX_ID = 15;
		public static final int TAX_TYPE_CD = 3;
		public static final int CITX_TAX_ST_CD = 3;
		public static final int CITX_TAX_CTR_CD = 4;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int EXPIRATION_DT = 10;
		public static final int EFFECTIVE_ACY_TS = 26;
		public static final int EXPIRATION_ACY_TS = 26;
		public static final int TAX_TYPE_DESC = 40;
		public static final int CITX_TAX_ID_CI = 1;
		public static final int TAX_TYPE_CD_CI = 1;
		public static final int CITX_TAX_ST_CD_CI = 1;
		public static final int CITX_TAX_ST_CD_NI = 1;
		public static final int CITX_TAX_CTR_CD_CI = 1;
		public static final int CITX_TAX_CTR_CD_NI = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int EXPIRATION_DT_CI = 1;
		public static final int EFFECTIVE_ACY_TS_CI = 1;
		public static final int EXPIRATION_ACY_TS_CI = 1;
		public static final int EXPIRATION_ACY_TS_NI = 1;
		public static final int CLIENT_TAX_DATA = CITX_TAX_ID_CI + CITX_TAX_ID + TAX_TYPE_CD_CI + TAX_TYPE_CD + CITX_TAX_ST_CD_CI + CITX_TAX_ST_CD_NI
				+ CITX_TAX_ST_CD + CITX_TAX_CTR_CD_CI + CITX_TAX_CTR_CD_NI + CITX_TAX_CTR_CD + USER_ID_CI + USER_ID + STATUS_CD_CI + STATUS_CD
				+ TERMINAL_ID_CI + TERMINAL_ID + EXPIRATION_DT_CI + EXPIRATION_DT + EFFECTIVE_ACY_TS_CI + EFFECTIVE_ACY_TS + EXPIRATION_ACY_TS_CI
				+ EXPIRATION_ACY_TS_NI + EXPIRATION_ACY_TS + Cw27fMoreRowsSw.Len.MORE_ROWS_SW + TAX_TYPE_DESC;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
