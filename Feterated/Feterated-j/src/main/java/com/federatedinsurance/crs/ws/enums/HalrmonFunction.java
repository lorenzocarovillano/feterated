/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: HALRMON-FUNCTION<br>
 * Variable: HALRMON-FUNCTION from copybook HALLMON<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HalrmonFunction {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char BEGIN_FUNCTION = 'B';
	public static final char END_FUNCTION = 'E';
	public static final char INFO_FUNCTION = 'I';

	//==== METHODS ====
	public void setFunction(char function) {
		this.value = function;
	}

	public char getFunction() {
		return this.value;
	}

	public void setBeginFunction() {
		value = BEGIN_FUNCTION;
	}

	public void setEndFunction() {
		value = END_FUNCTION;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FUNCTION = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
