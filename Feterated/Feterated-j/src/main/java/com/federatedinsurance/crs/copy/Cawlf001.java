/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CAWLF001<br>
 * Variable: CAWLF001 from copybook CAWLF001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cawlf001 {

	//==== PROPERTIES ====
	//Original name: CW01F-BUSINESS-CLIENT-CHK-SUM
	private String businessClientChkSum = DefaultValues.stringVal(Len.BUSINESS_CLIENT_CHK_SUM);
	//Original name: CW01F-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CW01F-CIBC-BUS-SEQ-NBR-KCRE
	private String cibcBusSeqNbrKcre = DefaultValues.stringVal(Len.CIBC_BUS_SEQ_NBR_KCRE);
	//Original name: CW01F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW01F-BUSINESS-CLIENT-KEY
	private Cw01fBusinessClientKey businessClientKey = new Cw01fBusinessClientKey();
	//Original name: CW01F-BUSINESS-CLIENT-DATA
	private Cw01fBusinessClientData businessClientData = new Cw01fBusinessClientData();

	//==== METHODS ====
	public void setBusinessClientRowFormatted(String data) {
		byte[] buffer = new byte[Len.BUSINESS_CLIENT_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.BUSINESS_CLIENT_ROW);
		setBusinessClientRowBytes(buffer, 1);
	}

	public String getBusinessClientRowFormatted() {
		return MarshalByteExt.bufferToStr(getBusinessClientRowBytes());
	}

	/**Original name: CW01F-BUSINESS-CLIENT-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CAWLF001 - BUSINESS_CLIENT TABLE                               *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  PP00015  03/07/2007 E404ASW   NEW COPYBOOK                     *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getBusinessClientRowBytes() {
		byte[] buffer = new byte[Len.BUSINESS_CLIENT_ROW];
		return getBusinessClientRowBytes(buffer, 1);
	}

	public void setBusinessClientRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setBusinessClientFixedBytes(buffer, position);
		position += Len.BUSINESS_CLIENT_FIXED;
		setBusinessClientDatesBytes(buffer, position);
		position += Len.BUSINESS_CLIENT_DATES;
		businessClientKey.setBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		businessClientData.setBusinessClientDataBytes(buffer, position);
	}

	public byte[] getBusinessClientRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getBusinessClientFixedBytes(buffer, position);
		position += Len.BUSINESS_CLIENT_FIXED;
		getBusinessClientDatesBytes(buffer, position);
		position += Len.BUSINESS_CLIENT_DATES;
		businessClientKey.getBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		businessClientData.getBusinessClientDataBytes(buffer, position);
		return buffer;
	}

	public void setBusinessClientFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		businessClientChkSum = MarshalByte.readFixedString(buffer, position, Len.BUSINESS_CLIENT_CHK_SUM);
		position += Len.BUSINESS_CLIENT_CHK_SUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		cibcBusSeqNbrKcre = MarshalByte.readString(buffer, position, Len.CIBC_BUS_SEQ_NBR_KCRE);
	}

	public byte[] getBusinessClientFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, businessClientChkSum, Len.BUSINESS_CLIENT_CHK_SUM);
		position += Len.BUSINESS_CLIENT_CHK_SUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		MarshalByte.writeString(buffer, position, cibcBusSeqNbrKcre, Len.CIBC_BUS_SEQ_NBR_KCRE);
		return buffer;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setCibcBusSeqNbrKcre(String cibcBusSeqNbrKcre) {
		this.cibcBusSeqNbrKcre = Functions.subString(cibcBusSeqNbrKcre, Len.CIBC_BUS_SEQ_NBR_KCRE);
	}

	public String getCibcBusSeqNbrKcre() {
		return this.cibcBusSeqNbrKcre;
	}

	public void setBusinessClientDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getBusinessClientDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUSINESS_CLIENT_CHK_SUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int CIBC_BUS_SEQ_NBR_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int BUSINESS_CLIENT_FIXED = BUSINESS_CLIENT_CHK_SUM + CLIENT_ID_KCRE + CIBC_BUS_SEQ_NBR_KCRE;
		public static final int BUSINESS_CLIENT_DATES = TRANS_PROCESS_DT;
		public static final int BUSINESS_CLIENT_ROW = BUSINESS_CLIENT_FIXED + BUSINESS_CLIENT_DATES + Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY
				+ Cw01fBusinessClientData.Len.BUSINESS_CLIENT_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
