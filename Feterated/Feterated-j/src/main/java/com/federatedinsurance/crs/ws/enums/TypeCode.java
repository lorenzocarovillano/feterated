/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: TYPE-CODE<br>
 * Variable: TYPE-CODE from copybook CISLNSRB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TypeCode {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char COMPANY = 'C';
	public static final char PERSON = 'P';
	public static final char SPACE = ' ';

	//==== METHODS ====
	public void setTypeCode(char typeCode) {
		this.value = typeCode;
	}

	public void setTypeCodeFormatted(String typeCode) {
		setTypeCode(Functions.charAt(typeCode, Types.CHAR_SIZE));
	}

	public char getTypeCode() {
		return this.value;
	}

	public boolean isCompany() {
		return value == COMPANY;
	}

	public boolean isPerson() {
		return value == PERSON;
	}

	public boolean isSpace() {
		return value == SPACE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TYPE_CODE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
