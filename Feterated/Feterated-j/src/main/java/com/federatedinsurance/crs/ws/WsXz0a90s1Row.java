/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.Xza9s1PolicyRow;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90S1-ROW<br>
 * Variable: WS-XZ0A90S1-ROW from program XZ0B90S0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90s1Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9S1-POLICY-ROW
	private Xza9s1PolicyRow row = new Xza9s1PolicyRow();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90S1_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90s1RowBytes(buf);
	}

	public String getWsXz0a90s1RowFormatted() {
		return getLocateRowFormatted();
	}

	public void setWsXz0a90s1RowBytes(byte[] buffer) {
		setWsXz0a90s1RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90s1RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90S1_ROW];
		return getWsXz0a90s1RowBytes(buffer, 1);
	}

	public void setWsXz0a90s1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setLocateRowBytes(buffer, position);
	}

	public byte[] getWsXz0a90s1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getLocateRowBytes(buffer, position);
		return buffer;
	}

	public String getLocateRowFormatted() {
		return row.getRowFormatted();
	}

	public void setLocateRowBytes(byte[] buffer, int offset) {
		int position = offset;
		row.setRowBytes(buffer, position);
	}

	public byte[] getLocateRowBytes(byte[] buffer, int offset) {
		int position = offset;
		row.getRowBytes(buffer, position);
		return buffer;
	}

	public Xza9s1PolicyRow getRow() {
		return row;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90s1RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LOCATE_ROW = Xza9s1PolicyRow.Len.ROW;
		public static final int WS_XZ0A90S1_ROW = LOCATE_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
