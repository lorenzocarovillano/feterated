/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X0012<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x0012 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x0012() {
	}

	public LFrameworkRequestAreaXz0x0012(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzc003ActNotRecRowFormatted(String data) {
		writeString(Pos.XZC003Q_ACT_NOT_REC_ROW, data, Len.XZC003Q_ACT_NOT_REC_ROW);
	}

	public String getXzc003ActNotRecRowFormatted() {
		return readFixedString(Pos.XZC003Q_ACT_NOT_REC_ROW, Len.XZC003Q_ACT_NOT_REC_ROW);
	}

	public void setXzc003qActNotRecCsumFormatted(String xzc003qActNotRecCsum) {
		writeString(Pos.XZC003Q_ACT_NOT_REC_CSUM, Trunc.toUnsignedNumeric(xzc003qActNotRecCsum, Len.XZC003Q_ACT_NOT_REC_CSUM),
				Len.XZC003Q_ACT_NOT_REC_CSUM);
	}

	/**Original name: XZC003Q-ACT-NOT-REC-CSUM<br>*/
	public int getXzc003qActNotRecCsum() {
		return readNumDispUnsignedInt(Pos.XZC003Q_ACT_NOT_REC_CSUM, Len.XZC003Q_ACT_NOT_REC_CSUM);
	}

	public String getXzc003rActNotRecCsumFormatted() {
		return readFixedString(Pos.XZC003Q_ACT_NOT_REC_CSUM, Len.XZC003Q_ACT_NOT_REC_CSUM);
	}

	public void setXzc003qCsrActNbrKcre(String xzc003qCsrActNbrKcre) {
		writeString(Pos.XZC003Q_CSR_ACT_NBR_KCRE, xzc003qCsrActNbrKcre, Len.XZC003Q_CSR_ACT_NBR_KCRE);
	}

	/**Original name: XZC003Q-CSR-ACT-NBR-KCRE<br>*/
	public String getXzc003qCsrActNbrKcre() {
		return readString(Pos.XZC003Q_CSR_ACT_NBR_KCRE, Len.XZC003Q_CSR_ACT_NBR_KCRE);
	}

	public void setXzc003qNotPrcTsKcre(String xzc003qNotPrcTsKcre) {
		writeString(Pos.XZC003Q_NOT_PRC_TS_KCRE, xzc003qNotPrcTsKcre, Len.XZC003Q_NOT_PRC_TS_KCRE);
	}

	/**Original name: XZC003Q-NOT-PRC-TS-KCRE<br>*/
	public String getXzc003qNotPrcTsKcre() {
		return readString(Pos.XZC003Q_NOT_PRC_TS_KCRE, Len.XZC003Q_NOT_PRC_TS_KCRE);
	}

	public void setXzc003qRecSeqNbrKcre(String xzc003qRecSeqNbrKcre) {
		writeString(Pos.XZC003Q_REC_SEQ_NBR_KCRE, xzc003qRecSeqNbrKcre, Len.XZC003Q_REC_SEQ_NBR_KCRE);
	}

	/**Original name: XZC003Q-REC-SEQ-NBR-KCRE<br>*/
	public String getXzc003qRecSeqNbrKcre() {
		return readString(Pos.XZC003Q_REC_SEQ_NBR_KCRE, Len.XZC003Q_REC_SEQ_NBR_KCRE);
	}

	public void setXzc003qTransProcessDt(String xzc003qTransProcessDt) {
		writeString(Pos.XZC003Q_TRANS_PROCESS_DT, xzc003qTransProcessDt, Len.XZC003Q_TRANS_PROCESS_DT);
	}

	/**Original name: XZC003Q-TRANS-PROCESS-DT<br>*/
	public String getXzc003qTransProcessDt() {
		return readString(Pos.XZC003Q_TRANS_PROCESS_DT, Len.XZC003Q_TRANS_PROCESS_DT);
	}

	public void setXzc003qCsrActNbr(String xzc003qCsrActNbr) {
		writeString(Pos.XZC003Q_CSR_ACT_NBR, xzc003qCsrActNbr, Len.XZC003Q_CSR_ACT_NBR);
	}

	/**Original name: XZC003Q-CSR-ACT-NBR<br>*/
	public String getXzc003qCsrActNbr() {
		return readString(Pos.XZC003Q_CSR_ACT_NBR, Len.XZC003Q_CSR_ACT_NBR);
	}

	public void setXzc003qNotPrcTs(String xzc003qNotPrcTs) {
		writeString(Pos.XZC003Q_NOT_PRC_TS, xzc003qNotPrcTs, Len.XZC003Q_NOT_PRC_TS);
	}

	/**Original name: XZC003Q-NOT-PRC-TS<br>*/
	public String getXzc003qNotPrcTs() {
		return readString(Pos.XZC003Q_NOT_PRC_TS, Len.XZC003Q_NOT_PRC_TS);
	}

	public void setXzc003qRecSeqNbrSign(char xzc003qRecSeqNbrSign) {
		writeChar(Pos.XZC003Q_REC_SEQ_NBR_SIGN, xzc003qRecSeqNbrSign);
	}

	/**Original name: XZC003Q-REC-SEQ-NBR-SIGN<br>*/
	public char getXzc003qRecSeqNbrSign() {
		return readChar(Pos.XZC003Q_REC_SEQ_NBR_SIGN);
	}

	public void setXzc003qRecSeqNbrFormatted(String xzc003qRecSeqNbr) {
		writeString(Pos.XZC003Q_REC_SEQ_NBR, Trunc.toUnsignedNumeric(xzc003qRecSeqNbr, Len.XZC003Q_REC_SEQ_NBR), Len.XZC003Q_REC_SEQ_NBR);
	}

	/**Original name: XZC003Q-REC-SEQ-NBR<br>*/
	public int getXzc003qRecSeqNbr() {
		return readNumDispUnsignedInt(Pos.XZC003Q_REC_SEQ_NBR, Len.XZC003Q_REC_SEQ_NBR);
	}

	public String getXzc003rRecSeqNbrFormatted() {
		return readFixedString(Pos.XZC003Q_REC_SEQ_NBR, Len.XZC003Q_REC_SEQ_NBR);
	}

	public void setXzc003qCsrActNbrCi(char xzc003qCsrActNbrCi) {
		writeChar(Pos.XZC003Q_CSR_ACT_NBR_CI, xzc003qCsrActNbrCi);
	}

	public void setXzc003qCsrActNbrCiFormatted(String xzc003qCsrActNbrCi) {
		setXzc003qCsrActNbrCi(Functions.charAt(xzc003qCsrActNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC003Q-CSR-ACT-NBR-CI<br>*/
	public char getXzc003qCsrActNbrCi() {
		return readChar(Pos.XZC003Q_CSR_ACT_NBR_CI);
	}

	public void setXzc003qNotPrcTsCi(char xzc003qNotPrcTsCi) {
		writeChar(Pos.XZC003Q_NOT_PRC_TS_CI, xzc003qNotPrcTsCi);
	}

	public void setXzc003qNotPrcTsCiFormatted(String xzc003qNotPrcTsCi) {
		setXzc003qNotPrcTsCi(Functions.charAt(xzc003qNotPrcTsCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC003Q-NOT-PRC-TS-CI<br>*/
	public char getXzc003qNotPrcTsCi() {
		return readChar(Pos.XZC003Q_NOT_PRC_TS_CI);
	}

	public void setXzc003qRecSeqNbrCi(char xzc003qRecSeqNbrCi) {
		writeChar(Pos.XZC003Q_REC_SEQ_NBR_CI, xzc003qRecSeqNbrCi);
	}

	public void setXzc003qRecSeqNbrCiFormatted(String xzc003qRecSeqNbrCi) {
		setXzc003qRecSeqNbrCi(Functions.charAt(xzc003qRecSeqNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC003Q-REC-SEQ-NBR-CI<br>*/
	public char getXzc003qRecSeqNbrCi() {
		return readChar(Pos.XZC003Q_REC_SEQ_NBR_CI);
	}

	public void setXzc003qRecTypCdCi(char xzc003qRecTypCdCi) {
		writeChar(Pos.XZC003Q_REC_TYP_CD_CI, xzc003qRecTypCdCi);
	}

	/**Original name: XZC003Q-REC-TYP-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getXzc003qRecTypCdCi() {
		return readChar(Pos.XZC003Q_REC_TYP_CD_CI);
	}

	public void setXzc003qRecTypCd(String xzc003qRecTypCd) {
		writeString(Pos.XZC003Q_REC_TYP_CD, xzc003qRecTypCd, Len.XZC003Q_REC_TYP_CD);
	}

	/**Original name: XZC003Q-REC-TYP-CD<br>*/
	public String getXzc003qRecTypCd() {
		return readString(Pos.XZC003Q_REC_TYP_CD, Len.XZC003Q_REC_TYP_CD);
	}

	public void setXzc003qRecCltIdCi(char xzc003qRecCltIdCi) {
		writeChar(Pos.XZC003Q_REC_CLT_ID_CI, xzc003qRecCltIdCi);
	}

	/**Original name: XZC003Q-REC-CLT-ID-CI<br>*/
	public char getXzc003qRecCltIdCi() {
		return readChar(Pos.XZC003Q_REC_CLT_ID_CI);
	}

	public void setXzc003qRecCltIdNi(char xzc003qRecCltIdNi) {
		writeChar(Pos.XZC003Q_REC_CLT_ID_NI, xzc003qRecCltIdNi);
	}

	/**Original name: XZC003Q-REC-CLT-ID-NI<br>*/
	public char getXzc003qRecCltIdNi() {
		return readChar(Pos.XZC003Q_REC_CLT_ID_NI);
	}

	public void setXzc003qRecCltId(String xzc003qRecCltId) {
		writeString(Pos.XZC003Q_REC_CLT_ID, xzc003qRecCltId, Len.XZC003Q_REC_CLT_ID);
	}

	/**Original name: XZC003Q-REC-CLT-ID<br>*/
	public String getXzc003qRecCltId() {
		return readString(Pos.XZC003Q_REC_CLT_ID, Len.XZC003Q_REC_CLT_ID);
	}

	public void setXzc003qRecNmCi(char xzc003qRecNmCi) {
		writeChar(Pos.XZC003Q_REC_NM_CI, xzc003qRecNmCi);
	}

	/**Original name: XZC003Q-REC-NM-CI<br>*/
	public char getXzc003qRecNmCi() {
		return readChar(Pos.XZC003Q_REC_NM_CI);
	}

	public void setXzc003qRecNmNi(char xzc003qRecNmNi) {
		writeChar(Pos.XZC003Q_REC_NM_NI, xzc003qRecNmNi);
	}

	/**Original name: XZC003Q-REC-NM-NI<br>*/
	public char getXzc003qRecNmNi() {
		return readChar(Pos.XZC003Q_REC_NM_NI);
	}

	public void setXzc003qRecNm(String xzc003qRecNm) {
		writeString(Pos.XZC003Q_REC_NM, xzc003qRecNm, Len.XZC003Q_REC_NM);
	}

	/**Original name: XZC003Q-REC-NM<br>*/
	public String getXzc003qRecNm() {
		return readString(Pos.XZC003Q_REC_NM, Len.XZC003Q_REC_NM);
	}

	public void setXzc003qRecAdrIdCi(char xzc003qRecAdrIdCi) {
		writeChar(Pos.XZC003Q_REC_ADR_ID_CI, xzc003qRecAdrIdCi);
	}

	/**Original name: XZC003Q-REC-ADR-ID-CI<br>*/
	public char getXzc003qRecAdrIdCi() {
		return readChar(Pos.XZC003Q_REC_ADR_ID_CI);
	}

	public void setXzc003qRecAdrIdNi(char xzc003qRecAdrIdNi) {
		writeChar(Pos.XZC003Q_REC_ADR_ID_NI, xzc003qRecAdrIdNi);
	}

	/**Original name: XZC003Q-REC-ADR-ID-NI<br>*/
	public char getXzc003qRecAdrIdNi() {
		return readChar(Pos.XZC003Q_REC_ADR_ID_NI);
	}

	public void setXzc003qRecAdrId(String xzc003qRecAdrId) {
		writeString(Pos.XZC003Q_REC_ADR_ID, xzc003qRecAdrId, Len.XZC003Q_REC_ADR_ID);
	}

	/**Original name: XZC003Q-REC-ADR-ID<br>*/
	public String getXzc003qRecAdrId() {
		return readString(Pos.XZC003Q_REC_ADR_ID, Len.XZC003Q_REC_ADR_ID);
	}

	public void setXzc003qLin1AdrCi(char xzc003qLin1AdrCi) {
		writeChar(Pos.XZC003Q_LIN1_ADR_CI, xzc003qLin1AdrCi);
	}

	/**Original name: XZC003Q-LIN-1-ADR-CI<br>*/
	public char getXzc003qLin1AdrCi() {
		return readChar(Pos.XZC003Q_LIN1_ADR_CI);
	}

	public void setXzc003qLin1AdrNi(char xzc003qLin1AdrNi) {
		writeChar(Pos.XZC003Q_LIN1_ADR_NI, xzc003qLin1AdrNi);
	}

	/**Original name: XZC003Q-LIN-1-ADR-NI<br>*/
	public char getXzc003qLin1AdrNi() {
		return readChar(Pos.XZC003Q_LIN1_ADR_NI);
	}

	public void setXzc003qLin1Adr(String xzc003qLin1Adr) {
		writeString(Pos.XZC003Q_LIN1_ADR, xzc003qLin1Adr, Len.XZC003Q_LIN1_ADR);
	}

	/**Original name: XZC003Q-LIN-1-ADR<br>*/
	public String getXzc003qLin1Adr() {
		return readString(Pos.XZC003Q_LIN1_ADR, Len.XZC003Q_LIN1_ADR);
	}

	public void setXzc003qLin2AdrCi(char xzc003qLin2AdrCi) {
		writeChar(Pos.XZC003Q_LIN2_ADR_CI, xzc003qLin2AdrCi);
	}

	/**Original name: XZC003Q-LIN-2-ADR-CI<br>*/
	public char getXzc003qLin2AdrCi() {
		return readChar(Pos.XZC003Q_LIN2_ADR_CI);
	}

	public void setXzc003qLin2AdrNi(char xzc003qLin2AdrNi) {
		writeChar(Pos.XZC003Q_LIN2_ADR_NI, xzc003qLin2AdrNi);
	}

	/**Original name: XZC003Q-LIN-2-ADR-NI<br>*/
	public char getXzc003qLin2AdrNi() {
		return readChar(Pos.XZC003Q_LIN2_ADR_NI);
	}

	public void setXzc003qLin2Adr(String xzc003qLin2Adr) {
		writeString(Pos.XZC003Q_LIN2_ADR, xzc003qLin2Adr, Len.XZC003Q_LIN2_ADR);
	}

	/**Original name: XZC003Q-LIN-2-ADR<br>*/
	public String getXzc003qLin2Adr() {
		return readString(Pos.XZC003Q_LIN2_ADR, Len.XZC003Q_LIN2_ADR);
	}

	public void setXzc003qCitNmCi(char xzc003qCitNmCi) {
		writeChar(Pos.XZC003Q_CIT_NM_CI, xzc003qCitNmCi);
	}

	/**Original name: XZC003Q-CIT-NM-CI<br>*/
	public char getXzc003qCitNmCi() {
		return readChar(Pos.XZC003Q_CIT_NM_CI);
	}

	public void setXzc003qCitNmNi(char xzc003qCitNmNi) {
		writeChar(Pos.XZC003Q_CIT_NM_NI, xzc003qCitNmNi);
	}

	/**Original name: XZC003Q-CIT-NM-NI<br>*/
	public char getXzc003qCitNmNi() {
		return readChar(Pos.XZC003Q_CIT_NM_NI);
	}

	public void setXzc003qCitNm(String xzc003qCitNm) {
		writeString(Pos.XZC003Q_CIT_NM, xzc003qCitNm, Len.XZC003Q_CIT_NM);
	}

	/**Original name: XZC003Q-CIT-NM<br>*/
	public String getXzc003qCitNm() {
		return readString(Pos.XZC003Q_CIT_NM, Len.XZC003Q_CIT_NM);
	}

	public void setXzc003qStAbbCi(char xzc003qStAbbCi) {
		writeChar(Pos.XZC003Q_ST_ABB_CI, xzc003qStAbbCi);
	}

	/**Original name: XZC003Q-ST-ABB-CI<br>*/
	public char getXzc003qStAbbCi() {
		return readChar(Pos.XZC003Q_ST_ABB_CI);
	}

	public void setXzc003qStAbbNi(char xzc003qStAbbNi) {
		writeChar(Pos.XZC003Q_ST_ABB_NI, xzc003qStAbbNi);
	}

	/**Original name: XZC003Q-ST-ABB-NI<br>*/
	public char getXzc003qStAbbNi() {
		return readChar(Pos.XZC003Q_ST_ABB_NI);
	}

	public void setXzc003qStAbb(String xzc003qStAbb) {
		writeString(Pos.XZC003Q_ST_ABB, xzc003qStAbb, Len.XZC003Q_ST_ABB);
	}

	/**Original name: XZC003Q-ST-ABB<br>*/
	public String getXzc003qStAbb() {
		return readString(Pos.XZC003Q_ST_ABB, Len.XZC003Q_ST_ABB);
	}

	public void setXzc003qPstCdCi(char xzc003qPstCdCi) {
		writeChar(Pos.XZC003Q_PST_CD_CI, xzc003qPstCdCi);
	}

	/**Original name: XZC003Q-PST-CD-CI<br>*/
	public char getXzc003qPstCdCi() {
		return readChar(Pos.XZC003Q_PST_CD_CI);
	}

	public void setXzc003qPstCdNi(char xzc003qPstCdNi) {
		writeChar(Pos.XZC003Q_PST_CD_NI, xzc003qPstCdNi);
	}

	/**Original name: XZC003Q-PST-CD-NI<br>*/
	public char getXzc003qPstCdNi() {
		return readChar(Pos.XZC003Q_PST_CD_NI);
	}

	public void setXzc003qPstCd(String xzc003qPstCd) {
		writeString(Pos.XZC003Q_PST_CD, xzc003qPstCd, Len.XZC003Q_PST_CD);
	}

	/**Original name: XZC003Q-PST-CD<br>*/
	public String getXzc003qPstCd() {
		return readString(Pos.XZC003Q_PST_CD, Len.XZC003Q_PST_CD);
	}

	public void setXzc003qMnlIndCi(char xzc003qMnlIndCi) {
		writeChar(Pos.XZC003Q_MNL_IND_CI, xzc003qMnlIndCi);
	}

	/**Original name: XZC003Q-MNL-IND-CI<br>*/
	public char getXzc003qMnlIndCi() {
		return readChar(Pos.XZC003Q_MNL_IND_CI);
	}

	public void setXzc003qMnlInd(char xzc003qMnlInd) {
		writeChar(Pos.XZC003Q_MNL_IND, xzc003qMnlInd);
	}

	/**Original name: XZC003Q-MNL-IND<br>*/
	public char getXzc003qMnlInd() {
		return readChar(Pos.XZC003Q_MNL_IND);
	}

	public void setXzc003qCerNbrCi(char xzc003qCerNbrCi) {
		writeChar(Pos.XZC003Q_CER_NBR_CI, xzc003qCerNbrCi);
	}

	/**Original name: XZC003Q-CER-NBR-CI<br>*/
	public char getXzc003qCerNbrCi() {
		return readChar(Pos.XZC003Q_CER_NBR_CI);
	}

	public void setXzc003qCerNbrNi(char xzc003qCerNbrNi) {
		writeChar(Pos.XZC003Q_CER_NBR_NI, xzc003qCerNbrNi);
	}

	/**Original name: XZC003Q-CER-NBR-NI<br>*/
	public char getXzc003qCerNbrNi() {
		return readChar(Pos.XZC003Q_CER_NBR_NI);
	}

	public void setXzc003qCerNbr(String xzc003qCerNbr) {
		writeString(Pos.XZC003Q_CER_NBR, xzc003qCerNbr, Len.XZC003Q_CER_NBR);
	}

	/**Original name: XZC003Q-CER-NBR<br>*/
	public String getXzc003qCerNbr() {
		return readString(Pos.XZC003Q_CER_NBR, Len.XZC003Q_CER_NBR);
	}

	public void setXzc003qRecTypShtDes(String xzc003qRecTypShtDes) {
		writeString(Pos.XZC003Q_REC_TYP_SHT_DES, xzc003qRecTypShtDes, Len.XZC003Q_REC_TYP_SHT_DES);
	}

	/**Original name: XZC003Q-REC-TYP-SHT-DES<br>*/
	public String getXzc003qRecTypShtDes() {
		return readString(Pos.XZC003Q_REC_TYP_SHT_DES, Len.XZC003Q_REC_TYP_SHT_DES);
	}

	public void setXzc003qRecTypLngDes(String xzc003qRecTypLngDes) {
		writeString(Pos.XZC003Q_REC_TYP_LNG_DES, xzc003qRecTypLngDes, Len.XZC003Q_REC_TYP_LNG_DES);
	}

	/**Original name: XZC003Q-REC-TYP-LNG-DES<br>*/
	public String getXzc003qRecTypLngDes() {
		return readString(Pos.XZC003Q_REC_TYP_LNG_DES, Len.XZC003Q_REC_TYP_LNG_DES);
	}

	public void setXzc003qRecSrOrdNbrSign(char xzc003qRecSrOrdNbrSign) {
		writeChar(Pos.XZC003Q_REC_SR_ORD_NBR_SIGN, xzc003qRecSrOrdNbrSign);
	}

	/**Original name: XZC003Q-REC-SR-ORD-NBR-SIGN<br>*/
	public char getXzc003qRecSrOrdNbrSign() {
		return readChar(Pos.XZC003Q_REC_SR_ORD_NBR_SIGN);
	}

	public void setXzc003qRecSrOrdNbrFormatted(String xzc003qRecSrOrdNbr) {
		writeString(Pos.XZC003Q_REC_SR_ORD_NBR, Trunc.toUnsignedNumeric(xzc003qRecSrOrdNbr, Len.XZC003Q_REC_SR_ORD_NBR), Len.XZC003Q_REC_SR_ORD_NBR);
	}

	/**Original name: XZC003Q-REC-SR-ORD-NBR<br>*/
	public int getXzc003qRecSrOrdNbr() {
		return readNumDispUnsignedInt(Pos.XZC003Q_REC_SR_ORD_NBR, Len.XZC003Q_REC_SR_ORD_NBR);
	}

	public String getXzc003rRecSrOrdNbrFormatted() {
		return readFixedString(Pos.XZC003Q_REC_SR_ORD_NBR, Len.XZC003Q_REC_SR_ORD_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0C0003 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZC003Q_ACT_NOT_REC_ROW = L_FW_REQ_XZ0C0003;
		public static final int XZC003Q_ACT_NOT_REC_FIXED = XZC003Q_ACT_NOT_REC_ROW;
		public static final int XZC003Q_ACT_NOT_REC_CSUM = XZC003Q_ACT_NOT_REC_FIXED;
		public static final int XZC003Q_CSR_ACT_NBR_KCRE = XZC003Q_ACT_NOT_REC_CSUM + Len.XZC003Q_ACT_NOT_REC_CSUM;
		public static final int XZC003Q_NOT_PRC_TS_KCRE = XZC003Q_CSR_ACT_NBR_KCRE + Len.XZC003Q_CSR_ACT_NBR_KCRE;
		public static final int XZC003Q_REC_SEQ_NBR_KCRE = XZC003Q_NOT_PRC_TS_KCRE + Len.XZC003Q_NOT_PRC_TS_KCRE;
		public static final int XZC003Q_ACT_NOT_REC_DATES = XZC003Q_REC_SEQ_NBR_KCRE + Len.XZC003Q_REC_SEQ_NBR_KCRE;
		public static final int XZC003Q_TRANS_PROCESS_DT = XZC003Q_ACT_NOT_REC_DATES;
		public static final int XZC003Q_ACT_NOT_REC_KEY = XZC003Q_TRANS_PROCESS_DT + Len.XZC003Q_TRANS_PROCESS_DT;
		public static final int XZC003Q_CSR_ACT_NBR = XZC003Q_ACT_NOT_REC_KEY;
		public static final int XZC003Q_NOT_PRC_TS = XZC003Q_CSR_ACT_NBR + Len.XZC003Q_CSR_ACT_NBR;
		public static final int XZC003Q_REC_SEQ_NBR_SIGN = XZC003Q_NOT_PRC_TS + Len.XZC003Q_NOT_PRC_TS;
		public static final int XZC003Q_REC_SEQ_NBR = XZC003Q_REC_SEQ_NBR_SIGN + Len.XZC003Q_REC_SEQ_NBR_SIGN;
		public static final int XZC003Q_ACT_NOT_REC_KEY_CI = XZC003Q_REC_SEQ_NBR + Len.XZC003Q_REC_SEQ_NBR;
		public static final int XZC003Q_CSR_ACT_NBR_CI = XZC003Q_ACT_NOT_REC_KEY_CI;
		public static final int XZC003Q_NOT_PRC_TS_CI = XZC003Q_CSR_ACT_NBR_CI + Len.XZC003Q_CSR_ACT_NBR_CI;
		public static final int XZC003Q_REC_SEQ_NBR_CI = XZC003Q_NOT_PRC_TS_CI + Len.XZC003Q_NOT_PRC_TS_CI;
		public static final int XZC003Q_ACT_NOT_REC_DATA = XZC003Q_REC_SEQ_NBR_CI + Len.XZC003Q_REC_SEQ_NBR_CI;
		public static final int XZC003Q_REC_TYP_CD_CI = XZC003Q_ACT_NOT_REC_DATA;
		public static final int XZC003Q_REC_TYP_CD = XZC003Q_REC_TYP_CD_CI + Len.XZC003Q_REC_TYP_CD_CI;
		public static final int XZC003Q_REC_CLT_ID_CI = XZC003Q_REC_TYP_CD + Len.XZC003Q_REC_TYP_CD;
		public static final int XZC003Q_REC_CLT_ID_NI = XZC003Q_REC_CLT_ID_CI + Len.XZC003Q_REC_CLT_ID_CI;
		public static final int XZC003Q_REC_CLT_ID = XZC003Q_REC_CLT_ID_NI + Len.XZC003Q_REC_CLT_ID_NI;
		public static final int XZC003Q_REC_NM_CI = XZC003Q_REC_CLT_ID + Len.XZC003Q_REC_CLT_ID;
		public static final int XZC003Q_REC_NM_NI = XZC003Q_REC_NM_CI + Len.XZC003Q_REC_NM_CI;
		public static final int XZC003Q_REC_NM = XZC003Q_REC_NM_NI + Len.XZC003Q_REC_NM_NI;
		public static final int XZC003Q_REC_ADR_ID_CI = XZC003Q_REC_NM + Len.XZC003Q_REC_NM;
		public static final int XZC003Q_REC_ADR_ID_NI = XZC003Q_REC_ADR_ID_CI + Len.XZC003Q_REC_ADR_ID_CI;
		public static final int XZC003Q_REC_ADR_ID = XZC003Q_REC_ADR_ID_NI + Len.XZC003Q_REC_ADR_ID_NI;
		public static final int XZC003Q_LIN1_ADR_CI = XZC003Q_REC_ADR_ID + Len.XZC003Q_REC_ADR_ID;
		public static final int XZC003Q_LIN1_ADR_NI = XZC003Q_LIN1_ADR_CI + Len.XZC003Q_LIN1_ADR_CI;
		public static final int XZC003Q_LIN1_ADR = XZC003Q_LIN1_ADR_NI + Len.XZC003Q_LIN1_ADR_NI;
		public static final int XZC003Q_LIN2_ADR_CI = XZC003Q_LIN1_ADR + Len.XZC003Q_LIN1_ADR;
		public static final int XZC003Q_LIN2_ADR_NI = XZC003Q_LIN2_ADR_CI + Len.XZC003Q_LIN2_ADR_CI;
		public static final int XZC003Q_LIN2_ADR = XZC003Q_LIN2_ADR_NI + Len.XZC003Q_LIN2_ADR_NI;
		public static final int XZC003Q_CIT_NM_CI = XZC003Q_LIN2_ADR + Len.XZC003Q_LIN2_ADR;
		public static final int XZC003Q_CIT_NM_NI = XZC003Q_CIT_NM_CI + Len.XZC003Q_CIT_NM_CI;
		public static final int XZC003Q_CIT_NM = XZC003Q_CIT_NM_NI + Len.XZC003Q_CIT_NM_NI;
		public static final int XZC003Q_ST_ABB_CI = XZC003Q_CIT_NM + Len.XZC003Q_CIT_NM;
		public static final int XZC003Q_ST_ABB_NI = XZC003Q_ST_ABB_CI + Len.XZC003Q_ST_ABB_CI;
		public static final int XZC003Q_ST_ABB = XZC003Q_ST_ABB_NI + Len.XZC003Q_ST_ABB_NI;
		public static final int XZC003Q_PST_CD_CI = XZC003Q_ST_ABB + Len.XZC003Q_ST_ABB;
		public static final int XZC003Q_PST_CD_NI = XZC003Q_PST_CD_CI + Len.XZC003Q_PST_CD_CI;
		public static final int XZC003Q_PST_CD = XZC003Q_PST_CD_NI + Len.XZC003Q_PST_CD_NI;
		public static final int XZC003Q_MNL_IND_CI = XZC003Q_PST_CD + Len.XZC003Q_PST_CD;
		public static final int XZC003Q_MNL_IND = XZC003Q_MNL_IND_CI + Len.XZC003Q_MNL_IND_CI;
		public static final int XZC003Q_CER_NBR_CI = XZC003Q_MNL_IND + Len.XZC003Q_MNL_IND;
		public static final int XZC003Q_CER_NBR_NI = XZC003Q_CER_NBR_CI + Len.XZC003Q_CER_NBR_CI;
		public static final int XZC003Q_CER_NBR = XZC003Q_CER_NBR_NI + Len.XZC003Q_CER_NBR_NI;
		public static final int XZC003Q_EXTENSION_FIELDS = XZC003Q_CER_NBR + Len.XZC003Q_CER_NBR;
		public static final int XZC003Q_REC_TYP_SHT_DES = XZC003Q_EXTENSION_FIELDS;
		public static final int XZC003Q_REC_TYP_LNG_DES = XZC003Q_REC_TYP_SHT_DES + Len.XZC003Q_REC_TYP_SHT_DES;
		public static final int XZC003Q_REC_SR_ORD_NBR_SIGN = XZC003Q_REC_TYP_LNG_DES + Len.XZC003Q_REC_TYP_LNG_DES;
		public static final int XZC003Q_REC_SR_ORD_NBR = XZC003Q_REC_SR_ORD_NBR_SIGN + Len.XZC003Q_REC_SR_ORD_NBR_SIGN;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC003Q_ACT_NOT_REC_CSUM = 9;
		public static final int XZC003Q_CSR_ACT_NBR_KCRE = 32;
		public static final int XZC003Q_NOT_PRC_TS_KCRE = 32;
		public static final int XZC003Q_REC_SEQ_NBR_KCRE = 32;
		public static final int XZC003Q_TRANS_PROCESS_DT = 10;
		public static final int XZC003Q_CSR_ACT_NBR = 9;
		public static final int XZC003Q_NOT_PRC_TS = 26;
		public static final int XZC003Q_REC_SEQ_NBR_SIGN = 1;
		public static final int XZC003Q_REC_SEQ_NBR = 5;
		public static final int XZC003Q_CSR_ACT_NBR_CI = 1;
		public static final int XZC003Q_NOT_PRC_TS_CI = 1;
		public static final int XZC003Q_REC_SEQ_NBR_CI = 1;
		public static final int XZC003Q_REC_TYP_CD_CI = 1;
		public static final int XZC003Q_REC_TYP_CD = 5;
		public static final int XZC003Q_REC_CLT_ID_CI = 1;
		public static final int XZC003Q_REC_CLT_ID_NI = 1;
		public static final int XZC003Q_REC_CLT_ID = 64;
		public static final int XZC003Q_REC_NM_CI = 1;
		public static final int XZC003Q_REC_NM_NI = 1;
		public static final int XZC003Q_REC_NM = 120;
		public static final int XZC003Q_REC_ADR_ID_CI = 1;
		public static final int XZC003Q_REC_ADR_ID_NI = 1;
		public static final int XZC003Q_REC_ADR_ID = 64;
		public static final int XZC003Q_LIN1_ADR_CI = 1;
		public static final int XZC003Q_LIN1_ADR_NI = 1;
		public static final int XZC003Q_LIN1_ADR = 45;
		public static final int XZC003Q_LIN2_ADR_CI = 1;
		public static final int XZC003Q_LIN2_ADR_NI = 1;
		public static final int XZC003Q_LIN2_ADR = 45;
		public static final int XZC003Q_CIT_NM_CI = 1;
		public static final int XZC003Q_CIT_NM_NI = 1;
		public static final int XZC003Q_CIT_NM = 30;
		public static final int XZC003Q_ST_ABB_CI = 1;
		public static final int XZC003Q_ST_ABB_NI = 1;
		public static final int XZC003Q_ST_ABB = 2;
		public static final int XZC003Q_PST_CD_CI = 1;
		public static final int XZC003Q_PST_CD_NI = 1;
		public static final int XZC003Q_PST_CD = 13;
		public static final int XZC003Q_MNL_IND_CI = 1;
		public static final int XZC003Q_MNL_IND = 1;
		public static final int XZC003Q_CER_NBR_CI = 1;
		public static final int XZC003Q_CER_NBR_NI = 1;
		public static final int XZC003Q_CER_NBR = 25;
		public static final int XZC003Q_REC_TYP_SHT_DES = 13;
		public static final int XZC003Q_REC_TYP_LNG_DES = 30;
		public static final int XZC003Q_REC_SR_ORD_NBR_SIGN = 1;
		public static final int XZC003Q_ACT_NOT_REC_FIXED = XZC003Q_ACT_NOT_REC_CSUM + XZC003Q_CSR_ACT_NBR_KCRE + XZC003Q_NOT_PRC_TS_KCRE
				+ XZC003Q_REC_SEQ_NBR_KCRE;
		public static final int XZC003Q_ACT_NOT_REC_DATES = XZC003Q_TRANS_PROCESS_DT;
		public static final int XZC003Q_ACT_NOT_REC_KEY = XZC003Q_CSR_ACT_NBR + XZC003Q_NOT_PRC_TS + XZC003Q_REC_SEQ_NBR_SIGN + XZC003Q_REC_SEQ_NBR;
		public static final int XZC003Q_ACT_NOT_REC_KEY_CI = XZC003Q_CSR_ACT_NBR_CI + XZC003Q_NOT_PRC_TS_CI + XZC003Q_REC_SEQ_NBR_CI;
		public static final int XZC003Q_ACT_NOT_REC_DATA = XZC003Q_REC_TYP_CD_CI + XZC003Q_REC_TYP_CD + XZC003Q_REC_CLT_ID_CI + XZC003Q_REC_CLT_ID_NI
				+ XZC003Q_REC_CLT_ID + XZC003Q_REC_NM_CI + XZC003Q_REC_NM_NI + XZC003Q_REC_NM + XZC003Q_REC_ADR_ID_CI + XZC003Q_REC_ADR_ID_NI
				+ XZC003Q_REC_ADR_ID + XZC003Q_LIN1_ADR_CI + XZC003Q_LIN1_ADR_NI + XZC003Q_LIN1_ADR + XZC003Q_LIN2_ADR_CI + XZC003Q_LIN2_ADR_NI
				+ XZC003Q_LIN2_ADR + XZC003Q_CIT_NM_CI + XZC003Q_CIT_NM_NI + XZC003Q_CIT_NM + XZC003Q_ST_ABB_CI + XZC003Q_ST_ABB_NI + XZC003Q_ST_ABB
				+ XZC003Q_PST_CD_CI + XZC003Q_PST_CD_NI + XZC003Q_PST_CD + XZC003Q_MNL_IND_CI + XZC003Q_MNL_IND + XZC003Q_CER_NBR_CI
				+ XZC003Q_CER_NBR_NI + XZC003Q_CER_NBR;
		public static final int XZC003Q_REC_SR_ORD_NBR = 5;
		public static final int XZC003Q_EXTENSION_FIELDS = XZC003Q_REC_TYP_SHT_DES + XZC003Q_REC_TYP_LNG_DES + XZC003Q_REC_SR_ORD_NBR_SIGN
				+ XZC003Q_REC_SR_ORD_NBR;
		public static final int XZC003Q_ACT_NOT_REC_ROW = XZC003Q_ACT_NOT_REC_FIXED + XZC003Q_ACT_NOT_REC_DATES + XZC003Q_ACT_NOT_REC_KEY
				+ XZC003Q_ACT_NOT_REC_KEY_CI + XZC003Q_ACT_NOT_REC_DATA + XZC003Q_EXTENSION_FIELDS;
		public static final int L_FW_REQ_XZ0C0003 = XZC003Q_ACT_NOT_REC_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0C0003;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
