/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRecCoTyp;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for tables [ACT_NOT_FRM_REC, ACT_NOT_REC, REC_CO_TYP]
 * 
 */
public class ActNotFrmRecCoTypDao extends BaseSqlDao<IActNotFrmRecCoTyp> {

	public ActNotFrmRecCoTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotFrmRecCoTyp> getToClass() {
		return IActNotFrmRecCoTyp.class;
	}

	public short selectRec(String csrActNbr, String notPrcTs, String coCd, char acyInd, short dft) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("coCd", coCd)
				.bind("acyInd", String.valueOf(acyInd)).scalarResultShort(dft);
	}
}
