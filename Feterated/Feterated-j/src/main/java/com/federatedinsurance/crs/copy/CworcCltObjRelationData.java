/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CWORC-CLT-OBJ-RELATION-DATA<br>
 * Variable: CWORC-CLT-OBJ-RELATION-DATA from copybook CAWLCORC<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class CworcCltObjRelationData {

	//==== PROPERTIES ====
	/**Original name: CWORC-CLIENT-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char clientIdCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: CWORC-RLT-TYP-CD-CI
	private char rltTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-RLT-TYP-CD
	private String rltTypCd = DefaultValues.stringVal(Len.RLT_TYP_CD);
	//Original name: CWORC-OBJ-CD-CI
	private char objCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-OBJ-CD
	private String objCd = DefaultValues.stringVal(Len.OBJ_CD);
	//Original name: CWORC-CIOR-SHW-OBJ-KEY-CI
	private char ciorShwObjKeyCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-SHW-OBJ-KEY
	private String ciorShwObjKey = DefaultValues.stringVal(Len.CIOR_SHW_OBJ_KEY);
	//Original name: CWORC-ADR-SEQ-NBR-CI
	private char adrSeqNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-ADR-SEQ-NBR-SIGN
	private char adrSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: CWORC-ADR-SEQ-NBR
	private String adrSeqNbr = DefaultValues.stringVal(Len.ADR_SEQ_NBR);
	//Original name: CWORC-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CWORC-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CWORC-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CWORC-CIOR-EXP-DT-CI
	private char ciorExpDtCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-EXP-DT
	private String ciorExpDt = DefaultValues.stringVal(Len.CIOR_EXP_DT);
	//Original name: CWORC-CIOR-EFF-ACY-TS-CI
	private char ciorEffAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-EFF-ACY-TS
	private String ciorEffAcyTs = DefaultValues.stringVal(Len.CIOR_EFF_ACY_TS);
	//Original name: CWORC-CIOR-EXP-ACY-TS-CI
	private char ciorExpAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-EXP-ACY-TS
	private String ciorExpAcyTs = DefaultValues.stringVal(Len.CIOR_EXP_ACY_TS);
	//Original name: CWORC-CIOR-LEG-ENT-CD-CI
	private char ciorLegEntCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-LEG-ENT-CD
	private String ciorLegEntCd = DefaultValues.stringVal(Len.CIOR_LEG_ENT_CD);
	//Original name: CWORC-CIOR-FST-NM-CI
	private char ciorFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-FST-NM
	private String ciorFstNm = DefaultValues.stringVal(Len.CIOR_FST_NM);
	//Original name: CWORC-CIOR-LST-NM-CI
	private char ciorLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-LST-NM
	private String ciorLstNm = DefaultValues.stringVal(Len.CIOR_LST_NM);
	//Original name: CWORC-CIOR-MDL-NM-CI
	private char ciorMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-MDL-NM
	private String ciorMdlNm = DefaultValues.stringVal(Len.CIOR_MDL_NM);
	//Original name: CWORC-CIOR-NM-PFX-CI
	private char ciorNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-NM-PFX
	private String ciorNmPfx = DefaultValues.stringVal(Len.CIOR_NM_PFX);
	//Original name: CWORC-CIOR-NM-SFX-CI
	private char ciorNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-NM-SFX
	private String ciorNmSfx = DefaultValues.stringVal(Len.CIOR_NM_SFX);
	//Original name: CWORC-CIOR-LNG-NM-CI
	private char ciorLngNmCi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-LNG-NM-NI
	private char ciorLngNmNi = DefaultValues.CHAR_VAL;
	//Original name: CWORC-CIOR-LNG-NM
	private String ciorLngNm = DefaultValues.stringVal(Len.CIOR_LNG_NM);

	//==== METHODS ====
	public void setCltObjRelationDataBytes(byte[] buffer, int offset) {
		int position = offset;
		clientIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		clientId = MarshalByte.readString(buffer, position, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		rltTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rltTypCd = MarshalByte.readString(buffer, position, Len.RLT_TYP_CD);
		position += Len.RLT_TYP_CD;
		objCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		objCd = MarshalByte.readString(buffer, position, Len.OBJ_CD);
		position += Len.OBJ_CD;
		ciorShwObjKeyCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorShwObjKey = MarshalByte.readString(buffer, position, Len.CIOR_SHW_OBJ_KEY);
		position += Len.CIOR_SHW_OBJ_KEY;
		adrSeqNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		adrSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		adrSeqNbr = MarshalByte.readFixedString(buffer, position, Len.ADR_SEQ_NBR);
		position += Len.ADR_SEQ_NBR;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		ciorExpDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorExpDt = MarshalByte.readString(buffer, position, Len.CIOR_EXP_DT);
		position += Len.CIOR_EXP_DT;
		ciorEffAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorEffAcyTs = MarshalByte.readString(buffer, position, Len.CIOR_EFF_ACY_TS);
		position += Len.CIOR_EFF_ACY_TS;
		ciorExpAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorExpAcyTs = MarshalByte.readString(buffer, position, Len.CIOR_EXP_ACY_TS);
		position += Len.CIOR_EXP_ACY_TS;
		ciorLegEntCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorLegEntCd = MarshalByte.readString(buffer, position, Len.CIOR_LEG_ENT_CD);
		position += Len.CIOR_LEG_ENT_CD;
		ciorFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorFstNm = MarshalByte.readString(buffer, position, Len.CIOR_FST_NM);
		position += Len.CIOR_FST_NM;
		ciorLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorLstNm = MarshalByte.readString(buffer, position, Len.CIOR_LST_NM);
		position += Len.CIOR_LST_NM;
		ciorMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorMdlNm = MarshalByte.readString(buffer, position, Len.CIOR_MDL_NM);
		position += Len.CIOR_MDL_NM;
		ciorNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorNmPfx = MarshalByte.readString(buffer, position, Len.CIOR_NM_PFX);
		position += Len.CIOR_NM_PFX;
		ciorNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorNmSfx = MarshalByte.readString(buffer, position, Len.CIOR_NM_SFX);
		position += Len.CIOR_NM_SFX;
		ciorLngNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorLngNmNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorLngNm = MarshalByte.readString(buffer, position, Len.CIOR_LNG_NM);
	}

	public byte[] getCltObjRelationDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, clientIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeChar(buffer, position, rltTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rltTypCd, Len.RLT_TYP_CD);
		position += Len.RLT_TYP_CD;
		MarshalByte.writeChar(buffer, position, objCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, objCd, Len.OBJ_CD);
		position += Len.OBJ_CD;
		MarshalByte.writeChar(buffer, position, ciorShwObjKeyCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorShwObjKey, Len.CIOR_SHW_OBJ_KEY);
		position += Len.CIOR_SHW_OBJ_KEY;
		MarshalByte.writeChar(buffer, position, adrSeqNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, adrSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, adrSeqNbr, Len.ADR_SEQ_NBR);
		position += Len.ADR_SEQ_NBR;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, ciorExpDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorExpDt, Len.CIOR_EXP_DT);
		position += Len.CIOR_EXP_DT;
		MarshalByte.writeChar(buffer, position, ciorEffAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorEffAcyTs, Len.CIOR_EFF_ACY_TS);
		position += Len.CIOR_EFF_ACY_TS;
		MarshalByte.writeChar(buffer, position, ciorExpAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorExpAcyTs, Len.CIOR_EXP_ACY_TS);
		position += Len.CIOR_EXP_ACY_TS;
		MarshalByte.writeChar(buffer, position, ciorLegEntCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorLegEntCd, Len.CIOR_LEG_ENT_CD);
		position += Len.CIOR_LEG_ENT_CD;
		MarshalByte.writeChar(buffer, position, ciorFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorFstNm, Len.CIOR_FST_NM);
		position += Len.CIOR_FST_NM;
		MarshalByte.writeChar(buffer, position, ciorLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorLstNm, Len.CIOR_LST_NM);
		position += Len.CIOR_LST_NM;
		MarshalByte.writeChar(buffer, position, ciorMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorMdlNm, Len.CIOR_MDL_NM);
		position += Len.CIOR_MDL_NM;
		MarshalByte.writeChar(buffer, position, ciorNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorNmPfx, Len.CIOR_NM_PFX);
		position += Len.CIOR_NM_PFX;
		MarshalByte.writeChar(buffer, position, ciorNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorNmSfx, Len.CIOR_NM_SFX);
		position += Len.CIOR_NM_SFX;
		MarshalByte.writeChar(buffer, position, ciorLngNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciorLngNmNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorLngNm, Len.CIOR_LNG_NM);
		return buffer;
	}

	public void initCltObjRelationDataSpaces() {
		clientIdCi = Types.SPACE_CHAR;
		clientId = "";
		rltTypCdCi = Types.SPACE_CHAR;
		rltTypCd = "";
		objCdCi = Types.SPACE_CHAR;
		objCd = "";
		ciorShwObjKeyCi = Types.SPACE_CHAR;
		ciorShwObjKey = "";
		adrSeqNbrCi = Types.SPACE_CHAR;
		adrSeqNbrSign = Types.SPACE_CHAR;
		adrSeqNbr = "";
		userIdCi = Types.SPACE_CHAR;
		userId = "";
		statusCdCi = Types.SPACE_CHAR;
		statusCd = Types.SPACE_CHAR;
		terminalIdCi = Types.SPACE_CHAR;
		terminalId = "";
		ciorExpDtCi = Types.SPACE_CHAR;
		ciorExpDt = "";
		ciorEffAcyTsCi = Types.SPACE_CHAR;
		ciorEffAcyTs = "";
		ciorExpAcyTsCi = Types.SPACE_CHAR;
		ciorExpAcyTs = "";
		ciorLegEntCdCi = Types.SPACE_CHAR;
		ciorLegEntCd = "";
		ciorFstNmCi = Types.SPACE_CHAR;
		ciorFstNm = "";
		ciorLstNmCi = Types.SPACE_CHAR;
		ciorLstNm = "";
		ciorMdlNmCi = Types.SPACE_CHAR;
		ciorMdlNm = "";
		ciorNmPfxCi = Types.SPACE_CHAR;
		ciorNmPfx = "";
		ciorNmSfxCi = Types.SPACE_CHAR;
		ciorNmSfx = "";
		ciorLngNmCi = Types.SPACE_CHAR;
		ciorLngNmNi = Types.SPACE_CHAR;
		ciorLngNm = "";
	}

	public void setClientIdCi(char clientIdCi) {
		this.clientIdCi = clientIdCi;
	}

	public char getClientIdCi() {
		return this.clientIdCi;
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setRltTypCdCi(char rltTypCdCi) {
		this.rltTypCdCi = rltTypCdCi;
	}

	public char getRltTypCdCi() {
		return this.rltTypCdCi;
	}

	public void setRltTypCd(String rltTypCd) {
		this.rltTypCd = Functions.subString(rltTypCd, Len.RLT_TYP_CD);
	}

	public String getRltTypCd() {
		return this.rltTypCd;
	}

	public void setObjCdCi(char objCdCi) {
		this.objCdCi = objCdCi;
	}

	public char getObjCdCi() {
		return this.objCdCi;
	}

	public void setObjCd(String objCd) {
		this.objCd = Functions.subString(objCd, Len.OBJ_CD);
	}

	public String getObjCd() {
		return this.objCd;
	}

	public void setCiorShwObjKeyCi(char ciorShwObjKeyCi) {
		this.ciorShwObjKeyCi = ciorShwObjKeyCi;
	}

	public char getCiorShwObjKeyCi() {
		return this.ciorShwObjKeyCi;
	}

	public void setCiorShwObjKey(String ciorShwObjKey) {
		this.ciorShwObjKey = Functions.subString(ciorShwObjKey, Len.CIOR_SHW_OBJ_KEY);
	}

	public String getCiorShwObjKey() {
		return this.ciorShwObjKey;
	}

	public void setAdrSeqNbrCi(char adrSeqNbrCi) {
		this.adrSeqNbrCi = adrSeqNbrCi;
	}

	public char getAdrSeqNbrCi() {
		return this.adrSeqNbrCi;
	}

	public void setAdrSeqNbrSign(char adrSeqNbrSign) {
		this.adrSeqNbrSign = adrSeqNbrSign;
	}

	public void setAdrSeqNbrSignFormatted(String adrSeqNbrSign) {
		setAdrSeqNbrSign(Functions.charAt(adrSeqNbrSign, Types.CHAR_SIZE));
	}

	public char getAdrSeqNbrSign() {
		return this.adrSeqNbrSign;
	}

	public void setAdrSeqNbrFormatted(String adrSeqNbr) {
		this.adrSeqNbr = Trunc.toUnsignedNumeric(adrSeqNbr, Len.ADR_SEQ_NBR);
	}

	public int getAdrSeqNbr() {
		return NumericDisplay.asInt(this.adrSeqNbr);
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setCiorExpDtCi(char ciorExpDtCi) {
		this.ciorExpDtCi = ciorExpDtCi;
	}

	public char getCiorExpDtCi() {
		return this.ciorExpDtCi;
	}

	public void setCiorExpDt(String ciorExpDt) {
		this.ciorExpDt = Functions.subString(ciorExpDt, Len.CIOR_EXP_DT);
	}

	public String getCiorExpDt() {
		return this.ciorExpDt;
	}

	public void setCiorEffAcyTsCi(char ciorEffAcyTsCi) {
		this.ciorEffAcyTsCi = ciorEffAcyTsCi;
	}

	public char getCiorEffAcyTsCi() {
		return this.ciorEffAcyTsCi;
	}

	public void setCiorEffAcyTs(String ciorEffAcyTs) {
		this.ciorEffAcyTs = Functions.subString(ciorEffAcyTs, Len.CIOR_EFF_ACY_TS);
	}

	public String getCiorEffAcyTs() {
		return this.ciorEffAcyTs;
	}

	public void setCiorExpAcyTsCi(char ciorExpAcyTsCi) {
		this.ciorExpAcyTsCi = ciorExpAcyTsCi;
	}

	public char getCiorExpAcyTsCi() {
		return this.ciorExpAcyTsCi;
	}

	public void setCiorExpAcyTs(String ciorExpAcyTs) {
		this.ciorExpAcyTs = Functions.subString(ciorExpAcyTs, Len.CIOR_EXP_ACY_TS);
	}

	public String getCiorExpAcyTs() {
		return this.ciorExpAcyTs;
	}

	public void setCiorLegEntCdCi(char ciorLegEntCdCi) {
		this.ciorLegEntCdCi = ciorLegEntCdCi;
	}

	public char getCiorLegEntCdCi() {
		return this.ciorLegEntCdCi;
	}

	public void setCiorLegEntCd(String ciorLegEntCd) {
		this.ciorLegEntCd = Functions.subString(ciorLegEntCd, Len.CIOR_LEG_ENT_CD);
	}

	public String getCiorLegEntCd() {
		return this.ciorLegEntCd;
	}

	public void setCiorFstNmCi(char ciorFstNmCi) {
		this.ciorFstNmCi = ciorFstNmCi;
	}

	public char getCiorFstNmCi() {
		return this.ciorFstNmCi;
	}

	public void setCiorFstNm(String ciorFstNm) {
		this.ciorFstNm = Functions.subString(ciorFstNm, Len.CIOR_FST_NM);
	}

	public String getCiorFstNm() {
		return this.ciorFstNm;
	}

	public void setCiorLstNmCi(char ciorLstNmCi) {
		this.ciorLstNmCi = ciorLstNmCi;
	}

	public char getCiorLstNmCi() {
		return this.ciorLstNmCi;
	}

	public void setCiorLstNm(String ciorLstNm) {
		this.ciorLstNm = Functions.subString(ciorLstNm, Len.CIOR_LST_NM);
	}

	public String getCiorLstNm() {
		return this.ciorLstNm;
	}

	public void setCiorMdlNmCi(char ciorMdlNmCi) {
		this.ciorMdlNmCi = ciorMdlNmCi;
	}

	public char getCiorMdlNmCi() {
		return this.ciorMdlNmCi;
	}

	public void setCiorMdlNm(String ciorMdlNm) {
		this.ciorMdlNm = Functions.subString(ciorMdlNm, Len.CIOR_MDL_NM);
	}

	public String getCiorMdlNm() {
		return this.ciorMdlNm;
	}

	public void setCiorNmPfxCi(char ciorNmPfxCi) {
		this.ciorNmPfxCi = ciorNmPfxCi;
	}

	public char getCiorNmPfxCi() {
		return this.ciorNmPfxCi;
	}

	public void setCiorNmPfx(String ciorNmPfx) {
		this.ciorNmPfx = Functions.subString(ciorNmPfx, Len.CIOR_NM_PFX);
	}

	public String getCiorNmPfx() {
		return this.ciorNmPfx;
	}

	public void setCiorNmSfxCi(char ciorNmSfxCi) {
		this.ciorNmSfxCi = ciorNmSfxCi;
	}

	public char getCiorNmSfxCi() {
		return this.ciorNmSfxCi;
	}

	public void setCiorNmSfx(String ciorNmSfx) {
		this.ciorNmSfx = Functions.subString(ciorNmSfx, Len.CIOR_NM_SFX);
	}

	public String getCiorNmSfx() {
		return this.ciorNmSfx;
	}

	public void setCiorLngNmCi(char ciorLngNmCi) {
		this.ciorLngNmCi = ciorLngNmCi;
	}

	public char getCiorLngNmCi() {
		return this.ciorLngNmCi;
	}

	public void setCiorLngNmNi(char ciorLngNmNi) {
		this.ciorLngNmNi = ciorLngNmNi;
	}

	public char getCiorLngNmNi() {
		return this.ciorLngNmNi;
	}

	public void setCiorLngNm(String ciorLngNm) {
		this.ciorLngNm = Functions.subString(ciorLngNm, Len.CIOR_LNG_NM);
	}

	public void setCiorLngNmSubstring(String replacement, int start, int length) {
		ciorLngNm = Functions.setSubstring(ciorLngNm, replacement, start, length);
	}

	public String getCiorLngNm() {
		return this.ciorLngNm;
	}

	public String getCiorLngNmFormatted() {
		return Functions.padBlanks(getCiorLngNm(), Len.CIOR_LNG_NM);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ID = 20;
		public static final int RLT_TYP_CD = 4;
		public static final int OBJ_CD = 4;
		public static final int CIOR_SHW_OBJ_KEY = 30;
		public static final int ADR_SEQ_NBR = 5;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CIOR_EXP_DT = 10;
		public static final int CIOR_EFF_ACY_TS = 26;
		public static final int CIOR_EXP_ACY_TS = 26;
		public static final int CIOR_LEG_ENT_CD = 2;
		public static final int CIOR_FST_NM = 30;
		public static final int CIOR_LST_NM = 60;
		public static final int CIOR_MDL_NM = 30;
		public static final int CIOR_NM_PFX = 4;
		public static final int CIOR_NM_SFX = 4;
		public static final int CIOR_LNG_NM = 132;
		public static final int CLIENT_ID_CI = 1;
		public static final int RLT_TYP_CD_CI = 1;
		public static final int OBJ_CD_CI = 1;
		public static final int CIOR_SHW_OBJ_KEY_CI = 1;
		public static final int ADR_SEQ_NBR_CI = 1;
		public static final int ADR_SEQ_NBR_SIGN = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int CIOR_EXP_DT_CI = 1;
		public static final int CIOR_EFF_ACY_TS_CI = 1;
		public static final int CIOR_EXP_ACY_TS_CI = 1;
		public static final int CIOR_LEG_ENT_CD_CI = 1;
		public static final int CIOR_FST_NM_CI = 1;
		public static final int CIOR_LST_NM_CI = 1;
		public static final int CIOR_MDL_NM_CI = 1;
		public static final int CIOR_NM_PFX_CI = 1;
		public static final int CIOR_NM_SFX_CI = 1;
		public static final int CIOR_LNG_NM_CI = 1;
		public static final int CIOR_LNG_NM_NI = 1;
		public static final int CLT_OBJ_RELATION_DATA = CLIENT_ID_CI + CLIENT_ID + RLT_TYP_CD_CI + RLT_TYP_CD + OBJ_CD_CI + OBJ_CD
				+ CIOR_SHW_OBJ_KEY_CI + CIOR_SHW_OBJ_KEY + ADR_SEQ_NBR_CI + ADR_SEQ_NBR_SIGN + ADR_SEQ_NBR + USER_ID_CI + USER_ID + STATUS_CD_CI
				+ STATUS_CD + TERMINAL_ID_CI + TERMINAL_ID + CIOR_EXP_DT_CI + CIOR_EXP_DT + CIOR_EFF_ACY_TS_CI + CIOR_EFF_ACY_TS + CIOR_EXP_ACY_TS_CI
				+ CIOR_EXP_ACY_TS + CIOR_LEG_ENT_CD_CI + CIOR_LEG_ENT_CD + CIOR_FST_NM_CI + CIOR_FST_NM + CIOR_LST_NM_CI + CIOR_LST_NM
				+ CIOR_MDL_NM_CI + CIOR_MDL_NM + CIOR_NM_PFX_CI + CIOR_NM_PFX + CIOR_NM_SFX_CI + CIOR_NM_SFX + CIOR_LNG_NM_CI + CIOR_LNG_NM_NI
				+ CIOR_LNG_NM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
