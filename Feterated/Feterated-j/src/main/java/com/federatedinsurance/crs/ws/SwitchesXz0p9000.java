/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.SwCallAddActNotWrdSvcFg;
import com.federatedinsurance.crs.ws.enums.SwEndStCncWrdRqrCsrFlag;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ0P9000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz0p9000 {

	//==== PROPERTIES ====
	//Original name: SW-END-ST-CNC-WRD-RQR-CSR-FLAG
	private SwEndStCncWrdRqrCsrFlag endStCncWrdRqrCsrFlag = new SwEndStCncWrdRqrCsrFlag();
	//Original name: SW-CALL-ADD-ACT-NOT-WRD-SVC-FG
	private SwCallAddActNotWrdSvcFg callAddActNotWrdSvcFg = new SwCallAddActNotWrdSvcFg();
	//Original name: SW-GET-POL-DTL-SVC-CALLED-FLAG
	private boolean getPolDtlSvcCalledFlag = false;
	//Original name: SW-ACT-NOT-WRD-SVC-CALLED-FLAG
	private boolean actNotWrdSvcCalledFlag = false;
	//Original name: SW-OOS-AS-OF-DATE-RQR-FLAG
	private boolean oosAsOfDateRqrFlag = false;

	//==== METHODS ====
	public void setGetPolDtlSvcCalledFlag(boolean getPolDtlSvcCalledFlag) {
		this.getPolDtlSvcCalledFlag = getPolDtlSvcCalledFlag;
	}

	public boolean isGetPolDtlSvcCalledFlag() {
		return this.getPolDtlSvcCalledFlag;
	}

	public void setActNotWrdSvcCalledFlag(boolean actNotWrdSvcCalledFlag) {
		this.actNotWrdSvcCalledFlag = actNotWrdSvcCalledFlag;
	}

	public boolean isActNotWrdSvcCalledFlag() {
		return this.actNotWrdSvcCalledFlag;
	}

	public void setOosAsOfDateRqrFlag(boolean oosAsOfDateRqrFlag) {
		this.oosAsOfDateRqrFlag = oosAsOfDateRqrFlag;
	}

	public boolean isOosAsOfDateRqrFlag() {
		return this.oosAsOfDateRqrFlag;
	}

	public SwCallAddActNotWrdSvcFg getCallAddActNotWrdSvcFg() {
		return callAddActNotWrdSvcFg;
	}

	public SwEndStCncWrdRqrCsrFlag getEndStCncWrdRqrCsrFlag() {
		return endStCncWrdRqrCsrFlag;
	}
}
