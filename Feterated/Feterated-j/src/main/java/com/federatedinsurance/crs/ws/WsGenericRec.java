/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-GENERIC-REC<br>
 * Variable: WS-GENERIC-REC from program HALOUIEH<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsGenericRec extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: GEUR-DUMMY-DATA-FIELD
	private String dummyDataField = DefaultValues.stringVal(Len.DUMMY_DATA_FIELD);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_GENERIC_REC;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsGenericRecBytes(buf);
	}

	public String getWsGenericRecFormatted() {
		return getGenEmptyDataRowFormatted();
	}

	public void setWsGenericRecBytes(byte[] buffer) {
		setWsGenericRecBytes(buffer, 1);
	}

	public byte[] getWsGenericRecBytes() {
		byte[] buffer = new byte[Len.WS_GENERIC_REC];
		return getWsGenericRecBytes(buffer, 1);
	}

	public void setWsGenericRecBytes(byte[] buffer, int offset) {
		int position = offset;
		setGenEmptyDataRowBytes(buffer, position);
	}

	public byte[] getWsGenericRecBytes(byte[] buffer, int offset) {
		int position = offset;
		getGenEmptyDataRowBytes(buffer, position);
		return buffer;
	}

	public String getGenEmptyDataRowFormatted() {
		return getDummyDataFieldFormatted();
	}

	public void setGenEmptyDataRowBytes(byte[] buffer, int offset) {
		int position = offset;
		dummyDataField = MarshalByte.readString(buffer, position, Len.DUMMY_DATA_FIELD);
	}

	public byte[] getGenEmptyDataRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, dummyDataField, Len.DUMMY_DATA_FIELD);
		return buffer;
	}

	public void setDummyDataField(String dummyDataField) {
		this.dummyDataField = Functions.subString(dummyDataField, Len.DUMMY_DATA_FIELD);
	}

	public String getDummyDataField() {
		return this.dummyDataField;
	}

	public String getDummyDataFieldFormatted() {
		return Functions.padBlanks(getDummyDataField(), Len.DUMMY_DATA_FIELD);
	}

	@Override
	public byte[] serialize() {
		return getWsGenericRecBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DUMMY_DATA_FIELD = 32;
		public static final int GEN_EMPTY_DATA_ROW = DUMMY_DATA_FIELD;
		public static final int WS_GENERIC_REC = GEN_EMPTY_DATA_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
