/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-OPERATION-NAME<br>
 * Variable: WS-OPERATION-NAME from program XZ0X9050<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsOperationNameXz0x9050 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.WS_OPERATION_NAME);
	public static final String GET_POL_LIST_BY_NOT = "GetPolicyListByNotification";
	public static final String GET_POL_LIST_BY_FORM = "GetPolicyListByForm";
	private static final String[] VALID_OPERATION = new String[] { "GetPolicyListByNotification", "GetPolicyListByForm" };

	//==== METHODS ====
	public void setWsOperationName(String wsOperationName) {
		this.value = Functions.subString(wsOperationName, Len.WS_OPERATION_NAME);
	}

	public String getWsOperationName() {
		return this.value;
	}

	public boolean isValidOperation() {
		return ArrayUtils.contains(VALID_OPERATION, value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_OPERATION_NAME = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
