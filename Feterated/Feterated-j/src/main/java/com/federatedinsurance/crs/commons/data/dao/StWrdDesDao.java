/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IStWrdDes;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [ST_WRD_DES]
 * 
 */
public class StWrdDesDao extends BaseSqlDao<IStWrdDes> {

	public StWrdDesDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IStWrdDes> getToClass() {
		return IStWrdDes.class;
	}

	public short selectRec(String xzh005StWrdSeqCd, char cfYes, short dft) {
		return buildQuery("selectRec").bind("xzh005StWrdSeqCd", xzh005StWrdSeqCd).bind("cfYes", String.valueOf(cfYes)).scalarResultShort(dft);
	}
}
