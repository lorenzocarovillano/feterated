/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-INVALID-INPUT<br>
 * Variable: EA-02-INVALID-INPUT from program XZC06090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea02InvalidInputXzc06090 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-INVALID-INPUT
	private String flr1 = "INVALID ACCOUNT";
	//Original name: FILLER-EA-02-INVALID-INPUT-1
	private String flr2 = " NUMBER:";
	//Original name: EA-02-ACT-NBR
	private String ea02ActNbr = DefaultValues.stringVal(Len.EA02_ACT_NBR);

	//==== METHODS ====
	public String getEa02InvalidInputFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidInputBytes());
	}

	public byte[] getEa02InvalidInputBytes() {
		byte[] buffer = new byte[Len.EA02_INVALID_INPUT];
		return getEa02InvalidInputBytes(buffer, 1);
	}

	public byte[] getEa02InvalidInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, ea02ActNbr, Len.EA02_ACT_NBR);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setEa02ActNbr(String ea02ActNbr) {
		this.ea02ActNbr = Functions.subString(ea02ActNbr, Len.EA02_ACT_NBR);
	}

	public String getEa02ActNbr() {
		return this.ea02ActNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA02_ACT_NBR = 25;
		public static final int FLR1 = 15;
		public static final int FLR2 = 9;
		public static final int EA02_INVALID_INPUT = EA02_ACT_NBR + FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
