/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: AF-ADDRESS-LINES<br>
 * Variable: AF-ADDRESS-LINES from program TS529099<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AfAddressLines extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int TA_ADR_LINE_MAXOCCURS = 6;

	//==== CONSTRUCTORS ====
	public AfAddressLines() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.AF_ADDRESS_LINES;
	}

	/**Original name: AF-ADDRESS-LINES<br>*/
	public byte[] getAfAddressLinesBytes() {
		byte[] buffer = new byte[Len.AF_ADDRESS_LINES];
		return getAfAddressLinesBytes(buffer, 1);
	}

	public byte[] getAfAddressLinesBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.AF_ADDRESS_LINES, Pos.AF_ADDRESS_LINES);
		return buffer;
	}

	public void setAlLine1(String alLine1) {
		writeString(Pos.AL_LINE1, alLine1, Len.AL_LINE1);
	}

	/**Original name: AF-AL-LINE-1<br>*/
	public String getAlLine1() {
		return readString(Pos.AL_LINE1, Len.AL_LINE1);
	}

	public void setAlLine2(String alLine2) {
		writeString(Pos.AL_LINE2, alLine2, Len.AL_LINE2);
	}

	/**Original name: AF-AL-LINE-2<br>*/
	public String getAlLine2() {
		return readString(Pos.AL_LINE2, Len.AL_LINE2);
	}

	public void setAlLine3(String alLine3) {
		writeString(Pos.AL_LINE3, alLine3, Len.AL_LINE3);
	}

	/**Original name: AF-AL-LINE-3<br>*/
	public String getAlLine3() {
		return readString(Pos.AL_LINE3, Len.AL_LINE3);
	}

	public void setAlLine4(String alLine4) {
		writeString(Pos.AL_LINE4, alLine4, Len.AL_LINE4);
	}

	/**Original name: AF-AL-LINE-4<br>*/
	public String getAlLine4() {
		return readString(Pos.AL_LINE4, Len.AL_LINE4);
	}

	public void setAlLine5(String alLine5) {
		writeString(Pos.AL_LINE5, alLine5, Len.AL_LINE5);
	}

	/**Original name: AF-AL-LINE-5<br>*/
	public String getAlLine5() {
		return readString(Pos.AL_LINE5, Len.AL_LINE5);
	}

	public void setAlLine6(String alLine6) {
		writeString(Pos.AL_LINE6, alLine6, Len.AL_LINE6);
	}

	/**Original name: AF-AL-LINE-6<br>*/
	public String getAlLine6() {
		return readString(Pos.AL_LINE6, Len.AL_LINE6);
	}

	public void setAfTableOfAddressLinesSubstring(String replacement, int start, int length) {
		writeSubstring(Pos.AF_TABLE_OF_ADDRESS_LINES, replacement, start, length, Len.AF_TABLE_OF_ADDRESS_LINES);
	}

	public String getAfTableOfAddressLinesFormatted() {
		return readFixedString(Pos.AF_TABLE_OF_ADDRESS_LINES, Len.AF_TABLE_OF_ADDRESS_LINES);
	}

	public void setTaAdrLine(int taAdrLineIdx, String taAdrLine) {
		int position = Pos.afTaAdrLine(taAdrLineIdx - 1);
		writeString(position, taAdrLine, Len.TA_ADR_LINE);
	}

	/**Original name: AF-TA-ADR-LINE<br>*/
	public String getTaAdrLine(int taAdrLineIdx) {
		int position = Pos.afTaAdrLine(taAdrLineIdx - 1);
		return readString(position, Len.TA_ADR_LINE);
	}

	public String getTaAdrLineFormatted(int taAdrLineIdx) {
		return Functions.padBlanks(getTaAdrLine(taAdrLineIdx), Len.TA_ADR_LINE);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int AF_ADDRESS_LINES = 1;
		public static final int AL_LINE1 = AF_ADDRESS_LINES;
		public static final int AL_LINE2 = AL_LINE1 + Len.AL_LINE1;
		public static final int AL_LINE3 = AL_LINE2 + Len.AL_LINE2;
		public static final int AL_LINE4 = AL_LINE3 + Len.AL_LINE3;
		public static final int AL_LINE5 = AL_LINE4 + Len.AL_LINE4;
		public static final int AL_LINE6 = AL_LINE5 + Len.AL_LINE5;
		public static final int AF_TABLE_OF_ADDRESS_LINES = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int afTaAdrLine(int idx) {
			return AF_TABLE_OF_ADDRESS_LINES + idx * Len.TA_ADR_LINE;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int AL_LINE1 = 45;
		public static final int AL_LINE2 = 45;
		public static final int AL_LINE3 = 45;
		public static final int AL_LINE4 = 45;
		public static final int AL_LINE5 = 45;
		public static final int TA_ADR_LINE = 45;
		public static final int AL_LINE6 = 45;
		public static final int AF_ADDRESS_LINES = AL_LINE1 + AL_LINE2 + AL_LINE3 + AL_LINE4 + AL_LINE5 + AL_LINE6;
		public static final int AF_TABLE_OF_ADDRESS_LINES = AfAddressLines.TA_ADR_LINE_MAXOCCURS * TA_ADR_LINE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
