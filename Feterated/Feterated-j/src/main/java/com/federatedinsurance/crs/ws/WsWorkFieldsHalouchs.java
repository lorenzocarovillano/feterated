/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.redefines.WsCharTable;

/**Original name: WS-WORK-FIELDS<br>
 * Variable: WS-WORK-FIELDS from program HALOUCHS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkFieldsHalouchs {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "HALOUCHS";
	//Original name: WS-RESPONSE-CODE
	private int wsResponseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int wsResponseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-DATA-KEY-DISP
	private String wsDataKeyDisp = DefaultValues.stringVal(Len.WS_DATA_KEY_DISP);
	//Original name: WS-CKSUM
	private long wsCksum = DefaultValues.BIN_LONG_VAL;
	//Original name: WS-BYTE-NUMBER
	private int wsByteNumber = DefaultValues.BIN_INT_VAL;
	//Original name: WS-HEX-CHAR
	private WsHexChar wsHexChar = new WsHexChar();
	//Original name: WS-SUB1
	private int wsSub1 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-CHAR-TABLE
	private WsCharTable wsCharTable = new WsCharTable();

	//==== METHODS ====
	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public void setWsResponseCode(int wsResponseCode) {
		this.wsResponseCode = wsResponseCode;
	}

	public int getWsResponseCode() {
		return this.wsResponseCode;
	}

	public void setWsResponseCode2(int wsResponseCode2) {
		this.wsResponseCode2 = wsResponseCode2;
	}

	public int getWsResponseCode2() {
		return this.wsResponseCode2;
	}

	public void setWsDataKeyDisp(long wsDataKeyDisp) {
		this.wsDataKeyDisp = PicFormatter.display("-Z(8)9").format(wsDataKeyDisp).toString();
	}

	public long getWsDataKeyDisp() {
		return PicParser.display("-Z(8)9").parseLong(this.wsDataKeyDisp);
	}

	public String getWsDataKeyDispFormatted() {
		return this.wsDataKeyDisp;
	}

	public String getWsDataKeyDispAsString() {
		return getWsDataKeyDispFormatted();
	}

	public void setWsCksum(long wsCksum) {
		this.wsCksum = wsCksum;
	}

	public long getWsCksum() {
		return this.wsCksum;
	}

	public void setWsByteNumber(int wsByteNumber) {
		this.wsByteNumber = wsByteNumber;
	}

	public int getWsByteNumber() {
		return this.wsByteNumber;
	}

	public void setWsSub1(int wsSub1) {
		this.wsSub1 = wsSub1;
	}

	public int getWsSub1() {
		return this.wsSub1;
	}

	public WsCharTable getWsCharTable() {
		return wsCharTable;
	}

	public WsHexChar getWsHexChar() {
		return wsHexChar;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_DATA_KEY_DISP = 10;
		public static final int WS_PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
