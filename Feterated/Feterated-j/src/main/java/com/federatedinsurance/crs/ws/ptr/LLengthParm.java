/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-LENGTH-PARM<br>
 * Variable: L-LENGTH-PARM from program TS030099<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LLengthParm extends BytesClass {

	//==== PROPERTIES ====
	public static final short LP132_BYTE_RECORD = ((short) 132);
	public static final short LP131_BYTE_RECORD = ((short) 131);
	public static final short LP198_BYTE_RECORD = ((short) 198);

	//==== CONSTRUCTORS ====
	public LLengthParm() {
	}

	public LLengthParm(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_LENGTH_PARM;
	}

	public byte[] getlLengthParmBytes() {
		byte[] buffer = new byte[Len.L_LENGTH_PARM];
		return getlLengthParmBytes(buffer, 1);
	}

	public byte[] getlLengthParmBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.L_LENGTH_PARM, Pos.L_LENGTH_PARM);
		return buffer;
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_LENGTH_PARM = 1;
		public static final int DATA_SIZE = L_LENGTH_PARM;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int DATA_SIZE = 2;
		public static final int L_LENGTH_PARM = DATA_SIZE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
