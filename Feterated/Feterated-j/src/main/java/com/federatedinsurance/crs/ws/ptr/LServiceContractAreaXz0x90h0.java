/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90H0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90h0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int I_POLICY_LIST_MAXOCCURS = 50;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90h0() {
	}

	public LServiceContractAreaXz0x90h0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiTkNotPrcTs(String iTkNotPrcTs) {
		writeString(Pos.I_TK_NOT_PRC_TS, iTkNotPrcTs, Len.I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9HI-TK-NOT-PRC-TS<br>*/
	public String getiTkNotPrcTs() {
		return readString(Pos.I_TK_NOT_PRC_TS, Len.I_TK_NOT_PRC_TS);
	}

	public void setiCsrActNbr(String iCsrActNbr) {
		writeString(Pos.I_CSR_ACT_NBR, iCsrActNbr, Len.I_CSR_ACT_NBR);
	}

	/**Original name: XZT9HI-CSR-ACT-NBR<br>*/
	public String getiCsrActNbr() {
		return readString(Pos.I_CSR_ACT_NBR, Len.I_CSR_ACT_NBR);
	}

	public void setiUserid(String iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT9HI-USERID<br>*/
	public String getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	public String getiUseridFormatted() {
		return Functions.padBlanks(getiUserid(), Len.I_USERID);
	}

	public void setiPolNbr(int iPolNbrIdx, String iPolNbr) {
		int position = Pos.xzt9hiPolNbr(iPolNbrIdx - 1);
		writeString(position, iPolNbr, Len.I_POL_NBR);
	}

	/**Original name: XZT9HI-POL-NBR<br>*/
	public String getiPolNbr(int iPolNbrIdx) {
		int position = Pos.xzt9hiPolNbr(iPolNbrIdx - 1);
		return readString(position, Len.I_POL_NBR);
	}

	public void setiPolEffDt(int iPolEffDtIdx, String iPolEffDt) {
		int position = Pos.xzt9hiPolEffDt(iPolEffDtIdx - 1);
		writeString(position, iPolEffDt, Len.I_POL_EFF_DT);
	}

	/**Original name: XZT9HI-POL-EFF-DT<br>*/
	public String getiPolEffDt(int iPolEffDtIdx) {
		int position = Pos.xzt9hiPolEffDt(iPolEffDtIdx - 1);
		return readString(position, Len.I_POL_EFF_DT);
	}

	public void setiPolExpDt(int iPolExpDtIdx, String iPolExpDt) {
		int position = Pos.xzt9hiPolExpDt(iPolExpDtIdx - 1);
		writeString(position, iPolExpDt, Len.I_POL_EXP_DT);
	}

	/**Original name: XZT9HI-POL-EXP-DT<br>*/
	public String getiPolExpDt(int iPolExpDtIdx) {
		int position = Pos.xzt9hiPolExpDt(iPolExpDtIdx - 1);
		return readString(position, Len.I_POL_EXP_DT);
	}

	public void setiNotEffDt(int iNotEffDtIdx, String iNotEffDt) {
		int position = Pos.xzt9hiNotEffDt(iNotEffDtIdx - 1);
		writeString(position, iNotEffDt, Len.I_NOT_EFF_DT);
	}

	/**Original name: XZT9HI-NOT-EFF-DT<br>*/
	public String getiNotEffDt(int iNotEffDtIdx) {
		int position = Pos.xzt9hiNotEffDt(iNotEffDtIdx - 1);
		return readString(position, Len.I_NOT_EFF_DT);
	}

	public void setiPolDueAmt(int iPolDueAmtIdx, AfDecimal iPolDueAmt) {
		int position = Pos.xzt9hiPolDueAmt(iPolDueAmtIdx - 1);
		writeDecimal(position, iPolDueAmt.copy());
	}

	/**Original name: XZT9HI-POL-DUE-AMT<br>*/
	public AfDecimal getiPolDueAmt(int iPolDueAmtIdx) {
		int position = Pos.xzt9hiPolDueAmt(iPolDueAmtIdx - 1);
		return readDecimal(position, Len.Int.I_POL_DUE_AMT, Len.Fract.I_POL_DUE_AMT);
	}

	public void setiPolBilStaCd(int iPolBilStaCdIdx, char iPolBilStaCd) {
		int position = Pos.xzt9hiPolBilStaCd(iPolBilStaCdIdx - 1);
		writeChar(position, iPolBilStaCd);
	}

	/**Original name: XZT9HI-POL-BIL-STA-CD<br>*/
	public char getiPolBilStaCd(int iPolBilStaCdIdx) {
		int position = Pos.xzt9hiPolBilStaCd(iPolBilStaCdIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT9H0_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_TECHNICAL_KEY = XZT9H0_SERVICE_INPUTS;
		public static final int I_TK_NOT_PRC_TS = I_TECHNICAL_KEY;
		public static final int I_CSR_ACT_NBR = I_TK_NOT_PRC_TS + Len.I_TK_NOT_PRC_TS;
		public static final int I_USERID = I_CSR_ACT_NBR + Len.I_CSR_ACT_NBR;
		public static final int I_POLICY_LIST_TBL = I_USERID + Len.I_USERID;
		public static final int XZT9H0_SERVICE_OUTPUTS = xzt9hiPolBilStaCd(I_POLICY_LIST_MAXOCCURS - 1) + Len.I_POL_BIL_STA_CD;
		public static final int FLR1 = XZT9H0_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt9hiPolicyList(int idx) {
			return I_POLICY_LIST_TBL + idx * Len.I_POLICY_LIST;
		}

		public static int xzt9hiPolNbr(int idx) {
			return xzt9hiPolicyList(idx);
		}

		public static int xzt9hiPolEffDt(int idx) {
			return xzt9hiPolNbr(idx) + Len.I_POL_NBR;
		}

		public static int xzt9hiPolExpDt(int idx) {
			return xzt9hiPolEffDt(idx) + Len.I_POL_EFF_DT;
		}

		public static int xzt9hiNotEffDt(int idx) {
			return xzt9hiPolExpDt(idx) + Len.I_POL_EXP_DT;
		}

		public static int xzt9hiPolDueAmt(int idx) {
			return xzt9hiNotEffDt(idx) + Len.I_NOT_EFF_DT;
		}

		public static int xzt9hiPolBilStaCd(int idx) {
			return xzt9hiPolDueAmt(idx) + Len.I_POL_DUE_AMT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_TK_NOT_PRC_TS = 26;
		public static final int I_CSR_ACT_NBR = 9;
		public static final int I_USERID = 8;
		public static final int I_POL_NBR = 25;
		public static final int I_POL_EFF_DT = 10;
		public static final int I_POL_EXP_DT = 10;
		public static final int I_NOT_EFF_DT = 10;
		public static final int I_POL_DUE_AMT = 10;
		public static final int I_POL_BIL_STA_CD = 1;
		public static final int I_POLICY_LIST = I_POL_NBR + I_POL_EFF_DT + I_POL_EXP_DT + I_NOT_EFF_DT + I_POL_DUE_AMT + I_POL_BIL_STA_CD;
		public static final int I_TECHNICAL_KEY = I_TK_NOT_PRC_TS;
		public static final int I_POLICY_LIST_TBL = LServiceContractAreaXz0x90h0.I_POLICY_LIST_MAXOCCURS * I_POLICY_LIST;
		public static final int XZT9H0_SERVICE_INPUTS = I_TECHNICAL_KEY + I_CSR_ACT_NBR + I_USERID + I_POLICY_LIST_TBL;
		public static final int FLR1 = 1;
		public static final int XZT9H0_SERVICE_OUTPUTS = FLR1;
		public static final int L_SERVICE_CONTRACT_AREA = XZT9H0_SERVICE_INPUTS + XZT9H0_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int I_POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int I_POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
