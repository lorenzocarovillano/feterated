/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-WORK-AREAS<br>
 * Variable: WS-WORK-AREAS from program HALRLOMG<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkAreasHalrlomg {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "HALRLOMG";
	//Original name: WS-DEFAULT-TS
	private String defaultTs = "0001-01-01-00.00.00.000000";

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getDefaultTs() {
		return this.defaultTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
