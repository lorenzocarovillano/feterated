/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X90M0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x90m0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x90m0() {
	}

	public LFrameworkRequestAreaXz0x90m0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzy9m0qCsrActNbr(String xzy9m0qCsrActNbr) {
		writeString(Pos.XZY9M0Q_CSR_ACT_NBR, xzy9m0qCsrActNbr, Len.XZY9M0Q_CSR_ACT_NBR);
	}

	/**Original name: XZY9M0Q-CSR-ACT-NBR<br>*/
	public String getXzy9m0qCsrActNbr() {
		return readString(Pos.XZY9M0Q_CSR_ACT_NBR, Len.XZY9M0Q_CSR_ACT_NBR);
	}

	public void setXzy9m0qNotPrcTs(String xzy9m0qNotPrcTs) {
		writeString(Pos.XZY9M0Q_NOT_PRC_TS, xzy9m0qNotPrcTs, Len.XZY9M0Q_NOT_PRC_TS);
	}

	/**Original name: XZY9M0Q-NOT-PRC-TS<br>*/
	public String getXzy9m0qNotPrcTs() {
		return readString(Pos.XZY9M0Q_NOT_PRC_TS, Len.XZY9M0Q_NOT_PRC_TS);
	}

	public void setXzy9m0qActNotStaCd(String xzy9m0qActNotStaCd) {
		writeString(Pos.XZY9M0Q_ACT_NOT_STA_CD, xzy9m0qActNotStaCd, Len.XZY9M0Q_ACT_NOT_STA_CD);
	}

	/**Original name: XZY9M0Q-ACT-NOT-STA-CD<br>*/
	public String getXzy9m0qActNotStaCd() {
		return readString(Pos.XZY9M0Q_ACT_NOT_STA_CD, Len.XZY9M0Q_ACT_NOT_STA_CD);
	}

	public void setXzy9m0qUserid(String xzy9m0qUserid) {
		writeString(Pos.XZY9M0Q_USERID, xzy9m0qUserid, Len.XZY9M0Q_USERID);
	}

	/**Original name: XZY9M0Q-USERID<br>*/
	public String getXzy9m0qUserid() {
		return readString(Pos.XZY9M0Q_USERID, Len.XZY9M0Q_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0Y90M0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZY9M0Q_UPDATE_NOT_ROW = L_FW_REQ_XZ0Y90M0;
		public static final int XZY9M0Q_CSR_ACT_NBR = XZY9M0Q_UPDATE_NOT_ROW;
		public static final int XZY9M0Q_NOT_PRC_TS = XZY9M0Q_CSR_ACT_NBR + Len.XZY9M0Q_CSR_ACT_NBR;
		public static final int XZY9M0Q_ACT_NOT_STA_CD = XZY9M0Q_NOT_PRC_TS + Len.XZY9M0Q_NOT_PRC_TS;
		public static final int XZY9M0Q_USERID = XZY9M0Q_ACT_NOT_STA_CD + Len.XZY9M0Q_ACT_NOT_STA_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY9M0Q_CSR_ACT_NBR = 9;
		public static final int XZY9M0Q_NOT_PRC_TS = 26;
		public static final int XZY9M0Q_ACT_NOT_STA_CD = 2;
		public static final int XZY9M0Q_USERID = 8;
		public static final int XZY9M0Q_UPDATE_NOT_ROW = XZY9M0Q_CSR_ACT_NBR + XZY9M0Q_NOT_PRC_TS + XZY9M0Q_ACT_NOT_STA_CD + XZY9M0Q_USERID;
		public static final int L_FW_REQ_XZ0Y90M0 = XZY9M0Q_UPDATE_NOT_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0Y90M0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
