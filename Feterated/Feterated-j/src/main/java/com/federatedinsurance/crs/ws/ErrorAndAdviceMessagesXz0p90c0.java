/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZ0P90C0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXz0p90c0 {

	//==== PROPERTIES ====
	//Original name: EA-01-NO-CER-HLD-NOT-IND-MSG
	private Ea01NoCerHldNotIndMsg ea01NoCerHldNotIndMsg = new Ea01NoCerHldNotIndMsg();
	//Original name: EA-02-NOTHING-ADDED-MSG
	private Ea02NothingAddedMsg ea02NothingAddedMsg = new Ea02NothingAddedMsg();
	//Original name: EA-03-MAX-CERTS-FOUND-MSG
	private Ea03MaxCertsFoundMsg ea03MaxCertsFoundMsg = new Ea03MaxCertsFoundMsg();

	//==== METHODS ====
	public Ea01NoCerHldNotIndMsg getEa01NoCerHldNotIndMsg() {
		return ea01NoCerHldNotIndMsg;
	}

	public Ea02NothingAddedMsg getEa02NothingAddedMsg() {
		return ea02NothingAddedMsg;
	}

	public Ea03MaxCertsFoundMsg getEa03MaxCertsFoundMsg() {
		return ea03MaxCertsFoundMsg;
	}
}
