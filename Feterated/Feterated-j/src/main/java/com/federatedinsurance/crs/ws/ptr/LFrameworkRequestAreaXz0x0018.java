/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X0018<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x0018 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x0018() {
	}

	public LFrameworkRequestAreaXz0x0018(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzc007ActNotFrmRecRowFormatted(String data) {
		writeString(Pos.XZC007Q_ACT_NOT_FRM_REC_ROW, data, Len.XZC007Q_ACT_NOT_FRM_REC_ROW);
	}

	public String getXzc007ActNotFrmRecRowFormatted() {
		return readFixedString(Pos.XZC007Q_ACT_NOT_FRM_REC_ROW, Len.XZC007Q_ACT_NOT_FRM_REC_ROW);
	}

	public void setXzc007qActNotFrmRecCsumFormatted(String xzc007qActNotFrmRecCsum) {
		writeString(Pos.XZC007Q_ACT_NOT_FRM_REC_CSUM, Trunc.toUnsignedNumeric(xzc007qActNotFrmRecCsum, Len.XZC007Q_ACT_NOT_FRM_REC_CSUM),
				Len.XZC007Q_ACT_NOT_FRM_REC_CSUM);
	}

	/**Original name: XZC007Q-ACT-NOT-FRM-REC-CSUM<br>*/
	public int getXzc007qActNotFrmRecCsum() {
		return readNumDispUnsignedInt(Pos.XZC007Q_ACT_NOT_FRM_REC_CSUM, Len.XZC007Q_ACT_NOT_FRM_REC_CSUM);
	}

	public String getXzc007rActNotFrmRecCsumFormatted() {
		return readFixedString(Pos.XZC007Q_ACT_NOT_FRM_REC_CSUM, Len.XZC007Q_ACT_NOT_FRM_REC_CSUM);
	}

	public void setXzc007qCsrActNbrKcre(String xzc007qCsrActNbrKcre) {
		writeString(Pos.XZC007Q_CSR_ACT_NBR_KCRE, xzc007qCsrActNbrKcre, Len.XZC007Q_CSR_ACT_NBR_KCRE);
	}

	/**Original name: XZC007Q-CSR-ACT-NBR-KCRE<br>*/
	public String getXzc007qCsrActNbrKcre() {
		return readString(Pos.XZC007Q_CSR_ACT_NBR_KCRE, Len.XZC007Q_CSR_ACT_NBR_KCRE);
	}

	public void setXzc007qNotPrcTsKcre(String xzc007qNotPrcTsKcre) {
		writeString(Pos.XZC007Q_NOT_PRC_TS_KCRE, xzc007qNotPrcTsKcre, Len.XZC007Q_NOT_PRC_TS_KCRE);
	}

	/**Original name: XZC007Q-NOT-PRC-TS-KCRE<br>*/
	public String getXzc007qNotPrcTsKcre() {
		return readString(Pos.XZC007Q_NOT_PRC_TS_KCRE, Len.XZC007Q_NOT_PRC_TS_KCRE);
	}

	public void setXzc007qFrmSeqNbrKcre(String xzc007qFrmSeqNbrKcre) {
		writeString(Pos.XZC007Q_FRM_SEQ_NBR_KCRE, xzc007qFrmSeqNbrKcre, Len.XZC007Q_FRM_SEQ_NBR_KCRE);
	}

	/**Original name: XZC007Q-FRM-SEQ-NBR-KCRE<br>*/
	public String getXzc007qFrmSeqNbrKcre() {
		return readString(Pos.XZC007Q_FRM_SEQ_NBR_KCRE, Len.XZC007Q_FRM_SEQ_NBR_KCRE);
	}

	public void setXzc007qRecSeqNbrKcre(String xzc007qRecSeqNbrKcre) {
		writeString(Pos.XZC007Q_REC_SEQ_NBR_KCRE, xzc007qRecSeqNbrKcre, Len.XZC007Q_REC_SEQ_NBR_KCRE);
	}

	/**Original name: XZC007Q-REC-SEQ-NBR-KCRE<br>*/
	public String getXzc007qRecSeqNbrKcre() {
		return readString(Pos.XZC007Q_REC_SEQ_NBR_KCRE, Len.XZC007Q_REC_SEQ_NBR_KCRE);
	}

	public void setXzc007qTransProcessDt(String xzc007qTransProcessDt) {
		writeString(Pos.XZC007Q_TRANS_PROCESS_DT, xzc007qTransProcessDt, Len.XZC007Q_TRANS_PROCESS_DT);
	}

	/**Original name: XZC007Q-TRANS-PROCESS-DT<br>*/
	public String getXzc007qTransProcessDt() {
		return readString(Pos.XZC007Q_TRANS_PROCESS_DT, Len.XZC007Q_TRANS_PROCESS_DT);
	}

	public void setXzc007qCsrActNbr(String xzc007qCsrActNbr) {
		writeString(Pos.XZC007Q_CSR_ACT_NBR, xzc007qCsrActNbr, Len.XZC007Q_CSR_ACT_NBR);
	}

	/**Original name: XZC007Q-CSR-ACT-NBR<br>*/
	public String getXzc007qCsrActNbr() {
		return readString(Pos.XZC007Q_CSR_ACT_NBR, Len.XZC007Q_CSR_ACT_NBR);
	}

	public void setXzc007qNotPrcTs(String xzc007qNotPrcTs) {
		writeString(Pos.XZC007Q_NOT_PRC_TS, xzc007qNotPrcTs, Len.XZC007Q_NOT_PRC_TS);
	}

	/**Original name: XZC007Q-NOT-PRC-TS<br>*/
	public String getXzc007qNotPrcTs() {
		return readString(Pos.XZC007Q_NOT_PRC_TS, Len.XZC007Q_NOT_PRC_TS);
	}

	public void setXzc007qFrmSeqNbrSign(char xzc007qFrmSeqNbrSign) {
		writeChar(Pos.XZC007Q_FRM_SEQ_NBR_SIGN, xzc007qFrmSeqNbrSign);
	}

	/**Original name: XZC007Q-FRM-SEQ-NBR-SIGN<br>*/
	public char getXzc007qFrmSeqNbrSign() {
		return readChar(Pos.XZC007Q_FRM_SEQ_NBR_SIGN);
	}

	public void setXzc007qFrmSeqNbrFormatted(String xzc007qFrmSeqNbr) {
		writeString(Pos.XZC007Q_FRM_SEQ_NBR, Trunc.toUnsignedNumeric(xzc007qFrmSeqNbr, Len.XZC007Q_FRM_SEQ_NBR), Len.XZC007Q_FRM_SEQ_NBR);
	}

	/**Original name: XZC007Q-FRM-SEQ-NBR<br>*/
	public int getXzc007qFrmSeqNbr() {
		return readNumDispUnsignedInt(Pos.XZC007Q_FRM_SEQ_NBR, Len.XZC007Q_FRM_SEQ_NBR);
	}

	public String getXzc007rFrmSeqNbrFormatted() {
		return readFixedString(Pos.XZC007Q_FRM_SEQ_NBR, Len.XZC007Q_FRM_SEQ_NBR);
	}

	public void setXzc007qRecSeqNbrSign(char xzc007qRecSeqNbrSign) {
		writeChar(Pos.XZC007Q_REC_SEQ_NBR_SIGN, xzc007qRecSeqNbrSign);
	}

	/**Original name: XZC007Q-REC-SEQ-NBR-SIGN<br>*/
	public char getXzc007qRecSeqNbrSign() {
		return readChar(Pos.XZC007Q_REC_SEQ_NBR_SIGN);
	}

	public void setXzc007qRecSeqNbrFormatted(String xzc007qRecSeqNbr) {
		writeString(Pos.XZC007Q_REC_SEQ_NBR, Trunc.toUnsignedNumeric(xzc007qRecSeqNbr, Len.XZC007Q_REC_SEQ_NBR), Len.XZC007Q_REC_SEQ_NBR);
	}

	/**Original name: XZC007Q-REC-SEQ-NBR<br>*/
	public int getXzc007qRecSeqNbr() {
		return readNumDispUnsignedInt(Pos.XZC007Q_REC_SEQ_NBR, Len.XZC007Q_REC_SEQ_NBR);
	}

	public String getXzc007rRecSeqNbrFormatted() {
		return readFixedString(Pos.XZC007Q_REC_SEQ_NBR, Len.XZC007Q_REC_SEQ_NBR);
	}

	public void setXzc007qCsrActNbrCi(char xzc007qCsrActNbrCi) {
		writeChar(Pos.XZC007Q_CSR_ACT_NBR_CI, xzc007qCsrActNbrCi);
	}

	public void setXzc007qCsrActNbrCiFormatted(String xzc007qCsrActNbrCi) {
		setXzc007qCsrActNbrCi(Functions.charAt(xzc007qCsrActNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC007Q-CSR-ACT-NBR-CI<br>*/
	public char getXzc007qCsrActNbrCi() {
		return readChar(Pos.XZC007Q_CSR_ACT_NBR_CI);
	}

	public void setXzc007qNotPrcTsCi(char xzc007qNotPrcTsCi) {
		writeChar(Pos.XZC007Q_NOT_PRC_TS_CI, xzc007qNotPrcTsCi);
	}

	public void setXzc007qNotPrcTsCiFormatted(String xzc007qNotPrcTsCi) {
		setXzc007qNotPrcTsCi(Functions.charAt(xzc007qNotPrcTsCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC007Q-NOT-PRC-TS-CI<br>*/
	public char getXzc007qNotPrcTsCi() {
		return readChar(Pos.XZC007Q_NOT_PRC_TS_CI);
	}

	public void setXzc007qFrmSeqNbrCi(char xzc007qFrmSeqNbrCi) {
		writeChar(Pos.XZC007Q_FRM_SEQ_NBR_CI, xzc007qFrmSeqNbrCi);
	}

	public void setXzc007qFrmSeqNbrCiFormatted(String xzc007qFrmSeqNbrCi) {
		setXzc007qFrmSeqNbrCi(Functions.charAt(xzc007qFrmSeqNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC007Q-FRM-SEQ-NBR-CI<br>*/
	public char getXzc007qFrmSeqNbrCi() {
		return readChar(Pos.XZC007Q_FRM_SEQ_NBR_CI);
	}

	public void setXzc007qRecSeqNbrCi(char xzc007qRecSeqNbrCi) {
		writeChar(Pos.XZC007Q_REC_SEQ_NBR_CI, xzc007qRecSeqNbrCi);
	}

	public void setXzc007qRecSeqNbrCiFormatted(String xzc007qRecSeqNbrCi) {
		setXzc007qRecSeqNbrCi(Functions.charAt(xzc007qRecSeqNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC007Q-REC-SEQ-NBR-CI<br>*/
	public char getXzc007qRecSeqNbrCi() {
		return readChar(Pos.XZC007Q_REC_SEQ_NBR_CI);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0C0007 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZC007Q_ACT_NOT_FRM_REC_ROW = L_FW_REQ_XZ0C0007;
		public static final int XZC007Q_ACT_NOT_FRM_REC_FIXED = XZC007Q_ACT_NOT_FRM_REC_ROW;
		public static final int XZC007Q_ACT_NOT_FRM_REC_CSUM = XZC007Q_ACT_NOT_FRM_REC_FIXED;
		public static final int XZC007Q_CSR_ACT_NBR_KCRE = XZC007Q_ACT_NOT_FRM_REC_CSUM + Len.XZC007Q_ACT_NOT_FRM_REC_CSUM;
		public static final int XZC007Q_NOT_PRC_TS_KCRE = XZC007Q_CSR_ACT_NBR_KCRE + Len.XZC007Q_CSR_ACT_NBR_KCRE;
		public static final int XZC007Q_FRM_SEQ_NBR_KCRE = XZC007Q_NOT_PRC_TS_KCRE + Len.XZC007Q_NOT_PRC_TS_KCRE;
		public static final int XZC007Q_REC_SEQ_NBR_KCRE = XZC007Q_FRM_SEQ_NBR_KCRE + Len.XZC007Q_FRM_SEQ_NBR_KCRE;
		public static final int XZC007Q_ACT_NOT_FRM_REC_DATES = XZC007Q_REC_SEQ_NBR_KCRE + Len.XZC007Q_REC_SEQ_NBR_KCRE;
		public static final int XZC007Q_TRANS_PROCESS_DT = XZC007Q_ACT_NOT_FRM_REC_DATES;
		public static final int XZC007Q_ACT_NOT_FRM_REC_KEY = XZC007Q_TRANS_PROCESS_DT + Len.XZC007Q_TRANS_PROCESS_DT;
		public static final int XZC007Q_CSR_ACT_NBR = XZC007Q_ACT_NOT_FRM_REC_KEY;
		public static final int XZC007Q_NOT_PRC_TS = XZC007Q_CSR_ACT_NBR + Len.XZC007Q_CSR_ACT_NBR;
		public static final int XZC007Q_FRM_SEQ_NBR_SIGN = XZC007Q_NOT_PRC_TS + Len.XZC007Q_NOT_PRC_TS;
		public static final int XZC007Q_FRM_SEQ_NBR = XZC007Q_FRM_SEQ_NBR_SIGN + Len.XZC007Q_FRM_SEQ_NBR_SIGN;
		public static final int XZC007Q_REC_SEQ_NBR_SIGN = XZC007Q_FRM_SEQ_NBR + Len.XZC007Q_FRM_SEQ_NBR;
		public static final int XZC007Q_REC_SEQ_NBR = XZC007Q_REC_SEQ_NBR_SIGN + Len.XZC007Q_REC_SEQ_NBR_SIGN;
		public static final int XZC007Q_ACT_NOT_FRM_REC_KEY_CI = XZC007Q_REC_SEQ_NBR + Len.XZC007Q_REC_SEQ_NBR;
		public static final int XZC007Q_CSR_ACT_NBR_CI = XZC007Q_ACT_NOT_FRM_REC_KEY_CI;
		public static final int XZC007Q_NOT_PRC_TS_CI = XZC007Q_CSR_ACT_NBR_CI + Len.XZC007Q_CSR_ACT_NBR_CI;
		public static final int XZC007Q_FRM_SEQ_NBR_CI = XZC007Q_NOT_PRC_TS_CI + Len.XZC007Q_NOT_PRC_TS_CI;
		public static final int XZC007Q_REC_SEQ_NBR_CI = XZC007Q_FRM_SEQ_NBR_CI + Len.XZC007Q_FRM_SEQ_NBR_CI;
		public static final int XZC007Q_ACT_NOT_FRM_REC_DATA = XZC007Q_REC_SEQ_NBR_CI + Len.XZC007Q_REC_SEQ_NBR_CI;
		public static final int FLR1 = XZC007Q_ACT_NOT_FRM_REC_DATA;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC007Q_ACT_NOT_FRM_REC_CSUM = 9;
		public static final int XZC007Q_CSR_ACT_NBR_KCRE = 32;
		public static final int XZC007Q_NOT_PRC_TS_KCRE = 32;
		public static final int XZC007Q_FRM_SEQ_NBR_KCRE = 32;
		public static final int XZC007Q_REC_SEQ_NBR_KCRE = 32;
		public static final int XZC007Q_TRANS_PROCESS_DT = 10;
		public static final int XZC007Q_CSR_ACT_NBR = 9;
		public static final int XZC007Q_NOT_PRC_TS = 26;
		public static final int XZC007Q_FRM_SEQ_NBR_SIGN = 1;
		public static final int XZC007Q_FRM_SEQ_NBR = 5;
		public static final int XZC007Q_REC_SEQ_NBR_SIGN = 1;
		public static final int XZC007Q_REC_SEQ_NBR = 5;
		public static final int XZC007Q_CSR_ACT_NBR_CI = 1;
		public static final int XZC007Q_NOT_PRC_TS_CI = 1;
		public static final int XZC007Q_FRM_SEQ_NBR_CI = 1;
		public static final int XZC007Q_REC_SEQ_NBR_CI = 1;
		public static final int XZC007Q_ACT_NOT_FRM_REC_FIXED = XZC007Q_ACT_NOT_FRM_REC_CSUM + XZC007Q_CSR_ACT_NBR_KCRE + XZC007Q_NOT_PRC_TS_KCRE
				+ XZC007Q_FRM_SEQ_NBR_KCRE + XZC007Q_REC_SEQ_NBR_KCRE;
		public static final int XZC007Q_ACT_NOT_FRM_REC_DATES = XZC007Q_TRANS_PROCESS_DT;
		public static final int XZC007Q_ACT_NOT_FRM_REC_KEY = XZC007Q_CSR_ACT_NBR + XZC007Q_NOT_PRC_TS + XZC007Q_FRM_SEQ_NBR_SIGN
				+ XZC007Q_FRM_SEQ_NBR + XZC007Q_REC_SEQ_NBR_SIGN + XZC007Q_REC_SEQ_NBR;
		public static final int XZC007Q_ACT_NOT_FRM_REC_KEY_CI = XZC007Q_CSR_ACT_NBR_CI + XZC007Q_NOT_PRC_TS_CI + XZC007Q_FRM_SEQ_NBR_CI
				+ XZC007Q_REC_SEQ_NBR_CI;
		public static final int FLR1 = 1;
		public static final int XZC007Q_ACT_NOT_FRM_REC_DATA = FLR1;
		public static final int XZC007Q_ACT_NOT_FRM_REC_ROW = XZC007Q_ACT_NOT_FRM_REC_FIXED + XZC007Q_ACT_NOT_FRM_REC_DATES
				+ XZC007Q_ACT_NOT_FRM_REC_KEY + XZC007Q_ACT_NOT_FRM_REC_KEY_CI + XZC007Q_ACT_NOT_FRM_REC_DATA;
		public static final int L_FW_REQ_XZ0C0007 = XZC007Q_ACT_NOT_FRM_REC_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0C0007;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
