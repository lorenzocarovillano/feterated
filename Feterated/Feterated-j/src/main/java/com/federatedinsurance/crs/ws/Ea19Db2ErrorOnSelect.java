/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-19-DB2-ERROR-ON-SELECT<br>
 * Variable: EA-19-DB2-ERROR-ON-SELECT from program XZ400000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea19Db2ErrorOnSelect {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-19-DB2-ERROR-ON-SELECT
	private String flr1 = "PGM =";
	//Original name: EA-19-PROGRAM-NAME
	private String programName = DefaultValues.stringVal(Len.PROGRAM_NAME);
	//Original name: FILLER-EA-19-DB2-ERROR-ON-SELECT-1
	private String flr2 = " PARA =";
	//Original name: EA-19-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-19-DB2-ERROR-ON-SELECT-2
	private String flr3 = " DB2 SELECT";
	//Original name: FILLER-EA-19-DB2-ERROR-ON-SELECT-3
	private String flr4 = "ERROR OCCURRED:";
	//Original name: FILLER-EA-19-DB2-ERROR-ON-SELECT-4
	private String flr5 = " SQLCODE =";
	//Original name: EA-19-SQLCODE
	private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);

	//==== METHODS ====
	public String getEa19Db2ErrorOnSelectFormatted() {
		return MarshalByteExt.bufferToStr(getEa19Db2ErrorOnSelectBytes());
	}

	public byte[] getEa19Db2ErrorOnSelectBytes() {
		byte[] buffer = new byte[Len.EA19_DB2_ERROR_ON_SELECT];
		return getEa19Db2ErrorOnSelectBytes(buffer, 1);
	}

	public byte[] getEa19Db2ErrorOnSelectBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, programName, Len.PROGRAM_NAME);
		position += Len.PROGRAM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setProgramName(String programName) {
		this.programName = Functions.subString(programName, Len.PROGRAM_NAME);
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setSqlcode(long sqlcode) {
		this.sqlcode = PicFormatter.display("++(4)9").format(sqlcode).toString();
	}

	public long getSqlcode() {
		return PicParser.display("++(4)9").parseLong(this.sqlcode);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int PARAGRAPH_NBR = 5;
		public static final int SQLCODE = 6;
		public static final int FLR1 = 6;
		public static final int FLR2 = 8;
		public static final int FLR3 = 12;
		public static final int FLR4 = 15;
		public static final int FLR5 = 11;
		public static final int EA19_DB2_ERROR_ON_SELECT = PROGRAM_NAME + PARAGRAPH_NBR + SQLCODE + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
