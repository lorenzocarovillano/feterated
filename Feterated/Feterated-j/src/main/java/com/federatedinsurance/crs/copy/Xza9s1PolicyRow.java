/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZA9S1-POLICY-ROW<br>
 * Variable: XZA9S1-POLICY-ROW from copybook XZ0A90S1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xza9s1PolicyRow {

	//==== PROPERTIES ====
	//Original name: XZA9S1-POLICY-ID
	private String policyId = DefaultValues.stringVal(Len.POLICY_ID);
	//Original name: XZA9S1-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZA9S1-OGN-EFF-DT
	private String ognEffDt = DefaultValues.stringVal(Len.OGN_EFF_DT);
	//Original name: XZA9S1-PLN-EXP-DT
	private String plnExpDt = DefaultValues.stringVal(Len.PLN_EXP_DT);
	//Original name: XZA9S1-QUOTE-SEQUENCE-NBR
	private int quoteSequenceNbr = DefaultValues.INT_VAL;
	//Original name: XZA9S1-ACT-NBR
	private String actNbr = DefaultValues.stringVal(Len.ACT_NBR);
	//Original name: XZA9S1-ACT-NBR-FMT
	private String actNbrFmt = DefaultValues.stringVal(Len.ACT_NBR_FMT);
	//Original name: XZA9S1-PRI-RSK-ST-ABB
	private String priRskStAbb = DefaultValues.stringVal(Len.PRI_RSK_ST_ABB);
	//Original name: XZA9S1-PRI-RSK-ST-DES
	private String priRskStDes = DefaultValues.stringVal(Len.PRI_RSK_ST_DES);
	//Original name: XZA9S1-LOB-CD
	private String lobCd = DefaultValues.stringVal(Len.LOB_CD);
	//Original name: XZA9S1-LOB-DES
	private String lobDes = DefaultValues.stringVal(Len.LOB_DES);
	//Original name: XZA9S1-TOB-CD
	private String tobCd = DefaultValues.stringVal(Len.TOB_CD);
	//Original name: XZA9S1-TOB-DES
	private String tobDes = DefaultValues.stringVal(Len.TOB_DES);
	//Original name: XZA9S1-SEG-CD
	private String segCd = DefaultValues.stringVal(Len.SEG_CD);
	//Original name: XZA9S1-SEG-DES
	private String segDes = DefaultValues.stringVal(Len.SEG_DES);
	//Original name: XZA9S1-LGE-ACT-IND
	private char lgeActInd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-PND-TRS-IND
	private char curPndTrsInd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-STA-CD
	private char curStaCd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-STA-DES
	private String curStaDes = DefaultValues.stringVal(Len.CUR_STA_DES);
	//Original name: XZA9S1-CUR-TRS-TYP-CD
	private char curTrsTypCd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-TRS-TYP-DES
	private String curTrsTypDes = DefaultValues.stringVal(Len.CUR_TRS_TYP_DES);
	//Original name: XZA9S1-COVERAGE-PARTS
	private String coverageParts = DefaultValues.stringVal(Len.COVERAGE_PARTS);
	//Original name: XZA9S1-RLT-TYP-CD
	private String rltTypCd = DefaultValues.stringVal(Len.RLT_TYP_CD);
	//Original name: XZA9S1-MNL-POL-IND
	private char mnlPolInd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-OGN-CD
	private char curOgnCd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-CUR-OGN-DES
	private String curOgnDes = DefaultValues.stringVal(Len.CUR_OGN_DES);
	//Original name: XZA9S1-BOND-IND
	private char bondInd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-BRANCH-NBR
	private String branchNbr = DefaultValues.stringVal(Len.BRANCH_NBR);
	//Original name: XZA9S1-BRANCH-DESC
	private String branchDesc = DefaultValues.stringVal(Len.BRANCH_DESC);
	//Original name: XZA9S1-WRT-PREM-AMT
	private long wrtPremAmt = DefaultValues.LONG_VAL;
	//Original name: XZA9S1-POL-ACT-IND
	private char polActInd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-PND-CNC-IND
	private char pndCncInd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-SCH-CNC-DT
	private String schCncDt = DefaultValues.stringVal(Len.SCH_CNC_DT);
	//Original name: XZA9S1-NOT-TYP-CD
	private String notTypCd = DefaultValues.stringVal(Len.NOT_TYP_CD);
	//Original name: XZA9S1-NOT-TYP-DES
	private String notTypDes = DefaultValues.stringVal(Len.NOT_TYP_DES);
	//Original name: XZA9S1-NOT-EMP-ID
	private String notEmpId = DefaultValues.stringVal(Len.NOT_EMP_ID);
	//Original name: XZA9S1-NOT-EMP-NM
	private String notEmpNm = DefaultValues.stringVal(Len.NOT_EMP_NM);
	//Original name: XZA9S1-NOT-PRC-DT
	private String notPrcDt = DefaultValues.stringVal(Len.NOT_PRC_DT);
	//Original name: XZA9S1-TMN-IND
	private char tmnInd = DefaultValues.CHAR_VAL;
	//Original name: XZA9S1-TMN-EMP-ID
	private String tmnEmpId = DefaultValues.stringVal(Len.TMN_EMP_ID);
	//Original name: XZA9S1-TMN-EMP-NM
	private String tmnEmpNm = DefaultValues.stringVal(Len.TMN_EMP_NM);
	//Original name: XZA9S1-TMN-PRC-DT
	private String tmnPrcDt = DefaultValues.stringVal(Len.TMN_PRC_DT);

	//==== METHODS ====
	public String getRowFormatted() {
		return MarshalByteExt.bufferToStr(getRowBytes());
	}

	public byte[] getRowBytes() {
		byte[] buffer = new byte[Len.ROW];
		return getRowBytes(buffer, 1);
	}

	public void setRowBytes(byte[] buffer, int offset) {
		int position = offset;
		policyId = MarshalByte.readString(buffer, position, Len.POLICY_ID);
		position += Len.POLICY_ID;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		ognEffDt = MarshalByte.readString(buffer, position, Len.OGN_EFF_DT);
		position += Len.OGN_EFF_DT;
		plnExpDt = MarshalByte.readString(buffer, position, Len.PLN_EXP_DT);
		position += Len.PLN_EXP_DT;
		quoteSequenceNbr = MarshalByte.readInt(buffer, position, Len.QUOTE_SEQUENCE_NBR);
		position += Len.QUOTE_SEQUENCE_NBR;
		actNbr = MarshalByte.readString(buffer, position, Len.ACT_NBR);
		position += Len.ACT_NBR;
		actNbrFmt = MarshalByte.readString(buffer, position, Len.ACT_NBR_FMT);
		position += Len.ACT_NBR_FMT;
		priRskStAbb = MarshalByte.readString(buffer, position, Len.PRI_RSK_ST_ABB);
		position += Len.PRI_RSK_ST_ABB;
		priRskStDes = MarshalByte.readString(buffer, position, Len.PRI_RSK_ST_DES);
		position += Len.PRI_RSK_ST_DES;
		lobCd = MarshalByte.readString(buffer, position, Len.LOB_CD);
		position += Len.LOB_CD;
		lobDes = MarshalByte.readString(buffer, position, Len.LOB_DES);
		position += Len.LOB_DES;
		tobCd = MarshalByte.readString(buffer, position, Len.TOB_CD);
		position += Len.TOB_CD;
		tobDes = MarshalByte.readString(buffer, position, Len.TOB_DES);
		position += Len.TOB_DES;
		segCd = MarshalByte.readString(buffer, position, Len.SEG_CD);
		position += Len.SEG_CD;
		segDes = MarshalByte.readString(buffer, position, Len.SEG_DES);
		position += Len.SEG_DES;
		lgeActInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		curPndTrsInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		curStaCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		curStaDes = MarshalByte.readString(buffer, position, Len.CUR_STA_DES);
		position += Len.CUR_STA_DES;
		curTrsTypCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		curTrsTypDes = MarshalByte.readString(buffer, position, Len.CUR_TRS_TYP_DES);
		position += Len.CUR_TRS_TYP_DES;
		coverageParts = MarshalByte.readString(buffer, position, Len.COVERAGE_PARTS);
		position += Len.COVERAGE_PARTS;
		rltTypCd = MarshalByte.readString(buffer, position, Len.RLT_TYP_CD);
		position += Len.RLT_TYP_CD;
		mnlPolInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		curOgnCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		curOgnDes = MarshalByte.readString(buffer, position, Len.CUR_OGN_DES);
		position += Len.CUR_OGN_DES;
		bondInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		branchNbr = MarshalByte.readString(buffer, position, Len.BRANCH_NBR);
		position += Len.BRANCH_NBR;
		branchDesc = MarshalByte.readString(buffer, position, Len.BRANCH_DESC);
		position += Len.BRANCH_DESC;
		wrtPremAmt = MarshalByte.readLong(buffer, position, Len.WRT_PREM_AMT);
		position += Len.WRT_PREM_AMT;
		polActInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pndCncInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		schCncDt = MarshalByte.readString(buffer, position, Len.SCH_CNC_DT);
		position += Len.SCH_CNC_DT;
		notTypCd = MarshalByte.readString(buffer, position, Len.NOT_TYP_CD);
		position += Len.NOT_TYP_CD;
		notTypDes = MarshalByte.readString(buffer, position, Len.NOT_TYP_DES);
		position += Len.NOT_TYP_DES;
		notEmpId = MarshalByte.readString(buffer, position, Len.NOT_EMP_ID);
		position += Len.NOT_EMP_ID;
		notEmpNm = MarshalByte.readString(buffer, position, Len.NOT_EMP_NM);
		position += Len.NOT_EMP_NM;
		notPrcDt = MarshalByte.readString(buffer, position, Len.NOT_PRC_DT);
		position += Len.NOT_PRC_DT;
		tmnInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tmnEmpId = MarshalByte.readString(buffer, position, Len.TMN_EMP_ID);
		position += Len.TMN_EMP_ID;
		tmnEmpNm = MarshalByte.readString(buffer, position, Len.TMN_EMP_NM);
		position += Len.TMN_EMP_NM;
		tmnPrcDt = MarshalByte.readString(buffer, position, Len.TMN_PRC_DT);
	}

	public byte[] getRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, policyId, Len.POLICY_ID);
		position += Len.POLICY_ID;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, ognEffDt, Len.OGN_EFF_DT);
		position += Len.OGN_EFF_DT;
		MarshalByte.writeString(buffer, position, plnExpDt, Len.PLN_EXP_DT);
		position += Len.PLN_EXP_DT;
		MarshalByte.writeInt(buffer, position, quoteSequenceNbr, Len.QUOTE_SEQUENCE_NBR);
		position += Len.QUOTE_SEQUENCE_NBR;
		MarshalByte.writeString(buffer, position, actNbr, Len.ACT_NBR);
		position += Len.ACT_NBR;
		MarshalByte.writeString(buffer, position, actNbrFmt, Len.ACT_NBR_FMT);
		position += Len.ACT_NBR_FMT;
		MarshalByte.writeString(buffer, position, priRskStAbb, Len.PRI_RSK_ST_ABB);
		position += Len.PRI_RSK_ST_ABB;
		MarshalByte.writeString(buffer, position, priRskStDes, Len.PRI_RSK_ST_DES);
		position += Len.PRI_RSK_ST_DES;
		MarshalByte.writeString(buffer, position, lobCd, Len.LOB_CD);
		position += Len.LOB_CD;
		MarshalByte.writeString(buffer, position, lobDes, Len.LOB_DES);
		position += Len.LOB_DES;
		MarshalByte.writeString(buffer, position, tobCd, Len.TOB_CD);
		position += Len.TOB_CD;
		MarshalByte.writeString(buffer, position, tobDes, Len.TOB_DES);
		position += Len.TOB_DES;
		MarshalByte.writeString(buffer, position, segCd, Len.SEG_CD);
		position += Len.SEG_CD;
		MarshalByte.writeString(buffer, position, segDes, Len.SEG_DES);
		position += Len.SEG_DES;
		MarshalByte.writeChar(buffer, position, lgeActInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, curPndTrsInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, curStaCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, curStaDes, Len.CUR_STA_DES);
		position += Len.CUR_STA_DES;
		MarshalByte.writeChar(buffer, position, curTrsTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, curTrsTypDes, Len.CUR_TRS_TYP_DES);
		position += Len.CUR_TRS_TYP_DES;
		MarshalByte.writeString(buffer, position, coverageParts, Len.COVERAGE_PARTS);
		position += Len.COVERAGE_PARTS;
		MarshalByte.writeString(buffer, position, rltTypCd, Len.RLT_TYP_CD);
		position += Len.RLT_TYP_CD;
		MarshalByte.writeChar(buffer, position, mnlPolInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, curOgnCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, curOgnDes, Len.CUR_OGN_DES);
		position += Len.CUR_OGN_DES;
		MarshalByte.writeChar(buffer, position, bondInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, branchNbr, Len.BRANCH_NBR);
		position += Len.BRANCH_NBR;
		MarshalByte.writeString(buffer, position, branchDesc, Len.BRANCH_DESC);
		position += Len.BRANCH_DESC;
		MarshalByte.writeLong(buffer, position, wrtPremAmt, Len.WRT_PREM_AMT);
		position += Len.WRT_PREM_AMT;
		MarshalByte.writeChar(buffer, position, polActInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pndCncInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, schCncDt, Len.SCH_CNC_DT);
		position += Len.SCH_CNC_DT;
		MarshalByte.writeString(buffer, position, notTypCd, Len.NOT_TYP_CD);
		position += Len.NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, notTypDes, Len.NOT_TYP_DES);
		position += Len.NOT_TYP_DES;
		MarshalByte.writeString(buffer, position, notEmpId, Len.NOT_EMP_ID);
		position += Len.NOT_EMP_ID;
		MarshalByte.writeString(buffer, position, notEmpNm, Len.NOT_EMP_NM);
		position += Len.NOT_EMP_NM;
		MarshalByte.writeString(buffer, position, notPrcDt, Len.NOT_PRC_DT);
		position += Len.NOT_PRC_DT;
		MarshalByte.writeChar(buffer, position, tmnInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tmnEmpId, Len.TMN_EMP_ID);
		position += Len.TMN_EMP_ID;
		MarshalByte.writeString(buffer, position, tmnEmpNm, Len.TMN_EMP_NM);
		position += Len.TMN_EMP_NM;
		MarshalByte.writeString(buffer, position, tmnPrcDt, Len.TMN_PRC_DT);
		return buffer;
	}

	public void setPolicyId(String policyId) {
		this.policyId = Functions.subString(policyId, Len.POLICY_ID);
	}

	public String getPolicyId() {
		return this.policyId;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	public void setOgnEffDt(String ognEffDt) {
		this.ognEffDt = Functions.subString(ognEffDt, Len.OGN_EFF_DT);
	}

	public String getOgnEffDt() {
		return this.ognEffDt;
	}

	public String getOgnEffDtFormatted() {
		return Functions.padBlanks(getOgnEffDt(), Len.OGN_EFF_DT);
	}

	public void setPlnExpDt(String plnExpDt) {
		this.plnExpDt = Functions.subString(plnExpDt, Len.PLN_EXP_DT);
	}

	public String getPlnExpDt() {
		return this.plnExpDt;
	}

	public String getPlnExpDtFormatted() {
		return Functions.padBlanks(getPlnExpDt(), Len.PLN_EXP_DT);
	}

	public void setQuoteSequenceNbr(int quoteSequenceNbr) {
		this.quoteSequenceNbr = quoteSequenceNbr;
	}

	public int getQuoteSequenceNbr() {
		return this.quoteSequenceNbr;
	}

	public void setActNbr(String actNbr) {
		this.actNbr = Functions.subString(actNbr, Len.ACT_NBR);
	}

	public String getActNbr() {
		return this.actNbr;
	}

	public void setActNbrFmt(String actNbrFmt) {
		this.actNbrFmt = Functions.subString(actNbrFmt, Len.ACT_NBR_FMT);
	}

	public String getActNbrFmt() {
		return this.actNbrFmt;
	}

	public void setPriRskStAbb(String priRskStAbb) {
		this.priRskStAbb = Functions.subString(priRskStAbb, Len.PRI_RSK_ST_ABB);
	}

	public String getPriRskStAbb() {
		return this.priRskStAbb;
	}

	public void setPriRskStDes(String priRskStDes) {
		this.priRskStDes = Functions.subString(priRskStDes, Len.PRI_RSK_ST_DES);
	}

	public String getPriRskStDes() {
		return this.priRskStDes;
	}

	public void setLobCd(String lobCd) {
		this.lobCd = Functions.subString(lobCd, Len.LOB_CD);
	}

	public String getLobCd() {
		return this.lobCd;
	}

	public void setLobDes(String lobDes) {
		this.lobDes = Functions.subString(lobDes, Len.LOB_DES);
	}

	public String getLobDes() {
		return this.lobDes;
	}

	public void setTobCd(String tobCd) {
		this.tobCd = Functions.subString(tobCd, Len.TOB_CD);
	}

	public String getTobCd() {
		return this.tobCd;
	}

	public void setTobDes(String tobDes) {
		this.tobDes = Functions.subString(tobDes, Len.TOB_DES);
	}

	public String getTobDes() {
		return this.tobDes;
	}

	public void setSegCd(String segCd) {
		this.segCd = Functions.subString(segCd, Len.SEG_CD);
	}

	public String getSegCd() {
		return this.segCd;
	}

	public void setSegDes(String segDes) {
		this.segDes = Functions.subString(segDes, Len.SEG_DES);
	}

	public String getSegDes() {
		return this.segDes;
	}

	public void setLgeActInd(char lgeActInd) {
		this.lgeActInd = lgeActInd;
	}

	public char getLgeActInd() {
		return this.lgeActInd;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setCurPndTrsInd(char curPndTrsInd) {
		this.curPndTrsInd = curPndTrsInd;
	}

	public char getCurPndTrsInd() {
		return this.curPndTrsInd;
	}

	public void setCurStaCd(char curStaCd) {
		this.curStaCd = curStaCd;
	}

	public char getCurStaCd() {
		return this.curStaCd;
	}

	public void setCurStaDes(String curStaDes) {
		this.curStaDes = Functions.subString(curStaDes, Len.CUR_STA_DES);
	}

	public String getCurStaDes() {
		return this.curStaDes;
	}

	public void setCurTrsTypCd(char curTrsTypCd) {
		this.curTrsTypCd = curTrsTypCd;
	}

	public char getCurTrsTypCd() {
		return this.curTrsTypCd;
	}

	public void setCurTrsTypDes(String curTrsTypDes) {
		this.curTrsTypDes = Functions.subString(curTrsTypDes, Len.CUR_TRS_TYP_DES);
	}

	public String getCurTrsTypDes() {
		return this.curTrsTypDes;
	}

	public void setCoverageParts(String coverageParts) {
		this.coverageParts = Functions.subString(coverageParts, Len.COVERAGE_PARTS);
	}

	public String getCoverageParts() {
		return this.coverageParts;
	}

	public void setRltTypCd(String rltTypCd) {
		this.rltTypCd = Functions.subString(rltTypCd, Len.RLT_TYP_CD);
	}

	public String getRltTypCd() {
		return this.rltTypCd;
	}

	public void setMnlPolInd(char mnlPolInd) {
		this.mnlPolInd = mnlPolInd;
	}

	public char getMnlPolInd() {
		return this.mnlPolInd;
	}

	public void setCurOgnCd(char curOgnCd) {
		this.curOgnCd = curOgnCd;
	}

	public char getCurOgnCd() {
		return this.curOgnCd;
	}

	public void setCurOgnDes(String curOgnDes) {
		this.curOgnDes = Functions.subString(curOgnDes, Len.CUR_OGN_DES);
	}

	public String getCurOgnDes() {
		return this.curOgnDes;
	}

	public void setBondInd(char bondInd) {
		this.bondInd = bondInd;
	}

	public char getBondInd() {
		return this.bondInd;
	}

	public void setBranchNbr(String branchNbr) {
		this.branchNbr = Functions.subString(branchNbr, Len.BRANCH_NBR);
	}

	public String getBranchNbr() {
		return this.branchNbr;
	}

	public void setBranchDesc(String branchDesc) {
		this.branchDesc = Functions.subString(branchDesc, Len.BRANCH_DESC);
	}

	public String getBranchDesc() {
		return this.branchDesc;
	}

	public void setWrtPremAmt(long wrtPremAmt) {
		this.wrtPremAmt = wrtPremAmt;
	}

	public long getWrtPremAmt() {
		return this.wrtPremAmt;
	}

	public void setPolActInd(char polActInd) {
		this.polActInd = polActInd;
	}

	public char getPolActInd() {
		return this.polActInd;
	}

	public void setPndCncInd(char pndCncInd) {
		this.pndCncInd = pndCncInd;
	}

	public char getPndCncInd() {
		return this.pndCncInd;
	}

	public void setSchCncDt(String schCncDt) {
		this.schCncDt = Functions.subString(schCncDt, Len.SCH_CNC_DT);
	}

	public String getSchCncDt() {
		return this.schCncDt;
	}

	public void setNotTypCd(String notTypCd) {
		this.notTypCd = Functions.subString(notTypCd, Len.NOT_TYP_CD);
	}

	public String getNotTypCd() {
		return this.notTypCd;
	}

	public void setNotTypDes(String notTypDes) {
		this.notTypDes = Functions.subString(notTypDes, Len.NOT_TYP_DES);
	}

	public String getNotTypDes() {
		return this.notTypDes;
	}

	public void setNotEmpId(String notEmpId) {
		this.notEmpId = Functions.subString(notEmpId, Len.NOT_EMP_ID);
	}

	public String getNotEmpId() {
		return this.notEmpId;
	}

	public void setNotEmpNm(String notEmpNm) {
		this.notEmpNm = Functions.subString(notEmpNm, Len.NOT_EMP_NM);
	}

	public String getNotEmpNm() {
		return this.notEmpNm;
	}

	public void setNotPrcDt(String notPrcDt) {
		this.notPrcDt = Functions.subString(notPrcDt, Len.NOT_PRC_DT);
	}

	public String getNotPrcDt() {
		return this.notPrcDt;
	}

	public void setTmnInd(char tmnInd) {
		this.tmnInd = tmnInd;
	}

	public char getTmnInd() {
		return this.tmnInd;
	}

	public void setTmnEmpId(String tmnEmpId) {
		this.tmnEmpId = Functions.subString(tmnEmpId, Len.TMN_EMP_ID);
	}

	public String getTmnEmpId() {
		return this.tmnEmpId;
	}

	public void setTmnEmpNm(String tmnEmpNm) {
		this.tmnEmpNm = Functions.subString(tmnEmpNm, Len.TMN_EMP_NM);
	}

	public String getTmnEmpNm() {
		return this.tmnEmpNm;
	}

	public void setTmnPrcDt(String tmnPrcDt) {
		this.tmnPrcDt = Functions.subString(tmnPrcDt, Len.TMN_PRC_DT);
	}

	public String getTmnPrcDt() {
		return this.tmnPrcDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POLICY_ID = 16;
		public static final int POL_NBR = 25;
		public static final int OGN_EFF_DT = 10;
		public static final int PLN_EXP_DT = 10;
		public static final int QUOTE_SEQUENCE_NBR = 5;
		public static final int ACT_NBR = 9;
		public static final int ACT_NBR_FMT = 9;
		public static final int PRI_RSK_ST_ABB = 3;
		public static final int PRI_RSK_ST_DES = 25;
		public static final int LOB_CD = 3;
		public static final int LOB_DES = 45;
		public static final int TOB_CD = 3;
		public static final int TOB_DES = 40;
		public static final int SEG_CD = 3;
		public static final int SEG_DES = 40;
		public static final int LGE_ACT_IND = 1;
		public static final int STATUS_CD = 1;
		public static final int CUR_PND_TRS_IND = 1;
		public static final int CUR_STA_CD = 1;
		public static final int CUR_STA_DES = 40;
		public static final int CUR_TRS_TYP_CD = 1;
		public static final int CUR_TRS_TYP_DES = 35;
		public static final int COVERAGE_PARTS = 40;
		public static final int RLT_TYP_CD = 4;
		public static final int MNL_POL_IND = 1;
		public static final int CUR_OGN_CD = 1;
		public static final int CUR_OGN_DES = 20;
		public static final int BOND_IND = 1;
		public static final int BRANCH_NBR = 2;
		public static final int BRANCH_DESC = 45;
		public static final int WRT_PREM_AMT = 11;
		public static final int POL_ACT_IND = 1;
		public static final int PND_CNC_IND = 1;
		public static final int SCH_CNC_DT = 10;
		public static final int NOT_TYP_CD = 5;
		public static final int NOT_TYP_DES = 35;
		public static final int NOT_EMP_ID = 6;
		public static final int NOT_EMP_NM = 67;
		public static final int NOT_PRC_DT = 10;
		public static final int TMN_IND = 1;
		public static final int TMN_EMP_ID = 6;
		public static final int TMN_EMP_NM = 67;
		public static final int TMN_PRC_DT = 10;
		public static final int ROW = POLICY_ID + POL_NBR + OGN_EFF_DT + PLN_EXP_DT + QUOTE_SEQUENCE_NBR + ACT_NBR + ACT_NBR_FMT + PRI_RSK_ST_ABB
				+ PRI_RSK_ST_DES + LOB_CD + LOB_DES + TOB_CD + TOB_DES + SEG_CD + SEG_DES + LGE_ACT_IND + STATUS_CD + CUR_PND_TRS_IND + CUR_STA_CD
				+ CUR_STA_DES + CUR_TRS_TYP_CD + CUR_TRS_TYP_DES + COVERAGE_PARTS + RLT_TYP_CD + MNL_POL_IND + CUR_OGN_CD + CUR_OGN_DES + BOND_IND
				+ BRANCH_NBR + BRANCH_DESC + WRT_PREM_AMT + POL_ACT_IND + PND_CNC_IND + SCH_CNC_DT + NOT_TYP_CD + NOT_TYP_DES + NOT_EMP_ID
				+ NOT_EMP_NM + NOT_PRC_DT + TMN_IND + TMN_EMP_ID + TMN_EMP_NM + TMN_PRC_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
