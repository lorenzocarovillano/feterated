/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-TMP-BUFF<br>
 * Variables: WS-TMP-BUFF from program CIWOSDX<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsTmpBuff {

	//==== PROPERTIES ====
	//Original name: WS-BUF-CHAR
	private char wsBufChar = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setWsTmpBuffBytes(byte[] buffer, int offset) {
		int position = offset;
		wsBufChar = MarshalByte.readChar(buffer, position);
	}

	public void initWsTmpBuffSpaces() {
		wsBufChar = Types.SPACE_CHAR;
	}

	public void setWsBufChar(char wsBufChar) {
		this.wsBufChar = wsBufChar;
	}

	public char getWsBufChar() {
		return this.wsBufChar;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_BUF_CHAR = 1;
		public static final int WS_TMP_BUFF = WS_BUF_CHAR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
