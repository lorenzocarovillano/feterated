/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: UIDG-ERRID-ERROR-CODE<br>
 * Variable: UIDG-ERRID-ERROR-CODE from copybook HALLUIDG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UidgErridErrorCode {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SUCCESSFUL = Types.SPACE_CHAR;
	public static final char SQLERR = 'S';
	public static final char DUPERR = 'D';

	//==== METHODS ====
	public void setErridErrorCode(char erridErrorCode) {
		this.value = erridErrorCode;
	}

	public void setUidgErridErrorCodeFormatted(String uidgErridErrorCode) {
		setErridErrorCode(Functions.charAt(uidgErridErrorCode, Types.CHAR_SIZE));
	}

	public char getErridErrorCode() {
		return this.value;
	}

	public void setUidgErridSuccessful() {
		setUidgErridErrorCodeFormatted(String.valueOf(SUCCESSFUL));
	}

	public void setUidgErridSqlerr() {
		value = SQLERR;
	}

	public void setUidgErridDuperr() {
		value = DUPERR;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERRID_ERROR_CODE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
