/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: HALLCCTX<br>
 * Copybook: HALLCCTX from copybook HALLCCTX<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Hallcctx {

	//==== PROPERTIES ====
	//Original name: HCTXC-CONTEXT
	private String t = DefaultValues.stringVal(Len.T);
	//Original name: FILLER-HCTXC-CONTEXT-INFO
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setInfoFormatted(String data) {
		byte[] buffer = new byte[Len.INFO];
		MarshalByte.writeString(buffer, 1, data, Len.INFO);
		setInfoBytes(buffer, 1);
	}

	public void setInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		t = MarshalByte.readString(buffer, position, Len.T);
		position += Len.T;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public void setT(String t) {
		this.t = Functions.subString(t, Len.T);
	}

	public String getT() {
		return this.t;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int T = 20;
		public static final int FLR1 = 32;
		public static final int INFO = T + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
