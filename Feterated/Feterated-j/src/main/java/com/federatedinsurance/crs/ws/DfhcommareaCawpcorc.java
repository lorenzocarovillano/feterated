/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program CAWPCORC<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaCawpcorc extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int FLR1_MAXOCCURS = 32767;
	private int fillerDfhcommareaListenerSize = FLR1_MAXOCCURS;
	private IValueChangeListener fillerDfhcommareaListener = new FillerDfhcommareaListener();
	//Original name: FILLER-DFHCOMMAREA
	private char[] flr1 = new char[FLR1_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public DfhcommareaCawpcorc() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return getDfhcommareaSize();
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void init() {
		for (int flr1Idx = 1; flr1Idx <= FLR1_MAXOCCURS; flr1Idx++) {
			setFlr1(flr1Idx, DefaultValues.CHAR_VAL);
		}
	}

	public int getDfhcommareaSize() {
		return Types.CHAR_SIZE * fillerDfhcommareaListenerSize;
	}

	public String getDfhcommareaFormatted() {
		return MarshalByteExt.bufferToStr(getDfhcommareaBytes());
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[getDfhcommareaSize()];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= fillerDfhcommareaListenerSize; idx++) {
			if (position <= buffer.length) {
				setFlr1(idx, MarshalByte.readChar(buffer, position));
				position += Types.CHAR_SIZE;
			} else {
				setFlr1(idx, Types.SPACE_CHAR);
				position += Types.CHAR_SIZE;
			}
		}
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= fillerDfhcommareaListenerSize; idx++) {
			MarshalByte.writeChar(buffer, position, getFlr1(idx));
			position += Types.CHAR_SIZE;
		}
		return buffer;
	}

	public void setFlr1(int flr1Idx, char flr1) {
		this.flr1[flr1Idx - 1] = flr1;
	}

	public char getFlr1(int flr1Idx) {
		return this.flr1[flr1Idx - 1];
	}

	public IValueChangeListener getFillerDfhcommareaListener() {
		return fillerDfhcommareaListener;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	/**Original name: FILLER-DFHCOMMAREA<br>*/
	public class FillerDfhcommareaListener implements IValueChangeListener {

		//==== METHODS ====
		@Override
		public void change() {
			fillerDfhcommareaListenerSize = FLR1_MAXOCCURS;
		}

		@Override
		public void change(int value) {
			fillerDfhcommareaListenerSize = value < 1 ? 0 : (value > FLR1_MAXOCCURS ? FLR1_MAXOCCURS : value);
		}
	}
}
