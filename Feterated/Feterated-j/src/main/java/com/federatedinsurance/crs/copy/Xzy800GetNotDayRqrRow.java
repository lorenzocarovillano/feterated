/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xzy800NlbeInd;
import com.federatedinsurance.crs.ws.enums.Xzy800WarningInd;
import com.federatedinsurance.crs.ws.occurs.Xzy800Warnings;

/**Original name: XZY800-GET-NOT-DAY-RQR-ROW<br>
 * Variable: XZY800-GET-NOT-DAY-RQR-ROW from copybook XZ0Y8000<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzy800GetNotDayRqrRow {

	//==== PROPERTIES ====
	public static final int WARNINGS_MAXOCCURS = 10;
	//Original name: XZY800-POL-PRI-RSK-ST-ABB
	private String polPriRskStAbb = DefaultValues.stringVal(Len.POL_PRI_RSK_ST_ABB);
	//Original name: XZY800-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZY800-POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: XZY800-MIN-POL-EFF-DT
	private String minPolEffDt = DefaultValues.stringVal(Len.MIN_POL_EFF_DT);
	//Original name: XZY800-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZY800-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZY800-NBR-OF-NOT-DAY
	private int nbrOfNotDay = DefaultValues.INT_VAL;
	//Original name: XZY800-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);
	//Original name: XZY800-NLBE-IND
	private Xzy800NlbeInd nlbeInd = new Xzy800NlbeInd();
	//Original name: XZY800-WARNING-IND
	private Xzy800WarningInd warningInd = new Xzy800WarningInd();
	//Original name: XZY800-NON-LOG-ERR-MSG
	private String nonLogErrMsg = DefaultValues.stringVal(Len.NON_LOG_ERR_MSG);
	//Original name: XZY800-WARNINGS
	private Xzy800Warnings[] warnings = new Xzy800Warnings[WARNINGS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Xzy800GetNotDayRqrRow() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int warningsIdx = 1; warningsIdx <= WARNINGS_MAXOCCURS; warningsIdx++) {
			warnings[warningsIdx - 1] = new Xzy800Warnings();
		}
	}

	public String getXzy800GetNotDayRqrRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzy800GetNotDayRqrRowBytes());
	}

	public byte[] getXzy800GetNotDayRqrRowBytes() {
		byte[] buffer = new byte[Len.XZY800_GET_NOT_DAY_RQR_ROW];
		return getXzy800GetNotDayRqrRowBytes(buffer, 1);
	}

	public void setXzy800GetNotDayRqrRowBytes(byte[] buffer, int offset) {
		int position = offset;
		polPriRskStAbb = MarshalByte.readString(buffer, position, Len.POL_PRI_RSK_ST_ABB);
		position += Len.POL_PRI_RSK_ST_ABB;
		actNotTypCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		polTypCd = MarshalByte.readString(buffer, position, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		minPolEffDt = MarshalByte.readString(buffer, position, Len.MIN_POL_EFF_DT);
		position += Len.MIN_POL_EFF_DT;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		nbrOfNotDay = MarshalByte.readInt(buffer, position, Len.NBR_OF_NOT_DAY);
		position += Len.NBR_OF_NOT_DAY;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
		position += Len.USERID;
		setErrorFlagsBytes(buffer, position);
		position += Len.ERROR_FLAGS;
		nonLogErrMsg = MarshalByte.readString(buffer, position, Len.NON_LOG_ERR_MSG);
		position += Len.NON_LOG_ERR_MSG;
		for (int idx = 1; idx <= WARNINGS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				warnings[idx - 1].setWarningsBytes(buffer, position);
				position += Xzy800Warnings.Len.WARNINGS;
			} else {
				warnings[idx - 1].initWarningsSpaces();
				position += Xzy800Warnings.Len.WARNINGS;
			}
		}
	}

	public byte[] getXzy800GetNotDayRqrRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
		position += Len.POL_PRI_RSK_ST_ABB;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, polTypCd, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		MarshalByte.writeString(buffer, position, minPolEffDt, Len.MIN_POL_EFF_DT);
		position += Len.MIN_POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeInt(buffer, position, nbrOfNotDay, Len.NBR_OF_NOT_DAY);
		position += Len.NBR_OF_NOT_DAY;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position += Len.USERID;
		getErrorFlagsBytes(buffer, position);
		position += Len.ERROR_FLAGS;
		MarshalByte.writeString(buffer, position, nonLogErrMsg, Len.NON_LOG_ERR_MSG);
		position += Len.NON_LOG_ERR_MSG;
		for (int idx = 1; idx <= WARNINGS_MAXOCCURS; idx++) {
			warnings[idx - 1].getWarningsBytes(buffer, position);
			position += Xzy800Warnings.Len.WARNINGS;
		}
		return buffer;
	}

	public void setPolPriRskStAbb(String polPriRskStAbb) {
		this.polPriRskStAbb = Functions.subString(polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
	}

	public String getPolPriRskStAbb() {
		return this.polPriRskStAbb;
	}

	public String getPolPriRskStAbbFormatted() {
		return Functions.padBlanks(getPolPriRskStAbb(), Len.POL_PRI_RSK_ST_ABB);
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getActNotTypCdFormatted() {
		return Functions.padBlanks(getActNotTypCd(), Len.ACT_NOT_TYP_CD);
	}

	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	public String getPolTypCd() {
		return this.polTypCd;
	}

	public String getPolTypCdFormatted() {
		return Functions.padBlanks(getPolTypCd(), Len.POL_TYP_CD);
	}

	public void setMinPolEffDt(String minPolEffDt) {
		this.minPolEffDt = Functions.subString(minPolEffDt, Len.MIN_POL_EFF_DT);
	}

	public String getMinPolEffDt() {
		return this.minPolEffDt;
	}

	public String getMinPolEffDtFormatted() {
		return Functions.padBlanks(getMinPolEffDt(), Len.MIN_POL_EFF_DT);
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public String getPolEffDtFormatted() {
		return Functions.padBlanks(getPolEffDt(), Len.POL_EFF_DT);
	}

	public void setNbrOfNotDay(int nbrOfNotDay) {
		this.nbrOfNotDay = nbrOfNotDay;
	}

	public int getNbrOfNotDay() {
		return this.nbrOfNotDay;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	public String getUseridFormatted() {
		return Functions.padBlanks(getUserid(), Len.USERID);
	}

	public void setErrorFlagsBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeInd.setNlbeInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		warningInd.setWarningInd(MarshalByte.readChar(buffer, position));
	}

	public byte[] getErrorFlagsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, nlbeInd.getNlbeInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, warningInd.getWarningInd());
		return buffer;
	}

	public void setNonLogErrMsg(String nonLogErrMsg) {
		this.nonLogErrMsg = Functions.subString(nonLogErrMsg, Len.NON_LOG_ERR_MSG);
	}

	public String getNonLogErrMsg() {
		return this.nonLogErrMsg;
	}

	public Xzy800NlbeInd getNlbeInd() {
		return nlbeInd;
	}

	public Xzy800WarningInd getWarningInd() {
		return warningInd;
	}

	public Xzy800Warnings getWarnings(int idx) {
		return warnings[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_PRI_RSK_ST_ABB = 2;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int POL_TYP_CD = 3;
		public static final int MIN_POL_EFF_DT = 10;
		public static final int POL_NBR = 25;
		public static final int POL_EFF_DT = 10;
		public static final int USERID = 8;
		public static final int NON_LOG_ERR_MSG = 500;
		public static final int NBR_OF_NOT_DAY = 5;
		public static final int ERROR_FLAGS = Xzy800NlbeInd.Len.NLBE_IND + Xzy800WarningInd.Len.WARNING_IND;
		public static final int XZY800_GET_NOT_DAY_RQR_ROW = POL_PRI_RSK_ST_ABB + ACT_NOT_TYP_CD + POL_TYP_CD + MIN_POL_EFF_DT + POL_NBR + POL_EFF_DT
				+ NBR_OF_NOT_DAY + USERID + ERROR_FLAGS + NON_LOG_ERR_MSG + Xzy800GetNotDayRqrRow.WARNINGS_MAXOCCURS * Xzy800Warnings.Len.WARNINGS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
