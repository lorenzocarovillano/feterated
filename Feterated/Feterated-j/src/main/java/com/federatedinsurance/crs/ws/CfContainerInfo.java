/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: CF-CONTAINER-INFO<br>
 * Variable: CF-CONTAINER-INFO from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfContainerInfo {

	//==== PROPERTIES ====
	//Original name: CF-CI-SVC-OUT-CONTAINER
	private String svcOutContainer = "XZPOLSVCOUT";
	//Original name: CF-CI-SVC-IN-CONTAINER
	private String svcInContainer = "XZPOLSVCIN";
	//Original name: CF-CI-INP-CONTAINER
	private String inpContainer = "XZPOLINFINP";
	//Original name: CF-CI-OUP-CONTAINER
	private String oupContainer = "XZPOLINFOUP";
	//Original name: CF-CI-IVORYH-CONTAINER
	private String ivoryhContainer = "IVORYH";

	//==== METHODS ====
	public String getSvcOutContainer() {
		return this.svcOutContainer;
	}

	public String getSvcOutContainerFormatted() {
		return Functions.padBlanks(getSvcOutContainer(), Len.SVC_OUT_CONTAINER);
	}

	public String getSvcInContainer() {
		return this.svcInContainer;
	}

	public String getSvcInContainerFormatted() {
		return Functions.padBlanks(getSvcInContainer(), Len.SVC_IN_CONTAINER);
	}

	public String getInpContainer() {
		return this.inpContainer;
	}

	public String getInpContainerFormatted() {
		return Functions.padBlanks(getInpContainer(), Len.INP_CONTAINER);
	}

	public String getOupContainer() {
		return this.oupContainer;
	}

	public String getOupContainerFormatted() {
		return Functions.padBlanks(getOupContainer(), Len.OUP_CONTAINER);
	}

	public String getIvoryhContainer() {
		return this.ivoryhContainer;
	}

	public String getIvoryhContainerFormatted() {
		return Functions.padBlanks(getIvoryhContainer(), Len.IVORYH_CONTAINER);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SVC_IN_CONTAINER = 16;
		public static final int IVORYH_CONTAINER = 16;
		public static final int INP_CONTAINER = 16;
		public static final int OUP_CONTAINER = 16;
		public static final int SVC_OUT_CONTAINER = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
