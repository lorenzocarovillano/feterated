/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZ003000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXz003000 {

	//==== PROPERTIES ====
	//Original name: EA-01-PROGRAM-MSG
	private Ea01ProgramMsgXz003000 ea01ProgramMsg = new Ea01ProgramMsgXz003000();
	//Original name: EA-02-PROGRAM-START
	private Ea02ProgramStart ea02ProgramStart = new Ea02ProgramStart();
	//Original name: EA-03-SUCCESSFUL-END
	private Ea03SuccessfulEndXz003000 ea03SuccessfulEnd = new Ea03SuccessfulEndXz003000();
	//Original name: EA-04-PARM-ERR-MSG
	private Ea04ParmErrMsg ea04ParmErrMsg = new Ea04ParmErrMsg();
	//Original name: EA-05-CICS-CALL-ERR
	private Ea05CicsCallErr ea05CicsCallErr = new Ea05CicsCallErr();
	//Original name: EA-06-CICS-PARM-MSG
	private Ea06CicsParmMsg ea06CicsParmMsg = new Ea06CicsParmMsg();
	//Original name: FILLER-EA-07-OPENING-PIPE-FAILED
	private String flr1 = "OPENING PIPE";
	//Original name: FILLER-EA-07-OPENING-PIPE-FAILED-1
	private String flr2 = " FAILED!";
	//Original name: FILLER-EA-08-OPENING-PIPE-WARNING
	private String flr3 = "OPENING PIPE";
	//Original name: FILLER-EA-08-OPENING-PIPE-WARNING-1
	private String flr4 = "WARNING!";
	//Original name: FILLER-EA-09-CALLING-PIPE-FAILED
	private String flr5 = "CALLING PIPE";
	//Original name: FILLER-EA-09-CALLING-PIPE-FAILED-1
	private String flr6 = " FAILED!";
	//Original name: FILLER-EA-10-CALLING-PIPE-WARNING
	private String flr7 = "CALLING PIPE";
	//Original name: FILLER-EA-10-CALLING-PIPE-WARNING-1
	private String flr8 = "WARNING!";
	//Original name: FILLER-EA-11-CLOSING-PIPE-FAILED
	private String flr9 = "CLOSING PIPE";
	//Original name: FILLER-EA-11-CLOSING-PIPE-FAILED-1
	private String flr10 = " FAILED!";
	//Original name: EA-13-OPEN-CURSOR-ERROR
	private Ea13OpenCursorError ea13OpenCursorError = new Ea13OpenCursorError();
	//Original name: EA-14-FETCH-CURSOR-ERROR
	private Ea14FetchCursorError ea14FetchCursorError = new Ea14FetchCursorError();
	//Original name: EA-15-CLOSE-CURSOR-ERROR
	private Ea15CloseCursorError ea15CloseCursorError = new Ea15CloseCursorError();
	//Original name: EA-16-GET-ACT-NOT-INPUT-PARMS
	private Ea16GetActNotInputParms ea16GetActNotInputParms = new Ea16GetActNotInputParms();
	//Original name: EA-17-DLT-ACT-NOT-INPUT-PARMS
	private Ea17DltActNotInputParms ea17DltActNotInputParms = new Ea17DltActNotInputParms();
	//Original name: EA-18-SERVICE-ERROR-FIELDS
	private Ea18ServiceErrorFields ea18ServiceErrorFields = new Ea18ServiceErrorFields();

	//==== METHODS ====
	public String getEa07OpeningPipeFailedFormatted() {
		return MarshalByteExt.bufferToStr(getEa07OpeningPipeFailedBytes());
	}

	/**Original name: EA-07-OPENING-PIPE-FAILED<br>*/
	public byte[] getEa07OpeningPipeFailedBytes() {
		byte[] buffer = new byte[Len.EA07_OPENING_PIPE_FAILED];
		return getEa07OpeningPipeFailedBytes(buffer, 1);
	}

	public byte[] getEa07OpeningPipeFailedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getEa08OpeningPipeWarningFormatted() {
		return MarshalByteExt.bufferToStr(getEa08OpeningPipeWarningBytes());
	}

	/**Original name: EA-08-OPENING-PIPE-WARNING<br>*/
	public byte[] getEa08OpeningPipeWarningBytes() {
		byte[] buffer = new byte[Len.EA08_OPENING_PIPE_WARNING];
		return getEa08OpeningPipeWarningBytes(buffer, 1);
	}

	public byte[] getEa08OpeningPipeWarningBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getEa09CallingPipeFailedFormatted() {
		return MarshalByteExt.bufferToStr(getEa09CallingPipeFailedBytes());
	}

	/**Original name: EA-09-CALLING-PIPE-FAILED<br>*/
	public byte[] getEa09CallingPipeFailedBytes() {
		byte[] buffer = new byte[Len.EA09_CALLING_PIPE_FAILED];
		return getEa09CallingPipeFailedBytes(buffer, 1);
	}

	public byte[] getEa09CallingPipeFailedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR4);
		return buffer;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getEa10CallingPipeWarningFormatted() {
		return MarshalByteExt.bufferToStr(getEa10CallingPipeWarningBytes());
	}

	/**Original name: EA-10-CALLING-PIPE-WARNING<br>*/
	public byte[] getEa10CallingPipeWarningBytes() {
		byte[] buffer = new byte[Len.EA10_CALLING_PIPE_WARNING];
		return getEa10CallingPipeWarningBytes(buffer, 1);
	}

	public byte[] getEa10CallingPipeWarningBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR4);
		return buffer;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public String getEa11ClosingPipeFailedFormatted() {
		return MarshalByteExt.bufferToStr(getEa11ClosingPipeFailedBytes());
	}

	/**Original name: EA-11-CLOSING-PIPE-FAILED<br>*/
	public byte[] getEa11ClosingPipeFailedBytes() {
		byte[] buffer = new byte[Len.EA11_CLOSING_PIPE_FAILED];
		return getEa11ClosingPipeFailedBytes(buffer, 1);
	}

	public byte[] getEa11ClosingPipeFailedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr10, Len.FLR4);
		return buffer;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public String getFlr10() {
		return this.flr10;
	}

	public Ea01ProgramMsgXz003000 getEa01ProgramMsg() {
		return ea01ProgramMsg;
	}

	public Ea02ProgramStart getEa02ProgramStart() {
		return ea02ProgramStart;
	}

	public Ea03SuccessfulEndXz003000 getEa03SuccessfulEnd() {
		return ea03SuccessfulEnd;
	}

	public Ea04ParmErrMsg getEa04ParmErrMsg() {
		return ea04ParmErrMsg;
	}

	public Ea05CicsCallErr getEa05CicsCallErr() {
		return ea05CicsCallErr;
	}

	public Ea06CicsParmMsg getEa06CicsParmMsg() {
		return ea06CicsParmMsg;
	}

	public Ea13OpenCursorError getEa13OpenCursorError() {
		return ea13OpenCursorError;
	}

	public Ea14FetchCursorError getEa14FetchCursorError() {
		return ea14FetchCursorError;
	}

	public Ea15CloseCursorError getEa15CloseCursorError() {
		return ea15CloseCursorError;
	}

	public Ea16GetActNotInputParms getEa16GetActNotInputParms() {
		return ea16GetActNotInputParms;
	}

	public Ea17DltActNotInputParms getEa17DltActNotInputParms() {
		return ea17DltActNotInputParms;
	}

	public Ea18ServiceErrorFields getEa18ServiceErrorFields() {
		return ea18ServiceErrorFields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR3 = 13;
		public static final int FLR4 = 8;
		public static final int EA08_OPENING_PIPE_WARNING = FLR3 + FLR4;
		public static final int EA07_OPENING_PIPE_FAILED = FLR3 + FLR4;
		public static final int EA10_CALLING_PIPE_WARNING = FLR3 + FLR4;
		public static final int EA09_CALLING_PIPE_FAILED = FLR3 + FLR4;
		public static final int EA11_CLOSING_PIPE_FAILED = FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
