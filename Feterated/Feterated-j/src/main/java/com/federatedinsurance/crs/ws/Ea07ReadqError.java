/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-07-READQ-ERROR<br>
 * Variable: EA-07-READQ-ERROR from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea07ReadqError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-07-READQ-ERROR
	private String flr1 = "CICS READQ";
	//Original name: FILLER-EA-07-READQ-ERROR-1
	private String flr2 = "ERROR.";
	//Original name: FILLER-EA-07-READQ-ERROR-2
	private String flr3 = "RESP CODE 1:";
	//Original name: EA-07-RESPONSE-CODE
	private String code = DefaultValues.stringVal(Len.CODE);
	//Original name: FILLER-EA-07-READQ-ERROR-3
	private String flr4 = " RESP CODE 2:";
	//Original name: EA-07-RESPONSE-CODE2
	private String code2 = DefaultValues.stringVal(Len.CODE2);

	//==== METHODS ====
	public String getEa07ReadqErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa07ReadqErrorBytes());
	}

	public byte[] getEa07ReadqErrorBytes() {
		byte[] buffer = new byte[Len.EA07_READQ_ERROR];
		return getEa07ReadqErrorBytes(buffer, 1);
	}

	public byte[] getEa07ReadqErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, code, Len.CODE);
		position += Len.CODE;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, code2, Len.CODE2);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setCode(long code) {
		this.code = PicFormatter.display("-Z(8)9").format(code).toString();
	}

	public long getCode() {
		return PicParser.display("-Z(8)9").parseLong(this.code);
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setCode2(long code2) {
		this.code2 = PicFormatter.display("-Z(8)9").format(code2).toString();
	}

	public long getCode2() {
		return PicParser.display("-Z(8)9").parseLong(this.code2);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODE = 10;
		public static final int CODE2 = 10;
		public static final int FLR1 = 11;
		public static final int FLR2 = 7;
		public static final int FLR3 = 13;
		public static final int FLR4 = 14;
		public static final int EA07_READQ_ERROR = CODE + CODE2 + FLR1 + FLR2 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
