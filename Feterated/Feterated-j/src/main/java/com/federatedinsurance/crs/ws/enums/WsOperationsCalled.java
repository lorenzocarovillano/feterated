/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-OPERATIONS-CALLED<br>
 * Variable: WS-OPERATIONS-CALLED from program XZ0P0022<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsOperationsCalled {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.WS_OPERATIONS_CALLED);
	public static final String SET_TMN_FLG = "setTerminationFlag";
	public static final String ADD_ACT_NOT = "AddAccountNotification";
	public static final String GET_INSURED_DETAIL = "GetInsuredDetail";
	public static final String GET_POL_STATUS_LIST = "GetAcyPndCncTmnPolListByAct";
	public static final String PREPARE_INS_POL = "PrepareInsuredPolicy";
	public static final String UPD_NOT_STA = "UpdateNotificationStatus";

	//==== METHODS ====
	public void setWsOperationsCalled(String wsOperationsCalled) {
		this.value = Functions.subString(wsOperationsCalled, Len.WS_OPERATIONS_CALLED);
	}

	public String getWsOperationsCalled() {
		return this.value;
	}

	public void setSetTmnFlg() {
		value = SET_TMN_FLG;
	}

	public void setAddActNot() {
		value = ADD_ACT_NOT;
	}

	public void setGetInsuredDetail() {
		value = GET_INSURED_DETAIL;
	}

	public void setGetPolStatusList() {
		value = GET_POL_STATUS_LIST;
	}

	public void setPrepareInsPol() {
		value = PREPARE_INS_POL;
	}

	public void setUpdNotSta() {
		value = UPD_NOT_STA;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_OPERATIONS_CALLED = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
