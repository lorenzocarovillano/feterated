/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: HALRLOMG-GRP-LOK-CLR-SW<br>
 * Variable: HALRLOMG-GRP-LOK-CLR-SW from copybook HALLLOMG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HalrlomgGrpLokClrSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char DEL_GRP_LEGACY_LOK = 'D';
	public static final char KEEP_GRP_LEGACY_LOK = 'K';

	//==== METHODS ====
	public void setGrpLokClrSw(char grpLokClrSw) {
		this.value = grpLokClrSw;
	}

	public char getGrpLokClrSw() {
		return this.value;
	}

	public void setDelGrpLegacyLok() {
		value = DEL_GRP_LEGACY_LOK;
	}

	public void setKeepGrpLegacyLok() {
		value = KEEP_GRP_LEGACY_LOK;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int GRP_LOK_CLR_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
