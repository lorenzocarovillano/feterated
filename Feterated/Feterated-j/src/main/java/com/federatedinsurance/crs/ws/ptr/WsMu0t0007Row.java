/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-MU0T0007-ROW<br>
 * Variable: WS-MU0T0007-ROW from program XZ0B90Q0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class WsMu0t0007Row extends BytesClass {

	//==== PROPERTIES ====
	public static final int MUT007_SERVICE_OUTPUTS_MAXOCCURS = 50;

	//==== CONSTRUCTORS ====
	public WsMu0t0007Row() {
	}

	public WsMu0t0007Row(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_MU0T0007_ROW;
	}

	public void setMut07iActNbr(String mut07iActNbr) {
		writeString(Pos.MUT07I_ACT_NBR, mut07iActNbr, Len.MUT07I_ACT_NBR);
	}

	/**Original name: MUT07I-ACT-NBR<br>*/
	public String getMut07iActNbr() {
		return readString(Pos.MUT07I_ACT_NBR, Len.MUT07I_ACT_NBR);
	}

	public void setMut07iUserid(String mut07iUserid) {
		writeString(Pos.MUT07I_USERID, mut07iUserid, Len.MUT07I_USERID);
	}

	/**Original name: MUT07I-USERID<br>*/
	public String getMut07iUserid() {
		return readString(Pos.MUT07I_USERID, Len.MUT07I_USERID);
	}

	public void setMut07oTkCltId(int mut07oTkCltIdIdx, String mut07oTkCltId) {
		int position = Pos.mut07oTkCltId(mut07oTkCltIdIdx - 1);
		writeString(position, mut07oTkCltId, Len.MUT07O_TK_CLT_ID);
	}

	/**Original name: MUT07O-TK-CLT-ID<br>*/
	public String getMut07oTkCltId(int mut07oTkCltIdIdx) {
		int position = Pos.mut07oTkCltId(mut07oTkCltIdIdx - 1);
		return readString(position, Len.MUT07O_TK_CLT_ID);
	}

	public void setMut07oTkAdrId(int mut07oTkAdrIdIdx, String mut07oTkAdrId) {
		int position = Pos.mut07oTkAdrId(mut07oTkAdrIdIdx - 1);
		writeString(position, mut07oTkAdrId, Len.MUT07O_TK_ADR_ID);
	}

	/**Original name: MUT07O-TK-ADR-ID<br>*/
	public String getMut07oTkAdrId(int mut07oTkAdrIdIdx) {
		int position = Pos.mut07oTkAdrId(mut07oTkAdrIdIdx - 1);
		return readString(position, Len.MUT07O_TK_ADR_ID);
	}

	public void setMut07oTkAdrSeqNbrFormatted(int mut07oTkAdrSeqNbrIdx, String mut07oTkAdrSeqNbr) {
		int position = Pos.mut07oTkAdrSeqNbr(mut07oTkAdrSeqNbrIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(mut07oTkAdrSeqNbr, Len.MUT07O_TK_ADR_SEQ_NBR), Len.MUT07O_TK_ADR_SEQ_NBR);
	}

	/**Original name: MUT07O-TK-ADR-SEQ-NBR<br>*/
	public int getMut07oTkAdrSeqNbr(int mut07oTkAdrSeqNbrIdx) {
		int position = Pos.mut07oTkAdrSeqNbr(mut07oTkAdrSeqNbrIdx - 1);
		return readNumDispUnsignedInt(position, Len.MUT07O_TK_ADR_SEQ_NBR);
	}

	public void setMut07oTkActNameIdFormatted(int mut07oTkActNameIdIdx, String mut07oTkActNameId) {
		int position = Pos.mut07oTkActNameId(mut07oTkActNameIdIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(mut07oTkActNameId, Len.MUT07O_TK_ACT_NAME_ID), Len.MUT07O_TK_ACT_NAME_ID);
	}

	/**Original name: MUT07O-TK-ACT-NAME-ID<br>*/
	public int getMut07oTkActNameId(int mut07oTkActNameIdIdx) {
		int position = Pos.mut07oTkActNameId(mut07oTkActNameIdIdx - 1);
		return readNumDispUnsignedInt(position, Len.MUT07O_TK_ACT_NAME_ID);
	}

	public void setMut07oIdvNmInd(int mut07oIdvNmIndIdx, char mut07oIdvNmInd) {
		int position = Pos.mut07oIdvNmInd(mut07oIdvNmIndIdx - 1);
		writeChar(position, mut07oIdvNmInd);
	}

	/**Original name: MUT07O-IDV-NM-IND<br>*/
	public char getMut07oIdvNmInd(int mut07oIdvNmIndIdx) {
		int position = Pos.mut07oIdvNmInd(mut07oIdvNmIndIdx - 1);
		return readChar(position);
	}

	public void setMut07oDspNm(int mut07oDspNmIdx, String mut07oDspNm) {
		int position = Pos.mut07oDspNm(mut07oDspNmIdx - 1);
		writeString(position, mut07oDspNm, Len.MUT07O_DSP_NM);
	}

	/**Original name: MUT07O-DSP-NM<br>*/
	public String getMut07oDspNm(int mut07oDspNmIdx) {
		int position = Pos.mut07oDspNm(mut07oDspNmIdx - 1);
		return readString(position, Len.MUT07O_DSP_NM);
	}

	public void setMut07oSrNm(int mut07oSrNmIdx, String mut07oSrNm) {
		int position = Pos.mut07oSrNm(mut07oSrNmIdx - 1);
		writeString(position, mut07oSrNm, Len.MUT07O_SR_NM);
	}

	/**Original name: MUT07O-SR-NM<br>*/
	public String getMut07oSrNm(int mut07oSrNmIdx) {
		int position = Pos.mut07oSrNm(mut07oSrNmIdx - 1);
		return readString(position, Len.MUT07O_SR_NM);
	}

	public void setMut07oLstNm(int mut07oLstNmIdx, String mut07oLstNm) {
		int position = Pos.mut07oLstNm(mut07oLstNmIdx - 1);
		writeString(position, mut07oLstNm, Len.MUT07O_LST_NM);
	}

	/**Original name: MUT07O-LST-NM<br>*/
	public String getMut07oLstNm(int mut07oLstNmIdx) {
		int position = Pos.mut07oLstNm(mut07oLstNmIdx - 1);
		return readString(position, Len.MUT07O_LST_NM);
	}

	public void setMut07oFstNm(int mut07oFstNmIdx, String mut07oFstNm) {
		int position = Pos.mut07oFstNm(mut07oFstNmIdx - 1);
		writeString(position, mut07oFstNm, Len.MUT07O_FST_NM);
	}

	/**Original name: MUT07O-FST-NM<br>*/
	public String getMut07oFstNm(int mut07oFstNmIdx) {
		int position = Pos.mut07oFstNm(mut07oFstNmIdx - 1);
		return readString(position, Len.MUT07O_FST_NM);
	}

	public void setMut07oMdlNm(int mut07oMdlNmIdx, String mut07oMdlNm) {
		int position = Pos.mut07oMdlNm(mut07oMdlNmIdx - 1);
		writeString(position, mut07oMdlNm, Len.MUT07O_MDL_NM);
	}

	/**Original name: MUT07O-MDL-NM<br>*/
	public String getMut07oMdlNm(int mut07oMdlNmIdx) {
		int position = Pos.mut07oMdlNm(mut07oMdlNmIdx - 1);
		return readString(position, Len.MUT07O_MDL_NM);
	}

	public void setMut07oPfx(int mut07oPfxIdx, String mut07oPfx) {
		int position = Pos.mut07oPfx(mut07oPfxIdx - 1);
		writeString(position, mut07oPfx, Len.MUT07O_PFX);
	}

	/**Original name: MUT07O-PFX<br>*/
	public String getMut07oPfx(int mut07oPfxIdx) {
		int position = Pos.mut07oPfx(mut07oPfxIdx - 1);
		return readString(position, Len.MUT07O_PFX);
	}

	public void setMut07oSfx(int mut07oSfxIdx, String mut07oSfx) {
		int position = Pos.mut07oSfx(mut07oSfxIdx - 1);
		writeString(position, mut07oSfx, Len.MUT07O_SFX);
	}

	/**Original name: MUT07O-SFX<br>*/
	public String getMut07oSfx(int mut07oSfxIdx) {
		int position = Pos.mut07oSfx(mut07oSfxIdx - 1);
		return readString(position, Len.MUT07O_SFX);
	}

	public void setMut07oLin1Adr(int mut07oLin1AdrIdx, String mut07oLin1Adr) {
		int position = Pos.mut07oLin1Adr(mut07oLin1AdrIdx - 1);
		writeString(position, mut07oLin1Adr, Len.MUT07O_LIN1_ADR);
	}

	/**Original name: MUT07O-LIN-1-ADR<br>*/
	public String getMut07oLin1Adr(int mut07oLin1AdrIdx) {
		int position = Pos.mut07oLin1Adr(mut07oLin1AdrIdx - 1);
		return readString(position, Len.MUT07O_LIN1_ADR);
	}

	public void setMut07oLin2Adr(int mut07oLin2AdrIdx, String mut07oLin2Adr) {
		int position = Pos.mut07oLin2Adr(mut07oLin2AdrIdx - 1);
		writeString(position, mut07oLin2Adr, Len.MUT07O_LIN2_ADR);
	}

	/**Original name: MUT07O-LIN-2-ADR<br>*/
	public String getMut07oLin2Adr(int mut07oLin2AdrIdx) {
		int position = Pos.mut07oLin2Adr(mut07oLin2AdrIdx - 1);
		return readString(position, Len.MUT07O_LIN2_ADR);
	}

	public void setMut07oCitNm(int mut07oCitNmIdx, String mut07oCitNm) {
		int position = Pos.mut07oCitNm(mut07oCitNmIdx - 1);
		writeString(position, mut07oCitNm, Len.MUT07O_CIT_NM);
	}

	/**Original name: MUT07O-CIT-NM<br>*/
	public String getMut07oCitNm(int mut07oCitNmIdx) {
		int position = Pos.mut07oCitNm(mut07oCitNmIdx - 1);
		return readString(position, Len.MUT07O_CIT_NM);
	}

	public void setMut07oStAbb(int mut07oStAbbIdx, String mut07oStAbb) {
		int position = Pos.mut07oStAbb(mut07oStAbbIdx - 1);
		writeString(position, mut07oStAbb, Len.MUT07O_ST_ABB);
	}

	/**Original name: MUT07O-ST-ABB<br>*/
	public String getMut07oStAbb(int mut07oStAbbIdx) {
		int position = Pos.mut07oStAbb(mut07oStAbbIdx - 1);
		return readString(position, Len.MUT07O_ST_ABB);
	}

	public void setMut07oPstCd(int mut07oPstCdIdx, String mut07oPstCd) {
		int position = Pos.mut07oPstCd(mut07oPstCdIdx - 1);
		writeString(position, mut07oPstCd, Len.MUT07O_PST_CD);
	}

	/**Original name: MUT07O-PST-CD<br>*/
	public String getMut07oPstCd(int mut07oPstCdIdx) {
		int position = Pos.mut07oPstCd(mut07oPstCdIdx - 1);
		return readString(position, Len.MUT07O_PST_CD);
	}

	public void setMut07oCtyNm(int mut07oCtyNmIdx, String mut07oCtyNm) {
		int position = Pos.mut07oCtyNm(mut07oCtyNmIdx - 1);
		writeString(position, mut07oCtyNm, Len.MUT07O_CTY_NM);
	}

	/**Original name: MUT07O-CTY-NM<br>*/
	public String getMut07oCtyNm(int mut07oCtyNmIdx) {
		int position = Pos.mut07oCtyNm(mut07oCtyNmIdx - 1);
		return readString(position, Len.MUT07O_CTY_NM);
	}

	public void setMut07oRltTypCd(int mut07oRltTypCdIdx, String mut07oRltTypCd) {
		int position = Pos.mut07oRltTypCd(mut07oRltTypCdIdx - 1);
		writeString(position, mut07oRltTypCd, Len.MUT07O_RLT_TYP_CD);
	}

	/**Original name: MUT07O-RLT-TYP-CD<br>*/
	public String getMut07oRltTypCd(int mut07oRltTypCdIdx) {
		int position = Pos.mut07oRltTypCd(mut07oRltTypCdIdx - 1);
		return readString(position, Len.MUT07O_RLT_TYP_CD);
	}

	public void setMut07oRltTypDes(int mut07oRltTypDesIdx, String mut07oRltTypDes) {
		int position = Pos.mut07oRltTypDes(mut07oRltTypDesIdx - 1);
		writeString(position, mut07oRltTypDes, Len.MUT07O_RLT_TYP_DES);
	}

	/**Original name: MUT07O-RLT-TYP-DES<br>*/
	public String getMut07oRltTypDes(int mut07oRltTypDesIdx) {
		int position = Pos.mut07oRltTypDes(mut07oRltTypDesIdx - 1);
		return readString(position, Len.MUT07O_RLT_TYP_DES);
	}

	public void setMut07oActTypCd(int mut07oActTypCdIdx, String mut07oActTypCd) {
		int position = Pos.mut07oActTypCd(mut07oActTypCdIdx - 1);
		writeString(position, mut07oActTypCd, Len.MUT07O_ACT_TYP_CD);
	}

	/**Original name: MUT07O-ACT-TYP-CD<br>*/
	public String getMut07oActTypCd(int mut07oActTypCdIdx) {
		int position = Pos.mut07oActTypCd(mut07oActTypCdIdx - 1);
		return readString(position, Len.MUT07O_ACT_TYP_CD);
	}

	public void setMut07oActTypDes(int mut07oActTypDesIdx, String mut07oActTypDes) {
		int position = Pos.mut07oActTypDes(mut07oActTypDesIdx - 1);
		writeString(position, mut07oActTypDes, Len.MUT07O_ACT_TYP_DES);
	}

	/**Original name: MUT07O-ACT-TYP-DES<br>*/
	public String getMut07oActTypDes(int mut07oActTypDesIdx) {
		int position = Pos.mut07oActTypDes(mut07oActTypDesIdx - 1);
		return readString(position, Len.MUT07O_ACT_TYP_DES);
	}

	public void setMut07oCltEffDt(int mut07oCltEffDtIdx, String mut07oCltEffDt) {
		int position = Pos.mut07oCltEffDt(mut07oCltEffDtIdx - 1);
		writeString(position, mut07oCltEffDt, Len.MUT07O_CLT_EFF_DT);
	}

	/**Original name: MUT07O-CLT-EFF-DT<br>*/
	public String getMut07oCltEffDt(int mut07oCltEffDtIdx) {
		int position = Pos.mut07oCltEffDt(mut07oCltEffDtIdx - 1);
		return readString(position, Len.MUT07O_CLT_EFF_DT);
	}

	public void setMut07oCltExpDt(int mut07oCltExpDtIdx, String mut07oCltExpDt) {
		int position = Pos.mut07oCltExpDt(mut07oCltExpDtIdx - 1);
		writeString(position, mut07oCltExpDt, Len.MUT07O_CLT_EXP_DT);
	}

	/**Original name: MUT07O-CLT-EXP-DT<br>*/
	public String getMut07oCltExpDt(int mut07oCltExpDtIdx) {
		int position = Pos.mut07oCltExpDt(mut07oCltExpDtIdx - 1);
		return readString(position, Len.MUT07O_CLT_EXP_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_MU0T0007_ROW = 1;
		public static final int MUT007_SERVICE_INPUTS = WS_MU0T0007_ROW;
		public static final int MUT07I_ACT_NBR = MUT007_SERVICE_INPUTS;
		public static final int MUT07I_USERID = MUT07I_ACT_NBR + Len.MUT07I_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int mut007ServiceOutputs(int idx) {
			return MUT07I_USERID + Len.MUT07I_USERID + idx * Len.MUT007_SERVICE_OUTPUTS;
		}

		public static int mut07oTechnicalKeys(int idx) {
			return mut007ServiceOutputs(idx);
		}

		public static int mut07oTkCltId(int idx) {
			return mut07oTechnicalKeys(idx);
		}

		public static int mut07oTkAdrId(int idx) {
			return mut07oTkCltId(idx) + Len.MUT07O_TK_CLT_ID;
		}

		public static int mut07oTkAdrSeqNbr(int idx) {
			return mut07oTkAdrId(idx) + Len.MUT07O_TK_ADR_ID;
		}

		public static int mut07oTkActNameId(int idx) {
			return mut07oTkAdrSeqNbr(idx) + Len.MUT07O_TK_ADR_SEQ_NBR;
		}

		public static int mut07oClientName(int idx) {
			return mut07oTkActNameId(idx) + Len.MUT07O_TK_ACT_NAME_ID;
		}

		public static int mut07oIdvNmInd(int idx) {
			return mut07oClientName(idx);
		}

		public static int mut07oDspNm(int idx) {
			return mut07oIdvNmInd(idx) + Len.MUT07O_IDV_NM_IND;
		}

		public static int mut07oSrNm(int idx) {
			return mut07oDspNm(idx) + Len.MUT07O_DSP_NM;
		}

		public static int mut07oLstNm(int idx) {
			return mut07oSrNm(idx) + Len.MUT07O_SR_NM;
		}

		public static int mut07oFstNm(int idx) {
			return mut07oLstNm(idx) + Len.MUT07O_LST_NM;
		}

		public static int mut07oMdlNm(int idx) {
			return mut07oFstNm(idx) + Len.MUT07O_FST_NM;
		}

		public static int mut07oPfx(int idx) {
			return mut07oMdlNm(idx) + Len.MUT07O_MDL_NM;
		}

		public static int mut07oSfx(int idx) {
			return mut07oPfx(idx) + Len.MUT07O_PFX;
		}

		public static int mut07oBusinessMailAddress(int idx) {
			return mut07oSfx(idx) + Len.MUT07O_SFX;
		}

		public static int mut07oLin1Adr(int idx) {
			return mut07oBusinessMailAddress(idx);
		}

		public static int mut07oLin2Adr(int idx) {
			return mut07oLin1Adr(idx) + Len.MUT07O_LIN1_ADR;
		}

		public static int mut07oCitNm(int idx) {
			return mut07oLin2Adr(idx) + Len.MUT07O_LIN2_ADR;
		}

		public static int mut07oStAbb(int idx) {
			return mut07oCitNm(idx) + Len.MUT07O_CIT_NM;
		}

		public static int mut07oPstCd(int idx) {
			return mut07oStAbb(idx) + Len.MUT07O_ST_ABB;
		}

		public static int mut07oCtyNm(int idx) {
			return mut07oPstCd(idx) + Len.MUT07O_PST_CD;
		}

		public static int mut07oClientRelationType(int idx) {
			return mut07oCtyNm(idx) + Len.MUT07O_CTY_NM;
		}

		public static int mut07oRltTypCd(int idx) {
			return mut07oClientRelationType(idx);
		}

		public static int mut07oRltTypDes(int idx) {
			return mut07oRltTypCd(idx) + Len.MUT07O_RLT_TYP_CD;
		}

		public static int mut07oAccountType(int idx) {
			return mut07oRltTypDes(idx) + Len.MUT07O_RLT_TYP_DES;
		}

		public static int mut07oActTypCd(int idx) {
			return mut07oAccountType(idx);
		}

		public static int mut07oActTypDes(int idx) {
			return mut07oActTypCd(idx) + Len.MUT07O_ACT_TYP_CD;
		}

		public static int mut07oCltEffDt(int idx) {
			return mut07oActTypDes(idx) + Len.MUT07O_ACT_TYP_DES;
		}

		public static int mut07oCltExpDt(int idx) {
			return mut07oCltEffDt(idx) + Len.MUT07O_CLT_EFF_DT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int MUT07I_ACT_NBR = 9;
		public static final int MUT07I_USERID = 8;
		public static final int MUT07O_TK_CLT_ID = 20;
		public static final int MUT07O_TK_ADR_ID = 20;
		public static final int MUT07O_TK_ADR_SEQ_NBR = 5;
		public static final int MUT07O_TK_ACT_NAME_ID = 5;
		public static final int MUT07O_TECHNICAL_KEYS = MUT07O_TK_CLT_ID + MUT07O_TK_ADR_ID + MUT07O_TK_ADR_SEQ_NBR + MUT07O_TK_ACT_NAME_ID;
		public static final int MUT07O_IDV_NM_IND = 1;
		public static final int MUT07O_DSP_NM = 120;
		public static final int MUT07O_SR_NM = 60;
		public static final int MUT07O_LST_NM = 60;
		public static final int MUT07O_FST_NM = 30;
		public static final int MUT07O_MDL_NM = 30;
		public static final int MUT07O_PFX = 4;
		public static final int MUT07O_SFX = 4;
		public static final int MUT07O_CLIENT_NAME = MUT07O_IDV_NM_IND + MUT07O_DSP_NM + MUT07O_SR_NM + MUT07O_LST_NM + MUT07O_FST_NM + MUT07O_MDL_NM
				+ MUT07O_PFX + MUT07O_SFX;
		public static final int MUT07O_LIN1_ADR = 45;
		public static final int MUT07O_LIN2_ADR = 45;
		public static final int MUT07O_CIT_NM = 30;
		public static final int MUT07O_ST_ABB = 3;
		public static final int MUT07O_PST_CD = 13;
		public static final int MUT07O_CTY_NM = 30;
		public static final int MUT07O_BUSINESS_MAIL_ADDRESS = MUT07O_LIN1_ADR + MUT07O_LIN2_ADR + MUT07O_CIT_NM + MUT07O_ST_ABB + MUT07O_PST_CD
				+ MUT07O_CTY_NM;
		public static final int MUT07O_RLT_TYP_CD = 4;
		public static final int MUT07O_RLT_TYP_DES = 40;
		public static final int MUT07O_CLIENT_RELATION_TYPE = MUT07O_RLT_TYP_CD + MUT07O_RLT_TYP_DES;
		public static final int MUT07O_ACT_TYP_CD = 4;
		public static final int MUT07O_ACT_TYP_DES = 40;
		public static final int MUT07O_ACCOUNT_TYPE = MUT07O_ACT_TYP_CD + MUT07O_ACT_TYP_DES;
		public static final int MUT07O_CLT_EFF_DT = 10;
		public static final int MUT07O_CLT_EXP_DT = 10;
		public static final int MUT007_SERVICE_OUTPUTS = MUT07O_TECHNICAL_KEYS + MUT07O_CLIENT_NAME + MUT07O_BUSINESS_MAIL_ADDRESS
				+ MUT07O_CLIENT_RELATION_TYPE + MUT07O_ACCOUNT_TYPE + MUT07O_CLT_EFF_DT + MUT07O_CLT_EXP_DT;
		public static final int MUT007_SERVICE_INPUTS = MUT07I_ACT_NBR + MUT07I_USERID;
		public static final int WS_MU0T0007_ROW = MUT007_SERVICE_INPUTS + WsMu0t0007Row.MUT007_SERVICE_OUTPUTS_MAXOCCURS * MUT007_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
