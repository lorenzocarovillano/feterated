/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-PARAGRAPH<br>
 * Variable: SA-PARAGRAPH from program XZ003000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaParagraph {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.PARAGRAPH);
	public static final String PN3100 = "3100";
	public static final String PN3200 = "3200";
	public static final String PN9200 = "9200";

	//==== METHODS ====
	public void setParagraph(String paragraph) {
		this.value = Functions.subString(paragraph, Len.PARAGRAPH);
	}

	public String getParagraph() {
		return this.value;
	}

	public void setPn3100() {
		value = PN3100;
	}

	public void setPn3200() {
		value = PN3200;
	}

	public void setPn9200() {
		value = PN9200;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PARAGRAPH = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
