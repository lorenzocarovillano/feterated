/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SA-RETURN-CODE<br>
 * Variable: SA-RETURN-CODE from program XZ001000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaReturnCode {

	//==== PROPERTIES ====
	private short value = ((short) 0);
	public static final short MISSING_CICS_PARM = ((short) 100);
	public static final short REACH_TABLE_MAX = ((short) 110);
	public static final short WF_CALL_FAILED = ((short) 120);
	public static final short OTHER_DB2_ERROR = ((short) 200);
	public static final short CONTENTION_DB2_ERROR = ((short) 300);
	public static final short FATAL_DB2_ERROR = ((short) 500);

	//==== METHODS ====
	public void setReturnCode(short returnCode) {
		this.value = returnCode;
	}

	public short getReturnCode() {
		return this.value;
	}

	public void setMissingCicsParm() {
		value = MISSING_CICS_PARM;
	}

	public void setReachTableMax() {
		value = REACH_TABLE_MAX;
	}

	public void setWfCallFailed() {
		value = WF_CALL_FAILED;
	}

	public void setContentionDb2Error() {
		value = CONTENTION_DB2_ERROR;
	}

	public void setFatalDb2Error() {
		value = FATAL_DB2_ERROR;
	}
}
