/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [CLT_OBJ_RELATION_V]
 * 
 */
public interface ICltObjRelationV extends BaseSqlTo {

	/**
	 * Host Variable CW08H-TCH-OBJECT-KEY
	 * 
	 */
	String getTchObjectKey();

	void setTchObjectKey(String tchObjectKey);

	/**
	 * Host Variable CW08H-HISTORY-VLD-NBR
	 * 
	 */
	short getHistoryVldNbr();

	void setHistoryVldNbr(short historyVldNbr);

	/**
	 * Host Variable CW08H-CIOR-EFF-DT
	 * 
	 */
	String getCiorEffDt();

	void setCiorEffDt(String ciorEffDt);

	/**
	 * Host Variable CW08H-OBJ-SYS-ID
	 * 
	 */
	String getObjSysId();

	void setObjSysId(String objSysId);

	/**
	 * Host Variable CW08H-CIOR-OBJ-SEQ-NBR
	 * 
	 */
	short getCiorObjSeqNbr();

	void setCiorObjSeqNbr(short ciorObjSeqNbr);

	/**
	 * Host Variable CW08H-CLIENT-ID
	 * 
	 */
	String getClientId();

	void setClientId(String clientId);

	/**
	 * Host Variable CW08H-RLT-TYP-CD
	 * 
	 */
	String getRltTypCd();

	void setRltTypCd(String rltTypCd);

	/**
	 * Host Variable CW08H-OBJ-CD
	 * 
	 */
	String getObjCd();

	void setObjCd(String objCd);

	/**
	 * Host Variable CW08H-CIOR-SHW-OBJ-KEY
	 * 
	 */
	String getCiorShwObjKey();

	void setCiorShwObjKey(String ciorShwObjKey);

	/**
	 * Host Variable CW08H-ADR-SEQ-NBR
	 * 
	 */
	short getAdrSeqNbr();

	void setAdrSeqNbr(short adrSeqNbr);

	/**
	 * Host Variable CW08H-USER-ID
	 * 
	 */
	String getUserId();

	void setUserId(String userId);

	/**
	 * Host Variable CW08H-STATUS-CD
	 * 
	 */
	char getStatusCd();

	void setStatusCd(char statusCd);

	/**
	 * Host Variable CW08H-TERMINAL-ID
	 * 
	 */
	String getTerminalId();

	void setTerminalId(String terminalId);

	/**
	 * Host Variable CW08H-CIOR-EXP-DT
	 * 
	 */
	String getCiorExpDt();

	void setCiorExpDt(String ciorExpDt);

	/**
	 * Host Variable CW08H-CIOR-EFF-ACY-TS
	 * 
	 */
	String getCiorEffAcyTs();

	void setCiorEffAcyTs(String ciorEffAcyTs);

	/**
	 * Host Variable CW08H-CIOR-EXP-ACY-TS
	 * 
	 */
	String getCiorExpAcyTs();

	void setCiorExpAcyTs(String ciorExpAcyTs);
};
