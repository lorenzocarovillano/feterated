/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0Q0021<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0q0021Data {

	//==== PROPERTIES ====
	//Original name: CF-BUS-OBJ-XZ-GET-NOT-DT-BPO
	private String cfBusObjXzGetNotDtBpo = "XZ_DET_NOT_DT_BPO";
	//Original name: FILLER-CF-INVALID-OPERATION
	private String flr1 = "REQUEST MODULE";
	//Original name: FILLER-CF-INVALID-OPERATION-1
	private String flr2 = "- INVALID";
	//Original name: FILLER-CF-INVALID-OPERATION-2
	private String flr3 = "OPERATION";
	//Original name: FILLER-EA-02-INVALID-OPERATION-MSG
	private String flr6 = "XZ0Q0021 -";
	//Original name: FILLER-EA-02-INVALID-OPERATION-MSG-1
	private String flr7 = "INVALID OPER:";
	//Original name: EA-02-INVALID-OPERATION-NAME
	private String ea02InvalidOperationName = DefaultValues.stringVal(Len.EA02_INVALID_OPERATION_NAME);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0q0021 workingStorageArea = new WorkingStorageAreaXz0q0021();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: HALLUSW
	private Hallusw hallusw = new Hallusw();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public String getCfBusObjXzGetNotDtBpo() {
		return this.cfBusObjXzGetNotDtBpo;
	}

	public String getCfInvalidOperationFormatted() {
		return MarshalByteExt.bufferToStr(getCfInvalidOperationBytes());
	}

	/**Original name: CF-INVALID-OPERATION<br>*/
	public byte[] getCfInvalidOperationBytes() {
		byte[] buffer = new byte[Len.CF_INVALID_OPERATION];
		return getCfInvalidOperationBytes(buffer, 1);
	}

	public byte[] getCfInvalidOperationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getEa02InvalidOperationMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidOperationMsgBytes());
	}

	/**Original name: EA-02-INVALID-OPERATION-MSG<br>*/
	public byte[] getEa02InvalidOperationMsgBytes() {
		byte[] buffer = new byte[Len.EA02_INVALID_OPERATION_MSG];
		return getEa02InvalidOperationMsgBytes(buffer, 1);
	}

	public byte[] getEa02InvalidOperationMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, ea02InvalidOperationName, Len.EA02_INVALID_OPERATION_NAME);
		return buffer;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setEa02InvalidOperationName(String ea02InvalidOperationName) {
		this.ea02InvalidOperationName = Functions.subString(ea02InvalidOperationName, Len.EA02_INVALID_OPERATION_NAME);
	}

	public String getEa02InvalidOperationName() {
		return this.ea02InvalidOperationName;
	}

	/**Original name: WS-USW-MSG<br>*/
	public byte[] getWsUswMsgBytes() {
		byte[] buffer = new byte[Len.WS_USW_MSG];
		return getWsUswMsgBytes(buffer, 1);
	}

	public byte[] getWsUswMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		hallusw.getCommonBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	public WorkingStorageAreaXz0q0021 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA01_FAILED_LINK_PGM_NAME = 8;
		public static final int EA02_INVALID_OPERATION_NAME = 32;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_USW_MSG = Hallusw.Len.COMMON;
		public static final int FLR1 = 15;
		public static final int FLR2 = 10;
		public static final int FLR3 = 9;
		public static final int CF_INVALID_OPERATION = FLR1 + FLR2 + FLR3;
		public static final int FLR6 = 11;
		public static final int FLR7 = 14;
		public static final int EA02_INVALID_OPERATION_MSG = EA02_INVALID_OPERATION_NAME + FLR6 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
