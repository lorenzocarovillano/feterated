/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HALOUKRP-RETURN-CODE<br>
 * Variable: HALOUKRP-RETURN-CODE from copybook HALLUKRP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HaloukrpReturnCode {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.RETURN_CODE);
	public static final String OKAY = "00";
	public static final String NOTFND = "01";

	//==== METHODS ====
	public void setReturnCode(short returnCode) {
		this.value = NumericDisplay.asString(returnCode, Len.RETURN_CODE);
	}

	public void setReturnCodeFormatted(String returnCode) {
		this.value = Trunc.toUnsignedNumeric(returnCode, Len.RETURN_CODE);
	}

	public short getReturnCode() {
		return NumericDisplay.asShort(this.value);
	}

	public String getReturnCodeFormatted() {
		return this.value;
	}

	public boolean isHaloukrpOkay() {
		return getReturnCodeFormatted().equals(OKAY);
	}

	public void setOkay() {
		setReturnCodeFormatted(OKAY);
	}

	public boolean isHaloukrpNotfnd() {
		return getReturnCodeFormatted().equals(NOTFND);
	}

	public void setNotfnd() {
		setReturnCodeFormatted(NOTFND);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURN_CODE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
