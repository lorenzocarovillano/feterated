/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.federatedinsurance.crs.commons.data.to.IActNotPolTypDtaExt;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclactNotTyp;
import com.federatedinsurance.crs.copy.DclpolDtaExt;
import com.federatedinsurance.crs.copy.Tt008001;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZC01090<br>
 * Generated as a class for rule WS.<br>*/
public class Xzc01090Data implements IActNotPolTypDtaExt {

	//==== PROPERTIES ====
	//Original name: DCLPOL-DTA-EXT
	private DclpolDtaExt dclpolDtaExt = new DclpolDtaExt();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: DCLACT-NOT-TYP
	private DclactNotTyp dclactNotTyp = new DclactNotTyp();
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXzc01090 constantFields = new ConstantFieldsXzc01090();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXzc01090 errorAndAdviceMessages = new ErrorAndAdviceMessagesXzc01090();
	//Original name: NI-EMP-ID
	private short niEmpId = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-NLBE-IDX
	private short ssNlbeIdx = DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-SYSTEM-NOTICE-FLAG
	private boolean swSystemNoticeFlag = false;
	//Original name: WS-RESPONSE-CODE
	private int wsResponseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int wsResponseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: TT008001
	private Tt008001 tt008001 = new Tt008001();

	//==== METHODS ====
	public void setNiEmpId(short niEmpId) {
		this.niEmpId = niEmpId;
	}

	public short getNiEmpId() {
		return this.niEmpId;
	}

	public void setSsNlbeIdx(short ssNlbeIdx) {
		this.ssNlbeIdx = ssNlbeIdx;
	}

	public short getSsNlbeIdx() {
		return this.ssNlbeIdx;
	}

	public void setSwSystemNoticeFlag(boolean swSystemNoticeFlag) {
		this.swSystemNoticeFlag = swSystemNoticeFlag;
	}

	public boolean isSwSystemNoticeFlag() {
		return this.swSystemNoticeFlag;
	}

	public void setWsResponseCode(int wsResponseCode) {
		this.wsResponseCode = wsResponseCode;
	}

	public int getWsResponseCode() {
		return this.wsResponseCode;
	}

	public void setWsResponseCode2(int wsResponseCode2) {
		this.wsResponseCode2 = wsResponseCode2;
	}

	public int getWsResponseCode2() {
		return this.wsResponseCode2;
	}

	@Override
	public String getActNotDes() {
		return dclactNotTyp.getActNotDes();
	}

	@Override
	public void setActNotDes(String actNotDes) {
		this.dclactNotTyp.setActNotDes(actNotDes);
	}

	@Override
	public String getActNotTypCd() {
		return dclactNot.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.dclactNot.setActNotTypCd(actNotTypCd);
	}

	public ConstantFieldsXzc01090 getConstantFields() {
		return constantFields;
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclactNotTyp getDclactNotTyp() {
		return dclactNotTyp;
	}

	public DclpolDtaExt getDclpolDtaExt() {
		return dclpolDtaExt;
	}

	@Override
	public String getEmpId() {
		return dclactNot.getEmpId();
	}

	@Override
	public void setEmpId(String empId) {
		this.dclactNot.setEmpId(empId);
	}

	@Override
	public String getEmpIdObj() {
		if (getNiEmpId() >= 0) {
			return getEmpId();
		} else {
			return null;
		}
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		if (empIdObj != null) {
			setEmpId(empIdObj);
			setNiEmpId(((short) 0));
		} else {
			setNiEmpId(((short) -1));
		}
	}

	public ErrorAndAdviceMessagesXzc01090 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	@Override
	public String getNotDt() {
		return dclactNot.getNotDt();
	}

	@Override
	public void setNotDt(String notDt) {
		this.dclactNot.setNotDt(notDt);
	}

	@Override
	public String getNotEffDt() {
		return dclactNotPol.getNotEffDt();
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		this.dclactNotPol.setNotEffDt(notEffDt);
	}

	public Tt008001 getTt008001() {
		return tt008001;
	}
}
