/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-HOLD-AREA<br>
 * Variable: WS-HOLD-AREA from program CAWS002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsHoldArea {

	//==== PROPERTIES ====
	//Original name: WS-LIT-FIELD-NM
	private String litFieldNm = DefaultValues.stringVal(Len.LIT_FIELD_NM);
	//Original name: WS-VALID-DATE-HOLD
	private String validDateHold = DefaultValues.stringVal(Len.VALID_DATE_HOLD);
	//Original name: WS-LINK-TO-PGM-NM
	private String linkToPgmNm = DefaultValues.stringVal(Len.LINK_TO_PGM_NM);
	//Original name: WS-HOLD-DTA-TXT
	private String holdDtaTxt = DefaultValues.stringVal(Len.HOLD_DTA_TXT);
	//Original name: WS-HOLD-AMT
	private AfDecimal holdAmt = new AfDecimal(DefaultValues.DEC_VAL, 14, 2);
	//Original name: WS-HOLD-SIGN
	private char holdSign = DefaultValues.CHAR_VAL;
	//Original name: WS-HOLD-CI
	private char holdCi = DefaultValues.CHAR_VAL;
	public static final char IGNORE_COL_IND = '*';
	//Original name: WS-CALC-TIMESTAMP
	private String calcTimestamp = DefaultValues.stringVal(Len.CALC_TIMESTAMP);

	//==== METHODS ====
	public void setLitFieldNm(String litFieldNm) {
		this.litFieldNm = Functions.subString(litFieldNm, Len.LIT_FIELD_NM);
	}

	public String getLitFieldNm() {
		return this.litFieldNm;
	}

	public void setValidDateHold(String validDateHold) {
		this.validDateHold = Functions.subString(validDateHold, Len.VALID_DATE_HOLD);
	}

	public String getValidDateHold() {
		return this.validDateHold;
	}

	public String getValidDateHoldFormatted() {
		return Functions.padBlanks(getValidDateHold(), Len.VALID_DATE_HOLD);
	}

	public void setLinkToPgmNm(String linkToPgmNm) {
		this.linkToPgmNm = Functions.subString(linkToPgmNm, Len.LINK_TO_PGM_NM);
	}

	public String getLinkToPgmNm() {
		return this.linkToPgmNm;
	}

	public void setHoldDtaTxt(String holdDtaTxt) {
		this.holdDtaTxt = Functions.subString(holdDtaTxt, Len.HOLD_DTA_TXT);
	}

	public String getHoldDtaTxt() {
		return this.holdDtaTxt;
	}

	public String getHoldDtaTxtFormatted() {
		return Functions.padBlanks(getHoldDtaTxt(), Len.HOLD_DTA_TXT);
	}

	public void setHoldAmt(AfDecimal holdAmt) {
		this.holdAmt.assign(holdAmt);
	}

	public AfDecimal getHoldAmt() {
		return this.holdAmt.copy();
	}

	public void setHoldSign(char holdSign) {
		this.holdSign = holdSign;
	}

	public void setHoldSignFormatted(String holdSign) {
		setHoldSign(Functions.charAt(holdSign, Types.CHAR_SIZE));
	}

	public char getHoldSign() {
		return this.holdSign;
	}

	public void setHoldCi(char holdCi) {
		this.holdCi = holdCi;
	}

	public char getHoldCi() {
		return this.holdCi;
	}

	public boolean isIgnoreColInd() {
		return holdCi == IGNORE_COL_IND;
	}

	public void setIgnoreColInd() {
		holdCi = IGNORE_COL_IND;
	}

	public void setCalcTimestamp(String calcTimestamp) {
		this.calcTimestamp = Functions.subString(calcTimestamp, Len.CALC_TIMESTAMP);
	}

	public String getCalcTimestamp() {
		return this.calcTimestamp;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LIT_FIELD_NM = 30;
		public static final int VALID_DATE_HOLD = 10;
		public static final int LINK_TO_PGM_NM = 8;
		public static final int HOLD_DTA_TXT = 32;
		public static final int CALC_TIMESTAMP = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
