/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ005I-STA-CD-LIS<br>
 * Variables: XZ005I-STA-CD-LIS from copybook XZ05CL1I<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xz005iStaCdLis {

	//==== PROPERTIES ====
	//Original name: XZ005I-STA-CD
	private String xz005iStaCd = DefaultValues.stringVal(Len.XZ005I_STA_CD);

	//==== METHODS ====
	public byte[] getStaCdLisBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xz005iStaCd, Len.XZ005I_STA_CD);
		return buffer;
	}

	public void initStaCdLisLowValues() {
		xz005iStaCd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ005I_STA_CD);
	}

	public void setXz005iStaCd(String xz005iStaCd) {
		this.xz005iStaCd = Functions.subString(xz005iStaCd, Len.XZ005I_STA_CD);
	}

	public String getXz005iStaCd() {
		return this.xz005iStaCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZ005I_STA_CD = 2;
		public static final int STA_CD_LIS = XZ005I_STA_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
