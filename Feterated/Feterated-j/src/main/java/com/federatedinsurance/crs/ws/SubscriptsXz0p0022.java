/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program XZ0P0022<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsXz0p0022 {

	//==== PROPERTIES ====
	/**Original name: SS-IP<br>
	 * <pre>    Input policy subscript</pre>*/
	private short ip = DefaultValues.BIN_SHORT_VAL;
	/**Original name: SS-TP<br>
	 * <pre>    Terminate policy service list</pre>*/
	private short tp = DefaultValues.BIN_SHORT_VAL;
	/**Original name: SS-AP<br>
	 * <pre>    Active policy subscript for output from XZ0X90S0</pre>*/
	private short ap = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-MSG-IDX
	private short msgIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short MSG_IDX_MAX = ((short) 10);
	//Original name: SS-WNG-IDX
	private short wngIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short WNG_IDX_MAX = ((short) 10);

	//==== METHODS ====
	public void setIp(short ip) {
		this.ip = ip;
	}

	public short getIp() {
		return this.ip;
	}

	public void setTp(short tp) {
		this.tp = tp;
	}

	public short getTp() {
		return this.tp;
	}

	public void setAp(short ap) {
		this.ap = ap;
	}

	public short getAp() {
		return this.ap;
	}

	public void setMsgIdx(short msgIdx) {
		this.msgIdx = msgIdx;
	}

	public short getMsgIdx() {
		return this.msgIdx;
	}

	public boolean isMsgIdxMax() {
		return msgIdx == MSG_IDX_MAX;
	}

	public void setWngIdx(short wngIdx) {
		this.wngIdx = wngIdx;
	}

	public short getWngIdx() {
		return this.wngIdx;
	}

	public boolean isWngIdxMax() {
		return wngIdx == WNG_IDX_MAX;
	}
}
