/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0G90M1<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0g90m1Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0g90m1 constantFields = new ConstantFieldsXz0g90m1();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXz0g90m1 errorAndAdviceMessages = new ErrorAndAdviceMessagesXz0g90m1();
	//Original name: SUBSCRIPTS
	private SubscriptsXz0g90m1 subscripts = new SubscriptsXz0g90m1();
	//Original name: WS-MISC-WORK-FLDS
	private WsMiscWorkFldsXz0g90m1 wsMiscWorkFlds = new WsMiscWorkFldsXz0g90m1();
	//Original name: SERVICE-PROXY-CONTRACT
	private WsProxyProgramArea serviceProxyContract = new WsProxyProgramArea();
	//Original name: INT-REGISTER1
	private short intRegister1 = DefaultValues.SHORT_VAL;

	//==== METHODS ====
	public void setIntRegister1(short intRegister1) {
		this.intRegister1 = intRegister1;
	}

	public short getIntRegister1() {
		return this.intRegister1;
	}

	public ConstantFieldsXz0g90m1 getConstantFields() {
		return constantFields;
	}

	public ErrorAndAdviceMessagesXz0g90m1 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public WsProxyProgramArea getServiceProxyContract() {
		return serviceProxyContract;
	}

	public SubscriptsXz0g90m1 getSubscripts() {
		return subscripts;
	}

	public WsMiscWorkFldsXz0g90m1 getWsMiscWorkFlds() {
		return wsMiscWorkFlds;
	}
}
