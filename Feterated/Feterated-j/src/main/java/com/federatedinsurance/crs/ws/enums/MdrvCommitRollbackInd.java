/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: MDRV-COMMIT-ROLLBACK-IND<br>
 * Variable: MDRV-COMMIT-ROLLBACK-IND from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvCommitRollbackInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char COMMIT = Types.SPACE_CHAR;
	public static final char ROLLBACK = 'N';

	//==== METHODS ====
	public void setCommitRollbackInd(char commitRollbackInd) {
		this.value = commitRollbackInd;
	}

	public void setCommitRollbackIndFormatted(String commitRollbackInd) {
		setCommitRollbackInd(Functions.charAt(commitRollbackInd, Types.CHAR_SIZE));
	}

	public char getCommitRollbackInd() {
		return this.value;
	}

	public void setCommit() {
		setCommitRollbackIndFormatted(String.valueOf(COMMIT));
	}

	public boolean isRollback() {
		return value == ROLLBACK;
	}

	public void setRollback() {
		value = ROLLBACK;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int COMMIT_ROLLBACK_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
