/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: BX0T0003<br>
 * Variable: BX0T0003 from copybook BX0T0003<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Bx0t0003 {

	//==== PROPERTIES ====
	//Original name: BXT03I-ACCOUNT-NBR
	private String bxt03iAccountNbr = DefaultValues.stringVal(Len.BXT03I_ACCOUNT_NBR);
	//Original name: BXT03I-USERID
	private String bxt03iUserid = DefaultValues.stringVal(Len.BXT03I_USERID);
	//Original name: BXT03O-ACCOUNT-NBR
	private String bxt03oAccountNbr = DefaultValues.stringVal(Len.BXT03O_ACCOUNT_NBR);
	//Original name: BXT03O-USERID
	private String bxt03oUserid = DefaultValues.stringVal(Len.BXT03O_USERID);
	//Original name: BXT03O-ACCOUNT-INFO
	private Bxt03oAccountInfo bxt03oAccountInfo = new Bxt03oAccountInfo();
	/**Original name: FILLER-BXT003-SERVICE-OUTPUTS<br>
	 * <pre> SPACE FOR FUTURE FIELDS</pre>*/
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setBxt003ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		setBxt03iTechnicalKeyBytes(buffer, position);
	}

	public byte[] getBxt003ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		getBxt03iTechnicalKeyBytes(buffer, position);
		return buffer;
	}

	public void setBxt03iTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		bxt03iAccountNbr = MarshalByte.readString(buffer, position, Len.BXT03I_ACCOUNT_NBR);
		position += Len.BXT03I_ACCOUNT_NBR;
		bxt03iUserid = MarshalByte.readString(buffer, position, Len.BXT03I_USERID);
	}

	public byte[] getBxt03iTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, bxt03iAccountNbr, Len.BXT03I_ACCOUNT_NBR);
		position += Len.BXT03I_ACCOUNT_NBR;
		MarshalByte.writeString(buffer, position, bxt03iUserid, Len.BXT03I_USERID);
		return buffer;
	}

	public void setBxt03iAccountNbr(String bxt03iAccountNbr) {
		this.bxt03iAccountNbr = Functions.subString(bxt03iAccountNbr, Len.BXT03I_ACCOUNT_NBR);
	}

	public String getBxt03iAccountNbr() {
		return this.bxt03iAccountNbr;
	}

	public void setBxt03iUserid(String bxt03iUserid) {
		this.bxt03iUserid = Functions.subString(bxt03iUserid, Len.BXT03I_USERID);
	}

	public String getBxt03iUserid() {
		return this.bxt03iUserid;
	}

	public void setBxt003ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		setBxt03oTechnicalKeyBytes(buffer, position);
		position += Len.BXT03O_TECHNICAL_KEY;
		bxt03oAccountInfo.setBxt03oAccountInfoBytes(buffer, position);
		position += Bxt03oAccountInfo.Len.BXT03O_ACCOUNT_INFO;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getBxt003ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		getBxt03oTechnicalKeyBytes(buffer, position);
		position += Len.BXT03O_TECHNICAL_KEY;
		bxt03oAccountInfo.getBxt03oAccountInfoBytes(buffer, position);
		position += Bxt03oAccountInfo.Len.BXT03O_ACCOUNT_INFO;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setBxt03oTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		bxt03oAccountNbr = MarshalByte.readString(buffer, position, Len.BXT03O_ACCOUNT_NBR);
		position += Len.BXT03O_ACCOUNT_NBR;
		bxt03oUserid = MarshalByte.readString(buffer, position, Len.BXT03O_USERID);
	}

	public byte[] getBxt03oTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, bxt03oAccountNbr, Len.BXT03O_ACCOUNT_NBR);
		position += Len.BXT03O_ACCOUNT_NBR;
		MarshalByte.writeString(buffer, position, bxt03oUserid, Len.BXT03O_USERID);
		return buffer;
	}

	public void setBxt03oAccountNbr(String bxt03oAccountNbr) {
		this.bxt03oAccountNbr = Functions.subString(bxt03oAccountNbr, Len.BXT03O_ACCOUNT_NBR);
	}

	public String getBxt03oAccountNbr() {
		return this.bxt03oAccountNbr;
	}

	public void setBxt03oUserid(String bxt03oUserid) {
		this.bxt03oUserid = Functions.subString(bxt03oUserid, Len.BXT03O_USERID);
	}

	public String getBxt03oUserid() {
		return this.bxt03oUserid;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public Bxt03oAccountInfo getBxt03oAccountInfo() {
		return bxt03oAccountInfo;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BXT03I_ACCOUNT_NBR = 9;
		public static final int BXT03I_USERID = 8;
		public static final int BXT03I_TECHNICAL_KEY = BXT03I_ACCOUNT_NBR + BXT03I_USERID;
		public static final int BXT003_SERVICE_INPUTS = BXT03I_TECHNICAL_KEY;
		public static final int BXT03O_ACCOUNT_NBR = 9;
		public static final int BXT03O_USERID = 8;
		public static final int BXT03O_TECHNICAL_KEY = BXT03O_ACCOUNT_NBR + BXT03O_USERID;
		public static final int FLR1 = 100;
		public static final int BXT003_SERVICE_OUTPUTS = BXT03O_TECHNICAL_KEY + Bxt03oAccountInfo.Len.BXT03O_ACCOUNT_INFO + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
