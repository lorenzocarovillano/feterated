/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-05-DB2-ERROR-ON-SELECT<br>
 * Variable: EA-05-DB2-ERROR-ON-SELECT from program XZ0G0001<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea05Db2ErrorOnSelect {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-05-DB2-ERROR-ON-SELECT
	private String flr1 = "PGM =";
	//Original name: EA-05-PROGRAM-NAME
	private String programName = DefaultValues.stringVal(Len.PROGRAM_NAME);
	//Original name: FILLER-EA-05-DB2-ERROR-ON-SELECT-1
	private String flr2 = " PARA =";
	//Original name: EA-05-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-05-DB2-ERROR-ON-SELECT-2
	private String flr3 = " DB2 SELECT";
	//Original name: FILLER-EA-05-DB2-ERROR-ON-SELECT-3
	private String flr4 = "ERROR OCCURRED:";
	//Original name: FILLER-EA-05-DB2-ERROR-ON-SELECT-4
	private String flr5 = " SQLCODE =";
	//Original name: EA-05-SQLCODE
	private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);
	//Original name: FILLER-EA-05-DB2-ERROR-ON-SELECT-5
	private String flr6 = " & SQLSTATE =";
	//Original name: EA-05-SQLSTATE
	private String sqlstate = DefaultValues.stringVal(Len.SQLSTATE);

	//==== METHODS ====
	public String getEa05Db2ErrorOnSelectFormatted() {
		return MarshalByteExt.bufferToStr(getEa05Db2ErrorOnSelectBytes());
	}

	public byte[] getEa05Db2ErrorOnSelectBytes() {
		byte[] buffer = new byte[Len.EA05_DB2_ERROR_ON_SELECT];
		return getEa05Db2ErrorOnSelectBytes(buffer, 1);
	}

	public byte[] getEa05Db2ErrorOnSelectBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, programName, Len.PROGRAM_NAME);
		position += Len.PROGRAM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
		position += Len.SQLCODE;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, sqlstate, Len.SQLSTATE);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setProgramName(String programName) {
		this.programName = Functions.subString(programName, Len.PROGRAM_NAME);
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setSqlcode(long sqlcode) {
		this.sqlcode = PicFormatter.display("++(4)9").format(sqlcode).toString();
	}

	public long getSqlcode() {
		return PicParser.display("++(4)9").parseLong(this.sqlcode);
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setSqlstate(String sqlstate) {
		this.sqlstate = Functions.subString(sqlstate, Len.SQLSTATE);
	}

	public String getSqlstate() {
		return this.sqlstate;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int PARAGRAPH_NBR = 5;
		public static final int SQLCODE = 6;
		public static final int SQLSTATE = 5;
		public static final int FLR1 = 6;
		public static final int FLR2 = 8;
		public static final int FLR3 = 12;
		public static final int FLR4 = 15;
		public static final int FLR5 = 11;
		public static final int FLR6 = 14;
		public static final int EA05_DB2_ERROR_ON_SELECT = PROGRAM_NAME + PARAGRAPH_NBR + SQLCODE + SQLSTATE + FLR1 + FLR2 + FLR3 + FLR4 + FLR5
				+ FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
