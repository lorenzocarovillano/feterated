/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.federatedinsurance.crs.copy.Ivoryh;
import com.federatedinsurance.crs.copy.Xz009iServiceInputs;
import com.federatedinsurance.crs.copy.Xz009oTrsDtlRsp;
import com.federatedinsurance.crs.copy.Xzc090co;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZC09090<br>
 * Generated as a class for rule WS.<br>*/
public class Xzc09090Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXzc09090 constantFields = new ConstantFieldsXzc09090();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXzc09090 errorAndAdviceMessages = new ErrorAndAdviceMessagesXzc09090();
	//Original name: SAVE-AREA
	private SaveAreaXzc09090 saveArea = new SaveAreaXzc09090();
	//Original name: SS-ER
	private short ssEr = DefaultValues.BIN_SHORT_VAL;
	//Original name: URI-LKU-LINKAGE
	private Ts571cb1 ts571cb1 = new Ts571cb1();
	//Original name: IVORYH
	private Ivoryh ivoryh = new Ivoryh();
	//Original name: XZ009I-SERVICE-INPUTS
	private Xz009iServiceInputs xz009iServiceInputs = new Xz009iServiceInputs();
	//Original name: XZC090CO
	private Xzc090co xzc090co = new Xzc090co();

	//==== METHODS ====
	public void setSsEr(short ssEr) {
		this.ssEr = ssEr;
	}

	public short getSsEr() {
		return this.ssEr;
	}

	/**Original name: CALLABLE-INPUTS<br>*/
	public byte[] getCallableInputsBytes() {
		byte[] buffer = new byte[Len.CALLABLE_INPUTS];
		return getCallableInputsBytes(buffer, 1);
	}

	public byte[] getCallableInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xz009iServiceInputs.getXzc09iServiceInputsBytes(buffer, position);
		return buffer;
	}

	public void setCallableOutputsBytes(byte[] buffer) {
		setCallableOutputsBytes(buffer, 1);
	}

	/**Original name: CALLABLE-OUTPUTS<br>*/
	public byte[] getCallableOutputsBytes() {
		byte[] buffer = new byte[Len.CALLABLE_OUTPUTS];
		return getCallableOutputsBytes(buffer, 1);
	}

	public void setCallableOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc090co.getTrsDtlRsp().setXzc09oServiceOutputsBytes(buffer, position);
		position += Xz009oTrsDtlRsp.Len.XZC09O_SERVICE_OUTPUTS;
		xzc090co.setErrorInformationBytes(buffer, position);
	}

	public byte[] getCallableOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc090co.getTrsDtlRsp().getXzc09oServiceOutputsBytes(buffer, position);
		position += Xz009oTrsDtlRsp.Len.XZC09O_SERVICE_OUTPUTS;
		xzc090co.getErrorInformationBytes(buffer, position);
		return buffer;
	}

	public ConstantFieldsXzc09090 getConstantFields() {
		return constantFields;
	}

	public ErrorAndAdviceMessagesXzc09090 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public Ivoryh getIvoryh() {
		return ivoryh;
	}

	public SaveAreaXzc09090 getSaveArea() {
		return saveArea;
	}

	public Ts571cb1 getTs571cb1() {
		return ts571cb1;
	}

	public Xz009iServiceInputs getXz009iServiceInputs() {
		return xz009iServiceInputs;
	}

	public Xzc090co getXzc090co() {
		return xzc090co;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CALLABLE_INPUTS = Xz009iServiceInputs.Len.XZC09I_SERVICE_INPUTS;
		public static final int CALLABLE_OUTPUTS = Xz009oTrsDtlRsp.Len.XZC09O_SERVICE_OUTPUTS + Xzc090co.Len.ERROR_INFORMATION;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
