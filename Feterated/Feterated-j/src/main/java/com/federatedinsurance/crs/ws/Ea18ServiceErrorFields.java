/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-18-SERVICE-ERROR-FIELDS<br>
 * Variable: EA-18-SERVICE-ERROR-FIELDS from program XZ003000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea18ServiceErrorFields {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-18-SERVICE-ERROR-FIELDS
	private String flr1 = "ERROR CODE IS";
	//Original name: EA-18-SE-CODE
	private String code = DefaultValues.stringVal(Len.CODE);
	//Original name: FILLER-EA-18-SERVICE-ERROR-FIELDS-1
	private String flr2 = " MESSAGE IS";
	//Original name: EA-18-SE-MESSAGE
	private String message = DefaultValues.stringVal(Len.MESSAGE);

	//==== METHODS ====
	public String getEa18ServiceErrorFieldsFormatted() {
		return MarshalByteExt.bufferToStr(getEa18ServiceErrorFieldsBytes());
	}

	public byte[] getEa18ServiceErrorFieldsBytes() {
		byte[] buffer = new byte[Len.EA18_SERVICE_ERROR_FIELDS];
		return getEa18ServiceErrorFieldsBytes(buffer, 1);
	}

	public byte[] getEa18ServiceErrorFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, code, Len.CODE);
		position += Len.CODE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, message, Len.MESSAGE);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setCodeFormatted(String code) {
		this.code = Trunc.toUnsignedNumeric(code, Len.CODE);
	}

	public short getCode() {
		return NumericDisplay.asShort(this.code);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setMessage(String message) {
		this.message = Functions.subString(message, Len.MESSAGE);
	}

	public String getMessage() {
		return this.message;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODE = 4;
		public static final int MESSAGE = 100;
		public static final int FLR1 = 14;
		public static final int FLR2 = 12;
		public static final int EA18_SERVICE_ERROR_FIELDS = CODE + MESSAGE + FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
