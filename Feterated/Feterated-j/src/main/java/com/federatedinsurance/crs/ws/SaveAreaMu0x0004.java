/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaveAreaMu0x0004 {

	//==== PROPERTIES ====
	//Original name: SA-ADDRESS-1
	private String address1 = DefaultValues.stringVal(Len.ADDRESS1);
	//Original name: SA-ADDRESS-2
	private String address2 = DefaultValues.stringVal(Len.ADDRESS2);
	//Original name: SA-FORMATTED-FEIN
	private SaFormattedFein formattedFein = new SaFormattedFein();
	//Original name: SA-FORMATTED-SSN
	private SaFormattedSsn formattedSsn = new SaFormattedSsn();
	//Original name: SA-UNFORMATTED-ACT-NBR
	private String unformattedActNbr = DefaultValues.stringVal(Len.UNFORMATTED_ACT_NBR);
	//Original name: SA-FORMATTED-ACT-NBR
	private SaFormattedActNbr formattedActNbr = new SaFormattedActNbr();
	//Original name: SA-LST-NM
	private String lstNm = "";
	//Original name: SA-FST-NM
	private String fstNm = "";
	//Original name: SA-MDL-NM
	private String mdlNm = "";
	//Original name: SA-SFX
	private String sfx = "";
	//Original name: SA-DISPLAY-NAME
	private String displayName = "";
	//Original name: SA-SRT-NAME
	private String srtName = "";
	//Original name: SA-DSP-DELIMITERS
	private SaDspDelimiters dspDelimiters = new SaDspDelimiters();
	//Original name: SA-SRT-DELIMITERS
	private SaSrtDelimiters srtDelimiters = new SaSrtDelimiters();
	/**Original name: SA-FULL-COMPANY-NAME-1<br>
	 * <pre>* IF COMPANY NAME OVERFLOWED SAVE THE 2 PARTS INTO LARGER FIELDS
	 * * TO BE ABLE TO DELIMIT BY 2 SPACES WITHOUT LOSINGINFORMATION.</pre>*/
	private String fullCompanyName1 = DefaultValues.stringVal(Len.FULL_COMPANY_NAME1);
	//Original name: SA-FULL-COMPANY-NAME-2
	private String fullCompanyName2 = DefaultValues.stringVal(Len.FULL_COMPANY_NAME2);

	//==== METHODS ====
	public void setAddress1(String address1) {
		this.address1 = Functions.subString(address1, Len.ADDRESS1);
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress2(String address2) {
		this.address2 = Functions.subString(address2, Len.ADDRESS2);
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setUnformattedActNbr(String unformattedActNbr) {
		this.unformattedActNbr = Functions.subString(unformattedActNbr, Len.UNFORMATTED_ACT_NBR);
	}

	public String getUnformattedActNbr() {
		return this.unformattedActNbr;
	}

	public String getUnformattedActNbrFormatted() {
		return Functions.padBlanks(getUnformattedActNbr(), Len.UNFORMATTED_ACT_NBR);
	}

	public void setLstNm(String lstNm) {
		this.lstNm = Functions.subString(lstNm, Len.LST_NM);
	}

	public String getLstNm() {
		return this.lstNm;
	}

	public String getLstNmFormatted() {
		return Functions.padBlanks(getLstNm(), Len.LST_NM);
	}

	public void setFstNm(String fstNm) {
		this.fstNm = Functions.subString(fstNm, Len.FST_NM);
	}

	public String getFstNm() {
		return this.fstNm;
	}

	public String getFstNmFormatted() {
		return Functions.padBlanks(getFstNm(), Len.FST_NM);
	}

	public void setMdlNm(String mdlNm) {
		this.mdlNm = Functions.subString(mdlNm, Len.MDL_NM);
	}

	public String getMdlNm() {
		return this.mdlNm;
	}

	public String getMdlNmFormatted() {
		return Functions.padBlanks(getMdlNm(), Len.MDL_NM);
	}

	public void setSfx(String sfx) {
		this.sfx = Functions.subString(sfx, Len.SFX);
	}

	public String getSfx() {
		return this.sfx;
	}

	public String getSfxFormatted() {
		return Functions.padBlanks(getSfx(), Len.SFX);
	}

	public void setDisplayName(String displayName) {
		this.displayName = Functions.subString(displayName, Len.DISPLAY_NAME);
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public String getDisplayNameFormatted() {
		return Functions.padBlanks(getDisplayName(), Len.DISPLAY_NAME);
	}

	public void setSrtName(String srtName) {
		this.srtName = Functions.subString(srtName, Len.SRT_NAME);
	}

	public String getSrtName() {
		return this.srtName;
	}

	public String getSrtNameFormatted() {
		return Functions.padBlanks(getSrtName(), Len.SRT_NAME);
	}

	public void setFullCompanyName1(String fullCompanyName1) {
		this.fullCompanyName1 = Functions.subString(fullCompanyName1, Len.FULL_COMPANY_NAME1);
	}

	public String getFullCompanyName1() {
		return this.fullCompanyName1;
	}

	public String getFullCompanyName1Formatted() {
		return Functions.padBlanks(getFullCompanyName1(), Len.FULL_COMPANY_NAME1);
	}

	public void setFullCompanyName2(String fullCompanyName2) {
		this.fullCompanyName2 = Functions.subString(fullCompanyName2, Len.FULL_COMPANY_NAME2);
	}

	public String getFullCompanyName2() {
		return this.fullCompanyName2;
	}

	public String getFullCompanyName2Formatted() {
		return Functions.padBlanks(getFullCompanyName2(), Len.FULL_COMPANY_NAME2);
	}

	public SaDspDelimiters getDspDelimiters() {
		return dspDelimiters;
	}

	public SaFormattedActNbr getFormattedActNbr() {
		return formattedActNbr;
	}

	public SaFormattedFein getFormattedFein() {
		return formattedFein;
	}

	public SaFormattedSsn getFormattedSsn() {
		return formattedSsn;
	}

	public SaSrtDelimiters getSrtDelimiters() {
		return srtDelimiters;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ADDRESS1 = 45;
		public static final int ADDRESS2 = 45;
		public static final int UNFORMATTED_ACT_NBR = 7;
		public static final int FULL_COMPANY_NAME1 = 62;
		public static final int FULL_COMPANY_NAME2 = 47;
		public static final int FST_NM = 32;
		public static final int MDL_NM = 32;
		public static final int LST_NM = 62;
		public static final int SFX = 6;
		public static final int SRT_NAME = 30;
		public static final int DISPLAY_NAME = 120;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
