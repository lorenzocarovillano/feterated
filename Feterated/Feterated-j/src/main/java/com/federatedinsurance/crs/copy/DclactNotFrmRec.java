/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRec;

/**Original name: DCLACT-NOT-FRM-REC<br>
 * Variable: DCLACT-NOT-FRM-REC from copybook XZH00017<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclactNotFrmRec implements IActNotFrmRec {

	//==== PROPERTIES ====
	//Original name: CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FRM-SEQ-NBR
	private short frmSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: REC-SEQ-NBR
	private short recSeqNbr = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public String getXzh007ActNotFrmRecRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzh007ActNotFrmRecRowBytes());
	}

	public byte[] getXzh007ActNotFrmRecRowBytes() {
		byte[] buffer = new byte[Len.XZH007_ACT_NOT_FRM_REC_ROW];
		return getXzh007ActNotFrmRecRowBytes(buffer, 1);
	}

	public byte[] getXzh007ActNotFrmRecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeBinaryShort(buffer, position, frmSeqNbr);
		position += Types.SHORT_SIZE;
		MarshalByte.writeBinaryShort(buffer, position, recSeqNbr);
		return buffer;
	}

	public void initXzh007ActNotFrmRecRowSpaces() {
		csrActNbr = "";
		notPrcTs = "";
		frmSeqNbr = Types.INVALID_BINARY_SHORT_VAL;
		recSeqNbr = Types.INVALID_BINARY_SHORT_VAL;
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public void setFrmSeqNbr(short frmSeqNbr) {
		this.frmSeqNbr = frmSeqNbr;
	}

	@Override
	public short getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	@Override
	public void setRecSeqNbr(short recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	@Override
	public short getRecSeqNbr() {
		return this.recSeqNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FRM_SEQ_NBR = 2;
		public static final int REC_SEQ_NBR = 2;
		public static final int XZH007_ACT_NOT_FRM_REC_ROW = CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR + REC_SEQ_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
