/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WS-OPERATIONS-CALLED<br>
 * Variable: WS-OPERATIONS-CALLED from program XZ0P90E0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsOperationsCalledXz0p90e0 {

	//==== PROPERTIES ====
	//Original name: WS-ADD-ACT-NOT-FRM
	private String addActNotFrm = "AddAccountNotificationForm";
	//Original name: WS-ADD-ACT-NOT-FRM-REC
	private String addActNotFrmRec = "AddAccountNotificationFormRec";
	//Original name: WS-ADD-ACT-NOT-POL-FRM
	private String addActNotPolFrm = "AddAccountNotificationPolFrm";
	//Original name: WS-GET-POLICY-LIST
	private String getPolicyList = "GetPolicyListByNotification";
	//Original name: WS-GET-ACT-BILL-DETAIL
	private String getActBillDetail = "GetActBillDetail";

	//==== METHODS ====
	public String getAddActNotFrm() {
		return this.addActNotFrm;
	}

	public String getAddActNotFrmRec() {
		return this.addActNotFrmRec;
	}

	public String getAddActNotPolFrm() {
		return this.addActNotPolFrm;
	}

	public String getGetPolicyList() {
		return this.getPolicyList;
	}

	public String getGetActBillDetail() {
		return this.getActBillDetail;
	}
}
