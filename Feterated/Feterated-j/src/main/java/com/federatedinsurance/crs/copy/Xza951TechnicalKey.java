/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZA951-TECHNICAL-KEY<br>
 * Variable: XZA951-TECHNICAL-KEY from copybook XZ0A9051<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xza951TechnicalKey {

	//==== PROPERTIES ====
	//Original name: XZA951-NIN-CLT-ID
	private String ninCltId = DefaultValues.stringVal(Len.NIN_CLT_ID);
	//Original name: XZA951-NIN-ADR-ID
	private String ninAdrId = DefaultValues.stringVal(Len.NIN_ADR_ID);
	//Original name: XZA951-WF-STARTED-IND
	private char wfStartedInd = DefaultValues.CHAR_VAL;
	//Original name: XZA951-POL-BIL-STA-CD
	private char polBilStaCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		ninCltId = MarshalByte.readString(buffer, position, Len.NIN_CLT_ID);
		position += Len.NIN_CLT_ID;
		ninAdrId = MarshalByte.readString(buffer, position, Len.NIN_ADR_ID);
		position += Len.NIN_ADR_ID;
		wfStartedInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polBilStaCd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ninCltId, Len.NIN_CLT_ID);
		position += Len.NIN_CLT_ID;
		MarshalByte.writeString(buffer, position, ninAdrId, Len.NIN_ADR_ID);
		position += Len.NIN_ADR_ID;
		MarshalByte.writeChar(buffer, position, wfStartedInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polBilStaCd);
		return buffer;
	}

	public void setNinCltId(String ninCltId) {
		this.ninCltId = Functions.subString(ninCltId, Len.NIN_CLT_ID);
	}

	public String getNinCltId() {
		return this.ninCltId;
	}

	public void setNinAdrId(String ninAdrId) {
		this.ninAdrId = Functions.subString(ninAdrId, Len.NIN_ADR_ID);
	}

	public String getNinAdrId() {
		return this.ninAdrId;
	}

	public void setWfStartedInd(char wfStartedInd) {
		this.wfStartedInd = wfStartedInd;
	}

	public char getWfStartedInd() {
		return this.wfStartedInd;
	}

	public void setPolBilStaCd(char polBilStaCd) {
		this.polBilStaCd = polBilStaCd;
	}

	public char getPolBilStaCd() {
		return this.polBilStaCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NIN_CLT_ID = 64;
		public static final int NIN_ADR_ID = 64;
		public static final int WF_STARTED_IND = 1;
		public static final int POL_BIL_STA_CD = 1;
		public static final int TECHNICAL_KEY = NIN_CLT_ID + NIN_ADR_ID + WF_STARTED_IND + POL_BIL_STA_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
