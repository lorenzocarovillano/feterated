/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WS-OPERATIONS-CALLED<br>
 * Variable: WS-OPERATIONS-CALLED from program XZ0P9000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsOperationsCalledXz0p9000 {

	//==== PROPERTIES ====
	//Original name: WS-GET-POLICY-LIST
	private String getPolicyList = "GetPolicyListByNotification";
	//Original name: WS-GET-POLICY-DETAIL-BASIC
	private String getPolicyDetailBasic = "GetPolDtlBasic";
	//Original name: WS-ADD-ACT-NOT-WRD
	private String addActNotWrd = "AddAccountNotificationWording";

	//==== METHODS ====
	public String getGetPolicyList() {
		return this.getPolicyList;
	}

	public String getGetPolicyDetailBasic() {
		return this.getPolicyDetailBasic;
	}

	public String getAddActNotWrd() {
		return this.addActNotWrd;
	}
}
