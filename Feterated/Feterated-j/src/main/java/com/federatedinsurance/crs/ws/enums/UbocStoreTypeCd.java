/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UBOC-STORE-TYPE-CD<br>
 * Variable: UBOC-STORE-TYPE-CD from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocStoreTypeCd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char UMT = 'U';
	public static final char LINKAGE = 'L';
	public static final char VSAM = 'V';

	//==== METHODS ====
	public void setUbocStoreTypeCd(char ubocStoreTypeCd) {
		this.value = ubocStoreTypeCd;
	}

	public char getUbocStoreTypeCd() {
		return this.value;
	}

	public boolean isUbocStoreTypeUmt() {
		return value == UMT;
	}

	public void setUbocStoreTypeVsam() {
		value = VSAM;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_STORE_TYPE_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
