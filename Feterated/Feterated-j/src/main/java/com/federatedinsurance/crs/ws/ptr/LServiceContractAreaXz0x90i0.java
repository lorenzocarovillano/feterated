/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90I0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90i0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int IO_FRM_REC_LIST_MAXOCCURS = 10000;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90i0() {
	}

	public LServiceContractAreaXz0x90i0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setIiTkNotPrcTs(String iiTkNotPrcTs) {
		writeString(Pos.II_TK_NOT_PRC_TS, iiTkNotPrcTs, Len.II_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9II-TK-NOT-PRC-TS<br>*/
	public String getIiTkNotPrcTs() {
		return readString(Pos.II_TK_NOT_PRC_TS, Len.II_TK_NOT_PRC_TS);
	}

	public void setIiCsrActNbr(String iiCsrActNbr) {
		writeString(Pos.II_CSR_ACT_NBR, iiCsrActNbr, Len.II_CSR_ACT_NBR);
	}

	/**Original name: XZT9II-CSR-ACT-NBR<br>*/
	public String getIiCsrActNbr() {
		return readString(Pos.II_CSR_ACT_NBR, Len.II_CSR_ACT_NBR);
	}

	public void setIiUserid(String iiUserid) {
		writeString(Pos.II_USERID, iiUserid, Len.II_USERID);
	}

	/**Original name: XZT9II-USERID<br>*/
	public String getIiUserid() {
		return readString(Pos.II_USERID, Len.II_USERID);
	}

	public String getIiUseridFormatted() {
		return Functions.padBlanks(getIiUserid(), Len.II_USERID);
	}

	public void setIoTkNotPrcTs(String ioTkNotPrcTs) {
		writeString(Pos.IO_TK_NOT_PRC_TS, ioTkNotPrcTs, Len.IO_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9IO-TK-NOT-PRC-TS<br>*/
	public String getIoTkNotPrcTs() {
		return readString(Pos.IO_TK_NOT_PRC_TS, Len.IO_TK_NOT_PRC_TS);
	}

	public void setIoCsrActNbr(String ioCsrActNbr) {
		writeString(Pos.IO_CSR_ACT_NBR, ioCsrActNbr, Len.IO_CSR_ACT_NBR);
	}

	/**Original name: XZT9IO-CSR-ACT-NBR<br>*/
	public String getIoCsrActNbr() {
		return readString(Pos.IO_CSR_ACT_NBR, Len.IO_CSR_ACT_NBR);
	}

	public void setIoTkFrmSeqNbr(int ioTkFrmSeqNbrIdx, int ioTkFrmSeqNbr) {
		int position = Pos.xzt9ioTkFrmSeqNbr(ioTkFrmSeqNbrIdx - 1);
		writeInt(position, ioTkFrmSeqNbr, Len.Int.IO_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT9IO-TK-FRM-SEQ-NBR<br>*/
	public int getIoTkFrmSeqNbr(int ioTkFrmSeqNbrIdx) {
		int position = Pos.xzt9ioTkFrmSeqNbr(ioTkFrmSeqNbrIdx - 1);
		return readNumDispInt(position, Len.IO_TK_FRM_SEQ_NBR);
	}

	public void setIoTkRecSeqNbr(int ioTkRecSeqNbrIdx, int ioTkRecSeqNbr) {
		int position = Pos.xzt9ioTkRecSeqNbr(ioTkRecSeqNbrIdx - 1);
		writeInt(position, ioTkRecSeqNbr, Len.Int.IO_TK_REC_SEQ_NBR);
	}

	/**Original name: XZT9IO-TK-REC-SEQ-NBR<br>*/
	public int getIoTkRecSeqNbr(int ioTkRecSeqNbrIdx) {
		int position = Pos.xzt9ioTkRecSeqNbr(ioTkRecSeqNbrIdx - 1);
		return readNumDispInt(position, Len.IO_TK_REC_SEQ_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT90I_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int II_TECHNICAL_KEY = XZT90I_SERVICE_INPUTS;
		public static final int II_TK_NOT_PRC_TS = II_TECHNICAL_KEY;
		public static final int II_CSR_ACT_NBR = II_TK_NOT_PRC_TS + Len.II_TK_NOT_PRC_TS;
		public static final int II_USERID = II_CSR_ACT_NBR + Len.II_CSR_ACT_NBR;
		public static final int XZT90I_SERVICE_OUTPUTS = II_USERID + Len.II_USERID;
		public static final int IO_TECHNICAL_KEY = XZT90I_SERVICE_OUTPUTS;
		public static final int IO_TK_NOT_PRC_TS = IO_TECHNICAL_KEY;
		public static final int IO_CSR_ACT_NBR = IO_TK_NOT_PRC_TS + Len.IO_TK_NOT_PRC_TS;
		public static final int IO_TABLE_OF_FRM_RECS = IO_CSR_ACT_NBR + Len.IO_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt9ioFrmRecList(int idx) {
			return IO_TABLE_OF_FRM_RECS + idx * Len.IO_FRM_REC_LIST;
		}

		public static int xzt9ioFrmRecTechnicalKey(int idx) {
			return xzt9ioFrmRecList(idx);
		}

		public static int xzt9ioTkFrmSeqNbr(int idx) {
			return xzt9ioFrmRecTechnicalKey(idx);
		}

		public static int xzt9ioTkRecSeqNbr(int idx) {
			return xzt9ioTkFrmSeqNbr(idx) + Len.IO_TK_FRM_SEQ_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int II_TK_NOT_PRC_TS = 26;
		public static final int II_CSR_ACT_NBR = 9;
		public static final int II_USERID = 8;
		public static final int IO_TK_NOT_PRC_TS = 26;
		public static final int IO_CSR_ACT_NBR = 9;
		public static final int IO_TK_FRM_SEQ_NBR = 5;
		public static final int IO_TK_REC_SEQ_NBR = 5;
		public static final int IO_FRM_REC_TECHNICAL_KEY = IO_TK_FRM_SEQ_NBR + IO_TK_REC_SEQ_NBR;
		public static final int IO_FRM_REC_LIST = IO_FRM_REC_TECHNICAL_KEY;
		public static final int II_TECHNICAL_KEY = II_TK_NOT_PRC_TS;
		public static final int XZT90I_SERVICE_INPUTS = II_TECHNICAL_KEY + II_CSR_ACT_NBR + II_USERID;
		public static final int IO_TECHNICAL_KEY = IO_TK_NOT_PRC_TS;
		public static final int IO_TABLE_OF_FRM_RECS = LServiceContractAreaXz0x90i0.IO_FRM_REC_LIST_MAXOCCURS * IO_FRM_REC_LIST;
		public static final int XZT90I_SERVICE_OUTPUTS = IO_TECHNICAL_KEY + IO_CSR_ACT_NBR + IO_TABLE_OF_FRM_RECS;
		public static final int L_SERVICE_CONTRACT_AREA = XZT90I_SERVICE_INPUTS + XZT90I_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int IO_TK_FRM_SEQ_NBR = 5;
			public static final int IO_TK_REC_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
