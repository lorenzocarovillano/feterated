/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0B90P0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0b90p0 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-CERT-POL-ROWS
	private short maxCertPolRows = ((short) 50);
	//Original name: CF-MAX-EXT-MSG
	private short maxExtMsg = ((short) 10);
	//Original name: CF-SP-CERT-POL-LIS-PGM
	private String spCertPolLisPgm = "XZC03090";

	//==== METHODS ====
	public short getMaxCertPolRows() {
		return this.maxCertPolRows;
	}

	public short getMaxExtMsg() {
		return this.maxExtMsg;
	}

	public String getSpCertPolLisPgm() {
		return this.spCertPolLisPgm;
	}
}
