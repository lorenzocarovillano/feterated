/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-NOTHING-FOUND-MSG<br>
 * Variable: EA-01-NOTHING-FOUND-MSG from program XZ0B90I0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01NothingFoundMsgXz0b90i0 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG
	private String flr1 = "No";
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG-1
	private String flr2 = "ACT_NOT_FRM_REC";
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG-2
	private String flr3 = " records";
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG-3
	private String flr4 = "found for";
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG-4
	private String flr5 = "search criteria";
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG-5
	private String flr6 = " entered:";
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG-6
	private String flr7 = "Account =";
	//Original name: EA-01-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG-7
	private String flr8 = "; Notification";
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG-8
	private String flr9 = "TimeStamp =";
	//Original name: EA-01-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-01-NOTHING-FOUND-MSG-9
	private char flr10 = '.';

	//==== METHODS ====
	public String getEa01NothingFoundMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01NothingFoundMsgBytes());
	}

	public byte[] getEa01NothingFoundMsgBytes() {
		byte[] buffer = new byte[Len.EA01_NOTHING_FOUND_MSG];
		return getEa01NothingFoundMsgBytes(buffer, 1);
	}

	public byte[] getEa01NothingFoundMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR9);
		position += Len.FLR9;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr10);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr10() {
		return this.flr10;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 3;
		public static final int FLR2 = 15;
		public static final int FLR3 = 9;
		public static final int FLR4 = 10;
		public static final int FLR9 = 12;
		public static final int FLR10 = 1;
		public static final int EA01_NOTHING_FOUND_MSG = CSR_ACT_NBR + NOT_PRC_TS + FLR10 + FLR1 + 3 * FLR2 + FLR3 + 3 * FLR4 + FLR9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
