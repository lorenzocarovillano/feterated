/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program CAWI048<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsCawi048 {

	//==== PROPERTIES ====
	//Original name: CF-REQUEST-UMT-MODULE
	private String requestUmtModule = "TS020100";
	//Original name: CF-RESPONSE-UMT-MODULE
	private String responseUmtModule = "TS020200";
	//Original name: CF-TOB
	private String tob = "TOB";

	//==== METHODS ====
	public String getRequestUmtModule() {
		return this.requestUmtModule;
	}

	public String getResponseUmtModule() {
		return this.responseUmtModule;
	}

	public String getTob() {
		return this.tob;
	}
}
