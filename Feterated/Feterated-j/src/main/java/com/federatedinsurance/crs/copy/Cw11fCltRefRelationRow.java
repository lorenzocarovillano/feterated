/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW11F-CLT-REF-RELATION-ROW<br>
 * Variable: CW11F-CLT-REF-RELATION-ROW from copybook CAWLF011<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw11fCltRefRelationRow {

	//==== PROPERTIES ====
	//Original name: CW11F-CLT-REF-RELATION-CSUM
	private String cltRefRelationCsum = DefaultValues.stringVal(Len.CLT_REF_RELATION_CSUM);
	//Original name: CW11F-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CW11F-CIRF-REF-SEQ-NBR-KCRE
	private String cirfRefSeqNbrKcre = DefaultValues.stringVal(Len.CIRF_REF_SEQ_NBR_KCRE);
	//Original name: CW11F-HISTORY-VLD-NBR-KCRE
	private String historyVldNbrKcre = DefaultValues.stringVal(Len.HISTORY_VLD_NBR_KCRE);
	//Original name: CW11F-CIRF-EFF-DT-KCRE
	private String cirfEffDtKcre = DefaultValues.stringVal(Len.CIRF_EFF_DT_KCRE);
	//Original name: CW11F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW11F-CLT-REF-RELATION-KEY
	private Cw01fBusinessClientKey cltRefRelationKey = new Cw01fBusinessClientKey();
	//Original name: CW11F-CLT-REF-RELATION-DATA
	private Cw11fCltRefRelationData cltRefRelationData = new Cw11fCltRefRelationData();

	//==== METHODS ====
	public void setCw11fCltRefRelationRowFormatted(String data) {
		byte[] buffer = new byte[Len.CW11F_CLT_REF_RELATION_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CW11F_CLT_REF_RELATION_ROW);
		setCw11fCltRefRelationRowBytes(buffer, 1);
	}

	public String getCw11fCltRefRelationRowFormatted() {
		return MarshalByteExt.bufferToStr(getCw11fCltRefRelationRowBytes());
	}

	public byte[] getCw11fCltRefRelationRowBytes() {
		byte[] buffer = new byte[Len.CW11F_CLT_REF_RELATION_ROW];
		return getCw11fCltRefRelationRowBytes(buffer, 1);
	}

	public void setCw11fCltRefRelationRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setCltRefRelationFixedBytes(buffer, position);
		position += Len.CLT_REF_RELATION_FIXED;
		setCltRefRelationDatesBytes(buffer, position);
		position += Len.CLT_REF_RELATION_DATES;
		cltRefRelationKey.setBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		cltRefRelationData.setCltRefRelationDataBytes(buffer, position);
	}

	public byte[] getCw11fCltRefRelationRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getCltRefRelationFixedBytes(buffer, position);
		position += Len.CLT_REF_RELATION_FIXED;
		getCltRefRelationDatesBytes(buffer, position);
		position += Len.CLT_REF_RELATION_DATES;
		cltRefRelationKey.getBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		cltRefRelationData.getCltRefRelationDataBytes(buffer, position);
		return buffer;
	}

	public void setCltRefRelationFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		cltRefRelationCsum = MarshalByte.readFixedString(buffer, position, Len.CLT_REF_RELATION_CSUM);
		position += Len.CLT_REF_RELATION_CSUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		cirfRefSeqNbrKcre = MarshalByte.readString(buffer, position, Len.CIRF_REF_SEQ_NBR_KCRE);
		position += Len.CIRF_REF_SEQ_NBR_KCRE;
		historyVldNbrKcre = MarshalByte.readString(buffer, position, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		cirfEffDtKcre = MarshalByte.readString(buffer, position, Len.CIRF_EFF_DT_KCRE);
	}

	public byte[] getCltRefRelationFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cltRefRelationCsum, Len.CLT_REF_RELATION_CSUM);
		position += Len.CLT_REF_RELATION_CSUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		MarshalByte.writeString(buffer, position, cirfRefSeqNbrKcre, Len.CIRF_REF_SEQ_NBR_KCRE);
		position += Len.CIRF_REF_SEQ_NBR_KCRE;
		MarshalByte.writeString(buffer, position, historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		MarshalByte.writeString(buffer, position, cirfEffDtKcre, Len.CIRF_EFF_DT_KCRE);
		return buffer;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setCirfRefSeqNbrKcre(String cirfRefSeqNbrKcre) {
		this.cirfRefSeqNbrKcre = Functions.subString(cirfRefSeqNbrKcre, Len.CIRF_REF_SEQ_NBR_KCRE);
	}

	public String getCirfRefSeqNbrKcre() {
		return this.cirfRefSeqNbrKcre;
	}

	public void setHistoryVldNbrKcre(String historyVldNbrKcre) {
		this.historyVldNbrKcre = Functions.subString(historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
	}

	public String getHistoryVldNbrKcre() {
		return this.historyVldNbrKcre;
	}

	public void setCirfEffDtKcre(String cirfEffDtKcre) {
		this.cirfEffDtKcre = Functions.subString(cirfEffDtKcre, Len.CIRF_EFF_DT_KCRE);
	}

	public String getCirfEffDtKcre() {
		return this.cirfEffDtKcre;
	}

	public void setCltRefRelationDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getCltRefRelationDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLT_REF_RELATION_CSUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int CIRF_REF_SEQ_NBR_KCRE = 32;
		public static final int HISTORY_VLD_NBR_KCRE = 32;
		public static final int CIRF_EFF_DT_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLT_REF_RELATION_FIXED = CLT_REF_RELATION_CSUM + CLIENT_ID_KCRE + CIRF_REF_SEQ_NBR_KCRE + HISTORY_VLD_NBR_KCRE
				+ CIRF_EFF_DT_KCRE;
		public static final int CLT_REF_RELATION_DATES = TRANS_PROCESS_DT;
		public static final int CW11F_CLT_REF_RELATION_ROW = CLT_REF_RELATION_FIXED + CLT_REF_RELATION_DATES
				+ Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY + Cw11fCltRefRelationData.Len.CLT_REF_RELATION_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
