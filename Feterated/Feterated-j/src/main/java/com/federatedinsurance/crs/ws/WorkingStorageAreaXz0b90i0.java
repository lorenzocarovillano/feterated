/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0B90I0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0b90i0 {

	//==== PROPERTIES ====
	//Original name: WS-ROW-COUNT
	private int rowCount = 0;
	public static final int NOTHING_FOUND = 0;
	//Original name: WS-MAX-ROWS
	private String maxRows = DefaultValues.stringVal(Len.MAX_ROWS);
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0B90I0";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-LIST-KEY
	private String busObjNmListKey = "XZ_GET_FRM_REC_LIST_KEY";
	//Original name: WS-BUS-OBJ-NM-LIST-DETAIL
	private String busObjNmListDetail = "XZ_GET_FRM_REC_LIST_DETAIL";

	//==== METHODS ====
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public int getRowCount() {
		return this.rowCount;
	}

	public boolean isNothingFound() {
		return rowCount == NOTHING_FOUND;
	}

	public void setNothingFound() {
		rowCount = NOTHING_FOUND;
	}

	public void setMaxRows(long maxRows) {
		this.maxRows = PicFormatter.display("Z(8)9").format(maxRows).toString();
	}

	public long getMaxRows() {
		return PicParser.display("Z(8)9").parseLong(this.maxRows);
	}

	public String getMaxRowsFormatted() {
		return this.maxRows;
	}

	public String getMaxRowsAsString() {
		return getMaxRowsFormatted();
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNmListKey() {
		return this.busObjNmListKey;
	}

	public String getBusObjNmListDetail() {
		return this.busObjNmListDetail;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_ROWS = 9;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
