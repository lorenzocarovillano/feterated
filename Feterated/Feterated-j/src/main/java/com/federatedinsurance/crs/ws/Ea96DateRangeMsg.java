/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-96-DATE-RANGE-MSG<br>
 * Variable: EA-96-DATE-RANGE-MSG from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea96DateRangeMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-96-DATE-RANGE-MSG
	private String flr1 = " PGM = XZ004000";
	//Original name: FILLER-EA-96-DATE-RANGE-MSG-1
	private String flr2 = " -";
	//Original name: FILLER-EA-96-DATE-RANGE-MSG-2
	private String flr3 = "(INFORMATIONAL)";
	//Original name: FILLER-EA-96-DATE-RANGE-MSG-3
	private String flr4 = " USING DATE :";
	//Original name: EA-96-DATE-START
	private String ea96DateStart = DefaultValues.stringVal(Len.EA96_DATE_START);

	//==== METHODS ====
	public String getEa96DateRangeMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa96DateRangeMsgBytes());
	}

	public byte[] getEa96DateRangeMsgBytes() {
		byte[] buffer = new byte[Len.EA96_DATE_RANGE_MSG];
		return getEa96DateRangeMsgBytes(buffer, 1);
	}

	public byte[] getEa96DateRangeMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, ea96DateStart, Len.EA96_DATE_START);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setEa96DateStart(String ea96DateStart) {
		this.ea96DateStart = Functions.subString(ea96DateStart, Len.EA96_DATE_START);
	}

	public String getEa96DateStart() {
		return this.ea96DateStart;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA96_DATE_START = 26;
		public static final int FLR1 = 15;
		public static final int FLR2 = 3;
		public static final int FLR4 = 14;
		public static final int EA96_DATE_RANGE_MSG = EA96_DATE_START + 2 * FLR1 + FLR2 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
