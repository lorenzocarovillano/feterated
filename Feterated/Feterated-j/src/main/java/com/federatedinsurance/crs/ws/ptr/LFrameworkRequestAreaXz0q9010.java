/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q9010<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q9010 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZA910_POL_NBR_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q9010() {
	}

	public LFrameworkRequestAreaXz0q9010(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza910qMaxWrdRows(short xza910qMaxWrdRows) {
		writeBinaryShort(Pos.XZA910_MAX_WRD_ROWS, xza910qMaxWrdRows);
	}

	/**Original name: XZA910Q-MAX-WRD-ROWS<br>*/
	public short getXza910qMaxWrdRows() {
		return readBinaryShort(Pos.XZA910_MAX_WRD_ROWS);
	}

	public void setXza910qCsrActNbr(String xza910qCsrActNbr) {
		writeString(Pos.XZA910_CSR_ACT_NBR, xza910qCsrActNbr, Len.XZA910_CSR_ACT_NBR);
	}

	/**Original name: XZA910Q-CSR-ACT-NBR<br>*/
	public String getXza910qCsrActNbr() {
		return readString(Pos.XZA910_CSR_ACT_NBR, Len.XZA910_CSR_ACT_NBR);
	}

	public void setXza910qNotPrcTs(String xza910qNotPrcTs) {
		writeString(Pos.XZA910_NOT_PRC_TS, xza910qNotPrcTs, Len.XZA910_NOT_PRC_TS);
	}

	/**Original name: XZA910Q-NOT-PRC-TS<br>*/
	public String getXza910qNotPrcTs() {
		return readString(Pos.XZA910_NOT_PRC_TS, Len.XZA910_NOT_PRC_TS);
	}

	public void setXza910qPolListBytes(byte[] buffer) {
		setXza910qPolListBytes(buffer, 1);
	}

	public void setXza910qPolListBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.XZA910_POL_LIST, Pos.XZA910_POL_LIST);
	}

	public void setXza910qPolNbr(int xza910qPolNbrIdx, String xza910qPolNbr) {
		int position = Pos.xza910PolNbr(xza910qPolNbrIdx - 1);
		writeString(position, xza910qPolNbr, Len.XZA910_POL_NBR);
	}

	/**Original name: XZA910Q-POL-NBR<br>*/
	public String getXza910qPolNbr(int xza910qPolNbrIdx) {
		int position = Pos.xza910PolNbr(xza910qPolNbrIdx - 1);
		return readString(position, Len.XZA910_POL_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A9010 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA910_ACT_NOT_WRD_ROW = L_FW_REQ_XZ0A9010;
		public static final int XZA910_MAX_WRD_ROWS = XZA910_ACT_NOT_WRD_ROW;
		public static final int XZA910_CSR_ACT_NBR = XZA910_MAX_WRD_ROWS + Len.XZA910_MAX_WRD_ROWS;
		public static final int XZA910_NOT_PRC_TS = XZA910_CSR_ACT_NBR + Len.XZA910_CSR_ACT_NBR;
		public static final int XZA910_POL_LIST = XZA910_NOT_PRC_TS + Len.XZA910_NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xza910PolNbr(int idx) {
			return XZA910_POL_LIST + idx * Len.XZA910_POL_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA910_MAX_WRD_ROWS = 2;
		public static final int XZA910_CSR_ACT_NBR = 9;
		public static final int XZA910_NOT_PRC_TS = 26;
		public static final int XZA910_POL_NBR = 25;
		public static final int XZA910_POL_LIST = LFrameworkRequestAreaXz0q9010.XZA910_POL_NBR_MAXOCCURS * XZA910_POL_NBR;
		public static final int XZA910_ACT_NOT_WRD_ROW = XZA910_MAX_WRD_ROWS + XZA910_CSR_ACT_NBR + XZA910_NOT_PRC_TS + XZA910_POL_LIST;
		public static final int L_FW_REQ_XZ0A9010 = XZA910_ACT_NOT_WRD_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A9010;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
