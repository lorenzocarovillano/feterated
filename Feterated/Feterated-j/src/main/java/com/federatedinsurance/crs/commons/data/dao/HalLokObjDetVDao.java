/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalLokObjDetV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_LOK_OBJ_DET_V]
 * 
 */
public class HalLokObjDetVDao extends BaseSqlDao<IHalLokObjDetV> {

	private Cursor wsCursor12;
	private Cursor wsCursor2;
	private Cursor wsCursor3;
	private final IRowMapper<IHalLokObjDetV> fetchWsCursor12Rm = buildNamedRowMapper(IHalLokObjDetV.class, "sessionId", "tchKey", "appId");
	private final IRowMapper<IHalLokObjDetV> selectRecRm = buildNamedRowMapper(IHalLokObjDetV.class, "userid", "zappedInd", "lokTypeCd", "updByMsgId",
			"zappedBy", "zappedTs");
	private final IRowMapper<IHalLokObjDetV> selectRec1Rm = buildNamedRowMapper(IHalLokObjDetV.class, "userid", "lokTypeCd", "zappedInd", "zappedBy");
	private final IRowMapper<IHalLokObjDetV> fetchWsCursor2Rm = buildNamedRowMapper(IHalLokObjDetV.class, "sessionId", "tchKey", "appId", "tableNm",
			"userId", "lokTypeCd");

	public HalLokObjDetVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalLokObjDetV> getToClass() {
		return IHalLokObjDetV.class;
	}

	public DbAccessStatus insertRec(IHalLokObjDetV iHalLokObjDetV) {
		return buildQuery("insertRec").bind(iHalLokObjDetV).executeInsert();
	}

	public DbAccessStatus updateRec(short wsTmoValue, String hlodSessionId, String hlodTchKey, String hlodAppId) {
		return buildQuery("updateRec").bind("wsTmoValue", wsTmoValue).bind("hlodSessionId", hlodSessionId).bind("hlodTchKey", hlodTchKey)
				.bind("hlodAppId", hlodAppId).executeUpdate();
	}

	public DbAccessStatus updateRec1(IHalLokObjDetV iHalLokObjDetV) {
		return buildQuery("updateRec1").bind(iHalLokObjDetV).executeUpdate();
	}

	public DbAccessStatus openWsCursor12(String hlodTchKey) {
		wsCursor12 = buildQuery("openWsCursor12").bind("hlodTchKey", hlodTchKey).open();
		return dbStatus;
	}

	public DbAccessStatus closeWsCursor12() {
		return closeCursor(wsCursor12);
	}

	public IHalLokObjDetV fetchWsCursor12(IHalLokObjDetV iHalLokObjDetV) {
		return fetch(wsCursor12, iHalLokObjDetV, fetchWsCursor12Rm);
	}

	public DbAccessStatus updateRec2(IHalLokObjDetV iHalLokObjDetV) {
		return buildQuery("updateRec2").bind(iHalLokObjDetV).executeUpdate();
	}

	public IHalLokObjDetV selectRec(String sessionId, String tchKey, String appId, IHalLokObjDetV iHalLokObjDetV) {
		return buildQuery("selectRec").bind("sessionId", sessionId).bind("tchKey", tchKey).bind("appId", appId).rowMapper(selectRecRm)
				.singleResult(iHalLokObjDetV);
	}

	public DbAccessStatus updateRec3(IHalLokObjDetV iHalLokObjDetV) {
		return buildQuery("updateRec3").bind(iHalLokObjDetV).executeUpdate();
	}

	public IHalLokObjDetV selectRec1(String sessionId, String tchKey, String appId, IHalLokObjDetV iHalLokObjDetV) {
		return buildQuery("selectRec1").bind("sessionId", sessionId).bind("tchKey", tchKey).bind("appId", appId).rowMapper(selectRec1Rm)
				.singleResult(iHalLokObjDetV);
	}

	public DbAccessStatus openWsCursor2(String hlodSessionId) {
		wsCursor2 = buildQuery("openWsCursor2").bind("hlodSessionId", hlodSessionId).open();
		return dbStatus;
	}

	public DbAccessStatus closeWsCursor2() {
		return closeCursor(wsCursor2);
	}

	public IHalLokObjDetV fetchWsCursor2(IHalLokObjDetV iHalLokObjDetV) {
		return fetch(wsCursor2, iHalLokObjDetV, fetchWsCursor2Rm);
	}

	public DbAccessStatus deleteRec(String sessionId, String tchKey, String appId, String tableNm) {
		return buildQuery("deleteRec").bind("sessionId", sessionId).bind("tchKey", tchKey).bind("appId", appId).bind("tableNm", tableNm)
				.executeDelete();
	}

	public long selectRec2(String tchKey, String appId, String tableNm, long dft) {
		return buildQuery("selectRec2").bind("tchKey", tchKey).bind("appId", appId).bind("tableNm", tableNm).scalarResultLong(dft);
	}

	public String selectRec3(IHalLokObjDetV iHalLokObjDetV, String dft) {
		return buildQuery("selectRec3").bind(iHalLokObjDetV).scalarResultString(dft);
	}

	public DbAccessStatus deleteRec1(String tchKey, String appId, String tableNm, String userid) {
		return buildQuery("deleteRec1").bind("tchKey", tchKey).bind("appId", appId).bind("tableNm", tableNm).bind("userid", userid).executeDelete();
	}

	public DbAccessStatus openWsCursor3(String tchKey, String appId) {
		wsCursor3 = buildQuery("openWsCursor3").bind("tchKey", tchKey).bind("appId", appId).open();
		return dbStatus;
	}

	public DbAccessStatus closeWsCursor3() {
		return closeCursor(wsCursor3);
	}

	public IHalLokObjDetV fetchWsCursor3(IHalLokObjDetV iHalLokObjDetV) {
		return fetch(wsCursor3, iHalLokObjDetV, fetchWsCursor12Rm);
	}

	public DbAccessStatus updateRec4(IHalLokObjDetV iHalLokObjDetV) {
		return buildQuery("updateRec4").bind(iHalLokObjDetV).executeUpdate();
	}

	public DbAccessStatus updateRec5(IHalLokObjDetV iHalLokObjDetV) {
		return buildQuery("updateRec5").bind(iHalLokObjDetV).executeUpdate();
	}
}
