/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HOLIDAY_PRC]
 * 
 */
public interface IHolidayPrc extends BaseSqlTo {

	/**
	 * Host Variable CTR-CD
	 * 
	 */
	String getCtrCd();

	void setCtrCd(String ctrCd);

	/**
	 * Host Variable STATE-CD
	 * 
	 */
	String getStateCd();

	void setStateCd(String stateCd);

	/**
	 * Host Variable APP-01-XCL-CD
	 * 
	 */
	String getApp01XclCd();

	void setApp01XclCd(String app01XclCd);

	/**
	 * Host Variable APP-02-XCL-CD
	 * 
	 */
	String getApp02XclCd();

	void setApp02XclCd(String app02XclCd);

	/**
	 * Host Variable APP-03-XCL-CD
	 * 
	 */
	String getApp03XclCd();

	void setApp03XclCd(String app03XclCd);

	/**
	 * Host Variable APP-04-XCL-CD
	 * 
	 */
	String getApp04XclCd();

	void setApp04XclCd(String app04XclCd);

	/**
	 * Host Variable APP-05-XCL-CD
	 * 
	 */
	String getApp05XclCd();

	void setApp05XclCd(String app05XclCd);

	/**
	 * Host Variable APP-06-XCL-CD
	 * 
	 */
	String getApp06XclCd();

	void setApp06XclCd(String app06XclCd);

	/**
	 * Host Variable APP-07-XCL-CD
	 * 
	 */
	String getApp07XclCd();

	void setApp07XclCd(String app07XclCd);

	/**
	 * Host Variable APP-08-XCL-CD
	 * 
	 */
	String getApp08XclCd();

	void setApp08XclCd(String app08XclCd);

	/**
	 * Host Variable APP-09-XCL-CD
	 * 
	 */
	String getApp09XclCd();

	void setApp09XclCd(String app09XclCd);

	/**
	 * Host Variable APP-10-XCL-CD
	 * 
	 */
	String getApp10XclCd();

	void setApp10XclCd(String app10XclCd);

	/**
	 * Host Variable APP-11-XCL-CD
	 * 
	 */
	String getApp11XclCd();

	void setApp11XclCd(String app11XclCd);

	/**
	 * Host Variable APP-12-XCL-CD
	 * 
	 */
	String getApp12XclCd();

	void setApp12XclCd(String app12XclCd);

	/**
	 * Host Variable APP-13-XCL-CD
	 * 
	 */
	String getApp13XclCd();

	void setApp13XclCd(String app13XclCd);

	/**
	 * Host Variable APP-14-XCL-CD
	 * 
	 */
	String getApp14XclCd();

	void setApp14XclCd(String app14XclCd);

	/**
	 * Host Variable APP-15-XCL-CD
	 * 
	 */
	String getApp15XclCd();

	void setApp15XclCd(String app15XclCd);

	/**
	 * Host Variable APP-16-XCL-CD
	 * 
	 */
	String getApp16XclCd();

	void setApp16XclCd(String app16XclCd);

	/**
	 * Host Variable APP-17-XCL-CD
	 * 
	 */
	String getApp17XclCd();

	void setApp17XclCd(String app17XclCd);

	/**
	 * Host Variable APP-18-XCL-CD
	 * 
	 */
	String getApp18XclCd();

	void setApp18XclCd(String app18XclCd);

	/**
	 * Host Variable APP-19-XCL-CD
	 * 
	 */
	String getApp19XclCd();

	void setApp19XclCd(String app19XclCd);

	/**
	 * Host Variable APP-20-XCL-CD
	 * 
	 */
	String getApp20XclCd();

	void setApp20XclCd(String app20XclCd);

	/**
	 * Host Variable HOLIDAY-DES
	 * 
	 */
	String getHolidayDes();

	void setHolidayDes(String holidayDes);
};
