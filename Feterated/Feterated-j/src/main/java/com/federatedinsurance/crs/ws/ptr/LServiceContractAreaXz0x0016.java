/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0016<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0016 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0016() {
	}

	public LServiceContractAreaXz0x0016(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt16iTkNotPrcTs(String xzt16iTkNotPrcTs) {
		writeString(Pos.XZT16I_TK_NOT_PRC_TS, xzt16iTkNotPrcTs, Len.XZT16I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT16I-TK-NOT-PRC-TS<br>*/
	public String getXzt16iTkNotPrcTs() {
		return readString(Pos.XZT16I_TK_NOT_PRC_TS, Len.XZT16I_TK_NOT_PRC_TS);
	}

	public void setXzt16iTkFrmSeqNbr(int xzt16iTkFrmSeqNbr) {
		writeInt(Pos.XZT16I_TK_FRM_SEQ_NBR, xzt16iTkFrmSeqNbr, Len.Int.XZT16I_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT16I-TK-FRM-SEQ-NBR<br>*/
	public int getXzt16iTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT16I_TK_FRM_SEQ_NBR, Len.XZT16I_TK_FRM_SEQ_NBR);
	}

	public String getXzt16iTkFrmSeqNbrFormatted() {
		return readFixedString(Pos.XZT16I_TK_FRM_SEQ_NBR, Len.XZT16I_TK_FRM_SEQ_NBR);
	}

	public void setXzt16iCsrActNbr(String xzt16iCsrActNbr) {
		writeString(Pos.XZT16I_CSR_ACT_NBR, xzt16iCsrActNbr, Len.XZT16I_CSR_ACT_NBR);
	}

	/**Original name: XZT16I-CSR-ACT-NBR<br>*/
	public String getXzt16iCsrActNbr() {
		return readString(Pos.XZT16I_CSR_ACT_NBR, Len.XZT16I_CSR_ACT_NBR);
	}

	public void setXzt16iPolNbr(String xzt16iPolNbr) {
		writeString(Pos.XZT16I_POL_NBR, xzt16iPolNbr, Len.XZT16I_POL_NBR);
	}

	/**Original name: XZT16I-POL-NBR<br>*/
	public String getXzt16iPolNbr() {
		return readString(Pos.XZT16I_POL_NBR, Len.XZT16I_POL_NBR);
	}

	public void setXzt16iUserid(String xzt16iUserid) {
		writeString(Pos.XZT16I_USERID, xzt16iUserid, Len.XZT16I_USERID);
	}

	/**Original name: XZT16I-USERID<br>*/
	public String getXzt16iUserid() {
		return readString(Pos.XZT16I_USERID, Len.XZT16I_USERID);
	}

	public String getXzt16iUseridFormatted() {
		return Functions.padBlanks(getXzt16iUserid(), Len.XZT16I_USERID);
	}

	public void setXzt16oTkNotPrcTs(String xzt16oTkNotPrcTs) {
		writeString(Pos.XZT16O_TK_NOT_PRC_TS, xzt16oTkNotPrcTs, Len.XZT16O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT16O-TK-NOT-PRC-TS<br>*/
	public String getXzt16oTkNotPrcTs() {
		return readString(Pos.XZT16O_TK_NOT_PRC_TS, Len.XZT16O_TK_NOT_PRC_TS);
	}

	public void setXzt16oTkFrmSeqNbr(int xzt16oTkFrmSeqNbr) {
		writeInt(Pos.XZT16O_TK_FRM_SEQ_NBR, xzt16oTkFrmSeqNbr, Len.Int.XZT16O_TK_FRM_SEQ_NBR);
	}

	public void setXzt16oTkFrmSeqNbrFormatted(String xzt16oTkFrmSeqNbr) {
		writeString(Pos.XZT16O_TK_FRM_SEQ_NBR, Trunc.toUnsignedNumeric(xzt16oTkFrmSeqNbr, Len.XZT16O_TK_FRM_SEQ_NBR), Len.XZT16O_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT16O-TK-FRM-SEQ-NBR<br>*/
	public int getXzt16oTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT16O_TK_FRM_SEQ_NBR, Len.XZT16O_TK_FRM_SEQ_NBR);
	}

	public void setXzt16oTkPolFrmCsumFormatted(String xzt16oTkPolFrmCsum) {
		writeString(Pos.XZT16O_TK_POL_FRM_CSUM, Trunc.toUnsignedNumeric(xzt16oTkPolFrmCsum, Len.XZT16O_TK_POL_FRM_CSUM), Len.XZT16O_TK_POL_FRM_CSUM);
	}

	/**Original name: XZT16O-TK-POL-FRM-CSUM<br>*/
	public int getXzt16oTkPolFrmCsum() {
		return readNumDispUnsignedInt(Pos.XZT16O_TK_POL_FRM_CSUM, Len.XZT16O_TK_POL_FRM_CSUM);
	}

	public void setXzt16oCsrActNbr(String xzt16oCsrActNbr) {
		writeString(Pos.XZT16O_CSR_ACT_NBR, xzt16oCsrActNbr, Len.XZT16O_CSR_ACT_NBR);
	}

	/**Original name: XZT16O-CSR-ACT-NBR<br>*/
	public String getXzt16oCsrActNbr() {
		return readString(Pos.XZT16O_CSR_ACT_NBR, Len.XZT16O_CSR_ACT_NBR);
	}

	public void setXzt16oPolNbr(String xzt16oPolNbr) {
		writeString(Pos.XZT16O_POL_NBR, xzt16oPolNbr, Len.XZT16O_POL_NBR);
	}

	/**Original name: XZT16O-POL-NBR<br>*/
	public String getXzt16oPolNbr() {
		return readString(Pos.XZT16O_POL_NBR, Len.XZT16O_POL_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT016_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT16I_TECHNICAL_KEY = XZT016_SERVICE_INPUTS;
		public static final int XZT16I_TK_NOT_PRC_TS = XZT16I_TECHNICAL_KEY;
		public static final int XZT16I_TK_FRM_SEQ_NBR = XZT16I_TK_NOT_PRC_TS + Len.XZT16I_TK_NOT_PRC_TS;
		public static final int XZT16I_CSR_ACT_NBR = XZT16I_TK_FRM_SEQ_NBR + Len.XZT16I_TK_FRM_SEQ_NBR;
		public static final int XZT16I_POL_NBR = XZT16I_CSR_ACT_NBR + Len.XZT16I_CSR_ACT_NBR;
		public static final int XZT16I_USERID = XZT16I_POL_NBR + Len.XZT16I_POL_NBR;
		public static final int XZT016_SERVICE_OUTPUTS = XZT16I_USERID + Len.XZT16I_USERID;
		public static final int XZT16O_TECHNICAL_KEY = XZT016_SERVICE_OUTPUTS;
		public static final int XZT16O_TK_NOT_PRC_TS = XZT16O_TECHNICAL_KEY;
		public static final int XZT16O_TK_FRM_SEQ_NBR = XZT16O_TK_NOT_PRC_TS + Len.XZT16O_TK_NOT_PRC_TS;
		public static final int XZT16O_TK_POL_FRM_CSUM = XZT16O_TK_FRM_SEQ_NBR + Len.XZT16O_TK_FRM_SEQ_NBR;
		public static final int XZT16O_CSR_ACT_NBR = XZT16O_TK_POL_FRM_CSUM + Len.XZT16O_TK_POL_FRM_CSUM;
		public static final int XZT16O_POL_NBR = XZT16O_CSR_ACT_NBR + Len.XZT16O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT16I_TK_NOT_PRC_TS = 26;
		public static final int XZT16I_TK_FRM_SEQ_NBR = 5;
		public static final int XZT16I_CSR_ACT_NBR = 9;
		public static final int XZT16I_POL_NBR = 25;
		public static final int XZT16I_USERID = 8;
		public static final int XZT16O_TK_NOT_PRC_TS = 26;
		public static final int XZT16O_TK_FRM_SEQ_NBR = 5;
		public static final int XZT16O_TK_POL_FRM_CSUM = 9;
		public static final int XZT16O_CSR_ACT_NBR = 9;
		public static final int XZT16I_TECHNICAL_KEY = XZT16I_TK_NOT_PRC_TS + XZT16I_TK_FRM_SEQ_NBR;
		public static final int XZT016_SERVICE_INPUTS = XZT16I_TECHNICAL_KEY + XZT16I_CSR_ACT_NBR + XZT16I_POL_NBR + XZT16I_USERID;
		public static final int XZT16O_TECHNICAL_KEY = XZT16O_TK_NOT_PRC_TS + XZT16O_TK_FRM_SEQ_NBR + XZT16O_TK_POL_FRM_CSUM;
		public static final int XZT16O_POL_NBR = 25;
		public static final int XZT016_SERVICE_OUTPUTS = XZT16O_TECHNICAL_KEY + XZT16O_CSR_ACT_NBR + XZT16O_POL_NBR;
		public static final int L_SERVICE_CONTRACT_AREA = XZT016_SERVICE_INPUTS + XZT016_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT16I_TK_FRM_SEQ_NBR = 5;
			public static final int XZT16O_TK_FRM_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
