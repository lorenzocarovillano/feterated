/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ESTO-CALL-ETRA-SW<br>
 * Variable: ESTO-CALL-ETRA-SW from copybook HALLESTO<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class EstoCallEtraSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NO_FAILURE_LOG_ONLY = 'L';
	public static final char FAILURE_ROW_TYPE = 'F';

	//==== METHODS ====
	public void setEstoCallEtraSw(char estoCallEtraSw) {
		this.value = estoCallEtraSw;
	}

	public char getEstoCallEtraSw() {
		return this.value;
	}

	public boolean isEstoFailureRowType() {
		return value == FAILURE_ROW_TYPE;
	}

	public void setFailureRowType() {
		value = FAILURE_ROW_TYPE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ESTO_CALL_ETRA_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
