/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.util.display.DisplayUtil;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;

/**Original name: DQ508100<br>*/
public class Dq508100 extends Program {

	//==== METHODS ====
	/**Original name: PROCESSING<br>*/
	public long execute() {
		// COB_CODE: PERFORM TASK1.
		task1();
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Dq508100 getInstance() {
		return (Programs.getInstance(Dq508100.class));
	}

	/**Original name: TASK1<br>*/
	private void task1() {
		// COB_CODE: DISPLAY 'TASK1'.
		DisplayUtil.sysout.write("TASK1");
	}
}
