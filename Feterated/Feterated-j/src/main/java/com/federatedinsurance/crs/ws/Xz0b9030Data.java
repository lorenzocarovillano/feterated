/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IActNotTyp;
import com.federatedinsurance.crs.copy.DclactNotTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9030<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9030Data implements IActNotTyp {

	//==== PROPERTIES ====
	//Original name: CF-YES
	private char cfYes = 'Y';
	/**Original name: NI-DSY-ORD-NBR<br>
	 * <pre>* NULL INDICATORS</pre>*/
	private short niDsyOrdNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-DOC-DES
	private short niDocDes = DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-END-ACT-NOT-TYP-CSR-FLAG
	private boolean swEndActNotTypCsrFlag = false;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9030 workingStorageArea = new WorkingStorageAreaXz0b9030();
	//Original name: WS-XZ0A9030-ROW
	private WsXz0a9030Row wsXz0a9030Row = new WsXz0a9030Row();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: DCLACT-NOT-TYP
	private DclactNotTyp dclactNotTyp = new DclactNotTyp();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public char getCfYes() {
		return this.cfYes;
	}

	public void setNiDsyOrdNbr(short niDsyOrdNbr) {
		this.niDsyOrdNbr = niDsyOrdNbr;
	}

	public short getNiDsyOrdNbr() {
		return this.niDsyOrdNbr;
	}

	public short getNiDocDes() {
		return this.niDocDes;
	}

	public void setSwEndActNotTypCsrFlag(boolean swEndActNotTypCsrFlag) {
		this.swEndActNotTypCsrFlag = swEndActNotTypCsrFlag;
	}

	public boolean isSwEndActNotTypCsrFlag() {
		return this.swEndActNotTypCsrFlag;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	@Override
	public String getActNotDes() {
		return dclactNotTyp.getActNotDes();
	}

	@Override
	public void setActNotDes(String actNotDes) {
		this.dclactNotTyp.setActNotDes(actNotDes);
	}

	@Override
	public String getActNotPthCd() {
		return dclactNotTyp.getActNotPthCd();
	}

	@Override
	public void setActNotPthCd(String actNotPthCd) {
		this.dclactNotTyp.setActNotPthCd(actNotPthCd);
	}

	@Override
	public String getActNotPthDes() {
		return dclactNotTyp.getActNotPthDes();
	}

	@Override
	public void setActNotPthDes(String actNotPthDes) {
		this.dclactNotTyp.setActNotPthDes(actNotPthDes);
	}

	@Override
	public String getActNotTypCd() {
		return dclactNotTyp.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.dclactNotTyp.setActNotTypCd(actNotTypCd);
	}

	public DclactNotTyp getDclactNotTyp() {
		return dclactNotTyp;
	}

	@Override
	public String getDocDes() {
		return FixedStrings.get(dclactNotTyp.getDocDesText(), dclactNotTyp.getDocDesLen());
	}

	@Override
	public void setDocDes(String docDes) {
		this.dclactNotTyp.setDocDesText(docDes);
		this.dclactNotTyp.setDocDesLen((((short) strLen(docDes))));
	}

	@Override
	public short getDsyOrdNbr() {
		return dclactNotTyp.getDsyOrdNbr();
	}

	@Override
	public void setDsyOrdNbr(short dsyOrdNbr) {
		this.dclactNotTyp.setDsyOrdNbr(dsyOrdNbr);
	}

	@Override
	public Short getDsyOrdNbrObj() {
		if (getNiDsyOrdNbr() >= 0) {
			return (getDsyOrdNbr());
		} else {
			return null;
		}
	}

	@Override
	public void setDsyOrdNbrObj(Short dsyOrdNbrObj) {
		if (dsyOrdNbrObj != null) {
			setDsyOrdNbr((dsyOrdNbrObj));
			setNiDsyOrdNbr(((short) 0));
		} else {
			setNiDsyOrdNbr(((short) -1));
		}
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b9030 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9030Row getWsXz0a9030Row() {
		return wsXz0a9030Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
