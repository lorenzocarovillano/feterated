/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q0021<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q0021 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZY021_CNC_POL_LIST_MAXOCCURS = 150;
	public static final String XZY021_IMP_TMN_TYP = "IMP";
	public static final String XZY021_UW_TMN_REQ_TYP = "TRU";
	public static final String XZY021_INSD_TMN_REQ_TYP = "TRI";
	public static final String XZY021_NON_PAY_CNC = "NPC";
	public static final String XZY021_INSD_CNC = "CNI";
	public static final String XZY021_UW_CNC = "CNU";
	private static final String[] XZY021_VLD_TMN_REQ_TYP = new String[] { "IMP", "TRU" };

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q0021() {
	}

	public LFrameworkRequestAreaXz0q0021(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzy021CsrActNbr(String xzy021CsrActNbr) {
		writeString(Pos.XZY021_CSR_ACT_NBR, xzy021CsrActNbr, Len.XZY021_CSR_ACT_NBR);
	}

	/**Original name: XZY021-CSR-ACT-NBR<br>*/
	public String getXzy021CsrActNbr() {
		return readString(Pos.XZY021_CSR_ACT_NBR, Len.XZY021_CSR_ACT_NBR);
	}

	public void setXzy021ActTmnDt(String xzy021ActTmnDt) {
		writeString(Pos.XZY021_ACT_TMN_DT, xzy021ActTmnDt, Len.XZY021_ACT_TMN_DT);
	}

	/**Original name: XZY021-ACT-TMN-DT<br>*/
	public String getXzy021ActTmnDt() {
		return readString(Pos.XZY021_ACT_TMN_DT, Len.XZY021_ACT_TMN_DT);
	}

	public void setXzy021ActNotTypCd(String xzy021ActNotTypCd) {
		writeString(Pos.XZY021_ACT_NOT_TYP_CD, xzy021ActNotTypCd, Len.XZY021_ACT_NOT_TYP_CD);
	}

	/**Original name: XZY021-ACT-NOT-TYP-CD<br>*/
	public String getXzy021ActNotTypCd() {
		return readString(Pos.XZY021_ACT_NOT_TYP_CD, Len.XZY021_ACT_NOT_TYP_CD);
	}

	public void setXzy021qCncPolListBytes(int xzy021qCncPolListIdx, byte[] buffer) {
		setXzy021qCncPolListBytes(xzy021qCncPolListIdx, buffer, 1);
	}

	public void setXzy021qCncPolListBytes(int xzy021qCncPolListIdx, byte[] buffer, int offset) {
		int position = Pos.xzy021CncPolList(xzy021qCncPolListIdx - 1);
		setBytes(buffer, offset, Len.XZY021_CNC_POL_LIST, position);
	}

	public void setXzy021CpPolNbr(int xzy021CpPolNbrIdx, String xzy021CpPolNbr) {
		int position = Pos.xzy021CpPolNbr(xzy021CpPolNbrIdx - 1);
		writeString(position, xzy021CpPolNbr, Len.XZY021_CP_POL_NBR);
	}

	/**Original name: XZY021-CP-POL-NBR<br>*/
	public String getXzy021CpPolNbr(int xzy021CpPolNbrIdx) {
		int position = Pos.xzy021CpPolNbr(xzy021CpPolNbrIdx - 1);
		return readString(position, Len.XZY021_CP_POL_NBR);
	}

	public void setXzy021NotDt(String xzy021NotDt) {
		writeString(Pos.XZY021_NOT_DT, xzy021NotDt, Len.XZY021_NOT_DT);
	}

	/**Original name: XZY021-NOT-DT<br>*/
	public String getXzy021NotDt() {
		return readString(Pos.XZY021_NOT_DT, Len.XZY021_NOT_DT);
	}

	public void setXzy021Userid(String xzy021Userid) {
		writeString(Pos.XZY021_USERID, xzy021Userid, Len.XZY021_USERID);
	}

	/**Original name: XZY021-USERID<br>*/
	public String getXzy021Userid() {
		return readString(Pos.XZY021_USERID, Len.XZY021_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0Y0021 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZY021_GET_NOT_DT_ROW = L_FW_REQ_XZ0Y0021;
		public static final int XZY021_CSR_ACT_NBR = XZY021_GET_NOT_DT_ROW;
		public static final int XZY021_ACT_TMN_DT = XZY021_CSR_ACT_NBR + Len.XZY021_CSR_ACT_NBR;
		public static final int XZY021_ACT_NOT_TYP_CD = XZY021_ACT_TMN_DT + Len.XZY021_ACT_TMN_DT;
		public static final int XZY021_NOT_DT = xzy021CpPolNbr(XZY021_CNC_POL_LIST_MAXOCCURS - 1) + Len.XZY021_CP_POL_NBR;
		public static final int XZY021_USERID = XZY021_NOT_DT + Len.XZY021_NOT_DT;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzy021CncPolList(int idx) {
			return XZY021_ACT_NOT_TYP_CD + Len.XZY021_ACT_NOT_TYP_CD + idx * Len.XZY021_CNC_POL_LIST;
		}

		public static int xzy021CpPolNbr(int idx) {
			return xzy021CncPolList(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY021_CSR_ACT_NBR = 9;
		public static final int XZY021_ACT_TMN_DT = 10;
		public static final int XZY021_ACT_NOT_TYP_CD = 5;
		public static final int XZY021_CP_POL_NBR = 25;
		public static final int XZY021_CNC_POL_LIST = XZY021_CP_POL_NBR;
		public static final int XZY021_NOT_DT = 10;
		public static final int XZY021_USERID = 8;
		public static final int XZY021_GET_NOT_DT_ROW = XZY021_CSR_ACT_NBR + XZY021_ACT_TMN_DT + XZY021_ACT_NOT_TYP_CD
				+ LFrameworkRequestAreaXz0q0021.XZY021_CNC_POL_LIST_MAXOCCURS * XZY021_CNC_POL_LIST + XZY021_NOT_DT + XZY021_USERID;
		public static final int L_FW_REQ_XZ0Y0021 = XZY021_GET_NOT_DT_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0Y0021;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
