/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalSecDpDfltV;
import com.federatedinsurance.crs.copy.Cawlc002;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclhalSecDpDflt;
import com.federatedinsurance.crs.copy.DclhalUowObjHierV;
import com.federatedinsurance.crs.copy.Hallcctx;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.redefines.CidpTableInfo;
import com.federatedinsurance.crs.ws.redefines.Cw02cClientTabTab;
import com.federatedinsurance.crs.ws.redefines.WsSe3CurIsoDateTime;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program CAWS002<br>
 * Generated as a class for rule WS.<br>*/
public class Caws002Data implements IHalSecDpDfltV {

	//==== PROPERTIES ====
	//Original name: WS-SPECIFIC-WORK-AREAS
	private WsSpecificWorkAreasCaws002 wsSpecificWorkAreas = new WsSpecificWorkAreasCaws002();
	//Original name: WS-GENERIC-FIELDS
	private WsGenericFields wsGenericFields = new WsGenericFields();
	//Original name: WS-ACT-CUR-ISO-DATE-TIME
	private WsActCurIsoDateTime wsActCurIsoDateTime = new WsActCurIsoDateTime();
	//Original name: CW02C-CLIENT-TAB-TAB
	private Cw02cClientTabTab cw02cClientTabTab = new Cw02cClientTabTab();
	//Original name: TBL-IDX
	private int tblIdx = 1;
	//Original name: CAWLC002
	private Cawlc002 cawlc002 = new Cawlc002();
	//Original name: HALLCCTX
	private Hallcctx hallcctx = new Hallcctx();
	//Original name: DCLHAL-SEC-DP-DFLT
	private DclhalSecDpDflt dclhalSecDpDflt = new DclhalSecDpDflt();
	//Original name: WS-BDO-SWITCHES
	private WsBdoSwitches wsBdoSwitches = new WsBdoSwitches();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: CIDP-TABLE-INFO
	private CidpTableInfo cidpTableInfo = new CidpTableInfo();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: DATE-STRUCTURE
	private DateStructureCawpcorc dateStructure = new DateStructureCawpcorc();
	//Original name: WS-SE3-CUR-ISO-DATE-TIME
	private WsSe3CurIsoDateTime wsSe3CurIsoDateTime = new WsSe3CurIsoDateTime();
	//Original name: DCLHAL-UOW-OBJ-HIER-V
	private DclhalUowObjHierV dclhalUowObjHierV = new DclhalUowObjHierV();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public void setTblIdx(int tblIdx) {
		this.tblIdx = tblIdx;
	}

	public int getTblIdx() {
		return this.tblIdx;
	}

	public void setWsDataPrivacyInfoFormatted(String data) {
		byte[] buffer = new byte[Len.WS_DATA_PRIVACY_INFO];
		MarshalByte.writeString(buffer, 1, data, Len.WS_DATA_PRIVACY_INFO);
		setWsDataPrivacyInfoBytes(buffer, 1);
	}

	public String getWsDataPrivacyInfoFormatted() {
		return getCidpPassedInfoFormatted();
	}

	public void setWsDataPrivacyInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setCidpPassedInfoBytes(buffer, position);
	}

	public String getCidpPassedInfoFormatted() {
		return MarshalByteExt.bufferToStr(getCidpPassedInfoBytes());
	}

	/**Original name: CIDP-PASSED-INFO<br>
	 * <pre>****************************************************************
	 *  HALLCIDP                                                      *
	 *  COMMON INTERFACE TO DATA PRIVACY                              *
	 *  USED BY BUSINESS DATA OBJECTS TO PASS DATA ROWS FOR           *
	 *  DATA PRIVACY CHECKING.                                        *
	 * ****************************************************************
	 *  SI#       PRGRMR  DATE       DESCRIPTION                      *
	 *  --------- ------- ---------- ---------------------------------*
	 *  SAVANNAH  PM      31MAY2000  INITIAL CODING                   *
	 *  14969     18448   25JUN2001  MADE CHANGES REQUIRED FOR        *
	 *                               ENHANCED DATA PRIVACY AND SET    *
	 *                               DEFAULTS PROCESSING.             *
	 *                               (ARCHITECTURE 2.3)               *
	 * ****************************************************************</pre>*/
	public byte[] getCidpPassedInfoBytes() {
		byte[] buffer = new byte[Len.CIDP_PASSED_INFO];
		return getCidpPassedInfoBytes(buffer, 1);
	}

	public void setCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.setCidpTableInfoBytes(buffer, position);
	}

	public byte[] getCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.getCidpTableInfoBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getAllSecAscsLit() {
		return wsGenericFields.getAllSecAscsLit();
	}

	@Override
	public void setAllSecAscsLit(String allSecAscsLit) {
		this.wsGenericFields.setAllSecAscsLit(allSecAscsLit);
	}

	@Override
	public String getAllSecGrpsLit() {
		return wsGenericFields.getAllSecGrpsLit();
	}

	@Override
	public void setAllSecGrpsLit(String allSecGrpsLit) {
		this.wsGenericFields.setAllSecGrpsLit(allSecGrpsLit);
	}

	@Override
	public String getAllUowsLit() {
		return wsGenericFields.getAllUowsLit();
	}

	@Override
	public void setAllUowsLit(String allUowsLit) {
		this.wsGenericFields.setAllUowsLit(allUowsLit);
	}

	@Override
	public String getAssocBusObjNm() {
		return wsSpecificWorkAreas.getWsAssocBusObjNm();
	}

	@Override
	public void setAssocBusObjNm(String assocBusObjNm) {
		this.wsSpecificWorkAreas.setWsAssocBusObjNm(assocBusObjNm);
	}

	@Override
	public String getAuthUserid() {
		return wsGenericFields.getAuthUserid();
	}

	@Override
	public void setAuthUserid(String authUserid) {
		this.wsGenericFields.setAuthUserid(authUserid);
	}

	@Override
	public String getBusObjNm() {
		return dclhalSecDpDflt.getBusObjNm();
	}

	@Override
	public void setBusObjNm(String busObjNm) {
		this.dclhalSecDpDflt.setBusObjNm(busObjNm);
	}

	public Cawlc002 getCawlc002() {
		return cawlc002;
	}

	public CidpTableInfo getCidpTableInfo() {
		return cidpTableInfo;
	}

	@Override
	public char getCmnAtrInd() {
		return dclhalSecDpDflt.getCmnAtrInd().getCmnAtrInd();
	}

	@Override
	public void setCmnAtrInd(char cmnAtrInd) {
		this.dclhalSecDpDflt.getCmnAtrInd().setCmnAtrInd(cmnAtrInd);
	}

	@Override
	public String getCmnNm() {
		return dclhalSecDpDflt.getCmnNm();
	}

	@Override
	public void setCmnNm(String cmnNm) {
		this.dclhalSecDpDflt.setCmnNm(cmnNm);
	}

	@Override
	public String getContext() {
		return dclhalSecDpDflt.getContext();
	}

	@Override
	public void setContext(String context) {
		this.dclhalSecDpDflt.setContext(context);
	}

	public Cw02cClientTabTab getCw02cClientTabTab() {
		return cw02cClientTabTab;
	}

	public DateStructureCawpcorc getDateStructure() {
		return dateStructure;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclhalSecDpDflt getDclhalSecDpDflt() {
		return dclhalSecDpDflt;
	}

	public DclhalUowObjHierV getDclhalUowObjHierV() {
		return dclhalUowObjHierV;
	}

	@Override
	public String getDefaultFld1() {
		return wsGenericFields.getDefaultFld1();
	}

	@Override
	public void setDefaultFld1(String defaultFld1) {
		this.wsGenericFields.setDefaultFld1(defaultFld1);
	}

	@Override
	public String getDefaultFld2() {
		return wsGenericFields.getDefaultFld2();
	}

	@Override
	public void setDefaultFld2(String defaultFld2) {
		this.wsGenericFields.setDefaultFld2(defaultFld2);
	}

	@Override
	public String getDefaultFld3() {
		return wsGenericFields.getDefaultFld3();
	}

	@Override
	public void setDefaultFld3(String defaultFld3) {
		this.wsGenericFields.setDefaultFld3(defaultFld3);
	}

	@Override
	public String getDefaultFld4() {
		return wsGenericFields.getDefaultFld4();
	}

	@Override
	public void setDefaultFld4(String defaultFld4) {
		this.wsGenericFields.setDefaultFld4(defaultFld4);
	}

	@Override
	public String getDefaultFld5() {
		return wsGenericFields.getDefaultFld5();
	}

	@Override
	public void setDefaultFld5(String defaultFld5) {
		this.wsGenericFields.setDefaultFld5(defaultFld5);
	}

	@Override
	public AfDecimal getDflDtaAmt() {
		return dclhalSecDpDflt.getDflDtaAmt();
	}

	@Override
	public void setDflDtaAmt(AfDecimal dflDtaAmt) {
		this.dclhalSecDpDflt.setDflDtaAmt(dflDtaAmt.copy());
	}

	@Override
	public AfDecimal getDflDtaAmtObj() {
		if (wsSpecificWorkAreas.getWsDflDtaAmtNi() >= 0) {
			return getDflDtaAmt().toString();
		} else {
			return null;
		}
	}

	@Override
	public void setDflDtaAmtObj(AfDecimal dflDtaAmtObj) {
		if (dflDtaAmtObj != null) {
			setDflDtaAmt(new AfDecimal(dflDtaAmtObj, 14, 2));
			wsSpecificWorkAreas.setWsDflDtaAmtNi(((short) 0));
		} else {
			wsSpecificWorkAreas.setWsDflDtaAmtNi(((short) -1));
		}
	}

	@Override
	public String getDflDtaTxt() {
		return dclhalSecDpDflt.getDflDtaTxt();
	}

	@Override
	public void setDflDtaTxt(String dflDtaTxt) {
		this.dclhalSecDpDflt.setDflDtaTxt(dflDtaTxt);
	}

	@Override
	public int getDflOfsNbr() {
		return dclhalSecDpDflt.getDflOfsNbr();
	}

	@Override
	public void setDflOfsNbr(int dflOfsNbr) {
		this.dclhalSecDpDflt.setDflOfsNbr(dflOfsNbr);
	}

	@Override
	public Integer getDflOfsNbrObj() {
		if (wsSpecificWorkAreas.getWsDflOfsNbrNi() >= 0) {
			return (getDflOfsNbr());
		} else {
			return null;
		}
	}

	@Override
	public void setDflOfsNbrObj(Integer dflOfsNbrObj) {
		if (dflOfsNbrObj != null) {
			setDflOfsNbr((dflOfsNbrObj));
			wsSpecificWorkAreas.setWsDflOfsNbrNi(((short) 0));
		} else {
			wsSpecificWorkAreas.setWsDflOfsNbrNi(((short) -1));
		}
	}

	@Override
	public String getDflOfsPer() {
		return dclhalSecDpDflt.getDflOfsPer().getDflOfsPer();
	}

	@Override
	public void setDflOfsPer(String dflOfsPer) {
		this.dclhalSecDpDflt.getDflOfsPer().setDflOfsPer(dflOfsPer);
	}

	@Override
	public char getDflTypInd() {
		return dclhalSecDpDflt.getDflTypInd().getDflTypInd();
	}

	@Override
	public void setDflTypInd(char dflTypInd) {
		this.dclhalSecDpDflt.getDflTypInd().setDflTypInd(dflTypInd);
	}

	@Override
	public String getEffectiveDt() {
		return dclhalSecDpDflt.getEffectiveDt();
	}

	@Override
	public void setEffectiveDt(String effectiveDt) {
		this.dclhalSecDpDflt.setEffectiveDt(effectiveDt);
	}

	@Override
	public String getExpirationDt() {
		return dclhalSecDpDflt.getExpirationDt();
	}

	@Override
	public void setExpirationDt(String expirationDt) {
		this.dclhalSecDpDflt.setExpirationDt(expirationDt);
	}

	@Override
	public String getGenTxtFld1() {
		return dclhalSecDpDflt.getGenTxtFld1();
	}

	@Override
	public void setGenTxtFld1(String genTxtFld1) {
		this.dclhalSecDpDflt.setGenTxtFld1(genTxtFld1);
	}

	@Override
	public String getGenTxtFld2() {
		return dclhalSecDpDflt.getGenTxtFld2();
	}

	@Override
	public void setGenTxtFld2(String genTxtFld2) {
		this.dclhalSecDpDflt.setGenTxtFld2(genTxtFld2);
	}

	@Override
	public String getGenTxtFld3() {
		return dclhalSecDpDflt.getGenTxtFld3();
	}

	@Override
	public void setGenTxtFld3(String genTxtFld3) {
		this.dclhalSecDpDflt.setGenTxtFld3(genTxtFld3);
	}

	@Override
	public String getGenTxtFld4() {
		return dclhalSecDpDflt.getGenTxtFld4();
	}

	@Override
	public void setGenTxtFld4(String genTxtFld4) {
		this.dclhalSecDpDflt.setGenTxtFld4(genTxtFld4);
	}

	@Override
	public String getGenTxtFld5() {
		return dclhalSecDpDflt.getGenTxtFld5();
	}

	@Override
	public void setGenTxtFld5(String genTxtFld5) {
		this.dclhalSecDpDflt.setGenTxtFld5(genTxtFld5);
	}

	public Hallcctx getHallcctx() {
		return hallcctx;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getReqTypCd() {
		return dclhalSecDpDflt.getReqTypCd().getReqTypCd();
	}

	@Override
	public void setReqTypCd(String reqTypCd) {
		this.dclhalSecDpDflt.getReqTypCd().setReqTypCd(reqTypCd);
	}

	@Override
	public String getSe3CurIsoDate() {
		return wsSe3CurIsoDateTime.getSe3CurIsoDate();
	}

	@Override
	public void setSe3CurIsoDate(String se3CurIsoDate) {
		this.wsSe3CurIsoDateTime.setSe3CurIsoDate(se3CurIsoDate);
	}

	@Override
	public String getSecAscTyp() {
		return wsGenericFields.getSecAscTyp();
	}

	@Override
	public void setSecAscTyp(String secAscTyp) {
		this.wsGenericFields.setSecAscTyp(secAscTyp);
	}

	@Override
	public String getSecGrpNm() {
		return wsGenericFields.getSecGrpNm();
	}

	@Override
	public void setSecGrpNm(String secGrpNm) {
		this.wsGenericFields.setSecGrpNm(secGrpNm);
	}

	@Override
	public int getSeqNbr() {
		return dclhalSecDpDflt.getSeqNbr();
	}

	@Override
	public void setSeqNbr(int seqNbr) {
		this.dclhalSecDpDflt.setSeqNbr(seqNbr);
	}

	@Override
	public String getSuppliedFld1() {
		return wsGenericFields.getSuppliedFld1();
	}

	@Override
	public void setSuppliedFld1(String suppliedFld1) {
		this.wsGenericFields.setSuppliedFld1(suppliedFld1);
	}

	@Override
	public String getSuppliedFld2() {
		return wsGenericFields.getSuppliedFld2();
	}

	@Override
	public void setSuppliedFld2(String suppliedFld2) {
		this.wsGenericFields.setSuppliedFld2(suppliedFld2);
	}

	@Override
	public String getSuppliedFld3() {
		return wsGenericFields.getSuppliedFld3();
	}

	@Override
	public void setSuppliedFld3(String suppliedFld3) {
		this.wsGenericFields.setSuppliedFld3(suppliedFld3);
	}

	@Override
	public String getSuppliedFld4() {
		return wsGenericFields.getSuppliedFld4();
	}

	@Override
	public void setSuppliedFld4(String suppliedFld4) {
		this.wsGenericFields.setSuppliedFld4(suppliedFld4);
	}

	@Override
	public String getSuppliedFld5() {
		return wsGenericFields.getSuppliedFld5();
	}

	@Override
	public void setSuppliedFld5(String suppliedFld5) {
		this.wsGenericFields.setSuppliedFld5(suppliedFld5);
	}

	@Override
	public String getUowNm() {
		return wsGenericFields.getUowNm();
	}

	@Override
	public void setUowNm(String uowNm) {
		this.wsGenericFields.setUowNm(uowNm);
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WsActCurIsoDateTime getWsActCurIsoDateTime() {
		return wsActCurIsoDateTime;
	}

	public WsBdoSwitches getWsBdoSwitches() {
		return wsBdoSwitches;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsGenericFields getWsGenericFields() {
		return wsGenericFields;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSe3CurIsoDateTime getWsSe3CurIsoDateTime() {
		return wsSe3CurIsoDateTime;
	}

	public WsSpecificWorkAreasCaws002 getWsSpecificWorkAreas() {
		return wsSpecificWorkAreas;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_APPLID = 8;
		public static final int CIDP_PASSED_INFO = CidpTableInfo.Len.CIDP_TABLE_INFO;
		public static final int WS_DATA_PRIVACY_INFO = CIDP_PASSED_INFO;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
