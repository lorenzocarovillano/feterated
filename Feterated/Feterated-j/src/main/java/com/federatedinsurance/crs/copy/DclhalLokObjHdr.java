/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalLokObjHdrV;

/**Original name: DCLHAL-LOK-OBJ-HDR<br>
 * Variable: DCLHAL-LOK-OBJ-HDR from copybook HALLGLOH<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalLokObjHdr implements IHalLokObjHdrV {

	//==== PROPERTIES ====
	//Original name: HLOH-TCH-KEY
	private String tchKey = DefaultValues.stringVal(Len.TCH_KEY);
	//Original name: HLOH-APP-ID
	private String appId = DefaultValues.stringVal(Len.APP_ID);
	//Original name: HLOH-LAST-ACY-TS
	private String lastAcyTs = DefaultValues.stringVal(Len.LAST_ACY_TS);

	//==== METHODS ====
	public String getDclhalLokObjHdrFormatted() {
		return MarshalByteExt.bufferToStr(getDclhalLokObjHdrBytes());
	}

	public byte[] getDclhalLokObjHdrBytes() {
		byte[] buffer = new byte[Len.DCLHAL_LOK_OBJ_HDR];
		return getDclhalLokObjHdrBytes(buffer, 1);
	}

	public byte[] getDclhalLokObjHdrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tchKey, Len.TCH_KEY);
		position += Len.TCH_KEY;
		MarshalByte.writeString(buffer, position, appId, Len.APP_ID);
		position += Len.APP_ID;
		MarshalByte.writeString(buffer, position, lastAcyTs, Len.LAST_ACY_TS);
		return buffer;
	}

	public void setTchKey(String tchKey) {
		this.tchKey = Functions.subString(tchKey, Len.TCH_KEY);
	}

	public String getTchKey() {
		return this.tchKey;
	}

	public String getTchKeyFormatted() {
		return Functions.padBlanks(getTchKey(), Len.TCH_KEY);
	}

	public void setAppId(String appId) {
		this.appId = Functions.subString(appId, Len.APP_ID);
	}

	public String getAppId() {
		return this.appId;
	}

	public String getAppIdFormatted() {
		return Functions.padBlanks(getAppId(), Len.APP_ID);
	}

	public String getLastAcyTs() {
		return this.lastAcyTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TCH_KEY = 126;
		public static final int APP_ID = 10;
		public static final int LAST_ACY_TS = 26;
		public static final int DCLHAL_LOK_OBJ_HDR = TCH_KEY + APP_ID + LAST_ACY_TS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
