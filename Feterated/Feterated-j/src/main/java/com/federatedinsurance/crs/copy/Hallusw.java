/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: HALLUSW<br>
 * Copybook: HALLUSW from copybook HALLUSW<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Hallusw {

	//==== PROPERTIES ====
	//Original name: USW-ID
	private String id = DefaultValues.stringVal(Len.ID);
	//Original name: USW-BUS-OBJ-SWITCH
	private String busObjSwitch = DefaultValues.stringVal(Len.BUS_OBJ_SWITCH);

	//==== METHODS ====
	public void setCommonBytes(byte[] buffer) {
		setCommonBytes(buffer, 1);
	}

	public void setCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		setKeyBytes(buffer, position);
	}

	public byte[] getCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		getKeyBytes(buffer, position);
		return buffer;
	}

	public String getKeyFormatted() {
		return MarshalByteExt.bufferToStr(getKeyBytes());
	}

	public void setKeyBytes(byte[] buffer) {
		setKeyBytes(buffer, 1);
	}

	/**Original name: USW-KEY<br>*/
	public byte[] getKeyBytes() {
		byte[] buffer = new byte[Len.KEY];
		return getKeyBytes(buffer, 1);
	}

	public void setKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		id = MarshalByte.readString(buffer, position, Len.ID);
		position += Len.ID;
		busObjSwitch = MarshalByte.readString(buffer, position, Len.BUS_OBJ_SWITCH);
	}

	public byte[] getKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, id, Len.ID);
		position += Len.ID;
		MarshalByte.writeString(buffer, position, busObjSwitch, Len.BUS_OBJ_SWITCH);
		return buffer;
	}

	public void setId(String id) {
		this.id = Functions.subString(id, Len.ID);
	}

	public String getId() {
		return this.id;
	}

	public String getIdFormatted() {
		return Functions.padBlanks(getId(), Len.ID);
	}

	public void setBusObjSwitch(String busObjSwitch) {
		this.busObjSwitch = Functions.subString(busObjSwitch, Len.BUS_OBJ_SWITCH);
	}

	public String getBusObjSwitch() {
		return this.busObjSwitch;
	}

	public String getBusObjSwitchFormatted() {
		return Functions.padBlanks(getBusObjSwitch(), Len.BUS_OBJ_SWITCH);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID = 32;
		public static final int BUS_OBJ_SWITCH = 32;
		public static final int KEY = ID + BUS_OBJ_SWITCH;
		public static final int COMMON = KEY;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
