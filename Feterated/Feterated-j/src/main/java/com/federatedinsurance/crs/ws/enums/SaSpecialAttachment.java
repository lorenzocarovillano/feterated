/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-SPECIAL-ATTACHMENT<br>
 * Variable: SA-SPECIAL-ATTACHMENT from program XZ0P90E0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaSpecialAttachment {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.SPECIAL_ATTACHMENT);
	public static final String F10176 = "F10-176";
	public static final String SF380 = "SF-380";
	public static final String SF70 = "SF-70";

	//==== METHODS ====
	public void setSpecialAttachment(String specialAttachment) {
		this.value = Functions.subString(specialAttachment, Len.SPECIAL_ATTACHMENT);
	}

	public String getSpecialAttachment() {
		return this.value;
	}

	public String getSpecialAttachmentFormatted() {
		return Functions.padBlanks(getSpecialAttachment(), Len.SPECIAL_ATTACHMENT);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SPECIAL_ATTACHMENT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
