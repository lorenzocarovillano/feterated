/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-INV-ACT-NOT-TYP-CD<br>
 * Variable: EA-01-INV-ACT-NOT-TYP-CD from program XZ0B90B0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01InvActNotTypCd {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD
	private String flr1 = "Account";
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-1
	private String flr2 = "notification";
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-2
	private String flr3 = "type code of";
	//Original name: EA-01-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-3
	private String flr4 = " does not match";
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-4
	private String flr5 = " the value in";
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-5
	private String flr6 = "the ACT_NOT";
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-6
	private String flr7 = "table of";
	//Original name: EA-01-ACT-NOT-TYP-CD-FROM-TBL
	private String actNotTypCdFromTbl = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD_FROM_TBL);
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-7
	private String flr8 = " for account";
	//Original name: EA-01-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-8
	private String flr9 = " and process";
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-9
	private String flr10 = "time stamp";
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-10
	private String flr11 = "date";
	//Original name: EA-01-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-01-INV-ACT-NOT-TYP-CD-11
	private char flr12 = '.';

	//==== METHODS ====
	public String getEa01InvActNotTypCdFormatted() {
		return MarshalByteExt.bufferToStr(getEa01InvActNotTypCdBytes());
	}

	public byte[] getEa01InvActNotTypCdBytes() {
		byte[] buffer = new byte[Len.EA01_INV_ACT_NOT_TYP_CD];
		return getEa01InvActNotTypCdBytes(buffer, 1);
	}

	public byte[] getEa01InvActNotTypCdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, actNotTypCdFromTbl, Len.ACT_NOT_TYP_CD_FROM_TBL);
		position += Len.ACT_NOT_TYP_CD_FROM_TBL;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr10, Len.FLR10);
		position += Len.FLR10;
		MarshalByte.writeString(buffer, position, flr11, Len.FLR11);
		position += Len.FLR11;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr12);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setActNotTypCdFromTbl(String actNotTypCdFromTbl) {
		this.actNotTypCdFromTbl = Functions.subString(actNotTypCdFromTbl, Len.ACT_NOT_TYP_CD_FROM_TBL);
	}

	public String getActNotTypCdFromTbl() {
		return this.actNotTypCdFromTbl;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public String getFlr10() {
		return this.flr10;
	}

	public String getFlr11() {
		return this.flr11;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr12() {
		return this.flr12;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int ACT_NOT_TYP_CD_FROM_TBL = 5;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 8;
		public static final int FLR2 = 13;
		public static final int FLR4 = 15;
		public static final int FLR5 = 14;
		public static final int FLR6 = 12;
		public static final int FLR7 = 9;
		public static final int FLR10 = 11;
		public static final int FLR11 = 5;
		public static final int FLR12 = 1;
		public static final int EA01_INV_ACT_NOT_TYP_CD = ACT_NOT_TYP_CD + ACT_NOT_TYP_CD_FROM_TBL + CSR_ACT_NBR + NOT_PRC_TS + FLR11 + FLR12 + FLR10
				+ FLR1 + 4 * FLR2 + FLR4 + FLR5 + FLR6 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
