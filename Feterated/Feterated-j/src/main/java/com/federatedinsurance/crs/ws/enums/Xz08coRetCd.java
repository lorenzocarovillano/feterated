/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: XZ08CO-RET-CD<br>
 * Variable: XZ08CO-RET-CD from copybook XZ08CI1O<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xz08coRetCd {

	//==== PROPERTIES ====
	private short value = DefaultValues.SHORT_VAL;
	public static final short OK = ((short) 0);
	public static final short WARNING = ((short) 100);
	public static final short SYSTEM_ERROR = ((short) 200);
	public static final short FAULT = ((short) 300);

	//==== METHODS ====
	public void setRetCd(short retCd) {
		this.value = retCd;
	}

	public short getRetCd() {
		return this.value;
	}

	public boolean isOk() {
		return value == OK;
	}

	public void setOk() {
		value = OK;
	}

	public boolean isInvalidInput() {
		return value == WARNING;
	}

	public void setInvalidInput() {
		value = WARNING;
	}

	public void setSystemError() {
		value = SYSTEM_ERROR;
	}

	public boolean isSpioRcFault() {
		return value == FAULT;
	}

	public void setFault() {
		value = FAULT;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZ08CO_RET_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
