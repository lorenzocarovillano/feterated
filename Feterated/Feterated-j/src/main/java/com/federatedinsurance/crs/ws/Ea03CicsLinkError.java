/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-03-CICS-LINK-ERROR<br>
 * Variable: EA-03-CICS-LINK-ERROR from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea03CicsLinkError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-03-CICS-LINK-ERROR
	private String flr1 = "CICS LINK TO";
	//Original name: EA-03-FAILED-LINK-PGM-NAME
	private String failedLinkPgmName = DefaultValues.stringVal(Len.FAILED_LINK_PGM_NAME);
	//Original name: FILLER-EA-03-CICS-LINK-ERROR-1
	private String flr2 = " FAILED.";
	//Original name: FILLER-EA-03-CICS-LINK-ERROR-2
	private String flr3 = "RESP CODE 1:";
	//Original name: EA-03-RESPONSE-CODE
	private String responseCode = DefaultValues.stringVal(Len.RESPONSE_CODE);
	//Original name: FILLER-EA-03-CICS-LINK-ERROR-3
	private String flr4 = " RESP CODE 2:";
	//Original name: EA-03-RESPONSE-CODE2
	private String responseCode2 = DefaultValues.stringVal(Len.RESPONSE_CODE2);

	//==== METHODS ====
	public String getEa03CicsLinkErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa03CicsLinkErrorBytes());
	}

	public byte[] getEa03CicsLinkErrorBytes() {
		byte[] buffer = new byte[Len.EA03_CICS_LINK_ERROR];
		return getEa03CicsLinkErrorBytes(buffer, 1);
	}

	public byte[] getEa03CicsLinkErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, failedLinkPgmName, Len.FAILED_LINK_PGM_NAME);
		position += Len.FAILED_LINK_PGM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, responseCode, Len.RESPONSE_CODE);
		position += Len.RESPONSE_CODE;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, responseCode2, Len.RESPONSE_CODE2);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setFailedLinkPgmName(String failedLinkPgmName) {
		this.failedLinkPgmName = Functions.subString(failedLinkPgmName, Len.FAILED_LINK_PGM_NAME);
	}

	public String getFailedLinkPgmName() {
		return this.failedLinkPgmName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setResponseCode(long responseCode) {
		this.responseCode = PicFormatter.display("-Z(8)9").format(responseCode).toString();
	}

	public long getResponseCode() {
		return PicParser.display("-Z(8)9").parseLong(this.responseCode);
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setResponseCode2(long responseCode2) {
		this.responseCode2 = PicFormatter.display("-Z(8)9").format(responseCode2).toString();
	}

	public long getResponseCode2() {
		return PicParser.display("-Z(8)9").parseLong(this.responseCode2);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FAILED_LINK_PGM_NAME = 8;
		public static final int RESPONSE_CODE = 10;
		public static final int RESPONSE_CODE2 = 10;
		public static final int FLR1 = 13;
		public static final int FLR2 = 9;
		public static final int FLR4 = 14;
		public static final int EA03_CICS_LINK_ERROR = FAILED_LINK_PGM_NAME + RESPONSE_CODE + RESPONSE_CODE2 + 2 * FLR1 + FLR2 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
