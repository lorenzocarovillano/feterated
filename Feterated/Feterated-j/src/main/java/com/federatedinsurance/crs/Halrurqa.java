/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.storage.KeyType;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.ws.DefaultComm;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.HalrurqaData;
import com.federatedinsurance.crs.ws.LioHalrurqaLinkage;
import com.federatedinsurance.crs.ws.LioUowBusObjData;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.enums.HalrurqaFunction;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: HALRURQA<br>
 * <pre>AUTHOR.       CSC.
 * DATE-WRITTEN. 04 JULY 2001.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - URQM REQUEST UMT ACCESS MODULE.             **
 * *                                                             **
 * * PLATFORM - I-BASE                                           **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -  URQM REQUEST UMT ACCESS MODULE.                  **
 * *                                                             **
 * *                                                             **
 * *            FUNCTIONS CURRENTLY AVAILABLE:                   **
 * *                                                             **
 * *            READ      - READ A SPECIFIC URQM REQUEST UMT     **
 * *                        RECORD.                              **
 * *            WRITE     - WRITE A NEW URQM REQUEST UMT         **
 * *                        RECORD.                              **
 * *            UPDATE    - UPDATE A URQM REQUEST UMT            **
 * *                        RECORD.                              **
 * *            ALLDEL    - DELETE ALL URQM REQUEST UMT RECORDS  **
 * *                        FOR THE MSG ID.                      **
 * *            DELBO     - DELETE ALL URQM REQUEST UMT RECORDS  **
 * *                        FOR THE MSG ID AND BUS OBJ.          **
 * *                                                             **
 * *                                                             **
 * * HOW TO USE THIS ROUTINE:                                    **
 * *                                                             **
 * * 1. READ FUNCTION - READ A SPECIFIC RECORD                   **
 * *                                                             **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRURQA-READ-FUNC TO TRUE.                       **
 * *       MOVE WS-POLICY-TAB-V   TO HALRURQA-BUS-OBJ-NM.        **
 * *       MOVE 1                 TO HALRURQA-REC-SEQ.           **
 * *                                                             **
 * *       CALL HALRURQA-HALRURQA-LIT USING                      **
 * *            DFHEIBLK                                         **
 * *            DFHCOMMAREA                                      **
 * *            UBOC-RECORD                                      **
 * *            WS-HALRURQA-LINKAGE                              **
 * *            PB01C-POLICY-TAB-ROW.                            **
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *                                                             **
 * *      (RECORD FOUND):                                        **
 * *                                                             **
 * *       HALRURQA-REC-FOUND     - SET TO TRUE                  **
 * *       HALRURQA-ACTION-CODE   - SET TO ACTION CODE           **
 * *       HALRURQA-BUS-OBJ-DATA-LENGTH                          **
 * *                              - LENGTH OF RECORD RETURNED    **
 * *       RECORD LAYOUT (3RD PARAM) - CONTAINS RETURNED RECORD  **
 * *                                                             **
 * *      (RECORD NOT FOUND):                                    **
 * *                                                             **
 * *       HALRURQA-REC-NOT-FOUND - SET TO TRUE                  **
 * *       HALRURQA-ACTION-CODE   - SET TO ALL '?'               **
 * *       HALRURQA-BUS-OBJ-DATA-LENGTH                          **
 * *                              - SET TO 0                     **
 * *       RECORD LAYOUT (3RD PARAM) - CONTAINS ALL '?'          **
 * *                                                             **
 * *                                                             **
 * * 2. WRITE FUNCTION - WRITE A RECORD                          **
 * *                                                             **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRURQA-WRITE-FUNC   TO TRUE.                    **
 * *       MOVE WS-POLICY-TAB-V      TO HALRURQA-BUS-OBJ-NM.     **
 * *       MOVE 'FETCH'              TO HALRURQA-ACTION-CODE.    **
 * *       MOVE LENGTH OF PB01C-POLICY-TAB-ROW                   **
 * *         TO HALRURQA-BUS-OBJ-DATA-LENGTH.                    **
 * *                                                             **
 * *       MOVE 'SOME DATA'          TO PB01C-POL-CHANGE-DT.     **
 * *                                                             **
 * *       CALL HALRURQA-HALRURQA-LIT USING                      **
 * *            DFHEIBLK                                         **
 * *            DFHCOMMAREA                                      **
 * *            UBOC-RECORD                                      **
 * *            WS-HALRURQA-LINKAGE                              **
 * *            PB01C-POLICY-TAB-ROW.                            **
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *       HALRURQA-REC-SEQ       - REC SEQ USED TO WRITE REC    **
 * *                                                             **
 * *                                                             **
 * * 3. UPDATE FUNCTION - UPDATE A SPECIFIC RECORD               **
 * *                                                             **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRURQA-UPDATE-FUNC  TO TRUE.                    **
 * *       MOVE WS-POLICY-TAB-V      TO HALRURQA-BUS-OBJ-NM.     **
 * *       MOVE 2                    TO HALRURQA-REC-SEQ.        **
 * *                                                             **
 * *    ** NOTE: IF ACTION-CODE NOT FILLED, EXISTING ONE FROM    **
 * *    **       RECORD WILL BE USED                             **
 * *       MOVE 'IGNORE'             TO HALRURQA-ACTION-CODE.    **
 * *                                                             **
 * *       MOVE 'SOME DATA'          TO PB01C-POL-CHANGE-DT.     **
 * *                                                             **
 * *       CALL HALRURQA-HALRURQA-LIT USING                      **
 * *            DFHEIBLK                                         **
 * *            DFHCOMMAREA                                      **
 * *            UBOC-RECORD                                      **
 * *            WS-HALRURQA-LINKAGE                              **
 * *            PB01C-POLICY-TAB-ROW.                            **
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *                                                             **
 * *       (NONE)                                                **
 * *                                                             **
 * *                                                             **
 * * 4. ALLDEL FUNCTION - DELETE ALL RECORDS FOR THE MSG ID      **
 * *                                                             **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRURQA-ALLDEL-FUNC  TO TRUE.                    **
 * *                                                             **
 * *       CALL HALRURQA-HALRURQA-LIT USING                      **
 * *            DFHEIBLK                                         **
 * *            DFHCOMMAREA                                      **
 * *            UBOC-RECORD                                      **
 * *            WS-HALRURQA-LINKAGE.                             **
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *                                                             **
 * *       (NONE)                                                **
 * *                                                             **
 * * 5. DELBO  FUNCTION - DELETE ALL RECORDS FOR A MSG ID AND    **
 * *                      BUS OBJ.                               **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRURQA-DELBO-FUNC   TO TRUE.                    **
 * *       MOVE WS-POLICY-TAB-V      TO HALRURQA-BUS-OBJ-NM.     **
 * *                                                             **
 * *       CALL HALRURQA-HALRURQA-LIT USING                      **
 * *            DFHEIBLK                                         **
 * *            DFHCOMMAREA                                      **
 * *            UBOC-RECORD                                      **
 * *            WS-HALRURQA-LINKAGE.                             **
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *                                                             **
 * *       (NONE)                                                **
 * *                                                             **
 * *                                                             **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED IN THE FOLLOW-**
 * *                       WAYS:                                 **
 * *                                                             **
 * *                       THIS MODULE IS DYNAMICALLY CALLED     **
 * *                       BY ANY MODULE REQUIRING ITS SERVICES. **
 * *                                                             **
 * * DATA ACCESS METHODS - UMT (VSAM), DB2.                      **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * --------  ---------  --------   ----------------------------**
 * * 15756     04 JUL 00  JAF (NU)   NEW                         **
 * * 17543I    12 FEB 02  18448      ADD NEW CODE FOR ALLDEL AND **
 * *                                 DELBO.                      **
 * * 17241     20 FEB 02  18448      REMOVE REFERENCES TO IAP.   **
 * * 21245     23 APR 02  18448      ENSURE THAT ONLY THE BO     **
 * *                                 SPECIFIED IS DELETED.       **
 * * 28068     21 NOV 02  18448      INCREASE REC COUNT.
 * ****************************************************************</pre>*/
public class Halrurqa extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>****************************************************************
	 * **MYND*  START OF:                             *SAVANNAH 2.0****
	 * **MYND*                                        *SAVANNAH 2.0****
	 * **MYND*  GENERIC         WORKING-STORAGE       *SAVANNAH 2.0****
	 * **MYND*  (SPECIFIC TO BPOS)                    *SAVANNAH 2.0****
	 * ****************************************************************
	 * * REPLACE HALLJWSA WITH HALLJWSB.
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NO IAP REFERENCES)                *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 OCT. 16, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *  CASE 17241  PRGMR AICI448          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private ExecContext execContext = null;
	//Original name: DEFAULT-COMM
	private DefaultComm defaultComm;
	//Original name: WORKING-STORAGE
	private HalrurqaData ws = new HalrurqaData();
	//Original name: LI-UBOC
	private Dfhcommarea liUboc;
	//Original name: LIO-HALRURQA-LINKAGE
	private LioHalrurqaLinkage lioHalrurqaLinkage;
	//Original name: LIO-HALRURQA-DATA
	private LioUowBusObjData lioHalrurqaData = new LioUowBusObjData();

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DefaultComm defaultComm, Dfhcommarea liUboc, LioHalrurqaLinkage lioHalrurqaLinkage,
			LioUowBusObjData lioHalrurqaData) {
		this.execContext = execContext;
		this.defaultComm = defaultComm;
		this.liUboc = liUboc;
		this.lioHalrurqaLinkage = lioHalrurqaLinkage;
		this.lioHalrurqaData = lioHalrurqaData;
		registerArgListeners();
		mainline();
		mainlineX();
		deleteArgListeners();
		return 0;
	}

	public static Halrurqa getInstance() {
		return (Programs.getInstance(Halrurqa.class));
	}

	/**Original name: 0000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  MAIN PROCESSING CONTROL                                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 0100-INITIALIZATION.
		initialization();
		//*   IF NOT UBOC-UOW-OK
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-MAINLINE-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-MAINLINE-X
			mainlineX();
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN HALRURQA-WRITE-FUNC
		//                   PERFORM 0200-WRITE-FUNC
		//               WHEN HALRURQA-UPDATE-FUNC
		//                   PERFORM 0300-UPDATE-FUNC
		//               WHEN HALRURQA-READ-FUNC
		//                   PERFORM 0400-READ-FUNC
		//               WHEN HALRURQA-ALLDEL-FUNC
		//                   PERFORM 0500-ALLDEL-FUNC
		//               WHEN HALRURQA-DELBO-FUNC
		//                   PERFORM 0600-DELBO-FUNC
		//               WHEN OTHER
		//                   GO TO 0000-MAINLINE-X
		//           END-EVALUATE.
		switch (lioHalrurqaLinkage.getFunction().getFunction()) {

		case HalrurqaFunction.WRITE_FUNC:// COB_CODE: PERFORM 0200-WRITE-FUNC
			writeFunc();
			break;

		case HalrurqaFunction.UPDATE_FUNC:// COB_CODE: PERFORM 0300-UPDATE-FUNC
			updateFunc();
			break;

		case HalrurqaFunction.READ_FUNC:// COB_CODE: PERFORM 0400-READ-FUNC
			readFunc();
			break;

		case HalrurqaFunction.ALLDEL_FUNC:// COB_CODE: PERFORM 0500-ALLDEL-FUNC
			alldelFunc();
			break;

		case HalrurqaFunction.DELBO_FUNC:// COB_CODE: PERFORM 0600-DELBO-FUNC
			delboFunc();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-ACTION-CODE    OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvActionCode();
			// COB_CODE: MOVE '0000-MAINLINE'
			//                TO EFAL-ERR-PARAGRAPH  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0000-MAINLINE");
			// COB_CODE: MOVE 'INVALID FUNCTION PASSED IN LINKAGE'
			//                TO EFAL-ERR-COMMENT    OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID FUNCTION PASSED IN LINKAGE");
			// COB_CODE: STRING 'HALRURQA-FUNCTION='
			//                  HALRURQA-FUNCTION                     ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HALRURQA-FUNCTION=",
					String.valueOf(lioHalrurqaLinkage.getFunction().getFunction()), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0000-MAINLINE-X
			mainlineX();
			break;
		}
		//**  PERFORM 0900-TERMINATION.
		// COB_CODE: PERFORM 9999-TERMINATION.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=HALRURQA.CBL:line=473, because the code is unreachable.
	}

	/**Original name: 0000-MAINLINE-X<br>*/
	private void mainlineX() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 0100-INITIALIZATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  INITIALIZATION.                                                *
	 *                                                                 *
	 * *****************************************************************
	 * * INIT WARN/ERR W-S</pre>*/
	private void initialization() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		//* VALIDATE COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(liUboc.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(liUboc.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowRespHeaderStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowRespDataStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowRespWarningsStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 0200-WRITE-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  WRITE FUNCTION: CONTROL THE WRITING                            *
	 *                  OF A URQM REQUEST RECORD AFTER FIRST           *
	 *                  ESTABLISHING WHICH IS THE NEXT AVAILABLE       *
	 *                  URQM-REC-SEQ TO USE.                           *
	 *                                                                 *
	 * *****************************************************************
	 * * ESTABLISH MAX REC SEQ FOR THIS BUS-OBJ TYPE ON URQM REQ UMT</pre>*/
	private void writeFunc() {
		// COB_CODE: PERFORM 0210-MAX-REC-SEQ.
		maxRecSeq();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0200-WRITE-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0200-WRITE-FUNC-X
			return;
		}
		//* NOW WRITE RECORD TO URQM REQ UMT
		// COB_CODE: PERFORM 0220-WRITE-REC.
		writeRec();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0200-WRITE-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0200-WRITE-FUNC-X
			return;
		}
	}

	/**Original name: 0210-MAX-REC-SEQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ESTABLISH THE MAX SEQ NO ALREADY ON URQM REQ UMT FOR THIS      *
	 *  BUS OBJ TYPE                                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void maxRecSeq() {
		// COB_CODE: SET WS0215-REC-FOUND TO TRUE.
		ws.getWs0215RecFoundSw().setFound();
		// COB_CODE: MOVE ZERO TO WS0210-MAX-REC-SEQ.
		ws.setWs0210MaxRecSeq(0);
		// COB_CODE: PERFORM 0215-READ-REC
		//                   VARYING URQM-REC-SEQ FROM 1 BY 1
		//                     UNTIL UBOC-HALT-AND-RETURN
		//                        OR WS0215-REC-NOT-FOUND.
		ws.getUrqmCommon().setRecSeq(1);
		while (!(liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWs0215RecFoundSw().isNotFound())) {
			readRec();
			ws.getUrqmCommon().setRecSeq(Trunc.toInt(ws.getUrqmCommon().getRecSeq() + 1, 5));
		}
	}

	/**Original name: 0215-READ-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  READ A RECORD ON URQM REQ UMT                                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID         TO URQM-ID.
		ws.getUrqmCommon().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRURQA-BUS-OBJ-NM TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(lioHalrurqaLinkage.getBusObjNm());
		// COB_CODE: EXEC CICS
		//                READ FILE  (UBOC-UOW-REQ-MSG-STORE)
		//                INTO       (URQM-COMMON)
		//                RIDFLD     (URQM-KEY)
		//                KEYLENGTH  (LENGTH OF URQM-KEY)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, UrqmCommon.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   MOVE URQM-REC-SEQ    TO WS0210-MAX-REC-SEQ
		//               WHEN DFHRESP(NOTFND)
		//                   SET WS0215-REC-NOT-FOUND TO TRUE
		//               WHEN OTHER
		//                   GO TO 0215-READ-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET WS0215-REC-FOUND TO TRUE
			ws.getWs0215RecFoundSw().setFound();
			// COB_CODE: MOVE URQM-REC-SEQ    TO WS0210-MAX-REC-SEQ
			ws.setWs0210MaxRecSeqFormatted(ws.getUrqmCommon().getRecSeqFormatted());
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET WS0215-REC-NOT-FOUND TO TRUE
			ws.getWs0215RecFoundSw().setNotFound();
		} else {
			// COB_CODE: SET WS-LOG-ERROR       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '0215-READ-REC'
			//                                  TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0215-READ-REC");
			// COB_CODE: MOVE 'READ REQ URQM MSG STORE FAILED'
			//                                  TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ REQ URQM MSG STORE FAILED");
			// COB_CODE: STRING 'URQM-ID='            URQM-ID              ';'
			//                  'URQM-BUS-OBJ='       URQM-BUS-OBJ         ';'
			//                  'URQM-REC-SEQ='       URQM-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "URQM-ID=", ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(),
							";", "URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0215-READ-REC-X
			return;
		}
	}

	/**Original name: 0220-WRITE-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  WRITE FUNCTION: WRITE A URQM REQUEST RECORD                    *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID                 TO URQM-ID.
		ws.getUrqmCommon().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRURQA-BUS-OBJ-NM         TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(lioHalrurqaLinkage.getBusObjNm());
		// COB_CODE: ADD 1 WS0210-MAX-REC-SEQ     GIVING URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(Trunc.toInt(1 + ws.getWs0210MaxRecSeq(), 5));
		// COB_CODE: ADD LENGTH OF URQM-MSG-HDR
		//               HALRURQA-BUS-OBJ-DATA-LENGTH
		//                                        GIVING URQM-UOW-BUFFER-LENGTH.
		ws.getUrqmCommon().setUowBufferLength(((short) (UrqmCommon.Len.MSG_HDR + lioHalrurqaLinkage.getBusObjDataLength0())));
		// COB_CODE: MOVE HALRURQA-BUS-OBJ-NM         TO URQM-MSG-BUS-OBJ-NM.
		ws.getUrqmCommon().setMsgBusObjNm(lioHalrurqaLinkage.getBusObjNm());
		// COB_CODE: MOVE HALRURQA-ACTION-CODE        TO URQM-ACTION-CODE.
		ws.getUrqmCommon().getActionCode().setUbocPassThruAction(lioHalrurqaLinkage.getActionCode());
		// COB_CODE: MOVE HALRURQA-BUS-OBJ-DATA-LENGTH
		//                                            TO URQM-BUS-OBJ-DATA-LENGTH.
		ws.getUrqmCommon().setBusObjDataLength(lioHalrurqaLinkage.getBusObjDataLength0());
		// COB_CODE: MOVE LIO-HALRURQA-DATA           TO URQM-MSG-DATA.
		ws.getUrqmCommon().setMsgData(lioHalrurqaData.getLioUowBusObjDataFormatted());
		// COB_CODE: EXEC CICS
		//                WRITE
		//                FILE       (UBOC-UOW-REQ-MSG-STORE)
		//                FROM       (URQM-COMMON)
		//                LENGTH     (LENGTH OF URQM-COMMON)
		//                RIDFLD     (URQM-KEY)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getUrqmCommon().getUrqmCommonBytes());
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   MOVE URQM-REC-SEQ TO HALRURQA-REC-SEQ
		//               WHEN OTHER
		//                   GO TO 0220-WRITE-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: MOVE URQM-REC-SEQ TO HALRURQA-REC-SEQ
			lioHalrurqaLinkage.setRecSeqFormatted(ws.getUrqmCommon().getRecSeqFormatted());
		} else {
			// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '0220-WRITE-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0220-WRITE-REC");
			// COB_CODE: MOVE 'WRITE URQM REQ MSG STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE URQM REQ MSG STORE FAILED");
			// COB_CODE: STRING 'URQM-ID='            URQM-ID              ';'
			//                  'URQM-BUS-OBJ='       URQM-BUS-OBJ         ';'
			//                  'URQM-REC-SEQ='       URQM-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "URQM-ID=", ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(),
							";", "URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0220-WRITE-REC-X
			return;
		}
	}

	/**Original name: 0300-UPDATE-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  UPDATE FUNCTION: CONTROL THE UPDATING OF A                     *
	 *                  URQM REQUEST RECORD AFTER FIRST RE-READING THE *
	 *                  RECORD TO ESTABLISH AN UPDATE LOCK.            *
	 *                                                                 *
	 * *****************************************************************
	 * * ESTABLISH AN UPDATE LOCK BY RE-READING THE RECORD</pre>*/
	private void updateFunc() {
		// COB_CODE: PERFORM 0310-READ-FOR-UPDATE-LOCK.
		readForUpdateLock();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0300-UPDATE-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0300-UPDATE-FUNC-X
			return;
		}
		//* REWRITE (UPDATE) RECORD
		// COB_CODE: PERFORM 0320-UPDATE-REC.
		updateRec();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0300-UPDATE-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0300-UPDATE-FUNC-X
			return;
		}
	}

	/**Original name: 0310-READ-FOR-UPDATE-LOCK_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  RE-READ RECORD TO ESTABLISH UPDATE LOCK.                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readForUpdateLock() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID         TO URQM-ID.
		ws.getUrqmCommon().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRURQA-BUS-OBJ-NM TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(lioHalrurqaLinkage.getBusObjNm());
		// COB_CODE: MOVE HALRURQA-REC-SEQ    TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeqFormatted(lioHalrurqaLinkage.getRecSeqFormatted());
		// COB_CODE: EXEC CICS
		//                READ FILE  (UBOC-UOW-REQ-MSG-STORE)
		//                INTO       (URQM-COMMON)
		//                RIDFLD     (URQM-KEY)
		//                KEYLENGTH  (LENGTH OF URQM-KEY)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//                UPDATE
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, UrqmCommon.Len.KEY, true);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 0310-READ-FOR-UPDATE-LOCK-X
		//               WHEN OTHER
		//                   GO TO 0310-READ-FOR-UPDATE-LOCK-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET WS-LOG-ERROR                            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-EXI-ROW-NOT-FOUND  OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspExiRowNotFound();
			// COB_CODE: MOVE '0310-READ-FOR-UPDATE-LOCK'
			//                TO EFAL-ERR-PARAGRAPH  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0310-READ-FOR-UPDATE-LOCK");
			// COB_CODE: MOVE 'EXISTING URQM REC NOT FOUND'
			//                TO EFAL-ERR-COMMENT    OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("EXISTING URQM REC NOT FOUND");
			// COB_CODE: STRING 'URQM-ID='            URQM-ID              ';'
			//                  'URQM-BUS-OBJ='       URQM-BUS-OBJ         ';'
			//                  'URQM-REC-SEQ='       URQM-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "URQM-ID=", ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(),
							";", "URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0310-READ-FOR-UPDATE-LOCK-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '0310-READ-FOR-UPDATE-LOCK'
			//                                  TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0310-READ-FOR-UPDATE-LOCK");
			// COB_CODE: MOVE 'READ (FOR UPDATE) REQ URQM MSG STORE FAILED'
			//                                  TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ (FOR UPDATE) REQ URQM MSG STORE FAILED");
			// COB_CODE: STRING 'URQM-ID='            URQM-ID              ';'
			//                  'URQM-BUS-OBJ='       URQM-BUS-OBJ         ';'
			//                  'URQM-REC-SEQ='       URQM-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "URQM-ID=", ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(),
							";", "URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0310-READ-FOR-UPDATE-LOCK-X
			return;
		}
	}

	/**Original name: 0320-UPDATE-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  UPDATE (RE-WRITE) RECORD TO UPDATE IT.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * USE ACTION-CODE IF SUPPLIED OTHERWISE LEAVE ALONE</pre>*/
	private void updateRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF HALRURQA-ACTION-CODE NOT = SPACES
		//               MOVE HALRURQA-ACTION-CODE  TO URQM-ACTION-CODE
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lioHalrurqaLinkage.getActionCode())) {
			// COB_CODE: MOVE HALRURQA-ACTION-CODE  TO URQM-ACTION-CODE
			ws.getUrqmCommon().getActionCode().setUbocPassThruAction(lioHalrurqaLinkage.getActionCode());
		}
		//* FOLLOWING SO THAT LENGTH OF DATA IN LINKAGE IS CALCULATED
		//* CORRECTLY.
		// COB_CODE: MOVE URQM-BUS-OBJ-DATA-LENGTH
		//                                      TO HALRURQA-BUS-OBJ-DATA-LENGTH.
		lioHalrurqaLinkage.setBusObjDataLength(ws.getUrqmCommon().getBusObjDataLength());
		// COB_CODE: MOVE LIO-HALRURQA-DATA     TO URQM-MSG-DATA.
		ws.getUrqmCommon().setMsgData(lioHalrurqaData.getLioUowBusObjDataFormatted());
		// COB_CODE: EXEC CICS
		//                REWRITE
		//                FILE       (UBOC-UOW-REQ-MSG-STORE)
		//                FROM       (URQM-COMMON)
		//                LENGTH     (LENGTH OF URQM-COMMON)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getUrqmCommon().getUrqmCommonBytes());
			iRowDAO.update(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0320-UPDATE-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR                          TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-REWRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsRewriteUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '0320-UPDATE-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0320-UPDATE-REC");
			// COB_CODE: MOVE 'REWRITE URQM REQ MSG STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("REWRITE URQM REQ MSG STORE FAILED");
			// COB_CODE: STRING 'URQM-ID='            URQM-ID              ';'
			//                  'URQM-BUS-OBJ='       URQM-BUS-OBJ         ';'
			//                  'URQM-REC-SEQ='       URQM-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "URQM-ID=", ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(),
							";", "URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0320-UPDATE-REC-X
			return;
		}
	}

	/**Original name: 0400-READ-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  READ FUNCTION:  READ A SPECIFIC URQM REQUEST RECORD            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readFunc() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID         TO URQM-ID.
		ws.getUrqmCommon().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRURQA-BUS-OBJ-NM TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(lioHalrurqaLinkage.getBusObjNm());
		// COB_CODE: MOVE HALRURQA-REC-SEQ    TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeqFormatted(lioHalrurqaLinkage.getRecSeqFormatted());
		// COB_CODE: EXEC CICS
		//                READ FILE  (UBOC-UOW-REQ-MSG-STORE)
		//                INTO       (URQM-COMMON)
		//                RIDFLD     (URQM-KEY)
		//                KEYLENGTH  (LENGTH OF URQM-KEY)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, UrqmCommon.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   MOVE URQM-MSG-DATA     TO LIO-HALRURQA-DATA
		//               WHEN DFHRESP(NOTFND)
		//                   MOVE ALL '?'           TO LIO-HALRURQA-DATA
		//               WHEN OTHER
		//                   GO TO 0400-READ-FUNC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET HALRURQA-REC-FOUND TO TRUE
			lioHalrurqaLinkage.getRecFoundSw().setFound();
			// COB_CODE: MOVE URQM-ACTION-CODE  TO HALRURQA-ACTION-CODE
			lioHalrurqaLinkage.setActionCode(ws.getUrqmCommon().getActionCode().getUbocPassThruAction());
			// COB_CODE: MOVE URQM-BUS-OBJ-DATA-LENGTH
			//                                 TO HALRURQA-BUS-OBJ-DATA-LENGTH
			lioHalrurqaLinkage.setBusObjDataLength(ws.getUrqmCommon().getBusObjDataLength());
			// COB_CODE: MOVE URQM-MSG-DATA     TO LIO-HALRURQA-DATA
			lioHalrurqaData.setLioUowBusObjDataFormatted(ws.getUrqmCommon().getMsgDataFormatted());
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET HALRURQA-REC-NOT-FOUND TO TRUE
			lioHalrurqaLinkage.getRecFoundSw().setNotFound();
			// COB_CODE: MOVE ALL '?'           TO HALRURQA-ACTION-CODE
			lioHalrurqaLinkage.setActionCode(LiteralGenerator.create("?", LioHalrurqaLinkage.Len.ACTION_CODE));
			// COB_CODE: MOVE 0                TO HALRURQA-BUS-OBJ-DATA-LENGTH
			lioHalrurqaLinkage.setBusObjDataLength(((short) 0));
			// COB_CODE: MOVE ALL '?'           TO LIO-HALRURQA-DATA
			lioHalrurqaData.setLioUowBusObjDataFormatted(LiteralGenerator.create("?", lioHalrurqaData.getLioUowBusObjDataSize()));
		} else {
			// COB_CODE: SET WS-LOG-ERROR       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '0400-READ-FUNC'
			//                                  TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0400-READ-FUNC");
			// COB_CODE: MOVE 'READ REQ URQM MSG STORE FAILED'
			//                                  TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ REQ URQM MSG STORE FAILED");
			// COB_CODE: STRING 'URQM-ID='            URQM-ID              ';'
			//                  'URQM-BUS-OBJ='       URQM-BUS-OBJ         ';'
			//                  'URQM-REC-SEQ='       URQM-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "URQM-ID=", ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(),
							";", "URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0400-READ-FUNC-X
			return;
		}
	}

	/**Original name: 0500-ALLDEL-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ALLDEL FUNCTION: DELETE ALL REQUEST  UMT ROWS FOR A GIVEN MSGID*
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void alldelFunc() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET START-BROWSE-UOW-REQUEST TO TRUE.
		ws.getWsClearUowRequestSwitch().setStartBrowseUowRequest();
		// COB_CODE: MOVE UBOC-MSG-ID TO URQM-ID.
		ws.getUrqmCommon().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE LOW-VALUES  TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(LiteralGenerator.create(Types.LOW_CHAR_VAL, UrqmCommon.Len.BUS_OBJ));
		// COB_CODE: MOVE 0           TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(0);
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (UBOC-UOW-REQ-MSG-STORE)
		//               RIDFLD       (URQM-KEY)
		//               KEYLENGTH    (LENGTH OF URQM-KEY)
		//               GTEQ
		//               RESP         (WS-RESPONSE-CODE)
		//               RESP2        (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, UrqmCommon.Len.KEY);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 0500-ALLDEL-FUNC-X
		//               WHEN OTHER
		//                   GO TO 0500-ALLDEL-FUNC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: GO TO 0500-ALLDEL-FUNC-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: STRING 'URQM-KEY = ' URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0500-ALLDEL-FUNC'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0500-ALLDEL-FUNC");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM STARTBR UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM STARTBR UMT");
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0500-ALLDEL-FUNC-X
			return;
		}
		// COB_CODE: PERFORM 0505-READNEXT-UOW-REQUEST
		//               UNTIL NO-MORE-UOW-REQUESTS.
		while (!ws.getWsClearUowRequestSwitch().isNoMoreUowRequests()) {
			readnextUowRequest();
		}
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (UBOC-UOW-REQ-MSG-STORE)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'URQM-KEY = ' URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0500-ALLDEL-FUNC'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0500-ALLDEL-FUNC");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM ENDBR UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM ENDBR UMT");
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 0505-READNEXT-UOW-REQUEST_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  READ EACH RECORD FROM THE UOW REQUEST UMT AND DELETE IT.
	 * ***************************************************************</pre>*/
	private void readnextUowRequest() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (UBOC-UOW-REQ-MSG-STORE)
		//               INTO          (URQM-COMMON)
		//               RIDFLD        (URQM-KEY)
		//               KEYLENGTH     (LENGTH OF URQM-KEY)
		//               RESP          (WS-RESPONSE-CODE)
		//               RESP2         (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, UrqmCommon.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setKeyBytes(iRowData.getKey());
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN DFHRESP(ENDFILE)
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 0505-READNEXT-UOW-REQUEST-X
		//               WHEN OTHER
		//                   GO TO 0505-READNEXT-UOW-REQUEST-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF URQM-ID NOT EQUAL UBOC-MSG-ID
			//               GO TO 0505-READNEXT-UOW-REQUEST-X
			//           ELSE
			//               CONTINUE
			//           END-IF
			if (!Conditions.eq(ws.getUrqmCommon().getId(), liUboc.getCommInfo().getUbocMsgId())) {
				// COB_CODE: SET NO-MORE-UOW-REQUESTS TO TRUE
				ws.getWsClearUowRequestSwitch().setNoMoreUowRequests();
				// COB_CODE: GO TO 0505-READNEXT-UOW-REQUEST-X
				return;
			} else {
				// COB_CODE: CONTINUE
				//continue
			}
		} else if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ENDFILE)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND)) {
			// COB_CODE: SET NO-MORE-UOW-REQUESTS TO TRUE
			ws.getWsClearUowRequestSwitch().setNoMoreUowRequests();
			// COB_CODE: GO TO 0505-READNEXT-UOW-REQUEST-X
			return;
		} else {
			// COB_CODE: SET NO-MORE-UOW-REQUESTS TO TRUE
			ws.getWsClearUowRequestSwitch().setNoMoreUowRequests();
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'URQM-KEY = ' URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0505-READNEXT-UOW-REQUEST'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0505-READNEXT-UOW-REQUEST");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNXT UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNXT UMT");
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0505-READNEXT-UOW-REQUEST-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETE FILE (UBOC-UOW-REQ-MSG-STORE)
		//               RIDFLD      (URQM-KEY)
		//               KEYLENGTH   (LENGTH OF URQM-KEY)
		//               RESP        (WS-RESPONSE-CODE)
		//               RESP2       (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowDAO.delete(iRowData, UrqmCommon.Len.KEY);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET NO-MORE-UOW-REQUESTS TO TRUE
			ws.getWsClearUowRequestSwitch().setNoMoreUowRequests();
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: STRING 'URQM-KEY = ' URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0505-READNEXT-UOW-REQUEST'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0505-READNEXT-UOW-REQUEST");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE UMT");
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 0600-DELBO-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ALLDEL FUNCTION: DELETE ALL REQUEST  UMT ROWS FOR A GIVEN MSGID*
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void delboFunc() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET START-BROWSE-UOW-REQUEST TO TRUE.
		ws.getWsClearUowRequestSwitch().setStartBrowseUowRequest();
		// COB_CODE: MOVE UBOC-MSG-ID         TO URQM-ID.
		ws.getUrqmCommon().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRURQA-BUS-OBJ-NM TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(lioHalrurqaLinkage.getBusObjNm());
		// COB_CODE: MOVE 0                   TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(0);
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (UBOC-UOW-REQ-MSG-STORE)
		//               RIDFLD       (URQM-KEY)
		//               KEYLENGTH    (LENGTH OF URQM-KEY)
		//               GTEQ
		//               RESP         (WS-RESPONSE-CODE)
		//               RESP2        (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, UrqmCommon.Len.KEY);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 0600-DELBO-FUNC-X
		//               WHEN OTHER
		//                   GO TO 0600-DELBO-FUNC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: GO TO 0600-DELBO-FUNC-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: STRING 'URQM-KEY = ' URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0600-DELBO-FUNC'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-DELBO-FUNC");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM STARTBR UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM STARTBR UMT");
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0600-DELBO-FUNC-X
			return;
		}
		// COB_CODE: PERFORM 0605-READNEXT-UOW-REQUEST
		//               UNTIL NO-MORE-UOW-REQUESTS.
		while (!ws.getWsClearUowRequestSwitch().isNoMoreUowRequests()) {
			readnextUowRequest1();
		}
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (UBOC-UOW-REQ-MSG-STORE)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'URQM-KEY = ' URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0600-DELBO-FUNC'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-DELBO-FUNC");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM ENDBR UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM ENDBR UMT");
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 0605-READNEXT-UOW-REQUEST_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  READ EACH RECORD FROM THE UOW REQUEST UMT FOR THE MSGID AND
	 *  BUS OBJ AND DELETE IT.
	 * ***************************************************************</pre>*/
	private void readnextUowRequest1() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (UBOC-UOW-REQ-MSG-STORE)
		//               INTO          (URQM-COMMON)
		//               RIDFLD        (URQM-KEY)
		//               KEYLENGTH     (LENGTH OF URQM-KEY)
		//               RESP          (WS-RESPONSE-CODE)
		//               RESP2         (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, UrqmCommon.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setKeyBytes(iRowData.getKey());
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//                    WHEN DFHRESP(NORMAL)
		//                        END-IF
		//           *17543I             IF URQM-ID NOT EQUAL UBOC-MSG-ID
		//           *17543I                 SET NO-MORE-UOW-REQUESTS TO TRUE
		//           *17543I                 GO TO 0605-READNEXT-UOW-REQUEST-X
		//           *17543I             ELSE
		//           *17543I                 CONTINUE
		//           *17543I             END-IF
		//                    WHEN DFHRESP(ENDFILE)
		//                    WHEN DFHRESP(NOTFND)
		//                        GO TO 0605-READNEXT-UOW-REQUEST-X
		//                    WHEN OTHER
		//                        GO TO 0605-READNEXT-UOW-REQUEST-X
		//                END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF URQM-ID EQUAL UBOC-MSG-ID
			//             AND URQM-BUS-OBJ EQUAL HALRURQA-BUS-OBJ-NM
			//               CONTINUE
			//           ELSE
			//               GO TO 0605-READNEXT-UOW-REQUEST-X
			//           END-IF
			if (Conditions.eq(ws.getUrqmCommon().getId(), liUboc.getCommInfo().getUbocMsgId())
					&& Conditions.eq(ws.getUrqmCommon().getBusObj(), lioHalrurqaLinkage.getBusObjNm())) {
				// COB_CODE: CONTINUE
				//continue
			} else {
				// COB_CODE: SET NO-MORE-UOW-REQUESTS TO TRUE
				ws.getWsClearUowRequestSwitch().setNoMoreUowRequests();
				// COB_CODE: GO TO 0605-READNEXT-UOW-REQUEST-X
				return;
			}
			//17543I             IF URQM-ID NOT EQUAL UBOC-MSG-ID
			//17543I                 SET NO-MORE-UOW-REQUESTS TO TRUE
			//17543I                 GO TO 0605-READNEXT-UOW-REQUEST-X
			//17543I             ELSE
			//17543I                 CONTINUE
			//17543I             END-IF
		} else if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ENDFILE)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND)) {
			// COB_CODE: SET NO-MORE-UOW-REQUESTS TO TRUE
			ws.getWsClearUowRequestSwitch().setNoMoreUowRequests();
			// COB_CODE: GO TO 0605-READNEXT-UOW-REQUEST-X
			return;
		} else {
			// COB_CODE: SET NO-MORE-UOW-REQUESTS TO TRUE
			ws.getWsClearUowRequestSwitch().setNoMoreUowRequests();
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'URQM-KEY = ' URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0605-READNEXT-UOW-REQUEST'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0605-READNEXT-UOW-REQUEST");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNXT UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNXT UMT");
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0605-READNEXT-UOW-REQUEST-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETE FILE (UBOC-UOW-REQ-MSG-STORE)
		//               RIDFLD      (URQM-KEY)
		//               KEYLENGTH   (LENGTH OF URQM-KEY)
		//               RESP        (WS-RESPONSE-CODE)
		//               RESP2       (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowDAO.delete(iRowData, UrqmCommon.Len.KEY);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET NO-MORE-UOW-REQUESTS TO TRUE
			ws.getWsClearUowRequestSwitch().setNoMoreUowRequests();
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: STRING 'URQM-KEY = ' URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0605-READNEXT-UOW-REQUEST'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0605-READNEXT-UOW-REQUEST");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE UMT");
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(liUboc.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(liUboc.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(liUboc.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALRURQA", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			liUboc.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			liUboc.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}

	public void deleteArgListeners() {
		lioHalrurqaLinkage.getBusObjDataLength().deleteListener(lioHalrurqaData.getFillerLioUowBusObjDataListener());
	}

	public void registerArgListeners() {
		lioHalrurqaLinkage.getBusObjDataLength().addListener(lioHalrurqaData.getFillerLioUowBusObjDataListener());
		lioHalrurqaLinkage.getBusObjDataLength().notifyListeners();
	}
}
