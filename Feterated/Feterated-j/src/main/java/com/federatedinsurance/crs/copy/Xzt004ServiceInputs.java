/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZT004-SERVICE-INPUTS<br>
 * Variable: XZT004-SERVICE-INPUTS from copybook XZ0T0004<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzt004ServiceInputs {

	//==== PROPERTIES ====
	//Original name: XZT04I-ACCOUNT-NUMBER
	private String accountNumber = DefaultValues.stringVal(Len.ACCOUNT_NUMBER);
	//Original name: XZT04I-OWNER-STATE
	private String ownerState = DefaultValues.stringVal(Len.OWNER_STATE);
	//Original name: XZT04I-POLICY-NUMBER
	private String policyNumber = DefaultValues.stringVal(Len.POLICY_NUMBER);
	//Original name: XZT04I-POLICY-EFF-DT
	private String policyEffDt = DefaultValues.stringVal(Len.POLICY_EFF_DT);
	//Original name: XZT04I-POLICY-EXP-DT
	private String policyExpDt = DefaultValues.stringVal(Len.POLICY_EXP_DT);
	//Original name: XZT04I-POLICY-TYPE
	private String policyType = DefaultValues.stringVal(Len.POLICY_TYPE);
	//Original name: XZT04I-POLICY-CANC-DT
	private String policyCancDt = DefaultValues.stringVal(Len.POLICY_CANC_DT);
	//Original name: XZT04I-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== METHODS ====
	public byte[] getXzt004ServiceInputsBytes() {
		byte[] buffer = new byte[Len.XZT004_SERVICE_INPUTS];
		return getXzt004ServiceInputsBytes(buffer, 1);
	}

	public void setXzt004ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		accountNumber = MarshalByte.readString(buffer, position, Len.ACCOUNT_NUMBER);
		position += Len.ACCOUNT_NUMBER;
		ownerState = MarshalByte.readString(buffer, position, Len.OWNER_STATE);
		position += Len.OWNER_STATE;
		policyNumber = MarshalByte.readString(buffer, position, Len.POLICY_NUMBER);
		position += Len.POLICY_NUMBER;
		policyEffDt = MarshalByte.readString(buffer, position, Len.POLICY_EFF_DT);
		position += Len.POLICY_EFF_DT;
		policyExpDt = MarshalByte.readString(buffer, position, Len.POLICY_EXP_DT);
		position += Len.POLICY_EXP_DT;
		policyType = MarshalByte.readString(buffer, position, Len.POLICY_TYPE);
		position += Len.POLICY_TYPE;
		policyCancDt = MarshalByte.readString(buffer, position, Len.POLICY_CANC_DT);
		position += Len.POLICY_CANC_DT;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
	}

	public byte[] getXzt004ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, accountNumber, Len.ACCOUNT_NUMBER);
		position += Len.ACCOUNT_NUMBER;
		MarshalByte.writeString(buffer, position, ownerState, Len.OWNER_STATE);
		position += Len.OWNER_STATE;
		MarshalByte.writeString(buffer, position, policyNumber, Len.POLICY_NUMBER);
		position += Len.POLICY_NUMBER;
		MarshalByte.writeString(buffer, position, policyEffDt, Len.POLICY_EFF_DT);
		position += Len.POLICY_EFF_DT;
		MarshalByte.writeString(buffer, position, policyExpDt, Len.POLICY_EXP_DT);
		position += Len.POLICY_EXP_DT;
		MarshalByte.writeString(buffer, position, policyType, Len.POLICY_TYPE);
		position += Len.POLICY_TYPE;
		MarshalByte.writeString(buffer, position, policyCancDt, Len.POLICY_CANC_DT);
		position += Len.POLICY_CANC_DT;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = Functions.subString(accountNumber, Len.ACCOUNT_NUMBER);
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setOwnerState(String ownerState) {
		this.ownerState = Functions.subString(ownerState, Len.OWNER_STATE);
	}

	public String getOwnerState() {
		return this.ownerState;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = Functions.subString(policyNumber, Len.POLICY_NUMBER);
	}

	public String getPolicyNumber() {
		return this.policyNumber;
	}

	public void setPolicyEffDt(String policyEffDt) {
		this.policyEffDt = Functions.subString(policyEffDt, Len.POLICY_EFF_DT);
	}

	public String getPolicyEffDt() {
		return this.policyEffDt;
	}

	public void setPolicyExpDt(String policyExpDt) {
		this.policyExpDt = Functions.subString(policyExpDt, Len.POLICY_EXP_DT);
	}

	public String getPolicyExpDt() {
		return this.policyExpDt;
	}

	public void setPolicyType(String policyType) {
		this.policyType = Functions.subString(policyType, Len.POLICY_TYPE);
	}

	public String getPolicyType() {
		return this.policyType;
	}

	public void setPolicyCancDt(String policyCancDt) {
		this.policyCancDt = Functions.subString(policyCancDt, Len.POLICY_CANC_DT);
	}

	public String getPolicyCancDt() {
		return this.policyCancDt;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACCOUNT_NUMBER = 9;
		public static final int OWNER_STATE = 2;
		public static final int POLICY_NUMBER = 25;
		public static final int POLICY_EFF_DT = 10;
		public static final int POLICY_EXP_DT = 10;
		public static final int POLICY_TYPE = 3;
		public static final int POLICY_CANC_DT = 10;
		public static final int USERID = 8;
		public static final int XZT004_SERVICE_INPUTS = ACCOUNT_NUMBER + OWNER_STATE + POLICY_NUMBER + POLICY_EFF_DT + POLICY_EXP_DT + POLICY_TYPE
				+ POLICY_CANC_DT + USERID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
