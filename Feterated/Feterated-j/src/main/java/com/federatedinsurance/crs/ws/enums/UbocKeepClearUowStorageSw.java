/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UBOC-KEEP-CLEAR-UOW-STORAGE-SW<br>
 * Variable: UBOC-KEEP-CLEAR-UOW-STORAGE-SW from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocKeepClearUowStorageSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CLEAR_UOW_STORAGE = 'C';
	public static final char KEEP_UOW_STORAGE = 'K';

	//==== METHODS ====
	public void setUbocKeepClearUowStorageSw(char ubocKeepClearUowStorageSw) {
		this.value = ubocKeepClearUowStorageSw;
	}

	public char getUbocKeepClearUowStorageSw() {
		return this.value;
	}

	public boolean isUbocClearUowStorage() {
		return value == CLEAR_UOW_STORAGE;
	}

	public void setUbocClearUowStorage() {
		value = CLEAR_UOW_STORAGE;
	}

	public boolean isUbocKeepUowStorage() {
		return value == KEEP_UOW_STORAGE;
	}

	public void setUbocKeepUowStorage() {
		value = KEEP_UOW_STORAGE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_KEEP_CLEAR_UOW_STORAGE_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
