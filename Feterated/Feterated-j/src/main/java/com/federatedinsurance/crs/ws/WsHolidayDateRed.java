/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-HOLIDAY-DATE-RED<br>
 * Variable: WS-HOLIDAY-DATE-RED from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsHolidayDateRed {

	//==== PROPERTIES ====
	//Original name: WS-HOL-DATE-YEAR
	private String holDateYear = DefaultValues.stringVal(Len.HOL_DATE_YEAR);
	//Original name: WS-HOL-DATE-DASH1
	private char holDateDash1 = DefaultValues.CHAR_VAL;
	//Original name: WS-HOL-DATE-MONTH
	private String holDateMonth = DefaultValues.stringVal(Len.HOL_DATE_MONTH);
	//Original name: WS-HOL-DATE-DASH2
	private char holDateDash2 = DefaultValues.CHAR_VAL;
	//Original name: WS-HOL-DATE-DAY
	private String holDateDay = DefaultValues.stringVal(Len.HOL_DATE_DAY);

	//==== METHODS ====
	/**Original name: WS-HOLIDAY-DATE<br>*/
	public String getWsHolidayDate() {
		int position = 1;
		return MarshalByte.readString(getWsHolidayDateRedBytes(), position, Len.WS_HOLIDAY_DATE);
	}

	/**Original name: WS-HOLIDAY-DATE-RED<br>*/
	public byte[] getWsHolidayDateRedBytes() {
		byte[] buffer = new byte[Len.WS_HOLIDAY_DATE_RED];
		return getWsHolidayDateRedBytes(buffer, 1);
	}

	public byte[] getWsHolidayDateRedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, holDateYear, Len.HOL_DATE_YEAR);
		position += Len.HOL_DATE_YEAR;
		MarshalByte.writeChar(buffer, position, holDateDash1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, holDateMonth, Len.HOL_DATE_MONTH);
		position += Len.HOL_DATE_MONTH;
		MarshalByte.writeChar(buffer, position, holDateDash2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, holDateDay, Len.HOL_DATE_DAY);
		return buffer;
	}

	public void setHolDateYear(String holDateYear) {
		this.holDateYear = Functions.subString(holDateYear, Len.HOL_DATE_YEAR);
	}

	public String getHolDateYear() {
		return this.holDateYear;
	}

	public void setHolDateDash1(char holDateDash1) {
		this.holDateDash1 = holDateDash1;
	}

	public void setHolDateDash1Formatted(String holDateDash1) {
		setHolDateDash1(Functions.charAt(holDateDash1, Types.CHAR_SIZE));
	}

	public char getHolDateDash1() {
		return this.holDateDash1;
	}

	public void setHolDateMonth(String holDateMonth) {
		this.holDateMonth = Functions.subString(holDateMonth, Len.HOL_DATE_MONTH);
	}

	public String getHolDateMonth() {
		return this.holDateMonth;
	}

	public void setHolDateDash2(char holDateDash2) {
		this.holDateDash2 = holDateDash2;
	}

	public void setHolDateDash2Formatted(String holDateDash2) {
		setHolDateDash2(Functions.charAt(holDateDash2, Types.CHAR_SIZE));
	}

	public char getHolDateDash2() {
		return this.holDateDash2;
	}

	public void setHolDateDay(String holDateDay) {
		this.holDateDay = Functions.subString(holDateDay, Len.HOL_DATE_DAY);
	}

	public String getHolDateDay() {
		return this.holDateDay;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HOL_DATE_YEAR = 4;
		public static final int HOL_DATE_MONTH = 2;
		public static final int HOL_DATE_DAY = 2;
		public static final int HOL_DATE_DASH1 = 1;
		public static final int HOL_DATE_DASH2 = 1;
		public static final int WS_HOLIDAY_DATE_RED = HOL_DATE_YEAR + HOL_DATE_DASH1 + HOL_DATE_MONTH + HOL_DATE_DASH2 + HOL_DATE_DAY;
		public static final int WS_HOLIDAY_DATE = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_HOLIDAY_DATE = 10;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
