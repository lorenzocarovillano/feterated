/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-NOTHING-FOUND-MSG<br>
 * Variable: EA-02-NOTHING-FOUND-MSG from program XZ0B9081<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02NothingFoundMsgXz0b9081 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG
	private String flr1 = "No third";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-1
	private String flr2 = "parties";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-2
	private String flr3 = "found for";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-3
	private String flr4 = "search criteria";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-4
	private String flr5 = " entered:";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-5
	private String flr6 = "Account =";
	//Original name: EA-02-CSR-ACT-NBR
	private String ea02CsrActNbr = DefaultValues.stringVal(Len.EA02_CSR_ACT_NBR);
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-6
	private char flr7 = '.';

	//==== METHODS ====
	public String getEa02NothingFoundMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02NothingFoundMsgBytes());
	}

	public byte[] getEa02NothingFoundMsgBytes() {
		byte[] buffer = new byte[Len.EA02_NOTHING_FOUND_MSG];
		return getEa02NothingFoundMsgBytes(buffer, 1);
	}

	public byte[] getEa02NothingFoundMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea02CsrActNbr, Len.EA02_CSR_ACT_NBR);
		position += Len.EA02_CSR_ACT_NBR;
		MarshalByte.writeChar(buffer, position, flr7);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setEa02CsrActNbr(String ea02CsrActNbr) {
		this.ea02CsrActNbr = Functions.subString(ea02CsrActNbr, Len.EA02_CSR_ACT_NBR);
	}

	public String getEa02CsrActNbr() {
		return this.ea02CsrActNbr;
	}

	public char getFlr7() {
		return this.flr7;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA02_CSR_ACT_NBR = 9;
		public static final int FLR1 = 9;
		public static final int FLR2 = 8;
		public static final int FLR3 = 10;
		public static final int FLR4 = 15;
		public static final int FLR7 = 1;
		public static final int EA02_NOTHING_FOUND_MSG = EA02_CSR_ACT_NBR + FLR1 + FLR2 + 3 * FLR3 + FLR4 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
