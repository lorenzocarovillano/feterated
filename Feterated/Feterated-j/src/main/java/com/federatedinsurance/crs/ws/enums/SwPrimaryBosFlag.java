/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-PRIMARY-BOS-FLAG<br>
 * Variable: SW-PRIMARY-BOS-FLAG from program TS020000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwPrimaryBosFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FIRST_PRIMARY_BO = 'F';
	public static final char NEXT_PRIMARY_BO = 'X';
	public static final char NO_MORE_PRIMARY_BOS = 'N';

	//==== METHODS ====
	public void setPrimaryBosFlag(char primaryBosFlag) {
		this.value = primaryBosFlag;
	}

	public char getPrimaryBosFlag() {
		return this.value;
	}

	public boolean isFirstPrimaryBo() {
		return value == FIRST_PRIMARY_BO;
	}

	public void setFirstPrimaryBo() {
		value = FIRST_PRIMARY_BO;
	}

	public void setNextPrimaryBo() {
		value = NEXT_PRIMARY_BO;
	}

	public boolean isNoMorePrimaryBos() {
		return value == NO_MORE_PRIMARY_BOS;
	}

	public void setNoMorePrimaryBos() {
		value = NO_MORE_PRIMARY_BOS;
	}
}
