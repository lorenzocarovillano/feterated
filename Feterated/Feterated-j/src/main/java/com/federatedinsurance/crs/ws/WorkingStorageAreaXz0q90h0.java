/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsOperationNameXz0x90h0;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0Q90H0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0q90h0 {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0Q90H0";
	//Original name: WS-OPERATION-NAME
	private WsOperationNameXz0x90h0 operationName = new WsOperationNameXz0x90h0();

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public WsOperationNameXz0x90h0 getOperationName() {
		return operationName;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
