/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: SCRATCH-AREA<br>
 * Variable: SCRATCH-AREA from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class ScratchArea extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final char DATE_FUNCTION_EDIT = '0';
	public static final char DATE_INPUT_PROCESS = '1';
	public static final char DATE_OUT_NO_ADJ = '2';
	public static final char DATE_OUT_PROCESS = '3';
	public static final char DATE_DIFF_FOR_OUT = '4';

	//==== CONSTRUCTORS ====
	public ScratchArea() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.SCRATCH_AREA;
	}

	public void setScratchArea(String scratchArea) {
		writeString(Pos.SCRATCH_AREA, scratchArea, Len.SCRATCH_AREA);
	}

	public void setScratchAreaFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.SCRATCH_AREA, Pos.SCRATCH_AREA);
	}

	public void setScratchAreaSubstring(String replacement, int start, int length) {
		writeSubstring(Pos.SCRATCH_AREA, replacement, start, length, Len.SCRATCH_AREA);
	}

	/**Original name: SCRATCH-AREA<br>
	 * <pre>              AMPM OR OTHER MORNING INDICATOR
	 *               SPACES FOR 24 HOUR CLOCK
	 *               - ON WORKSTATION ONLY:
	 *               ? GET INDICATOR FROM OS/2 CONTROL PANEL</pre>*/
	public String getScratchArea() {
		return readString(Pos.SCRATCH_AREA, Len.SCRATCH_AREA);
	}

	public byte[] getScratchAreaAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.SCRATCH_AREA, Pos.SCRATCH_AREA);
		return buffer;
	}

	public void setDateExtendedError(String dateExtendedError) {
		writeString(Pos.DATE_EXTENDED_ERROR, dateExtendedError, Len.DATE_EXTENDED_ERROR);
	}

	/**Original name: DATE-EXTENDED-ERROR<br>*/
	public String getDateExtendedError() {
		return readString(Pos.DATE_EXTENDED_ERROR, Len.DATE_EXTENDED_ERROR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int SCRATCH_AREA = 1;
		public static final int SCRATCH_AREA_REDEF = 1;
		public static final int POSITION_IN_PROCESS = SCRATCH_AREA_REDEF;
		public static final int DATE_EXTENDED_ERROR = POSITION_IN_PROCESS + Len.POSITION_IN_PROCESS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int POSITION_IN_PROCESS = 1;
		public static final int SCRATCH_AREA = 300;
		public static final int DATE_EXTENDED_ERROR = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
