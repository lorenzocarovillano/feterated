/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;

/**Original name: O-IN-HOUSE-STRUCT-198-FILE<br>
 * File: O-IN-HOUSE-STRUCT-198-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OInHouseStruct198FileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: IHO-STRUCT-198-CARRIAGE-CTRL
	private String ihoStruct198CarriageCtrl = DefaultValues.stringVal(Len.IHO_STRUCT198_CARRIAGE_CTRL);
	//Original name: IHO-STRUCT-198-DATA
	private String ihoStruct198Data = DefaultValues.stringVal(Len.IHO_STRUCT198_DATA);

	//==== METHODS ====
	public void setoInHouseStruct198RecordBytes(byte[] buffer, int offset) {
		int position = offset;
		setIhoStruct198CarriageCtrlXBytes(buffer, position);
		position += Len.IHO_STRUCT198_CARRIAGE_CTRL_X;
		ihoStruct198Data = MarshalByte.readString(buffer, position, Len.IHO_STRUCT198_DATA);
	}

	public byte[] getoInHouseStruct198RecordBytes(byte[] buffer, int offset) {
		int position = offset;
		getIhoStruct198CarriageCtrlXBytes(buffer, position);
		position += Len.IHO_STRUCT198_CARRIAGE_CTRL_X;
		MarshalByte.writeString(buffer, position, ihoStruct198Data, Len.IHO_STRUCT198_DATA);
		return buffer;
	}

	public void setIhoStruct198CarriageCtrlXFormatted(String data) {
		byte[] buffer = new byte[Len.IHO_STRUCT198_CARRIAGE_CTRL_X];
		MarshalByte.writeString(buffer, 1, data, Len.IHO_STRUCT198_CARRIAGE_CTRL_X);
		setIhoStruct198CarriageCtrlXBytes(buffer, 1);
	}

	public void setIhoStruct198CarriageCtrlXBytes(byte[] buffer, int offset) {
		int position = offset;
		ihoStruct198CarriageCtrl = MarshalByte.readFixedString(buffer, position, Len.IHO_STRUCT198_CARRIAGE_CTRL);
	}

	public byte[] getIhoStruct198CarriageCtrlXBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ihoStruct198CarriageCtrl, Len.IHO_STRUCT198_CARRIAGE_CTRL);
		return buffer;
	}

	public void setIhoStruct198Data(String ihoStruct198Data) {
		this.ihoStruct198Data = Functions.subString(ihoStruct198Data, Len.IHO_STRUCT198_DATA);
	}

	public String getIhoStruct198Data() {
		return this.ihoStruct198Data;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoInHouseStruct198RecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoInHouseStruct198RecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_IN_HOUSE_STRUCT198_RECORD;
	}

	@Override
	public IBuffer copy() {
		OInHouseStruct198FileTO copyTO = new OInHouseStruct198FileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int IHO_STRUCT198_CARRIAGE_CTRL = 1;
		public static final int IHO_STRUCT198_CARRIAGE_CTRL_X = IHO_STRUCT198_CARRIAGE_CTRL;
		public static final int IHO_STRUCT198_DATA = 198;
		public static final int O_IN_HOUSE_STRUCT198_RECORD = IHO_STRUCT198_CARRIAGE_CTRL_X + IHO_STRUCT198_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
