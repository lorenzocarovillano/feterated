/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotWrdStDes;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [ACT_NOT_WRD, ST_WRD_DES]
 * 
 */
public class ActNotWrdStDesDao extends BaseSqlDao<IActNotWrdStDes> {

	private Cursor wordingCsr;
	private final IRowMapper<IActNotWrdStDes> fetchWordingCsrRm = buildNamedRowMapper(IActNotWrdStDes.class, "seqCd", "fontTypObj", "txt");

	public ActNotWrdStDesDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotWrdStDes> getToClass() {
		return IActNotWrdStDes.class;
	}

	public DbAccessStatus openWordingCsr(String csrActNbr, String notPrcTs, String wsPolList, char cfYes) {
		wordingCsr = buildQuery("openWordingCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("wsPolList", wsPolList)
				.bind("cfYes", String.valueOf(cfYes)).open();
		return dbStatus;
	}

	public IActNotWrdStDes fetchWordingCsr(IActNotWrdStDes iActNotWrdStDes) {
		return fetch(wordingCsr, iActNotWrdStDes, fetchWordingCsrRm);
	}

	public DbAccessStatus closeWordingCsr() {
		return closeCursor(wordingCsr);
	}
}
