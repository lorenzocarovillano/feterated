/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: HUFSC-HAL-UOW-FUN-SEC-DATA<br>
 * Variable: HUFSC-HAL-UOW-FUN-SEC-DATA from copybook HALLCUFS<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class HufscHalUowFunSecData {

	//==== PROPERTIES ====
	/**Original name: HUFSC-AUT-ACTION-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char autActionCdCi = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-AUT-ACTION-CD
	private String autActionCd = DefaultValues.stringVal(Len.AUT_ACTION_CD);
	//Original name: HUFSC-AUT-FUN-NM-CI
	private char autFunNmCi = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-AUT-FUN-NM
	private String autFunNm = DefaultValues.stringVal(Len.AUT_FUN_NM);
	//Original name: HUFSC-LINK-TO-MDU-NM-CI
	private char linkToMduNmCi = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-LINK-TO-MDU-NM
	private String linkToMduNm = DefaultValues.stringVal(Len.LINK_TO_MDU_NM);
	//Original name: HUFSC-APP-NM-CI
	private char appNmCi = DefaultValues.CHAR_VAL;
	//Original name: HUFSC-APP-NM
	private String appNm = DefaultValues.stringVal(Len.APP_NM);

	//==== METHODS ====
	public byte[] getHalUowFunSecDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, autActionCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, autActionCd, Len.AUT_ACTION_CD);
		position += Len.AUT_ACTION_CD;
		MarshalByte.writeChar(buffer, position, autFunNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, autFunNm, Len.AUT_FUN_NM);
		position += Len.AUT_FUN_NM;
		MarshalByte.writeChar(buffer, position, linkToMduNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, linkToMduNm, Len.LINK_TO_MDU_NM);
		position += Len.LINK_TO_MDU_NM;
		MarshalByte.writeChar(buffer, position, appNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, appNm, Len.APP_NM);
		return buffer;
	}

	public void initHalUowFunSecDataSpaces() {
		autActionCdCi = Types.SPACE_CHAR;
		autActionCd = "";
		autFunNmCi = Types.SPACE_CHAR;
		autFunNm = "";
		linkToMduNmCi = Types.SPACE_CHAR;
		linkToMduNm = "";
		appNmCi = Types.SPACE_CHAR;
		appNm = "";
	}

	public char getAutActionCdCi() {
		return this.autActionCdCi;
	}

	public void setAutActionCd(String autActionCd) {
		this.autActionCd = Functions.subString(autActionCd, Len.AUT_ACTION_CD);
	}

	public String getAutActionCd() {
		return this.autActionCd;
	}

	public char getAutFunNmCi() {
		return this.autFunNmCi;
	}

	public void setAutFunNm(String autFunNm) {
		this.autFunNm = Functions.subString(autFunNm, Len.AUT_FUN_NM);
	}

	public String getAutFunNm() {
		return this.autFunNm;
	}

	public char getLinkToMduNmCi() {
		return this.linkToMduNmCi;
	}

	public String getLinkToMduNm() {
		return this.linkToMduNm;
	}

	public char getAppNmCi() {
		return this.appNmCi;
	}

	public void setAppNm(String appNm) {
		this.appNm = Functions.subString(appNm, Len.APP_NM);
	}

	public String getAppNm() {
		return this.appNm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int AUT_ACTION_CD = 32;
		public static final int AUT_FUN_NM = 32;
		public static final int LINK_TO_MDU_NM = 8;
		public static final int APP_NM = 8;
		public static final int AUT_ACTION_CD_CI = 1;
		public static final int AUT_FUN_NM_CI = 1;
		public static final int LINK_TO_MDU_NM_CI = 1;
		public static final int APP_NM_CI = 1;
		public static final int HAL_UOW_FUN_SEC_DATA = AUT_ACTION_CD_CI + AUT_ACTION_CD + AUT_FUN_NM_CI + AUT_FUN_NM + LINK_TO_MDU_NM_CI
				+ LINK_TO_MDU_NM + APP_NM_CI + APP_NM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
