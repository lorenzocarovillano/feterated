/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: FILLER-EA-01-PARM-NOT-FOUND-4<br>
 * Variable: FILLER-EA-01-PARM-NOT-FOUND-4 from program TS547099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FillerEa01ParmNotFound4 {

	//==== PROPERTIES ====
	private String value = "";
	public static final String CICS_REGION_PARM = "CICS REGION (APPL ID)";
	public static final String CICS_PROGRAM_NM_PARM = "CICS PROGRAM NAME";
	public static final String TRAN_ID_PARM = "TRAN ID";

	//==== METHODS ====
	public void setFlr5(String flr5) {
		this.value = Functions.subString(flr5, Len.FLR5);
	}

	public String getFlr5() {
		return this.value;
	}

	public void setCicsRegionParm() {
		value = CICS_REGION_PARM;
	}

	public void setCicsProgramNmParm() {
		value = CICS_PROGRAM_NM_PARM;
	}

	public void setTranIdParm() {
		value = TRAN_ID_PARM;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR5 = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
