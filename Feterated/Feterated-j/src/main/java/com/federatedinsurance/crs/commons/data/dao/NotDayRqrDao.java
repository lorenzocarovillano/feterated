/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.INotDayRqr;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [NOT_DAY_RQR]
 * 
 */
public class NotDayRqrDao extends BaseSqlDao<INotDayRqr> {

	public NotDayRqrDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<INotDayRqr> getToClass() {
		return INotDayRqr.class;
	}

	public short selectRec(String stAbb, String actNotTypCd, String polTypCd, short dft) {
		return buildQuery("selectRec").bind("stAbb", stAbb).bind("actNotTypCd", actNotTypCd).bind("polTypCd", polTypCd).scalarResultShort(dft);
	}

	public short selectRec1(INotDayRqr iNotDayRqr, short dft) {
		return buildQuery("selectRec1").bind(iNotDayRqr).scalarResultShort(dft);
	}
}
