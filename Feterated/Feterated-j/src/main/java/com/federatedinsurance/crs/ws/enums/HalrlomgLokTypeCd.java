/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: HALRLOMG-LOK-TYPE-CD<br>
 * Variable: HALRLOMG-LOK-TYPE-CD from copybook HALLLOMG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HalrlomgLokTypeCd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char LOCK_PES_READUPDT = 'P';
	public static final char LOCK_PES_READONLY = 'R';
	public static final char LOCK_OPTIMISTIC = 'O';
	public static final char LOCK_DUMMY = 'D';
	public static final char LOCK_GROUP_PESSI = 'G';
	public static final char NO_LOCK_NEEDED = 'N';

	//==== METHODS ====
	public void setLokTypeCd(char lokTypeCd) {
		this.value = lokTypeCd;
	}

	public char getLokTypeCd() {
		return this.value;
	}

	public boolean isLockPesReadupdt() {
		return value == LOCK_PES_READUPDT;
	}

	public void setHalrlomgLockPesReadupdt() {
		value = LOCK_PES_READUPDT;
	}

	public boolean isHalrlomgLockOptimistic() {
		return value == LOCK_OPTIMISTIC;
	}

	public void setHalrlomgLockOptimistic() {
		value = LOCK_OPTIMISTIC;
	}

	public boolean isLockGroupPessi() {
		return value == LOCK_GROUP_PESSI;
	}

	public void setHalrlomgLockGroupPessi() {
		value = LOCK_GROUP_PESSI;
	}

	public boolean isHalrlomgNoLockNeeded() {
		return value == NO_LOCK_NEEDED;
	}

	public void setHalrlomgNoLockNeeded() {
		value = NO_LOCK_NEEDED;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LOK_TYPE_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
