/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IFedRmtPrtTab;

/**Original name: DCLFED-RMT-PRT-TAB<br>
 * Variable: DCLFED-RMT-PRT-TAB from copybook DBH00002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclfedRmtPrtTab implements IFedRmtPrtTab {

	//==== PROPERTIES ====
	//Original name: RPT-NBR
	private String rptNbr = DefaultValues.stringVal(Len.RPT_NBR);
	//Original name: RPT-DESC
	private String rptDesc = DefaultValues.stringVal(Len.RPT_DESC);
	//Original name: RPT-ACT-FLG
	private char rptActFlg = DefaultValues.CHAR_VAL;
	//Original name: RPT-BAL-FLG
	private char rptBalFlg = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-1
	private String offLoc1 = DefaultValues.stringVal(Len.OFF_LOC1);
	//Original name: OFF-LOC-FLG-1
	private char offLocFlg1 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-2
	private String offLoc2 = DefaultValues.stringVal(Len.OFF_LOC2);
	//Original name: OFF-LOC-FLG-2
	private char offLocFlg2 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-3
	private String offLoc3 = DefaultValues.stringVal(Len.OFF_LOC3);
	//Original name: OFF-LOC-FLG-3
	private char offLocFlg3 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-4
	private String offLoc4 = DefaultValues.stringVal(Len.OFF_LOC4);
	//Original name: OFF-LOC-FLG-4
	private char offLocFlg4 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-5
	private String offLoc5 = DefaultValues.stringVal(Len.OFF_LOC5);
	//Original name: OFF-LOC-FLG-5
	private char offLocFlg5 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-6
	private String offLoc6 = DefaultValues.stringVal(Len.OFF_LOC6);
	//Original name: OFF-LOC-FLG-6
	private char offLocFlg6 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-7
	private String offLoc7 = DefaultValues.stringVal(Len.OFF_LOC7);
	//Original name: OFF-LOC-FLG-7
	private char offLocFlg7 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-8
	private String offLoc8 = DefaultValues.stringVal(Len.OFF_LOC8);
	//Original name: OFF-LOC-FLG-8
	private char offLocFlg8 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-9
	private String offLoc9 = DefaultValues.stringVal(Len.OFF_LOC9);
	//Original name: OFF-LOC-FLG-9
	private char offLocFlg9 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-10
	private String offLoc10 = DefaultValues.stringVal(Len.OFF_LOC10);
	//Original name: OFF-LOC-FLG-10
	private char offLocFlg10 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-11
	private String offLoc11 = DefaultValues.stringVal(Len.OFF_LOC11);
	//Original name: OFF-LOC-FLG-11
	private char offLocFlg11 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-12
	private String offLoc12 = DefaultValues.stringVal(Len.OFF_LOC12);
	//Original name: OFF-LOC-FLG-12
	private char offLocFlg12 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-13
	private String offLoc13 = DefaultValues.stringVal(Len.OFF_LOC13);
	//Original name: OFF-LOC-FLG-13
	private char offLocFlg13 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-14
	private String offLoc14 = DefaultValues.stringVal(Len.OFF_LOC14);
	//Original name: OFF-LOC-FLG-14
	private char offLocFlg14 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-15
	private String offLoc15 = DefaultValues.stringVal(Len.OFF_LOC15);
	//Original name: OFF-LOC-FLG-15
	private char offLocFlg15 = DefaultValues.CHAR_VAL;
	//Original name: OFF-LOC-DEFLT-FLG
	private char offLocDefltFlg = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public String getDclfedRmtPrtTabFormatted() {
		return MarshalByteExt.bufferToStr(getDclfedRmtPrtTabBytes());
	}

	public byte[] getDclfedRmtPrtTabBytes() {
		byte[] buffer = new byte[Len.DCLFED_RMT_PRT_TAB];
		return getDclfedRmtPrtTabBytes(buffer, 1);
	}

	public byte[] getDclfedRmtPrtTabBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, rptNbr, Len.RPT_NBR);
		position += Len.RPT_NBR;
		MarshalByte.writeString(buffer, position, rptDesc, Len.RPT_DESC);
		position += Len.RPT_DESC;
		MarshalByte.writeChar(buffer, position, rptActFlg);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, rptBalFlg);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc1, Len.OFF_LOC1);
		position += Len.OFF_LOC1;
		MarshalByte.writeChar(buffer, position, offLocFlg1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc2, Len.OFF_LOC2);
		position += Len.OFF_LOC2;
		MarshalByte.writeChar(buffer, position, offLocFlg2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc3, Len.OFF_LOC3);
		position += Len.OFF_LOC3;
		MarshalByte.writeChar(buffer, position, offLocFlg3);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc4, Len.OFF_LOC4);
		position += Len.OFF_LOC4;
		MarshalByte.writeChar(buffer, position, offLocFlg4);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc5, Len.OFF_LOC5);
		position += Len.OFF_LOC5;
		MarshalByte.writeChar(buffer, position, offLocFlg5);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc6, Len.OFF_LOC6);
		position += Len.OFF_LOC6;
		MarshalByte.writeChar(buffer, position, offLocFlg6);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc7, Len.OFF_LOC7);
		position += Len.OFF_LOC7;
		MarshalByte.writeChar(buffer, position, offLocFlg7);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc8, Len.OFF_LOC8);
		position += Len.OFF_LOC8;
		MarshalByte.writeChar(buffer, position, offLocFlg8);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc9, Len.OFF_LOC9);
		position += Len.OFF_LOC9;
		MarshalByte.writeChar(buffer, position, offLocFlg9);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc10, Len.OFF_LOC10);
		position += Len.OFF_LOC10;
		MarshalByte.writeChar(buffer, position, offLocFlg10);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc11, Len.OFF_LOC11);
		position += Len.OFF_LOC11;
		MarshalByte.writeChar(buffer, position, offLocFlg11);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc12, Len.OFF_LOC12);
		position += Len.OFF_LOC12;
		MarshalByte.writeChar(buffer, position, offLocFlg12);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc13, Len.OFF_LOC13);
		position += Len.OFF_LOC13;
		MarshalByte.writeChar(buffer, position, offLocFlg13);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc14, Len.OFF_LOC14);
		position += Len.OFF_LOC14;
		MarshalByte.writeChar(buffer, position, offLocFlg14);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, offLoc15, Len.OFF_LOC15);
		position += Len.OFF_LOC15;
		MarshalByte.writeChar(buffer, position, offLocFlg15);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, offLocDefltFlg);
		return buffer;
	}

	@Override
	public void setRptNbr(String rptNbr) {
		this.rptNbr = Functions.subString(rptNbr, Len.RPT_NBR);
	}

	@Override
	public String getRptNbr() {
		return this.rptNbr;
	}

	@Override
	public void setRptDesc(String rptDesc) {
		this.rptDesc = Functions.subString(rptDesc, Len.RPT_DESC);
	}

	@Override
	public String getRptDesc() {
		return this.rptDesc;
	}

	@Override
	public void setRptActFlg(char rptActFlg) {
		this.rptActFlg = rptActFlg;
	}

	@Override
	public char getRptActFlg() {
		return this.rptActFlg;
	}

	@Override
	public void setRptBalFlg(char rptBalFlg) {
		this.rptBalFlg = rptBalFlg;
	}

	@Override
	public char getRptBalFlg() {
		return this.rptBalFlg;
	}

	@Override
	public void setOffLoc1(String offLoc1) {
		this.offLoc1 = Functions.subString(offLoc1, Len.OFF_LOC1);
	}

	@Override
	public String getOffLoc1() {
		return this.offLoc1;
	}

	@Override
	public void setOffLocFlg1(char offLocFlg1) {
		this.offLocFlg1 = offLocFlg1;
	}

	@Override
	public char getOffLocFlg1() {
		return this.offLocFlg1;
	}

	@Override
	public void setOffLoc2(String offLoc2) {
		this.offLoc2 = Functions.subString(offLoc2, Len.OFF_LOC2);
	}

	@Override
	public String getOffLoc2() {
		return this.offLoc2;
	}

	@Override
	public void setOffLocFlg2(char offLocFlg2) {
		this.offLocFlg2 = offLocFlg2;
	}

	@Override
	public char getOffLocFlg2() {
		return this.offLocFlg2;
	}

	@Override
	public void setOffLoc3(String offLoc3) {
		this.offLoc3 = Functions.subString(offLoc3, Len.OFF_LOC3);
	}

	@Override
	public String getOffLoc3() {
		return this.offLoc3;
	}

	@Override
	public void setOffLocFlg3(char offLocFlg3) {
		this.offLocFlg3 = offLocFlg3;
	}

	@Override
	public char getOffLocFlg3() {
		return this.offLocFlg3;
	}

	@Override
	public void setOffLoc4(String offLoc4) {
		this.offLoc4 = Functions.subString(offLoc4, Len.OFF_LOC4);
	}

	@Override
	public String getOffLoc4() {
		return this.offLoc4;
	}

	@Override
	public void setOffLocFlg4(char offLocFlg4) {
		this.offLocFlg4 = offLocFlg4;
	}

	@Override
	public char getOffLocFlg4() {
		return this.offLocFlg4;
	}

	@Override
	public void setOffLoc5(String offLoc5) {
		this.offLoc5 = Functions.subString(offLoc5, Len.OFF_LOC5);
	}

	@Override
	public String getOffLoc5() {
		return this.offLoc5;
	}

	@Override
	public void setOffLocFlg5(char offLocFlg5) {
		this.offLocFlg5 = offLocFlg5;
	}

	@Override
	public char getOffLocFlg5() {
		return this.offLocFlg5;
	}

	@Override
	public void setOffLoc6(String offLoc6) {
		this.offLoc6 = Functions.subString(offLoc6, Len.OFF_LOC6);
	}

	@Override
	public String getOffLoc6() {
		return this.offLoc6;
	}

	@Override
	public void setOffLocFlg6(char offLocFlg6) {
		this.offLocFlg6 = offLocFlg6;
	}

	@Override
	public char getOffLocFlg6() {
		return this.offLocFlg6;
	}

	@Override
	public void setOffLoc7(String offLoc7) {
		this.offLoc7 = Functions.subString(offLoc7, Len.OFF_LOC7);
	}

	@Override
	public String getOffLoc7() {
		return this.offLoc7;
	}

	@Override
	public void setOffLocFlg7(char offLocFlg7) {
		this.offLocFlg7 = offLocFlg7;
	}

	@Override
	public char getOffLocFlg7() {
		return this.offLocFlg7;
	}

	@Override
	public void setOffLoc8(String offLoc8) {
		this.offLoc8 = Functions.subString(offLoc8, Len.OFF_LOC8);
	}

	@Override
	public String getOffLoc8() {
		return this.offLoc8;
	}

	@Override
	public void setOffLocFlg8(char offLocFlg8) {
		this.offLocFlg8 = offLocFlg8;
	}

	@Override
	public char getOffLocFlg8() {
		return this.offLocFlg8;
	}

	@Override
	public void setOffLoc9(String offLoc9) {
		this.offLoc9 = Functions.subString(offLoc9, Len.OFF_LOC9);
	}

	@Override
	public String getOffLoc9() {
		return this.offLoc9;
	}

	@Override
	public void setOffLocFlg9(char offLocFlg9) {
		this.offLocFlg9 = offLocFlg9;
	}

	@Override
	public char getOffLocFlg9() {
		return this.offLocFlg9;
	}

	@Override
	public void setOffLoc10(String offLoc10) {
		this.offLoc10 = Functions.subString(offLoc10, Len.OFF_LOC10);
	}

	@Override
	public String getOffLoc10() {
		return this.offLoc10;
	}

	@Override
	public void setOffLocFlg10(char offLocFlg10) {
		this.offLocFlg10 = offLocFlg10;
	}

	@Override
	public char getOffLocFlg10() {
		return this.offLocFlg10;
	}

	@Override
	public void setOffLoc11(String offLoc11) {
		this.offLoc11 = Functions.subString(offLoc11, Len.OFF_LOC11);
	}

	@Override
	public String getOffLoc11() {
		return this.offLoc11;
	}

	@Override
	public void setOffLocFlg11(char offLocFlg11) {
		this.offLocFlg11 = offLocFlg11;
	}

	@Override
	public char getOffLocFlg11() {
		return this.offLocFlg11;
	}

	@Override
	public void setOffLoc12(String offLoc12) {
		this.offLoc12 = Functions.subString(offLoc12, Len.OFF_LOC12);
	}

	@Override
	public String getOffLoc12() {
		return this.offLoc12;
	}

	@Override
	public void setOffLocFlg12(char offLocFlg12) {
		this.offLocFlg12 = offLocFlg12;
	}

	@Override
	public char getOffLocFlg12() {
		return this.offLocFlg12;
	}

	@Override
	public void setOffLoc13(String offLoc13) {
		this.offLoc13 = Functions.subString(offLoc13, Len.OFF_LOC13);
	}

	@Override
	public String getOffLoc13() {
		return this.offLoc13;
	}

	@Override
	public void setOffLocFlg13(char offLocFlg13) {
		this.offLocFlg13 = offLocFlg13;
	}

	@Override
	public char getOffLocFlg13() {
		return this.offLocFlg13;
	}

	@Override
	public void setOffLoc14(String offLoc14) {
		this.offLoc14 = Functions.subString(offLoc14, Len.OFF_LOC14);
	}

	@Override
	public String getOffLoc14() {
		return this.offLoc14;
	}

	@Override
	public void setOffLocFlg14(char offLocFlg14) {
		this.offLocFlg14 = offLocFlg14;
	}

	@Override
	public char getOffLocFlg14() {
		return this.offLocFlg14;
	}

	@Override
	public void setOffLoc15(String offLoc15) {
		this.offLoc15 = Functions.subString(offLoc15, Len.OFF_LOC15);
	}

	@Override
	public String getOffLoc15() {
		return this.offLoc15;
	}

	@Override
	public void setOffLocFlg15(char offLocFlg15) {
		this.offLocFlg15 = offLocFlg15;
	}

	@Override
	public char getOffLocFlg15() {
		return this.offLocFlg15;
	}

	@Override
	public void setOffLocDefltFlg(char offLocDefltFlg) {
		this.offLocDefltFlg = offLocDefltFlg;
	}

	@Override
	public char getOffLocDefltFlg() {
		return this.offLocDefltFlg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RPT_NBR = 6;
		public static final int RPT_DESC = 80;
		public static final int OFF_LOC1 = 2;
		public static final int OFF_LOC2 = 2;
		public static final int OFF_LOC3 = 2;
		public static final int OFF_LOC4 = 2;
		public static final int OFF_LOC5 = 2;
		public static final int OFF_LOC6 = 2;
		public static final int OFF_LOC7 = 2;
		public static final int OFF_LOC8 = 2;
		public static final int OFF_LOC9 = 2;
		public static final int OFF_LOC10 = 2;
		public static final int OFF_LOC11 = 2;
		public static final int OFF_LOC12 = 2;
		public static final int OFF_LOC13 = 2;
		public static final int OFF_LOC14 = 2;
		public static final int OFF_LOC15 = 2;
		public static final int RPT_ACT_FLG = 1;
		public static final int RPT_BAL_FLG = 1;
		public static final int OFF_LOC_FLG1 = 1;
		public static final int OFF_LOC_FLG2 = 1;
		public static final int OFF_LOC_FLG3 = 1;
		public static final int OFF_LOC_FLG4 = 1;
		public static final int OFF_LOC_FLG5 = 1;
		public static final int OFF_LOC_FLG6 = 1;
		public static final int OFF_LOC_FLG7 = 1;
		public static final int OFF_LOC_FLG8 = 1;
		public static final int OFF_LOC_FLG9 = 1;
		public static final int OFF_LOC_FLG10 = 1;
		public static final int OFF_LOC_FLG11 = 1;
		public static final int OFF_LOC_FLG12 = 1;
		public static final int OFF_LOC_FLG13 = 1;
		public static final int OFF_LOC_FLG14 = 1;
		public static final int OFF_LOC_FLG15 = 1;
		public static final int OFF_LOC_DEFLT_FLG = 1;
		public static final int DCLFED_RMT_PRT_TAB = RPT_NBR + RPT_DESC + RPT_ACT_FLG + RPT_BAL_FLG + OFF_LOC1 + OFF_LOC_FLG1 + OFF_LOC2
				+ OFF_LOC_FLG2 + OFF_LOC3 + OFF_LOC_FLG3 + OFF_LOC4 + OFF_LOC_FLG4 + OFF_LOC5 + OFF_LOC_FLG5 + OFF_LOC6 + OFF_LOC_FLG6 + OFF_LOC7
				+ OFF_LOC_FLG7 + OFF_LOC8 + OFF_LOC_FLG8 + OFF_LOC9 + OFF_LOC_FLG9 + OFF_LOC10 + OFF_LOC_FLG10 + OFF_LOC11 + OFF_LOC_FLG11 + OFF_LOC12
				+ OFF_LOC_FLG12 + OFF_LOC13 + OFF_LOC_FLG13 + OFF_LOC14 + OFF_LOC_FLG14 + OFF_LOC15 + OFF_LOC_FLG15 + OFF_LOC_DEFLT_FLG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
