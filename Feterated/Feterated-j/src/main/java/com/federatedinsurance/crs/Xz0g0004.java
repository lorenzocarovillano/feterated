/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.federatedinsurance.crs.copy.CommunicationShellCommon;
import com.federatedinsurance.crs.copy.DriverSpecificData;
import com.federatedinsurance.crs.ws.DfhcommareaXz0g0004;
import com.federatedinsurance.crs.ws.ProxyProgramCommonXz0g0004;
import com.federatedinsurance.crs.ws.Xz0g0004Data;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0004;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;

/**Original name: XZ0G0004<br>
 * <pre>AUTHOR.       GREG KOETTER.
 * DATE-WRITTEN. 17 SEP 2008.
 * ***************************************************************
 *   PROGRAM TITLE - BATCH COBOL FRAMEWORK PROXY PROGRAM FOR     *
 *                     UOW        :XZ_GET_RESCIND_IND            *
 *                     OPERATIONS :GetRescindInd                 *
 *                                                               *
 *   PLATFORM - HOST CICS - CALLED FROM A BATCH PROGRAM          *
 *                                                               *
 *   PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
 *              EXECUTION OF FRAMEWORK MAIN DRIVER               *
 *                                                               *
 *   PROGRAM INITIATION - LINKED FROM A COBOL BATCH PROGRAM      *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
 *                         OUTPUT RETURNED VIA DFHCOMMAREA       *
 *                         INPUT AND OUTPUT ARE SENT TO MAIN     *
 *                         PROXY PROGRAM THROUGH MEMORY AND      *
 *                         DFHCOMMAREA (PROXY-PROGRAM-COMMON)    *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    07JUL05   E404LJL   INITIAL TEMPLATE VERSION        *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TO07614  17SEP08   E404GRK   INITIAL PROGRAM                 *
 *                                                               *
 * ***************************************************************</pre>*/
public class Xz0g0004 extends Program {

	//==== PROPERTIES ====
	private ExecContext execContext = null;
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	//Original name: WORKING-STORAGE
	private Xz0g0004Data ws = new Xz0g0004Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaXz0g0004 dfhcommarea;
	/**Original name: L-SERVICE-CONTRACT-AREA<br>
	 * <pre>* REQUIRED TO CALL MAIN PROXY APPLICTION.</pre>*/
	private LServiceContractAreaXz0x0004 lServiceContractArea = new LServiceContractAreaXz0x0004(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaXz0g0004 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0g0004 getInstance() {
		return (Programs.getInstance(Xz0g0004.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getProxyProgramCommon().getErrorReturnCode().isFatalErrorCode()
				|| dfhcommarea.getProxyProgramCommon().getErrorReturnCode().isNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-CALL-FRAMEWORK
		//              THRU 3000-EXIT.
		callFramework();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getProxyProgramCommon().getErrorReturnCode().isFatalErrorCode()
				|| dfhcommarea.getProxyProgramCommon().getErrorReturnCode().isNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: PERFORM 8000-ENDING-HOUSEKEEPING
		//              THRU 8000-EXIT.
		endingHousekeeping();
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM INITIALIZATION AND OTHER SETUP TASKS                 *
	 *  SERVICE INPUT AND SERVICE OUTPUT AREAS HAVE BEEN ALLOCATED   *
	 *  BY EITHER IVORY OR THE CALLING (CONSUMING) COBOL PROGRAM.    *
	 *  GET ADDRESSABILITY TO THEM VIA THE POINTER PASSED IN.        *
	 * ***************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE DSD-ERROR-HANDLING-PARMS
		//                      PPC-ERROR-HANDLING-PARMS.
		initErrorHandlingParms();
		initErrorHandlingParms1();
		//***************************************************************
		// FRAMEWORK FORMAT MEMORY NEEDS TO BE ALLOCATED BY THIS        *
		// PROGRAM BEFORE CALLING THE MAIN DRIVER.                      *
		//***************************************************************
		// COB_CODE: PERFORM 2100-ALLOCATE-FW-MEMORY
		//              THRU 2100-EXIT.
		allocateFwMemory();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getProxyProgramCommon().getErrorReturnCode().isFatalErrorCode()
				|| dfhcommarea.getProxyProgramCommon().getErrorReturnCode().isNlbeCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-ALLOCATE-FW-MEMORY<br>
	 * <pre>***************************************************************
	 *  ALLOCATE THE ADDRESS FOR SENDING AND RECEIVING OPERATION     *
	 *  INPUT AND OUTPUT DATA.                                       *
	 * ***************************************************************</pre>*/
	private void allocateFwMemory() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE LENGTH OF L-SERVICE-CONTRACT-AREA
		//                                       TO PPC-SERVICE-DATA-SIZE.
		dfhcommarea.getProxyProgramCommon().setServiceDataSize(LServiceContractAreaXz0x0004.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		dfhcommarea.getProxyProgramCommon().setServiceDataPointer(
				cicsStorageManager.getmainNonshared(execContext, dfhcommarea.getProxyProgramCommon().getServiceDataSize(), ws.getCfBlank()));
		ws.setWsResponseCode(execContext.getResp());
		ws.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET MA-FATAL-ERROR-CODE TO TRUE
			ws.getTs020drv().getCommunicationShellCommon().getMaReturnCode().setMaFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.setWsEibrespDisplay(ws.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.setWsEibresp2Display(ws.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '1000-ALLOCATE-FW-MEMORY-2'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO MA-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(CommunicationShellCommon.Len.MA_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsProgramNameFormatted(), ".  Failed paragraph is ", "1000-ALLOCATE-FW-MEMORY-2",
							".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ", ws.getWsEibrespDisplayAsString(),
							".  Failed module EIBRESP2 Code is ", ws.getWsEibresp2DisplayAsString(), "." });
			ws.getTs020drv().getCommunicationShellCommon()
					.setMaErrorMessage(concatUtil.replaceInString(ws.getTs020drv().getCommunicationShellCommon().getMaErrorMessageFormatted()));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		//    MAIN PROXY IS EXPECTING ADDRESS OF INPUT AND OUTPUT
		//    IN SERVICE DATA POINTER.
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO PPC-SERVICE-DATA-POINTER.
		lServiceContractArea = ((pointerManager.resolve(dfhcommarea.getProxyProgramCommon().getServiceDataPointer(),
				LServiceContractAreaXz0x0004.class)));
		// COB_CODE: INITIALIZE L-SERVICE-CONTRACT-AREA.
		initLServiceContractArea();
	}

	/**Original name: 3000-CALL-FRAMEWORK<br>
	 * <pre>***************************************************************
	 *  PERFORM SET UP FOR AND MAKE THE CALL TO THE FRAMEWORK MAIN   *
	 *  DRIVER.                                                      *
	 * ***************************************************************</pre>*/
	private void callFramework() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 3100-SET-UP-INPUT
		//              THRU 3100-EXIT.
		setUpInput();
		// COB_CODE: EXEC CICS LINK
		//                      PROGRAM    (CF-MAIN-DRIVER)
		//                      COMMAREA   (PROXY-PROGRAM-COMMON)
		//                      RESP       (WS-RESPONSE-CODE)
		//                      RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0G0004", execContext).commarea(dfhcommarea.getProxyProgramCommon())
				.length(ProxyProgramCommonXz0g0004.Len.PROXY_PROGRAM_COMMON).link(ws.getCfMainDriver(), new Xz0x0004());
		ws.setWsResponseCode(execContext.getResp());
		ws.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.getProxyProgramCommon().getErrorReturnCode().setFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.setWsEibrespDisplay(ws.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.setWsEibresp2Display(ws.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '3000-CALL-FRAMEWORK'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(ProxyProgramCommonXz0g0004.Len.FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsProgramNameFormatted(), ".  Failed paragraph is ", "3000-CALL-FRAMEWORK",
							".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ", ws.getWsEibrespDisplayAsString(),
							".  Failed module EIBRESP2 Code is ", ws.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.getProxyProgramCommon()
					.setFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getProxyProgramCommon().getFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: IF NOT DSD-NO-ERROR-CODE
		//                                       TO PPC-ERROR-HANDLING-PARMS
		//           END-IF.
		if (!ws.getTs020drv().getDriverSpecificData().getErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: MOVE DSD-ERROR-HANDLING-PARMS
			//                                   TO PPC-ERROR-HANDLING-PARMS
			dfhcommarea.getProxyProgramCommon().setErrorHandlingParmsBytes(ws.getTs020drv().getDriverSpecificData().getDsdErrorHandlingParmsBytes());
		}
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getProxyProgramCommon().getErrorReturnCode().isFatalErrorCode()
				|| dfhcommarea.getProxyProgramCommon().getErrorReturnCode().isNlbeCode()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3200-SET-UP-OUTPUT
		//              THRU 3200-EXIT.
		setUpOutput();
	}

	/**Original name: 3100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
	 *  INPUT AREA                                                   *
	 * ***************************************************************
	 *   FRAMEWORK PARAMETERS</pre>*/
	private void setUpInput() {
		// COB_CODE: MOVE PPC-OPERATION          TO CSC-OPERATION
		//                                          WS-OPERATION-NAME.
		ws.getTs020drv().getCommunicationShellCommon().getCscGeneralParms().setOperation(dfhcommarea.getProxyProgramCommon().getOperation());
		ws.setWsOperationName(dfhcommarea.getProxyProgramCommon().getOperation());
		// COB_CODE: MOVE CF-UNIT-OF-WORK        TO CSC-UNIT-OF-WORK.
		ws.getTs020drv().getCommunicationShellCommon().getCscGeneralParms().setUnitOfWork(ws.getCfUnitOfWork());
		// COB_CODE: MOVE PPC-BYPASS-SYNCPOINT-MDRV-IND
		//                                       TO DSD-BYPASS-SYNCPOINT-MDRV-IND.
		ws.getTs020drv().getDriverSpecificData().getBypassSyncpointMdrvInd()
				.setBypassSyncpointMdrvInd(dfhcommarea.getProxyProgramCommon().getBypassSyncpointMdrvInd().getPpcBypassSyncpointMdrvInd());
		//  OPERATION REQUEST PARAMETERS
		// COB_CODE: MOVE XZT004-SERVICE-INPUTS  OF DFHCOMMAREA
		//                                       TO XZT004-SERVICE-INPUTS
		//                                       OF L-SERVICE-CONTRACT-AREA.
		lServiceContractArea.setXzt004ServiceInputsBytes(dfhcommarea.getXz0t0004().getXzt004ServiceInputs().getXzt004ServiceInputsBytes());
		// COB_CODE: MOVE XZT04I-USERID          OF L-SERVICE-CONTRACT-AREA
		//                                       TO CSC-AUTH-USERID.
		ws.getTs020drv().getCommunicationShellCommon().getCscGeneralParms().setAuthUserid(lServiceContractArea.getXzt04iUserid());
		// COB_CODE: INITIALIZE XZT004-SERVICE-OUTPUTS
		//                   OF L-SERVICE-CONTRACT-AREA.
		initXzt004ServiceOutputs();
	}

	/**Original name: 3200-SET-UP-OUTPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
	 *  RESPONSE AREA.                                               *
	 * ***************************************************************
	 *   OPERATION RESULTS</pre>*/
	private void setUpOutput() {
		// COB_CODE: MOVE XZT004-SERVICE-OUTPUTS OF L-SERVICE-CONTRACT-AREA
		//                                       TO XZT004-SERVICE-OUTPUTS
		//                                          OF DFHCOMMAREA.
		dfhcommarea.getXz0t0004().setXzt004ServiceOutputsBytes(lServiceContractArea.getXzt004ServiceOutputsBytes());
	}

	/**Original name: 8000-ENDING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM CLEANUP TASKS SUCH AS FREEING MEMORY.                *
	 * ***************************************************************</pre>*/
	private void endingHousekeeping() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(L-SERVICE-CONTRACT-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lServiceContractArea));
		ws.setWsResponseCode(execContext.getResp());
		ws.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET MA-FATAL-ERROR-CODE TO TRUE
			ws.getTs020drv().getCommunicationShellCommon().getMaReturnCode().setMaFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.setWsEibrespDisplay(ws.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.setWsEibresp2Display(ws.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-1'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO MA-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(CommunicationShellCommon.Len.MA_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsProgramNameFormatted(), ".  Failed paragraph is ", "8000-FREE-FW-MEMORY-1",
							".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ", ws.getWsEibrespDisplayAsString(),
							".  Failed module EIBRESP2 Code is ", ws.getWsEibresp2DisplayAsString(), "." });
			ws.getTs020drv().getCommunicationShellCommon()
					.setMaErrorMessage(concatUtil.replaceInString(ws.getTs020drv().getCommunicationShellCommon().getMaErrorMessageFormatted()));
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
	}

	public void initErrorHandlingParms() {
		ws.getTs020drv().getDriverSpecificData().getErrorReturnCode().setErrorReturnCodeFormatted("0000");
		ws.getTs020drv().getDriverSpecificData().setFatalErrorMessage("");
		ws.getTs020drv().getDriverSpecificData().setNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			ws.getTs020drv().getDriverSpecificData().getNonLoggableErrors(idx0).setDsdNonLogErrMsg("");
		}
		ws.getTs020drv().getDriverSpecificData().setWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.WARNINGS_MAXOCCURS; idx0++) {
			ws.getTs020drv().getDriverSpecificData().getWarnings(idx0).setDsdWarnMsg("");
		}
	}

	public void initErrorHandlingParms1() {
		dfhcommarea.getProxyProgramCommon().getErrorReturnCode().setErrorReturnCodeFormatted("0000");
		dfhcommarea.getProxyProgramCommon().setFatalErrorMessage("");
		dfhcommarea.getProxyProgramCommon().setNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= ProxyProgramCommonXz0g0004.NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.getProxyProgramCommon().getNonLoggableErrors(idx0).setPpcNonLogErrMsg("");
		}
		dfhcommarea.getProxyProgramCommon().setWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= ProxyProgramCommonXz0g0004.WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.getProxyProgramCommon().getWarnings(idx0).setPpcWarnMsg("");
		}
	}

	public void initLServiceContractArea() {
		lServiceContractArea.setXzt04iAccountNumber("");
		lServiceContractArea.setXzt04iOwnerState("");
		lServiceContractArea.setXzt04iPolicyNumber("");
		lServiceContractArea.setXzt04iPolicyEffDt("");
		lServiceContractArea.setXzt04iPolicyExpDt("");
		lServiceContractArea.setXzt04iPolicyType("");
		lServiceContractArea.setXzt04iPolicyCancDt("");
		lServiceContractArea.setXzt04iUserid("");
		lServiceContractArea.setXzt04oRescindInd(Types.SPACE_CHAR);
	}

	public void initXzt004ServiceOutputs() {
		lServiceContractArea.setXzt04oRescindInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
