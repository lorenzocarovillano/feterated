/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-NONLOG-PLACEHOLDER-VALUES<br>
 * Variable: WS-NONLOG-PLACEHOLDER-VALUES from program HALOUIDG<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsNonlogPlaceholderValues extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: WS-NONLOG-ERR-COL1-NAME
	private String nonlogErrCol1Name = "";
	/**Original name: WS-NONLOG-ERR-COL1-VALUE<br>
	 * <pre>    05  WS-NONLOG-ERR-COL1-VALUE       PIC X(50).</pre>*/
	private String nonlogErrCol1Value = "";
	//Original name: WS-NONLOG-ERR-COL2-NAME
	private String nonlogErrCol2Name = "";
	/**Original name: WS-NONLOG-ERR-COL2-VALUE<br>
	 * <pre>    05  WS-NONLOG-ERR-COL2-VALUE       PIC X(50).</pre>*/
	private String nonlogErrCol2Value = "";
	/**Original name: WS-NONLOG-ERR-CONTEXT-TEXT<br>
	 * <pre>    05  WS-NONLOG-ERR-CONTEXT-TEXT     PIC X(32).</pre>*/
	private String nonlogErrContextText = "";
	/**Original name: WS-NONLOG-ERR-CONTEXT-VALUE<br>
	 * <pre>    05  WS-NONLOG-ERR-CONTEXT-VALUE    PIC X(50).</pre>*/
	private String nonlogErrContextValue = "";
	//Original name: WS-NONLOG-ERR-ALLTXT-TEXT
	private String nonlogErrAlltxtText = "";
	//Original name: WS-ERR-WNG-TXT-D
	private String errWngTxtD = "";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_NONLOG_PLACEHOLDER_VALUES;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsNonlogPlaceholderValuesBytes(buf);
	}

	public String getWsNonlogPlaceholderValuesFormatted() {
		return MarshalByteExt.bufferToStr(getWsNonlogPlaceholderValuesBytes());
	}

	public void setWsNonlogPlaceholderValuesBytes(byte[] buffer) {
		setWsNonlogPlaceholderValuesBytes(buffer, 1);
	}

	public byte[] getWsNonlogPlaceholderValuesBytes() {
		byte[] buffer = new byte[Len.WS_NONLOG_PLACEHOLDER_VALUES];
		return getWsNonlogPlaceholderValuesBytes(buffer, 1);
	}

	public void setWsNonlogPlaceholderValuesBytes(byte[] buffer, int offset) {
		int position = offset;
		nonlogErrCol1Name = MarshalByte.readString(buffer, position, Len.NONLOG_ERR_COL1_NAME);
		position += Len.NONLOG_ERR_COL1_NAME;
		nonlogErrCol1Value = MarshalByte.readString(buffer, position, Len.NONLOG_ERR_COL1_VALUE);
		position += Len.NONLOG_ERR_COL1_VALUE;
		nonlogErrCol2Name = MarshalByte.readString(buffer, position, Len.NONLOG_ERR_COL2_NAME);
		position += Len.NONLOG_ERR_COL2_NAME;
		nonlogErrCol2Value = MarshalByte.readString(buffer, position, Len.NONLOG_ERR_COL2_VALUE);
		position += Len.NONLOG_ERR_COL2_VALUE;
		nonlogErrContextText = MarshalByte.readString(buffer, position, Len.NONLOG_ERR_CONTEXT_TEXT);
		position += Len.NONLOG_ERR_CONTEXT_TEXT;
		nonlogErrContextValue = MarshalByte.readString(buffer, position, Len.NONLOG_ERR_CONTEXT_VALUE);
		position += Len.NONLOG_ERR_CONTEXT_VALUE;
		nonlogErrAlltxtText = MarshalByte.readString(buffer, position, Len.NONLOG_ERR_ALLTXT_TEXT);
		position += Len.NONLOG_ERR_ALLTXT_TEXT;
		errWngTxtD = MarshalByte.readString(buffer, position, Len.ERR_WNG_TXT_D);
	}

	public byte[] getWsNonlogPlaceholderValuesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, nonlogErrCol1Name, Len.NONLOG_ERR_COL1_NAME);
		position += Len.NONLOG_ERR_COL1_NAME;
		MarshalByte.writeString(buffer, position, nonlogErrCol1Value, Len.NONLOG_ERR_COL1_VALUE);
		position += Len.NONLOG_ERR_COL1_VALUE;
		MarshalByte.writeString(buffer, position, nonlogErrCol2Name, Len.NONLOG_ERR_COL2_NAME);
		position += Len.NONLOG_ERR_COL2_NAME;
		MarshalByte.writeString(buffer, position, nonlogErrCol2Value, Len.NONLOG_ERR_COL2_VALUE);
		position += Len.NONLOG_ERR_COL2_VALUE;
		MarshalByte.writeString(buffer, position, nonlogErrContextText, Len.NONLOG_ERR_CONTEXT_TEXT);
		position += Len.NONLOG_ERR_CONTEXT_TEXT;
		MarshalByte.writeString(buffer, position, nonlogErrContextValue, Len.NONLOG_ERR_CONTEXT_VALUE);
		position += Len.NONLOG_ERR_CONTEXT_VALUE;
		MarshalByte.writeString(buffer, position, nonlogErrAlltxtText, Len.NONLOG_ERR_ALLTXT_TEXT);
		position += Len.NONLOG_ERR_ALLTXT_TEXT;
		MarshalByte.writeString(buffer, position, errWngTxtD, Len.ERR_WNG_TXT_D);
		return buffer;
	}

	public void initWsNonlogPlaceholderValuesSpaces() {
		nonlogErrCol1Name = "";
		nonlogErrCol1Value = "";
		nonlogErrCol2Name = "";
		nonlogErrCol2Value = "";
		nonlogErrContextText = "";
		nonlogErrContextValue = "";
		nonlogErrAlltxtText = "";
		errWngTxtD = "";
	}

	public void setNonlogErrCol1Name(String nonlogErrCol1Name) {
		this.nonlogErrCol1Name = Functions.subString(nonlogErrCol1Name, Len.NONLOG_ERR_COL1_NAME);
	}

	public String getNonlogErrCol1Name() {
		return this.nonlogErrCol1Name;
	}

	public void setNonlogErrCol1Value(String nonlogErrCol1Value) {
		this.nonlogErrCol1Value = Functions.subString(nonlogErrCol1Value, Len.NONLOG_ERR_COL1_VALUE);
	}

	public String getNonlogErrCol1Value() {
		return this.nonlogErrCol1Value;
	}

	public String getNonlogErrCol1ValueFormatted() {
		return Functions.padBlanks(getNonlogErrCol1Value(), Len.NONLOG_ERR_COL1_VALUE);
	}

	public void setNonlogErrCol2Name(String nonlogErrCol2Name) {
		this.nonlogErrCol2Name = Functions.subString(nonlogErrCol2Name, Len.NONLOG_ERR_COL2_NAME);
	}

	public String getNonlogErrCol2Name() {
		return this.nonlogErrCol2Name;
	}

	public String getNonlogErrCol2NameFormatted() {
		return Functions.padBlanks(getNonlogErrCol2Name(), Len.NONLOG_ERR_COL2_NAME);
	}

	public void setNonlogErrCol2Value(String nonlogErrCol2Value) {
		this.nonlogErrCol2Value = Functions.subString(nonlogErrCol2Value, Len.NONLOG_ERR_COL2_VALUE);
	}

	public String getNonlogErrCol2Value() {
		return this.nonlogErrCol2Value;
	}

	public String getNonlogErrCol2ValueFormatted() {
		return Functions.padBlanks(getNonlogErrCol2Value(), Len.NONLOG_ERR_COL2_VALUE);
	}

	public void setNonlogErrContextText(String nonlogErrContextText) {
		this.nonlogErrContextText = Functions.subString(nonlogErrContextText, Len.NONLOG_ERR_CONTEXT_TEXT);
	}

	public String getNonlogErrContextText() {
		return this.nonlogErrContextText;
	}

	public String getNonlogErrContextTextFormatted() {
		return Functions.padBlanks(getNonlogErrContextText(), Len.NONLOG_ERR_CONTEXT_TEXT);
	}

	public void setNonlogErrContextValue(String nonlogErrContextValue) {
		this.nonlogErrContextValue = Functions.subString(nonlogErrContextValue, Len.NONLOG_ERR_CONTEXT_VALUE);
	}

	public String getNonlogErrContextValue() {
		return this.nonlogErrContextValue;
	}

	public String getNonlogErrContextValueFormatted() {
		return Functions.padBlanks(getNonlogErrContextValue(), Len.NONLOG_ERR_CONTEXT_VALUE);
	}

	public void setNonlogErrAlltxtText(String nonlogErrAlltxtText) {
		this.nonlogErrAlltxtText = Functions.subString(nonlogErrAlltxtText, Len.NONLOG_ERR_ALLTXT_TEXT);
	}

	public String getNonlogErrAlltxtText() {
		return this.nonlogErrAlltxtText;
	}

	public String getNonlogErrAlltxtTextFormatted() {
		return Functions.padBlanks(getNonlogErrAlltxtText(), Len.NONLOG_ERR_ALLTXT_TEXT);
	}

	public void setErrWngTxtD(String errWngTxtD) {
		this.errWngTxtD = Functions.subString(errWngTxtD, Len.ERR_WNG_TXT_D);
	}

	public String getErrWngTxtD() {
		return this.errWngTxtD;
	}

	public String getErrWngTxtDFormatted() {
		return Functions.padBlanks(getErrWngTxtD(), Len.ERR_WNG_TXT_D);
	}

	@Override
	public byte[] serialize() {
		return getWsNonlogPlaceholderValuesBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NONLOG_ERR_COL1_NAME = 32;
		public static final int NONLOG_ERR_COL1_VALUE = 75;
		public static final int NONLOG_ERR_COL2_NAME = 32;
		public static final int NONLOG_ERR_COL2_VALUE = 75;
		public static final int NONLOG_ERR_CONTEXT_TEXT = 100;
		public static final int NONLOG_ERR_CONTEXT_VALUE = 100;
		public static final int NONLOG_ERR_ALLTXT_TEXT = 500;
		public static final int ERR_WNG_TXT_D = 500;
		public static final int WS_NONLOG_PLACEHOLDER_VALUES = NONLOG_ERR_COL1_NAME + NONLOG_ERR_COL1_VALUE + NONLOG_ERR_COL2_NAME
				+ NONLOG_ERR_COL2_VALUE + NONLOG_ERR_CONTEXT_TEXT + NONLOG_ERR_CONTEXT_VALUE + NONLOG_ERR_ALLTXT_TEXT + ERR_WNG_TXT_D;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
