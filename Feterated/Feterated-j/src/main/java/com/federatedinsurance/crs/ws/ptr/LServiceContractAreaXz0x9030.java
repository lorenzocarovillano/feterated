/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9030<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9030 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT93O_ACT_NOT_TYP_ROW_MAXOCCURS = 25;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9030() {
	}

	public LServiceContractAreaXz0x9030(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt93iUserid(String xzt93iUserid) {
		writeString(Pos.XZT93I_USERID, xzt93iUserid, Len.XZT93I_USERID);
	}

	/**Original name: XZT93I-USERID<br>*/
	public String getXzt93iUserid() {
		return readString(Pos.XZT93I_USERID, Len.XZT93I_USERID);
	}

	public String getXzt93iUseridFormatted() {
		return Functions.padBlanks(getXzt93iUserid(), Len.XZT93I_USERID);
	}

	public void setXzt93oTkDsyOrdNbr(int xzt93oTkDsyOrdNbrIdx, int xzt93oTkDsyOrdNbr) {
		int position = Pos.xzt93oTkDsyOrdNbr(xzt93oTkDsyOrdNbrIdx - 1);
		writeInt(position, xzt93oTkDsyOrdNbr, Len.Int.XZT93O_TK_DSY_ORD_NBR);
	}

	/**Original name: XZT93O-TK-DSY-ORD-NBR<br>*/
	public int getXzt93oTkDsyOrdNbr(int xzt93oTkDsyOrdNbrIdx) {
		int position = Pos.xzt93oTkDsyOrdNbr(xzt93oTkDsyOrdNbrIdx - 1);
		return readNumDispInt(position, Len.XZT93O_TK_DSY_ORD_NBR);
	}

	public void setXzt93oTkActNotPthCd(int xzt93oTkActNotPthCdIdx, String xzt93oTkActNotPthCd) {
		int position = Pos.xzt93oTkActNotPthCd(xzt93oTkActNotPthCdIdx - 1);
		writeString(position, xzt93oTkActNotPthCd, Len.XZT93O_TK_ACT_NOT_PTH_CD);
	}

	/**Original name: XZT93O-TK-ACT-NOT-PTH-CD<br>*/
	public String getXzt93oTkActNotPthCd(int xzt93oTkActNotPthCdIdx) {
		int position = Pos.xzt93oTkActNotPthCd(xzt93oTkActNotPthCdIdx - 1);
		return readString(position, Len.XZT93O_TK_ACT_NOT_PTH_CD);
	}

	public void setXzt93oTkActNotPthDes(int xzt93oTkActNotPthDesIdx, String xzt93oTkActNotPthDes) {
		int position = Pos.xzt93oTkActNotPthDes(xzt93oTkActNotPthDesIdx - 1);
		writeString(position, xzt93oTkActNotPthDes, Len.XZT93O_TK_ACT_NOT_PTH_DES);
	}

	/**Original name: XZT93O-TK-ACT-NOT-PTH-DES<br>*/
	public String getXzt93oTkActNotPthDes(int xzt93oTkActNotPthDesIdx) {
		int position = Pos.xzt93oTkActNotPthDes(xzt93oTkActNotPthDesIdx - 1);
		return readString(position, Len.XZT93O_TK_ACT_NOT_PTH_DES);
	}

	public void setXzt93oActNotTypCd(int xzt93oActNotTypCdIdx, String xzt93oActNotTypCd) {
		int position = Pos.xzt93oActNotTypCd(xzt93oActNotTypCdIdx - 1);
		writeString(position, xzt93oActNotTypCd, Len.XZT93O_ACT_NOT_TYP_CD);
	}

	/**Original name: XZT93O-ACT-NOT-TYP-CD<br>*/
	public String getXzt93oActNotTypCd(int xzt93oActNotTypCdIdx) {
		int position = Pos.xzt93oActNotTypCd(xzt93oActNotTypCdIdx - 1);
		return readString(position, Len.XZT93O_ACT_NOT_TYP_CD);
	}

	public void setXzt93oActNotDes(int xzt93oActNotDesIdx, String xzt93oActNotDes) {
		int position = Pos.xzt93oActNotDes(xzt93oActNotDesIdx - 1);
		writeString(position, xzt93oActNotDes, Len.XZT93O_ACT_NOT_DES);
	}

	/**Original name: XZT93O-ACT-NOT-DES<br>*/
	public String getXzt93oActNotDes(int xzt93oActNotDesIdx) {
		int position = Pos.xzt93oActNotDes(xzt93oActNotDesIdx - 1);
		return readString(position, Len.XZT93O_ACT_NOT_DES);
	}

	public void setXzt93oDocDes(int xzt93oDocDesIdx, String xzt93oDocDes) {
		int position = Pos.xzt93oDocDes(xzt93oDocDesIdx - 1);
		writeString(position, xzt93oDocDes, Len.XZT93O_DOC_DES);
	}

	/**Original name: XZT93O-DOC-DES<br>*/
	public String getXzt93oDocDes(int xzt93oDocDesIdx) {
		int position = Pos.xzt93oDocDes(xzt93oDocDesIdx - 1);
		return readString(position, Len.XZT93O_DOC_DES);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT903_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT93I_USERID = XZT903_SERVICE_INPUTS;
		public static final int FLR1 = XZT93I_USERID + Len.XZT93I_USERID;
		public static final int XZT903_SERVICE_OUTPUTS = FLR1 + Len.FLR1;
		public static final int XZT93O_ACT_NOT_TYP_ROW_TBL = XZT903_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt93oActNotTypRow(int idx) {
			return XZT93O_ACT_NOT_TYP_ROW_TBL + idx * Len.XZT93O_ACT_NOT_TYP_ROW;
		}

		public static int xzt93oTechnicalKey(int idx) {
			return xzt93oActNotTypRow(idx);
		}

		public static int xzt93oTkDsyOrdNbr(int idx) {
			return xzt93oTechnicalKey(idx);
		}

		public static int xzt93oTkActNotPthCd(int idx) {
			return xzt93oTkDsyOrdNbr(idx) + Len.XZT93O_TK_DSY_ORD_NBR;
		}

		public static int xzt93oTkActNotPthDes(int idx) {
			return xzt93oTkActNotPthCd(idx) + Len.XZT93O_TK_ACT_NOT_PTH_CD;
		}

		public static int xzt93oActNotTypCd(int idx) {
			return xzt93oTkActNotPthDes(idx) + Len.XZT93O_TK_ACT_NOT_PTH_DES;
		}

		public static int xzt93oActNotDes(int idx) {
			return xzt93oActNotTypCd(idx) + Len.XZT93O_ACT_NOT_TYP_CD;
		}

		public static int xzt93oDocDes(int idx) {
			return xzt93oActNotDes(idx) + Len.XZT93O_ACT_NOT_DES;
		}

		public static int flr2(int idx) {
			return xzt93oDocDes(idx) + Len.XZT93O_DOC_DES;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT93I_USERID = 8;
		public static final int FLR1 = 100;
		public static final int XZT93O_TK_DSY_ORD_NBR = 5;
		public static final int XZT93O_TK_ACT_NOT_PTH_CD = 5;
		public static final int XZT93O_TK_ACT_NOT_PTH_DES = 35;
		public static final int XZT93O_TECHNICAL_KEY = XZT93O_TK_DSY_ORD_NBR + XZT93O_TK_ACT_NOT_PTH_CD + XZT93O_TK_ACT_NOT_PTH_DES;
		public static final int XZT93O_ACT_NOT_TYP_CD = 5;
		public static final int XZT93O_ACT_NOT_DES = 35;
		public static final int XZT93O_DOC_DES = 240;
		public static final int FLR2 = 200;
		public static final int XZT93O_ACT_NOT_TYP_ROW = XZT93O_TECHNICAL_KEY + XZT93O_ACT_NOT_TYP_CD + XZT93O_ACT_NOT_DES + XZT93O_DOC_DES + FLR2;
		public static final int XZT903_SERVICE_INPUTS = XZT93I_USERID + FLR1;
		public static final int XZT93O_ACT_NOT_TYP_ROW_TBL = LServiceContractAreaXz0x9030.XZT93O_ACT_NOT_TYP_ROW_MAXOCCURS * XZT93O_ACT_NOT_TYP_ROW;
		public static final int XZT903_SERVICE_OUTPUTS = XZT93O_ACT_NOT_TYP_ROW_TBL;
		public static final int L_SERVICE_CONTRACT_AREA = XZT903_SERVICE_INPUTS + XZT903_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT93O_TK_DSY_ORD_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
