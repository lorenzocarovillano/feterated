/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LI-ERR-CONTEXT-TEXT<br>
 * Variable: LI-ERR-CONTEXT-TEXT from program HALRPLAC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class LiErrContextText extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LI-ERR-CONTEXT-TEXT
	private String liErrContextText = "";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.LI_ERR_CONTEXT_TEXT;
	}

	@Override
	public void deserialize(byte[] buf) {
		setLiErrContextTextFromBuffer(buf);
	}

	public void setLiErrContextText(String liErrContextText) {
		this.liErrContextText = Functions.subString(liErrContextText, Len.LI_ERR_CONTEXT_TEXT);
	}

	public void setLiErrContextTextFromBuffer(byte[] buffer, int offset) {
		setLiErrContextText(MarshalByte.readString(buffer, offset, Len.LI_ERR_CONTEXT_TEXT));
	}

	public void setLiErrContextTextFromBuffer(byte[] buffer) {
		setLiErrContextTextFromBuffer(buffer, 1);
	}

	public String getLiErrContextText() {
		return this.liErrContextText;
	}

	public String getLiErrContextTextFormatted() {
		return Functions.padBlanks(getLiErrContextText(), Len.LI_ERR_CONTEXT_TEXT);
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.strToBuffer(getLiErrContextText(), Len.LI_ERR_CONTEXT_TEXT);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LI_ERR_CONTEXT_TEXT = 100;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int LI_ERR_CONTEXT_TEXT = 100;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int LI_ERR_CONTEXT_TEXT = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
