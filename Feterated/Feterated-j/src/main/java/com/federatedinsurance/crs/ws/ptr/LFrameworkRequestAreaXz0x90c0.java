/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X90C0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x90c0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x90c0() {
	}

	public LFrameworkRequestAreaXz0x90c0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzy9c0qCsrActNbr(String xzy9c0qCsrActNbr) {
		writeString(Pos.XZY9C0Q_CSR_ACT_NBR, xzy9c0qCsrActNbr, Len.XZY9C0Q_CSR_ACT_NBR);
	}

	/**Original name: XZY9C0Q-CSR-ACT-NBR<br>*/
	public String getXzy9c0qCsrActNbr() {
		return readString(Pos.XZY9C0Q_CSR_ACT_NBR, Len.XZY9C0Q_CSR_ACT_NBR);
	}

	public void setXzy9c0qNotPrcTs(String xzy9c0qNotPrcTs) {
		writeString(Pos.XZY9C0Q_NOT_PRC_TS, xzy9c0qNotPrcTs, Len.XZY9C0Q_NOT_PRC_TS);
	}

	/**Original name: XZY9C0Q-NOT-PRC-TS<br>*/
	public String getXzy9c0qNotPrcTs() {
		return readString(Pos.XZY9C0Q_NOT_PRC_TS, Len.XZY9C0Q_NOT_PRC_TS);
	}

	public void setXzy9c0qUserid(String xzy9c0qUserid) {
		writeString(Pos.XZY9C0Q_USERID, xzy9c0qUserid, Len.XZY9C0Q_USERID);
	}

	/**Original name: XZY9C0Q-USERID<br>*/
	public String getXzy9c0qUserid() {
		return readString(Pos.XZY9C0Q_USERID, Len.XZY9C0Q_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0Y90C0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZY9C0Q_CERT_HOLDER_ROW = L_FW_REQ_XZ0Y90C0;
		public static final int XZY9C0Q_CSR_ACT_NBR = XZY9C0Q_CERT_HOLDER_ROW;
		public static final int XZY9C0Q_NOT_PRC_TS = XZY9C0Q_CSR_ACT_NBR + Len.XZY9C0Q_CSR_ACT_NBR;
		public static final int XZY9C0Q_USERID = XZY9C0Q_NOT_PRC_TS + Len.XZY9C0Q_NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY9C0Q_CSR_ACT_NBR = 9;
		public static final int XZY9C0Q_NOT_PRC_TS = 26;
		public static final int XZY9C0Q_USERID = 8;
		public static final int XZY9C0Q_CERT_HOLDER_ROW = XZY9C0Q_CSR_ACT_NBR + XZY9C0Q_NOT_PRC_TS + XZY9C0Q_USERID;
		public static final int L_FW_REQ_XZ0Y90C0 = XZY9C0Q_CERT_HOLDER_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0Y90C0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
