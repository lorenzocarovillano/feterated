/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X90S0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x90s0 extends BytesClass {

	//==== PROPERTIES ====
	public static final String XZA9S0Q_PF_BY_ACT = "GetAcyPndCncTmnPolListByAct";

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x90s0() {
	}

	public LFrameworkRequestAreaXz0x90s0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza9s0qMaxRowsReturned(short xza9s0qMaxRowsReturned) {
		writeBinaryShort(Pos.XZA9S0Q_MAX_ROWS_RETURNED, xza9s0qMaxRowsReturned);
	}

	/**Original name: XZA9S0Q-MAX-ROWS-RETURNED<br>*/
	public short getXza9s0qMaxRowsReturned() {
		return readBinaryShort(Pos.XZA9S0Q_MAX_ROWS_RETURNED);
	}

	public void setXza9s0qPrimaryFunction(String xza9s0qPrimaryFunction) {
		writeString(Pos.XZA9S0Q_PRIMARY_FUNCTION, xza9s0qPrimaryFunction, Len.XZA9S0Q_PRIMARY_FUNCTION);
	}

	/**Original name: XZA9S0Q-PRIMARY-FUNCTION<br>*/
	public String getXza9s0qPrimaryFunction() {
		return readString(Pos.XZA9S0Q_PRIMARY_FUNCTION, Len.XZA9S0Q_PRIMARY_FUNCTION);
	}

	public void setXza9s0qActNbr(String xza9s0qActNbr) {
		writeString(Pos.XZA9S0Q_ACT_NBR, xza9s0qActNbr, Len.XZA9S0Q_ACT_NBR);
	}

	/**Original name: XZA9S0Q-ACT-NBR<br>*/
	public String getXza9s0qActNbr() {
		return readString(Pos.XZA9S0Q_ACT_NBR, Len.XZA9S0Q_ACT_NBR);
	}

	public void setXza9s0qUserid(String xza9s0qUserid) {
		writeString(Pos.XZA9S0Q_USERID, xza9s0qUserid, Len.XZA9S0Q_USERID);
	}

	/**Original name: XZA9S0Q-USERID<br>*/
	public String getXza9s0qUserid() {
		return readString(Pos.XZA9S0Q_USERID, Len.XZA9S0Q_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_ACY_PND_CNC_TMN_POL = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA9S0Q_POLICY_LOCATE_ROW = L_FW_REQ_ACY_PND_CNC_TMN_POL;
		public static final int XZA9S0Q_POLICY_INPUT_DATA = XZA9S0Q_POLICY_LOCATE_ROW;
		public static final int XZA9S0Q_MAX_ROWS_RETURNED = XZA9S0Q_POLICY_INPUT_DATA;
		public static final int XZA9S0Q_PRIMARY_FUNCTION = XZA9S0Q_MAX_ROWS_RETURNED + Len.XZA9S0Q_MAX_ROWS_RETURNED;
		public static final int XZA9S0Q_ACT_NBR = XZA9S0Q_PRIMARY_FUNCTION + Len.XZA9S0Q_PRIMARY_FUNCTION;
		public static final int XZA9S0Q_USERID = XZA9S0Q_ACT_NBR + Len.XZA9S0Q_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9S0Q_MAX_ROWS_RETURNED = 2;
		public static final int XZA9S0Q_PRIMARY_FUNCTION = 32;
		public static final int XZA9S0Q_ACT_NBR = 9;
		public static final int XZA9S0Q_USERID = 9;
		public static final int XZA9S0Q_POLICY_INPUT_DATA = XZA9S0Q_MAX_ROWS_RETURNED + XZA9S0Q_PRIMARY_FUNCTION + XZA9S0Q_ACT_NBR + XZA9S0Q_USERID;
		public static final int XZA9S0Q_POLICY_LOCATE_ROW = XZA9S0Q_POLICY_INPUT_DATA;
		public static final int L_FW_REQ_ACY_PND_CNC_TMN_POL = XZA9S0Q_POLICY_LOCATE_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_ACY_PND_CNC_TMN_POL;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
