/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.io.file.FileRecord;
import com.federatedinsurance.crs.ws.OTsx131cmComRecord;

/**Original name: O-TSX131CM-COM-FILE<br>
 * File: O-TSX131CM-COM-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OTsx131cmComFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: O-TSX131CM-COM-RECORD
	private OTsx131cmComRecord oTsx131cmComRecord = new OTsx131cmComRecord();

	//==== METHODS ====
	@Override
	public void getData(byte[] destination, int offset) {
		oTsx131cmComRecord.getoTsx131cmComRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		oTsx131cmComRecord.setoTsx131cmComRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return OTsx131cmComRecord.Len.O_TSX131CM_COM_RECORD;
	}

	public OTsx131cmComRecord getoTsx131cmComRecord() {
		return oTsx131cmComRecord;
	}

	@Override
	public IBuffer copy() {
		OTsx131cmComFileTO copyTO = new OTsx131cmComFileTO();
		copyTO.assign(this);
		return copyTO;
	}
}
