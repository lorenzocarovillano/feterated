/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TP-PI-OPEN-CLOSE-TALLY<br>
 * Variable: TP-PI-OPEN-CLOSE-TALLY from program TS547099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TpPiOpenCloseTally {

	//==== PROPERTIES ====
	private short value = DefaultValues.BIN_SHORT_VAL;
	public static final short KEEP_PIPE_OPEN_MIN = ((short) 1);
	public static final short KEEP_PIPE_OPEN_MAX = ((short) 32767);
	public static final short PIPE_IS_CLOSED = ((short) 0);

	//==== METHODS ====
	public void setOpenCloseTally(short openCloseTally) {
		this.value = openCloseTally;
	}

	public short getOpenCloseTally() {
		return this.value;
	}

	public boolean isKeepPipeOpen() {
		short fld = value;
		return fld >= KEEP_PIPE_OPEN_MIN && fld <= KEEP_PIPE_OPEN_MAX;
	}

	public boolean isPipeIsClosed() {
		return value == PIPE_IS_CLOSED;
	}

	public void setPipeIsClosed() {
		value = PIPE_IS_CLOSED;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPEN_CLOSE_TALLY = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
