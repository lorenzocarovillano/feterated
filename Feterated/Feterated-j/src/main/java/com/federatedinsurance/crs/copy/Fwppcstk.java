/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: FWPPCSTK<br>
 * Variable: FWPPCSTK from copybook FWPPCSTK<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Fwppcstk {

	//==== PROPERTIES ====
	//Original name: XZ005I-TK-USERID
	private String tkUserid = DefaultValues.stringVal(Len.TK_USERID);
	//Original name: XZ005I-TK-PASSWORD
	private String tkPassword = DefaultValues.stringVal(Len.TK_PASSWORD);
	//Original name: XZ005I-TK-URI
	private String tkUri = DefaultValues.stringVal(Len.TK_URI);
	//Original name: XZ005I-TK-FED-TAR-SYS
	private String tkFedTarSys = DefaultValues.stringVal(Len.TK_FED_TAR_SYS);

	//==== METHODS ====
	public void initFwppcstkLowValues() {
		initTechnicalKeysLowValues();
	}

	public byte[] getTechnicalKeysBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tkUserid, Len.TK_USERID);
		position += Len.TK_USERID;
		MarshalByte.writeString(buffer, position, tkPassword, Len.TK_PASSWORD);
		position += Len.TK_PASSWORD;
		MarshalByte.writeString(buffer, position, tkUri, Len.TK_URI);
		position += Len.TK_URI;
		MarshalByte.writeString(buffer, position, tkFedTarSys, Len.TK_FED_TAR_SYS);
		return buffer;
	}

	public void initTechnicalKeysLowValues() {
		tkUserid = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.TK_USERID);
		tkPassword = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.TK_PASSWORD);
		tkUri = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.TK_URI);
		tkFedTarSys = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.TK_FED_TAR_SYS);
	}

	public void setTkUserid(String tkUserid) {
		this.tkUserid = Functions.subString(tkUserid, Len.TK_USERID);
	}

	public String getTkUserid() {
		return this.tkUserid;
	}

	public void setTkPassword(String tkPassword) {
		this.tkPassword = Functions.subString(tkPassword, Len.TK_PASSWORD);
	}

	public String getTkPassword() {
		return this.tkPassword;
	}

	public void setTkUri(String tkUri) {
		this.tkUri = Functions.subString(tkUri, Len.TK_URI);
	}

	public String getTkUri() {
		return this.tkUri;
	}

	public void setTkFedTarSys(String tkFedTarSys) {
		this.tkFedTarSys = Functions.subString(tkFedTarSys, Len.TK_FED_TAR_SYS);
	}

	public String getTkFedTarSys() {
		return this.tkFedTarSys;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TK_USERID = 8;
		public static final int TK_PASSWORD = 8;
		public static final int TK_URI = 256;
		public static final int TK_FED_TAR_SYS = 5;
		public static final int TECHNICAL_KEYS = TK_USERID + TK_PASSWORD + TK_URI + TK_FED_TAR_SYS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
