/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Halluidg;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALOUIDG<br>
 * Generated as a class for rule WS.<br>*/
public class HalouidgData {

	//==== PROPERTIES ====
	//Original name: W-GENERAL-WORKFIELDS
	private WGeneralWorkfields wGeneralWorkfields = new WGeneralWorkfields();
	//Original name: W2000-WORKFIELDS
	private W2000Workfields w2000Workfields = new W2000Workfields();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: W-CURRENT-TIMESTAMP-NUM
	private WCurrentTimestampNum wCurrentTimestampNum = new WCurrentTimestampNum();
	//Original name: WS-SPECIFIC-MISC
	private WsSpecificMisc wsSpecificMisc = new WsSpecificMisc();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== CONSTRUCTORS ====
	public HalouidgData() {
		init();
	}

	//==== METHODS ====
	public void init() {
		wCurrentTimestampNum.setwCurrentTimestamp("");
	}

	public void setwHalouidgCommareaFormatted(String data) {
		byte[] buffer = new byte[Len.W_HALOUIDG_COMMAREA];
		MarshalByte.writeString(buffer, 1, data, Len.W_HALOUIDG_COMMAREA);
		setwHalouidgCommareaBytes(buffer, 1);
	}

	public String getwHalouidgCommareaFormatted() {
		return MarshalByteExt.bufferToStr(getwHalouidgCommareaBytes());
	}

	/**Original name: W-HALOUIDG-COMMAREA<br>
	 * <pre>****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK*
	 * **CSC *                                     *BUSINESS FRAMEWORK*
	 * **CSC *  LAYOUT OF MODULE-SPECIFIC PART OF  *BUSINESS FRAMEWORK*
	 * **CSC *  COMMAREA (MAPPING ONTO APP DATA    *BUSINESS FRAMEWORK*
	 * **CSC *  BUFFER OF UBOC COMMAREA).          *BUSINESS FRAMEWORK*
	 * ****************************************************************</pre>*/
	public byte[] getwHalouidgCommareaBytes() {
		byte[] buffer = new byte[Len.W_HALOUIDG_COMMAREA];
		return getwHalouidgCommareaBytes(buffer, 1);
	}

	public void setwHalouidgCommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.setUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.setUidgCaOutputBytes(buffer, position);
	}

	public byte[] getwHalouidgCommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.getUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.getUidgCaOutputBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public W2000Workfields getW2000Workfields() {
		return w2000Workfields;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsSpecificMisc getWsSpecificMisc() {
		return wsSpecificMisc;
	}

	public WCurrentTimestampNum getwCurrentTimestampNum() {
		return wCurrentTimestampNum;
	}

	public WGeneralWorkfields getwGeneralWorkfields() {
		return wGeneralWorkfields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_APPLID = 8;
		public static final int W_HALOUIDG_COMMAREA = Halluidg.Len.UIDG_CA_INCOMING + Halluidg.Len.UIDG_CA_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
