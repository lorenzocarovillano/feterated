/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.CommunicationShellCommon;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program TS020100<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaTs020100 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: UBOC-COMM-INFO
	private UbocCommInfo ubocCommInfo = new UbocCommInfo();
	//Original name: UBOC-APP-DATA-BUFFER-LENGTH
	private String ubocAppDataBufferLength = DefaultValues.stringVal(Len.UBOC_APP_DATA_BUFFER_LENGTH);
	//Original name: UBOC-APP-DATA-BUFFER
	private String ubocAppDataBuffer = DefaultValues.stringVal(Len.UBOC_APP_DATA_BUFFER);
	//Original name: COMMUNICATION-SHELL-COMMON
	private CommunicationShellCommon communicationShellCommon = new CommunicationShellCommon();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public String getDfhcommareaFormatted() {
		return MarshalByteExt.bufferToStr(getDfhcommareaBytes());
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		setUbocRecordBytes(buffer, position);
		position += Len.UBOC_RECORD;
		communicationShellCommon.setCommunicationShellCommonBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		getUbocRecordBytes(buffer, position);
		position += Len.UBOC_RECORD;
		communicationShellCommon.getCommunicationShellCommonBytes(buffer, position);
		return buffer;
	}

	public void setUbocRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		ubocCommInfo.setCommInfoBytes(buffer, position);
		position += UbocCommInfo.Len.COMM_INFO;
		setUbocExtraDataBytes(buffer, position);
	}

	public byte[] getUbocRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		ubocCommInfo.getCommInfoBytes(buffer, position);
		position += UbocCommInfo.Len.COMM_INFO;
		getUbocExtraDataBytes(buffer, position);
		return buffer;
	}

	public void setUbocExtraDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ubocAppDataBufferLength = MarshalByte.readFixedString(buffer, position, Len.UBOC_APP_DATA_BUFFER_LENGTH);
		position += Len.UBOC_APP_DATA_BUFFER_LENGTH;
		ubocAppDataBuffer = MarshalByte.readString(buffer, position, Len.UBOC_APP_DATA_BUFFER);
	}

	public byte[] getUbocExtraDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ubocAppDataBufferLength, Len.UBOC_APP_DATA_BUFFER_LENGTH);
		position += Len.UBOC_APP_DATA_BUFFER_LENGTH;
		MarshalByte.writeString(buffer, position, ubocAppDataBuffer, Len.UBOC_APP_DATA_BUFFER);
		return buffer;
	}

	public void setUbocAppDataBufferLength(short ubocAppDataBufferLength) {
		this.ubocAppDataBufferLength = NumericDisplay.asString(ubocAppDataBufferLength, Len.UBOC_APP_DATA_BUFFER_LENGTH);
	}

	public short getUbocAppDataBufferLength() {
		return NumericDisplay.asShort(this.ubocAppDataBufferLength);
	}

	public void setUbocAppDataBuffer(String ubocAppDataBuffer) {
		this.ubocAppDataBuffer = Functions.subString(ubocAppDataBuffer, Len.UBOC_APP_DATA_BUFFER);
	}

	public String getUbocAppDataBuffer() {
		return this.ubocAppDataBuffer;
	}

	public String getUbocAppDataBufferFormatted() {
		return Functions.padBlanks(getUbocAppDataBuffer(), Len.UBOC_APP_DATA_BUFFER);
	}

	public CommunicationShellCommon getCommunicationShellCommon() {
		return communicationShellCommon;
	}

	public UbocCommInfo getUbocCommInfo() {
		return ubocCommInfo;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_APP_DATA_BUFFER_LENGTH = 4;
		public static final int UBOC_APP_DATA_BUFFER = 5000;
		public static final int UBOC_EXTRA_DATA = UBOC_APP_DATA_BUFFER_LENGTH + UBOC_APP_DATA_BUFFER;
		public static final int UBOC_RECORD = UbocCommInfo.Len.COMM_INFO + UBOC_EXTRA_DATA;
		public static final int DFHCOMMAREA = UBOC_RECORD + CommunicationShellCommon.Len.COMMUNICATION_SHELL_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
