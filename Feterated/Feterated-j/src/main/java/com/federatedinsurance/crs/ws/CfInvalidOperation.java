/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: CF-INVALID-OPERATION<br>
 * Variable: CF-INVALID-OPERATION from program MU0Q0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfInvalidOperation {

	//==== PROPERTIES ====
	//Original name: FILLER-CF-INVALID-OPERATION
	private String flr1 = "REQUEST MODULE";
	//Original name: FILLER-CF-INVALID-OPERATION-1
	private String flr2 = "- INVALID";
	//Original name: FILLER-CF-INVALID-OPERATION-2
	private String flr3 = "OPERATION";

	//==== METHODS ====
	public String getInvalidOperationFormatted() {
		return MarshalByteExt.bufferToStr(getInvalidOperationBytes());
	}

	public byte[] getInvalidOperationBytes() {
		byte[] buffer = new byte[Len.INVALID_OPERATION];
		return getInvalidOperationBytes(buffer, 1);
	}

	public byte[] getInvalidOperationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 15;
		public static final int FLR2 = 10;
		public static final int FLR3 = 9;
		public static final int INVALID_OPERATION = FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
