/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.SaDb2AccessProgram;
import com.federatedinsurance.crs.ws.enums.SaLpDataSize;
import com.federatedinsurance.crs.ws.enums.SaReportOfficeLocationFlag;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program TS030099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class SaveAreaTs030099 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: SA-REPORT-NUMBER
	private String reportNumber = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.REPORT_NUMBER);
	//Original name: SA-REPORT-OFFICE-LOCATION
	private String reportOfficeLocation = "";
	//Original name: SA-DIVISION
	private char division = DefaultValues.CHAR_VAL;
	//Original name: SA-SUBDIVISION
	private char subdivision = DefaultValues.CHAR_VAL;
	//Original name: SA-REPORT-OFFICE-LOCATION-FLAG
	private SaReportOfficeLocationFlag reportOfficeLocationFlag = new SaReportOfficeLocationFlag();
	//Original name: SA-TSX132CM-FILE-NAME
	private SaTsx132cmFileName tsx132cmFileName = new SaTsx132cmFileName();
	//Original name: SA-TSX131CM-FILE-NAME
	private SaTsx131cmFileName tsx131cmFileName = new SaTsx131cmFileName();
	//Original name: SA-TSXXXXBL-FILE-NAME
	private SaTsxxxxblFileName tsxxxxblFileName = new SaTsxxxxblFileName();
	//Original name: SA-CURRENT-DATE-TIME
	private SaCurrentDateTime currentDateTime = new SaCurrentDateTime();
	//Original name: SA-BALANCE-CARRIAGE-CONTROL
	private char balanceCarriageControl = DefaultValues.CHAR_VAL;
	//Original name: SA-BALANCE-REPORT-DATA
	private String balanceReportData = DefaultValues.stringVal(Len.BALANCE_REPORT_DATA);
	//Original name: SA-CARRIAGE-CONTROL
	private char carriageControl = DefaultValues.CHAR_VAL;
	//Original name: SA-LP-DATA-SIZE
	private SaLpDataSize lpDataSize = new SaLpDataSize();
	/**Original name: SA-DB2-ACCESS-PROGRAM<br>
	 * <pre>    THE PROGRAM IS DEFAULTED IN CASE A CALLER FORGETS TO DO
	 *     THE FIRST CALL.  WE'LL ACCESS ASSUMING WE ARE CONNECTED
	 *     TO DB2.  IT'S POSSIBLE THIS COULD WORK FINE IF THE ROOT
	 *     HAS CONNECTED TO DB2 AND FORGETS THE FIRST CALL.</pre>*/
	private SaDb2AccessProgram db2AccessProgram = new SaDb2AccessProgram();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.SAVE_AREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setSaveAreaBytes(buf);
	}

	public String getSaveAreaFormatted() {
		return MarshalByteExt.bufferToStr(getSaveAreaBytes());
	}

	public void setSaveAreaBytes(byte[] buffer) {
		setSaveAreaBytes(buffer, 1);
	}

	public byte[] getSaveAreaBytes() {
		byte[] buffer = new byte[Len.SAVE_AREA];
		return getSaveAreaBytes(buffer, 1);
	}

	public void setSaveAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		setReportNumberDdNameBytes(buffer, position);
		position += Len.REPORT_NUMBER_DD_NAME;
		setOfficeLocationBytes(buffer, position);
		position += Len.OFFICE_LOCATION;
		reportOfficeLocationFlag.setReportOfficeLocationFlag(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		tsx132cmFileName.setTsx132cmFileNameBytes(buffer, position);
		position += SaTsx132cmFileName.Len.TSX132CM_FILE_NAME;
		tsx131cmFileName.setTsx131cmFileNameBytes(buffer, position);
		position += SaTsx131cmFileName.Len.TSX131CM_FILE_NAME;
		tsxxxxblFileName.setTsxxxxblFileNameBytes(buffer, position);
		position += SaTsxxxxblFileName.Len.TSXXXXBL_FILE_NAME;
		currentDateTime.setCurrentDateTimeBytes(buffer, position);
		position += SaCurrentDateTime.Len.CURRENT_DATE_TIME;
		setBalanceReportRecordBytes(buffer, position);
		position += Len.BALANCE_REPORT_RECORD;
		carriageControl = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		setLengthParmBytes(buffer, position);
		position += Len.LENGTH_PARM;
		db2AccessProgram.setDb2AccessProgram(MarshalByte.readString(buffer, position, SaDb2AccessProgram.Len.DB2_ACCESS_PROGRAM));
	}

	public byte[] getSaveAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		getReportNumberDdNameBytes(buffer, position);
		position += Len.REPORT_NUMBER_DD_NAME;
		getOfficeLocationBytes(buffer, position);
		position += Len.OFFICE_LOCATION;
		MarshalByte.writeChar(buffer, position, reportOfficeLocationFlag.getReportOfficeLocationFlag());
		position += Types.CHAR_SIZE;
		tsx132cmFileName.getTsx132cmFileNameBytes(buffer, position);
		position += SaTsx132cmFileName.Len.TSX132CM_FILE_NAME;
		tsx131cmFileName.getTsx131cmFileNameBytes(buffer, position);
		position += SaTsx131cmFileName.Len.TSX131CM_FILE_NAME;
		tsxxxxblFileName.getTsxxxxblFileNameBytes(buffer, position);
		position += SaTsxxxxblFileName.Len.TSXXXXBL_FILE_NAME;
		currentDateTime.getCurrentDateTimeBytes(buffer, position);
		position += SaCurrentDateTime.Len.CURRENT_DATE_TIME;
		getBalanceReportRecordBytes(buffer, position);
		position += Len.BALANCE_REPORT_RECORD;
		MarshalByte.writeChar(buffer, position, carriageControl);
		position += Types.CHAR_SIZE;
		getLengthParmBytes(buffer, position);
		position += Len.LENGTH_PARM;
		MarshalByte.writeString(buffer, position, db2AccessProgram.getDb2AccessProgram(), SaDb2AccessProgram.Len.DB2_ACCESS_PROGRAM);
		return buffer;
	}

	public String getReportNumberDdNameFormatted() {
		return MarshalByteExt.bufferToStr(getReportNumberDdNameBytes());
	}

	/**Original name: SA-REPORT-NUMBER-DD-NAME<br>*/
	public byte[] getReportNumberDdNameBytes() {
		byte[] buffer = new byte[Len.REPORT_NUMBER_DD_NAME];
		return getReportNumberDdNameBytes(buffer, 1);
	}

	public void setReportNumberDdNameBytes(byte[] buffer, int offset) {
		int position = offset;
		reportNumber = MarshalByte.readString(buffer, position, Len.REPORT_NUMBER);
		position += Len.REPORT_NUMBER;
		reportOfficeLocation = MarshalByte.readString(buffer, position, Len.REPORT_OFFICE_LOCATION);
	}

	public byte[] getReportNumberDdNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, reportNumber, Len.REPORT_NUMBER);
		position += Len.REPORT_NUMBER;
		MarshalByte.writeString(buffer, position, reportOfficeLocation, Len.REPORT_OFFICE_LOCATION);
		return buffer;
	}

	public void setReportNumber(String reportNumber) {
		this.reportNumber = Functions.subString(reportNumber, Len.REPORT_NUMBER);
	}

	public String getReportNumber() {
		return this.reportNumber;
	}

	public void setReportOfficeLocation(String reportOfficeLocation) {
		this.reportOfficeLocation = Functions.subString(reportOfficeLocation, Len.REPORT_OFFICE_LOCATION);
	}

	public String getReportOfficeLocation() {
		return this.reportOfficeLocation;
	}

	public void setOfficeLocationBytes(byte[] buffer) {
		setOfficeLocationBytes(buffer, 1);
	}

	/**Original name: SA-OFFICE-LOCATION<br>*/
	public byte[] getOfficeLocationBytes() {
		byte[] buffer = new byte[Len.OFFICE_LOCATION];
		return getOfficeLocationBytes(buffer, 1);
	}

	public void setOfficeLocationBytes(byte[] buffer, int offset) {
		int position = offset;
		division = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		subdivision = MarshalByte.readChar(buffer, position);
	}

	public byte[] getOfficeLocationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, division);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, subdivision);
		return buffer;
	}

	public void setDivision(char division) {
		this.division = division;
	}

	public char getDivision() {
		return this.division;
	}

	public void setSubdivision(char subdivision) {
		this.subdivision = subdivision;
	}

	public char getSubdivision() {
		return this.subdivision;
	}

	/**Original name: SA-BALANCE-REPORT-RECORD<br>*/
	public byte[] getBalanceReportRecordBytes() {
		byte[] buffer = new byte[Len.BALANCE_REPORT_RECORD];
		return getBalanceReportRecordBytes(buffer, 1);
	}

	public void setBalanceReportRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		balanceCarriageControl = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		balanceReportData = MarshalByte.readString(buffer, position, Len.BALANCE_REPORT_DATA);
	}

	public byte[] getBalanceReportRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, balanceCarriageControl);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, balanceReportData, Len.BALANCE_REPORT_DATA);
		return buffer;
	}

	public void setBalanceCarriageControl(char balanceCarriageControl) {
		this.balanceCarriageControl = balanceCarriageControl;
	}

	public char getBalanceCarriageControl() {
		return this.balanceCarriageControl;
	}

	public void setBalanceReportData(String balanceReportData) {
		this.balanceReportData = Functions.subString(balanceReportData, Len.BALANCE_REPORT_DATA);
	}

	public String getBalanceReportData() {
		return this.balanceReportData;
	}

	public void setCarriageControl(char carriageControl) {
		this.carriageControl = carriageControl;
	}

	public void setCarriageControlFormatted(String carriageControl) {
		setCarriageControl(Functions.charAt(carriageControl, Types.CHAR_SIZE));
	}

	public char getCarriageControl() {
		return this.carriageControl;
	}

	public void setLengthParmBytes(byte[] buffer) {
		setLengthParmBytes(buffer, 1);
	}

	public void setLengthParmBytes(byte[] buffer, int offset) {
		int position = offset;
		lpDataSize.setLpDataSize(MarshalByte.readPackedAsShort(buffer, position, SaLpDataSize.Len.Int.LP_DATA_SIZE, 0));
	}

	public byte[] getLengthParmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeShortAsPacked(buffer, position, lpDataSize.getLpDataSize(), SaLpDataSize.Len.Int.LP_DATA_SIZE, 0);
		return buffer;
	}

	public SaCurrentDateTime getCurrentDateTime() {
		return currentDateTime;
	}

	public SaDb2AccessProgram getDb2AccessProgram() {
		return db2AccessProgram;
	}

	public SaLpDataSize getLpDataSize() {
		return lpDataSize;
	}

	public SaReportOfficeLocationFlag getReportOfficeLocationFlag() {
		return reportOfficeLocationFlag;
	}

	public SaTsx131cmFileName getTsx131cmFileName() {
		return tsx131cmFileName;
	}

	public SaTsx132cmFileName getTsx132cmFileName() {
		return tsx132cmFileName;
	}

	public SaTsxxxxblFileName getTsxxxxblFileName() {
		return tsxxxxblFileName;
	}

	@Override
	public byte[] serialize() {
		return getSaveAreaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REPORT_NUMBER = 6;
		public static final int REPORT_OFFICE_LOCATION = 2;
		public static final int REPORT_NUMBER_DD_NAME = REPORT_NUMBER + REPORT_OFFICE_LOCATION;
		public static final int DIVISION = 1;
		public static final int SUBDIVISION = 1;
		public static final int OFFICE_LOCATION = DIVISION + SUBDIVISION;
		public static final int BALANCE_CARRIAGE_CONTROL = 1;
		public static final int BALANCE_REPORT_DATA = 198;
		public static final int BALANCE_REPORT_RECORD = BALANCE_CARRIAGE_CONTROL + BALANCE_REPORT_DATA;
		public static final int CARRIAGE_CONTROL = 1;
		public static final int LENGTH_PARM = SaLpDataSize.Len.LP_DATA_SIZE;
		public static final int SAVE_AREA = REPORT_NUMBER_DD_NAME + OFFICE_LOCATION + SaReportOfficeLocationFlag.Len.REPORT_OFFICE_LOCATION_FLAG
				+ SaTsx132cmFileName.Len.TSX132CM_FILE_NAME + SaTsx131cmFileName.Len.TSX131CM_FILE_NAME + SaTsxxxxblFileName.Len.TSXXXXBL_FILE_NAME
				+ SaCurrentDateTime.Len.CURRENT_DATE_TIME + BALANCE_REPORT_RECORD + CARRIAGE_CONTROL + LENGTH_PARM
				+ SaDb2AccessProgram.Len.DB2_ACCESS_PROGRAM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
