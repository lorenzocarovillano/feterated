/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9040-ROW<br>
 * Variable: WS-XZ0A9040-ROW from program XZ0B9040<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9040Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA940-MAX-FRM-ROWS
	private short maxFrmRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA940-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA940-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA940-PI-START-POINT
	private long piStartPoint = DefaultValues.LONG_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9040_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9040RowBytes(buf);
	}

	public String getWsXz0a9040RowFormatted() {
		return getGetFrmListKeyFormatted();
	}

	public void setWsXz0a9040RowBytes(byte[] buffer) {
		setWsXz0a9040RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9040RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9040_ROW];
		return getWsXz0a9040RowBytes(buffer, 1);
	}

	public void setWsXz0a9040RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetFrmListKeyBytes(buffer, position);
	}

	public byte[] getWsXz0a9040RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetFrmListKeyBytes(buffer, position);
		return buffer;
	}

	public String getGetFrmListKeyFormatted() {
		return MarshalByteExt.bufferToStr(getGetFrmListKeyBytes());
	}

	/**Original name: XZA940-GET-FRM-LIST-KEY<br>
	 * <pre>*************************************************************
	 *  XZ0A9040 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_FRM_LIST                           *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  20JAN2009 E404DLP    NEW                          *
	 *  PP02756  04FEB2011 E404DLP    ADDED PAGING FIELDS          *
	 * *************************************************************</pre>*/
	public byte[] getGetFrmListKeyBytes() {
		byte[] buffer = new byte[Len.GET_FRM_LIST_KEY];
		return getGetFrmListKeyBytes(buffer, 1);
	}

	public void setGetFrmListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		maxFrmRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		setPagingInputsBytes(buffer, position);
	}

	public byte[] getGetFrmListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxFrmRows);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		getPagingInputsBytes(buffer, position);
		return buffer;
	}

	public void setMaxFrmRows(short maxFrmRows) {
		this.maxFrmRows = maxFrmRows;
	}

	public short getMaxFrmRows() {
		return this.maxFrmRows;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setPagingInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		piStartPoint = MarshalByte.readLong(buffer, position, Len.PI_START_POINT);
	}

	public byte[] getPagingInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeLong(buffer, position, piStartPoint, Len.PI_START_POINT);
		return buffer;
	}

	public void setPiStartPoint(long piStartPoint) {
		this.piStartPoint = piStartPoint;
	}

	public long getPiStartPoint() {
		return this.piStartPoint;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9040RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_FRM_ROWS = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int PI_START_POINT = 10;
		public static final int PAGING_INPUTS = PI_START_POINT;
		public static final int GET_FRM_LIST_KEY = MAX_FRM_ROWS + CSR_ACT_NBR + NOT_PRC_TS + PAGING_INPUTS;
		public static final int WS_XZ0A9040_ROW = GET_FRM_LIST_KEY;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
