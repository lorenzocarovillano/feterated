/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WA-FORMATTED-DATE<br>
 * Variable: WA-FORMATTED-DATE from program TS020000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WaFormattedDate {

	//==== PROPERTIES ====
	//Original name: WA-FD-YYYY
	private String yyyy = DefaultValues.stringVal(Len.YYYY);
	//Original name: FILLER-WA-FORMATTED-DATE
	private char flr1 = '-';
	//Original name: WA-FD-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: FILLER-WA-FORMATTED-DATE-1
	private char flr2 = '-';
	//Original name: WA-FD-DD
	private String dd = DefaultValues.stringVal(Len.DD);

	//==== METHODS ====
	public String getWaFormattedDateFormatted() {
		return MarshalByteExt.bufferToStr(getWaFormattedDateBytes());
	}

	public byte[] getWaFormattedDateBytes() {
		byte[] buffer = new byte[Len.WA_FORMATTED_DATE];
		return getWaFormattedDateBytes(buffer, 1);
	}

	public byte[] getWaFormattedDateBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, yyyy, Len.YYYY);
		position += Len.YYYY;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position += Len.MM;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		return buffer;
	}

	public void setYyyy(String yyyy) {
		this.yyyy = Functions.subString(yyyy, Len.YYYY);
	}

	public String getYyyy() {
		return this.yyyy;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setMm(String mm) {
		this.mm = Functions.subString(mm, Len.MM);
	}

	public String getMm() {
		return this.mm;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setDd(String dd) {
		this.dd = Functions.subString(dd, Len.DD);
	}

	public String getDd() {
		return this.dd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YYYY = 4;
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int FLR1 = 1;
		public static final int WA_FORMATTED_DATE = YYYY + MM + DD + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
