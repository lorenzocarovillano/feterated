/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-NEW-REPORT-NUMBER-FLAG<br>
 * Variable: SW-NEW-REPORT-NUMBER-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwNewReportNumberFlag {

	//==== PROPERTIES ====
	private char value = '0';
	public static final char SAME_REPORT_NUMBER = '0';
	public static final char NEW_REPORT_NUMBER = '1';

	//==== METHODS ====
	public void setNewReportNumberFlag(char newReportNumberFlag) {
		this.value = newReportNumberFlag;
	}

	public char getNewReportNumberFlag() {
		return this.value;
	}

	public void setSameReportNumber() {
		value = SAME_REPORT_NUMBER;
	}

	public boolean isNewReportNumber() {
		return value == NEW_REPORT_NUMBER;
	}

	public void setNewReportNumber() {
		value = NEW_REPORT_NUMBER;
	}
}
