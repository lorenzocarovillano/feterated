/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q90G0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q90g0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q90g0() {
	}

	public LFrameworkRequestAreaXz0q90g0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza9g0qMaxRecRows(short xza9g0qMaxRecRows) {
		writeBinaryShort(Pos.XZA9G0_MAX_REC_ROWS, xza9g0qMaxRecRows);
	}

	/**Original name: XZA9G0Q-MAX-REC-ROWS<br>*/
	public short getXza9g0qMaxRecRows() {
		return readBinaryShort(Pos.XZA9G0_MAX_REC_ROWS);
	}

	public void setXza9g0qCsrActNbr(String xza9g0qCsrActNbr) {
		writeString(Pos.XZA9G0_CSR_ACT_NBR, xza9g0qCsrActNbr, Len.XZA9G0_CSR_ACT_NBR);
	}

	/**Original name: XZA9G0Q-CSR-ACT-NBR<br>*/
	public String getXza9g0qCsrActNbr() {
		return readString(Pos.XZA9G0_CSR_ACT_NBR, Len.XZA9G0_CSR_ACT_NBR);
	}

	public void setXza9g0qNotPrcTs(String xza9g0qNotPrcTs) {
		writeString(Pos.XZA9G0_NOT_PRC_TS, xza9g0qNotPrcTs, Len.XZA9G0_NOT_PRC_TS);
	}

	/**Original name: XZA9G0Q-NOT-PRC-TS<br>*/
	public String getXza9g0qNotPrcTs() {
		return readString(Pos.XZA9G0_NOT_PRC_TS, Len.XZA9G0_NOT_PRC_TS);
	}

	public void setXza9g0qRecallCertNbr(String xza9g0qRecallCertNbr) {
		writeString(Pos.XZA9G0_RECALL_CERT_NBR, xza9g0qRecallCertNbr, Len.XZA9G0_RECALL_CERT_NBR);
	}

	/**Original name: XZA9G0Q-RECALL-CERT-NBR<br>*/
	public String getXza9g0qRecallCertNbr() {
		return readString(Pos.XZA9G0_RECALL_CERT_NBR, Len.XZA9G0_RECALL_CERT_NBR);
	}

	public void setXza9g0qUserid(String xza9g0qUserid) {
		writeString(Pos.XZA9G0_USERID, xza9g0qUserid, Len.XZA9G0_USERID);
	}

	/**Original name: XZA9G0Q-USERID<br>*/
	public String getXza9g0qUserid() {
		return readString(Pos.XZA9G0_USERID, Len.XZA9G0_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A90G0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA9G0_GET_CERT_REC_LIST_KEY = L_FW_REQ_XZ0A90G0;
		public static final int XZA9G0_MAX_REC_ROWS = XZA9G0_GET_CERT_REC_LIST_KEY;
		public static final int XZA9G0_CSR_ACT_NBR = XZA9G0_MAX_REC_ROWS + Len.XZA9G0_MAX_REC_ROWS;
		public static final int XZA9G0_NOT_PRC_TS = XZA9G0_CSR_ACT_NBR + Len.XZA9G0_CSR_ACT_NBR;
		public static final int XZA9G0_RECALL_CERT_NBR = XZA9G0_NOT_PRC_TS + Len.XZA9G0_NOT_PRC_TS;
		public static final int XZA9G0_USERID = XZA9G0_RECALL_CERT_NBR + Len.XZA9G0_RECALL_CERT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9G0_MAX_REC_ROWS = 2;
		public static final int XZA9G0_CSR_ACT_NBR = 9;
		public static final int XZA9G0_NOT_PRC_TS = 26;
		public static final int XZA9G0_RECALL_CERT_NBR = 25;
		public static final int XZA9G0_USERID = 8;
		public static final int XZA9G0_GET_CERT_REC_LIST_KEY = XZA9G0_MAX_REC_ROWS + XZA9G0_CSR_ACT_NBR + XZA9G0_NOT_PRC_TS + XZA9G0_RECALL_CERT_NBR
				+ XZA9G0_USERID;
		public static final int L_FW_REQ_XZ0A90G0 = XZA9G0_GET_CERT_REC_LIST_KEY;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A90G0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
