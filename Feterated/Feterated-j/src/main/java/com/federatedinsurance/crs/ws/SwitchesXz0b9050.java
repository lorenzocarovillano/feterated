/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ0B9050<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz0b9050 {

	//==== PROPERTIES ====
	//Original name: SW-END-POL-LST-BY-NOT-CSR-FLAG
	private boolean endPolLstByNotCsrFlag = false;
	//Original name: SW-END-POL-LST-BY-FRM-CSR-FLAG
	private boolean endPolLstByFrmCsrFlag = false;
	//Original name: SW-END-POL-LST-BY-ADR-CSR-FLAG
	private boolean endPolLstByAdrCsrFlag = false;
	//Original name: SW-END-POL-LST-BY-OWN-CSR-FLAG
	private boolean endPolLstByOwnCsrFlag = false;
	//Original name: SW-ADD-FEE-TO-NON-POL-OWN-FLAG
	private boolean addFeeToNonPolOwnFlag = false;

	//==== METHODS ====
	public void setEndPolLstByNotCsrFlag(boolean endPolLstByNotCsrFlag) {
		this.endPolLstByNotCsrFlag = endPolLstByNotCsrFlag;
	}

	public boolean isEndPolLstByNotCsrFlag() {
		return this.endPolLstByNotCsrFlag;
	}

	public void setEndPolLstByFrmCsrFlag(boolean endPolLstByFrmCsrFlag) {
		this.endPolLstByFrmCsrFlag = endPolLstByFrmCsrFlag;
	}

	public boolean isEndPolLstByFrmCsrFlag() {
		return this.endPolLstByFrmCsrFlag;
	}

	public void setEndPolLstByAdrCsrFlag(boolean endPolLstByAdrCsrFlag) {
		this.endPolLstByAdrCsrFlag = endPolLstByAdrCsrFlag;
	}

	public boolean isEndPolLstByAdrCsrFlag() {
		return this.endPolLstByAdrCsrFlag;
	}

	public void setEndPolLstByOwnCsrFlag(boolean endPolLstByOwnCsrFlag) {
		this.endPolLstByOwnCsrFlag = endPolLstByOwnCsrFlag;
	}

	public boolean isEndPolLstByOwnCsrFlag() {
		return this.endPolLstByOwnCsrFlag;
	}

	public void setAddFeeToNonPolOwnFlag(boolean addFeeToNonPolOwnFlag) {
		this.addFeeToNonPolOwnFlag = addFeeToNonPolOwnFlag;
	}

	public boolean isAddFeeToNonPolOwnFlag() {
		return this.addFeeToNonPolOwnFlag;
	}
}
