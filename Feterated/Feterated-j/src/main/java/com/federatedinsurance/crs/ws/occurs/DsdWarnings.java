/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: DSD-WARNINGS<br>
 * Variables: DSD-WARNINGS from copybook TS020DRV<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class DsdWarnings {

	//==== PROPERTIES ====
	//Original name: DSD-WARN-MSG
	private String dsdWarnMsg = DefaultValues.stringVal(Len.DSD_WARN_MSG);

	//==== METHODS ====
	public void setWarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		dsdWarnMsg = MarshalByte.readString(buffer, position, Len.DSD_WARN_MSG);
	}

	public byte[] getWarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, dsdWarnMsg, Len.DSD_WARN_MSG);
		return buffer;
	}

	public void initWarningsSpaces() {
		dsdWarnMsg = "";
	}

	public void setDsdWarnMsg(String dsdWarnMsg) {
		this.dsdWarnMsg = Functions.subString(dsdWarnMsg, Len.DSD_WARN_MSG);
	}

	public String getDsdWarnMsg() {
		return this.dsdWarnMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DSD_WARN_MSG = 500;
		public static final int WARNINGS = DSD_WARN_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
