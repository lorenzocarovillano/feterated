/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalBoMduXrfV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [HAL_BO_MDU_XRF_V]
 * 
 */
public class HalBoMduXrfVDao extends BaseSqlDao<IHalBoMduXrfV> {

	public HalBoMduXrfVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalBoMduXrfV> getToClass() {
		return IHalBoMduXrfV.class;
	}

	public String selectByHbmxBusObjNm(String hbmxBusObjNm, String dft) {
		return buildQuery("selectByHbmxBusObjNm").bind("hbmxBusObjNm", hbmxBusObjNm).scalarResultString(dft);
	}

	public String selectByHbmxBusObjNm1(String hbmxBusObjNm, String dft) {
		return buildQuery("selectByHbmxBusObjNm1").bind("hbmxBusObjNm", hbmxBusObjNm).scalarResultString(dft);
	}
}
