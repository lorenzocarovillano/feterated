/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC010-NON-LOGGABLE-ERRORS<br>
 * Variables: XZC010-NON-LOGGABLE-ERRORS from copybook XZC010C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzc010NonLoggableErrors {

	//==== PROPERTIES ====
	//Original name: XZC010-NON-LOG-ERR-MSG
	private String xzc010NonLogErrMsg = DefaultValues.stringVal(Len.XZC010_NON_LOG_ERR_MSG);

	//==== METHODS ====
	public void setXzc010NonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc010NonLogErrMsg = MarshalByte.readString(buffer, position, Len.XZC010_NON_LOG_ERR_MSG);
	}

	public byte[] getXzc010NonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc010NonLogErrMsg, Len.XZC010_NON_LOG_ERR_MSG);
		return buffer;
	}

	public void initXzc010NonLoggableErrorsSpaces() {
		xzc010NonLogErrMsg = "";
	}

	public void setXzc010NonLogErrMsg(String xzc010NonLogErrMsg) {
		this.xzc010NonLogErrMsg = Functions.subString(xzc010NonLogErrMsg, Len.XZC010_NON_LOG_ERR_MSG);
	}

	public String getXzc010NonLogErrMsg() {
		return this.xzc010NonLogErrMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC010_NON_LOG_ERR_MSG = 500;
		public static final int XZC010_NON_LOGGABLE_ERRORS = XZC010_NON_LOG_ERR_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
