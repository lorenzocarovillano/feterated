/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TIME-SW<br>
 * Variable: WS-TIME-SW from program XPIODAT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTimeSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FOUND = 'Y';
	public static final char NOT_FOUND = 'N';

	//==== METHODS ====
	public void setWsTimeSw(char wsTimeSw) {
		this.value = wsTimeSw;
	}

	public void setWsTimeSwFormatted(String wsTimeSw) {
		setWsTimeSw(Functions.charAt(wsTimeSw, Types.CHAR_SIZE));
	}

	public char getWsTimeSw() {
		return this.value;
	}

	public boolean isFound() {
		return value == FOUND;
	}

	public boolean isNotFound() {
		return value == NOT_FOUND;
	}
}
