/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TP-POL-INF<br>
 * Variables: TP-POL-INF from program XZ0B90P0<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TpPolInf {

	//==== PROPERTIES ====
	//Original name: TP-POL-NBR
	private String nbr = DefaultValues.stringVal(Len.NBR);
	//Original name: TP-POL-EFF-DT
	private String effDt = DefaultValues.stringVal(Len.EFF_DT);
	//Original name: TP-POL-EXP-DT
	private String expDt = DefaultValues.stringVal(Len.EXP_DT);

	//==== METHODS ====
	public void initTpPolInfSpaces() {
		nbr = "";
		effDt = "";
		expDt = "";
	}

	public void setNbr(String nbr) {
		this.nbr = Functions.subString(nbr, Len.NBR);
	}

	public String getNbr() {
		return this.nbr;
	}

	public void setEffDt(String effDt) {
		this.effDt = Functions.subString(effDt, Len.EFF_DT);
	}

	public String getEffDt() {
		return this.effDt;
	}

	public void setExpDt(String expDt) {
		this.expDt = Functions.subString(expDt, Len.EXP_DT);
	}

	public String getExpDt() {
		return this.expDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NBR = 25;
		public static final int EFF_DT = 10;
		public static final int EXP_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
