/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-60-GEN-USER-ERROR-MSG<br>
 * Variable: EA-60-GEN-USER-ERROR-MSG from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea60GenUserErrorMsgTs547099 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-60-GEN-USER-ERROR-MSG
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-60-GEN-USER-ERROR-MSG-1
	private String flr2 = "AN EXCI USER";
	//Original name: FILLER-EA-60-GEN-USER-ERROR-MSG-2
	private String flr3 = "ERROR CODE WAS";
	//Original name: FILLER-EA-60-GEN-USER-ERROR-MSG-3
	private String flr4 = "CAPTURED.";
	//Original name: FILLER-EA-60-GEN-USER-ERROR-MSG-4
	private String flr5 = "PLEASE CORRECT.";

	//==== METHODS ====
	public String getEa60GenUserErrorMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa60GenUserErrorMsgBytes());
	}

	public byte[] getEa60GenUserErrorMsgBytes() {
		byte[] buffer = new byte[Len.EA60_GEN_USER_ERROR_MSG];
		return getEa60GenUserErrorMsgBytes(buffer, 1);
	}

	public byte[] getEa60GenUserErrorMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 13;
		public static final int FLR3 = 15;
		public static final int EA60_GEN_USER_ERROR_MSG = 2 * FLR1 + FLR2 + 2 * FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
