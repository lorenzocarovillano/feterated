/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.ITaxTypeV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [TAX_TYPE_V]
 * 
 */
public class TaxTypeVDao extends BaseSqlDao<ITaxTypeV> {

	public TaxTypeVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ITaxTypeV> getToClass() {
		return ITaxTypeV.class;
	}

	public String selectByTaxTypeCd(String taxTypeCd, String dft) {
		return buildQuery("selectByTaxTypeCd").bind("taxTypeCd", taxTypeCd).scalarResultString(dft);
	}
}
