/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZC09090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXzc09090 {

	//==== PROPERTIES ====
	//Original name: EA-01-ERROR
	private Ea01ErrorXzc09090 ea01Error = new Ea01ErrorXzc09090();
	//Original name: EA-02-INVALID-INPUT
	private Ea02InvalidInputXzc09090 ea02InvalidInput = new Ea02InvalidInputXzc09090();
	//Original name: EA-03-CICS-LINK-ERROR
	private Ea03CicsLinkError ea03CicsLinkError = new Ea03CicsLinkError();
	//Original name: EA-04-SOAP-FAULT
	private Ea04SoapFault ea04SoapFault = new Ea04SoapFault();
	//Original name: EA-05-WEB-URI-FAULT
	private Ea05WebUriFault ea05WebUriFault = new Ea05WebUriFault();
	//Original name: EA-06-CONTAINER-ERROR
	private Ea06ContainerError ea06ContainerError = new Ea06ContainerError();
	//Original name: EA-07-READQ-ERROR
	private Ea07ReadqError ea07ReadqError = new Ea07ReadqError();
	//Original name: EA-08-TAR-SYS-NOT-FND
	private Ea08TarSysNotFndXzc09090 ea08TarSysNotFnd = new Ea08TarSysNotFndXzc09090();

	//==== METHODS ====
	public Ea01ErrorXzc09090 getEa01Error() {
		return ea01Error;
	}

	public Ea02InvalidInputXzc09090 getEa02InvalidInput() {
		return ea02InvalidInput;
	}

	public Ea03CicsLinkError getEa03CicsLinkError() {
		return ea03CicsLinkError;
	}

	public Ea04SoapFault getEa04SoapFault() {
		return ea04SoapFault;
	}

	public Ea05WebUriFault getEa05WebUriFault() {
		return ea05WebUriFault;
	}

	public Ea06ContainerError getEa06ContainerError() {
		return ea06ContainerError;
	}

	public Ea07ReadqError getEa07ReadqError() {
		return ea07ReadqError;
	}

	public Ea08TarSysNotFndXzc09090 getEa08TarSysNotFnd() {
		return ea08TarSysNotFnd;
	}
}
