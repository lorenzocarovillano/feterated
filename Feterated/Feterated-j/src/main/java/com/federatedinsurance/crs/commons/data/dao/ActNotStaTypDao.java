/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotStaTyp;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [ACT_NOT_STA_TYP]
 * 
 */
public class ActNotStaTypDao extends BaseSqlDao<IActNotStaTyp> {

	public ActNotStaTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotStaTyp> getToClass() {
		return IActNotStaTyp.class;
	}

	public char selectRec(String actNotStaCd, char cfYes, char dft) {
		return buildQuery("selectRec").bind("actNotStaCd", actNotStaCd).bind("cfYes", String.valueOf(cfYes)).scalarResultChar(dft);
	}
}
