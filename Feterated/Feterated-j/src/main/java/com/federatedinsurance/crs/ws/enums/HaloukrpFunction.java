/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: HALOUKRP-FUNCTION<br>
 * Variable: HALOUKRP-FUNCTION from copybook HALLUKRP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HaloukrpFunction {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char STORE_FUNC = 'S';
	public static final char REWRITE_FUNC = 'U';
	public static final char RETRIEVE_FUNC = 'R';
	public static final char CLEAR_MSG_ID_FUNC = 'C';
	public static final char DELETE_FUNC = 'D';

	//==== METHODS ====
	public void setFunction(char function) {
		this.value = function;
	}

	public char getFunction() {
		return this.value;
	}

	public boolean isStoreFunc() {
		return value == STORE_FUNC;
	}

	public void setHaloukrpStoreFunc() {
		value = STORE_FUNC;
	}

	public boolean isRewriteFunc() {
		return value == REWRITE_FUNC;
	}

	public boolean isRetrieveFunc() {
		return value == RETRIEVE_FUNC;
	}

	public void setHaloukrpRetrieveFunc() {
		value = RETRIEVE_FUNC;
	}

	public boolean isClearMsgIdFunc() {
		return value == CLEAR_MSG_ID_FUNC;
	}

	public void setHaloukrpClearMsgIdFunc() {
		value = CLEAR_MSG_ID_FUNC;
	}

	public boolean isDeleteFunc() {
		return value == DELETE_FUNC;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FUNCTION = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
