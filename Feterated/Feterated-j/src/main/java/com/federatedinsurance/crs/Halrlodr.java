/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalBoLokExcVDao;
import com.federatedinsurance.crs.commons.data.dao.HalBoLokTypeVDao;
import com.federatedinsurance.crs.commons.data.dao.HalBoMduXrfVDao;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.commons.data.dao.HalSecProfileVDao;
import com.federatedinsurance.crs.commons.data.dao.HalUniversalCtVDao;
import com.federatedinsurance.crs.commons.data.dao.HalUowTransactVDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.ws.DefaultComm;
import com.federatedinsurance.crs.ws.HalrlodrData;
import com.federatedinsurance.crs.ws.UbocRecord;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsHalrlodrLinkage;
import com.federatedinsurance.crs.ws.WsHalrlomgLinkage1;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.enums.HalrlodrFunction;
import com.federatedinsurance.crs.ws.enums.HalrlomgLokTypeCd;
import com.federatedinsurance.crs.ws.enums.HalrlomgSuccessInd;
import com.federatedinsurance.crs.ws.enums.UbocUowLockStrategyCd;
import com.federatedinsurance.crs.ws.enums.UlliLegacyLockResp;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: HALRLODR<br>
 * <pre>AUTHOR.
 *                   CSC.
 * DATE-WRITTEN.
 *                   DEC 2000.
 * DATE-COMPILED.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  LOCK DRIVER BDO (BUSINESS DATA OBJECT)     **
 * *                                                             **
 * * PLATFORM - IBM MAINFRAME                                    **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE OF THIS PROGRAM IS THE BUSINESS INTERFACE BETWEEN   **
 * *           THE BDO AND THE LOCKING SUB-SYSTEM.               **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED AS FOLLOWS:   **
 * *                                                             **
 * *                       1) DYNAMICALLY CALLED BY A MODULE     **
 * *                                                             **
 * * DATA ACCESS METHODS - DATA PASSED IN LINKAGE                **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #       DATE     PROG    DESCRIPTION                     **
 * * --------  --------  ------  --------------------------------**
 * * (NEW)     12DEC00           NEW PROGRAM.                    **
 * * C13798    24MAY01   18600   CHANGE TO RETURN THE CHECKED-OUT**
 * *                             USER-ID INSTEAD OF THE LOGGED-ON**
 * *                             USER-ID.                        **
 * *                     07077   CHANGE TO SET BCMLCHK-CHK-TYPE  **
 * *                             PRIOR TO CALLING BCMOCHK. FIX TO**
 * *                             BCWS LOCKS ON ARCH_LOK_RUP_V.   **
 * * 16324     10SEP01   18448   CHANGE CODE ADDED BY BILLING SO **
 * *                             THAT IT DOES NOT NEGATIVELY     **
 * *                             IMPACT THE OTHER APPLICATIONS.  **
 * * 17238     08OCT01   18448   REPLACEMENT OF ORIGINAL HALRLODR**
 * *                             FOR INFRASTRUCTURE 2.3.  THE    **
 * *                             PROGRAM HAS BEEN REDESIGNED TO  **
 * *                             INTERFACE WITH LEGACY LOCK      **
 * *                             INTERFACE MODULES FOR ALL       **
 * *                             APP-SPECIFIC LOCK PROCESSING.   **
 * *                             INFRASTRUCTURE LOCK PROCESSING  **
 * *                             IS STILL HANDLED VIA CALLS TO   **
 * *                             HALRLOMG.                       **
 * * 17241     08OCT01   18448   ALL REFERENCES TO IAP AND OTHER **
 * *                             APPLICATIONS HAVE BEEN REMOVED. **
 * *                             THE DB2 SYSTEM DATE IS USED TO  **
 * *                             DERIVE THE EXPIRY TIMESTAMP.    **
 * * 19870B    22MAR02   18448   IF NO LEGACY LOCK MODULE, DON'T **
 * *                             CHECK RETURN.                   **
 * * 24597     23OCT02   18448   USE UMT FOR UOW LEVEL LOCK      **
 * *  GA                         PROCESSING.  VERIFY LOCK ON     **
 * *                             DETAIL IS SAME HAS REQUESTED    **
 * *                             LOCK.  IF NOT, AND PESSI IS     **
 * *                             REQUESTED, TRY TO GET A PESSI.  **
 * * 32701     23JUN03   18448   ADD GROUP LOCKING CAPABILITIES. **
 * * 34504     28AUG03   18448   CHECK LOCKS EVEN ON AN INSERT.  **
 * * 35840     07NOV03   18448   DISABLE THE HAL_LOKUSR NON-LOG  **
 * *                             WARNING.                        **
 * * 36295     01DEC03   18448   ADD SECOND PHASE OF GROUP LEVEL **
 * *                             LOCKING.  ALLOW CONVERSION OF   **
 * *                             PESSIMISTIC TO GROUP IF OWNER OF**
 * *                             PESSI IS THE GROUP MEMBER       **
 * *                             REQUESTING THE LOCK CONVERSION. **
 * *                             ALLOW CONVERSION OF GROUP TO    **
 * *                             PESSI IF USER IS A MEMBER OF THE**
 * *                             GROUP THAT CURRENTLY OWNS THE   **
 * *                             LOCK.                           **
 * * 37398     10FEB04    18448  LOCK TAKE OVER OF GROUP LEVEL   **
 * *                             LOCKS.  ALLOW A MEMBER OF THE   **
 * *                             ACTIVE LOCK GROUP TO TAKE OVER  **
 * *                             THE GROUP LOCK, CONVERTING IT TO**
 * *                             A PESSI LOCK WITHOUT FIRST      **
 * *                             HAVING ESTABLISHED AN ACTIVE    **
 * *                             GROUP LOCK FOR HIST SESSION ID. **
 * ****************************************************************
 * ********** REMAINDER OF CODE NO LONGER USED. **************
 * 0720-INIT-XPIOCHK-PARMS SECTION.
 * *****************************************************************
 *                                                                 *
 *  SET UP LOCK KEY AND OTHER PM PARAMETERS                        *
 *                                                                 *
 * *****************************************************************
 *     INITIALIZE WS-PMCA-PARMS.
 *     MOVE HALRLODR-TCH-KEY        TO PM-CIOR-FOLDER-ID
 *                                     PM-CIOR-REF-ID
 *                                     PM-CIOR-KEY.
 *     MOVE HALRLODR-APP-ID         TO PM-CIOR-APP-ID.
 *     MOVE HALRLODR-TABLE-NM       TO PM-CIOR-TABLE.
 *     MOVE WS-READ-LOCK-TYPE       TO PM-CIOR-FUNCTION.
 * * ISSUE NODE POPULATES DBLK_DB_NBR
 *     MOVE UBOC-AUTH-USERID        TO PMCA-ISSUE-NODE.
 *     MOVE UBOC-AUTH-USERID        TO PM-CIOR-USER-ID.
 *     MOVE UBOC-AUTH-USER-CLIENTID TO PM-CIOR-CLIENT-ID.
 *     MOVE SPACES                  TO PM-CIOR-EXPIRATION-DATE
 *                                     PM-CIOR-CHKD-OUT.
 *     MOVE '00'                    TO PM-CIOR-RET-CODE.
 *     IF PM-CIOR-EXPIRATION-DATE EQUAL SPACES
 *        MOVE WS-DEFAULT-EXP-TS    TO PM-CIOR-EXPIRATION-DATE.
 *     MOVE SPACES                  TO DATE-STRUCTURE.
 *     MOVE 'C'                     TO DATE-FUNCTION.
 *     MOVE 'S3'                    TO DATE-INP-FORMAT.
 *     MOVE 'B '                    TO DATE-OUT-FORMAT.
 *     MOVE SPACES                  TO DATE-LANGUAGE
 *                                     DATE-ADJ-FORMAT
 *                                     DATE-OUTPUT.
 *     PERFORM 1400-CALL-XPIODAT.
 *     MOVE DATE-OUTPUT             TO WS-CURRENT-TS
 *                                     WS-S3DT-BREAKDOWN.
 * 0720-INIT-XPIOCHK-PARMS-X.
 *     EXIT.
 * 0730-INIT-BCMOCHK-PARMS SECTION.
 * *****************************************************************
 *                                                                 *
 *  SET UP LOCK KEY AND OTHER PARAMETERS FOR BCMOCHK               *
 *                                                                 *
 * *****************************************************************
 *     INITIALIZE BCMLCHK-LINK-WORK-AREA.
 *     MOVE PM-CIOR-FOLDER-ID TO BCMLCHK-ACCT-TTY-KEY.
 *     MOVE PM-CIOR-USER-ID   TO BCMLCHK-USER-ID.
 *     MOVE PM-CIOR-FUNCTION  TO BCMLCHK-FUNCTION-NM.
 *     MOVE PM-CIOR-CLIENT-ID TO BCMLCHK-CLIENT-ID.
 *     MOVE PMCA-ISSUE-NODE   TO BCMLCHK-ISSUE-NODE-ID.
 *     MOVE ZERO              TO PM-CIOR-RET-CODE.
 * ***************************
 * * FUNCTION CALLED FOR... **
 * ***************************
 *     IF  CIOR-READ-FOR-UPDATE
 *     OR  CIOR-READ-ONLY
 *        EVALUATE TRUE
 *           WHEN HALRLODR-BCWS-ACCT
 *              MOVE 'ALK' TO BCMLCHK-CHK-TYPE
 *           WHEN HALRLODR-BCWS-TTY
 *              MOVE 'TLK' TO BCMLCHK-CHK-TYPE
 *           WHEN HALRLODR-BCWS-AGT
 *              MOVE 'GLK' TO BCMLCHK-CHK-TYPE
 *        END-EVALUATE
 *     ELSE
 *     IF CIOR-FORCE-RELEASE
 *     OR CIOR-RELEASE-READ-UPDATE
 *     OR CIOR-UPDATE
 *        EVALUATE TRUE
 *           WHEN HALRLODR-BCWS-ACCT
 *              MOVE 'AUL' TO BCMLCHK-CHK-TYPE
 *           WHEN HALRLODR-BCWS-TTY
 *              MOVE 'TUL' TO BCMLCHK-CHK-TYPE
 *           WHEN HALRLODR-BCWS-AGT
 *              MOVE 'GUL' TO BCMLCHK-CHK-TYPE
 *        END-EVALUATE
 *     ELSE
 *        SET CIOR-PARMS-INVALID TO TRUE
 *     END-IF.
 * 0730-INIT-BCMOCHK-PARMS-X.
 *     EXIT.
 * 0740-XPIOCHK-RON-PROC SECTION.
 * *****************************************************************
 *                                                                 *
 *  CARRY OUT LOCKING PROCESS FOR READONLY LOCKS DEPENDING ON      *
 *  THE PASSED FUNCTION                                            *
 *                                                                 *
 * *****************************************************************
 *     EVALUATE TRUE
 * * "CREATE_ACCESS_LOCK": CHECK FOR A READUPDT LOCK EXISTING FOR
 * *  THIS LOCK KEY. IF ONE IS FOUND ISSUE A WARNING MESSAGE BUT
 * *  CONTINUE PROCESSING. NO READONLY LOCK IS ACTUALLY WRITTEN.
 *         WHEN HALRLODR-CREATE-ACCESS-LOCK
 *              PERFORM 0800-CHK-XPI-RON-LOCK
 * * "AUTHENTICATE_ACCESS_LOCK": SHOULD NOT OCCUR -
 * * IF IT DOES IT'S INVALID
 *         WHEN OTHER
 *              SET WS-LOG-ERROR                TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
 *              SET EFAL-BUS-PROCESS-FAILED     TO TRUE
 *              SET BUSP-INVALID-FUNCTION       TO TRUE
 *              MOVE '0740-XPIOCHK-RON-PROC'
 *                TO EFAL-ERR-PARAGRAPH
 *              MOVE 'INVALID PASSED FUNCTION'  TO EFAL-ERR-COMMENT
 *              STRING 'HALRLODR-FUNCTION = '
 *                      HALRLODR-FUNCTION ';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 0740-XPIOCHK-RON-PROC-X
 *     END-EVALUATE.
 * 0740-XPIOCHK-RON-PROC-X.
 *     EXIT.
 * 0760-XPIOCHK-RUP-PROC SECTION.
 * *****************************************************************
 *                                                                 *
 *  CARRY OUT LOCKING PROCESS FOR READUPDT LOCKS DEPENDING ON      *
 *  THE PASSED FUNCTION                                            *
 *                                                                 *
 * *****************************************************************
 *     EVALUATE TRUE
 * * "AUTHENTICATE_ACCESS_LOCK": CHECKS TO SEE IF A LOCK ALREADY
 * *  EXISTS FOR THIS BUSINESS DATA. RESULTS CAN BE:
 * *  1. LOCK FOUND FOR ANOTHER USER - PRODUCES HAL_LOKOTH N.L.B.E.
 * *  2. NO LOCK FOUND - PRODUCES HAL_LOKNFD NON-LOGGABLE BUS. ERR.
 * *  3. LOCK FOUND FOR THIS USER - OK, NOW UPDATE THE LOCK EXPIRY
 * *     TIMESTAMP WITH THE CURRENT SYSTEM TIME
 * *     LOCK-TIMEOUT-INTERVAL.
 *         WHEN HALRLODR-AUTH-ACCESS-LOCK
 *              PERFORM 0900-CHK-XPI-RUP-LOCK
 * * "CREATE_ACCESS_LOCK": WRITES A READUPDT LOCK FOR THIS BUSINESS
 * *  DATA. RESULTS CAN BE:
 * *  1. LOCK FOUND FOR ANOTHER USER - PRODUCES LOCKERR001 N.L.B.E.
 * *  2. LOCK IS CREATED - OK.
 *         WHEN HALRLODR-CREATE-ACCESS-LOCK
 *              PERFORM 1000-LOCK-W-XPIOCHK
 *         WHEN OTHER
 *              SET WS-LOG-ERROR                TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
 *              SET EFAL-BUS-PROCESS-FAILED     TO TRUE
 *              SET BUSP-INVALID-FUNCTION       TO TRUE
 *              MOVE '0760-XPIOCHK-RUP-PROC'
 *                TO EFAL-ERR-PARAGRAPH
 *              MOVE 'INVALID LOCK ACTION CODE' TO EFAL-ERR-COMMENT
 *              STRING 'HALRLODR-FUNCTION = '
 *                      HALRLODR-FUNCTION ';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 0760-XPIOCHK-RUP-PROC-X
 *     END-EVALUATE.
 * 0760-XPIOCHK-RUP-PROC-X.
 *     EXIT.
 * 0800-CHK-XPI-RON-LOCK SECTION.
 * *****************************************************************
 *                                                                 *
 *  CIOR-FOLDER-LOCK OPTION IS USED TO DETERMINE IF SOMEONE ELSE   *
 *  HAS THE REQUESTED FOLDER CHECKED OUT FOR UPDATE. IF SO, A      *
 *  WARNING WILL BE SET WITH THE CLIENT ID OF THE USER             *
 *  THAT HOLDS THE LOCK.                                           *
 *                                                                 *
 * *****************************************************************
 *     SET CIOR-QUERY-READ-UPDATE            TO TRUE.
 *     PERFORM 1200-CALL-XPIOCHK.
 *     EVALUATE TRUE
 *         WHEN CIOR-PARMS-OK
 * * LOCK EXISTS - ISSUE A WARNING
 *              PERFORM 0850-ISSUE-XPI-LOCK-WNG
 *         WHEN CIOR-DATA-NOT-FOUND
 * * LOCK DOES NOT EXIST - OK
 *              GO TO 0800-CHK-XPI-RON-LOCK-X
 *         WHEN OTHER
 *              SET WS-LOG-ERROR                TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
 *              SET EFAL-IAP-FAILED             TO TRUE
 *              SET IAP-XPIOCHK-CALL-ERROR      TO TRUE
 *              MOVE '0800-CHK-XPI-RON-LOCK'
 *                TO EFAL-ERR-PARAGRAPH
 *              MOVE 'UNEXPECTED RETURN CODE FROM XPIOCHK'
 *                TO EFAL-ERR-COMMENT
 *              STRING 'PM-CIOR-RET-CODE = '
 *                      PM-CIOR-RET-CODE ';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 0800-CHK-XPI-RON-LOCK-X
 *     END-EVALUATE.
 * 0800-CHK-XPI-RON-LOCK-X.
 *     EXIT.
 * 0850-ISSUE-XPI-LOCK-WNG SECTION.
 * *****************************************************************
 *                                                                 *
 *  A LOCK EXISTS - WRITE A WARNING THAT THE FOLDER IS CHECKED     *
 *  OUT FOR UPDATE.                                                *
 *                                                                 *
 * *****************************************************************
 *     SET WS-LOG-WARNING            TO TRUE
 *     MOVE '0850-ISSUE-XPI-LOCK-WNG-X' TO EFAL-ERR-PARAGRAPH.
 *     STRING 'WARNING - LOCKED FOR UPDATE BY USERID: '
 *             PM-CIOR-USER-ID
 *             DELIMITED BY SIZE
 *             INTO EFAL-ERR-COMMENT
 *     END-STRING.
 *     PERFORM 9000-LOG-WARNING-OR-ERROR.
 * 0850-ISSUE-XPI-LOCK-WNG-X.
 *     EXIT.
 * 0900-CHK-XPI-RUP-LOCK SECTION.
 * *****************************************************************
 *                                                                 *
 *  CIOR-FOLDER-LOCK OPTION IS USED TO DETERMINE IF SOMEONE ELSE   *
 *  HAS THE REQUESTED FOLDER CHECKED OUT. IF SO, A NON-LOGGABLE    *
 *  BUSINESS ERROR WILL BE SET WITH THE CLIENT ID OF THE USER      *
 *  THAT HOLDS THE LOCK. A NON-LOGGABLE BUSINESS ERROR WILL ALSO   *
 *  BE SET IF HBLT-LOK-TYPE-CD    = 'P' AND NO S3 LOCK IS FOUND.   *
 *                                                                 *
 * *****************************************************************
 *     SET CIOR-QUERY-READ-UPDATE            TO TRUE.
 *     PERFORM 1200-CALL-XPIOCHK.
 *     EVALUATE TRUE
 *         WHEN CIOR-PARMS-OK
 * * LOCK EXISTS - CHECK WHO OWNS THE LOCK
 *              PERFORM 0950-CHECK-LOCK-USER
 *         WHEN CIOR-DATA-NOT-FOUND
 *           IF HBLT-LOK-TYPE-CD    = 'P'
 * * LOCK DOES NOT EXIST - ERROR IF LOKTYPE CODE IS PESSIMISTIC
 *              SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
 *              MOVE 'LOCKING: LOCK NOT FOUND'
 *                TO NLBE-FAILED-TABLE-OR-FILE
 *              MOVE SPACES TO NLBE-FAILED-COLUMN-OR-FIELD
 *              MOVE 'HAL_LOKNFD'        TO NLBE-ERROR-CODE
 *              SET UBOC-HALT-AND-RETURN TO TRUE
 *              PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
 *              GO TO 0900-CHK-XPI-RUP-LOCK-X
 *           ELSE
 *              GO TO 0900-CHK-XPI-RUP-LOCK-X
 *           END-IF
 *         WHEN OTHER
 *              SET WS-LOG-ERROR              TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE    TO TRUE
 *              SET EFAL-IAP-FAILED           TO TRUE
 *              SET IAP-XPIOCHK-CALL-ERROR    TO TRUE
 *              MOVE '0900-CHK-XPI-RUP-LOCK'
 *                   TO EFAL-ERR-PARAGRAPH
 *              MOVE 'UNEXPECTED RETURN CODE FROM XPIOCHK'
 *                   TO EFAL-ERR-COMMENT
 *              STRING 'PM-CIOR-RET-CODE = '
 *                      PM-CIOR-RET-CODE ';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 0900-CHK-XPI-RUP-LOCK-X
 *     END-EVALUATE.
 * 0900-CHK-XPI-RUP-LOCK-X.
 *     EXIT.
 * 0950-CHECK-LOCK-USER SECTION.
 * *****************************************************************
 *                                                                 *
 *  A LOCK EXISTS - CHECK THE USER WHO HAS THIS OBJECT LOCKED.     *
 *  IF LOCK OWNER IS THE CURRENT USER, THEN CHECKOUT THE FOLDER    *
 *  AGAIN - THIS HAS THE EFFECT OF UPDATING THE EXPIRY TIMESTAMP.  *
 *  IF A DIFFERENT USER OWNS THE LOCK, THEN ERROR.                 *
 *                                                                 *
 * *****************************************************************
 * **  IF PM-CIOR-USER-ID      EQUAL UBOC-AUTH-USERID
 * ** C13798  IF BCMLCHK-ISSUE-NODE-ID EQUAL UBOC-AUTH-USERID
 *     IF PM-CIOR-WHO-CHKD-OUT EQUAL UBOC-AUTH-USERID
 *       OR (HALRLODR-APP-BCWS
 *           AND BCMLCHK-ISSUE-NODE-ID EQUAL UBOC-AUTH-USERID)
 *         MOVE WS-READ-LOCK-TYPE TO PM-CIOR-FUNCTION
 * *****C13798         IF NOT HALRLODR-BCWS-BCWSACCT
 *         IF NOT HALRLODR-APP-BCWS
 *            PERFORM 1000-LOCK-W-XPIOCHK
 *         END-IF
 *     ELSE
 *         SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
 *         MOVE 'LOCK ALREADY EXISTS FOR USERID :'
 *           TO NLBE-FAILED-TABLE-OR-FILE
 * **      MOVE PM-CIOR-USER-ID
 * **C13798         MOVE BCMLCHK-ISSUE-NODE-ID
 *         IF HALRLODR-APP-BCWS
 *             MOVE BCMLCHK-ISSUE-NODE-ID
 *               TO NLBE-FAILED-COLUMN-OR-FIELD
 *                 WS-NONLOG-ERR-COL1-VALUE
 *         ELSE
 *             MOVE PM-CIOR-WHO-CHKD-OUT
 *               TO NLBE-FAILED-COLUMN-OR-FIELD
 *                 WS-NONLOG-ERR-COL1-VALUE
 *         END-IF
 *         MOVE 'HAL_LOKOTH'        TO NLBE-ERROR-CODE
 *         SET UBOC-HALT-AND-RETURN TO TRUE
 *         PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
 *         GO TO 0950-CHECK-LOCK-USER-X
 *     END-IF.
 * 0950-CHECK-LOCK-USER-X.
 *     EXIT.
 * 1000-LOCK-W-XPIOCHK SECTION.
 * *****************************************************************
 *                                                                 *
 *  CALLS PM TO CHECKOUT THE REQUESTED FOLDER.                     *
 *                                                                 *
 * *****************************************************************
 * * CALCULATE THE LOCK EXPIRATION TIME
 *     MOVE WS-READ-LOCK-TYPE   TO PM-CIOR-FUNCTION.
 *     PERFORM 1300-CALC-LOCK-EXP-TIME.
 *     PERFORM 1200-CALL-XPIOCHK.
 *     EVALUATE TRUE
 *         WHEN CIOR-PARMS-OK
 *              CONTINUE
 *         WHEN CIOR-DATA-CHECKED-OUT
 *              SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
 *              MOVE 'LOCK ALREADY EXISTS FOR USERID :'
 *                TO NLBE-FAILED-TABLE-OR-FILE
 * **           MOVE PM-CIOR-USER-ID
 *              MOVE PM-CIOR-WHO-CHKD-OUT
 *                TO NLBE-FAILED-COLUMN-OR-FIELD
 *                   WS-NONLOG-ERR-COL1-VALUE
 *              MOVE 'HAL_LOKOTH'        TO NLBE-ERROR-CODE
 *              SET UBOC-HALT-AND-RETURN TO TRUE
 *              PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
 *              GO TO 1000-LOCK-W-XPIOCHK-X
 *         WHEN OTHER
 *              SET WS-LOG-ERROR              TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE    TO TRUE
 *              SET EFAL-IAP-FAILED           TO TRUE
 *              SET IAP-XPIOCHK-CALL-ERROR    TO TRUE
 *              MOVE '1000-LOCK-W-XPIOCHK'
 *                TO EFAL-ERR-PARAGRAPH
 *              MOVE 'UNEXPECTED RETURN CODE FROM XPIOCHK'
 *                TO EFAL-ERR-COMMENT
 *              STRING 'PM-CIOR-RET-CODE = '
 *                      PM-CIOR-RET-CODE ';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 1000-LOCK-W-XPIOCHK-X
 *     END-EVALUATE.
 * 1000-LOCK-W-XPIOCHK-X.
 *     EXIT.
 * 1200-CALL-XPIOCHK SECTION.
 * *****************************************************************
 *                                                                 *
 *  CALL XPIOCHK ROUTINE                                           *
 *                                                                 *
 * *****************************************************************
 *     MOVE PM-CIOR-FUNCTION TO WS-SAVE-FUNCTION.
 *     EXEC CICS LINK
 *          PROGRAM   ('XPIOCHK')
 *          COMMAREA  (PMCA-PARAMETER-LIST)
 *          RESP      (WS-RESPONSE-CODE)
 *          RESP2     (WS-RESPONSE-CODE2)
 *     END-EXEC.
 *     MOVE WS-SAVE-FUNCTION TO PM-CIOR-FUNCTION.
 *     EVALUATE WS-RESPONSE-CODE
 *         WHEN DFHRESP(NORMAL)
 *              CONTINUE
 *         WHEN OTHER
 *              SET WS-LOG-ERROR             TO TRUE
 *              SET EFAL-SYSTEM-ERROR        TO TRUE
 *              SET EFAL-CICS-FAILED         TO TRUE
 *              SET ETRA-CICS-LINK           TO TRUE
 *              MOVE 'XPIOCHK'
 *                TO EFAL-ERR-OBJECT-NAME
 *              MOVE '1200-CALL-XPIOCHK'
 *                TO EFAL-ERR-PARAGRAPH
 *              MOVE 'LINK TO XPIOCHK MODULE FAILED'
 *                TO EFAL-ERR-COMMENT
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 1200-CALL-XPIOCHK-X
 *     END-EVALUATE.
 * 1200-CALL-XPIOCHK-X.
 *     EXIT.
 * 1300-CALC-LOCK-EXP-TIME SECTION.
 * *****************************************************************
 *                                                                 *
 *  CALCULATE THE LOCK EXPIRY TIME.                                *
 *                                                                 *
 * *****************************************************************
 *     MOVE UBOC-UOW-LOCK-TIMEOUT-INTERVAL
 *       TO WS-SCH-TM-MTE-NBR.
 *     IF WS-SCH-TM-MTE-NBR GREATER THAN 59
 *         MOVE 59 TO WS-SCH-TM-MTE-NBR.
 *     ADD WS-SCH-TM-MTE-NBR TO WS-S3TIME-MM
 *         GIVING WS-SCH-WORK-MM1.
 *     IF WS-SCH-WORK-MM1 LESS THAN OR EQUAL 59
 *         MOVE WS-SCH-WORK-MM1 TO WS-S3TIME-MM
 *     ELSE
 *         SUBTRACT WS-SCH-TM-MTE-NBR FROM 60
 *             GIVING WS-SCH-WORK-MM2
 *         SUBTRACT WS-SCH-WORK-MM2 FROM WS-S3TIME-MM
 *         MOVE SPACES TO DATE-STRUCTURE
 *         MOVE 'C'    TO DATE-FUNCTION
 *         MOVE 'B'    TO DATE-INP-FORMAT
 *         MOVE 'B'    TO DATE-OUT-FORMAT
 *         MOVE 'HD'   TO DATE-ADJ-FORMAT
 *         MOVE '+'    TO DATE-ADJUST-SIGN
 *         MOVE 1      TO DATE-ADJUST-NUMB
 *         MOVE WS-S3DT-BREAKDOWN TO DATE-INPUT
 *         PERFORM 1400-CALL-XPIODAT
 *         IF   DATE-RETURN-CODE NOT EQUAL 'OK'
 *           OR DATE-OUTPUT EQUAL SPACES
 *             SET WS-LOG-ERROR               TO TRUE
 *             SET EFAL-BUS-LOGIC-FAILURE     TO TRUE
 *             SET EFAL-IAP-FAILED            TO TRUE
 *             SET IAP-DATE-RETURN-ERROR      TO TRUE
 *             MOVE '1300-CALC-LOCK-EXP-TIME' TO EFAL-ERR-PARAGRAPH
 *             MOVE 'PM DATE ROUTINE ERROR'   TO EFAL-ERR-COMMENT
 *             STRING 'DATE-RETURN-CODE = '
 *                     DATE-RETURN-CODE ';'
 *                    'DATE-OUTPUT = '
 *                     DATE-OUTPUT ';'
 *                     DELIMITED BY SIZE
 *                     INTO EFAL-OBJ-DATA-KEY
 *             END-STRING
 *             PERFORM 9000-LOG-WARNING-OR-ERROR
 *             GO TO 1300-CALC-LOCK-EXP-TIME-X
 *         END-IF
 *         MOVE DATE-OUTPUT TO WS-S3DT-BREAKDOWN
 *     END-IF.
 *     MOVE WS-S3DT-BREAKDOWN    TO PM-CIOR-EXPIRATION-DATE.
 * 1300-CALC-LOCK-EXP-TIME-X.
 *     EXIT.
 * 1400-CALL-XPIODAT SECTION.
 * *****************************************************************
 *                                                                 *
 *  CALL XPIODAT DATE ROUTINE                                      *
 *                                                                 *
 * *****************************************************************
 *     COPY XPXCDAT.
 * 1400-CALL-XPIODAT-X.
 *     EXIT.
 * 1740-BCMOCHK-RON-PROC SECTION.
 * *****************************************************************
 *                                                                 *
 *  CARRY OUT LOCKING PROCESS FOR READONLY LOCKS DEPENDING ON      *
 *  THE PASSED FUNCTION                                            *
 *                                                                 *
 * *****************************************************************
 *     PERFORM 1860-DET-BCM-CHK-TYPE.
 *     EVALUATE TRUE
 * * "CREATE_ACCESS_LOCK": CHECK FOR A READUPDT LOCK EXISTING FOR
 * *  THIS LOCK KEY. IF ONE IS FOUND ISSUE A WARNING MESSAGE BUT
 * *  CONTINUE PROCESSING. NO READONLY LOCK IS ACTUALLY WRITTEN.
 *         WHEN HALRLODR-CREATE-ACCESS-LOCK
 *              PERFORM 1800-CHK-BCM-RON-LOCK
 * * "AUTHENTICATE_ACCESS_LOCK": SHOULD NOT OCCUR -
 * * IF IT DOES IT'S INVALID
 *         WHEN OTHER
 *              SET WS-LOG-ERROR                TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
 *              SET EFAL-BUS-PROCESS-FAILED     TO TRUE
 *              SET BUSP-INVALID-FUNCTION       TO TRUE
 *              MOVE '1740-BCMOCHK-RON-PROC'
 *                TO EFAL-ERR-PARAGRAPH
 *              MOVE 'INVALID PASSED FUNCTION'  TO EFAL-ERR-COMMENT
 *              STRING 'HALRLODR-FUNCTION = '
 *                      HALRLODR-FUNCTION ';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 1740-BCMOCHK-RON-PROC-X
 *     END-EVALUATE.
 * 1740-BCMOCHK-RON-PROC-X.
 *     EXIT.
 * 1760-BCMOCHK-RUP-PROC SECTION.
 * *****************************************************************
 *                                                                 *
 *  CARRY OUT LOCKING PROCESS FOR READUPDT LOCKS DEPENDING ON      *
 *  THE PASSED FUNCTION                                            *
 *                                                                 *
 * *****************************************************************
 *     PERFORM 1860-DET-BCM-CHK-TYPE.
 *     EVALUATE TRUE
 * * "AUTHENTICATE_ACCESS_LOCK": CHECKS TO SEE IF A LOCK ALREADY
 * *  EXISTS FOR THIS BUSINESS DATA. RESULTS CAN BE:
 * *  1. LOCK FOUND FOR ANOTHER USER - PRODUCES HAL_LOKOTH N.L.B.E.
 * *  2. NO LOCK FOUND - PRODUCES HAL_LOKNFD NON-LOGGABLE BUS. ERR.
 * *  3. LOCK FOUND FOR THIS USER - OK, NOW UPDATE THE LOCK EXPIRY
 * *     TIMESTAMP WITH THE CURRENT SYSTEM TIME
 * *     LOCK-TIMEOUT-INTERVAL.
 * *  LOCKS FOR BILLING (WITH THE EXCEPTION OF CASH) ARE NOT HELD
 * *  ON THE ARCH_LOK_RUP TABLE.  INSTEAD, THE LOCK INFORMATION IS
 * *  HELD ON THE PRIMARY ROW BEING LOCKED (EG. BIL_ACCOUNT_V FOR
 * *  AN ACCOUNT LOCK, OR BIL_THIRD_PARTY_V FOR A THIRD PARTY LOCK).
 *         WHEN HALRLODR-AUTH-ACCESS-LOCK
 *              PERFORM 1900-CHK-BCM-RUP-LOCK
 * * "CREATE_ACCESS_LOCK": WRITES A READUPDT LOCK FOR THIS BUSINESS
 * *  DATA. RESULTS CAN BE:
 * *  1. LOCK FOUND FOR ANOTHER USER - PRODUCES LOCKERR001 N.L.B.E.
 * *  2. LOCK IS CREATED - OK.
 *         WHEN HALRLODR-CREATE-ACCESS-LOCK
 *              PERFORM 2000-LOCK-W-BCMOCHK
 *         WHEN OTHER
 *              SET WS-LOG-ERROR                TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
 *              SET EFAL-BUS-PROCESS-FAILED     TO TRUE
 *              SET BUSP-INVALID-FUNCTION       TO TRUE
 *              MOVE '1760-BCMOCHK-RUP-PROC'
 *                TO EFAL-ERR-PARAGRAPH
 *              MOVE 'INVALID LOCK ACTION CODE' TO EFAL-ERR-COMMENT
 *              STRING 'HALRLODR-FUNCTION = '
 *                      HALRLODR-FUNCTION ';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 1760-BCMOCHK-RUP-PROC-X
 *     END-EVALUATE.
 * 1760-BCMOCHK-RUP-PROC-X.
 *     EXIT.
 * 1800-CHK-BCM-RON-LOCK SECTION.
 * *****************************************************************
 *                                                                 *
 *  CIOR-FOLDER-LOCK OPTION IS USED TO DETERMINE IF SOMEONE ELSE   *
 *  HAS THE REQUESTED FOLDER CHECKED OUT FOR UPDATE. IF SO, A      *
 *  WARNING WILL BE SET WITH THE CLIENT ID OF THE USER             *
 *  THAT HOLDS THE LOCK.                                           *
 *                                                                 *
 * *****************************************************************
 *     MOVE 'READONLY' TO BCMLCHK-FUNCTION-NM.
 *     PERFORM 2200-CALL-BCMOCHK.
 *     EVALUATE TRUE
 *         WHEN CIOR-DATA-CHECKED-OUT
 * * LOCK EXISTS - ISSUE A WARNING
 *              PERFORM 1850-ISSUE-BCM-LOCK-WNG
 *         WHEN CIOR-DATA-NOT-FOUND
 * * LOCK DOES NOT EXIST - OK
 *              GO TO 1800-CHK-BCM-RON-LOCK-X
 *         WHEN OTHER
 *              SET WS-LOG-ERROR                TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
 *              SET EFAL-LOCKING-FAILED         TO TRUE
 *              SET LOK-BCMOCHK-CALL-ERROR      TO TRUE
 *              MOVE '1800-CHK-BCM-RON-LOCK'
 *                TO EFAL-ERR-PARAGRAPH
 *              MOVE 'UNEXPECTED RETURN CODE FROM BCMOCHK'
 *                TO EFAL-ERR-COMMENT
 *              STRING 'BCMLER00-LINKAGE-BCMLER00= '
 *                      BCMLER00-LINKAGE-BCMLER00';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 1800-CHK-BCM-RON-LOCK-X
 *     END-EVALUATE.
 * 1800-CHK-BCM-RON-LOCK-X.
 *     EXIT.
 * 1900-CHK-BCM-RUP-LOCK SECTION.
 * *****************************************************************
 *                                                                 *
 *  CIOR-FOLDER-LOCK OPTION IS USED TO DETERMINE IF SOMEONE ELSE   *
 *  HAS THE REQUESTED FOLDER CHECKED OUT. IF SO, A NON-LOGGABLE    *
 *  BUSINESS ERROR WILL BE SET WITH THE CLIENT ID OF THE USER      *
 *  THAT HOLDS THE LOCK. A NON-LOGGABLE BUSINESS ERROR WILL ALSO   *
 *  BE SET IF HBLT-LOK-TYPE-CD    = 'P' AND NO S3 LOCK IS FOUND.   *
 *                                                                 *
 * *****************************************************************
 * *** SET CIOR-QUERY-READ-UPDATE            TO TRUE.
 *     MOVE 'READUPDT' TO BCMLCHK-FUNCTION-NM.
 *     PERFORM 2200-CALL-BCMOCHK.
 *     EVALUATE TRUE
 *         WHEN CIOR-PARMS-OK
 * * LOCK EXISTS - CHECK WHO OWNS THE LOCK
 *              PERFORM 0950-CHECK-LOCK-USER
 *         WHEN CIOR-DATA-NOT-FOUND
 *           IF HBLT-LOK-TYPE-CD    = 'P'
 * * LOCK DOES NOT EXIST - ERROR IF LOKTYPE CODE IS PESSIMISTIC
 *              SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
 *              MOVE 'LOCKING: LOCK NOT FOUND'
 *                TO NLBE-FAILED-TABLE-OR-FILE
 *              MOVE SPACES TO NLBE-FAILED-COLUMN-OR-FIELD
 *              MOVE 'HAL_LOKNFD'        TO NLBE-ERROR-CODE
 *              SET UBOC-HALT-AND-RETURN TO TRUE
 *              PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
 *              GO TO 1900-CHK-BCM-RUP-LOCK-X
 *           ELSE
 *              GO TO 1900-CHK-BCM-RUP-LOCK-X
 *           END-IF
 *         WHEN OTHER
 *              SET WS-LOG-ERROR              TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE    TO TRUE
 *              SET EFAL-IAP-FAILED           TO TRUE
 *              SET IAP-XPIOCHK-CALL-ERROR    TO TRUE
 *              MOVE '1900-CHK-BCM-RUP-LOCK'
 *                   TO EFAL-ERR-PARAGRAPH
 *              MOVE 'UNEXPECTED RETURN CODE FROM BCMOCHK'
 *                   TO EFAL-ERR-COMMENT
 *              STRING 'PM-CIOR-RET-CODE = '
 *                      PM-CIOR-RET-CODE ';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 1900-CHK-BCM-RUP-LOCK-X
 *     END-EVALUATE.
 * 1900-CHK-BCM-RUP-LOCK-X.
 *     EXIT.
 * 1850-ISSUE-BCM-LOCK-WNG SECTION.
 * *****************************************************************
 *                                                                 *
 *  A LOCK EXISTS - WRITE A WARNING THAT THE FOLDER IS CHECKED     *
 *  OUT FOR UPDATE.                                                *
 *                                                                 *
 * *****************************************************************
 *     SET WS-LOG-WARNING            TO TRUE
 *     MOVE '1850-ISSUE-BCM-LOCK-WNG' TO EFAL-ERR-PARAGRAPH.
 *     STRING 'WARNING - LOCKED FOR UPDATE BY USERID: '
 *             PM-CIOR-USER-ID
 *             DELIMITED BY SIZE
 *             INTO EFAL-ERR-COMMENT
 *     END-STRING.
 *     PERFORM 9000-LOG-WARNING-OR-ERROR.
 * 1850-ISSUE-BCM-LOCK-WNG-X.
 *     EXIT.
 * 1860-DET-BCM-CHK-TYPE SECTION.
 *     EVALUATE HALRLODR-APP-ID
 *        WHEN 'BCWSACCT'
 *           MOVE 'ALK' TO BCMLCHK-CHK-TYPE
 *        WHEN 'BCWSTTY'
 *           MOVE 'TLK' TO BCMLCHK-CHK-TYPE
 *        WHEN 'BCWSAGT'
 *           MOVE 'GLK' TO BCMLCHK-CHK-TYPE
 *     END-EVALUATE.
 * 1860-DET-BCM-CHK-TYPE-X.
 *     EXIT.
 * 1950-CHECK-LOCK-USER SECTION.
 * *****************************************************************
 *                                                                 *
 *                                                                 *
 * *****************************************************************
 *     IF BCMLCHK-USER-ID      EQUAL UBOC-AUTH-USERID
 *         CONTINUE
 *     ELSE
 *         SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
 *         MOVE 'LOCK ALREADY EXISTS FOR USERID :'
 *           TO NLBE-FAILED-TABLE-OR-FILE
 *         MOVE PM-CIOR-USER-ID
 *           TO NLBE-FAILED-COLUMN-OR-FIELD
 *              WS-NONLOG-ERR-COL1-VALUE
 *         MOVE 'HAL_LOKOTH'        TO NLBE-ERROR-CODE
 *         SET UBOC-HALT-AND-RETURN TO TRUE
 *         PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
 *         GO TO 1950-CHECK-LOCK-USER-X
 *     END-IF.
 * 1950-CHECK-LOCK-USER-X.
 *     EXIT.
 * 2000-LOCK-W-BCMOCHK SECTION.
 * *****************************************************************
 *                                                                 *
 *  CALLS PM TO CHECKOUT THE REQUESTED FOLDER.                     *
 *                                                                 *
 * *****************************************************************
 * * CALCULATE THE LOCK EXPIRATION TIME
 *     MOVE WS-READ-LOCK-TYPE   TO PM-CIOR-FUNCTION.
 *     MOVE PM-CIOR-FUNCTION    TO BCMLCHK-FUNCTION-NM.
 *     PERFORM 2200-CALL-BCMOCHK.
 *     EVALUATE TRUE
 *         WHEN CIOR-PARMS-OK
 *              CONTINUE
 *         WHEN CIOR-DATA-CHECKED-OUT
 *              SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
 *              MOVE 'LOCK ALREADY EXISTS FOR USERID :'
 *                TO NLBE-FAILED-TABLE-OR-FILE
 *              MOVE PM-CIOR-USER-ID
 *                TO NLBE-FAILED-COLUMN-OR-FIELD
 *                   WS-NONLOG-ERR-COL1-VALUE
 *              MOVE 'HAL_LOKOTH'        TO NLBE-ERROR-CODE
 *              SET UBOC-HALT-AND-RETURN TO TRUE
 *              PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
 *              GO TO 2000-LOCK-W-BCMOCHK-X
 *         WHEN OTHER
 *              SET WS-LOG-ERROR              TO TRUE
 *              SET EFAL-BUS-LOGIC-FAILURE    TO TRUE
 *              SET EFAL-IAP-FAILED           TO TRUE
 *              SET LOK-BCMOCHK-CALL-ERROR    TO TRUE
 *              MOVE '2000-LOCK-W-BCMOCHK'
 *                TO EFAL-ERR-PARAGRAPH
 *              MOVE 'UNEXPECTED RETURN CODE FROM BCMOCHK'
 *                TO EFAL-ERR-COMMENT
 *              STRING 'PM-CIOR-RET-CODE = '
 *                      PM-CIOR-RET-CODE ';'
 *                      DELIMITED BY SIZE
 *                      INTO EFAL-OBJ-DATA-KEY
 *              END-STRING
 *              PERFORM 9000-LOG-WARNING-OR-ERROR
 *              GO TO 2000-LOCK-W-BCMOCHK-X
 *     END-EVALUATE.
 * 2000-LOCK-W-BCMOCHK-X.
 *     EXIT.
 * 2200-CALL-BCMOCHK SECTION.
 *     INITIALIZE BCMLER00-LINKAGE-BCMLER00.
 *     COPY BCMCCHK.
 *     IF EIBRESP NOT EQUAL ZERO
 *          SET WS-LOG-ERROR             TO TRUE
 *          SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
 *          SET EFAL-CICS-FAILED OF WS-ESTO-INFO  TO TRUE
 *          SET ETRA-CICS-LINK OF WS-ESTO-INFO    TO TRUE
 *          MOVE 'BCMOCHK'
 *               TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
 *          MOVE '2200-CALL-BCMOCHK'
 *               TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
 *          MOVE 'LINK TO BCMOCHK FAILED'
 *               TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
 *         PERFORM 9000-LOG-WARNING-OR-ERROR
 *         GO TO 2200-CALL-BCMOCHK-X
 *     END-IF.
 *     IF BCMLCHK-RETURN-CODE NOT = SPACES
 *        MOVE BCMLCHK-RETURN-CODE TO PM-CIOR-RET-CODE
 *     ELSE
 *        SET CIOR-PARMS-OK TO TRUE.
 *     IF BCMLCHK-ERROR-YES
 *        IF BCMLCHK-SQL-CD = ZERO
 *           IF BCMLCHK-USER-ID NOT = PM-CIOR-USER-ID
 *           OR BCMLCHK-ISSUE-NODE-ID NOT = PMCA-ISSUE-NODE
 *              MOVE BCMLCHK-CLIENT-ID     TO PM-USR-CLIENT-ID
 *              MOVE BCMLCHK-FIRST-NAME    TO PM-USR-FIRST-NAME
 *              MOVE BCMLCHK-LAST-NAME     TO PM-USR-LAST-NAME
 *              MOVE BCMLCHK-ISSUE-NODE-ID TO PM-CIOR-WHO-CHKD-OUT
 *              MOVE BCMLCHK-USER-ID       TO PM-CIOR-WHEN-CHKD-OUT
 *              IF CIOR-FORCE-RELEASE
 *              OR CIOR-RELEASE-READ-UPDATE
 *                 SET CIOR-PARMS-OK TO TRUE
 *              ELSE
 *              IF CIOR-UPDATE
 *                 SET CIOR-NOT-READ-FOR-UPDATE TO TRUE
 *              ELSE
 *                 SET CIOR-DATA-CHECKED-OUT TO TRUE
 *           ELSE
 *              NEXT SENTENCE
 *        ELSE
 *        IF BCMLCHK-SQL-CD = +100
 *           SET CIOR-DATA-NOT-FOUND TO TRUE
 *        ELSE
 *           SET CIOR-SQL-ERROR-RETURN TO TRUE
 *           MOVE BCMLCHK-SQL-CD TO PM-CIOR-REF-ID.
 * 2200-CALL-BCMOCHK-X.
 *     EXIT.</pre>*/
public class Halrlodr extends BatchProgram {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>**** HALLJWSA BELOW HAS BEEN REPLACED WITH HALLJWSB *******
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NO IAP REFERENCES)                *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 OCT. 16, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *  CASE 17241  PRGMR AICI448          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private HalUowTransactVDao halUowTransactVDao = new HalUowTransactVDao(dbAccessStatus);
	private HalBoLokExcVDao halBoLokExcVDao = new HalBoLokExcVDao(dbAccessStatus);
	private HalUniversalCtVDao halUniversalCtVDao = new HalUniversalCtVDao(dbAccessStatus);
	private HalBoMduXrfVDao halBoMduXrfVDao = new HalBoMduXrfVDao(dbAccessStatus);
	private HalBoLokTypeVDao halBoLokTypeVDao = new HalBoLokTypeVDao(dbAccessStatus);
	private HalSecProfileVDao halSecProfileVDao = new HalSecProfileVDao(dbAccessStatus);
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private HalrlodrData ws = new HalrlodrData();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DefaultComm dfhcommarea;
	//Original name: WS-HALLUBOC-COMMS-AREA
	private WsHalrlomgLinkage1 wsHallubocCommsArea;
	//Original name: WS-HALRLODR-LINKAGE
	private WsHalrlodrLinkage wsHalrlodrLinkage;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DefaultComm dfhcommarea, WsHalrlomgLinkage1 wsHallubocCommsArea,
			WsHalrlodrLinkage wsHalrlodrLinkage) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		this.wsHallubocCommsArea = wsHallubocCommsArea;
		this.wsHalrlodrLinkage = wsHalrlodrLinkage;
		main1();
		mainX();
		return 0;
	}

	public static Halrlodr getInstance() {
		return (Programs.getInstance(Halrlodr.class));
	}

	/**Original name: 0000-MAIN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   THE 0000-MAIN PARAGRAPH IS RESPONSIBLE FOR CONTROLLING THE    *
	 *   PROCESSING OF THIS ROUTINE.                                   *
	 *                                                                 *
	 * *****************************************************************
	 * **  INITIALIZE WS-HALRLOMG-LINKAGE.</pre>*/
	private void main1() {
		// COB_CODE: PERFORM 0100-VALIDATE-INPUT.
		validateInput();
		// COB_CODE: PERFORM 0110-INITIALIZE.
		initialize();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-MAIN-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-MAIN-X
			mainX();
		}
		// COB_CODE: IF UBOC-UOW-NO-LOCK-NEEDED
		//            OR UBOC-HALT-AND-RETURN
		//               GO TO 0000-MAIN-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowLockStrategyCd().isNoLockAction()
				|| wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-MAIN-X
			mainX();
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN HALRLODR-CREATE-ACCESS-LOCK
		//               WHEN HALRLODR-CREATE-INSRET-LOCK
		//               WHEN HALRLODR-VERIFY-INSERT-ACCESS
		//                    PERFORM 0200-CREATE-ACCESS-LOCK
		//               WHEN HALRLODR-AUTH-ACCESS-LOCK
		//                    PERFORM 0500-AUTH-ACCESS-LOCK
		//               WHEN OTHER
		//                    GO TO 0000-MAIN-X
		//           END-EVALUATE.
		switch (wsHalrlodrLinkage.getFunction().getFunction()) {

		case HalrlodrFunction.CREATE_ACCESS_LOCK:
		case HalrlodrFunction.CREATE_INSRET_LOCK:
		case HalrlodrFunction.VERIFY_INSERT_ACCESS:// COB_CODE: PERFORM 0200-CREATE-ACCESS-LOCK
			createAccessLock();
			break;

		case HalrlodrFunction.AUTH_ACCESS_LOCK:// COB_CODE: PERFORM 0500-AUTH-ACCESS-LOCK
			authAccessLock();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INVALID-FUNCTION   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidFunction();
			// COB_CODE: MOVE 'HALRLODR'             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALRLODR");
			// COB_CODE: MOVE '0000-MAIN'            TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0000-MAIN");
			// COB_CODE: MOVE 'INVALID PASSED FUNCTION'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID PASSED FUNCTION");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0000-MAIN-X
			mainX();
			break;
		}
	}

	/**Original name: 0000-MAIN-X<br>*/
	private void mainX() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 0100-VALIDATE-INPUT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE INPUT PARAMETER(S)                                    *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void validateInput() {
		// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
		// COB_CODE: IF UBOC-UOW-LOCK-STRATEGY-CD EQUAL SPACES OR LOW-VALUES
		//               GO TO 0100-VALIDATE-INPUT-X
		//           END-IF.
		if (Conditions.eq(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowLockStrategyCd().getUbocUowLockStrategyCd(), Types.SPACE_CHAR)
				|| Conditions.eq(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowLockStrategyCd().getUbocUowLockStrategyCd(),
						Types.LOW_CHAR_VAL)) {
			// COB_CODE: SET WS-LOG-ERROR                TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET COMA-LOCK-TYPE-BLANK        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaLockTypeBlank();
			// COB_CODE: MOVE 'HALRLODR'                 TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALRLODR");
			// COB_CODE: MOVE '0100-VALIDATE-INPUT'      TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-VALIDATE-INPUT");
			// COB_CODE: MOVE 'UBOC-UOW-LOCK-STRATEGY-CD WAS EMPTY'
			//            TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-LOCK-STRATEGY-CD WAS EMPTY");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-VALIDATE-INPUT-X
			return;
		}
		// COB_CODE: IF UBOC-UOW-NAME EQUAL SPACES OR LOW-VALUES
		//               GO TO 0100-VALIDATE-INPUT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET WS-LOG-ERROR                TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET COMA-UOW-NAME-BLANK         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'HALRLODR'                 TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALRLODR");
			// COB_CODE: MOVE '0100-VALIDATE-INPUT'      TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-VALIDATE-INPUT");
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//            TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-VALIDATE-INPUT-X
			return;
		}
		// COB_CODE: IF HALRLODR-INPUT-LINKAGE EQUAL SPACES OR LOW-VALUES
		//               GO TO 0100-VALIDATE-INPUT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(wsHalrlodrLinkage.getInputLinkageBytes()) || Characters.EQ_LOW.test(wsHalrlodrLinkage.getInputLinkageBytes())) {
			// COB_CODE: SET WS-LOG-ERROR                TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQD-DATA-NOT-FOUND    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspReqdDataNotFound();
			// COB_CODE: MOVE 'HALRLODR'                 TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALRLODR");
			// COB_CODE: MOVE '0100-VALIDATE-INPUT'      TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-VALIDATE-INPUT");
			// COB_CODE: MOVE 'INPUT DATA INVALID'       TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INPUT DATA INVALID");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-VALIDATE-INPUT-X
			return;
		}
	}

	/**Original name: 0110-INITIALIZE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  INITIALIZE WORKING STORAGE                                     *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void initialize() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE WS-HALRLOMG-LINKAGE.
		initWsHalrlomgLinkage();
		// COB_CODE: SET RETRIEVE-LEGACY-LOCK-MODULE TO TRUE.
		ws.getWsSwitches().getLegacyLockModSw().setRetrieveLegacyLockModule();
		// SET EXPIRATION TIMESTAMP
		// COB_CODE: MOVE UBOC-UOW-NAME TO HUTC-UOW-NM.
		ws.getDclhalUowTransact().setUowNm(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: EXEC SQL
		//             SELECT CURRENT TIMESTAMP + HUTC_LOK_TMO_ITV MINUTES
		//              INTO :WS-EXPIRY-TS
		//             FROM HAL_UOW_TRANSACT_V
		//              WHERE UOW_NM = :HUTC-UOW-NM
		//           END-EXEC.
		ws.getWsWorkFields()
				.setExpiryTs(halUowTransactVDao.selectByHutcUowNm(ws.getDclhalUowTransact().getUowNm(), ws.getWsWorkFields().getExpiryTs()));
		// COB_CODE: IF SQLCODE NOT = ZERO
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (sqlca.getSqlcode() != 0) {
			// COB_CODE: SET WS-LOG-ERROR            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SET-CURRENT-TS TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2SetCurrentTs();
			// COB_CODE: MOVE 'CURRENT TIMESTAMP'    TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("CURRENT TIMESTAMP");
			// COB_CODE: MOVE '0110-INITIALIZE'      TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0110-INITIALIZE");
			// COB_CODE: MOVE 'DERIVING EXPIRY TS FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DERIVING EXPIRY TS FAILED");
			// COB_CODE: STRING 'TIMEOUT-INTERVAL-MINUTES='
			//                   UBOC-UOW-LOCK-TIMEOUT-INTERVAL ';'
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "TIMEOUT-INTERVAL-MINUTES=",
					wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowLockTimeoutIntervalAsString(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 0200-CREATE-ACCESS-LOCK_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PROCESSING FOR FUNCTION 'CREATE ACCESS LOCK'                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void createAccessLock() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 0300-LOOKUP-LOCK-EXCPT.
		lookupLockExcpt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0200-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: MOVE SPACES                   TO HALRLOMG-LOK-TYPE-CD.
		ws.getWsHalrlomgLinkage().getLokTypeCd().setLokTypeCd(Types.SPACE_CHAR);
		// COB_CODE: PERFORM 0900-DETERMINE-LOCK-TYPE.
		determineLockType();
		// COB_CODE: IF UBOC-HALT-AND-RETURN OR HALRLOMG-NO-LOCK-NEEDED
		//               GO TO 0200-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsHalrlomgLinkage().getLokTypeCd().isHalrlomgNoLockNeeded()) {
			// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		}
		//* IF THE LOCK TYPE IN UBOC IS 'GROUP', SEE IF THE USER IS IN A
		//* LOCK GROUP.  IF HE ISN'T, CHANGE THE LOCK TYPE IN UBOC TO
		//* PESSIMISTIC FOR THE NON-GROUP USER.
		// COB_CODE: PERFORM 0910-DETERMINE-LOCK-GROUP.
		determineLockGroup();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0200-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		}
		//17238      IF HALRLODR-CREATE-ACCESS-LOCK
		//               SET HALRLOMG-CHECK-FETCH-LOCK TO TRUE
		//17238      ELSE
		//17238          SET HALRLOMG-CHECK-INSRET-LOCK TO TRUE
		//17238      END-IF.
		// COB_CODE: EVALUATE TRUE
		//               WHEN HALRLODR-CREATE-ACCESS-LOCK
		//                   SET HALRLOMG-CHECK-FETCH-LOCK TO TRUE
		//               WHEN HALRLODR-VERIFY-INSERT-ACCESS
		//                   SET HALRLOMG-VERIFY-INSERT-ACCESS TO TRUE
		//               WHEN OTHER
		//                   SET HALRLOMG-CHECK-INSRET-LOCK TO TRUE
		//           END-EVALUATE.
		switch (wsHalrlodrLinkage.getFunction().getFunction()) {

		case HalrlodrFunction.CREATE_ACCESS_LOCK:// COB_CODE: SET HALRLOMG-CHECK-FETCH-LOCK TO TRUE
			ws.getWsHalrlomgLinkage().getFunction().setHalrlomgCheckFetchLock();
			break;

		case HalrlodrFunction.VERIFY_INSERT_ACCESS:// COB_CODE: SET HALRLOMG-VERIFY-INSERT-ACCESS TO TRUE
			ws.getWsHalrlomgLinkage().getFunction().setHalrlomgVerifyInsertAccess();
			break;

		default:// COB_CODE: SET HALRLOMG-CHECK-INSRET-LOCK TO TRUE
			ws.getWsHalrlomgLinkage().getFunction().setHalrlomgCheckInsretLock();
			break;
		}
		// COB_CODE: PERFORM 0600-CALL-LOCK-MANAGER.
		callLockManager();
		//    IF HALRLOMG-FOUND
		//     OR UBOC-HALT-AND-RETURN
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0200-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: IF HALRLOMG-ZAPPED
		//              GO TO 0200-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getSuccessInd().isZapped()) {
			// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			// COB_CODE: MOVE HALRLODR-TABLE-NM
			//             TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(wsHalrlodrLinkage.getTableNm());
			// COB_CODE: MOVE SPACES TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("");
			// COB_CODE: MOVE HALRLOMG-ZAPPED-BY-USERID
			//             TO WS-NONLOG-ERR-COL1-VALUE
			ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(ws.getWsHalrlomgLinkage().getZappedByUserid());
			// COB_CODE: MOVE 'HAL_LOKZAP'        TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("HAL_LOKZAP");
			// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		}
		//* CHECK LEGACY LOCK IF REQUIRED
		//*       WHEN UBOC-UOW-LOCK-OPTIMISTIC
		// COB_CODE:      EVALUATE TRUE
		//           **       WHEN UBOC-UOW-LOCK-OPTIMISTIC
		//                    WHEN HALRLOMG-LOCK-OPTIMISTIC
		//                         PERFORM 0700-LOCK-PROCESSING
		//           **            SET HALRLOMG-LOCK-OPTIMISTIC
		//           **             TO TRUE
		//           **       WHEN UBOC-UOW-NO-LOCK-NEEDED
		//                    WHEN HALRLOMG-NO-LOCK-NEEDED
		//                         GO TO 0200-CREATE-ACCESS-LOCK-X
		//           **       WHEN UBOC-UOW-LOCK-PESSIMISTIC
		//                    WHEN HALRLOMG-LOCK-PES-READUPDT
		//                    WHEN HALRLOMG-LOCK-GROUP-PESSI
		//           ***           SET WS-READ-UPDATE-LOCK TO TRUE
		//                         PERFORM 0700-LOCK-PROCESSING
		//           **            SET HALRLOMG-LOCK-PES-READUPDT
		//           **             TO TRUE
		//           **       WHEN UBOC-UOW-LOCK-MIXED
		//           **            PERFORM 0400-LOOKUP-LOCK-TYPE
		//           **            IF UBOC-HALT-AND-RETURN
		//           **                GO TO 0200-CREATE-ACCESS-LOCK-X
		//           **            END-IF
		//           ** CHECK LOCK TYPE CODE RETRIEVED FROM THE TABLE
		//           **            EVALUATE TRUE
		//           **                WHEN HBLT-LOK-TYPE-CD    = 'P'
		//           ****                   SET WS-READ-UPDATE-LOCK TO TRUE
		//           **17238               SET ULLI-CREATE-UPDATE-LOCK TO TRUE
		//           **                    PERFORM 0700-LOCK-PROCESSING
		//           **                    SET HALRLOMG-LOCK-PES-READUPDT
		//           **                      TO TRUE
		//           **
		//           ****              WHEN HBLT-LOK-TYPE-CD    = 'R'
		//           ****                   SET WS-READ-ONLY-LOCK TO TRUE
		//           ****                   PERFORM 0700-LOCK-PROCESSING
		//           ****                   SET HALRLOMG-LOCK-PES-READONLY
		//           ****                    TO TRUE
		//           **
		//           **                WHEN HBLT-LOK-TYPE-CD    = 'O'
		//           ** 17238              SET ULLI-VERIFY-UPDATE-LOCK TO TRUE
		//           ** 17238              PERFORM 0700-LOCK-PROCESSING
		//           **                    SET HALRLOMG-LOCK-OPTIMISTIC
		//           **                      TO TRUE
		//           **
		//           **                WHEN HBLT-LOK-TYPE-CD    = 'N'
		//           **                     GO TO 0200-CREATE-ACCESS-LOCK-X
		//           **
		//           **                WHEN OTHER
		//           **                     SET WS-LOG-ERROR            TO TRUE
		//           **                     SET EFAL-BUS-LOGIC-FAILURE  TO TRUE
		//           **                     SET EFAL-BUS-PROCESS-FAILED TO TRUE
		//           **                     SET BUSP-INV-ACTION-CODE    TO TRUE
		//           **                     MOVE 'HALRLODR'
		//           **                       TO EFAL-ERR-OBJECT-NAME
		//           **                     MOVE '0200-CREATE-ACCESS-LOCK'
		//           **                       TO EFAL-ERR-PARAGRAPH
		//           **                     MOVE 'INVALID TABLE LOCK TYPE'
		//           **                       TO EFAL-ERR-COMMENT
		//           **                     STRING 'HBLT-LOK-TYPE-CD='
		//           **                             HBLT-LOK-TYPE-CD    ';'
		//           **                             DELIMITED BY SIZE
		//           **                             INTO EFAL-OBJ-DATA-KEY
		//           **                     END-STRING
		//           **                     PERFORM 9000-LOG-WARNING-OR-ERROR
		//           **                     GO TO 0200-CREATE-ACCESS-LOCK-X
		//           **            END-EVALUATE
		//                    WHEN OTHER
		//                         GO TO 0200-CREATE-ACCESS-LOCK-X
		//                END-EVALUATE.
		switch (ws.getWsHalrlomgLinkage().getLokTypeCd().getLokTypeCd()) {

		case HalrlomgLokTypeCd.LOCK_OPTIMISTIC:// COB_CODE: SET ULLI-VERIFY-UPDATE-LOCK TO TRUE
			ws.getHallulli().getLegacyLockFunc().setVerifyUpdateLock();
			// COB_CODE: PERFORM 0700-LOCK-PROCESSING
			lockProcessing();
			//*            SET HALRLOMG-LOCK-OPTIMISTIC
			//*             TO TRUE
			//*       WHEN UBOC-UOW-NO-LOCK-NEEDED
			break;

		case HalrlomgLokTypeCd.NO_LOCK_NEEDED:// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		//*       WHEN UBOC-UOW-LOCK-PESSIMISTIC

		case HalrlomgLokTypeCd.LOCK_PES_READUPDT:
		case HalrlomgLokTypeCd.LOCK_GROUP_PESSI://**           SET WS-READ-UPDATE-LOCK TO TRUE
			// COB_CODE: IF HALRLODR-VERIFY-INSERT-ACCESS
			//               SET ULLI-VERIFY-UPDATE-LOCK TO TRUE
			//           ELSE
			//               SET ULLI-CREATE-UPDATE-LOCK TO TRUE
			//           END-IF
			if (wsHalrlodrLinkage.getFunction().isVerifyInsertAccess()) {
				// COB_CODE: SET ULLI-VERIFY-UPDATE-LOCK TO TRUE
				ws.getHallulli().getLegacyLockFunc().setVerifyUpdateLock();
			} else {
				// COB_CODE: SET ULLI-CREATE-UPDATE-LOCK TO TRUE
				ws.getHallulli().getLegacyLockFunc().setCreateUpdateLock();
			}
			// COB_CODE: PERFORM 0700-LOCK-PROCESSING
			lockProcessing();
			//*            SET HALRLOMG-LOCK-PES-READUPDT
			//*             TO TRUE
			//*       WHEN UBOC-UOW-LOCK-MIXED
			//*            PERFORM 0400-LOOKUP-LOCK-TYPE
			//*            IF UBOC-HALT-AND-RETURN
			//*                GO TO 0200-CREATE-ACCESS-LOCK-X
			//*            END-IF
			//* CHECK LOCK TYPE CODE RETRIEVED FROM THE TABLE
			//*            EVALUATE TRUE
			//*                WHEN HBLT-LOK-TYPE-CD    = 'P'
			//***                   SET WS-READ-UPDATE-LOCK TO TRUE
			//*17238               SET ULLI-CREATE-UPDATE-LOCK TO TRUE
			//*                    PERFORM 0700-LOCK-PROCESSING
			//*                    SET HALRLOMG-LOCK-PES-READUPDT
			//*                      TO TRUE
			//*
			//***              WHEN HBLT-LOK-TYPE-CD    = 'R'
			//***                   SET WS-READ-ONLY-LOCK TO TRUE
			//***                   PERFORM 0700-LOCK-PROCESSING
			//***                   SET HALRLOMG-LOCK-PES-READONLY
			//***                    TO TRUE
			//*
			//*                WHEN HBLT-LOK-TYPE-CD    = 'O'
			//* 17238              SET ULLI-VERIFY-UPDATE-LOCK TO TRUE
			//* 17238              PERFORM 0700-LOCK-PROCESSING
			//*                    SET HALRLOMG-LOCK-OPTIMISTIC
			//*                      TO TRUE
			//*
			//*                WHEN HBLT-LOK-TYPE-CD    = 'N'
			//*                     GO TO 0200-CREATE-ACCESS-LOCK-X
			//*
			//*                WHEN OTHER
			//*                     SET WS-LOG-ERROR            TO TRUE
			//*                     SET EFAL-BUS-LOGIC-FAILURE  TO TRUE
			//*                     SET EFAL-BUS-PROCESS-FAILED TO TRUE
			//*                     SET BUSP-INV-ACTION-CODE    TO TRUE
			//*                     MOVE 'HALRLODR'
			//*                       TO EFAL-ERR-OBJECT-NAME
			//*                     MOVE '0200-CREATE-ACCESS-LOCK'
			//*                       TO EFAL-ERR-PARAGRAPH
			//*                     MOVE 'INVALID TABLE LOCK TYPE'
			//*                       TO EFAL-ERR-COMMENT
			//*                     STRING 'HBLT-LOK-TYPE-CD='
			//*                             HBLT-LOK-TYPE-CD    ';'
			//*                             DELIMITED BY SIZE
			//*                             INTO EFAL-OBJ-DATA-KEY
			//*                     END-STRING
			//*                     PERFORM 9000-LOG-WARNING-OR-ERROR
			//*                     GO TO 0200-CREATE-ACCESS-LOCK-X
			//*            END-EVALUATE
			break;

		default:// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-ACTION-CODE     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvActionCode();
			// COB_CODE: MOVE 'HALRLODR'
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALRLODR");
			// COB_CODE: MOVE '0200-CREATE-ACCESS-LOCK' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0200-CREATE-ACCESS-LOCK");
			//*            MOVE 'INVALID STRATEGY CODE' TO EFAL-ERR-COMMENT
			//*            STRING 'UBOC-UOW-LOCK-STRATEGY-CD='
			//*                    UBOC-UOW-LOCK-STRATEGY-CD ';'
			// COB_CODE: MOVE 'INVALID LOCK TYPE' TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID LOCK TYPE");
			// COB_CODE: STRING 'HALRLOMG-LOK-TYPE-CD='
			//                   HALRLOMG-LOK-TYPE-CD ';'
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY
			//                   END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HALRLOMG-LOK-TYPE-CD=",
					String.valueOf(ws.getWsHalrlomgLinkage().getLokTypeCd().getLokTypeCd()), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0200-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: IF HALRLOMG-LOCK-PES-READUPDT
		//             OR HALRLOMG-LOCK-GROUP-PESSI
		//               END-IF
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt() || ws.getWsHalrlomgLinkage().getLokTypeCd().isLockGroupPessi()) {
			// COB_CODE: IF HALRLODR-VERIFY-INSERT-ACCESS
			//               PERFORM 0215-CHECK-VERIFY-LOCK-RET
			//           ELSE
			//               PERFORM 0210-CHECK-CREATE-LOCK-RET
			//           END-IF
			if (wsHalrlodrLinkage.getFunction().isVerifyInsertAccess()) {
				// COB_CODE: PERFORM 0215-CHECK-VERIFY-LOCK-RET
				checkVerifyLockRet();
			} else {
				// COB_CODE: PERFORM 0210-CHECK-CREATE-LOCK-RET
				checkCreateLockRet();
			}
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0200-CREATE-ACCESS-LOCK-X
			//           END-IF
			if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
				return;
			}
		}
		// COB_CODE: IF HALRLOMG-LOCK-OPTIMISTIC AND LEGACY-LOCK-MODULE-FOUND
		//               END-IF
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getLokTypeCd().isHalrlomgLockOptimistic()
				&& ws.getWsSwitches().getLegacyLockModSw().isLegacyLockModuleFound()) {
			// COB_CODE: PERFORM 0215-CHECK-VERIFY-LOCK-RET
			checkVerifyLockRet();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0200-CREATE-ACCESS-LOCK-X
			//           END-IF
			if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
				return;
			}
		}
		// COB_CODE: IF HALRLODR-VERIFY-INSERT-ACCESS
		//             OR HALRLOMG-FOUND
		//               GO TO 0200-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (wsHalrlodrLinkage.getFunction().isVerifyInsertAccess() || ws.getWsHalrlomgLinkage().getSuccessInd().isFound()) {
			// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: IF HALRLOMG-TYPE-MISMATCH
		//               SET HALRLOMG-UPGRADE-LOCK-ENTRY TO TRUE
		//           ELSE
		//               SET HALRLOMG-ADD-LOCK-ENTRY TO TRUE
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getSuccessInd().isTypeMismatch()) {
			// COB_CODE: SET HALRLOMG-UPGRADE-LOCK-ENTRY TO TRUE
			ws.getWsHalrlomgLinkage().getFunction().setHalrlomgUpgradeLockEntry();
		} else {
			// COB_CODE: SET HALRLOMG-ADD-LOCK-ENTRY TO TRUE
			ws.getWsHalrlomgLinkage().getFunction().setHalrlomgAddLockEntry();
		}
		// COB_CODE: PERFORM 0600-CALL-LOCK-MANAGER.
		callLockManager();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0200-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: IF (HALRLOMG-UPGRADE-LOCK-ENTRY
		//               OR (HALRLOMG-ADD-LOCK-ENTRY AND HALRLOMG-LOCK-TAKEOVER))
		//             AND (HALRLOMG-LOCK-PES-READUPDT
		//                  OR HALRLOMG-LOCK-GROUP-PESSI)
		//               END-IF
		//           END-IF.
		if ((ws.getWsHalrlomgLinkage().getFunction().isHalrlomgUpgradeLockEntry() || ws.getWsHalrlomgLinkage().getFunction().isHalrlomgAddLockEntry()
				&& ws.getWsHalrlomgLinkage().getSuccessInd().isHalrlomgLockTakeover())
				&& (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt() || ws.getWsHalrlomgLinkage().getLokTypeCd().isLockGroupPessi())) {
			// COB_CODE: SET ULLI-UPGRADE-LOCK TO TRUE
			ws.getHallulli().getLegacyLockFunc().setUpgradeLock();
			// COB_CODE: PERFORM 0700-LOCK-PROCESSING
			lockProcessing();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0200-CREATE-ACCESS-LOCK-X
			//           END-IF
			if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0200-CREATE-ACCESS-LOCK-X
				return;
			}
			//** IF CONVERTING FROM GROUP TO PESSI, ZAP ANY EXISTING GROUP
			//** LOCKS HELD BY OTHER GROUP MEMBERS.
			// COB_CODE: IF HALRLOMG-LOCK-PES-READUPDT
			//               PERFORM 0600-CALL-LOCK-MANAGER
			//           END-IF
			if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt()) {
				// COB_CODE: SET HALRLOMG-UPDATE-LOCK-ENTRY TO TRUE
				ws.getWsHalrlomgLinkage().getFunction().setHalrlomgUpdateLockEntry();
				// COB_CODE: PERFORM 0600-CALL-LOCK-MANAGER
				callLockManager();
			}
		}
	}

	/**Original name: 0210-CHECK-CREATE-LOCK-RET_FIRST_SENTENCES<br>
	 * <pre>************************************************************
	 *  EVALUATE THE RESPONSE FROM THE LOCK LEGACY INTERFACE MODULE
	 *  FOR THE CREATE LOCK REQUEST FOR 'CREATE ACCESS LOCK'.
	 * ************************************************************</pre>*/
	private void checkCreateLockRet() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EVALUATE TRUE
		//               WHEN ULLI-LOCK-CREATED
		//               WHEN ULLI-LOCK-UPDATED
		//                   CONTINUE
		//               WHEN ULLI-OTHER-LOCK-FOUND
		//                   GO TO 0210-CHECK-CREATE-LOCK-RET-X
		//               WHEN ULLI-UNKNOWN-OR-ERROR
		//                   GO TO 0210-CHECK-CREATE-LOCK-RET-X
		//               WHEN OTHER
		//                   GO TO 0210-CHECK-CREATE-LOCK-RET-X
		//           END-EVALUATE.
		switch (ws.getHallulli().getLegacyLockResp().getLegacyLockResp()) {

		case UlliLegacyLockResp.LOCK_CREATED:
		case UlliLegacyLockResp.LOCK_UPDATED:// COB_CODE: CONTINUE
			//continue
			break;

		case UlliLegacyLockResp.OTHER_LOCK_FOUND:// COB_CODE: IF HALRLOMG-TYPE-MISMATCH
			//               END-IF
			//           END-IF
			if (ws.getWsHalrlomgLinkage().getSuccessInd().isTypeMismatch()) {
				// COB_CODE: PERFORM 0920-DETERMINE-UPGRADE
				determineUpgrade();
				// COB_CODE: IF WS-UPGRADE-VALID
				//               GO TO 0210-CHECK-CREATE-LOCK-RET-X
				//           END-IF
				if (ws.getWsSwitches().getUpgradeValiditySw().isValid()) {
					// COB_CODE: GO TO 0210-CHECK-CREATE-LOCK-RET-X
					return;
				}
			}
			// COB_CODE: IF UBOC-GROUP-LOCK-USER
			//             AND HALRLOMG-LOCK-PES-READUPDT
			//             AND ULLI-LEGACY-LOCK-USERID = UBOC-LOCK-GROUP
			//             AND NOT HALRLOMG-TYPE-MISMATCH
			//               GO TO 0210-CHECK-CREATE-LOCK-RET-X
			//           END-IF
			if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUserInLockGrpSw().isUbocGroupLockUser()
					&& ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt()
					&& Conditions.eq(ws.getHallulli().getLegacyLockUserid(), wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocLockGroup())
					&& !ws.getWsHalrlomgLinkage().getSuccessInd().isTypeMismatch()) {
				// COB_CODE: SET HALRLOMG-LOCK-TAKEOVER TO TRUE
				ws.getWsHalrlomgLinkage().getSuccessInd().setHalrlomgLockTakeover();
				// COB_CODE: GO TO 0210-CHECK-CREATE-LOCK-RET-X
				return;
			}
			// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			// COB_CODE: MOVE ULLI-TABLE-NM
			//             TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getHallulli().getTableNm());
			// COB_CODE: MOVE SPACES
			//             TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("");
			// COB_CODE: MOVE ULLI-LEGACY-LOCK-USERID
			//             TO WS-NONLOG-ERR-COL1-VALUE
			ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(ws.getHallulli().getLegacyLockUserid());
			// COB_CODE: MOVE 'HAL_LOKOTH' TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("HAL_LOKOTH");
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: GO TO 0210-CHECK-CREATE-LOCK-RET-X
			return;

		case UlliLegacyLockResp.UNKNOWN_OR_ERROR:// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: GO TO 0210-CHECK-CREATE-LOCK-RET-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR              TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-LOCKING-FAILED       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLockingFailed();
			// COB_CODE: SET LOK-INVALID-LEGACY-RET    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setLokInvalidLegacyRet();
			// COB_CODE: MOVE '0210-CHECK-CREATE-LOCK-RET'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0210-CHECK-CREATE-LOCK-RET");
			// COB_CODE: STRING 'UNEXPECTED RETURN FROM LEGACY LOCK UTIL: '
			//                   WS-LEGACY-LOCK-MODULE
			//             DELIMITED BY SIZE
			//             INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "UNEXPECTED RETURN FROM LEGACY LOCK UTIL: ",
					ws.getWsWorkFields().getLegacyLockModuleFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'RET CD='
			//                   ULLI-LEGACY-LOCK-RESP ';'
			//                  'KEY='
			//                   ULLI-TCH-KEY ';'
			//                  'BO='
			//                   ULLI-BUS-OBJ-NM
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "RET CD=", String.valueOf(ws.getHallulli().getLegacyLockResp().getLegacyLockResp()), ";", "KEY=",
							ws.getHallulli().getTchKeyFormatted(), ";", "BO=", ws.getHallulli().getBusObjNmFormatted() });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0210-CHECK-CREATE-LOCK-RET-X
			return;
		}
	}

	/**Original name: 0215-CHECK-VERIFY-LOCK-RET_FIRST_SENTENCES<br>
	 * <pre>************************************************************
	 *  EVALUATE THE RESPONSE FROM THE LOCK LEGACY INTERFACE MODULE
	 *   FOR THE VERIFY LOCK REQUEST FOR 'CREATE ACCESS LOCK -
	 *   OPTIMISTIC'.
	 * ************************************************************</pre>*/
	private void checkVerifyLockRet() {
		ConcatUtil concatUtil = null;
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ULLI-LOCK-NOT-FOUND
		//                        CONTINUE
		//                    WHEN ULLI-USER-LOCK-FOUND
		//                        CONTINUE
		//           *17238              SET WS-NON-LOGGABLE-WARNING TO TRUE
		//           *17238              MOVE ULLI-TABLE-NM
		//           *17238                TO UWRN-FAILED-TABLE-OR-FILE
		//           *17238              MOVE SPACES
		//           *17238                TO UWRN-FAILED-COLUMN-OR-FIELD
		//           *17238              MOVE UBOC-AUTH-USERID
		//           *17238                TO WS-NONLOG-ERR-COL1-VALUE
		//           *17238              MOVE 'HAL_LOKUSR' TO UWRN-WARNING-CODE
		//           *17238              PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
		//           *17238              GO TO 0215-CHECK-VERIFY-LOCK-RET-X
		//                    WHEN ULLI-OTHER-LOCK-FOUND
		//                        GO TO 0215-CHECK-VERIFY-LOCK-RET-X
		//                    WHEN ULLI-UNKNOWN-OR-ERROR
		//                        GO TO 0215-CHECK-VERIFY-LOCK-RET-X
		//                    WHEN OTHER
		//                        GO TO 0215-CHECK-VERIFY-LOCK-RET-X
		//                END-EVALUATE.
		switch (ws.getHallulli().getLegacyLockResp().getLegacyLockResp()) {

		case UlliLegacyLockResp.LOCK_NOT_FOUND:// COB_CODE: CONTINUE
			//continue
			break;

		case UlliLegacyLockResp.USER_LOCK_FOUND:// COB_CODE: CONTINUE
			//continue
			//17238              SET WS-NON-LOGGABLE-WARNING TO TRUE
			//17238              MOVE ULLI-TABLE-NM
			//17238                TO UWRN-FAILED-TABLE-OR-FILE
			//17238              MOVE SPACES
			//17238                TO UWRN-FAILED-COLUMN-OR-FIELD
			//17238              MOVE UBOC-AUTH-USERID
			//17238                TO WS-NONLOG-ERR-COL1-VALUE
			//17238              MOVE 'HAL_LOKUSR' TO UWRN-WARNING-CODE
			//17238              PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			//17238              GO TO 0215-CHECK-VERIFY-LOCK-RET-X
			break;

		case UlliLegacyLockResp.OTHER_LOCK_FOUND:// COB_CODE: IF HALRLOMG-CHECK-INSRET-LOCK
			//             OR HALRLODR-VERIFY-INSERT-ACCESS
			//               MOVE 'HAL_LOKOTH' TO NLBE-ERROR-CODE
			//           ELSE
			//               MOVE 'HAL_LOKOTH' TO UWRN-WARNING-CODE
			//           END-IF
			if (ws.getWsHalrlomgLinkage().getFunction().isCheckInsretLock() || wsHalrlodrLinkage.getFunction().isVerifyInsertAccess()) {
				// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
				wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
				// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
				ws.getWsNonLoggableWarnOrErrSw().setBusErr();
				// COB_CODE: MOVE ULLI-TABLE-NM
				//             TO NLBE-FAILED-TABLE-OR-FILE
				ws.getNlbeCommon().setFailedTableOrFile(ws.getHallulli().getTableNm());
				// COB_CODE: MOVE SPACES
				//             TO NLBE-FAILED-COLUMN-OR-FIELD
				ws.getNlbeCommon().setFailedColumnOrField("");
				// COB_CODE: MOVE ULLI-LEGACY-LOCK-USERID
				//             TO WS-NONLOG-ERR-COL1-VALUE
				ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(ws.getHallulli().getLegacyLockUserid());
				// COB_CODE: MOVE 'HAL_LOKOTH' TO NLBE-ERROR-CODE
				ws.getNlbeCommon().setErrorCode("HAL_LOKOTH");
			} else {
				// COB_CODE: SET WS-NON-LOGGABLE-WARNING TO TRUE
				ws.getWsNonLoggableWarnOrErrSw().setWarning();
				// COB_CODE: MOVE ULLI-TABLE-NM
				//             TO UWRN-FAILED-TABLE-OR-FILE
				ws.getUwrnCommon().setFailedTableOrFile(ws.getHallulli().getTableNm());
				// COB_CODE: MOVE SPACES
				//             TO UWRN-FAILED-COLUMN-OR-FIELD
				ws.getUwrnCommon().setFailedColumnOrField("");
				// COB_CODE: MOVE ULLI-LEGACY-LOCK-USERID
				//             TO WS-NONLOG-ERR-COL1-VALUE
				ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(ws.getHallulli().getLegacyLockUserid());
				// COB_CODE: MOVE 'HAL_LOKOTH' TO UWRN-WARNING-CODE
				ws.getUwrnCommon().setWarningCode("HAL_LOKOTH");
			}
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 0215-CHECK-VERIFY-LOCK-RET-X
			return;

		case UlliLegacyLockResp.UNKNOWN_OR_ERROR:// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: GO TO 0215-CHECK-VERIFY-LOCK-RET-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR              TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-LOCKING-FAILED       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLockingFailed();
			// COB_CODE: SET LOK-INVALID-LEGACY-RET    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setLokInvalidLegacyRet();
			// COB_CODE: MOVE '0215-CHECK-VERIFY-LOCK-RET'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0215-CHECK-VERIFY-LOCK-RET");
			// COB_CODE: STRING 'UNEXPECTED RETURN FROM LEGACY LOCK UTIL: '
			//                   WS-LEGACY-LOCK-MODULE
			//             DELIMITED BY SIZE
			//             INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "UNEXPECTED RETURN FROM LEGACY LOCK UTIL: ",
					ws.getWsWorkFields().getLegacyLockModuleFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'RET CD='
			//                   ULLI-LEGACY-LOCK-RESP ';'
			//                  'KEY='
			//                   ULLI-TCH-KEY ';'
			//                  'BO='
			//                   ULLI-BUS-OBJ-NM
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "RET CD=", String.valueOf(ws.getHallulli().getLegacyLockResp().getLegacyLockResp()), ";", "KEY=",
							ws.getHallulli().getTchKeyFormatted(), ";", "BO=", ws.getHallulli().getBusObjNmFormatted() });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0215-CHECK-VERIFY-LOCK-RET-X
			return;
		}
	}

	/**Original name: 0300-LOOKUP-LOCK-EXCPT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  LOOKUP TO THE LOCK EXCEPTION TABLE - HAL_BO_LOK_EXCPT          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void lookupLockExcpt() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-UOW-NAME            TO HBLE-UOW-NM.
		ws.getDclhalBoLokExc().setUowNm(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE HALRLODR-BUS-OBJ-NM      TO HBLE-BUS-OBJ-NM.
		ws.getDclhalBoLokExc().setBusObjNm(wsHalrlodrLinkage.getBusObjNm());
		//* CURSOR1: EXACT MATCH ON PRIMARY KEY
		// COB_CODE: EXEC SQL
		//             DECLARE WS-CURSOR1 CURSOR FOR
		//                 SELECT HBLE_CONTEXT_TXT,
		//                        HBLE_LOK_EXC_MDU
		//                   FROM HAL_BO_LOK_EXC_V
		//                  WHERE UOW_NM   = :HBLE-UOW-NM
		//                    AND BUS_OBJ_NM = :HBLE-BUS-OBJ-NM
		//                    FOR READ ONLY
		//           END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
		// COB_CODE: EXEC SQL
		//               OPEN WS-CURSOR1
		//           END-EXEC.
		halBoLokExcVDao.openWsCursor11(ws.getDclhalBoLokExc().getUowNm(), ws.getDclhalBoLokExc().getBusObjNm());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 0300-LOOKUP-LOCK-EXCPT-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR              TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED           TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_BO_LOK_EXC'         TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_LOK_EXC");
			// COB_CODE: MOVE '0300-LOOKUP-LOCK-EXCPT' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0300-LOOKUP-LOCK-EXCPT");
			// COB_CODE: MOVE 'OPEN CURSOR1 FAILED'    TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CURSOR1 FAILED");
			// COB_CODE: STRING 'UBOC-UOW-NAME='
			//                   UBOC-UOW-NAME ';'
			//                  'HALRLODR-BUS-OBJ-NM='
			//                   HALRLODR-BUS-OBJ-NM  ';'
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UBOC-UOW-NAME=", wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowNameFormatted(), ";",
							"HALRLODR-BUS-OBJ-NM=", wsHalrlodrLinkage.getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0300-LOOKUP-LOCK-EXCPT-X
			return;
		}
		// COB_CODE: SET WS-NOT-END-OF-CURSOR1 TO TRUE.
		ws.getWsSwitches().getEndOfCursor1Sw().setNotEndOfCursor1();
		// COB_CODE: PERFORM 0350-FETCH-CURSOR1
		//             UNTIL UBOC-HALT-AND-RETURN
		//                OR WS-END-OF-CURSOR1.
		while (!(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsSwitches().getEndOfCursor1Sw().isEndOfCursor1())) {
			fetchCursor1();
		}
		// COB_CODE: EXEC SQL
		//               CLOSE WS-CURSOR1
		//           END-EXEC.
		halBoLokExcVDao.closeWsCursor11();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 0300-LOOKUP-LOCK-EXCPT-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING            TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-DB2-FAILED           TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_BO_LOK_EXC'         TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_LOK_EXC");
			// COB_CODE: MOVE '0300-LOOKUP-LOCK-EXCPT' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0300-LOOKUP-LOCK-EXCPT");
			// COB_CODE: MOVE 'CLOSE CURSOR1 FAILED'   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CURSOR1 FAILED");
			// COB_CODE: STRING 'UBOC-UOW-NAME='
			//                   UBOC-UOW-NAME ';'
			//                  'HALRLODR-BUS-OBJ-NM='
			//                   HALRLODR-BUS-OBJ-NM  ';'
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UBOC-UOW-NAME=", wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowNameFormatted(), ";",
							"HALRLODR-BUS-OBJ-NM=", wsHalrlodrLinkage.getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0300-LOOKUP-LOCK-EXCPT-X
			return;
		}
	}

	/**Original name: 0350-FETCH-CURSOR1_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  FETCH ROW DATA USING CURSOR WITH EXACT KEY                     *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void fetchCursor1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//               FETCH WS-CURSOR1
		//                INTO :HBLE-CONTEXT-TXT,
		//                     :HBLE-LOK-EXC-MDU
		//           END-EXEC.
		halBoLokExcVDao.fetchWsCursor11(ws.getDclhalBoLokExc());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    PERFORM 0355-CALL-LOCK-EXCPT-BPO
		//               WHEN ERD-SQL-NOT-FOUND
		//                    SET WS-END-OF-CURSOR1 TO TRUE
		//               WHEN OTHER
		//                    GO TO 0350-FETCH-CURSOR1-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: PERFORM 0355-CALL-LOCK-EXCPT-BPO
			callLockExcptBpo();
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CURSOR1 TO TRUE
			ws.getWsSwitches().getEndOfCursor1Sw().setEndOfCursor1();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'HAL_BO_LOK_EXC'      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_LOK_EXC");
			// COB_CODE: MOVE '0350-FETCH-CURSOR1'  TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0350-FETCH-CURSOR1");
			// COB_CODE: MOVE 'FETCH FROM CURSOR1 FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH FROM CURSOR1 FAILED");
			// COB_CODE: STRING 'UBOC-UOW-NAME='
			//                   UBOC-UOW-NAME ';'
			//                  'HALRLODR-BUS-OBJ-NM='
			//                   HALRLODR-BUS-OBJ-NM  ';'
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UBOC-UOW-NAME=", wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowNameFormatted(), ";",
							"HALRLODR-BUS-OBJ-NM=", wsHalrlodrLinkage.getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0350-FETCH-CURSOR1-X
			return;
		}
	}

	/**Original name: 0355-CALL-LOCK-EXCPT-BPO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL THE LOCK EXCEPTION BPO NAMED IN LOK_EXCPT_MOD             *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void callLockExcptBpo() {
		// COB_CODE: INITIALIZE UBOC-EXTRA-DATA.
		initUbocExtraData();
		// COB_CODE: MOVE HALRLODR-INPUT-LINKAGE TO UBOC-APP-DATA-BUFFER.
		wsHallubocCommsArea.getUbocRecord().setAppDataBuffer(wsHalrlodrLinkage.getInputLinkageFormatted());
		// COB_CODE: MOVE LENGTH OF HALRLODR-INPUT-LINKAGE
		//             TO UBOC-APP-DATA-BUFFER-LENGTH.
		wsHallubocCommsArea.getUbocRecord().setAppDataBufferLength(((short) WsHalrlodrLinkage.Len.INPUT_LINKAGE));
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM   (HBLE-LOK-EXC-MDU)
		//                COMMAREA  (UBOC-RECORD)
		//                LENGTH    (LENGTH OF UBOC-RECORD)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALRLODR", execContext).commarea(wsHallubocCommsArea.getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD)
				.link(ws.getDclhalBoLokExc().getLokExcMdu());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    CONTINUE
		//               WHEN OTHER
		//                    GO TO 0355-CALL-LOCK-EXCPT-BPO-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE HBLE-LOK-EXC-MDU      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getDclhalBoLokExc().getLokExcMdu());
			// COB_CODE: MOVE '0355-CALL-LOCK-EXCPT-BPO'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0355-CALL-LOCK-EXCPT-BPO");
			// COB_CODE: MOVE 'CALL TO LOCK EXCEPTION BPO FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CALL TO LOCK EXCEPTION BPO FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0355-CALL-LOCK-EXCPT-BPO-X
			return;
		}
	}

	/**Original name: 0500-AUTH-ACCESS-LOCK_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PROCESSING FOR FUNCTION 'AUTHENTICATE_ACCESS_LOCK'             *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void authAccessLock() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE SPACES                    TO HALRLOMG-LOK-TYPE-CD.
		ws.getWsHalrlomgLinkage().getLokTypeCd().setLokTypeCd(Types.SPACE_CHAR);
		// COB_CODE: PERFORM 0900-DETERMINE-LOCK-TYPE.
		determineLockType();
		// COB_CODE: IF UBOC-HALT-AND-RETURN OR HALRLOMG-NO-LOCK-NEEDED
		//               GO TO 0500-AUTH-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsHalrlomgLinkage().getLokTypeCd().isHalrlomgNoLockNeeded()) {
			// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: PERFORM 0910-DETERMINE-LOCK-GROUP.
		determineLockGroup();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0500-AUTH-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: SET HALRLOMG-CHECK-UPDATE-LOCK TO TRUE.
		ws.getWsHalrlomgLinkage().getFunction().setHalrlomgCheckUpdateLock();
		// COB_CODE: PERFORM 0600-CALL-LOCK-MANAGER.
		callLockManager();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0500-AUTH-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
			return;
		}
		//* CHECK LOCK MANAGER SUCCESS INDICATOR
		// COB_CODE: EVALUATE TRUE
		//               WHEN HALRLOMG-FOUND
		//               WHEN HALRLOMG-TYPE-MISMATCH
		//                    CONTINUE
		//               WHEN HALRLOMG-NOT-FOUND
		//                    GO TO 0500-AUTH-ACCESS-LOCK-X
		//               WHEN HALRLOMG-ZAPPED
		//                    GO TO 0500-AUTH-ACCESS-LOCK-X
		//               WHEN OTHER
		//                    GO TO 0500-AUTH-ACCESS-LOCK-X
		//           END-EVALUATE.
		switch (ws.getWsHalrlomgLinkage().getSuccessInd().getSuccessInd()) {

		case HalrlomgSuccessInd.FOUND:
		case HalrlomgSuccessInd.TYPE_MISMATCH:// COB_CODE: CONTINUE
			//continue
			break;

		case HalrlomgSuccessInd.NOT_FOUND:// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			//*            MOVE 'LOCKING: LOCK NOT FOUND'
			// COB_CODE: MOVE HALRLODR-TABLE-NM
			//             TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(wsHalrlodrLinkage.getTableNm());
			// COB_CODE: MOVE SPACES TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("");
			// COB_CODE: MOVE 'HAL_LOKNFD'        TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("HAL_LOKNFD");
			// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
			return;

		case HalrlomgSuccessInd.ZAPPED:// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			//*            MOVE 'LOCKING: LOCK SUPERSEDED'
			// COB_CODE: MOVE HALRLODR-TABLE-NM
			//             TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(wsHalrlodrLinkage.getTableNm());
			//*            MOVE HALRLOMG-ZAPPED-BY-USERID
			// COB_CODE: MOVE SPACES
			//             TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("");
			// COB_CODE: MOVE HALRLOMG-ZAPPED-BY-USERID
			//             TO WS-NONLOG-ERR-COL1-VALUE
			ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(ws.getWsHalrlomgLinkage().getZappedByUserid());
			// COB_CODE: MOVE 'HAL_LOKZAP'        TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("HAL_LOKZAP");
			// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-ACTION-CODE    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvActionCode();
			// COB_CODE: MOVE 'HALRLODR'
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALRLODR");
			// COB_CODE: MOVE '0500-AUTH-ACCESS-LOCK'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0500-AUTH-ACCESS-LOCK");
			// COB_CODE: MOVE 'INVALID SUCCESS INDICATOR'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID SUCCESS INDICATOR");
			// COB_CODE: STRING 'HALRLOMG-SUCCESS-IND='
			//                   HALRLOMG-SUCCESS-IND ';'
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HALRLOMG-SUCCESS-IND=",
					String.valueOf(ws.getWsHalrlomgLinkage().getSuccessInd().getSuccessInd()), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
			return;
		}
		//*   SET WS-READ-UPDATE-LOCK   TO TRUE.
		// COB_CODE: IF HALRLOMG-FOUND
		//               SET ULLI-VERIFY-UPDATE-LOCK TO TRUE
		//           ELSE
		//               SET ULLI-CREATE-UPDATE-LOCK TO TRUE
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getSuccessInd().isFound()) {
			// COB_CODE: SET ULLI-VERIFY-UPDATE-LOCK TO TRUE
			ws.getHallulli().getLegacyLockFunc().setVerifyUpdateLock();
		} else {
			// COB_CODE: SET ULLI-CREATE-UPDATE-LOCK TO TRUE
			ws.getHallulli().getLegacyLockFunc().setCreateUpdateLock();
		}
		//*   MOVE HALRLOMG-LOK-TYPE-CD TO HBLT-LOK-TYPE-CD.
		// COB_CODE: PERFORM 0700-LOCK-PROCESSING.
		lockProcessing();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0500-AUTH-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: IF LEGACY-LOCK-MODULE-FOUND
		//               END-IF
		//           END-IF.
		if (ws.getWsSwitches().getLegacyLockModSw().isLegacyLockModuleFound()) {
			// COB_CODE: IF ULLI-VERIFY-UPDATE-LOCK
			//               PERFORM 0510-CHECK-VERIFY-RETURN
			//           ELSE
			//               PERFORM 0515-CHECK-CREATE-RETURN
			//           END-IF
			if (ws.getHallulli().getLegacyLockFunc().isVerifyUpdateLock()) {
				// COB_CODE: PERFORM 0510-CHECK-VERIFY-RETURN
				checkVerifyReturn();
			} else {
				// COB_CODE: PERFORM 0515-CHECK-CREATE-RETURN
				checkCreateReturn();
			}
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0500-AUTH-ACCESS-LOCK-X
			//           END-IF
			if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
				return;
			}
		}
		// COB_CODE: IF HALRLOMG-TYPE-MISMATCH
		//               SET HALRLOMG-UPGRADE-LOCK-ENTRY TO TRUE
		//           ELSE
		//               SET HALRLOMG-UPDATE-LOCK-ENTRY TO TRUE
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getSuccessInd().isTypeMismatch()) {
			// COB_CODE: SET HALRLOMG-UPGRADE-LOCK-ENTRY TO TRUE
			ws.getWsHalrlomgLinkage().getFunction().setHalrlomgUpgradeLockEntry();
		} else {
			// COB_CODE: SET HALRLOMG-UPDATE-LOCK-ENTRY TO TRUE
			ws.getWsHalrlomgLinkage().getFunction().setHalrlomgUpdateLockEntry();
		}
		// COB_CODE: PERFORM 0600-CALL-LOCK-MANAGER.
		callLockManager();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0500-AUTH-ACCESS-LOCK-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
			return;
		}
		// COB_CODE: IF HALRLOMG-UPGRADE-LOCK-ENTRY
		//             AND (HALRLOMG-LOCK-PES-READUPDT
		//                  OR HALRLOMG-LOCK-GROUP-PESSI)
		//               END-IF
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getFunction().isHalrlomgUpgradeLockEntry()
				&& (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt() || ws.getWsHalrlomgLinkage().getLokTypeCd().isLockGroupPessi())) {
			// COB_CODE: SET ULLI-UPGRADE-LOCK TO TRUE
			ws.getHallulli().getLegacyLockFunc().setUpgradeLock();
			// COB_CODE: PERFORM 0700-LOCK-PROCESSING
			lockProcessing();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0500-AUTH-ACCESS-LOCK-X
			//           END-IF
			if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0500-AUTH-ACCESS-LOCK-X
				return;
			}
			//** IF CONVERTING FROM GROUP TO PESSI, ZAP ANY EXISTING GROUP
			//** LOCKS HELD BY OTHER GROUP MEMBERS.
			// COB_CODE: IF HALRLOMG-LOCK-PES-READUPDT
			//               PERFORM 0600-CALL-LOCK-MANAGER
			//           END-IF
			if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt()) {
				// COB_CODE: SET HALRLOMG-UPDATE-LOCK-ENTRY TO TRUE
				ws.getWsHalrlomgLinkage().getFunction().setHalrlomgUpdateLockEntry();
				// COB_CODE: PERFORM 0600-CALL-LOCK-MANAGER
				callLockManager();
			}
		}
	}

	/**Original name: 0510-CHECK-VERIFY-RETURN_FIRST_SENTENCES<br>
	 * <pre>************************************************************
	 *  EVALUATE THE RESPONSE FROM THE LOCK LEGACY INTERFACE MODULE
	 *   FOR THE VERIFY LOCK REQUEST FOR 'AUTH ACCESS LOCK -
	 *   PESSIMISTIC'.
	 * ************************************************************</pre>*/
	private void checkVerifyReturn() {
		ConcatUtil concatUtil = null;
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ULLI-USER-LOCK-FOUND
		//                        PERFORM 0520-UPDATE-LOCK-EXPIRY-TS
		//                    WHEN ULLI-OTHER-LOCK-FOUND
		//                        GO TO 0510-CHECK-VERIFY-RETURN-X
		//                    WHEN ULLI-LOCK-NOT-FOUND
		//           **17238      IF HBLT-LOK-TYPE-CD    = 'P'
		//                        END-IF
		//                    WHEN ULLI-UNKNOWN-OR-ERROR
		//                        GO TO 0510-CHECK-VERIFY-RETURN-X
		//                    WHEN OTHER
		//                        GO TO 0510-CHECK-VERIFY-RETURN-X
		//                END-EVALUATE.
		switch (ws.getHallulli().getLegacyLockResp().getLegacyLockResp()) {

		case UlliLegacyLockResp.USER_LOCK_FOUND:// COB_CODE: PERFORM 0520-UPDATE-LOCK-EXPIRY-TS
			updateLockExpiryTs();
			break;

		case UlliLegacyLockResp.OTHER_LOCK_FOUND:// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			// COB_CODE: MOVE ULLI-TABLE-NM
			//             TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getHallulli().getTableNm());
			// COB_CODE: MOVE SPACES
			//             TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("");
			// COB_CODE: MOVE ULLI-LEGACY-LOCK-USERID
			//             TO WS-NONLOG-ERR-COL1-VALUE
			ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(ws.getHallulli().getLegacyLockUserid());
			// COB_CODE: MOVE 'HAL_LOKOTH' TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("HAL_LOKOTH");
			// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 0510-CHECK-VERIFY-RETURN-X
			return;

		case UlliLegacyLockResp.LOCK_NOT_FOUND://*17238      IF HBLT-LOK-TYPE-CD    = 'P'
			// COB_CODE:              IF HALRLOMG-LOCK-PES-READUPDT
			//                          OR HALRLOMG-LOCK-GROUP-PESSI
			//           ** LOCK DOES NOT EXIST - ERROR IF LOKTYPE CODE IS PESSIMISTIC
			//                            GO TO 0510-CHECK-VERIFY-RETURN-X
			//                        ELSE
			//                            GO TO 0510-CHECK-VERIFY-RETURN-X
			//                        END-IF
			if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt() || ws.getWsHalrlomgLinkage().getLokTypeCd().isLockGroupPessi()) {
				//* LOCK DOES NOT EXIST - ERROR IF LOKTYPE CODE IS PESSIMISTIC
				// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
				ws.getWsNonLoggableWarnOrErrSw().setBusErr();
				// COB_CODE: MOVE ULLI-TABLE-NM
				//             TO NLBE-FAILED-TABLE-OR-FILE
				ws.getNlbeCommon().setFailedTableOrFile(ws.getHallulli().getTableNm());
				// COB_CODE: MOVE SPACES TO NLBE-FAILED-COLUMN-OR-FIELD
				ws.getNlbeCommon().setFailedColumnOrField("");
				// COB_CODE: MOVE 'HAL_LOKNFD'        TO NLBE-ERROR-CODE
				ws.getNlbeCommon().setErrorCode("HAL_LOKNFD");
				// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
				wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
				// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
				procNonLogWrnOrErr();
				// COB_CODE: GO TO 0510-CHECK-VERIFY-RETURN-X
				return;
			} else {
				// COB_CODE: GO TO 0510-CHECK-VERIFY-RETURN-X
				return;
			}

		case UlliLegacyLockResp.UNKNOWN_OR_ERROR:// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: GO TO 0510-CHECK-VERIFY-RETURN-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR              TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-LOCKING-FAILED       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLockingFailed();
			// COB_CODE: SET LOK-INVALID-LEGACY-RET    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setLokInvalidLegacyRet();
			// COB_CODE: MOVE '0510-CHECK-VERIFY-RETURN'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0510-CHECK-VERIFY-RETURN");
			// COB_CODE: STRING 'UNEXPECTED RETURN FROM LEGACY LOCK UTIL: '
			//                   WS-LEGACY-LOCK-MODULE
			//             DELIMITED BY SIZE
			//             INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "UNEXPECTED RETURN FROM LEGACY LOCK UTIL: ",
					ws.getWsWorkFields().getLegacyLockModuleFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'RET CD='
			//                   ULLI-LEGACY-LOCK-RESP ';'
			//                  'KEY='
			//                   ULLI-TCH-KEY ';'
			//                  'BO='
			//                   ULLI-BUS-OBJ-NM
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "RET CD=", String.valueOf(ws.getHallulli().getLegacyLockResp().getLegacyLockResp()), ";", "KEY=",
							ws.getHallulli().getTchKeyFormatted(), ";", "BO=", ws.getHallulli().getBusObjNmFormatted() });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0510-CHECK-VERIFY-RETURN-X
			return;
		}
	}

	/**Original name: 0515-CHECK-CREATE-RETURN_FIRST_SENTENCES<br>
	 * <pre>************************************************************
	 *  EVALUATE THE RESPONSE FROM THE LOCK LEGACY INTERFACE MODULE
	 *  FOR THE AUTHENTICATE LOCK REQUEST - UPGRADE LOCK TYPE.
	 * ************************************************************</pre>*/
	private void checkCreateReturn() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EVALUATE TRUE
		//               WHEN ULLI-LOCK-CREATED
		//               WHEN ULLI-LOCK-UPDATED
		//                   CONTINUE
		//               WHEN ULLI-OTHER-LOCK-FOUND
		//                   GO TO 0515-CHECK-CREATE-RETURN-X
		//               WHEN ULLI-UNKNOWN-OR-ERROR
		//                   GO TO 0515-CHECK-CREATE-RETURN-X
		//               WHEN OTHER
		//                   GO TO 0515-CHECK-CREATE-RETURN-X
		//           END-EVALUATE.
		switch (ws.getHallulli().getLegacyLockResp().getLegacyLockResp()) {

		case UlliLegacyLockResp.LOCK_CREATED:
		case UlliLegacyLockResp.LOCK_UPDATED:// COB_CODE: CONTINUE
			//continue
			break;

		case UlliLegacyLockResp.OTHER_LOCK_FOUND:// COB_CODE: IF HALRLOMG-TYPE-MISMATCH
			//               END-IF
			//           END-IF
			if (ws.getWsHalrlomgLinkage().getSuccessInd().isTypeMismatch()) {
				// COB_CODE: PERFORM 0920-DETERMINE-UPGRADE
				determineUpgrade();
				// COB_CODE: IF WS-UPGRADE-VALID
				//               GO TO 0515-CHECK-CREATE-RETURN-X
				//           END-IF
				if (ws.getWsSwitches().getUpgradeValiditySw().isValid()) {
					// COB_CODE: GO TO 0515-CHECK-CREATE-RETURN-X
					return;
				}
			}
			// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			// COB_CODE: MOVE ULLI-TABLE-NM
			//             TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getHallulli().getTableNm());
			// COB_CODE: MOVE SPACES
			//             TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("");
			// COB_CODE: MOVE ULLI-LEGACY-LOCK-USERID
			//             TO WS-NONLOG-ERR-COL1-VALUE
			ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(ws.getHallulli().getLegacyLockUserid());
			// COB_CODE: MOVE 'HAL_LOKOTH' TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("HAL_LOKOTH");
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: GO TO 0515-CHECK-CREATE-RETURN-X
			return;

		case UlliLegacyLockResp.UNKNOWN_OR_ERROR:// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: GO TO 0515-CHECK-CREATE-RETURN-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR              TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-LOCKING-FAILED       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLockingFailed();
			// COB_CODE: SET LOK-INVALID-LEGACY-RET    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setLokInvalidLegacyRet();
			// COB_CODE: MOVE '0515-CHECK-CREATE-RETURN'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0515-CHECK-CREATE-RETURN");
			// COB_CODE: STRING 'UNEXPECTED RETURN FROM LEGACY LOCK UTIL: '
			//                   WS-LEGACY-LOCK-MODULE
			//             DELIMITED BY SIZE
			//             INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "UNEXPECTED RETURN FROM LEGACY LOCK UTIL: ",
					ws.getWsWorkFields().getLegacyLockModuleFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'RET CD='
			//                   ULLI-LEGACY-LOCK-RESP ';'
			//                  'KEY='
			//                   ULLI-TCH-KEY ';'
			//                  'BO='
			//                   ULLI-BUS-OBJ-NM
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "RET CD=", String.valueOf(ws.getHallulli().getLegacyLockResp().getLegacyLockResp()), ";", "KEY=",
							ws.getHallulli().getTchKeyFormatted(), ";", "BO=", ws.getHallulli().getBusObjNmFormatted() });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0515-CHECK-CREATE-RETURN-X
			return;
		}
	}

	/**Original name: 0520-UPDATE-LOCK-EXPIRY-TS_FIRST_SENTENCES<br>
	 * <pre>************************************************************
	 *  EVALUATE THE RESPONSE FROM THE LOCK LEGACY INTERFACE MODULE
	 *  FOR THE UPDATE LOCK REQUEST FOR 'AUTH ACCESS LOCK'.
	 * ************************************************************</pre>*/
	private void updateLockExpiryTs() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ULLI-CREATE-UPDATE-LOCK TO TRUE.
		ws.getHallulli().getLegacyLockFunc().setCreateUpdateLock();
		// COB_CODE: PERFORM 0700-LOCK-PROCESSING.
		lockProcessing();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0520-UPDATE-LOCK-EXPIRY-TS-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0520-UPDATE-LOCK-EXPIRY-TS-X
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN ULLI-LOCK-UPDATED
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0520-UPDATE-LOCK-EXPIRY-TS-X
		//           END-EVALUATE.
		switch (ws.getHallulli().getLegacyLockResp().getLegacyLockResp()) {

		case UlliLegacyLockResp.LOCK_UPDATED:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR              TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-LOCKING-FAILED       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLockingFailed();
			// COB_CODE: SET LOK-INVALID-LEGACY-RET    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setLokInvalidLegacyRet();
			// COB_CODE: MOVE '0520-UPDATE-LOCK-EXPIRY-TS'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0520-UPDATE-LOCK-EXPIRY-TS");
			// COB_CODE: STRING 'UNEXPECTED RETURN FROM LEGACY LOCK UTIL: '
			//                   WS-LEGACY-LOCK-MODULE
			//             DELIMITED BY SIZE
			//             INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "UNEXPECTED RETURN FROM LEGACY LOCK UTIL: ",
					ws.getWsWorkFields().getLegacyLockModuleFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'RET CD='
			//                   ULLI-LEGACY-LOCK-RESP ';'
			//                  'KEY='
			//                   ULLI-TCH-KEY ';'
			//                  'BO='
			//                   ULLI-BUS-OBJ-NM
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "RET CD=", String.valueOf(ws.getHallulli().getLegacyLockResp().getLegacyLockResp()), ";", "KEY=",
							ws.getHallulli().getTchKeyFormatted(), ";", "BO=", ws.getHallulli().getBusObjNmFormatted() });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0520-UPDATE-LOCK-EXPIRY-TS-X
			return;
		}
	}

	/**Original name: 0600-CALL-LOCK-MANAGER_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL THE LOCK MANAGER PROGRAM HALRLOMG                         *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void callLockManager() {
		Halrlomg halrlomg = null;
		// COB_CODE: MOVE HALRLODR-TCH-KEY  TO HALRLOMG-TCH-KEY.
		ws.getWsHalrlomgLinkage().setTchKey(wsHalrlodrLinkage.getTchKey());
		// COB_CODE: MOVE HALRLODR-APP-ID   TO HALRLOMG-APP-ID.
		ws.getWsHalrlomgLinkage().setAppId(wsHalrlodrLinkage.getAppId());
		// COB_CODE: MOVE HALRLODR-TABLE-NM TO HALRLOMG-TABLE-NM.
		ws.getWsHalrlomgLinkage().setTableNm(wsHalrlodrLinkage.getTableNm());
		// COB_CODE: CALL WS-HALRLOMG-NAME
		//                USING      DFHEIBLK
		//                           DFHCOMMAREA
		//                           WS-HALLUBOC-COMMS-AREA
		//                           WS-HALRLOMG-LINKAGE.
		halrlomg = Halrlomg.getInstance();
		halrlomg.run(execContext, dfhcommarea, wsHallubocCommsArea, ws.getWsHalrlomgLinkage());
	}

	/**Original name: 0700-LOCK-PROCESSING_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  LOCK PROCESSING ROUTINE - SETUP LEGACY LINKAGE, DETERMINE      *
	 *  LEGACY MODULE, LINK TO LEGACY MODULE, EVALUATE RESPONSE.       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void lockProcessing() {
		// COB_CODE: PERFORM 0720-INIT-LEGACY-LINKAGE.
		initLegacyLinkage();
		// COB_CODE: IF RETRIEVE-LEGACY-LOCK-MODULE
		//               PERFORM 0730-GET-LEGACY-LOCK-MODULE
		//           END-IF.
		if (ws.getWsSwitches().getLegacyLockModSw().isRetrieveLegacyLockModule()) {
			// COB_CODE: PERFORM 0730-GET-LEGACY-LOCK-MODULE
			getLegacyLockModule();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//             OR NO-LEGACY-LOCK-MODULE
		//               GO TO 0700-LOCK-PROCESSING-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsSwitches().getLegacyLockModSw().isNoLegacyLockModule()) {
			// COB_CODE: GO TO 0700-LOCK-PROCESSING-X
			return;
		}
		// COB_CODE: IF HALRLOMG-LOCK-GROUP-PESSI
		//               MOVE UBOC-LOCK-GROUP TO UBOC-AUTH-USERID
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockGroupPessi()) {
			// COB_CODE: MOVE UBOC-AUTH-USERID TO WS-UBOC-AUTH-USERID
			ws.getWsWorkFields().setUbocAuthUserid(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocAuthUserid());
			// COB_CODE: MOVE UBOC-LOCK-GROUP TO UBOC-AUTH-USERID
			wsHallubocCommsArea.getUbocRecord().getCommInfo().setUbocAuthUserid(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocLockGroup());
		}
		// COB_CODE: PERFORM 0800-CALL-LEGACY-LOCKING.
		callLegacyLocking();
		// COB_CODE: IF HALRLOMG-LOCK-GROUP-PESSI
		//               MOVE WS-UBOC-AUTH-USERID TO UBOC-AUTH-USERID
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockGroupPessi()) {
			// COB_CODE: MOVE WS-UBOC-AUTH-USERID TO UBOC-AUTH-USERID
			wsHallubocCommsArea.getUbocRecord().getCommInfo().setUbocAuthUserid(ws.getWsWorkFields().getUbocAuthUserid());
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0700-LOCK-PROCESSING-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0700-LOCK-PROCESSING-X
			return;
		}
	}

	/**Original name: 0720-INIT-LEGACY-LINKAGE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  SET UP REMAINING LINKAGE FOR LEGACY LOCK MODULE.               *
	 *                                                                 *
	 * *****************************************************************
	 * * MOVE LOCK DRIVER INFO TO LEGACY LOCK COPYBOOK.</pre>*/
	private void initLegacyLinkage() {
		// COB_CODE: MOVE HALRLODR-TCH-KEY        TO ULLI-TCH-KEY.
		ws.getHallulli().setTchKey(wsHalrlodrLinkage.getTchKey());
		// COB_CODE: MOVE HALRLODR-APP-ID         TO ULLI-APP-ID.
		ws.getHallulli().setAppId(wsHalrlodrLinkage.getAppId());
		// COB_CODE: MOVE HALRLODR-TABLE-NM       TO ULLI-TABLE-NM.
		ws.getHallulli().setTableNm(wsHalrlodrLinkage.getTableNm());
		// COB_CODE: MOVE HALRLODR-BUS-OBJ-NM     TO ULLI-BUS-OBJ-NM.
		ws.getHallulli().setBusObjNm(wsHalrlodrLinkage.getBusObjNm());
		//* SET ADDITTIONAL LEGACY LOCK INFO.
		//* FUNCTION ALREADY SET BY CALLING SECTION.
		// COB_CODE: MOVE WS-EXPIRY-TS            TO ULLI-EXPIRATION-TS.
		ws.getHallulli().setExpirationTs(ws.getWsWorkFields().getExpiryTs());
		// COB_CODE: SET ULLI-UNKNOWN-OR-ERROR    TO TRUE.
		ws.getHallulli().getLegacyLockResp().setUnknownOrError();
		// COB_CODE: MOVE SPACES                  TO ULLI-LEGACY-LOCK-USERID.
		ws.getHallulli().setLegacyLockUserid("");
	}

	/**Original name: 0730-GET-LEGACY-LOCK-MODULE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DETERMINE THE LEGACY LOCK MODULE ASSOCIATED WITH THIS
	 *  APPLICATION.
	 * *****************************************************************</pre>*/
	private void getLegacyLockModule() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE HALRLODR-APP-ID          TO HUC2H-ENTRY-KEY-CD.
		ws.getDclgenHalUniversalCt2().setEntryKeyCd(wsHalrlodrLinkage.getAppId());
		// COB_CODE: MOVE WS-UCT2-LEGACY-LOCK-VIEW TO HUC2H-TBL-LBL-TXT.
		ws.getDclgenHalUniversalCt2().setTblLblTxt(ws.getWsWorkFields().getUct2LegacyLockView());
		//* ONLY ONE ROW PER APPLICATION IS VALID FOR THE LEGACY LOCK VIEW
		// COB_CODE: EXEC SQL
		//               SELECT HUC2_ENTRY_DTA_TXT
		//                 INTO :HUC2H-ENTRY-DTA-TXT
		//                 FROM HAL_UNIVERSAL_CT_V
		//                 WHERE HUC2_TBL_LBL_TXT  = :HUC2H-TBL-LBL-TXT
		//                   AND HUC2_ENTRY_KEY_CD = :HUC2H-ENTRY-KEY-CD
		//           END-EXEC.
		ws.getDclgenHalUniversalCt2().setEntryDtaTxt(halUniversalCtVDao.selectRec1(ws.getDclgenHalUniversalCt2().getTblLblTxt(),
				ws.getDclgenHalUniversalCt2().getEntryKeyCd(), ws.getDclgenHalUniversalCt2().getEntryDtaTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                         CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           **17238       IF UBOC-UOW-LOCK-PESSIMISTIC
		//           **17238         OR (UBOC-UOW-LOCK-MIXED
		//           **17238             AND HBLT-LOK-TYPE-CD = 'P')
		//                        END-IF
		//                    WHEN OTHER
		//                         GO TO 0730-GET-LEGACY-LOCK-MODULE-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://*17238       IF UBOC-UOW-LOCK-PESSIMISTIC
			//*17238         OR (UBOC-UOW-LOCK-MIXED
			//*17238             AND HBLT-LOK-TYPE-CD = 'P')
			// COB_CODE:  IF HALRLOMG-LOCK-PES-READUPDT
			//              OR HALRLOMG-LOCK-GROUP-PESSI
			//                GO TO 0730-GET-LEGACY-LOCK-MODULE-X
			//           ELSE
			//                GO TO 0730-GET-LEGACY-LOCK-MODULE-X
			//           END-IF
			if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt() || ws.getWsHalrlomgLinkage().getLokTypeCd().isLockGroupPessi()) {
				// COB_CODE: SET WS-LOG-ERROR         TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
				// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
				// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
				// COB_CODE: MOVE 'HAL_UNIV_CTL2_V' TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UNIV_CTL2_V");
				// COB_CODE: MOVE '0730-GET-LEGACY-LOCK-MODULE'
				//             TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0730-GET-LEGACY-LOCK-MODULE");
				// COB_CODE: MOVE 'LEGACY LOCK MODULE NOT FOUND'
				//             TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LEGACY LOCK MODULE NOT FOUND");
				// COB_CODE: STRING 'HALRLODR-APP-ID='
				//                  HALRLODR-APP-ID  ';'
				//                  'LOGICAL LABEL='
				//                  HUC2H-TBL-LBL-TXT
				//             DELIMITED BY SIZE
				//             INTO EFAL-OBJ-DATA-KEY
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HALRLODR-APP-ID=", wsHalrlodrLinkage.getAppIdFormatted(),
						";", "LOGICAL LABEL=", ws.getDclgenHalUniversalCt2().getTblLblTxtFormatted());
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 0730-GET-LEGACY-LOCK-MODULE-X
				return;
			} else {
				// COB_CODE: SET NO-LEGACY-LOCK-MODULE TO TRUE
				ws.getWsSwitches().getLegacyLockModSw().setNoLegacyLockModule();
				// COB_CODE: MOVE SPACES TO WS-LEGACY-LOCK-MODULE
				ws.getWsWorkFields().setLegacyLockModule("");
				// COB_CODE: GO TO 0730-GET-LEGACY-LOCK-MODULE-X
				return;
			}

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_UNIV_CTL2_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UNIV_CTL2_V");
			// COB_CODE: MOVE '0730-GET-LEGACY-LOCK-MODULE'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0730-GET-LEGACY-LOCK-MODULE");
			// COB_CODE: MOVE 'SELECT OF LEGACY LOCK MODULE FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT OF LEGACY LOCK MODULE FAILED");
			// COB_CODE: STRING 'HALRLODR-APP-ID='
			//                   HALRLODR-APP-ID  ';'
			//                  'LOGICAL LABEL='
			//                   HUC2H-TBL-LBL-TXT
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HALRLODR-APP-ID=", wsHalrlodrLinkage.getAppIdFormatted(),
					";", "LOGICAL LABEL=", ws.getDclgenHalUniversalCt2().getTblLblTxtFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0730-GET-LEGACY-LOCK-MODULE-X
			return;
		}
		// RETRIEVE MODULE FROM XRF TABLE
		// COB_CODE: MOVE HUC2H-ENTRY-DTA-TXT TO HBMX-BUS-OBJ-NM.
		ws.getDclhalBoMduXrfV().setBusObjNm(ws.getDclgenHalUniversalCt2().getEntryDtaTxt());
		// COB_CODE: EXEC SQL
		//               SELECT HBMX_BOBJ_MDU_NM
		//                 INTO  :HBMX-BOBJ-MDU-NM
		//                 FROM   HAL_BO_MDU_XRF_V
		//               WHERE BUS_OBJ_NM = :HBMX-BUS-OBJ-NM
		//           END-EXEC.
		ws.getDclhalBoMduXrfV()
				.setBobjMduNm(halBoMduXrfVDao.selectByHbmxBusObjNm(ws.getDclhalBoMduXrfV().getBusObjNm(), ws.getDclhalBoMduXrfV().getBobjMduNm()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0730-GET-LEGACY-LOCK-MODULE-X
		//               WHEN OTHER
		//                   GO TO 0730-GET-LEGACY-LOCK-MODULE-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-LOG-ERROR                             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF");
			// COB_CODE: MOVE '0730-GET-LEGACY-LOCK-MODULE'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0730-GET-LEGACY-LOCK-MODULE");
			// COB_CODE: MOVE 'EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0730-GET-LEGACY-LOCK-MODULE-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF");
			// COB_CODE: MOVE '0730-GET-LEGACY-LOCK-MODULE'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0730-GET-LEGACY-LOCK-MODULE");
			// COB_CODE: MOVE 'SELECT FROM OBJ XREF TABLE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT FROM OBJ XREF TABLE FAILED");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0730-GET-LEGACY-LOCK-MODULE-X
			return;
		}
		//* CHECK MODULE NAME PRESENT IN OBJ XREF ROW RETURNED
		// COB_CODE: IF HBMX-BOBJ-MDU-NM = SPACES
		//               GO TO 0730-GET-LEGACY-LOCK-MODULE-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclhalBoMduXrfV().getBobjMduNm())) {
			// COB_CODE: SET WS-LOG-ERROR                              TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE    OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED   OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUIRED-FIELD-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequiredFieldBlank();
			// COB_CODE: MOVE '0730-GET-LEGACY-LOCK-MODULE'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0730-GET-LEGACY-LOCK-MODULE");
			// COB_CODE: MOVE 'MODULE NAME ON OBJ XREF ROW IS BLANK'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MODULE NAME ON OBJ XREF ROW IS BLANK");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//                  'HBMX-BOBJ-MDU-NM=' HBMX-BOBJ-MDU-NM ';'
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";", "HBMX-BOBJ-MDU-NM=", ws.getDclhalBoMduXrfV().getBobjMduNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0730-GET-LEGACY-LOCK-MODULE-X
			return;
		}
		// COB_CODE: MOVE HBMX-BOBJ-MDU-NM TO WS-LEGACY-LOCK-MODULE.
		ws.getWsWorkFields().setLegacyLockModule(ws.getDclhalBoMduXrfV().getBobjMduNm());
		// COB_CODE: SET LEGACY-LOCK-MODULE-FOUND TO TRUE.
		ws.getWsSwitches().getLegacyLockModSw().setLegacyLockModuleFound();
	}

	/**Original name: 0800-CALL-LEGACY-LOCKING_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LINK TO THE SPECIFIED LEGACY LOCK INTERFACE UTILITY
	 * *****************************************************************</pre>*/
	private void callLegacyLocking() {
		// COB_CODE: MOVE LENGTH OF WS-HALLULLI-LINKAGE
		//             TO UBOC-APP-DATA-BUFFER-LENGTH.
		wsHallubocCommsArea.getUbocRecord().setAppDataBufferLength(((short) HalrlodrData.Len.WS_HALLULLI_LINKAGE));
		// COB_CODE: MOVE WS-HALLULLI-LINKAGE TO UBOC-APP-DATA-BUFFER.
		wsHallubocCommsArea.getUbocRecord().setAppDataBuffer(ws.getWsHallulliLinkageFormatted());
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM   (WS-LEGACY-LOCK-MODULE)
		//                COMMAREA  (UBOC-RECORD)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALRLODR", execContext).commarea(wsHallubocCommsArea.getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD)
				.link(ws.getWsWorkFields().getLegacyLockModule());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    CONTINUE
		//               WHEN OTHER
		//                    GO TO 0800-CALL-LEGACY-LOCKING-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK           TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE WS-LEGACY-LOCK-MODULE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsWorkFields().getLegacyLockModule());
			// COB_CODE: MOVE '0800-CALL-LEGACY-LOCKING'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0800-CALL-LEGACY-LOCKING");
			// COB_CODE: MOVE 'LINK TO LEGACY LOCK MODULE FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO LEGACY LOCK MODULE FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0800-CALL-LEGACY-LOCKING-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0800-CALL-LEGACY-LOCKING-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0800-CALL-LEGACY-LOCKING-X
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER TO WS-HALLULLI-LINKAGE.
		ws.setWsHallulliLinkageFormatted(wsHallubocCommsArea.getUbocRecord().getAppDataBufferFormatted());
	}

	/**Original name: 0900-DETERMINE-LOCK-TYPE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  BASED ON THE LOCK TYPE FOR THE UOW, SET THE VALID LOCK TYPE.
	 * *****************************************************************
	 * * CHECK LOCK STRATEGY CODE</pre>*/
	private void determineLockType() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-LOCK-OPTIMISTIC
		//                   SET HALRLOMG-LOCK-OPTIMISTIC TO TRUE
		//               WHEN UBOC-UOW-NO-LOCK-NEEDED
		//                   SET HALRLOMG-NO-LOCK-NEEDED TO TRUE
		//               WHEN UBOC-UOW-LOCK-PESSIMISTIC
		//                   SET HALRLOMG-LOCK-PES-READUPDT TO TRUE
		//               WHEN UBOC-UOW-LOCK-GROUP-PESSI
		//                   SET HALRLOMG-LOCK-GROUP-PESSI TO TRUE
		//               WHEN UBOC-UOW-LOCK-MIXED
		//                   END-EVALUATE
		//               WHEN OTHER
		//                   GO TO 0900-DETERMINE-LOCK-TYPE-X
		//           END-EVALUATE.
		switch (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowLockStrategyCd().getUbocUowLockStrategyCd()) {

		case UbocUowLockStrategyCd.LOCK_OPTIMISTIC:// COB_CODE: SET HALRLOMG-LOCK-OPTIMISTIC TO TRUE
			ws.getWsHalrlomgLinkage().getLokTypeCd().setHalrlomgLockOptimistic();
			break;

		case UbocUowLockStrategyCd.NO_LOCK_NEEDED:// COB_CODE: SET HALRLOMG-NO-LOCK-NEEDED TO TRUE
			ws.getWsHalrlomgLinkage().getLokTypeCd().setHalrlomgNoLockNeeded();
			break;

		case UbocUowLockStrategyCd.LOCK_PESSIMISTIC:// COB_CODE: SET HALRLOMG-LOCK-PES-READUPDT TO TRUE
			ws.getWsHalrlomgLinkage().getLokTypeCd().setHalrlomgLockPesReadupdt();
			break;

		case UbocUowLockStrategyCd.LOCK_GROUP_PESSI:// COB_CODE: SET HALRLOMG-LOCK-GROUP-PESSI TO TRUE
			ws.getWsHalrlomgLinkage().getLokTypeCd().setHalrlomgLockGroupPessi();
			break;

		case UbocUowLockStrategyCd.LOCK_MIXED:// COB_CODE: PERFORM 0905-LOOKUP-LOCK-TYPE
			lookupLockType();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0900-DETERMINE-LOCK-TYPE-X
			//           END-IF
			if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0900-DETERMINE-LOCK-TYPE-X
				return;
			}
			//* CHECK LOCK TYPE CODE RETRIEVED FROM THE TABLE
			// COB_CODE: EVALUATE TRUE
			//               WHEN HBLT-LOK-TYPE-CD    = 'P'
			//                   SET HALRLOMG-LOCK-PES-READUPDT TO TRUE
			//               WHEN HBLT-LOK-TYPE-CD    = 'O'
			//                   SET HALRLOMG-LOCK-OPTIMISTIC TO TRUE
			//               WHEN HBLT-LOK-TYPE-CD    = 'N'
			//                   SET HALRLOMG-NO-LOCK-NEEDED TO TRUE
			//               WHEN HBLT-LOK-TYPE-CD    = 'G'
			//                   SET HALRLOMG-LOCK-GROUP-PESSI TO TRUE
			//               WHEN OTHER
			//                   GO TO 0900-DETERMINE-LOCK-TYPE-X
			//           END-EVALUATE
			if (ws.getDclhalBoLokType().getLokTypeCd() == 'P') {
				// COB_CODE: SET HALRLOMG-LOCK-PES-READUPDT TO TRUE
				ws.getWsHalrlomgLinkage().getLokTypeCd().setHalrlomgLockPesReadupdt();
			} else if (ws.getDclhalBoLokType().getLokTypeCd() == 'O') {
				// COB_CODE: SET HALRLOMG-LOCK-OPTIMISTIC TO TRUE
				ws.getWsHalrlomgLinkage().getLokTypeCd().setHalrlomgLockOptimistic();
			} else if (ws.getDclhalBoLokType().getLokTypeCd() == 'N') {
				// COB_CODE: SET HALRLOMG-NO-LOCK-NEEDED TO TRUE
				ws.getWsHalrlomgLinkage().getLokTypeCd().setHalrlomgNoLockNeeded();
			} else if (ws.getDclhalBoLokType().getLokTypeCd() == 'G') {
				// COB_CODE: SET HALRLOMG-LOCK-GROUP-PESSI TO TRUE
				ws.getWsHalrlomgLinkage().getLokTypeCd().setHalrlomgLockGroupPessi();
			} else {
				// COB_CODE: SET WS-LOG-ERROR            TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
				// COB_CODE: SET BUSP-INV-ACTION-CODE    TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvActionCode();
				// COB_CODE: MOVE 'HALRLODR'
				//             TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALRLODR");
				// COB_CODE: MOVE '0900-DETERMINE-LOCK-TYPE'
				//             TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0900-DETERMINE-LOCK-TYPE");
				// COB_CODE: MOVE 'INVALID TABLE LOCK TYPE'
				//             TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID TABLE LOCK TYPE");
				// COB_CODE: STRING 'HBLT-LOK-TYPE-CD='
				//                   HBLT-LOK-TYPE-CD    ';'
				//                   DELIMITED BY SIZE
				//                   INTO EFAL-OBJ-DATA-KEY
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBLT-LOK-TYPE-CD=",
						String.valueOf(ws.getDclhalBoLokType().getLokTypeCd()), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 0900-DETERMINE-LOCK-TYPE-X
				return;
			}
			break;

		default:// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-ACTION-CODE     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvActionCode();
			// COB_CODE: MOVE 'HALRLODR'
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALRLODR");
			// COB_CODE: MOVE '0900-DETERMINE-LOCK-TYPE' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0900-DETERMINE-LOCK-TYPE");
			// COB_CODE: MOVE 'INVALID STRATEGY CODE' TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID STRATEGY CODE");
			// COB_CODE: STRING 'UBOC-UOW-LOCK-STRATEGY-CD='
			//                   UBOC-UOW-LOCK-STRATEGY-CD ';'
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY
			//                   END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UBOC-UOW-LOCK-STRATEGY-CD=",
					String.valueOf(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowLockStrategyCd().getUbocUowLockStrategyCd()), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0900-DETERMINE-LOCK-TYPE-X
			return;
		}
	}

	/**Original name: 0905-LOOKUP-LOCK-TYPE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  LOOKUP TO THE LOCK TYPE TABLE - HAL_BO_LOK_TYPE                *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void lookupLockType() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-UOW-NAME            TO HBLT-UOW-NM.
		ws.getDclhalBoLokType().setUowNm(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE HALRLODR-BUS-OBJ-NM      TO HBLT-BUS-OBJ-NM.
		ws.getDclhalBoLokType().setBusObjNm(wsHalrlodrLinkage.getBusObjNm());
		// COB_CODE: EXEC SQL
		//              SELECT LOK_TYPE_CD
		//                INTO :HBLT-LOK-TYPE-CD
		//                   FROM HAL_BO_LOK_TYPE_V
		//                  WHERE UOW_NM   = :HBLT-UOW-NM
		//                    AND BUS_OBJ_NM = :HBLT-BUS-OBJ-NM
		//           END-EXEC.
		ws.getDclhalBoLokType().setLokTypeCd(halBoLokTypeVDao.selectRec(ws.getDclhalBoLokType().getUowNm(), ws.getDclhalBoLokType().getBusObjNm(),
				ws.getDclhalBoLokType().getLokTypeCd()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                    MOVE 'P' TO HBLT-LOK-TYPE-CD
		//               WHEN OTHER
		//                    GO TO 0905-LOOKUP-LOCK-TYPE-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: MOVE 'P' TO HBLT-LOK-TYPE-CD
			ws.getDclhalBoLokType().setLokTypeCdFormatted("P");
			break;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_BO_LOK_TYPE_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_LOK_TYPE_V");
			// COB_CODE: MOVE '0905-LOOKUP-LOCK-TYPE'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0905-LOOKUP-LOCK-TYPE");
			// COB_CODE: MOVE 'SELECT FROM HAL_BO_LOK_TYPE TABLE FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT FROM HAL_BO_LOK_TYPE TABLE FAILED");
			// COB_CODE: STRING 'UBOC-UOW-NAME='
			//                   UBOC-UOW-NAME ';'
			//                  'HALRLODR-BUS-OBJ-NM='
			//                   HALRLODR-BUS-OBJ-NM  ';'
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UBOC-UOW-NAME=", wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowNameFormatted(), ";",
							"HALRLODR-BUS-OBJ-NM=", wsHalrlodrLinkage.getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0905-LOOKUP-LOCK-TYPE-X
			return;
		}
	}

	/**Original name: 0910-DETERMINE-LOCK-GROUP_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  IF THE LOCK TYPE FOR THIS UOW IS GROUP-PESSIMISTIC, SEE IF THE
	 *  CURRENT USER IS IN A LOCK GROUP.  IF SO, USE THE LOCK GROUP ID
	 *  FOR LOCKING.  IF NOT, CHANGE THE LOCK TYPE TO PESSIMISTIC AND
	 *  USE THE USER ID FOR LOCKING.
	 * *****************************************************************</pre>*/
	private void determineLockGroup() {
		// COB_CODE: IF HALRLOMG-LOCK-GROUP-PESSI
		//             OR HALRLOMG-LOCK-PES-READUPDT
		//               CONTINUE
		//           ELSE
		//               GO TO 0910-DETERMINE-LOCK-GROUP-X
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockGroupPessi() || ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 0910-DETERMINE-LOCK-GROUP-X
			return;
		}
		// COB_CODE: IF UBOC-SINGLE-LOCK-USER OR UBOC-GROUP-LOCK-USER
		//               GO TO 0910-DETERMINE-LOCK-GROUP-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUserInLockGrpSw().isUbocSingleLockUser()
				|| wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUserInLockGrpSw().isUbocGroupLockUser()) {
			// COB_CODE: GO TO 0910-DETERMINE-LOCK-GROUP-X
			return;
		}
		// COB_CODE: INITIALIZE HHSPH-HAL-SEC-PROFILE-ROW.
		initHhsphHalSecProfileRow();
		// COB_CODE: MOVE UBOC-AUTH-USERID         TO HHSPH-HSP-ID-KEY.
		ws.getHhsphHalSecProfileRow().setHspIdKey(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: MOVE WS-LOCK-GROUP-LABEL      TO HHSPH-SEC-PROFILE-TYP.
		ws.getHhsphHalSecProfileRow().setSecProfileTyp(ws.getWsWorkFields().getLockGroupLabel());
		// COB_CODE: EXEC SQL
		//              SELECT HSP_DTA_TXT
		//                INTO :HHSPH-HSP-DTA-TXT
		//                   FROM HAL_SEC_PROFILE_V
		//                  WHERE HSP_ID_KEY      = :HHSPH-HSP-ID-KEY
		//                    AND SEC_PROFILE_TYP = :HHSPH-SEC-PROFILE-TYP
		//                    AND EFFECTIVE_DT    =
		//                     (SELECT MAX(EFFECTIVE_DT)
		//                      FROM HAL_SEC_PROFILE_V
		//                      WHERE HSP_ID_KEY     = :HHSPH-HSP-ID-KEY
		//                       AND SEC_PROFILE_TYP = :HHSPH-SEC-PROFILE-TYP
		//                       AND EFFECTIVE_DT <= CURRENT DATE
		//                       AND EXPIRATION_DT > CURRENT DATE)
		//           END-EXEC.
		ws.getHhsphHalSecProfileRow().setHspDtaTxt(halSecProfileVDao.selectRec(ws.getHhsphHalSecProfileRow().getHspIdKey(),
				ws.getHhsphHalSecProfileRow().getSecProfileTyp(), ws.getHhsphHalSecProfileRow().getHspDtaTxt()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    SET UBOC-GROUP-LOCK-USER TO TRUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                    SET HALRLOMG-LOCK-PES-READUPDT TO TRUE
		//               WHEN OTHER
		//                    GO TO 0910-DETERMINE-LOCK-GROUP-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: MOVE HHSPH-HSP-DTA-TXT TO UBOC-LOCK-GROUP
			wsHallubocCommsArea.getUbocRecord().getCommInfo().setUbocLockGroup(ws.getHhsphHalSecProfileRow().getHspDtaTxt());
			// COB_CODE: SET UBOC-GROUP-LOCK-USER TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUserInLockGrpSw().setUbocGroupLockUser();
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET UBOC-UOW-LOCK-PESSIMISTIC TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowLockStrategyCd().setUbocUowLockPessimistic();
			// COB_CODE: SET UBOC-SINGLE-LOCK-USER TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUserInLockGrpSw().setUbocSingleLockUser();
			// COB_CODE: SET HALRLOMG-LOCK-PES-READUPDT TO TRUE
			ws.getWsHalrlomgLinkage().getLokTypeCd().setHalrlomgLockPesReadupdt();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_SEC_PROFILE_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_SEC_PROFILE_V");
			// COB_CODE: MOVE '0910-DETERMINE-LOCK-GROUP'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0910-DETERMINE-LOCK-GROUP");
			// COB_CODE: MOVE 'RETRIEVAL OF LOCK GROUP FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("RETRIEVAL OF LOCK GROUP FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'USERID='
			//                   UBOC-AUTH-USERID ';'
			//                  ' SECURITY PROFILE='
			//                   WS-LOCK-GROUP-LABEL ';'
			//                   DELIMITED BY SIZE
			//                   INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("USERID=")
							.append(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocAuthUseridFormatted()).append(";")
							.append(" SECURITY PROFILE=").append(ws.getWsWorkFields().getLockGroupLabelFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0910-DETERMINE-LOCK-GROUP-X
			return;
		}
	}

	/**Original name: 0920-DETERMINE-UPGRADE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SEE IF THE LOCK TYPE FOR THIS LOCK CAN BE SWITCHED FROM GROUP  *
	 *  TO AN INDIVIDUAL PESSIMISTIC.                                  *
	 * *****************************************************************</pre>*/
	private void determineUpgrade() {
		// COB_CODE: SET WS-UPGRADE-INVALID TO TRUE.
		ws.getWsSwitches().getUpgradeValiditySw().setInvalid();
		//* IF THEY WANT A PESSI, THAT MEANS THAT THERE IS EITHER A GROUP
		//* OR ANOTHER USER'S PESSI IN FORCE AGAINST THE DATA.
		//* IF THE USER IS IN A LOCK GROUP, SEE IF IT IS THE GROUP THAT
		//* CURRENTLY OWNS THE LOCK FOR THE DATA.  IF SO, THEN CONVERT THE
		//* LOCK FROM GROUP TO PESSI FOR THE USER BY SETTING THE MISMATCH
		//* FLAG.
		// COB_CODE: IF HALRLOMG-LOCK-PES-READUPDT
		//               END-IF
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockPesReadupdt()) {
			// COB_CODE: IF UBOC-GROUP-LOCK-USER
			//               END-IF
			//           END-IF
			if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUserInLockGrpSw().isUbocGroupLockUser()) {
				// COB_CODE: IF UBOC-LOCK-GROUP = ULLI-LEGACY-LOCK-USERID
				//               GO TO 0920-DETERMINE-UPGRADE-X
				//           END-IF
				if (Conditions.eq(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocLockGroup(), ws.getHallulli().getLegacyLockUserid())) {
					// COB_CODE: SET WS-UPGRADE-VALID TO TRUE
					ws.getWsSwitches().getUpgradeValiditySw().setValid();
					// COB_CODE: GO TO 0920-DETERMINE-UPGRADE-X
					return;
				}
			}
		}
		//* IF THE USER WANTS A GROUP LOCK, EITHER THERE IS AN EXISTING
		//* LOCK FOR THIS USER, ANOTHER USER, OR ANOTHER GROUP. IF THE
		//* EXISTING LOCK IS A PESSI FOR THIS USER, THEN ALLOW THE
		//* CONVERSION OF THE LOCK FROM PESSI TO GROUP.
		// COB_CODE: IF HALRLOMG-LOCK-GROUP-PESSI
		//               END-IF
		//           END-IF.
		if (ws.getWsHalrlomgLinkage().getLokTypeCd().isLockGroupPessi()) {
			// COB_CODE: IF UBOC-AUTH-USERID = ULLI-LEGACY-LOCK-USERID
			//               GO TO 0920-DETERMINE-UPGRADE-X
			//           END-IF
			if (Conditions.eq(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocAuthUserid(), ws.getHallulli().getLegacyLockUserid())) {
				// COB_CODE: SET WS-UPGRADE-VALID TO TRUE
				ws.getWsSwitches().getUpgradeValiditySw().setValid();
				// COB_CODE: GO TO 0920-DETERMINE-UPGRADE-X
				return;
			}
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning()
				&& wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError() && !wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails()
						.getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsWorkFields().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsWorkFields().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALRLODR", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw()
					.setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw()
					.setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			wsHallubocCommsArea.getUbocRecord().setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			wsHallubocCommsArea.getUbocRecord().setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWsWorkFields().getProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		wsHallubocCommsArea.getUbocRecord().getCommInfo()
				.setUbocNbrNonlogBlErrs(Trunc.toInt(1 + wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWsWorkFields().getProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		wsHallubocCommsArea.getUbocRecord().getCommInfo()
				.setUbocNbrWarnings(Trunc.toInt(1 + wsHallubocCommsArea.getUbocRecord().getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWsWorkFields().getApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	public void initWsHalrlomgLinkage() {
		ws.getWsHalrlomgLinkage().getFunction().setFunction("");
		ws.getWsHalrlomgLinkage().setTchKey("");
		ws.getWsHalrlomgLinkage().setAppId("");
		ws.getWsHalrlomgLinkage().setTableNm("");
		ws.getWsHalrlomgLinkage().getLokTypeCd().setLokTypeCd(Types.SPACE_CHAR);
		ws.getWsHalrlomgLinkage().setTsqName("");
		ws.getWsHalrlomgLinkage().getSuccessInd().setSuccessInd(Types.SPACE_CHAR);
		ws.getWsHalrlomgLinkage().setZappedByUserid("");
		ws.getWsHalrlomgLinkage().getGrpLokClrSw().setGrpLokClrSw(Types.SPACE_CHAR);
	}

	public void initUbocExtraData() {
		wsHallubocCommsArea.getUbocRecord().setAppDataBufferLengthFormatted("0000");
		wsHallubocCommsArea.getUbocRecord().setAppDataBuffer("");
	}

	public void initHhsphHalSecProfileRow() {
		ws.getHhsphHalSecProfileRow().setHspIdKey("");
		ws.getHhsphHalSecProfileRow().setSecProfileTyp("");
		ws.getHhsphHalSecProfileRow().setSeqNbr(((short) 0));
		ws.getHhsphHalSecProfileRow().setHspGenTxtFld1("");
		ws.getHhsphHalSecProfileRow().setHspGenTxtFld2("");
		ws.getHhsphHalSecProfileRow().setHspGenTxtFld3("");
		ws.getHhsphHalSecProfileRow().setHspGenTxtFld4("");
		ws.getHhsphHalSecProfileRow().setHspGenTxtFld5("");
		ws.getHhsphHalSecProfileRow().setEffectiveDt("");
		ws.getHhsphHalSecProfileRow().setExpirationDt("");
		ws.getHhsphHalSecProfileRow().setHspDtaTxt("");
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
