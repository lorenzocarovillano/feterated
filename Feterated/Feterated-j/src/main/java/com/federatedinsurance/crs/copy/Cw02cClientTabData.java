/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW02C-CLIENT-TAB-DATA<br>
 * Variable: CW02C-CLIENT-TAB-DATA from copybook CAWLC002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw02cClientTabData {

	//==== PROPERTIES ====
	/**Original name: CW02C-CICL-PRI-SUB-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char ciclPriSubCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-PRI-SUB-CD
	private String ciclPriSubCd = DefaultValues.stringVal(Len.CICL_PRI_SUB_CD);
	//Original name: CW02C-CICL-FST-NM-CI
	private char ciclFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-FST-NM
	private String ciclFstNm = DefaultValues.stringVal(Len.CICL_FST_NM);
	//Original name: CW02C-CICL-LST-NM-CI
	private char ciclLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-LST-NM
	private String ciclLstNm = DefaultValues.stringVal(Len.CICL_LST_NM);
	//Original name: CW02C-CICL-MDL-NM-CI
	private char ciclMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-MDL-NM
	private String ciclMdlNm = DefaultValues.stringVal(Len.CICL_MDL_NM);
	//Original name: CW02C-NM-PFX-CI
	private char nmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-NM-PFX
	private String nmPfx = DefaultValues.stringVal(Len.NM_PFX);
	//Original name: CW02C-NM-SFX-CI
	private char nmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-NM-SFX
	private String nmSfx = DefaultValues.stringVal(Len.NM_SFX);
	//Original name: CW02C-PRIMARY-PRO-DSN-CD-CI
	private char primaryProDsnCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-PRIMARY-PRO-DSN-CD-NI
	private char primaryProDsnCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-PRIMARY-PRO-DSN-CD
	private String primaryProDsnCd = DefaultValues.stringVal(Len.PRIMARY_PRO_DSN_CD);
	//Original name: CW02C-LEG-ENT-CD-CI
	private char legEntCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-LEG-ENT-CD
	private String legEntCd = DefaultValues.stringVal(Len.LEG_ENT_CD);
	//Original name: CW02C-CICL-SDX-CD-CI
	private char ciclSdxCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-SDX-CD
	private String ciclSdxCd = DefaultValues.stringVal(Len.CICL_SDX_CD);
	//Original name: CW02C-CICL-OGN-INCEPT-DT-CI
	private char ciclOgnInceptDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-OGN-INCEPT-DT-NI
	private char ciclOgnInceptDtNi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-OGN-INCEPT-DT
	private String ciclOgnInceptDt = DefaultValues.stringVal(Len.CICL_OGN_INCEPT_DT);
	//Original name: CW02C-CICL-ADD-NM-IND-CI
	private char ciclAddNmIndCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-ADD-NM-IND-NI
	private char ciclAddNmIndNi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-ADD-NM-IND
	private char ciclAddNmInd = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-DOB-DT-CI
	private char ciclDobDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-DOB-DT
	private String ciclDobDt = DefaultValues.stringVal(Len.CICL_DOB_DT);
	//Original name: CW02C-CICL-BIR-ST-CD-CI
	private char ciclBirStCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-BIR-ST-CD
	private String ciclBirStCd = DefaultValues.stringVal(Len.CICL_BIR_ST_CD);
	//Original name: CW02C-GENDER-CD-CI
	private char genderCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-GENDER-CD
	private char genderCd = DefaultValues.CHAR_VAL;
	//Original name: CW02C-PRI-LGG-CD-CI
	private char priLggCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-PRI-LGG-CD
	private String priLggCd = DefaultValues.stringVal(Len.PRI_LGG_CD);
	//Original name: CW02C-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW02C-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW02C-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW02C-CICL-EXP-DT-CI
	private char ciclExpDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-EXP-DT
	private String ciclExpDt = DefaultValues.stringVal(Len.CICL_EXP_DT);
	//Original name: CW02C-CICL-EFF-ACY-TS-CI
	private char ciclEffAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-EFF-ACY-TS
	private String ciclEffAcyTs = DefaultValues.stringVal(Len.CICL_EFF_ACY_TS);
	//Original name: CW02C-CICL-EXP-ACY-TS-CI
	private char ciclExpAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-EXP-ACY-TS
	private String ciclExpAcyTs = DefaultValues.stringVal(Len.CICL_EXP_ACY_TS);
	//Original name: CW02C-STATUTORY-TLE-CD-CI
	private char statutoryTleCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-STATUTORY-TLE-CD
	private String statutoryTleCd = DefaultValues.stringVal(Len.STATUTORY_TLE_CD);
	//Original name: CW02C-CICL-LNG-NM-CI
	private char ciclLngNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-LNG-NM-NI
	private char ciclLngNmNi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-LNG-NM
	private String ciclLngNm = DefaultValues.stringVal(Len.CICL_LNG_NM);
	//Original name: CW02C-LO-LST-NM-MCH-CD-CI
	private char loLstNmMchCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-LO-LST-NM-MCH-CD-NI
	private char loLstNmMchCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-LO-LST-NM-MCH-CD
	private String loLstNmMchCd = DefaultValues.stringVal(Len.LO_LST_NM_MCH_CD);
	//Original name: CW02C-HI-LST-NM-MCH-CD-CI
	private char hiLstNmMchCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-HI-LST-NM-MCH-CD-NI
	private char hiLstNmMchCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-HI-LST-NM-MCH-CD
	private String hiLstNmMchCd = DefaultValues.stringVal(Len.HI_LST_NM_MCH_CD);
	//Original name: CW02C-LO-FST-NM-MCH-CD-CI
	private char loFstNmMchCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-LO-FST-NM-MCH-CD-NI
	private char loFstNmMchCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-LO-FST-NM-MCH-CD
	private String loFstNmMchCd = DefaultValues.stringVal(Len.LO_FST_NM_MCH_CD);
	//Original name: CW02C-HI-FST-NM-MCH-CD-CI
	private char hiFstNmMchCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-HI-FST-NM-MCH-CD-NI
	private char hiFstNmMchCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-HI-FST-NM-MCH-CD
	private String hiFstNmMchCd = DefaultValues.stringVal(Len.HI_FST_NM_MCH_CD);
	//Original name: CW02C-CICL-ACQ-SRC-CD-CI
	private char ciclAcqSrcCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-ACQ-SRC-CD-NI
	private char ciclAcqSrcCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02C-CICL-ACQ-SRC-CD
	private String ciclAcqSrcCd = DefaultValues.stringVal(Len.CICL_ACQ_SRC_CD);

	//==== METHODS ====
	public void setClientTabDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ciclPriSubCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclPriSubCd = MarshalByte.readString(buffer, position, Len.CICL_PRI_SUB_CD);
		position += Len.CICL_PRI_SUB_CD;
		ciclFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclFstNm = MarshalByte.readString(buffer, position, Len.CICL_FST_NM);
		position += Len.CICL_FST_NM;
		ciclLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclLstNm = MarshalByte.readString(buffer, position, Len.CICL_LST_NM);
		position += Len.CICL_LST_NM;
		ciclMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclMdlNm = MarshalByte.readString(buffer, position, Len.CICL_MDL_NM);
		position += Len.CICL_MDL_NM;
		nmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		nmPfx = MarshalByte.readString(buffer, position, Len.NM_PFX);
		position += Len.NM_PFX;
		nmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		nmSfx = MarshalByte.readString(buffer, position, Len.NM_SFX);
		position += Len.NM_SFX;
		primaryProDsnCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		primaryProDsnCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		primaryProDsnCd = MarshalByte.readString(buffer, position, Len.PRIMARY_PRO_DSN_CD);
		position += Len.PRIMARY_PRO_DSN_CD;
		legEntCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		legEntCd = MarshalByte.readString(buffer, position, Len.LEG_ENT_CD);
		position += Len.LEG_ENT_CD;
		ciclSdxCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclSdxCd = MarshalByte.readString(buffer, position, Len.CICL_SDX_CD);
		position += Len.CICL_SDX_CD;
		ciclOgnInceptDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclOgnInceptDtNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclOgnInceptDt = MarshalByte.readString(buffer, position, Len.CICL_OGN_INCEPT_DT);
		position += Len.CICL_OGN_INCEPT_DT;
		ciclAddNmIndCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclAddNmIndNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclAddNmInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclDobDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclDobDt = MarshalByte.readString(buffer, position, Len.CICL_DOB_DT);
		position += Len.CICL_DOB_DT;
		ciclBirStCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclBirStCd = MarshalByte.readString(buffer, position, Len.CICL_BIR_ST_CD);
		position += Len.CICL_BIR_ST_CD;
		genderCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		genderCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		priLggCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		priLggCd = MarshalByte.readString(buffer, position, Len.PRI_LGG_CD);
		position += Len.PRI_LGG_CD;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		ciclExpDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclExpDt = MarshalByte.readString(buffer, position, Len.CICL_EXP_DT);
		position += Len.CICL_EXP_DT;
		ciclEffAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclEffAcyTs = MarshalByte.readString(buffer, position, Len.CICL_EFF_ACY_TS);
		position += Len.CICL_EFF_ACY_TS;
		ciclExpAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclExpAcyTs = MarshalByte.readString(buffer, position, Len.CICL_EXP_ACY_TS);
		position += Len.CICL_EXP_ACY_TS;
		statutoryTleCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statutoryTleCd = MarshalByte.readString(buffer, position, Len.STATUTORY_TLE_CD);
		position += Len.STATUTORY_TLE_CD;
		ciclLngNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclLngNmNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclLngNm = MarshalByte.readString(buffer, position, Len.CICL_LNG_NM);
		position += Len.CICL_LNG_NM;
		loLstNmMchCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		loLstNmMchCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		loLstNmMchCd = MarshalByte.readString(buffer, position, Len.LO_LST_NM_MCH_CD);
		position += Len.LO_LST_NM_MCH_CD;
		hiLstNmMchCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hiLstNmMchCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hiLstNmMchCd = MarshalByte.readString(buffer, position, Len.HI_LST_NM_MCH_CD);
		position += Len.HI_LST_NM_MCH_CD;
		loFstNmMchCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		loFstNmMchCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		loFstNmMchCd = MarshalByte.readString(buffer, position, Len.LO_FST_NM_MCH_CD);
		position += Len.LO_FST_NM_MCH_CD;
		hiFstNmMchCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hiFstNmMchCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hiFstNmMchCd = MarshalByte.readString(buffer, position, Len.HI_FST_NM_MCH_CD);
		position += Len.HI_FST_NM_MCH_CD;
		ciclAcqSrcCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclAcqSrcCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclAcqSrcCd = MarshalByte.readString(buffer, position, Len.CICL_ACQ_SRC_CD);
	}

	public byte[] getClientTabDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, ciclPriSubCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclPriSubCd, Len.CICL_PRI_SUB_CD);
		position += Len.CICL_PRI_SUB_CD;
		MarshalByte.writeChar(buffer, position, ciclFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclFstNm, Len.CICL_FST_NM);
		position += Len.CICL_FST_NM;
		MarshalByte.writeChar(buffer, position, ciclLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclLstNm, Len.CICL_LST_NM);
		position += Len.CICL_LST_NM;
		MarshalByte.writeChar(buffer, position, ciclMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclMdlNm, Len.CICL_MDL_NM);
		position += Len.CICL_MDL_NM;
		MarshalByte.writeChar(buffer, position, nmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nmPfx, Len.NM_PFX);
		position += Len.NM_PFX;
		MarshalByte.writeChar(buffer, position, nmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nmSfx, Len.NM_SFX);
		position += Len.NM_SFX;
		MarshalByte.writeChar(buffer, position, primaryProDsnCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, primaryProDsnCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, primaryProDsnCd, Len.PRIMARY_PRO_DSN_CD);
		position += Len.PRIMARY_PRO_DSN_CD;
		MarshalByte.writeChar(buffer, position, legEntCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, legEntCd, Len.LEG_ENT_CD);
		position += Len.LEG_ENT_CD;
		MarshalByte.writeChar(buffer, position, ciclSdxCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclSdxCd, Len.CICL_SDX_CD);
		position += Len.CICL_SDX_CD;
		MarshalByte.writeChar(buffer, position, ciclOgnInceptDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclOgnInceptDtNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclOgnInceptDt, Len.CICL_OGN_INCEPT_DT);
		position += Len.CICL_OGN_INCEPT_DT;
		MarshalByte.writeChar(buffer, position, ciclAddNmIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclAddNmIndNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclAddNmInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclDobDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclDobDt, Len.CICL_DOB_DT);
		position += Len.CICL_DOB_DT;
		MarshalByte.writeChar(buffer, position, ciclBirStCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclBirStCd, Len.CICL_BIR_ST_CD);
		position += Len.CICL_BIR_ST_CD;
		MarshalByte.writeChar(buffer, position, genderCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, genderCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, priLggCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, priLggCd, Len.PRI_LGG_CD);
		position += Len.PRI_LGG_CD;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, ciclExpDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclExpDt, Len.CICL_EXP_DT);
		position += Len.CICL_EXP_DT;
		MarshalByte.writeChar(buffer, position, ciclEffAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclEffAcyTs, Len.CICL_EFF_ACY_TS);
		position += Len.CICL_EFF_ACY_TS;
		MarshalByte.writeChar(buffer, position, ciclExpAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclExpAcyTs, Len.CICL_EXP_ACY_TS);
		position += Len.CICL_EXP_ACY_TS;
		MarshalByte.writeChar(buffer, position, statutoryTleCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, statutoryTleCd, Len.STATUTORY_TLE_CD);
		position += Len.STATUTORY_TLE_CD;
		MarshalByte.writeChar(buffer, position, ciclLngNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclLngNmNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclLngNm, Len.CICL_LNG_NM);
		position += Len.CICL_LNG_NM;
		MarshalByte.writeChar(buffer, position, loLstNmMchCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, loLstNmMchCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, loLstNmMchCd, Len.LO_LST_NM_MCH_CD);
		position += Len.LO_LST_NM_MCH_CD;
		MarshalByte.writeChar(buffer, position, hiLstNmMchCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hiLstNmMchCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hiLstNmMchCd, Len.HI_LST_NM_MCH_CD);
		position += Len.HI_LST_NM_MCH_CD;
		MarshalByte.writeChar(buffer, position, loFstNmMchCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, loFstNmMchCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, loFstNmMchCd, Len.LO_FST_NM_MCH_CD);
		position += Len.LO_FST_NM_MCH_CD;
		MarshalByte.writeChar(buffer, position, hiFstNmMchCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hiFstNmMchCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hiFstNmMchCd, Len.HI_FST_NM_MCH_CD);
		position += Len.HI_FST_NM_MCH_CD;
		MarshalByte.writeChar(buffer, position, ciclAcqSrcCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclAcqSrcCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclAcqSrcCd, Len.CICL_ACQ_SRC_CD);
		return buffer;
	}

	public void setCw02cCiclPriSubCdCi(char cw02cCiclPriSubCdCi) {
		this.ciclPriSubCdCi = cw02cCiclPriSubCdCi;
	}

	public char getCw02cCiclPriSubCdCi() {
		return this.ciclPriSubCdCi;
	}

	public void setCw02cCiclPriSubCd(String cw02cCiclPriSubCd) {
		this.ciclPriSubCd = Functions.subString(cw02cCiclPriSubCd, Len.CICL_PRI_SUB_CD);
	}

	public String getCw02cCiclPriSubCd() {
		return this.ciclPriSubCd;
	}

	public void setCw02cCiclFstNmCi(char cw02cCiclFstNmCi) {
		this.ciclFstNmCi = cw02cCiclFstNmCi;
	}

	public char getCw02cCiclFstNmCi() {
		return this.ciclFstNmCi;
	}

	public void setCw02cCiclFstNm(String cw02cCiclFstNm) {
		this.ciclFstNm = Functions.subString(cw02cCiclFstNm, Len.CICL_FST_NM);
	}

	public String getCw02cCiclFstNm() {
		return this.ciclFstNm;
	}

	public void setCw02cCiclLstNmCi(char cw02cCiclLstNmCi) {
		this.ciclLstNmCi = cw02cCiclLstNmCi;
	}

	public char getCw02cCiclLstNmCi() {
		return this.ciclLstNmCi;
	}

	public void setCw02cCiclLstNm(String cw02cCiclLstNm) {
		this.ciclLstNm = Functions.subString(cw02cCiclLstNm, Len.CICL_LST_NM);
	}

	public String getCw02cCiclLstNm() {
		return this.ciclLstNm;
	}

	public void setCw02cCiclMdlNmCi(char cw02cCiclMdlNmCi) {
		this.ciclMdlNmCi = cw02cCiclMdlNmCi;
	}

	public char getCw02cCiclMdlNmCi() {
		return this.ciclMdlNmCi;
	}

	public void setCw02cCiclMdlNm(String cw02cCiclMdlNm) {
		this.ciclMdlNm = Functions.subString(cw02cCiclMdlNm, Len.CICL_MDL_NM);
	}

	public String getCw02cCiclMdlNm() {
		return this.ciclMdlNm;
	}

	public void setCw02cNmPfxCi(char cw02cNmPfxCi) {
		this.nmPfxCi = cw02cNmPfxCi;
	}

	public char getCw02cNmPfxCi() {
		return this.nmPfxCi;
	}

	public void setCw02cNmPfx(String cw02cNmPfx) {
		this.nmPfx = Functions.subString(cw02cNmPfx, Len.NM_PFX);
	}

	public String getCw02cNmPfx() {
		return this.nmPfx;
	}

	public void setCw02cNmSfxCi(char cw02cNmSfxCi) {
		this.nmSfxCi = cw02cNmSfxCi;
	}

	public char getCw02cNmSfxCi() {
		return this.nmSfxCi;
	}

	public void setCw02cNmSfx(String cw02cNmSfx) {
		this.nmSfx = Functions.subString(cw02cNmSfx, Len.NM_SFX);
	}

	public String getCw02cNmSfx() {
		return this.nmSfx;
	}

	public void setCw02cPrimaryProDsnCdCi(char cw02cPrimaryProDsnCdCi) {
		this.primaryProDsnCdCi = cw02cPrimaryProDsnCdCi;
	}

	public char getCw02cPrimaryProDsnCdCi() {
		return this.primaryProDsnCdCi;
	}

	public void setCw02cPrimaryProDsnCdNi(char cw02cPrimaryProDsnCdNi) {
		this.primaryProDsnCdNi = cw02cPrimaryProDsnCdNi;
	}

	public char getCw02cPrimaryProDsnCdNi() {
		return this.primaryProDsnCdNi;
	}

	public void setCw02cPrimaryProDsnCd(String cw02cPrimaryProDsnCd) {
		this.primaryProDsnCd = Functions.subString(cw02cPrimaryProDsnCd, Len.PRIMARY_PRO_DSN_CD);
	}

	public String getCw02cPrimaryProDsnCd() {
		return this.primaryProDsnCd;
	}

	public void setCw02cLegEntCdCi(char cw02cLegEntCdCi) {
		this.legEntCdCi = cw02cLegEntCdCi;
	}

	public char getCw02cLegEntCdCi() {
		return this.legEntCdCi;
	}

	public void setCw02cLegEntCd(String cw02cLegEntCd) {
		this.legEntCd = Functions.subString(cw02cLegEntCd, Len.LEG_ENT_CD);
	}

	public String getCw02cLegEntCd() {
		return this.legEntCd;
	}

	public void setCw02cCiclSdxCdCi(char cw02cCiclSdxCdCi) {
		this.ciclSdxCdCi = cw02cCiclSdxCdCi;
	}

	public char getCw02cCiclSdxCdCi() {
		return this.ciclSdxCdCi;
	}

	public void setCw02cCiclSdxCd(String cw02cCiclSdxCd) {
		this.ciclSdxCd = Functions.subString(cw02cCiclSdxCd, Len.CICL_SDX_CD);
	}

	public String getCw02cCiclSdxCd() {
		return this.ciclSdxCd;
	}

	public void setCw02cCiclOgnInceptDtCi(char cw02cCiclOgnInceptDtCi) {
		this.ciclOgnInceptDtCi = cw02cCiclOgnInceptDtCi;
	}

	public char getCw02cCiclOgnInceptDtCi() {
		return this.ciclOgnInceptDtCi;
	}

	public void setCw02cCiclOgnInceptDtNi(char cw02cCiclOgnInceptDtNi) {
		this.ciclOgnInceptDtNi = cw02cCiclOgnInceptDtNi;
	}

	public char getCw02cCiclOgnInceptDtNi() {
		return this.ciclOgnInceptDtNi;
	}

	public void setCw02cCiclOgnInceptDt(String cw02cCiclOgnInceptDt) {
		this.ciclOgnInceptDt = Functions.subString(cw02cCiclOgnInceptDt, Len.CICL_OGN_INCEPT_DT);
	}

	public String getCw02cCiclOgnInceptDt() {
		return this.ciclOgnInceptDt;
	}

	public void setCw02cCiclAddNmIndCi(char cw02cCiclAddNmIndCi) {
		this.ciclAddNmIndCi = cw02cCiclAddNmIndCi;
	}

	public char getCw02cCiclAddNmIndCi() {
		return this.ciclAddNmIndCi;
	}

	public void setCw02cCiclAddNmIndNi(char cw02cCiclAddNmIndNi) {
		this.ciclAddNmIndNi = cw02cCiclAddNmIndNi;
	}

	public char getCw02cCiclAddNmIndNi() {
		return this.ciclAddNmIndNi;
	}

	public void setCw02cCiclAddNmInd(char cw02cCiclAddNmInd) {
		this.ciclAddNmInd = cw02cCiclAddNmInd;
	}

	public void setCw02cCiclAddNmIndFormatted(String cw02cCiclAddNmInd) {
		setCw02cCiclAddNmInd(Functions.charAt(cw02cCiclAddNmInd, Types.CHAR_SIZE));
	}

	public char getCw02cCiclAddNmInd() {
		return this.ciclAddNmInd;
	}

	public void setCw02cCiclDobDtCi(char cw02cCiclDobDtCi) {
		this.ciclDobDtCi = cw02cCiclDobDtCi;
	}

	public char getCw02cCiclDobDtCi() {
		return this.ciclDobDtCi;
	}

	public void setCw02cCiclDobDt(String cw02cCiclDobDt) {
		this.ciclDobDt = Functions.subString(cw02cCiclDobDt, Len.CICL_DOB_DT);
	}

	public String getCw02cCiclDobDt() {
		return this.ciclDobDt;
	}

	public void setCw02cCiclBirStCdCi(char cw02cCiclBirStCdCi) {
		this.ciclBirStCdCi = cw02cCiclBirStCdCi;
	}

	public char getCw02cCiclBirStCdCi() {
		return this.ciclBirStCdCi;
	}

	public void setCw02cCiclBirStCd(String cw02cCiclBirStCd) {
		this.ciclBirStCd = Functions.subString(cw02cCiclBirStCd, Len.CICL_BIR_ST_CD);
	}

	public String getCw02cCiclBirStCd() {
		return this.ciclBirStCd;
	}

	public void setCw02cGenderCdCi(char cw02cGenderCdCi) {
		this.genderCdCi = cw02cGenderCdCi;
	}

	public char getCw02cGenderCdCi() {
		return this.genderCdCi;
	}

	public void setCw02cGenderCd(char cw02cGenderCd) {
		this.genderCd = cw02cGenderCd;
	}

	public void setCw02cGenderCdFormatted(String cw02cGenderCd) {
		setCw02cGenderCd(Functions.charAt(cw02cGenderCd, Types.CHAR_SIZE));
	}

	public char getCw02cGenderCd() {
		return this.genderCd;
	}

	public void setCw02cPriLggCdCi(char cw02cPriLggCdCi) {
		this.priLggCdCi = cw02cPriLggCdCi;
	}

	public char getCw02cPriLggCdCi() {
		return this.priLggCdCi;
	}

	public void setCw02cPriLggCd(String cw02cPriLggCd) {
		this.priLggCd = Functions.subString(cw02cPriLggCd, Len.PRI_LGG_CD);
	}

	public String getCw02cPriLggCd() {
		return this.priLggCd;
	}

	public void setCw02cUserIdCi(char cw02cUserIdCi) {
		this.userIdCi = cw02cUserIdCi;
	}

	public char getCw02cUserIdCi() {
		return this.userIdCi;
	}

	public void setCw02cUserId(String cw02cUserId) {
		this.userId = Functions.subString(cw02cUserId, Len.USER_ID);
	}

	public String getCw02cUserId() {
		return this.userId;
	}

	public void setCw02cStatusCdCi(char cw02cStatusCdCi) {
		this.statusCdCi = cw02cStatusCdCi;
	}

	public char getCw02cStatusCdCi() {
		return this.statusCdCi;
	}

	public void setCw02cStatusCd(char cw02cStatusCd) {
		this.statusCd = cw02cStatusCd;
	}

	public void setCw02cStatusCdFormatted(String cw02cStatusCd) {
		setCw02cStatusCd(Functions.charAt(cw02cStatusCd, Types.CHAR_SIZE));
	}

	public char getCw02cStatusCd() {
		return this.statusCd;
	}

	public void setCw02cTerminalIdCi(char cw02cTerminalIdCi) {
		this.terminalIdCi = cw02cTerminalIdCi;
	}

	public char getCw02cTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setCw02cTerminalId(String cw02cTerminalId) {
		this.terminalId = Functions.subString(cw02cTerminalId, Len.TERMINAL_ID);
	}

	public String getCw02cTerminalId() {
		return this.terminalId;
	}

	public void setCw02cCiclExpDtCi(char cw02cCiclExpDtCi) {
		this.ciclExpDtCi = cw02cCiclExpDtCi;
	}

	public char getCw02cCiclExpDtCi() {
		return this.ciclExpDtCi;
	}

	public void setCw02cCiclExpDt(String cw02cCiclExpDt) {
		this.ciclExpDt = Functions.subString(cw02cCiclExpDt, Len.CICL_EXP_DT);
	}

	public String getCw02cCiclExpDt() {
		return this.ciclExpDt;
	}

	public void setCw02cCiclEffAcyTsCi(char cw02cCiclEffAcyTsCi) {
		this.ciclEffAcyTsCi = cw02cCiclEffAcyTsCi;
	}

	public char getCw02cCiclEffAcyTsCi() {
		return this.ciclEffAcyTsCi;
	}

	public void setCw02cCiclEffAcyTs(String cw02cCiclEffAcyTs) {
		this.ciclEffAcyTs = Functions.subString(cw02cCiclEffAcyTs, Len.CICL_EFF_ACY_TS);
	}

	public String getCw02cCiclEffAcyTs() {
		return this.ciclEffAcyTs;
	}

	public void setCw02cCiclExpAcyTsCi(char cw02cCiclExpAcyTsCi) {
		this.ciclExpAcyTsCi = cw02cCiclExpAcyTsCi;
	}

	public char getCw02cCiclExpAcyTsCi() {
		return this.ciclExpAcyTsCi;
	}

	public void setCw02cCiclExpAcyTs(String cw02cCiclExpAcyTs) {
		this.ciclExpAcyTs = Functions.subString(cw02cCiclExpAcyTs, Len.CICL_EXP_ACY_TS);
	}

	public String getCw02cCiclExpAcyTs() {
		return this.ciclExpAcyTs;
	}

	public void setCw02cStatutoryTleCdCi(char cw02cStatutoryTleCdCi) {
		this.statutoryTleCdCi = cw02cStatutoryTleCdCi;
	}

	public char getCw02cStatutoryTleCdCi() {
		return this.statutoryTleCdCi;
	}

	public void setCw02cStatutoryTleCd(String cw02cStatutoryTleCd) {
		this.statutoryTleCd = Functions.subString(cw02cStatutoryTleCd, Len.STATUTORY_TLE_CD);
	}

	public String getCw02cStatutoryTleCd() {
		return this.statutoryTleCd;
	}

	public void setCw02cCiclLngNmCi(char cw02cCiclLngNmCi) {
		this.ciclLngNmCi = cw02cCiclLngNmCi;
	}

	public char getCw02cCiclLngNmCi() {
		return this.ciclLngNmCi;
	}

	public void setCw02cCiclLngNmNi(char cw02cCiclLngNmNi) {
		this.ciclLngNmNi = cw02cCiclLngNmNi;
	}

	public char getCw02cCiclLngNmNi() {
		return this.ciclLngNmNi;
	}

	public void setCw02cCiclLngNm(String cw02cCiclLngNm) {
		this.ciclLngNm = Functions.subString(cw02cCiclLngNm, Len.CICL_LNG_NM);
	}

	public String getCw02cCiclLngNm() {
		return this.ciclLngNm;
	}

	public void setCw02cLoLstNmMchCdCi(char cw02cLoLstNmMchCdCi) {
		this.loLstNmMchCdCi = cw02cLoLstNmMchCdCi;
	}

	public char getCw02cLoLstNmMchCdCi() {
		return this.loLstNmMchCdCi;
	}

	public void setCw02cLoLstNmMchCdNi(char cw02cLoLstNmMchCdNi) {
		this.loLstNmMchCdNi = cw02cLoLstNmMchCdNi;
	}

	public char getCw02cLoLstNmMchCdNi() {
		return this.loLstNmMchCdNi;
	}

	public void setCw02cLoLstNmMchCd(String cw02cLoLstNmMchCd) {
		this.loLstNmMchCd = Functions.subString(cw02cLoLstNmMchCd, Len.LO_LST_NM_MCH_CD);
	}

	public String getCw02cLoLstNmMchCd() {
		return this.loLstNmMchCd;
	}

	public void setCw02cHiLstNmMchCdCi(char cw02cHiLstNmMchCdCi) {
		this.hiLstNmMchCdCi = cw02cHiLstNmMchCdCi;
	}

	public char getCw02cHiLstNmMchCdCi() {
		return this.hiLstNmMchCdCi;
	}

	public void setCw02cHiLstNmMchCdNi(char cw02cHiLstNmMchCdNi) {
		this.hiLstNmMchCdNi = cw02cHiLstNmMchCdNi;
	}

	public char getCw02cHiLstNmMchCdNi() {
		return this.hiLstNmMchCdNi;
	}

	public void setCw02cHiLstNmMchCd(String cw02cHiLstNmMchCd) {
		this.hiLstNmMchCd = Functions.subString(cw02cHiLstNmMchCd, Len.HI_LST_NM_MCH_CD);
	}

	public String getCw02cHiLstNmMchCd() {
		return this.hiLstNmMchCd;
	}

	public void setCw02cLoFstNmMchCdCi(char cw02cLoFstNmMchCdCi) {
		this.loFstNmMchCdCi = cw02cLoFstNmMchCdCi;
	}

	public char getCw02cLoFstNmMchCdCi() {
		return this.loFstNmMchCdCi;
	}

	public void setCw02cLoFstNmMchCdNi(char cw02cLoFstNmMchCdNi) {
		this.loFstNmMchCdNi = cw02cLoFstNmMchCdNi;
	}

	public char getCw02cLoFstNmMchCdNi() {
		return this.loFstNmMchCdNi;
	}

	public void setCw02cLoFstNmMchCd(String cw02cLoFstNmMchCd) {
		this.loFstNmMchCd = Functions.subString(cw02cLoFstNmMchCd, Len.LO_FST_NM_MCH_CD);
	}

	public String getCw02cLoFstNmMchCd() {
		return this.loFstNmMchCd;
	}

	public void setCw02cHiFstNmMchCdCi(char cw02cHiFstNmMchCdCi) {
		this.hiFstNmMchCdCi = cw02cHiFstNmMchCdCi;
	}

	public char getCw02cHiFstNmMchCdCi() {
		return this.hiFstNmMchCdCi;
	}

	public void setCw02cHiFstNmMchCdNi(char cw02cHiFstNmMchCdNi) {
		this.hiFstNmMchCdNi = cw02cHiFstNmMchCdNi;
	}

	public char getCw02cHiFstNmMchCdNi() {
		return this.hiFstNmMchCdNi;
	}

	public void setCw02cHiFstNmMchCd(String cw02cHiFstNmMchCd) {
		this.hiFstNmMchCd = Functions.subString(cw02cHiFstNmMchCd, Len.HI_FST_NM_MCH_CD);
	}

	public String getCw02cHiFstNmMchCd() {
		return this.hiFstNmMchCd;
	}

	public void setCw02cCiclAcqSrcCdCi(char cw02cCiclAcqSrcCdCi) {
		this.ciclAcqSrcCdCi = cw02cCiclAcqSrcCdCi;
	}

	public char getCw02cCiclAcqSrcCdCi() {
		return this.ciclAcqSrcCdCi;
	}

	public void setCw02cCiclAcqSrcCdNi(char cw02cCiclAcqSrcCdNi) {
		this.ciclAcqSrcCdNi = cw02cCiclAcqSrcCdNi;
	}

	public char getCw02cCiclAcqSrcCdNi() {
		return this.ciclAcqSrcCdNi;
	}

	public void setCw02cCiclAcqSrcCd(String cw02cCiclAcqSrcCd) {
		this.ciclAcqSrcCd = Functions.subString(cw02cCiclAcqSrcCd, Len.CICL_ACQ_SRC_CD);
	}

	public String getCw02cCiclAcqSrcCd() {
		return this.ciclAcqSrcCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICL_PRI_SUB_CD = 4;
		public static final int CICL_FST_NM = 30;
		public static final int CICL_LST_NM = 60;
		public static final int CICL_MDL_NM = 30;
		public static final int NM_PFX = 4;
		public static final int NM_SFX = 4;
		public static final int PRIMARY_PRO_DSN_CD = 4;
		public static final int LEG_ENT_CD = 2;
		public static final int CICL_SDX_CD = 8;
		public static final int CICL_OGN_INCEPT_DT = 10;
		public static final int CICL_DOB_DT = 10;
		public static final int CICL_BIR_ST_CD = 3;
		public static final int PRI_LGG_CD = 3;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CICL_EXP_DT = 10;
		public static final int CICL_EFF_ACY_TS = 26;
		public static final int CICL_EXP_ACY_TS = 26;
		public static final int STATUTORY_TLE_CD = 3;
		public static final int CICL_LNG_NM = 132;
		public static final int LO_LST_NM_MCH_CD = 35;
		public static final int HI_LST_NM_MCH_CD = 35;
		public static final int LO_FST_NM_MCH_CD = 35;
		public static final int HI_FST_NM_MCH_CD = 35;
		public static final int CICL_ACQ_SRC_CD = 3;
		public static final int CW02C_CICL_PRI_SUB_CD_CI = 1;
		public static final int CW02C_CICL_FST_NM_CI = 1;
		public static final int CW02C_CICL_LST_NM_CI = 1;
		public static final int CW02C_CICL_MDL_NM_CI = 1;
		public static final int CW02C_NM_PFX_CI = 1;
		public static final int CW02C_NM_SFX_CI = 1;
		public static final int CW02C_PRIMARY_PRO_DSN_CD_CI = 1;
		public static final int CW02C_PRIMARY_PRO_DSN_CD_NI = 1;
		public static final int CW02C_LEG_ENT_CD_CI = 1;
		public static final int CW02C_CICL_SDX_CD_CI = 1;
		public static final int CW02C_CICL_OGN_INCEPT_DT_CI = 1;
		public static final int CW02C_CICL_OGN_INCEPT_DT_NI = 1;
		public static final int CW02C_CICL_ADD_NM_IND_CI = 1;
		public static final int CW02C_CICL_ADD_NM_IND_NI = 1;
		public static final int CW02C_CICL_ADD_NM_IND = 1;
		public static final int CW02C_CICL_DOB_DT_CI = 1;
		public static final int CW02C_CICL_BIR_ST_CD_CI = 1;
		public static final int CW02C_GENDER_CD_CI = 1;
		public static final int CW02C_GENDER_CD = 1;
		public static final int CW02C_PRI_LGG_CD_CI = 1;
		public static final int CW02C_USER_ID_CI = 1;
		public static final int CW02C_STATUS_CD_CI = 1;
		public static final int CW02C_STATUS_CD = 1;
		public static final int CW02C_TERMINAL_ID_CI = 1;
		public static final int CW02C_CICL_EXP_DT_CI = 1;
		public static final int CW02C_CICL_EFF_ACY_TS_CI = 1;
		public static final int CW02C_CICL_EXP_ACY_TS_CI = 1;
		public static final int CW02C_STATUTORY_TLE_CD_CI = 1;
		public static final int CW02C_CICL_LNG_NM_CI = 1;
		public static final int CW02C_CICL_LNG_NM_NI = 1;
		public static final int CW02C_LO_LST_NM_MCH_CD_CI = 1;
		public static final int CW02C_LO_LST_NM_MCH_CD_NI = 1;
		public static final int CW02C_HI_LST_NM_MCH_CD_CI = 1;
		public static final int CW02C_HI_LST_NM_MCH_CD_NI = 1;
		public static final int CW02C_LO_FST_NM_MCH_CD_CI = 1;
		public static final int CW02C_LO_FST_NM_MCH_CD_NI = 1;
		public static final int CW02C_HI_FST_NM_MCH_CD_CI = 1;
		public static final int CW02C_HI_FST_NM_MCH_CD_NI = 1;
		public static final int CW02C_CICL_ACQ_SRC_CD_CI = 1;
		public static final int CW02C_CICL_ACQ_SRC_CD_NI = 1;
		public static final int CLIENT_TAB_DATA = CW02C_CICL_PRI_SUB_CD_CI + CICL_PRI_SUB_CD + CW02C_CICL_FST_NM_CI + CICL_FST_NM
				+ CW02C_CICL_LST_NM_CI + CICL_LST_NM + CW02C_CICL_MDL_NM_CI + CICL_MDL_NM + CW02C_NM_PFX_CI + NM_PFX + CW02C_NM_SFX_CI + NM_SFX
				+ CW02C_PRIMARY_PRO_DSN_CD_CI + CW02C_PRIMARY_PRO_DSN_CD_NI + PRIMARY_PRO_DSN_CD + CW02C_LEG_ENT_CD_CI + LEG_ENT_CD
				+ CW02C_CICL_SDX_CD_CI + CICL_SDX_CD + CW02C_CICL_OGN_INCEPT_DT_CI + CW02C_CICL_OGN_INCEPT_DT_NI + CICL_OGN_INCEPT_DT
				+ CW02C_CICL_ADD_NM_IND_CI + CW02C_CICL_ADD_NM_IND_NI + CW02C_CICL_ADD_NM_IND + CW02C_CICL_DOB_DT_CI + CICL_DOB_DT
				+ CW02C_CICL_BIR_ST_CD_CI + CICL_BIR_ST_CD + CW02C_GENDER_CD_CI + CW02C_GENDER_CD + CW02C_PRI_LGG_CD_CI + PRI_LGG_CD
				+ CW02C_USER_ID_CI + USER_ID + CW02C_STATUS_CD_CI + CW02C_STATUS_CD + CW02C_TERMINAL_ID_CI + TERMINAL_ID + CW02C_CICL_EXP_DT_CI
				+ CICL_EXP_DT + CW02C_CICL_EFF_ACY_TS_CI + CICL_EFF_ACY_TS + CW02C_CICL_EXP_ACY_TS_CI + CICL_EXP_ACY_TS + CW02C_STATUTORY_TLE_CD_CI
				+ STATUTORY_TLE_CD + CW02C_CICL_LNG_NM_CI + CW02C_CICL_LNG_NM_NI + CICL_LNG_NM + CW02C_LO_LST_NM_MCH_CD_CI + CW02C_LO_LST_NM_MCH_CD_NI
				+ LO_LST_NM_MCH_CD + CW02C_HI_LST_NM_MCH_CD_CI + CW02C_HI_LST_NM_MCH_CD_NI + HI_LST_NM_MCH_CD + CW02C_LO_FST_NM_MCH_CD_CI
				+ CW02C_LO_FST_NM_MCH_CD_NI + LO_FST_NM_MCH_CD + CW02C_HI_FST_NM_MCH_CD_CI + CW02C_HI_FST_NM_MCH_CD_NI + HI_FST_NM_MCH_CD
				+ CW02C_CICL_ACQ_SRC_CD_CI + CW02C_CICL_ACQ_SRC_CD_NI + CICL_ACQ_SRC_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
