/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.ReturnErrorStatus;

/**Original name: RETURNED-DATA<br>
 * Variables: RETURNED-DATA from copybook CISLNSRB<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class ReturnedData {

	//==== PROPERTIES ====
	//Original name: RETURN-PROGRAM-ID
	private String returnProgramId = DefaultValues.stringVal(Len.RETURN_PROGRAM_ID);
	//Original name: RETURN-ERROR-CODE
	private String returnErrorCode = DefaultValues.stringVal(Len.RETURN_ERROR_CODE);
	//Original name: RETURN-ERROR-STATUS
	private ReturnErrorStatus returnErrorStatus = new ReturnErrorStatus();
	//Original name: MESSAGE-RETURNED
	private String messageReturned = DefaultValues.stringVal(Len.MESSAGE_RETURNED);

	//==== METHODS ====
	public void setReturnedDataBytes(byte[] buffer, int offset) {
		int position = offset;
		setReturnedCodesBytes(buffer, position);
		position += Len.RETURNED_CODES;
		setReturnErrorMessageBytes(buffer, position);
	}

	public byte[] getReturnedDataBytes(byte[] buffer, int offset) {
		int position = offset;
		getReturnedCodesBytes(buffer, position);
		position += Len.RETURNED_CODES;
		getReturnErrorMessageBytes(buffer, position);
		return buffer;
	}

	public void initReturnedDataSpaces() {
		initReturnedCodesSpaces();
		initReturnErrorMessageSpaces();
	}

	public void setReturnedCodesBytes(byte[] buffer, int offset) {
		int position = offset;
		returnProgramId = MarshalByte.readString(buffer, position, Len.RETURN_PROGRAM_ID);
		position += Len.RETURN_PROGRAM_ID;
		returnErrorCode = MarshalByte.readString(buffer, position, Len.RETURN_ERROR_CODE);
		position += Len.RETURN_ERROR_CODE;
		returnErrorStatus.setReturnErrorStatus(MarshalByte.readChar(buffer, position));
	}

	public byte[] getReturnedCodesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, returnProgramId, Len.RETURN_PROGRAM_ID);
		position += Len.RETURN_PROGRAM_ID;
		MarshalByte.writeString(buffer, position, returnErrorCode, Len.RETURN_ERROR_CODE);
		position += Len.RETURN_ERROR_CODE;
		MarshalByte.writeChar(buffer, position, returnErrorStatus.getReturnErrorStatus());
		return buffer;
	}

	public void initReturnedCodesSpaces() {
		returnProgramId = "";
		returnErrorCode = "";
		returnErrorStatus.setReturnErrorStatus(Types.SPACE_CHAR);
	}

	public void setReturnProgramId(String returnProgramId) {
		this.returnProgramId = Functions.subString(returnProgramId, Len.RETURN_PROGRAM_ID);
	}

	public String getReturnProgramId() {
		return this.returnProgramId;
	}

	public void setReturnErrorCode(String returnErrorCode) {
		this.returnErrorCode = Functions.subString(returnErrorCode, Len.RETURN_ERROR_CODE);
	}

	public String getReturnErrorCode() {
		return this.returnErrorCode;
	}

	public void setReturnErrorMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		messageReturned = MarshalByte.readString(buffer, position, Len.MESSAGE_RETURNED);
	}

	public byte[] getReturnErrorMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, messageReturned, Len.MESSAGE_RETURNED);
		return buffer;
	}

	public void initReturnErrorMessageSpaces() {
		messageReturned = "";
	}

	public void setMessageReturned(String messageReturned) {
		this.messageReturned = Functions.subString(messageReturned, Len.MESSAGE_RETURNED);
	}

	public String getMessageReturned() {
		return this.messageReturned;
	}

	public ReturnErrorStatus getReturnErrorStatus() {
		return returnErrorStatus;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURN_PROGRAM_ID = 8;
		public static final int RETURN_ERROR_CODE = 3;
		public static final int RETURNED_CODES = RETURN_PROGRAM_ID + RETURN_ERROR_CODE + ReturnErrorStatus.Len.RETURN_ERROR_STATUS;
		public static final int MESSAGE_RETURNED = 30;
		public static final int RETURN_ERROR_MESSAGE = MESSAGE_RETURNED;
		public static final int RETURNED_DATA = RETURNED_CODES + RETURN_ERROR_MESSAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
