/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-INVALID-INPUT<br>
 * Variable: EA-02-INVALID-INPUT from program XZC05090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea02InvalidInputXzc05090 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-INVALID-INPUT
	private String flr1 = "INVALID INPUT.";
	//Original name: FILLER-EA-02-INVALID-INPUT-1
	private String flr2 = " ACCOUNT";
	//Original name: FILLER-EA-02-INVALID-INPUT-2
	private String flr3 = "NUMBER:";
	//Original name: EA-02-ACCOUNT-NUMBER
	private String accountNumber = DefaultValues.stringVal(Len.ACCOUNT_NUMBER);
	//Original name: FILLER-EA-02-INVALID-INPUT-3
	private String flr4 = " NOTIFICATION";
	//Original name: FILLER-EA-02-INVALID-INPUT-4
	private String flr5 = "PROCESS";
	//Original name: FILLER-EA-02-INVALID-INPUT-5
	private String flr6 = "TIMESTAMP:";
	//Original name: EA-02-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);

	//==== METHODS ====
	public String getEa02InvalidInputFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidInputBytes());
	}

	public byte[] getEa02InvalidInputBytes() {
		byte[] buffer = new byte[Len.EA02_INVALID_INPUT];
		return getEa02InvalidInputBytes(buffer, 1);
	}

	public byte[] getEa02InvalidInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, accountNumber, Len.ACCOUNT_NUMBER);
		position += Len.ACCOUNT_NUMBER;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = Functions.subString(accountNumber, Len.ACCOUNT_NUMBER);
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACCOUNT_NUMBER = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 15;
		public static final int FLR2 = 9;
		public static final int FLR3 = 8;
		public static final int FLR4 = 14;
		public static final int FLR6 = 11;
		public static final int EA02_INVALID_INPUT = ACCOUNT_NUMBER + NOT_PRC_TS + FLR1 + FLR2 + 2 * FLR3 + FLR4 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
