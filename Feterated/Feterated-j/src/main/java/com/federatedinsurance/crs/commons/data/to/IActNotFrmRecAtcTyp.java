/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ACT_NOT, FRM_REC_ATC_TYP]
 * 
 */
public interface IActNotFrmRecAtcTyp extends BaseSqlTo {

	/**
	 * Host Variable ACT-OWN-CLT-ID
	 * 
	 */
	String getOwnCltId();

	void setOwnCltId(String ownCltId);

	/**
	 * Host Variable ACT-NOT-TYP-CD
	 * 
	 */
	String getNotTypCd();

	void setNotTypCd(String notTypCd);

	/**
	 * Host Variable ACT-OWN-ADR-ID
	 * 
	 */
	String getOwnAdrId();

	void setOwnAdrId(String ownAdrId);
};
