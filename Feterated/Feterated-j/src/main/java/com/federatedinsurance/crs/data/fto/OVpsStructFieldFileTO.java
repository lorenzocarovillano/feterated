/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;

/**Original name: O-VPS-STRUCT-FIELD-FILE<br>
 * File: O-VPS-STRUCT-FIELD-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OVpsStructFieldFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: VSO-STRUCT-CARRIAGE-CTRL
	private String vsoStructCarriageCtrl = DefaultValues.stringVal(Len.VSO_STRUCT_CARRIAGE_CTRL);
	//Original name: VSO-STRUCT-DATA
	private String vsoStructData = DefaultValues.stringVal(Len.VSO_STRUCT_DATA);

	//==== METHODS ====
	public void setoVpsStructFieldRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		setVsoStructCarriageCtrlXBytes(buffer, position);
		position += Len.VSO_STRUCT_CARRIAGE_CTRL_X;
		vsoStructData = MarshalByte.readString(buffer, position, Len.VSO_STRUCT_DATA);
	}

	public byte[] getoVpsStructFieldRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		getVsoStructCarriageCtrlXBytes(buffer, position);
		position += Len.VSO_STRUCT_CARRIAGE_CTRL_X;
		MarshalByte.writeString(buffer, position, vsoStructData, Len.VSO_STRUCT_DATA);
		return buffer;
	}

	public void setVsoStructCarriageCtrlXFormatted(String data) {
		byte[] buffer = new byte[Len.VSO_STRUCT_CARRIAGE_CTRL_X];
		MarshalByte.writeString(buffer, 1, data, Len.VSO_STRUCT_CARRIAGE_CTRL_X);
		setVsoStructCarriageCtrlXBytes(buffer, 1);
	}

	public void setVsoStructCarriageCtrlXBytes(byte[] buffer, int offset) {
		int position = offset;
		vsoStructCarriageCtrl = MarshalByte.readFixedString(buffer, position, Len.VSO_STRUCT_CARRIAGE_CTRL);
	}

	public byte[] getVsoStructCarriageCtrlXBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, vsoStructCarriageCtrl, Len.VSO_STRUCT_CARRIAGE_CTRL);
		return buffer;
	}

	public void setVsoStructData(String vsoStructData) {
		this.vsoStructData = Functions.subString(vsoStructData, Len.VSO_STRUCT_DATA);
	}

	public String getVsoStructData() {
		return this.vsoStructData;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoVpsStructFieldRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoVpsStructFieldRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_VPS_STRUCT_FIELD_RECORD;
	}

	@Override
	public IBuffer copy() {
		OVpsStructFieldFileTO copyTO = new OVpsStructFieldFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VSO_STRUCT_CARRIAGE_CTRL = 1;
		public static final int VSO_STRUCT_CARRIAGE_CTRL_X = VSO_STRUCT_CARRIAGE_CTRL;
		public static final int VSO_STRUCT_DATA = 132;
		public static final int O_VPS_STRUCT_FIELD_RECORD = VSO_STRUCT_CARRIAGE_CTRL_X + VSO_STRUCT_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
