/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0G90M1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0g90m1 {

	//==== PROPERTIES ====
	//Original name: CF-INITIALIZE-GETMAIN
	private char initializeGetmain = Types.SPACE_CHAR;
	//Original name: CF-SERVICE-PROXY-PGM
	private String serviceProxyPgm = "XZ0X90M0";
	//Original name: CF-UPD-NOT-STA
	private String updNotSta = "UpdateNotificationStatus";
	//Original name: CF-BLANK
	private char blank = Types.SPACE_CHAR;
	//Original name: CF-CN-PROXY-CBK-INP-NM
	private String cnProxyCbkInpNm = "FWPXYCOMONINP";
	//Original name: CF-CN-PROXY-CBK-OUP-NM
	private String cnProxyCbkOupNm = "FWPXYCOMONOUP";
	//Original name: CF-CN-SERVICE-CBK-INP-NM
	private String cnServiceCbkInpNm = "FWPXYSERVICEINP";
	//Original name: CF-CN-SERVICE-CBK-OUP-NM
	private String cnServiceCbkOupNm = "FWPXYSERVICEOUP";

	//==== METHODS ====
	public char getInitializeGetmain() {
		return this.initializeGetmain;
	}

	public String getServiceProxyPgm() {
		return this.serviceProxyPgm;
	}

	public String getUpdNotSta() {
		return this.updNotSta;
	}

	public char getBlank() {
		return this.blank;
	}

	public String getCnProxyCbkInpNm() {
		return this.cnProxyCbkInpNm;
	}

	public String getCnProxyCbkInpNmFormatted() {
		return Functions.padBlanks(getCnProxyCbkInpNm(), Len.CN_PROXY_CBK_INP_NM);
	}

	public String getCnProxyCbkOupNm() {
		return this.cnProxyCbkOupNm;
	}

	public String getCnProxyCbkOupNmFormatted() {
		return Functions.padBlanks(getCnProxyCbkOupNm(), Len.CN_PROXY_CBK_OUP_NM);
	}

	public String getCnServiceCbkInpNm() {
		return this.cnServiceCbkInpNm;
	}

	public String getCnServiceCbkInpNmFormatted() {
		return Functions.padBlanks(getCnServiceCbkInpNm(), Len.CN_SERVICE_CBK_INP_NM);
	}

	public String getCnServiceCbkOupNm() {
		return this.cnServiceCbkOupNm;
	}

	public String getCnServiceCbkOupNmFormatted() {
		return Functions.padBlanks(getCnServiceCbkOupNm(), Len.CN_SERVICE_CBK_OUP_NM);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CN_PROXY_CBK_INP_NM = 16;
		public static final int CN_SERVICE_CBK_INP_NM = 16;
		public static final int CN_SERVICE_CBK_OUP_NM = 16;
		public static final int CN_PROXY_CBK_OUP_NM = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
