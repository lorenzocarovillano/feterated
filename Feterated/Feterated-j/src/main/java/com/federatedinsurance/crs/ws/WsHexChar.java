/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-HEX-CHAR<br>
 * Variable: WS-HEX-CHAR from program HALOUCHS<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsHexChar {

	//==== PROPERTIES ====
	//Original name: WS-HEX-CHAR1
	private char char1 = DefaultValues.CHAR_VAL;
	//Original name: WS-HEX-CHAR2
	private char char2 = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setWsHexValue(int wsHexValue) {
		int position = 1;
		byte[] buffer = getWsHexCharBytes();
		MarshalByte.writeBinaryUnsignedShort(buffer, position, wsHexValue);
		setWsHexCharBytes(buffer);
	}

	/**Original name: WS-HEX-VALUE<br>*/
	public int getWsHexValue() {
		int position = 1;
		return MarshalByte.readBinaryUnsignedShort(getWsHexCharBytes(), position);
	}

	public void setWsHexCharBytes(byte[] buffer) {
		setWsHexCharBytes(buffer, 1);
	}

	/**Original name: WS-HEX-CHAR<br>*/
	public byte[] getWsHexCharBytes() {
		byte[] buffer = new byte[Len.WS_HEX_CHAR];
		return getWsHexCharBytes(buffer, 1);
	}

	public void setWsHexCharBytes(byte[] buffer, int offset) {
		int position = offset;
		char1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		char2 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getWsHexCharBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, char1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, char2);
		return buffer;
	}

	public void setChar1(char char1) {
		this.char1 = char1;
	}

	public char getChar1() {
		return this.char1;
	}

	public void setChar2(char char2) {
		this.char2 = char2;
	}

	public char getChar2() {
		return this.char2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CHAR1 = 1;
		public static final int CHAR2 = 1;
		public static final int WS_HEX_CHAR = CHAR1 + CHAR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
