/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.RiActiveFlag;
import com.federatedinsurance.crs.ws.enums.RiBalanceFileFlag;
import com.federatedinsurance.crs.ws.occurs.Flr1;

/**Original name: REPORT-INFORMATION<br>
 * Variable: REPORT-INFORMATION from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ReportInformation {

	//==== PROPERTIES ====
	public static final int FLR1_MAXOCCURS = 15;
	//Original name: RI-REPORT-NBR
	private String reportNbr = DefaultValues.stringVal(Len.REPORT_NBR);
	//Original name: RI-REPORT-DESCRIPTION
	private String reportDescription = DefaultValues.stringVal(Len.REPORT_DESCRIPTION);
	//Original name: RI-ACTIVE-FLAG
	private RiActiveFlag activeFlag = new RiActiveFlag();
	//Original name: RI-BALANCE-FILE-FLAG
	private RiBalanceFileFlag balanceFileFlag = new RiBalanceFileFlag();
	//Original name: FILLER-RI-OFFICE-LOCATION-DATA
	private Flr1[] flr1 = new Flr1[FLR1_MAXOCCURS];
	//Original name: RI-OFF-LOCATION-DEFAULT-FLAG
	private char offLocationDefaultFlag = DefaultValues.CHAR_VAL;

	//==== CONSTRUCTORS ====
	public ReportInformation() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int flr1Idx = 1; flr1Idx <= FLR1_MAXOCCURS; flr1Idx++) {
			flr1[flr1Idx - 1] = new Flr1();
		}
	}

	public void setReportInformationFormatted(String data) {
		byte[] buffer = new byte[Len.REPORT_INFORMATION];
		MarshalByte.writeString(buffer, 1, data, Len.REPORT_INFORMATION);
		setReportInformationBytes(buffer, 1);
	}

	public void setReportInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		reportNbr = MarshalByte.readString(buffer, position, Len.REPORT_NBR);
		position += Len.REPORT_NBR;
		reportDescription = MarshalByte.readString(buffer, position, Len.REPORT_DESCRIPTION);
		position += Len.REPORT_DESCRIPTION;
		activeFlag.setActiveFlag(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		balanceFileFlag.setBalanceFileFlag(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		setOfficeLocationDataBytes(buffer, position);
		position += Len.OFFICE_LOCATION_DATA;
		offLocationDefaultFlag = MarshalByte.readChar(buffer, position);
	}

	public void setReportNbr(String reportNbr) {
		this.reportNbr = Functions.subString(reportNbr, Len.REPORT_NBR);
	}

	public String getReportNbr() {
		return this.reportNbr;
	}

	public void setReportDescription(String reportDescription) {
		this.reportDescription = Functions.subString(reportDescription, Len.REPORT_DESCRIPTION);
	}

	public String getReportDescription() {
		return this.reportDescription;
	}

	public void setOfficeLocationDataBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= FLR1_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				flr1[idx - 1].setFlr1Bytes(buffer, position);
				position += Flr1.Len.FLR1;
			} else {
				flr1[idx - 1].initFlr1Spaces();
				position += Flr1.Len.FLR1;
			}
		}
	}

	public void setOffLocationDefaultFlag(char offLocationDefaultFlag) {
		this.offLocationDefaultFlag = offLocationDefaultFlag;
	}

	public char getOffLocationDefaultFlag() {
		return this.offLocationDefaultFlag;
	}

	public RiActiveFlag getActiveFlag() {
		return activeFlag;
	}

	public RiBalanceFileFlag getBalanceFileFlag() {
		return balanceFileFlag;
	}

	public Flr1 getFlr1(int idx) {
		return flr1[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REPORT_NBR = 6;
		public static final int REPORT_DESCRIPTION = 80;
		public static final int OFFICE_LOCATION_DATA = ReportInformation.FLR1_MAXOCCURS * Flr1.Len.FLR1;
		public static final int OFF_LOCATION_DEFAULT_FLAG = 1;
		public static final int REPORT_INFORMATION = REPORT_NBR + REPORT_DESCRIPTION + RiActiveFlag.Len.ACTIVE_FLAG
				+ RiBalanceFileFlag.Len.BALANCE_FILE_FLAG + OFFICE_LOCATION_DATA + OFF_LOCATION_DEFAULT_FLAG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
