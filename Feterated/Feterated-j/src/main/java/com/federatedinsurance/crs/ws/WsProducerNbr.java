/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-PRODUCER-NBR<br>
 * Variable: WS-PRODUCER-NBR from program XZ0P0022<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsProducerNbr {

	//==== PROPERTIES ====
	//Original name: WS-PRODUCER-NBR-START
	private char start2 = DefaultValues.CHAR_VAL;
	//Original name: FILLER-WS-PRODUCER-NBR
	private char flr1 = '-';
	//Original name: WS-PRODUCER-NBR-END
	private String end = DefaultValues.stringVal(Len.END);

	//==== METHODS ====
	public String getWsProducerNbrFormatted() {
		return MarshalByteExt.bufferToStr(getWsProducerNbrBytes());
	}

	public byte[] getWsProducerNbrBytes() {
		byte[] buffer = new byte[Len.WS_PRODUCER_NBR];
		return getWsProducerNbrBytes(buffer, 1);
	}

	public byte[] getWsProducerNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, start2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, end, Len.END);
		return buffer;
	}

	public void setStart2(char start2) {
		this.start2 = start2;
	}

	public void setStart2Formatted(String start2) {
		setStart2(Functions.charAt(start2, Types.CHAR_SIZE));
	}

	public char getStart2() {
		return this.start2;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setEnd(String end) {
		this.end = Functions.subString(end, Len.END);
	}

	public String getEnd() {
		return this.end;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int END = 3;
		public static final int START2 = 1;
		public static final int FLR1 = 1;
		public static final int WS_PRODUCER_NBR = START2 + END + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
