/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-CLEAR-UOW-REQUEST-FLAG<br>
 * Variable: SW-CLEAR-UOW-REQUEST-FLAG from program TS020000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwClearUowRequestFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char START_BROWSE_UOW_REQUEST = '0';
	public static final char NO_MORE_UOW_REQUESTS = '1';

	//==== METHODS ====
	public void setClearUowRequestFlag(char clearUowRequestFlag) {
		this.value = clearUowRequestFlag;
	}

	public char getClearUowRequestFlag() {
		return this.value;
	}

	public void setStartBrowseUowRequest() {
		value = START_BROWSE_UOW_REQUEST;
	}

	public boolean isNoMoreUowRequests() {
		return value == NO_MORE_UOW_REQUESTS;
	}

	public void setNoMoreUowRequests() {
		value = NO_MORE_UOW_REQUESTS;
	}
}
