/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0U8010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0u8010 {

	//==== PROPERTIES ====
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0U8010";
	//Original name: WS-FRM-ATC-IND
	private char frmAtcInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public void setFrmAtcInd(char frmAtcInd) {
		this.frmAtcInd = frmAtcInd;
	}

	public char getFrmAtcInd() {
		return this.frmAtcInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
