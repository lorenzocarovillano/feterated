/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-SPECIFIC-WORK-AREAS<br>
 * Variable: WS-SPECIFIC-WORK-AREAS from program CAWPCORC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSpecificWorkAreasCawpcorc {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "CAWPCORC";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CIWS";
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "CLT_OBJECT_RELATED_CLIENT_BPO";
	//Original name: WS-008-BUS-OBJ-NM
	private String ws008BusObjNm = "CLT_OBJ_RELATION_V";
	//Original name: WS-OWN-OBJ-NM
	private String ownObjNm = "CLT_OBJ_RELATION_OWNER";
	//Original name: WS-BUS-OBJ-DATA-ROW
	private String busObjDataRow = "";

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public String getWs008BusObjNm() {
		return this.ws008BusObjNm;
	}

	public String getOwnObjNm() {
		return this.ownObjNm;
	}

	public void setBusObjDataRow(String busObjDataRow) {
		this.busObjDataRow = Functions.subString(busObjDataRow, Len.BUS_OBJ_DATA_ROW);
	}

	public String getBusObjDataRow() {
		return this.busObjDataRow;
	}

	public String getBusObjDataRowFormatted() {
		return Functions.padBlanks(getBusObjDataRow(), Len.BUS_OBJ_DATA_ROW);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUS_OBJ_DATA_ROW = 1000;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
