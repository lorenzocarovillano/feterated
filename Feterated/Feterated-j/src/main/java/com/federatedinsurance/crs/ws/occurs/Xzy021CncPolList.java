/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZY021-CNC-POL-LIST<br>
 * Variables: XZY021-CNC-POL-LIST from copybook XZ0Y0021<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzy021CncPolList {

	//==== PROPERTIES ====
	//Original name: XZY021-CP-POL-NBR
	private String xzy021CpPolNbr = DefaultValues.stringVal(Len.XZY021_CP_POL_NBR);

	//==== METHODS ====
	public void setCncPolListBytes(byte[] buffer, int offset) {
		int position = offset;
		xzy021CpPolNbr = MarshalByte.readString(buffer, position, Len.XZY021_CP_POL_NBR);
	}

	public byte[] getCncPolListBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzy021CpPolNbr, Len.XZY021_CP_POL_NBR);
		return buffer;
	}

	public void initCncPolListSpaces() {
		xzy021CpPolNbr = "";
	}

	public void setXzy021CpPolNbr(String xzy021CpPolNbr) {
		this.xzy021CpPolNbr = Functions.subString(xzy021CpPolNbr, Len.XZY021_CP_POL_NBR);
	}

	public String getXzy021CpPolNbr() {
		return this.xzy021CpPolNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY021_CP_POL_NBR = 25;
		public static final int CNC_POL_LIST = XZY021_CP_POL_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
