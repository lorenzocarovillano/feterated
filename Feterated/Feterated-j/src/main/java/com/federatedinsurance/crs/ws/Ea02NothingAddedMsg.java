/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-NOTHING-ADDED-MSG<br>
 * Variable: EA-02-NOTHING-ADDED-MSG from program XZ0P90C0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02NothingAddedMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-NOTHING-ADDED-MSG
	private String flr1 = "No certs";
	//Original name: FILLER-EA-02-NOTHING-ADDED-MSG-1
	private String flr2 = "added to the";
	//Original name: FILLER-EA-02-NOTHING-ADDED-MSG-2
	private String flr3 = "recipient table";
	//Original name: FILLER-EA-02-NOTHING-ADDED-MSG-3
	private String flr4 = " - for";
	//Original name: FILLER-EA-02-NOTHING-ADDED-MSG-4
	private String flr5 = "Account =";
	//Original name: EA-02-CSR-ACT-NBR
	private String ea02CsrActNbr = DefaultValues.stringVal(Len.EA02_CSR_ACT_NBR);
	//Original name: FILLER-EA-02-NOTHING-ADDED-MSG-5
	private char flr6 = '.';

	//==== METHODS ====
	public String getEa02NothingAddedMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02NothingAddedMsgBytes());
	}

	public byte[] getEa02NothingAddedMsgBytes() {
		byte[] buffer = new byte[Len.EA02_NOTHING_ADDED_MSG];
		return getEa02NothingAddedMsgBytes(buffer, 1);
	}

	public byte[] getEa02NothingAddedMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea02CsrActNbr, Len.EA02_CSR_ACT_NBR);
		position += Len.EA02_CSR_ACT_NBR;
		MarshalByte.writeChar(buffer, position, flr6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa02CsrActNbr(String ea02CsrActNbr) {
		this.ea02CsrActNbr = Functions.subString(ea02CsrActNbr, Len.EA02_CSR_ACT_NBR);
	}

	public String getEa02CsrActNbr() {
		return this.ea02CsrActNbr;
	}

	public char getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA02_CSR_ACT_NBR = 9;
		public static final int FLR1 = 9;
		public static final int FLR2 = 13;
		public static final int FLR3 = 15;
		public static final int FLR4 = 7;
		public static final int FLR5 = 10;
		public static final int FLR6 = 1;
		public static final int EA02_NOTHING_ADDED_MSG = EA02_CSR_ACT_NBR + FLR1 + FLR2 + FLR3 + FLR4 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
