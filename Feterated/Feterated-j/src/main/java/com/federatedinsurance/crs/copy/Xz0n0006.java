/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: XZ0N0006<br>
 * Variable: XZ0N0006 from copybook XZ0N0006<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz0n0006 {

	//==== PROPERTIES ====
	//Original name: XZN006-TRANS-PROCESS-DT
	private String transProcessDt = "Transaction Processing Date";
	//Original name: XZN006-FRM-NBR
	private String frmNbr = "Frm Nbr";
	//Original name: XZN006-FRM-EDT-DT
	private String frmEdtDt = "Frm Edt Dt";
	//Original name: FILLER-XZN006-PROCESSING-CONTEXT-TEXT
	private String flr1 = "the account,";
	//Original name: FILLER-XZN006-PROCESSING-CONTEXT-TEXT-1
	private String flr2 = "notification";
	//Original name: FILLER-XZN006-PROCESSING-CONTEXT-TEXT-2
	private String flr3 = "timestamp, and";
	//Original name: FILLER-XZN006-PROCESSING-CONTEXT-TEXT-3
	private String flr4 = "form seq nbr:";

	//==== METHODS ====
	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public String getFrmNbr() {
		return this.frmNbr;
	}

	public String getFrmEdtDt() {
		return this.frmEdtDt;
	}

	public String getProcessingContextTextFormatted() {
		return MarshalByteExt.bufferToStr(getProcessingContextTextBytes());
	}

	/**Original name: XZN006-PROCESSING-CONTEXT-TEXT<br>*/
	public byte[] getProcessingContextTextBytes() {
		byte[] buffer = new byte[Len.PROCESSING_CONTEXT_TEXT];
		return getProcessingContextTextBytes(buffer, 1);
	}

	public byte[] getProcessingContextTextBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 13;
		public static final int FLR3 = 15;
		public static final int FLR4 = 14;
		public static final int PROCESSING_CONTEXT_TEXT = 2 * FLR1 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
