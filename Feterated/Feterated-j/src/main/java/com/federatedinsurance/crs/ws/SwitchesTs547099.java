/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.SwDataParmPassedFlag;
import com.federatedinsurance.crs.ws.enums.SwErrorFoundFlag;
import com.federatedinsurance.crs.ws.enums.SwRegionInfFndFlag;
import com.federatedinsurance.crs.ws.enums.SwRetryCicsCallFlag;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesTs547099 {

	//==== PROPERTIES ====
	//Original name: SW-FIRST-TIME-CALLED-FLAG
	private boolean firstTimeCalledFlag = true;
	//Original name: SW-ERROR-FOUND-FLAG
	private SwErrorFoundFlag errorFoundFlag = new SwErrorFoundFlag();
	//Original name: SW-DATA-PARM-PASSED-FLAG
	private SwDataParmPassedFlag dataParmPassedFlag = new SwDataParmPassedFlag();
	//Original name: SW-REGION-INF-FND-FLAG
	private SwRegionInfFndFlag regionInfFndFlag = new SwRegionInfFndFlag();
	//Original name: SW-RETRY-CICS-CALL-FLAG
	private SwRetryCicsCallFlag retryCicsCallFlag = new SwRetryCicsCallFlag();

	//==== METHODS ====
	public void setFirstTimeCalledFlag(boolean firstTimeCalledFlag) {
		this.firstTimeCalledFlag = firstTimeCalledFlag;
	}

	public boolean isFirstTimeCalledFlag() {
		return this.firstTimeCalledFlag;
	}

	public SwDataParmPassedFlag getDataParmPassedFlag() {
		return dataParmPassedFlag;
	}

	public SwErrorFoundFlag getErrorFoundFlag() {
		return errorFoundFlag;
	}

	public SwRegionInfFndFlag getRegionInfFndFlag() {
		return regionInfFndFlag;
	}

	public SwRetryCicsCallFlag getRetryCicsCallFlag() {
		return retryCicsCallFlag;
	}
}
