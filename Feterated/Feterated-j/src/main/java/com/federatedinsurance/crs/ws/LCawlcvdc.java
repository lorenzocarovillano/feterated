/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.CwvdcInvalidCharInd;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-CAWLCVDC<br>
 * Variable: L-CAWLCVDC from program CAWRVDCH<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LCawlcvdc extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: CWVDC-EDIT-FIELD
	private String editField = DefaultValues.stringVal(Len.EDIT_FIELD);
	//Original name: CWVDC-INVALID-CHAR-IND
	private CwvdcInvalidCharInd invalidCharInd = new CwvdcInvalidCharInd();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_CAWLCVDC;
	}

	@Override
	public void deserialize(byte[] buf) {
		setlCawlcvdcBytes(buf);
	}

	public void setlCawlcvdcBytes(byte[] buffer) {
		setlCawlcvdcBytes(buffer, 1);
	}

	public byte[] getlCawlcvdcBytes() {
		byte[] buffer = new byte[Len.L_CAWLCVDC];
		return getlCawlcvdcBytes(buffer, 1);
	}

	public void setlCawlcvdcBytes(byte[] buffer, int offset) {
		int position = offset;
		setPassedParmsBytes(buffer, position);
	}

	public byte[] getlCawlcvdcBytes(byte[] buffer, int offset) {
		int position = offset;
		getPassedParmsBytes(buffer, position);
		return buffer;
	}

	public void setPassedParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		editField = MarshalByte.readString(buffer, position, Len.EDIT_FIELD);
		position += Len.EDIT_FIELD;
		invalidCharInd.setInvalidCharInd(MarshalByte.readChar(buffer, position));
	}

	public byte[] getPassedParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, editField, Len.EDIT_FIELD);
		position += Len.EDIT_FIELD;
		MarshalByte.writeChar(buffer, position, invalidCharInd.getInvalidCharInd());
		return buffer;
	}

	public void setEditField(String editField) {
		this.editField = Functions.subString(editField, Len.EDIT_FIELD);
	}

	public String getEditField() {
		return this.editField;
	}

	public CwvdcInvalidCharInd getInvalidCharInd() {
		return invalidCharInd;
	}

	@Override
	public byte[] serialize() {
		return getlCawlcvdcBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EDIT_FIELD = 60;
		public static final int PASSED_PARMS = EDIT_FIELD + CwvdcInvalidCharInd.Len.INVALID_CHAR_IND;
		public static final int L_CAWLCVDC = PASSED_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
