/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-END-OF-PLACEHOLDER-SW<br>
 * Variable: WS-END-OF-PLACEHOLDER-SW from program HALRPLAC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEndOfPlaceholderSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_OF_PLACEHOLDER = 'Y';
	public static final char NOT_END_OF_PLACEHOLDER = 'N';

	//==== METHODS ====
	public void setEndOfPlaceholderSw(char endOfPlaceholderSw) {
		this.value = endOfPlaceholderSw;
	}

	public char getEndOfPlaceholderSw() {
		return this.value;
	}

	public boolean isEndOfPlaceholder() {
		return value == END_OF_PLACEHOLDER;
	}

	public void setEndOfPlaceholder() {
		value = END_OF_PLACEHOLDER;
	}

	public void setNotEndOfPlaceholder() {
		value = NOT_END_OF_PLACEHOLDER;
	}
}
