/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.DfhcommareaFnc02090;
import com.federatedinsurance.crs.ws.DfhcommareaTs571098;
import com.federatedinsurance.crs.ws.Fnc02090Data;
import com.federatedinsurance.crs.ws.IvoryhFnc02090;
import com.federatedinsurance.crs.ws.Ts571cb1;
import com.federatedinsurance.crs.ws.WorkingStorageAreaFnc02090;
import com.federatedinsurance.crs.ws.occurs.Fnc02iMatchStringArray;
import com.federatedinsurance.crs.ws.redefines.CallableInputs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: FNC02090<br>
 * <pre>AUTHOR.       KRISTI SCHETTLER.
 * DATE-WRITTEN. 18 JUN 2010.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - DATAFLUX INTERFACE PROGRAM                  **
 * *                                                             **
 * * PURPOSE -  CALL A CALLABLE WEB SERVICE TO INTERFACE WITH    **
 * *            DATAFLUX AND RETRIEVE THE MATCH CODES THAT       **
 * *            CORRESPOND WITH THE GIVEN INPUT STRINGS.         **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS **
 * *                       LINKED TO BY AN APPLICATION PROGRAM.  **
 * *                                                             **
 * * DATA ACCESS METHODS - CICS LINKAGE                          **
 * *                       DB2 DATABASE                          **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #          DATE      PROG             DESCRIPTION        **
 * * --------   ---------  --------   ---------------------------**
 * * TO07602-35 06/18/2010 E404KXS    NEW                        **
 * * TL000253   12/14/2011 E404KXS    CALLABLE SERVICE MIGRATION **
 * *  CS037     12/01/2012 E404RAH    DATAFLUX UPGRADE CHANGES   **
 * *                                  -CALLABLE SERVICE COPYBOOK **
 * *                                      CHANGES                **
 * *                                  -RETURN ALL MATCH CODES    **
 * * 11824      05/12/2016  E404JAL   WSRR MIGRATION.            **
 * * 11824.22   08/04/2016  E404JAL   FIX APPLID EVALUATION      **
 * * 11824.24   09/08/2016 E404JAL    ADD ADDITIONAL ENVIRONMENTS**
 * * 20163      08/25/2019 E404JAL    ADD ADDITIONAL ENVIRONMENTS**
 * ****************************************************************</pre>*/
public class Fnc02090 extends Program {

	//==== PROPERTIES ====
	private ExecContext execContext = null;
	//Original name: WORKING-STORAGE
	private Fnc02090Data ws = new Fnc02090Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaFnc02090 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaFnc02090 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Fnc02090 getInstance() {
		return (Programs.getInstance(Fnc02090.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>*****************************************************************
	 *   MAIN PROCESSING CONTROL
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: PERFORM 3000-GET-MATCH-CODES
		//              THRU 3000-EXIT.
		rng3000GetMatchCodes();
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>****************************************************************
	 *  INITIALIZE OUTPUT AND VERIFY INPUT.
	 * ****************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE FNC020-PROGRAM-OUTPUT.
		initFnc020ProgramOutput();
		// COB_CODE: SET FNC02O-EC-NO-ERROR      TO TRUE.
		dfhcommarea.getoErrorCode().setOk();
		// COB_CODE: PERFORM 2100-VERIFY-INPUT
		//              THRU 2100-EXIT.
		verifyInput();
		// COB_CODE: PERFORM 2200-RETRIEVE-SVC-URI
		//              THRU 2200-EXIT.
		retrieveSvcUri();
		// COB_CODE: PERFORM 2300-DET-CICS-ENV-INF
		//              THRU 2300-EXIT.
		detCicsEnvInf();
	}

	/**Original name: 2100-VERIFY-INPUT<br>
	 * <pre>****************************************************************
	 *  VERIFY THAT THE CONSUMER PASSED IN VALID INPUTS.
	 * ****************************************************************
	 *     ONLY THE FIRST ITEM MUST BE FILLED IN</pre>*/
	private void verifyInput() {
		// COB_CODE: IF FNC02I-INPUT-STRING(+1) = SPACES
		//             OR
		//              FNC02I-INPUT-STRING(+1) = LOW-VALUES
		//             OR
		//              FNC02I-INPUT-MATCH-TYPE(+1) = SPACES
		//             OR
		//              FNC02I-INPUT-MATCH-TYPE(+1) = LOW-VALUES
		//                  THRU 9100-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getiMatchStringArray(1).getStringFld())
				|| Characters.EQ_LOW.test(dfhcommarea.getiMatchStringArray(1).getStringFld(), Fnc02iMatchStringArray.Len.STRING_FLD)
				|| Characters.EQ_SPACE.test(dfhcommarea.getiMatchStringArray(1).getMatchType())
				|| Characters.EQ_LOW.test(dfhcommarea.getiMatchStringArray(1).getMatchType(), Fnc02iMatchStringArray.Len.MATCH_TYPE)) {
			// COB_CODE: MOVE CF-PN-2100         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getCfParagraphNames().getPn2100());
			// COB_CODE: PERFORM 9100-INVALID-INPUT-ERROR
			//              THRU 9100-EXIT
			invalidInputError();
		}
	}

	/**Original name: 2200-RETRIEVE-SVC-URI<br>
	 * <pre> TL253 START
	 * *****************************************************************
	 *  CALL THE WEB SERVICE URI LOOKUP COMMON ROUTINE
	 * *****************************************************************</pre>*/
	private void retrieveSvcUri() {
		// COB_CODE: INITIALIZE URI-LKU-LINKAGE.
		initTs571cb1();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO UL-SERVICE-MNEMONIC.
		ws.getTs571cb1().setServiceMnemonic(ws.getConstantFields().getCfWebSvcId());
		// COB_CODE: SET WS-WEB-SVC-LKU-PGM      TO TRUE.
		ws.getWorkingStorageArea().getCicsProgram().setWebSvcLkuPgm();
		// CALL WEB SERVICE LOOKUP INTERFACE PROGRAM
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (WS-CICS-PROGRAM)
		//               COMMAREA (URI-LKU-LINKAGE)
		//               LENGTH   (LENGTH OF URI-LKU-LINKAGE)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("FNC02090", execContext).commarea(ws.getTs571cb1()).length(Ts571cb1.Len.TS571CB1)
				.link(ws.getWorkingStorageArea().getCicsProgram().getCicsProgram(), new Ts571099());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// CHECK FOR ERROR FROM CICS LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9200-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-2200         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getCfParagraphNames().getPn2200());
			// COB_CODE: PERFORM 9200-CICS-LINK-ERROR
			//              THRU 9200-EXIT
			cicsLinkError();
		}
		// CHECK IF RETURNING URI IS INVALID
		// COB_CODE: IF UL-URI = SPACES
		//             OR
		//              UL-URI = LOW-VALUES
		//                  THRU 9400-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getTs571cb1().getUri()) || Characters.EQ_LOW.test(ws.getTs571cb1().getUri(), Ts571cb1.Len.URI)) {
			// COB_CODE: MOVE CF-PN-2200         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getCfParagraphNames().getPn2200());
			// COB_CODE: PERFORM 9400-URI-NOT-FOUND-ERROR
			//              THRU 9400-EXIT
			uriNotFoundError();
		}
		// COB_CODE: MOVE UL-URI                 TO WS-WEB-SVC-URI.
		ws.getWorkingStorageArea().setWebSvcUri(ws.getTs571cb1().getUri());
	}

	/**Original name: 2300-DET-CICS-ENV-INF<br>
	 * <pre> TL253 END
	 * *****************************************************************
	 *  DETERMINE THE CICS REGION SERVICE IS RUNNING IN
	 * *****************************************************************</pre>*/
	private void detCicsEnvInf() {
		// COB_CODE: EXEC CICS ASSIGN
		//               APPLID(SA-CICS-APPLID)
		//           END-EXEC.
		ws.getSaCicsApplid().setSaCicsApplid(execContext.getApplicationId());
		execContext.clearStatus();
	}

	/**Original name: 3000-GET-MATCH-CODES<br>
	 * <pre>*****************************************************************
	 *  CALL THE CLTGetCityMatchCodeCallable SERVICE TO INTERFACE
	 *  WITH DATAFLUX AND RETURN THE MATCH CODES FOR EACH STRING
	 *  PASSED IN. EVEN THOUGH THE SERVICE SAYS IT RETURNS THE
	 *  'CITY' MATCH CODES, IT CAN BE USED FOR ALL TYPES.
	 * *****************************************************************</pre>*/
	private void getMatchCodes() {
		// COB_CODE: MOVE +0                     TO SS-IO.
		ws.setSsIo(((short) 0));
	}

	/**Original name: 3000-A<br>*/
	private String a() {
		// COB_CODE: ADD +1                      TO SS-IO.
		ws.setSsIo(Trunc.toShort(1 + ws.getSsIo(), 4));
		// COB_CODE: IF FNC02I-INPUT-STRING(SS-IO) = SPACES
		//             OR
		//              FNC02I-INPUT-STRING(SS-IO) = LOW-VALUES
		//               GO TO 3000-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getiMatchStringArray(ws.getSsIo()).getStringFld())
				|| Characters.EQ_LOW.test(dfhcommarea.getiMatchStringArray(ws.getSsIo()).getStringFld(), Fnc02iMatchStringArray.Len.STRING_FLD)) {
			// COB_CODE: GO TO 3000-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3100-INIT-WEB-SVC-CALL
		//              THRU 3100-EXIT.
		initWebSvcCall();
		// COB_CODE: PERFORM 3200-MOVE-WEB-SVC-INPUT
		//              THRU 3200-EXIT.
		moveWebSvcInput();
		// COB_CODE: PERFORM 3300-LINK-TO-IVORY
		//              THRU 3300-EXIT.
		linkToIvory();
		// COB_CODE: PERFORM 3400-MOVE-WEB-SVC-OUTPUT
		//              THRU 3400-EXIT.
		moveWebSvcOutput();
		// COB_CODE: IF SS-IO-MAX
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.isSsIoMax()) {
			// COB_CODE: GO TO 3000-EXIT
			return "";
		}
		// COB_CODE: GO TO 3000-A.
		return "3000-A";
	}

	/**Original name: 3100-INIT-WEB-SVC-CALL<br>
	 * <pre>*****************************************************************
	 *  INITIAL SETUP FOR CALLING IVORY WEB SERVICE PROGRAM
	 * *****************************************************************</pre>*/
	private void initWebSvcCall() {
		// COB_CODE: MOVE LOW-VALUES             TO NON-STATIC-FIELDS
		//                                          CALLABLE-INPUTS
		//                                          CALLABLE-OUTPUTS.
		ws.getIvoryh().getNonStaticFields().initNonStaticFieldsLowValues();
		ws.getIvoryh().getCallableInputs().initCallableInputsLowValues();
		ws.getIvoryh().getCallableInputs().initCallableOutputsLowValues();
		// COB_CODE: MOVE LENGTH OF CALLABLE-INPUTS
		//                                       TO SOAP-REQUEST-LENGTH
		//                                          SOAP-RESPONSE-LENGTH.
		ws.getIvoryh().getNonStaticFields().setRequestLength(CallableInputs.Len.CALLABLE_INPUTS);
		ws.getIvoryh().getNonStaticFields().setResponseLength(CallableInputs.Len.CALLABLE_INPUTS);
		// COB_CODE: MOVE CF-SOAP-SERVICE        TO SOAP-SERVICE.
		ws.getIvoryh().getNonStaticFields().setService(ws.getConstantFields().getCfSoapService());
		// COB_CODE: MOVE CF-SOAP-OPERATION      TO SOAP-OPERATION.
		ws.getIvoryh().getNonStaticFields().setOperation(ws.getConstantFields().getCfSoapOperation());
		// COB_CODE: MOVE CF-SOAP-LAYOUT-VERSION TO SOAP-LAYOUT-VERSION.
		ws.getIvoryh().setSoapLayoutVersion(ws.getConstantFields().getCfSoapLayoutVersion());
		// COB_CODE: SET SOAP-TRACE-STOR-CHARSET-EBCDIC
		//               SOAP-REQUEST-STOR-TYPE-CAREA
		//               SOAP-RESPONSE-STOR-TYPE-CAREA
		//               SOAP-TRACE-STOR-TYPE-TDQ
		//               SOAP-ERROR-STOR-TYPE-CAREA
		//                                       TO TRUE.
		ws.getIvoryh().getNonStaticFields().getTraceStorCharset().setEbcdic();
		ws.getIvoryh().getNonStaticFields().getRequestStorType().setSoapRequestStorTypeCarea();
		ws.getIvoryh().getNonStaticFields().getResponseStorType().setSoapResponseStorTypeCarea();
		ws.getIvoryh().getNonStaticFields().getTraceStorType().setTdq();
		ws.getIvoryh().getNonStaticFields().getErrorStorType().setSoapErrorStorTypeCarea();
		// COB_CODE: MOVE 'GWAO'                 TO SOAP-TRACE-STOR-VALUE.
		ws.getIvoryh().getNonStaticFields().setTraceStorValue("GWAO");
		// COB_CODE: MOVE SPACES                 TO SOAP-ERROR-STOR-VALUE.
		ws.getIvoryh().getNonStaticFields().setErrorStorValue("");
		// COB_CODE: PERFORM 3110-GET-CALLABLE-CRED
		//              THRU 3110-EXIT.
		getCallableCred();
	}

	/**Original name: 3110-GET-CALLABLE-CRED<br>
	 * <pre>*****************************************************************
	 *  SETUP INPUT FOR THE SERVICE CALL TO IVORY
	 * *****************************************************************</pre>*/
	private void getCallableCred() {
		// COB_CODE: INITIALIZE UID-LKU-LINKAGE.
		initUidLkuLinkage();
		// COB_CODE: MOVE C-UID-PWD-TSQ          TO UI-TSQ-NAME.
		ws.getUidLkuLinkage().setTsqName(ws.getConstantFields().getcUidPwdTsq());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (CF-TSQ-UID-LKU-RUT)
		//               COMMAREA (UID-LKU-LINKAGE)
		//               LENGTH   (LENGTH OF UID-LKU-LINKAGE)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("FNC02090", execContext).commarea(ws.getUidLkuLinkage()).length(DfhcommareaTs571098.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getCfTsqUidLkuRut(), new Ts571098());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9200-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-TSQ-UID-LKU-RUT TO EA-03-DATA
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setData2(ws.getConstantFields().getCfTsqUidLkuRut());
			// COB_CODE: PERFORM 9200-CICS-LINK-ERROR
			//              THRU 9200-EXIT
			cicsLinkError();
		}
		// COB_CODE: IF UI-USER-ID <= SPACES
		//                  THRU 9500-EXIT
		//           END-IF.
		if (Characters.LTE_SPACE.test(ws.getUidLkuLinkage().getUserId())) {
			// COB_CODE: MOVE CF-TSQ-UID-LKU-RUT TO EA-08-PGM
			ws.getErrorAndAdviceMessages().getEa08SubPgmError().setPgm(ws.getConstantFields().getCfTsqUidLkuRut());
			// COB_CODE: MOVE 0                  TO EA-08-RET-CD
			ws.getErrorAndAdviceMessages().getEa08SubPgmError().setRetCd(0);
			// COB_CODE: SET EA-08-NO-CRED-FOUND TO TRUE
			ws.getErrorAndAdviceMessages().getEa08SubPgmError().getFlr8().setNoCredFound();
			// COB_CODE: MOVE EA-08-SUB-PGM-ERROR
			//                                   TO EA-01-ERROR-MESSAGE
			ws.getErrorAndAdviceMessages().getEa01Error()
					.setErrorMessage(ws.getErrorAndAdviceMessages().getEa08SubPgmError().getEa08SubPgmErrorFormatted());
			// COB_CODE: PERFORM 9500-SVC-CALL-ERROR
			//              THRU 9500-EXIT
			svcCallError();
		}
	}

	/**Original name: 3200-MOVE-WEB-SVC-INPUT<br>
	 * <pre>*****************************************************************
	 *  SETUP INPUT FOR THE SERVICE CALL TO IVORY
	 * *****************************************************************</pre>*/
	private void moveWebSvcInput() {
		// COB_CODE: MOVE UI-USER-ID             TO MU003I-TK-USERID.
		ws.getIvoryh().getCallableInputs().setMu003iTkUserid(ws.getUidLkuLinkage().getUserId());
		// COB_CODE: MOVE UI-PASSWORD            TO MU003I-TK-PASSWORD.
		ws.getIvoryh().getCallableInputs().setMu003iTkPassword(ws.getUidLkuLinkage().getPassword());
		// COB_CODE: MOVE WS-WEB-SVC-URI         TO MU003I-TK-URI.
		ws.getIvoryh().getCallableInputs().setMu003iTkUri(ws.getWorkingStorageArea().getWebSvcUri());
		// COB_CODE: PERFORM 3210-DET-TAR-SYS
		//              THRU 3210-EXIT.
		detTarSys();
		// COB_CODE: MOVE SA-TAR-SYS             TO MU003I-TK-FED-TAR-SYS.
		ws.getIvoryh().getCallableInputs().setMu003iTkFedTarSys(ws.getSaTarSys().getSaTarSys());
		// COB_CODE: MOVE FNC02I-INPUT-MATCH-TYPE(SS-IO)
		//                                       TO MU003I-MATCH-TYPE.
		ws.getIvoryh().getCallableInputs().setMu003iMatchType(dfhcommarea.getiMatchStringArray(ws.getSsIo()).getMatchType());
		// COB_CODE: MOVE FNC02I-INPUT-STRING(SS-IO)
		//                                       TO MU003I-DATA-VALUE.
		ws.getIvoryh().getCallableInputs().setMu003iDataValue(dfhcommarea.getiMatchStringArray(ws.getSsIo()).getStringFld());
	}

	/**Original name: 3210-DET-TAR-SYS<br>*/
	private void detTarSys() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN SA-CA-DEV1
		//                   SET SA-TS-DEV1      TO TRUE
		//               WHEN SA-CA-DEV2
		//                   SET SA-TS-DEV2      TO TRUE
		//               WHEN SA-CA-BETA1
		//                   SET SA-TS-BETA1     TO TRUE
		//               WHEN SA-CA-BETA2
		//                   SET SA-TS-BETA2     TO TRUE
		//               WHEN SA-CA-EDUC1
		//                   SET SA-TS-EDUC1     TO TRUE
		//               WHEN SA-CA-EDUC2
		//                   SET SA-TS-EDUC2     TO TRUE
		//               WHEN SA-CA-PROD
		//                   SET SA-TS-PROD      TO TRUE
		//               WHEN OTHER
		//                      THRU 9600-EXIT
		//           END-EVALUATE.
		if (ws.getSaCicsApplid().isDev1()) {
			// COB_CODE: SET SA-TS-DEV1      TO TRUE
			ws.getSaTarSys().setDev1();
		} else if (ws.getSaCicsApplid().isDev2()) {
			// COB_CODE: SET SA-TS-DEV2      TO TRUE
			ws.getSaTarSys().setDev2();
		} else if (ws.getSaCicsApplid().isBeta1()) {
			// COB_CODE: SET SA-TS-BETA1     TO TRUE
			ws.getSaTarSys().setBeta1();
		} else if (ws.getSaCicsApplid().isBeta2()) {
			// COB_CODE: SET SA-TS-BETA2     TO TRUE
			ws.getSaTarSys().setBeta2();
		} else if (ws.getSaCicsApplid().isEduc1()) {
			// COB_CODE: SET SA-TS-EDUC1     TO TRUE
			ws.getSaTarSys().setEduc1();
		} else if (ws.getSaCicsApplid().isEduc2()) {
			// COB_CODE: SET SA-TS-EDUC2     TO TRUE
			ws.getSaTarSys().setEduc2();
		} else if (ws.getSaCicsApplid().isProd()) {
			// COB_CODE: SET SA-TS-PROD      TO TRUE
			ws.getSaTarSys().setProd();
		} else {
			// COB_CODE: MOVE CF-PN-3210     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getCfParagraphNames().getPn3210());
			// COB_CODE: PERFORM 9600-CICS-APPLID-ERROR
			//              THRU 9600-EXIT
			cicsApplidError();
		}
	}

	/**Original name: 3300-LINK-TO-IVORY<br>
	 * <pre>*****************************************************************
	 *  CALL THE IVORY PROCESSING PROGRAM
	 * *****************************************************************</pre>*/
	private void linkToIvory() {
		// COB_CODE: SET WS-CALLABLE-SVC-PGM     TO TRUE.
		ws.getWorkingStorageArea().getCicsProgram().setCallableSvcPgm();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (WS-CICS-PROGRAM)
		//               COMMAREA (IVORYH)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("FNC02090", execContext).commarea(ws.getIvoryh()).length(IvoryhFnc02090.Len.IVORYH)
				.link(ws.getWorkingStorageArea().getCicsProgram().getCicsProgram());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9200-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getCfParagraphNames().getPn3300());
			// COB_CODE: PERFORM 9200-CICS-LINK-ERROR
			//              THRU 9200-EXIT
			cicsLinkError();
		}
		// COB_CODE: IF SOAP-RETURNCODE NOT = ZERO
		//                  THRU 9300-EXIT
		//           END-IF.
		if (ws.getIvoryh().getNonStaticFields().getReturncode() != 0) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getCfParagraphNames().getPn3300());
			// COB_CODE: PERFORM 9300-SOAP-FAULT-ERROR
			//              THRU 9300-EXIT
			soapFaultError();
		}
		// COB_CODE: IF MU003O-RETURN-CODE NOT = SPACES
		//                  THRU 9500-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getIvoryh().getCallableInputs().getMu003oReturnCode())) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getCfParagraphNames().getPn3300());
			// COB_CODE: PERFORM 9500-SVC-CALL-ERROR
			//              THRU 9500-EXIT
			svcCallError();
		}
	}

	/**Original name: 3400-MOVE-WEB-SVC-OUTPUT<br>
	 * <pre>*****************************************************************
	 *  PROCESS RESULTS.
	 * *****************************************************************</pre>*/
	private void moveWebSvcOutput() {
		// COB_CODE: MOVE FNC02I-INPUT-MATCH-TYPE(SS-IO)
		//                                       TO
		//                                       FNC02O-OUTPUT-MATCH-TYPE(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).setMatchType(dfhcommarea.getiMatchStringArray(ws.getSsIo()).getMatchType());
		// COB_CODE: MOVE FNC02I-INPUT-STRING(SS-IO)
		//                                       TO FNC02O-OUTPUT-STRING(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).setStringFld(dfhcommarea.getiMatchStringArray(ws.getSsIo()).getStringFld());
		// COB_CODE: MOVE MU003O-MC-95           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc95());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-95(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd95(ws.getWorkingStorageArea().getMatchCd());
		// COB_CODE: MOVE MU003O-MC-90           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc90());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-90(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd90(ws.getWorkingStorageArea().getMatchCd());
		// COB_CODE: MOVE MU003O-MC-85           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc85());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-85(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd85(ws.getWorkingStorageArea().getMatchCd());
		// COB_CODE: MOVE MU003O-MC-80           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc80());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-80(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd80(ws.getWorkingStorageArea().getMatchCd());
		// COB_CODE: MOVE MU003O-MC-75           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc75());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-75(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd75(ws.getWorkingStorageArea().getMatchCd());
		// COB_CODE: MOVE MU003O-MC-70           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc70());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-70(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd70(ws.getWorkingStorageArea().getMatchCd());
		// COB_CODE: MOVE MU003O-MC-65           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc65());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-65(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd65(ws.getWorkingStorageArea().getMatchCd());
		// COB_CODE: MOVE MU003O-MC-60           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc60());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-60(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd60(ws.getWorkingStorageArea().getMatchCd());
		// COB_CODE: MOVE MU003O-MC-55           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc55());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-55(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd55(ws.getWorkingStorageArea().getMatchCd());
		// COB_CODE: MOVE MU003O-MC-50           TO WS-MATCH-CD.
		ws.getWorkingStorageArea().setMatchCd(ws.getIvoryh().getCallableInputs().getMu003oMc50());
		// COB_CODE: PERFORM 3410-EDIT-MATCH-CODE
		//              THRU 3410-EXIT.
		editMatchCode();
		// COB_CODE: MOVE WS-MATCH-CD            TO FNC02O-MCH-CD-50(SS-IO).
		dfhcommarea.getoMatchStringArray(ws.getSsIo()).getMatchCodes().setCd50(ws.getWorkingStorageArea().getMatchCd());
	}

	/**Original name: 3410-EDIT-MATCH-CODE<br>
	 * <pre>*****************************************************************
	 *  MAKE SURE THE MATCH CODE RETURNED IS VALID.
	 * *****************************************************************
	 *  IF AN INVALID VALUE WAS RETURNED, MARK IT AS INVALID.</pre>*/
	private void editMatchCode() {
		// COB_CODE: IF WS-MATCH-CD = SPACES
		//             OR
		//              WS-MATCH-CD = LOW-VALUES
		//             OR
		//              WS-MATCH-CD = CF-INVALID-CD
		//                                       TO WS-MATCH-CD
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWorkingStorageArea().getMatchCd())
				|| Characters.EQ_LOW.test(ws.getWorkingStorageArea().getMatchCd(), WorkingStorageAreaFnc02090.Len.MATCH_CD)
				|| Conditions.eq(ws.getWorkingStorageArea().getMatchCd(), ws.getConstantFields().getCfInvalidCd())) {
			// COB_CODE: MOVE CF-INVALID-IDENTIFIER
			//                                   TO WS-MATCH-CD
			ws.getWorkingStorageArea().setMatchCd(ws.getConstantFields().getCfInvalidIdentifier());
		}
	}

	/**Original name: 9100-INVALID-INPUT-ERROR<br>
	 * <pre>*****************************************************************
	 *  INVALID INPUT ERROR
	 * *****************************************************************</pre>*/
	private void invalidInputError() {
		// COB_CODE: SET FNC02O-EC-INVALID-INPUT TO TRUE.
		dfhcommarea.getoErrorCode().setInvalidInput();
		// COB_CODE: MOVE FNC02I-INPUT-MATCH-TYPE(+1)
		//                                       TO EA-02-INPUT-MATCH-TYPE.
		ws.getErrorAndAdviceMessages().getEa02InvalidInput().setMatchType(dfhcommarea.getiMatchStringArray(1).getMatchType());
		// COB_CODE: MOVE FNC02I-INPUT-STRING(+1)
		//                                       TO EA-02-INPUT-STRING.
		ws.getErrorAndAdviceMessages().getEa02InvalidInput().setStringFld(dfhcommarea.getiMatchStringArray(1).getStringFld());
		// COB_CODE: MOVE EA-02-INVALID-INPUT    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa02InvalidInput().getEa02InvalidInputFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO FNC02O-ERROR-MESSAGE.
		dfhcommarea.setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9200-CICS-LINK-ERROR<br>
	 * <pre>*****************************************************************
	 *  CICS LINK ERROR
	 * *****************************************************************</pre>*/
	private void cicsLinkError() {
		// COB_CODE: SET FNC02O-EC-SYSTEM-ERROR  TO TRUE.
		dfhcommarea.getoErrorCode().setFault();
		// COB_CODE: MOVE WS-CICS-PROGRAM        TO EA-03-FAILED-LINK-PGM-NAME.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getWorkingStorageArea().getCicsProgram().getCicsProgram());
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EA-03-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode(ws.getWorkingStorageArea().getResponseCode());
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EA-03-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode2(ws.getWorkingStorageArea().getResponseCode2());
		// COB_CODE: MOVE EA-03-CICS-LINK-ERROR  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa03CicsLinkError().getEa03CicsLinkErrorFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO FNC02O-ERROR-MESSAGE.
		dfhcommarea.setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9300-SOAP-FAULT-ERROR<br>
	 * <pre>*****************************************************************
	 *  SOAP FAULT ERROR
	 * *****************************************************************</pre>*/
	private void soapFaultError() {
		// COB_CODE: SET FNC02O-EC-SOAP-FAULT    TO TRUE.
		dfhcommarea.getoErrorCode().setSystemError();
		// COB_CODE: MOVE SOAP-RETURNCODE        TO EA-04-SOAP-RETURN-CODE.
		ws.getErrorAndAdviceMessages().getEa04SoapFault().setCode(((int) (ws.getIvoryh().getNonStaticFields().getReturncode())));
		// COB_CODE: MOVE CALLABLE-OUTPUTS       TO EA-04-SOAP-RETURN-MSG.
		ws.getErrorAndAdviceMessages().getEa04SoapFault().setMsg(ws.getIvoryh().getCallableInputs().getCallableOutputsFormatted());
		// COB_CODE: MOVE EA-04-SOAP-FAULT       TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error().setErrorMessage(ws.getErrorAndAdviceMessages().getEa04SoapFault().getEa04SoapFaultFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO FNC02O-ERROR-MESSAGE.
		dfhcommarea.setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9400-URI-NOT-FOUND-ERROR<br>
	 * <pre> TL253 START
	 * *****************************************************************
	 *  SERVICE URI NOT FOUND ERROR
	 * *****************************************************************</pre>*/
	private void uriNotFoundError() {
		// COB_CODE: SET FNC02O-EC-SYSTEM-ERROR  TO TRUE.
		dfhcommarea.getoErrorCode().setFault();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO EA-05-WEB-SRV-ID.
		ws.getErrorAndAdviceMessages().getEa05WebUriError().setEa05WebSrvId(ws.getConstantFields().getCfWebSvcId());
		// COB_CODE: MOVE EA-05-WEB-URI-ERROR    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa05WebUriError().getEa05WebUriErrorFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO FNC02O-ERROR-MESSAGE.
		dfhcommarea.setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9500-SVC-CALL-ERROR<br>
	 * <pre> TL253 END
	 * *****************************************************************
	 *  CALL TO IVORY HAS ENCOUNTERED AN ERROR
	 * *****************************************************************</pre>*/
	private void svcCallError() {
		// COB_CODE: SET FNC02O-EC-SOAP-FAULT    TO TRUE.
		dfhcommarea.getoErrorCode().setSystemError();
		// COB_CODE: MOVE EA-01-ERROR            TO FNC02O-ERROR-MESSAGE.
		dfhcommarea.setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9600-CICS-APPLID-ERROR<br>
	 * <pre>*****************************************************************
	 *  CALL TO DATAFLUX SERVICE HAD AN ERROR
	 * *****************************************************************</pre>*/
	private void cicsApplidError() {
		// COB_CODE: SET FNC02O-EC-SYSTEM-ERROR  TO TRUE.
		dfhcommarea.getoErrorCode().setFault();
		// COB_CODE: MOVE SA-CICS-APPLID         TO EA-07-CICS-RGN.
		ws.getErrorAndAdviceMessages().getEa07TarSysNotFnd().setEa07CicsRgn(ws.getSaCicsApplid().getSaCicsApplid());
		// COB_CODE: MOVE EA-07-TAR-SYS-NOT-FND  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa07TarSysNotFnd().getEa07TarSysNotFndFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO FNC02O-ERROR-MESSAGE.
		dfhcommarea.setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: RNG_3000-GET-MATCH-CODES-_-3000-EXIT<br>*/
	private void rng3000GetMatchCodes() {
		String retcode = "";
		boolean goto3000A = false;
		getMatchCodes();
		do {
			goto3000A = false;
			retcode = a();
		} while (retcode.equals("3000-A"));
	}

	public void initFnc020ProgramOutput() {
		for (int idx0 = 1; idx0 <= DfhcommareaFnc02090.O_MATCH_STRING_ARRAY_MAXOCCURS; idx0++) {
			dfhcommarea.getoMatchStringArray(idx0).setStringFld("");
			dfhcommarea.getoMatchStringArray(idx0).setMatchType("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd50("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd55("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd60("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd65("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd70("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd75("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd80("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd85("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd90("");
			dfhcommarea.getoMatchStringArray(idx0).getMatchCodes().setCd95("");
		}
		dfhcommarea.getoErrorCode().setRetCd(((short) 0));
		dfhcommarea.setoErrorMessage("");
	}

	public void initTs571cb1() {
		ws.getTs571cb1().setServiceMnemonic("");
		ws.getTs571cb1().setUri("");
		ws.getTs571cb1().setCicsSysid("");
		ws.getTs571cb1().setServicePrefix("");
	}

	public void initUidLkuLinkage() {
		ws.getUidLkuLinkage().setTsqName("");
		ws.getUidLkuLinkage().setUserId("");
		ws.getUidLkuLinkage().setPassword("");
		ws.getUidLkuLinkage().setError("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
