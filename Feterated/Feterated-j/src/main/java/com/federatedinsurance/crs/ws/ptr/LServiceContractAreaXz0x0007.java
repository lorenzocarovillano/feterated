/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0007<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0007 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0007() {
	}

	public LServiceContractAreaXz0x0007(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt07iCsrActNbr(String xzt07iCsrActNbr) {
		writeString(Pos.XZT07I_CSR_ACT_NBR, xzt07iCsrActNbr, Len.XZT07I_CSR_ACT_NBR);
	}

	/**Original name: XZT07I-CSR-ACT-NBR<br>*/
	public String getXzt07iCsrActNbr() {
		return readString(Pos.XZT07I_CSR_ACT_NBR, Len.XZT07I_CSR_ACT_NBR);
	}

	public void setXzt07iNotPrcTs(String xzt07iNotPrcTs) {
		writeString(Pos.XZT07I_NOT_PRC_TS, xzt07iNotPrcTs, Len.XZT07I_NOT_PRC_TS);
	}

	/**Original name: XZT07I-NOT-PRC-TS<br>*/
	public String getXzt07iNotPrcTs() {
		return readString(Pos.XZT07I_NOT_PRC_TS, Len.XZT07I_NOT_PRC_TS);
	}

	public void setXzt07iPolNbr(String xzt07iPolNbr) {
		writeString(Pos.XZT07I_POL_NBR, xzt07iPolNbr, Len.XZT07I_POL_NBR);
	}

	/**Original name: XZT07I-POL-NBR<br>*/
	public String getXzt07iPolNbr() {
		return readString(Pos.XZT07I_POL_NBR, Len.XZT07I_POL_NBR);
	}

	public void setXzt07iUserid(String xzt07iUserid) {
		writeString(Pos.XZT07I_USERID, xzt07iUserid, Len.XZT07I_USERID);
	}

	/**Original name: XZT07I-USERID<br>*/
	public String getXzt07iUserid() {
		return readString(Pos.XZT07I_USERID, Len.XZT07I_USERID);
	}

	public String getXzt07iUseridFormatted() {
		return Functions.padBlanks(getXzt07iUserid(), Len.XZT07I_USERID);
	}

	public void setXzt07oTkNotPrcTs(String xzt07oTkNotPrcTs) {
		writeString(Pos.XZT07O_TK_NOT_PRC_TS, xzt07oTkNotPrcTs, Len.XZT07O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT07O-TK-NOT-PRC-TS<br>*/
	public String getXzt07oTkNotPrcTs() {
		return readString(Pos.XZT07O_TK_NOT_PRC_TS, Len.XZT07O_TK_NOT_PRC_TS);
	}

	public void setXzt07oTkNinCltId(String xzt07oTkNinCltId) {
		writeString(Pos.XZT07O_TK_NIN_CLT_ID, xzt07oTkNinCltId, Len.XZT07O_TK_NIN_CLT_ID);
	}

	/**Original name: XZT07O-TK-NIN-CLT-ID<br>*/
	public String getXzt07oTkNinCltId() {
		return readString(Pos.XZT07O_TK_NIN_CLT_ID, Len.XZT07O_TK_NIN_CLT_ID);
	}

	public void setXzt07oTkNinAdrId(String xzt07oTkNinAdrId) {
		writeString(Pos.XZT07O_TK_NIN_ADR_ID, xzt07oTkNinAdrId, Len.XZT07O_TK_NIN_ADR_ID);
	}

	/**Original name: XZT07O-TK-NIN-ADR-ID<br>*/
	public String getXzt07oTkNinAdrId() {
		return readString(Pos.XZT07O_TK_NIN_ADR_ID, Len.XZT07O_TK_NIN_ADR_ID);
	}

	public void setXzt07oTkWfStartedInd(char xzt07oTkWfStartedInd) {
		writeChar(Pos.XZT07O_TK_WF_STARTED_IND, xzt07oTkWfStartedInd);
	}

	/**Original name: XZT07O-TK-WF-STARTED-IND<br>*/
	public char getXzt07oTkWfStartedInd() {
		return readChar(Pos.XZT07O_TK_WF_STARTED_IND);
	}

	public void setXzt07oTkPolBilStaCd(char xzt07oTkPolBilStaCd) {
		writeChar(Pos.XZT07O_TK_POL_BIL_STA_CD, xzt07oTkPolBilStaCd);
	}

	/**Original name: XZT07O-TK-POL-BIL-STA-CD<br>*/
	public char getXzt07oTkPolBilStaCd() {
		return readChar(Pos.XZT07O_TK_POL_BIL_STA_CD);
	}

	public void setXzt07oTkActNotPolCsumFormatted(String xzt07oTkActNotPolCsum) {
		writeString(Pos.XZT07O_TK_ACT_NOT_POL_CSUM, Trunc.toUnsignedNumeric(xzt07oTkActNotPolCsum, Len.XZT07O_TK_ACT_NOT_POL_CSUM),
				Len.XZT07O_TK_ACT_NOT_POL_CSUM);
	}

	/**Original name: XZT07O-TK-ACT-NOT-POL-CSUM<br>*/
	public int getXzt07oTkActNotPolCsum() {
		return readNumDispUnsignedInt(Pos.XZT07O_TK_ACT_NOT_POL_CSUM, Len.XZT07O_TK_ACT_NOT_POL_CSUM);
	}

	public void setXzt07oCsrActNbr(String xzt07oCsrActNbr) {
		writeString(Pos.XZT07O_CSR_ACT_NBR, xzt07oCsrActNbr, Len.XZT07O_CSR_ACT_NBR);
	}

	/**Original name: XZT07O-CSR-ACT-NBR<br>*/
	public String getXzt07oCsrActNbr() {
		return readString(Pos.XZT07O_CSR_ACT_NBR, Len.XZT07O_CSR_ACT_NBR);
	}

	public void setXzt07oPolNbr(String xzt07oPolNbr) {
		writeString(Pos.XZT07O_POL_NBR, xzt07oPolNbr, Len.XZT07O_POL_NBR);
	}

	/**Original name: XZT07O-POL-NBR<br>*/
	public String getXzt07oPolNbr() {
		return readString(Pos.XZT07O_POL_NBR, Len.XZT07O_POL_NBR);
	}

	public void setXzt07oPolTypCd(String xzt07oPolTypCd) {
		writeString(Pos.XZT07O_POL_TYP_CD, xzt07oPolTypCd, Len.XZT07O_POL_TYP_CD);
	}

	/**Original name: XZT07O-POL-TYP-CD<br>*/
	public String getXzt07oPolTypCd() {
		return readString(Pos.XZT07O_POL_TYP_CD, Len.XZT07O_POL_TYP_CD);
	}

	public void setXzt07oPolTypDes(String xzt07oPolTypDes) {
		writeString(Pos.XZT07O_POL_TYP_DES, xzt07oPolTypDes, Len.XZT07O_POL_TYP_DES);
	}

	/**Original name: XZT07O-POL-TYP-DES<br>*/
	public String getXzt07oPolTypDes() {
		return readString(Pos.XZT07O_POL_TYP_DES, Len.XZT07O_POL_TYP_DES);
	}

	public void setXzt07oPolPriRskStAbb(String xzt07oPolPriRskStAbb) {
		writeString(Pos.XZT07O_POL_PRI_RSK_ST_ABB, xzt07oPolPriRskStAbb, Len.XZT07O_POL_PRI_RSK_ST_ABB);
	}

	/**Original name: XZT07O-POL-PRI-RSK-ST-ABB<br>*/
	public String getXzt07oPolPriRskStAbb() {
		return readString(Pos.XZT07O_POL_PRI_RSK_ST_ABB, Len.XZT07O_POL_PRI_RSK_ST_ABB);
	}

	public void setXzt07oNotEffDt(String xzt07oNotEffDt) {
		writeString(Pos.XZT07O_NOT_EFF_DT, xzt07oNotEffDt, Len.XZT07O_NOT_EFF_DT);
	}

	/**Original name: XZT07O-NOT-EFF-DT<br>*/
	public String getXzt07oNotEffDt() {
		return readString(Pos.XZT07O_NOT_EFF_DT, Len.XZT07O_NOT_EFF_DT);
	}

	public void setXzt07oPolEffDt(String xzt07oPolEffDt) {
		writeString(Pos.XZT07O_POL_EFF_DT, xzt07oPolEffDt, Len.XZT07O_POL_EFF_DT);
	}

	/**Original name: XZT07O-POL-EFF-DT<br>*/
	public String getXzt07oPolEffDt() {
		return readString(Pos.XZT07O_POL_EFF_DT, Len.XZT07O_POL_EFF_DT);
	}

	public void setXzt07oPolExpDt(String xzt07oPolExpDt) {
		writeString(Pos.XZT07O_POL_EXP_DT, xzt07oPolExpDt, Len.XZT07O_POL_EXP_DT);
	}

	/**Original name: XZT07O-POL-EXP-DT<br>*/
	public String getXzt07oPolExpDt() {
		return readString(Pos.XZT07O_POL_EXP_DT, Len.XZT07O_POL_EXP_DT);
	}

	public void setXzt07oPolDueAmt(AfDecimal xzt07oPolDueAmt) {
		writeDecimal(Pos.XZT07O_POL_DUE_AMT, xzt07oPolDueAmt.copy());
	}

	/**Original name: XZT07O-POL-DUE-AMT<br>*/
	public AfDecimal getXzt07oPolDueAmt() {
		return readDecimal(Pos.XZT07O_POL_DUE_AMT, Len.Int.XZT07O_POL_DUE_AMT, Len.Fract.XZT07O_POL_DUE_AMT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT007_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT07I_CSR_ACT_NBR = XZT007_SERVICE_INPUTS;
		public static final int XZT07I_NOT_PRC_TS = XZT07I_CSR_ACT_NBR + Len.XZT07I_CSR_ACT_NBR;
		public static final int XZT07I_POL_NBR = XZT07I_NOT_PRC_TS + Len.XZT07I_NOT_PRC_TS;
		public static final int XZT07I_USERID = XZT07I_POL_NBR + Len.XZT07I_POL_NBR;
		public static final int XZT007_SERVICE_OUTPUTS = XZT07I_USERID + Len.XZT07I_USERID;
		public static final int XZT07O_TECHNICAL_KEY = XZT007_SERVICE_OUTPUTS;
		public static final int XZT07O_TK_NOT_PRC_TS = XZT07O_TECHNICAL_KEY;
		public static final int XZT07O_TK_NIN_CLT_ID = XZT07O_TK_NOT_PRC_TS + Len.XZT07O_TK_NOT_PRC_TS;
		public static final int XZT07O_TK_NIN_ADR_ID = XZT07O_TK_NIN_CLT_ID + Len.XZT07O_TK_NIN_CLT_ID;
		public static final int XZT07O_TK_WF_STARTED_IND = XZT07O_TK_NIN_ADR_ID + Len.XZT07O_TK_NIN_ADR_ID;
		public static final int XZT07O_TK_POL_BIL_STA_CD = XZT07O_TK_WF_STARTED_IND + Len.XZT07O_TK_WF_STARTED_IND;
		public static final int XZT07O_TK_ACT_NOT_POL_CSUM = XZT07O_TK_POL_BIL_STA_CD + Len.XZT07O_TK_POL_BIL_STA_CD;
		public static final int XZT07O_CSR_ACT_NBR = XZT07O_TK_ACT_NOT_POL_CSUM + Len.XZT07O_TK_ACT_NOT_POL_CSUM;
		public static final int XZT07O_POL_NBR = XZT07O_CSR_ACT_NBR + Len.XZT07O_CSR_ACT_NBR;
		public static final int XZT07O_POLICY_TYPE = XZT07O_POL_NBR + Len.XZT07O_POL_NBR;
		public static final int XZT07O_POL_TYP_CD = XZT07O_POLICY_TYPE;
		public static final int XZT07O_POL_TYP_DES = XZT07O_POL_TYP_CD + Len.XZT07O_POL_TYP_CD;
		public static final int XZT07O_POL_PRI_RSK_ST_ABB = XZT07O_POL_TYP_DES + Len.XZT07O_POL_TYP_DES;
		public static final int XZT07O_NOT_EFF_DT = XZT07O_POL_PRI_RSK_ST_ABB + Len.XZT07O_POL_PRI_RSK_ST_ABB;
		public static final int XZT07O_POL_EFF_DT = XZT07O_NOT_EFF_DT + Len.XZT07O_NOT_EFF_DT;
		public static final int XZT07O_POL_EXP_DT = XZT07O_POL_EFF_DT + Len.XZT07O_POL_EFF_DT;
		public static final int XZT07O_POL_DUE_AMT = XZT07O_POL_EXP_DT + Len.XZT07O_POL_EXP_DT;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT07I_CSR_ACT_NBR = 9;
		public static final int XZT07I_NOT_PRC_TS = 26;
		public static final int XZT07I_POL_NBR = 25;
		public static final int XZT07I_USERID = 8;
		public static final int XZT07O_TK_NOT_PRC_TS = 26;
		public static final int XZT07O_TK_NIN_CLT_ID = 64;
		public static final int XZT07O_TK_NIN_ADR_ID = 64;
		public static final int XZT07O_TK_WF_STARTED_IND = 1;
		public static final int XZT07O_TK_POL_BIL_STA_CD = 1;
		public static final int XZT07O_TK_ACT_NOT_POL_CSUM = 9;
		public static final int XZT07O_CSR_ACT_NBR = 9;
		public static final int XZT07O_POL_NBR = 25;
		public static final int XZT07O_POL_TYP_CD = 3;
		public static final int XZT07O_POL_TYP_DES = 30;
		public static final int XZT07O_POL_PRI_RSK_ST_ABB = 2;
		public static final int XZT07O_NOT_EFF_DT = 10;
		public static final int XZT07O_POL_EFF_DT = 10;
		public static final int XZT07O_POL_EXP_DT = 10;
		public static final int XZT007_SERVICE_INPUTS = XZT07I_CSR_ACT_NBR + XZT07I_NOT_PRC_TS + XZT07I_POL_NBR + XZT07I_USERID;
		public static final int XZT07O_TECHNICAL_KEY = XZT07O_TK_NOT_PRC_TS + XZT07O_TK_NIN_CLT_ID + XZT07O_TK_NIN_ADR_ID + XZT07O_TK_WF_STARTED_IND
				+ XZT07O_TK_POL_BIL_STA_CD + XZT07O_TK_ACT_NOT_POL_CSUM;
		public static final int XZT07O_POLICY_TYPE = XZT07O_POL_TYP_CD + XZT07O_POL_TYP_DES;
		public static final int XZT07O_POL_DUE_AMT = 10;
		public static final int XZT007_SERVICE_OUTPUTS = XZT07O_TECHNICAL_KEY + XZT07O_CSR_ACT_NBR + XZT07O_POL_NBR + XZT07O_POLICY_TYPE
				+ XZT07O_POL_PRI_RSK_ST_ABB + XZT07O_NOT_EFF_DT + XZT07O_POL_EFF_DT + XZT07O_POL_EXP_DT + XZT07O_POL_DUE_AMT;
		public static final int L_SERVICE_CONTRACT_AREA = XZT007_SERVICE_INPUTS + XZT007_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT07O_POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZT07O_POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
