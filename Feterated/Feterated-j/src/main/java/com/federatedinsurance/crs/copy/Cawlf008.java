/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CAWLF008<br>
 * Variable: CAWLF008 from copybook CAWLF008<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cawlf008 {

	//==== PROPERTIES ====
	//Original name: CW08F-CLT-OBJ-RELATION-FIXED
	private Cw06fCltCltRelationFixed cltObjRelationFixed = new Cw06fCltCltRelationFixed();
	//Original name: CW08F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW08F-CLT-OBJ-RELATION-KEY
	private Cw08fCltObjRelationKey cltObjRelationKey = new Cw08fCltObjRelationKey();
	//Original name: CW08F-CLT-OBJ-RELATION-DATA
	private Cw08fCltObjRelationData cltObjRelationData = new Cw08fCltObjRelationData();

	//==== METHODS ====
	public void setCltObjRelationRowFormatted(String data) {
		byte[] buffer = new byte[Len.CLT_OBJ_RELATION_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CLT_OBJ_RELATION_ROW);
		setCltObjRelationRowBytes(buffer, 1);
	}

	public String getCltObjRelationRowFormatted() {
		return MarshalByteExt.bufferToStr(getCltObjRelationRowBytes());
	}

	/**Original name: CW08F-CLT-OBJ-RELATION-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CAWLF008 - CLT_OBJ_RELATION TABLE                              *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  PP00015  03/07/2007 E404ASW   NEW COPYBOOK                     *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getCltObjRelationRowBytes() {
		byte[] buffer = new byte[Len.CLT_OBJ_RELATION_ROW];
		return getCltObjRelationRowBytes(buffer, 1);
	}

	public void setCltObjRelationRowBytes(byte[] buffer, int offset) {
		int position = offset;
		cltObjRelationFixed.setCltCltRelationFixedBytes(buffer, position);
		position += Cw06fCltCltRelationFixed.Len.CLT_CLT_RELATION_FIXED;
		setCltObjRelationDatesBytes(buffer, position);
		position += Len.CLT_OBJ_RELATION_DATES;
		cltObjRelationKey.setCltObjRelationKeyBytes(buffer, position);
		position += Cw08fCltObjRelationKey.Len.CLT_OBJ_RELATION_KEY;
		cltObjRelationData.setCltObjRelationDataBytes(buffer, position);
	}

	public byte[] getCltObjRelationRowBytes(byte[] buffer, int offset) {
		int position = offset;
		cltObjRelationFixed.getCltCltRelationFixedBytes(buffer, position);
		position += Cw06fCltCltRelationFixed.Len.CLT_CLT_RELATION_FIXED;
		getCltObjRelationDatesBytes(buffer, position);
		position += Len.CLT_OBJ_RELATION_DATES;
		cltObjRelationKey.getCltObjRelationKeyBytes(buffer, position);
		position += Cw08fCltObjRelationKey.Len.CLT_OBJ_RELATION_KEY;
		cltObjRelationData.getCltObjRelationDataBytes(buffer, position);
		return buffer;
	}

	public void setCltObjRelationDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getCltObjRelationDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public Cw08fCltObjRelationData getCltObjRelationData() {
		return cltObjRelationData;
	}

	public Cw06fCltCltRelationFixed getCltObjRelationFixed() {
		return cltObjRelationFixed;
	}

	public Cw08fCltObjRelationKey getCltObjRelationKey() {
		return cltObjRelationKey;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLT_OBJ_RELATION_DATES = TRANS_PROCESS_DT;
		public static final int CLT_OBJ_RELATION_ROW = Cw06fCltCltRelationFixed.Len.CLT_CLT_RELATION_FIXED + CLT_OBJ_RELATION_DATES
				+ Cw08fCltObjRelationKey.Len.CLT_OBJ_RELATION_KEY + Cw08fCltObjRelationData.Len.CLT_OBJ_RELATION_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
