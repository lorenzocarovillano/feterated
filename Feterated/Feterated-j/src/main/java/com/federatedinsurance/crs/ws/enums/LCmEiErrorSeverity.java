/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;

/**Original name: L-CM-EI-ERROR-SEVERITY<br>
 * Variable: L-CM-EI-ERROR-SEVERITY from copybook TS54801<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LCmEiErrorSeverity {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NO_ERROR = Types.SPACE_CHAR;
	public static final char WARNING = '1';
	public static final char LOGIC_ERROR = '2';
	public static final char SYSTEM_ERROR = '3';

	//==== METHODS ====
	public void setErrorSeverity(char errorSeverity) {
		this.value = errorSeverity;
	}

	public char getErrorSeverity() {
		return this.value;
	}

	public boolean isNoError() {
		return value == NO_ERROR;
	}

	public boolean isWarning() {
		return value == WARNING;
	}

	public void setWarning() {
		value = WARNING;
	}

	public void setLogicError() {
		value = LOGIC_ERROR;
	}

	public void setSystemError() {
		value = SYSTEM_ERROR;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_SEVERITY = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
