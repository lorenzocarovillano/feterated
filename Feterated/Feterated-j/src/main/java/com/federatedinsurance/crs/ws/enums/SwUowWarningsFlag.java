/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-UOW-WARNINGS-FLAG<br>
 * Variable: SW-UOW-WARNINGS-FLAG from program TS020000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwUowWarningsFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char START_OF_UOW_WARNINGS = '0';
	public static final char NO_MORE_UOW_WARNINGS = '1';

	//==== METHODS ====
	public void setUowWarningsFlag(char uowWarningsFlag) {
		this.value = uowWarningsFlag;
	}

	public char getUowWarningsFlag() {
		return this.value;
	}

	public void setStartOfUowWarnings() {
		value = START_OF_UOW_WARNINGS;
	}

	public boolean isNoMoreUowWarnings() {
		return value == NO_MORE_UOW_WARNINGS;
	}

	public void setNoMoreUowWarnings() {
		value = NO_MORE_UOW_WARNINGS;
	}
}
