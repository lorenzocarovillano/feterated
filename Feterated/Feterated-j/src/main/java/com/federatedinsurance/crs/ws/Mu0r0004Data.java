/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Ts020tbl;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program MU0R0004<br>
 * Generated as a class for rule WS.<br>*/
public class Mu0r0004Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsMu0r0004 constantFields = new ConstantFieldsMu0r0004();
	//Original name: SS-ADR-IDX
	private short ssAdrIdx = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-SIC-TOB-IDX
	private short ssSicTobIdx = ((short) 0);
	//Original name: WS-MISC-WORK-FLDS
	private WsMiscWorkFldsMu0r0004 wsMiscWorkFlds = new WsMiscWorkFldsMu0r0004();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: TS020TBL
	private Ts020tbl ts020tbl = new Ts020tbl();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: INT-REGISTER1
	private int intRegister1 = DefaultValues.INT_VAL;

	//==== METHODS ====
	public void setSsAdrIdx(short ssAdrIdx) {
		this.ssAdrIdx = ssAdrIdx;
	}

	public short getSsAdrIdx() {
		return this.ssAdrIdx;
	}

	public void setSsSicTobIdx(short ssSicTobIdx) {
		this.ssSicTobIdx = ssSicTobIdx;
	}

	public short getSsSicTobIdx() {
		return this.ssSicTobIdx;
	}

	public void setTableFormatterDataFormatted(String data) {
		byte[] buffer = new byte[Len.TABLE_FORMATTER_DATA];
		MarshalByte.writeString(buffer, 1, data, Len.TABLE_FORMATTER_DATA);
		setTableFormatterDataBytes(buffer, 1);
	}

	public String getTableFormatterDataFormatted() {
		return ts020tbl.getTableFormatterParmsFormatted();
	}

	public void setTableFormatterDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ts020tbl.setTableFormatterParmsBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public void setIntRegister1(int intRegister1) {
		this.intRegister1 = intRegister1;
	}

	public int getIntRegister1() {
		return this.intRegister1;
	}

	public ConstantFieldsMu0r0004 getConstantFields() {
		return constantFields;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Ts020tbl getTs020tbl() {
		return ts020tbl;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsMiscWorkFldsMu0r0004 getWsMiscWorkFlds() {
		return wsMiscWorkFlds;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int TABLE_FORMATTER_DATA = Ts020tbl.Len.TABLE_FORMATTER_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
