/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC080CI<br>
 * Copybook: XZC080CI from copybook XZC080CI<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Xzc080ci {

	//==== PROPERTIES ====
	//Original name: XZC08I-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZC08I-TRM-EXP-DT
	private String trmExpDt = DefaultValues.stringVal(Len.TRM_EXP_DT);

	//==== METHODS ====
	public void setServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		trmExpDt = MarshalByte.readString(buffer, position, Len.TRM_EXP_DT);
	}

	public byte[] getServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, trmExpDt, Len.TRM_EXP_DT);
		return buffer;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setTrmExpDt(String trmExpDt) {
		this.trmExpDt = Functions.subString(trmExpDt, Len.TRM_EXP_DT);
	}

	public String getTrmExpDt() {
		return this.trmExpDt;
	}

	public String getTrmExpDtFormatted() {
		return Functions.padBlanks(getTrmExpDt(), Len.TRM_EXP_DT);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int TRM_EXP_DT = 10;
		public static final int SERVICE_INPUTS = POL_NBR + TRM_EXP_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
