/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-NOTHING-ADDED-MSG<br>
 * Variable: EA-01-NOTHING-ADDED-MSG from program XZ0P90E0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01NothingAddedMsgXz0p90e0 {

	//==== PROPERTIES ====
	/**Original name: FILLER-EA-01-NOTHING-ADDED-MSG<br>
	 * <pre> DO NOT CHANGE THE WORDING OF THIS ERROR MESSAGE.
	 *  CONSUMERS OF THIS SERVICE ARE KEYING OFF IT.</pre>*/
	private String flr1 = "No forms were";
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-1
	private String flr2 = "attached.";
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-2
	private String flr3 = "Account =";
	//Original name: EA-01-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-3
	private String flr4 = "; Notification";
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-4
	private String flr5 = "Type Code =";
	//Original name: EA-01-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-5
	private char flr6 = '.';

	//==== METHODS ====
	public String getEa01NothingAddedMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01NothingAddedMsgBytes());
	}

	public byte[] getEa01NothingAddedMsgBytes() {
		byte[] buffer = new byte[Len.EA01_NOTHING_ADDED_MSG];
		return getEa01NothingAddedMsgBytes(buffer, 1);
	}

	public byte[] getEa01NothingAddedMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeChar(buffer, position, flr6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public char getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int FLR1 = 14;
		public static final int FLR2 = 10;
		public static final int FLR4 = 15;
		public static final int FLR5 = 12;
		public static final int FLR6 = 1;
		public static final int EA01_NOTHING_ADDED_MSG = CSR_ACT_NBR + ACT_NOT_TYP_CD + FLR1 + 2 * FLR2 + FLR4 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
