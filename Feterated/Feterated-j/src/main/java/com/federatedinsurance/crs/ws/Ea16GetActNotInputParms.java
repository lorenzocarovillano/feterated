/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-16-GET-ACT-NOT-INPUT-PARMS<br>
 * Variable: EA-16-GET-ACT-NOT-INPUT-PARMS from program XZ003000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea16GetActNotInputParms {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-16-GET-ACT-NOT-INPUT-PARMS
	private String flr1 = "GetAccountNotif";
	//Original name: FILLER-EA-16-GET-ACT-NOT-INPUT-PARMS-1
	private String flr2 = "icationDtl";
	//Original name: FILLER-EA-16-GET-ACT-NOT-INPUT-PARMS-2
	private String flr3 = "ByActTS SVC";
	//Original name: FILLER-EA-16-GET-ACT-NOT-INPUT-PARMS-3
	private String flr4 = " FAILED!";
	//Original name: FILLER-EA-16-GET-ACT-NOT-INPUT-PARMS-4
	private String flr5 = "PARMS:";
	//Original name: FILLER-EA-16-GET-ACT-NOT-INPUT-PARMS-5
	private String flr6 = "ACT NBR =";
	//Original name: EA-16-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-16-GET-ACT-NOT-INPUT-PARMS-6
	private String flr7 = " TS =";
	//Original name: EA-16-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-16-GET-ACT-NOT-INPUT-PARMS-7
	private String flr8 = " USERID =";
	//Original name: EA-16-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== METHODS ====
	public String getEa16GetActNotInputParmsFormatted() {
		return MarshalByteExt.bufferToStr(getEa16GetActNotInputParmsBytes());
	}

	public byte[] getEa16GetActNotInputParmsBytes() {
		byte[] buffer = new byte[Len.EA16_GET_ACT_NOT_INPUT_PARMS];
		return getEa16GetActNotInputParmsBytes(buffer, 1);
	}

	public byte[] getEa16GetActNotInputParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int USERID = 8;
		public static final int FLR1 = 15;
		public static final int FLR2 = 10;
		public static final int FLR3 = 11;
		public static final int FLR4 = 9;
		public static final int FLR5 = 7;
		public static final int FLR7 = 6;
		public static final int EA16_GET_ACT_NOT_INPUT_PARMS = CSR_ACT_NBR + NOT_PRC_TS + USERID + FLR1 + 3 * FLR2 + FLR3 + FLR4 + FLR5 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
