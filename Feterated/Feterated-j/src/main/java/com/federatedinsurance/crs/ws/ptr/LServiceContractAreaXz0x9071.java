/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9071<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9071 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT971O_ACY_OVL_ROW_MAXOCCURS = 2000;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9071() {
	}

	public LServiceContractAreaXz0x9071(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt971iUserid(String xzt971iUserid) {
		writeString(Pos.XZT971I_USERID, xzt971iUserid, Len.XZT971I_USERID);
	}

	/**Original name: XZT971I-USERID<br>*/
	public String getXzt971iUserid() {
		return readString(Pos.XZT971I_USERID, Len.XZT971I_USERID);
	}

	public String getXzt971iUseridFormatted() {
		return Functions.padBlanks(getXzt971iUserid(), Len.XZT971I_USERID);
	}

	public void setXzt971oAcyOvlRowBytes(int xzt971oAcyOvlRowIdx, byte[] buffer) {
		setXzt971oAcyOvlRowBytes(xzt971oAcyOvlRowIdx, buffer, 1);
	}

	public void setXzt971oAcyOvlRowBytes(int xzt971oAcyOvlRowIdx, byte[] buffer, int offset) {
		int position = Pos.xzt971oAcyOvlRow(xzt971oAcyOvlRowIdx - 1);
		setBytes(buffer, offset, Len.XZT971O_ACY_OVL_ROW, position);
	}

	public void setXzt971oFrmNbr(int xzt971oFrmNbrIdx, String xzt971oFrmNbr) {
		int position = Pos.xzt971oFrmNbr(xzt971oFrmNbrIdx - 1);
		writeString(position, xzt971oFrmNbr, Len.XZT971O_FRM_NBR);
	}

	/**Original name: XZT971O-FRM-NBR<br>*/
	public String getXzt971oFrmNbr(int xzt971oFrmNbrIdx) {
		int position = Pos.xzt971oFrmNbr(xzt971oFrmNbrIdx - 1);
		return readString(position, Len.XZT971O_FRM_NBR);
	}

	public void setXzt971oFrmEdtDt(int xzt971oFrmEdtDtIdx, String xzt971oFrmEdtDt) {
		int position = Pos.xzt971oFrmEdtDt(xzt971oFrmEdtDtIdx - 1);
		writeString(position, xzt971oFrmEdtDt, Len.XZT971O_FRM_EDT_DT);
	}

	/**Original name: XZT971O-FRM-EDT-DT<br>*/
	public String getXzt971oFrmEdtDt(int xzt971oFrmEdtDtIdx) {
		int position = Pos.xzt971oFrmEdtDt(xzt971oFrmEdtDtIdx - 1);
		return readString(position, Len.XZT971O_FRM_EDT_DT);
	}

	public void setXzt971oRecTypCd(int xzt971oRecTypCdIdx, String xzt971oRecTypCd) {
		int position = Pos.xzt971oRecTypCd(xzt971oRecTypCdIdx - 1);
		writeString(position, xzt971oRecTypCd, Len.XZT971O_REC_TYP_CD);
	}

	/**Original name: XZT971O-REC-TYP-CD<br>*/
	public String getXzt971oRecTypCd(int xzt971oRecTypCdIdx) {
		int position = Pos.xzt971oRecTypCd(xzt971oRecTypCdIdx - 1);
		return readString(position, Len.XZT971O_REC_TYP_CD);
	}

	public void setXzt971oOvlEdlFrmNm(int xzt971oOvlEdlFrmNmIdx, String xzt971oOvlEdlFrmNm) {
		int position = Pos.xzt971oOvlEdlFrmNm(xzt971oOvlEdlFrmNmIdx - 1);
		writeString(position, xzt971oOvlEdlFrmNm, Len.XZT971O_OVL_EDL_FRM_NM);
	}

	/**Original name: XZT971O-OVL-EDL-FRM-NM<br>*/
	public String getXzt971oOvlEdlFrmNm(int xzt971oOvlEdlFrmNmIdx) {
		int position = Pos.xzt971oOvlEdlFrmNm(xzt971oOvlEdlFrmNmIdx - 1);
		return readString(position, Len.XZT971O_OVL_EDL_FRM_NM);
	}

	public void setXzt971oStAbb(int xzt971oStAbbIdx, String xzt971oStAbb) {
		int position = Pos.xzt971oStAbb(xzt971oStAbbIdx - 1);
		writeString(position, xzt971oStAbb, Len.XZT971O_ST_ABB);
	}

	/**Original name: XZT971O-ST-ABB<br>*/
	public String getXzt971oStAbb(int xzt971oStAbbIdx) {
		int position = Pos.xzt971oStAbb(xzt971oStAbbIdx - 1);
		return readString(position, Len.XZT971O_ST_ABB);
	}

	public void setXzt971oSpePrcCd(int xzt971oSpePrcCdIdx, String xzt971oSpePrcCd) {
		int position = Pos.xzt971oSpePrcCd(xzt971oSpePrcCdIdx - 1);
		writeString(position, xzt971oSpePrcCd, Len.XZT971O_SPE_PRC_CD);
	}

	/**Original name: XZT971O-SPE-PRC-CD<br>*/
	public String getXzt971oSpePrcCd(int xzt971oSpePrcCdIdx) {
		int position = Pos.xzt971oSpePrcCd(xzt971oSpePrcCdIdx - 1);
		return readString(position, Len.XZT971O_SPE_PRC_CD);
	}

	public void setXzt971oOvlSrCd(int xzt971oOvlSrCdIdx, String xzt971oOvlSrCd) {
		int position = Pos.xzt971oOvlSrCd(xzt971oOvlSrCdIdx - 1);
		writeString(position, xzt971oOvlSrCd, Len.XZT971O_OVL_SR_CD);
	}

	/**Original name: XZT971O-OVL-SR-CD<br>*/
	public String getXzt971oOvlSrCd(int xzt971oOvlSrCdIdx) {
		int position = Pos.xzt971oOvlSrCd(xzt971oOvlSrCdIdx - 1);
		return readString(position, Len.XZT971O_OVL_SR_CD);
	}

	public void setXzt971oOvlDes(int xzt971oOvlDesIdx, String xzt971oOvlDes) {
		int position = Pos.xzt971oOvlDes(xzt971oOvlDesIdx - 1);
		writeString(position, xzt971oOvlDes, Len.XZT971O_OVL_DES);
	}

	/**Original name: XZT971O-OVL-DES<br>*/
	public String getXzt971oOvlDes(int xzt971oOvlDesIdx) {
		int position = Pos.xzt971oOvlDes(xzt971oOvlDesIdx - 1);
		return readString(position, Len.XZT971O_OVL_DES);
	}

	public void setXzt971oXclvInd(int xzt971oXclvIndIdx, char xzt971oXclvInd) {
		int position = Pos.xzt971oXclvInd(xzt971oXclvIndIdx - 1);
		writeChar(position, xzt971oXclvInd);
	}

	/**Original name: XZT971O-XCLV-IND<br>*/
	public char getXzt971oXclvInd(int xzt971oXclvIndIdx) {
		int position = Pos.xzt971oXclvInd(xzt971oXclvIndIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT9071_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT971I_USERID = XZT9071_SERVICE_INPUTS;
		public static final int XZT9071_SERVICE_OUTPUTS = XZT971I_USERID + Len.XZT971I_USERID;
		public static final int XZT971O_TABLE_OF_ACY_OVL_ROWS = XZT9071_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt971oAcyOvlRow(int idx) {
			return XZT971O_TABLE_OF_ACY_OVL_ROWS + idx * Len.XZT971O_ACY_OVL_ROW;
		}

		public static int xzt971oFrmNbr(int idx) {
			return xzt971oAcyOvlRow(idx);
		}

		public static int xzt971oFrmEdtDt(int idx) {
			return xzt971oFrmNbr(idx) + Len.XZT971O_FRM_NBR;
		}

		public static int xzt971oRecTypCd(int idx) {
			return xzt971oFrmEdtDt(idx) + Len.XZT971O_FRM_EDT_DT;
		}

		public static int xzt971oOvlEdlFrmNm(int idx) {
			return xzt971oRecTypCd(idx) + Len.XZT971O_REC_TYP_CD;
		}

		public static int xzt971oStAbb(int idx) {
			return xzt971oOvlEdlFrmNm(idx) + Len.XZT971O_OVL_EDL_FRM_NM;
		}

		public static int xzt971oSpePrcCd(int idx) {
			return xzt971oStAbb(idx) + Len.XZT971O_ST_ABB;
		}

		public static int xzt971oOvlSrCd(int idx) {
			return xzt971oSpePrcCd(idx) + Len.XZT971O_SPE_PRC_CD;
		}

		public static int xzt971oOvlDes(int idx) {
			return xzt971oOvlSrCd(idx) + Len.XZT971O_OVL_SR_CD;
		}

		public static int xzt971oXclvInd(int idx) {
			return xzt971oOvlDes(idx) + Len.XZT971O_OVL_DES;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT971I_USERID = 8;
		public static final int XZT971O_FRM_NBR = 30;
		public static final int XZT971O_FRM_EDT_DT = 10;
		public static final int XZT971O_REC_TYP_CD = 5;
		public static final int XZT971O_OVL_EDL_FRM_NM = 30;
		public static final int XZT971O_ST_ABB = 2;
		public static final int XZT971O_SPE_PRC_CD = 8;
		public static final int XZT971O_OVL_SR_CD = 8;
		public static final int XZT971O_OVL_DES = 30;
		public static final int XZT971O_XCLV_IND = 1;
		public static final int XZT971O_ACY_OVL_ROW = XZT971O_FRM_NBR + XZT971O_FRM_EDT_DT + XZT971O_REC_TYP_CD + XZT971O_OVL_EDL_FRM_NM
				+ XZT971O_ST_ABB + XZT971O_SPE_PRC_CD + XZT971O_OVL_SR_CD + XZT971O_OVL_DES + XZT971O_XCLV_IND;
		public static final int XZT9071_SERVICE_INPUTS = XZT971I_USERID;
		public static final int XZT971O_TABLE_OF_ACY_OVL_ROWS = LServiceContractAreaXz0x9071.XZT971O_ACY_OVL_ROW_MAXOCCURS * XZT971O_ACY_OVL_ROW;
		public static final int XZT9071_SERVICE_OUTPUTS = XZT971O_TABLE_OF_ACY_OVL_ROWS;
		public static final int L_SERVICE_CONTRACT_AREA = XZT9071_SERVICE_INPUTS + XZT9071_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
