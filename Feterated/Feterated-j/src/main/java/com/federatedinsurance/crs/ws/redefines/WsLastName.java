/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-LAST-NAME<br>
 * Variable: WS-LAST-NAME from program CIWBNSRB<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsLastName extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int FLR1_MAXOCCURS = 60;

	//==== CONSTRUCTORS ====
	public WsLastName() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_LAST_NAME;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "", Len.WS_LAST_NAME);
	}

	public void setWsLastName(String wsLastName) {
		writeString(Pos.WS_LAST_NAME, wsLastName, Len.WS_LAST_NAME);
	}

	/**Original name: WS-LAST-NAME<br>*/
	public String getWsLastName() {
		return readString(Pos.WS_LAST_NAME, Len.WS_LAST_NAME);
	}

	/**Original name: WS-LAST-NAME-BYTE<br>*/
	public char getNameByte(int nameByteIdx) {
		int position = Pos.wsLastNameByte(nameByteIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_LAST_NAME = 1;
		public static final int WS_LAST_NAME_TABLE = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int flr1(int idx) {
			return WS_LAST_NAME_TABLE + idx * Len.FLR1;
		}

		public static int wsLastNameByte(int idx) {
			return flr1(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int NAME_BYTE = 1;
		public static final int FLR1 = NAME_BYTE;
		public static final int WS_LAST_NAME = 60;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
