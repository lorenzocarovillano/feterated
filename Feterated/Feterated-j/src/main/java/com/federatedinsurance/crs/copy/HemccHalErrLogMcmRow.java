/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: HEMCC-HAL-ERR-LOG-MCM-ROW<br>
 * Variable: HEMCC-HAL-ERR-LOG-MCM-ROW from copybook HALLCEMC<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class HemccHalErrLogMcmRow {

	//==== PROPERTIES ====
	//Original name: HEMCC-HAL-ERR-LOG-MCM-CHK-SUM
	private String halErrLogMcmChkSum = DefaultValues.stringVal(Len.HAL_ERR_LOG_MCM_CHK_SUM);
	//Original name: HEMCC-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: HEMCC-UOW-ID-CI
	private char uowIdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-UOW-ID
	private String uowId = DefaultValues.stringVal(Len.UOW_ID);
	//Original name: HEMCC-FAIL-TS-CI
	private char failTsCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-FAIL-TS
	private String failTs = DefaultValues.stringVal(Len.FAIL_TS);
	//Original name: HEMCC-HAL-ERR-LOG-MCM-DATA
	private HemccHalErrLogMcmData halErrLogMcmData = new HemccHalErrLogMcmData();

	//==== METHODS ====
	public String getHemccHalErrLogMcmRowFormatted() {
		return MarshalByteExt.bufferToStr(getHemccHalErrLogMcmRowBytes());
	}

	public byte[] getHemccHalErrLogMcmRowBytes() {
		byte[] buffer = new byte[Len.HEMCC_HAL_ERR_LOG_MCM_ROW];
		return getHemccHalErrLogMcmRowBytes(buffer, 1);
	}

	public byte[] getHemccHalErrLogMcmRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getHalErrLogMcmFixedBytes(buffer, position);
		position += Len.HAL_ERR_LOG_MCM_FIXED;
		getHalErrLogMcmDatesBytes(buffer, position);
		position += Len.HAL_ERR_LOG_MCM_DATES;
		getHalErrLogMcmKeyBytes(buffer, position);
		position += Len.HAL_ERR_LOG_MCM_KEY;
		halErrLogMcmData.getHalErrLogMcmDataBytes(buffer, position);
		return buffer;
	}

	public void initHemccHalErrLogMcmRowSpaces() {
		initHalErrLogMcmFixedSpaces();
		initHalErrLogMcmDatesSpaces();
		initHalErrLogMcmKeySpaces();
		halErrLogMcmData.initHalErrLogMcmDataSpaces();
	}

	public byte[] getHalErrLogMcmFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, halErrLogMcmChkSum, Len.HAL_ERR_LOG_MCM_CHK_SUM);
		return buffer;
	}

	public void initHalErrLogMcmFixedSpaces() {
		halErrLogMcmChkSum = "";
	}

	public byte[] getHalErrLogMcmDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void initHalErrLogMcmDatesSpaces() {
		transProcessDt = "";
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public byte[] getHalErrLogMcmKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, uowIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, uowId, Len.UOW_ID);
		position += Len.UOW_ID;
		MarshalByte.writeChar(buffer, position, failTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, failTs, Len.FAIL_TS);
		return buffer;
	}

	public void initHalErrLogMcmKeySpaces() {
		uowIdCi = Types.SPACE_CHAR;
		uowId = "";
		failTsCi = Types.SPACE_CHAR;
		failTs = "";
	}

	public char getUowIdCi() {
		return this.uowIdCi;
	}

	public void setUowId(String uowId) {
		this.uowId = Functions.subString(uowId, Len.UOW_ID);
	}

	public String getUowId() {
		return this.uowId;
	}

	public char getFailTsCi() {
		return this.failTsCi;
	}

	public void setFailTs(String failTs) {
		this.failTs = Functions.subString(failTs, Len.FAIL_TS);
	}

	public String getFailTs() {
		return this.failTs;
	}

	public HemccHalErrLogMcmData getHalErrLogMcmData() {
		return halErrLogMcmData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HAL_ERR_LOG_MCM_CHK_SUM = 9;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int UOW_ID = 32;
		public static final int FAIL_TS = 26;
		public static final int HAL_ERR_LOG_MCM_FIXED = HAL_ERR_LOG_MCM_CHK_SUM;
		public static final int HAL_ERR_LOG_MCM_DATES = TRANS_PROCESS_DT;
		public static final int UOW_ID_CI = 1;
		public static final int FAIL_TS_CI = 1;
		public static final int HAL_ERR_LOG_MCM_KEY = UOW_ID_CI + UOW_ID + FAIL_TS_CI + FAIL_TS;
		public static final int HEMCC_HAL_ERR_LOG_MCM_ROW = HAL_ERR_LOG_MCM_FIXED + HAL_ERR_LOG_MCM_DATES + HAL_ERR_LOG_MCM_KEY
				+ HemccHalErrLogMcmData.Len.HAL_ERR_LOG_MCM_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
