/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXzc02090 {

	//==== PROPERTIES ====
	//Original name: CF-DB2-IS-NULL
	private short db2IsNull = ((short) -1);
	//Original name: CF-SQL-GOOD
	private int sqlGood = 0;
	//Original name: CF-SQL-NOT-FOUND
	private int sqlNotFound = 100;
	//Original name: CF-PROGRAM-NAME
	private String programName = "XZC02090";
	//Original name: CF-PARAGRAPH-NAMES
	private CfParagraphNamesXzc02090 paragraphNames = new CfParagraphNamesXzc02090();
	//Original name: CF-PEOPLE-SEARCH-MODULE
	private String peopleSearchModule = "TT008099";
	//Original name: CF-YES
	private char yes = 'Y';
	//Original name: CF-NO
	private char no = 'N';
	//Original name: CF-CONVERTED-EMP-ID
	private String convertedEmpId = "FXZ701";
	//Original name: CF-CONVERTED-TMN
	private String convertedTmn = "CONVERTED";

	//==== METHODS ====
	public short getDb2IsNull() {
		return this.db2IsNull;
	}

	public int getSqlGood() {
		return this.sqlGood;
	}

	public int getSqlNotFound() {
		return this.sqlNotFound;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getPeopleSearchModule() {
		return this.peopleSearchModule;
	}

	public char getYes() {
		return this.yes;
	}

	public char getNo() {
		return this.no;
	}

	public String getConvertedEmpId() {
		return this.convertedEmpId;
	}

	public String getConvertedTmn() {
		return this.convertedTmn;
	}

	public CfParagraphNamesXzc02090 getParagraphNames() {
		return paragraphNames;
	}
}
