/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC05I-STA-CD-LIS<br>
 * Variables: XZC05I-STA-CD-LIS from copybook XZC050C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzc05iStaCdLis {

	//==== PROPERTIES ====
	//Original name: XZC05I-STA-CD
	private String xzc05iStaCd = DefaultValues.stringVal(Len.XZC05I_STA_CD);

	//==== METHODS ====
	public void setiStaCdLisBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc05iStaCd = MarshalByte.readString(buffer, position, Len.XZC05I_STA_CD);
	}

	public byte[] getiStaCdLisBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc05iStaCd, Len.XZC05I_STA_CD);
		return buffer;
	}

	public void initiStaCdLisSpaces() {
		xzc05iStaCd = "";
	}

	public void setXzc05iStaCd(String xzc05iStaCd) {
		this.xzc05iStaCd = Functions.subString(xzc05iStaCd, Len.XZC05I_STA_CD);
	}

	public String getXzc05iStaCd() {
		return this.xzc05iStaCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC05I_STA_CD = 2;
		public static final int I_STA_CD_LIS = XZC05I_STA_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
