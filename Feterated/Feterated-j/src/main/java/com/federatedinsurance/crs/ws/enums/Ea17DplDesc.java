/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-17-DPL-DESC<br>
 * Variable: EA-17-DPL-DESC from program TS547099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ea17DplDesc {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.EA17_DPL_DESC);
	public static final String LEN_ERR = "COMMAREA LEGNTH ERROR.";
	public static final String PGM_ID_ERR = "PROGRAM ID ERROR.";
	public static final String SYSID_ERR = "SYSTEM ID ERROR.";
	public static final String NOT_AUTH = "NOT AUTHORIZED.";
	public static final String TERM_ERR = "TERMINAL ERROR.";
	public static final String ROLLED_BACK = "UNABLE TO SYNC.";

	//==== METHODS ====
	public void setEa17DplDesc(String ea17DplDesc) {
		this.value = Functions.subString(ea17DplDesc, Len.EA17_DPL_DESC);
	}

	public String getEa17DplDesc() {
		return this.value;
	}

	public void setLenErr() {
		value = LEN_ERR;
	}

	public void setPgmIdErr() {
		value = PGM_ID_ERR;
	}

	public void setSysidErr() {
		value = SYSID_ERR;
	}

	public void setNotAuth() {
		value = NOT_AUTH;
	}

	public void setTermErr() {
		value = TERM_ERR;
	}

	public void setRolledBack() {
		value = ROLLED_BACK;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA17_DPL_DESC = 25;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
