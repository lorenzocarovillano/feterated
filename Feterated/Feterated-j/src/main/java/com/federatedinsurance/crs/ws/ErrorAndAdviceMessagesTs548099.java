/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesTs548099 {

	//==== PROPERTIES ====
	//Original name: EA-01-PARM-NOT-FOUND
	private Ea01ParmNotFound ea01ParmNotFound = new Ea01ParmNotFound();
	//Original name: EA-02-FUNCTION-INVALID
	private Ea02FunctionInvalid ea02FunctionInvalid = new Ea02FunctionInvalid();
	//Original name: EA-03-REGION-SWAPPED
	private Ea03RegionSwapped ea03RegionSwapped = new Ea03RegionSwapped();
	//Original name: EA-20-GEN-WARNING-MSG
	private Ea20GenWarningMsg ea20GenWarningMsg = new Ea20GenWarningMsg();
	//Original name: EA-21-WNG-PIPE-OPEN-MSG
	private Ea21WngPipeOpenMsg ea21WngPipeOpenMsg = new Ea21WngPipeOpenMsg();
	//Original name: EA-22-WNG-PIPE-CLOSED-MSG
	private Ea22WngPipeClosedMsg ea22WngPipeClosedMsg = new Ea22WngPipeClosedMsg();
	//Original name: EA-40-GEN-RETRYABLE-MSG
	private Ea40GenRetryableMsg ea40GenRetryableMsg = new Ea40GenRetryableMsg();
	//Original name: EA-41-RTY-NO-FREE-SESSIONS-MSG
	private Ea41RtyNoFreeSessionsMsg ea41RtyNoFreeSessionsMsg = new Ea41RtyNoFreeSessionsMsg();
	//Original name: EA-42-RTY-NO-CONNECTION-MSG
	private Ea42RtyNoConnectionMsg ea42RtyNoConnectionMsg = new Ea42RtyNoConnectionMsg();
	//Original name: EA-60-GEN-USER-ERROR-MSG
	private Ea60GenUserErrorMsg ea60GenUserErrorMsg = new Ea60GenUserErrorMsg();
	//Original name: EA-61-USR-INVALID-APPL-ID-MSG
	private Ea61UsrInvalidApplIdMsg ea61UsrInvalidApplIdMsg = new Ea61UsrInvalidApplIdMsg();
	//Original name: EA-62-USR-INVALID-USR-TKN-MSG
	private Ea62UsrInvalidUsrTknMsg ea62UsrInvalidUsrTknMsg = new Ea62UsrInvalidUsrTknMsg();
	//Original name: EA-63-USR-PIPE-NOT-CLOSED-MSG
	private Ea63UsrPipeNotClosedMsg ea63UsrPipeNotClosedMsg = new Ea63UsrPipeNotClosedMsg();
	//Original name: EA-64-USR-PIPE-NOT-OPEN-MSG
	private Ea64UsrPipeNotOpenMsg ea64UsrPipeNotOpenMsg = new Ea64UsrPipeNotOpenMsg();
	//Original name: EA-65-USR-INVALID-PIP-TKN-MSG
	private Ea65UsrInvalidPipTknMsg ea65UsrInvalidPipTknMsg = new Ea65UsrInvalidPipTknMsg();
	//Original name: EA-66-USR-DFHXCOPT-LOAD-MSG
	private Ea66UsrDfhxcoptLoadMsg ea66UsrDfhxcoptLoadMsg = new Ea66UsrDfhxcoptLoadMsg();
	//Original name: EA-80-GEN-SYSTEM-ERR-MSG
	private Ea80GenSystemErrMsg ea80GenSystemErrMsg = new Ea80GenSystemErrMsg();
	//Original name: EA-81-SYS-CALL-CICS-FAIL-MSG
	private Ea81SysCallCicsFailMsg ea81SysCallCicsFailMsg = new Ea81SysCallCicsFailMsg();
	//Original name: EA-82-SYS-CONNECTION-FAIL-MSG
	private Ea82SysConnectionFailMsg ea82SysConnectionFailMsg = new Ea82SysConnectionFailMsg();

	//==== METHODS ====
	public Ea01ParmNotFound getEa01ParmNotFound() {
		return ea01ParmNotFound;
	}

	public Ea02FunctionInvalid getEa02FunctionInvalid() {
		return ea02FunctionInvalid;
	}

	public Ea03RegionSwapped getEa03RegionSwapped() {
		return ea03RegionSwapped;
	}

	public Ea20GenWarningMsg getEa20GenWarningMsg() {
		return ea20GenWarningMsg;
	}

	public Ea21WngPipeOpenMsg getEa21WngPipeOpenMsg() {
		return ea21WngPipeOpenMsg;
	}

	public Ea22WngPipeClosedMsg getEa22WngPipeClosedMsg() {
		return ea22WngPipeClosedMsg;
	}

	public Ea40GenRetryableMsg getEa40GenRetryableMsg() {
		return ea40GenRetryableMsg;
	}

	public Ea41RtyNoFreeSessionsMsg getEa41RtyNoFreeSessionsMsg() {
		return ea41RtyNoFreeSessionsMsg;
	}

	public Ea42RtyNoConnectionMsg getEa42RtyNoConnectionMsg() {
		return ea42RtyNoConnectionMsg;
	}

	public Ea60GenUserErrorMsg getEa60GenUserErrorMsg() {
		return ea60GenUserErrorMsg;
	}

	public Ea61UsrInvalidApplIdMsg getEa61UsrInvalidApplIdMsg() {
		return ea61UsrInvalidApplIdMsg;
	}

	public Ea62UsrInvalidUsrTknMsg getEa62UsrInvalidUsrTknMsg() {
		return ea62UsrInvalidUsrTknMsg;
	}

	public Ea63UsrPipeNotClosedMsg getEa63UsrPipeNotClosedMsg() {
		return ea63UsrPipeNotClosedMsg;
	}

	public Ea64UsrPipeNotOpenMsg getEa64UsrPipeNotOpenMsg() {
		return ea64UsrPipeNotOpenMsg;
	}

	public Ea65UsrInvalidPipTknMsg getEa65UsrInvalidPipTknMsg() {
		return ea65UsrInvalidPipTknMsg;
	}

	public Ea66UsrDfhxcoptLoadMsg getEa66UsrDfhxcoptLoadMsg() {
		return ea66UsrDfhxcoptLoadMsg;
	}

	public Ea80GenSystemErrMsg getEa80GenSystemErrMsg() {
		return ea80GenSystemErrMsg;
	}

	public Ea81SysCallCicsFailMsg getEa81SysCallCicsFailMsg() {
		return ea81SysCallCicsFailMsg;
	}

	public Ea82SysConnectionFailMsg getEa82SysConnectionFailMsg() {
		return ea82SysConnectionFailMsg;
	}
}
