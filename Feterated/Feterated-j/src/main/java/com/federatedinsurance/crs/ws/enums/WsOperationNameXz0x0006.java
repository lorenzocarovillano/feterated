/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-OPERATION-NAME<br>
 * Variable: WS-OPERATION-NAME from program XZ0X0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsOperationNameXz0x0006 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.WS_OPERATION_NAME);
	public static final String ADD_ACT_NOT = "AddAccountNotification";
	public static final String UPDATE_ACT_NOT = "UpdateAccountNotification";
	public static final String DELETE_ACT_NOT = "DeleteAccountNotification";
	private static final String[] VALID_OPERATION = new String[] { "AddAccountNotification", "UpdateAccountNotification",
			"DeleteAccountNotification" };

	//==== METHODS ====
	public void setWsOperationName(String wsOperationName) {
		this.value = Functions.subString(wsOperationName, Len.WS_OPERATION_NAME);
	}

	public String getWsOperationName() {
		return this.value;
	}

	public boolean isDeleteActNot() {
		return value.equals(DELETE_ACT_NOT);
	}

	public boolean isValidOperation() {
		return ArrayUtils.contains(VALID_OPERATION, value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_OPERATION_NAME = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
