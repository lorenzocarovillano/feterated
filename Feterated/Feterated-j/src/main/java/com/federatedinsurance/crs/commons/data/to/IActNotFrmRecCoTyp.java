/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ACT_NOT_FRM_REC, ACT_NOT_REC, REC_CO_TYP]
 * 
 */
public interface IActNotFrmRecCoTyp extends BaseSqlTo {

};
