/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90S0-ROW<br>
 * Variable: WS-XZ0A90S0-ROW from program XZ0B90S0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90s0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9S0-MAX-ROWS-RETURNED
	private short maxRowsReturned = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA9S0-PRIMARY-FUNCTION
	private String primaryFunction = DefaultValues.stringVal(Len.PRIMARY_FUNCTION);
	public static final String PF_BY_ACT = "GetAcyPndCncTmnPolListByAct";
	//Original name: XZA9S0-ACT-NBR
	private String actNbr = DefaultValues.stringVal(Len.ACT_NBR);
	//Original name: XZA9S0-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90S0_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90s0RowBytes(buf);
	}

	public String getWsXz0a90s0RowFormatted() {
		return getPolicyLocateRowFormatted();
	}

	public void setWsXz0a90s0RowBytes(byte[] buffer) {
		setWsXz0a90s0RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90s0RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90S0_ROW];
		return getWsXz0a90s0RowBytes(buffer, 1);
	}

	public void setWsXz0a90s0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setPolicyLocateRowBytes(buffer, position);
	}

	public byte[] getWsXz0a90s0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getPolicyLocateRowBytes(buffer, position);
		return buffer;
	}

	public String getPolicyLocateRowFormatted() {
		return getPolicyInputDataFormatted();
	}

	public void setPolicyLocateRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setPolicyInputDataBytes(buffer, position);
	}

	public byte[] getPolicyLocateRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getPolicyInputDataBytes(buffer, position);
		return buffer;
	}

	public String getPolicyInputDataFormatted() {
		return MarshalByteExt.bufferToStr(getPolicyInputDataBytes());
	}

	/**Original name: XZA9S0-POLICY-INPUT-DATA<br>*/
	public byte[] getPolicyInputDataBytes() {
		byte[] buffer = new byte[Len.POLICY_INPUT_DATA];
		return getPolicyInputDataBytes(buffer, 1);
	}

	public void setPolicyInputDataBytes(byte[] buffer, int offset) {
		int position = offset;
		maxRowsReturned = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		primaryFunction = MarshalByte.readString(buffer, position, Len.PRIMARY_FUNCTION);
		position += Len.PRIMARY_FUNCTION;
		actNbr = MarshalByte.readString(buffer, position, Len.ACT_NBR);
		position += Len.ACT_NBR;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
	}

	public byte[] getPolicyInputDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxRowsReturned);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, primaryFunction, Len.PRIMARY_FUNCTION);
		position += Len.PRIMARY_FUNCTION;
		MarshalByte.writeString(buffer, position, actNbr, Len.ACT_NBR);
		position += Len.ACT_NBR;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setMaxRowsReturned(short maxRowsReturned) {
		this.maxRowsReturned = maxRowsReturned;
	}

	public short getMaxRowsReturned() {
		return this.maxRowsReturned;
	}

	public void setPrimaryFunction(String primaryFunction) {
		this.primaryFunction = Functions.subString(primaryFunction, Len.PRIMARY_FUNCTION);
	}

	public String getPrimaryFunction() {
		return this.primaryFunction;
	}

	public void setActNbr(String actNbr) {
		this.actNbr = Functions.subString(actNbr, Len.ACT_NBR);
	}

	public String getActNbr() {
		return this.actNbr;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90s0RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_ROWS_RETURNED = 2;
		public static final int PRIMARY_FUNCTION = 32;
		public static final int ACT_NBR = 9;
		public static final int USERID = 9;
		public static final int POLICY_INPUT_DATA = MAX_ROWS_RETURNED + PRIMARY_FUNCTION + ACT_NBR + USERID;
		public static final int POLICY_LOCATE_ROW = POLICY_INPUT_DATA;
		public static final int WS_XZ0A90S0_ROW = POLICY_LOCATE_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
