/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WS-OPERATIONS-CALLED<br>
 * Variable: WS-OPERATIONS-CALLED from program XZ0P90A0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsOperationsCalledXz0p90a0 {

	//==== PROPERTIES ====
	//Original name: WS-ADD-ACT-NOT-REC
	private String addActNotRec = "AddAccountNotificationRecipient";
	//Original name: WS-GET-TTY-LIST
	private String getTtyList = "GetThirdPartyList";
	//Original name: WS-GET-TTY-CERT-LIST
	private String getTtyCertList = "GetThirdPartyCertList";

	//==== METHODS ====
	public String getAddActNotRec() {
		return this.addActNotRec;
	}

	public String getGetTtyList() {
		return this.getTtyList;
	}

	public String getGetTtyCertList() {
		return this.getTtyCertList;
	}
}
