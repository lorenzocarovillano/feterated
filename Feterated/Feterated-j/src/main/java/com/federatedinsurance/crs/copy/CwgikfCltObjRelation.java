/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CWGIKF-CLT-OBJ-RELATION<br>
 * Variable: CWGIKF-CLT-OBJ-RELATION from copybook CAWLFGIK<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CwgikfCltObjRelation {

	//==== PROPERTIES ====
	//Original name: CWGIKF-TCH-OBJECT-KEY
	private String tchObjectKey = DefaultValues.stringVal(Len.TCH_OBJECT_KEY);
	//Original name: CWGIKF-RLT-TYP-CD
	private String rltTypCd = DefaultValues.stringVal(Len.RLT_TYP_CD);
	//Original name: CWGIKF-OBJ-CD
	private String objCd = DefaultValues.stringVal(Len.OBJ_CD);
	//Original name: CWGIKF-SHW-OBJ-KEY
	private String shwObjKey = DefaultValues.stringVal(Len.SHW_OBJ_KEY);
	//Original name: CWGIKF-OBJ-SYS-ID
	private String objSysId = DefaultValues.stringVal(Len.OBJ_SYS_ID);

	//==== METHODS ====
	public void setCltObjRelationBytes(byte[] buffer, int offset) {
		int position = offset;
		tchObjectKey = MarshalByte.readString(buffer, position, Len.TCH_OBJECT_KEY);
		position += Len.TCH_OBJECT_KEY;
		rltTypCd = MarshalByte.readString(buffer, position, Len.RLT_TYP_CD);
		position += Len.RLT_TYP_CD;
		objCd = MarshalByte.readString(buffer, position, Len.OBJ_CD);
		position += Len.OBJ_CD;
		shwObjKey = MarshalByte.readString(buffer, position, Len.SHW_OBJ_KEY);
		position += Len.SHW_OBJ_KEY;
		objSysId = MarshalByte.readString(buffer, position, Len.OBJ_SYS_ID);
	}

	public byte[] getCltObjRelationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tchObjectKey, Len.TCH_OBJECT_KEY);
		position += Len.TCH_OBJECT_KEY;
		MarshalByte.writeString(buffer, position, rltTypCd, Len.RLT_TYP_CD);
		position += Len.RLT_TYP_CD;
		MarshalByte.writeString(buffer, position, objCd, Len.OBJ_CD);
		position += Len.OBJ_CD;
		MarshalByte.writeString(buffer, position, shwObjKey, Len.SHW_OBJ_KEY);
		position += Len.SHW_OBJ_KEY;
		MarshalByte.writeString(buffer, position, objSysId, Len.OBJ_SYS_ID);
		return buffer;
	}

	public void setTchObjectKey(String tchObjectKey) {
		this.tchObjectKey = Functions.subString(tchObjectKey, Len.TCH_OBJECT_KEY);
	}

	public String getTchObjectKey() {
		return this.tchObjectKey;
	}

	public void setRltTypCd(String rltTypCd) {
		this.rltTypCd = Functions.subString(rltTypCd, Len.RLT_TYP_CD);
	}

	public String getRltTypCd() {
		return this.rltTypCd;
	}

	public void setObjCd(String objCd) {
		this.objCd = Functions.subString(objCd, Len.OBJ_CD);
	}

	public String getObjCd() {
		return this.objCd;
	}

	public void setShwObjKey(String shwObjKey) {
		this.shwObjKey = Functions.subString(shwObjKey, Len.SHW_OBJ_KEY);
	}

	public String getShwObjKey() {
		return this.shwObjKey;
	}

	public void setObjSysId(String objSysId) {
		this.objSysId = Functions.subString(objSysId, Len.OBJ_SYS_ID);
	}

	public String getObjSysId() {
		return this.objSysId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TCH_OBJECT_KEY = 20;
		public static final int RLT_TYP_CD = 4;
		public static final int OBJ_CD = 4;
		public static final int SHW_OBJ_KEY = 30;
		public static final int OBJ_SYS_ID = 4;
		public static final int CLT_OBJ_RELATION = TCH_OBJECT_KEY + RLT_TYP_CD + OBJ_CD + SHW_OBJ_KEY + OBJ_SYS_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
