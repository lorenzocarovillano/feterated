/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-FORMATTED-FEIN<br>
 * Variable: SA-FORMATTED-FEIN from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaFormattedFein {

	//==== PROPERTIES ====
	//Original name: SA-FF-FIRST-2
	private String first2 = DefaultValues.stringVal(Len.FIRST2);
	//Original name: FILLER-SA-FORMATTED-FEIN
	private char flr1 = '-';
	//Original name: SA-FF-LAST-7
	private String last7 = DefaultValues.stringVal(Len.LAST7);

	//==== METHODS ====
	public String getFormattedFeinFormatted() {
		return MarshalByteExt.bufferToStr(getFormattedFeinBytes());
	}

	public byte[] getFormattedFeinBytes() {
		byte[] buffer = new byte[Len.FORMATTED_FEIN];
		return getFormattedFeinBytes(buffer, 1);
	}

	public byte[] getFormattedFeinBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, first2, Len.FIRST2);
		position += Len.FIRST2;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, last7, Len.LAST7);
		return buffer;
	}

	public void setFirst2(String first2) {
		this.first2 = Functions.subString(first2, Len.FIRST2);
	}

	public String getFirst2() {
		return this.first2;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setLast7(String last7) {
		this.last7 = Functions.subString(last7, Len.LAST7);
	}

	public String getLast7() {
		return this.last7;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FIRST2 = 2;
		public static final int LAST7 = 7;
		public static final int FLR1 = 1;
		public static final int FORMATTED_FEIN = FIRST2 + LAST7 + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
