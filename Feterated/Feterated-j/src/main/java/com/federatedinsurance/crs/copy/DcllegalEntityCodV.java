/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLLEGAL-ENTITY-COD-V<br>
 * Variable: DCLLEGAL-ENTITY-COD-V from copybook MUH00044<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DcllegalEntityCodV {

	//==== PROPERTIES ====
	//Original name: LEG-ENT-CD
	private String legEntCd = DefaultValues.stringVal(Len.LEG_ENT_CD);
	//Original name: CTR-NBR-CD
	private short ctrNbrCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: LEG-ENT-DES
	private String legEntDes = DefaultValues.stringVal(Len.LEG_ENT_DES);

	//==== METHODS ====
	public void setLegEntCd(String legEntCd) {
		this.legEntCd = Functions.subString(legEntCd, Len.LEG_ENT_CD);
	}

	public String getLegEntCd() {
		return this.legEntCd;
	}

	public String getLegEntCdFormatted() {
		return Functions.padBlanks(getLegEntCd(), Len.LEG_ENT_CD);
	}

	public void setCtrNbrCd(short ctrNbrCd) {
		this.ctrNbrCd = ctrNbrCd;
	}

	public short getCtrNbrCd() {
		return this.ctrNbrCd;
	}

	public void setLegEntDes(String legEntDes) {
		this.legEntDes = Functions.subString(legEntDes, Len.LEG_ENT_DES);
	}

	public String getLegEntDes() {
		return this.legEntDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LEG_ENT_CD = 10;
		public static final int LEG_ENT_DES = 40;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
