/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-15-DB2-UPDATE-ERROR<br>
 * Variable: EA-15-DB2-UPDATE-ERROR from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea15Db2UpdateError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-15-DB2-UPDATE-ERROR
	private String flr1 = "XZ001000 -";
	//Original name: FILLER-EA-15-DB2-UPDATE-ERROR-1
	private String flr2 = "UPDATE";
	//Original name: FILLER-EA-15-DB2-UPDATE-ERROR-2
	private String flr3 = " ERROR:";
	//Original name: FILLER-EA-15-DB2-UPDATE-ERROR-3
	private String flr4 = " PARAGRAPH #";
	//Original name: EA-15-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-15-DB2-UPDATE-ERROR-4
	private String flr5 = " ACCT=";
	//Original name: EA-15-ACCOUNT-NBR
	private String accountNbr = DefaultValues.stringVal(Len.ACCOUNT_NBR);
	//Original name: FILLER-EA-15-DB2-UPDATE-ERROR-5
	private String flr6 = " POL=";
	//Original name: EA-15-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: FILLER-EA-15-DB2-UPDATE-ERROR-6
	private String flr7 = " TS=";
	//Original name: EA-15-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-15-DB2-UPDATE-ERROR-7
	private String flr8 = " SQLCODE=";
	//Original name: EA-15-SQLCODE
	private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);

	//==== METHODS ====
	public String getEa15Db2UpdateErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa15Db2UpdateErrorBytes());
	}

	public byte[] getEa15Db2UpdateErrorBytes() {
		byte[] buffer = new byte[Len.EA15_DB2_UPDATE_ERROR];
		return getEa15Db2UpdateErrorBytes(buffer, 1);
	}

	public byte[] getEa15Db2UpdateErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, accountNbr, Len.ACCOUNT_NBR);
		position += Len.ACCOUNT_NBR;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		position += Len.FLR8;
		MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setAccountNbr(String accountNbr) {
		this.accountNbr = Functions.subString(accountNbr, Len.ACCOUNT_NBR);
	}

	public String getAccountNbr() {
		return this.accountNbr;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setSqlcode(long sqlcode) {
		this.sqlcode = PicFormatter.display("++(4)9").format(sqlcode).toString();
	}

	public long getSqlcode() {
		return PicParser.display("++(4)9").parseLong(this.sqlcode);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PARAGRAPH_NBR = 5;
		public static final int ACCOUNT_NBR = 9;
		public static final int POL_NBR = 7;
		public static final int NOT_PRC_TS = 26;
		public static final int SQLCODE = 6;
		public static final int FLR1 = 11;
		public static final int FLR2 = 6;
		public static final int FLR3 = 7;
		public static final int FLR4 = 12;
		public static final int FLR7 = 5;
		public static final int FLR8 = 9;
		public static final int EA15_DB2_UPDATE_ERROR = PARAGRAPH_NBR + ACCOUNT_NBR + POL_NBR + NOT_PRC_TS + SQLCODE + FLR1 + 2 * FLR2 + 2 * FLR3
				+ FLR4 + FLR7 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
