/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW49F-FED-SEG-INFO-ROW<br>
 * Variable: CW49F-FED-SEG-INFO-ROW from copybook CAWLF049<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw49fFedSegInfoRow {

	//==== PROPERTIES ====
	//Original name: CW49F-FED-SEG-INFO-CSUM
	private String fedSegInfoCsum = DefaultValues.stringVal(Len.FED_SEG_INFO_CSUM);
	//Original name: CW49F-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CW49F-HISTORY-VLD-NBR-KCRE
	private String historyVldNbrKcre = DefaultValues.stringVal(Len.HISTORY_VLD_NBR_KCRE);
	//Original name: CW49F-EFFECTIVE-DT-KCRE
	private String effectiveDtKcre = DefaultValues.stringVal(Len.EFFECTIVE_DT_KCRE);
	//Original name: CW49F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW49F-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: CW49F-HISTORY-VLD-NBR-SIGNED
	private String historyVldNbrSigned = DefaultValues.stringVal(Len.HISTORY_VLD_NBR_SIGNED);
	//Original name: CW49F-EFFECTIVE-DT
	private String effectiveDt = DefaultValues.stringVal(Len.EFFECTIVE_DT);
	//Original name: CW49F-CLIENT-ID-CI
	private char clientIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW49F-HISTORY-VLD-NBR-CI
	private char historyVldNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW49F-EFFECTIVE-DT-CI
	private char effectiveDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW49F-FED-SEG-INFO-DATA
	private Cw49fFedSegInfoData fedSegInfoData = new Cw49fFedSegInfoData();

	//==== METHODS ====
	public void setCw49fFedSegInfoRowFormatted(String data) {
		byte[] buffer = new byte[Len.CW49F_FED_SEG_INFO_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CW49F_FED_SEG_INFO_ROW);
		setCw49fFedSegInfoRowBytes(buffer, 1);
	}

	public String getCw49fFedSegInfoRowFormatted() {
		return MarshalByteExt.bufferToStr(getCw49fFedSegInfoRowBytes());
	}

	public byte[] getCw49fFedSegInfoRowBytes() {
		byte[] buffer = new byte[Len.CW49F_FED_SEG_INFO_ROW];
		return getCw49fFedSegInfoRowBytes(buffer, 1);
	}

	public void setCw49fFedSegInfoRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setFedSegInfoFixedBytes(buffer, position);
		position += Len.FED_SEG_INFO_FIXED;
		setFedSegInfoDatesBytes(buffer, position);
		position += Len.FED_SEG_INFO_DATES;
		setFedSegInfoKeyBytes(buffer, position);
		position += Len.FED_SEG_INFO_KEY;
		setFedSegInfoKeyCiBytes(buffer, position);
		position += Len.FED_SEG_INFO_KEY_CI;
		fedSegInfoData.setFedSegInfoDataBytes(buffer, position);
	}

	public byte[] getCw49fFedSegInfoRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getFedSegInfoFixedBytes(buffer, position);
		position += Len.FED_SEG_INFO_FIXED;
		getFedSegInfoDatesBytes(buffer, position);
		position += Len.FED_SEG_INFO_DATES;
		getFedSegInfoKeyBytes(buffer, position);
		position += Len.FED_SEG_INFO_KEY;
		getFedSegInfoKeyCiBytes(buffer, position);
		position += Len.FED_SEG_INFO_KEY_CI;
		fedSegInfoData.getFedSegInfoDataBytes(buffer, position);
		return buffer;
	}

	public void setFedSegInfoFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		fedSegInfoCsum = MarshalByte.readFixedString(buffer, position, Len.FED_SEG_INFO_CSUM);
		position += Len.FED_SEG_INFO_CSUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		historyVldNbrKcre = MarshalByte.readString(buffer, position, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		effectiveDtKcre = MarshalByte.readString(buffer, position, Len.EFFECTIVE_DT_KCRE);
	}

	public byte[] getFedSegInfoFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, fedSegInfoCsum, Len.FED_SEG_INFO_CSUM);
		position += Len.FED_SEG_INFO_CSUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		MarshalByte.writeString(buffer, position, historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		MarshalByte.writeString(buffer, position, effectiveDtKcre, Len.EFFECTIVE_DT_KCRE);
		return buffer;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setHistoryVldNbrKcre(String historyVldNbrKcre) {
		this.historyVldNbrKcre = Functions.subString(historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
	}

	public String getHistoryVldNbrKcre() {
		return this.historyVldNbrKcre;
	}

	public void setEffectiveDtKcre(String effectiveDtKcre) {
		this.effectiveDtKcre = Functions.subString(effectiveDtKcre, Len.EFFECTIVE_DT_KCRE);
	}

	public String getEffectiveDtKcre() {
		return this.effectiveDtKcre;
	}

	public void setFedSegInfoDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getFedSegInfoDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setFedSegInfoKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		clientId = MarshalByte.readString(buffer, position, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		historyVldNbrSigned = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.HISTORY_VLD_NBR_SIGNED), Len.HISTORY_VLD_NBR_SIGNED);
		position += Len.HISTORY_VLD_NBR_SIGNED;
		effectiveDt = MarshalByte.readString(buffer, position, Len.EFFECTIVE_DT);
	}

	public byte[] getFedSegInfoKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeString(buffer, position, historyVldNbrSigned, Len.HISTORY_VLD_NBR_SIGNED);
		position += Len.HISTORY_VLD_NBR_SIGNED;
		MarshalByte.writeString(buffer, position, effectiveDt, Len.EFFECTIVE_DT);
		return buffer;
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setEffectiveDt(String effectiveDt) {
		this.effectiveDt = Functions.subString(effectiveDt, Len.EFFECTIVE_DT);
	}

	public String getEffectiveDt() {
		return this.effectiveDt;
	}

	public void setFedSegInfoKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		clientIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		effectiveDtCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getFedSegInfoKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, clientIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, historyVldNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, effectiveDtCi);
		return buffer;
	}

	public void setClientIdCi(char clientIdCi) {
		this.clientIdCi = clientIdCi;
	}

	public char getClientIdCi() {
		return this.clientIdCi;
	}

	public void setHistoryVldNbrCi(char historyVldNbrCi) {
		this.historyVldNbrCi = historyVldNbrCi;
	}

	public char getHistoryVldNbrCi() {
		return this.historyVldNbrCi;
	}

	public void setEffectiveDtCi(char effectiveDtCi) {
		this.effectiveDtCi = effectiveDtCi;
	}

	public char getEffectiveDtCi() {
		return this.effectiveDtCi;
	}

	public Cw49fFedSegInfoData getFedSegInfoData() {
		return fedSegInfoData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FED_SEG_INFO_CSUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int HISTORY_VLD_NBR_KCRE = 32;
		public static final int EFFECTIVE_DT_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLIENT_ID = 20;
		public static final int HISTORY_VLD_NBR_SIGNED = 6;
		public static final int EFFECTIVE_DT = 10;
		public static final int FED_SEG_INFO_FIXED = FED_SEG_INFO_CSUM + CLIENT_ID_KCRE + HISTORY_VLD_NBR_KCRE + EFFECTIVE_DT_KCRE;
		public static final int FED_SEG_INFO_DATES = TRANS_PROCESS_DT;
		public static final int FED_SEG_INFO_KEY = CLIENT_ID + HISTORY_VLD_NBR_SIGNED + EFFECTIVE_DT;
		public static final int CLIENT_ID_CI = 1;
		public static final int HISTORY_VLD_NBR_CI = 1;
		public static final int EFFECTIVE_DT_CI = 1;
		public static final int FED_SEG_INFO_KEY_CI = CLIENT_ID_CI + HISTORY_VLD_NBR_CI + EFFECTIVE_DT_CI;
		public static final int CW49F_FED_SEG_INFO_ROW = FED_SEG_INFO_FIXED + FED_SEG_INFO_DATES + FED_SEG_INFO_KEY + FED_SEG_INFO_KEY_CI
				+ Cw49fFedSegInfoData.Len.FED_SEG_INFO_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
