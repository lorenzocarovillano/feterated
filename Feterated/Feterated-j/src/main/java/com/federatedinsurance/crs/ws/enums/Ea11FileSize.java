/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-11-FILE-SIZE<br>
 * Variable: EA-11-FILE-SIZE from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ea11FileSize {

	//==== PROPERTIES ====
	public String value = "000";
	public static final String FILE143_BYTES = "143";
	public static final String FILE144_BYTES = "144";

	//==== METHODS ====
	public void setFileSize(short fileSize) {
		this.value = NumericDisplay.asString(fileSize, Len.FILE_SIZE);
	}

	public void setFileSizeFormatted(String fileSize) {
		this.value = Trunc.toUnsignedNumeric(fileSize, Len.FILE_SIZE);
	}

	public short getFileSize() {
		return NumericDisplay.asShort(this.value);
	}

	public void setFile143Bytes() {
		setFileSizeFormatted(FILE143_BYTES);
	}

	public void setFile144Bytes() {
		setFileSizeFormatted(FILE144_BYTES);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FILE_SIZE = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
