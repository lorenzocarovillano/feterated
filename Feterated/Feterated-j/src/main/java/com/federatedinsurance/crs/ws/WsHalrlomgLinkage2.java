/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.HalrlomgFunction;
import com.federatedinsurance.crs.ws.enums.HalrlomgGrpLokClrSw;
import com.federatedinsurance.crs.ws.enums.HalrlomgLokTypeCd;
import com.federatedinsurance.crs.ws.enums.HalrlomgSuccessInd;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-HALRLOMG-LINKAGE-2<br>
 * Variable: WS-HALRLOMG-LINKAGE-2 from program HALRLOMG<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsHalrlomgLinkage2 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: HALRLOMG-FUNCTION
	private HalrlomgFunction function = new HalrlomgFunction();
	//Original name: HALRLOMG-TCH-KEY
	private String tchKey = DefaultValues.stringVal(Len.TCH_KEY);
	//Original name: HALRLOMG-APP-ID
	private String appId = DefaultValues.stringVal(Len.APP_ID);
	/**Original name: HALRLOMG-TABLE-NM<br>
	 * <pre>**        88 HALRLOMG-APP-BCWS
	 * **           VALUE 'BCWS', 'BCWSACCT', 'BCWSTTY', 'BCWSAGT'.</pre>*/
	private String tableNm = DefaultValues.stringVal(Len.TABLE_NM);
	//Original name: HALRLOMG-LOK-TYPE-CD
	private HalrlomgLokTypeCd lokTypeCd = new HalrlomgLokTypeCd();
	//Original name: HALRLOMG-TSQ-NAME
	private String tsqName = DefaultValues.stringVal(Len.TSQ_NAME);
	//Original name: HALRLOMG-SUCCESS-IND
	private HalrlomgSuccessInd successInd = new HalrlomgSuccessInd();
	//Original name: HALRLOMG-ZAPPED-BY-USERID
	private String zappedByUserid = DefaultValues.stringVal(Len.ZAPPED_BY_USERID);
	//Original name: HALRLOMG-GRP-LOK-CLR-SW
	private HalrlomgGrpLokClrSw grpLokClrSw = new HalrlomgGrpLokClrSw();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_HALRLOMG_LINKAGE2;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsHalrlomgLinkage2Bytes(buf);
	}

	public String getWsHalrlomgLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getWsHalrlomgLinkage2Bytes());
	}

	public void setWsHalrlomgLinkage2Bytes(byte[] buffer) {
		setWsHalrlomgLinkage2Bytes(buffer, 1);
	}

	public byte[] getWsHalrlomgLinkage2Bytes() {
		byte[] buffer = new byte[Len.WS_HALRLOMG_LINKAGE2];
		return getWsHalrlomgLinkage2Bytes(buffer, 1);
	}

	public void setWsHalrlomgLinkage2Bytes(byte[] buffer, int offset) {
		int position = offset;
		setInputLinkageBytes(buffer, position);
		position += Len.INPUT_LINKAGE;
		setOutputLinkageBytes(buffer, position);
	}

	public byte[] getWsHalrlomgLinkage2Bytes(byte[] buffer, int offset) {
		int position = offset;
		getInputLinkageBytes(buffer, position);
		position += Len.INPUT_LINKAGE;
		getOutputLinkageBytes(buffer, position);
		return buffer;
	}

	public void setInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		function.setFunction(MarshalByte.readString(buffer, position, HalrlomgFunction.Len.FUNCTION));
		position += HalrlomgFunction.Len.FUNCTION;
		tchKey = MarshalByte.readString(buffer, position, Len.TCH_KEY);
		position += Len.TCH_KEY;
		appId = MarshalByte.readString(buffer, position, Len.APP_ID);
		position += Len.APP_ID;
		tableNm = MarshalByte.readString(buffer, position, Len.TABLE_NM);
		position += Len.TABLE_NM;
		lokTypeCd.setLokTypeCd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		tsqName = MarshalByte.readString(buffer, position, Len.TSQ_NAME);
	}

	public byte[] getInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, function.getFunction(), HalrlomgFunction.Len.FUNCTION);
		position += HalrlomgFunction.Len.FUNCTION;
		MarshalByte.writeString(buffer, position, tchKey, Len.TCH_KEY);
		position += Len.TCH_KEY;
		MarshalByte.writeString(buffer, position, appId, Len.APP_ID);
		position += Len.APP_ID;
		MarshalByte.writeString(buffer, position, tableNm, Len.TABLE_NM);
		position += Len.TABLE_NM;
		MarshalByte.writeChar(buffer, position, lokTypeCd.getLokTypeCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tsqName, Len.TSQ_NAME);
		return buffer;
	}

	public void setTchKey(String tchKey) {
		this.tchKey = Functions.subString(tchKey, Len.TCH_KEY);
	}

	public String getTchKey() {
		return this.tchKey;
	}

	public void setAppId(String appId) {
		this.appId = Functions.subString(appId, Len.APP_ID);
	}

	public String getAppId() {
		return this.appId;
	}

	public void setTableNm(String tableNm) {
		this.tableNm = Functions.subString(tableNm, Len.TABLE_NM);
	}

	public String getTableNm() {
		return this.tableNm;
	}

	public void setTsqName(String tsqName) {
		this.tsqName = Functions.subString(tsqName, Len.TSQ_NAME);
	}

	public String getTsqName() {
		return this.tsqName;
	}

	public String getTsqNameFormatted() {
		return Functions.padBlanks(getTsqName(), Len.TSQ_NAME);
	}

	public void setOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		successInd.setSuccessInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		zappedByUserid = MarshalByte.readString(buffer, position, Len.ZAPPED_BY_USERID);
		position += Len.ZAPPED_BY_USERID;
		grpLokClrSw.setGrpLokClrSw(MarshalByte.readChar(buffer, position));
	}

	public byte[] getOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, successInd.getSuccessInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, zappedByUserid, Len.ZAPPED_BY_USERID);
		position += Len.ZAPPED_BY_USERID;
		MarshalByte.writeChar(buffer, position, grpLokClrSw.getGrpLokClrSw());
		return buffer;
	}

	public void setZappedByUserid(String zappedByUserid) {
		this.zappedByUserid = Functions.subString(zappedByUserid, Len.ZAPPED_BY_USERID);
	}

	public String getZappedByUserid() {
		return this.zappedByUserid;
	}

	public HalrlomgFunction getFunction() {
		return function;
	}

	public HalrlomgGrpLokClrSw getGrpLokClrSw() {
		return grpLokClrSw;
	}

	public HalrlomgLokTypeCd getLokTypeCd() {
		return lokTypeCd;
	}

	public HalrlomgSuccessInd getSuccessInd() {
		return successInd;
	}

	@Override
	public byte[] serialize() {
		return getWsHalrlomgLinkage2Bytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TCH_KEY = 126;
		public static final int APP_ID = 10;
		public static final int TABLE_NM = 18;
		public static final int TSQ_NAME = 8;
		public static final int INPUT_LINKAGE = HalrlomgFunction.Len.FUNCTION + TCH_KEY + APP_ID + TABLE_NM + HalrlomgLokTypeCd.Len.LOK_TYPE_CD
				+ TSQ_NAME;
		public static final int ZAPPED_BY_USERID = 32;
		public static final int OUTPUT_LINKAGE = HalrlomgSuccessInd.Len.SUCCESS_IND + ZAPPED_BY_USERID + HalrlomgGrpLokClrSw.Len.GRP_LOK_CLR_SW;
		public static final int WS_HALRLOMG_LINKAGE2 = INPUT_LINKAGE + OUTPUT_LINKAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
