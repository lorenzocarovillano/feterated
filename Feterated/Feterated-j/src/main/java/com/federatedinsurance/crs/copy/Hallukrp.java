/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.HaloukrpFunction;
import com.federatedinsurance.crs.ws.enums.HaloukrpReturnCode;

/**Original name: HALLUKRP<br>
 * Variable: HALLUKRP from copybook HALLUKRP<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Hallukrp {

	//==== PROPERTIES ====
	//Original name: HALOUKRP-FUNCTION
	private HaloukrpFunction function = new HaloukrpFunction();
	//Original name: HALOUKRP-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	/**Original name: HALOUKRP-KEY-REPL-LABEL<br>
	 * <pre>      07  HALOUKRP-KEY-REPL-LABEL    PIC X(32).</pre>*/
	private String keyReplLabel = DefaultValues.stringVal(Len.KEY_REPL_LABEL);
	//Original name: HALOUKRP-KEY-REPL-KEY-LEN
	private int keyReplKeyLen = DefaultValues.BIN_INT_VAL;
	//Original name: HALOUKRP-KEY-REPL-KEY
	private String keyReplKey = DefaultValues.stringVal(Len.KEY_REPL_KEY);
	//Original name: HALOUKRP-RETURN-CODE
	private HaloukrpReturnCode returnCode = new HaloukrpReturnCode();

	//==== METHODS ====
	public void setInputFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		function.setFunction(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		keyReplLabel = MarshalByte.readString(buffer, position, Len.KEY_REPL_LABEL);
	}

	public byte[] getInputFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, function.getFunction());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, keyReplLabel, Len.KEY_REPL_LABEL);
		return buffer;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setKeyReplLabel(String keyReplLabel) {
		this.keyReplLabel = Functions.subString(keyReplLabel, Len.KEY_REPL_LABEL);
	}

	public String getKeyReplLabel() {
		return this.keyReplLabel;
	}

	public void setInputOutputFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		keyReplKeyLen = MarshalByte.readBinaryUnsignedShort(buffer, position);
		position += Types.SHORT_SIZE;
		keyReplKey = MarshalByte.readString(buffer, position, Len.KEY_REPL_KEY);
		position += Len.KEY_REPL_KEY;
		returnCode.value = MarshalByte.readFixedString(buffer, position, HaloukrpReturnCode.Len.RETURN_CODE);
	}

	public byte[] getInputOutputFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryUnsignedShort(buffer, position, keyReplKeyLen);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, keyReplKey, Len.KEY_REPL_KEY);
		position += Len.KEY_REPL_KEY;
		MarshalByte.writeString(buffer, position, returnCode.value, HaloukrpReturnCode.Len.RETURN_CODE);
		return buffer;
	}

	public void setKeyReplKeyLen(int keyReplKeyLen) {
		this.keyReplKeyLen = keyReplKeyLen;
	}

	public int getKeyReplKeyLen() {
		return this.keyReplKeyLen;
	}

	public void setKeyReplKey(String keyReplKey) {
		this.keyReplKey = Functions.subString(keyReplKey, Len.KEY_REPL_KEY);
	}

	public String getKeyReplKey() {
		return this.keyReplKey;
	}

	public String getKeyReplKeyFormatted() {
		return Functions.padBlanks(getKeyReplKey(), Len.KEY_REPL_KEY);
	}

	public HaloukrpFunction getFunction() {
		return function;
	}

	public HaloukrpReturnCode getReturnCode() {
		return returnCode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUS_OBJ_NM = 32;
		public static final int KEY_REPL_LABEL = 150;
		public static final int KEY_REPL_KEY = 40;
		public static final int INPUT_FIELDS = HaloukrpFunction.Len.FUNCTION + BUS_OBJ_NM + KEY_REPL_LABEL;
		public static final int KEY_REPL_KEY_LEN = 2;
		public static final int INPUT_OUTPUT_FIELDS = KEY_REPL_KEY_LEN + KEY_REPL_KEY + HaloukrpReturnCode.Len.RETURN_CODE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
