/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW02F-CLIENT-TAB-DATA<br>
 * Variable: CW02F-CLIENT-TAB-DATA from copybook CAWLF002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw02fClientTabData {

	//==== PROPERTIES ====
	/**Original name: CW02F-CICL-PRI-SUB-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char ciclPriSubCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-PRI-SUB-CD
	private String ciclPriSubCd = DefaultValues.stringVal(Len.CICL_PRI_SUB_CD);
	//Original name: CW02F-CICL-FST-NM-CI
	private char ciclFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-FST-NM
	private String ciclFstNm = DefaultValues.stringVal(Len.CICL_FST_NM);
	//Original name: CW02F-CICL-LST-NM-CI
	private char ciclLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-LST-NM
	private String ciclLstNm = DefaultValues.stringVal(Len.CICL_LST_NM);
	//Original name: CW02F-CICL-MDL-NM-CI
	private char ciclMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-MDL-NM
	private String ciclMdlNm = DefaultValues.stringVal(Len.CICL_MDL_NM);
	//Original name: CW02F-NM-PFX-CI
	private char nmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-NM-PFX
	private String nmPfx = DefaultValues.stringVal(Len.NM_PFX);
	//Original name: CW02F-NM-SFX-CI
	private char nmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-NM-SFX
	private String nmSfx = DefaultValues.stringVal(Len.NM_SFX);
	//Original name: CW02F-PRIMARY-PRO-DSN-CD-CI
	private char primaryProDsnCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-PRIMARY-PRO-DSN-CD-NI
	private char primaryProDsnCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-PRIMARY-PRO-DSN-CD
	private String primaryProDsnCd = DefaultValues.stringVal(Len.PRIMARY_PRO_DSN_CD);
	//Original name: CW02F-LEG-ENT-CD-CI
	private char legEntCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-LEG-ENT-CD
	private String legEntCd = DefaultValues.stringVal(Len.LEG_ENT_CD);
	//Original name: CW02F-CICL-SDX-CD-CI
	private char ciclSdxCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-SDX-CD
	private String ciclSdxCd = DefaultValues.stringVal(Len.CICL_SDX_CD);
	//Original name: CW02F-CICL-OGN-INCEPT-DT-CI
	private char ciclOgnInceptDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-OGN-INCEPT-DT-NI
	private char ciclOgnInceptDtNi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-OGN-INCEPT-DT
	private String ciclOgnInceptDt = DefaultValues.stringVal(Len.CICL_OGN_INCEPT_DT);
	//Original name: CW02F-CICL-ADD-NM-IND-CI
	private char ciclAddNmIndCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-ADD-NM-IND-NI
	private char ciclAddNmIndNi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-ADD-NM-IND
	private char ciclAddNmInd = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-DOB-DT-CI
	private char ciclDobDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-DOB-DT
	private String ciclDobDt = DefaultValues.stringVal(Len.CICL_DOB_DT);
	//Original name: CW02F-CICL-BIR-ST-CD-CI
	private char ciclBirStCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-BIR-ST-CD
	private String ciclBirStCd = DefaultValues.stringVal(Len.CICL_BIR_ST_CD);
	//Original name: CW02F-GENDER-CD-CI
	private char genderCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-GENDER-CD
	private char genderCd = DefaultValues.CHAR_VAL;
	//Original name: CW02F-PRI-LGG-CD-CI
	private char priLggCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-PRI-LGG-CD
	private String priLggCd = DefaultValues.stringVal(Len.PRI_LGG_CD);
	//Original name: CW02F-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW02F-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW02F-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW02F-CICL-EXP-DT-CI
	private char ciclExpDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-EXP-DT
	private String ciclExpDt = DefaultValues.stringVal(Len.CICL_EXP_DT);
	//Original name: CW02F-CICL-EFF-ACY-TS-CI
	private char ciclEffAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-EFF-ACY-TS
	private String ciclEffAcyTs = DefaultValues.stringVal(Len.CICL_EFF_ACY_TS);
	//Original name: CW02F-CICL-EXP-ACY-TS-CI
	private char ciclExpAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-EXP-ACY-TS
	private String ciclExpAcyTs = DefaultValues.stringVal(Len.CICL_EXP_ACY_TS);
	//Original name: CW02F-STATUTORY-TLE-CD-CI
	private char statutoryTleCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-STATUTORY-TLE-CD
	private String statutoryTleCd = DefaultValues.stringVal(Len.STATUTORY_TLE_CD);
	//Original name: CW02F-CICL-LNG-NM-CI
	private char ciclLngNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-LNG-NM-NI
	private char ciclLngNmNi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-LNG-NM
	private String ciclLngNm = DefaultValues.stringVal(Len.CICL_LNG_NM);
	//Original name: CW02F-LO-LST-NM-MCH-CD-CI
	private char loLstNmMchCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-LO-LST-NM-MCH-CD-NI
	private char loLstNmMchCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-LO-LST-NM-MCH-CD
	private String loLstNmMchCd = DefaultValues.stringVal(Len.LO_LST_NM_MCH_CD);
	//Original name: CW02F-HI-LST-NM-MCH-CD-CI
	private char hiLstNmMchCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-HI-LST-NM-MCH-CD-NI
	private char hiLstNmMchCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-HI-LST-NM-MCH-CD
	private String hiLstNmMchCd = DefaultValues.stringVal(Len.HI_LST_NM_MCH_CD);
	//Original name: CW02F-LO-FST-NM-MCH-CD-CI
	private char loFstNmMchCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-LO-FST-NM-MCH-CD-NI
	private char loFstNmMchCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-LO-FST-NM-MCH-CD
	private String loFstNmMchCd = DefaultValues.stringVal(Len.LO_FST_NM_MCH_CD);
	//Original name: CW02F-HI-FST-NM-MCH-CD-CI
	private char hiFstNmMchCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-HI-FST-NM-MCH-CD-NI
	private char hiFstNmMchCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-HI-FST-NM-MCH-CD
	private String hiFstNmMchCd = DefaultValues.stringVal(Len.HI_FST_NM_MCH_CD);
	//Original name: CW02F-CICL-ACQ-SRC-CD-CI
	private char ciclAcqSrcCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-ACQ-SRC-CD-NI
	private char ciclAcqSrcCdNi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-ACQ-SRC-CD
	private String ciclAcqSrcCd = DefaultValues.stringVal(Len.CICL_ACQ_SRC_CD);
	//Original name: CW02F-LEG-ENT-DESC
	private String legEntDesc = DefaultValues.stringVal(Len.LEG_ENT_DESC);

	//==== METHODS ====
	public void setClientTabDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ciclPriSubCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclPriSubCd = MarshalByte.readString(buffer, position, Len.CICL_PRI_SUB_CD);
		position += Len.CICL_PRI_SUB_CD;
		ciclFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclFstNm = MarshalByte.readString(buffer, position, Len.CICL_FST_NM);
		position += Len.CICL_FST_NM;
		ciclLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclLstNm = MarshalByte.readString(buffer, position, Len.CICL_LST_NM);
		position += Len.CICL_LST_NM;
		ciclMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclMdlNm = MarshalByte.readString(buffer, position, Len.CICL_MDL_NM);
		position += Len.CICL_MDL_NM;
		nmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		nmPfx = MarshalByte.readString(buffer, position, Len.NM_PFX);
		position += Len.NM_PFX;
		nmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		nmSfx = MarshalByte.readString(buffer, position, Len.NM_SFX);
		position += Len.NM_SFX;
		primaryProDsnCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		primaryProDsnCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		primaryProDsnCd = MarshalByte.readString(buffer, position, Len.PRIMARY_PRO_DSN_CD);
		position += Len.PRIMARY_PRO_DSN_CD;
		legEntCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		legEntCd = MarshalByte.readString(buffer, position, Len.LEG_ENT_CD);
		position += Len.LEG_ENT_CD;
		ciclSdxCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclSdxCd = MarshalByte.readString(buffer, position, Len.CICL_SDX_CD);
		position += Len.CICL_SDX_CD;
		ciclOgnInceptDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclOgnInceptDtNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclOgnInceptDt = MarshalByte.readString(buffer, position, Len.CICL_OGN_INCEPT_DT);
		position += Len.CICL_OGN_INCEPT_DT;
		ciclAddNmIndCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclAddNmIndNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclAddNmInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclDobDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclDobDt = MarshalByte.readString(buffer, position, Len.CICL_DOB_DT);
		position += Len.CICL_DOB_DT;
		ciclBirStCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclBirStCd = MarshalByte.readString(buffer, position, Len.CICL_BIR_ST_CD);
		position += Len.CICL_BIR_ST_CD;
		genderCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		genderCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		priLggCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		priLggCd = MarshalByte.readString(buffer, position, Len.PRI_LGG_CD);
		position += Len.PRI_LGG_CD;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		ciclExpDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclExpDt = MarshalByte.readString(buffer, position, Len.CICL_EXP_DT);
		position += Len.CICL_EXP_DT;
		ciclEffAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclEffAcyTs = MarshalByte.readString(buffer, position, Len.CICL_EFF_ACY_TS);
		position += Len.CICL_EFF_ACY_TS;
		ciclExpAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclExpAcyTs = MarshalByte.readString(buffer, position, Len.CICL_EXP_ACY_TS);
		position += Len.CICL_EXP_ACY_TS;
		statutoryTleCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statutoryTleCd = MarshalByte.readString(buffer, position, Len.STATUTORY_TLE_CD);
		position += Len.STATUTORY_TLE_CD;
		ciclLngNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclLngNmNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclLngNm = MarshalByte.readString(buffer, position, Len.CICL_LNG_NM);
		position += Len.CICL_LNG_NM;
		loLstNmMchCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		loLstNmMchCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		loLstNmMchCd = MarshalByte.readString(buffer, position, Len.LO_LST_NM_MCH_CD);
		position += Len.LO_LST_NM_MCH_CD;
		hiLstNmMchCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hiLstNmMchCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hiLstNmMchCd = MarshalByte.readString(buffer, position, Len.HI_LST_NM_MCH_CD);
		position += Len.HI_LST_NM_MCH_CD;
		loFstNmMchCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		loFstNmMchCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		loFstNmMchCd = MarshalByte.readString(buffer, position, Len.LO_FST_NM_MCH_CD);
		position += Len.LO_FST_NM_MCH_CD;
		hiFstNmMchCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hiFstNmMchCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hiFstNmMchCd = MarshalByte.readString(buffer, position, Len.HI_FST_NM_MCH_CD);
		position += Len.HI_FST_NM_MCH_CD;
		ciclAcqSrcCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclAcqSrcCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclAcqSrcCd = MarshalByte.readString(buffer, position, Len.CICL_ACQ_SRC_CD);
		position += Len.CICL_ACQ_SRC_CD;
		legEntDesc = MarshalByte.readString(buffer, position, Len.LEG_ENT_DESC);
	}

	public byte[] getClientTabDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, ciclPriSubCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclPriSubCd, Len.CICL_PRI_SUB_CD);
		position += Len.CICL_PRI_SUB_CD;
		MarshalByte.writeChar(buffer, position, ciclFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclFstNm, Len.CICL_FST_NM);
		position += Len.CICL_FST_NM;
		MarshalByte.writeChar(buffer, position, ciclLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclLstNm, Len.CICL_LST_NM);
		position += Len.CICL_LST_NM;
		MarshalByte.writeChar(buffer, position, ciclMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclMdlNm, Len.CICL_MDL_NM);
		position += Len.CICL_MDL_NM;
		MarshalByte.writeChar(buffer, position, nmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nmPfx, Len.NM_PFX);
		position += Len.NM_PFX;
		MarshalByte.writeChar(buffer, position, nmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, nmSfx, Len.NM_SFX);
		position += Len.NM_SFX;
		MarshalByte.writeChar(buffer, position, primaryProDsnCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, primaryProDsnCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, primaryProDsnCd, Len.PRIMARY_PRO_DSN_CD);
		position += Len.PRIMARY_PRO_DSN_CD;
		MarshalByte.writeChar(buffer, position, legEntCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, legEntCd, Len.LEG_ENT_CD);
		position += Len.LEG_ENT_CD;
		MarshalByte.writeChar(buffer, position, ciclSdxCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclSdxCd, Len.CICL_SDX_CD);
		position += Len.CICL_SDX_CD;
		MarshalByte.writeChar(buffer, position, ciclOgnInceptDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclOgnInceptDtNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclOgnInceptDt, Len.CICL_OGN_INCEPT_DT);
		position += Len.CICL_OGN_INCEPT_DT;
		MarshalByte.writeChar(buffer, position, ciclAddNmIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclAddNmIndNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclAddNmInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclDobDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclDobDt, Len.CICL_DOB_DT);
		position += Len.CICL_DOB_DT;
		MarshalByte.writeChar(buffer, position, ciclBirStCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclBirStCd, Len.CICL_BIR_ST_CD);
		position += Len.CICL_BIR_ST_CD;
		MarshalByte.writeChar(buffer, position, genderCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, genderCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, priLggCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, priLggCd, Len.PRI_LGG_CD);
		position += Len.PRI_LGG_CD;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, ciclExpDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclExpDt, Len.CICL_EXP_DT);
		position += Len.CICL_EXP_DT;
		MarshalByte.writeChar(buffer, position, ciclEffAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclEffAcyTs, Len.CICL_EFF_ACY_TS);
		position += Len.CICL_EFF_ACY_TS;
		MarshalByte.writeChar(buffer, position, ciclExpAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclExpAcyTs, Len.CICL_EXP_ACY_TS);
		position += Len.CICL_EXP_ACY_TS;
		MarshalByte.writeChar(buffer, position, statutoryTleCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, statutoryTleCd, Len.STATUTORY_TLE_CD);
		position += Len.STATUTORY_TLE_CD;
		MarshalByte.writeChar(buffer, position, ciclLngNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclLngNmNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclLngNm, Len.CICL_LNG_NM);
		position += Len.CICL_LNG_NM;
		MarshalByte.writeChar(buffer, position, loLstNmMchCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, loLstNmMchCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, loLstNmMchCd, Len.LO_LST_NM_MCH_CD);
		position += Len.LO_LST_NM_MCH_CD;
		MarshalByte.writeChar(buffer, position, hiLstNmMchCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hiLstNmMchCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hiLstNmMchCd, Len.HI_LST_NM_MCH_CD);
		position += Len.HI_LST_NM_MCH_CD;
		MarshalByte.writeChar(buffer, position, loFstNmMchCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, loFstNmMchCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, loFstNmMchCd, Len.LO_FST_NM_MCH_CD);
		position += Len.LO_FST_NM_MCH_CD;
		MarshalByte.writeChar(buffer, position, hiFstNmMchCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hiFstNmMchCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hiFstNmMchCd, Len.HI_FST_NM_MCH_CD);
		position += Len.HI_FST_NM_MCH_CD;
		MarshalByte.writeChar(buffer, position, ciclAcqSrcCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciclAcqSrcCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclAcqSrcCd, Len.CICL_ACQ_SRC_CD);
		position += Len.CICL_ACQ_SRC_CD;
		MarshalByte.writeString(buffer, position, legEntDesc, Len.LEG_ENT_DESC);
		return buffer;
	}

	public void setCiclPriSubCdCi(char ciclPriSubCdCi) {
		this.ciclPriSubCdCi = ciclPriSubCdCi;
	}

	public char getCiclPriSubCdCi() {
		return this.ciclPriSubCdCi;
	}

	public void setCiclPriSubCd(String ciclPriSubCd) {
		this.ciclPriSubCd = Functions.subString(ciclPriSubCd, Len.CICL_PRI_SUB_CD);
	}

	public String getCiclPriSubCd() {
		return this.ciclPriSubCd;
	}

	public void setCiclFstNmCi(char ciclFstNmCi) {
		this.ciclFstNmCi = ciclFstNmCi;
	}

	public char getCiclFstNmCi() {
		return this.ciclFstNmCi;
	}

	public void setCiclFstNm(String ciclFstNm) {
		this.ciclFstNm = Functions.subString(ciclFstNm, Len.CICL_FST_NM);
	}

	public String getCiclFstNm() {
		return this.ciclFstNm;
	}

	public void setCiclLstNmCi(char ciclLstNmCi) {
		this.ciclLstNmCi = ciclLstNmCi;
	}

	public char getCiclLstNmCi() {
		return this.ciclLstNmCi;
	}

	public void setCiclLstNm(String ciclLstNm) {
		this.ciclLstNm = Functions.subString(ciclLstNm, Len.CICL_LST_NM);
	}

	public String getCiclLstNm() {
		return this.ciclLstNm;
	}

	public void setCiclMdlNmCi(char ciclMdlNmCi) {
		this.ciclMdlNmCi = ciclMdlNmCi;
	}

	public char getCiclMdlNmCi() {
		return this.ciclMdlNmCi;
	}

	public void setCiclMdlNm(String ciclMdlNm) {
		this.ciclMdlNm = Functions.subString(ciclMdlNm, Len.CICL_MDL_NM);
	}

	public String getCiclMdlNm() {
		return this.ciclMdlNm;
	}

	public void setNmPfxCi(char nmPfxCi) {
		this.nmPfxCi = nmPfxCi;
	}

	public char getNmPfxCi() {
		return this.nmPfxCi;
	}

	public void setNmPfx(String nmPfx) {
		this.nmPfx = Functions.subString(nmPfx, Len.NM_PFX);
	}

	public String getNmPfx() {
		return this.nmPfx;
	}

	public void setNmSfxCi(char nmSfxCi) {
		this.nmSfxCi = nmSfxCi;
	}

	public char getNmSfxCi() {
		return this.nmSfxCi;
	}

	public void setNmSfx(String nmSfx) {
		this.nmSfx = Functions.subString(nmSfx, Len.NM_SFX);
	}

	public String getNmSfx() {
		return this.nmSfx;
	}

	public void setPrimaryProDsnCdCi(char primaryProDsnCdCi) {
		this.primaryProDsnCdCi = primaryProDsnCdCi;
	}

	public char getPrimaryProDsnCdCi() {
		return this.primaryProDsnCdCi;
	}

	public void setPrimaryProDsnCdNi(char primaryProDsnCdNi) {
		this.primaryProDsnCdNi = primaryProDsnCdNi;
	}

	public char getPrimaryProDsnCdNi() {
		return this.primaryProDsnCdNi;
	}

	public void setPrimaryProDsnCd(String primaryProDsnCd) {
		this.primaryProDsnCd = Functions.subString(primaryProDsnCd, Len.PRIMARY_PRO_DSN_CD);
	}

	public String getPrimaryProDsnCd() {
		return this.primaryProDsnCd;
	}

	public void setLegEntCdCi(char legEntCdCi) {
		this.legEntCdCi = legEntCdCi;
	}

	public char getLegEntCdCi() {
		return this.legEntCdCi;
	}

	public void setLegEntCd(String legEntCd) {
		this.legEntCd = Functions.subString(legEntCd, Len.LEG_ENT_CD);
	}

	public String getLegEntCd() {
		return this.legEntCd;
	}

	public void setCiclSdxCdCi(char ciclSdxCdCi) {
		this.ciclSdxCdCi = ciclSdxCdCi;
	}

	public char getCiclSdxCdCi() {
		return this.ciclSdxCdCi;
	}

	public void setCiclSdxCd(String ciclSdxCd) {
		this.ciclSdxCd = Functions.subString(ciclSdxCd, Len.CICL_SDX_CD);
	}

	public String getCiclSdxCd() {
		return this.ciclSdxCd;
	}

	public void setCiclOgnInceptDtCi(char ciclOgnInceptDtCi) {
		this.ciclOgnInceptDtCi = ciclOgnInceptDtCi;
	}

	public char getCiclOgnInceptDtCi() {
		return this.ciclOgnInceptDtCi;
	}

	public void setCiclOgnInceptDtNi(char ciclOgnInceptDtNi) {
		this.ciclOgnInceptDtNi = ciclOgnInceptDtNi;
	}

	public char getCiclOgnInceptDtNi() {
		return this.ciclOgnInceptDtNi;
	}

	public void setCiclOgnInceptDt(String ciclOgnInceptDt) {
		this.ciclOgnInceptDt = Functions.subString(ciclOgnInceptDt, Len.CICL_OGN_INCEPT_DT);
	}

	public String getCiclOgnInceptDt() {
		return this.ciclOgnInceptDt;
	}

	public void setCiclAddNmIndCi(char ciclAddNmIndCi) {
		this.ciclAddNmIndCi = ciclAddNmIndCi;
	}

	public char getCiclAddNmIndCi() {
		return this.ciclAddNmIndCi;
	}

	public void setCiclAddNmIndNi(char ciclAddNmIndNi) {
		this.ciclAddNmIndNi = ciclAddNmIndNi;
	}

	public char getCiclAddNmIndNi() {
		return this.ciclAddNmIndNi;
	}

	public void setCiclAddNmInd(char ciclAddNmInd) {
		this.ciclAddNmInd = ciclAddNmInd;
	}

	public char getCiclAddNmInd() {
		return this.ciclAddNmInd;
	}

	public void setCiclDobDtCi(char ciclDobDtCi) {
		this.ciclDobDtCi = ciclDobDtCi;
	}

	public char getCiclDobDtCi() {
		return this.ciclDobDtCi;
	}

	public void setCiclDobDt(String ciclDobDt) {
		this.ciclDobDt = Functions.subString(ciclDobDt, Len.CICL_DOB_DT);
	}

	public String getCiclDobDt() {
		return this.ciclDobDt;
	}

	public void setCiclBirStCdCi(char ciclBirStCdCi) {
		this.ciclBirStCdCi = ciclBirStCdCi;
	}

	public char getCiclBirStCdCi() {
		return this.ciclBirStCdCi;
	}

	public void setCiclBirStCd(String ciclBirStCd) {
		this.ciclBirStCd = Functions.subString(ciclBirStCd, Len.CICL_BIR_ST_CD);
	}

	public String getCiclBirStCd() {
		return this.ciclBirStCd;
	}

	public void setGenderCdCi(char genderCdCi) {
		this.genderCdCi = genderCdCi;
	}

	public char getGenderCdCi() {
		return this.genderCdCi;
	}

	public void setGenderCd(char genderCd) {
		this.genderCd = genderCd;
	}

	public char getGenderCd() {
		return this.genderCd;
	}

	public void setPriLggCdCi(char priLggCdCi) {
		this.priLggCdCi = priLggCdCi;
	}

	public char getPriLggCdCi() {
		return this.priLggCdCi;
	}

	public void setPriLggCd(String priLggCd) {
		this.priLggCd = Functions.subString(priLggCd, Len.PRI_LGG_CD);
	}

	public String getPriLggCd() {
		return this.priLggCd;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setCiclExpDtCi(char ciclExpDtCi) {
		this.ciclExpDtCi = ciclExpDtCi;
	}

	public char getCiclExpDtCi() {
		return this.ciclExpDtCi;
	}

	public void setCiclExpDt(String ciclExpDt) {
		this.ciclExpDt = Functions.subString(ciclExpDt, Len.CICL_EXP_DT);
	}

	public String getCiclExpDt() {
		return this.ciclExpDt;
	}

	public void setCiclEffAcyTsCi(char ciclEffAcyTsCi) {
		this.ciclEffAcyTsCi = ciclEffAcyTsCi;
	}

	public char getCiclEffAcyTsCi() {
		return this.ciclEffAcyTsCi;
	}

	public void setCiclEffAcyTs(String ciclEffAcyTs) {
		this.ciclEffAcyTs = Functions.subString(ciclEffAcyTs, Len.CICL_EFF_ACY_TS);
	}

	public String getCiclEffAcyTs() {
		return this.ciclEffAcyTs;
	}

	public void setCiclExpAcyTsCi(char ciclExpAcyTsCi) {
		this.ciclExpAcyTsCi = ciclExpAcyTsCi;
	}

	public char getCiclExpAcyTsCi() {
		return this.ciclExpAcyTsCi;
	}

	public void setCiclExpAcyTs(String ciclExpAcyTs) {
		this.ciclExpAcyTs = Functions.subString(ciclExpAcyTs, Len.CICL_EXP_ACY_TS);
	}

	public String getCiclExpAcyTs() {
		return this.ciclExpAcyTs;
	}

	public void setStatutoryTleCdCi(char statutoryTleCdCi) {
		this.statutoryTleCdCi = statutoryTleCdCi;
	}

	public char getStatutoryTleCdCi() {
		return this.statutoryTleCdCi;
	}

	public void setStatutoryTleCd(String statutoryTleCd) {
		this.statutoryTleCd = Functions.subString(statutoryTleCd, Len.STATUTORY_TLE_CD);
	}

	public String getStatutoryTleCd() {
		return this.statutoryTleCd;
	}

	public void setCiclLngNmCi(char ciclLngNmCi) {
		this.ciclLngNmCi = ciclLngNmCi;
	}

	public char getCiclLngNmCi() {
		return this.ciclLngNmCi;
	}

	public void setCiclLngNmNi(char ciclLngNmNi) {
		this.ciclLngNmNi = ciclLngNmNi;
	}

	public char getCiclLngNmNi() {
		return this.ciclLngNmNi;
	}

	public void setCiclLngNm(String ciclLngNm) {
		this.ciclLngNm = Functions.subString(ciclLngNm, Len.CICL_LNG_NM);
	}

	public String getCiclLngNm() {
		return this.ciclLngNm;
	}

	public void setLoLstNmMchCdCi(char loLstNmMchCdCi) {
		this.loLstNmMchCdCi = loLstNmMchCdCi;
	}

	public char getLoLstNmMchCdCi() {
		return this.loLstNmMchCdCi;
	}

	public void setLoLstNmMchCdNi(char loLstNmMchCdNi) {
		this.loLstNmMchCdNi = loLstNmMchCdNi;
	}

	public char getLoLstNmMchCdNi() {
		return this.loLstNmMchCdNi;
	}

	public void setLoLstNmMchCd(String loLstNmMchCd) {
		this.loLstNmMchCd = Functions.subString(loLstNmMchCd, Len.LO_LST_NM_MCH_CD);
	}

	public String getLoLstNmMchCd() {
		return this.loLstNmMchCd;
	}

	public void setHiLstNmMchCdCi(char hiLstNmMchCdCi) {
		this.hiLstNmMchCdCi = hiLstNmMchCdCi;
	}

	public char getHiLstNmMchCdCi() {
		return this.hiLstNmMchCdCi;
	}

	public void setHiLstNmMchCdNi(char hiLstNmMchCdNi) {
		this.hiLstNmMchCdNi = hiLstNmMchCdNi;
	}

	public char getHiLstNmMchCdNi() {
		return this.hiLstNmMchCdNi;
	}

	public void setHiLstNmMchCd(String hiLstNmMchCd) {
		this.hiLstNmMchCd = Functions.subString(hiLstNmMchCd, Len.HI_LST_NM_MCH_CD);
	}

	public String getHiLstNmMchCd() {
		return this.hiLstNmMchCd;
	}

	public void setLoFstNmMchCdCi(char loFstNmMchCdCi) {
		this.loFstNmMchCdCi = loFstNmMchCdCi;
	}

	public char getLoFstNmMchCdCi() {
		return this.loFstNmMchCdCi;
	}

	public void setLoFstNmMchCdNi(char loFstNmMchCdNi) {
		this.loFstNmMchCdNi = loFstNmMchCdNi;
	}

	public char getLoFstNmMchCdNi() {
		return this.loFstNmMchCdNi;
	}

	public void setLoFstNmMchCd(String loFstNmMchCd) {
		this.loFstNmMchCd = Functions.subString(loFstNmMchCd, Len.LO_FST_NM_MCH_CD);
	}

	public String getLoFstNmMchCd() {
		return this.loFstNmMchCd;
	}

	public void setHiFstNmMchCdCi(char hiFstNmMchCdCi) {
		this.hiFstNmMchCdCi = hiFstNmMchCdCi;
	}

	public char getHiFstNmMchCdCi() {
		return this.hiFstNmMchCdCi;
	}

	public void setHiFstNmMchCdNi(char hiFstNmMchCdNi) {
		this.hiFstNmMchCdNi = hiFstNmMchCdNi;
	}

	public char getHiFstNmMchCdNi() {
		return this.hiFstNmMchCdNi;
	}

	public void setHiFstNmMchCd(String hiFstNmMchCd) {
		this.hiFstNmMchCd = Functions.subString(hiFstNmMchCd, Len.HI_FST_NM_MCH_CD);
	}

	public String getHiFstNmMchCd() {
		return this.hiFstNmMchCd;
	}

	public void setCiclAcqSrcCdCi(char ciclAcqSrcCdCi) {
		this.ciclAcqSrcCdCi = ciclAcqSrcCdCi;
	}

	public char getCiclAcqSrcCdCi() {
		return this.ciclAcqSrcCdCi;
	}

	public void setCiclAcqSrcCdNi(char ciclAcqSrcCdNi) {
		this.ciclAcqSrcCdNi = ciclAcqSrcCdNi;
	}

	public char getCiclAcqSrcCdNi() {
		return this.ciclAcqSrcCdNi;
	}

	public void setCiclAcqSrcCd(String ciclAcqSrcCd) {
		this.ciclAcqSrcCd = Functions.subString(ciclAcqSrcCd, Len.CICL_ACQ_SRC_CD);
	}

	public String getCiclAcqSrcCd() {
		return this.ciclAcqSrcCd;
	}

	public void setLegEntDesc(String legEntDesc) {
		this.legEntDesc = Functions.subString(legEntDesc, Len.LEG_ENT_DESC);
	}

	public String getLegEntDesc() {
		return this.legEntDesc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICL_PRI_SUB_CD = 4;
		public static final int CICL_FST_NM = 30;
		public static final int CICL_LST_NM = 60;
		public static final int CICL_MDL_NM = 30;
		public static final int NM_PFX = 4;
		public static final int NM_SFX = 4;
		public static final int PRIMARY_PRO_DSN_CD = 4;
		public static final int LEG_ENT_CD = 2;
		public static final int CICL_SDX_CD = 8;
		public static final int CICL_OGN_INCEPT_DT = 10;
		public static final int CICL_DOB_DT = 10;
		public static final int CICL_BIR_ST_CD = 3;
		public static final int PRI_LGG_CD = 3;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CICL_EXP_DT = 10;
		public static final int CICL_EFF_ACY_TS = 26;
		public static final int CICL_EXP_ACY_TS = 26;
		public static final int STATUTORY_TLE_CD = 3;
		public static final int CICL_LNG_NM = 132;
		public static final int LO_LST_NM_MCH_CD = 35;
		public static final int HI_LST_NM_MCH_CD = 35;
		public static final int LO_FST_NM_MCH_CD = 35;
		public static final int HI_FST_NM_MCH_CD = 35;
		public static final int CICL_ACQ_SRC_CD = 3;
		public static final int LEG_ENT_DESC = 40;
		public static final int CICL_PRI_SUB_CD_CI = 1;
		public static final int CICL_FST_NM_CI = 1;
		public static final int CICL_LST_NM_CI = 1;
		public static final int CICL_MDL_NM_CI = 1;
		public static final int NM_PFX_CI = 1;
		public static final int NM_SFX_CI = 1;
		public static final int PRIMARY_PRO_DSN_CD_CI = 1;
		public static final int PRIMARY_PRO_DSN_CD_NI = 1;
		public static final int LEG_ENT_CD_CI = 1;
		public static final int CICL_SDX_CD_CI = 1;
		public static final int CICL_OGN_INCEPT_DT_CI = 1;
		public static final int CICL_OGN_INCEPT_DT_NI = 1;
		public static final int CICL_ADD_NM_IND_CI = 1;
		public static final int CICL_ADD_NM_IND_NI = 1;
		public static final int CICL_ADD_NM_IND = 1;
		public static final int CICL_DOB_DT_CI = 1;
		public static final int CICL_BIR_ST_CD_CI = 1;
		public static final int GENDER_CD_CI = 1;
		public static final int GENDER_CD = 1;
		public static final int PRI_LGG_CD_CI = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int CICL_EXP_DT_CI = 1;
		public static final int CICL_EFF_ACY_TS_CI = 1;
		public static final int CICL_EXP_ACY_TS_CI = 1;
		public static final int STATUTORY_TLE_CD_CI = 1;
		public static final int CICL_LNG_NM_CI = 1;
		public static final int CICL_LNG_NM_NI = 1;
		public static final int LO_LST_NM_MCH_CD_CI = 1;
		public static final int LO_LST_NM_MCH_CD_NI = 1;
		public static final int HI_LST_NM_MCH_CD_CI = 1;
		public static final int HI_LST_NM_MCH_CD_NI = 1;
		public static final int LO_FST_NM_MCH_CD_CI = 1;
		public static final int LO_FST_NM_MCH_CD_NI = 1;
		public static final int HI_FST_NM_MCH_CD_CI = 1;
		public static final int HI_FST_NM_MCH_CD_NI = 1;
		public static final int CICL_ACQ_SRC_CD_CI = 1;
		public static final int CICL_ACQ_SRC_CD_NI = 1;
		public static final int CLIENT_TAB_DATA = CICL_PRI_SUB_CD_CI + CICL_PRI_SUB_CD + CICL_FST_NM_CI + CICL_FST_NM + CICL_LST_NM_CI + CICL_LST_NM
				+ CICL_MDL_NM_CI + CICL_MDL_NM + NM_PFX_CI + NM_PFX + NM_SFX_CI + NM_SFX + PRIMARY_PRO_DSN_CD_CI + PRIMARY_PRO_DSN_CD_NI
				+ PRIMARY_PRO_DSN_CD + LEG_ENT_CD_CI + LEG_ENT_CD + CICL_SDX_CD_CI + CICL_SDX_CD + CICL_OGN_INCEPT_DT_CI + CICL_OGN_INCEPT_DT_NI
				+ CICL_OGN_INCEPT_DT + CICL_ADD_NM_IND_CI + CICL_ADD_NM_IND_NI + CICL_ADD_NM_IND + CICL_DOB_DT_CI + CICL_DOB_DT + CICL_BIR_ST_CD_CI
				+ CICL_BIR_ST_CD + GENDER_CD_CI + GENDER_CD + PRI_LGG_CD_CI + PRI_LGG_CD + USER_ID_CI + USER_ID + STATUS_CD_CI + STATUS_CD
				+ TERMINAL_ID_CI + TERMINAL_ID + CICL_EXP_DT_CI + CICL_EXP_DT + CICL_EFF_ACY_TS_CI + CICL_EFF_ACY_TS + CICL_EXP_ACY_TS_CI
				+ CICL_EXP_ACY_TS + STATUTORY_TLE_CD_CI + STATUTORY_TLE_CD + CICL_LNG_NM_CI + CICL_LNG_NM_NI + CICL_LNG_NM + LO_LST_NM_MCH_CD_CI
				+ LO_LST_NM_MCH_CD_NI + LO_LST_NM_MCH_CD + HI_LST_NM_MCH_CD_CI + HI_LST_NM_MCH_CD_NI + HI_LST_NM_MCH_CD + LO_FST_NM_MCH_CD_CI
				+ LO_FST_NM_MCH_CD_NI + LO_FST_NM_MCH_CD + HI_FST_NM_MCH_CD_CI + HI_FST_NM_MCH_CD_NI + HI_FST_NM_MCH_CD + CICL_ACQ_SRC_CD_CI
				+ CICL_ACQ_SRC_CD_NI + CICL_ACQ_SRC_CD + LEG_ENT_DESC;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
