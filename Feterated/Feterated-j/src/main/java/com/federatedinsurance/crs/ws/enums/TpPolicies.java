/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TP-POLICIES<br>
 * Variable: TP-POLICIES from program XZ0P90K0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TpPolicies {

	//==== PROPERTIES ====
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TP_POLICIES);
	//Original name: TP-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: TP-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: TP-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: TP-POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: TP-POL-PRI-RSK-ST-ABB
	private String polPriRskStAbb = DefaultValues.stringVal(Len.POL_PRI_RSK_ST_ABB);
	//Original name: TP-MIN-POL-EFF-DT
	private String minPolEffDt = DefaultValues.stringVal(Len.MIN_POL_EFF_DT);
	//Original name: TP-POL-BIL-STA-CD
	private char polBilStaCd = DefaultValues.CHAR_VAL;
	//Original name: TP-POL-DUE-AMT
	private AfDecimal polDueAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: TP-PROP-CAN-DT
	private String propCanDt = DefaultValues.stringVal(Len.PROP_CAN_DT);
	//Original name: TP-NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);

	//==== METHODS ====
	public String getTpPoliciesFormatted() {
		return MarshalByteExt.bufferToStr(getTpPoliciesBytes());
	}

	public byte[] getTpPoliciesBytes() {
		byte[] buffer = new byte[Len.TP_POLICIES];
		return getTpPoliciesBytes(buffer, 1);
	}

	public byte[] getTpPoliciesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeString(buffer, position, polTypCd, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		MarshalByte.writeString(buffer, position, polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
		position += Len.POL_PRI_RSK_ST_ABB;
		MarshalByte.writeString(buffer, position, minPolEffDt, Len.MIN_POL_EFF_DT);
		position += Len.MIN_POL_EFF_DT;
		MarshalByte.writeChar(buffer, position, polBilStaCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeDecimal(buffer, position, polDueAmt.copy());
		position += Len.POL_DUE_AMT;
		MarshalByte.writeString(buffer, position, propCanDt, Len.PROP_CAN_DT);
		position += Len.PROP_CAN_DT;
		MarshalByte.writeString(buffer, position, notEffDt, Len.NOT_EFF_DT);
		return buffer;
	}

	public void initTpPoliciesHighValues() {
		polNbr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_NBR);
		polEffDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_EFF_DT);
		polExpDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_EXP_DT);
		polTypCd = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_TYP_CD);
		polPriRskStAbb = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_PRI_RSK_ST_ABB);
		minPolEffDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.MIN_POL_EFF_DT);
		polBilStaCd = Types.HIGH_CHAR_VAL;
		polDueAmt.setHigh();
		propCanDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PROP_CAN_DT);
		notEffDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NOT_EFF_DT);
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTpPoliciesFormatted()).equals(END_OF_TABLE);
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	public String getPolTypCd() {
		return this.polTypCd;
	}

	public void setPolPriRskStAbb(String polPriRskStAbb) {
		this.polPriRskStAbb = Functions.subString(polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
	}

	public String getPolPriRskStAbb() {
		return this.polPriRskStAbb;
	}

	public void setMinPolEffDt(String minPolEffDt) {
		this.minPolEffDt = Functions.subString(minPolEffDt, Len.MIN_POL_EFF_DT);
	}

	public String getMinPolEffDt() {
		return this.minPolEffDt;
	}

	public void setPolBilStaCd(char polBilStaCd) {
		this.polBilStaCd = polBilStaCd;
	}

	public char getPolBilStaCd() {
		return this.polBilStaCd;
	}

	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.polDueAmt.assign(polDueAmt);
	}

	public AfDecimal getPolDueAmt() {
		return this.polDueAmt.copy();
	}

	public void setPropCanDt(String propCanDt) {
		this.propCanDt = Functions.subString(propCanDt, Len.PROP_CAN_DT);
	}

	public String getPropCanDt() {
		return this.propCanDt;
	}

	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	public String getNotEffDt() {
		return this.notEffDt;
	}

	public String getNotEffDtFormatted() {
		return Functions.padBlanks(getNotEffDt(), Len.NOT_EFF_DT);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int POL_TYP_CD = 3;
		public static final int POL_PRI_RSK_ST_ABB = 2;
		public static final int MIN_POL_EFF_DT = 10;
		public static final int POL_BIL_STA_CD = 1;
		public static final int POL_DUE_AMT = 10;
		public static final int PROP_CAN_DT = 10;
		public static final int NOT_EFF_DT = 10;
		public static final int TP_POLICIES = POL_NBR + POL_EFF_DT + POL_EXP_DT + POL_TYP_CD + POL_PRI_RSK_ST_ABB + MIN_POL_EFF_DT + POL_BIL_STA_CD
				+ POL_DUE_AMT + PROP_CAN_DT + NOT_EFF_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
