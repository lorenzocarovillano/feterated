/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-61-USR-INVALID-USR-TKN-MSG<br>
 * Variable: EA-61-USR-INVALID-USR-TKN-MSG from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea61UsrInvalidUsrTknMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-61-USR-INVALID-USR-TKN-MSG
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-61-USR-INVALID-USR-TKN-MSG-1
	private String flr2 = "INVALID USER";
	//Original name: FILLER-EA-61-USR-INVALID-USR-TKN-MSG-2
	private String flr3 = "TOKEN PROVIDED.";
	//Original name: FILLER-EA-61-USR-INVALID-USR-TKN-MSG-3
	private String flr4 = "  (";
	//Original name: EA-61-USER-TOKEN
	private String ea61UserToken = DefaultValues.stringVal(Len.EA61_USER_TOKEN);
	//Original name: FILLER-EA-61-USR-INVALID-USR-TKN-MSG-4
	private char flr5 = ')';

	//==== METHODS ====
	public String getEa61UsrInvalidUsrTknMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa61UsrInvalidUsrTknMsgBytes());
	}

	public byte[] getEa61UsrInvalidUsrTknMsgBytes() {
		byte[] buffer = new byte[Len.EA61_USR_INVALID_USR_TKN_MSG];
		return getEa61UsrInvalidUsrTknMsgBytes(buffer, 1);
	}

	public byte[] getEa61UsrInvalidUsrTknMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, ea61UserToken, Len.EA61_USER_TOKEN);
		position += Len.EA61_USER_TOKEN;
		MarshalByte.writeChar(buffer, position, flr5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setEa61UserToken(long ea61UserToken) {
		this.ea61UserToken = NumericDisplay.asString(ea61UserToken, Len.EA61_USER_TOKEN);
	}

	public long getEa61UserToken() {
		return NumericDisplay.asLong(this.ea61UserToken);
	}

	public char getFlr5() {
		return this.flr5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA61_USER_TOKEN = 10;
		public static final int FLR1 = 11;
		public static final int FLR2 = 13;
		public static final int FLR3 = 15;
		public static final int FLR4 = 3;
		public static final int FLR5 = 1;
		public static final int EA61_USR_INVALID_USR_TKN_MSG = EA61_USER_TOKEN + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
