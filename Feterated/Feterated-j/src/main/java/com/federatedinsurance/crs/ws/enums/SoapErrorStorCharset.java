/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SOAP-ERROR-STOR-CHARSET<br>
 * Variable: SOAP-ERROR-STOR-CHARSET from copybook TS570CB1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SoapErrorStorCharset {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.ERROR_STOR_CHARSET);
	public static final String EBCDIC = "EB";
	public static final String ASCII = "AS";

	//==== METHODS ====
	public void setErrorStorCharset(String errorStorCharset) {
		this.value = Functions.subString(errorStorCharset, Len.ERROR_STOR_CHARSET);
	}

	public String getErrorStorCharset() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_STOR_CHARSET = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
