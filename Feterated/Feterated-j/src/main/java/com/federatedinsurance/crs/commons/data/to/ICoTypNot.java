/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [CO_TYP, NOT_CO_TYP]
 * 
 */
public interface ICoTypNot extends BaseSqlTo {

	/**
	 * Host Variable ACT-NOT-TYP-CD
	 * 
	 */
	String getActNotTypCd();

	void setActNotTypCd(String actNotTypCd);

	/**
	 * Host Variable CO-CD
	 * 
	 */
	String getCoCd();

	void setCoCd(String coCd);

	/**
	 * Host Variable CO-DES
	 * 
	 */
	String getCoDes();

	void setCoDes(String coDes);

	/**
	 * Host Variable SPE-CRT-CD
	 * 
	 */
	String getSpeCrtCd();

	void setSpeCrtCd(String speCrtCd);

	/**
	 * Nullable property for SPE-CRT-CD
	 * 
	 */
	String getSpeCrtCdObj();

	void setSpeCrtCdObj(String speCrtCdObj);

	/**
	 * Host Variable CO-OFS-NBR
	 * 
	 */
	short getCoOfsNbr();

	void setCoOfsNbr(short coOfsNbr);

	/**
	 * Host Variable DTB-CD
	 * 
	 */
	char getDtbCd();

	void setDtbCd(char dtbCd);
};
