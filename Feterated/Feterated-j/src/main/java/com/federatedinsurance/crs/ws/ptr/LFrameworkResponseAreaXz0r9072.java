/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9072<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9072 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9072_MAXOCCURS = 2500;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9072() {
	}

	public LFrameworkResponseAreaXz0r9072(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a9072Formatted(int lFwRespXz0a9072Idx) {
		int position = Pos.lFwRespXz0a9072(lFwRespXz0a9072Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9072);
	}

	public void setlFwRespXz0a9072Bytes(int lFwRespXz0a9072Idx, byte[] buffer) {
		setlFwRespXz0a9072Bytes(lFwRespXz0a9072Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A9072<br>*/
	public byte[] getlFwRespXz0a9072Bytes(int lFwRespXz0a9072Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_XZ0A9072];
		return getlFwRespXz0a9072Bytes(lFwRespXz0a9072Idx, buffer, 1);
	}

	public void setlFwRespXz0a9072Bytes(int lFwRespXz0a9072Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9072(lFwRespXz0a9072Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A9072, position);
	}

	public byte[] getlFwRespXz0a9072Bytes(int lFwRespXz0a9072Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9072(lFwRespXz0a9072Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A9072, position);
		return buffer;
	}

	public void setXza972rMaxTagRows(int xza972rMaxTagRowsIdx, short xza972rMaxTagRows) {
		int position = Pos.xza972MaxTagRows(xza972rMaxTagRowsIdx - 1);
		writeBinaryShort(position, xza972rMaxTagRows);
	}

	/**Original name: XZA972R-MAX-TAG-ROWS<br>*/
	public short getXza972rMaxTagRows(int xza972rMaxTagRowsIdx) {
		int position = Pos.xza972MaxTagRows(xza972rMaxTagRowsIdx - 1);
		return readBinaryShort(position);
	}

	/**Original name: XZA972R-ACY-TAG-INFO<br>*/
	public byte[] getXza972rAcyTagInfoBytes(int xza972rAcyTagInfoIdx) {
		byte[] buffer = new byte[Len.XZA972_ACY_TAG_INFO];
		return getXza972rAcyTagInfoBytes(xza972rAcyTagInfoIdx, buffer, 1);
	}

	public byte[] getXza972rAcyTagInfoBytes(int xza972rAcyTagInfoIdx, byte[] buffer, int offset) {
		int position = Pos.xza972AcyTagInfo(xza972rAcyTagInfoIdx - 1);
		getBytes(buffer, offset, Len.XZA972_ACY_TAG_INFO, position);
		return buffer;
	}

	public void setXza972rEdlFrmNm(int xza972rEdlFrmNmIdx, String xza972rEdlFrmNm) {
		int position = Pos.xza972EdlFrmNm(xza972rEdlFrmNmIdx - 1);
		writeString(position, xza972rEdlFrmNm, Len.XZA972_EDL_FRM_NM);
	}

	/**Original name: XZA972R-EDL-FRM-NM<br>*/
	public String getXza972rEdlFrmNm(int xza972rEdlFrmNmIdx) {
		int position = Pos.xza972EdlFrmNm(xza972rEdlFrmNmIdx - 1);
		return readString(position, Len.XZA972_EDL_FRM_NM);
	}

	public void setXza972rDtaGrpNm(int xza972rDtaGrpNmIdx, String xza972rDtaGrpNm) {
		int position = Pos.xza972DtaGrpNm(xza972rDtaGrpNmIdx - 1);
		writeString(position, xza972rDtaGrpNm, Len.XZA972_DTA_GRP_NM);
	}

	/**Original name: XZA972R-DTA-GRP-NM<br>*/
	public String getXza972rDtaGrpNm(int xza972rDtaGrpNmIdx) {
		int position = Pos.xza972DtaGrpNm(xza972rDtaGrpNmIdx - 1);
		return readString(position, Len.XZA972_DTA_GRP_NM);
	}

	public void setXza972rDtaGrpFldNbr(int xza972rDtaGrpFldNbrIdx, int xza972rDtaGrpFldNbr) {
		int position = Pos.xza972DtaGrpFldNbr(xza972rDtaGrpFldNbrIdx - 1);
		writeInt(position, xza972rDtaGrpFldNbr, Len.Int.XZA972R_DTA_GRP_FLD_NBR);
	}

	/**Original name: XZA972R-DTA-GRP-FLD-NBR<br>*/
	public int getXza972rDtaGrpFldNbr(int xza972rDtaGrpFldNbrIdx) {
		int position = Pos.xza972DtaGrpFldNbr(xza972rDtaGrpFldNbrIdx - 1);
		return readNumDispInt(position, Len.XZA972_DTA_GRP_FLD_NBR);
	}

	public void setXza972rTagNm(int xza972rTagNmIdx, String xza972rTagNm) {
		int position = Pos.xza972TagNm(xza972rTagNmIdx - 1);
		writeString(position, xza972rTagNm, Len.XZA972_TAG_NM);
	}

	/**Original name: XZA972R-TAG-NM<br>*/
	public String getXza972rTagNm(int xza972rTagNmIdx) {
		int position = Pos.xza972TagNm(xza972rTagNmIdx - 1);
		return readString(position, Len.XZA972_TAG_NM);
	}

	public void setXza972rJusCd(int xza972rJusCdIdx, char xza972rJusCd) {
		int position = Pos.xza972JusCd(xza972rJusCdIdx - 1);
		writeChar(position, xza972rJusCd);
	}

	/**Original name: XZA972R-JUS-CD<br>*/
	public char getXza972rJusCd(int xza972rJusCdIdx) {
		int position = Pos.xza972JusCd(xza972rJusCdIdx - 1);
		return readChar(position);
	}

	public void setXza972rSpePrcCd(int xza972rSpePrcCdIdx, String xza972rSpePrcCd) {
		int position = Pos.xza972SpePrcCd(xza972rSpePrcCdIdx - 1);
		writeString(position, xza972rSpePrcCd, Len.XZA972_SPE_PRC_CD);
	}

	/**Original name: XZA972R-SPE-PRC-CD<br>*/
	public String getXza972rSpePrcCd(int xza972rSpePrcCdIdx) {
		int position = Pos.xza972SpePrcCd(xza972rSpePrcCdIdx - 1);
		return readString(position, Len.XZA972_SPE_PRC_CD);
	}

	public void setXza972rLenNbr(int xza972rLenNbrIdx, int xza972rLenNbr) {
		int position = Pos.xza972LenNbr(xza972rLenNbrIdx - 1);
		writeInt(position, xza972rLenNbr, Len.Int.XZA972R_LEN_NBR);
	}

	/**Original name: XZA972R-LEN-NBR<br>*/
	public int getXza972rLenNbr(int xza972rLenNbrIdx) {
		int position = Pos.xza972LenNbr(xza972rLenNbrIdx - 1);
		return readNumDispInt(position, Len.XZA972_LEN_NBR);
	}

	public void setXza972rTagOccCnt(int xza972rTagOccCntIdx, int xza972rTagOccCnt) {
		int position = Pos.xza972TagOccCnt(xza972rTagOccCntIdx - 1);
		writeInt(position, xza972rTagOccCnt, Len.Int.XZA972R_TAG_OCC_CNT);
	}

	/**Original name: XZA972R-TAG-OCC-CNT<br>*/
	public int getXza972rTagOccCnt(int xza972rTagOccCntIdx) {
		int position = Pos.xza972TagOccCnt(xza972rTagOccCntIdx - 1);
		return readNumDispInt(position, Len.XZA972_TAG_OCC_CNT);
	}

	public void setXza972rDelInd(int xza972rDelIndIdx, char xza972rDelInd) {
		int position = Pos.xza972DelInd(xza972rDelIndIdx - 1);
		writeChar(position, xza972rDelInd);
	}

	/**Original name: XZA972R-DEL-IND<br>*/
	public char getXza972rDelInd(int xza972rDelIndIdx) {
		int position = Pos.xza972DelInd(xza972rDelIndIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9072(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9072;
		}

		public static int xza972AcyTagRow(int idx) {
			return lFwRespXz0a9072(idx);
		}

		public static int xza972MaxTagRows(int idx) {
			return xza972AcyTagRow(idx);
		}

		public static int xza972AcyTagInfo(int idx) {
			return xza972MaxTagRows(idx) + Len.XZA972_MAX_TAG_ROWS;
		}

		public static int xza972EdlFrmNm(int idx) {
			return xza972AcyTagInfo(idx);
		}

		public static int xza972DtaGrpNm(int idx) {
			return xza972EdlFrmNm(idx) + Len.XZA972_EDL_FRM_NM;
		}

		public static int xza972DtaGrpFldNbr(int idx) {
			return xza972DtaGrpNm(idx) + Len.XZA972_DTA_GRP_NM;
		}

		public static int xza972TagNm(int idx) {
			return xza972DtaGrpFldNbr(idx) + Len.XZA972_DTA_GRP_FLD_NBR;
		}

		public static int xza972JusCd(int idx) {
			return xza972TagNm(idx) + Len.XZA972_TAG_NM;
		}

		public static int xza972SpePrcCd(int idx) {
			return xza972JusCd(idx) + Len.XZA972_JUS_CD;
		}

		public static int xza972LenNbr(int idx) {
			return xza972SpePrcCd(idx) + Len.XZA972_SPE_PRC_CD;
		}

		public static int xza972TagOccCnt(int idx) {
			return xza972LenNbr(idx) + Len.XZA972_LEN_NBR;
		}

		public static int xza972DelInd(int idx) {
			return xza972TagOccCnt(idx) + Len.XZA972_TAG_OCC_CNT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA972_MAX_TAG_ROWS = 2;
		public static final int XZA972_EDL_FRM_NM = 30;
		public static final int XZA972_DTA_GRP_NM = 8;
		public static final int XZA972_DTA_GRP_FLD_NBR = 5;
		public static final int XZA972_TAG_NM = 30;
		public static final int XZA972_JUS_CD = 1;
		public static final int XZA972_SPE_PRC_CD = 8;
		public static final int XZA972_LEN_NBR = 5;
		public static final int XZA972_TAG_OCC_CNT = 5;
		public static final int XZA972_DEL_IND = 1;
		public static final int XZA972_ACY_TAG_INFO = XZA972_EDL_FRM_NM + XZA972_DTA_GRP_NM + XZA972_DTA_GRP_FLD_NBR + XZA972_TAG_NM + XZA972_JUS_CD
				+ XZA972_SPE_PRC_CD + XZA972_LEN_NBR + XZA972_TAG_OCC_CNT + XZA972_DEL_IND;
		public static final int XZA972_ACY_TAG_ROW = XZA972_MAX_TAG_ROWS + XZA972_ACY_TAG_INFO;
		public static final int L_FW_RESP_XZ0A9072 = XZA972_ACY_TAG_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0r9072.L_FW_RESP_XZ0A9072_MAXOCCURS * L_FW_RESP_XZ0A9072;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA972R_DTA_GRP_FLD_NBR = 5;
			public static final int XZA972R_LEN_NBR = 5;
			public static final int XZA972R_TAG_OCC_CNT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
