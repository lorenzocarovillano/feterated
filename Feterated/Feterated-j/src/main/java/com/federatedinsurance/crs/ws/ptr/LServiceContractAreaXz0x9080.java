/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9080<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9080 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT98O_TTY_LIST_MAXOCCURS = 2500;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9080() {
	}

	public LServiceContractAreaXz0x9080(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt98iTkNotPrcTs(String xzt98iTkNotPrcTs) {
		writeString(Pos.XZT98I_TK_NOT_PRC_TS, xzt98iTkNotPrcTs, Len.XZT98I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT98I-TK-NOT-PRC-TS<br>*/
	public String getXzt98iTkNotPrcTs() {
		return readString(Pos.XZT98I_TK_NOT_PRC_TS, Len.XZT98I_TK_NOT_PRC_TS);
	}

	public void setXzt98iCsrActNbr(String xzt98iCsrActNbr) {
		writeString(Pos.XZT98I_CSR_ACT_NBR, xzt98iCsrActNbr, Len.XZT98I_CSR_ACT_NBR);
	}

	/**Original name: XZT98I-CSR-ACT-NBR<br>*/
	public String getXzt98iCsrActNbr() {
		return readString(Pos.XZT98I_CSR_ACT_NBR, Len.XZT98I_CSR_ACT_NBR);
	}

	public void setXzt98iUserid(String xzt98iUserid) {
		writeString(Pos.XZT98I_USERID, xzt98iUserid, Len.XZT98I_USERID);
	}

	/**Original name: XZT98I-USERID<br>*/
	public String getXzt98iUserid() {
		return readString(Pos.XZT98I_USERID, Len.XZT98I_USERID);
	}

	public String getXzt98iUseridFormatted() {
		return Functions.padBlanks(getXzt98iUserid(), Len.XZT98I_USERID);
	}

	public void setXzt98oTkNotPrcTs(String xzt98oTkNotPrcTs) {
		writeString(Pos.XZT98O_TK_NOT_PRC_TS, xzt98oTkNotPrcTs, Len.XZT98O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT98O-TK-NOT-PRC-TS<br>*/
	public String getXzt98oTkNotPrcTs() {
		return readString(Pos.XZT98O_TK_NOT_PRC_TS, Len.XZT98O_TK_NOT_PRC_TS);
	}

	public void setXzt98oCsrActNbr(String xzt98oCsrActNbr) {
		writeString(Pos.XZT98O_CSR_ACT_NBR, xzt98oCsrActNbr, Len.XZT98O_CSR_ACT_NBR);
	}

	/**Original name: XZT98O-CSR-ACT-NBR<br>*/
	public String getXzt98oCsrActNbr() {
		return readString(Pos.XZT98O_CSR_ACT_NBR, Len.XZT98O_CSR_ACT_NBR);
	}

	public void setXzt98oTkClientId(int xzt98oTkClientIdIdx, String xzt98oTkClientId) {
		int position = Pos.xzt98oTkClientId(xzt98oTkClientIdIdx - 1);
		writeString(position, xzt98oTkClientId, Len.XZT98O_TK_CLIENT_ID);
	}

	/**Original name: XZT98O-TK-CLIENT-ID<br>*/
	public String getXzt98oTkClientId(int xzt98oTkClientIdIdx) {
		int position = Pos.xzt98oTkClientId(xzt98oTkClientIdIdx - 1);
		return readString(position, Len.XZT98O_TK_CLIENT_ID);
	}

	public void setXzt98oTkAdrId(int xzt98oTkAdrIdIdx, String xzt98oTkAdrId) {
		int position = Pos.xzt98oTkAdrId(xzt98oTkAdrIdIdx - 1);
		writeString(position, xzt98oTkAdrId, Len.XZT98O_TK_ADR_ID);
	}

	/**Original name: XZT98O-TK-ADR-ID<br>*/
	public String getXzt98oTkAdrId(int xzt98oTkAdrIdIdx) {
		int position = Pos.xzt98oTkAdrId(xzt98oTkAdrIdIdx - 1);
		return readString(position, Len.XZT98O_TK_ADR_ID);
	}

	public void setXzt98oRecTypCd(int xzt98oRecTypCdIdx, String xzt98oRecTypCd) {
		int position = Pos.xzt98oRecTypCd(xzt98oRecTypCdIdx - 1);
		writeString(position, xzt98oRecTypCd, Len.XZT98O_REC_TYP_CD);
	}

	/**Original name: XZT98O-REC-TYP-CD<br>*/
	public String getXzt98oRecTypCd(int xzt98oRecTypCdIdx) {
		int position = Pos.xzt98oRecTypCd(xzt98oRecTypCdIdx - 1);
		return readString(position, Len.XZT98O_REC_TYP_CD);
	}

	public void setXzt98oRecTypDes(int xzt98oRecTypDesIdx, String xzt98oRecTypDes) {
		int position = Pos.xzt98oRecTypDes(xzt98oRecTypDesIdx - 1);
		writeString(position, xzt98oRecTypDes, Len.XZT98O_REC_TYP_DES);
	}

	/**Original name: XZT98O-REC-TYP-DES<br>*/
	public String getXzt98oRecTypDes(int xzt98oRecTypDesIdx) {
		int position = Pos.xzt98oRecTypDes(xzt98oRecTypDesIdx - 1);
		return readString(position, Len.XZT98O_REC_TYP_DES);
	}

	public void setXzt98oName(int xzt98oNameIdx, String xzt98oName) {
		int position = Pos.xzt98oName(xzt98oNameIdx - 1);
		writeString(position, xzt98oName, Len.XZT98O_NAME);
	}

	/**Original name: XZT98O-NAME<br>*/
	public String getXzt98oName(int xzt98oNameIdx) {
		int position = Pos.xzt98oName(xzt98oNameIdx - 1);
		return readString(position, Len.XZT98O_NAME);
	}

	public void setXzt98oAdrLin1(int xzt98oAdrLin1Idx, String xzt98oAdrLin1) {
		int position = Pos.xzt98oAdrLin1(xzt98oAdrLin1Idx - 1);
		writeString(position, xzt98oAdrLin1, Len.XZT98O_ADR_LIN1);
	}

	/**Original name: XZT98O-ADR-LIN1<br>*/
	public String getXzt98oAdrLin1(int xzt98oAdrLin1Idx) {
		int position = Pos.xzt98oAdrLin1(xzt98oAdrLin1Idx - 1);
		return readString(position, Len.XZT98O_ADR_LIN1);
	}

	public void setXzt98oAdrLin2(int xzt98oAdrLin2Idx, String xzt98oAdrLin2) {
		int position = Pos.xzt98oAdrLin2(xzt98oAdrLin2Idx - 1);
		writeString(position, xzt98oAdrLin2, Len.XZT98O_ADR_LIN2);
	}

	/**Original name: XZT98O-ADR-LIN2<br>*/
	public String getXzt98oAdrLin2(int xzt98oAdrLin2Idx) {
		int position = Pos.xzt98oAdrLin2(xzt98oAdrLin2Idx - 1);
		return readString(position, Len.XZT98O_ADR_LIN2);
	}

	public void setXzt98oCityNm(int xzt98oCityNmIdx, String xzt98oCityNm) {
		int position = Pos.xzt98oCityNm(xzt98oCityNmIdx - 1);
		writeString(position, xzt98oCityNm, Len.XZT98O_CITY_NM);
	}

	/**Original name: XZT98O-CITY-NM<br>*/
	public String getXzt98oCityNm(int xzt98oCityNmIdx) {
		int position = Pos.xzt98oCityNm(xzt98oCityNmIdx - 1);
		return readString(position, Len.XZT98O_CITY_NM);
	}

	public void setXzt98oStateAbb(int xzt98oStateAbbIdx, String xzt98oStateAbb) {
		int position = Pos.xzt98oStateAbb(xzt98oStateAbbIdx - 1);
		writeString(position, xzt98oStateAbb, Len.XZT98O_STATE_ABB);
	}

	/**Original name: XZT98O-STATE-ABB<br>*/
	public String getXzt98oStateAbb(int xzt98oStateAbbIdx) {
		int position = Pos.xzt98oStateAbb(xzt98oStateAbbIdx - 1);
		return readString(position, Len.XZT98O_STATE_ABB);
	}

	public void setXzt98oPstCd(int xzt98oPstCdIdx, String xzt98oPstCd) {
		int position = Pos.xzt98oPstCd(xzt98oPstCdIdx - 1);
		writeString(position, xzt98oPstCd, Len.XZT98O_PST_CD);
	}

	/**Original name: XZT98O-PST-CD<br>*/
	public String getXzt98oPstCd(int xzt98oPstCdIdx) {
		int position = Pos.xzt98oPstCd(xzt98oPstCdIdx - 1);
		return readString(position, Len.XZT98O_PST_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT908_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT98I_TECHNICAL_KEY = XZT908_SERVICE_INPUTS;
		public static final int XZT98I_TK_NOT_PRC_TS = XZT98I_TECHNICAL_KEY;
		public static final int XZT98I_CSR_ACT_NBR = XZT98I_TK_NOT_PRC_TS + Len.XZT98I_TK_NOT_PRC_TS;
		public static final int XZT98I_USERID = XZT98I_CSR_ACT_NBR + Len.XZT98I_CSR_ACT_NBR;
		public static final int XZT908_SERVICE_OUTPUTS = XZT98I_USERID + Len.XZT98I_USERID;
		public static final int XZT98O_TECHNICAL_KEY = XZT908_SERVICE_OUTPUTS;
		public static final int XZT98O_TK_NOT_PRC_TS = XZT98O_TECHNICAL_KEY;
		public static final int XZT98O_CSR_ACT_NBR = XZT98O_TK_NOT_PRC_TS + Len.XZT98O_TK_NOT_PRC_TS;
		public static final int XZT98O_TTY_LIST_TBL = XZT98O_CSR_ACT_NBR + Len.XZT98O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt98oTtyList(int idx) {
			return XZT98O_TTY_LIST_TBL + idx * Len.XZT98O_TTY_LIST;
		}

		public static int xzt98oTtyTechnicalKey(int idx) {
			return xzt98oTtyList(idx);
		}

		public static int xzt98oTkClientId(int idx) {
			return xzt98oTtyTechnicalKey(idx);
		}

		public static int xzt98oTkAdrId(int idx) {
			return xzt98oTkClientId(idx) + Len.XZT98O_TK_CLIENT_ID;
		}

		public static int xzt98oRecTyp(int idx) {
			return xzt98oTkAdrId(idx) + Len.XZT98O_TK_ADR_ID;
		}

		public static int xzt98oRecTypCd(int idx) {
			return xzt98oRecTyp(idx);
		}

		public static int xzt98oRecTypDes(int idx) {
			return xzt98oRecTypCd(idx) + Len.XZT98O_REC_TYP_CD;
		}

		public static int xzt98oName(int idx) {
			return xzt98oRecTypDes(idx) + Len.XZT98O_REC_TYP_DES;
		}

		public static int xzt98oAdrLin1(int idx) {
			return xzt98oName(idx) + Len.XZT98O_NAME;
		}

		public static int xzt98oAdrLin2(int idx) {
			return xzt98oAdrLin1(idx) + Len.XZT98O_ADR_LIN1;
		}

		public static int xzt98oCityNm(int idx) {
			return xzt98oAdrLin2(idx) + Len.XZT98O_ADR_LIN2;
		}

		public static int xzt98oStateAbb(int idx) {
			return xzt98oCityNm(idx) + Len.XZT98O_CITY_NM;
		}

		public static int xzt98oPstCd(int idx) {
			return xzt98oStateAbb(idx) + Len.XZT98O_STATE_ABB;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT98I_TK_NOT_PRC_TS = 26;
		public static final int XZT98I_CSR_ACT_NBR = 9;
		public static final int XZT98I_USERID = 8;
		public static final int XZT98O_TK_NOT_PRC_TS = 26;
		public static final int XZT98O_CSR_ACT_NBR = 9;
		public static final int XZT98O_TK_CLIENT_ID = 64;
		public static final int XZT98O_TK_ADR_ID = 64;
		public static final int XZT98O_TTY_TECHNICAL_KEY = XZT98O_TK_CLIENT_ID + XZT98O_TK_ADR_ID;
		public static final int XZT98O_REC_TYP_CD = 5;
		public static final int XZT98O_REC_TYP_DES = 13;
		public static final int XZT98O_REC_TYP = XZT98O_REC_TYP_CD + XZT98O_REC_TYP_DES;
		public static final int XZT98O_NAME = 120;
		public static final int XZT98O_ADR_LIN1 = 45;
		public static final int XZT98O_ADR_LIN2 = 45;
		public static final int XZT98O_CITY_NM = 30;
		public static final int XZT98O_STATE_ABB = 2;
		public static final int XZT98O_PST_CD = 13;
		public static final int XZT98O_TTY_LIST = XZT98O_TTY_TECHNICAL_KEY + XZT98O_REC_TYP + XZT98O_NAME + XZT98O_ADR_LIN1 + XZT98O_ADR_LIN2
				+ XZT98O_CITY_NM + XZT98O_STATE_ABB + XZT98O_PST_CD;
		public static final int XZT98I_TECHNICAL_KEY = XZT98I_TK_NOT_PRC_TS;
		public static final int XZT908_SERVICE_INPUTS = XZT98I_TECHNICAL_KEY + XZT98I_CSR_ACT_NBR + XZT98I_USERID;
		public static final int XZT98O_TECHNICAL_KEY = XZT98O_TK_NOT_PRC_TS;
		public static final int XZT98O_TTY_LIST_TBL = LServiceContractAreaXz0x9080.XZT98O_TTY_LIST_MAXOCCURS * XZT98O_TTY_LIST;
		public static final int XZT908_SERVICE_OUTPUTS = XZT98O_TECHNICAL_KEY + XZT98O_CSR_ACT_NBR + XZT98O_TTY_LIST_TBL;
		public static final int L_SERVICE_CONTRACT_AREA = XZT908_SERVICE_INPUTS + XZT908_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
