/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: W-READ-VSAM-IND<br>
 * Variable: W-READ-VSAM-IND from program HALOETRA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WReadVsamInd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.READ_VSAM_IND);
	public static final String NOT_FOUND = "0";
	public static final String FOUND = "1";

	//==== METHODS ====
	public void setReadVsamInd(short readVsamInd) {
		this.value = NumericDisplay.asString(readVsamInd, Len.READ_VSAM_IND);
	}

	public void setReadVsamIndFormatted(String readVsamInd) {
		this.value = Trunc.toUnsignedNumeric(readVsamInd, Len.READ_VSAM_IND);
	}

	public short getReadVsamInd() {
		return NumericDisplay.asShort(this.value);
	}

	public String getReadVsamIndFormatted() {
		return this.value;
	}

	public void setNotFound() {
		setReadVsamIndFormatted(NOT_FOUND);
	}

	public boolean isFound() {
		return getReadVsamIndFormatted().equals(FOUND);
	}

	public void setFound() {
		setReadVsamIndFormatted(FOUND);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int READ_VSAM_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
