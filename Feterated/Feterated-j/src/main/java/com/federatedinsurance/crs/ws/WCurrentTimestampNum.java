/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: W-CURRENT-TIMESTAMP-NUM<br>
 * Variable: W-CURRENT-TIMESTAMP-NUM from program HALOUIDG<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WCurrentTimestampNum {

	//==== PROPERTIES ====
	//Original name: W-YEAR-NUM
	private String yearNum = DefaultValues.stringVal(Len.YEAR_NUM);
	//Original name: FILLER-W-CURRENT-TIMESTAMP-NUM
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: W-MONTH-NUM
	private String monthNum = DefaultValues.stringVal(Len.MONTH_NUM);
	//Original name: FILLER-W-CURRENT-TIMESTAMP-NUM-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: W-DAY-NUM
	private String dayNum = DefaultValues.stringVal(Len.DAY_NUM);
	//Original name: FILLER-W-CURRENT-TIMESTAMP-NUM-2
	private char flr3 = DefaultValues.CHAR_VAL;
	//Original name: W-HOURS-NUM
	private String hoursNum = DefaultValues.stringVal(Len.HOURS_NUM);
	//Original name: FILLER-W-CURRENT-TIMESTAMP-NUM-3
	private char flr4 = DefaultValues.CHAR_VAL;
	//Original name: W-MINUTES-NUM
	private String minutesNum = DefaultValues.stringVal(Len.MINUTES_NUM);
	//Original name: FILLER-W-CURRENT-TIMESTAMP-NUM-4
	private char flr5 = DefaultValues.CHAR_VAL;
	//Original name: W-SECONDS-NUM
	private String secondsNum = DefaultValues.stringVal(Len.SECONDS_NUM);
	//Original name: FILLER-W-CURRENT-TIMESTAMP-NUM-5
	private char flr6 = DefaultValues.CHAR_VAL;
	//Original name: W-MICROS-NUM
	private String microsNum = DefaultValues.stringVal(Len.MICROS_NUM);

	//==== METHODS ====
	public void setwCurrentTimestamp(String wCurrentTimestamp) {
		int position = 1;
		byte[] buffer = getwCurrentTimestampNumBytes();
		MarshalByte.writeString(buffer, position, wCurrentTimestamp, Len.W_CURRENT_TIMESTAMP);
		setwCurrentTimestampNumBytes(buffer);
	}

	/**Original name: W-CURRENT-TIMESTAMP<br>*/
	public String getwCurrentTimestamp() {
		int position = 1;
		return MarshalByte.readString(getwCurrentTimestampNumBytes(), position, Len.W_CURRENT_TIMESTAMP);
	}

	public void setwCurrentTimestampNumBytes(byte[] buffer) {
		setwCurrentTimestampNumBytes(buffer, 1);
	}

	/**Original name: W-CURRENT-TIMESTAMP-NUM<br>*/
	public byte[] getwCurrentTimestampNumBytes() {
		byte[] buffer = new byte[Len.W_CURRENT_TIMESTAMP_NUM];
		return getwCurrentTimestampNumBytes(buffer, 1);
	}

	public void setwCurrentTimestampNumBytes(byte[] buffer, int offset) {
		int position = offset;
		yearNum = MarshalByte.readFixedString(buffer, position, Len.YEAR_NUM);
		position += Len.YEAR_NUM;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		monthNum = MarshalByte.readFixedString(buffer, position, Len.MONTH_NUM);
		position += Len.MONTH_NUM;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dayNum = MarshalByte.readFixedString(buffer, position, Len.DAY_NUM);
		position += Len.DAY_NUM;
		flr3 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		hoursNum = MarshalByte.readFixedString(buffer, position, Len.HOURS_NUM);
		position += Len.HOURS_NUM;
		flr4 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		minutesNum = MarshalByte.readFixedString(buffer, position, Len.MINUTES_NUM);
		position += Len.MINUTES_NUM;
		flr5 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		secondsNum = MarshalByte.readFixedString(buffer, position, Len.SECONDS_NUM);
		position += Len.SECONDS_NUM;
		flr6 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		microsNum = MarshalByte.readFixedString(buffer, position, Len.MICROS_NUM);
	}

	public byte[] getwCurrentTimestampNumBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, yearNum, Len.YEAR_NUM);
		position += Len.YEAR_NUM;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, monthNum, Len.MONTH_NUM);
		position += Len.MONTH_NUM;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dayNum, Len.DAY_NUM);
		position += Len.DAY_NUM;
		MarshalByte.writeChar(buffer, position, flr3);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hoursNum, Len.HOURS_NUM);
		position += Len.HOURS_NUM;
		MarshalByte.writeChar(buffer, position, flr4);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, minutesNum, Len.MINUTES_NUM);
		position += Len.MINUTES_NUM;
		MarshalByte.writeChar(buffer, position, flr5);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, secondsNum, Len.SECONDS_NUM);
		position += Len.SECONDS_NUM;
		MarshalByte.writeChar(buffer, position, flr6);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, microsNum, Len.MICROS_NUM);
		return buffer;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setFlr3(char flr3) {
		this.flr3 = flr3;
	}

	public char getFlr3() {
		return this.flr3;
	}

	public short getHoursNum() {
		return NumericDisplay.asShort(this.hoursNum);
	}

	public void setFlr4(char flr4) {
		this.flr4 = flr4;
	}

	public char getFlr4() {
		return this.flr4;
	}

	public short getMinutesNum() {
		return NumericDisplay.asShort(this.minutesNum);
	}

	public void setFlr5(char flr5) {
		this.flr5 = flr5;
	}

	public char getFlr5() {
		return this.flr5;
	}

	public short getSecondsNum() {
		return NumericDisplay.asShort(this.secondsNum);
	}

	public void setFlr6(char flr6) {
		this.flr6 = flr6;
	}

	public char getFlr6() {
		return this.flr6;
	}

	public void setMicrosNum(int microsNum) {
		this.microsNum = NumericDisplay.asString(microsNum, Len.MICROS_NUM);
	}

	public int getMicrosNum() {
		return NumericDisplay.asInt(this.microsNum);
	}

	public String getMicrosNumFormatted() {
		return this.microsNum;
	}

	public String getMicrosNumAsString() {
		return getMicrosNumFormatted();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YEAR_NUM = 4;
		public static final int FLR1 = 1;
		public static final int MONTH_NUM = 2;
		public static final int DAY_NUM = 2;
		public static final int HOURS_NUM = 2;
		public static final int MINUTES_NUM = 2;
		public static final int SECONDS_NUM = 2;
		public static final int MICROS_NUM = 6;
		public static final int W_CURRENT_TIMESTAMP_NUM = YEAR_NUM + MONTH_NUM + DAY_NUM + HOURS_NUM + MINUTES_NUM + SECONDS_NUM + MICROS_NUM
				+ 6 * FLR1;
		public static final int W_CURRENT_TIMESTAMP = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int W_CURRENT_TIMESTAMP = 26;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
