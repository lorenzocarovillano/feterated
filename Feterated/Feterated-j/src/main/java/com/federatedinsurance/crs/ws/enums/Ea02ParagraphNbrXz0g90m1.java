/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-PARAGRAPH-NBR<br>
 * Variable: EA-02-PARAGRAPH-NBR from program XZ0G90M1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ea02ParagraphNbrXz0g90m1 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	public static final String PN2100 = "2100";
	public static final String PN8100 = "8100";

	//==== METHODS ====
	public void setParagraphNbr(String paragraphNbr) {
		this.value = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.value;
	}

	public void setPn2100() {
		value = PN2100;
	}

	public void setPn8100() {
		value = PN8100;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PARAGRAPH_NBR = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
