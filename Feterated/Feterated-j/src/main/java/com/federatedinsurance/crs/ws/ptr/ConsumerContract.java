/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: CONSUMER-CONTRACT<br>
 * Variable: CONSUMER-CONTRACT from program XZ0G90M1<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class ConsumerContract extends BytesClass {

	//==== PROPERTIES ====
	public static final int M1I_UPDATE_NOT_LIST_MAXOCCURS = 1000;

	//==== CONSTRUCTORS ====
	public ConsumerContract() {
	}

	public ConsumerContract(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.CONSUMER_CONTRACT;
	}

	public byte[] getConsumerContractBytes() {
		byte[] buffer = new byte[Len.CONSUMER_CONTRACT];
		return getConsumerContractBytes(buffer, 1);
	}

	public byte[] getConsumerContractBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.CONSUMER_CONTRACT, Pos.CONSUMER_CONTRACT);
		return buffer;
	}

	public void setXzt90m1ServiceInputsBytes(byte[] buffer) {
		setXzt90m1ServiceInputsBytes(buffer, 1);
	}

	public void setXzt90m1ServiceInputsBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.XZT90M1_SERVICE_INPUTS, Pos.XZT90M1_SERVICE_INPUTS);
	}

	public void setM1iTkActNotStaCd(String m1iTkActNotStaCd) {
		writeString(Pos.M1I_TK_ACT_NOT_STA_CD, m1iTkActNotStaCd, Len.M1I_TK_ACT_NOT_STA_CD);
	}

	/**Original name: XZT9M1I-TK-ACT-NOT-STA-CD<br>*/
	public String getM1iTkActNotStaCd() {
		return readString(Pos.M1I_TK_ACT_NOT_STA_CD, Len.M1I_TK_ACT_NOT_STA_CD);
	}

	public void setM1iUserid(String m1iUserid) {
		writeString(Pos.M1I_USERID, m1iUserid, Len.M1I_USERID);
	}

	/**Original name: XZT9M1I-USERID<br>*/
	public String getM1iUserid() {
		return readString(Pos.M1I_USERID, Len.M1I_USERID);
	}

	public void setM1iTkNotPrcTs(int m1iTkNotPrcTsIdx, String m1iTkNotPrcTs) {
		int position = Pos.xzt9m1iTkNotPrcTs(m1iTkNotPrcTsIdx - 1);
		writeString(position, m1iTkNotPrcTs, Len.M1I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9M1I-TK-NOT-PRC-TS<br>*/
	public String getM1iTkNotPrcTs(int m1iTkNotPrcTsIdx) {
		int position = Pos.xzt9m1iTkNotPrcTs(m1iTkNotPrcTsIdx - 1);
		return readString(position, Len.M1I_TK_NOT_PRC_TS);
	}

	public void setM1iCsrActNbr(int m1iCsrActNbrIdx, String m1iCsrActNbr) {
		int position = Pos.xzt9m1iCsrActNbr(m1iCsrActNbrIdx - 1);
		writeString(position, m1iCsrActNbr, Len.M1I_CSR_ACT_NBR);
	}

	/**Original name: XZT9M1I-CSR-ACT-NBR<br>*/
	public String getM1iCsrActNbr(int m1iCsrActNbrIdx) {
		int position = Pos.xzt9m1iCsrActNbr(m1iCsrActNbrIdx - 1);
		return readString(position, Len.M1I_CSR_ACT_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int CONSUMER_CONTRACT = 1;
		public static final int XZT90M1_SERVICE_INPUTS = CONSUMER_CONTRACT;
		public static final int M1I_TECHNICAL_KEY = XZT90M1_SERVICE_INPUTS;
		public static final int M1I_TK_ACT_NOT_STA_CD = M1I_TECHNICAL_KEY;
		public static final int M1I_USERID = M1I_TK_ACT_NOT_STA_CD + Len.M1I_TK_ACT_NOT_STA_CD;
		public static final int M1I_UPDATE_NOT_LIST_TBL = M1I_USERID + Len.M1I_USERID;
		public static final int XZT90M1_SERVICE_OUTPUTS = xzt9m1iCsrActNbr(M1I_UPDATE_NOT_LIST_MAXOCCURS - 1) + Len.M1I_CSR_ACT_NBR;
		public static final int FLR1 = XZT90M1_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt9m1iUpdateNotList(int idx) {
			return M1I_UPDATE_NOT_LIST_TBL + idx * Len.M1I_UPDATE_NOT_LIST;
		}

		public static int xzt9m1iListTechnicalKey(int idx) {
			return xzt9m1iUpdateNotList(idx);
		}

		public static int xzt9m1iTkNotPrcTs(int idx) {
			return xzt9m1iListTechnicalKey(idx);
		}

		public static int xzt9m1iCsrActNbr(int idx) {
			return xzt9m1iTkNotPrcTs(idx) + Len.M1I_TK_NOT_PRC_TS;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int M1I_TK_ACT_NOT_STA_CD = 2;
		public static final int M1I_USERID = 8;
		public static final int M1I_TK_NOT_PRC_TS = 26;
		public static final int M1I_LIST_TECHNICAL_KEY = M1I_TK_NOT_PRC_TS;
		public static final int M1I_CSR_ACT_NBR = 9;
		public static final int M1I_UPDATE_NOT_LIST = M1I_LIST_TECHNICAL_KEY + M1I_CSR_ACT_NBR;
		public static final int M1I_TECHNICAL_KEY = M1I_TK_ACT_NOT_STA_CD;
		public static final int M1I_UPDATE_NOT_LIST_TBL = ConsumerContract.M1I_UPDATE_NOT_LIST_MAXOCCURS * M1I_UPDATE_NOT_LIST;
		public static final int XZT90M1_SERVICE_INPUTS = M1I_TECHNICAL_KEY + M1I_USERID + M1I_UPDATE_NOT_LIST_TBL;
		public static final int FLR1 = 1;
		public static final int XZT90M1_SERVICE_OUTPUTS = FLR1;
		public static final int CONSUMER_CONTRACT = XZT90M1_SERVICE_INPUTS + XZT90M1_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
