/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90R0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90r0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90r0() {
	}

	public LServiceContractAreaXz0x90r0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt9r0ServiceInputsBytes(byte[] buffer) {
		setXzt9r0ServiceInputsBytes(buffer, 1);
	}

	public void setXzt9r0ServiceInputsBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.XZT9R0_SERVICE_INPUTS, Pos.XZT9R0_SERVICE_INPUTS);
	}

	public void setiPolNbr(String iPolNbr) {
		writeString(Pos.I_POL_NBR, iPolNbr, Len.I_POL_NBR);
	}

	/**Original name: XZT9RI-POL-NBR<br>*/
	public String getiPolNbr() {
		return readString(Pos.I_POL_NBR, Len.I_POL_NBR);
	}

	public void setiPolEffDt(String iPolEffDt) {
		writeString(Pos.I_POL_EFF_DT, iPolEffDt, Len.I_POL_EFF_DT);
	}

	/**Original name: XZT9RI-POL-EFF-DT<br>*/
	public String getiPolEffDt() {
		return readString(Pos.I_POL_EFF_DT, Len.I_POL_EFF_DT);
	}

	public void setiPolExpDt(String iPolExpDt) {
		writeString(Pos.I_POL_EXP_DT, iPolExpDt, Len.I_POL_EXP_DT);
	}

	/**Original name: XZT9RI-POL-EXP-DT<br>*/
	public String getiPolExpDt() {
		return readString(Pos.I_POL_EXP_DT, Len.I_POL_EXP_DT);
	}

	public void setiUserid(String iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT9RI-USERID<br>*/
	public String getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	public String getiUseridFormatted() {
		return Functions.padBlanks(getiUserid(), Len.I_USERID);
	}

	/**Original name: XZT9R0-SERVICE-OUTPUTS<br>*/
	public byte[] getXzt9r0ServiceOutputsBytes() {
		byte[] buffer = new byte[Len.XZT9R0_SERVICE_OUTPUTS];
		return getXzt9r0ServiceOutputsBytes(buffer, 1);
	}

	public byte[] getXzt9r0ServiceOutputsBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.XZT9R0_SERVICE_OUTPUTS, Pos.XZT9R0_SERVICE_OUTPUTS);
		return buffer;
	}

	public void setoPolNbr(String oPolNbr) {
		writeString(Pos.O_POL_NBR, oPolNbr, Len.O_POL_NBR);
	}

	/**Original name: XZT9RO-POL-NBR<br>*/
	public String getoPolNbr() {
		return readString(Pos.O_POL_NBR, Len.O_POL_NBR);
	}

	public void setoPolEffDt(String oPolEffDt) {
		writeString(Pos.O_POL_EFF_DT, oPolEffDt, Len.O_POL_EFF_DT);
	}

	/**Original name: XZT9RO-POL-EFF-DT<br>*/
	public String getoPolEffDt() {
		return readString(Pos.O_POL_EFF_DT, Len.O_POL_EFF_DT);
	}

	public void setoPolExpDt(String oPolExpDt) {
		writeString(Pos.O_POL_EXP_DT, oPolExpDt, Len.O_POL_EXP_DT);
	}

	/**Original name: XZT9RO-POL-EXP-DT<br>*/
	public String getoPolExpDt() {
		return readString(Pos.O_POL_EXP_DT, Len.O_POL_EXP_DT);
	}

	public void setoPndCncInd(char oPndCncInd) {
		writeChar(Pos.O_PND_CNC_IND, oPndCncInd);
	}

	/**Original name: XZT9RO-PND-CNC-IND<br>*/
	public char getoPndCncInd() {
		return readChar(Pos.O_PND_CNC_IND);
	}

	public void setoSchCncDt(String oSchCncDt) {
		writeString(Pos.O_SCH_CNC_DT, oSchCncDt, Len.O_SCH_CNC_DT);
	}

	/**Original name: XZT9RO-SCH-CNC-DT<br>*/
	public String getoSchCncDt() {
		return readString(Pos.O_SCH_CNC_DT, Len.O_SCH_CNC_DT);
	}

	public void setoNotTypCd(String oNotTypCd) {
		writeString(Pos.O_NOT_TYP_CD, oNotTypCd, Len.O_NOT_TYP_CD);
	}

	/**Original name: XZT9RO-NOT-TYP-CD<br>*/
	public String getoNotTypCd() {
		return readString(Pos.O_NOT_TYP_CD, Len.O_NOT_TYP_CD);
	}

	public void setoNotTypDes(String oNotTypDes) {
		writeString(Pos.O_NOT_TYP_DES, oNotTypDes, Len.O_NOT_TYP_DES);
	}

	/**Original name: XZT9RO-NOT-TYP-DES<br>*/
	public String getoNotTypDes() {
		return readString(Pos.O_NOT_TYP_DES, Len.O_NOT_TYP_DES);
	}

	public void setoNotEmpId(String oNotEmpId) {
		writeString(Pos.O_NOT_EMP_ID, oNotEmpId, Len.O_NOT_EMP_ID);
	}

	/**Original name: XZT9RO-NOT-EMP-ID<br>*/
	public String getoNotEmpId() {
		return readString(Pos.O_NOT_EMP_ID, Len.O_NOT_EMP_ID);
	}

	public void setoNotEmpNm(String oNotEmpNm) {
		writeString(Pos.O_NOT_EMP_NM, oNotEmpNm, Len.O_NOT_EMP_NM);
	}

	/**Original name: XZT9RO-NOT-EMP-NM<br>*/
	public String getoNotEmpNm() {
		return readString(Pos.O_NOT_EMP_NM, Len.O_NOT_EMP_NM);
	}

	public void setoNotPrcDt(String oNotPrcDt) {
		writeString(Pos.O_NOT_PRC_DT, oNotPrcDt, Len.O_NOT_PRC_DT);
	}

	/**Original name: XZT9RO-NOT-PRC-DT<br>*/
	public String getoNotPrcDt() {
		return readString(Pos.O_NOT_PRC_DT, Len.O_NOT_PRC_DT);
	}

	public void setoUwTmnFlg(char oUwTmnFlg) {
		writeChar(Pos.O_UW_TMN_FLG, oUwTmnFlg);
	}

	/**Original name: XZT9RO-UW-TMN-FLG<br>*/
	public char getoUwTmnFlg() {
		return readChar(Pos.O_UW_TMN_FLG);
	}

	public void setoTmnEmpId(String oTmnEmpId) {
		writeString(Pos.O_TMN_EMP_ID, oTmnEmpId, Len.O_TMN_EMP_ID);
	}

	/**Original name: XZT9RO-TMN-EMP-ID<br>*/
	public String getoTmnEmpId() {
		return readString(Pos.O_TMN_EMP_ID, Len.O_TMN_EMP_ID);
	}

	public void setoTmnEmpNm(String oTmnEmpNm) {
		writeString(Pos.O_TMN_EMP_NM, oTmnEmpNm, Len.O_TMN_EMP_NM);
	}

	/**Original name: XZT9RO-TMN-EMP-NM<br>*/
	public String getoTmnEmpNm() {
		return readString(Pos.O_TMN_EMP_NM, Len.O_TMN_EMP_NM);
	}

	public void setoTmnPrcDt(String oTmnPrcDt) {
		writeString(Pos.O_TMN_PRC_DT, oTmnPrcDt, Len.O_TMN_PRC_DT);
	}

	/**Original name: XZT9RO-TMN-PRC-DT<br>*/
	public String getoTmnPrcDt() {
		return readString(Pos.O_TMN_PRC_DT, Len.O_TMN_PRC_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT9R0_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_POL_NBR = XZT9R0_SERVICE_INPUTS;
		public static final int I_POL_EFF_DT = I_POL_NBR + Len.I_POL_NBR;
		public static final int I_POL_EXP_DT = I_POL_EFF_DT + Len.I_POL_EFF_DT;
		public static final int I_USERID = I_POL_EXP_DT + Len.I_POL_EXP_DT;
		public static final int FLR1 = I_USERID + Len.I_USERID;
		public static final int XZT9R0_SERVICE_OUTPUTS = FLR1 + Len.FLR1;
		public static final int O_POL_KEYS = XZT9R0_SERVICE_OUTPUTS;
		public static final int O_POL_NBR = O_POL_KEYS;
		public static final int O_POL_EFF_DT = O_POL_NBR + Len.O_POL_NBR;
		public static final int O_POL_EXP_DT = O_POL_EFF_DT + Len.O_POL_EFF_DT;
		public static final int O_CNC_DATA = O_POL_EXP_DT + Len.O_POL_EXP_DT;
		public static final int O_PND_CNC_IND = O_CNC_DATA;
		public static final int O_SCH_CNC_DT = O_PND_CNC_IND + Len.O_PND_CNC_IND;
		public static final int O_NOT_TYP_CD = O_SCH_CNC_DT + Len.O_SCH_CNC_DT;
		public static final int O_NOT_TYP_DES = O_NOT_TYP_CD + Len.O_NOT_TYP_CD;
		public static final int O_NOT_EMP_ID = O_NOT_TYP_DES + Len.O_NOT_TYP_DES;
		public static final int O_NOT_EMP_NM = O_NOT_EMP_ID + Len.O_NOT_EMP_ID;
		public static final int O_NOT_PRC_DT = O_NOT_EMP_NM + Len.O_NOT_EMP_NM;
		public static final int O_TMN_DATA = O_NOT_PRC_DT + Len.O_NOT_PRC_DT;
		public static final int O_UW_TMN_FLG = O_TMN_DATA;
		public static final int O_TMN_EMP_ID = O_UW_TMN_FLG + Len.O_UW_TMN_FLG;
		public static final int O_TMN_EMP_NM = O_TMN_EMP_ID + Len.O_TMN_EMP_ID;
		public static final int O_TMN_PRC_DT = O_TMN_EMP_NM + Len.O_TMN_EMP_NM;
		public static final int FLR2 = O_TMN_PRC_DT + Len.O_TMN_PRC_DT;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_POL_NBR = 9;
		public static final int I_POL_EFF_DT = 10;
		public static final int I_POL_EXP_DT = 10;
		public static final int I_USERID = 8;
		public static final int FLR1 = 100;
		public static final int O_POL_NBR = 9;
		public static final int O_POL_EFF_DT = 10;
		public static final int O_POL_EXP_DT = 10;
		public static final int O_PND_CNC_IND = 1;
		public static final int O_SCH_CNC_DT = 10;
		public static final int O_NOT_TYP_CD = 5;
		public static final int O_NOT_TYP_DES = 35;
		public static final int O_NOT_EMP_ID = 6;
		public static final int O_NOT_EMP_NM = 67;
		public static final int O_NOT_PRC_DT = 10;
		public static final int O_UW_TMN_FLG = 1;
		public static final int O_TMN_EMP_ID = 6;
		public static final int O_TMN_EMP_NM = 67;
		public static final int O_TMN_PRC_DT = 10;
		public static final int XZT9R0_SERVICE_INPUTS = I_POL_NBR + I_POL_EFF_DT + I_POL_EXP_DT + I_USERID + FLR1;
		public static final int O_POL_KEYS = O_POL_NBR + O_POL_EFF_DT + O_POL_EXP_DT;
		public static final int O_CNC_DATA = O_PND_CNC_IND + O_SCH_CNC_DT + O_NOT_TYP_CD + O_NOT_TYP_DES + O_NOT_EMP_ID + O_NOT_EMP_NM + O_NOT_PRC_DT;
		public static final int O_TMN_DATA = O_UW_TMN_FLG + O_TMN_EMP_ID + O_TMN_EMP_NM + O_TMN_PRC_DT;
		public static final int FLR2 = 800;
		public static final int XZT9R0_SERVICE_OUTPUTS = O_POL_KEYS + O_CNC_DATA + O_TMN_DATA + FLR2;
		public static final int L_SERVICE_CONTRACT_AREA = XZT9R0_SERVICE_INPUTS + XZT9R0_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
