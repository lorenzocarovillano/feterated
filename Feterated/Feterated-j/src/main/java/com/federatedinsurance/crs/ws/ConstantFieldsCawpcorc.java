/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program CAWPCORC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsCawpcorc {

	//==== PROPERTIES ====
	//Original name: CF-OWNER
	private String owner = "OWN";
	//Original name: CF-COOWNER
	private String coowner = "COWN";
	//Original name: CF-FIRST-NAMED-INSURED
	private String firstNamedInsured = "FNI";

	//==== METHODS ====
	public String getOwner() {
		return this.owner;
	}

	public String getCoowner() {
		return this.coowner;
	}

	public String getFirstNamedInsured() {
		return this.firstNamedInsured;
	}
}
