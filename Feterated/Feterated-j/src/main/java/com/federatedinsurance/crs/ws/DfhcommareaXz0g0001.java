/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program XZ0G0001<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaXz0g0001 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZT01I-SI-ACT-NBR
	private String xzt01iSiActNbr = DefaultValues.stringVal(Len.XZT01I_SI_ACT_NBR);
	//Original name: XZT01I-SI-USERID
	private String xzt01iSiUserid = DefaultValues.stringVal(Len.XZT01I_SI_USERID);
	//Original name: XZT01O-PC-ACT-NBR
	private String xzt01oPcActNbr = DefaultValues.stringVal(Len.XZT01O_PC_ACT_NBR);
	//Original name: XZT01O-PC-PCN-CNT
	private String xzt01oPcPcnCnt = DefaultValues.stringVal(Len.XZT01O_PC_PCN_CNT);
	//Original name: XZT01O-EO-ERROR-CD
	private String xzt01oEoErrorCd = DefaultValues.stringVal(Len.XZT01O_EO_ERROR_CD);
	//Original name: XZT01O-EO-ERROR-MSG
	private String xzt01oEoErrorMsg = DefaultValues.stringVal(Len.XZT01O_EO_ERROR_MSG);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		setXzt001ServiceInputsBytes(buffer, position);
		position += Len.XZT001_SERVICE_INPUTS;
		setXzt001ServiceOutputsBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		getXzt001ServiceInputsBytes(buffer, position);
		position += Len.XZT001_SERVICE_INPUTS;
		getXzt001ServiceOutputsBytes(buffer, position);
		return buffer;
	}

	public void setXzt001ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt01iSiActNbr = MarshalByte.readString(buffer, position, Len.XZT01I_SI_ACT_NBR);
		position += Len.XZT01I_SI_ACT_NBR;
		xzt01iSiUserid = MarshalByte.readString(buffer, position, Len.XZT01I_SI_USERID);
	}

	public byte[] getXzt001ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt01iSiActNbr, Len.XZT01I_SI_ACT_NBR);
		position += Len.XZT01I_SI_ACT_NBR;
		MarshalByte.writeString(buffer, position, xzt01iSiUserid, Len.XZT01I_SI_USERID);
		return buffer;
	}

	public void setXzt01iSiActNbr(String xzt01iSiActNbr) {
		this.xzt01iSiActNbr = Functions.subString(xzt01iSiActNbr, Len.XZT01I_SI_ACT_NBR);
	}

	public String getXzt01iSiActNbr() {
		return this.xzt01iSiActNbr;
	}

	public void setXzt01iSiUserid(String xzt01iSiUserid) {
		this.xzt01iSiUserid = Functions.subString(xzt01iSiUserid, Len.XZT01I_SI_USERID);
	}

	public String getXzt01iSiUserid() {
		return this.xzt01iSiUserid;
	}

	public void setXzt001ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		setXzt01oPcnCountOutputBytes(buffer, position);
		position += Len.XZT01O_PCN_COUNT_OUTPUT;
		setXzt01oErrorOuputBytes(buffer, position);
	}

	public byte[] getXzt001ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		getXzt01oPcnCountOutputBytes(buffer, position);
		position += Len.XZT01O_PCN_COUNT_OUTPUT;
		getXzt01oErrorOuputBytes(buffer, position);
		return buffer;
	}

	public void setXzt01oPcnCountOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt01oPcActNbr = MarshalByte.readString(buffer, position, Len.XZT01O_PC_ACT_NBR);
		position += Len.XZT01O_PC_ACT_NBR;
		xzt01oPcPcnCnt = MarshalByte.readFixedString(buffer, position, Len.XZT01O_PC_PCN_CNT);
	}

	public byte[] getXzt01oPcnCountOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt01oPcActNbr, Len.XZT01O_PC_ACT_NBR);
		position += Len.XZT01O_PC_ACT_NBR;
		MarshalByte.writeString(buffer, position, xzt01oPcPcnCnt, Len.XZT01O_PC_PCN_CNT);
		return buffer;
	}

	public void setXzt01oPcActNbr(String xzt01oPcActNbr) {
		this.xzt01oPcActNbr = Functions.subString(xzt01oPcActNbr, Len.XZT01O_PC_ACT_NBR);
	}

	public String getXzt01oPcActNbr() {
		return this.xzt01oPcActNbr;
	}

	public void setXzt01oPcPcnCnt(short xzt01oPcPcnCnt) {
		this.xzt01oPcPcnCnt = NumericDisplay.asString(xzt01oPcPcnCnt, Len.XZT01O_PC_PCN_CNT);
	}

	public void setXzt01oPcPcnCntFormatted(String xzt01oPcPcnCnt) {
		this.xzt01oPcPcnCnt = Trunc.toUnsignedNumeric(xzt01oPcPcnCnt, Len.XZT01O_PC_PCN_CNT);
	}

	public short getXzt01oPcPcnCnt() {
		return NumericDisplay.asShort(this.xzt01oPcPcnCnt);
	}

	public String getXzt01oPcPcnCntFormatted() {
		return this.xzt01oPcPcnCnt;
	}

	public void setXzt01oErrorOuputBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt01oEoErrorCd = MarshalByte.readFixedString(buffer, position, Len.XZT01O_EO_ERROR_CD);
		position += Len.XZT01O_EO_ERROR_CD;
		xzt01oEoErrorMsg = MarshalByte.readString(buffer, position, Len.XZT01O_EO_ERROR_MSG);
	}

	public byte[] getXzt01oErrorOuputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzt01oEoErrorCd, Len.XZT01O_EO_ERROR_CD);
		position += Len.XZT01O_EO_ERROR_CD;
		MarshalByte.writeString(buffer, position, xzt01oEoErrorMsg, Len.XZT01O_EO_ERROR_MSG);
		return buffer;
	}

	public void setXzt01oEoErrorCdFormatted(String xzt01oEoErrorCd) {
		this.xzt01oEoErrorCd = Trunc.toUnsignedNumeric(xzt01oEoErrorCd, Len.XZT01O_EO_ERROR_CD);
	}

	public short getXzt01oEoErrorCd() {
		return NumericDisplay.asShort(this.xzt01oEoErrorCd);
	}

	public String getXzt01oEoErrorCdFormatted() {
		return this.xzt01oEoErrorCd;
	}

	public void setXzt01oEoErrorMsg(String xzt01oEoErrorMsg) {
		this.xzt01oEoErrorMsg = Functions.subString(xzt01oEoErrorMsg, Len.XZT01O_EO_ERROR_MSG);
	}

	public String getXzt01oEoErrorMsg() {
		return this.xzt01oEoErrorMsg;
	}

	public String getXzt01oEoErrorMsgFormatted() {
		return Functions.padBlanks(getXzt01oEoErrorMsg(), Len.XZT01O_EO_ERROR_MSG);
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT01I_SI_ACT_NBR = 9;
		public static final int XZT01I_SI_USERID = 8;
		public static final int XZT001_SERVICE_INPUTS = XZT01I_SI_ACT_NBR + XZT01I_SI_USERID;
		public static final int XZT01O_PC_ACT_NBR = 9;
		public static final int XZT01O_PC_PCN_CNT = 3;
		public static final int XZT01O_PCN_COUNT_OUTPUT = XZT01O_PC_ACT_NBR + XZT01O_PC_PCN_CNT;
		public static final int XZT01O_EO_ERROR_CD = 4;
		public static final int XZT01O_EO_ERROR_MSG = 250;
		public static final int XZT01O_ERROR_OUPUT = XZT01O_EO_ERROR_CD + XZT01O_EO_ERROR_MSG;
		public static final int XZT001_SERVICE_OUTPUTS = XZT01O_PCN_COUNT_OUTPUT + XZT01O_ERROR_OUPUT;
		public static final int DFHCOMMAREA = XZT001_SERVICE_INPUTS + XZT001_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
