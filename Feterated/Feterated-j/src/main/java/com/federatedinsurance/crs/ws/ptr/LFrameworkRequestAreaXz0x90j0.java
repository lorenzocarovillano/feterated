/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X90J0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x90j0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x90j0() {
	}

	public LFrameworkRequestAreaXz0x90j0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzy9j0qCsrActNbr(String xzy9j0qCsrActNbr) {
		writeString(Pos.XZY9J0Q_CSR_ACT_NBR, xzy9j0qCsrActNbr, Len.XZY9J0Q_CSR_ACT_NBR);
	}

	/**Original name: XZY9J0Q-CSR-ACT-NBR<br>*/
	public String getXzy9j0qCsrActNbr() {
		return readString(Pos.XZY9J0Q_CSR_ACT_NBR, Len.XZY9J0Q_CSR_ACT_NBR);
	}

	public void setXzy9j0qNotPrcTs(String xzy9j0qNotPrcTs) {
		writeString(Pos.XZY9J0Q_NOT_PRC_TS, xzy9j0qNotPrcTs, Len.XZY9J0Q_NOT_PRC_TS);
	}

	/**Original name: XZY9J0Q-NOT-PRC-TS<br>*/
	public String getXzy9j0qNotPrcTs() {
		return readString(Pos.XZY9J0Q_NOT_PRC_TS, Len.XZY9J0Q_NOT_PRC_TS);
	}

	public void setXzy9j0qUserid(String xzy9j0qUserid) {
		writeString(Pos.XZY9J0Q_USERID, xzy9j0qUserid, Len.XZY9J0Q_USERID);
	}

	/**Original name: XZY9J0Q-USERID<br>*/
	public String getXzy9j0qUserid() {
		return readString(Pos.XZY9J0Q_USERID, Len.XZY9J0Q_USERID);
	}

	public void setXzy9j0qFrmAtcInd(char xzy9j0qFrmAtcInd) {
		writeChar(Pos.XZY9J0Q_FRM_ATC_IND, xzy9j0qFrmAtcInd);
	}

	/**Original name: XZY9J0Q-FRM-ATC-IND<br>*/
	public char getXzy9j0qFrmAtcInd() {
		return readChar(Pos.XZY9J0Q_FRM_ATC_IND);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0Y90J0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZY9J0Q_PREPARE_NOT_ROW = L_FW_REQ_XZ0Y90J0;
		public static final int XZY9J0Q_CSR_ACT_NBR = XZY9J0Q_PREPARE_NOT_ROW;
		public static final int XZY9J0Q_NOT_PRC_TS = XZY9J0Q_CSR_ACT_NBR + Len.XZY9J0Q_CSR_ACT_NBR;
		public static final int XZY9J0Q_USERID = XZY9J0Q_NOT_PRC_TS + Len.XZY9J0Q_NOT_PRC_TS;
		public static final int XZY9J0Q_FRM_ATC_IND = XZY9J0Q_USERID + Len.XZY9J0Q_USERID;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY9J0Q_CSR_ACT_NBR = 9;
		public static final int XZY9J0Q_NOT_PRC_TS = 26;
		public static final int XZY9J0Q_USERID = 8;
		public static final int XZY9J0Q_FRM_ATC_IND = 1;
		public static final int XZY9J0Q_PREPARE_NOT_ROW = XZY9J0Q_CSR_ACT_NBR + XZY9J0Q_NOT_PRC_TS + XZY9J0Q_USERID + XZY9J0Q_FRM_ATC_IND;
		public static final int L_FW_REQ_XZ0Y90J0 = XZY9J0Q_PREPARE_NOT_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0Y90J0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
