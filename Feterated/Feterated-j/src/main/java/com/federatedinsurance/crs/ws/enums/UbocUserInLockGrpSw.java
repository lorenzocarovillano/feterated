/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;

/**Original name: UBOC-USER-IN-LOCK-GRP-SW<br>
 * Variable: UBOC-USER-IN-LOCK-GRP-SW from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocUserInLockGrpSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char LOCK_GRP_NOT_CHKD = Types.SPACE_CHAR;
	public static final char SINGLE_LOCK_USER = 'S';
	public static final char GROUP_LOCK_USER = 'G';

	//==== METHODS ====
	public void setUbocUserInLockGrpSw(char ubocUserInLockGrpSw) {
		this.value = ubocUserInLockGrpSw;
	}

	public char getUbocUserInLockGrpSw() {
		return this.value;
	}

	public boolean isUbocSingleLockUser() {
		return value == SINGLE_LOCK_USER;
	}

	public void setUbocSingleLockUser() {
		value = SINGLE_LOCK_USER;
	}

	public boolean isUbocGroupLockUser() {
		return value == GROUP_LOCK_USER;
	}

	public void setUbocGroupLockUser() {
		value = GROUP_LOCK_USER;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_USER_IN_LOCK_GRP_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
