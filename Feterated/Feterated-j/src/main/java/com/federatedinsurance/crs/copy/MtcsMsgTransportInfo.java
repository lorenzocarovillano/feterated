/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.MtcsPassInLinkageInd;

/**Original name: MTCS-MSG-TRANSPORT-INFO<br>
 * Variable: MTCS-MSG-TRANSPORT-INFO from copybook HALLMTCS<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class MtcsMsgTransportInfo {

	//==== PROPERTIES ====
	//Original name: MTCS-UOW-NAME
	private String uowName = DefaultValues.stringVal(Len.UOW_NAME);
	//Original name: MTCS-MSGS-DEL-TIME
	private int msgsDelTime = DefaultValues.BIN_INT_VAL;
	//Original name: MTCS-WARNINGS-DEL-TIME
	private int warningsDelTime = DefaultValues.BIN_INT_VAL;
	//Original name: MTCS-PASS-IN-LINKAGE-IND
	private MtcsPassInLinkageInd passInLinkageInd = new MtcsPassInLinkageInd();
	//Original name: MTCS-STORE-TYPE-CD
	private char storeTypeCd = DefaultValues.CHAR_VAL;
	public static final char STORE_TYPE_UMT = 'U';
	//Original name: MTCS-MDR-REQ-STORE
	private String mdrReqStore = DefaultValues.stringVal(Len.MDR_REQ_STORE);
	//Original name: MTCS-MDR-RSP-STORE
	private String mdrRspStore = DefaultValues.stringVal(Len.MDR_RSP_STORE);
	//Original name: MTCS-UOW-REQ-MSG-STORE
	private String uowReqMsgStore = DefaultValues.stringVal(Len.UOW_REQ_MSG_STORE);
	//Original name: MTCS-UOW-REQ-SWITCHES-STORE
	private String uowReqSwitchesStore = DefaultValues.stringVal(Len.UOW_REQ_SWITCHES_STORE);
	//Original name: MTCS-UOW-RESP-HEADER-STORE
	private String uowRespHeaderStore = DefaultValues.stringVal(Len.UOW_RESP_HEADER_STORE);
	//Original name: MTCS-UOW-RESP-DATA-STORE
	private String uowRespDataStore = DefaultValues.stringVal(Len.UOW_RESP_DATA_STORE);
	//Original name: MTCS-UOW-RESP-WARNINGS-STORE
	private String uowRespWarningsStore = DefaultValues.stringVal(Len.UOW_RESP_WARNINGS_STORE);
	//Original name: MTCS-UOW-KEY-REPLACE-STORE
	private String uowKeyReplaceStore = DefaultValues.stringVal(Len.UOW_KEY_REPLACE_STORE);
	//Original name: MTCS-UOW-RESP-NL-BL-ERRS-STORE
	private String uowRespNlBlErrsStore = DefaultValues.stringVal(Len.UOW_RESP_NL_BL_ERRS_STORE);

	//==== METHODS ====
	public void setMtcsMsgTransportInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		uowName = MarshalByte.readString(buffer, position, Len.UOW_NAME);
		position += Len.UOW_NAME;
		msgsDelTime = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		warningsDelTime = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		passInLinkageInd.setPassInLinkageInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		storeTypeCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mdrReqStore = MarshalByte.readString(buffer, position, Len.MDR_REQ_STORE);
		position += Len.MDR_REQ_STORE;
		mdrRspStore = MarshalByte.readString(buffer, position, Len.MDR_RSP_STORE);
		position += Len.MDR_RSP_STORE;
		uowReqMsgStore = MarshalByte.readString(buffer, position, Len.UOW_REQ_MSG_STORE);
		position += Len.UOW_REQ_MSG_STORE;
		uowReqSwitchesStore = MarshalByte.readString(buffer, position, Len.UOW_REQ_SWITCHES_STORE);
		position += Len.UOW_REQ_SWITCHES_STORE;
		uowRespHeaderStore = MarshalByte.readString(buffer, position, Len.UOW_RESP_HEADER_STORE);
		position += Len.UOW_RESP_HEADER_STORE;
		uowRespDataStore = MarshalByte.readString(buffer, position, Len.UOW_RESP_DATA_STORE);
		position += Len.UOW_RESP_DATA_STORE;
		uowRespWarningsStore = MarshalByte.readString(buffer, position, Len.UOW_RESP_WARNINGS_STORE);
		position += Len.UOW_RESP_WARNINGS_STORE;
		uowKeyReplaceStore = MarshalByte.readString(buffer, position, Len.UOW_KEY_REPLACE_STORE);
		position += Len.UOW_KEY_REPLACE_STORE;
		uowRespNlBlErrsStore = MarshalByte.readString(buffer, position, Len.UOW_RESP_NL_BL_ERRS_STORE);
	}

	public void initMtcsMsgTransportInfoSpaces() {
		uowName = "";
		msgsDelTime = Types.INVALID_BINARY_INT_VAL;
		warningsDelTime = Types.INVALID_BINARY_INT_VAL;
		passInLinkageInd.setPassInLinkageInd(Types.SPACE_CHAR);
		storeTypeCd = Types.SPACE_CHAR;
		mdrReqStore = "";
		mdrRspStore = "";
		uowReqMsgStore = "";
		uowReqSwitchesStore = "";
		uowRespHeaderStore = "";
		uowRespDataStore = "";
		uowRespWarningsStore = "";
		uowKeyReplaceStore = "";
		uowRespNlBlErrsStore = "";
	}

	public void setUowName(String uowName) {
		this.uowName = Functions.subString(uowName, Len.UOW_NAME);
	}

	public String getUowName() {
		return this.uowName;
	}

	public void setMsgsDelTime(int msgsDelTime) {
		this.msgsDelTime = msgsDelTime;
	}

	public int getMsgsDelTime() {
		return this.msgsDelTime;
	}

	public void setWarningsDelTime(int warningsDelTime) {
		this.warningsDelTime = warningsDelTime;
	}

	public int getWarningsDelTime() {
		return this.warningsDelTime;
	}

	public void setStoreTypeCd(char storeTypeCd) {
		this.storeTypeCd = storeTypeCd;
	}

	public char getStoreTypeCd() {
		return this.storeTypeCd;
	}

	public void setMdrReqStore(String mdrReqStore) {
		this.mdrReqStore = Functions.subString(mdrReqStore, Len.MDR_REQ_STORE);
	}

	public String getMdrReqStore() {
		return this.mdrReqStore;
	}

	public void setMdrRspStore(String mdrRspStore) {
		this.mdrRspStore = Functions.subString(mdrRspStore, Len.MDR_RSP_STORE);
	}

	public String getMdrRspStore() {
		return this.mdrRspStore;
	}

	public void setUowReqMsgStore(String uowReqMsgStore) {
		this.uowReqMsgStore = Functions.subString(uowReqMsgStore, Len.UOW_REQ_MSG_STORE);
	}

	public String getUowReqMsgStore() {
		return this.uowReqMsgStore;
	}

	public String getUowReqMsgStoreFormatted() {
		return Functions.padBlanks(getUowReqMsgStore(), Len.UOW_REQ_MSG_STORE);
	}

	public void setUowReqSwitchesStore(String uowReqSwitchesStore) {
		this.uowReqSwitchesStore = Functions.subString(uowReqSwitchesStore, Len.UOW_REQ_SWITCHES_STORE);
	}

	public String getUowReqSwitchesStore() {
		return this.uowReqSwitchesStore;
	}

	public String getUowReqSwitchesStoreFormatted() {
		return Functions.padBlanks(getUowReqSwitchesStore(), Len.UOW_REQ_SWITCHES_STORE);
	}

	public void setUowRespHeaderStore(String uowRespHeaderStore) {
		this.uowRespHeaderStore = Functions.subString(uowRespHeaderStore, Len.UOW_RESP_HEADER_STORE);
	}

	public String getUowRespHeaderStore() {
		return this.uowRespHeaderStore;
	}

	public String getUowRespHeaderStoreFormatted() {
		return Functions.padBlanks(getUowRespHeaderStore(), Len.UOW_RESP_HEADER_STORE);
	}

	public void setUowRespDataStore(String uowRespDataStore) {
		this.uowRespDataStore = Functions.subString(uowRespDataStore, Len.UOW_RESP_DATA_STORE);
	}

	public String getUowRespDataStore() {
		return this.uowRespDataStore;
	}

	public String getUowRespDataStoreFormatted() {
		return Functions.padBlanks(getUowRespDataStore(), Len.UOW_RESP_DATA_STORE);
	}

	public void setUowRespWarningsStore(String uowRespWarningsStore) {
		this.uowRespWarningsStore = Functions.subString(uowRespWarningsStore, Len.UOW_RESP_WARNINGS_STORE);
	}

	public String getUowRespWarningsStore() {
		return this.uowRespWarningsStore;
	}

	public String getUowRespWarningsStoreFormatted() {
		return Functions.padBlanks(getUowRespWarningsStore(), Len.UOW_RESP_WARNINGS_STORE);
	}

	public void setUowKeyReplaceStore(String uowKeyReplaceStore) {
		this.uowKeyReplaceStore = Functions.subString(uowKeyReplaceStore, Len.UOW_KEY_REPLACE_STORE);
	}

	public String getUowKeyReplaceStore() {
		return this.uowKeyReplaceStore;
	}

	public void setUowRespNlBlErrsStore(String uowRespNlBlErrsStore) {
		this.uowRespNlBlErrsStore = Functions.subString(uowRespNlBlErrsStore, Len.UOW_RESP_NL_BL_ERRS_STORE);
	}

	public String getUowRespNlBlErrsStore() {
		return this.uowRespNlBlErrsStore;
	}

	public String getUowRespNlBlErrsStoreFormatted() {
		return Functions.padBlanks(getUowRespNlBlErrsStore(), Len.UOW_RESP_NL_BL_ERRS_STORE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NAME = 32;
		public static final int MDR_REQ_STORE = 32;
		public static final int MDR_RSP_STORE = 32;
		public static final int UOW_REQ_MSG_STORE = 32;
		public static final int UOW_REQ_SWITCHES_STORE = 32;
		public static final int UOW_RESP_HEADER_STORE = 32;
		public static final int UOW_RESP_DATA_STORE = 32;
		public static final int UOW_RESP_WARNINGS_STORE = 32;
		public static final int UOW_KEY_REPLACE_STORE = 32;
		public static final int UOW_RESP_NL_BL_ERRS_STORE = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
