/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0023<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0023 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0023() {
	}

	public LServiceContractAreaXz0x0023(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiPolNbr(String iPolNbr) {
		writeString(Pos.I_POL_NBR, iPolNbr, Len.I_POL_NBR);
	}

	/**Original name: XZ0T23I-POL-NBR<br>*/
	public String getiPolNbr() {
		return readString(Pos.I_POL_NBR, Len.I_POL_NBR);
	}

	public void setiPolEffDt(String iPolEffDt) {
		writeString(Pos.I_POL_EFF_DT, iPolEffDt, Len.I_POL_EFF_DT);
	}

	/**Original name: XZ0T23I-POL-EFF-DT<br>*/
	public String getiPolEffDt() {
		return readString(Pos.I_POL_EFF_DT, Len.I_POL_EFF_DT);
	}

	public void setiPolExpDt(String iPolExpDt) {
		writeString(Pos.I_POL_EXP_DT, iPolExpDt, Len.I_POL_EXP_DT);
	}

	/**Original name: XZ0T23I-POL-EXP-DT<br>*/
	public String getiPolExpDt() {
		return readString(Pos.I_POL_EXP_DT, Len.I_POL_EXP_DT);
	}

	public void setoPolNbr(String oPolNbr) {
		writeString(Pos.O_POL_NBR, oPolNbr, Len.O_POL_NBR);
	}

	/**Original name: XZ0T23O-POL-NBR<br>*/
	public String getoPolNbr() {
		return readString(Pos.O_POL_NBR, Len.O_POL_NBR);
	}

	public void setoPolEffDt(String oPolEffDt) {
		writeString(Pos.O_POL_EFF_DT, oPolEffDt, Len.O_POL_EFF_DT);
	}

	/**Original name: XZ0T23O-POL-EFF-DT<br>*/
	public String getoPolEffDt() {
		return readString(Pos.O_POL_EFF_DT, Len.O_POL_EFF_DT);
	}

	public void setoPolExpDt(String oPolExpDt) {
		writeString(Pos.O_POL_EXP_DT, oPolExpDt, Len.O_POL_EXP_DT);
	}

	/**Original name: XZ0T23O-POL-EXP-DT<br>*/
	public String getoPolExpDt() {
		return readString(Pos.O_POL_EXP_DT, Len.O_POL_EXP_DT);
	}

	public void setoPndCncInd(char oPndCncInd) {
		writeChar(Pos.O_PND_CNC_IND, oPndCncInd);
	}

	/**Original name: XZ0T23O-PND-CNC-IND<br>*/
	public char getoPndCncInd() {
		return readChar(Pos.O_PND_CNC_IND);
	}

	public void setoSchCncDt(String oSchCncDt) {
		writeString(Pos.O_SCH_CNC_DT, oSchCncDt, Len.O_SCH_CNC_DT);
	}

	/**Original name: XZ0T23O-SCH-CNC-DT<br>*/
	public String getoSchCncDt() {
		return readString(Pos.O_SCH_CNC_DT, Len.O_SCH_CNC_DT);
	}

	public void setoNotTypCd(String oNotTypCd) {
		writeString(Pos.O_NOT_TYP_CD, oNotTypCd, Len.O_NOT_TYP_CD);
	}

	/**Original name: XZ0T23O-NOT-TYP-CD<br>*/
	public String getoNotTypCd() {
		return readString(Pos.O_NOT_TYP_CD, Len.O_NOT_TYP_CD);
	}

	public void setoNotTypDes(String oNotTypDes) {
		writeString(Pos.O_NOT_TYP_DES, oNotTypDes, Len.O_NOT_TYP_DES);
	}

	/**Original name: XZ0T23O-NOT-TYP-DES<br>*/
	public String getoNotTypDes() {
		return readString(Pos.O_NOT_TYP_DES, Len.O_NOT_TYP_DES);
	}

	public void setoNotEmpId(String oNotEmpId) {
		writeString(Pos.O_NOT_EMP_ID, oNotEmpId, Len.O_NOT_EMP_ID);
	}

	/**Original name: XZ0T23O-NOT-EMP-ID<br>*/
	public String getoNotEmpId() {
		return readString(Pos.O_NOT_EMP_ID, Len.O_NOT_EMP_ID);
	}

	public void setoNotEmpNm(String oNotEmpNm) {
		writeString(Pos.O_NOT_EMP_NM, oNotEmpNm, Len.O_NOT_EMP_NM);
	}

	/**Original name: XZ0T23O-NOT-EMP-NM<br>*/
	public String getoNotEmpNm() {
		return readString(Pos.O_NOT_EMP_NM, Len.O_NOT_EMP_NM);
	}

	public void setoNotPrcDt(String oNotPrcDt) {
		writeString(Pos.O_NOT_PRC_DT, oNotPrcDt, Len.O_NOT_PRC_DT);
	}

	/**Original name: XZ0T23O-NOT-PRC-DT<br>*/
	public String getoNotPrcDt() {
		return readString(Pos.O_NOT_PRC_DT, Len.O_NOT_PRC_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_POL_NBR = SERVICE_INPUTS;
		public static final int I_POL_EFF_DT = I_POL_NBR + Len.I_POL_NBR;
		public static final int I_POL_EXP_DT = I_POL_EFF_DT + Len.I_POL_EFF_DT;
		public static final int FLR1 = I_POL_EXP_DT + Len.I_POL_EXP_DT;
		public static final int SERVICE_OUTPUTS = FLR1 + Len.FLR1;
		public static final int O_POL_INF = SERVICE_OUTPUTS;
		public static final int O_POL_NBR = O_POL_INF;
		public static final int O_POL_EFF_DT = O_POL_NBR + Len.O_POL_NBR;
		public static final int O_POL_EXP_DT = O_POL_EFF_DT + Len.O_POL_EFF_DT;
		public static final int O_CNC_INF = O_POL_EXP_DT + Len.O_POL_EXP_DT;
		public static final int O_PND_CNC_IND = O_CNC_INF;
		public static final int O_SCH_CNC_DT = O_PND_CNC_IND + Len.O_PND_CNC_IND;
		public static final int O_NOT_TYP = O_SCH_CNC_DT + Len.O_SCH_CNC_DT;
		public static final int O_NOT_TYP_CD = O_NOT_TYP;
		public static final int O_NOT_TYP_DES = O_NOT_TYP_CD + Len.O_NOT_TYP_CD;
		public static final int O_NOT_EMP_ID = O_NOT_TYP_DES + Len.O_NOT_TYP_DES;
		public static final int O_NOT_EMP_NM = O_NOT_EMP_ID + Len.O_NOT_EMP_ID;
		public static final int O_NOT_PRC_DT = O_NOT_EMP_NM + Len.O_NOT_EMP_NM;
		public static final int FLR2 = O_NOT_PRC_DT + Len.O_NOT_PRC_DT;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_POL_NBR = 25;
		public static final int I_POL_EFF_DT = 10;
		public static final int I_POL_EXP_DT = 10;
		public static final int FLR1 = 100;
		public static final int O_POL_NBR = 25;
		public static final int O_POL_EFF_DT = 10;
		public static final int O_POL_EXP_DT = 10;
		public static final int O_PND_CNC_IND = 1;
		public static final int O_SCH_CNC_DT = 10;
		public static final int O_NOT_TYP_CD = 5;
		public static final int O_NOT_TYP_DES = 35;
		public static final int O_NOT_EMP_ID = 6;
		public static final int O_NOT_EMP_NM = 67;
		public static final int O_NOT_PRC_DT = 10;
		public static final int SERVICE_INPUTS = I_POL_NBR + I_POL_EFF_DT + I_POL_EXP_DT + FLR1;
		public static final int O_POL_INF = O_POL_NBR + O_POL_EFF_DT + O_POL_EXP_DT;
		public static final int O_NOT_TYP = O_NOT_TYP_CD + O_NOT_TYP_DES;
		public static final int O_CNC_INF = O_PND_CNC_IND + O_SCH_CNC_DT + O_NOT_TYP + O_NOT_EMP_ID + O_NOT_EMP_NM + O_NOT_PRC_DT;
		public static final int SERVICE_OUTPUTS = O_POL_INF + O_CNC_INF + FLR1;
		public static final int L_SERVICE_CONTRACT_AREA = SERVICE_INPUTS + SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
