/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-CONNECT-ERROR-MSG<br>
 * Variable: EA-01-CONNECT-ERROR-MSG from program TS030299<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01ConnectErrorMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-1
	private String flr2 = "TS030299 -";
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-2
	private String flr3 = "CONNECT ERROR;";
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-3
	private String flr4 = "FUNCTION =";
	//Original name: EA-01-FUNCTION
	private String function = DefaultValues.stringVal(Len.FUNCTION);
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-4
	private String flr5 = "  SSID =";
	//Original name: EA-01-SSID
	private String ssid = DefaultValues.stringVal(Len.SSID);
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-5
	private String flr6 = ", RETURN =";
	//Original name: EA-01-RETURN
	private String returnFld = DefaultValues.stringVal(Len.RETURN_FLD);
	//Original name: FILLER-EA-01-CONNECT-ERROR-MSG-6
	private String flr7 = ", REASON =";
	//Original name: EA-01-REASON
	private String reason = DefaultValues.stringVal(Len.REASON);

	//==== METHODS ====
	public String getEa01ConnectErrorMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01ConnectErrorMsgBytes());
	}

	public byte[] getEa01ConnectErrorMsgBytes() {
		byte[] buffer = new byte[Len.EA01_CONNECT_ERROR_MSG];
		return getEa01ConnectErrorMsgBytes(buffer, 1);
	}

	public byte[] getEa01ConnectErrorMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, function, Len.FUNCTION);
		position += Len.FUNCTION;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ssid, Len.SSID);
		position += Len.SSID;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, returnFld, Len.RETURN_FLD);
		position += Len.RETURN_FLD;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, reason, Len.REASON);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setFunction(String function) {
		this.function = Functions.subString(function, Len.FUNCTION);
	}

	public String getFunction() {
		return this.function;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setSsid(String ssid) {
		this.ssid = Functions.subString(ssid, Len.SSID);
	}

	public String getSsid() {
		return this.ssid;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setReturnFld(String returnFld) {
		this.returnFld = Functions.subString(returnFld, Len.RETURN_FLD);
	}

	public String getReturnFld() {
		return this.returnFld;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setReason(String reason) {
		this.reason = Functions.subString(reason, Len.REASON);
	}

	public String getReason() {
		return this.reason;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FUNCTION = 18;
		public static final int SSID = 4;
		public static final int RETURN_FLD = 4;
		public static final int REASON = 4;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 15;
		public static final int FLR5 = 9;
		public static final int EA01_CONNECT_ERROR_MSG = FUNCTION + SSID + RETURN_FLD + REASON + FLR1 + 4 * FLR2 + FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
