/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A0004-ROW<br>
 * Variable: WS-XZ0A0004-ROW from program XZ0B0004<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a0004Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA004-PRIMARY-FUNCTION
	private String primaryFunction = DefaultValues.stringVal(Len.PRIMARY_FUNCTION);
	public static final String GET_RESCIND_IND = "GetRescindInd";
	//Original name: XZA004-ACCOUNT-NUMBER
	private String accountNumber = DefaultValues.stringVal(Len.ACCOUNT_NUMBER);
	//Original name: XZA004-OWNER-STATE
	private String ownerState = DefaultValues.stringVal(Len.OWNER_STATE);
	//Original name: XZA004-POLICY-NUMBER
	private String policyNumber = DefaultValues.stringVal(Len.POLICY_NUMBER);
	//Original name: XZA004-POLICY-EFF-DT
	private String policyEffDt = DefaultValues.stringVal(Len.POLICY_EFF_DT);
	//Original name: XZA004-POLICY-EXP-DT
	private String policyExpDt = DefaultValues.stringVal(Len.POLICY_EXP_DT);
	//Original name: XZA004-POLICY-TYPE
	private String policyType = DefaultValues.stringVal(Len.POLICY_TYPE);
	//Original name: XZA004-POLICY-CANC-DT
	private String policyCancDt = DefaultValues.stringVal(Len.POLICY_CANC_DT);
	//Original name: XZA004-RESCIND-IND
	private char rescindInd = DefaultValues.CHAR_VAL;
	//Original name: XZA004-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A0004_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a0004RowBytes(buf);
	}

	public String getWsXz0a0004RowFormatted() {
		return getGetRescindIndRowFormatted();
	}

	public void setWsXz0a0004RowBytes(byte[] buffer) {
		setWsXz0a0004RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a0004RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A0004_ROW];
		return getWsXz0a0004RowBytes(buffer, 1);
	}

	public void setWsXz0a0004RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetRescindIndRowBytes(buffer, position);
	}

	public byte[] getWsXz0a0004RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetRescindIndRowBytes(buffer, position);
		return buffer;
	}

	public String getGetRescindIndRowFormatted() {
		return MarshalByteExt.bufferToStr(getGetRescindIndRowBytes());
	}

	/**Original name: XZA004-GET-RESCIND-IND-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0A0001 - GET RESCIND INDICATOR BPO                           *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 08/04/2008  E404JWL   NEW                              *
	 *  TO07614 03/09/2009  E404KXS   ADDED USERID FIELD               *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getGetRescindIndRowBytes() {
		byte[] buffer = new byte[Len.GET_RESCIND_IND_ROW];
		return getGetRescindIndRowBytes(buffer, 1);
	}

	public void setGetRescindIndRowBytes(byte[] buffer, int offset) {
		int position = offset;
		primaryFunction = MarshalByte.readString(buffer, position, Len.PRIMARY_FUNCTION);
		position += Len.PRIMARY_FUNCTION;
		accountNumber = MarshalByte.readString(buffer, position, Len.ACCOUNT_NUMBER);
		position += Len.ACCOUNT_NUMBER;
		ownerState = MarshalByte.readString(buffer, position, Len.OWNER_STATE);
		position += Len.OWNER_STATE;
		policyNumber = MarshalByte.readString(buffer, position, Len.POLICY_NUMBER);
		position += Len.POLICY_NUMBER;
		policyEffDt = MarshalByte.readString(buffer, position, Len.POLICY_EFF_DT);
		position += Len.POLICY_EFF_DT;
		policyExpDt = MarshalByte.readString(buffer, position, Len.POLICY_EXP_DT);
		position += Len.POLICY_EXP_DT;
		policyType = MarshalByte.readString(buffer, position, Len.POLICY_TYPE);
		position += Len.POLICY_TYPE;
		policyCancDt = MarshalByte.readString(buffer, position, Len.POLICY_CANC_DT);
		position += Len.POLICY_CANC_DT;
		rescindInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
	}

	public byte[] getGetRescindIndRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, primaryFunction, Len.PRIMARY_FUNCTION);
		position += Len.PRIMARY_FUNCTION;
		MarshalByte.writeString(buffer, position, accountNumber, Len.ACCOUNT_NUMBER);
		position += Len.ACCOUNT_NUMBER;
		MarshalByte.writeString(buffer, position, ownerState, Len.OWNER_STATE);
		position += Len.OWNER_STATE;
		MarshalByte.writeString(buffer, position, policyNumber, Len.POLICY_NUMBER);
		position += Len.POLICY_NUMBER;
		MarshalByte.writeString(buffer, position, policyEffDt, Len.POLICY_EFF_DT);
		position += Len.POLICY_EFF_DT;
		MarshalByte.writeString(buffer, position, policyExpDt, Len.POLICY_EXP_DT);
		position += Len.POLICY_EXP_DT;
		MarshalByte.writeString(buffer, position, policyType, Len.POLICY_TYPE);
		position += Len.POLICY_TYPE;
		MarshalByte.writeString(buffer, position, policyCancDt, Len.POLICY_CANC_DT);
		position += Len.POLICY_CANC_DT;
		MarshalByte.writeChar(buffer, position, rescindInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setPrimaryFunction(String primaryFunction) {
		this.primaryFunction = Functions.subString(primaryFunction, Len.PRIMARY_FUNCTION);
	}

	public String getPrimaryFunction() {
		return this.primaryFunction;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = Functions.subString(accountNumber, Len.ACCOUNT_NUMBER);
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public String getAccountNumberFormatted() {
		return Functions.padBlanks(getAccountNumber(), Len.ACCOUNT_NUMBER);
	}

	public void setOwnerState(String ownerState) {
		this.ownerState = Functions.subString(ownerState, Len.OWNER_STATE);
	}

	public String getOwnerState() {
		return this.ownerState;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = Functions.subString(policyNumber, Len.POLICY_NUMBER);
	}

	public String getPolicyNumber() {
		return this.policyNumber;
	}

	public String getPolicyNumberFormatted() {
		return Functions.padBlanks(getPolicyNumber(), Len.POLICY_NUMBER);
	}

	public void setPolicyEffDt(String policyEffDt) {
		this.policyEffDt = Functions.subString(policyEffDt, Len.POLICY_EFF_DT);
	}

	public String getPolicyEffDt() {
		return this.policyEffDt;
	}

	public String getPolicyEffDtFormatted() {
		return Functions.padBlanks(getPolicyEffDt(), Len.POLICY_EFF_DT);
	}

	public void setPolicyExpDt(String policyExpDt) {
		this.policyExpDt = Functions.subString(policyExpDt, Len.POLICY_EXP_DT);
	}

	public String getPolicyExpDt() {
		return this.policyExpDt;
	}

	public void setPolicyType(String policyType) {
		this.policyType = Functions.subString(policyType, Len.POLICY_TYPE);
	}

	public String getPolicyType() {
		return this.policyType;
	}

	public void setPolicyCancDt(String policyCancDt) {
		this.policyCancDt = Functions.subString(policyCancDt, Len.POLICY_CANC_DT);
	}

	public String getPolicyCancDt() {
		return this.policyCancDt;
	}

	public void setRescindInd(char rescindInd) {
		this.rescindInd = rescindInd;
	}

	public char getRescindInd() {
		return this.rescindInd;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a0004RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PRIMARY_FUNCTION = 32;
		public static final int ACCOUNT_NUMBER = 9;
		public static final int OWNER_STATE = 2;
		public static final int POLICY_NUMBER = 25;
		public static final int POLICY_EFF_DT = 10;
		public static final int POLICY_EXP_DT = 10;
		public static final int POLICY_TYPE = 3;
		public static final int POLICY_CANC_DT = 10;
		public static final int RESCIND_IND = 1;
		public static final int USERID = 8;
		public static final int GET_RESCIND_IND_ROW = PRIMARY_FUNCTION + ACCOUNT_NUMBER + OWNER_STATE + POLICY_NUMBER + POLICY_EFF_DT + POLICY_EXP_DT
				+ POLICY_TYPE + POLICY_CANC_DT + RESCIND_IND + USERID;
		public static final int WS_XZ0A0004_ROW = GET_RESCIND_IND_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
