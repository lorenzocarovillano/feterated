/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsCicsProgram;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program FNC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaFnc02090 {

	//==== PROPERTIES ====
	//Original name: WS-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-CICS-PROGRAM
	private WsCicsProgram cicsProgram = new WsCicsProgram();
	//Original name: WS-WEB-SVC-URI
	private String webSvcUri = "";
	//Original name: WS-MATCH-CD
	private String matchCd = DefaultValues.stringVal(Len.MATCH_CD);

	//==== METHODS ====
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public void setWebSvcUri(String webSvcUri) {
		this.webSvcUri = Functions.subString(webSvcUri, Len.WEB_SVC_URI);
	}

	public String getWebSvcUri() {
		return this.webSvcUri;
	}

	public void setMatchCd(String matchCd) {
		this.matchCd = Functions.subString(matchCd, Len.MATCH_CD);
	}

	public String getMatchCd() {
		return this.matchCd;
	}

	public WsCicsProgram getCicsProgram() {
		return cicsProgram;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MATCH_CD = 35;
		public static final int WEB_SVC_URI = 256;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
