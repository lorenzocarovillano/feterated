/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q9071<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q9071 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q9071() {
	}

	public LFrameworkRequestAreaXz0q9071(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza971qMaxOvlRows(short xza971qMaxOvlRows) {
		writeBinaryShort(Pos.XZA971_MAX_OVL_ROWS, xza971qMaxOvlRows);
	}

	/**Original name: XZA971Q-MAX-OVL-ROWS<br>*/
	public short getXza971qMaxOvlRows() {
		return readBinaryShort(Pos.XZA971_MAX_OVL_ROWS);
	}

	public void setXza971qFrmNbr(String xza971qFrmNbr) {
		writeString(Pos.XZA971_FRM_NBR, xza971qFrmNbr, Len.XZA971_FRM_NBR);
	}

	/**Original name: XZA971Q-FRM-NBR<br>*/
	public String getXza971qFrmNbr() {
		return readString(Pos.XZA971_FRM_NBR, Len.XZA971_FRM_NBR);
	}

	public void setXza971qFrmEdtDt(String xza971qFrmEdtDt) {
		writeString(Pos.XZA971_FRM_EDT_DT, xza971qFrmEdtDt, Len.XZA971_FRM_EDT_DT);
	}

	/**Original name: XZA971Q-FRM-EDT-DT<br>*/
	public String getXza971qFrmEdtDt() {
		return readString(Pos.XZA971_FRM_EDT_DT, Len.XZA971_FRM_EDT_DT);
	}

	public void setXza971qRecTypCd(String xza971qRecTypCd) {
		writeString(Pos.XZA971_REC_TYP_CD, xza971qRecTypCd, Len.XZA971_REC_TYP_CD);
	}

	/**Original name: XZA971Q-REC-TYP-CD<br>*/
	public String getXza971qRecTypCd() {
		return readString(Pos.XZA971_REC_TYP_CD, Len.XZA971_REC_TYP_CD);
	}

	public void setXza971qOvlEdlFrmNm(String xza971qOvlEdlFrmNm) {
		writeString(Pos.XZA971_OVL_EDL_FRM_NM, xza971qOvlEdlFrmNm, Len.XZA971_OVL_EDL_FRM_NM);
	}

	/**Original name: XZA971Q-OVL-EDL-FRM-NM<br>*/
	public String getXza971qOvlEdlFrmNm() {
		return readString(Pos.XZA971_OVL_EDL_FRM_NM, Len.XZA971_OVL_EDL_FRM_NM);
	}

	public void setXza971qStAbb(String xza971qStAbb) {
		writeString(Pos.XZA971_ST_ABB, xza971qStAbb, Len.XZA971_ST_ABB);
	}

	/**Original name: XZA971Q-ST-ABB<br>*/
	public String getXza971qStAbb() {
		return readString(Pos.XZA971_ST_ABB, Len.XZA971_ST_ABB);
	}

	public void setXza971qSpePrcCd(String xza971qSpePrcCd) {
		writeString(Pos.XZA971_SPE_PRC_CD, xza971qSpePrcCd, Len.XZA971_SPE_PRC_CD);
	}

	/**Original name: XZA971Q-SPE-PRC-CD<br>*/
	public String getXza971qSpePrcCd() {
		return readString(Pos.XZA971_SPE_PRC_CD, Len.XZA971_SPE_PRC_CD);
	}

	public void setXza971qOvlSrCd(String xza971qOvlSrCd) {
		writeString(Pos.XZA971_OVL_SR_CD, xza971qOvlSrCd, Len.XZA971_OVL_SR_CD);
	}

	/**Original name: XZA971Q-OVL-SR-CD<br>*/
	public String getXza971qOvlSrCd() {
		return readString(Pos.XZA971_OVL_SR_CD, Len.XZA971_OVL_SR_CD);
	}

	public void setXza971qOvlDes(String xza971qOvlDes) {
		writeString(Pos.XZA971_OVL_DES, xza971qOvlDes, Len.XZA971_OVL_DES);
	}

	/**Original name: XZA971Q-OVL-DES<br>*/
	public String getXza971qOvlDes() {
		return readString(Pos.XZA971_OVL_DES, Len.XZA971_OVL_DES);
	}

	public void setXza971qXclvInd(char xza971qXclvInd) {
		writeChar(Pos.XZA971_XCLV_IND, xza971qXclvInd);
	}

	/**Original name: XZA971Q-XCLV-IND<br>*/
	public char getXza971qXclvInd() {
		return readChar(Pos.XZA971_XCLV_IND);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A9071 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA971_ACY_OVL_ROW = L_FW_REQ_XZ0A9071;
		public static final int XZA971_MAX_OVL_ROWS = XZA971_ACY_OVL_ROW;
		public static final int XZA971_ACY_OVL_INFO = XZA971_MAX_OVL_ROWS + Len.XZA971_MAX_OVL_ROWS;
		public static final int XZA971_FRM_NBR = XZA971_ACY_OVL_INFO;
		public static final int XZA971_FRM_EDT_DT = XZA971_FRM_NBR + Len.XZA971_FRM_NBR;
		public static final int XZA971_REC_TYP_CD = XZA971_FRM_EDT_DT + Len.XZA971_FRM_EDT_DT;
		public static final int XZA971_OVL_EDL_FRM_NM = XZA971_REC_TYP_CD + Len.XZA971_REC_TYP_CD;
		public static final int XZA971_ST_ABB = XZA971_OVL_EDL_FRM_NM + Len.XZA971_OVL_EDL_FRM_NM;
		public static final int XZA971_SPE_PRC_CD = XZA971_ST_ABB + Len.XZA971_ST_ABB;
		public static final int XZA971_OVL_SR_CD = XZA971_SPE_PRC_CD + Len.XZA971_SPE_PRC_CD;
		public static final int XZA971_OVL_DES = XZA971_OVL_SR_CD + Len.XZA971_OVL_SR_CD;
		public static final int XZA971_XCLV_IND = XZA971_OVL_DES + Len.XZA971_OVL_DES;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA971_MAX_OVL_ROWS = 2;
		public static final int XZA971_FRM_NBR = 30;
		public static final int XZA971_FRM_EDT_DT = 10;
		public static final int XZA971_REC_TYP_CD = 5;
		public static final int XZA971_OVL_EDL_FRM_NM = 30;
		public static final int XZA971_ST_ABB = 2;
		public static final int XZA971_SPE_PRC_CD = 8;
		public static final int XZA971_OVL_SR_CD = 8;
		public static final int XZA971_OVL_DES = 30;
		public static final int XZA971_XCLV_IND = 1;
		public static final int XZA971_ACY_OVL_INFO = XZA971_FRM_NBR + XZA971_FRM_EDT_DT + XZA971_REC_TYP_CD + XZA971_OVL_EDL_FRM_NM + XZA971_ST_ABB
				+ XZA971_SPE_PRC_CD + XZA971_OVL_SR_CD + XZA971_OVL_DES + XZA971_XCLV_IND;
		public static final int XZA971_ACY_OVL_ROW = XZA971_MAX_OVL_ROWS + XZA971_ACY_OVL_INFO;
		public static final int L_FW_REQ_XZ0A9071 = XZA971_ACY_OVL_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A9071;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
