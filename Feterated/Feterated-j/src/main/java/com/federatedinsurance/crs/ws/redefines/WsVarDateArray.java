/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-VAR-DATE-ARRAY<br>
 * Variable: WS-VAR-DATE-ARRAY from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsVarDateArray extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int ARRAY_CHAR_MAXOCCURS = 8;

	//==== CONSTRUCTORS ====
	public WsVarDateArray() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_VAR_DATE_ARRAY;
	}

	public void initWsVarDateArraySpaces() {
		fill(Pos.WS_VAR_DATE_ARRAY, Len.WS_VAR_DATE_ARRAY, Types.SPACE_CHAR);
	}

	/**Original name: WS-VAR-DATE-CHAR<br>*/
	public char getCharFld(int charFldIdx) {
		int position = Pos.wsVarDateChar(charFldIdx - 1);
		return readChar(position);
	}

	public void setYearAd(String yearAd) {
		writeString(Pos.YEAR_AD, yearAd, Len.YEAR_AD);
	}

	/**Original name: WS-VAR-DATE-YEAR-AD<br>*/
	public String getYearAd() {
		return readString(Pos.YEAR_AD, Len.YEAR_AD);
	}

	public void setDayAm(String dayAm) {
		writeString(Pos.DAY_AM, dayAm, Len.DAY_AM);
	}

	/**Original name: WS-VAR-DATE-DAY-AM<br>*/
	public String getDayAm() {
		return readString(Pos.DAY_AM, Len.DAY_AM);
	}

	public void setDays(String days) {
		writeString(Pos.DAYS, days, Len.DAYS);
	}

	/**Original name: WS-VAR-DATE-DAYS<br>*/
	public String getDays() {
		return readString(Pos.DAYS, Len.DAYS);
	}

	public void setYearAm(String yearAm) {
		writeString(Pos.YEAR_AM, yearAm, Len.YEAR_AM);
	}

	/**Original name: WS-VAR-DATE-YEAR-AM<br>*/
	public String getYearAm() {
		return readString(Pos.YEAR_AM, Len.YEAR_AM);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_VAR_DATE_ARRAY = 1;
		public static final int ARRAY_IN = WS_VAR_DATE_ARRAY;
		public static final int WS_VAR_DATE_ARRAY_RED = 1;
		public static final int WS_VAR_DATE_ARRAY_AD = 1;
		public static final int YEAR_AD = WS_VAR_DATE_ARRAY_AD;
		public static final int YEARS_AD = YEAR_AD + Len.YEAR_AD;
		public static final int WS_VAR_DATE_ARRAY_AM = 1;
		public static final int DAY_AM = WS_VAR_DATE_ARRAY_AM;
		public static final int DAYS = DAY_AM + Len.DAY_AM;
		public static final int YEAR_AM = DAYS + Len.DAYS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int wsVarDateArrayChar(int idx) {
			return WS_VAR_DATE_ARRAY_RED + idx * Len.ARRAY_CHAR;
		}

		public static int wsVarDateChar(int idx) {
			return wsVarDateArrayChar(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CHAR_FLD = 1;
		public static final int ARRAY_CHAR = CHAR_FLD;
		public static final int YEAR_AD = 4;
		public static final int DAY_AM = 2;
		public static final int DAYS = 2;
		public static final int ARRAY_IN = 8;
		public static final int WS_VAR_DATE_ARRAY = ARRAY_IN;
		public static final int YEAR_AM = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
