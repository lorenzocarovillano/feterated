/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: SA-CHECK-STRING-GRP<br>
 * Variable: SA-CHECK-STRING-GRP from program CAWRVDCH<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SaCheckStringGrp extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int CHAR_FLD_MAXOCCURS = 60;

	//==== CONSTRUCTORS ====
	public SaCheckStringGrp() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.SA_CHECK_STRING_GRP;
	}

	public void setSaCheckStringGrp(String saCheckStringGrp) {
		writeString(Pos.SA_CHECK_STRING_GRP, saCheckStringGrp, Len.SA_CHECK_STRING_GRP);
	}

	/**Original name: SA-CHECK-STRING-GRP<br>*/
	public String getSaCheckStringGrp() {
		return readString(Pos.SA_CHECK_STRING_GRP, Len.SA_CHECK_STRING_GRP);
	}

	/**Original name: SA-CHECK-STRING-CHAR<br>*/
	public char getCharFld(int charFldIdx) {
		int position = Pos.saCheckStringChar(charFldIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int SA_CHECK_STRING_GRP = 1;
		public static final int FILLER_SAVE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int saCheckStringChar(int idx) {
			return FILLER_SAVE_AREA + idx * Len.CHAR_FLD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CHAR_FLD = 1;
		public static final int SA_CHECK_STRING_GRP = 60;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
