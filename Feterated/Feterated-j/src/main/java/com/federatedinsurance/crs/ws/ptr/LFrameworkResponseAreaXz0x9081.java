/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0X9081<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0x9081 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9081_MAXOCCURS = 2500;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0x9081() {
	}

	public LFrameworkResponseAreaXz0x9081(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza981rMaxTtyRows(int xza981rMaxTtyRowsIdx, short xza981rMaxTtyRows) {
		int position = Pos.xza981rMaxTtyRows(xza981rMaxTtyRowsIdx - 1);
		writeBinaryShort(position, xza981rMaxTtyRows);
	}

	/**Original name: XZA981R-MAX-TTY-ROWS<br>*/
	public short getXza981rMaxTtyRows(int xza981rMaxTtyRowsIdx) {
		int position = Pos.xza981rMaxTtyRows(xza981rMaxTtyRowsIdx - 1);
		return readBinaryShort(position);
	}

	public void setXza981rCsrActNbr(int xza981rCsrActNbrIdx, String xza981rCsrActNbr) {
		int position = Pos.xza981rCsrActNbr(xza981rCsrActNbrIdx - 1);
		writeString(position, xza981rCsrActNbr, Len.XZA981R_CSR_ACT_NBR);
	}

	/**Original name: XZA981R-CSR-ACT-NBR<br>*/
	public String getXza981rCsrActNbr(int xza981rCsrActNbrIdx) {
		int position = Pos.xza981rCsrActNbr(xza981rCsrActNbrIdx - 1);
		return readString(position, Len.XZA981R_CSR_ACT_NBR);
	}

	public void setXza981rNotPrcTs(int xza981rNotPrcTsIdx, String xza981rNotPrcTs) {
		int position = Pos.xza981rNotPrcTs(xza981rNotPrcTsIdx - 1);
		writeString(position, xza981rNotPrcTs, Len.XZA981R_NOT_PRC_TS);
	}

	/**Original name: XZA981R-NOT-PRC-TS<br>*/
	public String getXza981rNotPrcTs(int xza981rNotPrcTsIdx) {
		int position = Pos.xza981rNotPrcTs(xza981rNotPrcTsIdx - 1);
		return readString(position, Len.XZA981R_NOT_PRC_TS);
	}

	public void setXza981rUserid(int xza981rUseridIdx, String xza981rUserid) {
		int position = Pos.xza981rUserid(xza981rUseridIdx - 1);
		writeString(position, xza981rUserid, Len.XZA981R_USERID);
	}

	/**Original name: XZA981R-USERID<br>*/
	public String getXza981rUserid(int xza981rUseridIdx) {
		int position = Pos.xza981rUserid(xza981rUseridIdx - 1);
		return readString(position, Len.XZA981R_USERID);
	}

	public void setXza981rClientId(int xza981rClientIdIdx, String xza981rClientId) {
		int position = Pos.xza981rClientId(xza981rClientIdIdx - 1);
		writeString(position, xza981rClientId, Len.XZA981R_CLIENT_ID);
	}

	/**Original name: XZA981R-CLIENT-ID<br>*/
	public String getXza981rClientId(int xza981rClientIdIdx) {
		int position = Pos.xza981rClientId(xza981rClientIdIdx - 1);
		return readString(position, Len.XZA981R_CLIENT_ID);
	}

	public void setXza981rAdrId(int xza981rAdrIdIdx, String xza981rAdrId) {
		int position = Pos.xza981rAdrId(xza981rAdrIdIdx - 1);
		writeString(position, xza981rAdrId, Len.XZA981R_ADR_ID);
	}

	/**Original name: XZA981R-ADR-ID<br>*/
	public String getXza981rAdrId(int xza981rAdrIdIdx) {
		int position = Pos.xza981rAdrId(xza981rAdrIdIdx - 1);
		return readString(position, Len.XZA981R_ADR_ID);
	}

	public void setXza981rRecTypCd(int xza981rRecTypCdIdx, String xza981rRecTypCd) {
		int position = Pos.xza981rRecTypCd(xza981rRecTypCdIdx - 1);
		writeString(position, xza981rRecTypCd, Len.XZA981R_REC_TYP_CD);
	}

	/**Original name: XZA981R-REC-TYP-CD<br>*/
	public String getXza981rRecTypCd(int xza981rRecTypCdIdx) {
		int position = Pos.xza981rRecTypCd(xza981rRecTypCdIdx - 1);
		return readString(position, Len.XZA981R_REC_TYP_CD);
	}

	public void setXza981rRecTypDes(int xza981rRecTypDesIdx, String xza981rRecTypDes) {
		int position = Pos.xza981rRecTypDes(xza981rRecTypDesIdx - 1);
		writeString(position, xza981rRecTypDes, Len.XZA981R_REC_TYP_DES);
	}

	/**Original name: XZA981R-REC-TYP-DES<br>*/
	public String getXza981rRecTypDes(int xza981rRecTypDesIdx) {
		int position = Pos.xza981rRecTypDes(xza981rRecTypDesIdx - 1);
		return readString(position, Len.XZA981R_REC_TYP_DES);
	}

	public void setXza981rName(int xza981rNameIdx, String xza981rName) {
		int position = Pos.xza981rName(xza981rNameIdx - 1);
		writeString(position, xza981rName, Len.XZA981R_NAME);
	}

	/**Original name: XZA981R-NAME<br>*/
	public String getXza981rName(int xza981rNameIdx) {
		int position = Pos.xza981rName(xza981rNameIdx - 1);
		return readString(position, Len.XZA981R_NAME);
	}

	public void setXza981rAdrLin1(int xza981rAdrLin1Idx, String xza981rAdrLin1) {
		int position = Pos.xza981rAdrLin1(xza981rAdrLin1Idx - 1);
		writeString(position, xza981rAdrLin1, Len.XZA981R_ADR_LIN1);
	}

	/**Original name: XZA981R-ADR-LIN1<br>*/
	public String getXza981rAdrLin1(int xza981rAdrLin1Idx) {
		int position = Pos.xza981rAdrLin1(xza981rAdrLin1Idx - 1);
		return readString(position, Len.XZA981R_ADR_LIN1);
	}

	public void setXza981rAdrLin2(int xza981rAdrLin2Idx, String xza981rAdrLin2) {
		int position = Pos.xza981rAdrLin2(xza981rAdrLin2Idx - 1);
		writeString(position, xza981rAdrLin2, Len.XZA981R_ADR_LIN2);
	}

	/**Original name: XZA981R-ADR-LIN2<br>*/
	public String getXza981rAdrLin2(int xza981rAdrLin2Idx) {
		int position = Pos.xza981rAdrLin2(xza981rAdrLin2Idx - 1);
		return readString(position, Len.XZA981R_ADR_LIN2);
	}

	public void setXza981rCityNm(int xza981rCityNmIdx, String xza981rCityNm) {
		int position = Pos.xza981rCityNm(xza981rCityNmIdx - 1);
		writeString(position, xza981rCityNm, Len.XZA981R_CITY_NM);
	}

	/**Original name: XZA981R-CITY-NM<br>*/
	public String getXza981rCityNm(int xza981rCityNmIdx) {
		int position = Pos.xza981rCityNm(xza981rCityNmIdx - 1);
		return readString(position, Len.XZA981R_CITY_NM);
	}

	public void setXza981rStateAbb(int xza981rStateAbbIdx, String xza981rStateAbb) {
		int position = Pos.xza981rStateAbb(xza981rStateAbbIdx - 1);
		writeString(position, xza981rStateAbb, Len.XZA981R_STATE_ABB);
	}

	/**Original name: XZA981R-STATE-ABB<br>*/
	public String getXza981rStateAbb(int xza981rStateAbbIdx) {
		int position = Pos.xza981rStateAbb(xza981rStateAbbIdx - 1);
		return readString(position, Len.XZA981R_STATE_ABB);
	}

	public void setXza981rPstCd(int xza981rPstCdIdx, String xza981rPstCd) {
		int position = Pos.xza981rPstCd(xza981rPstCdIdx - 1);
		writeString(position, xza981rPstCd, Len.XZA981R_PST_CD);
	}

	/**Original name: XZA981R-PST-CD<br>*/
	public String getXza981rPstCd(int xza981rPstCdIdx) {
		int position = Pos.xza981rPstCd(xza981rPstCdIdx - 1);
		return readString(position, Len.XZA981R_PST_CD);
	}

	public void setXza981rCerNbr(int xza981rCerNbrIdx, String xza981rCerNbr) {
		int position = Pos.xza981rCerNbr(xza981rCerNbrIdx - 1);
		writeString(position, xza981rCerNbr, Len.XZA981R_CER_NBR);
	}

	/**Original name: XZA981R-CER-NBR<br>*/
	public String getXza981rCerNbr(int xza981rCerNbrIdx) {
		int position = Pos.xza981rCerNbr(xza981rCerNbrIdx - 1);
		return readString(position, Len.XZA981R_CER_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9081(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9081;
		}

		public static int xza981rTtyCertInfoRow(int idx) {
			return lFwRespXz0a9081(idx);
		}

		public static int xza981rMaxTtyRows(int idx) {
			return xza981rTtyCertInfoRow(idx);
		}

		public static int xza981rCsrActNbr(int idx) {
			return xza981rMaxTtyRows(idx) + Len.XZA981R_MAX_TTY_ROWS;
		}

		public static int xza981rNotPrcTs(int idx) {
			return xza981rCsrActNbr(idx) + Len.XZA981R_CSR_ACT_NBR;
		}

		public static int xza981rUserid(int idx) {
			return xza981rNotPrcTs(idx) + Len.XZA981R_NOT_PRC_TS;
		}

		public static int xza981rTtyCertList(int idx) {
			return xza981rUserid(idx) + Len.XZA981R_USERID;
		}

		public static int xza981rClientId(int idx) {
			return xza981rTtyCertList(idx);
		}

		public static int xza981rAdrId(int idx) {
			return xza981rClientId(idx) + Len.XZA981R_CLIENT_ID;
		}

		public static int xza981rRecTypCd(int idx) {
			return xza981rAdrId(idx) + Len.XZA981R_ADR_ID;
		}

		public static int xza981rRecTypDes(int idx) {
			return xza981rRecTypCd(idx) + Len.XZA981R_REC_TYP_CD;
		}

		public static int xza981rName(int idx) {
			return xza981rRecTypDes(idx) + Len.XZA981R_REC_TYP_DES;
		}

		public static int xza981rAdrLin1(int idx) {
			return xza981rName(idx) + Len.XZA981R_NAME;
		}

		public static int xza981rAdrLin2(int idx) {
			return xza981rAdrLin1(idx) + Len.XZA981R_ADR_LIN1;
		}

		public static int xza981rCityNm(int idx) {
			return xza981rAdrLin2(idx) + Len.XZA981R_ADR_LIN2;
		}

		public static int xza981rStateAbb(int idx) {
			return xza981rCityNm(idx) + Len.XZA981R_CITY_NM;
		}

		public static int xza981rPstCd(int idx) {
			return xza981rStateAbb(idx) + Len.XZA981R_STATE_ABB;
		}

		public static int xza981rCerNbr(int idx) {
			return xza981rPstCd(idx) + Len.XZA981R_PST_CD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA981R_MAX_TTY_ROWS = 2;
		public static final int XZA981R_CSR_ACT_NBR = 9;
		public static final int XZA981R_NOT_PRC_TS = 26;
		public static final int XZA981R_USERID = 8;
		public static final int XZA981R_CLIENT_ID = 64;
		public static final int XZA981R_ADR_ID = 64;
		public static final int XZA981R_REC_TYP_CD = 5;
		public static final int XZA981R_REC_TYP_DES = 13;
		public static final int XZA981R_NAME = 120;
		public static final int XZA981R_ADR_LIN1 = 45;
		public static final int XZA981R_ADR_LIN2 = 45;
		public static final int XZA981R_CITY_NM = 30;
		public static final int XZA981R_STATE_ABB = 2;
		public static final int XZA981R_PST_CD = 13;
		public static final int XZA981R_CER_NBR = 25;
		public static final int XZA981R_TTY_CERT_LIST = XZA981R_CLIENT_ID + XZA981R_ADR_ID + XZA981R_REC_TYP_CD + XZA981R_REC_TYP_DES + XZA981R_NAME
				+ XZA981R_ADR_LIN1 + XZA981R_ADR_LIN2 + XZA981R_CITY_NM + XZA981R_STATE_ABB + XZA981R_PST_CD + XZA981R_CER_NBR;
		public static final int XZA981R_TTY_CERT_INFO_ROW = XZA981R_MAX_TTY_ROWS + XZA981R_CSR_ACT_NBR + XZA981R_NOT_PRC_TS + XZA981R_USERID
				+ XZA981R_TTY_CERT_LIST;
		public static final int L_FW_RESP_XZ0A9081 = XZA981R_TTY_CERT_INFO_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0x9081.L_FW_RESP_XZ0A9081_MAXOCCURS * L_FW_RESP_XZ0A9081;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
