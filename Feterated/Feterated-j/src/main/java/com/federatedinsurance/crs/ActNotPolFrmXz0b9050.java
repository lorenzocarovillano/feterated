/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.federatedinsurance.crs.commons.data.to.IActNotPolFrm;
import com.federatedinsurance.crs.ws.Xz0b9050Data;

/**Original name: ActNotPolFrmXz0b9050<br>*/
public class ActNotPolFrmXz0b9050 implements IActNotPolFrm {

	//==== PROPERTIES ====
	private Xz0b9050Data ws;

	//==== CONSTRUCTORS ====
	public ActNotPolFrmXz0b9050(Xz0b9050Data ws) {
		this.ws = ws;
	}

	//==== METHODS ====
	@Override
	public String getCsrActNbr() {
		return ws.getDclactNotPol().getCsrActNbr();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		ws.getDclactNotPol().setCsrActNbr(csrActNbr);
	}

	@Override
	public String getNinAdrId() {
		return ws.getDclactNotPol().getNinAdrId();
	}

	@Override
	public void setNinAdrId(String ninAdrId) {
		ws.getDclactNotPol().setNinAdrId(ninAdrId);
	}

	@Override
	public String getNinCltId() {
		return ws.getDclactNotPol().getNinCltId();
	}

	@Override
	public void setNinCltId(String ninCltId) {
		ws.getDclactNotPol().setNinCltId(ninCltId);
	}

	@Override
	public String getNotEffDt() {
		return ws.getDclactNotPol().getNotEffDt();
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		ws.getDclactNotPol().setNotEffDt(notEffDt);
	}

	@Override
	public String getNotEffDtObj() {
		if (ws.getNiNotEffDt() >= 0) {
			return getNotEffDt();
		} else {
			return null;
		}
	}

	@Override
	public void setNotEffDtObj(String notEffDtObj) {
		if (notEffDtObj != null) {
			setNotEffDt(notEffDtObj);
			ws.setNiNotEffDt(((short) 0));
		} else {
			ws.setNiNotEffDt(((short) -1));
		}
	}

	@Override
	public String getNotPrcTs() {
		return ws.getDclactNotPol().getNotPrcTs();
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		ws.getDclactNotPol().setNotPrcTs(notPrcTs);
	}

	@Override
	public char getPolBilStaCd() {
		return ws.getDclactNotPol().getPolBilStaCd();
	}

	@Override
	public void setPolBilStaCd(char polBilStaCd) {
		ws.getDclactNotPol().setPolBilStaCd(polBilStaCd);
	}

	@Override
	public Character getPolBilStaCdObj() {
		if (ws.getNiPolBilStaCd() >= 0) {
			return (getPolBilStaCd());
		} else {
			return null;
		}
	}

	@Override
	public void setPolBilStaCdObj(Character polBilStaCdObj) {
		if (polBilStaCdObj != null) {
			setPolBilStaCd((polBilStaCdObj));
			ws.setNiPolBilStaCd(((short) 0));
		} else {
			ws.setNiPolBilStaCd(((short) -1));
		}
	}

	@Override
	public AfDecimal getPolDueAmt() {
		return ws.getDclactNotPol().getPolDueAmt();
	}

	@Override
	public void setPolDueAmt(AfDecimal polDueAmt) {
		ws.getDclactNotPol().setPolDueAmt(polDueAmt.copy());
	}

	@Override
	public AfDecimal getPolDueAmtObj() {
		if (ws.getNiPolDueAmt() >= 0) {
			return getPolDueAmt().toString();
		} else {
			return null;
		}
	}

	@Override
	public void setPolDueAmtObj(AfDecimal polDueAmtObj) {
		if (polDueAmtObj != null) {
			setPolDueAmt(new AfDecimal(polDueAmtObj, 10, 2));
			ws.setNiPolDueAmt(((short) 0));
		} else {
			ws.setNiPolDueAmt(((short) -1));
		}
	}

	@Override
	public String getPolEffDt() {
		return ws.getDclactNotPol().getPolEffDt();
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		ws.getDclactNotPol().setPolEffDt(polEffDt);
	}

	@Override
	public String getPolExpDt() {
		return ws.getDclactNotPol().getPolExpDt();
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		ws.getDclactNotPol().setPolExpDt(polExpDt);
	}

	@Override
	public String getPolNbr() {
		return ws.getDclactNotPol().getPolNbr();
	}

	@Override
	public void setPolNbr(String polNbr) {
		ws.getDclactNotPol().setPolNbr(polNbr);
	}

	@Override
	public String getPolPriRskStAbb() {
		return ws.getDclactNotPol().getPolPriRskStAbb();
	}

	@Override
	public void setPolPriRskStAbb(String polPriRskStAbb) {
		ws.getDclactNotPol().setPolPriRskStAbb(polPriRskStAbb);
	}

	@Override
	public String getPolTypCd() {
		return ws.getDclactNotPol().getPolTypCd();
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		ws.getDclactNotPol().setPolTypCd(polTypCd);
	}

	@Override
	public char getWfStartedInd() {
		return ws.getDclactNotPol().getWfStartedInd();
	}

	@Override
	public void setWfStartedInd(char wfStartedInd) {
		ws.getDclactNotPol().setWfStartedInd(wfStartedInd);
	}

	@Override
	public Character getWfStartedIndObj() {
		if (ws.getNiWfStartedInd() >= 0) {
			return (getWfStartedInd());
		} else {
			return null;
		}
	}

	@Override
	public void setWfStartedIndObj(Character wfStartedIndObj) {
		if (wfStartedIndObj != null) {
			setWfStartedInd((wfStartedIndObj));
			ws.setNiWfStartedInd(((short) 0));
		} else {
			ws.setNiWfStartedInd(((short) -1));
		}
	}
}
