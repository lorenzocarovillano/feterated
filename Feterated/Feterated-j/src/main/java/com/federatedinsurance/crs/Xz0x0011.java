/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.DriverSpecificData;
import com.federatedinsurance.crs.ws.ConstantFieldsXz0x0011;
import com.federatedinsurance.crs.ws.DfhcommareaTs020000;
import com.federatedinsurance.crs.ws.WorkingStorageAreaXz0x0005;
import com.federatedinsurance.crs.ws.ptr.DfhcommareaMu0x0004;
import com.federatedinsurance.crs.ws.ptr.LContractLocation;
import com.federatedinsurance.crs.ws.ptr.LFrameworkRequestAreaXz0x0012;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0011;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0X0011<br>
 * <pre>AUTHOR.       DAWN POSSEHL.
 * DATE-WRITTEN. 03 FEB 2009.
 * ***************************************************************
 *   PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
 *                   UOW        : XZ_GET_ACT_NOT_REC             *
 *                   OPERATIONS : GetActNotRecipientDtlByActTSRec*
 *                                                               *
 *   PLATFORM - HOST CICS                                        *
 *                                                               *
 *   PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
 *              EXECUTION OF FRAMEWORK MAIN DRIVER               *
 *                                                               *
 *   PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
 *                        PROGRAM OR FROM AN IVORY WRAPPER       *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
 *                         AND SHARED MEMORY                     *
 *                         OUTPUT RETURNED VIA DFHCOMMAREA       *
 *                         AND SHARED MEMORY                     *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #    DATE      EMP ID              DESCRIPTION          *
 *  -------- ---------  -------   ------------------------------ *
 *  TO07614  02/03/2009 E404DLP   INITIAL PROGRAM                *
 *  TO07614  03/09/2009 E404DLP   CHANGED ADR-SEQ-NBR TO ADR-ID  *
 *  TL000143 02/18/2010 E404DMA   UPDATED TO DYNAMIC PROXY LOGIC *
 *  PP02500  07/25/2012 E404BPO   RECOMPILE FOR XZ0C0003 CHANGE  *
 *                                                               *
 * ***************************************************************</pre>*/
public class Xz0x0011 extends Program {

	//==== PROPERTIES ====
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0x0011 constantFields = new ConstantFieldsXz0x0011();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0x0005 workingStorageArea = new WorkingStorageAreaXz0x0005();
	//Original name: MAIN-DRIVER-DATA
	private DfhcommareaTs020000 mainDriverData = new DfhcommareaTs020000();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaMu0x0004 dfhcommarea = new DfhcommareaMu0x0004(null);
	/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
	 * <pre>* "FRAMEWORK" REQUEST AREA FOR THIS OPERATION
	 * * INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE REQUEST TO
	 * * THE BUSINESS FRAMEWORK.</pre>*/
	private LFrameworkRequestAreaXz0x0012 lFrameworkRequestArea = new LFrameworkRequestAreaXz0x0012(null);
	/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
	 * <pre>* "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION
	 * * INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE RESPONSE FROM
	 * * THE BUSINESS FRAMEWORK.</pre>*/
	private LFrameworkRequestAreaXz0x0012 lFrameworkResponseArea = new LFrameworkRequestAreaXz0x0012(null);
	/**Original name: L-SERVICE-CONTRACT-AREA<br>
	 * <pre>* SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
	 * * BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK</pre>*/
	private LServiceContractAreaXz0x0011 lServiceContractArea = new LServiceContractAreaXz0x0011(null);
	/**Original name: L-CONTRACT-LOCATION<br>
	 * <pre>***************************************************************
	 *  TSC21PRO - PROXY COMMON LOGIC                                *
	 * ***************************************************************
	 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR COMMON LOGIC      *
	 *         COPYBOOK VERSIONING.                                  *
	 *         APPLICATION CODING.                                   *
	 *                                                               *
	 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
	 *                                                               *
	 *    WR #    DATE     EMP ID              DESCRIPTION           *
	 *  -------- --------- -------   ------------------------------- *
	 *  TS129    06MAR06   E404LJL   INITIAL VERSION                 *
	 *  TL000113 30JUN09   E404DMA   EXPAND FUNCTIONALITY SUCH THAT  *
	 *                               DATA CAN BE PASSED VIA CHANNEL  *
	 *                               & CONTAINERS, COMMAREA ONLY, OR *
	 *                               REFERENCE STORAGE (POINTERS).   *
	 * ***************************************************************</pre>*/
	private LContractLocation lContractLocation = new LContractLocation(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaMu0x0004 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea.assignBc(dfhcommarea);
		mainline();
		exit();
		return 0;
	}

	public static Xz0x0011 getInstance() {
		return (Programs.getInstance(Xz0x0011.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-CALL-FRAMEWORK
		//              THRU 3000-EXIT.
		callFramework();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: PERFORM 8000-ENDING-HOUSEKEEPING
		//              THRU 8000-EXIT.
		endingHousekeeping();
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM INITIALIZATION AND OTHER SETUP TASKS                 *
	 *  DETERMINE METHOD OF CAPTURING INPUT CONTRACTS TO GET         *
	 *  ADDRESSABILITY TO THEM.                                      *
	 * ***************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO WS-PROGRAM-NAME.
		workingStorageArea.setWsProgramName(constantFields.getProgramName());
		// COB_CODE: PERFORM 2500-CAPTURE-INPUT-CONTRACTS
		//              THRU 2500-EXIT.
		captureInputContracts();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: INITIALIZE DSD-ERROR-HANDLING-PARMS
		//                      PPC-ERROR-HANDLING-PARMS.
		initDsdErrorHandlingParms();
		initPpcErrorHandlingParms();
		//***************************************************************
		// FRAMEWORK FORMAT MEMORY NEEDS TO BE ALLOCATED BY THIS        *
		// PROGRAM BEFORE CALLING THE MAIN DRIVER.                      *
		//***************************************************************
		// COB_CODE: PERFORM 2100-ALLOCATE-FW-MEMORY
		//              THRU 2100-EXIT.
		allocateFwMemory();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-GET-USERID
		//              THRU 2200-EXIT.
		getUserid();
	}

	/**Original name: 2100-ALLOCATE-FW-MEMORY<br>
	 * <pre>***************************************************************
	 *  ALLOCATE THE FRAMEWORK REQUEST AND RESPONSE AREAS            *
	 *  FOR THIS OPERATION.                                          *
	 * ***************************************************************</pre>*/
	private void allocateFwMemory() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF L-FRAMEWORK-REQUEST-AREA
		//                                       TO MA-INPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(LFrameworkRequestAreaXz0x0012.Len.L_FRAMEWORK_REQUEST_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaInputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2100-ALLOCATE-FW-MEMORY'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2100-ALLOCATE-FW-MEMORY", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-FRAMEWORK-REQUEST-AREA
		//                                       TO MA-INPUT-POINTER.
		lFrameworkRequestArea = ((pointerManager
				.resolve(mainDriverData.getCommunicationShellCommon().getMaInputPointer(), LFrameworkRequestAreaXz0x0012.class)));
		// COB_CODE: INITIALIZE L-FRAMEWORK-REQUEST-AREA.
		initLFrameworkRequestArea();
		// COB_CODE: MOVE LENGTH OF L-FRAMEWORK-RESPONSE-AREA
		//                                       TO MA-OUTPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaOutputDataSize(LFrameworkRequestAreaXz0x0012.Len.L_FRAMEWORK_REQUEST_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//              SET(MA-OUTPUT-POINTER)
		//              FLENGTH(MA-OUTPUT-DATA-SIZE)
		//              INITIMG(CF-BLANK)
		//              RESP(WS-RESPONSE-CODE)
		//              RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaOutputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaOutputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2100-ALLOCATE-FW-MEMORY'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2100-ALLOCATE-FW-MEMORY", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-FRAMEWORK-RESPONSE-AREA
		//                                       TO MA-OUTPUT-POINTER.
		lFrameworkResponseArea = ((pointerManager
				.resolve(mainDriverData.getCommunicationShellCommon().getMaOutputPointer(), LFrameworkRequestAreaXz0x0012.class)));
		// COB_CODE: INITIALIZE L-FRAMEWORK-RESPONSE-AREA.
		initLFrameworkResponseArea();
	}

	/**Original name: 2200-GET-USERID<br>
	 * <pre>***************************************************************
	 *  GET THE USERID FOR THIS TRANSACTION FROM CICS AND POPULATE   *
	 *  THE FRAMEWORK LEVEL USERID FIELD.  THIS CAN BE OVERRIDEN BY  *
	 *  A USER-SUPPLIED USERID IF NECESSARY.                         *
	 * ***************************************************************</pre>*/
	private void getUserid() {
		// COB_CODE: INITIALIZE CSC-AUTH-USERID.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setAuthUserid("");
		// COB_CODE: EXEC CICS ASSIGN
		//               USERID(CSC-AUTH-USERID)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setAuthUserid(execContext.getUserId());
		execContext.clearStatus();
	}

	/**Original name: 2500-CAPTURE-INPUT-CONTRACTS<br>
	 * <pre>***************************************************************
	 *  DETERMINE IF THE DATA WAS PASSED USING CHANNELS AND          *
	 *  CONTAINERS, COMMAREA AND REFERENCE STORAGE, OR COMMARE ONLY. *
	 *  BASED ON THAT, CAPTURE THE PROXY COMMON CONTRACT AND SERVICE *
	 *  CONTRACT TO BE USED.                                         *
	 * ***************************************************************
	 *     IF THE DATA WAS PASSED USING CHANNELS AND CONTAINERS, THEN
	 *      THE COMMAREA WOULD NOT BE ALLOCATED, RESULTING IN A COMMAREA
	 *      LENGTH OF ZERO.  IF THE CONTRACT IS IN REFERENCE STORAGE,
	 *      THEN THE ONLY THING IN THE COMMAREA SHOULD BE THE PROXY
	 *      COMMON COPYBOOK.</pre>*/
	private void captureInputContracts() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EIBCALEN = +0
		//                      THRU 2510-EXIT
		//               WHEN EIBCALEN = LENGTH OF PROXY-PROGRAM-COMMON
		//                             + LENGTH OF L-SERVICE-CONTRACT-AREA
		//                      THRU 2520-EXIT
		//               WHEN OTHER
		//                      THRU 2530-EXIT
		//           END-EVALUATE.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 2510-CAPTURE-FROM-CHANNEL
			//              THRU 2510-EXIT
			captureFromChannel();
		} else if (execContext.getCommAreaLen() == DfhcommareaMu0x0004.Len.PROXY_PROGRAM_COMMON
				+ LServiceContractAreaXz0x0011.Len.L_SERVICE_CONTRACT_AREA) {
			// COB_CODE: PERFORM 2520-CAPTURE-FROM-COMMAREA
			//              THRU 2520-EXIT
			captureFromCommarea();
		} else {
			// COB_CODE: PERFORM 2530-CAPTURE-FROM-REF-STORAGE
			//              THRU 2530-EXIT
			captureFromRefStorage();
		}
	}

	/**Original name: 2510-CAPTURE-FROM-CHANNEL<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING CHANNELS AND CONTAINERS.  ALLOCATE AND *
	 *  CAPTURE THE PROXY COMMON CONTRACT FOLLOWED BY THE SERVICE    *
	 *  CONTRACT.                                                    *
	 * ***************************************************************</pre>*/
	private void captureFromChannel() {
		// COB_CODE: PERFORM 2511-ALLOCATE-PROXY-CNT
		//              THRU 2511-EXIT.
		allocateProxyCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2512-GET-PROXY-COMMON-CNT
		//              THRU 2512-EXIT.
		getProxyCommonCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2513-ALLOCATE-SERVICE-CONTRACT
		//              THRU 2513-EXIT.
		allocateServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		//    HAVE THE DEVELOPER SPECIFY THE LOCATION AND LENGTHS OF THE
		//     SERVICE CONTRACT INPUT AND OUTPUT AREAS.
		// COB_CODE: PERFORM 2514-CAPTURE-SVC-CTT-ATB
		//              THRU 2514-EXIT.
		captureSvcCttAtb();
		// COB_CODE: PERFORM 2515-GET-SERVICE-CONTRACT
		//              THRU 2515-EXIT.
		getServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
	}

	/**Original name: 2511-ALLOCATE-PROXY-CNT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE PROXY COMMON CONTRACT
	 * ***************************************************************</pre>*/
	private void allocateProxyCnt() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF DFHCOMMAREA  TO MA-INPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(DfhcommareaMu0x0004.Len.DFHCOMMAREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaInputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		//!   IF AN ISSUE IS ENCOUNTERED HERE, AN ABEND WILL LIKELY OCCUR.
		//!    ALTHOUGH NOT DESIRED, THERE IS NOTHING ELSE THAT CAN BE DONE
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2511-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2511-ALLOCATE-PROXY-CNT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2511-ALLOCATE-PROXY-CNT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2511-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF DFHCOMMAREA  TO MA-INPUT-POINTER.
		dfhcommarea = ((pointerManager.resolve(mainDriverData.getCommunicationShellCommon().getMaInputPointer(),
				DfhcommareaMu0x0004.class)));
		// COB_CODE: INITIALIZE DFHCOMMAREA.
		initDfhcommarea();
		//    WITH THE PROXY COMMON CONTRACT ALLOCATED, SPECIFY BOTH THE
		//     SIZE AND LOCATION OF THE INPUT AND OUTPUT AREAS.
		// COB_CODE: SET WS-PC-INPUT-PTR         TO ADDRESS OF PPC-INPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setInputPtr(pointerManager.addressOf(dfhcommarea));
		// COB_CODE: SET WS-PC-OUTPUT-PTR        TO ADDRESS OF PPC-OUTPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setOutputPtr(pointerManager.addressOf(dfhcommarea, DfhcommareaMu0x0004.Pos.PPC_OUTPUT_PARMS));
		// COB_CODE: COMPUTE WS-PC-INPUT-LEN = LENGTH OF PPC-INPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setInputLen(DfhcommareaMu0x0004.Len.PPC_INPUT_PARMS);
		// COB_CODE: COMPUTE WS-PC-OUTPUT-LEN = LENGTH OF PPC-OUTPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setOutputLen(DfhcommareaMu0x0004.Len.PPC_OUTPUT_PARMS);
	}

	/**Original name: 2512-GET-PROXY-COMMON-CNT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE PROXY COMMON CONTRACT       *
	 * ***************************************************************</pre>*/
	private void getProxyCommonCnt() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-PC-INPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsProxyContractAtb().getInputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-PROXY-CBK-INP-NM)
		//               INTO       (L-CONTRACT-LOCATION (1: WS-PC-INPUT-LEN))
		//               FLENGTH    (WS-PC-INPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsProxyContractAtb().getInputLen());
		Channel.read(execContext, "", constantFields.getCopybookNames().getProxyCbkInpNmFormatted(), tsOutputData);
		lContractLocation.setlContractLocationFromBuffer(tsOutputData.getData());
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2512-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2512-GET-PROXY-COMMON-CNT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2512-GET-PROXY-COMMON-CNT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2512-EXIT
			return;
		}
	}

	/**Original name: 2513-ALLOCATE-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT  *
	 * ***************************************************************</pre>*/
	private void allocateServiceContract() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF L-SERVICE-CONTRACT-AREA
		//                                       TO MA-INPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(LServiceContractAreaXz0x0011.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaInputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2513-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2513-ALLOCATE-SERVICE-CONTRACT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2513-ALLOCATE-SERVICE-CONTRACT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2513-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO MA-INPUT-POINTER.
		lServiceContractArea = ((pointerManager
				.resolve(mainDriverData.getCommunicationShellCommon().getMaInputPointer(), LServiceContractAreaXz0x0011.class)));
		// COB_CODE: INITIALIZE L-SERVICE-CONTRACT-AREA.
		initLServiceContractArea();
	}

	/**Original name: 2514-CAPTURE-SVC-CTT-ATB<br>
	 * <pre>***************************************************************
	 *  CAPTURE BOTH THE SIZE AND LOCATION OF THE INPUT AND OUTPUT   *
	 *  AREAS OF THE SERVICE CONTRACT AS SPECIFIED BY THE DEVELOPER  *
	 * ***************************************************************</pre>*/
	private void captureSvcCttAtb() {
		// COB_CODE: PERFORM 12514-SET-SVC-CTT-ATB
		//              THRU 12514-EXIT.
		setSvcCttAtb();
	}

	/**Original name: 2515-GET-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE SERVICE CONTRACT            *
	 * ***************************************************************</pre>*/
	private void getServiceContract() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-SC-INPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsServiceContractAtb().getInputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-SERVICE-CBK-INP-NM)
		//               INTO       (L-CONTRACT-LOCATION (1: WS-SC-INPUT-LEN))
		//               FLENGTH    (WS-SC-INPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsServiceContractAtb().getInputLen());
		Channel.read(execContext, "", constantFields.getCopybookNames().getServiceCbkInpNmFormatted(), tsOutputData);
		lContractLocation.setlContractLocationFromBuffer(tsOutputData.getData());
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2515-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2515-GET-SERVICE-CONTRACT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2515-GET-SERVICE-CONTRACT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2515-EXIT
			return;
		}
	}

	/**Original name: 2520-CAPTURE-FROM-COMMAREA<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING THE COMMAREA ONLY.  SINCE THE          *
	 *  ENVIRONMENT WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY,   *
	 *  SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT.              *
	 * ***************************************************************
	 *   SINCE THE SERVICE CONTRACT IS APPENDED TO THE PROXY CONTRACT,
	 *    SET THE LOCATION OF THE SERVICE CONTRACT TO BE IMMEDIATELY
	 *    AFTER THE PROXY CONTRACT.</pre>*/
	private void captureFromCommarea() {
		// COB_CODE: SET WS-CA-PTR               TO
		//                                        ADDRESS OF PROXY-PROGRAM-COMMON.
		workingStorageArea.getWsCaPtr().setWsCaPtr(pointerManager.addressOf(dfhcommarea));
		// COB_CODE: ADD LENGTH OF PROXY-PROGRAM-COMMON
		//                                       TO WS-CA-PTR-VAL.
		workingStorageArea.getWsCaPtr().setWsCaPtrVal(DfhcommareaMu0x0004.Len.PROXY_PROGRAM_COMMON + workingStorageArea.getWsCaPtr().getWsCaPtrVal());
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO WS-CA-PTR.
		lServiceContractArea = ((pointerManager.resolve(workingStorageArea.getWsCaPtr().getWsCaPtr(),
				LServiceContractAreaXz0x0011.class)));
	}

	/**Original name: 2530-CAPTURE-FROM-REF-STORAGE<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING REFERENCE STORAGE.  SINCE THE CALLER   *
	 *  WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY, SET THE       *
	 *  ADDRESSIBILITY OF THE SERVICE CONTRACT.                      *
	 * ***************************************************************</pre>*/
	private void captureFromRefStorage() {
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO PPC-SERVICE-DATA-POINTER.
		lServiceContractArea = ((pointerManager.resolve(dfhcommarea.getPpcServiceDataPointer(),
				LServiceContractAreaXz0x0011.class)));
	}

	/**Original name: 3000-CALL-FRAMEWORK<br>
	 * <pre>***************************************************************
	 *  PERFORM SET UP FOR AND MAKE THE CALL TO THE FRAMEWORK MAIN   *
	 *  DRIVER.                                                      *
	 * ***************************************************************</pre>*/
	private void callFramework() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 3100-SET-UP-INPUT
		//              THRU 3100-EXIT.
		setUpInput();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CF-MAIN-DRIVER)
		//               COMMAREA   (MAIN-DRIVER-DATA)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0X0011", execContext).commarea(mainDriverData).length(DfhcommareaTs020000.Len.DFHCOMMAREA)
				.link(constantFields.getMainDriver(), new Ts020000());
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '3000-CALL-FRAMEWORK'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"3000-CALL-FRAMEWORK", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: IF NOT DSD-NO-ERROR-CODE
		//                                       TO PPC-ERROR-HANDLING-PARMS
		//           END-IF.
		if (!mainDriverData.getDriverSpecificData().getErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: MOVE DSD-ERROR-HANDLING-PARMS
			//                                   TO PPC-ERROR-HANDLING-PARMS
			dfhcommarea.setPpcErrorHandlingParmsBytes(mainDriverData.getDriverSpecificData().getDsdErrorHandlingParmsBytes());
		}
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3200-SET-UP-OUTPUT
		//              THRU 3200-EXIT.
		setUpOutput();
	}

	/**Original name: 3100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
	 *  INPUT AREA                                                   *
	 * ***************************************************************
	 *  FRAMEWORK PARAMETERS</pre>*/
	private void setUpInput() {
		// COB_CODE: MOVE PPC-OPERATION          TO CSC-OPERATION.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setOperation(dfhcommarea.getPpcOperation());
		// COB_CODE: MOVE CF-UNIT-OF-WORK        TO CSC-UNIT-OF-WORK.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setUnitOfWork(constantFields.getUnitOfWork());
		// COB_CODE: MOVE CF-REQUEST-MODULE      TO CSC-REQUEST-MODULE.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setRequestModule(constantFields.getRequestModule());
		// COB_CODE: MOVE CF-RESPONSE-MODULE     TO CSC-RESPONSE-MODULE.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setResponseModule(constantFields.getResponseModule());
		// COB_CODE: MOVE PPC-BYPASS-SYNCPOINT-MDRV-IND
		//                                       TO DSD-BYPASS-SYNCPOINT-MDRV-IND.
		mainDriverData.getDriverSpecificData().getBypassSyncpointMdrvInd().setBypassSyncpointMdrvInd(dfhcommarea.getPpcBypassSyncpointMdrvInd());
		// COB_CODE: PERFORM 13100-SET-UP-INPUT
		//              THRU 13100-EXIT.
		setUpInput1();
	}

	/**Original name: 3200-SET-UP-OUTPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
	 *  RESPONSE AREA.                                               *
	 * ***************************************************************</pre>*/
	private void setUpOutput() {
		// COB_CODE: PERFORM 13200-SET-UP-OUTPUT
		//              THRU 13200-EXIT.
		setUpOutput1();
		// COB_CODE: IF EIBCALEN = +0
		//                  THRU 3210-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 3210-OUTPUT-SVC-CONTRACT-CTA
			//              THRU 3210-EXIT
			outputSvcContractCta();
		}
	}

	/**Original name: 3210-OUTPUT-SVC-CONTRACT-CTA<br>
	 * <pre>***************************************************************
	 *  SINCE THE SERVICE CONTRACT WILL NOT BE UPDATED FURTHER FROM  *
	 *  THIS POINT, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************</pre>*/
	private void outputSvcContractCta() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-SC-OUTPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsServiceContractAtb().getOutputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-SERVICE-CBK-OUP-NM)
		//               FROM       (L-CONTRACT-LOCATION (1: WS-SC-OUTPUT-LEN))
		//               FLENGTH    (WS-SC-OUTPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsServiceContractAtb().getOutputLen());
		tsOutputData.setData(lContractLocation.getlContractLocationAsBuffer());
		Channel.write(execContext, "", constantFields.getCopybookNames().getServiceCbkOupNmFormatted(), tsOutputData);
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3210-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '3210-OUTPUT-SVC-CONTRACT-CTA'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"3210-OUTPUT-SVC-CONTRACT-CTA", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 3210-EXIT
			return;
		}
	}

	/**Original name: 8000-ENDING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM CLEANUP TASKS SUCH AS FREEING MEMORY.                *
	 * ***************************************************************</pre>*/
	private void endingHousekeeping() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(L-FRAMEWORK-REQUEST-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lFrameworkRequestArea));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-1'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-1", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(L-FRAMEWORK-RESPONSE-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lFrameworkResponseArea));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-2'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-2", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IF USING CHANNELS AND CONTAINERS, DEALLOCATE THE SERVICE
		//     CONTRACT AND THE PROXY CONTRACT.  OTHERWISE, THAT WILL BE
		//     THE RESPONSIBILITY OF THE CALLER AND/OR THE CICS ENVIRONMENT
		// COB_CODE: IF EIBCALEN = +0
		//               CONTINUE
		//           ELSE
		//               GO TO 8000-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
		//     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
		// COB_CODE: IF ADDRESS OF L-SERVICE-CONTRACT-AREA NOT = NULLS
		//               END-EXEC
		//           END-IF.
		if (!pointerManager.isNullPointer(pointerManager.addressOf(lServiceContractArea))) {
			// COB_CODE: EXEC CICS FREEMAIN
			//               DATA(L-SERVICE-CONTRACT-AREA)
			//               RESP(WS-RESPONSE-CODE)
			//               RESP2(WS-RESPONSE-CODE2)
			//           END-EXEC
			cicsStorageManager.freemain(execContext, pointerManager.addressOf(lServiceContractArea));
			workingStorageArea.setWsResponseCode(execContext.getResp());
			workingStorageArea.setWsResponseCode2(execContext.getResp2());
		}
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL
				&& !pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-3'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-3", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
		//              THRU 8100-EXIT.
		outputProxyCommonCta();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 8000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
		//     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
		// COB_CODE: IF ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               END-EXEC
		//           END-IF.
		if (!pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: EXEC CICS FREEMAIN
			//               DATA(DFHCOMMAREA)
			//               RESP(WS-RESPONSE-CODE)
			//               RESP2(WS-RESPONSE-CODE2)
			//           END-EXEC
			cicsStorageManager.freemain(execContext, pointerManager.addressOf(dfhcommarea));
			workingStorageArea.setWsResponseCode(execContext.getResp());
			workingStorageArea.setWsResponseCode2(execContext.getResp2());
		}
		//!   RECORD AN ERROR IF ONE OCCURS AND THE MEMORY IS STILL
		//!    ALLOCATED.  IF IT IS NOT ALLOCATED, THEN WE WOULD RUN INTO
		//!    ADDRESSING ISSUES AND MOST LIKELY ABEND.  EITHER WAY THOUGH,
		//!    THE ERROR WILL NOT GET BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL
				&& !pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-4'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-4", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
	}

	/**Original name: 8100-OUTPUT-PROXY-COMMON-CTA<br>
	 * <pre>***************************************************************
	 *  SINCE THE PROXY COMMON CONTRACT SHOULD NOT BE UPDATED ANY    *
	 *  FURTHER, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW    *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************
	 *     IT IS POSSIBLE TO GET INTO THIS PARAGRAPH WHEN USING A METHOD
	 *      OTHER THAN CHANNELS.  BECAUSE OF THIS, WE WILL MAKE SURE WE
	 *      ARE UTILIZING A CHANNEL BEFORE PLACING THE COPYBOOK INTO A
	 *      CONTAINER.</pre>*/
	private void outputProxyCommonCta() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT EIBCALEN = +0
		//               GO TO 8100-EXIT
		//           END-IF.
		if (!(execContext.getCommAreaLen() == 0)) {
			// COB_CODE: GO TO 8100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-PC-OUTPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsProxyContractAtb().getOutputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-PROXY-CBK-OUP-NM)
		//               FROM       (L-CONTRACT-LOCATION (1: WS-PC-OUTPUT-LEN))
		//               FLENGTH    (WS-PC-OUTPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsProxyContractAtb().getOutputLen());
		tsOutputData.setData(lContractLocation.getlContractLocationAsBuffer());
		Channel.write(execContext, "", constantFields.getCopybookNames().getProxyCbkOupNmFormatted(), tsOutputData);
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		//!   RECORD AN ERROR IF ONE OCCURS, BUT THE ERROR WILL NOT GET
		//!   BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8100-OUTPUT-PROXY-COMMON'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8100-OUTPUT-PROXY-COMMON", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 8100-EXIT
			return;
		}
	}

	/**Original name: 12514-SET-SVC-CTT-ATB<br>
	 * <pre>***************************************************************
	 *  SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
	 * ***************************************************************</pre>*/
	private void setSvcCttAtb() {
		// COB_CODE: SET WS-SC-INPUT-PTR         TO ADDRESS OF
		//                                             XZT011-SERVICE-INPUTS.
		workingStorageArea.getWsServiceContractAtb().setInputPtr(pointerManager.addressOf(lServiceContractArea));
		// COB_CODE: SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
		//                                             XZT011-SERVICE-OUTPUTS.
		workingStorageArea.getWsServiceContractAtb()
				.setOutputPtr(pointerManager.addressOf(lServiceContractArea, LServiceContractAreaXz0x0011.Pos.XZT011_SERVICE_OUTPUTS));
		// COB_CODE: MOVE LENGTH OF XZT011-SERVICE-INPUTS
		//                                       TO WS-SC-INPUT-LEN.
		workingStorageArea.getWsServiceContractAtb().setInputLen(LServiceContractAreaXz0x0011.Len.XZT011_SERVICE_INPUTS);
		// COB_CODE: MOVE LENGTH OF XZT011-SERVICE-OUTPUTS
		//                                       TO WS-SC-OUTPUT-LEN.
		workingStorageArea.getWsServiceContractAtb().setOutputLen(LServiceContractAreaXz0x0011.Len.XZT011_SERVICE_OUTPUTS);
	}

	/**Original name: 13100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
	 *  INPUT AREA                                                   *
	 * ***************************************************************
	 *  IF A USERID WAS SUPPLIED, PASS IT TO THE FRAMEWORK</pre>*/
	private void setUpInput1() {
		// COB_CODE: IF XZT11I-USERID NOT = SPACES
		//             AND
		//              XZT11I-USERID NOT = LOW-VALUES
		//               MOVE XZT11I-USERID      TO CSC-AUTH-USERID
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getXzt11iUserid())
				&& !Characters.EQ_LOW.test(lServiceContractArea.getXzt11iUseridFormatted())) {
			// COB_CODE: MOVE XZT11I-USERID      TO CSC-AUTH-USERID
			mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setAuthUserid(lServiceContractArea.getXzt11iUserid());
		}
		// COB_CODE: MOVE XZT11I-CSR-ACT-NBR     TO XZC003Q-CSR-ACT-NBR.
		lFrameworkRequestArea.setXzc003qCsrActNbr(lServiceContractArea.getXzt11iCsrActNbr());
		// COB_CODE: MOVE XZT11I-TK-NOT-PRC-TS   TO XZC003Q-NOT-PRC-TS.
		lFrameworkRequestArea.setXzc003qNotPrcTs(lServiceContractArea.getXzt11iTkNotPrcTs());
		// COB_CODE: MOVE XZT11I-TK-REC-SEQ-NBR  TO XZC003Q-REC-SEQ-NBR.
		lFrameworkRequestArea.setXzc003qRecSeqNbrFormatted(Trunc.removeSign(lServiceContractArea.getXzt11iTkRecSeqNbrFormatted()));
		// COB_CODE: IF XZT11I-TK-REC-SEQ-NBR  >= +0
		//               MOVE CF-POSITIVE        TO XZC003Q-REC-SEQ-NBR-SIGN
		//           ELSE
		//               MOVE CF-NEGATIVE        TO XZC003Q-REC-SEQ-NBR-SIGN
		//           END-IF.
		if (lServiceContractArea.getXzt11iTkRecSeqNbr() >= 0) {
			// COB_CODE: MOVE CF-POSITIVE        TO XZC003Q-REC-SEQ-NBR-SIGN
			lFrameworkRequestArea.setXzc003qRecSeqNbrSign(constantFields.getPositive());
		} else {
			// COB_CODE: MOVE CF-NEGATIVE        TO XZC003Q-REC-SEQ-NBR-SIGN
			lFrameworkRequestArea.setXzc003qRecSeqNbrSign(constantFields.getNegative());
		}
	}

	/**Original name: 13200-SET-UP-OUTPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
	 *  RESPONSE AREA.                                               *
	 * ***************************************************************</pre>*/
	private void setUpOutput1() {
		// COB_CODE: MOVE XZC003R-CSR-ACT-NBR    TO XZT11O-CSR-ACT-NBR.
		lServiceContractArea.setXzt11oCsrActNbr(lFrameworkResponseArea.getXzc003qCsrActNbr());
		// COB_CODE: MOVE XZC003R-NOT-PRC-TS     TO XZT11O-TK-NOT-PRC-TS.
		lServiceContractArea.setXzt11oTkNotPrcTs(lFrameworkResponseArea.getXzc003qNotPrcTs());
		// COB_CODE: MOVE XZC003R-REC-SEQ-NBR    TO XZT11O-TK-REC-SEQ-NBR.
		lServiceContractArea.setXzt11oTkRecSeqNbrFormatted(Trunc.addPlusSign(lFrameworkResponseArea.getXzc003rRecSeqNbrFormatted()));
		// COB_CODE: IF XZC003R-REC-SEQ-NBR-SIGN = CF-NEGATIVE
		//                       CF-NEGATIVE-ONE
		//           END-IF.
		if (lFrameworkResponseArea.getXzc003qRecSeqNbrSign() == constantFields.getNegative()) {
			// COB_CODE: COMPUTE XZT11O-TK-REC-SEQ-NBR =
			//                   XZT11O-TK-REC-SEQ-NBR *
			//                   CF-NEGATIVE-ONE
			lServiceContractArea.setXzt11oTkRecSeqNbr(Trunc.toInt(lServiceContractArea.getXzt11oTkRecSeqNbr() * constantFields.getNegativeOne(), 5));
		}
		// COB_CODE: MOVE XZC003R-REC-CLT-ID     TO XZT11O-TK-REC-CLT-ID.
		lServiceContractArea.setXzt11oTkRecCltId(lFrameworkResponseArea.getXzc003qRecCltId());
		// COB_CODE: MOVE XZC003R-REC-ADR-ID     TO XZT11O-TK-REC-ADR-ID.
		lServiceContractArea.setXzt11oTkRecAdrId(lFrameworkResponseArea.getXzc003qRecAdrId());
		// COB_CODE: MOVE XZC003R-MNL-IND        TO XZT11O-TK-MNL-IND.
		lServiceContractArea.setXzt11oTkMnlInd(lFrameworkResponseArea.getXzc003qMnlInd());
		// COB_CODE: MOVE XZC003R-ACT-NOT-REC-CSUM
		//                                       TO XZT11O-TK-ACT-NOT-REC-CSUM.
		lServiceContractArea.setXzt11oTkActNotRecCsumFormatted(lFrameworkResponseArea.getXzc003rActNotRecCsumFormatted());
		// COB_CODE: MOVE XZC003R-REC-TYP-CD     TO XZT11O-REC-TYP-CD.
		lServiceContractArea.setXzt11oRecTypCd(lFrameworkResponseArea.getXzc003qRecTypCd());
		// COB_CODE: MOVE XZC003R-REC-TYP-SHT-DES
		//                                       TO XZT11O-REC-TYP-SHT-DES.
		lServiceContractArea.setXzt11oRecTypShtDes(lFrameworkResponseArea.getXzc003qRecTypShtDes());
		// COB_CODE: MOVE XZC003R-REC-TYP-LNG-DES
		//                                       TO XZT11O-REC-TYP-LNG-DES.
		lServiceContractArea.setXzt11oRecTypLngDes(lFrameworkResponseArea.getXzc003qRecTypLngDes());
		// COB_CODE: MOVE XZC003R-REC-SR-ORD-NBR TO XZT11O-REC-TYP-SR-ORD-NBR.
		lServiceContractArea.setXzt11oRecTypSrOrdNbrFormatted(Trunc.addPlusSign(lFrameworkResponseArea.getXzc003rRecSrOrdNbrFormatted()));
		// COB_CODE: IF XZC003R-REC-SR-ORD-NBR-SIGN = CF-NEGATIVE
		//                       CF-NEGATIVE-ONE
		//           END-IF.
		if (lFrameworkResponseArea.getXzc003qRecSrOrdNbrSign() == constantFields.getNegative()) {
			// COB_CODE: COMPUTE XZT11O-REC-TYP-SR-ORD-NBR =
			//                   XZT11O-REC-TYP-SR-ORD-NBR *
			//                   CF-NEGATIVE-ONE
			lServiceContractArea
					.setXzt11oRecTypSrOrdNbr(Trunc.toInt(lServiceContractArea.getXzt11oRecTypSrOrdNbr() * constantFields.getNegativeOne(), 5));
		}
		// COB_CODE: MOVE XZC003R-REC-NM         TO XZT11O-REC-NAME.
		lServiceContractArea.setXzt11oRecName(lFrameworkResponseArea.getXzc003qRecNm());
		// COB_CODE: MOVE XZC003R-LIN-1-ADR      TO XZT11O-LIN-1-ADR.
		lServiceContractArea.setXzt11oLin1Adr(lFrameworkResponseArea.getXzc003qLin1Adr());
		// COB_CODE: MOVE XZC003R-LIN-2-ADR      TO XZT11O-LIN-2-ADR.
		lServiceContractArea.setXzt11oLin2Adr(lFrameworkResponseArea.getXzc003qLin2Adr());
		// COB_CODE: MOVE XZC003R-CIT-NM         TO XZT11O-CITY-NAME.
		lServiceContractArea.setXzt11oCityName(lFrameworkResponseArea.getXzc003qCitNm());
		// COB_CODE: MOVE XZC003R-ST-ABB         TO XZT11O-STATE-ABB.
		lServiceContractArea.setXzt11oStateAbb(lFrameworkResponseArea.getXzc003qStAbb());
		// COB_CODE: MOVE XZC003R-PST-CD         TO XZT11O-POSTAL-CD.
		lServiceContractArea.setXzt11oPostalCd(lFrameworkResponseArea.getXzc003qPstCd());
		// COB_CODE: MOVE XZC003R-CER-NBR        TO XZT11O-CERT-NBR.
		lServiceContractArea.setXzt11oCertNbr(lFrameworkResponseArea.getXzc003qCerNbr());
	}

	public void initDsdErrorHandlingParms() {
		mainDriverData.getDriverSpecificData().getErrorReturnCode().setErrorReturnCodeFormatted("0000");
		mainDriverData.getDriverSpecificData().setFatalErrorMessage("");
		mainDriverData.getDriverSpecificData().setNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			mainDriverData.getDriverSpecificData().getNonLoggableErrors(idx0).setDsdNonLogErrMsg("");
		}
		mainDriverData.getDriverSpecificData().setWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.WARNINGS_MAXOCCURS; idx0++) {
			mainDriverData.getDriverSpecificData().getWarnings(idx0).setDsdWarnMsg("");
		}
	}

	public void initPpcErrorHandlingParms() {
		dfhcommarea.setPpcErrorReturnCodeFormatted("0000");
		dfhcommarea.setPpcFatalErrorMessage("");
		dfhcommarea.setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcNonLogErrMsg(idx0, "");
		}
		dfhcommarea.setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcWarnMsg(idx0, "");
		}
	}

	public void initMemoryAllocationParms() {
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(0);
		mainDriverData.getCommunicationShellCommon().setMaOutputDataSize(0);
		mainDriverData.getCommunicationShellCommon().getMaReturnCode().setMaReturnCodeFormatted("0000");
		mainDriverData.getCommunicationShellCommon().setMaErrorMessage("");
	}

	public void initLFrameworkRequestArea() {
		lFrameworkRequestArea.setXzc003qActNotRecCsumFormatted("000000000");
		lFrameworkRequestArea.setXzc003qCsrActNbrKcre("");
		lFrameworkRequestArea.setXzc003qNotPrcTsKcre("");
		lFrameworkRequestArea.setXzc003qRecSeqNbrKcre("");
		lFrameworkRequestArea.setXzc003qTransProcessDt("");
		lFrameworkRequestArea.setXzc003qCsrActNbr("");
		lFrameworkRequestArea.setXzc003qNotPrcTs("");
		lFrameworkRequestArea.setXzc003qRecSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecSeqNbrFormatted("00000");
		lFrameworkRequestArea.setXzc003qCsrActNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qNotPrcTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecTypCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecTypCd("");
		lFrameworkRequestArea.setXzc003qRecCltIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecCltIdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecCltId("");
		lFrameworkRequestArea.setXzc003qRecNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecNmNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecNm("");
		lFrameworkRequestArea.setXzc003qRecAdrIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecAdrIdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecAdrId("");
		lFrameworkRequestArea.setXzc003qLin1AdrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qLin1AdrNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qLin1Adr("");
		lFrameworkRequestArea.setXzc003qLin2AdrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qLin2AdrNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qLin2Adr("");
		lFrameworkRequestArea.setXzc003qCitNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qCitNmNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qCitNm("");
		lFrameworkRequestArea.setXzc003qStAbbCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qStAbbNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qStAbb("");
		lFrameworkRequestArea.setXzc003qPstCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qPstCdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qPstCd("");
		lFrameworkRequestArea.setXzc003qMnlIndCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qMnlInd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qCerNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qCerNbrNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qCerNbr("");
		lFrameworkRequestArea.setXzc003qRecTypShtDes("");
		lFrameworkRequestArea.setXzc003qRecTypLngDes("");
		lFrameworkRequestArea.setXzc003qRecSrOrdNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setXzc003qRecSrOrdNbrFormatted("00000");
	}

	public void initLFrameworkResponseArea() {
		lFrameworkResponseArea.setXzc003qActNotRecCsumFormatted("000000000");
		lFrameworkResponseArea.setXzc003qCsrActNbrKcre("");
		lFrameworkResponseArea.setXzc003qNotPrcTsKcre("");
		lFrameworkResponseArea.setXzc003qRecSeqNbrKcre("");
		lFrameworkResponseArea.setXzc003qTransProcessDt("");
		lFrameworkResponseArea.setXzc003qCsrActNbr("");
		lFrameworkResponseArea.setXzc003qNotPrcTs("");
		lFrameworkResponseArea.setXzc003qRecSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecSeqNbrFormatted("00000");
		lFrameworkResponseArea.setXzc003qCsrActNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qNotPrcTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecTypCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecTypCd("");
		lFrameworkResponseArea.setXzc003qRecCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecCltIdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecCltId("");
		lFrameworkResponseArea.setXzc003qRecNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecNmNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecNm("");
		lFrameworkResponseArea.setXzc003qRecAdrIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecAdrIdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecAdrId("");
		lFrameworkResponseArea.setXzc003qLin1AdrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qLin1AdrNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qLin1Adr("");
		lFrameworkResponseArea.setXzc003qLin2AdrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qLin2AdrNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qLin2Adr("");
		lFrameworkResponseArea.setXzc003qCitNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qCitNmNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qCitNm("");
		lFrameworkResponseArea.setXzc003qStAbbCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qStAbbNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qStAbb("");
		lFrameworkResponseArea.setXzc003qPstCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qPstCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qPstCd("");
		lFrameworkResponseArea.setXzc003qMnlIndCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qMnlInd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qCerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qCerNbrNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qCerNbr("");
		lFrameworkResponseArea.setXzc003qRecTypShtDes("");
		lFrameworkResponseArea.setXzc003qRecTypLngDes("");
		lFrameworkResponseArea.setXzc003qRecSrOrdNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setXzc003qRecSrOrdNbrFormatted("00000");
	}

	public void initDfhcommarea() {
		dfhcommarea.setPpcServiceDataSize(0);
		dfhcommarea.setPpcBypassSyncpointMdrvInd(Types.SPACE_CHAR);
		dfhcommarea.setPpcOperation("");
		dfhcommarea.setPpcErrorReturnCodeFormatted("0000");
		dfhcommarea.setPpcFatalErrorMessage("");
		dfhcommarea.setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcNonLogErrMsg(idx0, "");
		}
		dfhcommarea.setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcWarnMsg(idx0, "");
		}
	}

	public void initLServiceContractArea() {
		lServiceContractArea.setXzt11iTkNotPrcTs("");
		lServiceContractArea.setXzt11iTkRecSeqNbr(0);
		lServiceContractArea.setXzt11iCsrActNbr("");
		lServiceContractArea.setXzt11iUserid("");
		lServiceContractArea.setXzt11oTkNotPrcTs("");
		lServiceContractArea.setXzt11oTkRecSeqNbr(0);
		lServiceContractArea.setXzt11oTkRecCltId("");
		lServiceContractArea.setXzt11oTkRecAdrId("");
		lServiceContractArea.setXzt11oTkMnlInd(Types.SPACE_CHAR);
		lServiceContractArea.setXzt11oTkActNotRecCsumFormatted("000000000");
		lServiceContractArea.setXzt11oCsrActNbr("");
		lServiceContractArea.setXzt11oRecTypCd("");
		lServiceContractArea.setXzt11oRecTypShtDes("");
		lServiceContractArea.setXzt11oRecTypLngDes("");
		lServiceContractArea.setXzt11oRecTypSrOrdNbr(0);
		lServiceContractArea.setXzt11oRecName("");
		lServiceContractArea.setXzt11oLin1Adr("");
		lServiceContractArea.setXzt11oLin2Adr("");
		lServiceContractArea.setXzt11oCityName("");
		lServiceContractArea.setXzt11oStateAbb("");
		lServiceContractArea.setXzt11oPostalCd("");
		lServiceContractArea.setXzt11oCertNbr("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
