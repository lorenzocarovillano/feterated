/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0013<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0013 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0013() {
	}

	public LServiceContractAreaXz0x0013(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt13iTkNotPrcTs(String xzt13iTkNotPrcTs) {
		writeString(Pos.XZT13I_TK_NOT_PRC_TS, xzt13iTkNotPrcTs, Len.XZT13I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT13I-TK-NOT-PRC-TS<br>*/
	public String getXzt13iTkNotPrcTs() {
		return readString(Pos.XZT13I_TK_NOT_PRC_TS, Len.XZT13I_TK_NOT_PRC_TS);
	}

	public void setXzt13iTkFrmSeqNbr(int xzt13iTkFrmSeqNbr) {
		writeInt(Pos.XZT13I_TK_FRM_SEQ_NBR, xzt13iTkFrmSeqNbr, Len.Int.XZT13I_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT13I-TK-FRM-SEQ-NBR<br>*/
	public int getXzt13iTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT13I_TK_FRM_SEQ_NBR, Len.XZT13I_TK_FRM_SEQ_NBR);
	}

	public String getXzt13iTkFrmSeqNbrFormatted() {
		return readFixedString(Pos.XZT13I_TK_FRM_SEQ_NBR, Len.XZT13I_TK_FRM_SEQ_NBR);
	}

	public void setXzt13iCsrActNbr(String xzt13iCsrActNbr) {
		writeString(Pos.XZT13I_CSR_ACT_NBR, xzt13iCsrActNbr, Len.XZT13I_CSR_ACT_NBR);
	}

	/**Original name: XZT13I-CSR-ACT-NBR<br>*/
	public String getXzt13iCsrActNbr() {
		return readString(Pos.XZT13I_CSR_ACT_NBR, Len.XZT13I_CSR_ACT_NBR);
	}

	public void setXzt13iUserid(String xzt13iUserid) {
		writeString(Pos.XZT13I_USERID, xzt13iUserid, Len.XZT13I_USERID);
	}

	/**Original name: XZT13I-USERID<br>*/
	public String getXzt13iUserid() {
		return readString(Pos.XZT13I_USERID, Len.XZT13I_USERID);
	}

	public String getXzt13iUseridFormatted() {
		return Functions.padBlanks(getXzt13iUserid(), Len.XZT13I_USERID);
	}

	public void setXzt13oTkNotPrcTs(String xzt13oTkNotPrcTs) {
		writeString(Pos.XZT13O_TK_NOT_PRC_TS, xzt13oTkNotPrcTs, Len.XZT13O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT13O-TK-NOT-PRC-TS<br>*/
	public String getXzt13oTkNotPrcTs() {
		return readString(Pos.XZT13O_TK_NOT_PRC_TS, Len.XZT13O_TK_NOT_PRC_TS);
	}

	public void setXzt13oTkFrmSeqNbr(int xzt13oTkFrmSeqNbr) {
		writeInt(Pos.XZT13O_TK_FRM_SEQ_NBR, xzt13oTkFrmSeqNbr, Len.Int.XZT13O_TK_FRM_SEQ_NBR);
	}

	public void setXzt13oTkFrmSeqNbrFormatted(String xzt13oTkFrmSeqNbr) {
		writeString(Pos.XZT13O_TK_FRM_SEQ_NBR, Trunc.toUnsignedNumeric(xzt13oTkFrmSeqNbr, Len.XZT13O_TK_FRM_SEQ_NBR), Len.XZT13O_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT13O-TK-FRM-SEQ-NBR<br>*/
	public int getXzt13oTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT13O_TK_FRM_SEQ_NBR, Len.XZT13O_TK_FRM_SEQ_NBR);
	}

	public void setXzt13oTkActNotFrmCsumFormatted(String xzt13oTkActNotFrmCsum) {
		writeString(Pos.XZT13O_TK_ACT_NOT_FRM_CSUM, Trunc.toUnsignedNumeric(xzt13oTkActNotFrmCsum, Len.XZT13O_TK_ACT_NOT_FRM_CSUM),
				Len.XZT13O_TK_ACT_NOT_FRM_CSUM);
	}

	/**Original name: XZT13O-TK-ACT-NOT-FRM-CSUM<br>*/
	public int getXzt13oTkActNotFrmCsum() {
		return readNumDispUnsignedInt(Pos.XZT13O_TK_ACT_NOT_FRM_CSUM, Len.XZT13O_TK_ACT_NOT_FRM_CSUM);
	}

	public void setXzt13oCsrActNbr(String xzt13oCsrActNbr) {
		writeString(Pos.XZT13O_CSR_ACT_NBR, xzt13oCsrActNbr, Len.XZT13O_CSR_ACT_NBR);
	}

	/**Original name: XZT13O-CSR-ACT-NBR<br>*/
	public String getXzt13oCsrActNbr() {
		return readString(Pos.XZT13O_CSR_ACT_NBR, Len.XZT13O_CSR_ACT_NBR);
	}

	public void setXzt13oFrmNbr(String xzt13oFrmNbr) {
		writeString(Pos.XZT13O_FRM_NBR, xzt13oFrmNbr, Len.XZT13O_FRM_NBR);
	}

	/**Original name: XZT13O-FRM-NBR<br>*/
	public String getXzt13oFrmNbr() {
		return readString(Pos.XZT13O_FRM_NBR, Len.XZT13O_FRM_NBR);
	}

	public void setXzt13oFrmEdtDt(String xzt13oFrmEdtDt) {
		writeString(Pos.XZT13O_FRM_EDT_DT, xzt13oFrmEdtDt, Len.XZT13O_FRM_EDT_DT);
	}

	/**Original name: XZT13O-FRM-EDT-DT<br>*/
	public String getXzt13oFrmEdtDt() {
		return readString(Pos.XZT13O_FRM_EDT_DT, Len.XZT13O_FRM_EDT_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT013_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT13I_TECHNICAL_KEY = XZT013_SERVICE_INPUTS;
		public static final int XZT13I_TK_NOT_PRC_TS = XZT13I_TECHNICAL_KEY;
		public static final int XZT13I_TK_FRM_SEQ_NBR = XZT13I_TK_NOT_PRC_TS + Len.XZT13I_TK_NOT_PRC_TS;
		public static final int XZT13I_CSR_ACT_NBR = XZT13I_TK_FRM_SEQ_NBR + Len.XZT13I_TK_FRM_SEQ_NBR;
		public static final int XZT13I_USERID = XZT13I_CSR_ACT_NBR + Len.XZT13I_CSR_ACT_NBR;
		public static final int XZT013_SERVICE_OUTPUTS = XZT13I_USERID + Len.XZT13I_USERID;
		public static final int XZT13O_TECHNICAL_KEY = XZT013_SERVICE_OUTPUTS;
		public static final int XZT13O_TK_NOT_PRC_TS = XZT13O_TECHNICAL_KEY;
		public static final int XZT13O_TK_FRM_SEQ_NBR = XZT13O_TK_NOT_PRC_TS + Len.XZT13O_TK_NOT_PRC_TS;
		public static final int XZT13O_TK_ACT_NOT_FRM_CSUM = XZT13O_TK_FRM_SEQ_NBR + Len.XZT13O_TK_FRM_SEQ_NBR;
		public static final int XZT13O_CSR_ACT_NBR = XZT13O_TK_ACT_NOT_FRM_CSUM + Len.XZT13O_TK_ACT_NOT_FRM_CSUM;
		public static final int XZT13O_FRM_NBR = XZT13O_CSR_ACT_NBR + Len.XZT13O_CSR_ACT_NBR;
		public static final int XZT13O_FRM_EDT_DT = XZT13O_FRM_NBR + Len.XZT13O_FRM_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT13I_TK_NOT_PRC_TS = 26;
		public static final int XZT13I_TK_FRM_SEQ_NBR = 5;
		public static final int XZT13I_CSR_ACT_NBR = 9;
		public static final int XZT13I_USERID = 8;
		public static final int XZT13O_TK_NOT_PRC_TS = 26;
		public static final int XZT13O_TK_FRM_SEQ_NBR = 5;
		public static final int XZT13O_TK_ACT_NOT_FRM_CSUM = 9;
		public static final int XZT13O_CSR_ACT_NBR = 9;
		public static final int XZT13O_FRM_NBR = 30;
		public static final int XZT13I_TECHNICAL_KEY = XZT13I_TK_NOT_PRC_TS + XZT13I_TK_FRM_SEQ_NBR;
		public static final int XZT013_SERVICE_INPUTS = XZT13I_TECHNICAL_KEY + XZT13I_CSR_ACT_NBR + XZT13I_USERID;
		public static final int XZT13O_TECHNICAL_KEY = XZT13O_TK_NOT_PRC_TS + XZT13O_TK_FRM_SEQ_NBR + XZT13O_TK_ACT_NOT_FRM_CSUM;
		public static final int XZT13O_FRM_EDT_DT = 10;
		public static final int XZT013_SERVICE_OUTPUTS = XZT13O_TECHNICAL_KEY + XZT13O_CSR_ACT_NBR + XZT13O_FRM_NBR + XZT13O_FRM_EDT_DT;
		public static final int L_SERVICE_CONTRACT_AREA = XZT013_SERVICE_INPUTS + XZT013_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT13I_TK_FRM_SEQ_NBR = 5;
			public static final int XZT13O_TK_FRM_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
