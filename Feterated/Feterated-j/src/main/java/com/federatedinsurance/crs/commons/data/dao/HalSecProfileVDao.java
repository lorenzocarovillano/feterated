/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalSecProfileV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [HAL_SEC_PROFILE_V]
 * 
 */
public class HalSecProfileVDao extends BaseSqlDao<IHalSecProfileV> {

	public HalSecProfileVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalSecProfileV> getToClass() {
		return IHalSecProfileV.class;
	}

	public String selectRec(String hspIdKey, String secProfileTyp, String dft) {
		return buildQuery("selectRec").bind("hspIdKey", hspIdKey).bind("secProfileTyp", secProfileTyp).scalarResultString(dft);
	}
}
