/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WS-MONTH-LIT<br>
 * Variable: WS-MONTH-LIT from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsMonthLit extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int MONTH_ENTRY_MAXOCCURS = 12;

	//==== CONSTRUCTORS ====
	public WsMonthLit() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_MONTH_LIT;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "JANUARY", Len.LIT_JAN_ID);
		position += Len.LIT_JAN_ID;
		writeShort(position, ((short) 24), Len.Int.LIT_JAN_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_JAN_POS_AD;
		writeShort(position, ((short) 21), Len.Int.LIT_JAN_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_JAN_POS_AM;
		writeString(position, "FEBRUARY", Len.LIT_FEB_ID);
		position += Len.LIT_FEB_ID;
		writeShort(position, ((short) 25), Len.Int.LIT_FEB_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_FEB_POS_AD;
		writeShort(position, ((short) 22), Len.Int.LIT_FEB_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_FEB_POS_AM;
		writeString(position, "MARCH", Len.LIT_MAR_ID);
		position += Len.LIT_MAR_ID;
		writeShort(position, ((short) 22), Len.Int.LIT_MAR_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_MAR_POS_AD;
		writeShort(position, ((short) 19), Len.Int.LIT_MAR_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_MAR_POS_AM;
		writeString(position, "APRIL", Len.LIT_APR_ID);
		position += Len.LIT_APR_ID;
		writeShort(position, ((short) 22), Len.Int.LIT_APR_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_APR_POS_AD;
		writeShort(position, ((short) 19), Len.Int.LIT_APR_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_APR_POS_AM;
		writeString(position, "MAY", Len.LIT_MAY_ID);
		position += Len.LIT_MAY_ID;
		writeShort(position, ((short) 20), Len.Int.LIT_MAY_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_MAY_POS_AD;
		writeShort(position, ((short) 17), Len.Int.LIT_MAY_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_MAY_POS_AM;
		writeString(position, "JUNE", Len.LIT_JUN_ID);
		position += Len.LIT_JUN_ID;
		writeShort(position, ((short) 21), Len.Int.LIT_JUN_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_JUN_POS_AD;
		writeShort(position, ((short) 18), Len.Int.LIT_JUN_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_JUN_POS_AM;
		writeString(position, "JULY", Len.LIT_JUL_ID);
		position += Len.LIT_JUL_ID;
		writeShort(position, ((short) 21), Len.Int.LIT_JUL_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_JUL_POS_AD;
		writeShort(position, ((short) 18), Len.Int.LIT_JUL_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_JUL_POS_AM;
		writeString(position, "AUGUST", Len.LIT_AUG_ID);
		position += Len.LIT_AUG_ID;
		writeShort(position, ((short) 23), Len.Int.LIT_AUG_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_AUG_POS_AD;
		writeShort(position, ((short) 20), Len.Int.LIT_AUG_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_AUG_POS_AM;
		writeString(position, "SEPTEMBER", Len.LIT_SEP_ID);
		position += Len.LIT_SEP_ID;
		writeShort(position, ((short) 26), Len.Int.LIT_SEP_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_SEP_POS_AD;
		writeShort(position, ((short) 23), Len.Int.LIT_SEP_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_SEP_POS_AM;
		writeString(position, "OCTOBER", Len.LIT_OCT_ID);
		position += Len.LIT_OCT_ID;
		writeShort(position, ((short) 24), Len.Int.LIT_OCT_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_OCT_POS_AD;
		writeShort(position, ((short) 21), Len.Int.LIT_OCT_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_OCT_POS_AM;
		writeString(position, "NOVEMBER", Len.LIT_NOV_ID);
		position += Len.LIT_NOV_ID;
		writeShort(position, ((short) 25), Len.Int.LIT_NOV_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_NOV_POS_AD;
		writeShort(position, ((short) 22), Len.Int.LIT_NOV_POS_AM, SignType.NO_SIGN);
		position += Len.LIT_NOV_POS_AM;
		writeString(position, "DECEMBER", Len.LIT_DEC_ID);
		position += Len.LIT_DEC_ID;
		writeShort(position, ((short) 25), Len.Int.LIT_DEC_POS_AD, SignType.NO_SIGN);
		position += Len.LIT_DEC_POS_AD;
		writeShort(position, ((short) 22), Len.Int.LIT_DEC_POS_AM, SignType.NO_SIGN);
	}

	/**Original name: WS-LIT-ID<br>*/
	public String getLitId(int litIdIdx) {
		int position = Pos.wsLitId(litIdIdx - 1);
		return readString(position, Len.LIT_ID);
	}

	/**Original name: WS-LIT-POS-AD<br>*/
	public short getLitPosAd(int litPosAdIdx) {
		int position = Pos.wsLitPosAd(litPosAdIdx - 1);
		return readNumDispUnsignedShort(position, Len.LIT_POS_AD);
	}

	/**Original name: WS-LIT-POS-AM<br>*/
	public short getLitPosAm(int litPosAmIdx) {
		int position = Pos.wsLitPosAm(litPosAmIdx - 1);
		return readNumDispUnsignedShort(position, Len.LIT_POS_AM);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_MONTH_LIT = 1;
		public static final int LIT_JAN = WS_MONTH_LIT;
		public static final int LIT_JAN_ID = LIT_JAN;
		public static final int LIT_JAN_POS_AD = LIT_JAN_ID + Len.LIT_JAN_ID;
		public static final int LIT_JAN_POS_AM = LIT_JAN_POS_AD + Len.LIT_JAN_POS_AD;
		public static final int LIT_FEB = LIT_JAN_POS_AM + Len.LIT_JAN_POS_AM;
		public static final int LIT_FEB_ID = LIT_FEB;
		public static final int LIT_FEB_POS_AD = LIT_FEB_ID + Len.LIT_FEB_ID;
		public static final int LIT_FEB_POS_AM = LIT_FEB_POS_AD + Len.LIT_FEB_POS_AD;
		public static final int LIT_MAR = LIT_FEB_POS_AM + Len.LIT_FEB_POS_AM;
		public static final int LIT_MAR_ID = LIT_MAR;
		public static final int LIT_MAR_POS_AD = LIT_MAR_ID + Len.LIT_MAR_ID;
		public static final int LIT_MAR_POS_AM = LIT_MAR_POS_AD + Len.LIT_MAR_POS_AD;
		public static final int LIT_APR = LIT_MAR_POS_AM + Len.LIT_MAR_POS_AM;
		public static final int LIT_APR_ID = LIT_APR;
		public static final int LIT_APR_POS_AD = LIT_APR_ID + Len.LIT_APR_ID;
		public static final int LIT_APR_POS_AM = LIT_APR_POS_AD + Len.LIT_APR_POS_AD;
		public static final int LIT_MAY = LIT_APR_POS_AM + Len.LIT_APR_POS_AM;
		public static final int LIT_MAY_ID = LIT_MAY;
		public static final int LIT_MAY_POS_AD = LIT_MAY_ID + Len.LIT_MAY_ID;
		public static final int LIT_MAY_POS_AM = LIT_MAY_POS_AD + Len.LIT_MAY_POS_AD;
		public static final int LIT_JUN = LIT_MAY_POS_AM + Len.LIT_MAY_POS_AM;
		public static final int LIT_JUN_ID = LIT_JUN;
		public static final int LIT_JUN_POS_AD = LIT_JUN_ID + Len.LIT_JUN_ID;
		public static final int LIT_JUN_POS_AM = LIT_JUN_POS_AD + Len.LIT_JUN_POS_AD;
		public static final int LIT_JUL = LIT_JUN_POS_AM + Len.LIT_JUN_POS_AM;
		public static final int LIT_JUL_ID = LIT_JUL;
		public static final int LIT_JUL_POS_AD = LIT_JUL_ID + Len.LIT_JUL_ID;
		public static final int LIT_JUL_POS_AM = LIT_JUL_POS_AD + Len.LIT_JUL_POS_AD;
		public static final int LIT_AUG = LIT_JUL_POS_AM + Len.LIT_JUL_POS_AM;
		public static final int LIT_AUG_ID = LIT_AUG;
		public static final int LIT_AUG_POS_AD = LIT_AUG_ID + Len.LIT_AUG_ID;
		public static final int LIT_AUG_POS_AM = LIT_AUG_POS_AD + Len.LIT_AUG_POS_AD;
		public static final int LIT_SEP = LIT_AUG_POS_AM + Len.LIT_AUG_POS_AM;
		public static final int LIT_SEP_ID = LIT_SEP;
		public static final int LIT_SEP_POS_AD = LIT_SEP_ID + Len.LIT_SEP_ID;
		public static final int LIT_SEP_POS_AM = LIT_SEP_POS_AD + Len.LIT_SEP_POS_AD;
		public static final int LIT_OCT = LIT_SEP_POS_AM + Len.LIT_SEP_POS_AM;
		public static final int LIT_OCT_ID = LIT_OCT;
		public static final int LIT_OCT_POS_AD = LIT_OCT_ID + Len.LIT_OCT_ID;
		public static final int LIT_OCT_POS_AM = LIT_OCT_POS_AD + Len.LIT_OCT_POS_AD;
		public static final int LIT_NOV = LIT_OCT_POS_AM + Len.LIT_OCT_POS_AM;
		public static final int LIT_NOV_ID = LIT_NOV;
		public static final int LIT_NOV_POS_AD = LIT_NOV_ID + Len.LIT_NOV_ID;
		public static final int LIT_NOV_POS_AM = LIT_NOV_POS_AD + Len.LIT_NOV_POS_AD;
		public static final int LIT_DEC = LIT_NOV_POS_AM + Len.LIT_NOV_POS_AM;
		public static final int LIT_DEC_ID = LIT_DEC;
		public static final int LIT_DEC_POS_AD = LIT_DEC_ID + Len.LIT_DEC_ID;
		public static final int LIT_DEC_POS_AM = LIT_DEC_POS_AD + Len.LIT_DEC_POS_AD;
		public static final int WS_MONTH_LIT_RED = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int wsMonthEntry(int idx) {
			return WS_MONTH_LIT_RED + idx * Len.MONTH_ENTRY;
		}

		public static int wsLitId(int idx) {
			return wsMonthEntry(idx);
		}

		public static int wsLitPosAd(int idx) {
			return wsLitId(idx) + Len.LIT_ID;
		}

		public static int wsLitPosAm(int idx) {
			return wsLitPosAd(idx) + Len.LIT_POS_AD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int LIT_JAN_ID = 10;
		public static final int LIT_JAN_POS_AD = 2;
		public static final int LIT_JAN_POS_AM = 2;
		public static final int LIT_FEB_ID = 10;
		public static final int LIT_FEB_POS_AD = 2;
		public static final int LIT_FEB_POS_AM = 2;
		public static final int LIT_MAR_ID = 10;
		public static final int LIT_MAR_POS_AD = 2;
		public static final int LIT_MAR_POS_AM = 2;
		public static final int LIT_APR_ID = 10;
		public static final int LIT_APR_POS_AD = 2;
		public static final int LIT_APR_POS_AM = 2;
		public static final int LIT_MAY_ID = 10;
		public static final int LIT_MAY_POS_AD = 2;
		public static final int LIT_MAY_POS_AM = 2;
		public static final int LIT_JUN_ID = 10;
		public static final int LIT_JUN_POS_AD = 2;
		public static final int LIT_JUN_POS_AM = 2;
		public static final int LIT_JUL_ID = 10;
		public static final int LIT_JUL_POS_AD = 2;
		public static final int LIT_JUL_POS_AM = 2;
		public static final int LIT_AUG_ID = 10;
		public static final int LIT_AUG_POS_AD = 2;
		public static final int LIT_AUG_POS_AM = 2;
		public static final int LIT_SEP_ID = 10;
		public static final int LIT_SEP_POS_AD = 2;
		public static final int LIT_SEP_POS_AM = 2;
		public static final int LIT_OCT_ID = 10;
		public static final int LIT_OCT_POS_AD = 2;
		public static final int LIT_OCT_POS_AM = 2;
		public static final int LIT_NOV_ID = 10;
		public static final int LIT_NOV_POS_AD = 2;
		public static final int LIT_NOV_POS_AM = 2;
		public static final int LIT_DEC_ID = 10;
		public static final int LIT_DEC_POS_AD = 2;
		public static final int LIT_ID = 10;
		public static final int LIT_POS_AD = 2;
		public static final int LIT_POS_AM = 2;
		public static final int MONTH_ENTRY = LIT_ID + LIT_POS_AD + LIT_POS_AM;
		public static final int LIT_JAN = LIT_JAN_ID + LIT_JAN_POS_AD + LIT_JAN_POS_AM;
		public static final int LIT_FEB = LIT_FEB_ID + LIT_FEB_POS_AD + LIT_FEB_POS_AM;
		public static final int LIT_MAR = LIT_MAR_ID + LIT_MAR_POS_AD + LIT_MAR_POS_AM;
		public static final int LIT_APR = LIT_APR_ID + LIT_APR_POS_AD + LIT_APR_POS_AM;
		public static final int LIT_MAY = LIT_MAY_ID + LIT_MAY_POS_AD + LIT_MAY_POS_AM;
		public static final int LIT_JUN = LIT_JUN_ID + LIT_JUN_POS_AD + LIT_JUN_POS_AM;
		public static final int LIT_JUL = LIT_JUL_ID + LIT_JUL_POS_AD + LIT_JUL_POS_AM;
		public static final int LIT_AUG = LIT_AUG_ID + LIT_AUG_POS_AD + LIT_AUG_POS_AM;
		public static final int LIT_SEP = LIT_SEP_ID + LIT_SEP_POS_AD + LIT_SEP_POS_AM;
		public static final int LIT_OCT = LIT_OCT_ID + LIT_OCT_POS_AD + LIT_OCT_POS_AM;
		public static final int LIT_NOV = LIT_NOV_ID + LIT_NOV_POS_AD + LIT_NOV_POS_AM;
		public static final int LIT_DEC_POS_AM = 2;
		public static final int LIT_DEC = LIT_DEC_ID + LIT_DEC_POS_AD + LIT_DEC_POS_AM;
		public static final int WS_MONTH_LIT = LIT_JAN + LIT_FEB + LIT_MAR + LIT_APR + LIT_MAY + LIT_JUN + LIT_JUL + LIT_AUG + LIT_SEP + LIT_OCT
				+ LIT_NOV + LIT_DEC;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int LIT_JAN_POS_AD = 2;
			public static final int LIT_JAN_POS_AM = 2;
			public static final int LIT_FEB_POS_AD = 2;
			public static final int LIT_FEB_POS_AM = 2;
			public static final int LIT_MAR_POS_AD = 2;
			public static final int LIT_MAR_POS_AM = 2;
			public static final int LIT_APR_POS_AD = 2;
			public static final int LIT_APR_POS_AM = 2;
			public static final int LIT_MAY_POS_AD = 2;
			public static final int LIT_MAY_POS_AM = 2;
			public static final int LIT_JUN_POS_AD = 2;
			public static final int LIT_JUN_POS_AM = 2;
			public static final int LIT_JUL_POS_AD = 2;
			public static final int LIT_JUL_POS_AM = 2;
			public static final int LIT_AUG_POS_AD = 2;
			public static final int LIT_AUG_POS_AM = 2;
			public static final int LIT_SEP_POS_AD = 2;
			public static final int LIT_SEP_POS_AM = 2;
			public static final int LIT_OCT_POS_AD = 2;
			public static final int LIT_OCT_POS_AM = 2;
			public static final int LIT_NOV_POS_AD = 2;
			public static final int LIT_NOV_POS_AM = 2;
			public static final int LIT_DEC_POS_AD = 2;
			public static final int LIT_DEC_POS_AM = 2;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
