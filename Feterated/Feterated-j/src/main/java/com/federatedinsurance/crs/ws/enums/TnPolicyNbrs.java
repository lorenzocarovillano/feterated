/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TN-POLICY-NBRS<br>
 * Variable: TN-POLICY-NBRS from program XZ0P90E0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TnPolicyNbrs implements ICopyable<TnPolicyNbrs> {

	//==== PROPERTIES ====
	public static final String END_OF_POL_NBRS = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POLICY_NBRS);
	//Original name: TN-POLICY-NUMBER
	private String policyNumber = DefaultValues.stringVal(Len.POLICY_NUMBER);

	//==== CONSTRUCTORS ====
	public TnPolicyNbrs() {
	}

	public TnPolicyNbrs(TnPolicyNbrs policyNbrs) {
		this();
		this.policyNumber = policyNbrs.policyNumber;
	}

	//==== METHODS ====
	public String getPolicyNbrsFormatted() {
		return getPolicyNumberFormatted();
	}

	public byte[] getPolicyNbrsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, policyNumber, Len.POLICY_NUMBER);
		return buffer;
	}

	public void initPolicyNbrsHighValues() {
		policyNumber = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POLICY_NUMBER);
	}

	public boolean isEndOfPolNbrs() {
		return Functions.trimAfter(getPolicyNbrsFormatted()).equals(END_OF_POL_NBRS);
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = Functions.subString(policyNumber, Len.POLICY_NUMBER);
	}

	public String getPolicyNumber() {
		return this.policyNumber;
	}

	public String getPolicyNumberFormatted() {
		return Functions.padBlanks(getPolicyNumber(), Len.POLICY_NUMBER);
	}

	@Override
	public TnPolicyNbrs copy() {
		return new TnPolicyNbrs(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POLICY_NUMBER = 25;
		public static final int POLICY_NBRS = POLICY_NUMBER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
