/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPol1;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xzy800GetNotDayRqrRow;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B0004<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b0004Data implements IActNotPol1 {

	//==== PROPERTIES ====
	//Original name: CF-ACT-NOT-TYP-CD-IMP
	private String cfActNotTypCdImp = "IMP";
	//Original name: CF-GET-NOT-DAY-RQR-UTY-PGM
	private String cfGetNotDayRqrUtyPgm = "XZ0U8000";
	//Original name: SS-WNG-IDX
	private short ssWngIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short SS_WNG_IDX_MAX = ((short) 10);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b0004 workingStorageArea = new WorkingStorageAreaXz0b0004();
	//Original name: WS-XZ0A0004-ROW
	private WsXz0a0004Row wsXz0a0004Row = new WsXz0a0004Row();
	//Original name: XZY800-GET-NOT-DAY-RQR-ROW
	private Xzy800GetNotDayRqrRow xzy800GetNotDayRqrRow = new Xzy800GetNotDayRqrRow();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	@Override
	public void setCfActNotTypCdImp(String cfActNotTypCdImp) {
		this.cfActNotTypCdImp = Functions.subString(cfActNotTypCdImp, Len.CF_ACT_NOT_TYP_CD_IMP);
	}

	@Override
	public String getCfActNotTypCdImp() {
		return this.cfActNotTypCdImp;
	}

	public String getCfActNotTypCdImpFormatted() {
		return Functions.padBlanks(getCfActNotTypCdImp(), Len.CF_ACT_NOT_TYP_CD_IMP);
	}

	public String getCfGetNotDayRqrUtyPgm() {
		return this.cfGetNotDayRqrUtyPgm;
	}

	public void setSsWngIdx(short ssWngIdx) {
		this.ssWngIdx = ssWngIdx;
	}

	public short getSsWngIdx() {
		return this.ssWngIdx;
	}

	public boolean isSsWngIdxMax() {
		return ssWngIdx == SS_WNG_IDX_MAX;
	}

	public void setWsXz0y8000RowFormatted(String data) {
		byte[] buffer = new byte[Len.WS_XZ0Y8000_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0Y8000_ROW);
		setWsXz0y8000RowBytes(buffer, 1);
	}

	public String getWsXz0y8000RowFormatted() {
		return xzy800GetNotDayRqrRow.getXzy800GetNotDayRqrRowFormatted();
	}

	public void setWsXz0y8000RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xzy800GetNotDayRqrRow.setXzy800GetNotDayRqrRowBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getActNotStaCd() {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public String getActNotTypCd() {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public String getActOwnCltId() {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public String getCfActNotImpending() {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public void setCfActNotImpending(String cfActNotImpending) {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public String getCfActNotNonpay() {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public void setCfActNotNonpay(String cfActNotNonpay) {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public String getCfActNotRescind() {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public void setCfActNotRescind(String cfActNotRescind) {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public String getCsrActNbr() {
		return dclactNot.getCsrActNbr();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.dclactNot.setCsrActNbr(csrActNbr);
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	@Override
	public String getEmpId() {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public void setEmpId(String empId) {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotDt() {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public void setNotDt(String notDt) {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public String getNotEffDt() {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public String getNotPrcTs() {
		return dclactNot.getNotPrcTs();
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.dclactNot.setNotPrcTs(notPrcTs);
	}

	@Override
	public String getPolEffDt() {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public String getPolExpDt() {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public String getPolNbr() {
		return dclactNotPol.getPolNbr();
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.dclactNotPol.setPolNbr(polNbr);
	}

	@Override
	public String getPolTypCd() {
		throw new FieldNotMappedException("polTypCd");
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		throw new FieldNotMappedException("polTypCd");
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		return getTotFeeAmt().toString();
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b0004 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	@Override
	public short getWsNbrOfNotDay() {
		return workingStorageArea.getNbrOfNotDay();
	}

	@Override
	public void setWsNbrOfNotDay(short wsNbrOfNotDay) {
		this.workingStorageArea.setNbrOfNotDay(wsNbrOfNotDay);
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	@Override
	public String getWsPolicyCancDt() {
		return workingStorageArea.getPolicyCancDt();
	}

	@Override
	public void setWsPolicyCancDt(String wsPolicyCancDt) {
		this.workingStorageArea.setPolicyCancDt(wsPolicyCancDt);
	}

	public WsXz0a0004Row getWsXz0a0004Row() {
		return wsXz0a0004Row;
	}

	public Xzy800GetNotDayRqrRow getXzy800GetNotDayRqrRow() {
		return xzy800GetNotDayRqrRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CF_ACT_NOT_TYP_CD_IMP = 5;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_XZ0Y8000_ROW = Xzy800GetNotDayRqrRow.Len.XZY800_GET_NOT_DAY_RQR_ROW;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
