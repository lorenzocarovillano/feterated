/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-RETRY-CICS-CALL-FLAG<br>
 * Variable: SW-RETRY-CICS-CALL-FLAG from program TS547099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwRetryCicsCallFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char DO_NOT_RETRY_CICS_CALL = '0';
	public static final char RETRY_CICS_CALL = '1';

	//==== METHODS ====
	public void setRetryCicsCallFlag(char retryCicsCallFlag) {
		this.value = retryCicsCallFlag;
	}

	public char getRetryCicsCallFlag() {
		return this.value;
	}

	public void setDoNotRetryCicsCall() {
		value = DO_NOT_RETRY_CICS_CALL;
	}

	public boolean isRetryCicsCall() {
		return value == RETRY_CICS_CALL;
	}

	public void setRetryCicsCall() {
		value = RETRY_CICS_CALL;
	}
}
