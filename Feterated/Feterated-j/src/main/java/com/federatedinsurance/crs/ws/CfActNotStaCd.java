/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-ACT-NOT-STA-CD<br>
 * Variable: CF-ACT-NOT-STA-CD from program XZ0P90K0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfActNotStaCd {

	//==== PROPERTIES ====
	//Original name: CF-ACT-NOT-STATUS-STARTED
	private String started = "10";
	//Original name: CF-ACT-NOT-STATUS-PREPARED
	private String prepared = "20";
	//Original name: CF-ACT-NOT-STATUS-COMPLETE
	private String complete = "70";

	//==== METHODS ====
	public String getStarted() {
		return this.started;
	}

	public String getPrepared() {
		return this.prepared;
	}

	public String getComplete() {
		return this.complete;
	}
}
