/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WT-WC-PRC-MM-DD-YYYY<br>
 * Variable: WT-WC-PRC-MM-DD-YYYY from copybook XZC00401<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WtWcPrcMmDdYyyy {

	//==== PROPERTIES ====
	//Original name: WT-WC-PRC-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: FILLER-WT-WC-PRC-MM-DD-YYYY
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: WT-WC-PRC-DD
	private String dd = DefaultValues.stringVal(Len.DD);
	//Original name: FILLER-WT-WC-PRC-MM-DD-YYYY-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: WT-WC-PRC-CC
	private String cc = DefaultValues.stringVal(Len.CC);
	//Original name: WT-WC-PRC-YY
	private String yy = DefaultValues.stringVal(Len.YY);

	//==== METHODS ====
	public void setWcPrcMmDdYyyyBytes(byte[] buffer) {
		setWcPrcMmDdYyyyBytes(buffer, 1);
	}

	public void setWcPrcMmDdYyyyBytes(byte[] buffer, int offset) {
		int position = offset;
		mm = MarshalByte.readString(buffer, position, Len.MM);
		position += Len.MM;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dd = MarshalByte.readString(buffer, position, Len.DD);
		position += Len.DD;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		setYyyyBytes(buffer, position);
	}

	public byte[] getWcPrcMmDdYyyyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position += Len.MM;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		position += Len.DD;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		getYyyyBytes(buffer, position);
		return buffer;
	}

	public void initWcPrcMmDdYyyySpaces() {
		mm = "";
		flr1 = Types.SPACE_CHAR;
		dd = "";
		flr2 = Types.SPACE_CHAR;
		initYyyySpaces();
	}

	public void setMm(String mm) {
		this.mm = Functions.subString(mm, Len.MM);
	}

	public String getMm() {
		return this.mm;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setDd(String dd) {
		this.dd = Functions.subString(dd, Len.DD);
	}

	public String getDd() {
		return this.dd;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setYyyyBytes(byte[] buffer, int offset) {
		int position = offset;
		cc = MarshalByte.readString(buffer, position, Len.CC);
		position += Len.CC;
		yy = MarshalByte.readString(buffer, position, Len.YY);
	}

	public byte[] getYyyyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cc, Len.CC);
		position += Len.CC;
		MarshalByte.writeString(buffer, position, yy, Len.YY);
		return buffer;
	}

	public void initYyyySpaces() {
		cc = "";
		yy = "";
	}

	public void setCc(String cc) {
		this.cc = Functions.subString(cc, Len.CC);
	}

	public String getCc() {
		return this.cc;
	}

	public void setYy(String yy) {
		this.yy = Functions.subString(yy, Len.YY);
	}

	public String getYy() {
		return this.yy;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int CC = 2;
		public static final int YY = 2;
		public static final int FLR1 = 1;
		public static final int YYYY = CC + YY;
		public static final int WC_PRC_MM_DD_YYYY = MM + DD + YYYY + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
