/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZ0P90H0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXz0p90h0 {

	//==== PROPERTIES ====
	//Original name: EA-01-NOTHING-ADDED-MSG
	private Ea01NothingAddedMsgXz0p90h0 ea01NothingAddedMsg = new Ea01NothingAddedMsgXz0p90h0();
	//Original name: EA-02-NO-POLS-FOR-NOT-MSG
	private Ea02NoPolsForNotMsg ea02NoPolsForNotMsg = new Ea02NoPolsForNotMsg();
	//Original name: FILLER-EA-03-NO-PRINT-FOR-NOT-TYP-WNG
	private String flr1 = "Notification";
	//Original name: FILLER-EA-03-NO-PRINT-FOR-NOT-TYP-WNG-1
	private String flr2 = "type will not";
	//Original name: FILLER-EA-03-NO-PRINT-FOR-NOT-TYP-WNG-2
	private String flr3 = "produce print.";

	//==== METHODS ====
	public String getEa03NoPrintForNotTypWngFormatted() {
		return MarshalByteExt.bufferToStr(getEa03NoPrintForNotTypWngBytes());
	}

	/**Original name: EA-03-NO-PRINT-FOR-NOT-TYP-WNG<br>
	 * <pre> DO NOT CHANGE WORDING.  IT IS NEEDED BY CRS TO DETERMINE
	 *  SCREENS.</pre>*/
	public byte[] getEa03NoPrintForNotTypWngBytes() {
		byte[] buffer = new byte[Len.EA03_NO_PRINT_FOR_NOT_TYP_WNG];
		return getEa03NoPrintForNotTypWngBytes(buffer, 1);
	}

	public byte[] getEa03NoPrintForNotTypWngBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public Ea01NothingAddedMsgXz0p90h0 getEa01NothingAddedMsg() {
		return ea01NothingAddedMsg;
	}

	public Ea02NoPolsForNotMsg getEa02NoPolsForNotMsg() {
		return ea02NoPolsForNotMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 13;
		public static final int FLR2 = 14;
		public static final int FLR3 = 15;
		public static final int EA03_NO_PRINT_FOR_NOT_TYP_WNG = FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
