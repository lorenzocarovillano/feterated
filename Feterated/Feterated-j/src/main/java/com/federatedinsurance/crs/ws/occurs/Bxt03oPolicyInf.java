/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BXT03O-POLICY-INF<br>
 * Variables: BXT03O-POLICY-INF from copybook BX0T0003<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Bxt03oPolicyInf {

	//==== PROPERTIES ====
	//Original name: BXT03O-POLICY-NBR
	private String policyNbr = DefaultValues.stringVal(Len.POLICY_NBR);
	//Original name: BXT03O-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: BXT03O-PROP-CAN-DT
	private String propCanDt = DefaultValues.stringVal(Len.PROP_CAN_DT);
	//Original name: BXT03O-POL-AMT-DUE
	private AfDecimal polAmtDue = new AfDecimal(DefaultValues.DEC_VAL, 17, 2);
	//Original name: BXT03O-PCN-IND
	private char pcnInd = DefaultValues.CHAR_VAL;
	//Original name: BXT03O-POL-STATUS-CD
	private char polStatusCd = DefaultValues.CHAR_VAL;
	//Original name: BXT03O-POL-STATUS-DES
	private String polStatusDes = DefaultValues.stringVal(Len.POL_STATUS_DES);

	//==== METHODS ====
	public void setPolicyInfBytes(byte[] buffer, int offset) {
		int position = offset;
		policyNbr = MarshalByte.readString(buffer, position, Len.POLICY_NBR);
		position += Len.POLICY_NBR;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		propCanDt = MarshalByte.readString(buffer, position, Len.PROP_CAN_DT);
		position += Len.PROP_CAN_DT;
		polAmtDue.assign(MarshalByte.readDecimal(buffer, position, Len.Int.POL_AMT_DUE, Len.Fract.POL_AMT_DUE));
		position += Len.POL_AMT_DUE;
		pcnInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polStatusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polStatusDes = MarshalByte.readString(buffer, position, Len.POL_STATUS_DES);
	}

	public byte[] getPolicyInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, policyNbr, Len.POLICY_NBR);
		position += Len.POLICY_NBR;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, propCanDt, Len.PROP_CAN_DT);
		position += Len.PROP_CAN_DT;
		MarshalByte.writeDecimal(buffer, position, polAmtDue.copy());
		position += Len.POL_AMT_DUE;
		MarshalByte.writeChar(buffer, position, pcnInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polStatusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, polStatusDes, Len.POL_STATUS_DES);
		return buffer;
	}

	public void initPolicyInfSpaces() {
		policyNbr = "";
		polEffDt = "";
		propCanDt = "";
		polAmtDue.setNaN();
		pcnInd = Types.SPACE_CHAR;
		polStatusCd = Types.SPACE_CHAR;
		polStatusDes = "";
	}

	public void setPolicyNbr(String policyNbr) {
		this.policyNbr = Functions.subString(policyNbr, Len.POLICY_NBR);
	}

	public String getPolicyNbr() {
		return this.policyNbr;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPropCanDt(String propCanDt) {
		this.propCanDt = Functions.subString(propCanDt, Len.PROP_CAN_DT);
	}

	public String getPropCanDt() {
		return this.propCanDt;
	}

	public void setPolAmtDue(AfDecimal polAmtDue) {
		this.polAmtDue.assign(polAmtDue);
	}

	public AfDecimal getPolAmtDue() {
		return this.polAmtDue.copy();
	}

	public void setPcnInd(char pcnInd) {
		this.pcnInd = pcnInd;
	}

	public char getPcnInd() {
		return this.pcnInd;
	}

	public void setPolStatusCd(char polStatusCd) {
		this.polStatusCd = polStatusCd;
	}

	public char getPolStatusCd() {
		return this.polStatusCd;
	}

	public void setPolStatusDes(String polStatusDes) {
		this.polStatusDes = Functions.subString(polStatusDes, Len.POL_STATUS_DES);
	}

	public String getPolStatusDes() {
		return this.polStatusDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POLICY_NBR = 25;
		public static final int POL_EFF_DT = 10;
		public static final int PROP_CAN_DT = 10;
		public static final int POL_AMT_DUE = 17;
		public static final int PCN_IND = 1;
		public static final int POL_STATUS_CD = 1;
		public static final int POL_STATUS_DES = 22;
		public static final int POLICY_INF = POLICY_NBR + POL_EFF_DT + PROP_CAN_DT + POL_AMT_DUE + PCN_IND + POL_STATUS_CD + POL_STATUS_DES;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int POL_AMT_DUE = 15;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int POL_AMT_DUE = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
