/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WS-OPERATIONS-CALLED<br>
 * Variable: WS-OPERATIONS-CALLED from program XZ0P90K0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsOperationsCalledXz0p90k0 {

	//==== PROPERTIES ====
	//Original name: WS-ADD-ACT-NOT
	private String addActNot = "AddAccountNotification";
	//Original name: WS-GET-BILLING-DETAIL
	private String getBillingDetail = "GetBillDetailForAct";
	//Original name: WS-GET-INSURED-DETAIL
	private String getInsuredDetail = "GetInsuredDetail";
	//Original name: WS-PREPARE-INS-POL
	private String prepareInsPol = "PrepareInsuredPolicy";
	//Original name: WS-PREPARE-NOT
	private String prepareNot = "PrepareNotification";
	//Original name: WS-PREPARE-TTY
	private String prepareTty = "PrepareThirdPartyList";
	//Original name: WS-UPD-NOT-STA
	private String updNotSta = "UpdateNotificationStatus";

	//==== METHODS ====
	public String getAddActNot() {
		return this.addActNot;
	}

	public String getGetBillingDetail() {
		return this.getBillingDetail;
	}

	public String getGetInsuredDetail() {
		return this.getInsuredDetail;
	}

	public String getPrepareInsPol() {
		return this.prepareInsPol;
	}

	public String getPrepareNot() {
		return this.prepareNot;
	}

	public String getPrepareTty() {
		return this.prepareTty;
	}

	public String getUpdNotSta() {
		return this.updNotSta;
	}
}
