/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9040<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9040 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT94O_FRM_LIST_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9040() {
	}

	public LServiceContractAreaXz0x9040(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt94iPiStartPoint(long xzt94iPiStartPoint) {
		writeLong(Pos.XZT94I_PI_START_POINT, xzt94iPiStartPoint, Len.Int.XZT94I_PI_START_POINT);
	}

	/**Original name: XZT94I-PI-START-POINT<br>*/
	public long getXzt94iPiStartPoint() {
		return readNumDispLong(Pos.XZT94I_PI_START_POINT, Len.XZT94I_PI_START_POINT);
	}

	public void setXzt94iTkNotPrcTs(String xzt94iTkNotPrcTs) {
		writeString(Pos.XZT94I_TK_NOT_PRC_TS, xzt94iTkNotPrcTs, Len.XZT94I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT94I-TK-NOT-PRC-TS<br>*/
	public String getXzt94iTkNotPrcTs() {
		return readString(Pos.XZT94I_TK_NOT_PRC_TS, Len.XZT94I_TK_NOT_PRC_TS);
	}

	public void setXzt94iCsrActNbr(String xzt94iCsrActNbr) {
		writeString(Pos.XZT94I_CSR_ACT_NBR, xzt94iCsrActNbr, Len.XZT94I_CSR_ACT_NBR);
	}

	/**Original name: XZT94I-CSR-ACT-NBR<br>*/
	public String getXzt94iCsrActNbr() {
		return readString(Pos.XZT94I_CSR_ACT_NBR, Len.XZT94I_CSR_ACT_NBR);
	}

	public void setXzt94iUserid(String xzt94iUserid) {
		writeString(Pos.XZT94I_USERID, xzt94iUserid, Len.XZT94I_USERID);
	}

	/**Original name: XZT94I-USERID<br>*/
	public String getXzt94iUserid() {
		return readString(Pos.XZT94I_USERID, Len.XZT94I_USERID);
	}

	public String getXzt94iUseridFormatted() {
		return Functions.padBlanks(getXzt94iUserid(), Len.XZT94I_USERID);
	}

	public void setXzt94oPoStartPoint(long xzt94oPoStartPoint) {
		writeLong(Pos.XZT94O_PO_START_POINT, xzt94oPoStartPoint, Len.Int.XZT94O_PO_START_POINT);
	}

	/**Original name: XZT94O-PO-START-POINT<br>*/
	public long getXzt94oPoStartPoint() {
		return readNumDispLong(Pos.XZT94O_PO_START_POINT, Len.XZT94O_PO_START_POINT);
	}

	public void setXzt94oTkNotPrcTs(String xzt94oTkNotPrcTs) {
		writeString(Pos.XZT94O_TK_NOT_PRC_TS, xzt94oTkNotPrcTs, Len.XZT94O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT94O-TK-NOT-PRC-TS<br>*/
	public String getXzt94oTkNotPrcTs() {
		return readString(Pos.XZT94O_TK_NOT_PRC_TS, Len.XZT94O_TK_NOT_PRC_TS);
	}

	public void setXzt94oCsrActNbr(String xzt94oCsrActNbr) {
		writeString(Pos.XZT94O_CSR_ACT_NBR, xzt94oCsrActNbr, Len.XZT94O_CSR_ACT_NBR);
	}

	/**Original name: XZT94O-CSR-ACT-NBR<br>*/
	public String getXzt94oCsrActNbr() {
		return readString(Pos.XZT94O_CSR_ACT_NBR, Len.XZT94O_CSR_ACT_NBR);
	}

	public void setXzt94oTkFrmSeqNbr(int xzt94oTkFrmSeqNbrIdx, int xzt94oTkFrmSeqNbr) {
		int position = Pos.xzt94oTkFrmSeqNbr(xzt94oTkFrmSeqNbrIdx - 1);
		writeInt(position, xzt94oTkFrmSeqNbr, Len.Int.XZT94O_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT94O-TK-FRM-SEQ-NBR<br>*/
	public int getXzt94oTkFrmSeqNbr(int xzt94oTkFrmSeqNbrIdx) {
		int position = Pos.xzt94oTkFrmSeqNbr(xzt94oTkFrmSeqNbrIdx - 1);
		return readNumDispInt(position, Len.XZT94O_TK_FRM_SEQ_NBR);
	}

	public void setXzt94oFrmNbr(int xzt94oFrmNbrIdx, String xzt94oFrmNbr) {
		int position = Pos.xzt94oFrmNbr(xzt94oFrmNbrIdx - 1);
		writeString(position, xzt94oFrmNbr, Len.XZT94O_FRM_NBR);
	}

	/**Original name: XZT94O-FRM-NBR<br>*/
	public String getXzt94oFrmNbr(int xzt94oFrmNbrIdx) {
		int position = Pos.xzt94oFrmNbr(xzt94oFrmNbrIdx - 1);
		return readString(position, Len.XZT94O_FRM_NBR);
	}

	public void setXzt94oFrmEdtDt(int xzt94oFrmEdtDtIdx, String xzt94oFrmEdtDt) {
		int position = Pos.xzt94oFrmEdtDt(xzt94oFrmEdtDtIdx - 1);
		writeString(position, xzt94oFrmEdtDt, Len.XZT94O_FRM_EDT_DT);
	}

	/**Original name: XZT94O-FRM-EDT-DT<br>*/
	public String getXzt94oFrmEdtDt(int xzt94oFrmEdtDtIdx) {
		int position = Pos.xzt94oFrmEdtDt(xzt94oFrmEdtDtIdx - 1);
		return readString(position, Len.XZT94O_FRM_EDT_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT904_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT94I_PAGING_INPUTS = XZT904_SERVICE_INPUTS;
		public static final int XZT94I_PI_START_POINT = XZT94I_PAGING_INPUTS;
		public static final int XZT94I_TECHNICAL_KEY = XZT94I_PI_START_POINT + Len.XZT94I_PI_START_POINT;
		public static final int XZT94I_TK_NOT_PRC_TS = XZT94I_TECHNICAL_KEY;
		public static final int XZT94I_CSR_ACT_NBR = XZT94I_TK_NOT_PRC_TS + Len.XZT94I_TK_NOT_PRC_TS;
		public static final int XZT94I_USERID = XZT94I_CSR_ACT_NBR + Len.XZT94I_CSR_ACT_NBR;
		public static final int XZT904_SERVICE_OUTPUTS = XZT94I_USERID + Len.XZT94I_USERID;
		public static final int XZT94O_PAGING_OUTPUTS = XZT904_SERVICE_OUTPUTS;
		public static final int XZT94O_PO_START_POINT = XZT94O_PAGING_OUTPUTS;
		public static final int XZT94O_TECHNICAL_KEY = XZT94O_PO_START_POINT + Len.XZT94O_PO_START_POINT;
		public static final int XZT94O_TK_NOT_PRC_TS = XZT94O_TECHNICAL_KEY;
		public static final int XZT94O_CSR_ACT_NBR = XZT94O_TK_NOT_PRC_TS + Len.XZT94O_TK_NOT_PRC_TS;
		public static final int XZT94O_FRM_LIST_TBL = XZT94O_CSR_ACT_NBR + Len.XZT94O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt94oFrmList(int idx) {
			return XZT94O_FRM_LIST_TBL + idx * Len.XZT94O_FRM_LIST;
		}

		public static int xzt94oFrmTechnicalKey(int idx) {
			return xzt94oFrmList(idx);
		}

		public static int xzt94oTkFrmSeqNbr(int idx) {
			return xzt94oFrmTechnicalKey(idx);
		}

		public static int xzt94oFrmNbr(int idx) {
			return xzt94oTkFrmSeqNbr(idx) + Len.XZT94O_TK_FRM_SEQ_NBR;
		}

		public static int xzt94oFrmEdtDt(int idx) {
			return xzt94oFrmNbr(idx) + Len.XZT94O_FRM_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT94I_PI_START_POINT = 10;
		public static final int XZT94I_TK_NOT_PRC_TS = 26;
		public static final int XZT94I_CSR_ACT_NBR = 9;
		public static final int XZT94I_USERID = 8;
		public static final int XZT94O_PO_START_POINT = 10;
		public static final int XZT94O_TK_NOT_PRC_TS = 26;
		public static final int XZT94O_CSR_ACT_NBR = 9;
		public static final int XZT94O_TK_FRM_SEQ_NBR = 5;
		public static final int XZT94O_FRM_TECHNICAL_KEY = XZT94O_TK_FRM_SEQ_NBR;
		public static final int XZT94O_FRM_NBR = 30;
		public static final int XZT94O_FRM_EDT_DT = 10;
		public static final int XZT94O_FRM_LIST = XZT94O_FRM_TECHNICAL_KEY + XZT94O_FRM_NBR + XZT94O_FRM_EDT_DT;
		public static final int XZT94I_PAGING_INPUTS = XZT94I_PI_START_POINT;
		public static final int XZT94I_TECHNICAL_KEY = XZT94I_TK_NOT_PRC_TS;
		public static final int XZT904_SERVICE_INPUTS = XZT94I_PAGING_INPUTS + XZT94I_TECHNICAL_KEY + XZT94I_CSR_ACT_NBR + XZT94I_USERID;
		public static final int XZT94O_PAGING_OUTPUTS = XZT94O_PO_START_POINT;
		public static final int XZT94O_TECHNICAL_KEY = XZT94O_TK_NOT_PRC_TS;
		public static final int XZT94O_FRM_LIST_TBL = LServiceContractAreaXz0x9040.XZT94O_FRM_LIST_MAXOCCURS * XZT94O_FRM_LIST;
		public static final int XZT904_SERVICE_OUTPUTS = XZT94O_PAGING_OUTPUTS + XZT94O_TECHNICAL_KEY + XZT94O_CSR_ACT_NBR + XZT94O_FRM_LIST_TBL;
		public static final int L_SERVICE_CONTRACT_AREA = XZT904_SERVICE_INPUTS + XZT904_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT94I_PI_START_POINT = 10;
			public static final int XZT94O_PO_START_POINT = 10;
			public static final int XZT94O_TK_FRM_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
