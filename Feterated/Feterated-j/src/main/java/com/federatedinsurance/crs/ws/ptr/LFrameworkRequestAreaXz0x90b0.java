/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X90B0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x90b0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x90b0() {
	}

	public LFrameworkRequestAreaXz0x90b0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setCsrActNbr(String csrActNbr) {
		writeString(Pos.CSR_ACT_NBR, csrActNbr, Len.CSR_ACT_NBR);
	}

	/**Original name: XZA9B0Q-CSR-ACT-NBR<br>*/
	public String getCsrActNbr() {
		return readString(Pos.CSR_ACT_NBR, Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		writeString(Pos.NOT_PRC_TS, notPrcTs, Len.NOT_PRC_TS);
	}

	/**Original name: XZA9B0Q-NOT-PRC-TS<br>*/
	public String getNotPrcTs() {
		return readString(Pos.NOT_PRC_TS, Len.NOT_PRC_TS);
	}

	public void setActNotTypCd(String actNotTypCd) {
		writeString(Pos.ACT_NOT_TYP_CD, actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	/**Original name: XZA9B0Q-ACT-NOT-TYP-CD<br>*/
	public String getActNotTypCd() {
		return readString(Pos.ACT_NOT_TYP_CD, Len.ACT_NOT_TYP_CD);
	}

	public void setUserid(String userid) {
		writeString(Pos.USERID, userid, Len.USERID);
	}

	/**Original name: XZA9B0Q-USERID<br>*/
	public String getUserid() {
		return readString(Pos.USERID, Len.USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int GET_NOT_DAYS_RQR_HDR = L_FRAMEWORK_REQUEST_AREA;
		public static final int CSR_ACT_NBR = GET_NOT_DAYS_RQR_HDR;
		public static final int NOT_PRC_TS = CSR_ACT_NBR + Len.CSR_ACT_NBR;
		public static final int ACT_NOT_TYP_CD = NOT_PRC_TS + Len.NOT_PRC_TS;
		public static final int USERID = ACT_NOT_TYP_CD + Len.ACT_NOT_TYP_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int USERID = 8;
		public static final int GET_NOT_DAYS_RQR_HDR = CSR_ACT_NBR + NOT_PRC_TS + ACT_NOT_TYP_CD + USERID;
		public static final int L_FRAMEWORK_REQUEST_AREA = GET_NOT_DAYS_RQR_HDR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
