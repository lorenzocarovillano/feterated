/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ0Y90H1<br>
 * Variable: XZ0Y90H1 from copybook XZ0Y90H1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz0y90h1 {

	//==== PROPERTIES ====
	//Original name: XZY9H1-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZY9H1-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZY9H1-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZY9H1-NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);
	//Original name: XZY9H1-POL-DUE-AMT
	private AfDecimal polDueAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: XZY9H1-POL-BIL-STA-CD
	private char polBilStaCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void initXz0y90h1Spaces() {
		initPreparePolicyRowSpaces();
	}

	public void setPreparePolicyRowBytes(byte[] buffer, int offset) {
		int position = offset;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		polExpDt = MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		notEffDt = MarshalByte.readString(buffer, position, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		polDueAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.POL_DUE_AMT, Len.Fract.POL_DUE_AMT));
		position += Len.POL_DUE_AMT;
		polBilStaCd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getPreparePolicyRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeString(buffer, position, notEffDt, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		MarshalByte.writeDecimal(buffer, position, polDueAmt.copy());
		position += Len.POL_DUE_AMT;
		MarshalByte.writeChar(buffer, position, polBilStaCd);
		return buffer;
	}

	public void initPreparePolicyRowSpaces() {
		polNbr = "";
		polEffDt = "";
		polExpDt = "";
		notEffDt = "";
		polDueAmt.setNaN();
		polBilStaCd = Types.SPACE_CHAR;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	public String getNotEffDt() {
		return this.notEffDt;
	}

	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.polDueAmt.assign(polDueAmt);
	}

	public AfDecimal getPolDueAmt() {
		return this.polDueAmt.copy();
	}

	public void setPolBilStaCd(char polBilStaCd) {
		this.polBilStaCd = polBilStaCd;
	}

	public char getPolBilStaCd() {
		return this.polBilStaCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int NOT_EFF_DT = 10;
		public static final int POL_DUE_AMT = 10;
		public static final int POL_BIL_STA_CD = 1;
		public static final int PREPARE_POLICY_ROW = POL_NBR + POL_EFF_DT + POL_EXP_DT + NOT_EFF_DT + POL_DUE_AMT + POL_BIL_STA_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
