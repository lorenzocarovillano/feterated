/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ400000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz400000 {

	//==== PROPERTIES ====
	//Original name: CF-NI-NULL
	private short niNull = ((short) -1);
	//Original name: CF-SC-GOOD-RETURN
	private short scGoodReturn = ((short) 0);
	//Original name: CF-CN-ACT-NOT-PURGE-CSR
	private String cnActNotPurgeCsr = "ACT_NOT_PURGE_CSR";
	//Original name: CF-GET-ACT-NOT-SVC-INTF
	private String getActNotSvcIntf = "XZ0G0005";
	//Original name: CF-MTN-ACT-NOT-SVC-INTF
	private String mtnActNotSvcIntf = "XZ0G0006";
	//Original name: CF-CICS-TARGET-TRAN-ID
	private String cicsTargetTranId = "KXZ1";
	//Original name: CF-PROGRAM-NAME
	private String programName = "XZ400000";

	//==== METHODS ====
	public short getNiNull() {
		return this.niNull;
	}

	public short getScGoodReturn() {
		return this.scGoodReturn;
	}

	public String getCnActNotPurgeCsr() {
		return this.cnActNotPurgeCsr;
	}

	public String getGetActNotSvcIntf() {
		return this.getActNotSvcIntf;
	}

	public String getMtnActNotSvcIntf() {
		return this.mtnActNotSvcIntf;
	}

	public String getCicsTargetTranId() {
		return this.cicsTargetTranId;
	}

	public String getProgramName() {
		return this.programName;
	}
}
