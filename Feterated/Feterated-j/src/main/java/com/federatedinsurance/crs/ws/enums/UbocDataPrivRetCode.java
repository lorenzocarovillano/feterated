/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: UBOC-DATA-PRIV-RET-CODE<br>
 * Variable: UBOC-DATA-PRIV-RET-CODE from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocDataPrivRetCode {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.UBOC_DATA_PRIV_RET_CODE);
	public static final String DATA_PRIV_CHECK_OK = "";
	public static final String ROW_EXCLUDED = "DPR01";
	public static final String ROW_ALTERED = "DPR02";
	public static final String UNAUTHORIZED_ACTION = "DPR03";

	//==== METHODS ====
	public void setUbocDataPrivRetCode(String ubocDataPrivRetCode) {
		this.value = Functions.subString(ubocDataPrivRetCode, Len.UBOC_DATA_PRIV_RET_CODE);
	}

	public String getUbocDataPrivRetCode() {
		return this.value;
	}

	public boolean isDperDataPrivCheckOk() {
		return value.equals(DATA_PRIV_CHECK_OK);
	}

	public void setDataPrivCheckOk() {
		value = DATA_PRIV_CHECK_OK;
	}

	public boolean isRowExcluded() {
		return value.equals(ROW_EXCLUDED);
	}

	public void setRowExcluded() {
		value = ROW_EXCLUDED;
	}

	public void setRowAltered() {
		value = ROW_ALTERED;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_DATA_PRIV_RET_CODE = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
