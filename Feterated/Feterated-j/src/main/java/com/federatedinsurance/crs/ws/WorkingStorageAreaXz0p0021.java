/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IXz0p0021Generic;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P0021<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p0021 implements IXz0p0021Generic {

	//==== PROPERTIES ====
	//Original name: WS-BUS-LOC-ST
	private String busLocSt = DefaultValues.stringVal(Len.BUS_LOC_ST);
	//Original name: WS-NOT-DAY-RQR
	private short notDayRqr = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: WS-CPT-NOT-DT
	private String cptNotDt = DefaultValues.stringVal(Len.CPT_NOT_DT);
	//Original name: WS-CUR-DT
	private String curDt = DefaultValues.stringVal(Len.CUR_DT);
	//Original name: WS-RQR-NOT-DT
	private String rqrNotDt = DefaultValues.stringVal(Len.RQR_NOT_DT);
	//Original name: WS-RUN-DT
	private WsRunDt runDt = new WsRunDt();
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0P0021";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-DET-NOT-DT
	private String detNotDt = "determineNotificationDate";

	//==== METHODS ====
	public void setBusLocSt(String busLocSt) {
		this.busLocSt = Functions.subString(busLocSt, Len.BUS_LOC_ST);
	}

	public String getBusLocSt() {
		return this.busLocSt;
	}

	public void setNotDayRqr(short notDayRqr) {
		this.notDayRqr = notDayRqr;
	}

	public short getNotDayRqr() {
		return this.notDayRqr;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setCptNotDt(String cptNotDt) {
		this.cptNotDt = Functions.subString(cptNotDt, Len.CPT_NOT_DT);
	}

	public String getCptNotDt() {
		return this.cptNotDt;
	}

	public String getCptNotDtFormatted() {
		return Functions.padBlanks(getCptNotDt(), Len.CPT_NOT_DT);
	}

	public void setCurDt(String curDt) {
		this.curDt = Functions.subString(curDt, Len.CUR_DT);
	}

	public String getCurDt() {
		return this.curDt;
	}

	public String getCurDtFormatted() {
		return Functions.padBlanks(getCurDt(), Len.CUR_DT);
	}

	public void setRqrNotDt(String rqrNotDt) {
		this.rqrNotDt = Functions.subString(rqrNotDt, Len.RQR_NOT_DT);
	}

	public String getRqrNotDt() {
		return this.rqrNotDt;
	}

	public String getRqrNotDtFormatted() {
		return Functions.padBlanks(getRqrNotDt(), Len.RQR_NOT_DT);
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getDetNotDt() {
		return this.detNotDt;
	}

	public WsRunDt getRunDt() {
		return runDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUS_LOC_ST = 3;
		public static final int POL_EXP_DT = 10;
		public static final int CPT_NOT_DT = 10;
		public static final int CUR_DT = 10;
		public static final int RQR_NOT_DT = 10;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
