/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-ACT-CUR-ISO-DATE-TIME<br>
 * Variable: WS-ACT-CUR-ISO-DATE-TIME from program CAWS002<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsActCurIsoDateTime {

	//==== PROPERTIES ====
	//Original name: WS-ACT-CUR-ISO-DATE
	private String curIsoDate = DefaultValues.stringVal(Len.CUR_ISO_DATE);
	//Original name: WS-ACT-CUT-DT-TM-SEP
	private char cutDtTmSep = DefaultValues.CHAR_VAL;
	//Original name: WS-ACT-CUR-ISO-TIME
	private String curIsoTime = DefaultValues.stringVal(Len.CUR_ISO_TIME);

	//==== METHODS ====
	public void setWsActCurrentTimestamp(String wsActCurrentTimestamp) {
		int position = 1;
		byte[] buffer = getWsActCurIsoDateTimeBytes();
		MarshalByte.writeString(buffer, position, wsActCurrentTimestamp, Len.WS_ACT_CURRENT_TIMESTAMP);
		setWsActCurIsoDateTimeBytes(buffer);
	}

	/**Original name: WS-ACT-CURRENT-TIMESTAMP<br>*/
	public String getWsActCurrentTimestamp() {
		int position = 1;
		return MarshalByte.readString(getWsActCurIsoDateTimeBytes(), position, Len.WS_ACT_CURRENT_TIMESTAMP);
	}

	public void setWsActCurIsoDateTimeBytes(byte[] buffer) {
		setWsActCurIsoDateTimeBytes(buffer, 1);
	}

	/**Original name: WS-ACT-CUR-ISO-DATE-TIME<br>*/
	public byte[] getWsActCurIsoDateTimeBytes() {
		byte[] buffer = new byte[Len.WS_ACT_CUR_ISO_DATE_TIME];
		return getWsActCurIsoDateTimeBytes(buffer, 1);
	}

	public void setWsActCurIsoDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		curIsoDate = MarshalByte.readString(buffer, position, Len.CUR_ISO_DATE);
		position += Len.CUR_ISO_DATE;
		cutDtTmSep = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		curIsoTime = MarshalByte.readString(buffer, position, Len.CUR_ISO_TIME);
	}

	public byte[] getWsActCurIsoDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, curIsoDate, Len.CUR_ISO_DATE);
		position += Len.CUR_ISO_DATE;
		MarshalByte.writeChar(buffer, position, cutDtTmSep);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, curIsoTime, Len.CUR_ISO_TIME);
		return buffer;
	}

	public void setCurIsoDate(String curIsoDate) {
		this.curIsoDate = Functions.subString(curIsoDate, Len.CUR_ISO_DATE);
	}

	public String getCurIsoDate() {
		return this.curIsoDate;
	}

	public void setCutDtTmSep(char cutDtTmSep) {
		this.cutDtTmSep = cutDtTmSep;
	}

	public char getCutDtTmSep() {
		return this.cutDtTmSep;
	}

	public void setCurIsoTime(String curIsoTime) {
		this.curIsoTime = Functions.subString(curIsoTime, Len.CUR_ISO_TIME);
	}

	public String getCurIsoTime() {
		return this.curIsoTime;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CUR_ISO_DATE = 10;
		public static final int CUR_ISO_TIME = 15;
		public static final int CUT_DT_TM_SEP = 1;
		public static final int WS_ACT_CUR_ISO_DATE_TIME = CUR_ISO_DATE + CUT_DT_TM_SEP + CUR_ISO_TIME;
		public static final int WS_ACT_CURRENT_TIMESTAMP = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_ACT_CURRENT_TIMESTAMP = 26;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
