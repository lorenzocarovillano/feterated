/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TU-CERT-INFO<br>
 * Variables: TU-CERT-INFO from program XZ0P90C0<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TuCertInfo implements ICopyable<TuCertInfo> {

	//==== PROPERTIES ====
	//Original name: TU-CER-HLD-ID
	private String cerHldId = DefaultValues.stringVal(Len.CER_HLD_ID);
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ID);
	//Original name: TU-CER-NBR
	private String cerNbr = DefaultValues.stringVal(Len.CER_NBR);
	/**Original name: TU-CER-NBR-SORT<br>
	 * <pre>        Field needs to be justified to properly sort certificate
	 *         number.</pre>*/
	private String cerNbrSort = DefaultValues.stringVal(Len.CER_NBR_SORT);
	//Original name: TU-CER-HLD-NM
	private String cerHldNm = DefaultValues.stringVal(Len.CER_HLD_NM);
	//Original name: TU-CER-HLD-ADR-ID
	private String cerHldAdrId = DefaultValues.stringVal(Len.CER_HLD_ADR_ID);
	//Original name: TU-CER-HLD-ADR1
	private String cerHldAdr1 = DefaultValues.stringVal(Len.CER_HLD_ADR1);
	//Original name: TU-CER-HLD-ADR2
	private String cerHldAdr2 = DefaultValues.stringVal(Len.CER_HLD_ADR2);
	//Original name: TU-CER-HLD-CIT
	private String cerHldCit = DefaultValues.stringVal(Len.CER_HLD_CIT);
	//Original name: TU-CER-HLD-ST-ABB
	private String cerHldStAbb = DefaultValues.stringVal(Len.CER_HLD_ST_ABB);
	//Original name: TU-CER-HLD-PST-CD
	private String cerHldPstCd = DefaultValues.stringVal(Len.CER_HLD_PST_CD);

	//==== CONSTRUCTORS ====
	public TuCertInfo() {
	}

	public TuCertInfo(TuCertInfo tuCertInfo) {
		this();
		this.cerHldId = tuCertInfo.cerHldId;
		this.cerNbr = tuCertInfo.cerNbr;
		this.cerNbrSort = tuCertInfo.cerNbrSort;
		this.cerHldNm = tuCertInfo.cerHldNm;
		this.cerHldAdrId = tuCertInfo.cerHldAdrId;
		this.cerHldAdr1 = tuCertInfo.cerHldAdr1;
		this.cerHldAdr2 = tuCertInfo.cerHldAdr2;
		this.cerHldCit = tuCertInfo.cerHldCit;
		this.cerHldStAbb = tuCertInfo.cerHldStAbb;
		this.cerHldPstCd = tuCertInfo.cerHldPstCd;
	}

	//==== METHODS ====
	public TuCertInfo initTuCertInfoHighValues() {
		cerHldId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ID);
		cerNbr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_NBR);
		cerNbrSort = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_NBR_SORT);
		cerHldNm = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_NM);
		cerHldAdrId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ADR_ID);
		cerHldAdr1 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ADR1);
		cerHldAdr2 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ADR2);
		cerHldCit = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_CIT);
		cerHldStAbb = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_ST_ABB);
		cerHldPstCd = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_HLD_PST_CD);
		return this;
	}

	public void setCerHldId(String cerHldId) {
		this.cerHldId = Functions.subString(cerHldId, Len.CER_HLD_ID);
	}

	public String getCerHldId() {
		return this.cerHldId;
	}

	public boolean isEndOfTable() {
		return cerHldId.equals(END_OF_TABLE);
	}

	public void setCerNbr(String cerNbr) {
		this.cerNbr = Functions.subString(cerNbr, Len.CER_NBR);
	}

	public String getCerNbr() {
		return this.cerNbr;
	}

	public void setCerNbrSort(String cerNbrSort) {
		this.cerNbrSort = Functions.subString(cerNbrSort, Len.CER_NBR_SORT);
	}

	public String getCerNbrSort() {
		return this.cerNbrSort;
	}

	public void setCerHldNm(String cerHldNm) {
		this.cerHldNm = Functions.subString(cerHldNm, Len.CER_HLD_NM);
	}

	public String getCerHldNm() {
		return this.cerHldNm;
	}

	public void setCerHldAdrId(String cerHldAdrId) {
		this.cerHldAdrId = Functions.subString(cerHldAdrId, Len.CER_HLD_ADR_ID);
	}

	public String getCerHldAdrId() {
		return this.cerHldAdrId;
	}

	public void setCerHldAdr1(String cerHldAdr1) {
		this.cerHldAdr1 = Functions.subString(cerHldAdr1, Len.CER_HLD_ADR1);
	}

	public String getCerHldAdr1() {
		return this.cerHldAdr1;
	}

	public void setCerHldAdr2(String cerHldAdr2) {
		this.cerHldAdr2 = Functions.subString(cerHldAdr2, Len.CER_HLD_ADR2);
	}

	public String getCerHldAdr2() {
		return this.cerHldAdr2;
	}

	public void setCerHldCit(String cerHldCit) {
		this.cerHldCit = Functions.subString(cerHldCit, Len.CER_HLD_CIT);
	}

	public String getCerHldCit() {
		return this.cerHldCit;
	}

	public void setCerHldStAbb(String cerHldStAbb) {
		this.cerHldStAbb = Functions.subString(cerHldStAbb, Len.CER_HLD_ST_ABB);
	}

	public String getCerHldStAbb() {
		return this.cerHldStAbb;
	}

	public void setCerHldPstCd(String cerHldPstCd) {
		this.cerHldPstCd = Functions.subString(cerHldPstCd, Len.CER_HLD_PST_CD);
	}

	public String getCerHldPstCd() {
		return this.cerHldPstCd;
	}

	@Override
	public TuCertInfo copy() {
		return new TuCertInfo(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CER_HLD_ID = 64;
		public static final int CER_NBR = 25;
		public static final int CER_NBR_SORT = 25;
		public static final int CER_HLD_NM = 120;
		public static final int CER_HLD_ADR_ID = 64;
		public static final int CER_HLD_ADR1 = 45;
		public static final int CER_HLD_ADR2 = 45;
		public static final int CER_HLD_CIT = 30;
		public static final int CER_HLD_ST_ABB = 2;
		public static final int CER_HLD_PST_CD = 13;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
