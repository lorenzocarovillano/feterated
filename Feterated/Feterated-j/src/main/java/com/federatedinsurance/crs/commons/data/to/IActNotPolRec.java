/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ACT_NOT_POL_REC]
 * 
 */
public interface IActNotPolRec extends BaseSqlTo {

	/**
	 * Host Variable XZH008-CSR-ACT-NBR
	 * 
	 */
	String getCsrActNbr();

	void setCsrActNbr(String csrActNbr);

	/**
	 * Host Variable XZH008-NOT-PRC-TS
	 * 
	 */
	String getNotPrcTs();

	void setNotPrcTs(String notPrcTs);

	/**
	 * Host Variable XZH008-POL-NBR
	 * 
	 */
	String getPolNbr();

	void setPolNbr(String polNbr);

	/**
	 * Host Variable XZH008-REC-SEQ-NBR
	 * 
	 */
	short getRecSeqNbr();

	void setRecSeqNbr(short recSeqNbr);
};
