/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsMu0x0004 {

	//==== PROPERTIES ====
	//Original name: CF-BLANK
	private char blank = Types.SPACE_CHAR;
	//Original name: CF-POSITIVE
	private char positive = '+';
	//Original name: CF-DASH
	private char dash = '-';
	//Original name: CF-YES
	private char yes = 'Y';
	//Original name: CF-NO
	private char no = 'N';
	//Original name: CF-NAME-DELIMITER
	private String nameDelimiter = "";
	//Original name: CF-SORT-DELIMITER
	private String sortDelimiter = ",";
	//Original name: CF-DSP-DELIMITER
	private char dspDelimiter = '@';
	//Original name: CF-SPACE-DSP-DELIMITER
	private String spaceDspDelimiter = " @";
	//Original name: CF-INDIVIDUAL
	private String individual = "IN";
	//Original name: CF-PC-SIC
	private String pcSic = "SIC";
	//Original name: CF-PC-TOB
	private String pcTob = "TOB";
	//Original name: CF-PRIMARY
	private String primary = "PR";
	//Original name: CF-AC-BSM
	private String acBsm = "BSM";
	//Original name: CF-AC-BSL
	private String acBsl = "BSL";
	//Original name: CF-PHONE-NBR-CODE
	private CfPhoneNbrCode phoneNbrCode = new CfPhoneNbrCode();
	//Original name: CF-TT-FEIN
	private String ttFein = "FED";
	//Original name: CF-TT-SSN
	private String ttSsn = "SSN";
	//Original name: CF-ACT-NBR-CODE
	private CfActNbrCode actNbrCode = new CfActNbrCode();
	//Original name: CF-MARKETING-CODE
	private CfMarketingCode marketingCode = new CfMarketingCode();
	//Original name: CF-MA-AGC-CD
	private char maAgcCd = 'A';
	//Original name: CF-MA-MUT-CD
	private char maMutCd = 'M';
	//Original name: CF-MA-AGC-CD-DES
	private String maAgcCdDes = "AGENCY";
	//Original name: CF-MA-MUT-CD-DES
	private String maMutCdDes = "MUTUAL";
	//Original name: CF-COOWNER-LOOKUP
	private String coownerLookup = "04";
	//Original name: CF-COL-IS-NULL
	private char colIsNull = 'Y';
	//Original name: CF-MAIN-DRIVER
	private String mainDriver = "TS020000";
	//Original name: CF-UNIT-OF-WORK
	private String unitOfWork = "GET_CLIENT_DETAIL";
	//Original name: CF-2ND-UNIT-OF-WORK
	private String cf2ndUnitOfWork = "GET_CLIENT_DETAIL_NO_SWAP";
	//Original name: CF-PROGRAM-NAME
	private String programName = "MU0X0004";
	//Original name: CF-REQUEST-MODULE
	private String requestModule = "MU0Q0004";
	//Original name: CF-RESPONSE-MODULE
	private String responseModule = "MU0R0004";
	//Original name: CF-COPYBOOK-NAMES
	private CfCopybookNames copybookNames = new CfCopybookNames();

	//==== METHODS ====
	public char getBlank() {
		return this.blank;
	}

	public char getPositive() {
		return this.positive;
	}

	public char getDash() {
		return this.dash;
	}

	public char getYes() {
		return this.yes;
	}

	public char getNo() {
		return this.no;
	}

	public String getNameDelimiter() {
		return this.nameDelimiter;
	}

	public String getNameDelimiterFormatted() {
		return Functions.padBlanks(getNameDelimiter(), Len.NAME_DELIMITER);
	}

	public String getSortDelimiter() {
		return this.sortDelimiter;
	}

	public char getDspDelimiter() {
		return this.dspDelimiter;
	}

	public String getSpaceDspDelimiter() {
		return this.spaceDspDelimiter;
	}

	public String getIndividual() {
		return this.individual;
	}

	public String getPcSic() {
		return this.pcSic;
	}

	public String getPcTob() {
		return this.pcTob;
	}

	public String getPrimary() {
		return this.primary;
	}

	public String getAcBsm() {
		return this.acBsm;
	}

	public String getAcBsl() {
		return this.acBsl;
	}

	public String getTtFein() {
		return this.ttFein;
	}

	public String getTtSsn() {
		return this.ttSsn;
	}

	public char getMaAgcCd() {
		return this.maAgcCd;
	}

	public char getMaMutCd() {
		return this.maMutCd;
	}

	public String getMaAgcCdDes() {
		return this.maAgcCdDes;
	}

	public String getMaMutCdDes() {
		return this.maMutCdDes;
	}

	public String getCoownerLookup() {
		return this.coownerLookup;
	}

	public char getColIsNull() {
		return this.colIsNull;
	}

	public String getMainDriver() {
		return this.mainDriver;
	}

	public String getUnitOfWork() {
		return this.unitOfWork;
	}

	public String getCf2ndUnitOfWork() {
		return this.cf2ndUnitOfWork;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getRequestModule() {
		return this.requestModule;
	}

	public String getResponseModule() {
		return this.responseModule;
	}

	public CfActNbrCode getActNbrCode() {
		return actNbrCode;
	}

	public CfCopybookNames getCopybookNames() {
		return copybookNames;
	}

	public CfMarketingCode getMarketingCode() {
		return marketingCode;
	}

	public CfPhoneNbrCode getPhoneNbrCode() {
		return phoneNbrCode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NAME_DELIMITER = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
