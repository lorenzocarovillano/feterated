/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-WORK-MSECS-RED<br>
 * Variable: WS-WORK-MSECS-RED from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsWorkMsecsRed {

	//==== PROPERTIES ====
	//Original name: WS-WORK-MSECS-AMPM
	private String msecsAmpm = DefaultValues.stringVal(Len.MSECS_AMPM);
	//Original name: FILLER-WS-WORK-MSECS-RED
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setWsWorkMsecs(int wsWorkMsecs) {
		int position = 1;
		byte[] buffer = getWsWorkMsecsRedBytes();
		MarshalByte.writeInt(buffer, position, wsWorkMsecs, Len.Int.WS_WORK_MSECS, SignType.NO_SIGN);
		setWsWorkMsecsRedBytes(buffer);
	}

	public void setWsWorkMsecsFormatted(String wsWorkMsecs) {
		int position = 1;
		byte[] buffer = getWsWorkMsecsRedBytes();
		MarshalByte.writeString(buffer, position, Trunc.toNumeric(wsWorkMsecs, Len.WS_WORK_MSECS), Len.WS_WORK_MSECS);
		setWsWorkMsecsRedBytes(buffer);
	}

	/**Original name: WS-WORK-MSECS<br>*/
	public int getWsWorkMsecs() {
		int position = 1;
		return MarshalByte.readInt(getWsWorkMsecsRedBytes(), position, Len.Int.WS_WORK_MSECS, SignType.NO_SIGN);
	}

	public String getWsWorkMsecsFormatted() {
		int position = 1;
		return MarshalByte.readFixedString(getWsWorkMsecsRedBytes(), position, Len.WS_WORK_MSECS);
	}

	public void setWsWorkMsecsRedFormatted(String data) {
		byte[] buffer = new byte[Len.WS_WORK_MSECS_RED];
		MarshalByte.writeString(buffer, 1, data, Len.WS_WORK_MSECS_RED);
		setWsWorkMsecsRedBytes(buffer, 1);
	}

	public void setWsWorkMsecsRedBytes(byte[] buffer) {
		setWsWorkMsecsRedBytes(buffer, 1);
	}

	/**Original name: WS-WORK-MSECS-RED<br>*/
	public byte[] getWsWorkMsecsRedBytes() {
		byte[] buffer = new byte[Len.WS_WORK_MSECS_RED];
		return getWsWorkMsecsRedBytes(buffer, 1);
	}

	public void setWsWorkMsecsRedBytes(byte[] buffer, int offset) {
		int position = offset;
		msecsAmpm = MarshalByte.readString(buffer, position, Len.MSECS_AMPM);
		position += Len.MSECS_AMPM;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getWsWorkMsecsRedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, msecsAmpm, Len.MSECS_AMPM);
		position += Len.MSECS_AMPM;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void initWsWorkMsecsRedZeroes() {
		msecsAmpm = "00";
		flr1 = "0000";
	}

	public void initWsWorkMsecsRedSpaces() {
		msecsAmpm = "";
		flr1 = "";
	}

	public void setMsecsAmpm(String msecsAmpm) {
		this.msecsAmpm = Functions.subString(msecsAmpm, Len.MSECS_AMPM);
	}

	public String getMsecsAmpm() {
		return this.msecsAmpm;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MSECS_AMPM = 2;
		public static final int FLR1 = 4;
		public static final int WS_WORK_MSECS_RED = MSECS_AMPM + FLR1;
		public static final int WS_WORK_MSECS = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_WORK_MSECS = 6;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
