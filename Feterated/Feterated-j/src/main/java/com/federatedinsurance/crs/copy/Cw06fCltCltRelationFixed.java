/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CW06F-CLT-CLT-RELATION-FIXED<br>
 * Variable: CW06F-CLT-CLT-RELATION-FIXED from copybook CAWLF006<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw06fCltCltRelationFixed {

	//==== PROPERTIES ====
	//Original name: CW06F-CLT-CLT-RELATION-CSUM
	private String cltCltRelationCsum = DefaultValues.stringVal(Len.CLT_CLT_RELATION_CSUM);
	//Original name: CW06F-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CW06F-CLT-TYP-CD-KCRE
	private String cltTypCdKcre = DefaultValues.stringVal(Len.CLT_TYP_CD_KCRE);
	//Original name: CW06F-HISTORY-VLD-NBR-KCRE
	private String historyVldNbrKcre = DefaultValues.stringVal(Len.HISTORY_VLD_NBR_KCRE);
	//Original name: CW06F-CICR-XRF-ID-KCRE
	private String cicrXrfIdKcre = DefaultValues.stringVal(Len.CICR_XRF_ID_KCRE);
	//Original name: CW06F-XRF-TYP-CD-KCRE
	private String xrfTypCdKcre = DefaultValues.stringVal(Len.XRF_TYP_CD_KCRE);
	//Original name: CW06F-CICR-EFF-DT-KCRE
	private String cicrEffDtKcre = DefaultValues.stringVal(Len.CICR_EFF_DT_KCRE);

	//==== METHODS ====
	public void setCltCltRelationFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		cltCltRelationCsum = MarshalByte.readFixedString(buffer, position, Len.CLT_CLT_RELATION_CSUM);
		position += Len.CLT_CLT_RELATION_CSUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		cltTypCdKcre = MarshalByte.readString(buffer, position, Len.CLT_TYP_CD_KCRE);
		position += Len.CLT_TYP_CD_KCRE;
		historyVldNbrKcre = MarshalByte.readString(buffer, position, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		cicrXrfIdKcre = MarshalByte.readString(buffer, position, Len.CICR_XRF_ID_KCRE);
		position += Len.CICR_XRF_ID_KCRE;
		xrfTypCdKcre = MarshalByte.readString(buffer, position, Len.XRF_TYP_CD_KCRE);
		position += Len.XRF_TYP_CD_KCRE;
		cicrEffDtKcre = MarshalByte.readString(buffer, position, Len.CICR_EFF_DT_KCRE);
	}

	public byte[] getCltCltRelationFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cltCltRelationCsum, Len.CLT_CLT_RELATION_CSUM);
		position += Len.CLT_CLT_RELATION_CSUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		MarshalByte.writeString(buffer, position, cltTypCdKcre, Len.CLT_TYP_CD_KCRE);
		position += Len.CLT_TYP_CD_KCRE;
		MarshalByte.writeString(buffer, position, historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		MarshalByte.writeString(buffer, position, cicrXrfIdKcre, Len.CICR_XRF_ID_KCRE);
		position += Len.CICR_XRF_ID_KCRE;
		MarshalByte.writeString(buffer, position, xrfTypCdKcre, Len.XRF_TYP_CD_KCRE);
		position += Len.XRF_TYP_CD_KCRE;
		MarshalByte.writeString(buffer, position, cicrEffDtKcre, Len.CICR_EFF_DT_KCRE);
		return buffer;
	}

	public void initCltObjRelationFixedSpaces() {
		cltCltRelationCsum = "";
		clientIdKcre = "";
		cltTypCdKcre = "";
		historyVldNbrKcre = "";
		cicrXrfIdKcre = "";
		xrfTypCdKcre = "";
		cicrEffDtKcre = "";
	}

	public void setCltObjRelationCsumFormatted(String cltObjRelationCsum) {
		this.cltCltRelationCsum = Trunc.toUnsignedNumeric(cltObjRelationCsum, Len.CLT_CLT_RELATION_CSUM);
	}

	public int getCltObjRelationCsum() {
		return NumericDisplay.asInt(this.cltCltRelationCsum);
	}

	public String getCltObjRelationCsumFormatted() {
		return this.cltCltRelationCsum;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setCltTypCdKcre(String cltTypCdKcre) {
		this.cltTypCdKcre = Functions.subString(cltTypCdKcre, Len.CLT_TYP_CD_KCRE);
	}

	public String getCltTypCdKcre() {
		return this.cltTypCdKcre;
	}

	public void setHistoryVldNbrKcre(String historyVldNbrKcre) {
		this.historyVldNbrKcre = Functions.subString(historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
	}

	public String getHistoryVldNbrKcre() {
		return this.historyVldNbrKcre;
	}

	public void setCicrXrfIdKcre(String cicrXrfIdKcre) {
		this.cicrXrfIdKcre = Functions.subString(cicrXrfIdKcre, Len.CICR_XRF_ID_KCRE);
	}

	public String getCicrXrfIdKcre() {
		return this.cicrXrfIdKcre;
	}

	public void setXrfTypCdKcre(String xrfTypCdKcre) {
		this.xrfTypCdKcre = Functions.subString(xrfTypCdKcre, Len.XRF_TYP_CD_KCRE);
	}

	public String getXrfTypCdKcre() {
		return this.xrfTypCdKcre;
	}

	public void setCicrEffDtKcre(String cicrEffDtKcre) {
		this.cicrEffDtKcre = Functions.subString(cicrEffDtKcre, Len.CICR_EFF_DT_KCRE);
	}

	public String getCicrEffDtKcre() {
		return this.cicrEffDtKcre;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLT_CLT_RELATION_CSUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int CLT_TYP_CD_KCRE = 32;
		public static final int HISTORY_VLD_NBR_KCRE = 32;
		public static final int CICR_XRF_ID_KCRE = 32;
		public static final int XRF_TYP_CD_KCRE = 32;
		public static final int CICR_EFF_DT_KCRE = 32;
		public static final int CLT_CLT_RELATION_FIXED = CLT_CLT_RELATION_CSUM + CLIENT_ID_KCRE + CLT_TYP_CD_KCRE + HISTORY_VLD_NBR_KCRE
				+ CICR_XRF_ID_KCRE + XRF_TYP_CD_KCRE + CICR_EFF_DT_KCRE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
