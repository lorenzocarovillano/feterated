/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-SPECIFIC-WORK-AREAS<br>
 * Variable: WS-SPECIFIC-WORK-AREAS from program TS020100<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSpecificWorkAreas {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "TS020100";
	//Original name: WS-MAX-REC-SEQ
	private String maxRecSeq = DefaultValues.stringVal(Len.MAX_REC_SEQ);

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public void setMaxRecSeq(int maxRecSeq) {
		this.maxRecSeq = NumericDisplay.asString(maxRecSeq, Len.MAX_REC_SEQ);
	}

	public void setMaxRecSeqFormatted(String maxRecSeq) {
		this.maxRecSeq = Trunc.toUnsignedNumeric(maxRecSeq, Len.MAX_REC_SEQ);
	}

	public int getMaxRecSeq() {
		return NumericDisplay.asInt(this.maxRecSeq);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_REC_SEQ = 5;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
