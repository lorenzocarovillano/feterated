/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotPol1;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [ACT_NOT, ACT_NOT_POL]
 * 
 */
public class ActNotPol1Dao extends BaseSqlDao<IActNotPol1> {

	private Cursor polLstByAdrCsr;
	private Cursor polLstByOwnCsr;
	private final IRowMapper<IActNotPol1> fetchPolLstByAdrCsrRm = buildNamedRowMapper(IActNotPol1.class, "polNbr", "polTypCd", "totFeeAmtObj");

	public ActNotPol1Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotPol1> getToClass() {
		return IActNotPol1.class;
	}

	public String selectRec(IActNotPol1 iActNotPol1, String dft) {
		return buildQuery("selectRec").bind(iActNotPol1).scalarResultString(dft);
	}

	public DbAccessStatus openPolLstByAdrCsr(String csrActNbr, String notPrcTs) {
		polLstByAdrCsr = buildQuery("openPolLstByAdrCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public IActNotPol1 fetchPolLstByAdrCsr(IActNotPol1 iActNotPol1) {
		return fetch(polLstByAdrCsr, iActNotPol1, fetchPolLstByAdrCsrRm);
	}

	public DbAccessStatus closePolLstByAdrCsr() {
		return closeCursor(polLstByAdrCsr);
	}

	public DbAccessStatus openPolLstByOwnCsr(String csrActNbr, String notPrcTs) {
		polLstByOwnCsr = buildQuery("openPolLstByOwnCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public IActNotPol1 fetchPolLstByOwnCsr(IActNotPol1 iActNotPol1) {
		return fetch(polLstByOwnCsr, iActNotPol1, fetchPolLstByAdrCsrRm);
	}

	public DbAccessStatus closePolLstByOwnCsr() {
		return closeCursor(polLstByOwnCsr);
	}

	public short selectRec1(String csrActNbr, String saCurrentDt, String saOneYearAgoDt, short dft) {
		return buildQuery("selectRec1").bind("csrActNbr", csrActNbr).bind("saCurrentDt", saCurrentDt).bind("saOneYearAgoDt", saOneYearAgoDt)
				.scalarResultShort(dft);
	}

	public String selectRec2(IActNotPol1 iActNotPol1, String dft) {
		return buildQuery("selectRec2").bind(iActNotPol1).scalarResultString(dft);
	}

	public IActNotPol1 selectRec3(IActNotPol1 iActNotPol1) {
		return buildQuery("selectRec3").bind(iActNotPol1).singleResult(iActNotPol1);
	}

	public IActNotPol1 selectRec4(String polNbr, String polEffDt, String polExpDt, String csrActNbr, IActNotPol1 iActNotPol1) {
		return buildQuery("selectRec4").bind("polNbr", polNbr).bind("polEffDt", polEffDt).bind("polExpDt", polExpDt).bind("csrActNbr", csrActNbr)
				.singleResult(iActNotPol1);
	}
}
