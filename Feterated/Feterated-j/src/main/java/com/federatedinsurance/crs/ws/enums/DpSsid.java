/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DP-SSID<br>
 * Variable: DP-SSID from program TS030299<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class DpSsid extends SerializableParameter {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.SSID);
	public static final String PROD_DB2 = "DBP1";
	public static final String DEV_DB2 = "DBD1";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.SSID;
	}

	@Override
	public void deserialize(byte[] buf) {
		setSsidFromBuffer(buf);
	}

	public void setSsid(String ssid) {
		this.value = Functions.subString(ssid, Len.SSID);
	}

	public void setSsidFromBuffer(byte[] buffer) {
		value = MarshalByte.readString(buffer, 1, Len.SSID);
	}

	public String getSsid() {
		return this.value;
	}

	public String getSsidFormatted() {
		return Functions.padBlanks(getSsid(), Len.SSID);
	}

	public void setProdDb2() {
		value = PROD_DB2;
	}

	public void setDevDb2() {
		value = DEV_DB2;
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.strToBuffer(getSsid(), Len.SSID);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SSID = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int SSID = 4;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int SSID = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
