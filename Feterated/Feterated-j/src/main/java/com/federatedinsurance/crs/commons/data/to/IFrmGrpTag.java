/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [FRM_GRP, FRM_TAG]
 * 
 */
public interface IFrmGrpTag extends BaseSqlTo {

	/**
	 * Host Variable EDL-FRM-NM
	 * 
	 */
	String getEdlFrmNm();

	void setEdlFrmNm(String edlFrmNm);

	/**
	 * Host Variable DTA-GRP-NM
	 * 
	 */
	String getDtaGrpNm();

	void setDtaGrpNm(String dtaGrpNm);

	/**
	 * Host Variable DTA-GRP-FLD-NBR
	 * 
	 */
	short getDtaGrpFldNbr();

	void setDtaGrpFldNbr(short dtaGrpFldNbr);

	/**
	 * Host Variable TAG-NM
	 * 
	 */
	String getTagNm();

	void setTagNm(String tagNm);

	/**
	 * Host Variable JUS-CD
	 * 
	 */
	char getJusCd();

	void setJusCd(char jusCd);

	/**
	 * Host Variable SPE-PRC-CD
	 * 
	 */
	String getSpePrcCd();

	void setSpePrcCd(String spePrcCd);

	/**
	 * Nullable property for SPE-PRC-CD
	 * 
	 */
	String getSpePrcCdObj();

	void setSpePrcCdObj(String spePrcCdObj);

	/**
	 * Host Variable LEN-NBR
	 * 
	 */
	short getLenNbr();

	void setLenNbr(short lenNbr);

	/**
	 * Host Variable TAG-OCC-CNT
	 * 
	 */
	short getTagOccCnt();

	void setTagOccCnt(short tagOccCnt);

	/**
	 * Host Variable DEL-IND
	 * 
	 */
	char getDelInd();

	void setDelInd(char delInd);
};
