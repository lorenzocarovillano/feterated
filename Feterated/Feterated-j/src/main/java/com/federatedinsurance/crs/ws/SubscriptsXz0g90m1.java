/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program XZ0G90M1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsXz0g90m1 {

	//==== PROPERTIES ====
	//Original name: SS-DF-MSG-IDX
	private short dfMsgIdx = ((short) 0);
	public static final short DF_MSG_IDX_MAX = ((short) 10);
	//Original name: SS-DF-WNG-IDX
	private short dfWngIdx = ((short) 0);
	public static final short DF_WNG_IDX_MAX = ((short) 10);
	//Original name: SS-SP-MSG-IDX
	private short spMsgIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short SP_MSG_IDX_MAX = ((short) 10);
	//Original name: SS-SP-WNG-IDX
	private short spWngIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short SP_WNG_IDX_MAX = ((short) 10);
	//Original name: SS-UL
	private short ul = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setDfMsgIdx(short dfMsgIdx) {
		this.dfMsgIdx = dfMsgIdx;
	}

	public short getDfMsgIdx() {
		return this.dfMsgIdx;
	}

	public boolean isDfMsgIdxMax() {
		return dfMsgIdx == DF_MSG_IDX_MAX;
	}

	public void setDfWngIdx(short dfWngIdx) {
		this.dfWngIdx = dfWngIdx;
	}

	public short getDfWngIdx() {
		return this.dfWngIdx;
	}

	public boolean isDfWngIdxMax() {
		return dfWngIdx == DF_WNG_IDX_MAX;
	}

	public void setSpMsgIdx(short spMsgIdx) {
		this.spMsgIdx = spMsgIdx;
	}

	public short getSpMsgIdx() {
		return this.spMsgIdx;
	}

	public boolean isSpMsgIdxMax() {
		return spMsgIdx == SP_MSG_IDX_MAX;
	}

	public void setSpWngIdx(short spWngIdx) {
		this.spWngIdx = spWngIdx;
	}

	public short getSpWngIdx() {
		return this.spWngIdx;
	}

	public boolean isSpWngIdxMax() {
		return spWngIdx == SP_WNG_IDX_MAX;
	}

	public void setUl(short ul) {
		this.ul = ul;
	}

	public short getUl() {
		return this.ul;
	}
}
