/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.federatedinsurance.crs.commons.data.to.IActNotRec;
import com.federatedinsurance.crs.ws.Xz0p90e0Data;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: ActNotRecXz0p90e0<br>*/
public class ActNotRecXz0p90e0 implements IActNotRec {

	//==== PROPERTIES ====
	private Xz0p90e0Data ws;

	//==== CONSTRUCTORS ====
	public ActNotRecXz0p90e0(Xz0p90e0Data ws) {
		this.ws = ws;
	}

	//==== METHODS ====
	@Override
	public String getAdrId() {
		return ws.getDclactNotRec().getRecAdrId();
	}

	@Override
	public void setAdrId(String adrId) {
		ws.getDclactNotRec().setRecAdrId(adrId);
	}

	@Override
	public String getAdrIdObj() {
		if (ws.getNiRecAdrId() >= 0) {
			return getAdrId();
		} else {
			return null;
		}
	}

	@Override
	public void setAdrIdObj(String adrIdObj) {
		if (adrIdObj != null) {
			setAdrId(adrIdObj);
			ws.setNiRecAdrId(((short) 0));
		} else {
			ws.setNiRecAdrId(((short) -1));
		}
	}

	@Override
	public String getCerNbr() {
		throw new FieldNotMappedException("cerNbr");
	}

	@Override
	public void setCerNbr(String cerNbr) {
		throw new FieldNotMappedException("cerNbr");
	}

	@Override
	public String getCerNbrObj() {
		return getCerNbr();
	}

	@Override
	public void setCerNbrObj(String cerNbrObj) {
		setCerNbr(cerNbrObj);
	}

	@Override
	public String getCitNm() {
		throw new FieldNotMappedException("citNm");
	}

	@Override
	public void setCitNm(String citNm) {
		throw new FieldNotMappedException("citNm");
	}

	@Override
	public String getCitNmObj() {
		return getCitNm();
	}

	@Override
	public void setCitNmObj(String citNmObj) {
		setCitNm(citNmObj);
	}

	@Override
	public String getCltId() {
		return ws.getDclactNotRec().getRecCltId();
	}

	@Override
	public void setCltId(String cltId) {
		ws.getDclactNotRec().setRecCltId(cltId);
	}

	@Override
	public String getCltIdObj() {
		if (ws.getNiRecCltId() >= 0) {
			return getCltId();
		} else {
			return null;
		}
	}

	@Override
	public void setCltIdObj(String cltIdObj) {
		if (cltIdObj != null) {
			setCltId(cltIdObj);
			ws.setNiRecCltId(((short) 0));
		} else {
			ws.setNiRecCltId(((short) -1));
		}
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public String getLin1Adr() {
		throw new FieldNotMappedException("lin1Adr");
	}

	@Override
	public void setLin1Adr(String lin1Adr) {
		throw new FieldNotMappedException("lin1Adr");
	}

	@Override
	public String getLin1AdrObj() {
		return getLin1Adr();
	}

	@Override
	public void setLin1AdrObj(String lin1AdrObj) {
		setLin1Adr(lin1AdrObj);
	}

	@Override
	public String getLin2Adr() {
		throw new FieldNotMappedException("lin2Adr");
	}

	@Override
	public void setLin2Adr(String lin2Adr) {
		throw new FieldNotMappedException("lin2Adr");
	}

	@Override
	public String getLin2AdrObj() {
		return getLin2Adr();
	}

	@Override
	public void setLin2AdrObj(String lin2AdrObj) {
		setLin2Adr(lin2AdrObj);
	}

	@Override
	public char getMnlInd() {
		throw new FieldNotMappedException("mnlInd");
	}

	@Override
	public void setMnlInd(char mnlInd) {
		throw new FieldNotMappedException("mnlInd");
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getPstCd() {
		throw new FieldNotMappedException("pstCd");
	}

	@Override
	public void setPstCd(String pstCd) {
		throw new FieldNotMappedException("pstCd");
	}

	@Override
	public String getPstCdObj() {
		return getPstCd();
	}

	@Override
	public void setPstCdObj(String pstCdObj) {
		setPstCd(pstCdObj);
	}

	@Override
	public String getRecAdrId() {
		throw new FieldNotMappedException("recAdrId");
	}

	@Override
	public void setRecAdrId(String recAdrId) {
		throw new FieldNotMappedException("recAdrId");
	}

	@Override
	public String getRecAdrIdObj() {
		return getRecAdrId();
	}

	@Override
	public void setRecAdrIdObj(String recAdrIdObj) {
		setRecAdrId(recAdrIdObj);
	}

	@Override
	public String getRecCltId() {
		throw new FieldNotMappedException("recCltId");
	}

	@Override
	public void setRecCltId(String recCltId) {
		throw new FieldNotMappedException("recCltId");
	}

	@Override
	public String getRecCltIdObj() {
		return getRecCltId();
	}

	@Override
	public void setRecCltIdObj(String recCltIdObj) {
		setRecCltId(recCltIdObj);
	}

	@Override
	public String getRecNm() {
		throw new FieldNotMappedException("recNm");
	}

	@Override
	public void setRecNm(String recNm) {
		throw new FieldNotMappedException("recNm");
	}

	@Override
	public String getRecNmObj() {
		return getRecNm();
	}

	@Override
	public void setRecNmObj(String recNmObj) {
		setRecNm(recNmObj);
	}

	@Override
	public short getRecSeqNbr() {
		return ws.getDclactNotRec().getRecSeqNbr();
	}

	@Override
	public void setRecSeqNbr(short recSeqNbr) {
		ws.getDclactNotRec().setRecSeqNbr(recSeqNbr);
	}

	@Override
	public String getRecTypCd() {
		return ws.getDclactNotRec().getRecTypCd();
	}

	@Override
	public void setRecTypCd(String recTypCd) {
		ws.getDclactNotRec().setRecTypCd(recTypCd);
	}

	@Override
	public String getStAbb() {
		throw new FieldNotMappedException("stAbb");
	}

	@Override
	public void setStAbb(String stAbb) {
		throw new FieldNotMappedException("stAbb");
	}

	@Override
	public String getStAbbObj() {
		return getStAbb();
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		setStAbb(stAbbObj);
	}
}
