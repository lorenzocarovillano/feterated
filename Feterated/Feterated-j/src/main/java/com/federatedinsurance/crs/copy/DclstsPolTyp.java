/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IStsPolTyp;

/**Original name: DCLSTS-POL-TYP<br>
 * Variable: DCLSTS-POL-TYP from copybook MUH00637<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclstsPolTyp implements IStsPolTyp {

	//==== PROPERTIES ====
	//Original name: SIII-POL-TYP-CD
	private String siiiPolTypCd = DefaultValues.stringVal(Len.SIII_POL_TYP_CD);
	//Original name: STS-POL-TYP-CD
	private String stsPolTypCd = DefaultValues.stringVal(Len.STS_POL_TYP_CD);
	//Original name: EFF-DT
	private String effDt = DefaultValues.stringVal(Len.EFF_DT);
	//Original name: STS-POL-TYP-DES
	private String stsPolTypDes = DefaultValues.stringVal(Len.STS_POL_TYP_DES);
	//Original name: SIII-POL-TYP-DES
	private String siiiPolTypDes = DefaultValues.stringVal(Len.SIII_POL_TYP_DES);
	//Original name: EXP-DT
	private String expDt = DefaultValues.stringVal(Len.EXP_DT);
	//Original name: BOND-IND
	private char bondInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setSiiiPolTypCd(String siiiPolTypCd) {
		this.siiiPolTypCd = Functions.subString(siiiPolTypCd, Len.SIII_POL_TYP_CD);
	}

	public String getSiiiPolTypCd() {
		return this.siiiPolTypCd;
	}

	public String getSiiiPolTypCdFormatted() {
		return Functions.padBlanks(getSiiiPolTypCd(), Len.SIII_POL_TYP_CD);
	}

	public void setStsPolTypCd(String stsPolTypCd) {
		this.stsPolTypCd = Functions.subString(stsPolTypCd, Len.STS_POL_TYP_CD);
	}

	public String getStsPolTypCd() {
		return this.stsPolTypCd;
	}

	public void setEffDt(String effDt) {
		this.effDt = Functions.subString(effDt, Len.EFF_DT);
	}

	public String getEffDt() {
		return this.effDt;
	}

	public void setStsPolTypDes(String stsPolTypDes) {
		this.stsPolTypDes = Functions.subString(stsPolTypDes, Len.STS_POL_TYP_DES);
	}

	public String getStsPolTypDes() {
		return this.stsPolTypDes;
	}

	public void setSiiiPolTypDes(String siiiPolTypDes) {
		this.siiiPolTypDes = Functions.subString(siiiPolTypDes, Len.SIII_POL_TYP_DES);
	}

	public String getSiiiPolTypDes() {
		return this.siiiPolTypDes;
	}

	public void setExpDt(String expDt) {
		this.expDt = Functions.subString(expDt, Len.EXP_DT);
	}

	public String getExpDt() {
		return this.expDt;
	}

	public void setBondInd(char bondInd) {
		this.bondInd = bondInd;
	}

	public char getBondInd() {
		return this.bondInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SIII_POL_TYP_CD = 3;
		public static final int STS_POL_TYP_CD = 4;
		public static final int EFF_DT = 10;
		public static final int STS_POL_TYP_DES = 75;
		public static final int SIII_POL_TYP_DES = 30;
		public static final int EXP_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
