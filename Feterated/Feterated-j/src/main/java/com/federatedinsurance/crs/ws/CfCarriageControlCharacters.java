/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CF-CARRIAGE-CONTROL-CHARACTERS<br>
 * Variable: CF-CARRIAGE-CONTROL-CHARACTERS from program TS030099<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class CfCarriageControlCharacters {

	//==== PROPERTIES ====
	//Original name: CF-SUPPRESS-SPACE
	private String suppressSpace = "0";
	//Original name: CF-SINGLE-SPACE
	private String singleSpace = "1";
	//Original name: CF-DOUBLE-SPACE
	private String doubleSpace = "2";
	//Original name: CF-TRIPLE-SPACE
	private String tripleSpace = "3";
	//Original name: CF-TOP-OF-PAGE
	private char topOfPage = '1';
	//Original name: CF-SUPPRESS-SPACE-X
	private char suppressSpaceX = '+';
	//Original name: CF-SINGLE-SPACE-X
	private char singleSpaceX = Types.SPACE_CHAR;
	//Original name: CF-DOUBLE-SPACE-X
	private char doubleSpaceX = '0';
	//Original name: CF-TRIPLE-SPACE-X
	private char tripleSpaceX = '-';
	//Original name: CF-CHANNEL-ONE
	private char channelOne = '1';
	//Original name: CF-CHANNEL-TWO
	private char channelTwo = '2';
	//Original name: CF-CHANNEL-THREE
	private char channelThree = '3';
	//Original name: CF-CHANNEL-FOUR
	private char channelFour = '4';
	//Original name: CF-CHANNEL-FIVE
	private char channelFive = '5';
	//Original name: CF-CHANNEL-SIX
	private char channelSix = '6';
	//Original name: CF-CHANNEL-SEVEN
	private char channelSeven = '7';
	//Original name: CF-CHANNEL-EIGHT
	private char channelEight = '8';
	//Original name: CF-CHANNEL-NINE
	private char channelNine = '9';
	//Original name: CF-CHANNEL-TEN
	private char channelTen = 'A';
	//Original name: CF-CHANNEL-ELEVEN
	private char channelEleven = 'B';
	//Original name: CF-CHANNEL-TWELVE
	private char channelTwelve = 'C';
	//Original name: CF-PL-CARRIAGE-CTRL
	private String plCarriageCtrl = "!+0123ABCDEFGHIJKL";
	//Original name: CF-STRUCT-CARRIAGE-CTRL
	private String structCarriageCtrl = "!+1 0-123456789ABC";

	//==== METHODS ====
	public String getSuppressSpaceFormatted() {
		return this.suppressSpace;
	}

	public String getSingleSpaceFormatted() {
		return this.singleSpace;
	}

	public String getDoubleSpaceFormatted() {
		return this.doubleSpace;
	}

	public String getTripleSpaceFormatted() {
		return this.tripleSpace;
	}

	public char getTopOfPage() {
		return this.topOfPage;
	}

	public char getSuppressSpaceX() {
		return this.suppressSpaceX;
	}

	public char getSingleSpaceX() {
		return this.singleSpaceX;
	}

	public char getDoubleSpaceX() {
		return this.doubleSpaceX;
	}

	public char getTripleSpaceX() {
		return this.tripleSpaceX;
	}

	public char getChannelOne() {
		return this.channelOne;
	}

	public char getChannelTwo() {
		return this.channelTwo;
	}

	public char getChannelThree() {
		return this.channelThree;
	}

	public char getChannelFour() {
		return this.channelFour;
	}

	public char getChannelFive() {
		return this.channelFive;
	}

	public char getChannelSix() {
		return this.channelSix;
	}

	public char getChannelSeven() {
		return this.channelSeven;
	}

	public char getChannelEight() {
		return this.channelEight;
	}

	public char getChannelNine() {
		return this.channelNine;
	}

	public char getChannelTen() {
		return this.channelTen;
	}

	public char getChannelEleven() {
		return this.channelEleven;
	}

	public char getChannelTwelve() {
		return this.channelTwelve;
	}

	public String getPlCarriageCtrl() {
		return this.plCarriageCtrl;
	}

	public String getPlCarriageCtrlFormatted() {
		return Functions.padBlanks(getPlCarriageCtrl(), Len.PL_CARRIAGE_CTRL);
	}

	public String getStructCarriageCtrl() {
		return this.structCarriageCtrl;
	}

	public String getStructCarriageCtrlFormatted() {
		return Functions.padBlanks(getStructCarriageCtrl(), Len.STRUCT_CARRIAGE_CTRL);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PL_CARRIAGE_CTRL = 18;
		public static final int STRUCT_CARRIAGE_CTRL = 18;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
