/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLTAX-TYPE-V<br>
 * Variable: DCLTAX-TYPE-V from copybook MUH00476<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DcltaxTypeV {

	//==== PROPERTIES ====
	//Original name: TAX-TYPE-CD
	private String taxTypeCd = DefaultValues.stringVal(Len.TAX_TYPE_CD);
	//Original name: CTR-NBR-CD
	private short ctrNbrCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: CITT-TAX-TYPE-DES
	private String cittTaxTypeDes = DefaultValues.stringVal(Len.CITT_TAX_TYPE_DES);

	//==== METHODS ====
	public void setTaxTypeCd(String taxTypeCd) {
		this.taxTypeCd = Functions.subString(taxTypeCd, Len.TAX_TYPE_CD);
	}

	public String getTaxTypeCd() {
		return this.taxTypeCd;
	}

	public String getTaxTypeCdFormatted() {
		return Functions.padBlanks(getTaxTypeCd(), Len.TAX_TYPE_CD);
	}

	public void setCtrNbrCd(short ctrNbrCd) {
		this.ctrNbrCd = ctrNbrCd;
	}

	public short getCtrNbrCd() {
		return this.ctrNbrCd;
	}

	public void setCittTaxTypeDes(String cittTaxTypeDes) {
		this.cittTaxTypeDes = Functions.subString(cittTaxTypeDes, Len.CITT_TAX_TYPE_DES);
	}

	public String getCittTaxTypeDes() {
		return this.cittTaxTypeDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TAX_TYPE_CD = 10;
		public static final int CITT_TAX_TYPE_DES = 40;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
