/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.SoapErrorStorCharset;
import com.federatedinsurance.crs.ws.enums.SoapErrorStorType;
import com.federatedinsurance.crs.ws.enums.SoapRequestStorType;
import com.federatedinsurance.crs.ws.enums.SoapResponseStorType;
import com.federatedinsurance.crs.ws.enums.SoapTraceStorCharset;
import com.federatedinsurance.crs.ws.enums.SoapTraceStorType;

/**Original name: NON-STATIC-FIELDS<br>
 * Variable: NON-STATIC-FIELDS from copybook TS570CB1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class NonStaticFields {

	//==== PROPERTIES ====
	//Original name: SOAP-SERVICE
	private String service = DefaultValues.stringVal(Len.SERVICE);
	//Original name: SOAP-OPERATION
	private String operation = DefaultValues.stringVal(Len.OPERATION);
	//Original name: SOAP-REQUEST-STOR-TYPE
	private SoapRequestStorType requestStorType = new SoapRequestStorType();
	//Original name: SOAP-RESPONSE-STOR-TYPE
	private SoapResponseStorType responseStorType = new SoapResponseStorType();
	//Original name: SOAP-REQUEST-STORE-VALUE
	private String requestStoreValue = DefaultValues.stringVal(Len.REQUEST_STORE_VALUE);
	//Original name: SOAP-REQUEST-LENGTH
	private long requestLength = DefaultValues.BIN_LONG_VAL;
	//Original name: SOAP-RESPONSE-STORE-VALUE
	private String responseStoreValue = DefaultValues.stringVal(Len.RESPONSE_STORE_VALUE);
	//Original name: SOAP-RESPONSE-LENGTH
	private long responseLength = DefaultValues.BIN_LONG_VAL;
	//Original name: SOAP-TRACE-STOR-CHARSET
	private SoapTraceStorCharset traceStorCharset = new SoapTraceStorCharset();
	//Original name: SOAP-TRACE-STOR-TYPE
	private SoapTraceStorType traceStorType = new SoapTraceStorType();
	//Original name: SOAP-TRACE-STOR-VALUE
	private String traceStorValue = DefaultValues.stringVal(Len.TRACE_STOR_VALUE);
	//Original name: SOAP-ERROR-STOR-CHARSET
	private SoapErrorStorCharset errorStorCharset = new SoapErrorStorCharset();
	//Original name: SOAP-ERROR-STOR-TYPE
	private SoapErrorStorType errorStorType = new SoapErrorStorType();
	//Original name: SOAP-ERROR-STOR-VALUE
	private String errorStorValue = DefaultValues.stringVal(Len.ERROR_STOR_VALUE);
	//Original name: SOAP-RETURNCODE
	private long returncode = DefaultValues.BIN_LONG_VAL;
	//Original name: SOAP-RESERVED
	private String reserved = DefaultValues.stringVal(Len.RESERVED);

	//==== METHODS ====
	public void setNonStaticFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		service = MarshalByte.readString(buffer, position, Len.SERVICE);
		position += Len.SERVICE;
		operation = MarshalByte.readString(buffer, position, Len.OPERATION);
		position += Len.OPERATION;
		requestStorType.setRequestStorType(MarshalByte.readString(buffer, position, SoapRequestStorType.Len.REQUEST_STOR_TYPE));
		position += SoapRequestStorType.Len.REQUEST_STOR_TYPE;
		responseStorType.setResponseStorType(MarshalByte.readString(buffer, position, SoapResponseStorType.Len.RESPONSE_STOR_TYPE));
		position += SoapResponseStorType.Len.RESPONSE_STOR_TYPE;
		requestStoreValue = MarshalByte.readString(buffer, position, Len.REQUEST_STORE_VALUE);
		position += Len.REQUEST_STORE_VALUE;
		requestLength = MarshalByte.readBinaryUnsignedInt(buffer, position);
		position += Types.INT_SIZE;
		responseStoreValue = MarshalByte.readString(buffer, position, Len.RESPONSE_STORE_VALUE);
		position += Len.RESPONSE_STORE_VALUE;
		responseLength = MarshalByte.readBinaryUnsignedInt(buffer, position);
		position += Types.INT_SIZE;
		traceStorCharset.setTraceStorCharset(MarshalByte.readString(buffer, position, SoapTraceStorCharset.Len.TRACE_STOR_CHARSET));
		position += SoapTraceStorCharset.Len.TRACE_STOR_CHARSET;
		traceStorType.setTraceStorType(MarshalByte.readString(buffer, position, SoapTraceStorType.Len.TRACE_STOR_TYPE));
		position += SoapTraceStorType.Len.TRACE_STOR_TYPE;
		traceStorValue = MarshalByte.readString(buffer, position, Len.TRACE_STOR_VALUE);
		position += Len.TRACE_STOR_VALUE;
		errorStorCharset.setErrorStorCharset(MarshalByte.readString(buffer, position, SoapErrorStorCharset.Len.ERROR_STOR_CHARSET));
		position += SoapErrorStorCharset.Len.ERROR_STOR_CHARSET;
		errorStorType.setErrorStorType(MarshalByte.readString(buffer, position, SoapErrorStorType.Len.ERROR_STOR_TYPE));
		position += SoapErrorStorType.Len.ERROR_STOR_TYPE;
		errorStorValue = MarshalByte.readString(buffer, position, Len.ERROR_STOR_VALUE);
		position += Len.ERROR_STOR_VALUE;
		returncode = MarshalByte.readBinaryUnsignedInt(buffer, position);
		position += Types.INT_SIZE;
		reserved = MarshalByte.readString(buffer, position, Len.RESERVED);
	}

	public byte[] getNonStaticFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, service, Len.SERVICE);
		position += Len.SERVICE;
		MarshalByte.writeString(buffer, position, operation, Len.OPERATION);
		position += Len.OPERATION;
		MarshalByte.writeString(buffer, position, requestStorType.getRequestStorType(), SoapRequestStorType.Len.REQUEST_STOR_TYPE);
		position += SoapRequestStorType.Len.REQUEST_STOR_TYPE;
		MarshalByte.writeString(buffer, position, responseStorType.getResponseStorType(), SoapResponseStorType.Len.RESPONSE_STOR_TYPE);
		position += SoapResponseStorType.Len.RESPONSE_STOR_TYPE;
		MarshalByte.writeString(buffer, position, requestStoreValue, Len.REQUEST_STORE_VALUE);
		position += Len.REQUEST_STORE_VALUE;
		MarshalByte.writeBinaryUnsignedInt(buffer, position, requestLength);
		position += Types.INT_SIZE;
		MarshalByte.writeString(buffer, position, responseStoreValue, Len.RESPONSE_STORE_VALUE);
		position += Len.RESPONSE_STORE_VALUE;
		MarshalByte.writeBinaryUnsignedInt(buffer, position, responseLength);
		position += Types.INT_SIZE;
		MarshalByte.writeString(buffer, position, traceStorCharset.getTraceStorCharset(), SoapTraceStorCharset.Len.TRACE_STOR_CHARSET);
		position += SoapTraceStorCharset.Len.TRACE_STOR_CHARSET;
		MarshalByte.writeString(buffer, position, traceStorType.getTraceStorType(), SoapTraceStorType.Len.TRACE_STOR_TYPE);
		position += SoapTraceStorType.Len.TRACE_STOR_TYPE;
		MarshalByte.writeString(buffer, position, traceStorValue, Len.TRACE_STOR_VALUE);
		position += Len.TRACE_STOR_VALUE;
		MarshalByte.writeString(buffer, position, errorStorCharset.getErrorStorCharset(), SoapErrorStorCharset.Len.ERROR_STOR_CHARSET);
		position += SoapErrorStorCharset.Len.ERROR_STOR_CHARSET;
		MarshalByte.writeString(buffer, position, errorStorType.getErrorStorType(), SoapErrorStorType.Len.ERROR_STOR_TYPE);
		position += SoapErrorStorType.Len.ERROR_STOR_TYPE;
		MarshalByte.writeString(buffer, position, errorStorValue, Len.ERROR_STOR_VALUE);
		position += Len.ERROR_STOR_VALUE;
		MarshalByte.writeBinaryUnsignedInt(buffer, position, returncode);
		position += Types.INT_SIZE;
		MarshalByte.writeString(buffer, position, reserved, Len.RESERVED);
		return buffer;
	}

	public void initNonStaticFieldsLowValues() {
		service = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.SERVICE);
		operation = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.OPERATION);
		requestStorType.setRequestStorType(LiteralGenerator.create(Types.LOW_CHAR_VAL, SoapRequestStorType.Len.REQUEST_STOR_TYPE));
		responseStorType.setResponseStorType(LiteralGenerator.create(Types.LOW_CHAR_VAL, SoapResponseStorType.Len.RESPONSE_STOR_TYPE));
		requestStoreValue = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.REQUEST_STORE_VALUE);
		requestLength = Types.LOW_BINARY_LONG_VAL;
		responseStoreValue = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.RESPONSE_STORE_VALUE);
		responseLength = Types.LOW_BINARY_LONG_VAL;
		traceStorCharset.setTraceStorCharset(LiteralGenerator.create(Types.LOW_CHAR_VAL, SoapTraceStorCharset.Len.TRACE_STOR_CHARSET));
		traceStorType.setTraceStorType(LiteralGenerator.create(Types.LOW_CHAR_VAL, SoapTraceStorType.Len.TRACE_STOR_TYPE));
		traceStorValue = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.TRACE_STOR_VALUE);
		errorStorCharset.setErrorStorCharset(LiteralGenerator.create(Types.LOW_CHAR_VAL, SoapErrorStorCharset.Len.ERROR_STOR_CHARSET));
		errorStorType.setErrorStorType(LiteralGenerator.create(Types.LOW_CHAR_VAL, SoapErrorStorType.Len.ERROR_STOR_TYPE));
		errorStorValue = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.ERROR_STOR_VALUE);
		returncode = Types.LOW_BINARY_LONG_VAL;
		reserved = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.RESERVED);
	}

	public void setService(String service) {
		this.service = Functions.subString(service, Len.SERVICE);
	}

	public String getService() {
		return this.service;
	}

	public void setOperation(String operation) {
		this.operation = Functions.subString(operation, Len.OPERATION);
	}

	public String getOperation() {
		return this.operation;
	}

	public void setRequestStoreValue(String requestStoreValue) {
		this.requestStoreValue = Functions.subString(requestStoreValue, Len.REQUEST_STORE_VALUE);
	}

	public String getRequestStoreValue() {
		return this.requestStoreValue;
	}

	public void setRequestLength(long requestLength) {
		this.requestLength = requestLength;
	}

	public long getRequestLength() {
		return this.requestLength;
	}

	public void setResponseStoreValue(String responseStoreValue) {
		this.responseStoreValue = Functions.subString(responseStoreValue, Len.RESPONSE_STORE_VALUE);
	}

	public String getResponseStoreValue() {
		return this.responseStoreValue;
	}

	public void setResponseLength(long responseLength) {
		this.responseLength = responseLength;
	}

	public long getResponseLength() {
		return this.responseLength;
	}

	public void setTraceStorValue(String traceStorValue) {
		this.traceStorValue = Functions.subString(traceStorValue, Len.TRACE_STOR_VALUE);
	}

	public String getTraceStorValue() {
		return this.traceStorValue;
	}

	public void setErrorStorValue(String errorStorValue) {
		this.errorStorValue = Functions.subString(errorStorValue, Len.ERROR_STOR_VALUE);
	}

	public String getErrorStorValue() {
		return this.errorStorValue;
	}

	public void setReturncode(long returncode) {
		this.returncode = returncode;
	}

	public long getReturncode() {
		return this.returncode;
	}

	public void setReserved(String reserved) {
		this.reserved = Functions.subString(reserved, Len.RESERVED);
	}

	public String getReserved() {
		return this.reserved;
	}

	public SoapErrorStorType getErrorStorType() {
		return errorStorType;
	}

	public SoapRequestStorType getRequestStorType() {
		return requestStorType;
	}

	public SoapResponseStorType getResponseStorType() {
		return responseStorType;
	}

	public SoapTraceStorCharset getTraceStorCharset() {
		return traceStorCharset;
	}

	public SoapTraceStorType getTraceStorType() {
		return traceStorType;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SERVICE = 32;
		public static final int OPERATION = 32;
		public static final int REQUEST_STORE_VALUE = 32;
		public static final int RESPONSE_STORE_VALUE = 32;
		public static final int TRACE_STOR_VALUE = 32;
		public static final int ERROR_STOR_VALUE = 32;
		public static final int RESERVED = 24;
		public static final int REQUEST_LENGTH = 4;
		public static final int RESPONSE_LENGTH = 4;
		public static final int RETURNCODE = 4;
		public static final int NON_STATIC_FIELDS = SERVICE + OPERATION + SoapRequestStorType.Len.REQUEST_STOR_TYPE
				+ SoapResponseStorType.Len.RESPONSE_STOR_TYPE + REQUEST_STORE_VALUE + REQUEST_LENGTH + RESPONSE_STORE_VALUE + RESPONSE_LENGTH
				+ SoapTraceStorCharset.Len.TRACE_STOR_CHARSET + SoapTraceStorType.Len.TRACE_STOR_TYPE + TRACE_STOR_VALUE
				+ SoapErrorStorCharset.Len.ERROR_STOR_CHARSET + SoapErrorStorType.Len.ERROR_STOR_TYPE + ERROR_STOR_VALUE + RETURNCODE + RESERVED;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
