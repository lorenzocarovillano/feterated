/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPol1;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclpolDtaExt;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xzy800GetNotDayRqrRow;
import com.federatedinsurance.crs.ws.enums.TpPolicies;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0P90K0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0p90k0Data implements IActNotPol1 {

	//==== PROPERTIES ====
	public static final int TP_POLICIES_MAXOCCURS = 30;
	//Original name: AA-TOT-FEE-AMT
	private AfDecimal aaTotFeeAmt = new AfDecimal("0", 10, 2);
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0p90k0 constantFields = new ConstantFieldsXz0p90k0();
	//Original name: EA-01-INSUFFICIENT-DAYS
	private Ea01InsufficientDays ea01InsufficientDays = new Ea01InsufficientDays();
	//Original name: ES-01-FATAL-ERROR-MSG
	private Es01FatalErrorMsg es01FatalErrorMsg = new Es01FatalErrorMsg();
	//Original name: SUBSCRIPTS
	private SubscriptsXz0p90a0 subscripts = new SubscriptsXz0p90a0();
	//Original name: SWITCHES
	private SwitchesXz0p90k0 switches = new SwitchesXz0p90k0();
	//Original name: TP-POLICIES
	private TpPolicies[] tpPolicies = new TpPolicies[TP_POLICIES_MAXOCCURS];
	//Original name: IX-TP
	private int ixTp = 1;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0p90k0 workingStorageArea = new WorkingStorageAreaXz0p90k0();
	//Original name: WS-XZ0Y90K0-ROW
	private WsXz0y90a0Row wsXz0y90k0Row = new WsXz0y90a0Row();
	//Original name: XZY800-GET-NOT-DAY-RQR-ROW
	private Xzy800GetNotDayRqrRow xzy800GetNotDayRqrRow = new Xzy800GetNotDayRqrRow();
	//Original name: WS-PROXY-PROGRAM-AREA
	private WsProxyProgramArea wsProxyProgramArea = new WsProxyProgramArea();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: DCLPOL-DTA-EXT
	private DclpolDtaExt dclpolDtaExt = new DclpolDtaExt();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-BILLING-LINKAGE
	private WsBillingLinkage wsBillingLinkage = new WsBillingLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== CONSTRUCTORS ====
	public Xz0p90k0Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int tpPoliciesIdx = 1; tpPoliciesIdx <= TP_POLICIES_MAXOCCURS; tpPoliciesIdx++) {
			tpPolicies[tpPoliciesIdx - 1] = new TpPolicies();
		}
		initTableOfPoliciesHighValues();
	}

	public void setAaTotFeeAmt(AfDecimal aaTotFeeAmt) {
		this.aaTotFeeAmt.assign(aaTotFeeAmt);
	}

	public AfDecimal getAaTotFeeAmt() {
		return this.aaTotFeeAmt.copy();
	}

	public void initTableOfPoliciesHighValues() {
		for (int idx = 1; idx <= TP_POLICIES_MAXOCCURS; idx++) {
			tpPolicies[idx - 1].initTpPoliciesHighValues();
		}
	}

	public void setIxTp(int ixTp) {
		this.ixTp = ixTp;
	}

	public int getIxTp() {
		return this.ixTp;
	}

	public void setWsXz0y8000RowFormatted(String data) {
		byte[] buffer = new byte[Len.WS_XZ0Y8000_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0Y8000_ROW);
		setWsXz0y8000RowBytes(buffer, 1);
	}

	public String getWsXz0y8000RowFormatted() {
		return xzy800GetNotDayRqrRow.getXzy800GetNotDayRqrRowFormatted();
	}

	public void setWsXz0y8000RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xzy800GetNotDayRqrRow.setXzy800GetNotDayRqrRowBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getActNotStaCd() {
		return dclactNot.getActNotStaCd();
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		this.dclactNot.setActNotStaCd(actNotStaCd);
	}

	@Override
	public String getActNotTypCd() {
		return dclactNot.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.dclactNot.setActNotTypCd(actNotTypCd);
	}

	@Override
	public String getActOwnCltId() {
		return dclactNot.getActOwnCltId();
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		this.dclactNot.setActOwnCltId(actOwnCltId);
	}

	@Override
	public String getCfActNotImpending() {
		return constantFields.getActNotImpending();
	}

	@Override
	public void setCfActNotImpending(String cfActNotImpending) {
		this.constantFields.setActNotImpending(cfActNotImpending);
	}

	@Override
	public String getCfActNotNonpay() {
		return constantFields.getActNotNonpay();
	}

	@Override
	public void setCfActNotNonpay(String cfActNotNonpay) {
		this.constantFields.setActNotNonpay(cfActNotNonpay);
	}

	@Override
	public String getCfActNotRescind() {
		return constantFields.getActNotRescind();
	}

	@Override
	public void setCfActNotRescind(String cfActNotRescind) {
		this.constantFields.setActNotRescind(cfActNotRescind);
	}

	@Override
	public String getCfActNotTypCdImp() {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	@Override
	public void setCfActNotTypCdImp(String cfActNotTypCdImp) {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	public ConstantFieldsXz0p90k0 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		return dclactNot.getCsrActNbr();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.dclactNot.setCsrActNbr(csrActNbr);
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclpolDtaExt getDclpolDtaExt() {
		return dclpolDtaExt;
	}

	public Ea01InsufficientDays getEa01InsufficientDays() {
		return ea01InsufficientDays;
	}

	@Override
	public String getEmpId() {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public void setEmpId(String empId) {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	public Es01FatalErrorMsg getEs01FatalErrorMsg() {
		return es01FatalErrorMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotDt() {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public void setNotDt(String notDt) {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public String getNotEffDt() {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getPolEffDt() {
		return dclactNotPol.getPolEffDt();
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		this.dclactNotPol.setPolEffDt(polEffDt);
	}

	@Override
	public String getPolExpDt() {
		return dclactNotPol.getPolExpDt();
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		this.dclactNotPol.setPolExpDt(polExpDt);
	}

	@Override
	public String getPolNbr() {
		return dclactNotPol.getPolNbr();
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.dclactNotPol.setPolNbr(polNbr);
	}

	@Override
	public String getPolTypCd() {
		throw new FieldNotMappedException("polTypCd");
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		throw new FieldNotMappedException("polTypCd");
	}

	public SubscriptsXz0p90a0 getSubscripts() {
		return subscripts;
	}

	public SwitchesXz0p90k0 getSwitches() {
		return switches;
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		return getTotFeeAmt().toString();
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
	}

	public TpPolicies getTpPolicies(int idx) {
		return tpPolicies[idx - 1];
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0p90k0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsBillingLinkage getWsBillingLinkage() {
		return wsBillingLinkage;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	@Override
	public short getWsNbrOfNotDay() {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	@Override
	public void setWsNbrOfNotDay(short wsNbrOfNotDay) {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	@Override
	public String getWsPolicyCancDt() {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}

	@Override
	public void setWsPolicyCancDt(String wsPolicyCancDt) {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}

	public WsProxyProgramArea getWsProxyProgramArea() {
		return wsProxyProgramArea;
	}

	public WsXz0y90a0Row getWsXz0y90k0Row() {
		return wsXz0y90k0Row;
	}

	public Xzy800GetNotDayRqrRow getXzy800GetNotDayRqrRow() {
		return xzy800GetNotDayRqrRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_XZ0Y8000_ROW = Xzy800GetNotDayRqrRow.Len.XZY800_GET_NOT_DAY_RQR_ROW;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
