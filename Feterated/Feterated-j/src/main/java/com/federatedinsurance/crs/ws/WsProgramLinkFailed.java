/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-PROGRAM-LINK-FAILED<br>
 * Variable: WS-PROGRAM-LINK-FAILED from program MU0Q0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsProgramLinkFailed {

	//==== PROPERTIES ====
	//Original name: FILLER-WS-PROGRAM-LINK-FAILED
	private String flr1 = "CALL TO";
	//Original name: WS-FAILED-LINK-PGM-NAME
	private String wsFailedLinkPgmName = DefaultValues.stringVal(Len.WS_FAILED_LINK_PGM_NAME);
	//Original name: FILLER-WS-PROGRAM-LINK-FAILED-1
	private String flr2 = " FAILED.";

	//==== METHODS ====
	public String getProgramLinkFailedFormatted() {
		return MarshalByteExt.bufferToStr(getProgramLinkFailedBytes());
	}

	public byte[] getProgramLinkFailedBytes() {
		byte[] buffer = new byte[Len.PROGRAM_LINK_FAILED];
		return getProgramLinkFailedBytes(buffer, 1);
	}

	public byte[] getProgramLinkFailedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, wsFailedLinkPgmName, Len.WS_FAILED_LINK_PGM_NAME);
		position += Len.WS_FAILED_LINK_PGM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setWsFailedLinkPgmName(String wsFailedLinkPgmName) {
		this.wsFailedLinkPgmName = Functions.subString(wsFailedLinkPgmName, Len.WS_FAILED_LINK_PGM_NAME);
	}

	public String getWsFailedLinkPgmName() {
		return this.wsFailedLinkPgmName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_FAILED_LINK_PGM_NAME = 8;
		public static final int FLR1 = 8;
		public static final int PROGRAM_LINK_FAILED = WS_FAILED_LINK_PGM_NAME + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
