/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-CICS-PIPE-CONTRACT-ADDRESS<br>
 * Variable: L-CICS-PIPE-CONTRACT-ADDRESS from program TS547099<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LCicsPipeContractAddress extends BytesClass {

	//==== CONSTRUCTORS ====
	public LCicsPipeContractAddress() {
	}

	public LCicsPipeContractAddress(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_CICS_PIPE_CONTRACT_ADDRESS;
	}

	public void setlCicsPipeContractAddress(int lCicsPipeContractAddress) {
		writeBinaryInt(Pos.L_CICS_PIPE_CONTRACT_ADDRESS, lCicsPipeContractAddress);
	}

	/**Original name: L-CICS-PIPE-CONTRACT-ADDRESS<br>*/
	public int getlCicsPipeContractAddress() {
		return readBinaryInt(Pos.L_CICS_PIPE_CONTRACT_ADDRESS);
	}

	public int getlCicsPipeContractPointer() {
		return readBinaryInt(Pos.L_CICS_PIPE_CONTRACT_POINTER);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_CICS_PIPE_CONTRACT_ADDRESS = 1;
		public static final int L_CICS_PIPE_CONTRACT_POINTER = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int L_CICS_PIPE_CONTRACT_ADDRESS = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
