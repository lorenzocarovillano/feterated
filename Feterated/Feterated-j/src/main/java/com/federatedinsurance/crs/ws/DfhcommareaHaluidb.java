/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program HALUIDB<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaHaluidb extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int CA_LNK_DATA_MAXOCCURS = 32767;
	private int caLnkDataListenerSize = CA_LNK_DATA_MAXOCCURS;
	private IValueChangeListener caLnkDataListener = new CaLnkDataListener();
	//Original name: CA-LNK-DATA
	private char[] caLnkData = new char[CA_LNK_DATA_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public DfhcommareaHaluidb() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return getDfhcommareaSize();
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void init() {
		for (int caLnkDataIdx = 1; caLnkDataIdx <= CA_LNK_DATA_MAXOCCURS; caLnkDataIdx++) {
			setCaLnkData(caLnkDataIdx, DefaultValues.CHAR_VAL);
		}
	}

	public int getDfhcommareaSize() {
		return Types.CHAR_SIZE * caLnkDataListenerSize;
	}

	public String getDfhcommareaFormatted() {
		return MarshalByteExt.bufferToStr(getDfhcommareaBytes());
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[getDfhcommareaSize()];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= caLnkDataListenerSize; idx++) {
			if (position <= buffer.length) {
				setCaLnkData(idx, MarshalByte.readChar(buffer, position));
				position += Types.CHAR_SIZE;
			} else {
				setCaLnkData(idx, Types.SPACE_CHAR);
				position += Types.CHAR_SIZE;
			}
		}
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= caLnkDataListenerSize; idx++) {
			MarshalByte.writeChar(buffer, position, getCaLnkData(idx));
			position += Types.CHAR_SIZE;
		}
		return buffer;
	}

	public void initDfhcommareaSpaces() {
		for (int idx = 1; idx <= caLnkDataListenerSize; idx++) {
			caLnkData[idx - 1] = (Types.SPACE_CHAR);
		}
	}

	public void setCaLnkData(int caLnkDataIdx, char caLnkData) {
		this.caLnkData[caLnkDataIdx - 1] = caLnkData;
	}

	public char getCaLnkData(int caLnkDataIdx) {
		return this.caLnkData[caLnkDataIdx - 1];
	}

	public IValueChangeListener getCaLnkDataListener() {
		return caLnkDataListener;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	/**Original name: CA-LNK-DATA<br>*/
	public class CaLnkDataListener implements IValueChangeListener {

		//==== METHODS ====
		@Override
		public void change() {
			caLnkDataListenerSize = CA_LNK_DATA_MAXOCCURS;
		}

		@Override
		public void change(int value) {
			caLnkDataListenerSize = value < 1 ? 0 : (value > CA_LNK_DATA_MAXOCCURS ? CA_LNK_DATA_MAXOCCURS : value);
		}
	}
}
