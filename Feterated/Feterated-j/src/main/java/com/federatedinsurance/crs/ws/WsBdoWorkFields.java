/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-BDO-WORK-FIELDS<br>
 * Variable: WS-BDO-WORK-FIELDS from program CAWS002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsBdoWorkFields {

	//==== PROPERTIES ====
	//Original name: WS-UCT-ROW-COUNTER
	private short uctRowCounter = ((short) 0);
	//Original name: WS-SWITCH-TSQ-CNT
	private short switchTsqCnt = ((short) 0);
	//Original name: WS-SWITCH-OBJECT
	private String switchObject = DefaultValues.stringVal(Len.SWITCH_OBJECT);
	//Original name: WS-ORIGINAL-ACTION
	private String originalAction = "";

	//==== METHODS ====
	public void setUctRowCounter(short uctRowCounter) {
		this.uctRowCounter = uctRowCounter;
	}

	public short getUctRowCounter() {
		return this.uctRowCounter;
	}

	public void setSwitchTsqCnt(short switchTsqCnt) {
		this.switchTsqCnt = switchTsqCnt;
	}

	public short getSwitchTsqCnt() {
		return this.switchTsqCnt;
	}

	public void setSwitchObject(String switchObject) {
		this.switchObject = Functions.subString(switchObject, Len.SWITCH_OBJECT);
	}

	public String getSwitchObject() {
		return this.switchObject;
	}

	public void setOriginalAction(String originalAction) {
		this.originalAction = Functions.subString(originalAction, Len.ORIGINAL_ACTION);
	}

	public String getOriginalAction() {
		return this.originalAction;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SWITCH_OBJECT = 32;
		public static final int ORIGINAL_ACTION = 20;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
