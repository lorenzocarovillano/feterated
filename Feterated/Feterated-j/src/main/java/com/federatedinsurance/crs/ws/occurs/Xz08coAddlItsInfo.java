/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Xz08coAdr;
import com.federatedinsurance.crs.copy.Xz08coCtcNm;

/**Original name: XZ08CO-ADDL-ITS-INFO<br>
 * Variables: XZ08CO-ADDL-ITS-INFO from copybook XZ08CI1O<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xz08coAddlItsInfo {

	//==== PROPERTIES ====
	//Original name: XZ08CO-CTC-ID
	private String ctcId = DefaultValues.stringVal(Len.CTC_ID);
	//Original name: XZ08CO-CTC-NM
	private Xz08coCtcNm ctcNm = new Xz08coCtcNm();
	//Original name: XZ08CO-ADR
	private Xz08coAdr adr = new Xz08coAdr();

	//==== METHODS ====
	public void setXz08coAddlItsInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		ctcId = MarshalByte.readString(buffer, position, Len.CTC_ID);
		position += Len.CTC_ID;
		ctcNm.setCtcNmBytes(buffer, position);
		position += Xz08coCtcNm.Len.CTC_NM;
		adr.setAdrBytes(buffer, position);
	}

	public byte[] getXz08coAddlItsInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ctcId, Len.CTC_ID);
		position += Len.CTC_ID;
		ctcNm.getCtcNmBytes(buffer, position);
		position += Xz08coCtcNm.Len.CTC_NM;
		adr.getAdrBytes(buffer, position);
		return buffer;
	}

	public void initXz08coAddlItsInfoLowValues() {
		ctcId = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CTC_ID);
		ctcNm.initCtcNmLowValues();
		adr.initAdrLowValues();
	}

	public void initXz08coAddlItsInfoSpaces() {
		ctcId = "";
		ctcNm.initCtcNmSpaces();
		adr.initAdrSpaces();
	}

	public void setCtcId(String ctcId) {
		this.ctcId = Functions.subString(ctcId, Len.CTC_ID);
	}

	public String getCtcId() {
		return this.ctcId;
	}

	public Xz08coAdr getAdr() {
		return adr;
	}

	public Xz08coCtcNm getCtcNm() {
		return ctcNm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CTC_ID = 20;
		public static final int XZ08CO_ADDL_ITS_INFO = CTC_ID + Xz08coCtcNm.Len.CTC_NM + Xz08coAdr.Len.ADR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
