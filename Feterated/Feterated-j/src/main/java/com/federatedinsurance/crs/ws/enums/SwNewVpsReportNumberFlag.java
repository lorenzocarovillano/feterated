/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-NEW-VPS-REPORT-NUMBER-FLAG<br>
 * Variable: SW-NEW-VPS-REPORT-NUMBER-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwNewVpsReportNumberFlag {

	//==== PROPERTIES ====
	private char value = '0';
	public static final char SAME_VPS_REPORT_NUMBER = '0';
	public static final char NEW_VPS_REPORT_NUMBER = '1';

	//==== METHODS ====
	public void setNewVpsReportNumberFlag(char newVpsReportNumberFlag) {
		this.value = newVpsReportNumberFlag;
	}

	public char getNewVpsReportNumberFlag() {
		return this.value;
	}

	public void setSameVpsReportNumber() {
		value = SAME_VPS_REPORT_NUMBER;
	}

	public boolean isNewVpsReportNumber() {
		return value == NEW_VPS_REPORT_NUMBER;
	}

	public void setNewVpsReportNumber() {
		value = NEW_VPS_REPORT_NUMBER;
	}
}
