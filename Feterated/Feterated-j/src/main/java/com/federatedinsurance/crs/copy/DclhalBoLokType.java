/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalBoLokTypeV;

/**Original name: DCLHAL-BO-LOK-TYPE<br>
 * Variable: DCLHAL-BO-LOK-TYPE from copybook HALLGBLT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalBoLokType implements IHalBoLokTypeV {

	//==== PROPERTIES ====
	//Original name: HBLT-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HBLT-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: HBLT-LOK-TYPE-CD
	private char lokTypeCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setLokTypeCd(char lokTypeCd) {
		this.lokTypeCd = lokTypeCd;
	}

	public void setLokTypeCdFormatted(String lokTypeCd) {
		setLokTypeCd(Functions.charAt(lokTypeCd, Types.CHAR_SIZE));
	}

	public char getLokTypeCd() {
		return this.lokTypeCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NM = 32;
		public static final int BUS_OBJ_NM = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
