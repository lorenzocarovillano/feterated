/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.WsEndOfCursor1Sw;
import com.federatedinsurance.crs.ws.enums.WsLegacyLockModSw;
import com.federatedinsurance.crs.ws.enums.WsUpgradeValiditySw;

/**Original name: WS-SWITCHES<br>
 * Variable: WS-SWITCHES from program HALRLODR<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSwitches {

	//==== PROPERTIES ====
	//Original name: WS-END-OF-CURSOR1-SW
	private WsEndOfCursor1Sw endOfCursor1Sw = new WsEndOfCursor1Sw();
	//Original name: WS-LEGACY-LOCK-MOD-SW
	private WsLegacyLockModSw legacyLockModSw = new WsLegacyLockModSw();
	//Original name: WS-UPGRADE-VALIDITY-SW
	private WsUpgradeValiditySw upgradeValiditySw = new WsUpgradeValiditySw();

	//==== METHODS ====
	public WsEndOfCursor1Sw getEndOfCursor1Sw() {
		return endOfCursor1Sw;
	}

	public WsLegacyLockModSw getLegacyLockModSw() {
		return legacyLockModSw;
	}

	public WsUpgradeValiditySw getUpgradeValiditySw() {
		return upgradeValiditySw;
	}
}
