/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IFedTobCodeV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [FED_TOB_CODE_V]
 * 
 */
public class FedTobCodeVDao extends BaseSqlDao<IFedTobCodeV> {

	public FedTobCodeVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IFedTobCodeV> getToClass() {
		return IFedTobCodeV.class;
	}

	public String selectByTobCd(String tobCd, String dft) {
		return buildQuery("selectByTobCd").bind("tobCd", tobCd).scalarResultString(dft);
	}
}
