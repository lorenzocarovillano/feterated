/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.ReturnException;
import com.federatedinsurance.crs.ws.LFdData;
import com.federatedinsurance.crs.ws.LFdReportNmbr;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;

/**Original name: TS514099<br>
 * <pre>                                                                    LV001
 *         RENAMED AND CONVERTED TO COBOL II - 10/95                TS514099
 *                                                                  TS514099
 *             TITLE -   FD DDNAME MODIFIER                         TS514099
 *                                                                  TS514099
 *           THIS PROGRAM MODIFIES THE DDNAME ASSIGNED TO THE FD    TS514099
 *         PASSED TO THIS PROGRAM BY THE CALLING PROGRAM.           TS514099
 *           THIS IS DONE BY REPLACING THE DDNAME WITHIN THE DTF    TS514099
 *         WITH THE REPORT-NMBR(USED AS DDNAME) SUPPLIED BY         TS514099
 *         CALLING PROGRAM.                                         TS514099
 *           CALLING PROGRAM MUST ISSUE AN OPEN STATEMENT FOR       TS514099
 *         FD IMMEDIATELY AFTER CALLING THIS PROGRAM.               TS514099
 *           ASSUMPTION IS MADE BY THIS PROGRAM THAT CALLING        TS514099
 *         PROGRAM COMPLETES PRINTING OF EACH REPORT BEFORE         TS514099
 *         STARTING A NEW REPORT.                                   TS514099</pre>*/
public class Ts514099 extends Program {

	//==== PROPERTIES ====
	//Original name: L-FD-REPORT-NMBR
	private LFdReportNmbr lFdReportNmbr;
	//Original name: L-FD-DATA
	private LFdData lFdData;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LFdReportNmbr lFdReportNmbr, LFdData lFdData) {
		this.lFdReportNmbr = lFdReportNmbr;
		this.lFdData = lFdData;
		programControl();
		programExit();
		return 0;
	}

	public static Ts514099 getInstance() {
		return (Programs.getInstance(Ts514099.class));
	}

	/**Original name: 1000-PROGRAM-CONTROL<br>*/
	private void programControl() {
		// COB_CODE: MOVE  L-FD-REPORT-NMBR      TO  L-FD-DDNAME.
		lFdData.setlFdDdname(lFdReportNmbr.getlFdReportNmbrFormatted());
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}
}
