/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.Dfhxcplo;
import com.federatedinsurance.crs.copy.Dfhxcrco;
import com.federatedinsurance.crs.ws.enums.SwErrorFoundFlag;
import com.federatedinsurance.crs.ws.enums.SwRetryConnectionFlag;
import com.federatedinsurance.crs.ws.occurs.TbRaInf;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS548099<br>
 * Generated as a class for rule WS.<br>*/
public class Ts548099Data {

	//==== PROPERTIES ====
	public static final int TB_RA_INF_MAXOCCURS = 3;
	//Original name: CONSTANT-FIELDS
	private ConstantFields constantFields = new ConstantFields();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesTs548099 errorAndAdviceMessages = new ErrorAndAdviceMessagesTs548099();
	//Original name: SAVE-AREA
	private SaveArea saveArea = new SaveArea();
	//Original name: SUBSCRIPTS
	private SubscriptsTs548099 subscripts = new SubscriptsTs548099();
	//Original name: SW-ERROR-FOUND-FLAG
	private SwErrorFoundFlag swErrorFoundFlag = new SwErrorFoundFlag();
	//Original name: SW-RETRY-CONNECTION-FLAG
	private SwRetryConnectionFlag swRetryConnectionFlag = new SwRetryConnectionFlag();
	//Original name: TB-RA-INF
	private TbRaInf[] tbRaInf = new TbRaInf[TB_RA_INF_MAXOCCURS];
	//Original name: DFHXCPLO
	private Dfhxcplo dfhxcplo = new Dfhxcplo();
	//Original name: DFHXCRCO
	private Dfhxcrco dfhxcrco = new Dfhxcrco();

	//==== CONSTRUCTORS ====
	public Ts548099Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int tbRaInfIdx = 1; tbRaInfIdx <= TB_RA_INF_MAXOCCURS; tbRaInfIdx++) {
			tbRaInf[tbRaInfIdx - 1] = new TbRaInf();
		}
	}

	public ConstantFields getConstantFields() {
		return constantFields;
	}

	public Dfhxcplo getDfhxcplo() {
		return dfhxcplo;
	}

	public Dfhxcrco getDfhxcrco() {
		return dfhxcrco;
	}

	public ErrorAndAdviceMessagesTs548099 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public SaveArea getSaveArea() {
		return saveArea;
	}

	public SubscriptsTs548099 getSubscripts() {
		return subscripts;
	}

	public SwErrorFoundFlag getSwErrorFoundFlag() {
		return swErrorFoundFlag;
	}

	public SwRetryConnectionFlag getSwRetryConnectionFlag() {
		return swRetryConnectionFlag;
	}

	public TbRaInf getTbRaInf(int idx) {
		return tbRaInf[idx - 1];
	}
}
