/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpAccessStatus;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.federatedinsurance.crs.ws.DfhcommareaTs571098;
import com.federatedinsurance.crs.ws.Ts571098Data;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: TS571098<br>
 * <pre>AUTHOR.        J. PROFT.
 * DATE-WRITTEN.  MAY 2014
 *        TITLE - CALLABLE SECURITY COMMON ROUTINE
 *      PROCESS - THIS PROGRAM CONTAINS THE LOGIC NEEDED TO
 *                GET A USERID AND PASSWORD FROM A SECURED FILE ON
 *                A CICS REGION.  SEE T&L COMMON ROUTINE DOCUMENT
 *                FOR INSTRUCTIONS IN CALLING THIS SERVICE.
 *        INPUT - TSQ NAME PASSED IN VIA LINKAGE.
 *       OUTPUT - USERID AND PASSWORD FROM THE TSQ SPECIFIED.
 *     MAINTENANCE HISTORY
 *     *******************
 *     INFO    CHANGE
 *     NMBR     DATE    III  DESCRIPTION
 *     *****  ********  ***  **************************************
 *     TL539  05/01/14  JSP  CREATED.</pre>*/
public class Ts571098 extends Program {

	//==== PROPERTIES ====
	private ExecContext execContext = null;
	//Original name: WORKING-STORAGE
	private Ts571098Data ws = new Ts571098Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaTs571098 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaTs571098 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		getCredentials();
		programExit();
		return 0;
	}

	public static Ts571098 getInstance() {
		return (Programs.getInstance(Ts571098.class));
	}

	/**Original name: 1000-GET-CREDENTIALS<br>*/
	private void getCredentials() {
		TpOutputData tsQueueData = null;
		// COB_CODE: INITIALIZE SA-RESP-CODE
		//                      SA-RESP2-CODE
		//                      UI-ROUTINE-OUTPUTS.
		ws.setSaRespCode(0);
		ws.setSaResp2Code(0);
		initRoutineOutputs();
		//    GET THE USER NAME AND CREDENTIALS FROM TSQ
		// COB_CODE: EXEC CICS READQ
		//               TS QNAME    (UI-TSQ-NAME)
		//               INTO        (UI-ROUTINE-OUTPUTS)
		//               LENGTH      (LENGTH OF UI-ROUTINE-OUTPUTS)
		//               ITEM        (SA-ITEMNUM)
		//               NUMITEMS    (SA-NUM-ITEMS)
		//               RESP        (SA-RESP-CODE)
		//               RESP2       (SA-RESP2-CODE)
		//               NOHANDLE
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(DfhcommareaTs571098.Len.ROUTINE_OUTPUTS);
		TsQueueManager.read(execContext, dfhcommarea.getTsqNameFormatted(), ((short) (ws.getSaItemnum())), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			dfhcommarea.setRoutineOutputsBytes(tsQueueData.getData());
			ws.setSaNumItems(tsQueueData.getNumOfItems());
		}
		ws.setSaRespCode(execContext.getResp());
		ws.setSaResp2Code(execContext.getResp2());
		//    CHECK FOR CICS RESPONSE ERRORS
		// COB_CODE: IF SA-RESP-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 1000-PROGRAM-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaRespCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE UI-TSQ-NAME        TO EA-01-TSQ
			ws.getEa01TsqError().setTsq(dfhcommarea.getTsqName());
			// COB_CODE: MOVE SA-RESP-CODE       TO EA-01-RESP
			ws.getEa01TsqError().setResp(ws.getSaRespCodeFormatted());
			// COB_CODE: MOVE SA-RESP2-CODE      TO EA-01-RESP2
			ws.getEa01TsqError().setResp2(ws.getSaResp2CodeFormatted());
			// COB_CODE: MOVE EA-01-TSQ-ERROR    TO UI-ERROR
			dfhcommarea.setError(ws.getEa01TsqError().getEa01TsqErrorFormatted());
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
		//    CHECK THAT CREDENTIALS WERE RETURNED FROM CICS CALL
		// COB_CODE: IF UI-ROUTINE-OUTPUTS = SPACES
		//               MOVE EA-02-EMPTY-TSQ    TO UI-ERROR
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getRoutineOutputsBytes())) {
			// COB_CODE: MOVE EA-02-EMPTY-TSQ    TO UI-ERROR
			dfhcommarea.setError(ws.getEa02EmptyTsqFormatted());
		}
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
		// COB_CODE: EXIT.
		//exit
	}

	public void initRoutineOutputs() {
		dfhcommarea.setUserId("");
		dfhcommarea.setPassword("");
		dfhcommarea.setError("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
