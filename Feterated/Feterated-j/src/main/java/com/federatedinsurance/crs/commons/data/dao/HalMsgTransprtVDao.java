/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalMsgTransprtV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_MSG_TRANSPRT_V]
 * 
 */
public class HalMsgTransprtVDao extends BaseSqlDao<IHalMsgTransprtV> {

	private final IRowMapper<IHalMsgTransprtV> selectByHmtcUowNmRm = buildNamedRowMapper(IHalMsgTransprtV.class, "hmtcUowNm", "msgDelTm", "wngDelTm",
			"passLnkInd", "stgTypCd", "mdrvReqStg", "mdrvRspStg", "uowReqStg", "uowSwiStg", "uowHdrStg", "uowDtaStg", "uowWngStg", "uowKrpStg",
			"uowNlbeStg");

	public HalMsgTransprtVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalMsgTransprtV> getToClass() {
		return IHalMsgTransprtV.class;
	}

	public String selectByWsUowNm(String wsUowNm, String dft) {
		return buildQuery("selectByWsUowNm").bind("wsUowNm", wsUowNm).scalarResultString(dft);
	}

	public IHalMsgTransprtV selectByHmtcUowNm(String hmtcUowNm, IHalMsgTransprtV iHalMsgTransprtV) {
		return buildQuery("selectByHmtcUowNm").bind("hmtcUowNm", hmtcUowNm).rowMapper(selectByHmtcUowNmRm).singleResult(iHalMsgTransprtV);
	}
}
