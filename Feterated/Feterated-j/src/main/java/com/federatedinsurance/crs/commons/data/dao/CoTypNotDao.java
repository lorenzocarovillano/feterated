/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.ICoTypNot;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [CO_TYP, NOT_CO_TYP]
 * 
 */
public class CoTypNotDao extends BaseSqlDao<ICoTypNot> {

	private Cursor notCoCsr;
	private final IRowMapper<ICoTypNot> fetchNotCoCsrRm = buildNamedRowMapper(ICoTypNot.class, "actNotTypCd", "coCd", "coDes", "speCrtCdObj",
			"coOfsNbr", "dtbCd");

	public CoTypNotDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ICoTypNot> getToClass() {
		return ICoTypNot.class;
	}

	public DbAccessStatus openNotCoCsr(char s) {
		notCoCsr = buildQuery("openNotCoCsr").bind("s", String.valueOf(s)).open();
		return dbStatus;
	}

	public ICoTypNot fetchNotCoCsr(ICoTypNot iCoTypNot) {
		return fetch(notCoCsr, iCoTypNot, fetchNotCoCsrRm);
	}

	public DbAccessStatus closeNotCoCsr() {
		return closeCursor(notCoCsr);
	}
}
