/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: TU-POLICY-NBRS<br>
 * Variable: TU-POLICY-NBRS from program XZ0P90E0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TuPolicyNbrs {

	//==== PROPERTIES ====
	public static final String END_OF_POL_NBRS = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POLICY_NBRS);
	//Original name: TU-POLICY-NUMBER
	private String policyNumber = DefaultValues.stringVal(Len.POLICY_NUMBER);

	//==== METHODS ====
	public String getPolicyNbrsFormatted() {
		return getPolicyNumberFormatted();
	}

	public byte[] getPolicyNbrsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, policyNumber, Len.POLICY_NUMBER);
		return buffer;
	}

	public void initPolicyNbrsHighValues() {
		policyNumber = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POLICY_NUMBER);
	}

	public boolean isEndOfPolNbrs() {
		return Functions.trimAfter(getPolicyNbrsFormatted()).equals(END_OF_POL_NBRS);
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = Functions.subString(policyNumber, Len.POLICY_NUMBER);
	}

	public String getPolicyNumber() {
		return this.policyNumber;
	}

	public String getPolicyNumberFormatted() {
		return Functions.padBlanks(getPolicyNumber(), Len.POLICY_NUMBER);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POLICY_NUMBER = 25;
		public static final int POLICY_NBRS = POLICY_NUMBER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
