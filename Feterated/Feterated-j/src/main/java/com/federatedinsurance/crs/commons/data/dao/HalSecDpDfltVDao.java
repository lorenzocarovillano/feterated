/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalSecDpDfltV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_SEC_DP_DFLT_V]
 * 
 */
public class HalSecDpDfltVDao extends BaseSqlDao<IHalSecDpDfltV> {

	private Cursor wsCursor1;
	private final IRowMapper<IHalSecDpDfltV> fetchWsCursor1Rm = buildNamedRowMapper(IHalSecDpDfltV.class, "uowNm", "secGrpNm", "secAscTyp",
			"genTxtFld1", "genTxtFld2", "genTxtFld3", "genTxtFld4", "genTxtFld5", "busObjNm", "reqTypCd", "effectiveDt", "expirationDt", "seqNbr",
			"cmnNm", "cmnAtrInd", "dflTypInd", "dflDtaAmtObj", "dflDtaTxt", "dflOfsNbrObj", "dflOfsPer", "context");

	public HalSecDpDfltVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalSecDpDfltV> getToClass() {
		return IHalSecDpDfltV.class;
	}

	public DbAccessStatus openWsCursor1(IHalSecDpDfltV iHalSecDpDfltV) {
		wsCursor1 = buildQuery("openWsCursor1").bind(iHalSecDpDfltV).open();
		return dbStatus;
	}

	public DbAccessStatus closeWsCursor1() {
		return closeCursor(wsCursor1);
	}

	public IHalSecDpDfltV fetchWsCursor1(IHalSecDpDfltV iHalSecDpDfltV) {
		return fetch(wsCursor1, iHalSecDpDfltV, fetchWsCursor1Rm);
	}
}
