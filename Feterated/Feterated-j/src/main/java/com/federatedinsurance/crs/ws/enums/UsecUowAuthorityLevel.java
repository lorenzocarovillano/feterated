/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: USEC-UOW-AUTHORITY-LEVEL<br>
 * Variable: USEC-UOW-AUTHORITY-LEVEL from copybook HALLUSEC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UsecUowAuthorityLevel {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.UOW_AUTHORITY_LEVEL);
	public static final String AUT_NBR_UNKNOWN = "00000";
	public static final String SUPER_USER = "00100";

	//==== METHODS ====
	public void setUowAuthorityLevel(int uowAuthorityLevel) {
		this.value = NumericDisplay.asString(uowAuthorityLevel, Len.UOW_AUTHORITY_LEVEL);
	}

	public int getUowAuthorityLevel() {
		return NumericDisplay.asInt(this.value);
	}

	public String getUowAuthorityLevelFormatted() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_AUTHORITY_LEVEL = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
