/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-40-GEN-RETRYABLE-MSG<br>
 * Variable: EA-40-GEN-RETRYABLE-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea40GenRetryableMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-40-GEN-RETRYABLE-MSG
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-40-GEN-RETRYABLE-MSG-1
	private String flr2 = "AN EXCI";
	//Original name: FILLER-EA-40-GEN-RETRYABLE-MSG-2
	private String flr3 = "RETRYABLE ERROR";
	//Original name: FILLER-EA-40-GEN-RETRYABLE-MSG-3
	private String flr4 = " CODE WAS";
	//Original name: FILLER-EA-40-GEN-RETRYABLE-MSG-4
	private String flr5 = "CAPTURED.";
	//Original name: FILLER-EA-40-GEN-RETRYABLE-MSG-5
	private String flr6 = "PLEASE CORRECT.";

	//==== METHODS ====
	public String getEa40GenRetryableMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa40GenRetryableMsgBytes());
	}

	public byte[] getEa40GenRetryableMsgBytes() {
		byte[] buffer = new byte[Len.EA40_GEN_RETRYABLE_MSG];
		return getEa40GenRetryableMsgBytes(buffer, 1);
	}

	public byte[] getEa40GenRetryableMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 8;
		public static final int FLR3 = 15;
		public static final int FLR4 = 10;
		public static final int EA40_GEN_RETRYABLE_MSG = 2 * FLR1 + FLR2 + 2 * FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
