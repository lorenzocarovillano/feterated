/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0006<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0006 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0006() {
	}

	public LServiceContractAreaXz0x0006(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt006ServiceInputsBytes(byte[] buffer) {
		setXzt006ServiceInputsBytes(buffer, 1);
	}

	public void setXzt006ServiceInputsBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.XZT006_SERVICE_INPUTS, Pos.XZT006_SERVICE_INPUTS);
	}

	public void setXzt06iTkNotPrcTs(String xzt06iTkNotPrcTs) {
		writeString(Pos.XZT06I_TK_NOT_PRC_TS, xzt06iTkNotPrcTs, Len.XZT06I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT06I-TK-NOT-PRC-TS<br>*/
	public String getXzt06iTkNotPrcTs() {
		return readString(Pos.XZT06I_TK_NOT_PRC_TS, Len.XZT06I_TK_NOT_PRC_TS);
	}

	public void setXzt06iTkActOwnCltId(String xzt06iTkActOwnCltId) {
		writeString(Pos.XZT06I_TK_ACT_OWN_CLT_ID, xzt06iTkActOwnCltId, Len.XZT06I_TK_ACT_OWN_CLT_ID);
	}

	/**Original name: XZT06I-TK-ACT-OWN-CLT-ID<br>*/
	public String getXzt06iTkActOwnCltId() {
		return readString(Pos.XZT06I_TK_ACT_OWN_CLT_ID, Len.XZT06I_TK_ACT_OWN_CLT_ID);
	}

	public void setXzt06iTkActOwnAdrId(String xzt06iTkActOwnAdrId) {
		writeString(Pos.XZT06I_TK_ACT_OWN_ADR_ID, xzt06iTkActOwnAdrId, Len.XZT06I_TK_ACT_OWN_ADR_ID);
	}

	/**Original name: XZT06I-TK-ACT-OWN-ADR-ID<br>*/
	public String getXzt06iTkActOwnAdrId() {
		return readString(Pos.XZT06I_TK_ACT_OWN_ADR_ID, Len.XZT06I_TK_ACT_OWN_ADR_ID);
	}

	public void setXzt06iTkEmpId(String xzt06iTkEmpId) {
		writeString(Pos.XZT06I_TK_EMP_ID, xzt06iTkEmpId, Len.XZT06I_TK_EMP_ID);
	}

	/**Original name: XZT06I-TK-EMP-ID<br>*/
	public String getXzt06iTkEmpId() {
		return readString(Pos.XZT06I_TK_EMP_ID, Len.XZT06I_TK_EMP_ID);
	}

	public void setXzt06iTkStaMdfTs(String xzt06iTkStaMdfTs) {
		writeString(Pos.XZT06I_TK_STA_MDF_TS, xzt06iTkStaMdfTs, Len.XZT06I_TK_STA_MDF_TS);
	}

	/**Original name: XZT06I-TK-STA-MDF-TS<br>*/
	public String getXzt06iTkStaMdfTs() {
		return readString(Pos.XZT06I_TK_STA_MDF_TS, Len.XZT06I_TK_STA_MDF_TS);
	}

	public void setXzt06iTkActNotStaCd(String xzt06iTkActNotStaCd) {
		writeString(Pos.XZT06I_TK_ACT_NOT_STA_CD, xzt06iTkActNotStaCd, Len.XZT06I_TK_ACT_NOT_STA_CD);
	}

	/**Original name: XZT06I-TK-ACT-NOT-STA-CD<br>*/
	public String getXzt06iTkActNotStaCd() {
		return readString(Pos.XZT06I_TK_ACT_NOT_STA_CD, Len.XZT06I_TK_ACT_NOT_STA_CD);
	}

	public void setXzt06iTkSegCd(String xzt06iTkSegCd) {
		writeString(Pos.XZT06I_TK_SEG_CD, xzt06iTkSegCd, Len.XZT06I_TK_SEG_CD);
	}

	/**Original name: XZT06I-TK-SEG-CD<br>*/
	public String getXzt06iTkSegCd() {
		return readString(Pos.XZT06I_TK_SEG_CD, Len.XZT06I_TK_SEG_CD);
	}

	public void setXzt06iTkActTypCd(String xzt06iTkActTypCd) {
		writeString(Pos.XZT06I_TK_ACT_TYP_CD, xzt06iTkActTypCd, Len.XZT06I_TK_ACT_TYP_CD);
	}

	/**Original name: XZT06I-TK-ACT-TYP-CD<br>*/
	public String getXzt06iTkActTypCd() {
		return readString(Pos.XZT06I_TK_ACT_TYP_CD, Len.XZT06I_TK_ACT_TYP_CD);
	}

	public void setXzt06iTkActNotCsumFormatted(String xzt06iTkActNotCsum) {
		writeString(Pos.XZT06I_TK_ACT_NOT_CSUM, Trunc.toUnsignedNumeric(xzt06iTkActNotCsum, Len.XZT06I_TK_ACT_NOT_CSUM), Len.XZT06I_TK_ACT_NOT_CSUM);
	}

	/**Original name: XZT06I-TK-ACT-NOT-CSUM<br>*/
	public int getXzt06iTkActNotCsum() {
		return readNumDispUnsignedInt(Pos.XZT06I_TK_ACT_NOT_CSUM, Len.XZT06I_TK_ACT_NOT_CSUM);
	}

	public String getXzt06iTkActNotCsumFormatted() {
		return readFixedString(Pos.XZT06I_TK_ACT_NOT_CSUM, Len.XZT06I_TK_ACT_NOT_CSUM);
	}

	public void setXzt06iCsrActNbr(String xzt06iCsrActNbr) {
		writeString(Pos.XZT06I_CSR_ACT_NBR, xzt06iCsrActNbr, Len.XZT06I_CSR_ACT_NBR);
	}

	/**Original name: XZT06I-CSR-ACT-NBR<br>*/
	public String getXzt06iCsrActNbr() {
		return readString(Pos.XZT06I_CSR_ACT_NBR, Len.XZT06I_CSR_ACT_NBR);
	}

	public void setXzt06iActNotTypCd(String xzt06iActNotTypCd) {
		writeString(Pos.XZT06I_ACT_NOT_TYP_CD, xzt06iActNotTypCd, Len.XZT06I_ACT_NOT_TYP_CD);
	}

	/**Original name: XZT06I-ACT-NOT-TYP-CD<br>*/
	public String getXzt06iActNotTypCd() {
		return readString(Pos.XZT06I_ACT_NOT_TYP_CD, Len.XZT06I_ACT_NOT_TYP_CD);
	}

	public void setXzt06iNotDt(String xzt06iNotDt) {
		writeString(Pos.XZT06I_NOT_DT, xzt06iNotDt, Len.XZT06I_NOT_DT);
	}

	/**Original name: XZT06I-NOT-DT<br>*/
	public String getXzt06iNotDt() {
		return readString(Pos.XZT06I_NOT_DT, Len.XZT06I_NOT_DT);
	}

	public void setXzt06iPdcNbr(String xzt06iPdcNbr) {
		writeString(Pos.XZT06I_PDC_NBR, xzt06iPdcNbr, Len.XZT06I_PDC_NBR);
	}

	/**Original name: XZT06I-PDC-NBR<br>*/
	public String getXzt06iPdcNbr() {
		return readString(Pos.XZT06I_PDC_NBR, Len.XZT06I_PDC_NBR);
	}

	public void setXzt06iPdcNm(String xzt06iPdcNm) {
		writeString(Pos.XZT06I_PDC_NM, xzt06iPdcNm, Len.XZT06I_PDC_NM);
	}

	/**Original name: XZT06I-PDC-NM<br>*/
	public String getXzt06iPdcNm() {
		return readString(Pos.XZT06I_PDC_NM, Len.XZT06I_PDC_NM);
	}

	public void setXzt06iTotFeeAmt(AfDecimal xzt06iTotFeeAmt) {
		writeDecimal(Pos.XZT06I_TOT_FEE_AMT, xzt06iTotFeeAmt.copy());
	}

	/**Original name: XZT06I-TOT-FEE-AMT<br>*/
	public AfDecimal getXzt06iTotFeeAmt() {
		return readDecimal(Pos.XZT06I_TOT_FEE_AMT, Len.Int.XZT06I_TOT_FEE_AMT, Len.Fract.XZT06I_TOT_FEE_AMT);
	}

	public void setXzt06iStAbb(String xzt06iStAbb) {
		writeString(Pos.XZT06I_ST_ABB, xzt06iStAbb, Len.XZT06I_ST_ABB);
	}

	/**Original name: XZT06I-ST-ABB<br>*/
	public String getXzt06iStAbb() {
		return readString(Pos.XZT06I_ST_ABB, Len.XZT06I_ST_ABB);
	}

	public void setXzt06iCerHldNotInd(char xzt06iCerHldNotInd) {
		writeChar(Pos.XZT06I_CER_HLD_NOT_IND, xzt06iCerHldNotInd);
	}

	/**Original name: XZT06I-CER-HLD-NOT-IND<br>*/
	public char getXzt06iCerHldNotInd() {
		return readChar(Pos.XZT06I_CER_HLD_NOT_IND);
	}

	public void setXzt06iAddCncDay(int xzt06iAddCncDay) {
		writeInt(Pos.XZT06I_ADD_CNC_DAY, xzt06iAddCncDay, Len.Int.XZT06I_ADD_CNC_DAY);
	}

	/**Original name: XZT06I-ADD-CNC-DAY<br>*/
	public int getXzt06iAddCncDay() {
		return readNumDispInt(Pos.XZT06I_ADD_CNC_DAY, Len.XZT06I_ADD_CNC_DAY);
	}

	public String getXzt06iAddCncDayFormatted() {
		return readFixedString(Pos.XZT06I_ADD_CNC_DAY, Len.XZT06I_ADD_CNC_DAY);
	}

	public void setXzt06iReaDes(String xzt06iReaDes) {
		writeString(Pos.XZT06I_REA_DES, xzt06iReaDes, Len.XZT06I_REA_DES);
	}

	/**Original name: XZT06I-REA-DES<br>*/
	public String getXzt06iReaDes() {
		return readString(Pos.XZT06I_REA_DES, Len.XZT06I_REA_DES);
	}

	public void setXzt06iUserid(String xzt06iUserid) {
		writeString(Pos.XZT06I_USERID, xzt06iUserid, Len.XZT06I_USERID);
	}

	/**Original name: XZT06I-USERID<br>*/
	public String getXzt06iUserid() {
		return readString(Pos.XZT06I_USERID, Len.XZT06I_USERID);
	}

	public String getXzt06iUseridFormatted() {
		return Functions.padBlanks(getXzt06iUserid(), Len.XZT06I_USERID);
	}

	/**Original name: XZT006-SERVICE-OUTPUTS<br>*/
	public byte[] getXzt006ServiceOutputsBytes() {
		byte[] buffer = new byte[Len.XZT006_SERVICE_OUTPUTS];
		return getXzt006ServiceOutputsBytes(buffer, 1);
	}

	public byte[] getXzt006ServiceOutputsBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.XZT006_SERVICE_OUTPUTS, Pos.XZT006_SERVICE_OUTPUTS);
		return buffer;
	}

	public void setXzt06oTkNotPrcTs(String xzt06oTkNotPrcTs) {
		writeString(Pos.XZT06O_TK_NOT_PRC_TS, xzt06oTkNotPrcTs, Len.XZT06O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT06O-TK-NOT-PRC-TS<br>*/
	public String getXzt06oTkNotPrcTs() {
		return readString(Pos.XZT06O_TK_NOT_PRC_TS, Len.XZT06O_TK_NOT_PRC_TS);
	}

	public void setXzt06oTkActOwnCltId(String xzt06oTkActOwnCltId) {
		writeString(Pos.XZT06O_TK_ACT_OWN_CLT_ID, xzt06oTkActOwnCltId, Len.XZT06O_TK_ACT_OWN_CLT_ID);
	}

	/**Original name: XZT06O-TK-ACT-OWN-CLT-ID<br>*/
	public String getXzt06oTkActOwnCltId() {
		return readString(Pos.XZT06O_TK_ACT_OWN_CLT_ID, Len.XZT06O_TK_ACT_OWN_CLT_ID);
	}

	public void setXzt06oTkActOwnAdrId(String xzt06oTkActOwnAdrId) {
		writeString(Pos.XZT06O_TK_ACT_OWN_ADR_ID, xzt06oTkActOwnAdrId, Len.XZT06O_TK_ACT_OWN_ADR_ID);
	}

	/**Original name: XZT06O-TK-ACT-OWN-ADR-ID<br>*/
	public String getXzt06oTkActOwnAdrId() {
		return readString(Pos.XZT06O_TK_ACT_OWN_ADR_ID, Len.XZT06O_TK_ACT_OWN_ADR_ID);
	}

	public void setXzt06oTkEmpId(String xzt06oTkEmpId) {
		writeString(Pos.XZT06O_TK_EMP_ID, xzt06oTkEmpId, Len.XZT06O_TK_EMP_ID);
	}

	/**Original name: XZT06O-TK-EMP-ID<br>*/
	public String getXzt06oTkEmpId() {
		return readString(Pos.XZT06O_TK_EMP_ID, Len.XZT06O_TK_EMP_ID);
	}

	public void setXzt06oTkStaMdfTs(String xzt06oTkStaMdfTs) {
		writeString(Pos.XZT06O_TK_STA_MDF_TS, xzt06oTkStaMdfTs, Len.XZT06O_TK_STA_MDF_TS);
	}

	/**Original name: XZT06O-TK-STA-MDF-TS<br>*/
	public String getXzt06oTkStaMdfTs() {
		return readString(Pos.XZT06O_TK_STA_MDF_TS, Len.XZT06O_TK_STA_MDF_TS);
	}

	public void setXzt06oTkActNotStaCd(String xzt06oTkActNotStaCd) {
		writeString(Pos.XZT06O_TK_ACT_NOT_STA_CD, xzt06oTkActNotStaCd, Len.XZT06O_TK_ACT_NOT_STA_CD);
	}

	/**Original name: XZT06O-TK-ACT-NOT-STA-CD<br>*/
	public String getXzt06oTkActNotStaCd() {
		return readString(Pos.XZT06O_TK_ACT_NOT_STA_CD, Len.XZT06O_TK_ACT_NOT_STA_CD);
	}

	public void setXzt06oTkSegCd(String xzt06oTkSegCd) {
		writeString(Pos.XZT06O_TK_SEG_CD, xzt06oTkSegCd, Len.XZT06O_TK_SEG_CD);
	}

	/**Original name: XZT06O-TK-SEG-CD<br>*/
	public String getXzt06oTkSegCd() {
		return readString(Pos.XZT06O_TK_SEG_CD, Len.XZT06O_TK_SEG_CD);
	}

	public void setXzt06oTkActTypCd(String xzt06oTkActTypCd) {
		writeString(Pos.XZT06O_TK_ACT_TYP_CD, xzt06oTkActTypCd, Len.XZT06O_TK_ACT_TYP_CD);
	}

	/**Original name: XZT06O-TK-ACT-TYP-CD<br>*/
	public String getXzt06oTkActTypCd() {
		return readString(Pos.XZT06O_TK_ACT_TYP_CD, Len.XZT06O_TK_ACT_TYP_CD);
	}

	public void setXzt06oTkActNotCsumFormatted(String xzt06oTkActNotCsum) {
		writeString(Pos.XZT06O_TK_ACT_NOT_CSUM, Trunc.toUnsignedNumeric(xzt06oTkActNotCsum, Len.XZT06O_TK_ACT_NOT_CSUM), Len.XZT06O_TK_ACT_NOT_CSUM);
	}

	/**Original name: XZT06O-TK-ACT-NOT-CSUM<br>*/
	public int getXzt06oTkActNotCsum() {
		return readNumDispUnsignedInt(Pos.XZT06O_TK_ACT_NOT_CSUM, Len.XZT06O_TK_ACT_NOT_CSUM);
	}

	public void setXzt06oCsrActNbr(String xzt06oCsrActNbr) {
		writeString(Pos.XZT06O_CSR_ACT_NBR, xzt06oCsrActNbr, Len.XZT06O_CSR_ACT_NBR);
	}

	/**Original name: XZT06O-CSR-ACT-NBR<br>*/
	public String getXzt06oCsrActNbr() {
		return readString(Pos.XZT06O_CSR_ACT_NBR, Len.XZT06O_CSR_ACT_NBR);
	}

	public void setXzt06oActNotTypCd(String xzt06oActNotTypCd) {
		writeString(Pos.XZT06O_ACT_NOT_TYP_CD, xzt06oActNotTypCd, Len.XZT06O_ACT_NOT_TYP_CD);
	}

	/**Original name: XZT06O-ACT-NOT-TYP-CD<br>*/
	public String getXzt06oActNotTypCd() {
		return readString(Pos.XZT06O_ACT_NOT_TYP_CD, Len.XZT06O_ACT_NOT_TYP_CD);
	}

	public void setXzt06oActNotTypDesc(String xzt06oActNotTypDesc) {
		writeString(Pos.XZT06O_ACT_NOT_TYP_DESC, xzt06oActNotTypDesc, Len.XZT06O_ACT_NOT_TYP_DESC);
	}

	/**Original name: XZT06O-ACT-NOT-TYP-DESC<br>*/
	public String getXzt06oActNotTypDesc() {
		return readString(Pos.XZT06O_ACT_NOT_TYP_DESC, Len.XZT06O_ACT_NOT_TYP_DESC);
	}

	public void setXzt06oNotDt(String xzt06oNotDt) {
		writeString(Pos.XZT06O_NOT_DT, xzt06oNotDt, Len.XZT06O_NOT_DT);
	}

	/**Original name: XZT06O-NOT-DT<br>*/
	public String getXzt06oNotDt() {
		return readString(Pos.XZT06O_NOT_DT, Len.XZT06O_NOT_DT);
	}

	public void setXzt06oPdcNbr(String xzt06oPdcNbr) {
		writeString(Pos.XZT06O_PDC_NBR, xzt06oPdcNbr, Len.XZT06O_PDC_NBR);
	}

	/**Original name: XZT06O-PDC-NBR<br>*/
	public String getXzt06oPdcNbr() {
		return readString(Pos.XZT06O_PDC_NBR, Len.XZT06O_PDC_NBR);
	}

	public void setXzt06oPdcNm(String xzt06oPdcNm) {
		writeString(Pos.XZT06O_PDC_NM, xzt06oPdcNm, Len.XZT06O_PDC_NM);
	}

	/**Original name: XZT06O-PDC-NM<br>*/
	public String getXzt06oPdcNm() {
		return readString(Pos.XZT06O_PDC_NM, Len.XZT06O_PDC_NM);
	}

	public void setXzt06oTotFeeAmt(AfDecimal xzt06oTotFeeAmt) {
		writeDecimal(Pos.XZT06O_TOT_FEE_AMT, xzt06oTotFeeAmt.copy());
	}

	/**Original name: XZT06O-TOT-FEE-AMT<br>*/
	public AfDecimal getXzt06oTotFeeAmt() {
		return readDecimal(Pos.XZT06O_TOT_FEE_AMT, Len.Int.XZT06O_TOT_FEE_AMT, Len.Fract.XZT06O_TOT_FEE_AMT);
	}

	public void setXzt06oStAbb(String xzt06oStAbb) {
		writeString(Pos.XZT06O_ST_ABB, xzt06oStAbb, Len.XZT06O_ST_ABB);
	}

	/**Original name: XZT06O-ST-ABB<br>*/
	public String getXzt06oStAbb() {
		return readString(Pos.XZT06O_ST_ABB, Len.XZT06O_ST_ABB);
	}

	public void setXzt06oCerHldNotInd(char xzt06oCerHldNotInd) {
		writeChar(Pos.XZT06O_CER_HLD_NOT_IND, xzt06oCerHldNotInd);
	}

	/**Original name: XZT06O-CER-HLD-NOT-IND<br>*/
	public char getXzt06oCerHldNotInd() {
		return readChar(Pos.XZT06O_CER_HLD_NOT_IND);
	}

	public void setXzt06oAddCncDay(int xzt06oAddCncDay) {
		writeInt(Pos.XZT06O_ADD_CNC_DAY, xzt06oAddCncDay, Len.Int.XZT06O_ADD_CNC_DAY);
	}

	public void setXzt06oAddCncDayFormatted(String xzt06oAddCncDay) {
		writeString(Pos.XZT06O_ADD_CNC_DAY, Trunc.toUnsignedNumeric(xzt06oAddCncDay, Len.XZT06O_ADD_CNC_DAY), Len.XZT06O_ADD_CNC_DAY);
	}

	/**Original name: XZT06O-ADD-CNC-DAY<br>*/
	public int getXzt06oAddCncDay() {
		return readNumDispInt(Pos.XZT06O_ADD_CNC_DAY, Len.XZT06O_ADD_CNC_DAY);
	}

	public void setXzt06oReaDes(String xzt06oReaDes) {
		writeString(Pos.XZT06O_REA_DES, xzt06oReaDes, Len.XZT06O_REA_DES);
	}

	/**Original name: XZT06O-REA-DES<br>*/
	public String getXzt06oReaDes() {
		return readString(Pos.XZT06O_REA_DES, Len.XZT06O_REA_DES);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT006_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT06I_TECHNICAL_KEY = XZT006_SERVICE_INPUTS;
		public static final int XZT06I_TK_NOT_PRC_TS = XZT06I_TECHNICAL_KEY;
		public static final int XZT06I_TK_ACT_OWN_CLT_ID = XZT06I_TK_NOT_PRC_TS + Len.XZT06I_TK_NOT_PRC_TS;
		public static final int XZT06I_TK_ACT_OWN_ADR_ID = XZT06I_TK_ACT_OWN_CLT_ID + Len.XZT06I_TK_ACT_OWN_CLT_ID;
		public static final int XZT06I_TK_EMP_ID = XZT06I_TK_ACT_OWN_ADR_ID + Len.XZT06I_TK_ACT_OWN_ADR_ID;
		public static final int XZT06I_TK_STA_MDF_TS = XZT06I_TK_EMP_ID + Len.XZT06I_TK_EMP_ID;
		public static final int XZT06I_TK_ACT_NOT_STA_CD = XZT06I_TK_STA_MDF_TS + Len.XZT06I_TK_STA_MDF_TS;
		public static final int XZT06I_TK_SEG_CD = XZT06I_TK_ACT_NOT_STA_CD + Len.XZT06I_TK_ACT_NOT_STA_CD;
		public static final int XZT06I_TK_ACT_TYP_CD = XZT06I_TK_SEG_CD + Len.XZT06I_TK_SEG_CD;
		public static final int XZT06I_TK_ACT_NOT_CSUM = XZT06I_TK_ACT_TYP_CD + Len.XZT06I_TK_ACT_TYP_CD;
		public static final int XZT06I_CSR_ACT_NBR = XZT06I_TK_ACT_NOT_CSUM + Len.XZT06I_TK_ACT_NOT_CSUM;
		public static final int XZT06I_ACT_NOT_TYP_CD = XZT06I_CSR_ACT_NBR + Len.XZT06I_CSR_ACT_NBR;
		public static final int XZT06I_NOT_DT = XZT06I_ACT_NOT_TYP_CD + Len.XZT06I_ACT_NOT_TYP_CD;
		public static final int XZT06I_PDC = XZT06I_NOT_DT + Len.XZT06I_NOT_DT;
		public static final int XZT06I_PDC_NBR = XZT06I_PDC;
		public static final int XZT06I_PDC_NM = XZT06I_PDC_NBR + Len.XZT06I_PDC_NBR;
		public static final int XZT06I_TOT_FEE_AMT = XZT06I_PDC_NM + Len.XZT06I_PDC_NM;
		public static final int XZT06I_ST_ABB = XZT06I_TOT_FEE_AMT + Len.XZT06I_TOT_FEE_AMT;
		public static final int XZT06I_CER_HLD_NOT_IND = XZT06I_ST_ABB + Len.XZT06I_ST_ABB;
		public static final int XZT06I_ADD_CNC_DAY = XZT06I_CER_HLD_NOT_IND + Len.XZT06I_CER_HLD_NOT_IND;
		public static final int XZT06I_REA_DES = XZT06I_ADD_CNC_DAY + Len.XZT06I_ADD_CNC_DAY;
		public static final int XZT06I_USERID = XZT06I_REA_DES + Len.XZT06I_REA_DES;
		public static final int XZT006_SERVICE_OUTPUTS = XZT06I_USERID + Len.XZT06I_USERID;
		public static final int XZT06O_TECHNICAL_KEY = XZT006_SERVICE_OUTPUTS;
		public static final int XZT06O_TK_NOT_PRC_TS = XZT06O_TECHNICAL_KEY;
		public static final int XZT06O_TK_ACT_OWN_CLT_ID = XZT06O_TK_NOT_PRC_TS + Len.XZT06O_TK_NOT_PRC_TS;
		public static final int XZT06O_TK_ACT_OWN_ADR_ID = XZT06O_TK_ACT_OWN_CLT_ID + Len.XZT06O_TK_ACT_OWN_CLT_ID;
		public static final int XZT06O_TK_EMP_ID = XZT06O_TK_ACT_OWN_ADR_ID + Len.XZT06O_TK_ACT_OWN_ADR_ID;
		public static final int XZT06O_TK_STA_MDF_TS = XZT06O_TK_EMP_ID + Len.XZT06O_TK_EMP_ID;
		public static final int XZT06O_TK_ACT_NOT_STA_CD = XZT06O_TK_STA_MDF_TS + Len.XZT06O_TK_STA_MDF_TS;
		public static final int XZT06O_TK_SEG_CD = XZT06O_TK_ACT_NOT_STA_CD + Len.XZT06O_TK_ACT_NOT_STA_CD;
		public static final int XZT06O_TK_ACT_TYP_CD = XZT06O_TK_SEG_CD + Len.XZT06O_TK_SEG_CD;
		public static final int XZT06O_TK_ACT_NOT_CSUM = XZT06O_TK_ACT_TYP_CD + Len.XZT06O_TK_ACT_TYP_CD;
		public static final int XZT06O_CSR_ACT_NBR = XZT06O_TK_ACT_NOT_CSUM + Len.XZT06O_TK_ACT_NOT_CSUM;
		public static final int XZT06O_ACT_NOT_TYP = XZT06O_CSR_ACT_NBR + Len.XZT06O_CSR_ACT_NBR;
		public static final int XZT06O_ACT_NOT_TYP_CD = XZT06O_ACT_NOT_TYP;
		public static final int XZT06O_ACT_NOT_TYP_DESC = XZT06O_ACT_NOT_TYP_CD + Len.XZT06O_ACT_NOT_TYP_CD;
		public static final int XZT06O_NOT_DT = XZT06O_ACT_NOT_TYP_DESC + Len.XZT06O_ACT_NOT_TYP_DESC;
		public static final int XZT06O_PDC = XZT06O_NOT_DT + Len.XZT06O_NOT_DT;
		public static final int XZT06O_PDC_NBR = XZT06O_PDC;
		public static final int XZT06O_PDC_NM = XZT06O_PDC_NBR + Len.XZT06O_PDC_NBR;
		public static final int XZT06O_TOT_FEE_AMT = XZT06O_PDC_NM + Len.XZT06O_PDC_NM;
		public static final int XZT06O_ST_ABB = XZT06O_TOT_FEE_AMT + Len.XZT06O_TOT_FEE_AMT;
		public static final int XZT06O_CER_HLD_NOT_IND = XZT06O_ST_ABB + Len.XZT06O_ST_ABB;
		public static final int XZT06O_ADD_CNC_DAY = XZT06O_CER_HLD_NOT_IND + Len.XZT06O_CER_HLD_NOT_IND;
		public static final int XZT06O_REA_DES = XZT06O_ADD_CNC_DAY + Len.XZT06O_ADD_CNC_DAY;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT06I_TK_NOT_PRC_TS = 26;
		public static final int XZT06I_TK_ACT_OWN_CLT_ID = 64;
		public static final int XZT06I_TK_ACT_OWN_ADR_ID = 64;
		public static final int XZT06I_TK_EMP_ID = 6;
		public static final int XZT06I_TK_STA_MDF_TS = 26;
		public static final int XZT06I_TK_ACT_NOT_STA_CD = 2;
		public static final int XZT06I_TK_SEG_CD = 3;
		public static final int XZT06I_TK_ACT_TYP_CD = 2;
		public static final int XZT06I_TK_ACT_NOT_CSUM = 9;
		public static final int XZT06I_CSR_ACT_NBR = 9;
		public static final int XZT06I_ACT_NOT_TYP_CD = 5;
		public static final int XZT06I_NOT_DT = 10;
		public static final int XZT06I_PDC_NBR = 5;
		public static final int XZT06I_PDC_NM = 120;
		public static final int XZT06I_TOT_FEE_AMT = 10;
		public static final int XZT06I_ST_ABB = 2;
		public static final int XZT06I_CER_HLD_NOT_IND = 1;
		public static final int XZT06I_ADD_CNC_DAY = 5;
		public static final int XZT06I_REA_DES = 500;
		public static final int XZT06I_USERID = 8;
		public static final int XZT06O_TK_NOT_PRC_TS = 26;
		public static final int XZT06O_TK_ACT_OWN_CLT_ID = 64;
		public static final int XZT06O_TK_ACT_OWN_ADR_ID = 64;
		public static final int XZT06O_TK_EMP_ID = 6;
		public static final int XZT06O_TK_STA_MDF_TS = 26;
		public static final int XZT06O_TK_ACT_NOT_STA_CD = 2;
		public static final int XZT06O_TK_SEG_CD = 3;
		public static final int XZT06O_TK_ACT_TYP_CD = 2;
		public static final int XZT06O_TK_ACT_NOT_CSUM = 9;
		public static final int XZT06O_CSR_ACT_NBR = 9;
		public static final int XZT06O_ACT_NOT_TYP_CD = 5;
		public static final int XZT06O_ACT_NOT_TYP_DESC = 35;
		public static final int XZT06O_NOT_DT = 10;
		public static final int XZT06O_PDC_NBR = 5;
		public static final int XZT06O_PDC_NM = 120;
		public static final int XZT06O_TOT_FEE_AMT = 10;
		public static final int XZT06O_ST_ABB = 2;
		public static final int XZT06O_CER_HLD_NOT_IND = 1;
		public static final int XZT06O_ADD_CNC_DAY = 5;
		public static final int XZT06I_TECHNICAL_KEY = XZT06I_TK_NOT_PRC_TS + XZT06I_TK_ACT_OWN_CLT_ID + XZT06I_TK_ACT_OWN_ADR_ID + XZT06I_TK_EMP_ID
				+ XZT06I_TK_STA_MDF_TS + XZT06I_TK_ACT_NOT_STA_CD + XZT06I_TK_SEG_CD + XZT06I_TK_ACT_TYP_CD + XZT06I_TK_ACT_NOT_CSUM;
		public static final int XZT06I_PDC = XZT06I_PDC_NBR + XZT06I_PDC_NM;
		public static final int XZT006_SERVICE_INPUTS = XZT06I_TECHNICAL_KEY + XZT06I_CSR_ACT_NBR + XZT06I_ACT_NOT_TYP_CD + XZT06I_NOT_DT + XZT06I_PDC
				+ XZT06I_TOT_FEE_AMT + XZT06I_ST_ABB + XZT06I_CER_HLD_NOT_IND + XZT06I_ADD_CNC_DAY + XZT06I_REA_DES + XZT06I_USERID;
		public static final int XZT06O_TECHNICAL_KEY = XZT06O_TK_NOT_PRC_TS + XZT06O_TK_ACT_OWN_CLT_ID + XZT06O_TK_ACT_OWN_ADR_ID + XZT06O_TK_EMP_ID
				+ XZT06O_TK_STA_MDF_TS + XZT06O_TK_ACT_NOT_STA_CD + XZT06O_TK_SEG_CD + XZT06O_TK_ACT_TYP_CD + XZT06O_TK_ACT_NOT_CSUM;
		public static final int XZT06O_ACT_NOT_TYP = XZT06O_ACT_NOT_TYP_CD + XZT06O_ACT_NOT_TYP_DESC;
		public static final int XZT06O_PDC = XZT06O_PDC_NBR + XZT06O_PDC_NM;
		public static final int XZT06O_REA_DES = 500;
		public static final int XZT006_SERVICE_OUTPUTS = XZT06O_TECHNICAL_KEY + XZT06O_CSR_ACT_NBR + XZT06O_ACT_NOT_TYP + XZT06O_NOT_DT + XZT06O_PDC
				+ XZT06O_TOT_FEE_AMT + XZT06O_ST_ABB + XZT06O_CER_HLD_NOT_IND + XZT06O_ADD_CNC_DAY + XZT06O_REA_DES;
		public static final int L_SERVICE_CONTRACT_AREA = XZT006_SERVICE_INPUTS + XZT006_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT06I_TOT_FEE_AMT = 8;
			public static final int XZT06I_ADD_CNC_DAY = 5;
			public static final int XZT06O_TOT_FEE_AMT = 8;
			public static final int XZT06O_ADD_CNC_DAY = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZT06I_TOT_FEE_AMT = 2;
			public static final int XZT06O_TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
