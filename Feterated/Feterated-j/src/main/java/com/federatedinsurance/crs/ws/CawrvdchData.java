/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.federatedinsurance.crs.ws.enums.SwInvalidCharFoundSwitch;
import com.federatedinsurance.crs.ws.redefines.SaCheckStringGrp;
import org.apache.commons.lang3.ArrayUtils;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program CAWRVDCH<br>
 * Generated as a class for rule WS.<br>*/
public class CawrvdchData {

	//==== PROPERTIES ====
	//Original name: SA-SEARCH-STR-LENGTH
	private short saSearchStrLength = DefaultValues.BIN_SHORT_VAL;
	//Original name: SA-CHECK-STRING-GRP
	private SaCheckStringGrp saCheckStringGrp = new SaCheckStringGrp();
	//Original name: IX-CS
	private int ixCs = 1;
	//Original name: SA-CHECK-CHARACTER
	private char saCheckCharacter = DefaultValues.CHAR_VAL;
	private static final char[] SA_INVALID_CHARACTER = new char[] {'|', '+', '½', '/', '<', '>'};
    //Original name: SW-INVALID-CHAR-FOUND-SWITCH
	private SwInvalidCharFoundSwitch swInvalidCharFoundSwitch = new SwInvalidCharFoundSwitch();

	//==== METHODS ====
	public void setSaSearchStrLength(short saSearchStrLength) {
		this.saSearchStrLength = saSearchStrLength;
	}

	public short getSaSearchStrLength() {
		return this.saSearchStrLength;
	}

	public void setIxCs(int ixCs) {
		this.ixCs = ixCs;
	}

	public int getIxCs() {
		return this.ixCs;
	}

	public void setSaCheckCharacter(char saCheckCharacter) {
		this.saCheckCharacter = saCheckCharacter;
	}

	public char getSaCheckCharacter() {
		return this.saCheckCharacter;
	}

	public boolean isSaInvalidCharacter() {
		return ArrayUtils.contains(SA_INVALID_CHARACTER, saCheckCharacter);
	}

	public SaCheckStringGrp getSaCheckStringGrp() {
		return saCheckStringGrp;
	}

	public SwInvalidCharFoundSwitch getSwInvalidCharFoundSwitch() {
		return swInvalidCharFoundSwitch;
	}
}
