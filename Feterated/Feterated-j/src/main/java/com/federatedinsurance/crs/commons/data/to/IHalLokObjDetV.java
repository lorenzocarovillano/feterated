/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HAL_LOK_OBJ_DET_V]
 * 
 */
public interface IHalLokObjDetV extends BaseSqlTo {

	/**
	 * Host Variable HLOD-SESSION-ID
	 * 
	 */
	String getSessionId();

	void setSessionId(String sessionId);

	/**
	 * Host Variable HLOD-TCH-KEY
	 * 
	 */
	String getTchKey();

	void setTchKey(String tchKey);

	/**
	 * Host Variable HLOD-APP-ID
	 * 
	 */
	String getAppId();

	void setAppId(String appId);

	/**
	 * Host Variable HLOD-TABLE-NM
	 * 
	 */
	String getTableNm();

	void setTableNm(String tableNm);

	/**
	 * Host Variable HLOD-USERID
	 * 
	 */
	String getUserid();

	void setUserid(String userid);

	/**
	 * Host Variable HLOD-LOK-TYPE-CD
	 * 
	 */
	char getLokTypeCd();

	void setLokTypeCd(char lokTypeCd);

	/**
	 * Host Variable HLOD-UPD-BY-MSG-ID
	 * 
	 */
	String getUpdByMsgId();

	void setUpdByMsgId(String updByMsgId);

	/**
	 * Host Variable HLOD-ZAPPED-BY
	 * 
	 */
	String getZappedBy();

	void setZappedBy(String zappedBy);

	/**
	 * Host Variable HLOD-ZAPPED-TS
	 * 
	 */
	String getZappedTs();

	void setZappedTs(String zappedTs);

	/**
	 * Host Variable HLOD-ZAPPED-IND
	 * 
	 */
	char getZappedInd();

	void setZappedInd(char zappedInd);

	/**
	 * Host Variable WS-TMO-VALUE
	 * 
	 */
	short getWsTmoValue();

	void setWsTmoValue(short wsTmoValue);

	/**
	 * Host Variable WS-USER-ID
	 * 
	 */
	String getUserId();

	void setUserId(String userId);
};
