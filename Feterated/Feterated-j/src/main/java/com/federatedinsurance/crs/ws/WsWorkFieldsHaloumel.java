/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-WORK-FIELDS<br>
 * Variable: WS-WORK-FIELDS from program HALOUMEL<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkFieldsHaloumel {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "HALOUMEL";
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "";
	//Original name: WS-BUS-OBJ-MDU
	private String busObjMdu = "";
	//Original name: WS-MAINDRVR-ERR-MOD
	private String maindrvrErrMod = "MDR_ERROR_LOGGING_INTERFACE";
	//Original name: WS-SAV-HOST-ERR-LOG-UOW
	private String savHostErrLogUow = "SAVANNAH_HOST_ERROR_LOGGING";
	//Original name: WS-ERROR-STORAGE-VSAM
	private String errorStorageVsam = "HALFEIES";
	//Original name: WS-ERROR-STO-DTL-VSAM
	private String errorStoDtlVsam = "HALFEIED";
	//Original name: WS-ERROR-VSAM-NAME
	private String errorVsamName = DefaultValues.stringVal(Len.ERROR_VSAM_NAME);
	//Original name: WS-ERROR-ALERT-MOD
	private String errorAlertMod = "HALOEPAL";
	//Original name: WS-ERROR-LOGGING-CLT
	private String errorLoggingClt = "SAV_HOST_ERR_LOG_CLT";
	//Original name: WS-APPLID
	private String applid = DefaultValues.stringVal(Len.APPLID);
	//Original name: WS-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-NUM-ENTRIES
	private short numEntries = DefaultValues.SHORT_VAL;

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setBusObjMdu(String busObjMdu) {
		this.busObjMdu = Functions.subString(busObjMdu, Len.BUS_OBJ_MDU);
	}

	public String getBusObjMdu() {
		return this.busObjMdu;
	}

	public String getMaindrvrErrMod() {
		return this.maindrvrErrMod;
	}

	public String getSavHostErrLogUow() {
		return this.savHostErrLogUow;
	}

	public String getErrorStorageVsam() {
		return this.errorStorageVsam;
	}

	public String getErrorStoDtlVsam() {
		return this.errorStoDtlVsam;
	}

	public void setErrorVsamName(String errorVsamName) {
		this.errorVsamName = Functions.subString(errorVsamName, Len.ERROR_VSAM_NAME);
	}

	public String getErrorVsamName() {
		return this.errorVsamName;
	}

	public String getErrorVsamNameFormatted() {
		return Functions.padBlanks(getErrorVsamName(), Len.ERROR_VSAM_NAME);
	}

	public String getErrorAlertMod() {
		return this.errorAlertMod;
	}

	public String getErrorLoggingClt() {
		return this.errorLoggingClt;
	}

	public void setApplid(String applid) {
		this.applid = Functions.subString(applid, Len.APPLID);
	}

	public String getApplid() {
		return this.applid;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public void setNumEntries(short numEntries) {
		this.numEntries = numEntries;
	}

	public short getNumEntries() {
		return this.numEntries;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_VSAM_NAME = 32;
		public static final int APPLID = 8;
		public static final int NUM_ENTRIES = 2;
		public static final int BUS_OBJ_NM = 32;
		public static final int BUS_OBJ_MDU = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
