/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.WsObjRelCltFilterType;

/**Original name: WS-WORK-AREA<br>
 * Variable: WS-WORK-AREA from program CAWPCORC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkArea {

	//==== PROPERTIES ====
	//Original name: WS-UDAT-SEQ-NUM
	private short udatSeqNum = ((short) 0);
	//Original name: WS-URQM-SEQ-NUM
	private short urqmSeqNum = ((short) 0);
	//Original name: WS-OBJ-REL-CLT-FILTER-TYPE
	private WsObjRelCltFilterType objRelCltFilterType = new WsObjRelCltFilterType();

	//==== METHODS ====
	public void setUdatSeqNum(short udatSeqNum) {
		this.udatSeqNum = udatSeqNum;
	}

	public short getUdatSeqNum() {
		return this.udatSeqNum;
	}

	public void setUrqmSeqNum(short urqmSeqNum) {
		this.urqmSeqNum = urqmSeqNum;
	}

	public short getUrqmSeqNum() {
		return this.urqmSeqNum;
	}

	public WsObjRelCltFilterType getObjRelCltFilterType() {
		return objRelCltFilterType;
	}
}
