/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-TSQ-ERROR<br>
 * Variable: EA-01-TSQ-ERROR from program TS571098<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01TsqError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-TSQ-ERROR
	private String flr1 = "TEMP QUEUE";
	//Original name: FILLER-EA-01-TSQ-ERROR-1
	private String flr2 = "ERROR FOR";
	//Original name: EA-01-TSQ
	private String tsq = DefaultValues.stringVal(Len.TSQ);
	//Original name: FILLER-EA-01-TSQ-ERROR-2
	private String flr3 = ".  RESP CODE:";
	//Original name: EA-01-RESP
	private String resp = DefaultValues.stringVal(Len.RESP);
	//Original name: FILLER-EA-01-TSQ-ERROR-3
	private String flr4 = ".  RESP2 CODE:";
	//Original name: EA-01-RESP2
	private String resp2 = DefaultValues.stringVal(Len.RESP2);

	//==== METHODS ====
	public String getEa01TsqErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa01TsqErrorBytes());
	}

	public byte[] getEa01TsqErrorBytes() {
		byte[] buffer = new byte[Len.EA01_TSQ_ERROR];
		return getEa01TsqErrorBytes(buffer, 1);
	}

	public byte[] getEa01TsqErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, tsq, Len.TSQ);
		position += Len.TSQ;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, resp, Len.RESP);
		position += Len.RESP;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, resp2, Len.RESP2);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setTsq(String tsq) {
		this.tsq = Functions.subString(tsq, Len.TSQ);
	}

	public String getTsq() {
		return this.tsq;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setResp(String resp) {
		this.resp = Functions.subString(resp, Len.RESP);
	}

	public String getResp() {
		return this.resp;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setResp2(String resp2) {
		this.resp2 = Functions.subString(resp2, Len.RESP2);
	}

	public String getResp2() {
		return this.resp2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TSQ = 16;
		public static final int RESP = 16;
		public static final int RESP2 = 16;
		public static final int FLR1 = 11;
		public static final int FLR2 = 10;
		public static final int FLR3 = 14;
		public static final int FLR4 = 15;
		public static final int EA01_TSQ_ERROR = TSQ + RESP + RESP2 + FLR1 + FLR2 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
