/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: RI-BALANCE-FILE-FLAG<br>
 * Variable: RI-BALANCE-FILE-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class RiBalanceFileFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NOT_A_BALANCE_REPORT_FILE = 'N';
	public static final char BALANCE_REPORT_FILE = 'Y';

	//==== METHODS ====
	public void setBalanceFileFlag(char balanceFileFlag) {
		this.value = balanceFileFlag;
	}

	public char getBalanceFileFlag() {
		return this.value;
	}

	public void setNotABalanceReportFile() {
		value = NOT_A_BALANCE_REPORT_FILE;
	}

	public boolean isBalanceReportFile() {
		return value == BALANCE_REPORT_FILE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BALANCE_FILE_FLAG = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
