/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0B90S0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0b90s0 {

	//==== PROPERTIES ====
	//Original name: WS-ROW-COUNT
	private short rowCount = ((short) 0);
	public static final short NOTHING_FOUND = ((short) 0);
	//Original name: WS-EIBRESP-CD
	private short eibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0B90S0";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-CURRENT-DATE
	private String currentDate = DefaultValues.stringVal(Len.CURRENT_DATE);
	//Original name: WS-POL-CUTOFF-DT
	private String polCutoffDt = DefaultValues.stringVal(Len.POL_CUTOFF_DT);
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "XZ_ACY_PND_CNC_TMN_POL_LIST_BPO";

	//==== METHODS ====
	public void setRowCount(short rowCount) {
		this.rowCount = rowCount;
	}

	public short getRowCount() {
		return this.rowCount;
	}

	public boolean isNothingFound() {
		return rowCount == NOTHING_FOUND;
	}

	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd = eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public String getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public String getEibrespCdAsString() {
		return getEibrespCdFormatted();
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd = eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public String getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public String getEibresp2CdAsString() {
		return getEibresp2CdFormatted();
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = Functions.subString(currentDate, Len.CURRENT_DATE);
	}

	public String getCurrentDate() {
		return this.currentDate;
	}

	public void setPolCutoffDt(String polCutoffDt) {
		this.polCutoffDt = Functions.subString(polCutoffDt, Len.POL_CUTOFF_DT);
	}

	public String getPolCutoffDt() {
		return this.polCutoffDt;
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CURRENT_DATE = 10;
		public static final int POL_CUTOFF_DT = 10;
		public static final int EIBRESP_CD = 4;
		public static final int EIBRESP2_CD = 4;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
