/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program CIWBNSRB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesCiwbnsrb {

	//==== PROPERTIES ====
	//Original name: WS-FIRST-BLANK-SW
	private char wsFirstBlankSw = Types.SPACE_CHAR;
	public static final char FIRST_BLANK = '1';
	//Original name: WS-NAME-SW
	private char wsNameSw = Types.SPACE_CHAR;
	public static final char NAME_FOUND = '1';
	//Original name: WS-AND-SW
	private char wsAndSw = Types.SPACE_CHAR;
	public static final char AND_FOUND = '1';
	//Original name: WS-START-NAME-SW
	private char wsStartNameSw = Types.SPACE_CHAR;
	public static final char START_NAME_FOUND = '1';
	//Original name: WS-COMPANY-SW
	private char wsCompanySw = Types.SPACE_CHAR;
	public static final char COMPANY_FOUND = '1';
	//Original name: WS-COMMA-SW
	private char wsCommaSw = Types.SPACE_CHAR;
	public static final char COMMA_FOUND = '1';
	//Original name: WS-FIRST-PREFIX-SW
	private char wsFirstPrefixSw = Types.SPACE_CHAR;
	public static final char FIRST_PREFIX = '1';
	//Original name: WS-FIRST-SUFFIX-SW
	private char wsFirstSuffixSw = Types.SPACE_CHAR;
	public static final char FIRST_SUFFIX = '1';
	//Original name: WS-PREFIX-SW
	private char wsPrefixSw = Types.SPACE_CHAR;
	public static final char PREFIX_NOT_FOUND = '1';
	//Original name: WS-INVALID-CHAR-SW
	private char wsInvalidCharSw = Types.SPACE_CHAR;
	public static final char INVALID_CHAR_FOUND = '1';
	//Original name: WS-HYPHEN-SW
	private char wsHyphenSw = Types.SPACE_CHAR;
	public static final char HYPHEN_FOUND = '1';

	//==== METHODS ====
	public void setWsFirstBlankSw(char wsFirstBlankSw) {
		this.wsFirstBlankSw = wsFirstBlankSw;
	}

	public void setWsFirstBlankSwFormatted(String wsFirstBlankSw) {
		setWsFirstBlankSw(Functions.charAt(wsFirstBlankSw, Types.CHAR_SIZE));
	}

	public char getWsFirstBlankSw() {
		return this.wsFirstBlankSw;
	}

	public boolean isFirstBlank() {
		return wsFirstBlankSw == FIRST_BLANK;
	}

	public void setWsNameSw(char wsNameSw) {
		this.wsNameSw = wsNameSw;
	}

	public void setWsNameSwFormatted(String wsNameSw) {
		setWsNameSw(Functions.charAt(wsNameSw, Types.CHAR_SIZE));
	}

	public char getWsNameSw() {
		return this.wsNameSw;
	}

	public boolean isNameFound() {
		return wsNameSw == NAME_FOUND;
	}

	public void setWsAndSw(char wsAndSw) {
		this.wsAndSw = wsAndSw;
	}

	public void setWsAndSwFormatted(String wsAndSw) {
		setWsAndSw(Functions.charAt(wsAndSw, Types.CHAR_SIZE));
	}

	public char getWsAndSw() {
		return this.wsAndSw;
	}

	public boolean isAndFound() {
		return wsAndSw == AND_FOUND;
	}

	public void setWsStartNameSw(char wsStartNameSw) {
		this.wsStartNameSw = wsStartNameSw;
	}

	public void setWsStartNameSwFormatted(String wsStartNameSw) {
		setWsStartNameSw(Functions.charAt(wsStartNameSw, Types.CHAR_SIZE));
	}

	public char getWsStartNameSw() {
		return this.wsStartNameSw;
	}

	public boolean isStartNameFound() {
		return wsStartNameSw == START_NAME_FOUND;
	}

	public void setWsCompanySw(char wsCompanySw) {
		this.wsCompanySw = wsCompanySw;
	}

	public void setWsCompanySwFormatted(String wsCompanySw) {
		setWsCompanySw(Functions.charAt(wsCompanySw, Types.CHAR_SIZE));
	}

	public char getWsCompanySw() {
		return this.wsCompanySw;
	}

	public boolean isCompanyFound() {
		return wsCompanySw == COMPANY_FOUND;
	}

	public void setWsCommaSw(char wsCommaSw) {
		this.wsCommaSw = wsCommaSw;
	}

	public void setWsCommaSwFormatted(String wsCommaSw) {
		setWsCommaSw(Functions.charAt(wsCommaSw, Types.CHAR_SIZE));
	}

	public char getWsCommaSw() {
		return this.wsCommaSw;
	}

	public boolean isCommaFound() {
		return wsCommaSw == COMMA_FOUND;
	}

	public void setWsFirstPrefixSw(char wsFirstPrefixSw) {
		this.wsFirstPrefixSw = wsFirstPrefixSw;
	}

	public void setWsFirstPrefixSwFormatted(String wsFirstPrefixSw) {
		setWsFirstPrefixSw(Functions.charAt(wsFirstPrefixSw, Types.CHAR_SIZE));
	}

	public char getWsFirstPrefixSw() {
		return this.wsFirstPrefixSw;
	}

	public void setWsFirstSuffixSw(char wsFirstSuffixSw) {
		this.wsFirstSuffixSw = wsFirstSuffixSw;
	}

	public void setWsFirstSuffixSwFormatted(String wsFirstSuffixSw) {
		setWsFirstSuffixSw(Functions.charAt(wsFirstSuffixSw, Types.CHAR_SIZE));
	}

	public char getWsFirstSuffixSw() {
		return this.wsFirstSuffixSw;
	}

	public void setWsPrefixSw(char wsPrefixSw) {
		this.wsPrefixSw = wsPrefixSw;
	}

	public void setWsPrefixSwFormatted(String wsPrefixSw) {
		setWsPrefixSw(Functions.charAt(wsPrefixSw, Types.CHAR_SIZE));
	}

	public char getWsPrefixSw() {
		return this.wsPrefixSw;
	}

	public void setWsInvalidCharSw(char wsInvalidCharSw) {
		this.wsInvalidCharSw = wsInvalidCharSw;
	}

	public void setWsInvalidCharSwFormatted(String wsInvalidCharSw) {
		setWsInvalidCharSw(Functions.charAt(wsInvalidCharSw, Types.CHAR_SIZE));
	}

	public char getWsInvalidCharSw() {
		return this.wsInvalidCharSw;
	}

	public boolean isInvalidCharFound() {
		return wsInvalidCharSw == INVALID_CHAR_FOUND;
	}

	public void setWsHyphenSw(char wsHyphenSw) {
		this.wsHyphenSw = wsHyphenSw;
	}

	public void setWsHyphenSwFormatted(String wsHyphenSw) {
		setWsHyphenSw(Functions.charAt(wsHyphenSw, Types.CHAR_SIZE));
	}

	public char getWsHyphenSw() {
		return this.wsHyphenSw;
	}

	public boolean isHyphenFound() {
		return wsHyphenSw == HYPHEN_FOUND;
	}
}
