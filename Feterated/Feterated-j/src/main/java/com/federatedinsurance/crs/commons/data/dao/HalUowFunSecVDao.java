/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalUowFunSecV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_UOW_FUN_SEC_V]
 * 
 */
public class HalUowFunSecVDao extends BaseSqlDao<IHalUowFunSecV> {

	private Cursor cursor2;
	private final IRowMapper<IHalUowFunSecV> fetchCursor2Rm = buildNamedRowMapper(IHalUowFunSecV.class, "hufshSecAscTyp", "funSeqNbr", "autActionCd",
			"autFunNm", "lnkToMduNm", "appNm");

	public HalUowFunSecVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalUowFunSecV> getToClass() {
		return IHalUowFunSecV.class;
	}

	public DbAccessStatus openCursor2(IHalUowFunSecV iHalUowFunSecV) {
		cursor2 = buildQuery("openCursor2").bind(iHalUowFunSecV).open();
		return dbStatus;
	}

	public IHalUowFunSecV fetchCursor2(IHalUowFunSecV iHalUowFunSecV) {
		return fetch(cursor2, iHalUowFunSecV, fetchCursor2Rm);
	}

	public DbAccessStatus closeCursor2() {
		return closeCursor(cursor2);
	}
}
