/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IOvlTyp;

/**Original name: DCLOVL-TYP<br>
 * Variable: DCLOVL-TYP from copybook XZH00023<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclovlTyp implements IOvlTyp {

	//==== PROPERTIES ====
	//Original name: OVL-EDL-FRM-NM
	private String ovlEdlFrmNm = DefaultValues.stringVal(Len.OVL_EDL_FRM_NM);
	//Original name: OVL-DES
	private String ovlDes = DefaultValues.stringVal(Len.OVL_DES);
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;
	//Original name: XCLV-IND
	private char xclvInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setOvlEdlFrmNm(String ovlEdlFrmNm) {
		this.ovlEdlFrmNm = Functions.subString(ovlEdlFrmNm, Len.OVL_EDL_FRM_NM);
	}

	public String getOvlEdlFrmNm() {
		return this.ovlEdlFrmNm;
	}

	@Override
	public void setOvlDes(String ovlDes) {
		this.ovlDes = Functions.subString(ovlDes, Len.OVL_DES);
	}

	@Override
	public String getOvlDes() {
		return this.ovlDes;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	@Override
	public void setXclvInd(char xclvInd) {
		this.xclvInd = xclvInd;
	}

	@Override
	public char getXclvInd() {
		return this.xclvInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OVL_DES = 30;
		public static final int OVL_EDL_FRM_NM = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
