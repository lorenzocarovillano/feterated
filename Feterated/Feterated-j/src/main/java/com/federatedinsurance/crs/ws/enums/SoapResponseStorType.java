/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SOAP-RESPONSE-STOR-TYPE<br>
 * Variable: SOAP-RESPONSE-STOR-TYPE from copybook TS570CB1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SoapResponseStorType {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.RESPONSE_STOR_TYPE);
	public static final String CAREA = "CO";
	public static final String MEMORY = "ME";
	public static final String CHCONT = "CT";

	//==== METHODS ====
	public void setResponseStorType(String responseStorType) {
		this.value = Functions.subString(responseStorType, Len.RESPONSE_STOR_TYPE);
	}

	public String getResponseStorType() {
		return this.value;
	}

	public void setSoapResponseStorTypeCarea() {
		value = CAREA;
	}

	public void setChcont() {
		value = CHCONT;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESPONSE_STOR_TYPE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
