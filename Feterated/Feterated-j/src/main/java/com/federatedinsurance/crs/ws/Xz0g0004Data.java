/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.copy.Ts020drv;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0G0004<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0g0004Data {

	//==== PROPERTIES ====
	//Original name: CF-BLANK
	private char cfBlank = Types.SPACE_CHAR;
	//Original name: CF-MAIN-DRIVER
	private String cfMainDriver = "XZ0X0004";
	//Original name: CF-UNIT-OF-WORK
	private String cfUnitOfWork = "XZ_GET_RESCIND_IND";
	//Original name: WS-RESPONSE-CODE
	private int wsResponseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int wsResponseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-EIBRESP-DISPLAY
	private String wsEibrespDisplay = DefaultValues.stringVal(Len.WS_EIBRESP_DISPLAY);
	//Original name: WS-EIBRESP2-DISPLAY
	private String wsEibresp2Display = DefaultValues.stringVal(Len.WS_EIBRESP2_DISPLAY);
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "XZ0G0004";
	//Original name: WS-OPERATION-NAME
	private String wsOperationName = DefaultValues.stringVal(Len.WS_OPERATION_NAME);
	//Original name: TS020DRV
	private Ts020drv ts020drv = new Ts020drv();

	//==== METHODS ====
	public char getCfBlank() {
		return this.cfBlank;
	}

	public String getCfMainDriver() {
		return this.cfMainDriver;
	}

	public String getCfUnitOfWork() {
		return this.cfUnitOfWork;
	}

	public void setWsResponseCode(int wsResponseCode) {
		this.wsResponseCode = wsResponseCode;
	}

	public int getWsResponseCode() {
		return this.wsResponseCode;
	}

	public void setWsResponseCode2(int wsResponseCode2) {
		this.wsResponseCode2 = wsResponseCode2;
	}

	public int getWsResponseCode2() {
		return this.wsResponseCode2;
	}

	public void setWsEibrespDisplay(long wsEibrespDisplay) {
		this.wsEibrespDisplay = PicFormatter.display("-Z(8)9").format(wsEibrespDisplay).toString();
	}

	public long getWsEibrespDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.wsEibrespDisplay);
	}

	public String getWsEibrespDisplayFormatted() {
		return this.wsEibrespDisplay;
	}

	public String getWsEibrespDisplayAsString() {
		return getWsEibrespDisplayFormatted();
	}

	public void setWsEibresp2Display(long wsEibresp2Display) {
		this.wsEibresp2Display = PicFormatter.display("-Z(8)9").format(wsEibresp2Display).toString();
	}

	public long getWsEibresp2Display() {
		return PicParser.display("-Z(8)9").parseLong(this.wsEibresp2Display);
	}

	public String getWsEibresp2DisplayFormatted() {
		return this.wsEibresp2Display;
	}

	public String getWsEibresp2DisplayAsString() {
		return getWsEibresp2DisplayFormatted();
	}

	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public void setWsOperationName(String wsOperationName) {
		this.wsOperationName = Functions.subString(wsOperationName, Len.WS_OPERATION_NAME);
	}

	public String getWsOperationName() {
		return this.wsOperationName;
	}

	public Ts020drv getTs020drv() {
		return ts020drv;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_EIBRESP_DISPLAY = 10;
		public static final int WS_EIBRESP2_DISPLAY = 10;
		public static final int WS_OPERATION_NAME = 32;
		public static final int WS_PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
