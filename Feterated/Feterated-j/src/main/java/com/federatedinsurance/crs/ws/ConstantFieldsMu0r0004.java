/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program MU0R0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsMu0r0004 {

	//==== PROPERTIES ====
	//Original name: CF-PRIMARY
	private String primary = "PR";
	//Original name: CF-SIC
	private String sic = "SIC";
	//Original name: CF-MAX-PHONE-ROWS
	private short maxPhoneRows = ((short) 5);
	//Original name: CF-MAX-CLT-OBJ-REL-ROWS
	private short maxCltObjRelRows = ((short) 50);
	//Original name: CF-MAX-FED-BUS-TYP-ROWS
	private short maxFedBusTypRows = ((short) 6);
	//Original name: CF-MAX-CLT-CLT-RLT-ROWS
	private short maxCltCltRltRows = ((short) 7);
	//Original name: CF-TT-FEIN
	private String ttFein = "FED";
	//Original name: CF-TT-SSN
	private String ttSsn = "SSN";
	//Original name: CF-BDO-BUSINESS-OBJECT-NAMES
	private CfBdoBusinessObjectNames bdoBusinessObjectNames = new CfBdoBusinessObjectNames();
	//Original name: CF-INVALID-OPERATION
	private CfInvalidOperationMu0r0004 invalidOperation = new CfInvalidOperationMu0r0004();
	//Original name: CF-TABLE-FORMATTER-MODULES
	private CfTableFormatterModulesMu0r0004 tableFormatterModules = new CfTableFormatterModulesMu0r0004();

	//==== METHODS ====
	public String getPrimary() {
		return this.primary;
	}

	public String getSic() {
		return this.sic;
	}

	public short getMaxPhoneRows() {
		return this.maxPhoneRows;
	}

	public short getMaxCltObjRelRows() {
		return this.maxCltObjRelRows;
	}

	public short getMaxFedBusTypRows() {
		return this.maxFedBusTypRows;
	}

	public short getMaxCltCltRltRows() {
		return this.maxCltCltRltRows;
	}

	public String getTtFein() {
		return this.ttFein;
	}

	public String getTtSsn() {
		return this.ttSsn;
	}

	public CfBdoBusinessObjectNames getBdoBusinessObjectNames() {
		return bdoBusinessObjectNames;
	}

	public CfInvalidOperationMu0r0004 getInvalidOperation() {
		return invalidOperation;
	}

	public CfTableFormatterModulesMu0r0004 getTableFormatterModules() {
		return tableFormatterModules;
	}
}
