/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalBoLokExcV;

/**Original name: DCLHAL-BO-LOK-EXC<br>
 * Variable: DCLHAL-BO-LOK-EXC from copybook HALLGBLE<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalBoLokExc implements IHalBoLokExcV {

	//==== PROPERTIES ====
	//Original name: HBLE-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HBLE-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: HBLE-CONTEXT-TXT
	private String contextTxt = DefaultValues.stringVal(Len.CONTEXT_TXT);
	//Original name: HBLE-LOK-EXC-MDU
	private String lokExcMdu = DefaultValues.stringVal(Len.LOK_EXC_MDU);

	//==== METHODS ====
	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	@Override
	public void setContextTxt(String contextTxt) {
		this.contextTxt = Functions.subString(contextTxt, Len.CONTEXT_TXT);
	}

	@Override
	public String getContextTxt() {
		return this.contextTxt;
	}

	@Override
	public void setLokExcMdu(String lokExcMdu) {
		this.lokExcMdu = Functions.subString(lokExcMdu, Len.LOK_EXC_MDU);
	}

	@Override
	public String getLokExcMdu() {
		return this.lokExcMdu;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CONTEXT_TXT = 30;
		public static final int LOK_EXC_MDU = 32;
		public static final int UOW_NM = 32;
		public static final int BUS_OBJ_NM = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
