/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9070<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9070 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT97O_ACY_FRM_ROW_MAXOCCURS = 2000;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9070() {
	}

	public LServiceContractAreaXz0x9070(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt97iUserid(String xzt97iUserid) {
		writeString(Pos.XZT97I_USERID, xzt97iUserid, Len.XZT97I_USERID);
	}

	/**Original name: XZT97I-USERID<br>*/
	public String getXzt97iUserid() {
		return readString(Pos.XZT97I_USERID, Len.XZT97I_USERID);
	}

	public String getXzt97iUseridFormatted() {
		return Functions.padBlanks(getXzt97iUserid(), Len.XZT97I_USERID);
	}

	public void setXzt97oAcyFrmRowBytes(int xzt97oAcyFrmRowIdx, byte[] buffer) {
		setXzt97oAcyFrmRowBytes(xzt97oAcyFrmRowIdx, buffer, 1);
	}

	public void setXzt97oAcyFrmRowBytes(int xzt97oAcyFrmRowIdx, byte[] buffer, int offset) {
		int position = Pos.xzt97oAcyFrmRow(xzt97oAcyFrmRowIdx - 1);
		setBytes(buffer, offset, Len.XZT97O_ACY_FRM_ROW, position);
	}

	public void setXzt97oFrmNbr(int xzt97oFrmNbrIdx, String xzt97oFrmNbr) {
		int position = Pos.xzt97oFrmNbr(xzt97oFrmNbrIdx - 1);
		writeString(position, xzt97oFrmNbr, Len.XZT97O_FRM_NBR);
	}

	/**Original name: XZT97O-FRM-NBR<br>*/
	public String getXzt97oFrmNbr(int xzt97oFrmNbrIdx) {
		int position = Pos.xzt97oFrmNbr(xzt97oFrmNbrIdx - 1);
		return readString(position, Len.XZT97O_FRM_NBR);
	}

	public void setXzt97oFrmEdtDt(int xzt97oFrmEdtDtIdx, String xzt97oFrmEdtDt) {
		int position = Pos.xzt97oFrmEdtDt(xzt97oFrmEdtDtIdx - 1);
		writeString(position, xzt97oFrmEdtDt, Len.XZT97O_FRM_EDT_DT);
	}

	/**Original name: XZT97O-FRM-EDT-DT<br>*/
	public String getXzt97oFrmEdtDt(int xzt97oFrmEdtDtIdx) {
		int position = Pos.xzt97oFrmEdtDt(xzt97oFrmEdtDtIdx - 1);
		return readString(position, Len.XZT97O_FRM_EDT_DT);
	}

	public void setXzt97oActNotTypCd(int xzt97oActNotTypCdIdx, String xzt97oActNotTypCd) {
		int position = Pos.xzt97oActNotTypCd(xzt97oActNotTypCdIdx - 1);
		writeString(position, xzt97oActNotTypCd, Len.XZT97O_ACT_NOT_TYP_CD);
	}

	/**Original name: XZT97O-ACT-NOT-TYP-CD<br>*/
	public String getXzt97oActNotTypCd(int xzt97oActNotTypCdIdx) {
		int position = Pos.xzt97oActNotTypCd(xzt97oActNotTypCdIdx - 1);
		return readString(position, Len.XZT97O_ACT_NOT_TYP_CD);
	}

	public void setXzt97oEdlFrmNm(int xzt97oEdlFrmNmIdx, String xzt97oEdlFrmNm) {
		int position = Pos.xzt97oEdlFrmNm(xzt97oEdlFrmNmIdx - 1);
		writeString(position, xzt97oEdlFrmNm, Len.XZT97O_EDL_FRM_NM);
	}

	/**Original name: XZT97O-EDL-FRM-NM<br>*/
	public String getXzt97oEdlFrmNm(int xzt97oEdlFrmNmIdx) {
		int position = Pos.xzt97oEdlFrmNm(xzt97oEdlFrmNmIdx - 1);
		return readString(position, Len.XZT97O_EDL_FRM_NM);
	}

	public void setXzt97oFrmDes(int xzt97oFrmDesIdx, String xzt97oFrmDes) {
		int position = Pos.xzt97oFrmDes(xzt97oFrmDesIdx - 1);
		writeString(position, xzt97oFrmDes, Len.XZT97O_FRM_DES);
	}

	/**Original name: XZT97O-FRM-DES<br>*/
	public String getXzt97oFrmDes(int xzt97oFrmDesIdx) {
		int position = Pos.xzt97oFrmDes(xzt97oFrmDesIdx - 1);
		return readString(position, Len.XZT97O_FRM_DES);
	}

	public void setXzt97oSpePrcCd(int xzt97oSpePrcCdIdx, String xzt97oSpePrcCd) {
		int position = Pos.xzt97oSpePrcCd(xzt97oSpePrcCdIdx - 1);
		writeString(position, xzt97oSpePrcCd, Len.XZT97O_SPE_PRC_CD);
	}

	/**Original name: XZT97O-SPE-PRC-CD<br>*/
	public String getXzt97oSpePrcCd(int xzt97oSpePrcCdIdx) {
		int position = Pos.xzt97oSpePrcCd(xzt97oSpePrcCdIdx - 1);
		return readString(position, Len.XZT97O_SPE_PRC_CD);
	}

	public void setXzt97oDtnCd(int xzt97oDtnCdIdx, int xzt97oDtnCd) {
		int position = Pos.xzt97oDtnCd(xzt97oDtnCdIdx - 1);
		writeInt(position, xzt97oDtnCd, Len.Int.XZT97O_DTN_CD);
	}

	/**Original name: XZT97O-DTN-CD<br>*/
	public int getXzt97oDtnCd(int xzt97oDtnCdIdx) {
		int position = Pos.xzt97oDtnCd(xzt97oDtnCdIdx - 1);
		return readNumDispInt(position, Len.XZT97O_DTN_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT907_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT97I_USERID = XZT907_SERVICE_INPUTS;
		public static final int XZT907_SERVICE_OUTPUTS = XZT97I_USERID + Len.XZT97I_USERID;
		public static final int XZT97O_TABLE_OF_ACY_FRM_ROWS = XZT907_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt97oAcyFrmRow(int idx) {
			return XZT97O_TABLE_OF_ACY_FRM_ROWS + idx * Len.XZT97O_ACY_FRM_ROW;
		}

		public static int xzt97oFrmNbr(int idx) {
			return xzt97oAcyFrmRow(idx);
		}

		public static int xzt97oFrmEdtDt(int idx) {
			return xzt97oFrmNbr(idx) + Len.XZT97O_FRM_NBR;
		}

		public static int xzt97oActNotTypCd(int idx) {
			return xzt97oFrmEdtDt(idx) + Len.XZT97O_FRM_EDT_DT;
		}

		public static int xzt97oEdlFrmNm(int idx) {
			return xzt97oActNotTypCd(idx) + Len.XZT97O_ACT_NOT_TYP_CD;
		}

		public static int xzt97oFrmDes(int idx) {
			return xzt97oEdlFrmNm(idx) + Len.XZT97O_EDL_FRM_NM;
		}

		public static int xzt97oSpePrcCd(int idx) {
			return xzt97oFrmDes(idx) + Len.XZT97O_FRM_DES;
		}

		public static int xzt97oDtnCd(int idx) {
			return xzt97oSpePrcCd(idx) + Len.XZT97O_SPE_PRC_CD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT97I_USERID = 8;
		public static final int XZT97O_FRM_NBR = 30;
		public static final int XZT97O_FRM_EDT_DT = 10;
		public static final int XZT97O_ACT_NOT_TYP_CD = 5;
		public static final int XZT97O_EDL_FRM_NM = 30;
		public static final int XZT97O_FRM_DES = 20;
		public static final int XZT97O_SPE_PRC_CD = 8;
		public static final int XZT97O_DTN_CD = 5;
		public static final int XZT97O_ACY_FRM_ROW = XZT97O_FRM_NBR + XZT97O_FRM_EDT_DT + XZT97O_ACT_NOT_TYP_CD + XZT97O_EDL_FRM_NM + XZT97O_FRM_DES
				+ XZT97O_SPE_PRC_CD + XZT97O_DTN_CD;
		public static final int XZT907_SERVICE_INPUTS = XZT97I_USERID;
		public static final int XZT97O_TABLE_OF_ACY_FRM_ROWS = LServiceContractAreaXz0x9070.XZT97O_ACY_FRM_ROW_MAXOCCURS * XZT97O_ACY_FRM_ROW;
		public static final int XZT907_SERVICE_OUTPUTS = XZT97O_TABLE_OF_ACY_FRM_ROWS;
		public static final int L_SERVICE_CONTRACT_AREA = XZT907_SERVICE_INPUTS + XZT907_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT97O_DTN_CD = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
