/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsProxyProgramArea;
import com.federatedinsurance.crs.ws.Xz0p90a0Data;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0012;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x9080;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x9081;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0P90A0<br>
 * <pre>AUTHOR.       DAWN POSSEHL.
 * DATE-WRITTEN. 11 FEB 2009.
 * *****************************************************************
 *                                                                 *
 *   PROGRAM TITLE - PREPARE THIRD PARTY LIST                      *
 *                   XREF OBJ NM : XZ_PREPARE_THIRD_PARTY_LIST_BPO *
 *                   UOW         : XZ_PREPARE_THIRD_PARTY_LIST     *
 *                   OPERATION   : PrepareThirdPartyList           *
 *                                                                 *
 *   PURPOSE -  THIS BPO WILL CALL THE GET THIRD PARTY SERVICES.   *
 *              THEN FOR EVERY THIRD PARTY RETURN FROM THE CALLS,  *
 *              ADD THAT THIRD PARTY INFORMATION TO THE            *
 *              ACT_NOT_REC TABLE.                                 *
 *                                                                 *
 *   PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS   *
 *                         LINKED TO BY THE FRAMEWORK DRIVER.      *
 *                                                                 *
 *   DATA ACCESS METHODS - UMT STORAGE RECORDS                     *
 *                         DB2 DATABASE                            *
 *                                                                 *
 * *****************************************************************
 * ****************************************************************
 * * NOTE: THIS LOG FOR INFRASTRUCTURE USE ONLY FOR TEMPLATE     **
 * *       VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     **
 * *       APPLICATION CODING.                                   **
 * *     T E M P L A T E   M A I N T E N A N C E   L O G         **
 * * CASE#     DATE       PROG       DESCRIPTION                 **
 * * --------  ---------  --------   ----------------------------**
 * * TS129     06/13/2006 E404LJL    TEMPLATE CREATED            **
 * * YJ249     04/27/2007 E404NEM    STDS CHGS                   **
 * * TS130     12/28/2007 E404JSP    Changed a few bugs          **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * -------  ----------  --------   ----------------------------**
 * * TO07614  02/11/2009  E404DLP    NEW                         **
 * * TO07614  03/09/2009  E404DLP    CHG'D ADR-SEQ-NBR TO ADR-ID **
 * * PP02500  10/09/2012  E404BPO    STORE ADDITIONAL INFO FOR   **
 * *                                 THIRD PARTY CERTHOLDERS.    **
 * * 23515    10/23/2019  E404DLP    PASS TS TO THE CERT LIST SVC**
 * ****************************************************************</pre>*/
public class Xz0p90a0 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0p90a0Data ws = new Xz0p90a0Data();
	private ExecContext execContext = null;
	/**Original name: WS-XZ0T0012-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE ACT_NOT_REC SERVICE</pre>*/
	private LServiceContractAreaXz0x0012 wsXz0t0012Row = new LServiceContractAreaXz0x0012(null);
	/**Original name: WS-XZ0T9080-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE TTY LST SERVICE</pre>*/
	private LServiceContractAreaXz0x9080 wsXz0t9080Row = new LServiceContractAreaXz0x9080(null);
	/**Original name: WS-XZ0T9081-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE TTY CERT LST SERVICE</pre>*/
	private LServiceContractAreaXz0x9081 wsXz0t9081Row = new LServiceContractAreaXz0x9081(null);
	//Original name: DFHCOMMAREA
	private Dfhcommarea dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, Dfhcommarea dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0p90a0 getInstance() {
		return (Programs.getInstance(Xz0p90a0.class));
	}

	/**Original name: 1000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   MAIN PROCESSING CONTROL                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-PREPARE-TTY-INFO.
		prepareTtyInfo();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 4000-PREPARE-TTY-CERT-INFO.
		prepareTtyCertInfo();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *  INITIALIZATION CONTROL                                        *
	 *                                                                *
	 * ****************************************************************
	 * * INITIALIZE ERROR/WARNING STORAGE</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//* VALIDATE UBOC IN COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2100-READ-REQ-UMT-ROW.
		readReqUmtRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-READ-REQ-UMT-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR THE BPO INPUT ROW                     *
	 * *****************************************************************</pre>*/
	private void readReqUmtRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: SET HALRURQA-READ-FUNC      TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM-TTY-LIST TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNmTtyList());
		// COB_CODE: MOVE +1                     TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(1);
		// COB_CODE: INITIALIZE                  WS-XZ0Y90A0-ROW.
		initWsXz0y90a0Row();
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-XZ0Y90A0-ROW.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsXz0y90a0Row());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: IF HALRURQA-REC-NOT-FOUND
		//               GO TO 2100-EXIT
		//           END-IF.
		if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUEST-MSG-MISSING
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequestMsgMissing();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '2100-READ-REQ-UMT-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-READ-REQ-UMT-ROW");
			// COB_CODE: MOVE 'NO RECORD FOUND ON REQ MSG STORE'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO RECORD FOUND ON REQ MSG STORE");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HALRURQA-BUS-OBJ-NM='
			//                  HALRURQA-BUS-OBJ-NM
			//                  '; HALRURQA-REC-SEQ='
			//                  HALRURQA-REC-SEQ
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HALRURQA-BUS-OBJ-NM=").append(ws.getWsHalrurqaLinkage().getBusObjNmFormatted())
							.append("; HALRURQA-REC-SEQ=").append(ws.getWsHalrurqaLinkage().getRecSeqFormatted()).toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
	}

	/**Original name: 3000-PREPARE-TTY-INFO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ADD THE APPROPRIATE TTY INFO TO THE ACT_NOT_REC TABLE.         *
	 * *****************************************************************
	 * * ALLOCATE MEMORY FOR THE GET TTY SERVICE</pre>*/
	private void prepareTtyInfo() {
		// COB_CODE: PERFORM 3100-ALC-MEM-TTY-LIST-SVC.
		alcMemTtyListSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* GET THE TTY LIST FROM THE SERVICE
		// COB_CODE: PERFORM 3200-GET-TTY-LIST-SVC.
		getTtyListSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 3400-FREE-MEM-FOR-SERVICES
			freeMemForServices();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* ADD INFO FROM TTY LIST TO THE RECIPIENT TABLE
		// COB_CODE: PERFORM 3300-ADD-INFO-FROM-TTY-LIST.
		rng3300AddInfoFromTtyList();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 3400-FREE-MEM-FOR-SERVICES
			freeMemForServices();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//*  FREE UP CICS MEMORY FOR THE SERVICES
		// COB_CODE: PERFORM 3400-FREE-MEM-FOR-SERVICES.
		freeMemForServices();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3100-ALC-MEM-TTY-LIST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE GET TTY LIST SERVICE               *
	 * *****************************************************************</pre>*/
	private void alcMemTtyListSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T9080-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x9080.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-TTY-LIST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetTtyListSvc());
			// COB_CODE: MOVE '3100-ALC-MEM-TTY-LIST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3100-ALC-MEM-TTY-LIST-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3100-ALC-MEM-TTY-LIST-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3100-ALC-MEM-TTY-LIST-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T9080-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t9080Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x9080.class)));
		// COB_CODE: INITIALIZE WS-XZ0T9080-ROW.
		initWsXz0t9080Row();
	}

	/**Original name: 3200-GET-TTY-LIST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS NEEDED TO CALL THE TTY SERVICE.                 *
	 *   CALL THE SERVICE TO GET A LIST OF ALL ACTIVE TTY'S            *
	 *   FOR THE ACCOUNT NUMBER PASSED IN.                             *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void getTtyListSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-GET-TTY-LIST        TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getGetTtyList());
		// COB_CODE: MOVE XZY9A0-USERID          TO XZT98I-USERID.
		wsXz0t9080Row.setXzt98iUserid(ws.getWsXz0y90a0Row().getUserid());
		// COB_CODE: MOVE XZY9A0-CSR-ACT-NBR     TO XZT98I-CSR-ACT-NBR.
		wsXz0t9080Row.setXzt98iCsrActNbr(ws.getWsXz0y90a0Row().getCsrActNbr());
		// COB_CODE: MOVE XZY9A0-NOT-PRC-TS      TO XZT98I-TK-NOT-PRC-TS.
		wsXz0t9080Row.setXzt98iTkNotPrcTs(ws.getWsXz0y90a0Row().getNotPrcTs());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-GET-TTY-LIST-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90A0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getSpGetTtyListSvc(), new Xz0x9080());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3200-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-TTY-LIST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetTtyListSvc());
			// COB_CODE: MOVE '3200-GET-TTY-LIST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3200-GET-TTY-LIST-SVC");
			// COB_CODE: MOVE 'GET TTY LIST SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("GET TTY LIST SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3200-GET-TTY-LIST-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3200-GET-TTY-LIST-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3200-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-GET-TTY-LIST-SVC TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getSpGetTtyListSvc());
		// COB_CODE: MOVE '3200-GET-TTY-LIST-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("3200-GET-TTY-LIST-SVC");
		// COB_CODE: MOVE 'GET TTY LIST SVC'     TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("GET TTY LIST SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3300-ADD-INFO-FROM-TTY-LIST_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALL THE TTY INFORMATION FROM THE CALL WILL BE ADDED TO THE     *
	 *  ACT_NOT_REC TABLE.                                             *
	 * *****************************************************************</pre>*/
	private void addInfoFromTtyList() {
		// COB_CODE: COMPUTE WS-MAX-TTY-ROWS = LENGTH OF XZT98O-TTY-LIST-TBL
		//                                   / LENGTH OF XZT98O-TTY-LIST.
		ws.getWorkingStorageArea()
				.setMaxTtyRows((new AfDecimal(
						((((double) LServiceContractAreaXz0x9080.Len.XZT98O_TTY_LIST_TBL)) / LServiceContractAreaXz0x9080.Len.XZT98O_TTY_LIST), 9, 0))
								.toShort());
		// COB_CODE: MOVE +1                     TO SS-TL.
		ws.getSubscripts().setTl(((short) 1));
	}

	/**Original name: 3300-A<br>*/
	private String a() {
		// COB_CODE: IF XZT98O-REC-TYP-CD (SS-TL) = SPACES
		//               GO TO 3300-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(wsXz0t9080Row.getXzt98oRecTypCd(ws.getSubscripts().getTl()))) {
			// COB_CODE: GO TO 3300-EXIT
			return "";
		}
		// ALLOCATE MEMORY FOR THE SERVICE - FIRST TIME IN ONLY.
		// COB_CODE: IF SW-NO-TTY-INFO-ADDED
		//               MOVE WS-ADD-ACT-NOT-REC TO PPC-OPERATION
		//           END-IF.
		if (!ws.isSwTtyInfoAddedFlag()) {
			// COB_CODE: PERFORM 9700-ALC-MEM-ACT-NOT-REC-SVC
			alcMemActNotRecSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3300-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3300-EXIT
				return "";
			}
			// COB_CODE: SET SW-TTY-INFO-ADDED   TO TRUE
			ws.setSwTtyInfoAddedFlag(true);
			//*  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
			//*  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.
			// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
			//                                   TO TRUE
			ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
			// COB_CODE: MOVE WS-ADD-ACT-NOT-REC TO PPC-OPERATION
			ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getAddActNotRec());
		}
		// COB_CODE: PERFORM 3310-ADD-TTY-INFO.
		addTtyInfo();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3300-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3300-EXIT
			return "";
		}
		// COB_CODE: IF SS-TL = WS-MAX-TTY-ROWS
		//               GO TO 3300-EXIT
		//           END-IF.
		if (ws.getSubscripts().getTl() == ws.getWorkingStorageArea().getMaxTtyRows()) {
			// COB_CODE: GO TO 3300-EXIT
			return "";
		}
		// COB_CODE: ADD +1                      TO SS-TL.
		ws.getSubscripts().setTl(Trunc.toShort(1 + ws.getSubscripts().getTl(), 4));
		// COB_CODE: GO TO 3300-A.
		return "3300-A";
	}

	/**Original name: 3310-ADD-TTY-INFO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET UP THE INPUT PARMS.                                        *
	 *  CALL THE SERVICE TO ADD OUT THE ROW TO ACT_NOT_REC             *
	 * *****************************************************************
	 * *  MOVE IN THE VALUES WE NEED TO DO THE INSERT INTO ACT_NOT_REC</pre>*/
	private void addTtyInfo() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE WS-XZ0T0012-ROW.
		initWsXz0t0012Row();
		// COB_CODE: MOVE XZY9A0-USERID          TO XZT12I-USERID.
		wsXz0t0012Row.setXzt12iUserid(ws.getWsXz0y90a0Row().getUserid());
		// COB_CODE: MOVE XZY9A0-CSR-ACT-NBR     TO XZT12I-CSR-ACT-NBR.
		wsXz0t0012Row.setXzt12iCsrActNbr(ws.getWsXz0y90a0Row().getCsrActNbr());
		// COB_CODE: MOVE XZY9A0-NOT-PRC-TS      TO XZT12I-TK-NOT-PRC-TS.
		wsXz0t0012Row.setXzt12iTkNotPrcTs(ws.getWsXz0y90a0Row().getNotPrcTs());
		// COB_CODE: MOVE CF-MANUAL-IND          TO XZT12I-TK-MNL-IND.
		wsXz0t0012Row.setXzt12iTkMnlInd(ws.getConstantFields().getManualInd());
		// COB_CODE: MOVE XZT98O-REC-TYP-CD (SS-TL)
		//                                       TO XZT12I-REC-TYP-CD.
		wsXz0t0012Row.setXzt12iRecTypCd(wsXz0t9080Row.getXzt98oRecTypCd(ws.getSubscripts().getTl()));
		// COB_CODE: MOVE XZT98O-TK-CLIENT-ID (SS-TL)
		//                                       TO XZT12I-TK-REC-CLT-ID.
		wsXz0t0012Row.setXzt12iTkRecCltId(wsXz0t9080Row.getXzt98oTkClientId(ws.getSubscripts().getTl()));
		// COB_CODE: MOVE XZT98O-TK-ADR-ID (SS-TL)
		//                                       TO XZT12I-TK-REC-ADR-ID.
		wsXz0t0012Row.setXzt12iTkRecAdrId(wsXz0t9080Row.getXzt98oTkAdrId(ws.getSubscripts().getTl()));
		// COB_CODE: MOVE XZT98O-NAME (SS-TL)    TO XZT12I-REC-NAME.
		wsXz0t0012Row.setXzt12iRecName(wsXz0t9080Row.getXzt98oName(ws.getSubscripts().getTl()));
		// COB_CODE: MOVE XZT98O-ADR-LIN1 (SS-TL)
		//                                       TO XZT12I-LIN-1-ADR.
		wsXz0t0012Row.setXzt12iLin1Adr(wsXz0t9080Row.getXzt98oAdrLin1(ws.getSubscripts().getTl()));
		// COB_CODE: MOVE XZT98O-ADR-LIN2 (SS-TL)
		//                                       TO XZT12I-LIN-2-ADR.
		wsXz0t0012Row.setXzt12iLin2Adr(wsXz0t9080Row.getXzt98oAdrLin2(ws.getSubscripts().getTl()));
		// COB_CODE: MOVE XZT98O-CITY-NM (SS-TL) TO XZT12I-CITY-NAME.
		wsXz0t0012Row.setXzt12iCityName(wsXz0t9080Row.getXzt98oCityNm(ws.getSubscripts().getTl()));
		// COB_CODE: MOVE XZT98O-STATE-ABB (SS-TL)
		//                                       TO XZT12I-STATE-ABB.
		wsXz0t0012Row.setXzt12iStateAbb(wsXz0t9080Row.getXzt98oStateAbb(ws.getSubscripts().getTl()));
		// COB_CODE: MOVE XZT98O-PST-CD (SS-TL)  TO XZT12I-POSTAL-CD.
		wsXz0t0012Row.setXzt12iPostalCd(wsXz0t9080Row.getXzt98oPstCd(ws.getSubscripts().getTl()));
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-ADD-ACT-NOT-REC-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90A0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getSpAddActNotRecSvc(), new Xz0x0012());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3310-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpAddActNotRecSvc());
			// COB_CODE: MOVE '3310-ADD-TTY-INFO'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3310-ADD-TTY-INFO");
			// COB_CODE: MOVE 'ADD ACT-NOT-REC SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ADD ACT-NOT-REC SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3310-ADD-TTY-INFO'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3310-ADD-TTY-INFO", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3310-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getSpAddActNotRecSvc());
		// COB_CODE: MOVE '3310-ADD-TTY-INFO'    TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("3310-ADD-TTY-INFO");
		// COB_CODE: MOVE 'ADD ACT_NOT_REC SVC'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("ADD ACT_NOT_REC SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3400-FREE-MEM-FOR-SERVICES_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE SERVICES                               *
	 * *****************************************************************</pre>*/
	private void freeMemForServices() {
		// COB_CODE: PERFORM 3410-FREE-MEM-TTY-LIST-SVC.
		freeMemTtyListSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3400-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3400-EXIT
			return;
		}
		// ONLY NEED TO FREE THIS ONE IF WE ALLOCATED IT.
		// COB_CODE: IF SW-TTY-INFO-ADDED
		//               END-IF
		//           END-IF.
		if (ws.isSwTtyInfoAddedFlag()) {
			// COB_CODE: PERFORM 9800-FREE-MEM-ACT-NOT-REC-SVC
			freeMemActNotRecSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3400-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3400-EXIT
				return;
			}
		}
	}

	/**Original name: 3410-FREE-MEM-TTY-LIST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE TTY LIST SERVICE.                      *
	 * *****************************************************************</pre>*/
	private void freeMemTtyListSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T9080-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t9080Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-TTY-LIST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetTtyListSvc());
			// COB_CODE: MOVE '3410-FREE-MEM-TTY-LIST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3410-FREE-MEM-TTY-LIST-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3410-FREE-MEM-TTY-LIST-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3410-FREE-MEM-TTY-LIST-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 4000-PREPARE-TTY-CERT-INFO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ADD THE APPROPRIATE TTY CERT INFO TO THE ACT_NOT_REC TABLE.    *
	 * *****************************************************************
	 * * ALLOCATE MEMORY FOR THE GET TTY CERT SERVICE</pre>*/
	private void prepareTtyCertInfo() {
		// COB_CODE: PERFORM 4100-ALC-MEM-TTY-CERT-LIST-SVC.
		alcMemTtyCertListSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		//* GET THE TTY CERT LIST FROM THE SERVICE
		// COB_CODE: PERFORM 4200-GET-TTY-CERT-LIST-SVC.
		getTtyCertListSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 4400-FREE-MEM-FOR-SERVICES
			freeMemForServices1();
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		//* ADD INFO FROM TTY CERT LIST TO THE RECIPIENT TABLE
		// COB_CODE: PERFORM 4300-ADD-INFO-FROM-TTY-CER-LST.
		rng4300AddInfoFromTtyCerLst();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 4400-FREE-MEM-FOR-SERVICES
			freeMemForServices1();
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		//*  FREE UP CICS MEMORY FOR THE SERVICES
		// COB_CODE: PERFORM 4400-FREE-MEM-FOR-SERVICES.
		freeMemForServices1();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		//* A NON-LOGGABLE WARNING IS RETURNED IF NO TTY WAS ADDED
		//* FOR EITHER CALL TO THE RECIPIENT TABLE
		// COB_CODE: IF SW-NO-TTY-INFO-ADDED
		//            AND
		//              SW-NO-TTY-CERT-INFO-ADDED
		//               PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
		//           END-IF.
		if (!ws.isSwTtyInfoAddedFlag() && !ws.isSwTtyCertInfoAddedFlag()) {
			// COB_CODE: SET WS-NON-LOGGABLE-WARNING
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setWarning();
			// COB_CODE: MOVE WS-BUS-OBJ-NM-TTY-LIST
			//                                   TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getBusObjNmTtyList());
			// COB_CODE: MOVE 'ACT_NBR'          TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("ACT_NBR");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO UWRN-WARNING-CODE
			ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE XZY9A0-CSR-ACT-NBR TO EA-01-CSR-ACT-NBR
			ws.getEa01NothingAddedMsg().setCsrActNbr(ws.getWsXz0y90a0Row().getCsrActNbr());
			// COB_CODE: MOVE XZY9A0-NOT-PRC-TS  TO EA-01-NOT-PRC-TS
			ws.getEa01NothingAddedMsg().setNotPrcTs(ws.getWsXz0y90a0Row().getNotPrcTs());
			// COB_CODE: MOVE EA-01-NOTHING-ADDED-MSG
			//                                   TO WS-NONLOG-ERR-ALLTXT-TEXT
			ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getEa01NothingAddedMsg().getEa01NothingAddedMsgFormatted());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
		}
	}

	/**Original name: 4100-ALC-MEM-TTY-CERT-LIST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE GET TTY CERT LIST SERVICE          *
	 * *****************************************************************</pre>*/
	private void alcMemTtyCertListSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T9081-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x9081.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 4100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-TTY-CERT-LIST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetTtyCertListSvc());
			// COB_CODE: MOVE '4100-ALC-MEM-TTY-CERT-LIST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4100-ALC-MEM-TTY-CERT-LIST-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '4100-ALC-MEM-TTY-CERT-LIST-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"4100-ALC-MEM-TTY-CERT-LIST-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T9081-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t9081Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x9081.class)));
		// COB_CODE: INITIALIZE WS-XZ0T9081-ROW.
		initWsXz0t9081Row();
	}

	/**Original name: 4200-GET-TTY-CERT-LIST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS NEEDED TO CALL THE TTY CERT SERVICE             *
	 *   CALL THE SERVICE TO GET A LIST OF ALL ACTIVE TTY CERTS        *
	 *   FOR THE ACCOUNT NUMBER PASSED IN.                             *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void getTtyCertListSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-GET-TTY-CERT-LIST   TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getGetTtyCertList());
		// COB_CODE: MOVE XZY9A0-USERID          TO XZT981I-USERID.
		wsXz0t9081Row.setXzt981iUserid(ws.getWsXz0y90a0Row().getUserid());
		// COB_CODE: MOVE XZY9A0-CSR-ACT-NBR     TO XZT981I-CSR-ACT-NBR.
		wsXz0t9081Row.setXzt981iCsrActNbr(ws.getWsXz0y90a0Row().getCsrActNbr());
		// COB_CODE: MOVE XZY9A0-NOT-PRC-TS      TO XZT981I-TK-NOT-PRC-TS.
		wsXz0t9081Row.setXzt981iTkNotPrcTs(ws.getWsXz0y90a0Row().getNotPrcTs());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-GET-TTY-CERT-LIST-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90A0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getSpGetTtyCertListSvc(), new Xz0x9081());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 4200-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-TTY-CERT-LIST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetTtyCertListSvc());
			// COB_CODE: MOVE '4200-GET-TTY-CERT-LIST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4200-GET-TTY-CERT-LIST-SVC");
			// COB_CODE: MOVE 'GET TTY CERT LIST SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("GET TTY CERT LIST SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '4200-GET-TTY-CERT-LIST-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"4200-GET-TTY-CERT-LIST-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4200-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-GET-TTY-CERT-LIST-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getSpGetTtyCertListSvc());
		// COB_CODE: MOVE '4200-GET-TTY-CERT-LIST-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("4200-GET-TTY-CERT-LIST-SVC");
		// COB_CODE: MOVE 'GET TTY CERT LIST SVC'
		//                                       TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("GET TTY CERT LIST SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 4300-ADD-INFO-FROM-TTY-CER-LST_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALL THE TTY INFORMATION FROM THE CALL WILL BE ADDED TO THE     *
	 *  ACT_NOT_REC TABLE.                                             *
	 * *****************************************************************</pre>*/
	private void addInfoFromTtyCerLst() {
		// COB_CODE: COMPUTE WS-MAX-TTY-CERT-ROWS
		//                                   = LENGTH OF XZT981O-TTY-CERT-LIST-TBL
		//                                   / LENGTH OF XZT981O-TTY-CERT-LIST.
		ws.getWorkingStorageArea().setMaxTtyCertRows((new AfDecimal(
				((((double) LServiceContractAreaXz0x9081.Len.XZT981O_TTY_CERT_LIST_TBL)) / LServiceContractAreaXz0x9081.Len.XZT981O_TTY_CERT_LIST), 9,
				0)).toShort());
		// COB_CODE: MOVE +1                     TO SS-CL.
		ws.getSubscripts().setCl(((short) 1));
	}

	/**Original name: 4300-A<br>*/
	private String a1() {
		// COB_CODE: IF XZT981O-CER-NBR (SS-CL) = SPACES
		//               GO TO 4300-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(wsXz0t9081Row.getXzt981oCerNbr(ws.getSubscripts().getCl()))) {
			// COB_CODE: GO TO 4300-EXIT
			return "";
		}
		// ALLOCATE MEMORY FOR THE SERVICE - FIRST TIME IN ONLY.
		// COB_CODE: IF SW-NO-TTY-CERT-INFO-ADDED
		//               MOVE WS-ADD-ACT-NOT-REC TO PPC-OPERATION
		//           END-IF.
		if (!ws.isSwTtyCertInfoAddedFlag()) {
			// COB_CODE: PERFORM 9700-ALC-MEM-ACT-NOT-REC-SVC
			alcMemActNotRecSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 4300-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 4300-EXIT
				return "";
			}
			// COB_CODE: SET SW-TTY-CERT-INFO-ADDED
			//                                   TO TRUE
			ws.setSwTtyCertInfoAddedFlag(true);
			//*  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
			//*  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.
			// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
			//                                   TO TRUE
			ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
			// COB_CODE: MOVE WS-ADD-ACT-NOT-REC TO PPC-OPERATION
			ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getAddActNotRec());
		}
		// COB_CODE: PERFORM 4310-ADD-TTY-CERT-INFO.
		addTtyCertInfo();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4300-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4300-EXIT
			return "";
		}
		// COB_CODE: IF SS-CL = WS-MAX-TTY-CERT-ROWS
		//               GO TO 4300-EXIT
		//           END-IF.
		if (ws.getSubscripts().getCl() == ws.getWorkingStorageArea().getMaxTtyCertRows()) {
			// COB_CODE: GO TO 4300-EXIT
			return "";
		}
		// COB_CODE: ADD +1                      TO SS-CL.
		ws.getSubscripts().setCl(Trunc.toShort(1 + ws.getSubscripts().getCl(), 4));
		// COB_CODE: GO TO 4300-A.
		return "4300-A";
	}

	/**Original name: 4310-ADD-TTY-CERT-INFO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET UP THE INPUT PARMS.                                        *
	 *  CALL THE SERVICE TO ADD OUT THE ROW TO ACT_NOT_REC             *
	 * *****************************************************************
	 * *  MOVE IN THE VALUES WE NEED TO DO THE INSERT INTO ACT_NOT_REC</pre>*/
	private void addTtyCertInfo() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE WS-XZ0T0012-ROW.
		initWsXz0t0012Row();
		// COB_CODE: MOVE XZY9A0-USERID          TO XZT12I-USERID.
		wsXz0t0012Row.setXzt12iUserid(ws.getWsXz0y90a0Row().getUserid());
		// COB_CODE: MOVE XZY9A0-CSR-ACT-NBR     TO XZT12I-CSR-ACT-NBR.
		wsXz0t0012Row.setXzt12iCsrActNbr(ws.getWsXz0y90a0Row().getCsrActNbr());
		// COB_CODE: MOVE XZY9A0-NOT-PRC-TS      TO XZT12I-TK-NOT-PRC-TS.
		wsXz0t0012Row.setXzt12iTkNotPrcTs(ws.getWsXz0y90a0Row().getNotPrcTs());
		// COB_CODE: MOVE CF-MANUAL-IND          TO XZT12I-TK-MNL-IND.
		wsXz0t0012Row.setXzt12iTkMnlInd(ws.getConstantFields().getManualInd());
		// COB_CODE: MOVE CF-ADD-INS-REC-TYP-CD  TO XZT12I-REC-TYP-CD.
		wsXz0t0012Row.setXzt12iRecTypCd(ws.getConstantFields().getAddInsRecTypCd());
		// COB_CODE: MOVE XZT981O-TK-CLIENT-ID (SS-CL)
		//                                       TO XZT12I-TK-REC-CLT-ID.
		wsXz0t0012Row.setXzt12iTkRecCltId(wsXz0t9081Row.getXzt981oTkClientId(ws.getSubscripts().getCl()));
		// COB_CODE: MOVE XZT981O-TK-ADR-ID (SS-CL)
		//                                       TO XZT12I-TK-REC-ADR-ID.
		wsXz0t0012Row.setXzt12iTkRecAdrId(wsXz0t9081Row.getXzt981oTkAdrId(ws.getSubscripts().getCl()));
		// COB_CODE: MOVE XZT981O-NAME (SS-CL)   TO XZT12I-REC-NAME.
		wsXz0t0012Row.setXzt12iRecName(wsXz0t9081Row.getXzt981oName(ws.getSubscripts().getCl()));
		// COB_CODE: MOVE XZT981O-ADR-LIN1 (SS-CL)
		//                                       TO XZT12I-LIN-1-ADR.
		wsXz0t0012Row.setXzt12iLin1Adr(wsXz0t9081Row.getXzt981oAdrLin1(ws.getSubscripts().getCl()));
		// COB_CODE: MOVE XZT981O-ADR-LIN2 (SS-CL)
		//                                       TO XZT12I-LIN-2-ADR.
		wsXz0t0012Row.setXzt12iLin2Adr(wsXz0t9081Row.getXzt981oAdrLin2(ws.getSubscripts().getCl()));
		// COB_CODE: MOVE XZT981O-CITY-NM (SS-CL)
		//                                       TO XZT12I-CITY-NAME.
		wsXz0t0012Row.setXzt12iCityName(wsXz0t9081Row.getXzt981oCityNm(ws.getSubscripts().getCl()));
		// COB_CODE: MOVE XZT981O-STATE-ABB (SS-CL)
		//                                       TO XZT12I-STATE-ABB.
		wsXz0t0012Row.setXzt12iStateAbb(wsXz0t9081Row.getXzt981oStateAbb(ws.getSubscripts().getCl()));
		// COB_CODE: MOVE XZT981O-PST-CD (SS-CL) TO XZT12I-POSTAL-CD.
		wsXz0t0012Row.setXzt12iPostalCd(wsXz0t9081Row.getXzt981oPstCd(ws.getSubscripts().getCl()));
		// COB_CODE: MOVE XZT981O-CER-NBR (SS-CL)
		//                                       TO XZT12I-CERT-NBR.
		wsXz0t0012Row.setXzt12iCertNbr(wsXz0t9081Row.getXzt981oCerNbr(ws.getSubscripts().getCl()));
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-ADD-ACT-NOT-REC-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90A0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getSpAddActNotRecSvc(), new Xz0x0012());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 4310-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpAddActNotRecSvc());
			// COB_CODE: MOVE '4310-ADD-TTY-CERT-INFO'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4310-ADD-TTY-CERT-INFO");
			// COB_CODE: MOVE 'ADD ACT-NOT-REC SERVICE FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ADD ACT-NOT-REC SERVICE FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '4310-ADD-TTY-CERT-INFO'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"4310-ADD-TTY-CERT-INFO", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4310-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getSpAddActNotRecSvc());
		// COB_CODE: MOVE '4310-ADD-TTY-CERT-INFO'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("4310-ADD-TTY-CERT-INFO");
		// COB_CODE: MOVE 'ADD ACT_NOT_REC SVC'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("ADD ACT_NOT_REC SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 4400-FREE-MEM-FOR-SERVICES_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE SERVICES                               *
	 * *****************************************************************</pre>*/
	private void freeMemForServices1() {
		// COB_CODE: PERFORM 4410-FREE-MEM-TTY-CER-LST-SVC.
		freeMemTtyCerLstSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4400-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4400-EXIT
			return;
		}
		// ONLY NEED TO FREE THIS ONE IF WE ALLOCATED IT.
		// COB_CODE: IF SW-TTY-CERT-INFO-ADDED
		//               END-IF
		//           END-IF.
		if (ws.isSwTtyCertInfoAddedFlag()) {
			// COB_CODE: PERFORM 9800-FREE-MEM-ACT-NOT-REC-SVC
			freeMemActNotRecSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 4400-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 4400-EXIT
				return;
			}
		}
	}

	/**Original name: 4410-FREE-MEM-TTY-CER-LST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE CERT LIST SERVICE.                     *
	 * *****************************************************************</pre>*/
	private void freeMemTtyCerLstSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T9081-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t9081Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-TTY-CERT-LIST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpGetTtyCertListSvc());
			// COB_CODE: MOVE '4410-FREE-MEM-TTY-CER-LST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4410-FREE-MEM-TTY-CER-LST-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '4410-FREE-MEM-TTY-CER-LST-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"4410-FREE-MEM-TTY-CER-LST-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWorkingStorageArea().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90A0", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getCommInfo().setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getCommInfo().setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWorkingStorageArea().getApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: 9700-ALC-MEM-ACT-NOT-REC-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE MAINTAIN ACT_NOT_REC SERVICE.      *
	 * *****************************************************************</pre>*/
	private void alcMemActNotRecSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T0012-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x0012.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 9700-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpAddActNotRecSvc());
			// COB_CODE: MOVE '9700-ALC-MEM-ACT-NOT-REC-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9700-ALC-MEM-ACT-NOT-REC-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9700-ALC-MEM-ACT-NOT-REC-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9700-ALC-MEM-ACT-NOT-REC-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9700-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T0012-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t0012Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x0012.class)));
		// COB_CODE: INITIALIZE WS-XZ0T0012-ROW.
		initWsXz0t0012Row();
	}

	/**Original name: 9800-FREE-MEM-ACT-NOT-REC-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE MAINTAIN ACT_NOT_REC SERVICE.          *
	 * *****************************************************************</pre>*/
	private void freeMemActNotRecSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T0012-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t0012Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getSpAddActNotRecSvc());
			// COB_CODE: MOVE '9800-FREE-MEM-ACT-NOT-REC-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9800-FREE-MEM-ACT-NOT-REC-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9800-FREE-MEM-ACT-NOT-REC-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9800-FREE-MEM-ACT-NOT-REC-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9900-CHECK-ERRORS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   CHECK FOR ERROR, NLBE, OR WARNING RETURNED FROM SERVICE      *
	 * ****************************************************************</pre>*/
	private void checkErrors() {
		// COB_CODE: IF PPC-NO-ERROR-CODE
		//               GO TO 9900-EXIT
		//           END-IF.
		if (ws.getWsProxyProgramArea().getPpcErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: GO TO 9900-EXIT
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN PPC-FATAL-ERROR-CODE
		//                   PERFORM 9910-SET-FATAL-ERROR
		//               WHEN PPC-NLBE-CODE
		//                   PERFORM 9920-SET-NLBE
		//               WHEN PPC-WARNING-CODE
		//                   PERFORM 9930-SET-WARNING
		//           END-EVALUATE.
		switch (ws.getWsProxyProgramArea().getPpcErrorReturnCode().getDsdErrorReturnCodeFormatted()) {

		case DsdErrorReturnCode.FATAL_ERROR_CODE:// COB_CODE: PERFORM 9910-SET-FATAL-ERROR
			setFatalError();
			break;

		case DsdErrorReturnCode.NLBE_CODE:// COB_CODE: PERFORM 9920-SET-NLBE
			rng9920SetNlbe();
			break;

		case DsdErrorReturnCode.WARNING_CODE:// COB_CODE: PERFORM 9930-SET-WARNING
			rng9930SetWarning();
			break;

		default:
			break;
		}
	}

	/**Original name: 9910-SET-FATAL-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET FOR FATAL ERROR                                           *
	 *   SAVE OFF ANY ERROR MESSAGE THAT WAS RECEIVED FROM THE CALLED  *
	 *   SERVICE IN ORDER TO PUT IT IN THE DISPLAY AREAS AFTER LOGGING *
	 *   THE BUSINESS PROCESS ERROR.                                   *
	 *   THIS WILL ALLOW US TO DISPLAY THE ACTUAL ERROR THAT OCCURRED  *
	 *   IN THE CALLED SERVICE TO THE USER.                            *
	 * *****************************************************************</pre>*/
	private void setFatalError() {
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET ETRA-CICS-WEB-RECEIVE   TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
		// COB_CODE: MOVE WS-EC-MODULE           TO EFAL-ERR-OBJECT-NAME.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkingStorageArea().getErrorCheckInfo().getModule());
		// COB_CODE: MOVE WS-EC-PARAGRAPH        TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph(ws.getWorkingStorageArea().getErrorCheckInfo().getParagraph());
		// COB_CODE: MOVE PPC-FATAL-ERROR-MESSAGE
		//                                       TO EFAL-ERR-COMMENT
		//                                          EFAL-OBJ-DATA-KEY
		//                                          ES-01-FATAL-ERROR-MSG.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		ws.getEs01FatalErrorMsg().setEs01FatalErrorMsgFormatted(ws.getWsProxyProgramArea().getPpcFatalErrorMessageFormatted());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
		// COB_CODE: MOVE ES-01-FAILED-MODULE    TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule(ws.getEs01FatalErrorMsg().getFailedModule());
		// COB_CODE: MOVE ES-01-FAILED-PARAGRAPH TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph(ws.getEs01FatalErrorMsg().getFailedParagraph());
		// COB_CODE: MOVE ES-01-SQLCODE-DISPLAY  TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(ws.getEs01FatalErrorMsg().getSqlcodeDisplay());
		// COB_CODE: MOVE ES-01-EIBRESP-DISPLAY  TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(ws.getEs01FatalErrorMsg().getEibrespDisplay());
		// COB_CODE: MOVE ES-01-EIBRESP2-DISPLAY TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(ws.getEs01FatalErrorMsg().getEibresp2Display());
	}

	/**Original name: 9920-SET-NLBE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR NLBE                                                 *
	 * ****************************************************************</pre>*/
	private void setNlbe() {
		// COB_CODE: MOVE +0                     TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(((short) 0));
	}

	/**Original name: 9920-A<br>*/
	private String a2() {
		// COB_CODE: ADD +1                      TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(Trunc.toShort(1 + ws.getSubscripts().getMsgIdx(), 4));
		// COB_CODE: IF PPC-NON-LOG-ERR-MSG(SS-MSG-IDX) = SPACES
		//               GO TO 9920-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg())) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// we are setting UBOC-HALT-AND-RETURN to true so that if a service
		// gets an NLBE, the service ends.
		// COB_CODE: SET UBOC-HALT-AND-RETURN    TO TRUE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setBusErr();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO NLBE-FAILED-TABLE-OR-FILE.
		ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO NLBE-FAILED-COLUMN-OR-FIELD.
		ws.getNlbeCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-NON-LOG-ERR-MSG (SS-MSG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-MSG-IDX-MAX
		//               GO TO 9920-EXIT
		//           END-IF.
		if (ws.getSubscripts().isMsgIdxMax()) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// COB_CODE: GO TO 9920-A.
		return "9920-A";
	}

	/**Original name: 9930-SET-WARNING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR WARNING                                              *
	 * ****************************************************************</pre>*/
	private void setWarning() {
		// COB_CODE: MOVE +0                     TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(((short) 0));
	}

	/**Original name: 9930-A<br>*/
	private String a3() {
		// COB_CODE: ADD +1                      TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(Trunc.toShort(1 + ws.getSubscripts().getWngIdx(), 4));
		// COB_CODE: IF PPC-WARN-MSG(SS-WNG-IDX) = SPACES
		//               GO TO 9930-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg())) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: SET WS-NON-LOGGABLE-WARNING TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setWarning();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO UWRN-FAILED-TABLE-OR-FILE.
		ws.getUwrnCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO UWRN-FAILED-COLUMN-OR-FIELD.
		ws.getUwrnCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-WARN-MSG (SS-WNG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-WNG-IDX-MAX
		//               GO TO 9930-EXIT
		//           END-IF.
		if (ws.getSubscripts().isWngIdxMax()) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: GO TO 9930-A.
		return "9930-A";
	}

	/**Original name: RNG_3300-ADD-INFO-FROM-TTY-LIST_FIRST_SENTENCES-_-3300-EXIT<br>*/
	private void rng3300AddInfoFromTtyList() {
		String retcode = "";
		boolean goto3300A = false;
		addInfoFromTtyList();
		do {
			goto3300A = false;
			retcode = a();
		} while (retcode.equals("3300-A"));
	}

	/**Original name: RNG_4300-ADD-INFO-FROM-TTY-CER-LST_FIRST_SENTENCES-_-4300-EXIT<br>*/
	private void rng4300AddInfoFromTtyCerLst() {
		String retcode = "";
		boolean goto4300A = false;
		addInfoFromTtyCerLst();
		do {
			goto4300A = false;
			retcode = a1();
		} while (retcode.equals("4300-A"));
	}

	/**Original name: RNG_9920-SET-NLBE_FIRST_SENTENCES-_-9920-EXIT<br>*/
	private void rng9920SetNlbe() {
		String retcode = "";
		boolean goto9920A = false;
		setNlbe();
		do {
			goto9920A = false;
			retcode = a2();
		} while (retcode.equals("9920-A"));
	}

	/**Original name: RNG_9930-SET-WARNING_FIRST_SENTENCES-_-9930-EXIT<br>*/
	private void rng9930SetWarning() {
		String retcode = "";
		boolean goto9930A = false;
		setWarning();
		do {
			goto9930A = false;
			retcode = a3();
		} while (retcode.equals("9930-A"));
	}

	public void initWsXz0y90a0Row() {
		ws.getWsXz0y90a0Row().setCsrActNbr("");
		ws.getWsXz0y90a0Row().setNotPrcTs("");
		ws.getWsXz0y90a0Row().setUserid("");
	}

	public void initPpcMemoryAllocationParms() {
		ws.getWsProxyProgramArea().setPpcServiceDataSize(0);
	}

	public void initWsXz0t9080Row() {
		wsXz0t9080Row.setXzt98iTkNotPrcTs("");
		wsXz0t9080Row.setXzt98iCsrActNbr("");
		wsXz0t9080Row.setXzt98iUserid("");
		wsXz0t9080Row.setXzt98oTkNotPrcTs("");
		wsXz0t9080Row.setXzt98oCsrActNbr("");
		for (int idx0 = 1; idx0 <= LServiceContractAreaXz0x9080.XZT98O_TTY_LIST_MAXOCCURS; idx0++) {
			wsXz0t9080Row.setXzt98oTkClientId(idx0, "");
			wsXz0t9080Row.setXzt98oTkAdrId(idx0, "");
			wsXz0t9080Row.setXzt98oRecTypCd(idx0, "");
			wsXz0t9080Row.setXzt98oRecTypDes(idx0, "");
			wsXz0t9080Row.setXzt98oName(idx0, "");
			wsXz0t9080Row.setXzt98oAdrLin1(idx0, "");
			wsXz0t9080Row.setXzt98oAdrLin2(idx0, "");
			wsXz0t9080Row.setXzt98oCityNm(idx0, "");
			wsXz0t9080Row.setXzt98oStateAbb(idx0, "");
			wsXz0t9080Row.setXzt98oPstCd(idx0, "");
		}
	}

	public void initWsXz0t0012Row() {
		wsXz0t0012Row.setXzt12iTkNotPrcTs("");
		wsXz0t0012Row.setXzt12iTkRecSeqNbr(0);
		wsXz0t0012Row.setXzt12iTkRecCltId("");
		wsXz0t0012Row.setXzt12iTkRecAdrId("");
		wsXz0t0012Row.setXzt12iTkMnlInd(Types.SPACE_CHAR);
		wsXz0t0012Row.setXzt12iTkActNotRecCsumFormatted("000000000");
		wsXz0t0012Row.setXzt12iCsrActNbr("");
		wsXz0t0012Row.setXzt12iRecTypCd("");
		wsXz0t0012Row.setXzt12iRecName("");
		wsXz0t0012Row.setXzt12iLin1Adr("");
		wsXz0t0012Row.setXzt12iLin2Adr("");
		wsXz0t0012Row.setXzt12iCityName("");
		wsXz0t0012Row.setXzt12iStateAbb("");
		wsXz0t0012Row.setXzt12iPostalCd("");
		wsXz0t0012Row.setXzt12iCertNbr("");
		wsXz0t0012Row.setXzt12iUserid("");
		wsXz0t0012Row.setXzt12oTkNotPrcTs("");
		wsXz0t0012Row.setXzt12oTkRecSeqNbr(0);
		wsXz0t0012Row.setXzt12oTkRecCltId("");
		wsXz0t0012Row.setXzt12oTkRecAdrId("");
		wsXz0t0012Row.setXzt12oTkMnlInd(Types.SPACE_CHAR);
		wsXz0t0012Row.setXzt12oTkActNotRecCsumFormatted("000000000");
		wsXz0t0012Row.setXzt12oCsrActNbr("");
		wsXz0t0012Row.setXzt12oRecTypCd("");
		wsXz0t0012Row.setXzt12oRecTypShtDes("");
		wsXz0t0012Row.setXzt12oRecTypLngDes("");
		wsXz0t0012Row.setXzt12oRecTypSrOrdNbr(0);
		wsXz0t0012Row.setXzt12oRecName("");
		wsXz0t0012Row.setXzt12oLin1Adr("");
		wsXz0t0012Row.setXzt12oLin2Adr("");
		wsXz0t0012Row.setXzt12oCityName("");
		wsXz0t0012Row.setXzt12oStateAbb("");
		wsXz0t0012Row.setXzt12oPostalCd("");
		wsXz0t0012Row.setXzt12oCertNbr("");
	}

	public void initWsXz0t9081Row() {
		wsXz0t9081Row.setXzt981iTkNotPrcTs("");
		wsXz0t9081Row.setXzt981iCsrActNbr("");
		wsXz0t9081Row.setXzt981iUserid("");
		wsXz0t9081Row.setXzt981oTkNotPrcTs("");
		wsXz0t9081Row.setXzt981oCsrActNbr("");
		for (int idx0 = 1; idx0 <= LServiceContractAreaXz0x9081.XZT981O_TTY_CERT_LIST_MAXOCCURS; idx0++) {
			wsXz0t9081Row.setXzt981oTkClientId(idx0, "");
			wsXz0t9081Row.setXzt981oTkAdrId(idx0, "");
			wsXz0t9081Row.setXzt981oRecTypCd(idx0, "");
			wsXz0t9081Row.setXzt981oRecTypDes(idx0, "");
			wsXz0t9081Row.setXzt981oName(idx0, "");
			wsXz0t9081Row.setXzt981oAdrLin1(idx0, "");
			wsXz0t9081Row.setXzt981oAdrLin2(idx0, "");
			wsXz0t9081Row.setXzt981oCityNm(idx0, "");
			wsXz0t9081Row.setXzt981oStateAbb(idx0, "");
			wsXz0t9081Row.setXzt981oPstCd(idx0, "");
			wsXz0t9081Row.setXzt981oCerNbr(idx0, "");
		}
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
