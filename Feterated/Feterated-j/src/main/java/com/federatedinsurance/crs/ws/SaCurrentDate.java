/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: SA-CURRENT-DATE<br>
 * Variable: SA-CURRENT-DATE from program XZ004000<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class SaCurrentDate {

	//==== PROPERTIES ====
	//Original name: SA-CD-YYYY
	private String cdYyyy = DefaultValues.stringVal(Len.CD_YYYY);
	//Original name: FILLER-SA-CURRENT-DATE
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: SA-CD-MM
	private String cdMm = DefaultValues.stringVal(Len.CD_MM);
	//Original name: FILLER-SA-CURRENT-DATE-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: SA-CD-DD
	private String cdDd = DefaultValues.stringVal(Len.CD_DD);

	//==== METHODS ====
	public void setSaCurrentDt(String saCurrentDt) {
		int position = 1;
		byte[] buffer = getSaCurrentDateBytes();
		MarshalByte.writeString(buffer, position, saCurrentDt, Len.SA_CURRENT_DT);
		setSaCurrentDateBytes(buffer);
	}

	/**Original name: SA-CURRENT-DT<br>*/
	public String getSaCurrentDt() {
		int position = 1;
		return MarshalByte.readString(getSaCurrentDateBytes(), position, Len.SA_CURRENT_DT);
	}

	public String getSaCurrentDtFormatted() {
		int position = 1;
		return MarshalByte.readFixedString(getSaCurrentDateBytes(), position, Len.SA_CURRENT_DT);
	}

	public void setSaCurrentDateBytes(byte[] buffer) {
		setSaCurrentDateBytes(buffer, 1);
	}

	/**Original name: SA-CURRENT-DATE<br>*/
	public byte[] getSaCurrentDateBytes() {
		byte[] buffer = new byte[Len.SA_CURRENT_DATE];
		return getSaCurrentDateBytes(buffer, 1);
	}

	public void setSaCurrentDateBytes(byte[] buffer, int offset) {
		int position = offset;
		cdYyyy = MarshalByte.readFixedString(buffer, position, Len.CD_YYYY);
		position += Len.CD_YYYY;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cdMm = MarshalByte.readFixedString(buffer, position, Len.CD_MM);
		position += Len.CD_MM;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cdDd = MarshalByte.readFixedString(buffer, position, Len.CD_DD);
	}

	public byte[] getSaCurrentDateBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cdYyyy, Len.CD_YYYY);
		position += Len.CD_YYYY;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cdMm, Len.CD_MM);
		position += Len.CD_MM;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cdDd, Len.CD_DD);
		return buffer;
	}

	public String getCdYyyyFormatted() {
		return this.cdYyyy;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getCdMmFormatted() {
		return this.cdMm;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public String getCdDdFormatted() {
		return this.cdDd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CD_YYYY = 4;
		public static final int CD_MM = 2;
		public static final int CD_DD = 2;
		public static final int FLR1 = 1;
		public static final int SA_CURRENT_DATE = CD_YYYY + CD_MM + CD_DD + 2 * FLR1;
		public static final int SA_CURRENT_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int SA_CURRENT_DT = 10;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
