/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-03-REGION-SWAPPED<br>
 * Variable: EA-03-REGION-SWAPPED from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea03RegionSwapped {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-03-REGION-SWAPPED
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-03-REGION-SWAPPED-1
	private String flr2 = "THE CICS";
	//Original name: FILLER-EA-03-REGION-SWAPPED-2
	private String flr3 = "PIPE HAS BEEN";
	//Original name: FILLER-EA-03-REGION-SWAPPED-3
	private String flr4 = "CHANGED FROM";
	//Original name: EA-03-ORIG-REGION
	private String origRegion = DefaultValues.stringVal(Len.ORIG_REGION);
	//Original name: FILLER-EA-03-REGION-SWAPPED-4
	private String flr5 = " TO";
	//Original name: EA-03-REDIR-REGION
	private String redirRegion = DefaultValues.stringVal(Len.REDIR_REGION);

	//==== METHODS ====
	public String getEa03RegionSwappedFormatted() {
		return MarshalByteExt.bufferToStr(getEa03RegionSwappedBytes());
	}

	public byte[] getEa03RegionSwappedBytes() {
		byte[] buffer = new byte[Len.EA03_REGION_SWAPPED];
		return getEa03RegionSwappedBytes(buffer, 1);
	}

	public byte[] getEa03RegionSwappedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, origRegion, Len.ORIG_REGION);
		position += Len.ORIG_REGION;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, redirRegion, Len.REDIR_REGION);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setOrigRegion(String origRegion) {
		this.origRegion = Functions.subString(origRegion, Len.ORIG_REGION);
	}

	public String getOrigRegion() {
		return this.origRegion;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setRedirRegion(String redirRegion) {
		this.redirRegion = Functions.subString(redirRegion, Len.REDIR_REGION);
	}

	public String getRedirRegion() {
		return this.redirRegion;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ORIG_REGION = 8;
		public static final int REDIR_REGION = 8;
		public static final int FLR1 = 11;
		public static final int FLR2 = 9;
		public static final int FLR3 = 14;
		public static final int FLR4 = 13;
		public static final int FLR5 = 4;
		public static final int EA03_REGION_SWAPPED = ORIG_REGION + REDIR_REGION + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
