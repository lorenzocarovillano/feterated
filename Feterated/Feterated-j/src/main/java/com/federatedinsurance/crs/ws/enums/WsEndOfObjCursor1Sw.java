/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-END-OF-OBJ-CURSOR1-SW<br>
 * Variable: WS-END-OF-OBJ-CURSOR1-SW from program CAWPCORC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEndOfObjCursor1Sw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_OF_OBJ_CURSOR1 = 'Y';
	public static final char NOT_END_OF_OBJ_CURSOR1 = 'N';

	//==== METHODS ====
	public void setWsEndOfObjCursor1Sw(char wsEndOfObjCursor1Sw) {
		this.value = wsEndOfObjCursor1Sw;
	}

	public char getWsEndOfObjCursor1Sw() {
		return this.value;
	}

	public boolean isEndOfObjCursor1() {
		return value == END_OF_OBJ_CURSOR1;
	}

	public void setEndOfObjCursor1() {
		value = END_OF_OBJ_CURSOR1;
	}

	public void setNotEndOfObjCursor1() {
		value = NOT_END_OF_OBJ_CURSOR1;
	}
}
