/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: EXCI-DPL-RETAREA<br>
 * Variable: EXCI-DPL-RETAREA from copybook DFHXCPLO<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ExciDplRetareaTs547099 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: EXCI-DPL-RESP
	private long resp = DefaultValues.BIN_LONG_VAL;
	//Original name: EXCI-DPL-RESP2
	private long resp2 = DefaultValues.BIN_LONG_VAL;
	//Original name: EXCI-DPL-ABCODE
	private String abcode = DefaultValues.stringVal(Len.ABCODE);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.EXCI_DPL_RETAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setExciDplRetareaBytes(buf);
	}

	public String getExciDplRetareaFormatted() {
		return MarshalByteExt.bufferToStr(getExciDplRetareaBytes());
	}

	public void setExciDplRetareaBytes(byte[] buffer) {
		setExciDplRetareaBytes(buffer, 1);
	}

	public byte[] getExciDplRetareaBytes() {
		byte[] buffer = new byte[Len.EXCI_DPL_RETAREA];
		return getExciDplRetareaBytes(buffer, 1);
	}

	public void setExciDplRetareaBytes(byte[] buffer, int offset) {
		int position = offset;
		resp = MarshalByte.readBinaryUnsignedInt(buffer, position);
		position += Types.INT_SIZE;
		resp2 = MarshalByte.readBinaryUnsignedInt(buffer, position);
		position += Types.INT_SIZE;
		abcode = MarshalByte.readString(buffer, position, Len.ABCODE);
	}

	public byte[] getExciDplRetareaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryUnsignedInt(buffer, position, resp);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryUnsignedInt(buffer, position, resp2);
		position += Types.INT_SIZE;
		MarshalByte.writeString(buffer, position, abcode, Len.ABCODE);
		return buffer;
	}

	public void setResp(long resp) {
		this.resp = resp;
	}

	public long getResp() {
		return this.resp;
	}

	public void setResp2(long resp2) {
		this.resp2 = resp2;
	}

	public long getResp2() {
		return this.resp2;
	}

	public void setAbcode(String abcode) {
		this.abcode = Functions.subString(abcode, Len.ABCODE);
	}

	public String getAbcode() {
		return this.abcode;
	}

	@Override
	public byte[] serialize() {
		return getExciDplRetareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESP = 4;
		public static final int RESP2 = 4;
		public static final int ABCODE = 4;
		public static final int EXCI_DPL_RETAREA = RESP + RESP2 + ABCODE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
