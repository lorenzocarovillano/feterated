/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.WsBillingRegion;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P90K0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p90k0 {

	//==== PROPERTIES ====
	//Original name: WS-MAX-POL-ROWS
	private short wsMaxPolRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-MAX-FEE-ROWS
	private short wsMaxFeeRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-NBR-OF-NOT-DAY
	private short wsNbrOfNotDay = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-EIBRESP-CD
	private short wsEibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short wsEibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-CURRENT-TIMESTAMP-X
	private WsCurrentTimestampX wsCurrentTimestampX = new WsCurrentTimestampX();
	/**Original name: WS-BILLING-REGION<br>
	 * <pre>* !WARNING! DEV AND BETA BILLING REGIONS MAY CHANGE!
	 * * VERIFY THE FOLLOWING BEFORE TESTING!</pre>*/
	private WsBillingRegion wsBillingRegion = new WsBillingRegion();
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo wsErrorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-NOT-EFF-DT
	private String wsNotEffDt = DefaultValues.stringVal(Len.WS_NOT_EFF_DT);
	//Original name: WS-NOT-EFF-DT-MAX
	private SaDb2DateFormat wsNotEffDtMax = new SaDb2DateFormat();
	//Original name: WS-NBR-OF-NOT-DAY-X
	private String wsNbrOfNotDayX = DefaultValues.stringVal(Len.WS_NBR_OF_NOT_DAY_X);
	//Original name: WS-MAX-ROWS
	private String wsMaxRows = DefaultValues.stringVal(Len.WS_MAX_ROWS);
	//Original name: WS-PRODUCER-NBR
	private WsProducerNbr wsProducerNbr = new WsProducerNbr();
	//Original name: WS-MIN-POL-EFF-DT
	private String wsMinPolEffDt = DefaultValues.stringVal(Len.WS_MIN_POL_EFF_DT);
	//Original name: WS-POL-NBR
	private String wsPolNbr = DefaultValues.stringVal(Len.WS_POL_NBR);
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "XZ0P90K0";
	//Original name: WS-APPLICATION-NM
	private String wsApplicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-PREPARE-IMP-NOT
	private String wsBusObjNmPrepareImpNot = "XZ_PREPARE_IMP_NOTIFICATION";
	//Original name: WS-OPERATIONS-CALLED
	private WsOperationsCalledXz0p90k0 wsOperationsCalled = new WsOperationsCalledXz0p90k0();

	//==== METHODS ====
	public void setWsMaxPolRows(short wsMaxPolRows) {
		this.wsMaxPolRows = wsMaxPolRows;
	}

	public short getWsMaxPolRows() {
		return this.wsMaxPolRows;
	}

	public void setWsMaxFeeRows(short wsMaxFeeRows) {
		this.wsMaxFeeRows = wsMaxFeeRows;
	}

	public short getWsMaxFeeRows() {
		return this.wsMaxFeeRows;
	}

	public void setWsNbrOfNotDay(short wsNbrOfNotDay) {
		this.wsNbrOfNotDay = wsNbrOfNotDay;
	}

	public short getWsNbrOfNotDay() {
		return this.wsNbrOfNotDay;
	}

	public void setWsEibrespCd(short wsEibrespCd) {
		this.wsEibrespCd = wsEibrespCd;
	}

	public short getWsEibrespCd() {
		return this.wsEibrespCd;
	}

	public String getWsEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getWsEibrespCd(), Len.WS_EIBRESP_CD);
	}

	public String getWsEibrespCdAsString() {
		return getWsEibrespCdFormatted();
	}

	public void setWsEibresp2Cd(short wsEibresp2Cd) {
		this.wsEibresp2Cd = wsEibresp2Cd;
	}

	public short getWsEibresp2Cd() {
		return this.wsEibresp2Cd;
	}

	public String getWsEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getWsEibresp2Cd(), Len.WS_EIBRESP2_CD);
	}

	public String getWsEibresp2CdAsString() {
		return getWsEibresp2CdFormatted();
	}

	public void setWsNotEffDt(String wsNotEffDt) {
		this.wsNotEffDt = Functions.subString(wsNotEffDt, Len.WS_NOT_EFF_DT);
	}

	public String getWsNotEffDt() {
		return this.wsNotEffDt;
	}

	public void setWsNbrOfNotDayX(long wsNbrOfNotDayX) {
		this.wsNbrOfNotDayX = PicFormatter.display("Z(3)9").format(wsNbrOfNotDayX).toString();
	}

	public long getWsNbrOfNotDayX() {
		return PicParser.display("Z(3)9").parseLong(this.wsNbrOfNotDayX);
	}

	public String getWsNbrOfNotDayXFormatted() {
		return this.wsNbrOfNotDayX;
	}

	public String getWsNbrOfNotDayXAsString() {
		return getWsNbrOfNotDayXFormatted();
	}

	public void setWsMaxRows(long wsMaxRows) {
		this.wsMaxRows = PicFormatter.display("Z(3)9").format(wsMaxRows).toString();
	}

	public long getWsMaxRows() {
		return PicParser.display("Z(3)9").parseLong(this.wsMaxRows);
	}

	public String getWsMaxRowsFormatted() {
		return this.wsMaxRows;
	}

	public String getWsMaxRowsAsString() {
		return getWsMaxRowsFormatted();
	}

	public void setWsMinPolEffDt(String wsMinPolEffDt) {
		this.wsMinPolEffDt = Functions.subString(wsMinPolEffDt, Len.WS_MIN_POL_EFF_DT);
	}

	public String getWsMinPolEffDt() {
		return this.wsMinPolEffDt;
	}

	public void setWsPolNbr(String wsPolNbr) {
		this.wsPolNbr = Functions.subString(wsPolNbr, Len.WS_POL_NBR);
	}

	public String getWsPolNbr() {
		return this.wsPolNbr;
	}

	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public String getWsApplicationNm() {
		return this.wsApplicationNm;
	}

	public String getWsBusObjNmPrepareImpNot() {
		return this.wsBusObjNmPrepareImpNot;
	}

	public WsBillingRegion getWsBillingRegion() {
		return wsBillingRegion;
	}

	public WsCurrentTimestampX getWsCurrentTimestampX() {
		return wsCurrentTimestampX;
	}

	public WsErrorCheckInfo getWsErrorCheckInfo() {
		return wsErrorCheckInfo;
	}

	public SaDb2DateFormat getWsNotEffDtMax() {
		return wsNotEffDtMax;
	}

	public WsOperationsCalledXz0p90k0 getWsOperationsCalled() {
		return wsOperationsCalled;
	}

	public WsProducerNbr getWsProducerNbr() {
		return wsProducerNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_NOT_EFF_DT = 10;
		public static final int WS_NBR_OF_NOT_DAY_X = 4;
		public static final int WS_MAX_ROWS = 4;
		public static final int WS_MIN_POL_EFF_DT = 10;
		public static final int WS_POL_NBR = 25;
		public static final int WS_PROGRAM_NAME = 8;
		public static final int WS_EIBRESP_CD = 4;
		public static final int WS_EIBRESP2_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
