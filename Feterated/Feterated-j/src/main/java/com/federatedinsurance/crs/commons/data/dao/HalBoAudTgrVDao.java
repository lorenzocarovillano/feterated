/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalBoAudTgrV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_BO_AUD_TGR_V]
 * 
 */
public class HalBoAudTgrVDao extends BaseSqlDao<IHalBoAudTgrV> {

	private final IRowMapper<IHalBoAudTgrV> selectRecRm = buildNamedRowMapper(IHalBoAudTgrV.class, "tgrInd", "moduleNm");

	public HalBoAudTgrVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalBoAudTgrV> getToClass() {
		return IHalBoAudTgrV.class;
	}

	public IHalBoAudTgrV selectRec(String uowNm, String busObjNm, IHalBoAudTgrV iHalBoAudTgrV) {
		return buildQuery("selectRec").bind("uowNm", uowNm).bind("busObjNm", busObjNm).rowMapper(selectRecRm).singleResult(iHalBoAudTgrV);
	}
}
