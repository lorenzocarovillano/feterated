/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-DB2-DATE-FORMAT<br>
 * Variable: SA-DB2-DATE-FORMAT from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaDb2DateFormat {

	//==== PROPERTIES ====
	//Original name: SA-DB2-YYYY
	private String yyyy = DefaultValues.stringVal(Len.YYYY);
	//Original name: FILLER-SA-DB2-DATE-FORMAT
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: SA-DB2-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: FILLER-SA-DB2-DATE-FORMAT-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: SA-DB2-DD
	private String dd = DefaultValues.stringVal(Len.DD);

	//==== METHODS ====
	public void setWsNotEffDtMaxFormatted(String data) {
		byte[] buffer = new byte[Len.WS_NOT_EFF_DT_MAX];
		MarshalByte.writeString(buffer, 1, data, Len.WS_NOT_EFF_DT_MAX);
		setWsNotEffDtMaxBytes(buffer, 1);
	}

	public byte[] getWsNotEffDtMaxBytes() {
		byte[] buffer = new byte[Len.WS_NOT_EFF_DT_MAX];
		return getWsNotEffDtMaxBytes(buffer, 1);
	}

	public void setWsNotEffDtMaxBytes(byte[] buffer, int offset) {
		int position = offset;
		yyyy = MarshalByte.readString(buffer, position, Len.YYYY);
		position += Len.YYYY;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mm = MarshalByte.readString(buffer, position, Len.MM);
		position += Len.MM;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dd = MarshalByte.readString(buffer, position, Len.DD);
	}

	public byte[] getWsNotEffDtMaxBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, yyyy, Len.YYYY);
		position += Len.YYYY;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position += Len.MM;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		return buffer;
	}

	public void initWsNotEffDtMaxSpaces() {
		yyyy = "";
		flr1 = Types.SPACE_CHAR;
		mm = "";
		flr2 = Types.SPACE_CHAR;
		dd = "";
	}

	public void setYyyy(String yyyy) {
		this.yyyy = Functions.subString(yyyy, Len.YYYY);
	}

	public String getYyyy() {
		return this.yyyy;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setMm(String mm) {
		this.mm = Functions.subString(mm, Len.MM);
	}

	public String getMm() {
		return this.mm;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setDd(String dd) {
		this.dd = Functions.subString(dd, Len.DD);
	}

	public String getDd() {
		return this.dd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YYYY = 4;
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int FLR1 = 1;
		public static final int WS_NOT_EFF_DT_MAX = YYYY + MM + DD + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
