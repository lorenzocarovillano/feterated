/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.Conditions;
import com.federatedinsurance.crs.commons.data.dao.ActNotPol1Dao;
import com.federatedinsurance.crs.commons.data.dao.PolDtaExtDao;
import com.federatedinsurance.crs.copy.SqlcaTs030199;
import com.federatedinsurance.crs.ws.DfhcommareaXzc02090;
import com.federatedinsurance.crs.ws.LSearchCriteria;
import com.federatedinsurance.crs.ws.Xzc02090Data;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZC02090<br>
 * <pre>AUTHOR.       Anthony Lindsay.
 * DATE-WRITTEN. 11 MAR 2010.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - POLICY TERMINATION STATUS CICS PROGRAM      **
 * *                 SERVICE NAME:XZGetPolTmnStatus              **
 * *                                                             **
 * * PURPOSE -  FIND IF A POLICY TERM IS SET TO BE TERMINATED.   **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.        **
 * *                                                             **
 * ****************************************************************
 * *                                                             **
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * --------- ---------- -------   -----------------------------**
 * *TO07602-22 03-11-2010 E404ABL   NEW                          **
 * *TO07602-40 10-25-2010 E404KXS   GET MOST EFFECTIVE STATUS    **
 * *                                IF NO DATE IS PASSED IN.     **
 * ****************************************************************</pre>*/
public class Xzc02090 extends Program {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private SqlcaTs030199 sqlca = new SqlcaTs030199();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ExecContext execContext = null;
	private PolDtaExtDao polDtaExtDao = new PolDtaExtDao(dbAccessStatus);
	private ActNotPol1Dao actNotPol1Dao = new ActNotPol1Dao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xzc02090Data ws = new Xzc02090Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaXzc02090 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaXzc02090 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		programExit();
		return 0;
	}

	public static Xzc02090 getInstance() {
		return (Programs.getInstance(Xzc02090.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: PERFORM 3000-GET-POL-TMN-STA
		//              THRU 3000-EXIT.
		getPolTmnSta();
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  VERIFY THE INPUT SENT CONTAINS AN ACCOUNT NUMBER.            *
	 * ***************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE XZC020-SERVICE-OUTPUTS
		//                      XZC020-ERROR-HANDLING-PARMS.
		initXzc020ServiceOutputs();
		initXzc020ErrorHandlingParms();
		// COB_CODE: SET XZC020-NO-ERROR-CODE    TO TRUE.
		dfhcommarea.getXzc020ErrorReturnCode().setNoErrorCode();
		// COB_CODE: MOVE +1                     TO SS-NLBE-IDX.
		ws.setSsNlbeIdx(((short) 1));
		// COB_CODE: PERFORM 2100-VERIFY-INPUT
		//              THRU 2100-EXIT.
		verifyInput();
	}

	/**Original name: 2100-VERIFY-INPUT<br>
	 * <pre>**********************************************************
	 *   VERIFY POLICY TERM DATES OR FIND DATE NOT PASSED IN    *
	 * **********************************************************</pre>*/
	private void verifyInput() {
		// COB_CODE: INITIALIZE DCLPOL-DTA-EXT.
		initDclpolDtaExt();
		// BOTH EFFECTIVE AND EXPIRATION DATE PASSED IN
		// COB_CODE: IF XZC02I-POL-EFF-DT NOT = SPACES
		//             AND
		//              XZC02I-POL-EXP-DT NOT = SPACES
		//               GO TO 2100-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(dfhcommarea.getiPolEffDt()) && !Characters.EQ_SPACE.test(dfhcommarea.getiPolExpDt())) {
			// COB_CODE: PERFORM 2110-VERIFY-POLICY-TERM
			//              THRU 2110-EXIT
			verifyPolicyTerm();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// ONLY EFFECTIVE DATE PASSED IN
		// COB_CODE: IF XZC02I-POL-EFF-DT NOT = SPACES
		//             AND
		//              XZC02I-POL-EXP-DT = SPACES
		//               GO TO 2100-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(dfhcommarea.getiPolEffDt()) && Characters.EQ_SPACE.test(dfhcommarea.getiPolExpDt())) {
			// COB_CODE: PERFORM 2120-FIND-POL-EXP-DT
			//              THRU 2120-EXIT
			findPolExpDt();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// ONLY EXPIRATION DATE PASSED IN
		// COB_CODE: IF XZC02I-POL-EFF-DT = SPACES
		//             AND
		//              XZC02I-POL-EXP-DT NOT = SPACES
		//               GO TO 2100-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getiPolEffDt()) && !Characters.EQ_SPACE.test(dfhcommarea.getiPolExpDt())) {
			// COB_CODE: PERFORM 2130-FIND-POL-EFF-DT
			//              THRU 2130-EXIT
			findPolEffDt();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// NO DATES PASSED IN - GET MOST EFFECTIVE TERM
		// COB_CODE: PERFORM 2140-FIND-POL-DTS
		//              THRU 2140-EXIT.
		findPolDts();
	}

	/**Original name: 2110-VERIFY-POLICY-TERM<br>
	 * <pre>**********************************************************
	 *   VERIFY POLICY TERM DATES                               *
	 * **********************************************************</pre>*/
	private void verifyPolicyTerm() {
		// COB_CODE: MOVE XZC02I-POL-NBR         TO POL-NBR
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC02I-POL-EFF-DT      TO POL-EFF-DT
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: MOVE XZC02I-POL-EXP-DT      TO PLN-EXP-DT
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPlnExpDt(dfhcommarea.getiPolExpDt());
		// COB_CODE: EXEC SQL
		//               SELECT ACT_NBR
		//                 INTO :DCLPOL-DTA-EXT.ACT-NBR
		//                 FROM POL_DTA_EXT
		//                WHERE POL_NBR = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND POL_EFF_DT = :DCLPOL-DTA-EXT.POL-EFF-DT
		//                  AND PLN_EXP_DT = :DCLPOL-DTA-EXT.PLN-EXP-DT
		//                FETCH FIRST 1 ROW ONLY
		//           END-EXEC.
		ws.getDclpolDtaExt().setActNbr(polDtaExtDao.selectRec6(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt().getPolEffDt(),
				ws.getDclpolDtaExt().getPlnExpDt(), ws.getDclpolDtaExt().getActNbr()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                   CONTINUE
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                   GO TO 1000-PROGRAM-EXIT
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: CONTINUE
			//continue
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE CF-PROGRAM-NAME
			//                               TO EA-01-PROGRAM-NAME
			ws.getErrorAndAdviceMessages().getEa01InvalidPolicyTerm().setProgramName(ws.getConstantFields().getProgramName());
			// COB_CODE: MOVE CF-PN-2110     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01InvalidPolicyTerm().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2110());
			// COB_CODE: MOVE XZC02I-POL-NBR TO EA-01-POL-NBR
			ws.getErrorAndAdviceMessages().getEa01InvalidPolicyTerm().setPolNbr(dfhcommarea.getiPolNbr());
			// COB_CODE: MOVE XZC02I-POL-EFF-DT
			//                               TO EA-01-POL-EFF-DT
			ws.getErrorAndAdviceMessages().getEa01InvalidPolicyTerm().setPolEffDt(dfhcommarea.getiPolEffDt());
			// COB_CODE: MOVE XZC02I-POL-EXP-DT
			//                               TO EA-01-POL-EXP-DT
			ws.getErrorAndAdviceMessages().getEa01InvalidPolicyTerm().setPolExpDt(dfhcommarea.getiPolExpDt());
			// COB_CODE: SET XZC020-NLBE-CODE
			//                               TO TRUE
			dfhcommarea.getXzc020ErrorReturnCode().setNlbeCode();
			// COB_CODE: MOVE EA-01-INVALID-POLICY-TERM
			//                               TO
			//                           XZC020-NON-LOG-ERR-MSG(SS-NLBE-IDX)
			dfhcommarea.getXzc020NonLoggableErrors(ws.getSsNlbeIdx())
					.setXzc020NonLogErrMsg(ws.getErrorAndAdviceMessages().getEa01InvalidPolicyTerm().getEa01InvalidPolicyTermFormatted());
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		} else {
			// COB_CODE: MOVE CF-PN-2110     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2110());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 2120-FIND-POL-EXP-DT<br>
	 * <pre>**********************************************************
	 *   GET POLICY EXPIRATION DATE                             *
	 * **********************************************************</pre>*/
	private void findPolExpDt() {
		// COB_CODE: MOVE XZC02I-POL-NBR         TO POL-NBR
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC02I-POL-EFF-DT      TO POL-EFF-DT
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: EXEC SQL
		//               SELECT PLN_EXP_DT
		//                    , ACT_NBR
		//                 INTO :DCLPOL-DTA-EXT.PLN-EXP-DT
		//                    , :DCLPOL-DTA-EXT.ACT-NBR
		//                 FROM POL_DTA_EXT
		//                WHERE POL_NBR = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND POL_EFF_DT = :DCLPOL-DTA-EXT.POL-EFF-DT
		//                FETCH FIRST 1 ROW ONLY
		//           END-EXEC.
		polDtaExtDao.selectRec7(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt().getPolEffDt(), ws.getDclpolDtaExt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                                       TO XZC02I-POL-EXP-DT
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                   GO TO 1000-PROGRAM-EXIT
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: MOVE PLN-EXP-DT     OF DCLPOL-DTA-EXT
			//                               TO XZC02I-POL-EXP-DT
			dfhcommarea.setiPolExpDt(ws.getDclpolDtaExt().getPlnExpDt());
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE CF-PROGRAM-NAME
			//                               TO EA-02-PROGRAM-NAME
			ws.getErrorAndAdviceMessages().getEa02InvalidPolEffDt().setProgramName(ws.getConstantFields().getProgramName());
			// COB_CODE: MOVE CF-PN-2120     TO EA-02-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa02InvalidPolEffDt().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2120());
			// COB_CODE: MOVE XZC02I-POL-NBR TO EA-02-POL-NBR
			ws.getErrorAndAdviceMessages().getEa02InvalidPolEffDt().setPolNbr(dfhcommarea.getiPolNbr());
			// COB_CODE: MOVE XZC02I-POL-EFF-DT
			//                               TO EA-02-POL-EFF-DT
			ws.getErrorAndAdviceMessages().getEa02InvalidPolEffDt().setPolEffDt(dfhcommarea.getiPolEffDt());
			// COB_CODE: SET XZC020-NLBE-CODE
			//                               TO TRUE
			dfhcommarea.getXzc020ErrorReturnCode().setNlbeCode();
			// COB_CODE: MOVE EA-02-INVALID-POL-EFF-DT
			//                               TO
			//                           XZC020-NON-LOG-ERR-MSG(SS-NLBE-IDX)
			dfhcommarea.getXzc020NonLoggableErrors(ws.getSsNlbeIdx())
					.setXzc020NonLogErrMsg(ws.getErrorAndAdviceMessages().getEa02InvalidPolEffDt().getEa02InvalidPolEffDtFormatted());
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		} else {
			// COB_CODE: MOVE CF-PN-2120     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2120());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 2130-FIND-POL-EFF-DT<br>
	 * <pre>**********************************************************
	 *   GET POLICY EFFECTIVE DATE                              *
	 * **********************************************************</pre>*/
	private void findPolEffDt() {
		// COB_CODE: MOVE XZC02I-POL-NBR         TO POL-NBR
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC02I-POL-EXP-DT      TO PLN-EXP-DT
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPlnExpDt(dfhcommarea.getiPolExpDt());
		// COB_CODE: EXEC SQL
		//               SELECT POL_EFF_DT
		//                    , ACT_NBR
		//                 INTO :DCLPOL-DTA-EXT.POL-EFF-DT
		//                    , :DCLPOL-DTA-EXT.ACT-NBR
		//                 FROM POL_DTA_EXT
		//                WHERE POL_NBR = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND PLN_EXP_DT = :DCLPOL-DTA-EXT.PLN-EXP-DT
		//                FETCH FIRST 1 ROW ONLY
		//           END-EXEC.
		polDtaExtDao.selectRec8(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt().getPlnExpDt(), ws.getDclpolDtaExt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                                       TO XZC02I-POL-EFF-DT
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                   GO TO 1000-PROGRAM-EXIT
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: MOVE POL-EFF-DT     OF DCLPOL-DTA-EXT
			//                               TO XZC02I-POL-EFF-DT
			dfhcommarea.setiPolEffDt(ws.getDclpolDtaExt().getPolEffDt());
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE CF-PROGRAM-NAME
			//                               TO EA-03-PROGRAM-NAME
			ws.getErrorAndAdviceMessages().getEa03InvalidPolExpDt().setProgramName(ws.getConstantFields().getProgramName());
			// COB_CODE: MOVE CF-PN-2130     TO EA-03-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa03InvalidPolExpDt().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2130());
			// COB_CODE: MOVE XZC02I-POL-NBR TO EA-03-POL-NBR
			ws.getErrorAndAdviceMessages().getEa03InvalidPolExpDt().setPolNbr(dfhcommarea.getiPolNbr());
			// COB_CODE: MOVE XZC02I-POL-EXP-DT
			//                               TO EA-03-POL-EXP-DT
			ws.getErrorAndAdviceMessages().getEa03InvalidPolExpDt().setPolExpDt(dfhcommarea.getiPolExpDt());
			// COB_CODE: SET XZC020-NLBE-CODE
			//                               TO TRUE
			dfhcommarea.getXzc020ErrorReturnCode().setNlbeCode();
			// COB_CODE: MOVE EA-03-INVALID-POL-EXP-DT
			//                               TO
			//                           XZC020-NON-LOG-ERR-MSG(SS-NLBE-IDX)
			dfhcommarea.getXzc020NonLoggableErrors(ws.getSsNlbeIdx())
					.setXzc020NonLogErrMsg(ws.getErrorAndAdviceMessages().getEa03InvalidPolExpDt().getEa03InvalidPolExpDtFormatted());
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		} else {
			// COB_CODE: MOVE CF-PN-2130     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2130());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 2140-FIND-POL-DTS<br>
	 * <pre> T60240 START
	 * **********************************************************
	 *   GET THE MOST EFFECTIVE POLICY TERM DATES
	 * **********************************************************</pre>*/
	private void findPolDts() {
		// COB_CODE: MOVE XZC02I-POL-NBR         TO POL-NBR
		//                                       OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: EXEC SQL
		//               SELECT PD.POL_EFF_DT
		//                    , PD.PLN_EXP_DT
		//                    , PD.ACT_NBR
		//                 INTO :DCLPOL-DTA-EXT.POL-EFF-DT
		//                    , :DCLPOL-DTA-EXT.PLN-EXP-DT
		//                    , :DCLPOL-DTA-EXT.ACT-NBR
		//                 FROM POL_DTA_EXT PD
		//                WHERE PD.POL_NBR = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND PD.PLN_EXP_DT = (
		//                          SELECT MAX(PD2.PLN_EXP_DT)
		//                            FROM POL_DTA_EXT PD2
		//                           WHERE PD2.POL_NBR =
		//                                       :DCLPOL-DTA-EXT.POL-NBR)
		//           END-EXEC.
		polDtaExtDao.selectByR(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                                       TO XZC02I-POL-EXP-DT
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                   GO TO 1000-PROGRAM-EXIT
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: MOVE POL-EFF-DT     OF DCLPOL-DTA-EXT
			//                               TO XZC02I-POL-EFF-DT
			dfhcommarea.setiPolEffDt(ws.getDclpolDtaExt().getPolEffDt());
			// COB_CODE: MOVE PLN-EXP-DT     OF DCLPOL-DTA-EXT
			//                               TO XZC02I-POL-EXP-DT
			dfhcommarea.setiPolExpDt(ws.getDclpolDtaExt().getPlnExpDt());
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE CF-PROGRAM-NAME
			//                               TO EA-04-PROGRAM-NAME
			ws.getErrorAndAdviceMessages().getEa04InvalidPolicy().setProgramName(ws.getConstantFields().getProgramName());
			// COB_CODE: MOVE CF-PN-2140     TO EA-04-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa04InvalidPolicy().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2140());
			// COB_CODE: MOVE XZC02I-POL-NBR TO EA-04-POL-NBR
			ws.getErrorAndAdviceMessages().getEa04InvalidPolicy().setPolNbr(dfhcommarea.getiPolNbr());
			// COB_CODE: SET XZC020-NLBE-CODE
			//                               TO TRUE
			dfhcommarea.getXzc020ErrorReturnCode().setNlbeCode();
			// COB_CODE: MOVE EA-04-INVALID-POLICY
			//                               TO
			//                           XZC020-NON-LOG-ERR-MSG(SS-NLBE-IDX)
			dfhcommarea.getXzc020NonLoggableErrors(ws.getSsNlbeIdx())
					.setXzc020NonLogErrMsg(ws.getErrorAndAdviceMessages().getEa04InvalidPolicy().getEa04InvalidPolicyFormatted());
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		} else {
			// COB_CODE: MOVE CF-PN-2140     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2140());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 3000-GET-POL-TMN-STA<br>
	 * <pre> T60240 END</pre>*/
	private void getPolTmnSta() {
		// COB_CODE: PERFORM 3100-SET-INPUT
		//              THRU 3100-EXIT.
		setInput();
		// COB_CODE: PERFORM 3200-DETERMINE-POL-TMN-STA
		//              THRU 3200-EXIT.
		determinePolTmnSta();
		// COB_CODE: PERFORM 3300-SET-OUTPUT
		//              THRU 3300-EXIT.
		setOutput();
	}

	/**Original name: 3100-SET-INPUT<br>
	 * <pre>******************************************************
	 *  SET UP INPUT VARIABLES                              *
	 * ******************************************************</pre>*/
	private void setInput() {
		// COB_CODE: INITIALIZE DCLACT-NOT-POL.
		initDclactNotPol();
		// COB_CODE: MOVE XZC02I-POL-NBR         TO POL-NBR
		//                                       OF DCLACT-NOT-POL.
		ws.getDclactNotPol().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC02I-POL-EFF-DT      TO POL-EFF-DT
		//                                       OF DCLACT-NOT-POL.
		ws.getDclactNotPol().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: MOVE XZC02I-POL-EXP-DT      TO POL-EXP-DT
		//                                       OF DCLACT-NOT-POL.
		ws.getDclactNotPol().setPolExpDt(dfhcommarea.getiPolExpDt());
		// ACCOUNT NUMBER WAS FOUND IN THE 2100- PARAGRAPHS AND IS USED
		// USED TO IMPROVE EFFICIENCY ON THE POL TMN STA SQL STATEMENT.
		// COB_CODE: MOVE ACT-NBR                OF DCLPOL-DTA-EXT
		//                                       TO CSR-ACT-NBR OF DCLACT-NOT.
		ws.getDclactNot().setCsrActNbr(ws.getDclpolDtaExt().getActNbr());
	}

	/**Original name: 3200-DETERMINE-POL-TMN-STA<br>
	 * <pre>**********************************************************
	 *   CHECK TO SEE IF THE MOST RECENT TERMINATION RELATED    *
	 *   POLICY IS A 'UTM'.  IF SO WE WILL SET THE UNDERWRITE   *
	 *   TERMINATE INDICATOR TO 'Y'.                            *
	 * **********************************************************</pre>*/
	private void determinePolTmnSta() {
		// COB_CODE: EXEC SQL
		//               SELECT B.NOT_DT
		//                    , B.EMP_ID
		//                 INTO :DCLACT-NOT.NOT-DT
		//                    , :DCLACT-NOT.EMP-ID
		//                        :NI-EMP-ID
		//                 FROM ACT_NOT_POL A
		//                    , ACT_NOT B
		//                WHERE A.POL_NBR = :DCLACT-NOT-POL.POL-NBR
		//                  AND A.POL_EFF_DT = :DCLACT-NOT-POL.POL-EFF-DT
		//                  AND A.POL_EXP_DT = :DCLACT-NOT-POL.POL-EXP-DT
		//                  AND A.CSR_ACT_NBR = :DCLACT-NOT.CSR-ACT-NBR
		//                  AND A.NOT_EFF_DT IS NOT NULL
		//                  AND A.NOT_PRC_TS =
		//                         (SELECT MAX(D.NOT_PRC_TS)
		//                            FROM ACT_NOT_POL D
		//                               , ACT_NOT E
		//                           WHERE D.CSR_ACT_NBR = A.CSR_ACT_NBR
		//                             AND D.POL_EFF_DT = A.POL_EFF_DT
		//                             AND D.POL_EXP_DT = A.POL_EXP_DT
		//                             AND D.CSR_ACT_NBR = E.CSR_ACT_NBR
		//                             AND D.NOT_PRC_TS = E.NOT_PRC_TS
		//                             AND D.POL_NBR = A.POL_NBR
		//                             AND E.ACT_NOT_STA_CD = '70'
		//                             AND E.ACT_NOT_TYP_CD
		//                                 IN ('UTM','URT'))
		//                  AND A.CSR_ACT_NBR = B.CSR_ACT_NBR
		//                  AND A.NOT_PRC_TS = B.NOT_PRC_TS
		//                  AND B.ACT_NOT_TYP_CD = 'UTM'
		//           END-EXEC.
		actNotPol1Dao.selectRec4(ws.getDclactNotPol().getPolNbr(), ws.getDclactNotPol().getPolEffDt(), ws.getDclactNotPol().getPolExpDt(),
				ws.getDclactNot().getCsrActNbr(), ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SQL-GOOD
		//                   END-IF
		//               WHEN SQLCODE = CF-SQL-NOT-FOUND
		//                                       OF DCLACT-NOT
		//               WHEN OTHER
		//                      THRU 9910-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getConstantFields().getSqlGood()) {
			// COB_CODE: IF NI-EMP-ID = CF-DB2-IS-NULL
			//                               OF DCLACT-NOT
			//           END-IF
			if (ws.getNiEmpId() == ws.getConstantFields().getDb2IsNull()) {
				// COB_CODE: MOVE SPACES     TO EMP-ID
				//                           OF DCLACT-NOT
				ws.getDclactNot().setEmpId("");
			}
		} else if (sqlca.getSqlcode() == ws.getConstantFields().getSqlNotFound()) {
			// COB_CODE: MOVE SPACES         TO NOT-DT
			//                               OF DCLACT-NOT
			ws.getDclactNot().setNotDt("");
		} else {
			// COB_CODE: MOVE CF-PN-3200     TO EA-10-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3200());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 3300-SET-OUTPUT<br>
	 * <pre>******************************************************
	 *  SET THE OUTPUT TO BE RETURNED                       *
	 * ******************************************************</pre>*/
	private void setOutput() {
		// COB_CODE: MOVE POL-NBR                OF DCLACT-NOT-POL
		//                                       TO XZC02O-POL-NBR.
		dfhcommarea.getoPolKeys().setNbr(ws.getDclactNotPol().getPolNbr());
		// COB_CODE: MOVE POL-EFF-DT             OF DCLACT-NOT-POL
		//                                       TO XZC02O-POL-EFF-DT.
		dfhcommarea.getoPolKeys().setEffDt(ws.getDclactNotPol().getPolEffDt());
		// COB_CODE: MOVE POL-EXP-DT             OF DCLACT-NOT-POL
		//                                       TO XZC02O-POL-EXP-DT.
		dfhcommarea.getoPolKeys().setExpDt(ws.getDclactNotPol().getPolExpDt());
		// COB_CODE: IF NOT-DT OF DCLACT-NOT = SPACES
		//               GO TO 3300-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclactNot().getNotDt())) {
			// COB_CODE: MOVE CF-NO              TO XZC02O-UW-TMN-FLG
			dfhcommarea.setoUwTmnFlg(ws.getConstantFields().getNo());
			// COB_CODE: GO TO 3300-EXIT
			return;
		}
		// COB_CODE: MOVE CF-YES                 TO XZC02O-UW-TMN-FLG.
		dfhcommarea.setoUwTmnFlg(ws.getConstantFields().getYes());
		// COB_CODE: MOVE NOT-DT                 OF DCLACT-NOT
		//                                       TO XZC02O-TMN-PRC-DT.
		dfhcommarea.setoTmnPrcDt(ws.getDclactNot().getNotDt());
		//    FIND EMPLOYEE NAME
		// COB_CODE: IF EMP-ID OF DCLACT-NOT = SPACES
		//                                          XZC02O-TMN-EMP-ID
		//           ELSE
		//               END-IF
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclactNot().getEmpId())) {
			// COB_CODE: MOVE SPACES             TO XZC02O-TMN-EMP-NM
			//                                      XZC02O-TMN-EMP-ID
			dfhcommarea.setoTmnEmpNm("");
			dfhcommarea.setoTmnEmpId("");
		} else if (Conditions.eq(ws.getDclactNot().getEmpId(), ws.getConstantFields().getConvertedEmpId())) {
			// COB_CODE: IF EMP-ID OF DCLACT-NOT = CF-CONVERTED-EMP-ID
			//                                   TO XZC02O-TMN-EMP-NM
			//           ELSE
			//               MOVE L-OD-FUL-NM    TO XZC02O-TMN-EMP-NM
			//           END-IF
			// COB_CODE: MOVE SPACES         TO XZC02O-TMN-EMP-ID
			//                                  XZC02O-TMN-PRC-DT
			dfhcommarea.setoTmnEmpId("");
			dfhcommarea.setoTmnPrcDt("");
			// COB_CODE: MOVE CF-CONVERTED-TMN
			//                               TO XZC02O-TMN-EMP-NM
			dfhcommarea.setoTmnEmpNm(ws.getConstantFields().getConvertedTmn());
		} else {
			// COB_CODE: MOVE EMP-ID         OF DCLACT-NOT
			//                               TO XZC02O-TMN-EMP-ID
			dfhcommarea.setoTmnEmpId(ws.getDclactNot().getEmpId());
			// COB_CODE: PERFORM 3310-GET-EMP-NM
			//              THRU 3310-EXIT
			getEmpNm();
			// COB_CODE: MOVE L-OD-FUL-NM    TO XZC02O-TMN-EMP-NM
			dfhcommarea.setoTmnEmpNm(ws.getTt008001().getOdPplData().getFulNm());
		}
	}

	/**Original name: 3310-GET-EMP-NM<br>
	 * <pre>**********************************************************
	 *  FIND EMPLOYEE NAME USING PEOPLE SEARCH SERVICE          *
	 * **********************************************************
	 *  SET PEOPLE SEARCH INPUTS.</pre>*/
	private void getEmpNm() {
		// COB_CODE: MOVE XZC02I-USERID          TO L-SC-USER.
		ws.getTt008001().getSearchCriteria().setUser(dfhcommarea.getiUserid());
		// EXPLANATION OF FLAGS SET:
		//    (1) EMPLOYEE SEARCH TYPE
		//    (2) ACTIVE EMPLOYEES ONLY
		//    (3) EMPLOYEES ONLY (EXCLUDE NON-EMPLOYEES)
		//    (4) SINGLE USER ID RETURNED
		// COB_CODE: SET L-SC-SER-TYP-EMP
		//               L-SC-STA-ACY
		//               L-SC-EMP-CD
		//               L-SC-SGL-USR-ID-CD      TO TRUE.
		ws.getTt008001().getSearchCriteria().getSerTyp().setEmp();
		ws.getTt008001().getSearchCriteria().getStaCd().setAcy();
		ws.getTt008001().getSearchCriteria().getEmpNonempCd().setEmpCd();
		ws.getTt008001().getSearchCriteria().getUsrIdCd().setSglUsrIdCd();
		//    USE "EQUAL TO" EMPLOYEE ID AS CRITERA #1
		// COB_CODE: MOVE EMP-ID                 OF DCLACT-NOT
		//                                       TO L-SC-CRT1-VAL.
		ws.getTt008001().getSearchCriteria().setCrt1Val(ws.getDclactNot().getEmpId());
		// COB_CODE: SET L-SC-CRT1-TYP-EMP-ID
		//               L-SC-CRT1-COP-EQU       TO TRUE.
		ws.getTt008001().getSearchCriteria().getCrt1Typ().setEmpId();
		ws.getTt008001().getSearchCriteria().getCrt1Cop().setEqu();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-PEOPLE-SEARCH-MODULE)
		//               COMMAREA(L-SEARCH-CRITERIA)
		//               RESP    (WS-RESPONSE-CODE)
		//               RESP2   (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC02090", execContext).commarea(ws.getTt008001().getSearchCriteria()).length(LSearchCriteria.Len.SEARCH_CRITERIA)
				.link(ws.getConstantFields().getPeopleSearchModule(), new Tt008099());
		ws.setWsResponseCode(execContext.getResp());
		ws.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE TRUE
		//               WHEN WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                      THRU 9920-EXIT
		//               WHEN L-OD-ERR
		//                   GO TO 1000-PROGRAM-EXIT
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3310     TO EA-20-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3310());
			// COB_CODE: MOVE CF-PEOPLE-SEARCH-MODULE
			//                               TO EA-20-CICS-MODULE
			ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setCicsModule(ws.getConstantFields().getPeopleSearchModule());
			// COB_CODE: PERFORM 9920-CICS-LINK-ERROR
			//              THRU 9920-EXIT
			cicsLinkError();
		} else if (ws.getTt008001().getOdErrInd().isErr()) {
			// COB_CODE: SET XZC020-FATAL-ERROR-CODE
			//                               TO TRUE
			dfhcommarea.getXzc020ErrorReturnCode().setFatalErrorCode();
			// COB_CODE: MOVE CF-PROGRAM-NAME
			//                               TO EA-21-PROGRAM-NAME
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setProgramName(ws.getConstantFields().getProgramName());
			// COB_CODE: MOVE CF-PN-3310     TO EA-21-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3310());
			// COB_CODE: MOVE CF-PEOPLE-SEARCH-MODULE
			//                               TO EA-21-CICS-MODULE
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setCicsModule(ws.getConstantFields().getPeopleSearchModule());
			// COB_CODE: MOVE EMP-ID         OF DCLACT-NOT
			//                               TO EA-21-EMP-ID
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setEmpId(ws.getDclactNot().getEmpId());
			// COB_CODE: MOVE XZC02I-POL-NBR TO EA-21-POL-NBR
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setPolNbr(dfhcommarea.getiPolNbr());
			// COB_CODE: MOVE XZC02I-POL-EFF-DT
			//                               TO EA-21-POL-EFF-DT
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setPolEffDt(dfhcommarea.getiPolEffDt());
			// COB_CODE: MOVE XZC02I-POL-EXP-DT
			//                               TO EA-21-POL-EXP-DT
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setPolExpDt(dfhcommarea.getiPolExpDt());
			// COB_CODE: MOVE L-OD-MSG(1:54) TO EA-21-ERR-MSG
			ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().setErrMsg(ws.getTt008001().getOdMsgFormatted().substring((1) - 1, 54));
			// COB_CODE: MOVE EA-21-PEOPLE-SEARCH-ERROR
			//                               TO XZC020-FATAL-ERROR-MESSAGE
			dfhcommarea.setXzc020FatalErrorMessage(ws.getErrorAndAdviceMessages().getEa21PeopleSearchError().getEa21PeopleSearchErrorFormatted());
			// COB_CODE: INITIALIZE XZC020-SERVICE-OUTPUTS
			initXzc020ServiceOutputs();
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
	}

	/**Original name: 9910-SELECT-ERROR<br>
	 * <pre>******************************************************
	 *  SET UP ERROR ON THE SELECT                          *
	 * ******************************************************</pre>*/
	private void selectError() {
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO EA-10-PROGRAM-NAME.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setProgramName(ws.getConstantFields().getProgramName());
		// COB_CODE: MOVE SQLCODE                TO EA-10-SQLCODE.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setSqlcode(sqlca.getSqlcode());
		// COB_CODE: MOVE SQLSTATE               TO EA-10-SQLSTATE.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setSqlstate(sqlca.getSqlstate());
		// COB_CODE: MOVE XZC02I-POL-NBR         TO EA-10-POL-NBR.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC02I-POL-EFF-DT      TO EA-10-POL-EFF-DT.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: MOVE XZC02I-POL-EXP-DT      TO EA-10-POL-EXP-DT.
		ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().setPolExpDt(dfhcommarea.getiPolExpDt());
		// COB_CODE: SET XZC020-FATAL-ERROR-CODE TO TRUE.
		dfhcommarea.getXzc020ErrorReturnCode().setFatalErrorCode();
		// COB_CODE: MOVE EA-10-DB2-ERROR-ON-SELECT
		//                                       TO XZC020-FATAL-ERROR-MESSAGE.
		dfhcommarea.setXzc020FatalErrorMessage(ws.getErrorAndAdviceMessages().getEa10Db2ErrorOnSelect().getEa10Db2ErrorOnSelectFormatted());
		// COB_CODE: INITIALIZE XZC020-SERVICE-OUTPUTS.
		initXzc020ServiceOutputs();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9920-CICS-LINK-ERROR<br>
	 * <pre>******************************************************
	 *  SET UP ERROR ON CICS LINK                           *
	 * ******************************************************</pre>*/
	private void cicsLinkError() {
		// COB_CODE: SET XZC020-FATAL-ERROR-CODE TO TRUE.
		dfhcommarea.getXzc020ErrorReturnCode().setFatalErrorCode();
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO EA-20-PROGRAM-NAME.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setProgramName(ws.getConstantFields().getProgramName());
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EA-20-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setResponseCode(ws.getWsResponseCode());
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EA-20-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setResponseCode2(ws.getWsResponseCode2());
		// COB_CODE: MOVE XZC02I-POL-NBR         TO EA-20-POL-NBR.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setPolNbr(dfhcommarea.getiPolNbr());
		// COB_CODE: MOVE XZC02I-POL-EFF-DT      TO EA-20-POL-EFF-DT.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setPolEffDt(dfhcommarea.getiPolEffDt());
		// COB_CODE: MOVE XZC02I-POL-EXP-DT      TO EA-20-POL-EXP-DT.
		ws.getErrorAndAdviceMessages().getEa20CicsLinkError().setPolExpDt(dfhcommarea.getiPolExpDt());
		// COB_CODE: MOVE EA-20-CICS-LINK-ERROR  TO XZC020-FATAL-ERROR-MESSAGE.
		dfhcommarea.setXzc020FatalErrorMessage(ws.getErrorAndAdviceMessages().getEa20CicsLinkError().getEa20CicsLinkErrorFormatted());
		// COB_CODE: INITIALIZE XZC020-SERVICE-OUTPUTS.
		initXzc020ServiceOutputs();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	public void initXzc020ServiceOutputs() {
		dfhcommarea.getoPolKeys().setNbr("");
		dfhcommarea.getoPolKeys().setEffDt("");
		dfhcommarea.getoPolKeys().setExpDt("");
		dfhcommarea.setoUwTmnFlg(Types.SPACE_CHAR);
		dfhcommarea.setoTmnEmpId("");
		dfhcommarea.setoTmnEmpNm("");
		dfhcommarea.setoTmnPrcDt("");
	}

	public void initXzc020ErrorHandlingParms() {
		dfhcommarea.getXzc020ErrorReturnCode().setErrorReturnCodeFormatted("0000");
		dfhcommarea.setXzc020FatalErrorMessage("");
		dfhcommarea.setXzc020NonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaXzc02090.XZC020_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.getXzc020NonLoggableErrors(idx0).setXzc020NonLogErrMsg("");
		}
		dfhcommarea.setXzc020WarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaXzc02090.XZC020_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.getXzc020Warnings(idx0).setXzc020WarnMsg("");
		}
	}

	public void initDclpolDtaExt() {
		ws.getDclpolDtaExt().setPolNbr("");
		ws.getDclpolDtaExt().setPolEffDt("");
		ws.getDclpolDtaExt().setPlnExpDt("");
		ws.getDclpolDtaExt().setActNbr("");
		ws.getDclpolDtaExt().setPolId("");
		ws.getDclpolDtaExt().setCmpCd("");
		ws.getDclpolDtaExt().setPolTypCd("");
		ws.getDclpolDtaExt().setStsPolTypCd("");
		ws.getDclpolDtaExt().setBndInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setMolCrPolInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setAudInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setLgePrdInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setSplBilPolInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setSirAmt(0);
		ws.getDclpolDtaExt().setPriRskStAbb("");
		ws.getDclpolDtaExt().setPriRskStCd("");
		ws.getDclpolDtaExt().setPriRskCtyCd("");
		ws.getDclpolDtaExt().setPriRskTwnCd("");
		ws.getDclpolDtaExt().setRewPolNbr("");
		ws.getDclpolDtaExt().setRnlQteInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setWhyCncCd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setTmnExpCncDt("");
		ws.getDclpolDtaExt().setPolPerEnTrsCd("");
		ws.getDclpolDtaExt().setAcyPolInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setWrtPrmAmt(0);
		ws.getDclpolDtaExt().setFstTrmInd(Types.SPACE_CHAR);
		ws.getDclpolDtaExt().setSrcSysCd("");
	}

	public void initDclactNotPol() {
		ws.getDclactNotPol().setCsrActNbr("");
		ws.getDclactNotPol().setNotPrcTs("");
		ws.getDclactNotPol().setPolNbr("");
		ws.getDclactNotPol().setPolTypCd("");
		ws.getDclactNotPol().setPolPriRskStAbb("");
		ws.getDclactNotPol().setNotEffDt("");
		ws.getDclactNotPol().setPolEffDt("");
		ws.getDclactNotPol().setPolExpDt("");
		ws.getDclactNotPol().setPolDueAmt(new AfDecimal(0, 10, 2));
		ws.getDclactNotPol().setNinCltId("");
		ws.getDclactNotPol().setNinAdrId("");
		ws.getDclactNotPol().setWfStartedInd(Types.SPACE_CHAR);
		ws.getDclactNotPol().setPolBilStaCd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
