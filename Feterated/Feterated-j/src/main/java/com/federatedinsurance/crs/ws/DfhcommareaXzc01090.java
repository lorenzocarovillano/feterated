/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.Xzc010ServiceOutputs;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.occurs.Xzc010NonLoggableErrors;
import com.federatedinsurance.crs.ws.occurs.Xzc010Warnings;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program XZC01090<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaXzc01090 extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int XZC010_NON_LOGGABLE_ERRORS_MAXOCCURS = 10;
	public static final int XZC010_WARNINGS_MAXOCCURS = 10;
	//Original name: XZC01I-POL-NBR
	private String iPolNbr = DefaultValues.stringVal(Len.I_POL_NBR);
	//Original name: XZC01I-POL-EFF-DT
	private String iPolEffDt = DefaultValues.stringVal(Len.I_POL_EFF_DT);
	//Original name: XZC01I-POL-EXP-DT
	private String iPolExpDt = DefaultValues.stringVal(Len.I_POL_EXP_DT);
	//Original name: XZC01I-USERID
	private String iUserid = DefaultValues.stringVal(Len.I_USERID);
	//Original name: FILLER-XZC010-SERVICE-INPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: XZC010-SERVICE-OUTPUTS
	private Xzc010ServiceOutputs xzc010ServiceOutputs = new Xzc010ServiceOutputs();
	//Original name: XZC010-ERROR-RETURN-CODE
	private DsdErrorReturnCode xzc010ErrorReturnCode = new DsdErrorReturnCode();
	//Original name: XZC010-FATAL-ERROR-MESSAGE
	private String xzc010FatalErrorMessage = DefaultValues.stringVal(Len.XZC010_FATAL_ERROR_MESSAGE);
	//Original name: XZC010-NON-LOGGABLE-ERROR-CNT
	private String xzc010NonLoggableErrorCnt = DefaultValues.stringVal(Len.XZC010_NON_LOGGABLE_ERROR_CNT);
	//Original name: XZC010-NON-LOGGABLE-ERRORS
	private Xzc010NonLoggableErrors[] xzc010NonLoggableErrors = new Xzc010NonLoggableErrors[XZC010_NON_LOGGABLE_ERRORS_MAXOCCURS];
	//Original name: XZC010-WARNING-CNT
	private String xzc010WarningCnt = DefaultValues.stringVal(Len.XZC010_WARNING_CNT);
	//Original name: XZC010-WARNINGS
	private Xzc010Warnings[] xzc010Warnings = new Xzc010Warnings[XZC010_WARNINGS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public DfhcommareaXzc01090() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void init() {
		for (int xzc010NonLoggableErrorsIdx = 1; xzc010NonLoggableErrorsIdx <= XZC010_NON_LOGGABLE_ERRORS_MAXOCCURS; xzc010NonLoggableErrorsIdx++) {
			xzc010NonLoggableErrors[xzc010NonLoggableErrorsIdx - 1] = new Xzc010NonLoggableErrors();
		}
		for (int xzc010WarningsIdx = 1; xzc010WarningsIdx <= XZC010_WARNINGS_MAXOCCURS; xzc010WarningsIdx++) {
			xzc010Warnings[xzc010WarningsIdx - 1] = new Xzc010Warnings();
		}
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		setXzc010ServiceInputsBytes(buffer, position);
		position += Len.XZC010_SERVICE_INPUTS;
		xzc010ServiceOutputs.setXzc010ServiceOutputsBytes(buffer, position);
		position += Xzc010ServiceOutputs.Len.XZC010_SERVICE_OUTPUTS;
		setXzc010ErrorHandlingParmsBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		getXzc010ServiceInputsBytes(buffer, position);
		position += Len.XZC010_SERVICE_INPUTS;
		xzc010ServiceOutputs.getXzc010ServiceOutputsBytes(buffer, position);
		position += Xzc010ServiceOutputs.Len.XZC010_SERVICE_OUTPUTS;
		getXzc010ErrorHandlingParmsBytes(buffer, position);
		return buffer;
	}

	public void setXzc010ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		iPolNbr = MarshalByte.readString(buffer, position, Len.I_POL_NBR);
		position += Len.I_POL_NBR;
		iPolEffDt = MarshalByte.readString(buffer, position, Len.I_POL_EFF_DT);
		position += Len.I_POL_EFF_DT;
		iPolExpDt = MarshalByte.readString(buffer, position, Len.I_POL_EXP_DT);
		position += Len.I_POL_EXP_DT;
		iUserid = MarshalByte.readString(buffer, position, Len.I_USERID);
		position += Len.I_USERID;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzc010ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, iPolNbr, Len.I_POL_NBR);
		position += Len.I_POL_NBR;
		MarshalByte.writeString(buffer, position, iPolEffDt, Len.I_POL_EFF_DT);
		position += Len.I_POL_EFF_DT;
		MarshalByte.writeString(buffer, position, iPolExpDt, Len.I_POL_EXP_DT);
		position += Len.I_POL_EXP_DT;
		MarshalByte.writeString(buffer, position, iUserid, Len.I_USERID);
		position += Len.I_USERID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setiPolNbr(String iPolNbr) {
		this.iPolNbr = Functions.subString(iPolNbr, Len.I_POL_NBR);
	}

	public String getiPolNbr() {
		return this.iPolNbr;
	}

	public void setiPolEffDt(String iPolEffDt) {
		this.iPolEffDt = Functions.subString(iPolEffDt, Len.I_POL_EFF_DT);
	}

	public String getiPolEffDt() {
		return this.iPolEffDt;
	}

	public void setiPolExpDt(String iPolExpDt) {
		this.iPolExpDt = Functions.subString(iPolExpDt, Len.I_POL_EXP_DT);
	}

	public String getiPolExpDt() {
		return this.iPolExpDt;
	}

	public void setiUserid(String iUserid) {
		this.iUserid = Functions.subString(iUserid, Len.I_USERID);
	}

	public String getiUserid() {
		return this.iUserid;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	/**Original name: XZC010-ERROR-HANDLING-PARMS<br>
	 * <pre>* ERROR HANDLING</pre>*/
	public byte[] getXzc010ErrorHandlingParmsBytes() {
		byte[] buffer = new byte[Len.XZC010_ERROR_HANDLING_PARMS];
		return getXzc010ErrorHandlingParmsBytes(buffer, 1);
	}

	public void setXzc010ErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc010ErrorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		xzc010FatalErrorMessage = MarshalByte.readString(buffer, position, Len.XZC010_FATAL_ERROR_MESSAGE);
		position += Len.XZC010_FATAL_ERROR_MESSAGE;
		xzc010NonLoggableErrorCnt = MarshalByte.readFixedString(buffer, position, Len.XZC010_NON_LOGGABLE_ERROR_CNT);
		position += Len.XZC010_NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= XZC010_NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				xzc010NonLoggableErrors[idx - 1].setXzc010NonLoggableErrorsBytes(buffer, position);
				position += Xzc010NonLoggableErrors.Len.XZC010_NON_LOGGABLE_ERRORS;
			} else {
				xzc010NonLoggableErrors[idx - 1].initXzc010NonLoggableErrorsSpaces();
				position += Xzc010NonLoggableErrors.Len.XZC010_NON_LOGGABLE_ERRORS;
			}
		}
		xzc010WarningCnt = MarshalByte.readFixedString(buffer, position, Len.XZC010_WARNING_CNT);
		position += Len.XZC010_WARNING_CNT;
		for (int idx = 1; idx <= XZC010_WARNINGS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				xzc010Warnings[idx - 1].setXzc010WarningsBytes(buffer, position);
				position += Xzc010Warnings.Len.XZC010_WARNINGS;
			} else {
				xzc010Warnings[idx - 1].initXzc010WarningsSpaces();
				position += Xzc010Warnings.Len.XZC010_WARNINGS;
			}
		}
	}

	public byte[] getXzc010ErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc010ErrorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, xzc010FatalErrorMessage, Len.XZC010_FATAL_ERROR_MESSAGE);
		position += Len.XZC010_FATAL_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, xzc010NonLoggableErrorCnt, Len.XZC010_NON_LOGGABLE_ERROR_CNT);
		position += Len.XZC010_NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= XZC010_NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			xzc010NonLoggableErrors[idx - 1].getXzc010NonLoggableErrorsBytes(buffer, position);
			position += Xzc010NonLoggableErrors.Len.XZC010_NON_LOGGABLE_ERRORS;
		}
		MarshalByte.writeString(buffer, position, xzc010WarningCnt, Len.XZC010_WARNING_CNT);
		position += Len.XZC010_WARNING_CNT;
		for (int idx = 1; idx <= XZC010_WARNINGS_MAXOCCURS; idx++) {
			xzc010Warnings[idx - 1].getXzc010WarningsBytes(buffer, position);
			position += Xzc010Warnings.Len.XZC010_WARNINGS;
		}
		return buffer;
	}

	public void setXzc010FatalErrorMessage(String xzc010FatalErrorMessage) {
		this.xzc010FatalErrorMessage = Functions.subString(xzc010FatalErrorMessage, Len.XZC010_FATAL_ERROR_MESSAGE);
	}

	public String getXzc010FatalErrorMessage() {
		return this.xzc010FatalErrorMessage;
	}

	public void setXzc010NonLoggableErrorCntFormatted(String xzc010NonLoggableErrorCnt) {
		this.xzc010NonLoggableErrorCnt = Trunc.toUnsignedNumeric(xzc010NonLoggableErrorCnt, Len.XZC010_NON_LOGGABLE_ERROR_CNT);
	}

	public short getXzc010NonLoggableErrorCnt() {
		return NumericDisplay.asShort(this.xzc010NonLoggableErrorCnt);
	}

	public void setXzc010WarningCntFormatted(String xzc010WarningCnt) {
		this.xzc010WarningCnt = Trunc.toUnsignedNumeric(xzc010WarningCnt, Len.XZC010_WARNING_CNT);
	}

	public short getXzc010WarningCnt() {
		return NumericDisplay.asShort(this.xzc010WarningCnt);
	}

	public DsdErrorReturnCode getXzc010ErrorReturnCode() {
		return xzc010ErrorReturnCode;
	}

	public Xzc010NonLoggableErrors getXzc010NonLoggableErrors(int idx) {
		return xzc010NonLoggableErrors[idx - 1];
	}

	public Xzc010ServiceOutputs getXzc010ServiceOutputs() {
		return xzc010ServiceOutputs;
	}

	public Xzc010Warnings getXzc010Warnings(int idx) {
		return xzc010Warnings[idx - 1];
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int I_POL_NBR = 9;
		public static final int I_POL_EFF_DT = 10;
		public static final int I_POL_EXP_DT = 10;
		public static final int I_USERID = 8;
		public static final int FLR1 = 100;
		public static final int XZC010_SERVICE_INPUTS = I_POL_NBR + I_POL_EFF_DT + I_POL_EXP_DT + I_USERID + FLR1;
		public static final int XZC010_FATAL_ERROR_MESSAGE = 250;
		public static final int XZC010_NON_LOGGABLE_ERROR_CNT = 4;
		public static final int XZC010_WARNING_CNT = 4;
		public static final int XZC010_ERROR_HANDLING_PARMS = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + XZC010_FATAL_ERROR_MESSAGE
				+ XZC010_NON_LOGGABLE_ERROR_CNT
				+ DfhcommareaXzc01090.XZC010_NON_LOGGABLE_ERRORS_MAXOCCURS * Xzc010NonLoggableErrors.Len.XZC010_NON_LOGGABLE_ERRORS
				+ XZC010_WARNING_CNT + DfhcommareaXzc01090.XZC010_WARNINGS_MAXOCCURS * Xzc010Warnings.Len.XZC010_WARNINGS;
		public static final int DFHCOMMAREA = XZC010_SERVICE_INPUTS + Xzc010ServiceOutputs.Len.XZC010_SERVICE_OUTPUTS + XZC010_ERROR_HANDLING_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
