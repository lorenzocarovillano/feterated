/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.federatedinsurance.crs.ws.redefines.AfAddressLines;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS529099<br>
 * Generated as a class for rule WS.<br>*/
public class Ts529099Data {

	//==== PROPERTIES ====
	//Original name: ADDRESS-COMPONENTS
	private AddressComponents addressComponents = new AddressComponents();
	//Original name: AF-ADDRESS-LINES
	private AfAddressLines afAddressLines = new AfAddressLines();
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsTs529099 constantFields = new ConstantFieldsTs529099();
	//Original name: SAVE-AREA
	private SaveAreaTs529099 saveArea = new SaveAreaTs529099();
	//Original name: SS-TA
	private short ssTa = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-TA-LINE-POS-CURRENT
	private short ssTaLinePosCurrent = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-TA-LINE-POS-NEXT
	private short ssTaLinePosNext = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-DN-START
	private short ssDnStart = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-DN-LINE-BREAK
	private short ssDnLineBreak = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setSsTa(short ssTa) {
		this.ssTa = ssTa;
	}

	public short getSsTa() {
		return this.ssTa;
	}

	public void setSsTaLinePosCurrent(short ssTaLinePosCurrent) {
		this.ssTaLinePosCurrent = ssTaLinePosCurrent;
	}

	public short getSsTaLinePosCurrent() {
		return this.ssTaLinePosCurrent;
	}

	public void setSsTaLinePosNext(short ssTaLinePosNext) {
		this.ssTaLinePosNext = ssTaLinePosNext;
	}

	public short getSsTaLinePosNext() {
		return this.ssTaLinePosNext;
	}

	public void setSsDnStart(short ssDnStart) {
		this.ssDnStart = ssDnStart;
	}

	public short getSsDnStart() {
		return this.ssDnStart;
	}

	public void setSsDnLineBreak(short ssDnLineBreak) {
		this.ssDnLineBreak = ssDnLineBreak;
	}

	public short getSsDnLineBreak() {
		return this.ssDnLineBreak;
	}

	public AddressComponents getAddressComponents() {
		return addressComponents;
	}

	public AfAddressLines getAfAddressLines() {
		return afAddressLines;
	}

	public ConstantFieldsTs529099 getConstantFields() {
		return constantFields;
	}

	public SaveAreaTs529099 getSaveArea() {
		return saveArea;
	}
}
