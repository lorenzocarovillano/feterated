/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9072<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9072 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT972O_ACY_TAG_ROW_MAXOCCURS = 2500;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9072() {
	}

	public LServiceContractAreaXz0x9072(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt972iUserid(String xzt972iUserid) {
		writeString(Pos.XZT972I_USERID, xzt972iUserid, Len.XZT972I_USERID);
	}

	/**Original name: XZT972I-USERID<br>*/
	public String getXzt972iUserid() {
		return readString(Pos.XZT972I_USERID, Len.XZT972I_USERID);
	}

	public String getXzt972iUseridFormatted() {
		return Functions.padBlanks(getXzt972iUserid(), Len.XZT972I_USERID);
	}

	public void setXzt972oAcyTagRowBytes(int xzt972oAcyTagRowIdx, byte[] buffer) {
		setXzt972oAcyTagRowBytes(xzt972oAcyTagRowIdx, buffer, 1);
	}

	public void setXzt972oAcyTagRowBytes(int xzt972oAcyTagRowIdx, byte[] buffer, int offset) {
		int position = Pos.xzt972oAcyTagRow(xzt972oAcyTagRowIdx - 1);
		setBytes(buffer, offset, Len.XZT972O_ACY_TAG_ROW, position);
	}

	public void setXzt972oEdlFrmNm(int xzt972oEdlFrmNmIdx, String xzt972oEdlFrmNm) {
		int position = Pos.xzt972oEdlFrmNm(xzt972oEdlFrmNmIdx - 1);
		writeString(position, xzt972oEdlFrmNm, Len.XZT972O_EDL_FRM_NM);
	}

	/**Original name: XZT972O-EDL-FRM-NM<br>*/
	public String getXzt972oEdlFrmNm(int xzt972oEdlFrmNmIdx) {
		int position = Pos.xzt972oEdlFrmNm(xzt972oEdlFrmNmIdx - 1);
		return readString(position, Len.XZT972O_EDL_FRM_NM);
	}

	public void setXzt972oDtaGrpNm(int xzt972oDtaGrpNmIdx, String xzt972oDtaGrpNm) {
		int position = Pos.xzt972oDtaGrpNm(xzt972oDtaGrpNmIdx - 1);
		writeString(position, xzt972oDtaGrpNm, Len.XZT972O_DTA_GRP_NM);
	}

	/**Original name: XZT972O-DTA-GRP-NM<br>*/
	public String getXzt972oDtaGrpNm(int xzt972oDtaGrpNmIdx) {
		int position = Pos.xzt972oDtaGrpNm(xzt972oDtaGrpNmIdx - 1);
		return readString(position, Len.XZT972O_DTA_GRP_NM);
	}

	public void setXzt972oDtaGrpFldNbr(int xzt972oDtaGrpFldNbrIdx, int xzt972oDtaGrpFldNbr) {
		int position = Pos.xzt972oDtaGrpFldNbr(xzt972oDtaGrpFldNbrIdx - 1);
		writeInt(position, xzt972oDtaGrpFldNbr, Len.Int.XZT972O_DTA_GRP_FLD_NBR);
	}

	/**Original name: XZT972O-DTA-GRP-FLD-NBR<br>*/
	public int getXzt972oDtaGrpFldNbr(int xzt972oDtaGrpFldNbrIdx) {
		int position = Pos.xzt972oDtaGrpFldNbr(xzt972oDtaGrpFldNbrIdx - 1);
		return readNumDispInt(position, Len.XZT972O_DTA_GRP_FLD_NBR);
	}

	public void setXzt972oTagNm(int xzt972oTagNmIdx, String xzt972oTagNm) {
		int position = Pos.xzt972oTagNm(xzt972oTagNmIdx - 1);
		writeString(position, xzt972oTagNm, Len.XZT972O_TAG_NM);
	}

	/**Original name: XZT972O-TAG-NM<br>*/
	public String getXzt972oTagNm(int xzt972oTagNmIdx) {
		int position = Pos.xzt972oTagNm(xzt972oTagNmIdx - 1);
		return readString(position, Len.XZT972O_TAG_NM);
	}

	public void setXzt972oJusCd(int xzt972oJusCdIdx, char xzt972oJusCd) {
		int position = Pos.xzt972oJusCd(xzt972oJusCdIdx - 1);
		writeChar(position, xzt972oJusCd);
	}

	/**Original name: XZT972O-JUS-CD<br>*/
	public char getXzt972oJusCd(int xzt972oJusCdIdx) {
		int position = Pos.xzt972oJusCd(xzt972oJusCdIdx - 1);
		return readChar(position);
	}

	public void setXzt972oSpePrcCd(int xzt972oSpePrcCdIdx, String xzt972oSpePrcCd) {
		int position = Pos.xzt972oSpePrcCd(xzt972oSpePrcCdIdx - 1);
		writeString(position, xzt972oSpePrcCd, Len.XZT972O_SPE_PRC_CD);
	}

	/**Original name: XZT972O-SPE-PRC-CD<br>*/
	public String getXzt972oSpePrcCd(int xzt972oSpePrcCdIdx) {
		int position = Pos.xzt972oSpePrcCd(xzt972oSpePrcCdIdx - 1);
		return readString(position, Len.XZT972O_SPE_PRC_CD);
	}

	public void setXzt972oLenNbr(int xzt972oLenNbrIdx, int xzt972oLenNbr) {
		int position = Pos.xzt972oLenNbr(xzt972oLenNbrIdx - 1);
		writeInt(position, xzt972oLenNbr, Len.Int.XZT972O_LEN_NBR);
	}

	/**Original name: XZT972O-LEN-NBR<br>*/
	public int getXzt972oLenNbr(int xzt972oLenNbrIdx) {
		int position = Pos.xzt972oLenNbr(xzt972oLenNbrIdx - 1);
		return readNumDispInt(position, Len.XZT972O_LEN_NBR);
	}

	public void setXzt972oTagOccCnt(int xzt972oTagOccCntIdx, int xzt972oTagOccCnt) {
		int position = Pos.xzt972oTagOccCnt(xzt972oTagOccCntIdx - 1);
		writeInt(position, xzt972oTagOccCnt, Len.Int.XZT972O_TAG_OCC_CNT);
	}

	/**Original name: XZT972O-TAG-OCC-CNT<br>*/
	public int getXzt972oTagOccCnt(int xzt972oTagOccCntIdx) {
		int position = Pos.xzt972oTagOccCnt(xzt972oTagOccCntIdx - 1);
		return readNumDispInt(position, Len.XZT972O_TAG_OCC_CNT);
	}

	public void setXzt972oDelInd(int xzt972oDelIndIdx, char xzt972oDelInd) {
		int position = Pos.xzt972oDelInd(xzt972oDelIndIdx - 1);
		writeChar(position, xzt972oDelInd);
	}

	/**Original name: XZT972O-DEL-IND<br>*/
	public char getXzt972oDelInd(int xzt972oDelIndIdx) {
		int position = Pos.xzt972oDelInd(xzt972oDelIndIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT9072_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT972I_USERID = XZT9072_SERVICE_INPUTS;
		public static final int XZT9072_SERVICE_OUTPUTS = XZT972I_USERID + Len.XZT972I_USERID;
		public static final int XZT972O_TABLE_OF_ACY_TAG_ROWS = XZT9072_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt972oAcyTagRow(int idx) {
			return XZT972O_TABLE_OF_ACY_TAG_ROWS + idx * Len.XZT972O_ACY_TAG_ROW;
		}

		public static int xzt972oEdlFrmNm(int idx) {
			return xzt972oAcyTagRow(idx);
		}

		public static int xzt972oDtaGrpNm(int idx) {
			return xzt972oEdlFrmNm(idx) + Len.XZT972O_EDL_FRM_NM;
		}

		public static int xzt972oDtaGrpFldNbr(int idx) {
			return xzt972oDtaGrpNm(idx) + Len.XZT972O_DTA_GRP_NM;
		}

		public static int xzt972oTagNm(int idx) {
			return xzt972oDtaGrpFldNbr(idx) + Len.XZT972O_DTA_GRP_FLD_NBR;
		}

		public static int xzt972oJusCd(int idx) {
			return xzt972oTagNm(idx) + Len.XZT972O_TAG_NM;
		}

		public static int xzt972oSpePrcCd(int idx) {
			return xzt972oJusCd(idx) + Len.XZT972O_JUS_CD;
		}

		public static int xzt972oLenNbr(int idx) {
			return xzt972oSpePrcCd(idx) + Len.XZT972O_SPE_PRC_CD;
		}

		public static int xzt972oTagOccCnt(int idx) {
			return xzt972oLenNbr(idx) + Len.XZT972O_LEN_NBR;
		}

		public static int xzt972oDelInd(int idx) {
			return xzt972oTagOccCnt(idx) + Len.XZT972O_TAG_OCC_CNT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT972I_USERID = 8;
		public static final int XZT972O_EDL_FRM_NM = 30;
		public static final int XZT972O_DTA_GRP_NM = 8;
		public static final int XZT972O_DTA_GRP_FLD_NBR = 5;
		public static final int XZT972O_TAG_NM = 30;
		public static final int XZT972O_JUS_CD = 1;
		public static final int XZT972O_SPE_PRC_CD = 8;
		public static final int XZT972O_LEN_NBR = 5;
		public static final int XZT972O_TAG_OCC_CNT = 5;
		public static final int XZT972O_DEL_IND = 1;
		public static final int XZT972O_ACY_TAG_ROW = XZT972O_EDL_FRM_NM + XZT972O_DTA_GRP_NM + XZT972O_DTA_GRP_FLD_NBR + XZT972O_TAG_NM
				+ XZT972O_JUS_CD + XZT972O_SPE_PRC_CD + XZT972O_LEN_NBR + XZT972O_TAG_OCC_CNT + XZT972O_DEL_IND;
		public static final int XZT9072_SERVICE_INPUTS = XZT972I_USERID;
		public static final int XZT972O_TABLE_OF_ACY_TAG_ROWS = LServiceContractAreaXz0x9072.XZT972O_ACY_TAG_ROW_MAXOCCURS * XZT972O_ACY_TAG_ROW;
		public static final int XZT9072_SERVICE_OUTPUTS = XZT972O_TABLE_OF_ACY_TAG_ROWS;
		public static final int L_SERVICE_CONTRACT_AREA = XZT9072_SERVICE_INPUTS + XZT9072_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT972O_DTA_GRP_FLD_NBR = 5;
			public static final int XZT972O_LEN_NBR = 5;
			public static final int XZT972O_TAG_OCC_CNT = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
