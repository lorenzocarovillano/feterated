/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: CF-ACT-NOT-TYP-CD<br>
 * Variable: CF-ACT-NOT-TYP-CD from program XZ0P90L0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfActNotTypCdXz0p90l0 {

	//==== PROPERTIES ====
	//Original name: CF-ACT-NOT-IMPENDING
	private String impending = "IMP";
	//Original name: CF-ACT-NOT-RESCIND
	private String rescind = "RES";
	//Original name: CF-ACT-NOT-NONPAY
	private String nonpay = "NPC";

	//==== METHODS ====
	public void setImpending(String impending) {
		this.impending = Functions.subString(impending, Len.IMPENDING);
	}

	public String getImpending() {
		return this.impending;
	}

	public void setRescind(String rescind) {
		this.rescind = Functions.subString(rescind, Len.RESCIND);
	}

	public String getRescind() {
		return this.rescind;
	}

	public void setNonpay(String nonpay) {
		this.nonpay = Functions.subString(nonpay, Len.NONPAY);
	}

	public String getNonpay() {
		return this.nonpay;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int IMPENDING = 5;
		public static final int RESCIND = 5;
		public static final int NONPAY = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
