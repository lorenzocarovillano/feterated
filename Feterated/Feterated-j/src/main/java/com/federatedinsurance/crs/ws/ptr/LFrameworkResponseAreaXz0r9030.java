/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9030<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9030 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9030_MAXOCCURS = 25;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9030() {
	}

	public LFrameworkResponseAreaXz0r9030(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a9030Formatted(int lFwRespXz0a9030Idx) {
		int position = Pos.lFwRespXz0a9030(lFwRespXz0a9030Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9030);
	}

	public void setlFwRespXz0a9030Bytes(int lFwRespXz0a9030Idx, byte[] buffer) {
		setlFwRespXz0a9030Bytes(lFwRespXz0a9030Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A9030<br>*/
	public byte[] getlFwRespXz0a9030Bytes(int lFwRespXz0a9030Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_XZ0A9030];
		return getlFwRespXz0a9030Bytes(lFwRespXz0a9030Idx, buffer, 1);
	}

	public void setlFwRespXz0a9030Bytes(int lFwRespXz0a9030Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9030(lFwRespXz0a9030Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A9030, position);
	}

	public byte[] getlFwRespXz0a9030Bytes(int lFwRespXz0a9030Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9030(lFwRespXz0a9030Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A9030, position);
		return buffer;
	}

	public void setXza930rMaxActNotTypRows(int xza930rMaxActNotTypRowsIdx, short xza930rMaxActNotTypRows) {
		int position = Pos.xza930MaxActNotTypRows(xza930rMaxActNotTypRowsIdx - 1);
		writeBinaryShort(position, xza930rMaxActNotTypRows);
	}

	/**Original name: XZA930R-MAX-ACT-NOT-TYP-ROWS<br>*/
	public short getXza930rMaxActNotTypRows(int xza930rMaxActNotTypRowsIdx) {
		int position = Pos.xza930MaxActNotTypRows(xza930rMaxActNotTypRowsIdx - 1);
		return readBinaryShort(position);
	}

	public void setXza930rActNotTypCd(int xza930rActNotTypCdIdx, String xza930rActNotTypCd) {
		int position = Pos.xza930ActNotTypCd(xza930rActNotTypCdIdx - 1);
		writeString(position, xza930rActNotTypCd, Len.XZA930_ACT_NOT_TYP_CD);
	}

	/**Original name: XZA930R-ACT-NOT-TYP-CD<br>*/
	public String getXza930rActNotTypCd(int xza930rActNotTypCdIdx) {
		int position = Pos.xza930ActNotTypCd(xza930rActNotTypCdIdx - 1);
		return readString(position, Len.XZA930_ACT_NOT_TYP_CD);
	}

	public void setXza930rActNotDes(int xza930rActNotDesIdx, String xza930rActNotDes) {
		int position = Pos.xza930ActNotDes(xza930rActNotDesIdx - 1);
		writeString(position, xza930rActNotDes, Len.XZA930_ACT_NOT_DES);
	}

	/**Original name: XZA930R-ACT-NOT-DES<br>*/
	public String getXza930rActNotDes(int xza930rActNotDesIdx) {
		int position = Pos.xza930ActNotDes(xza930rActNotDesIdx - 1);
		return readString(position, Len.XZA930_ACT_NOT_DES);
	}

	public void setXza930rDsyOrdNbr(int xza930rDsyOrdNbrIdx, int xza930rDsyOrdNbr) {
		int position = Pos.xza930DsyOrdNbr(xza930rDsyOrdNbrIdx - 1);
		writeInt(position, xza930rDsyOrdNbr, Len.Int.XZA930R_DSY_ORD_NBR);
	}

	/**Original name: XZA930R-DSY-ORD-NBR<br>*/
	public int getXza930rDsyOrdNbr(int xza930rDsyOrdNbrIdx) {
		int position = Pos.xza930DsyOrdNbr(xza930rDsyOrdNbrIdx - 1);
		return readNumDispInt(position, Len.XZA930_DSY_ORD_NBR);
	}

	public void setXza930rDocDes(int xza930rDocDesIdx, String xza930rDocDes) {
		int position = Pos.xza930DocDes(xza930rDocDesIdx - 1);
		writeString(position, xza930rDocDes, Len.XZA930_DOC_DES);
	}

	/**Original name: XZA930R-DOC-DES<br>*/
	public String getXza930rDocDes(int xza930rDocDesIdx) {
		int position = Pos.xza930DocDes(xza930rDocDesIdx - 1);
		return readString(position, Len.XZA930_DOC_DES);
	}

	public void setXza930rActNotPthCd(int xza930rActNotPthCdIdx, String xza930rActNotPthCd) {
		int position = Pos.xza930ActNotPthCd(xza930rActNotPthCdIdx - 1);
		writeString(position, xza930rActNotPthCd, Len.XZA930_ACT_NOT_PTH_CD);
	}

	/**Original name: XZA930R-ACT-NOT-PTH-CD<br>*/
	public String getXza930rActNotPthCd(int xza930rActNotPthCdIdx) {
		int position = Pos.xza930ActNotPthCd(xza930rActNotPthCdIdx - 1);
		return readString(position, Len.XZA930_ACT_NOT_PTH_CD);
	}

	public void setXza930rActNotPthDes(int xza930rActNotPthDesIdx, String xza930rActNotPthDes) {
		int position = Pos.xza930ActNotPthDes(xza930rActNotPthDesIdx - 1);
		writeString(position, xza930rActNotPthDes, Len.XZA930_ACT_NOT_PTH_DES);
	}

	/**Original name: XZA930R-ACT-NOT-PTH-DES<br>*/
	public String getXza930rActNotPthDes(int xza930rActNotPthDesIdx) {
		int position = Pos.xza930ActNotPthDes(xza930rActNotPthDesIdx - 1);
		return readString(position, Len.XZA930_ACT_NOT_PTH_DES);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9030(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9030;
		}

		public static int xza930ActNotTypInfo(int idx) {
			return lFwRespXz0a9030(idx);
		}

		public static int xza930MaxActNotTypRows(int idx) {
			return xza930ActNotTypInfo(idx);
		}

		public static int xza930ActNotTypRow(int idx) {
			return xza930MaxActNotTypRows(idx) + Len.XZA930_MAX_ACT_NOT_TYP_ROWS;
		}

		public static int xza930ActNotTypCd(int idx) {
			return xza930ActNotTypRow(idx);
		}

		public static int xza930ActNotDes(int idx) {
			return xza930ActNotTypCd(idx) + Len.XZA930_ACT_NOT_TYP_CD;
		}

		public static int xza930DsyOrdNbr(int idx) {
			return xza930ActNotDes(idx) + Len.XZA930_ACT_NOT_DES;
		}

		public static int xza930DocDes(int idx) {
			return xza930DsyOrdNbr(idx) + Len.XZA930_DSY_ORD_NBR;
		}

		public static int xza930ActNotPthCd(int idx) {
			return xza930DocDes(idx) + Len.XZA930_DOC_DES;
		}

		public static int xza930ActNotPthDes(int idx) {
			return xza930ActNotPthCd(idx) + Len.XZA930_ACT_NOT_PTH_CD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA930_MAX_ACT_NOT_TYP_ROWS = 2;
		public static final int XZA930_ACT_NOT_TYP_CD = 5;
		public static final int XZA930_ACT_NOT_DES = 35;
		public static final int XZA930_DSY_ORD_NBR = 5;
		public static final int XZA930_DOC_DES = 240;
		public static final int XZA930_ACT_NOT_PTH_CD = 5;
		public static final int XZA930_ACT_NOT_PTH_DES = 35;
		public static final int XZA930_ACT_NOT_TYP_ROW = XZA930_ACT_NOT_TYP_CD + XZA930_ACT_NOT_DES + XZA930_DSY_ORD_NBR + XZA930_DOC_DES
				+ XZA930_ACT_NOT_PTH_CD + XZA930_ACT_NOT_PTH_DES;
		public static final int XZA930_ACT_NOT_TYP_INFO = XZA930_MAX_ACT_NOT_TYP_ROWS + XZA930_ACT_NOT_TYP_ROW;
		public static final int L_FW_RESP_XZ0A9030 = XZA930_ACT_NOT_TYP_INFO;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0r9030.L_FW_RESP_XZ0A9030_MAXOCCURS * L_FW_RESP_XZ0A9030;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA930R_DSY_ORD_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
