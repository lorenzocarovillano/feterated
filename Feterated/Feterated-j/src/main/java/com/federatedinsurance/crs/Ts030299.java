/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.DbService;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.config.ConfigSettings;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.dao.FedRmtPrtTabDao;
import com.federatedinsurance.crs.copy.SqlcaTs030199;
import com.federatedinsurance.crs.ws.DsnrliParms;
import com.federatedinsurance.crs.ws.LReportDbParameters;
import com.federatedinsurance.crs.ws.Ts030299Data;
import com.federatedinsurance.crs.ws.enums.LRdActionIndicator;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: TS030299<br>
 * <pre>AUTHOR.         ROB LIVENGOOD.
 * DATE-WRITTEN.   NOVEMBER  2007.
 *        TITLE - REPORT INFORMATION RETRIEVAL SUBPROGRAM
 * **************************************************************
 *  NOTE: SINCE THIS IS A DB2 PROGRAM, IT MUST BE COMPILED      *
 *        THRU ENDEVOR WHEN TESTING.  THAT MEANS THE MODULE     *
 *        BECOMES AVAILABLE FOR ANYONE ELSE TESTING A PROGRAM   *
 *        WHOSE LOAD MODULE IS PUT INTO AN ENDEVOR TESTLIB.  IT *
 *        MAY BE NECESSARY TO CHANGE THE NAME OF THIS MODULE    *
 *        UNTIL TESTING IS COMPLETE.                            *
 *                                                              *
 * **************************************************************
 *        INPUT - ACTION INDICATOR
 *              - REPORT NUMBER
 *       OUTPUT - STATUS CODE
 *              - TABLE DATA
 *              - ERROR MESSAGES
 *      PROCESS - THIS PROGRAM IS BEING CALLED BECAUSE IT HAS BEEN
 *                DETERMINED IN TS030199 THAT DB2 IS NOT ACTIVE.
 *                WE WILL USE A TECHNIQUE CALLED RRSAF - RECOVERABLE
 *                RESOURCE SERVICES ATTACHMENT FACILITY.  THIS GIVES
 *                US THE ABILITY TO BURY THE CONNECTION PARAMETERS
 *                IN THE PROGRAM AND NOT NEED ANY JCL TO ACCESS DB2.
 *              - IF THE ACTION INDICATOR IS 'CONNECT', WE WILL TRY
 *                TO CONNECT TO PROD DB2.  IF WE ARE UNABLE TO DO
 *                THAT, WE'LL CONNECT TO DEV DB2.
 *              - IF THE ACTION INDICATOR IS 'RETRIEVE', WE WILL
 *                READ THE REPORT INFORMATION FROM FED_RMT_PRT_TAB.
 *              - IF THE ACTION INDICATOR IS 'DISCONNECT', WE WILL
 *                END THE CONNECTION TO DB2.
 *              - RETURN APPROPRIATE STATUS CODE, TABLE INFORMATION
 *                (IF NEEDED), AND ERROR MESSAGES (IF NEEDED)
 *        NOTES - THIS PROGRAM IS USED IF TS030199 HAS DETERMINED
 *                THAT THERE IS NO CONNECTION TO DB2.
 *              - CHANGES TO THIS PROGRAM MAY ALSO CAUSE CHANGES TO
 *                TS030199.
 *     MAINTENANCE HISTORY
 *     *******************
 *      INFO   CHANGE
 *       NBR    DATE     III   DESCRIPTION
 *     *****  ********   ***   *************************************
 *    TO07600 11/29/07   RKL   NEW PROGRAM
 *    TO07600 11/30/07   DEL   STANDARDIZING PROGRAM
 *   TL000197 04/30/10   DMA   RETUN ONLY THE 'ACTIVE' RECORD.</pre>*/
public class Ts030299 extends Program {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private SqlcaTs030199 sqlca = new SqlcaTs030199();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private FedRmtPrtTabDao fedRmtPrtTabDao = new FedRmtPrtTabDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Ts030299Data ws = new Ts030299Data();
	//Original name: WHEN-COMPILED
	private static final String WHEN_COMPILED = ConfigSettings.whenCompiled(true);
	//Original name: L-REPORT-DB-PARAMETERS
	private LReportDbParameters lReportDbParameters;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LReportDbParameters lReportDbParameters) {
		this.lReportDbParameters = lReportDbParameters;
		retrieveReportData();
		programExit();
		return 0;
	}

	public static Ts030299 getInstance() {
		return (Programs.getInstance(Ts030299.class));
	}

	/**Original name: 1000-RETRIEVE-REPORT-DATA<br>*/
	private void retrieveReportData() {
		// COB_CODE: SET L-RD-STATUS-SUCCESSFUL  TO TRUE.
		lReportDbParameters.getStatus().setSuccessful();
		// COB_CODE: MOVE SPACES                 TO L-RD-TABLE-DATA
		//                                          L-RD-ERROR-MESSAGES.
		lReportDbParameters.setTableData("");
		lReportDbParameters.initErrorMessagesSpaces();
		// COB_CODE: IF SW-FIRST-TIME
		//                  THRU 2000-EXIT
		//           END-IF.
		if (ws.isSwFirstTimeFlag()) {
			// COB_CODE: PERFORM 2000-BEGINNING-HSKP
			//              THRU 2000-EXIT
			beginningHskp();
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN L-RD-AI-CONNECT
		//                      THRU 3000-EXIT
		//               WHEN L-RD-AI-RETRIEVE
		//                      THRU 4000-EXIT
		//               WHEN L-RD-AI-DISCONNECT
		//                      THRU 5000-EXIT
		//           END-EVALUATE.
		switch (lReportDbParameters.getActionIndicator().getActionIndicator()) {

		case LRdActionIndicator.CONNECT:// COB_CODE: PERFORM 3000-RRS-DB2-CONNECT
			//              THRU 3000-EXIT
			rrsDb2Connect();
			break;

		case LRdActionIndicator.RETRIEVE:// COB_CODE: PERFORM 4000-RETRIEVE-REPORT-DATA
			//              THRU 4000-EXIT
			retrieveReportData1();
			break;

		case LRdActionIndicator.DISCONNECT:// COB_CODE: PERFORM 5000-RRS-DB2-DISCONNECT
			//              THRU 5000-EXIT
			rrsDb2Disconnect();
			break;

		default:
			break;
		}
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 2000-BEGINNING-HSKP<br>*/
	private void beginningHskp() {
		// COB_CODE: SET SW-NOT-FIRST-TIME       TO TRUE.
		ws.setSwFirstTimeFlag(false);
		// COB_CODE: MOVE WHEN-COMPILED          TO EA-00-DATE
		//                                          EA-00-TIME.
		ws.getErrorAndAdviceMessages().getEa00ProgramMsg().setDateFld(WHEN_COMPILED);
		ws.getErrorAndAdviceMessages().getEa00ProgramMsg().setTimeFldFormatted(getWhenCompiledFormatted());
	}

	/**Original name: 3000-RRS-DB2-CONNECT<br>
	 * <pre>    THIS PROGRAM IS BEING CALLED WITH A 'CONNECT' ACTION BECAUSE
	 *     THE ROOT PROGRAM HAS NOT ESTABLISHED A CONNECTION TO THE
	 *     DATABASE.  TO CONNECT, WE ARE USING RRSAF SO WE DO NOT HAVE
	 *     TO HAVE PGM=IKJFT01 IN OUR JCL.
	 *     TRY TO CONNECT TO THE PRODUCTION SYSTEM FIRST.</pre>*/
	private void rrsDb2Connect() {
		StringParam dpRibptr = null;
		StringParam dpEibptr = null;
		StringParam dpTecb = null;
		StringParam dpSecb = null;
		StringParam dpReturn = null;
		StringParam dpReason = null;
		StringParam dpGroup = null;
		StringParam dpCorr = null;
		StringParam dpAcctt = null;
		StringParam dpAccti = null;
		StringParam dpPlan = null;
		StringParam dpReuse = null;
		// COB_CODE: SET DP-FUNCTION-IDENTIFY
		//               DP-SSID-PROD-DB2
		//               DP-COLL-PRODUCTION
		//               DP-PLAN-FEDPRINT        TO TRUE.
		ws.getDsnrliParms().getFunction().setIdentify();
		ws.getDsnrliParms().getSsid().setProdDb2();
		ws.getDsnrliParms().getColl().setProduction();
		ws.getDsnrliParms().setPlanFedprint();
		// COB_CODE: CALL 'DSNRLI' USING DP-FUNCTION
		//                               DP-SSID
		//                               DP-RIBPTR
		//                               DP-EIBPTR
		//                               DP-TECB
		//                               DP-SECB
		//                               DP-RETURN
		//                               DP-REASON
		//                               DP-GROUP.
		dpRibptr = new StringParam(ws.getDsnrliParms().getRibptr(), DsnrliParms.Len.RIBPTR);
		dpEibptr = new StringParam(ws.getDsnrliParms().getEibptr(), DsnrliParms.Len.EIBPTR);
		dpTecb = new StringParam(ws.getDsnrliParms().getTecb(), DsnrliParms.Len.TECB);
		dpSecb = new StringParam(ws.getDsnrliParms().getSecb(), DsnrliParms.Len.SECB);
		dpReturn = new StringParam(ws.getDsnrliParms().getReturnFld(), DsnrliParms.Len.RETURN_FLD);
		dpReason = new StringParam(ws.getDsnrliParms().getReason(), DsnrliParms.Len.REASON);
		dpGroup = new StringParam(ws.getDsnrliParms().getGroup(), DsnrliParms.Len.GROUP);
		DynamicCall.invoke("DSNRLI",
				new Object[] { ws.getDsnrliParms(), ws.getDsnrliParms().getSsid(), dpRibptr, dpEibptr, dpTecb, dpSecb, dpReturn, dpReason, dpGroup });
		ws.getDsnrliParms().setRibptr(dpRibptr.getString());
		ws.getDsnrliParms().setEibptr(dpEibptr.getString());
		ws.getDsnrliParms().setTecb(dpTecb.getString());
		ws.getDsnrliParms().setSecb(dpSecb.getString());
		ws.getDsnrliParms().setReturnFld(dpReturn.getString());
		ws.getDsnrliParms().setReason(dpReason.getString());
		ws.getDsnrliParms().setGroup(dpGroup.getString());
		//    IF THE ATTEMPT AT A CONNECT FAILS WITH PRODUCTION, WE WILL
		//    CONNECT TO DEVELOPMENT.
		// COB_CODE: IF DP-RETURN NOT = LOW-VALUES
		//               END-IF
		//           END-IF.
		if (!Characters.EQ_LOW.test(ws.getDsnrliParms().getReturnFldFormatted())) {
			// COB_CODE: PERFORM 3150-RRS-DB2-DEVL
			//              THRU 3150-EXIT
			rrsDb2Devl();
			// COB_CODE: IF DP-RETURN NOT = LOW-VALUES
			//               GO TO 3000-EXIT
			//           END-IF
			if (!Characters.EQ_LOW.test(ws.getDsnrliParms().getReturnFldFormatted())) {
				// COB_CODE: GO TO 3000-EXIT
				return;
			}
		}
		//    RRSAF SIGNON DB2
		// COB_CODE: SET DP-FUNCTION-SIGNON
		//               DP-CORR-FEDPRINT
		//               DP-ACCTT-PRINT-ROUTINE  TO TRUE.
		ws.getDsnrliParms().getFunction().setSignon();
		ws.getDsnrliParms().setCorrFedprint();
		ws.getDsnrliParms().setAccttPrintRoutine();
		// COB_CODE: CALL 'DSNRLI' USING DP-FUNCTION
		//                               DP-CORR
		//                               DP-ACCTT
		//                               DP-ACCTI
		//                               DP-RETURN
		//                               DP-REASON.
		dpCorr = new StringParam(ws.getDsnrliParms().getCorr(), DsnrliParms.Len.CORR);
		dpAcctt = new StringParam(ws.getDsnrliParms().getAcctt(), DsnrliParms.Len.ACCTT);
		dpAccti = new StringParam(ws.getDsnrliParms().getAccti(), DsnrliParms.Len.ACCTI);
		dpReturn = new StringParam(ws.getDsnrliParms().getReturnFld(), DsnrliParms.Len.RETURN_FLD);
		dpReason = new StringParam(ws.getDsnrliParms().getReason(), DsnrliParms.Len.REASON);
		DynamicCall.invoke("DSNRLI", new Object[] { ws.getDsnrliParms(), dpCorr, dpAcctt, dpAccti, dpReturn, dpReason });
		ws.getDsnrliParms().setCorr(dpCorr.getString());
		ws.getDsnrliParms().setAcctt(dpAcctt.getString());
		ws.getDsnrliParms().setAccti(dpAccti.getString());
		ws.getDsnrliParms().setReturnFld(dpReturn.getString());
		ws.getDsnrliParms().setReason(dpReason.getString());
		// COB_CODE: IF DP-RETURN NOT = LOW-VALUES
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!Characters.EQ_LOW.test(ws.getDsnrliParms().getReturnFldFormatted())) {
			// COB_CODE: PERFORM 9000-FILL-CONNECT-ERROR-MSG
			//              THRU 9000-EXIT
			fillConnectErrorMsg();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//    CREATE THREAD
		// COB_CODE: SET DP-FUNCTION-CREATE-THREAD
		//               DP-REUSE-RESET          TO TRUE.
		ws.getDsnrliParms().getFunction().setCreateThread();
		ws.getDsnrliParms().setReuseReset();
		// COB_CODE: CALL 'DSNRLI' USING DP-FUNCTION
		//                               DP-PLAN
		//                               DP-COLL
		//                               DP-REUSE
		//                               DP-RETURN
		//                               DP-REASON.
		dpPlan = new StringParam(ws.getDsnrliParms().getPlan(), DsnrliParms.Len.PLAN);
		dpReuse = new StringParam(ws.getDsnrliParms().getReuse(), DsnrliParms.Len.REUSE);
		dpReturn = new StringParam(ws.getDsnrliParms().getReturnFld(), DsnrliParms.Len.RETURN_FLD);
		dpReason = new StringParam(ws.getDsnrliParms().getReason(), DsnrliParms.Len.REASON);
		DynamicCall.invoke("DSNRLI", new Object[] { ws.getDsnrliParms(), dpPlan, ws.getDsnrliParms().getColl(), dpReuse, dpReturn, dpReason });
		ws.getDsnrliParms().setPlan(dpPlan.getString());
		ws.getDsnrliParms().setReuse(dpReuse.getString());
		ws.getDsnrliParms().setReturnFld(dpReturn.getString());
		ws.getDsnrliParms().setReason(dpReason.getString());
		// COB_CODE: IF DP-RETURN NOT = LOW-VALUES
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!Characters.EQ_LOW.test(ws.getDsnrliParms().getReturnFldFormatted())) {
			// COB_CODE: PERFORM 9000-FILL-CONNECT-ERROR-MSG
			//              THRU 9000-EXIT
			fillConnectErrorMsg();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3150-RRS-DB2-DEVL<br>
	 * <pre>    THE CONNECT TO PROD FAILED, SO WE'LL CONNECT TO THE
	 *     DEVELOPMENT SYSTEM.</pre>*/
	private void rrsDb2Devl() {
		StringParam dpRibptr = null;
		StringParam dpEibptr = null;
		StringParam dpTecb = null;
		StringParam dpSecb = null;
		StringParam dpReturn = null;
		StringParam dpReason = null;
		StringParam dpGroup = null;
		// COB_CODE: SET DP-FUNCTION-IDENTIFY
		//               DP-SSID-DEV-DB2
		//               DP-COLL-DEVELOPMENT
		//               DP-PLAN-FEDPRINT        TO TRUE.
		ws.getDsnrliParms().getFunction().setIdentify();
		ws.getDsnrliParms().getSsid().setDevDb2();
		ws.getDsnrliParms().getColl().setDevelopment();
		ws.getDsnrliParms().setPlanFedprint();
		// COB_CODE: CALL 'DSNRLI' USING DP-FUNCTION
		//                               DP-SSID
		//                               DP-RIBPTR
		//                               DP-EIBPTR
		//                               DP-TECB
		//                               DP-SECB
		//                               DP-RETURN
		//                               DP-REASON
		//                               DP-GROUP.
		dpRibptr = new StringParam(ws.getDsnrliParms().getRibptr(), DsnrliParms.Len.RIBPTR);
		dpEibptr = new StringParam(ws.getDsnrliParms().getEibptr(), DsnrliParms.Len.EIBPTR);
		dpTecb = new StringParam(ws.getDsnrliParms().getTecb(), DsnrliParms.Len.TECB);
		dpSecb = new StringParam(ws.getDsnrliParms().getSecb(), DsnrliParms.Len.SECB);
		dpReturn = new StringParam(ws.getDsnrliParms().getReturnFld(), DsnrliParms.Len.RETURN_FLD);
		dpReason = new StringParam(ws.getDsnrliParms().getReason(), DsnrliParms.Len.REASON);
		dpGroup = new StringParam(ws.getDsnrliParms().getGroup(), DsnrliParms.Len.GROUP);
		DynamicCall.invoke("DSNRLI",
				new Object[] { ws.getDsnrliParms(), ws.getDsnrliParms().getSsid(), dpRibptr, dpEibptr, dpTecb, dpSecb, dpReturn, dpReason, dpGroup });
		ws.getDsnrliParms().setRibptr(dpRibptr.getString());
		ws.getDsnrliParms().setEibptr(dpEibptr.getString());
		ws.getDsnrliParms().setTecb(dpTecb.getString());
		ws.getDsnrliParms().setSecb(dpSecb.getString());
		ws.getDsnrliParms().setReturnFld(dpReturn.getString());
		ws.getDsnrliParms().setReason(dpReason.getString());
		ws.getDsnrliParms().setGroup(dpGroup.getString());
		// COB_CODE: IF DP-RETURN NOT = LOW-VALUES
		//               GO TO 3150-EXIT
		//           END-IF.
		if (!Characters.EQ_LOW.test(ws.getDsnrliParms().getReturnFldFormatted())) {
			// COB_CODE: PERFORM 9000-FILL-CONNECT-ERROR-MSG
			//              THRU 9000-EXIT
			fillConnectErrorMsg();
			// COB_CODE: GO TO 3150-EXIT
			return;
		}
	}

	/**Original name: 4000-RETRIEVE-REPORT-DATA<br>
	 * <pre>    ONLY PULL THE REPORT RECORD WHERE THE RECORD STATUS CODE
	 *     IS ACTIVE (RATHER THAN PENDING OR HISTORICAL).</pre>*/
	private void retrieveReportData1() {
		// COB_CODE: MOVE L-RD-REPORT-NBR        TO  SA-REPORT-NBR.
		ws.setSaReportNbr(lReportDbParameters.getReportNbr());
		// COB_CODE: EXEC SQL
		//               SELECT
		//                   RPT_NBR,
		//                   RPT_DESC,
		//                   RPT_ACT_FLG,
		//                   RPT_BAL_FLG,
		//                   OFF_LOC_1,
		//                   OFF_LOC_FLG_1,
		//                   OFF_LOC_2,
		//                   OFF_LOC_FLG_2,
		//                   OFF_LOC_3,
		//                   OFF_LOC_FLG_3,
		//                   OFF_LOC_4,
		//                   OFF_LOC_FLG_4,
		//                   OFF_LOC_5,
		//                   OFF_LOC_FLG_5,
		//                   OFF_LOC_6,
		//                   OFF_LOC_FLG_6,
		//                   OFF_LOC_7,
		//                   OFF_LOC_FLG_7,
		//                   OFF_LOC_8,
		//                   OFF_LOC_FLG_8,
		//                   OFF_LOC_9,
		//                   OFF_LOC_FLG_9,
		//                   OFF_LOC_10,
		//                   OFF_LOC_FLG_10,
		//                   OFF_LOC_11,
		//                   OFF_LOC_FLG_11,
		//                   OFF_LOC_12,
		//                   OFF_LOC_FLG_12,
		//                   OFF_LOC_13,
		//                   OFF_LOC_FLG_13,
		//                   OFF_LOC_14,
		//                   OFF_LOC_FLG_14,
		//                   OFF_LOC_15,
		//                   OFF_LOC_FLG_15,
		//                   OFF_LOC_DEFLT_FLG
		//               INTO   :RPT-NBR                            ,
		//                      :RPT-DESC                           ,
		//                      :RPT-ACT-FLG                        ,
		//                      :RPT-BAL-FLG                        ,
		//                      :OFF-LOC-1                          ,
		//                      :OFF-LOC-FLG-1                      ,
		//                      :OFF-LOC-2                          ,
		//                      :OFF-LOC-FLG-2                      ,
		//                      :OFF-LOC-3                          ,
		//                      :OFF-LOC-FLG-3                      ,
		//                      :OFF-LOC-4                          ,
		//                      :OFF-LOC-FLG-4                      ,
		//                      :OFF-LOC-5                          ,
		//                      :OFF-LOC-FLG-5                      ,
		//                      :OFF-LOC-6                          ,
		//                      :OFF-LOC-FLG-6                      ,
		//                      :OFF-LOC-7                          ,
		//                      :OFF-LOC-FLG-7                      ,
		//                      :OFF-LOC-8                          ,
		//                      :OFF-LOC-FLG-8                      ,
		//                      :OFF-LOC-9                          ,
		//                      :OFF-LOC-FLG-9                      ,
		//                      :OFF-LOC-10                         ,
		//                      :OFF-LOC-FLG-10                     ,
		//                      :OFF-LOC-11                         ,
		//                      :OFF-LOC-FLG-11                     ,
		//                      :OFF-LOC-12                         ,
		//                      :OFF-LOC-FLG-12                     ,
		//                      :OFF-LOC-13                         ,
		//                      :OFF-LOC-FLG-13                     ,
		//                      :OFF-LOC-14                         ,
		//                      :OFF-LOC-FLG-14                     ,
		//                      :OFF-LOC-15                         ,
		//                      :OFF-LOC-FLG-15                     ,
		//                      :OFF-LOC-DEFLT-FLG
		//                   FROM  FED_RMT_PRT_TAB
		//                   WHERE RPT_NBR = :SA-REPORT-NBR
		//                     AND RPT_RCD_STA_CD = '0'
		//           END-EXEC.
		fedRmtPrtTabDao.selectBySaReportNbr(ws.getSaReportNbr(), ws.getDclfedRmtPrtTab());
		// COB_CODE: EVALUATE TRUE
		//               WHEN SQLCODE = CF-SC-RECORD-FOUND
		//                                       TO L-RD-TABLE-DATA
		//               WHEN SQLCODE = CF-SC-NO-RECORD-FOUND
		//                                       TO TRUE
		//               WHEN SQLCODE = CF-SC-NOT-CONNECTED
		//                                       TO TRUE
		//               WHEN OTHER
		//                   GO TO 4000-EXIT
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == ws.getCfScRecordFound()) {
			// COB_CODE: MOVE DCLFED-RMT-PRT-TAB
			//                               TO L-RD-TABLE-DATA
			lReportDbParameters.setTableData(ws.getDclfedRmtPrtTab().getDclfedRmtPrtTabFormatted());
		} else if (sqlca.getSqlcode() == ws.getCfScNoRecordFound()) {
			// COB_CODE: SET L-RD-STATUS-REPORT-NOT-FOUND
			//                               TO TRUE
			lReportDbParameters.getStatus().setReportNotFound();
		} else if (sqlca.getSqlcode() == ws.getCfScNotConnected()) {
			// COB_CODE: SET L-RD-STATUS-NOT-CONNECTED
			//                               TO TRUE
			lReportDbParameters.getStatus().setNotConnected();
		} else {
			// COB_CODE: PERFORM 9010-FILL-FETCH-ERROR-MSG
			//              THRU 9010-EXIT
			fillFetchErrorMsg();
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
	}

	/**Original name: 5000-RRS-DB2-DISCONNECT<br>*/
	private void rrsDb2Disconnect() {
		StringParam dpReturn = null;
		StringParam dpReason = null;
		// COB_CODE: EXEC SQL
		//               COMMIT
		//           END-EXEC.
		DbService.getCurrent().commit(dbAccessStatus);
		//    RRS TERM IDENT FROM DB2
		// COB_CODE: SET DP-FUNCTION-TERMINATE-IDENTIFY
		//                                       TO TRUE.
		ws.getDsnrliParms().getFunction().setTerminateIdentify();
		// COB_CODE: CALL 'DSNRLI' USING DP-FUNCTION
		//                               DP-RETURN
		//                               DP-REASON.
		dpReturn = new StringParam(ws.getDsnrliParms().getReturnFld(), DsnrliParms.Len.RETURN_FLD);
		dpReason = new StringParam(ws.getDsnrliParms().getReason(), DsnrliParms.Len.REASON);
		DynamicCall.invoke("DSNRLI", ws.getDsnrliParms(), dpReturn, dpReason);
		ws.getDsnrliParms().setReturnFld(dpReturn.getString());
		ws.getDsnrliParms().setReason(dpReason.getString());
		// COB_CODE: IF DP-RETURN NOT = LOW-VALUES
		//               GO TO 5000-EXIT
		//           END-IF.
		if (!Characters.EQ_LOW.test(ws.getDsnrliParms().getReturnFldFormatted())) {
			// COB_CODE: PERFORM 9000-FILL-CONNECT-ERROR-MSG
			//              THRU 9000-EXIT
			fillConnectErrorMsg();
			// COB_CODE: GO TO 5000-EXIT
			return;
		}
	}

	/**Original name: 9000-FILL-CONNECT-ERROR-MSG<br>*/
	private void fillConnectErrorMsg() {
		// COB_CODE: SET L-RD-STATUS-FATAL-ERROR TO TRUE.
		lReportDbParameters.getStatus().setFatalError();
		// COB_CODE: MOVE EA-00-PROGRAM-MSG      TO L-RD-EM-MESSAGE1.
		lReportDbParameters.setEmMessage1(ws.getErrorAndAdviceMessages().getEa00ProgramMsg().getEa00ProgramMsgFormatted());
		// COB_CODE: MOVE DP-FUNCTION            TO EA-01-FUNCTION.
		ws.getErrorAndAdviceMessages().getEa01ConnectErrorMsg().setFunction(ws.getDsnrliParms().getFunction().getFunction());
		// COB_CODE: MOVE DP-SSID                TO EA-01-SSID.
		ws.getErrorAndAdviceMessages().getEa01ConnectErrorMsg().setSsid(ws.getDsnrliParms().getSsid().getSsid());
		// COB_CODE: MOVE DP-RETURN              TO EA-01-RETURN.
		ws.getErrorAndAdviceMessages().getEa01ConnectErrorMsg().setReturnFld(ws.getDsnrliParms().getReturnFld());
		// COB_CODE: MOVE DP-REASON              TO EA-01-REASON.
		ws.getErrorAndAdviceMessages().getEa01ConnectErrorMsg().setReason(ws.getDsnrliParms().getReason());
		// COB_CODE: MOVE EA-01-CONNECT-ERROR-MSG
		//                                       TO L-RD-EM-MESSAGE2.
		lReportDbParameters.setEmMessage2(ws.getErrorAndAdviceMessages().getEa01ConnectErrorMsg().getEa01ConnectErrorMsgFormatted());
	}

	/**Original name: 9010-FILL-FETCH-ERROR-MSG<br>*/
	private void fillFetchErrorMsg() {
		// COB_CODE: SET L-RD-STATUS-FATAL-ERROR TO TRUE.
		lReportDbParameters.getStatus().setFatalError();
		// COB_CODE: MOVE EA-00-PROGRAM-MSG      TO L-RD-EM-MESSAGE1.
		lReportDbParameters.setEmMessage1(ws.getErrorAndAdviceMessages().getEa00ProgramMsg().getEa00ProgramMsgFormatted());
		// COB_CODE: MOVE SQLCODE                TO EA-02-SQLCODE.
		ws.getErrorAndAdviceMessages().getEa02FetchErrorMsg().setSqlcode(sqlca.getSqlcode());
		// COB_CODE: MOVE SQLERRMC               TO EA-02-ERROR-MSG.
		ws.getErrorAndAdviceMessages().getEa02FetchErrorMsg().setErrorMsg(sqlca.getSqlerrmc());
		// COB_CODE: MOVE EA-02-FETCH-ERROR-MSG  TO L-RD-EM-MESSAGE2.
		lReportDbParameters.setEmMessage2(ws.getErrorAndAdviceMessages().getEa02FetchErrorMsg().getEa02FetchErrorMsgFormatted());
	}

	public String getWhenCompiledFormatted() {
		return Functions.padBlanks(WHEN_COMPILED, Len.WHEN_COMPILED);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WHEN_COMPILED = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
