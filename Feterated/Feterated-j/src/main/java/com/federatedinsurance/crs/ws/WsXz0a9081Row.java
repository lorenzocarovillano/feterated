/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Xza981TtyCertList;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9081-ROW<br>
 * Variable: WS-XZ0A9081-ROW from program XZ0B9081<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9081Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA981-MAX-TTY-ROWS
	private short maxTtyRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA981-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA981-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA981-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);
	//Original name: XZA981-TTY-CERT-LIST
	private Xza981TtyCertList ttyCertList = new Xza981TtyCertList();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9081_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9081RowBytes(buf);
	}

	public String getWsXz0a9081RowFormatted() {
		return getTtyCertInfoRowFormatted();
	}

	public void setWsXz0a9081RowBytes(byte[] buffer) {
		setWsXz0a9081RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9081RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9081_ROW];
		return getWsXz0a9081RowBytes(buffer, 1);
	}

	public void setWsXz0a9081RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setTtyCertInfoRowBytes(buffer, position);
	}

	public byte[] getWsXz0a9081RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getTtyCertInfoRowBytes(buffer, position);
		return buffer;
	}

	public String getTtyCertInfoRowFormatted() {
		return MarshalByteExt.bufferToStr(getTtyCertInfoRowBytes());
	}

	/**Original name: XZA981-TTY-CERT-INFO-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0A9081 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_THIRD_PARTY_CERT_LIST              *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  28JAN2009 E404DLP    NEW                          *
	 *  PP02500  07SEP2012 E404BPO    CHANGE TTY-CERT-LIST         *
	 *                                STRUCTURE.                   *
	 *  20163.20 13JUL2018 E404DMW    UPDATED ADDRESS AND CLIENT   *
	 *                                IDS FROM 20 TO 64            *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getTtyCertInfoRowBytes() {
		byte[] buffer = new byte[Len.TTY_CERT_INFO_ROW];
		return getTtyCertInfoRowBytes(buffer, 1);
	}

	public void setTtyCertInfoRowBytes(byte[] buffer, int offset) {
		int position = offset;
		maxTtyRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
		position += Len.USERID;
		ttyCertList.setTtyCertListBytes(buffer, position);
	}

	public byte[] getTtyCertInfoRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxTtyRows);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position += Len.USERID;
		ttyCertList.getTtyCertListBytes(buffer, position);
		return buffer;
	}

	public void setMaxTtyRows(short maxTtyRows) {
		this.maxTtyRows = maxTtyRows;
	}

	public short getMaxTtyRows() {
		return this.maxTtyRows;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	public Xza981TtyCertList getTtyCertList() {
		return ttyCertList;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9081RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_TTY_ROWS = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int USERID = 8;
		public static final int TTY_CERT_INFO_ROW = MAX_TTY_ROWS + CSR_ACT_NBR + NOT_PRC_TS + USERID + Xza981TtyCertList.Len.TTY_CERT_LIST;
		public static final int WS_XZ0A9081_ROW = TTY_CERT_INFO_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
