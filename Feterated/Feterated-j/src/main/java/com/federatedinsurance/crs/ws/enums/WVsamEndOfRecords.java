/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: W-VSAM-END-OF-RECORDS<br>
 * Variable: W-VSAM-END-OF-RECORDS from program HALOESTO<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WVsamEndOfRecords {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.W_VSAM_END_OF_RECORDS);
	public static final String FIND_RECORDS = "0";
	public static final String NO_MORE_RECORDS = "1";

	//==== METHODS ====
	public void setwVsamEndOfRecords(short wVsamEndOfRecords) {
		this.value = NumericDisplay.asString(wVsamEndOfRecords, Len.W_VSAM_END_OF_RECORDS);
	}

	public void setwVsamEndOfRecordsFormatted(String wVsamEndOfRecords) {
		this.value = Trunc.toUnsignedNumeric(wVsamEndOfRecords, Len.W_VSAM_END_OF_RECORDS);
	}

	public short getwVsamEndOfRecords() {
		return NumericDisplay.asShort(this.value);
	}

	public String getwVsamEndOfRecordsFormatted() {
		return this.value;
	}

	public void setFindRecords() {
		setwVsamEndOfRecordsFormatted(FIND_RECORDS);
	}

	public boolean isNoMoreRecords() {
		return getwVsamEndOfRecordsFormatted().equals(NO_MORE_RECORDS);
	}

	public void setNoMoreRecords() {
		setwVsamEndOfRecordsFormatted(NO_MORE_RECORDS);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int W_VSAM_END_OF_RECORDS = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
