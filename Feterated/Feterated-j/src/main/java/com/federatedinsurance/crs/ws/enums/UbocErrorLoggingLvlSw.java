/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UBOC-ERROR-LOGGING-LVL-SW<br>
 * Variable: UBOC-ERROR-LOGGING-LVL-SW from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocErrorLoggingLvlSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char WITHIN_BUS_OBJ = 'B';
	public static final char WITHIN_MCM = 'C';
	public static final char WITHIN_MDRV = 'M';
	public static final char WITHIN_SECURITY = 'S';
	public static final char INTERMEDIATE_LOG = 'I';
	public static final char FINAL_LOG = 'F';

	//==== METHODS ====
	public void setErrorLoggingLvlSw(char errorLoggingLvlSw) {
		this.value = errorLoggingLvlSw;
	}

	public char getErrorLoggingLvlSw() {
		return this.value;
	}

	public void setWithinBusObj() {
		value = WITHIN_BUS_OBJ;
	}

	public void setIntermediateLog() {
		value = INTERMEDIATE_LOG;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_LOGGING_LVL_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
