/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-FORMATTED-SSN<br>
 * Variable: SA-FORMATTED-SSN from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaFormattedSsn {

	//==== PROPERTIES ====
	//Original name: SA-FS-FIRST-3
	private String first3 = DefaultValues.stringVal(Len.FIRST3);
	//Original name: FILLER-SA-FORMATTED-SSN
	private char flr1 = '-';
	//Original name: SA-FS-SECOND-2
	private String second2 = DefaultValues.stringVal(Len.SECOND2);
	//Original name: FILLER-SA-FORMATTED-SSN-1
	private char flr2 = '-';
	//Original name: SA-FS-LAST-4
	private String last4 = DefaultValues.stringVal(Len.LAST4);

	//==== METHODS ====
	public String getFormattedSsnFormatted() {
		return MarshalByteExt.bufferToStr(getFormattedSsnBytes());
	}

	public byte[] getFormattedSsnBytes() {
		byte[] buffer = new byte[Len.FORMATTED_SSN];
		return getFormattedSsnBytes(buffer, 1);
	}

	public byte[] getFormattedSsnBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, first3, Len.FIRST3);
		position += Len.FIRST3;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, second2, Len.SECOND2);
		position += Len.SECOND2;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, last4, Len.LAST4);
		return buffer;
	}

	public void setFirst3(String first3) {
		this.first3 = Functions.subString(first3, Len.FIRST3);
	}

	public String getFirst3() {
		return this.first3;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setSecond2(String second2) {
		this.second2 = Functions.subString(second2, Len.SECOND2);
	}

	public String getSecond2() {
		return this.second2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setLast4(String last4) {
		this.last4 = Functions.subString(last4, Len.LAST4);
	}

	public String getLast4() {
		return this.last4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FIRST3 = 3;
		public static final int SECOND2 = 2;
		public static final int LAST4 = 4;
		public static final int FLR1 = 1;
		public static final int FORMATTED_SSN = FIRST3 + SECOND2 + LAST4 + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
