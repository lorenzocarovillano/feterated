/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.UbocApplicationTsSw;
import com.federatedinsurance.crs.ws.enums.UbocDataPrivRetCode;
import com.federatedinsurance.crs.ws.enums.UbocKeepClearUowStorageSw;
import com.federatedinsurance.crs.ws.enums.UbocPassThruAction;
import com.federatedinsurance.crs.ws.enums.UbocStoreTypeCd;
import com.federatedinsurance.crs.ws.enums.UbocUowLockStrategyCd;
import com.federatedinsurance.crs.ws.enums.UbocUserInLockGrpSw;
import com.modernsystems.ctu.data.NumericDisplay;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: UBOC-COMM-INFO<br>
 * Variable: UBOC-COMM-INFO from copybook HALLUBOC<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class UbocCommInfo {

	//==== PROPERTIES ====
	/**Original name: UBOC-UOW-NAME<br>
	 * <pre>* SENT FROM HALOMDRV OR BUILT BY HALOMCM.</pre>*/
	private String ubocUowName = DefaultValues.stringVal(Len.UBOC_UOW_NAME);
	//Original name: UBOC-MSG-ID
	private String ubocMsgId = DefaultValues.stringVal(Len.UBOC_MSG_ID);
	//Original name: UBOC-AUTH-USERID
	private String ubocAuthUserid = DefaultValues.stringVal(Len.UBOC_AUTH_USERID);
	//Original name: UBOC-AUTH-USER-CLIENTID
	private String ubocAuthUserClientid = DefaultValues.stringVal(Len.UBOC_AUTH_USER_CLIENTID);
	//Original name: UBOC-TERMINAL-ID
	private String ubocTerminalId = DefaultValues.stringVal(Len.UBOC_TERMINAL_ID);
	//Original name: UBOC-PRIMARY-BUS-OBJ
	private String ubocPrimaryBusObj = DefaultValues.stringVal(Len.UBOC_PRIMARY_BUS_OBJ);
	//Original name: UBOC-STORE-TYPE-CD
	private UbocStoreTypeCd ubocStoreTypeCd = new UbocStoreTypeCd();
	//Original name: UBOC-MDR-REQ-STORE
	private String ubocMdrReqStore = DefaultValues.stringVal(Len.UBOC_MDR_REQ_STORE);
	//Original name: UBOC-MDR-RSP-STORE
	private String ubocMdrRspStore = DefaultValues.stringVal(Len.UBOC_MDR_RSP_STORE);
	//Original name: UBOC-UOW-REQ-MSG-STORE
	private String ubocUowReqMsgStore = DefaultValues.stringVal(Len.UBOC_UOW_REQ_MSG_STORE);
	//Original name: UBOC-UOW-REQ-SWITCHES-STORE
	private String ubocUowReqSwitchesStore = DefaultValues.stringVal(Len.UBOC_UOW_REQ_SWITCHES_STORE);
	//Original name: UBOC-UOW-RESP-HEADER-STORE
	private String ubocUowRespHeaderStore = DefaultValues.stringVal(Len.UBOC_UOW_RESP_HEADER_STORE);
	//Original name: UBOC-UOW-RESP-DATA-STORE
	private String ubocUowRespDataStore = DefaultValues.stringVal(Len.UBOC_UOW_RESP_DATA_STORE);
	//Original name: UBOC-UOW-RESP-WARNINGS-STORE
	private String ubocUowRespWarningsStore = DefaultValues.stringVal(Len.UBOC_UOW_RESP_WARNINGS_STORE);
	//Original name: UBOC-UOW-KEY-REPLACE-STORE
	private String ubocUowKeyReplaceStore = DefaultValues.stringVal(Len.UBOC_UOW_KEY_REPLACE_STORE);
	//Original name: UBOC-UOW-RESP-NL-BL-ERRS-STORE
	private String ubocUowRespNlBlErrsStore = DefaultValues.stringVal(Len.UBOC_UOW_RESP_NL_BL_ERRS_STORE);
	//Original name: UBOC-SESSION-ID
	private String ubocSessionId = DefaultValues.stringVal(Len.UBOC_SESSION_ID);
	//Original name: UBOC-UOW-LOCK-PROC-TSQ
	private String ubocUowLockProcTsq = DefaultValues.stringVal(Len.UBOC_UOW_LOCK_PROC_TSQ);
	/**Original name: UBOC-UOW-REQ-SWITCHES-TSQ<br>
	 * <pre>*32098       07 FILLER-1                          PIC X(16).</pre>*/
	private String ubocUowReqSwitchesTsq = DefaultValues.stringVal(Len.UBOC_UOW_REQ_SWITCHES_TSQ);
	/**Original name: UBOC-PASS-THRU-ACTION<br>
	 * <pre>* BUILT/USED BY HALOMCM, BUS OBJECTS, AND UTILITY OBJECTS</pre>*/
	private UbocPassThruAction ubocPassThruAction = new UbocPassThruAction();
	/**Original name: UBOC-NBR-HDR-ROWS<br>
	 * <pre>* BUILT BY BUSINESS OBJECTS, READ BY HALOMCM</pre>*/
	private String ubocNbrHdrRows = DefaultValues.stringVal(Len.UBOC_NBR_HDR_ROWS);
	//Original name: UBOC-NBR-DATA-ROWS
	private String ubocNbrDataRows = DefaultValues.stringVal(Len.UBOC_NBR_DATA_ROWS);
	//Original name: UBOC-NBR-WARNINGS
	private String ubocNbrWarnings = DefaultValues.stringVal(Len.UBOC_NBR_WARNINGS);
	//Original name: UBOC-NBR-NONLOG-BL-ERRS
	private String ubocNbrNonlogBlErrs = DefaultValues.stringVal(Len.UBOC_NBR_NONLOG_BL_ERRS);
	/**Original name: UBOC-APPLICATION-TS-SW<br>
	 * <pre>*       07 FILLER-2                          PIC X(20).
	 * * CREATED AND USED BY BDOS
	 * *       07 UBOC-APPLICATION-TS-SW            PIC X VALUE SPACE.</pre>*/
	private UbocApplicationTsSw ubocApplicationTsSw = new UbocApplicationTsSw();
	//Original name: UBOC-APPLICATION-TS
	private String ubocApplicationTs = DefaultValues.stringVal(Len.UBOC_APPLICATION_TS);
	//Original name: FILLER-UBOC-COMM-INFO
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	/**Original name: UBOC-UOW-LOCK-STRATEGY-CD<br>
	 * <pre>* USED FOR LOCK GENERATION AND DELETION
	 *         07 FILLER                            PIC X(10).</pre>*/
	private UbocUowLockStrategyCd ubocUowLockStrategyCd = new UbocUowLockStrategyCd();
	//Original name: UBOC-UOW-LOCK-TIMEOUT-INTERVAL
	private int ubocUowLockTimeoutInterval = DefaultValues.INT_VAL;
	//Original name: UBOC-USER-IN-LOCK-GRP-SW
	private UbocUserInLockGrpSw ubocUserInLockGrpSw = new UbocUserInLockGrpSw();
	//Original name: UBOC-LOCK-GROUP
	private String ubocLockGroup = DefaultValues.stringVal(Len.UBOC_LOCK_GROUP);
	//Original name: FILLER-3
	private String filler3 = DefaultValues.stringVal(Len.FILLER3);
	//Original name: UBOC-SEC-AND-DATA-PRIV-INFO
	private UbocSecAndDataPrivInfo ubocSecAndDataPrivInfo = new UbocSecAndDataPrivInfo();
	//Original name: UBOC-DATA-PRIV-RET-CODE
	private UbocDataPrivRetCode ubocDataPrivRetCode = new UbocDataPrivRetCode();
	/**Original name: UBOC-SEC-GROUP-NAME<br>
	 * <pre>        07 FILLER-4                          PIC X(20).</pre>*/
	private String ubocSecGroupName = DefaultValues.stringVal(Len.UBOC_SEC_GROUP_NAME);
	//Original name: FILLER-4
	private String filler4 = DefaultValues.stringVal(Len.FILLER4);
	//Original name: UBOC-AUDIT-PROCESSING-INFO
	private UbocAuditProcessingInfo ubocAuditProcessingInfo = new UbocAuditProcessingInfo();
	//Original name: FILLER-5
	private String filler5 = DefaultValues.stringVal(Len.FILLER5);
	/**Original name: UBOC-KEEP-CLEAR-UOW-STORAGE-SW<br>
	 * <pre>* RETURN SWITCH FROM INVOKED UOWS INDICATING IF UOW LEVEL
	 * * STORAGE SHOULD BE RETAINED OR CLEARED.
	 * * CURRENTLY THE DEFAULT IS TO ALWAYS CLEAR ALL UOW LEVEL
	 * * STORAGE. THE EXCEPTIONS ARE ERROR LOGGING SPECIFIC UOWS.</pre>*/
	private UbocKeepClearUowStorageSw ubocKeepClearUowStorageSw = new UbocKeepClearUowStorageSw();
	//Original name: UBOC-ERROR-DETAILS
	private UbocErrorDetails ubocErrorDetails = new UbocErrorDetails();

	//==== METHODS ====
	public void setCommInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		ubocUowName = MarshalByte.readString(buffer, position, Len.UBOC_UOW_NAME);
		position += Len.UBOC_UOW_NAME;
		ubocMsgId = MarshalByte.readString(buffer, position, Len.UBOC_MSG_ID);
		position += Len.UBOC_MSG_ID;
		ubocAuthUserid = MarshalByte.readString(buffer, position, Len.UBOC_AUTH_USERID);
		position += Len.UBOC_AUTH_USERID;
		ubocAuthUserClientid = MarshalByte.readString(buffer, position, Len.UBOC_AUTH_USER_CLIENTID);
		position += Len.UBOC_AUTH_USER_CLIENTID;
		ubocTerminalId = MarshalByte.readString(buffer, position, Len.UBOC_TERMINAL_ID);
		position += Len.UBOC_TERMINAL_ID;
		ubocPrimaryBusObj = MarshalByte.readString(buffer, position, Len.UBOC_PRIMARY_BUS_OBJ);
		position += Len.UBOC_PRIMARY_BUS_OBJ;
		ubocStoreTypeCd.setUbocStoreTypeCd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		ubocMdrReqStore = MarshalByte.readString(buffer, position, Len.UBOC_MDR_REQ_STORE);
		position += Len.UBOC_MDR_REQ_STORE;
		ubocMdrRspStore = MarshalByte.readString(buffer, position, Len.UBOC_MDR_RSP_STORE);
		position += Len.UBOC_MDR_RSP_STORE;
		ubocUowReqMsgStore = MarshalByte.readString(buffer, position, Len.UBOC_UOW_REQ_MSG_STORE);
		position += Len.UBOC_UOW_REQ_MSG_STORE;
		ubocUowReqSwitchesStore = MarshalByte.readString(buffer, position, Len.UBOC_UOW_REQ_SWITCHES_STORE);
		position += Len.UBOC_UOW_REQ_SWITCHES_STORE;
		ubocUowRespHeaderStore = MarshalByte.readString(buffer, position, Len.UBOC_UOW_RESP_HEADER_STORE);
		position += Len.UBOC_UOW_RESP_HEADER_STORE;
		ubocUowRespDataStore = MarshalByte.readString(buffer, position, Len.UBOC_UOW_RESP_DATA_STORE);
		position += Len.UBOC_UOW_RESP_DATA_STORE;
		ubocUowRespWarningsStore = MarshalByte.readString(buffer, position, Len.UBOC_UOW_RESP_WARNINGS_STORE);
		position += Len.UBOC_UOW_RESP_WARNINGS_STORE;
		ubocUowKeyReplaceStore = MarshalByte.readString(buffer, position, Len.UBOC_UOW_KEY_REPLACE_STORE);
		position += Len.UBOC_UOW_KEY_REPLACE_STORE;
		ubocUowRespNlBlErrsStore = MarshalByte.readString(buffer, position, Len.UBOC_UOW_RESP_NL_BL_ERRS_STORE);
		position += Len.UBOC_UOW_RESP_NL_BL_ERRS_STORE;
		ubocSessionId = MarshalByte.readString(buffer, position, Len.UBOC_SESSION_ID);
		position += Len.UBOC_SESSION_ID;
		ubocUowLockProcTsq = MarshalByte.readString(buffer, position, Len.UBOC_UOW_LOCK_PROC_TSQ);
		position += Len.UBOC_UOW_LOCK_PROC_TSQ;
		ubocUowReqSwitchesTsq = MarshalByte.readString(buffer, position, Len.UBOC_UOW_REQ_SWITCHES_TSQ);
		position += Len.UBOC_UOW_REQ_SWITCHES_TSQ;
		ubocPassThruAction.setUbocPassThruAction(MarshalByte.readString(buffer, position, UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION));
		position += UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION;
		ubocNbrHdrRows = MarshalByte.readFixedString(buffer, position, Len.UBOC_NBR_HDR_ROWS);
		position += Len.UBOC_NBR_HDR_ROWS;
		ubocNbrDataRows = MarshalByte.readFixedString(buffer, position, Len.UBOC_NBR_DATA_ROWS);
		position += Len.UBOC_NBR_DATA_ROWS;
		ubocNbrWarnings = MarshalByte.readFixedString(buffer, position, Len.UBOC_NBR_WARNINGS);
		position += Len.UBOC_NBR_WARNINGS;
		ubocNbrNonlogBlErrs = MarshalByte.readFixedString(buffer, position, Len.UBOC_NBR_NONLOG_BL_ERRS);
		position += Len.UBOC_NBR_NONLOG_BL_ERRS;
		ubocApplicationTsSw.setUbocApplicationTsSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		ubocApplicationTs = MarshalByte.readString(buffer, position, Len.UBOC_APPLICATION_TS);
		position += Len.UBOC_APPLICATION_TS;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		ubocUowLockStrategyCd.setUbocUowLockStrategyCd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		ubocUowLockTimeoutInterval = MarshalByte.readInt(buffer, position, Len.UBOC_UOW_LOCK_TIMEOUT_INTERVAL);
		position += Len.UBOC_UOW_LOCK_TIMEOUT_INTERVAL;
		ubocUserInLockGrpSw.setUbocUserInLockGrpSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		ubocLockGroup = MarshalByte.readString(buffer, position, Len.UBOC_LOCK_GROUP);
		position += Len.UBOC_LOCK_GROUP;
		filler3 = MarshalByte.readString(buffer, position, Len.FILLER3);
		position += Len.FILLER3;
		ubocSecAndDataPrivInfo.setUbocSecAndDataPrivInfoBytes(buffer, position);
		position += UbocSecAndDataPrivInfo.Len.UBOC_SEC_AND_DATA_PRIV_INFO;
		ubocDataPrivRetCode.setUbocDataPrivRetCode(MarshalByte.readString(buffer, position, UbocDataPrivRetCode.Len.UBOC_DATA_PRIV_RET_CODE));
		position += UbocDataPrivRetCode.Len.UBOC_DATA_PRIV_RET_CODE;
		ubocSecGroupName = MarshalByte.readString(buffer, position, Len.UBOC_SEC_GROUP_NAME);
		position += Len.UBOC_SEC_GROUP_NAME;
		filler4 = MarshalByte.readString(buffer, position, Len.FILLER4);
		position += Len.FILLER4;
		ubocAuditProcessingInfo.setUbocAuditProcessingInfoBytes(buffer, position);
		position += UbocAuditProcessingInfo.Len.UBOC_AUDIT_PROCESSING_INFO;
		filler5 = MarshalByte.readString(buffer, position, Len.FILLER5);
		position += Len.FILLER5;
		ubocKeepClearUowStorageSw.setUbocKeepClearUowStorageSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		ubocErrorDetails.setUbocErrorDetailsBytes(buffer, position);
	}

	public byte[] getCommInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ubocUowName, Len.UBOC_UOW_NAME);
		position += Len.UBOC_UOW_NAME;
		MarshalByte.writeString(buffer, position, ubocMsgId, Len.UBOC_MSG_ID);
		position += Len.UBOC_MSG_ID;
		MarshalByte.writeString(buffer, position, ubocAuthUserid, Len.UBOC_AUTH_USERID);
		position += Len.UBOC_AUTH_USERID;
		MarshalByte.writeString(buffer, position, ubocAuthUserClientid, Len.UBOC_AUTH_USER_CLIENTID);
		position += Len.UBOC_AUTH_USER_CLIENTID;
		MarshalByte.writeString(buffer, position, ubocTerminalId, Len.UBOC_TERMINAL_ID);
		position += Len.UBOC_TERMINAL_ID;
		MarshalByte.writeString(buffer, position, ubocPrimaryBusObj, Len.UBOC_PRIMARY_BUS_OBJ);
		position += Len.UBOC_PRIMARY_BUS_OBJ;
		MarshalByte.writeChar(buffer, position, ubocStoreTypeCd.getUbocStoreTypeCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ubocMdrReqStore, Len.UBOC_MDR_REQ_STORE);
		position += Len.UBOC_MDR_REQ_STORE;
		MarshalByte.writeString(buffer, position, ubocMdrRspStore, Len.UBOC_MDR_RSP_STORE);
		position += Len.UBOC_MDR_RSP_STORE;
		MarshalByte.writeString(buffer, position, ubocUowReqMsgStore, Len.UBOC_UOW_REQ_MSG_STORE);
		position += Len.UBOC_UOW_REQ_MSG_STORE;
		MarshalByte.writeString(buffer, position, ubocUowReqSwitchesStore, Len.UBOC_UOW_REQ_SWITCHES_STORE);
		position += Len.UBOC_UOW_REQ_SWITCHES_STORE;
		MarshalByte.writeString(buffer, position, ubocUowRespHeaderStore, Len.UBOC_UOW_RESP_HEADER_STORE);
		position += Len.UBOC_UOW_RESP_HEADER_STORE;
		MarshalByte.writeString(buffer, position, ubocUowRespDataStore, Len.UBOC_UOW_RESP_DATA_STORE);
		position += Len.UBOC_UOW_RESP_DATA_STORE;
		MarshalByte.writeString(buffer, position, ubocUowRespWarningsStore, Len.UBOC_UOW_RESP_WARNINGS_STORE);
		position += Len.UBOC_UOW_RESP_WARNINGS_STORE;
		MarshalByte.writeString(buffer, position, ubocUowKeyReplaceStore, Len.UBOC_UOW_KEY_REPLACE_STORE);
		position += Len.UBOC_UOW_KEY_REPLACE_STORE;
		MarshalByte.writeString(buffer, position, ubocUowRespNlBlErrsStore, Len.UBOC_UOW_RESP_NL_BL_ERRS_STORE);
		position += Len.UBOC_UOW_RESP_NL_BL_ERRS_STORE;
		MarshalByte.writeString(buffer, position, ubocSessionId, Len.UBOC_SESSION_ID);
		position += Len.UBOC_SESSION_ID;
		MarshalByte.writeString(buffer, position, ubocUowLockProcTsq, Len.UBOC_UOW_LOCK_PROC_TSQ);
		position += Len.UBOC_UOW_LOCK_PROC_TSQ;
		MarshalByte.writeString(buffer, position, ubocUowReqSwitchesTsq, Len.UBOC_UOW_REQ_SWITCHES_TSQ);
		position += Len.UBOC_UOW_REQ_SWITCHES_TSQ;
		MarshalByte.writeString(buffer, position, ubocPassThruAction.getUbocPassThruAction(), UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION);
		position += UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION;
		MarshalByte.writeString(buffer, position, ubocNbrHdrRows, Len.UBOC_NBR_HDR_ROWS);
		position += Len.UBOC_NBR_HDR_ROWS;
		MarshalByte.writeString(buffer, position, ubocNbrDataRows, Len.UBOC_NBR_DATA_ROWS);
		position += Len.UBOC_NBR_DATA_ROWS;
		MarshalByte.writeString(buffer, position, ubocNbrWarnings, Len.UBOC_NBR_WARNINGS);
		position += Len.UBOC_NBR_WARNINGS;
		MarshalByte.writeString(buffer, position, ubocNbrNonlogBlErrs, Len.UBOC_NBR_NONLOG_BL_ERRS);
		position += Len.UBOC_NBR_NONLOG_BL_ERRS;
		MarshalByte.writeChar(buffer, position, ubocApplicationTsSw.getUbocApplicationTsSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ubocApplicationTs, Len.UBOC_APPLICATION_TS);
		position += Len.UBOC_APPLICATION_TS;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeChar(buffer, position, ubocUowLockStrategyCd.getUbocUowLockStrategyCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeInt(buffer, position, ubocUowLockTimeoutInterval, Len.UBOC_UOW_LOCK_TIMEOUT_INTERVAL);
		position += Len.UBOC_UOW_LOCK_TIMEOUT_INTERVAL;
		MarshalByte.writeChar(buffer, position, ubocUserInLockGrpSw.getUbocUserInLockGrpSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ubocLockGroup, Len.UBOC_LOCK_GROUP);
		position += Len.UBOC_LOCK_GROUP;
		MarshalByte.writeString(buffer, position, filler3, Len.FILLER3);
		position += Len.FILLER3;
		ubocSecAndDataPrivInfo.getUbocSecAndDataPrivInfoBytes(buffer, position);
		position += UbocSecAndDataPrivInfo.Len.UBOC_SEC_AND_DATA_PRIV_INFO;
		MarshalByte.writeString(buffer, position, ubocDataPrivRetCode.getUbocDataPrivRetCode(), UbocDataPrivRetCode.Len.UBOC_DATA_PRIV_RET_CODE);
		position += UbocDataPrivRetCode.Len.UBOC_DATA_PRIV_RET_CODE;
		MarshalByte.writeString(buffer, position, ubocSecGroupName, Len.UBOC_SEC_GROUP_NAME);
		position += Len.UBOC_SEC_GROUP_NAME;
		MarshalByte.writeString(buffer, position, filler4, Len.FILLER4);
		position += Len.FILLER4;
		ubocAuditProcessingInfo.getUbocAuditProcessingInfoBytes(buffer, position);
		position += UbocAuditProcessingInfo.Len.UBOC_AUDIT_PROCESSING_INFO;
		MarshalByte.writeString(buffer, position, filler5, Len.FILLER5);
		position += Len.FILLER5;
		MarshalByte.writeChar(buffer, position, ubocKeepClearUowStorageSw.getUbocKeepClearUowStorageSw());
		position += Types.CHAR_SIZE;
		ubocErrorDetails.getUbocErrorDetailsBytes(buffer, position);
		return buffer;
	}

	public void setUbocUowName(String ubocUowName) {
		this.ubocUowName = Functions.subString(ubocUowName, Len.UBOC_UOW_NAME);
	}

	public String getUbocUowName() {
		return this.ubocUowName;
	}

	public String getUbocUowNameFormatted() {
		return Functions.padBlanks(getUbocUowName(), Len.UBOC_UOW_NAME);
	}

	public void setUbocMsgId(String ubocMsgId) {
		this.ubocMsgId = Functions.subString(ubocMsgId, Len.UBOC_MSG_ID);
	}

	public String getUbocMsgId() {
		return this.ubocMsgId;
	}

	public String getUbocMsgIdFormatted() {
		return Functions.padBlanks(getUbocMsgId(), Len.UBOC_MSG_ID);
	}

	public void setUbocAuthUserid(String ubocAuthUserid) {
		this.ubocAuthUserid = Functions.subString(ubocAuthUserid, Len.UBOC_AUTH_USERID);
	}

	public String getUbocAuthUserid() {
		return this.ubocAuthUserid;
	}

	public String getUbocAuthUseridFormatted() {
		return Functions.padBlanks(getUbocAuthUserid(), Len.UBOC_AUTH_USERID);
	}

	public void setUbocAuthUserClientid(String ubocAuthUserClientid) {
		this.ubocAuthUserClientid = Functions.subString(ubocAuthUserClientid, Len.UBOC_AUTH_USER_CLIENTID);
	}

	public String getUbocAuthUserClientid() {
		return this.ubocAuthUserClientid;
	}

	public String getUbocAuthUserClientidFormatted() {
		return Functions.padBlanks(getUbocAuthUserClientid(), Len.UBOC_AUTH_USER_CLIENTID);
	}

	public void setUbocTerminalId(String ubocTerminalId) {
		this.ubocTerminalId = Functions.subString(ubocTerminalId, Len.UBOC_TERMINAL_ID);
	}

	public String getUbocTerminalId() {
		return this.ubocTerminalId;
	}

	public void setUbocPrimaryBusObj(String ubocPrimaryBusObj) {
		this.ubocPrimaryBusObj = Functions.subString(ubocPrimaryBusObj, Len.UBOC_PRIMARY_BUS_OBJ);
	}

	public String getUbocPrimaryBusObj() {
		return this.ubocPrimaryBusObj;
	}

	public void setUbocMdrReqStore(String ubocMdrReqStore) {
		this.ubocMdrReqStore = Functions.subString(ubocMdrReqStore, Len.UBOC_MDR_REQ_STORE);
	}

	public String getUbocMdrReqStore() {
		return this.ubocMdrReqStore;
	}

	public void setUbocMdrRspStore(String ubocMdrRspStore) {
		this.ubocMdrRspStore = Functions.subString(ubocMdrRspStore, Len.UBOC_MDR_RSP_STORE);
	}

	public String getUbocMdrRspStore() {
		return this.ubocMdrRspStore;
	}

	public void setUbocUowReqMsgStore(String ubocUowReqMsgStore) {
		this.ubocUowReqMsgStore = Functions.subString(ubocUowReqMsgStore, Len.UBOC_UOW_REQ_MSG_STORE);
	}

	public String getUbocUowReqMsgStore() {
		return this.ubocUowReqMsgStore;
	}

	public String getUbocUowReqMsgStoreFormatted() {
		return Functions.padBlanks(getUbocUowReqMsgStore(), Len.UBOC_UOW_REQ_MSG_STORE);
	}

	public void setUbocUowReqSwitchesStore(String ubocUowReqSwitchesStore) {
		this.ubocUowReqSwitchesStore = Functions.subString(ubocUowReqSwitchesStore, Len.UBOC_UOW_REQ_SWITCHES_STORE);
	}

	public String getUbocUowReqSwitchesStore() {
		return this.ubocUowReqSwitchesStore;
	}

	public String getUbocUowReqSwitchesStoreFormatted() {
		return Functions.padBlanks(getUbocUowReqSwitchesStore(), Len.UBOC_UOW_REQ_SWITCHES_STORE);
	}

	public void setUbocUowRespHeaderStore(String ubocUowRespHeaderStore) {
		this.ubocUowRespHeaderStore = Functions.subString(ubocUowRespHeaderStore, Len.UBOC_UOW_RESP_HEADER_STORE);
	}

	public String getUbocUowRespHeaderStore() {
		return this.ubocUowRespHeaderStore;
	}

	public String getUbocUowRespHeaderStoreFormatted() {
		return Functions.padBlanks(getUbocUowRespHeaderStore(), Len.UBOC_UOW_RESP_HEADER_STORE);
	}

	public void setUbocUowRespDataStore(String ubocUowRespDataStore) {
		this.ubocUowRespDataStore = Functions.subString(ubocUowRespDataStore, Len.UBOC_UOW_RESP_DATA_STORE);
	}

	public String getUbocUowRespDataStore() {
		return this.ubocUowRespDataStore;
	}

	public String getUbocUowRespDataStoreFormatted() {
		return Functions.padBlanks(getUbocUowRespDataStore(), Len.UBOC_UOW_RESP_DATA_STORE);
	}

	public void setUbocUowRespWarningsStore(String ubocUowRespWarningsStore) {
		this.ubocUowRespWarningsStore = Functions.subString(ubocUowRespWarningsStore, Len.UBOC_UOW_RESP_WARNINGS_STORE);
	}

	public String getUbocUowRespWarningsStore() {
		return this.ubocUowRespWarningsStore;
	}

	public String getUbocUowRespWarningsStoreFormatted() {
		return Functions.padBlanks(getUbocUowRespWarningsStore(), Len.UBOC_UOW_RESP_WARNINGS_STORE);
	}

	public void setUbocUowKeyReplaceStore(String ubocUowKeyReplaceStore) {
		this.ubocUowKeyReplaceStore = Functions.subString(ubocUowKeyReplaceStore, Len.UBOC_UOW_KEY_REPLACE_STORE);
	}

	public String getUbocUowKeyReplaceStore() {
		return this.ubocUowKeyReplaceStore;
	}

	public String getUbocUowKeyReplaceStoreFormatted() {
		return Functions.padBlanks(getUbocUowKeyReplaceStore(), Len.UBOC_UOW_KEY_REPLACE_STORE);
	}

	public void setUbocUowRespNlBlErrsStore(String ubocUowRespNlBlErrsStore) {
		this.ubocUowRespNlBlErrsStore = Functions.subString(ubocUowRespNlBlErrsStore, Len.UBOC_UOW_RESP_NL_BL_ERRS_STORE);
	}

	public String getUbocUowRespNlBlErrsStore() {
		return this.ubocUowRespNlBlErrsStore;
	}

	public String getUbocUowRespNlBlErrsStoreFormatted() {
		return Functions.padBlanks(getUbocUowRespNlBlErrsStore(), Len.UBOC_UOW_RESP_NL_BL_ERRS_STORE);
	}

	public void setUbocSessionId(String ubocSessionId) {
		this.ubocSessionId = Functions.subString(ubocSessionId, Len.UBOC_SESSION_ID);
	}

	public String getUbocSessionId() {
		return this.ubocSessionId;
	}

	public void setUbocUowLockProcTsq(String ubocUowLockProcTsq) {
		this.ubocUowLockProcTsq = Functions.subString(ubocUowLockProcTsq, Len.UBOC_UOW_LOCK_PROC_TSQ);
	}

	public String getUbocUowLockProcTsq() {
		return this.ubocUowLockProcTsq;
	}

	public String getUbocUowLockProcTsqFormatted() {
		return Functions.padBlanks(getUbocUowLockProcTsq(), Len.UBOC_UOW_LOCK_PROC_TSQ);
	}

	public void setUbocUowReqSwitchesTsq(String ubocUowReqSwitchesTsq) {
		this.ubocUowReqSwitchesTsq = Functions.subString(ubocUowReqSwitchesTsq, Len.UBOC_UOW_REQ_SWITCHES_TSQ);
	}

	public String getUbocUowReqSwitchesTsq() {
		return this.ubocUowReqSwitchesTsq;
	}

	public String getUbocUowReqSwitchesTsqFormatted() {
		return Functions.padBlanks(getUbocUowReqSwitchesTsq(), Len.UBOC_UOW_REQ_SWITCHES_TSQ);
	}

	public void setUbocNbrHdrRows(int ubocNbrHdrRows) {
		this.ubocNbrHdrRows = NumericDisplay.asString(ubocNbrHdrRows, Len.UBOC_NBR_HDR_ROWS);
	}

	public void setUbocNbrHdrRowsFormatted(String ubocNbrHdrRows) {
		this.ubocNbrHdrRows = Trunc.toUnsignedNumeric(ubocNbrHdrRows, Len.UBOC_NBR_HDR_ROWS);
	}

	public int getUbocNbrHdrRows() {
		return NumericDisplay.asInt(this.ubocNbrHdrRows);
	}

	public String getUbocNbrHdrRowsFormatted() {
		return this.ubocNbrHdrRows;
	}

	public void setUbocNbrDataRows(int ubocNbrDataRows) {
		this.ubocNbrDataRows = NumericDisplay.asString(ubocNbrDataRows, Len.UBOC_NBR_DATA_ROWS);
	}

	public void setUbocNbrDataRowsFormatted(String ubocNbrDataRows) {
		this.ubocNbrDataRows = Trunc.toUnsignedNumeric(ubocNbrDataRows, Len.UBOC_NBR_DATA_ROWS);
	}

	public int getUbocNbrDataRows() {
		return NumericDisplay.asInt(this.ubocNbrDataRows);
	}

	public String getUbocNbrDataRowsFormatted() {
		return this.ubocNbrDataRows;
	}

	public void setUbocNbrWarnings(int ubocNbrWarnings) {
		this.ubocNbrWarnings = NumericDisplay.asString(ubocNbrWarnings, Len.UBOC_NBR_WARNINGS);
	}

	public void setUbocNbrWarningsFormatted(String ubocNbrWarnings) {
		this.ubocNbrWarnings = Trunc.toUnsignedNumeric(ubocNbrWarnings, Len.UBOC_NBR_WARNINGS);
	}

	public int getUbocNbrWarnings() {
		return NumericDisplay.asInt(this.ubocNbrWarnings);
	}

	public String getUbocNbrWarningsFormatted() {
		return this.ubocNbrWarnings;
	}

	public void setUbocNbrNonlogBlErrs(int ubocNbrNonlogBlErrs) {
		this.ubocNbrNonlogBlErrs = NumericDisplay.asString(ubocNbrNonlogBlErrs, Len.UBOC_NBR_NONLOG_BL_ERRS);
	}

	public void setUbocNbrNonlogBlErrsFormatted(String ubocNbrNonlogBlErrs) {
		this.ubocNbrNonlogBlErrs = Trunc.toUnsignedNumeric(ubocNbrNonlogBlErrs, Len.UBOC_NBR_NONLOG_BL_ERRS);
	}

	public int getUbocNbrNonlogBlErrs() {
		return NumericDisplay.asInt(this.ubocNbrNonlogBlErrs);
	}

	public String getUbocNbrNonlogBlErrsFormatted() {
		return this.ubocNbrNonlogBlErrs;
	}

	public void setUbocApplicationTs(String ubocApplicationTs) {
		this.ubocApplicationTs = Functions.subString(ubocApplicationTs, Len.UBOC_APPLICATION_TS);
	}

	public String getUbocApplicationTs() {
		return this.ubocApplicationTs;
	}

	public String getUbocApplicationTsFormatted() {
		return Functions.padBlanks(getUbocApplicationTs(), Len.UBOC_APPLICATION_TS);
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setUbocUowLockTimeoutInterval(int ubocUowLockTimeoutInterval) {
		this.ubocUowLockTimeoutInterval = ubocUowLockTimeoutInterval;
	}

	public int getUbocUowLockTimeoutInterval() {
		return this.ubocUowLockTimeoutInterval;
	}

	public String getUbocUowLockTimeoutIntervalFormatted() {
		return NumericDisplaySigned.asString(getUbocUowLockTimeoutInterval(), Len.UBOC_UOW_LOCK_TIMEOUT_INTERVAL);
	}

	public String getUbocUowLockTimeoutIntervalAsString() {
		return getUbocUowLockTimeoutIntervalFormatted();
	}

	public void setUbocLockGroup(String ubocLockGroup) {
		this.ubocLockGroup = Functions.subString(ubocLockGroup, Len.UBOC_LOCK_GROUP);
	}

	public String getUbocLockGroup() {
		return this.ubocLockGroup;
	}

	public void setFiller3(String filler3) {
		this.filler3 = Functions.subString(filler3, Len.FILLER3);
	}

	public String getFiller3() {
		return this.filler3;
	}

	public void setUbocSecGroupName(String ubocSecGroupName) {
		this.ubocSecGroupName = Functions.subString(ubocSecGroupName, Len.UBOC_SEC_GROUP_NAME);
	}

	public String getUbocSecGroupName() {
		return this.ubocSecGroupName;
	}

	public void setFiller4(String filler4) {
		this.filler4 = Functions.subString(filler4, Len.FILLER4);
	}

	public String getFiller4() {
		return this.filler4;
	}

	public void setFiller5(String filler5) {
		this.filler5 = Functions.subString(filler5, Len.FILLER5);
	}

	public String getFiller5() {
		return this.filler5;
	}

	public UbocApplicationTsSw getUbocApplicationTsSw() {
		return ubocApplicationTsSw;
	}

	public UbocAuditProcessingInfo getUbocAuditProcessingInfo() {
		return ubocAuditProcessingInfo;
	}

	public UbocDataPrivRetCode getUbocDataPrivRetCode() {
		return ubocDataPrivRetCode;
	}

	public UbocErrorDetails getUbocErrorDetails() {
		return ubocErrorDetails;
	}

	public UbocKeepClearUowStorageSw getUbocKeepClearUowStorageSw() {
		return ubocKeepClearUowStorageSw;
	}

	public UbocPassThruAction getUbocPassThruAction() {
		return ubocPassThruAction;
	}

	public UbocSecAndDataPrivInfo getUbocSecAndDataPrivInfo() {
		return ubocSecAndDataPrivInfo;
	}

	public UbocStoreTypeCd getUbocStoreTypeCd() {
		return ubocStoreTypeCd;
	}

	public UbocUowLockStrategyCd getUbocUowLockStrategyCd() {
		return ubocUowLockStrategyCd;
	}

	public UbocUserInLockGrpSw getUbocUserInLockGrpSw() {
		return ubocUserInLockGrpSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_UOW_NAME = 32;
		public static final int UBOC_MSG_ID = 32;
		public static final int UBOC_AUTH_USERID = 32;
		public static final int UBOC_AUTH_USER_CLIENTID = 20;
		public static final int UBOC_TERMINAL_ID = 8;
		public static final int UBOC_PRIMARY_BUS_OBJ = 32;
		public static final int UBOC_MDR_REQ_STORE = 32;
		public static final int UBOC_MDR_RSP_STORE = 32;
		public static final int UBOC_UOW_REQ_MSG_STORE = 32;
		public static final int UBOC_UOW_REQ_SWITCHES_STORE = 32;
		public static final int UBOC_UOW_RESP_HEADER_STORE = 32;
		public static final int UBOC_UOW_RESP_DATA_STORE = 32;
		public static final int UBOC_UOW_RESP_WARNINGS_STORE = 32;
		public static final int UBOC_UOW_KEY_REPLACE_STORE = 32;
		public static final int UBOC_UOW_RESP_NL_BL_ERRS_STORE = 32;
		public static final int UBOC_SESSION_ID = 32;
		public static final int UBOC_UOW_LOCK_PROC_TSQ = 16;
		public static final int UBOC_UOW_REQ_SWITCHES_TSQ = 16;
		public static final int UBOC_NBR_HDR_ROWS = 9;
		public static final int UBOC_NBR_DATA_ROWS = 9;
		public static final int UBOC_NBR_WARNINGS = 9;
		public static final int UBOC_NBR_NONLOG_BL_ERRS = 9;
		public static final int UBOC_APPLICATION_TS = 26;
		public static final int FLR1 = 3;
		public static final int UBOC_UOW_LOCK_TIMEOUT_INTERVAL = 9;
		public static final int UBOC_LOCK_GROUP = 8;
		public static final int FILLER3 = 11;
		public static final int UBOC_SEC_GROUP_NAME = 8;
		public static final int FILLER4 = 12;
		public static final int FILLER5 = 19;
		public static final int COMM_INFO = UBOC_UOW_NAME + UBOC_MSG_ID + UBOC_AUTH_USERID + UBOC_AUTH_USER_CLIENTID + UBOC_TERMINAL_ID
				+ UBOC_PRIMARY_BUS_OBJ + UbocStoreTypeCd.Len.UBOC_STORE_TYPE_CD + UBOC_MDR_REQ_STORE + UBOC_MDR_RSP_STORE + UBOC_UOW_REQ_MSG_STORE
				+ UBOC_UOW_REQ_SWITCHES_STORE + UBOC_UOW_RESP_HEADER_STORE + UBOC_UOW_RESP_DATA_STORE + UBOC_UOW_RESP_WARNINGS_STORE
				+ UBOC_UOW_KEY_REPLACE_STORE + UBOC_UOW_RESP_NL_BL_ERRS_STORE + UBOC_SESSION_ID + UBOC_UOW_LOCK_PROC_TSQ + UBOC_UOW_REQ_SWITCHES_TSQ
				+ UbocPassThruAction.Len.UBOC_PASS_THRU_ACTION + UBOC_NBR_HDR_ROWS + UBOC_NBR_DATA_ROWS + UBOC_NBR_WARNINGS + UBOC_NBR_NONLOG_BL_ERRS
				+ UbocApplicationTsSw.Len.UBOC_APPLICATION_TS_SW + UBOC_APPLICATION_TS + UbocUowLockStrategyCd.Len.UBOC_UOW_LOCK_STRATEGY_CD
				+ UBOC_UOW_LOCK_TIMEOUT_INTERVAL + UbocUserInLockGrpSw.Len.UBOC_USER_IN_LOCK_GRP_SW + UBOC_LOCK_GROUP + FILLER3
				+ UbocSecAndDataPrivInfo.Len.UBOC_SEC_AND_DATA_PRIV_INFO + UbocDataPrivRetCode.Len.UBOC_DATA_PRIV_RET_CODE + UBOC_SEC_GROUP_NAME
				+ FILLER4 + UbocAuditProcessingInfo.Len.UBOC_AUDIT_PROCESSING_INFO + FILLER5
				+ UbocKeepClearUowStorageSw.Len.UBOC_KEEP_CLEAR_UOW_STORAGE_SW + UbocErrorDetails.Len.UBOC_ERROR_DETAILS + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
