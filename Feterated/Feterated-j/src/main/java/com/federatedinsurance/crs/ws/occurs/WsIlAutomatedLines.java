/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-IL-AUTOMATED-LINES<br>
 * Variables: WS-IL-AUTOMATED-LINES from program XZ0P9000<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsIlAutomatedLines {

	//==== PROPERTIES ====
	//Original name: WS-IL-AL-INS-LIN-CD
	private String wsIlAlInsLinCd = DefaultValues.stringVal(Len.WS_IL_AL_INS_LIN_CD);

	//==== METHODS ====
	public void setWsIlAlInsLinCd(String wsIlAlInsLinCd) {
		this.wsIlAlInsLinCd = Functions.subString(wsIlAlInsLinCd, Len.WS_IL_AL_INS_LIN_CD);
	}

	public String getWsIlAlInsLinCd() {
		return this.wsIlAlInsLinCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_IL_AL_INS_LIN_CD = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
