/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X0008<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x0008 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x0008() {
	}

	public LFrameworkRequestAreaXz0x0008(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzc002ActNotPolRowFormatted(String data) {
		writeString(Pos.XZC002Q_ACT_NOT_POL_ROW, data, Len.XZC002Q_ACT_NOT_POL_ROW);
	}

	public String getXzc002ActNotPolRowFormatted() {
		return readFixedString(Pos.XZC002Q_ACT_NOT_POL_ROW, Len.XZC002Q_ACT_NOT_POL_ROW);
	}

	public void setXzc002qActNotPolCsumFormatted(String xzc002qActNotPolCsum) {
		writeString(Pos.XZC002Q_ACT_NOT_POL_CSUM, Trunc.toUnsignedNumeric(xzc002qActNotPolCsum, Len.XZC002Q_ACT_NOT_POL_CSUM),
				Len.XZC002Q_ACT_NOT_POL_CSUM);
	}

	/**Original name: XZC002Q-ACT-NOT-POL-CSUM<br>*/
	public int getXzc002qActNotPolCsum() {
		return readNumDispUnsignedInt(Pos.XZC002Q_ACT_NOT_POL_CSUM, Len.XZC002Q_ACT_NOT_POL_CSUM);
	}

	public String getXzc002rActNotPolCsumFormatted() {
		return readFixedString(Pos.XZC002Q_ACT_NOT_POL_CSUM, Len.XZC002Q_ACT_NOT_POL_CSUM);
	}

	public void setXzc002qCsrActNbrKcre(String xzc002qCsrActNbrKcre) {
		writeString(Pos.XZC002Q_CSR_ACT_NBR_KCRE, xzc002qCsrActNbrKcre, Len.XZC002Q_CSR_ACT_NBR_KCRE);
	}

	/**Original name: XZC002Q-CSR-ACT-NBR-KCRE<br>*/
	public String getXzc002qCsrActNbrKcre() {
		return readString(Pos.XZC002Q_CSR_ACT_NBR_KCRE, Len.XZC002Q_CSR_ACT_NBR_KCRE);
	}

	public void setXzc002qNotPrcTsKcre(String xzc002qNotPrcTsKcre) {
		writeString(Pos.XZC002Q_NOT_PRC_TS_KCRE, xzc002qNotPrcTsKcre, Len.XZC002Q_NOT_PRC_TS_KCRE);
	}

	/**Original name: XZC002Q-NOT-PRC-TS-KCRE<br>*/
	public String getXzc002qNotPrcTsKcre() {
		return readString(Pos.XZC002Q_NOT_PRC_TS_KCRE, Len.XZC002Q_NOT_PRC_TS_KCRE);
	}

	public void setXzc002qPolNbrKcre(String xzc002qPolNbrKcre) {
		writeString(Pos.XZC002Q_POL_NBR_KCRE, xzc002qPolNbrKcre, Len.XZC002Q_POL_NBR_KCRE);
	}

	/**Original name: XZC002Q-POL-NBR-KCRE<br>*/
	public String getXzc002qPolNbrKcre() {
		return readString(Pos.XZC002Q_POL_NBR_KCRE, Len.XZC002Q_POL_NBR_KCRE);
	}

	public void setXzc002qTransProcessDt(String xzc002qTransProcessDt) {
		writeString(Pos.XZC002Q_TRANS_PROCESS_DT, xzc002qTransProcessDt, Len.XZC002Q_TRANS_PROCESS_DT);
	}

	/**Original name: XZC002Q-TRANS-PROCESS-DT<br>*/
	public String getXzc002qTransProcessDt() {
		return readString(Pos.XZC002Q_TRANS_PROCESS_DT, Len.XZC002Q_TRANS_PROCESS_DT);
	}

	public void setXzc002qCsrActNbr(String xzc002qCsrActNbr) {
		writeString(Pos.XZC002Q_CSR_ACT_NBR, xzc002qCsrActNbr, Len.XZC002Q_CSR_ACT_NBR);
	}

	/**Original name: XZC002Q-CSR-ACT-NBR<br>*/
	public String getXzc002qCsrActNbr() {
		return readString(Pos.XZC002Q_CSR_ACT_NBR, Len.XZC002Q_CSR_ACT_NBR);
	}

	public void setXzc002qNotPrcTs(String xzc002qNotPrcTs) {
		writeString(Pos.XZC002Q_NOT_PRC_TS, xzc002qNotPrcTs, Len.XZC002Q_NOT_PRC_TS);
	}

	/**Original name: XZC002Q-NOT-PRC-TS<br>*/
	public String getXzc002qNotPrcTs() {
		return readString(Pos.XZC002Q_NOT_PRC_TS, Len.XZC002Q_NOT_PRC_TS);
	}

	public void setXzc002qPolNbr(String xzc002qPolNbr) {
		writeString(Pos.XZC002Q_POL_NBR, xzc002qPolNbr, Len.XZC002Q_POL_NBR);
	}

	/**Original name: XZC002Q-POL-NBR<br>*/
	public String getXzc002qPolNbr() {
		return readString(Pos.XZC002Q_POL_NBR, Len.XZC002Q_POL_NBR);
	}

	public void setXzc002qCsrActNbrCi(char xzc002qCsrActNbrCi) {
		writeChar(Pos.XZC002Q_CSR_ACT_NBR_CI, xzc002qCsrActNbrCi);
	}

	public void setXzc002qCsrActNbrCiFormatted(String xzc002qCsrActNbrCi) {
		setXzc002qCsrActNbrCi(Functions.charAt(xzc002qCsrActNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC002Q-CSR-ACT-NBR-CI<br>*/
	public char getXzc002qCsrActNbrCi() {
		return readChar(Pos.XZC002Q_CSR_ACT_NBR_CI);
	}

	public void setXzc002qNotPrcTsCi(char xzc002qNotPrcTsCi) {
		writeChar(Pos.XZC002Q_NOT_PRC_TS_CI, xzc002qNotPrcTsCi);
	}

	public void setXzc002qNotPrcTsCiFormatted(String xzc002qNotPrcTsCi) {
		setXzc002qNotPrcTsCi(Functions.charAt(xzc002qNotPrcTsCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC002Q-NOT-PRC-TS-CI<br>*/
	public char getXzc002qNotPrcTsCi() {
		return readChar(Pos.XZC002Q_NOT_PRC_TS_CI);
	}

	public void setXzc002qPolNbrCi(char xzc002qPolNbrCi) {
		writeChar(Pos.XZC002Q_POL_NBR_CI, xzc002qPolNbrCi);
	}

	public void setXzc002qPolNbrCiFormatted(String xzc002qPolNbrCi) {
		setXzc002qPolNbrCi(Functions.charAt(xzc002qPolNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC002Q-POL-NBR-CI<br>*/
	public char getXzc002qPolNbrCi() {
		return readChar(Pos.XZC002Q_POL_NBR_CI);
	}

	public void setXzc002qPolTypCdCi(char xzc002qPolTypCdCi) {
		writeChar(Pos.XZC002Q_POL_TYP_CD_CI, xzc002qPolTypCdCi);
	}

	/**Original name: XZC002Q-POL-TYP-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getXzc002qPolTypCdCi() {
		return readChar(Pos.XZC002Q_POL_TYP_CD_CI);
	}

	public void setXzc002qPolTypCd(String xzc002qPolTypCd) {
		writeString(Pos.XZC002Q_POL_TYP_CD, xzc002qPolTypCd, Len.XZC002Q_POL_TYP_CD);
	}

	/**Original name: XZC002Q-POL-TYP-CD<br>*/
	public String getXzc002qPolTypCd() {
		return readString(Pos.XZC002Q_POL_TYP_CD, Len.XZC002Q_POL_TYP_CD);
	}

	public void setXzc002qPolPriRskStAbbCi(char xzc002qPolPriRskStAbbCi) {
		writeChar(Pos.XZC002Q_POL_PRI_RSK_ST_ABB_CI, xzc002qPolPriRskStAbbCi);
	}

	/**Original name: XZC002Q-POL-PRI-RSK-ST-ABB-CI<br>*/
	public char getXzc002qPolPriRskStAbbCi() {
		return readChar(Pos.XZC002Q_POL_PRI_RSK_ST_ABB_CI);
	}

	public void setXzc002qPolPriRskStAbb(String xzc002qPolPriRskStAbb) {
		writeString(Pos.XZC002Q_POL_PRI_RSK_ST_ABB, xzc002qPolPriRskStAbb, Len.XZC002Q_POL_PRI_RSK_ST_ABB);
	}

	/**Original name: XZC002Q-POL-PRI-RSK-ST-ABB<br>*/
	public String getXzc002qPolPriRskStAbb() {
		return readString(Pos.XZC002Q_POL_PRI_RSK_ST_ABB, Len.XZC002Q_POL_PRI_RSK_ST_ABB);
	}

	public void setXzc002qNotEffDtCi(char xzc002qNotEffDtCi) {
		writeChar(Pos.XZC002Q_NOT_EFF_DT_CI, xzc002qNotEffDtCi);
	}

	/**Original name: XZC002Q-NOT-EFF-DT-CI<br>*/
	public char getXzc002qNotEffDtCi() {
		return readChar(Pos.XZC002Q_NOT_EFF_DT_CI);
	}

	public void setXzc002qNotEffDtNi(char xzc002qNotEffDtNi) {
		writeChar(Pos.XZC002Q_NOT_EFF_DT_NI, xzc002qNotEffDtNi);
	}

	/**Original name: XZC002Q-NOT-EFF-DT-NI<br>*/
	public char getXzc002qNotEffDtNi() {
		return readChar(Pos.XZC002Q_NOT_EFF_DT_NI);
	}

	public void setXzc002qNotEffDt(String xzc002qNotEffDt) {
		writeString(Pos.XZC002Q_NOT_EFF_DT, xzc002qNotEffDt, Len.XZC002Q_NOT_EFF_DT);
	}

	/**Original name: XZC002Q-NOT-EFF-DT<br>*/
	public String getXzc002qNotEffDt() {
		return readString(Pos.XZC002Q_NOT_EFF_DT, Len.XZC002Q_NOT_EFF_DT);
	}

	public void setXzc002qPolEffDtCi(char xzc002qPolEffDtCi) {
		writeChar(Pos.XZC002Q_POL_EFF_DT_CI, xzc002qPolEffDtCi);
	}

	/**Original name: XZC002Q-POL-EFF-DT-CI<br>*/
	public char getXzc002qPolEffDtCi() {
		return readChar(Pos.XZC002Q_POL_EFF_DT_CI);
	}

	public void setXzc002qPolEffDt(String xzc002qPolEffDt) {
		writeString(Pos.XZC002Q_POL_EFF_DT, xzc002qPolEffDt, Len.XZC002Q_POL_EFF_DT);
	}

	/**Original name: XZC002Q-POL-EFF-DT<br>*/
	public String getXzc002qPolEffDt() {
		return readString(Pos.XZC002Q_POL_EFF_DT, Len.XZC002Q_POL_EFF_DT);
	}

	public void setXzc002qPolExpDtCi(char xzc002qPolExpDtCi) {
		writeChar(Pos.XZC002Q_POL_EXP_DT_CI, xzc002qPolExpDtCi);
	}

	/**Original name: XZC002Q-POL-EXP-DT-CI<br>*/
	public char getXzc002qPolExpDtCi() {
		return readChar(Pos.XZC002Q_POL_EXP_DT_CI);
	}

	public void setXzc002qPolExpDt(String xzc002qPolExpDt) {
		writeString(Pos.XZC002Q_POL_EXP_DT, xzc002qPolExpDt, Len.XZC002Q_POL_EXP_DT);
	}

	/**Original name: XZC002Q-POL-EXP-DT<br>*/
	public String getXzc002qPolExpDt() {
		return readString(Pos.XZC002Q_POL_EXP_DT, Len.XZC002Q_POL_EXP_DT);
	}

	public void setXzc002qPolDueAmtCi(char xzc002qPolDueAmtCi) {
		writeChar(Pos.XZC002Q_POL_DUE_AMT_CI, xzc002qPolDueAmtCi);
	}

	/**Original name: XZC002Q-POL-DUE-AMT-CI<br>*/
	public char getXzc002qPolDueAmtCi() {
		return readChar(Pos.XZC002Q_POL_DUE_AMT_CI);
	}

	public void setXzc002qPolDueAmtNi(char xzc002qPolDueAmtNi) {
		writeChar(Pos.XZC002Q_POL_DUE_AMT_NI, xzc002qPolDueAmtNi);
	}

	/**Original name: XZC002Q-POL-DUE-AMT-NI<br>*/
	public char getXzc002qPolDueAmtNi() {
		return readChar(Pos.XZC002Q_POL_DUE_AMT_NI);
	}

	public void setXzc002qPolDueAmtSign(char xzc002qPolDueAmtSign) {
		writeChar(Pos.XZC002Q_POL_DUE_AMT_SIGN, xzc002qPolDueAmtSign);
	}

	/**Original name: XZC002Q-POL-DUE-AMT-SIGN<br>*/
	public char getXzc002qPolDueAmtSign() {
		return readChar(Pos.XZC002Q_POL_DUE_AMT_SIGN);
	}

	public void setXzc002qPolDueAmt(AfDecimal xzc002qPolDueAmt) {
		writeDecimal(Pos.XZC002Q_POL_DUE_AMT, xzc002qPolDueAmt.copy(), SignType.NO_SIGN);
	}

	/**Original name: XZC002Q-POL-DUE-AMT<br>*/
	public AfDecimal getXzc002qPolDueAmt() {
		return readDecimal(Pos.XZC002Q_POL_DUE_AMT, Len.Int.XZC002Q_POL_DUE_AMT, Len.Fract.XZC002Q_POL_DUE_AMT, SignType.NO_SIGN);
	}

	public void setXzc002qNinCltIdCi(char xzc002qNinCltIdCi) {
		writeChar(Pos.XZC002Q_NIN_CLT_ID_CI, xzc002qNinCltIdCi);
	}

	/**Original name: XZC002Q-NIN-CLT-ID-CI<br>*/
	public char getXzc002qNinCltIdCi() {
		return readChar(Pos.XZC002Q_NIN_CLT_ID_CI);
	}

	public void setXzc002qNinCltId(String xzc002qNinCltId) {
		writeString(Pos.XZC002Q_NIN_CLT_ID, xzc002qNinCltId, Len.XZC002Q_NIN_CLT_ID);
	}

	/**Original name: XZC002Q-NIN-CLT-ID<br>*/
	public String getXzc002qNinCltId() {
		return readString(Pos.XZC002Q_NIN_CLT_ID, Len.XZC002Q_NIN_CLT_ID);
	}

	public void setXzc002qNinAdrIdCi(char xzc002qNinAdrIdCi) {
		writeChar(Pos.XZC002Q_NIN_ADR_ID_CI, xzc002qNinAdrIdCi);
	}

	/**Original name: XZC002Q-NIN-ADR-ID-CI<br>*/
	public char getXzc002qNinAdrIdCi() {
		return readChar(Pos.XZC002Q_NIN_ADR_ID_CI);
	}

	public void setXzc002qNinAdrId(String xzc002qNinAdrId) {
		writeString(Pos.XZC002Q_NIN_ADR_ID, xzc002qNinAdrId, Len.XZC002Q_NIN_ADR_ID);
	}

	/**Original name: XZC002Q-NIN-ADR-ID<br>*/
	public String getXzc002qNinAdrId() {
		return readString(Pos.XZC002Q_NIN_ADR_ID, Len.XZC002Q_NIN_ADR_ID);
	}

	public void setXzc002qWfStartedIndCi(char xzc002qWfStartedIndCi) {
		writeChar(Pos.XZC002Q_WF_STARTED_IND_CI, xzc002qWfStartedIndCi);
	}

	/**Original name: XZC002Q-WF-STARTED-IND-CI<br>*/
	public char getXzc002qWfStartedIndCi() {
		return readChar(Pos.XZC002Q_WF_STARTED_IND_CI);
	}

	public void setXzc002qWfStartedIndNi(char xzc002qWfStartedIndNi) {
		writeChar(Pos.XZC002Q_WF_STARTED_IND_NI, xzc002qWfStartedIndNi);
	}

	/**Original name: XZC002Q-WF-STARTED-IND-NI<br>*/
	public char getXzc002qWfStartedIndNi() {
		return readChar(Pos.XZC002Q_WF_STARTED_IND_NI);
	}

	public void setXzc002qWfStartedInd(char xzc002qWfStartedInd) {
		writeChar(Pos.XZC002Q_WF_STARTED_IND, xzc002qWfStartedInd);
	}

	/**Original name: XZC002Q-WF-STARTED-IND<br>*/
	public char getXzc002qWfStartedInd() {
		return readChar(Pos.XZC002Q_WF_STARTED_IND);
	}

	public void setXzc002qPolBilStaCdCi(char xzc002qPolBilStaCdCi) {
		writeChar(Pos.XZC002Q_POL_BIL_STA_CD_CI, xzc002qPolBilStaCdCi);
	}

	/**Original name: XZC002Q-POL-BIL-STA-CD-CI<br>*/
	public char getXzc002qPolBilStaCdCi() {
		return readChar(Pos.XZC002Q_POL_BIL_STA_CD_CI);
	}

	public void setXzc002qPolBilStaCdNi(char xzc002qPolBilStaCdNi) {
		writeChar(Pos.XZC002Q_POL_BIL_STA_CD_NI, xzc002qPolBilStaCdNi);
	}

	/**Original name: XZC002Q-POL-BIL-STA-CD-NI<br>*/
	public char getXzc002qPolBilStaCdNi() {
		return readChar(Pos.XZC002Q_POL_BIL_STA_CD_NI);
	}

	public void setXzc002qPolBilStaCd(char xzc002qPolBilStaCd) {
		writeChar(Pos.XZC002Q_POL_BIL_STA_CD, xzc002qPolBilStaCd);
	}

	/**Original name: XZC002Q-POL-BIL-STA-CD<br>*/
	public char getXzc002qPolBilStaCd() {
		return readChar(Pos.XZC002Q_POL_BIL_STA_CD);
	}

	public void setXzc002qPolTypDes(String xzc002qPolTypDes) {
		writeString(Pos.XZC002Q_POL_TYP_DES, xzc002qPolTypDes, Len.XZC002Q_POL_TYP_DES);
	}

	/**Original name: XZC002Q-POL-TYP-DES<br>*/
	public String getXzc002qPolTypDes() {
		return readString(Pos.XZC002Q_POL_TYP_DES, Len.XZC002Q_POL_TYP_DES);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_ACT_NOT_POL = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZC002Q_ACT_NOT_POL_ROW = L_FW_REQ_ACT_NOT_POL;
		public static final int XZC002Q_ACT_NOT_POL_FIXED = XZC002Q_ACT_NOT_POL_ROW;
		public static final int XZC002Q_ACT_NOT_POL_CSUM = XZC002Q_ACT_NOT_POL_FIXED;
		public static final int XZC002Q_CSR_ACT_NBR_KCRE = XZC002Q_ACT_NOT_POL_CSUM + Len.XZC002Q_ACT_NOT_POL_CSUM;
		public static final int XZC002Q_NOT_PRC_TS_KCRE = XZC002Q_CSR_ACT_NBR_KCRE + Len.XZC002Q_CSR_ACT_NBR_KCRE;
		public static final int XZC002Q_POL_NBR_KCRE = XZC002Q_NOT_PRC_TS_KCRE + Len.XZC002Q_NOT_PRC_TS_KCRE;
		public static final int XZC002Q_ACT_NOT_POL_DATES = XZC002Q_POL_NBR_KCRE + Len.XZC002Q_POL_NBR_KCRE;
		public static final int XZC002Q_TRANS_PROCESS_DT = XZC002Q_ACT_NOT_POL_DATES;
		public static final int XZC002Q_ACT_NOT_POL_KEY = XZC002Q_TRANS_PROCESS_DT + Len.XZC002Q_TRANS_PROCESS_DT;
		public static final int XZC002Q_CSR_ACT_NBR = XZC002Q_ACT_NOT_POL_KEY;
		public static final int XZC002Q_NOT_PRC_TS = XZC002Q_CSR_ACT_NBR + Len.XZC002Q_CSR_ACT_NBR;
		public static final int XZC002Q_POL_NBR = XZC002Q_NOT_PRC_TS + Len.XZC002Q_NOT_PRC_TS;
		public static final int XZC002Q_ACT_NOT_POL_KEY_CI = XZC002Q_POL_NBR + Len.XZC002Q_POL_NBR;
		public static final int XZC002Q_CSR_ACT_NBR_CI = XZC002Q_ACT_NOT_POL_KEY_CI;
		public static final int XZC002Q_NOT_PRC_TS_CI = XZC002Q_CSR_ACT_NBR_CI + Len.XZC002Q_CSR_ACT_NBR_CI;
		public static final int XZC002Q_POL_NBR_CI = XZC002Q_NOT_PRC_TS_CI + Len.XZC002Q_NOT_PRC_TS_CI;
		public static final int XZC002Q_ACT_NOT_POL_DATA = XZC002Q_POL_NBR_CI + Len.XZC002Q_POL_NBR_CI;
		public static final int XZC002Q_POL_TYP_CD_CI = XZC002Q_ACT_NOT_POL_DATA;
		public static final int XZC002Q_POL_TYP_CD = XZC002Q_POL_TYP_CD_CI + Len.XZC002Q_POL_TYP_CD_CI;
		public static final int XZC002Q_POL_PRI_RSK_ST_ABB_CI = XZC002Q_POL_TYP_CD + Len.XZC002Q_POL_TYP_CD;
		public static final int XZC002Q_POL_PRI_RSK_ST_ABB = XZC002Q_POL_PRI_RSK_ST_ABB_CI + Len.XZC002Q_POL_PRI_RSK_ST_ABB_CI;
		public static final int XZC002Q_NOT_EFF_DT_CI = XZC002Q_POL_PRI_RSK_ST_ABB + Len.XZC002Q_POL_PRI_RSK_ST_ABB;
		public static final int XZC002Q_NOT_EFF_DT_NI = XZC002Q_NOT_EFF_DT_CI + Len.XZC002Q_NOT_EFF_DT_CI;
		public static final int XZC002Q_NOT_EFF_DT = XZC002Q_NOT_EFF_DT_NI + Len.XZC002Q_NOT_EFF_DT_NI;
		public static final int XZC002Q_POL_EFF_DT_CI = XZC002Q_NOT_EFF_DT + Len.XZC002Q_NOT_EFF_DT;
		public static final int XZC002Q_POL_EFF_DT = XZC002Q_POL_EFF_DT_CI + Len.XZC002Q_POL_EFF_DT_CI;
		public static final int XZC002Q_POL_EXP_DT_CI = XZC002Q_POL_EFF_DT + Len.XZC002Q_POL_EFF_DT;
		public static final int XZC002Q_POL_EXP_DT = XZC002Q_POL_EXP_DT_CI + Len.XZC002Q_POL_EXP_DT_CI;
		public static final int XZC002Q_POL_DUE_AMT_CI = XZC002Q_POL_EXP_DT + Len.XZC002Q_POL_EXP_DT;
		public static final int XZC002Q_POL_DUE_AMT_NI = XZC002Q_POL_DUE_AMT_CI + Len.XZC002Q_POL_DUE_AMT_CI;
		public static final int XZC002Q_POL_DUE_AMT_SIGN = XZC002Q_POL_DUE_AMT_NI + Len.XZC002Q_POL_DUE_AMT_NI;
		public static final int XZC002Q_POL_DUE_AMT = XZC002Q_POL_DUE_AMT_SIGN + Len.XZC002Q_POL_DUE_AMT_SIGN;
		public static final int XZC002Q_NIN_CLT_ID_CI = XZC002Q_POL_DUE_AMT + Len.XZC002Q_POL_DUE_AMT;
		public static final int XZC002Q_NIN_CLT_ID = XZC002Q_NIN_CLT_ID_CI + Len.XZC002Q_NIN_CLT_ID_CI;
		public static final int XZC002Q_NIN_ADR_ID_CI = XZC002Q_NIN_CLT_ID + Len.XZC002Q_NIN_CLT_ID;
		public static final int XZC002Q_NIN_ADR_ID = XZC002Q_NIN_ADR_ID_CI + Len.XZC002Q_NIN_ADR_ID_CI;
		public static final int XZC002Q_WF_STARTED_IND_CI = XZC002Q_NIN_ADR_ID + Len.XZC002Q_NIN_ADR_ID;
		public static final int XZC002Q_WF_STARTED_IND_NI = XZC002Q_WF_STARTED_IND_CI + Len.XZC002Q_WF_STARTED_IND_CI;
		public static final int XZC002Q_WF_STARTED_IND = XZC002Q_WF_STARTED_IND_NI + Len.XZC002Q_WF_STARTED_IND_NI;
		public static final int XZC002Q_POL_BIL_STA_CD_CI = XZC002Q_WF_STARTED_IND + Len.XZC002Q_WF_STARTED_IND;
		public static final int XZC002Q_POL_BIL_STA_CD_NI = XZC002Q_POL_BIL_STA_CD_CI + Len.XZC002Q_POL_BIL_STA_CD_CI;
		public static final int XZC002Q_POL_BIL_STA_CD = XZC002Q_POL_BIL_STA_CD_NI + Len.XZC002Q_POL_BIL_STA_CD_NI;
		public static final int XZC002Q_EXTENSION_FIELDS = XZC002Q_POL_BIL_STA_CD + Len.XZC002Q_POL_BIL_STA_CD;
		public static final int XZC002Q_POL_TYP_DES = XZC002Q_EXTENSION_FIELDS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC002Q_ACT_NOT_POL_CSUM = 9;
		public static final int XZC002Q_CSR_ACT_NBR_KCRE = 32;
		public static final int XZC002Q_NOT_PRC_TS_KCRE = 32;
		public static final int XZC002Q_POL_NBR_KCRE = 32;
		public static final int XZC002Q_TRANS_PROCESS_DT = 10;
		public static final int XZC002Q_CSR_ACT_NBR = 9;
		public static final int XZC002Q_NOT_PRC_TS = 26;
		public static final int XZC002Q_POL_NBR = 25;
		public static final int XZC002Q_CSR_ACT_NBR_CI = 1;
		public static final int XZC002Q_NOT_PRC_TS_CI = 1;
		public static final int XZC002Q_POL_NBR_CI = 1;
		public static final int XZC002Q_POL_TYP_CD_CI = 1;
		public static final int XZC002Q_POL_TYP_CD = 3;
		public static final int XZC002Q_POL_PRI_RSK_ST_ABB_CI = 1;
		public static final int XZC002Q_POL_PRI_RSK_ST_ABB = 2;
		public static final int XZC002Q_NOT_EFF_DT_CI = 1;
		public static final int XZC002Q_NOT_EFF_DT_NI = 1;
		public static final int XZC002Q_NOT_EFF_DT = 10;
		public static final int XZC002Q_POL_EFF_DT_CI = 1;
		public static final int XZC002Q_POL_EFF_DT = 10;
		public static final int XZC002Q_POL_EXP_DT_CI = 1;
		public static final int XZC002Q_POL_EXP_DT = 10;
		public static final int XZC002Q_POL_DUE_AMT_CI = 1;
		public static final int XZC002Q_POL_DUE_AMT_NI = 1;
		public static final int XZC002Q_POL_DUE_AMT_SIGN = 1;
		public static final int XZC002Q_POL_DUE_AMT = 10;
		public static final int XZC002Q_NIN_CLT_ID_CI = 1;
		public static final int XZC002Q_NIN_CLT_ID = 64;
		public static final int XZC002Q_NIN_ADR_ID_CI = 1;
		public static final int XZC002Q_NIN_ADR_ID = 64;
		public static final int XZC002Q_WF_STARTED_IND_CI = 1;
		public static final int XZC002Q_WF_STARTED_IND_NI = 1;
		public static final int XZC002Q_WF_STARTED_IND = 1;
		public static final int XZC002Q_POL_BIL_STA_CD_CI = 1;
		public static final int XZC002Q_POL_BIL_STA_CD_NI = 1;
		public static final int XZC002Q_POL_BIL_STA_CD = 1;
		public static final int XZC002Q_ACT_NOT_POL_FIXED = XZC002Q_ACT_NOT_POL_CSUM + XZC002Q_CSR_ACT_NBR_KCRE + XZC002Q_NOT_PRC_TS_KCRE
				+ XZC002Q_POL_NBR_KCRE;
		public static final int XZC002Q_ACT_NOT_POL_DATES = XZC002Q_TRANS_PROCESS_DT;
		public static final int XZC002Q_ACT_NOT_POL_KEY = XZC002Q_CSR_ACT_NBR + XZC002Q_NOT_PRC_TS + XZC002Q_POL_NBR;
		public static final int XZC002Q_ACT_NOT_POL_KEY_CI = XZC002Q_CSR_ACT_NBR_CI + XZC002Q_NOT_PRC_TS_CI + XZC002Q_POL_NBR_CI;
		public static final int XZC002Q_ACT_NOT_POL_DATA = XZC002Q_POL_TYP_CD_CI + XZC002Q_POL_TYP_CD + XZC002Q_POL_PRI_RSK_ST_ABB_CI
				+ XZC002Q_POL_PRI_RSK_ST_ABB + XZC002Q_NOT_EFF_DT_CI + XZC002Q_NOT_EFF_DT_NI + XZC002Q_NOT_EFF_DT + XZC002Q_POL_EFF_DT_CI
				+ XZC002Q_POL_EFF_DT + XZC002Q_POL_EXP_DT_CI + XZC002Q_POL_EXP_DT + XZC002Q_POL_DUE_AMT_CI + XZC002Q_POL_DUE_AMT_NI
				+ XZC002Q_POL_DUE_AMT_SIGN + XZC002Q_POL_DUE_AMT + XZC002Q_NIN_CLT_ID_CI + XZC002Q_NIN_CLT_ID + XZC002Q_NIN_ADR_ID_CI
				+ XZC002Q_NIN_ADR_ID + XZC002Q_WF_STARTED_IND_CI + XZC002Q_WF_STARTED_IND_NI + XZC002Q_WF_STARTED_IND + XZC002Q_POL_BIL_STA_CD_CI
				+ XZC002Q_POL_BIL_STA_CD_NI + XZC002Q_POL_BIL_STA_CD;
		public static final int XZC002Q_POL_TYP_DES = 30;
		public static final int XZC002Q_EXTENSION_FIELDS = XZC002Q_POL_TYP_DES;
		public static final int XZC002Q_ACT_NOT_POL_ROW = XZC002Q_ACT_NOT_POL_FIXED + XZC002Q_ACT_NOT_POL_DATES + XZC002Q_ACT_NOT_POL_KEY
				+ XZC002Q_ACT_NOT_POL_KEY_CI + XZC002Q_ACT_NOT_POL_DATA + XZC002Q_EXTENSION_FIELDS;
		public static final int L_FW_REQ_ACT_NOT_POL = XZC002Q_ACT_NOT_POL_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_ACT_NOT_POL;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZC002Q_POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZC002Q_POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
