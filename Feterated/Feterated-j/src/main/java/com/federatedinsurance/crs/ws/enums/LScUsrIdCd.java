/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;

/**Original name: L-SC-USR-ID-CD<br>
 * Variable: L-SC-USR-ID-CD from copybook TT008001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LScUsrIdCd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SGL_USR_ID_CD = 'S';
	public static final char ALL_USR_ID_CD = 'A';
	public static final char N_A_USR_ID_CD = Types.SPACE_CHAR;

	//==== METHODS ====
	public void setUsrIdCd(char usrIdCd) {
		this.value = usrIdCd;
	}

	public char getUsrIdCd() {
		return this.value;
	}

	public void setSglUsrIdCd() {
		value = SGL_USR_ID_CD;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int USR_ID_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
