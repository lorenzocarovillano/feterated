/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalBoAudTgrV;

/**Original name: DCLHAL-BO-AUD-TGR-V<br>
 * Variable: DCLHAL-BO-AUD-TGR-V from copybook HALLGBAT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalBoAudTgrV implements IHalBoAudTgrV {

	//==== PROPERTIES ====
	//Original name: HBAT-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HBAT-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: HBAT-TGR-IND
	private char tgrInd = DefaultValues.CHAR_VAL;
	//Original name: HBAT-MODULE-NM
	private String moduleNm = DefaultValues.stringVal(Len.MODULE_NM);

	//==== METHODS ====
	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public String getUowNmFormatted() {
		return Functions.padBlanks(getUowNm(), Len.UOW_NM);
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public String getBusObjNmFormatted() {
		return Functions.padBlanks(getBusObjNm(), Len.BUS_OBJ_NM);
	}

	@Override
	public void setTgrInd(char tgrInd) {
		this.tgrInd = tgrInd;
	}

	@Override
	public char getTgrInd() {
		return this.tgrInd;
	}

	@Override
	public void setModuleNm(String moduleNm) {
		this.moduleNm = Functions.subString(moduleNm, Len.MODULE_NM);
	}

	@Override
	public String getModuleNm() {
		return this.moduleNm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NM = 32;
		public static final int BUS_OBJ_NM = 32;
		public static final int MODULE_NM = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
