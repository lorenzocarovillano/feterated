/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-04-DB2-NOT-ACTIVE<br>
 * Variable: EA-04-DB2-NOT-ACTIVE from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea04Db2NotActive {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-04-DB2-NOT-ACTIVE
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-04-DB2-NOT-ACTIVE-1
	private String flr2 = "TS030099 -";
	//Original name: FILLER-EA-04-DB2-NOT-ACTIVE-2
	private String flr3 = "ACCESSING";
	//Original name: FILLER-EA-04-DB2-NOT-ACTIVE-3
	private String flr4 = "REPORT NUMBER";
	//Original name: FILLER-EA-04-DB2-NOT-ACTIVE-4
	private char flr5 = Types.QUOTE_CHAR;
	//Original name: EA-04-REPORT-NUMBER
	private String ea04ReportNumber = DefaultValues.stringVal(Len.EA04_REPORT_NUMBER);
	//Original name: FILLER-EA-04-DB2-NOT-ACTIVE-5
	private char flr6 = Types.QUOTE_CHAR;
	//Original name: FILLER-EA-04-DB2-NOT-ACTIVE-6
	private String flr7 = " BUT DB2 IS NOT";
	//Original name: FILLER-EA-04-DB2-NOT-ACTIVE-7
	private String flr8 = " ACTIVE.";

	//==== METHODS ====
	public String getEa04Db2NotActiveFormatted() {
		return MarshalByteExt.bufferToStr(getEa04Db2NotActiveBytes());
	}

	public byte[] getEa04Db2NotActiveBytes() {
		byte[] buffer = new byte[Len.EA04_DB2_NOT_ACTIVE];
		return getEa04Db2NotActiveBytes(buffer, 1);
	}

	public byte[] getEa04Db2NotActiveBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeChar(buffer, position, flr5);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ea04ReportNumber, Len.EA04_REPORT_NUMBER);
		position += Len.EA04_REPORT_NUMBER;
		MarshalByte.writeChar(buffer, position, flr6);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public char getFlr5() {
		return this.flr5;
	}

	public void setEa04ReportNumber(String ea04ReportNumber) {
		this.ea04ReportNumber = Functions.subString(ea04ReportNumber, Len.EA04_REPORT_NUMBER);
	}

	public String getEa04ReportNumber() {
		return this.ea04ReportNumber;
	}

	public char getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA04_REPORT_NUMBER = 6;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 10;
		public static final int FLR4 = 14;
		public static final int FLR7 = 15;
		public static final int FLR8 = 8;
		public static final int EA04_DB2_NOT_ACTIVE = EA04_REPORT_NUMBER + 3 * FLR1 + FLR2 + FLR3 + FLR4 + FLR7 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
