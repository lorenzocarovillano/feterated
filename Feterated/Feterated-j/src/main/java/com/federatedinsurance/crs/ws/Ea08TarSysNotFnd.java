/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-08-TAR-SYS-NOT-FND<br>
 * Variable: EA-08-TAR-SYS-NOT-FND from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea08TarSysNotFnd {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-08-TAR-SYS-NOT-FND
	private String flr1 = "TARGET SYSTEM";
	//Original name: FILLER-EA-08-TAR-SYS-NOT-FND-1
	private String flr2 = "NOT FOUND";
	//Original name: FILLER-EA-08-TAR-SYS-NOT-FND-2
	private String flr3 = "FOR CICS";
	//Original name: FILLER-EA-08-TAR-SYS-NOT-FND-3
	private String flr4 = "RGN:";
	//Original name: EA-08-CICS-RGN
	private String ea08CicsRgn = DefaultValues.stringVal(Len.EA08_CICS_RGN);

	//==== METHODS ====
	public String getEa08TarSysNotFndFormatted() {
		return MarshalByteExt.bufferToStr(getEa08TarSysNotFndBytes());
	}

	public byte[] getEa08TarSysNotFndBytes() {
		byte[] buffer = new byte[Len.EA08_TAR_SYS_NOT_FND];
		return getEa08TarSysNotFndBytes(buffer, 1);
	}

	public byte[] getEa08TarSysNotFndBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, ea08CicsRgn, Len.EA08_CICS_RGN);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setEa08CicsRgn(String ea08CicsRgn) {
		this.ea08CicsRgn = Functions.subString(ea08CicsRgn, Len.EA08_CICS_RGN);
	}

	public String getEa08CicsRgn() {
		return this.ea08CicsRgn;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA08_CICS_RGN = 8;
		public static final int FLR1 = 14;
		public static final int FLR2 = 10;
		public static final int FLR3 = 9;
		public static final int FLR4 = 5;
		public static final int EA08_TAR_SYS_NOT_FND = EA08_CICS_RGN + FLR1 + FLR2 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
