/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-DSY-PRT-GRP<br>
 * Variable: CF-DSY-PRT-GRP from program XZ0B9060<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class CfDsyPrtGrp {

	//==== PROPERTIES ====
	//Original name: CF-DSY-PRT-GRP-1
	private String grp1 = "06:30";
	//Original name: CF-DSY-PRT-GRP-2
	private String grp2 = "07:30";
	//Original name: CF-DSY-PRT-GRP-3
	private String grp3 = "08:30";
	//Original name: CF-DSY-PRT-GRP-4
	private String grp4 = "09:30";
	//Original name: CF-DSY-PRT-GRP-5
	private String grp5 = "10:30";
	//Original name: CF-DSY-PRT-GRP-6
	private String grp6 = "11:30";
	//Original name: CF-DSY-PRT-GRP-7
	private String grp7 = "12:30";

	//==== METHODS ====
	public String getGrp1() {
		return this.grp1;
	}

	public String getGrp2() {
		return this.grp2;
	}

	public String getGrp3() {
		return this.grp3;
	}

	public String getGrp4() {
		return this.grp4;
	}

	public String getGrp5() {
		return this.grp5;
	}

	public String getGrp6() {
		return this.grp6;
	}

	public String getGrp7() {
		return this.grp7;
	}
}
