/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalErrTranSupV;

/**Original name: DCLHALERT-ERR-TRAN-SUP-V<br>
 * Variable: DCLHALERT-ERR-TRAN-SUP-V from copybook HALLGETS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalertErrTranSupV implements IHalErrTranSupV {

	//==== PROPERTIES ====
	//Original name: HETS-FAIL-ACY-CD
	private String hetsFailAcyCd = DefaultValues.stringVal(Len.HETS_FAIL_ACY_CD);
	//Original name: APP-NM
	private String appNm = DefaultValues.stringVal(Len.APP_NM);
	//Original name: HETS-ERR-ATN-CD
	private String hetsErrAtnCd = DefaultValues.stringVal(Len.HETS_ERR_ATN_CD);
	//Original name: HETS-TYP-ERR
	private String hetsTypErr = DefaultValues.stringVal(Len.HETS_TYP_ERR);
	//Original name: HETS-ERR-PTY-NBR
	private short hetsErrPtyNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: HETS-ERR-TXT
	private String hetsErrTxt = DefaultValues.stringVal(Len.HETS_ERR_TXT);

	//==== METHODS ====
	public void setHetsFailAcyCd(String hetsFailAcyCd) {
		this.hetsFailAcyCd = Functions.subString(hetsFailAcyCd, Len.HETS_FAIL_ACY_CD);
	}

	public String getHetsFailAcyCd() {
		return this.hetsFailAcyCd;
	}

	public void setAppNm(String appNm) {
		this.appNm = Functions.subString(appNm, Len.APP_NM);
	}

	public String getAppNm() {
		return this.appNm;
	}

	public void setHetsErrAtnCd(String hetsErrAtnCd) {
		this.hetsErrAtnCd = Functions.subString(hetsErrAtnCd, Len.HETS_ERR_ATN_CD);
	}

	public String getHetsErrAtnCd() {
		return this.hetsErrAtnCd;
	}

	public void setHetsTypErr(String hetsTypErr) {
		this.hetsTypErr = Functions.subString(hetsTypErr, Len.HETS_TYP_ERR);
	}

	public String getHetsTypErr() {
		return this.hetsTypErr;
	}

	public void setHetsErrPtyNbr(short hetsErrPtyNbr) {
		this.hetsErrPtyNbr = hetsErrPtyNbr;
	}

	public short getHetsErrPtyNbr() {
		return this.hetsErrPtyNbr;
	}

	public void setHetsErrTxt(String hetsErrTxt) {
		this.hetsErrTxt = Functions.subString(hetsErrTxt, Len.HETS_ERR_TXT);
	}

	public String getHetsErrTxt() {
		return this.hetsErrTxt;
	}

	@Override
	public short getPtyNbr() {
		return getHetsErrPtyNbr();
	}

	@Override
	public void setPtyNbr(short ptyNbr) {
		this.setHetsErrPtyNbr(ptyNbr);
	}

	@Override
	public String getTxt() {
		return getHetsErrTxt();
	}

	@Override
	public void setTxt(String txt) {
		this.setHetsErrTxt(txt);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HETS_ERR_TXT = 100;
		public static final int HETS_FAIL_ACY_CD = 8;
		public static final int APP_NM = 10;
		public static final int HETS_ERR_ATN_CD = 30;
		public static final int HETS_TYP_ERR = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
