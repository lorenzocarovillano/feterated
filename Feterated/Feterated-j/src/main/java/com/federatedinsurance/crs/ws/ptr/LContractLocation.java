/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-CONTRACT-LOCATION<br>
 * Variable: L-CONTRACT-LOCATION from copybook TSC21PRO<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LContractLocation extends BytesClass {

	//==== CONSTRUCTORS ====
	public LContractLocation() {
	}

	public LContractLocation(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_CONTRACT_LOCATION;
	}

	public void setlContractLocationFromBuffer(byte[] buffer, int offset) {
		setByte(Pos.L_CONTRACT_LOCATION, buffer[offset - 1]);
	}

	public void setlContractLocationFromBuffer(byte[] buffer) {
		setlContractLocationFromBuffer(buffer, 1);
	}

	public byte[] getlContractLocationAsBuffer(byte[] buffer, int offset) {
		buffer[offset - 1] = getByte(Pos.L_CONTRACT_LOCATION);
		return buffer;
	}

	public byte[] getlContractLocationAsBuffer() {
		byte[] buffer = new byte[Types.CHAR_SIZE];
		return getlContractLocationAsBuffer(buffer, 1);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_CONTRACT_LOCATION = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int L_CONTRACT_LOCATION = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
