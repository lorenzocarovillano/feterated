/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.Types;

/**Original name: COM-OUTGOING-COL-IND-VAL<br>
 * Variable: COM-OUTGOING-COL-IND-VAL from copybook HALLCOM<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ComOutgoingColIndVal {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char IS_UPDATABLE_COL_IND = Types.SPACE_CHAR;
	public static final char IS_ENCRYPTED_COL_IND = 'E';
	public static final char IS_ALTERED_COL_IND = 'A';
	public static final char IS_DEFAULTED_COL_IND = 'D';
	public static final char IS_REQUIRED_COL_IND = 'Q';
	public static final char IS_VIEWONLY_COL_IND = 'V';
	public static final char IS_EXCLUDED_COL_IND = 'X';
	public static final char IS_ENCRYPTED_RON_COL_IND = 'F';
	public static final char IS_ALTERED_RON_COL_IND = 'B';
	public static final char IS_DEFAULTED_RON_COL_IND = 'C';
	private static final char[] OUTGOING_RETAINABLE_CI_VAL = new char[] { 'V', 'X', 'F', 'B', 'C' };

	//==== METHODS ====
	public void setOutgoingColIndVal(char outgoingColIndVal) {
		this.value = outgoingColIndVal;
	}

	public char getOutgoingColIndVal() {
		return this.value;
	}

	public boolean isComIsRequiredColInd() {
		return value == IS_REQUIRED_COL_IND;
	}

	public void setComIsViewonlyColInd() {
		value = IS_VIEWONLY_COL_IND;
	}

	public boolean isComOutgoingRetainableCiVal() {
		return ArrayUtils.contains(OUTGOING_RETAINABLE_CI_VAL, value);
	}
}
