/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZY022-CNC-POL-LIST<br>
 * Variables: XZY022-CNC-POL-LIST from copybook XZ0Y0022<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzy022CncPolList {

	//==== PROPERTIES ====
	//Original name: XZY022-CP-POL-NBR
	private String xzy022CpPolNbr = DefaultValues.stringVal(Len.XZY022_CP_POL_NBR);

	//==== METHODS ====
	public void setCncPolListBytes(byte[] buffer, int offset) {
		int position = offset;
		xzy022CpPolNbr = MarshalByte.readString(buffer, position, Len.XZY022_CP_POL_NBR);
	}

	public byte[] getCncPolListBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzy022CpPolNbr, Len.XZY022_CP_POL_NBR);
		return buffer;
	}

	public void initCncPolListSpaces() {
		xzy022CpPolNbr = "";
	}

	public void setXzy022CpPolNbr(String xzy022CpPolNbr) {
		this.xzy022CpPolNbr = Functions.subString(xzy022CpPolNbr, Len.XZY022_CP_POL_NBR);
	}

	public String getXzy022CpPolNbr() {
		return this.xzy022CpPolNbr;
	}

	public String getXzy022CpPolNbrFormatted() {
		return Functions.padBlanks(getXzy022CpPolNbr(), Len.XZY022_CP_POL_NBR);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY022_CP_POL_NBR = 25;
		public static final int CNC_POL_LIST = XZY022_CP_POL_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
