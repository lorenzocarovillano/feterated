/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SC-CRT1-TYP<br>
 * Variable: L-SC-CRT1-TYP from copybook TT008001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LScCrt1Typ {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.CRT1_TYP);
	public static final String NONE = "00";
	public static final String EMP_ID = "01";
	public static final String USR_ID = "02";
	public static final String DPT_CD = "03";
	public static final String ACT_CD = "04";
	public static final String SPV_EMP_ID = "05";
	public static final String MAL_CD = "06";
	public static final String PHN = "07";
	public static final String NM = "08";
	public static final String TER_ID = "09";

	//==== METHODS ====
	public void setCrt1Typ(String crt1Typ) {
		this.value = Functions.subString(crt1Typ, Len.CRT1_TYP);
	}

	public String getCrt1Typ() {
		return this.value;
	}

	public void setEmpId() {
		value = EMP_ID;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CRT1_TYP = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
