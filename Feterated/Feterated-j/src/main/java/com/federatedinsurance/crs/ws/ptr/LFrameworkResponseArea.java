/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program MU0X0004<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseArea extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_CLT_ADR_COMPOSITE_MAXOCCURS = 2;
	public static final int L_FW_RESP_CLIENT_PHONE_MAXOCCURS = 5;
	public static final int L_FW_RESP_CLT_OBJ_RELATION_MAXOCCURS = 50;
	public static final int L_FW_RESP_FED_BUSINESS_TYP_MAXOCCURS = 6;
	public static final int L_FW_RESP_CLT_CLT_REL_MAXOCCURS = 7;
	public static final char CWCACR_MORE_THAN_ONE_ROW = 'Y';
	public static final char CWCACR_NOT_MORE_THAN_ONE_ROW = 'N';
	public static final char CWEMR_MORE_THAN_ONE_ROW = 'Y';
	public static final char CWEMR_NOT_MORE_THAN_ONE_ROW = 'N';
	public static final char CW04R_MORE_THAN_ONE_ROW = 'Y';
	public static final char CW04R_NOT_MORE_THAN_ONE_ROW = 'N';
	public static final char CW11R_MORE_THAN_ONE_ROW = 'Y';
	public static final char CW11R_NOT_MORE_THAN_ONE_ROW = 'N';
	public static final char CW27R_MORE_THAN_ONE_ROW = 'Y';
	public static final char CW27R_NOT_MORE_THAN_ONE_ROW = 'N';

	//==== CONSTRUCTORS ====
	public LFrameworkResponseArea() {
	}

	public LFrameworkResponseArea(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setCw02rClientTabRowFormatted(String data) {
		writeString(Pos.CW02R_CLIENT_TAB_ROW, data, Len.CW02R_CLIENT_TAB_ROW);
	}

	public void setCw02rClientTabChkSumFormatted(String cw02rClientTabChkSum) {
		writeString(Pos.CW02R_CLIENT_TAB_CHK_SUM, Trunc.toUnsignedNumeric(cw02rClientTabChkSum, Len.CW02R_CLIENT_TAB_CHK_SUM),
				Len.CW02R_CLIENT_TAB_CHK_SUM);
	}

	/**Original name: CW02R-CLIENT-TAB-CHK-SUM<br>*/
	public int getCw02rClientTabChkSum() {
		return readNumDispUnsignedInt(Pos.CW02R_CLIENT_TAB_CHK_SUM, Len.CW02R_CLIENT_TAB_CHK_SUM);
	}

	public void setCw02rClientIdKcre(String cw02rClientIdKcre) {
		writeString(Pos.CW02R_CLIENT_ID_KCRE, cw02rClientIdKcre, Len.CW02R_CLIENT_ID_KCRE);
	}

	/**Original name: CW02R-CLIENT-ID-KCRE<br>*/
	public String getCw02rClientIdKcre() {
		return readString(Pos.CW02R_CLIENT_ID_KCRE, Len.CW02R_CLIENT_ID_KCRE);
	}

	public void setCw02rTransProcessDt(String cw02rTransProcessDt) {
		writeString(Pos.CW02R_TRANS_PROCESS_DT, cw02rTransProcessDt, Len.CW02R_TRANS_PROCESS_DT);
	}

	/**Original name: CW02R-TRANS-PROCESS-DT<br>*/
	public String getCw02rTransProcessDt() {
		return readString(Pos.CW02R_TRANS_PROCESS_DT, Len.CW02R_TRANS_PROCESS_DT);
	}

	public void setCw02rClientIdCi(char cw02rClientIdCi) {
		writeChar(Pos.CW02R_CLIENT_ID_CI, cw02rClientIdCi);
	}

	/**Original name: CW02R-CLIENT-ID-CI<br>*/
	public char getCw02rClientIdCi() {
		return readChar(Pos.CW02R_CLIENT_ID_CI);
	}

	public void setCw02rClientId(String cw02rClientId) {
		writeString(Pos.CW02R_CLIENT_ID, cw02rClientId, Len.CW02R_CLIENT_ID);
	}

	/**Original name: CW02R-CLIENT-ID<br>*/
	public String getCw02rClientId() {
		return readString(Pos.CW02R_CLIENT_ID, Len.CW02R_CLIENT_ID);
	}

	public void setCw02rHistoryVldNbrCi(char cw02rHistoryVldNbrCi) {
		writeChar(Pos.CW02R_HISTORY_VLD_NBR_CI, cw02rHistoryVldNbrCi);
	}

	/**Original name: CW02R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw02rHistoryVldNbrCi() {
		return readChar(Pos.CW02R_HISTORY_VLD_NBR_CI);
	}

	public void setCw02rHistoryVldNbrSign(char cw02rHistoryVldNbrSign) {
		writeChar(Pos.CW02R_HISTORY_VLD_NBR_SIGN, cw02rHistoryVldNbrSign);
	}

	/**Original name: CW02R-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw02rHistoryVldNbrSign() {
		return readChar(Pos.CW02R_HISTORY_VLD_NBR_SIGN);
	}

	public void setCw02rHistoryVldNbrFormatted(String cw02rHistoryVldNbr) {
		writeString(Pos.CW02R_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cw02rHistoryVldNbr, Len.CW02R_HISTORY_VLD_NBR), Len.CW02R_HISTORY_VLD_NBR);
	}

	/**Original name: CW02R-HISTORY-VLD-NBR<br>*/
	public int getCw02rHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CW02R_HISTORY_VLD_NBR, Len.CW02R_HISTORY_VLD_NBR);
	}

	public void setCw02rCiclEffDtCi(char cw02rCiclEffDtCi) {
		writeChar(Pos.CW02R_CICL_EFF_DT_CI, cw02rCiclEffDtCi);
	}

	/**Original name: CW02R-CICL-EFF-DT-CI<br>*/
	public char getCw02rCiclEffDtCi() {
		return readChar(Pos.CW02R_CICL_EFF_DT_CI);
	}

	public void setCw02rCiclEffDt(String cw02rCiclEffDt) {
		writeString(Pos.CW02R_CICL_EFF_DT, cw02rCiclEffDt, Len.CW02R_CICL_EFF_DT);
	}

	/**Original name: CW02R-CICL-EFF-DT<br>*/
	public String getCw02rCiclEffDt() {
		return readString(Pos.CW02R_CICL_EFF_DT, Len.CW02R_CICL_EFF_DT);
	}

	public void setCw02rCiclPriSubCdCi(char cw02rCiclPriSubCdCi) {
		writeChar(Pos.CW02R_CICL_PRI_SUB_CD_CI, cw02rCiclPriSubCdCi);
	}

	/**Original name: CW02R-CICL-PRI-SUB-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw02rCiclPriSubCdCi() {
		return readChar(Pos.CW02R_CICL_PRI_SUB_CD_CI);
	}

	public void setCw02rCiclPriSubCd(String cw02rCiclPriSubCd) {
		writeString(Pos.CW02R_CICL_PRI_SUB_CD, cw02rCiclPriSubCd, Len.CW02R_CICL_PRI_SUB_CD);
	}

	/**Original name: CW02R-CICL-PRI-SUB-CD<br>*/
	public String getCw02rCiclPriSubCd() {
		return readString(Pos.CW02R_CICL_PRI_SUB_CD, Len.CW02R_CICL_PRI_SUB_CD);
	}

	public void setCw02rCiclFstNmCi(char cw02rCiclFstNmCi) {
		writeChar(Pos.CW02R_CICL_FST_NM_CI, cw02rCiclFstNmCi);
	}

	/**Original name: CW02R-CICL-FST-NM-CI<br>*/
	public char getCw02rCiclFstNmCi() {
		return readChar(Pos.CW02R_CICL_FST_NM_CI);
	}

	public void setCw02rCiclFstNm(String cw02rCiclFstNm) {
		writeString(Pos.CW02R_CICL_FST_NM, cw02rCiclFstNm, Len.CW02R_CICL_FST_NM);
	}

	/**Original name: CW02R-CICL-FST-NM<br>*/
	public String getCw02rCiclFstNm() {
		return readString(Pos.CW02R_CICL_FST_NM, Len.CW02R_CICL_FST_NM);
	}

	public void setCw02rCiclLstNmCi(char cw02rCiclLstNmCi) {
		writeChar(Pos.CW02R_CICL_LST_NM_CI, cw02rCiclLstNmCi);
	}

	/**Original name: CW02R-CICL-LST-NM-CI<br>*/
	public char getCw02rCiclLstNmCi() {
		return readChar(Pos.CW02R_CICL_LST_NM_CI);
	}

	public void setCw02rCiclLstNm(String cw02rCiclLstNm) {
		writeString(Pos.CW02R_CICL_LST_NM, cw02rCiclLstNm, Len.CW02R_CICL_LST_NM);
	}

	/**Original name: CW02R-CICL-LST-NM<br>*/
	public String getCw02rCiclLstNm() {
		return readString(Pos.CW02R_CICL_LST_NM, Len.CW02R_CICL_LST_NM);
	}

	public void setCw02rCiclMdlNmCi(char cw02rCiclMdlNmCi) {
		writeChar(Pos.CW02R_CICL_MDL_NM_CI, cw02rCiclMdlNmCi);
	}

	/**Original name: CW02R-CICL-MDL-NM-CI<br>*/
	public char getCw02rCiclMdlNmCi() {
		return readChar(Pos.CW02R_CICL_MDL_NM_CI);
	}

	public void setCw02rCiclMdlNm(String cw02rCiclMdlNm) {
		writeString(Pos.CW02R_CICL_MDL_NM, cw02rCiclMdlNm, Len.CW02R_CICL_MDL_NM);
	}

	/**Original name: CW02R-CICL-MDL-NM<br>*/
	public String getCw02rCiclMdlNm() {
		return readString(Pos.CW02R_CICL_MDL_NM, Len.CW02R_CICL_MDL_NM);
	}

	public void setCw02rNmPfxCi(char cw02rNmPfxCi) {
		writeChar(Pos.CW02R_NM_PFX_CI, cw02rNmPfxCi);
	}

	/**Original name: CW02R-NM-PFX-CI<br>*/
	public char getCw02rNmPfxCi() {
		return readChar(Pos.CW02R_NM_PFX_CI);
	}

	public void setCw02rNmPfx(String cw02rNmPfx) {
		writeString(Pos.CW02R_NM_PFX, cw02rNmPfx, Len.CW02R_NM_PFX);
	}

	/**Original name: CW02R-NM-PFX<br>*/
	public String getCw02rNmPfx() {
		return readString(Pos.CW02R_NM_PFX, Len.CW02R_NM_PFX);
	}

	public void setCw02rNmSfxCi(char cw02rNmSfxCi) {
		writeChar(Pos.CW02R_NM_SFX_CI, cw02rNmSfxCi);
	}

	/**Original name: CW02R-NM-SFX-CI<br>*/
	public char getCw02rNmSfxCi() {
		return readChar(Pos.CW02R_NM_SFX_CI);
	}

	public void setCw02rNmSfx(String cw02rNmSfx) {
		writeString(Pos.CW02R_NM_SFX, cw02rNmSfx, Len.CW02R_NM_SFX);
	}

	/**Original name: CW02R-NM-SFX<br>*/
	public String getCw02rNmSfx() {
		return readString(Pos.CW02R_NM_SFX, Len.CW02R_NM_SFX);
	}

	public void setCw02rPrimaryProDsnCdCi(char cw02rPrimaryProDsnCdCi) {
		writeChar(Pos.CW02R_PRIMARY_PRO_DSN_CD_CI, cw02rPrimaryProDsnCdCi);
	}

	/**Original name: CW02R-PRIMARY-PRO-DSN-CD-CI<br>*/
	public char getCw02rPrimaryProDsnCdCi() {
		return readChar(Pos.CW02R_PRIMARY_PRO_DSN_CD_CI);
	}

	public void setCw02rPrimaryProDsnCdNi(char cw02rPrimaryProDsnCdNi) {
		writeChar(Pos.CW02R_PRIMARY_PRO_DSN_CD_NI, cw02rPrimaryProDsnCdNi);
	}

	/**Original name: CW02R-PRIMARY-PRO-DSN-CD-NI<br>*/
	public char getCw02rPrimaryProDsnCdNi() {
		return readChar(Pos.CW02R_PRIMARY_PRO_DSN_CD_NI);
	}

	public void setCw02rPrimaryProDsnCd(String cw02rPrimaryProDsnCd) {
		writeString(Pos.CW02R_PRIMARY_PRO_DSN_CD, cw02rPrimaryProDsnCd, Len.CW02R_PRIMARY_PRO_DSN_CD);
	}

	/**Original name: CW02R-PRIMARY-PRO-DSN-CD<br>*/
	public String getCw02rPrimaryProDsnCd() {
		return readString(Pos.CW02R_PRIMARY_PRO_DSN_CD, Len.CW02R_PRIMARY_PRO_DSN_CD);
	}

	public void setCw02rLegEntCdCi(char cw02rLegEntCdCi) {
		writeChar(Pos.CW02R_LEG_ENT_CD_CI, cw02rLegEntCdCi);
	}

	/**Original name: CW02R-LEG-ENT-CD-CI<br>*/
	public char getCw02rLegEntCdCi() {
		return readChar(Pos.CW02R_LEG_ENT_CD_CI);
	}

	public void setCw02rLegEntCd(String cw02rLegEntCd) {
		writeString(Pos.CW02R_LEG_ENT_CD, cw02rLegEntCd, Len.CW02R_LEG_ENT_CD);
	}

	/**Original name: CW02R-LEG-ENT-CD<br>*/
	public String getCw02rLegEntCd() {
		return readString(Pos.CW02R_LEG_ENT_CD, Len.CW02R_LEG_ENT_CD);
	}

	public void setCw02rCiclSdxCdCi(char cw02rCiclSdxCdCi) {
		writeChar(Pos.CW02R_CICL_SDX_CD_CI, cw02rCiclSdxCdCi);
	}

	/**Original name: CW02R-CICL-SDX-CD-CI<br>*/
	public char getCw02rCiclSdxCdCi() {
		return readChar(Pos.CW02R_CICL_SDX_CD_CI);
	}

	public void setCw02rCiclSdxCd(String cw02rCiclSdxCd) {
		writeString(Pos.CW02R_CICL_SDX_CD, cw02rCiclSdxCd, Len.CW02R_CICL_SDX_CD);
	}

	/**Original name: CW02R-CICL-SDX-CD<br>*/
	public String getCw02rCiclSdxCd() {
		return readString(Pos.CW02R_CICL_SDX_CD, Len.CW02R_CICL_SDX_CD);
	}

	public void setCw02rCiclOgnInceptDtCi(char cw02rCiclOgnInceptDtCi) {
		writeChar(Pos.CW02R_CICL_OGN_INCEPT_DT_CI, cw02rCiclOgnInceptDtCi);
	}

	/**Original name: CW02R-CICL-OGN-INCEPT-DT-CI<br>*/
	public char getCw02rCiclOgnInceptDtCi() {
		return readChar(Pos.CW02R_CICL_OGN_INCEPT_DT_CI);
	}

	public void setCw02rCiclOgnInceptDtNi(char cw02rCiclOgnInceptDtNi) {
		writeChar(Pos.CW02R_CICL_OGN_INCEPT_DT_NI, cw02rCiclOgnInceptDtNi);
	}

	/**Original name: CW02R-CICL-OGN-INCEPT-DT-NI<br>*/
	public char getCw02rCiclOgnInceptDtNi() {
		return readChar(Pos.CW02R_CICL_OGN_INCEPT_DT_NI);
	}

	public void setCw02rCiclOgnInceptDt(String cw02rCiclOgnInceptDt) {
		writeString(Pos.CW02R_CICL_OGN_INCEPT_DT, cw02rCiclOgnInceptDt, Len.CW02R_CICL_OGN_INCEPT_DT);
	}

	/**Original name: CW02R-CICL-OGN-INCEPT-DT<br>*/
	public String getCw02rCiclOgnInceptDt() {
		return readString(Pos.CW02R_CICL_OGN_INCEPT_DT, Len.CW02R_CICL_OGN_INCEPT_DT);
	}

	public void setCw02rCiclAddNmIndCi(char cw02rCiclAddNmIndCi) {
		writeChar(Pos.CW02R_CICL_ADD_NM_IND_CI, cw02rCiclAddNmIndCi);
	}

	/**Original name: CW02R-CICL-ADD-NM-IND-CI<br>*/
	public char getCw02rCiclAddNmIndCi() {
		return readChar(Pos.CW02R_CICL_ADD_NM_IND_CI);
	}

	public void setCw02rCiclAddNmIndNi(char cw02rCiclAddNmIndNi) {
		writeChar(Pos.CW02R_CICL_ADD_NM_IND_NI, cw02rCiclAddNmIndNi);
	}

	/**Original name: CW02R-CICL-ADD-NM-IND-NI<br>*/
	public char getCw02rCiclAddNmIndNi() {
		return readChar(Pos.CW02R_CICL_ADD_NM_IND_NI);
	}

	public void setCw02rCiclAddNmInd(char cw02rCiclAddNmInd) {
		writeChar(Pos.CW02R_CICL_ADD_NM_IND, cw02rCiclAddNmInd);
	}

	/**Original name: CW02R-CICL-ADD-NM-IND<br>*/
	public char getCw02rCiclAddNmInd() {
		return readChar(Pos.CW02R_CICL_ADD_NM_IND);
	}

	public void setCw02rCiclDobDtCi(char cw02rCiclDobDtCi) {
		writeChar(Pos.CW02R_CICL_DOB_DT_CI, cw02rCiclDobDtCi);
	}

	/**Original name: CW02R-CICL-DOB-DT-CI<br>*/
	public char getCw02rCiclDobDtCi() {
		return readChar(Pos.CW02R_CICL_DOB_DT_CI);
	}

	public void setCw02rCiclDobDt(String cw02rCiclDobDt) {
		writeString(Pos.CW02R_CICL_DOB_DT, cw02rCiclDobDt, Len.CW02R_CICL_DOB_DT);
	}

	/**Original name: CW02R-CICL-DOB-DT<br>*/
	public String getCw02rCiclDobDt() {
		return readString(Pos.CW02R_CICL_DOB_DT, Len.CW02R_CICL_DOB_DT);
	}

	public void setCw02rCiclBirStCdCi(char cw02rCiclBirStCdCi) {
		writeChar(Pos.CW02R_CICL_BIR_ST_CD_CI, cw02rCiclBirStCdCi);
	}

	/**Original name: CW02R-CICL-BIR-ST-CD-CI<br>*/
	public char getCw02rCiclBirStCdCi() {
		return readChar(Pos.CW02R_CICL_BIR_ST_CD_CI);
	}

	public void setCw02rCiclBirStCd(String cw02rCiclBirStCd) {
		writeString(Pos.CW02R_CICL_BIR_ST_CD, cw02rCiclBirStCd, Len.CW02R_CICL_BIR_ST_CD);
	}

	/**Original name: CW02R-CICL-BIR-ST-CD<br>*/
	public String getCw02rCiclBirStCd() {
		return readString(Pos.CW02R_CICL_BIR_ST_CD, Len.CW02R_CICL_BIR_ST_CD);
	}

	public void setCw02rGenderCdCi(char cw02rGenderCdCi) {
		writeChar(Pos.CW02R_GENDER_CD_CI, cw02rGenderCdCi);
	}

	/**Original name: CW02R-GENDER-CD-CI<br>*/
	public char getCw02rGenderCdCi() {
		return readChar(Pos.CW02R_GENDER_CD_CI);
	}

	public void setCw02rGenderCd(char cw02rGenderCd) {
		writeChar(Pos.CW02R_GENDER_CD, cw02rGenderCd);
	}

	/**Original name: CW02R-GENDER-CD<br>*/
	public char getCw02rGenderCd() {
		return readChar(Pos.CW02R_GENDER_CD);
	}

	public void setCw02rPriLggCdCi(char cw02rPriLggCdCi) {
		writeChar(Pos.CW02R_PRI_LGG_CD_CI, cw02rPriLggCdCi);
	}

	/**Original name: CW02R-PRI-LGG-CD-CI<br>*/
	public char getCw02rPriLggCdCi() {
		return readChar(Pos.CW02R_PRI_LGG_CD_CI);
	}

	public void setCw02rPriLggCd(String cw02rPriLggCd) {
		writeString(Pos.CW02R_PRI_LGG_CD, cw02rPriLggCd, Len.CW02R_PRI_LGG_CD);
	}

	/**Original name: CW02R-PRI-LGG-CD<br>*/
	public String getCw02rPriLggCd() {
		return readString(Pos.CW02R_PRI_LGG_CD, Len.CW02R_PRI_LGG_CD);
	}

	public void setCw02rUserIdCi(char cw02rUserIdCi) {
		writeChar(Pos.CW02R_USER_ID_CI, cw02rUserIdCi);
	}

	/**Original name: CW02R-USER-ID-CI<br>*/
	public char getCw02rUserIdCi() {
		return readChar(Pos.CW02R_USER_ID_CI);
	}

	public void setCw02rUserId(String cw02rUserId) {
		writeString(Pos.CW02R_USER_ID, cw02rUserId, Len.CW02R_USER_ID);
	}

	/**Original name: CW02R-USER-ID<br>*/
	public String getCw02rUserId() {
		return readString(Pos.CW02R_USER_ID, Len.CW02R_USER_ID);
	}

	public void setCw02rStatusCdCi(char cw02rStatusCdCi) {
		writeChar(Pos.CW02R_STATUS_CD_CI, cw02rStatusCdCi);
	}

	/**Original name: CW02R-STATUS-CD-CI<br>*/
	public char getCw02rStatusCdCi() {
		return readChar(Pos.CW02R_STATUS_CD_CI);
	}

	public void setCw02rStatusCd(char cw02rStatusCd) {
		writeChar(Pos.CW02R_STATUS_CD, cw02rStatusCd);
	}

	/**Original name: CW02R-STATUS-CD<br>*/
	public char getCw02rStatusCd() {
		return readChar(Pos.CW02R_STATUS_CD);
	}

	public void setCw02rTerminalIdCi(char cw02rTerminalIdCi) {
		writeChar(Pos.CW02R_TERMINAL_ID_CI, cw02rTerminalIdCi);
	}

	/**Original name: CW02R-TERMINAL-ID-CI<br>*/
	public char getCw02rTerminalIdCi() {
		return readChar(Pos.CW02R_TERMINAL_ID_CI);
	}

	public void setCw02rTerminalId(String cw02rTerminalId) {
		writeString(Pos.CW02R_TERMINAL_ID, cw02rTerminalId, Len.CW02R_TERMINAL_ID);
	}

	/**Original name: CW02R-TERMINAL-ID<br>*/
	public String getCw02rTerminalId() {
		return readString(Pos.CW02R_TERMINAL_ID, Len.CW02R_TERMINAL_ID);
	}

	public void setCw02rCiclExpDtCi(char cw02rCiclExpDtCi) {
		writeChar(Pos.CW02R_CICL_EXP_DT_CI, cw02rCiclExpDtCi);
	}

	/**Original name: CW02R-CICL-EXP-DT-CI<br>*/
	public char getCw02rCiclExpDtCi() {
		return readChar(Pos.CW02R_CICL_EXP_DT_CI);
	}

	public void setCw02rCiclExpDt(String cw02rCiclExpDt) {
		writeString(Pos.CW02R_CICL_EXP_DT, cw02rCiclExpDt, Len.CW02R_CICL_EXP_DT);
	}

	/**Original name: CW02R-CICL-EXP-DT<br>*/
	public String getCw02rCiclExpDt() {
		return readString(Pos.CW02R_CICL_EXP_DT, Len.CW02R_CICL_EXP_DT);
	}

	public void setCw02rCiclEffAcyTsCi(char cw02rCiclEffAcyTsCi) {
		writeChar(Pos.CW02R_CICL_EFF_ACY_TS_CI, cw02rCiclEffAcyTsCi);
	}

	/**Original name: CW02R-CICL-EFF-ACY-TS-CI<br>*/
	public char getCw02rCiclEffAcyTsCi() {
		return readChar(Pos.CW02R_CICL_EFF_ACY_TS_CI);
	}

	public void setCw02rCiclEffAcyTs(String cw02rCiclEffAcyTs) {
		writeString(Pos.CW02R_CICL_EFF_ACY_TS, cw02rCiclEffAcyTs, Len.CW02R_CICL_EFF_ACY_TS);
	}

	/**Original name: CW02R-CICL-EFF-ACY-TS<br>*/
	public String getCw02rCiclEffAcyTs() {
		return readString(Pos.CW02R_CICL_EFF_ACY_TS, Len.CW02R_CICL_EFF_ACY_TS);
	}

	public void setCw02rCiclExpAcyTsCi(char cw02rCiclExpAcyTsCi) {
		writeChar(Pos.CW02R_CICL_EXP_ACY_TS_CI, cw02rCiclExpAcyTsCi);
	}

	/**Original name: CW02R-CICL-EXP-ACY-TS-CI<br>*/
	public char getCw02rCiclExpAcyTsCi() {
		return readChar(Pos.CW02R_CICL_EXP_ACY_TS_CI);
	}

	public void setCw02rCiclExpAcyTs(String cw02rCiclExpAcyTs) {
		writeString(Pos.CW02R_CICL_EXP_ACY_TS, cw02rCiclExpAcyTs, Len.CW02R_CICL_EXP_ACY_TS);
	}

	/**Original name: CW02R-CICL-EXP-ACY-TS<br>*/
	public String getCw02rCiclExpAcyTs() {
		return readString(Pos.CW02R_CICL_EXP_ACY_TS, Len.CW02R_CICL_EXP_ACY_TS);
	}

	public void setCw02rStatutoryTleCdCi(char cw02rStatutoryTleCdCi) {
		writeChar(Pos.CW02R_STATUTORY_TLE_CD_CI, cw02rStatutoryTleCdCi);
	}

	/**Original name: CW02R-STATUTORY-TLE-CD-CI<br>*/
	public char getCw02rStatutoryTleCdCi() {
		return readChar(Pos.CW02R_STATUTORY_TLE_CD_CI);
	}

	public void setCw02rStatutoryTleCd(String cw02rStatutoryTleCd) {
		writeString(Pos.CW02R_STATUTORY_TLE_CD, cw02rStatutoryTleCd, Len.CW02R_STATUTORY_TLE_CD);
	}

	/**Original name: CW02R-STATUTORY-TLE-CD<br>*/
	public String getCw02rStatutoryTleCd() {
		return readString(Pos.CW02R_STATUTORY_TLE_CD, Len.CW02R_STATUTORY_TLE_CD);
	}

	public void setCw02rCiclLngNmCi(char cw02rCiclLngNmCi) {
		writeChar(Pos.CW02R_CICL_LNG_NM_CI, cw02rCiclLngNmCi);
	}

	/**Original name: CW02R-CICL-LNG-NM-CI<br>*/
	public char getCw02rCiclLngNmCi() {
		return readChar(Pos.CW02R_CICL_LNG_NM_CI);
	}

	public void setCw02rCiclLngNmNi(char cw02rCiclLngNmNi) {
		writeChar(Pos.CW02R_CICL_LNG_NM_NI, cw02rCiclLngNmNi);
	}

	/**Original name: CW02R-CICL-LNG-NM-NI<br>*/
	public char getCw02rCiclLngNmNi() {
		return readChar(Pos.CW02R_CICL_LNG_NM_NI);
	}

	public void setCw02rCiclLngNm(String cw02rCiclLngNm) {
		writeString(Pos.CW02R_CICL_LNG_NM, cw02rCiclLngNm, Len.CW02R_CICL_LNG_NM);
	}

	/**Original name: CW02R-CICL-LNG-NM<br>*/
	public String getCw02rCiclLngNm() {
		return readString(Pos.CW02R_CICL_LNG_NM, Len.CW02R_CICL_LNG_NM);
	}

	public void setCw02rLoLstNmMchCdCi(char cw02rLoLstNmMchCdCi) {
		writeChar(Pos.CW02R_LO_LST_NM_MCH_CD_CI, cw02rLoLstNmMchCdCi);
	}

	/**Original name: CW02R-LO-LST-NM-MCH-CD-CI<br>*/
	public char getCw02rLoLstNmMchCdCi() {
		return readChar(Pos.CW02R_LO_LST_NM_MCH_CD_CI);
	}

	public void setCw02rLoLstNmMchCdNi(char cw02rLoLstNmMchCdNi) {
		writeChar(Pos.CW02R_LO_LST_NM_MCH_CD_NI, cw02rLoLstNmMchCdNi);
	}

	/**Original name: CW02R-LO-LST-NM-MCH-CD-NI<br>*/
	public char getCw02rLoLstNmMchCdNi() {
		return readChar(Pos.CW02R_LO_LST_NM_MCH_CD_NI);
	}

	public void setCw02rLoLstNmMchCd(String cw02rLoLstNmMchCd) {
		writeString(Pos.CW02R_LO_LST_NM_MCH_CD, cw02rLoLstNmMchCd, Len.CW02R_LO_LST_NM_MCH_CD);
	}

	/**Original name: CW02R-LO-LST-NM-MCH-CD<br>*/
	public String getCw02rLoLstNmMchCd() {
		return readString(Pos.CW02R_LO_LST_NM_MCH_CD, Len.CW02R_LO_LST_NM_MCH_CD);
	}

	public void setCw02rHiLstNmMchCdCi(char cw02rHiLstNmMchCdCi) {
		writeChar(Pos.CW02R_HI_LST_NM_MCH_CD_CI, cw02rHiLstNmMchCdCi);
	}

	/**Original name: CW02R-HI-LST-NM-MCH-CD-CI<br>*/
	public char getCw02rHiLstNmMchCdCi() {
		return readChar(Pos.CW02R_HI_LST_NM_MCH_CD_CI);
	}

	public void setCw02rHiLstNmMchCdNi(char cw02rHiLstNmMchCdNi) {
		writeChar(Pos.CW02R_HI_LST_NM_MCH_CD_NI, cw02rHiLstNmMchCdNi);
	}

	/**Original name: CW02R-HI-LST-NM-MCH-CD-NI<br>*/
	public char getCw02rHiLstNmMchCdNi() {
		return readChar(Pos.CW02R_HI_LST_NM_MCH_CD_NI);
	}

	public void setCw02rHiLstNmMchCd(String cw02rHiLstNmMchCd) {
		writeString(Pos.CW02R_HI_LST_NM_MCH_CD, cw02rHiLstNmMchCd, Len.CW02R_HI_LST_NM_MCH_CD);
	}

	/**Original name: CW02R-HI-LST-NM-MCH-CD<br>*/
	public String getCw02rHiLstNmMchCd() {
		return readString(Pos.CW02R_HI_LST_NM_MCH_CD, Len.CW02R_HI_LST_NM_MCH_CD);
	}

	public void setCw02rLoFstNmMchCdCi(char cw02rLoFstNmMchCdCi) {
		writeChar(Pos.CW02R_LO_FST_NM_MCH_CD_CI, cw02rLoFstNmMchCdCi);
	}

	/**Original name: CW02R-LO-FST-NM-MCH-CD-CI<br>*/
	public char getCw02rLoFstNmMchCdCi() {
		return readChar(Pos.CW02R_LO_FST_NM_MCH_CD_CI);
	}

	public void setCw02rLoFstNmMchCdNi(char cw02rLoFstNmMchCdNi) {
		writeChar(Pos.CW02R_LO_FST_NM_MCH_CD_NI, cw02rLoFstNmMchCdNi);
	}

	/**Original name: CW02R-LO-FST-NM-MCH-CD-NI<br>*/
	public char getCw02rLoFstNmMchCdNi() {
		return readChar(Pos.CW02R_LO_FST_NM_MCH_CD_NI);
	}

	public void setCw02rLoFstNmMchCd(String cw02rLoFstNmMchCd) {
		writeString(Pos.CW02R_LO_FST_NM_MCH_CD, cw02rLoFstNmMchCd, Len.CW02R_LO_FST_NM_MCH_CD);
	}

	/**Original name: CW02R-LO-FST-NM-MCH-CD<br>*/
	public String getCw02rLoFstNmMchCd() {
		return readString(Pos.CW02R_LO_FST_NM_MCH_CD, Len.CW02R_LO_FST_NM_MCH_CD);
	}

	public void setCw02rHiFstNmMchCdCi(char cw02rHiFstNmMchCdCi) {
		writeChar(Pos.CW02R_HI_FST_NM_MCH_CD_CI, cw02rHiFstNmMchCdCi);
	}

	/**Original name: CW02R-HI-FST-NM-MCH-CD-CI<br>*/
	public char getCw02rHiFstNmMchCdCi() {
		return readChar(Pos.CW02R_HI_FST_NM_MCH_CD_CI);
	}

	public void setCw02rHiFstNmMchCdNi(char cw02rHiFstNmMchCdNi) {
		writeChar(Pos.CW02R_HI_FST_NM_MCH_CD_NI, cw02rHiFstNmMchCdNi);
	}

	/**Original name: CW02R-HI-FST-NM-MCH-CD-NI<br>*/
	public char getCw02rHiFstNmMchCdNi() {
		return readChar(Pos.CW02R_HI_FST_NM_MCH_CD_NI);
	}

	public void setCw02rHiFstNmMchCd(String cw02rHiFstNmMchCd) {
		writeString(Pos.CW02R_HI_FST_NM_MCH_CD, cw02rHiFstNmMchCd, Len.CW02R_HI_FST_NM_MCH_CD);
	}

	/**Original name: CW02R-HI-FST-NM-MCH-CD<br>*/
	public String getCw02rHiFstNmMchCd() {
		return readString(Pos.CW02R_HI_FST_NM_MCH_CD, Len.CW02R_HI_FST_NM_MCH_CD);
	}

	public void setCw02rCiclAcqSrcCdCi(char cw02rCiclAcqSrcCdCi) {
		writeChar(Pos.CW02R_CICL_ACQ_SRC_CD_CI, cw02rCiclAcqSrcCdCi);
	}

	/**Original name: CW02R-CICL-ACQ-SRC-CD-CI<br>*/
	public char getCw02rCiclAcqSrcCdCi() {
		return readChar(Pos.CW02R_CICL_ACQ_SRC_CD_CI);
	}

	public void setCw02rCiclAcqSrcCdNi(char cw02rCiclAcqSrcCdNi) {
		writeChar(Pos.CW02R_CICL_ACQ_SRC_CD_NI, cw02rCiclAcqSrcCdNi);
	}

	/**Original name: CW02R-CICL-ACQ-SRC-CD-NI<br>*/
	public char getCw02rCiclAcqSrcCdNi() {
		return readChar(Pos.CW02R_CICL_ACQ_SRC_CD_NI);
	}

	public void setCw02rCiclAcqSrcCd(String cw02rCiclAcqSrcCd) {
		writeString(Pos.CW02R_CICL_ACQ_SRC_CD, cw02rCiclAcqSrcCd, Len.CW02R_CICL_ACQ_SRC_CD);
	}

	/**Original name: CW02R-CICL-ACQ-SRC-CD<br>*/
	public String getCw02rCiclAcqSrcCd() {
		return readString(Pos.CW02R_CICL_ACQ_SRC_CD, Len.CW02R_CICL_ACQ_SRC_CD);
	}

	public void setCw02rLegEntDesc(String cw02rLegEntDesc) {
		writeString(Pos.CW02R_LEG_ENT_DESC, cw02rLegEntDesc, Len.CW02R_LEG_ENT_DESC);
	}

	/**Original name: CW02R-LEG-ENT-DESC<br>*/
	public String getCw02rLegEntDesc() {
		return readString(Pos.CW02R_LEG_ENT_DESC, Len.CW02R_LEG_ENT_DESC);
	}

	public void setCwcacrClientAddrCompositeFormatted(int cwcacrClientAddrCompositeIdx, String data) {
		int position = Pos.cwcacrClientAddrComposite(cwcacrClientAddrCompositeIdx - 1);
		writeString(position, data, Len.CWCACR_CLIENT_ADDR_COMPOSITE);
	}

	public void setCwcacrClientAddrCompChksumFormatted(int cwcacrClientAddrCompChksumIdx, String cwcacrClientAddrCompChksum) {
		int position = Pos.cwcacrClientAddrCompChksum(cwcacrClientAddrCompChksumIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cwcacrClientAddrCompChksum, Len.CWCACR_CLIENT_ADDR_COMP_CHKSUM),
				Len.CWCACR_CLIENT_ADDR_COMP_CHKSUM);
	}

	/**Original name: CWCACR-CLIENT-ADDR-COMP-CHKSUM<br>*/
	public int getCwcacrClientAddrCompChksum(int cwcacrClientAddrCompChksumIdx) {
		int position = Pos.cwcacrClientAddrCompChksum(cwcacrClientAddrCompChksumIdx - 1);
		return readNumDispUnsignedInt(position, Len.CWCACR_CLIENT_ADDR_COMP_CHKSUM);
	}

	public void setCwcacrAdrIdKcre(int cwcacrAdrIdKcreIdx, String cwcacrAdrIdKcre) {
		int position = Pos.cwcacrAdrIdKcre(cwcacrAdrIdKcreIdx - 1);
		writeString(position, cwcacrAdrIdKcre, Len.CWCACR_ADR_ID_KCRE);
	}

	/**Original name: CWCACR-ADR-ID-KCRE<br>*/
	public String getCwcacrAdrIdKcre(int cwcacrAdrIdKcreIdx) {
		int position = Pos.cwcacrAdrIdKcre(cwcacrAdrIdKcreIdx - 1);
		return readString(position, Len.CWCACR_ADR_ID_KCRE);
	}

	public void setCwcacrClientIdKcre(int cwcacrClientIdKcreIdx, String cwcacrClientIdKcre) {
		int position = Pos.cwcacrClientIdKcre(cwcacrClientIdKcreIdx - 1);
		writeString(position, cwcacrClientIdKcre, Len.CWCACR_CLIENT_ID_KCRE);
	}

	/**Original name: CWCACR-CLIENT-ID-KCRE<br>*/
	public String getCwcacrClientIdKcre(int cwcacrClientIdKcreIdx) {
		int position = Pos.cwcacrClientIdKcre(cwcacrClientIdKcreIdx - 1);
		return readString(position, Len.CWCACR_CLIENT_ID_KCRE);
	}

	public void setCwcacrAdrSeqNbrKcre(int cwcacrAdrSeqNbrKcreIdx, String cwcacrAdrSeqNbrKcre) {
		int position = Pos.cwcacrAdrSeqNbrKcre(cwcacrAdrSeqNbrKcreIdx - 1);
		writeString(position, cwcacrAdrSeqNbrKcre, Len.CWCACR_ADR_SEQ_NBR_KCRE);
	}

	/**Original name: CWCACR-ADR-SEQ-NBR-KCRE<br>*/
	public String getCwcacrAdrSeqNbrKcre(int cwcacrAdrSeqNbrKcreIdx) {
		int position = Pos.cwcacrAdrSeqNbrKcre(cwcacrAdrSeqNbrKcreIdx - 1);
		return readString(position, Len.CWCACR_ADR_SEQ_NBR_KCRE);
	}

	public void setCwcacrTchObjectKeyKcre(int cwcacrTchObjectKeyKcreIdx, String cwcacrTchObjectKeyKcre) {
		int position = Pos.cwcacrTchObjectKeyKcre(cwcacrTchObjectKeyKcreIdx - 1);
		writeString(position, cwcacrTchObjectKeyKcre, Len.CWCACR_TCH_OBJECT_KEY_KCRE);
	}

	/**Original name: CWCACR-TCH-OBJECT-KEY-KCRE<br>*/
	public String getCwcacrTchObjectKeyKcre(int cwcacrTchObjectKeyKcreIdx) {
		int position = Pos.cwcacrTchObjectKeyKcre(cwcacrTchObjectKeyKcreIdx - 1);
		return readString(position, Len.CWCACR_TCH_OBJECT_KEY_KCRE);
	}

	public void setCwcacrAdrIdCi(int cwcacrAdrIdCiIdx, char cwcacrAdrIdCi) {
		int position = Pos.cwcacrAdrIdCi(cwcacrAdrIdCiIdx - 1);
		writeChar(position, cwcacrAdrIdCi);
	}

	/**Original name: CWCACR-ADR-ID-CI<br>*/
	public char getCwcacrAdrIdCi(int cwcacrAdrIdCiIdx) {
		int position = Pos.cwcacrAdrIdCi(cwcacrAdrIdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrAdrId(int cwcacrAdrIdIdx, String cwcacrAdrId) {
		int position = Pos.cwcacrAdrId(cwcacrAdrIdIdx - 1);
		writeString(position, cwcacrAdrId, Len.CWCACR_ADR_ID);
	}

	/**Original name: CWCACR-ADR-ID<br>*/
	public String getCwcacrAdrId(int cwcacrAdrIdIdx) {
		int position = Pos.cwcacrAdrId(cwcacrAdrIdIdx - 1);
		return readString(position, Len.CWCACR_ADR_ID);
	}

	public void setCwcacrClientIdCi(int cwcacrClientIdCiIdx, char cwcacrClientIdCi) {
		int position = Pos.cwcacrClientIdCi(cwcacrClientIdCiIdx - 1);
		writeChar(position, cwcacrClientIdCi);
	}

	/**Original name: CWCACR-CLIENT-ID-CI<br>*/
	public char getCwcacrClientIdCi(int cwcacrClientIdCiIdx) {
		int position = Pos.cwcacrClientIdCi(cwcacrClientIdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrClientId(int cwcacrClientIdIdx, String cwcacrClientId) {
		int position = Pos.cwcacrClientId(cwcacrClientIdIdx - 1);
		writeString(position, cwcacrClientId, Len.CWCACR_CLIENT_ID);
	}

	/**Original name: CWCACR-CLIENT-ID<br>*/
	public String getCwcacrClientId(int cwcacrClientIdIdx) {
		int position = Pos.cwcacrClientId(cwcacrClientIdIdx - 1);
		return readString(position, Len.CWCACR_CLIENT_ID);
	}

	public void setCwcacrHistoryVldNbrCi(int cwcacrHistoryVldNbrCiIdx, char cwcacrHistoryVldNbrCi) {
		int position = Pos.cwcacrHistoryVldNbrCi(cwcacrHistoryVldNbrCiIdx - 1);
		writeChar(position, cwcacrHistoryVldNbrCi);
	}

	/**Original name: CWCACR-HISTORY-VLD-NBR-CI<br>*/
	public char getCwcacrHistoryVldNbrCi(int cwcacrHistoryVldNbrCiIdx) {
		int position = Pos.cwcacrHistoryVldNbrCi(cwcacrHistoryVldNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrHistoryVldNbrSign(int cwcacrHistoryVldNbrSignIdx, char cwcacrHistoryVldNbrSign) {
		int position = Pos.cwcacrHistoryVldNbrSign(cwcacrHistoryVldNbrSignIdx - 1);
		writeChar(position, cwcacrHistoryVldNbrSign);
	}

	/**Original name: CWCACR-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCwcacrHistoryVldNbrSign(int cwcacrHistoryVldNbrSignIdx) {
		int position = Pos.cwcacrHistoryVldNbrSign(cwcacrHistoryVldNbrSignIdx - 1);
		return readChar(position);
	}

	public void setCwcacrHistoryVldNbrFormatted(int cwcacrHistoryVldNbrIdx, String cwcacrHistoryVldNbr) {
		int position = Pos.cwcacrHistoryVldNbr(cwcacrHistoryVldNbrIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cwcacrHistoryVldNbr, Len.CWCACR_HISTORY_VLD_NBR), Len.CWCACR_HISTORY_VLD_NBR);
	}

	/**Original name: CWCACR-HISTORY-VLD-NBR<br>*/
	public int getCwcacrHistoryVldNbr(int cwcacrHistoryVldNbrIdx) {
		int position = Pos.cwcacrHistoryVldNbr(cwcacrHistoryVldNbrIdx - 1);
		return readNumDispUnsignedInt(position, Len.CWCACR_HISTORY_VLD_NBR);
	}

	public void setCwcacrAdrSeqNbrCi(int cwcacrAdrSeqNbrCiIdx, char cwcacrAdrSeqNbrCi) {
		int position = Pos.cwcacrAdrSeqNbrCi(cwcacrAdrSeqNbrCiIdx - 1);
		writeChar(position, cwcacrAdrSeqNbrCi);
	}

	/**Original name: CWCACR-ADR-SEQ-NBR-CI<br>*/
	public char getCwcacrAdrSeqNbrCi(int cwcacrAdrSeqNbrCiIdx) {
		int position = Pos.cwcacrAdrSeqNbrCi(cwcacrAdrSeqNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrAdrSeqNbrSign(int cwcacrAdrSeqNbrSignIdx, char cwcacrAdrSeqNbrSign) {
		int position = Pos.cwcacrAdrSeqNbrSign(cwcacrAdrSeqNbrSignIdx - 1);
		writeChar(position, cwcacrAdrSeqNbrSign);
	}

	/**Original name: CWCACR-ADR-SEQ-NBR-SIGN<br>*/
	public char getCwcacrAdrSeqNbrSign(int cwcacrAdrSeqNbrSignIdx) {
		int position = Pos.cwcacrAdrSeqNbrSign(cwcacrAdrSeqNbrSignIdx - 1);
		return readChar(position);
	}

	public void setCwcacrAdrSeqNbrFormatted(int cwcacrAdrSeqNbrIdx, String cwcacrAdrSeqNbr) {
		int position = Pos.cwcacrAdrSeqNbr(cwcacrAdrSeqNbrIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cwcacrAdrSeqNbr, Len.CWCACR_ADR_SEQ_NBR), Len.CWCACR_ADR_SEQ_NBR);
	}

	/**Original name: CWCACR-ADR-SEQ-NBR<br>*/
	public int getCwcacrAdrSeqNbr(int cwcacrAdrSeqNbrIdx) {
		int position = Pos.cwcacrAdrSeqNbr(cwcacrAdrSeqNbrIdx - 1);
		return readNumDispUnsignedInt(position, Len.CWCACR_ADR_SEQ_NBR);
	}

	public String getCwcacrAdrSeqNbrFormatted(int cwcacrAdrSeqNbrIdx) {
		int position = Pos.cwcacrAdrSeqNbr(cwcacrAdrSeqNbrIdx - 1);
		return readFixedString(position, Len.CWCACR_ADR_SEQ_NBR);
	}

	public void setCwcacrCiarEffDtCi(int cwcacrCiarEffDtCiIdx, char cwcacrCiarEffDtCi) {
		int position = Pos.cwcacrCiarEffDtCi(cwcacrCiarEffDtCiIdx - 1);
		writeChar(position, cwcacrCiarEffDtCi);
	}

	/**Original name: CWCACR-CIAR-EFF-DT-CI<br>*/
	public char getCwcacrCiarEffDtCi(int cwcacrCiarEffDtCiIdx) {
		int position = Pos.cwcacrCiarEffDtCi(cwcacrCiarEffDtCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCiarEffDt(int cwcacrCiarEffDtIdx, String cwcacrCiarEffDt) {
		int position = Pos.cwcacrCiarEffDt(cwcacrCiarEffDtIdx - 1);
		writeString(position, cwcacrCiarEffDt, Len.CWCACR_CIAR_EFF_DT);
	}

	/**Original name: CWCACR-CIAR-EFF-DT<br>*/
	public String getCwcacrCiarEffDt(int cwcacrCiarEffDtIdx) {
		int position = Pos.cwcacrCiarEffDt(cwcacrCiarEffDtIdx - 1);
		return readString(position, Len.CWCACR_CIAR_EFF_DT);
	}

	public void setCwcacrTransProcessDt(int cwcacrTransProcessDtIdx, String cwcacrTransProcessDt) {
		int position = Pos.cwcacrTransProcessDt(cwcacrTransProcessDtIdx - 1);
		writeString(position, cwcacrTransProcessDt, Len.CWCACR_TRANS_PROCESS_DT);
	}

	/**Original name: CWCACR-TRANS-PROCESS-DT<br>*/
	public String getCwcacrTransProcessDt(int cwcacrTransProcessDtIdx) {
		int position = Pos.cwcacrTransProcessDt(cwcacrTransProcessDtIdx - 1);
		return readString(position, Len.CWCACR_TRANS_PROCESS_DT);
	}

	public void setCwcacrUserIdCi(int cwcacrUserIdCiIdx, char cwcacrUserIdCi) {
		int position = Pos.cwcacrUserIdCi(cwcacrUserIdCiIdx - 1);
		writeChar(position, cwcacrUserIdCi);
	}

	/**Original name: CWCACR-USER-ID-CI<br>*/
	public char getCwcacrUserIdCi(int cwcacrUserIdCiIdx) {
		int position = Pos.cwcacrUserIdCi(cwcacrUserIdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrUserId(int cwcacrUserIdIdx, String cwcacrUserId) {
		int position = Pos.cwcacrUserId(cwcacrUserIdIdx - 1);
		writeString(position, cwcacrUserId, Len.CWCACR_USER_ID);
	}

	/**Original name: CWCACR-USER-ID<br>*/
	public String getCwcacrUserId(int cwcacrUserIdIdx) {
		int position = Pos.cwcacrUserId(cwcacrUserIdIdx - 1);
		return readString(position, Len.CWCACR_USER_ID);
	}

	public void setCwcacrTerminalIdCi(int cwcacrTerminalIdCiIdx, char cwcacrTerminalIdCi) {
		int position = Pos.cwcacrTerminalIdCi(cwcacrTerminalIdCiIdx - 1);
		writeChar(position, cwcacrTerminalIdCi);
	}

	/**Original name: CWCACR-TERMINAL-ID-CI<br>*/
	public char getCwcacrTerminalIdCi(int cwcacrTerminalIdCiIdx) {
		int position = Pos.cwcacrTerminalIdCi(cwcacrTerminalIdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrTerminalId(int cwcacrTerminalIdIdx, String cwcacrTerminalId) {
		int position = Pos.cwcacrTerminalId(cwcacrTerminalIdIdx - 1);
		writeString(position, cwcacrTerminalId, Len.CWCACR_TERMINAL_ID);
	}

	/**Original name: CWCACR-TERMINAL-ID<br>*/
	public String getCwcacrTerminalId(int cwcacrTerminalIdIdx) {
		int position = Pos.cwcacrTerminalId(cwcacrTerminalIdIdx - 1);
		return readString(position, Len.CWCACR_TERMINAL_ID);
	}

	public void setCwcacrCicaAdr1Ci(int cwcacrCicaAdr1CiIdx, char cwcacrCicaAdr1Ci) {
		int position = Pos.cwcacrCicaAdr1Ci(cwcacrCicaAdr1CiIdx - 1);
		writeChar(position, cwcacrCicaAdr1Ci);
	}

	/**Original name: CWCACR-CICA-ADR-1-CI<br>*/
	public char getCwcacrCicaAdr1Ci(int cwcacrCicaAdr1CiIdx) {
		int position = Pos.cwcacrCicaAdr1Ci(cwcacrCicaAdr1CiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaAdr1(int cwcacrCicaAdr1Idx, String cwcacrCicaAdr1) {
		int position = Pos.cwcacrCicaAdr1(cwcacrCicaAdr1Idx - 1);
		writeString(position, cwcacrCicaAdr1, Len.CWCACR_CICA_ADR1);
	}

	/**Original name: CWCACR-CICA-ADR-1<br>*/
	public String getCwcacrCicaAdr1(int cwcacrCicaAdr1Idx) {
		int position = Pos.cwcacrCicaAdr1(cwcacrCicaAdr1Idx - 1);
		return readString(position, Len.CWCACR_CICA_ADR1);
	}

	public void setCwcacrCicaAdr2Ci(int cwcacrCicaAdr2CiIdx, char cwcacrCicaAdr2Ci) {
		int position = Pos.cwcacrCicaAdr2Ci(cwcacrCicaAdr2CiIdx - 1);
		writeChar(position, cwcacrCicaAdr2Ci);
	}

	/**Original name: CWCACR-CICA-ADR-2-CI<br>*/
	public char getCwcacrCicaAdr2Ci(int cwcacrCicaAdr2CiIdx) {
		int position = Pos.cwcacrCicaAdr2Ci(cwcacrCicaAdr2CiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaAdr2Ni(int cwcacrCicaAdr2NiIdx, char cwcacrCicaAdr2Ni) {
		int position = Pos.cwcacrCicaAdr2Ni(cwcacrCicaAdr2NiIdx - 1);
		writeChar(position, cwcacrCicaAdr2Ni);
	}

	/**Original name: CWCACR-CICA-ADR-2-NI<br>*/
	public char getCwcacrCicaAdr2Ni(int cwcacrCicaAdr2NiIdx) {
		int position = Pos.cwcacrCicaAdr2Ni(cwcacrCicaAdr2NiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaAdr2(int cwcacrCicaAdr2Idx, String cwcacrCicaAdr2) {
		int position = Pos.cwcacrCicaAdr2(cwcacrCicaAdr2Idx - 1);
		writeString(position, cwcacrCicaAdr2, Len.CWCACR_CICA_ADR2);
	}

	/**Original name: CWCACR-CICA-ADR-2<br>*/
	public String getCwcacrCicaAdr2(int cwcacrCicaAdr2Idx) {
		int position = Pos.cwcacrCicaAdr2(cwcacrCicaAdr2Idx - 1);
		return readString(position, Len.CWCACR_CICA_ADR2);
	}

	public void setCwcacrCicaCitNmCi(int cwcacrCicaCitNmCiIdx, char cwcacrCicaCitNmCi) {
		int position = Pos.cwcacrCicaCitNmCi(cwcacrCicaCitNmCiIdx - 1);
		writeChar(position, cwcacrCicaCitNmCi);
	}

	/**Original name: CWCACR-CICA-CIT-NM-CI<br>*/
	public char getCwcacrCicaCitNmCi(int cwcacrCicaCitNmCiIdx) {
		int position = Pos.cwcacrCicaCitNmCi(cwcacrCicaCitNmCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaCitNm(int cwcacrCicaCitNmIdx, String cwcacrCicaCitNm) {
		int position = Pos.cwcacrCicaCitNm(cwcacrCicaCitNmIdx - 1);
		writeString(position, cwcacrCicaCitNm, Len.CWCACR_CICA_CIT_NM);
	}

	/**Original name: CWCACR-CICA-CIT-NM<br>*/
	public String getCwcacrCicaCitNm(int cwcacrCicaCitNmIdx) {
		int position = Pos.cwcacrCicaCitNm(cwcacrCicaCitNmIdx - 1);
		return readString(position, Len.CWCACR_CICA_CIT_NM);
	}

	public void setCwcacrCicaCtyCi(int cwcacrCicaCtyCiIdx, char cwcacrCicaCtyCi) {
		int position = Pos.cwcacrCicaCtyCi(cwcacrCicaCtyCiIdx - 1);
		writeChar(position, cwcacrCicaCtyCi);
	}

	/**Original name: CWCACR-CICA-CTY-CI<br>*/
	public char getCwcacrCicaCtyCi(int cwcacrCicaCtyCiIdx) {
		int position = Pos.cwcacrCicaCtyCi(cwcacrCicaCtyCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaCtyNi(int cwcacrCicaCtyNiIdx, char cwcacrCicaCtyNi) {
		int position = Pos.cwcacrCicaCtyNi(cwcacrCicaCtyNiIdx - 1);
		writeChar(position, cwcacrCicaCtyNi);
	}

	/**Original name: CWCACR-CICA-CTY-NI<br>*/
	public char getCwcacrCicaCtyNi(int cwcacrCicaCtyNiIdx) {
		int position = Pos.cwcacrCicaCtyNi(cwcacrCicaCtyNiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaCty(int cwcacrCicaCtyIdx, String cwcacrCicaCty) {
		int position = Pos.cwcacrCicaCty(cwcacrCicaCtyIdx - 1);
		writeString(position, cwcacrCicaCty, Len.CWCACR_CICA_CTY);
	}

	/**Original name: CWCACR-CICA-CTY<br>*/
	public String getCwcacrCicaCty(int cwcacrCicaCtyIdx) {
		int position = Pos.cwcacrCicaCty(cwcacrCicaCtyIdx - 1);
		return readString(position, Len.CWCACR_CICA_CTY);
	}

	public void setCwcacrStCdCi(int cwcacrStCdCiIdx, char cwcacrStCdCi) {
		int position = Pos.cwcacrStCdCi(cwcacrStCdCiIdx - 1);
		writeChar(position, cwcacrStCdCi);
	}

	/**Original name: CWCACR-ST-CD-CI<br>*/
	public char getCwcacrStCdCi(int cwcacrStCdCiIdx) {
		int position = Pos.cwcacrStCdCi(cwcacrStCdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrStCd(int cwcacrStCdIdx, String cwcacrStCd) {
		int position = Pos.cwcacrStCd(cwcacrStCdIdx - 1);
		writeString(position, cwcacrStCd, Len.CWCACR_ST_CD);
	}

	/**Original name: CWCACR-ST-CD<br>*/
	public String getCwcacrStCd(int cwcacrStCdIdx) {
		int position = Pos.cwcacrStCd(cwcacrStCdIdx - 1);
		return readString(position, Len.CWCACR_ST_CD);
	}

	public void setCwcacrCicaPstCdCi(int cwcacrCicaPstCdCiIdx, char cwcacrCicaPstCdCi) {
		int position = Pos.cwcacrCicaPstCdCi(cwcacrCicaPstCdCiIdx - 1);
		writeChar(position, cwcacrCicaPstCdCi);
	}

	/**Original name: CWCACR-CICA-PST-CD-CI<br>*/
	public char getCwcacrCicaPstCdCi(int cwcacrCicaPstCdCiIdx) {
		int position = Pos.cwcacrCicaPstCdCi(cwcacrCicaPstCdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaPstCd(int cwcacrCicaPstCdIdx, String cwcacrCicaPstCd) {
		int position = Pos.cwcacrCicaPstCd(cwcacrCicaPstCdIdx - 1);
		writeString(position, cwcacrCicaPstCd, Len.CWCACR_CICA_PST_CD);
	}

	/**Original name: CWCACR-CICA-PST-CD<br>*/
	public String getCwcacrCicaPstCd(int cwcacrCicaPstCdIdx) {
		int position = Pos.cwcacrCicaPstCd(cwcacrCicaPstCdIdx - 1);
		return readString(position, Len.CWCACR_CICA_PST_CD);
	}

	public void setCwcacrCtrCdCi(int cwcacrCtrCdCiIdx, char cwcacrCtrCdCi) {
		int position = Pos.cwcacrCtrCdCi(cwcacrCtrCdCiIdx - 1);
		writeChar(position, cwcacrCtrCdCi);
	}

	/**Original name: CWCACR-CTR-CD-CI<br>*/
	public char getCwcacrCtrCdCi(int cwcacrCtrCdCiIdx) {
		int position = Pos.cwcacrCtrCdCi(cwcacrCtrCdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCtrCd(int cwcacrCtrCdIdx, String cwcacrCtrCd) {
		int position = Pos.cwcacrCtrCd(cwcacrCtrCdIdx - 1);
		writeString(position, cwcacrCtrCd, Len.CWCACR_CTR_CD);
	}

	/**Original name: CWCACR-CTR-CD<br>*/
	public String getCwcacrCtrCd(int cwcacrCtrCdIdx) {
		int position = Pos.cwcacrCtrCd(cwcacrCtrCdIdx - 1);
		return readString(position, Len.CWCACR_CTR_CD);
	}

	public void setCwcacrCicaAddAdrIndCi(int cwcacrCicaAddAdrIndCiIdx, char cwcacrCicaAddAdrIndCi) {
		int position = Pos.cwcacrCicaAddAdrIndCi(cwcacrCicaAddAdrIndCiIdx - 1);
		writeChar(position, cwcacrCicaAddAdrIndCi);
	}

	/**Original name: CWCACR-CICA-ADD-ADR-IND-CI<br>*/
	public char getCwcacrCicaAddAdrIndCi(int cwcacrCicaAddAdrIndCiIdx) {
		int position = Pos.cwcacrCicaAddAdrIndCi(cwcacrCicaAddAdrIndCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaAddAdrIndNi(int cwcacrCicaAddAdrIndNiIdx, char cwcacrCicaAddAdrIndNi) {
		int position = Pos.cwcacrCicaAddAdrIndNi(cwcacrCicaAddAdrIndNiIdx - 1);
		writeChar(position, cwcacrCicaAddAdrIndNi);
	}

	/**Original name: CWCACR-CICA-ADD-ADR-IND-NI<br>*/
	public char getCwcacrCicaAddAdrIndNi(int cwcacrCicaAddAdrIndNiIdx) {
		int position = Pos.cwcacrCicaAddAdrIndNi(cwcacrCicaAddAdrIndNiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaAddAdrInd(int cwcacrCicaAddAdrIndIdx, char cwcacrCicaAddAdrInd) {
		int position = Pos.cwcacrCicaAddAdrInd(cwcacrCicaAddAdrIndIdx - 1);
		writeChar(position, cwcacrCicaAddAdrInd);
	}

	/**Original name: CWCACR-CICA-ADD-ADR-IND<br>*/
	public char getCwcacrCicaAddAdrInd(int cwcacrCicaAddAdrIndIdx) {
		int position = Pos.cwcacrCicaAddAdrInd(cwcacrCicaAddAdrIndIdx - 1);
		return readChar(position);
	}

	public void setCwcacrAddrStatusCdCi(int cwcacrAddrStatusCdCiIdx, char cwcacrAddrStatusCdCi) {
		int position = Pos.cwcacrAddrStatusCdCi(cwcacrAddrStatusCdCiIdx - 1);
		writeChar(position, cwcacrAddrStatusCdCi);
	}

	/**Original name: CWCACR-ADDR-STATUS-CD-CI<br>*/
	public char getCwcacrAddrStatusCdCi(int cwcacrAddrStatusCdCiIdx) {
		int position = Pos.cwcacrAddrStatusCdCi(cwcacrAddrStatusCdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrAddrStatusCd(int cwcacrAddrStatusCdIdx, char cwcacrAddrStatusCd) {
		int position = Pos.cwcacrAddrStatusCd(cwcacrAddrStatusCdIdx - 1);
		writeChar(position, cwcacrAddrStatusCd);
	}

	/**Original name: CWCACR-ADDR-STATUS-CD<br>*/
	public char getCwcacrAddrStatusCd(int cwcacrAddrStatusCdIdx) {
		int position = Pos.cwcacrAddrStatusCd(cwcacrAddrStatusCdIdx - 1);
		return readChar(position);
	}

	public void setCwcacrAddNmIndCi(int cwcacrAddNmIndCiIdx, char cwcacrAddNmIndCi) {
		int position = Pos.cwcacrAddNmIndCi(cwcacrAddNmIndCiIdx - 1);
		writeChar(position, cwcacrAddNmIndCi);
	}

	/**Original name: CWCACR-ADD-NM-IND-CI<br>*/
	public char getCwcacrAddNmIndCi(int cwcacrAddNmIndCiIdx) {
		int position = Pos.cwcacrAddNmIndCi(cwcacrAddNmIndCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrAddNmInd(int cwcacrAddNmIndIdx, char cwcacrAddNmInd) {
		int position = Pos.cwcacrAddNmInd(cwcacrAddNmIndIdx - 1);
		writeChar(position, cwcacrAddNmInd);
	}

	/**Original name: CWCACR-ADD-NM-IND<br>*/
	public char getCwcacrAddNmInd(int cwcacrAddNmIndIdx) {
		int position = Pos.cwcacrAddNmInd(cwcacrAddNmIndIdx - 1);
		return readChar(position);
	}

	public void setCwcacrAdrTypCdCi(int cwcacrAdrTypCdCiIdx, char cwcacrAdrTypCdCi) {
		int position = Pos.cwcacrAdrTypCdCi(cwcacrAdrTypCdCiIdx - 1);
		writeChar(position, cwcacrAdrTypCdCi);
	}

	/**Original name: CWCACR-ADR-TYP-CD-CI<br>*/
	public char getCwcacrAdrTypCdCi(int cwcacrAdrTypCdCiIdx) {
		int position = Pos.cwcacrAdrTypCdCi(cwcacrAdrTypCdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrAdrTypCd(int cwcacrAdrTypCdIdx, String cwcacrAdrTypCd) {
		int position = Pos.cwcacrAdrTypCd(cwcacrAdrTypCdIdx - 1);
		writeString(position, cwcacrAdrTypCd, Len.CWCACR_ADR_TYP_CD);
	}

	/**Original name: CWCACR-ADR-TYP-CD<br>*/
	public String getCwcacrAdrTypCd(int cwcacrAdrTypCdIdx) {
		int position = Pos.cwcacrAdrTypCd(cwcacrAdrTypCdIdx - 1);
		return readString(position, Len.CWCACR_ADR_TYP_CD);
	}

	public void setCwcacrTchObjectKeyCi(int cwcacrTchObjectKeyCiIdx, char cwcacrTchObjectKeyCi) {
		int position = Pos.cwcacrTchObjectKeyCi(cwcacrTchObjectKeyCiIdx - 1);
		writeChar(position, cwcacrTchObjectKeyCi);
	}

	/**Original name: CWCACR-TCH-OBJECT-KEY-CI<br>*/
	public char getCwcacrTchObjectKeyCi(int cwcacrTchObjectKeyCiIdx) {
		int position = Pos.cwcacrTchObjectKeyCi(cwcacrTchObjectKeyCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrTchObjectKey(int cwcacrTchObjectKeyIdx, String cwcacrTchObjectKey) {
		int position = Pos.cwcacrTchObjectKey(cwcacrTchObjectKeyIdx - 1);
		writeString(position, cwcacrTchObjectKey, Len.CWCACR_TCH_OBJECT_KEY);
	}

	/**Original name: CWCACR-TCH-OBJECT-KEY<br>*/
	public String getCwcacrTchObjectKey(int cwcacrTchObjectKeyIdx) {
		int position = Pos.cwcacrTchObjectKey(cwcacrTchObjectKeyIdx - 1);
		return readString(position, Len.CWCACR_TCH_OBJECT_KEY);
	}

	public void setCwcacrCiarExpDtCi(int cwcacrCiarExpDtCiIdx, String cwcacrCiarExpDtCi) {
		int position = Pos.cwcacrCiarExpDtCi(cwcacrCiarExpDtCiIdx - 1);
		writeString(position, cwcacrCiarExpDtCi, Len.CWCACR_CIAR_EXP_DT_CI);
	}

	/**Original name: CWCACR-CIAR-EXP-DT-CI<br>*/
	public String getCwcacrCiarExpDtCi(int cwcacrCiarExpDtCiIdx) {
		int position = Pos.cwcacrCiarExpDtCi(cwcacrCiarExpDtCiIdx - 1);
		return readString(position, Len.CWCACR_CIAR_EXP_DT_CI);
	}

	public void setCwcacrCiarExpDt(int cwcacrCiarExpDtIdx, String cwcacrCiarExpDt) {
		int position = Pos.cwcacrCiarExpDt(cwcacrCiarExpDtIdx - 1);
		writeString(position, cwcacrCiarExpDt, Len.CWCACR_CIAR_EXP_DT);
	}

	/**Original name: CWCACR-CIAR-EXP-DT<br>*/
	public String getCwcacrCiarExpDt(int cwcacrCiarExpDtIdx) {
		int position = Pos.cwcacrCiarExpDt(cwcacrCiarExpDtIdx - 1);
		return readString(position, Len.CWCACR_CIAR_EXP_DT);
	}

	public void setCwcacrCiarSerAdr1TxtCi(int cwcacrCiarSerAdr1TxtCiIdx, char cwcacrCiarSerAdr1TxtCi) {
		int position = Pos.cwcacrCiarSerAdr1TxtCi(cwcacrCiarSerAdr1TxtCiIdx - 1);
		writeChar(position, cwcacrCiarSerAdr1TxtCi);
	}

	/**Original name: CWCACR-CIAR-SER-ADR-1-TXT-CI<br>*/
	public char getCwcacrCiarSerAdr1TxtCi(int cwcacrCiarSerAdr1TxtCiIdx) {
		int position = Pos.cwcacrCiarSerAdr1TxtCi(cwcacrCiarSerAdr1TxtCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCiarSerAdr1Txt(int cwcacrCiarSerAdr1TxtIdx, String cwcacrCiarSerAdr1Txt) {
		int position = Pos.cwcacrCiarSerAdr1Txt(cwcacrCiarSerAdr1TxtIdx - 1);
		writeString(position, cwcacrCiarSerAdr1Txt, Len.CWCACR_CIAR_SER_ADR1_TXT);
	}

	/**Original name: CWCACR-CIAR-SER-ADR-1-TXT<br>*/
	public String getCwcacrCiarSerAdr1Txt(int cwcacrCiarSerAdr1TxtIdx) {
		int position = Pos.cwcacrCiarSerAdr1Txt(cwcacrCiarSerAdr1TxtIdx - 1);
		return readString(position, Len.CWCACR_CIAR_SER_ADR1_TXT);
	}

	public void setCwcacrCiarSerCitNmCi(int cwcacrCiarSerCitNmCiIdx, char cwcacrCiarSerCitNmCi) {
		int position = Pos.cwcacrCiarSerCitNmCi(cwcacrCiarSerCitNmCiIdx - 1);
		writeChar(position, cwcacrCiarSerCitNmCi);
	}

	/**Original name: CWCACR-CIAR-SER-CIT-NM-CI<br>*/
	public char getCwcacrCiarSerCitNmCi(int cwcacrCiarSerCitNmCiIdx) {
		int position = Pos.cwcacrCiarSerCitNmCi(cwcacrCiarSerCitNmCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCiarSerCitNm(int cwcacrCiarSerCitNmIdx, String cwcacrCiarSerCitNm) {
		int position = Pos.cwcacrCiarSerCitNm(cwcacrCiarSerCitNmIdx - 1);
		writeString(position, cwcacrCiarSerCitNm, Len.CWCACR_CIAR_SER_CIT_NM);
	}

	/**Original name: CWCACR-CIAR-SER-CIT-NM<br>*/
	public String getCwcacrCiarSerCitNm(int cwcacrCiarSerCitNmIdx) {
		int position = Pos.cwcacrCiarSerCitNm(cwcacrCiarSerCitNmIdx - 1);
		return readString(position, Len.CWCACR_CIAR_SER_CIT_NM);
	}

	public void setCwcacrCiarSerStCdCi(int cwcacrCiarSerStCdCiIdx, char cwcacrCiarSerStCdCi) {
		int position = Pos.cwcacrCiarSerStCdCi(cwcacrCiarSerStCdCiIdx - 1);
		writeChar(position, cwcacrCiarSerStCdCi);
	}

	/**Original name: CWCACR-CIAR-SER-ST-CD-CI<br>*/
	public char getCwcacrCiarSerStCdCi(int cwcacrCiarSerStCdCiIdx) {
		int position = Pos.cwcacrCiarSerStCdCi(cwcacrCiarSerStCdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCiarSerStCd(int cwcacrCiarSerStCdIdx, String cwcacrCiarSerStCd) {
		int position = Pos.cwcacrCiarSerStCd(cwcacrCiarSerStCdIdx - 1);
		writeString(position, cwcacrCiarSerStCd, Len.CWCACR_CIAR_SER_ST_CD);
	}

	/**Original name: CWCACR-CIAR-SER-ST-CD<br>*/
	public String getCwcacrCiarSerStCd(int cwcacrCiarSerStCdIdx) {
		int position = Pos.cwcacrCiarSerStCd(cwcacrCiarSerStCdIdx - 1);
		return readString(position, Len.CWCACR_CIAR_SER_ST_CD);
	}

	public void setCwcacrCiarSerPstCdCi(int cwcacrCiarSerPstCdCiIdx, char cwcacrCiarSerPstCdCi) {
		int position = Pos.cwcacrCiarSerPstCdCi(cwcacrCiarSerPstCdCiIdx - 1);
		writeChar(position, cwcacrCiarSerPstCdCi);
	}

	/**Original name: CWCACR-CIAR-SER-PST-CD-CI<br>*/
	public char getCwcacrCiarSerPstCdCi(int cwcacrCiarSerPstCdCiIdx) {
		int position = Pos.cwcacrCiarSerPstCdCi(cwcacrCiarSerPstCdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCiarSerPstCd(int cwcacrCiarSerPstCdIdx, String cwcacrCiarSerPstCd) {
		int position = Pos.cwcacrCiarSerPstCd(cwcacrCiarSerPstCdIdx - 1);
		writeString(position, cwcacrCiarSerPstCd, Len.CWCACR_CIAR_SER_PST_CD);
	}

	/**Original name: CWCACR-CIAR-SER-PST-CD<br>*/
	public String getCwcacrCiarSerPstCd(int cwcacrCiarSerPstCdIdx) {
		int position = Pos.cwcacrCiarSerPstCd(cwcacrCiarSerPstCdIdx - 1);
		return readString(position, Len.CWCACR_CIAR_SER_PST_CD);
	}

	public void setCwcacrRelStatusCdCi(int cwcacrRelStatusCdCiIdx, char cwcacrRelStatusCdCi) {
		int position = Pos.cwcacrRelStatusCdCi(cwcacrRelStatusCdCiIdx - 1);
		writeChar(position, cwcacrRelStatusCdCi);
	}

	/**Original name: CWCACR-REL-STATUS-CD-CI<br>*/
	public char getCwcacrRelStatusCdCi(int cwcacrRelStatusCdCiIdx) {
		int position = Pos.cwcacrRelStatusCdCi(cwcacrRelStatusCdCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrRelStatusCd(int cwcacrRelStatusCdIdx, char cwcacrRelStatusCd) {
		int position = Pos.cwcacrRelStatusCd(cwcacrRelStatusCdIdx - 1);
		writeChar(position, cwcacrRelStatusCd);
	}

	/**Original name: CWCACR-REL-STATUS-CD<br>*/
	public char getCwcacrRelStatusCd(int cwcacrRelStatusCdIdx) {
		int position = Pos.cwcacrRelStatusCd(cwcacrRelStatusCdIdx - 1);
		return readChar(position);
	}

	public void setCwcacrEffAcyTsCi(int cwcacrEffAcyTsCiIdx, char cwcacrEffAcyTsCi) {
		int position = Pos.cwcacrEffAcyTsCi(cwcacrEffAcyTsCiIdx - 1);
		writeChar(position, cwcacrEffAcyTsCi);
	}

	/**Original name: CWCACR-EFF-ACY-TS-CI<br>*/
	public char getCwcacrEffAcyTsCi(int cwcacrEffAcyTsCiIdx) {
		int position = Pos.cwcacrEffAcyTsCi(cwcacrEffAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrEffAcyTs(int cwcacrEffAcyTsIdx, String cwcacrEffAcyTs) {
		int position = Pos.cwcacrEffAcyTs(cwcacrEffAcyTsIdx - 1);
		writeString(position, cwcacrEffAcyTs, Len.CWCACR_EFF_ACY_TS);
	}

	/**Original name: CWCACR-EFF-ACY-TS<br>*/
	public String getCwcacrEffAcyTs(int cwcacrEffAcyTsIdx) {
		int position = Pos.cwcacrEffAcyTs(cwcacrEffAcyTsIdx - 1);
		return readString(position, Len.CWCACR_EFF_ACY_TS);
	}

	public void setCwcacrCiarExpAcyTsCi(int cwcacrCiarExpAcyTsCiIdx, char cwcacrCiarExpAcyTsCi) {
		int position = Pos.cwcacrCiarExpAcyTsCi(cwcacrCiarExpAcyTsCiIdx - 1);
		writeChar(position, cwcacrCiarExpAcyTsCi);
	}

	/**Original name: CWCACR-CIAR-EXP-ACY-TS-CI<br>*/
	public char getCwcacrCiarExpAcyTsCi(int cwcacrCiarExpAcyTsCiIdx) {
		int position = Pos.cwcacrCiarExpAcyTsCi(cwcacrCiarExpAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCiarExpAcyTs(int cwcacrCiarExpAcyTsIdx, String cwcacrCiarExpAcyTs) {
		int position = Pos.cwcacrCiarExpAcyTs(cwcacrCiarExpAcyTsIdx - 1);
		writeString(position, cwcacrCiarExpAcyTs, Len.CWCACR_CIAR_EXP_ACY_TS);
	}

	/**Original name: CWCACR-CIAR-EXP-ACY-TS<br>*/
	public String getCwcacrCiarExpAcyTs(int cwcacrCiarExpAcyTsIdx) {
		int position = Pos.cwcacrCiarExpAcyTs(cwcacrCiarExpAcyTsIdx - 1);
		return readString(position, Len.CWCACR_CIAR_EXP_ACY_TS);
	}

	public void setCwcacrMoreRowsSw(int cwcacrMoreRowsSwIdx, char cwcacrMoreRowsSw) {
		int position = Pos.cwcacrMoreRowsSw(cwcacrMoreRowsSwIdx - 1);
		writeChar(position, cwcacrMoreRowsSw);
	}

	/**Original name: CWCACR-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	public char getCwcacrMoreRowsSw(int cwcacrMoreRowsSwIdx) {
		int position = Pos.cwcacrMoreRowsSw(cwcacrMoreRowsSwIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaShwObjKeyCi(int cwcacrCicaShwObjKeyCiIdx, char cwcacrCicaShwObjKeyCi) {
		int position = Pos.cwcacrCicaShwObjKeyCi(cwcacrCicaShwObjKeyCiIdx - 1);
		writeChar(position, cwcacrCicaShwObjKeyCi);
	}

	/**Original name: CWCACR-CICA-SHW-OBJ-KEY-CI<br>
	 * <pre>* VARIABLE FOR SHW-OBJ-KEY(SHOWABLE OBJECT KEY)</pre>*/
	public char getCwcacrCicaShwObjKeyCi(int cwcacrCicaShwObjKeyCiIdx) {
		int position = Pos.cwcacrCicaShwObjKeyCi(cwcacrCicaShwObjKeyCiIdx - 1);
		return readChar(position);
	}

	public void setCwcacrCicaShwObjKey(int cwcacrCicaShwObjKeyIdx, String cwcacrCicaShwObjKey) {
		int position = Pos.cwcacrCicaShwObjKey(cwcacrCicaShwObjKeyIdx - 1);
		writeString(position, cwcacrCicaShwObjKey, Len.CWCACR_CICA_SHW_OBJ_KEY);
	}

	/**Original name: CWCACR-CICA-SHW-OBJ-KEY<br>*/
	public String getCwcacrCicaShwObjKey(int cwcacrCicaShwObjKeyIdx) {
		int position = Pos.cwcacrCicaShwObjKey(cwcacrCicaShwObjKeyIdx - 1);
		return readString(position, Len.CWCACR_CICA_SHW_OBJ_KEY);
	}

	public void setCwcacrBusObjNm(int cwcacrBusObjNmIdx, String cwcacrBusObjNm) {
		int position = Pos.cwcacrBusObjNm(cwcacrBusObjNmIdx - 1);
		writeString(position, cwcacrBusObjNm, Len.CWCACR_BUS_OBJ_NM);
	}

	/**Original name: CWCACR-BUS-OBJ-NM<br>*/
	public String getCwcacrBusObjNm(int cwcacrBusObjNmIdx) {
		int position = Pos.cwcacrBusObjNm(cwcacrBusObjNmIdx - 1);
		return readString(position, Len.CWCACR_BUS_OBJ_NM);
	}

	public void setCwcacrStDesc(int cwcacrStDescIdx, String cwcacrStDesc) {
		int position = Pos.cwcacrStDesc(cwcacrStDescIdx - 1);
		writeString(position, cwcacrStDesc, Len.CWCACR_ST_DESC);
	}

	/**Original name: CWCACR-ST-DESC<br>*/
	public String getCwcacrStDesc(int cwcacrStDescIdx) {
		int position = Pos.cwcacrStDesc(cwcacrStDescIdx - 1);
		return readString(position, Len.CWCACR_ST_DESC);
	}

	public void setCwcacrAdrTypDesc(int cwcacrAdrTypDescIdx, String cwcacrAdrTypDesc) {
		int position = Pos.cwcacrAdrTypDesc(cwcacrAdrTypDescIdx - 1);
		writeString(position, cwcacrAdrTypDesc, Len.CWCACR_ADR_TYP_DESC);
	}

	/**Original name: CWCACR-ADR-TYP-DESC<br>*/
	public String getCwcacrAdrTypDesc(int cwcacrAdrTypDescIdx) {
		int position = Pos.cwcacrAdrTypDesc(cwcacrAdrTypDescIdx - 1);
		return readString(position, Len.CWCACR_ADR_TYP_DESC);
	}

	public void setCwemrClientEmailRowFormatted(String data) {
		writeString(Pos.CWEMR_CLIENT_EMAIL_ROW, data, Len.CWEMR_CLIENT_EMAIL_ROW);
	}

	public void setCwemrClientEmailCsumFormatted(String cwemrClientEmailCsum) {
		writeString(Pos.CWEMR_CLIENT_EMAIL_CSUM, Trunc.toUnsignedNumeric(cwemrClientEmailCsum, Len.CWEMR_CLIENT_EMAIL_CSUM),
				Len.CWEMR_CLIENT_EMAIL_CSUM);
	}

	/**Original name: CWEMR-CLIENT-EMAIL-CSUM<br>*/
	public int getCwemrClientEmailCsum() {
		return readNumDispUnsignedInt(Pos.CWEMR_CLIENT_EMAIL_CSUM, Len.CWEMR_CLIENT_EMAIL_CSUM);
	}

	public void setCwemrClientIdKcre(String cwemrClientIdKcre) {
		writeString(Pos.CWEMR_CLIENT_ID_KCRE, cwemrClientIdKcre, Len.CWEMR_CLIENT_ID_KCRE);
	}

	/**Original name: CWEMR-CLIENT-ID-KCRE<br>*/
	public String getCwemrClientIdKcre() {
		return readString(Pos.CWEMR_CLIENT_ID_KCRE, Len.CWEMR_CLIENT_ID_KCRE);
	}

	public void setCwemrCiemSeqNbrKcre(String cwemrCiemSeqNbrKcre) {
		writeString(Pos.CWEMR_CIEM_SEQ_NBR_KCRE, cwemrCiemSeqNbrKcre, Len.CWEMR_CIEM_SEQ_NBR_KCRE);
	}

	/**Original name: CWEMR-CIEM-SEQ-NBR-KCRE<br>*/
	public String getCwemrCiemSeqNbrKcre() {
		return readString(Pos.CWEMR_CIEM_SEQ_NBR_KCRE, Len.CWEMR_CIEM_SEQ_NBR_KCRE);
	}

	public void setCwemrHistoryVldNbrKcre(String cwemrHistoryVldNbrKcre) {
		writeString(Pos.CWEMR_HISTORY_VLD_NBR_KCRE, cwemrHistoryVldNbrKcre, Len.CWEMR_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CWEMR-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCwemrHistoryVldNbrKcre() {
		return readString(Pos.CWEMR_HISTORY_VLD_NBR_KCRE, Len.CWEMR_HISTORY_VLD_NBR_KCRE);
	}

	public void setCwemrEffectiveDtKcre(String cwemrEffectiveDtKcre) {
		writeString(Pos.CWEMR_EFFECTIVE_DT_KCRE, cwemrEffectiveDtKcre, Len.CWEMR_EFFECTIVE_DT_KCRE);
	}

	/**Original name: CWEMR-EFFECTIVE-DT-KCRE<br>*/
	public String getCwemrEffectiveDtKcre() {
		return readString(Pos.CWEMR_EFFECTIVE_DT_KCRE, Len.CWEMR_EFFECTIVE_DT_KCRE);
	}

	public void setCwemrTransProcessDt(String cwemrTransProcessDt) {
		writeString(Pos.CWEMR_TRANS_PROCESS_DT, cwemrTransProcessDt, Len.CWEMR_TRANS_PROCESS_DT);
	}

	/**Original name: CWEMR-TRANS-PROCESS-DT<br>*/
	public String getCwemrTransProcessDt() {
		return readString(Pos.CWEMR_TRANS_PROCESS_DT, Len.CWEMR_TRANS_PROCESS_DT);
	}

	public void setCwemrClientIdCi(char cwemrClientIdCi) {
		writeChar(Pos.CWEMR_CLIENT_ID_CI, cwemrClientIdCi);
	}

	/**Original name: CWEMR-CLIENT-ID-CI<br>*/
	public char getCwemrClientIdCi() {
		return readChar(Pos.CWEMR_CLIENT_ID_CI);
	}

	public void setCwemrClientId(String cwemrClientId) {
		writeString(Pos.CWEMR_CLIENT_ID, cwemrClientId, Len.CWEMR_CLIENT_ID);
	}

	/**Original name: CWEMR-CLIENT-ID<br>*/
	public String getCwemrClientId() {
		return readString(Pos.CWEMR_CLIENT_ID, Len.CWEMR_CLIENT_ID);
	}

	public void setCwemrCiemSeqNbrCi(char cwemrCiemSeqNbrCi) {
		writeChar(Pos.CWEMR_CIEM_SEQ_NBR_CI, cwemrCiemSeqNbrCi);
	}

	/**Original name: CWEMR-CIEM-SEQ-NBR-CI<br>*/
	public char getCwemrCiemSeqNbrCi() {
		return readChar(Pos.CWEMR_CIEM_SEQ_NBR_CI);
	}

	public void setCwemrCiemSeqNbrSign(char cwemrCiemSeqNbrSign) {
		writeChar(Pos.CWEMR_CIEM_SEQ_NBR_SIGN, cwemrCiemSeqNbrSign);
	}

	/**Original name: CWEMR-CIEM-SEQ-NBR-SIGN<br>*/
	public char getCwemrCiemSeqNbrSign() {
		return readChar(Pos.CWEMR_CIEM_SEQ_NBR_SIGN);
	}

	public void setCwemrCiemSeqNbrFormatted(String cwemrCiemSeqNbr) {
		writeString(Pos.CWEMR_CIEM_SEQ_NBR, Trunc.toUnsignedNumeric(cwemrCiemSeqNbr, Len.CWEMR_CIEM_SEQ_NBR), Len.CWEMR_CIEM_SEQ_NBR);
	}

	/**Original name: CWEMR-CIEM-SEQ-NBR<br>*/
	public int getCwemrCiemSeqNbr() {
		return readNumDispUnsignedInt(Pos.CWEMR_CIEM_SEQ_NBR, Len.CWEMR_CIEM_SEQ_NBR);
	}

	public void setCwemrHistoryVldNbrCi(char cwemrHistoryVldNbrCi) {
		writeChar(Pos.CWEMR_HISTORY_VLD_NBR_CI, cwemrHistoryVldNbrCi);
	}

	/**Original name: CWEMR-HISTORY-VLD-NBR-CI<br>*/
	public char getCwemrHistoryVldNbrCi() {
		return readChar(Pos.CWEMR_HISTORY_VLD_NBR_CI);
	}

	public void setCwemrHistoryVldNbrSign(char cwemrHistoryVldNbrSign) {
		writeChar(Pos.CWEMR_HISTORY_VLD_NBR_SIGN, cwemrHistoryVldNbrSign);
	}

	/**Original name: CWEMR-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCwemrHistoryVldNbrSign() {
		return readChar(Pos.CWEMR_HISTORY_VLD_NBR_SIGN);
	}

	public void setCwemrHistoryVldNbrFormatted(String cwemrHistoryVldNbr) {
		writeString(Pos.CWEMR_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cwemrHistoryVldNbr, Len.CWEMR_HISTORY_VLD_NBR), Len.CWEMR_HISTORY_VLD_NBR);
	}

	/**Original name: CWEMR-HISTORY-VLD-NBR<br>*/
	public int getCwemrHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CWEMR_HISTORY_VLD_NBR, Len.CWEMR_HISTORY_VLD_NBR);
	}

	public void setCwemrEffectiveDtCi(char cwemrEffectiveDtCi) {
		writeChar(Pos.CWEMR_EFFECTIVE_DT_CI, cwemrEffectiveDtCi);
	}

	/**Original name: CWEMR-EFFECTIVE-DT-CI<br>*/
	public char getCwemrEffectiveDtCi() {
		return readChar(Pos.CWEMR_EFFECTIVE_DT_CI);
	}

	public void setCwemrEffectiveDt(String cwemrEffectiveDt) {
		writeString(Pos.CWEMR_EFFECTIVE_DT, cwemrEffectiveDt, Len.CWEMR_EFFECTIVE_DT);
	}

	/**Original name: CWEMR-EFFECTIVE-DT<br>*/
	public String getCwemrEffectiveDt() {
		return readString(Pos.CWEMR_EFFECTIVE_DT, Len.CWEMR_EFFECTIVE_DT);
	}

	public void setCwemrEmailTypeCdCi(char cwemrEmailTypeCdCi) {
		writeChar(Pos.CWEMR_EMAIL_TYPE_CD_CI, cwemrEmailTypeCdCi);
	}

	/**Original name: CWEMR-EMAIL-TYPE-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCwemrEmailTypeCdCi() {
		return readChar(Pos.CWEMR_EMAIL_TYPE_CD_CI);
	}

	public void setCwemrEmailTypeCd(String cwemrEmailTypeCd) {
		writeString(Pos.CWEMR_EMAIL_TYPE_CD, cwemrEmailTypeCd, Len.CWEMR_EMAIL_TYPE_CD);
	}

	/**Original name: CWEMR-EMAIL-TYPE-CD<br>*/
	public String getCwemrEmailTypeCd() {
		return readString(Pos.CWEMR_EMAIL_TYPE_CD, Len.CWEMR_EMAIL_TYPE_CD);
	}

	public void setCwemrUserIdCi(char cwemrUserIdCi) {
		writeChar(Pos.CWEMR_USER_ID_CI, cwemrUserIdCi);
	}

	/**Original name: CWEMR-USER-ID-CI<br>*/
	public char getCwemrUserIdCi() {
		return readChar(Pos.CWEMR_USER_ID_CI);
	}

	public void setCwemrUserId(String cwemrUserId) {
		writeString(Pos.CWEMR_USER_ID, cwemrUserId, Len.CWEMR_USER_ID);
	}

	/**Original name: CWEMR-USER-ID<br>*/
	public String getCwemrUserId() {
		return readString(Pos.CWEMR_USER_ID, Len.CWEMR_USER_ID);
	}

	public void setCwemrStatusCdCi(char cwemrStatusCdCi) {
		writeChar(Pos.CWEMR_STATUS_CD_CI, cwemrStatusCdCi);
	}

	/**Original name: CWEMR-STATUS-CD-CI<br>*/
	public char getCwemrStatusCdCi() {
		return readChar(Pos.CWEMR_STATUS_CD_CI);
	}

	public void setCwemrStatusCd(char cwemrStatusCd) {
		writeChar(Pos.CWEMR_STATUS_CD, cwemrStatusCd);
	}

	/**Original name: CWEMR-STATUS-CD<br>*/
	public char getCwemrStatusCd() {
		return readChar(Pos.CWEMR_STATUS_CD);
	}

	public void setCwemrTerminalIdCi(char cwemrTerminalIdCi) {
		writeChar(Pos.CWEMR_TERMINAL_ID_CI, cwemrTerminalIdCi);
	}

	/**Original name: CWEMR-TERMINAL-ID-CI<br>*/
	public char getCwemrTerminalIdCi() {
		return readChar(Pos.CWEMR_TERMINAL_ID_CI);
	}

	public void setCwemrTerminalId(String cwemrTerminalId) {
		writeString(Pos.CWEMR_TERMINAL_ID, cwemrTerminalId, Len.CWEMR_TERMINAL_ID);
	}

	/**Original name: CWEMR-TERMINAL-ID<br>*/
	public String getCwemrTerminalId() {
		return readString(Pos.CWEMR_TERMINAL_ID, Len.CWEMR_TERMINAL_ID);
	}

	public void setCwemrExpirationDtCi(char cwemrExpirationDtCi) {
		writeChar(Pos.CWEMR_EXPIRATION_DT_CI, cwemrExpirationDtCi);
	}

	/**Original name: CWEMR-EXPIRATION-DT-CI<br>*/
	public char getCwemrExpirationDtCi() {
		return readChar(Pos.CWEMR_EXPIRATION_DT_CI);
	}

	public void setCwemrExpirationDt(String cwemrExpirationDt) {
		writeString(Pos.CWEMR_EXPIRATION_DT, cwemrExpirationDt, Len.CWEMR_EXPIRATION_DT);
	}

	/**Original name: CWEMR-EXPIRATION-DT<br>*/
	public String getCwemrExpirationDt() {
		return readString(Pos.CWEMR_EXPIRATION_DT, Len.CWEMR_EXPIRATION_DT);
	}

	public void setCwemrEffectiveAcyTsCi(char cwemrEffectiveAcyTsCi) {
		writeChar(Pos.CWEMR_EFFECTIVE_ACY_TS_CI, cwemrEffectiveAcyTsCi);
	}

	/**Original name: CWEMR-EFFECTIVE-ACY-TS-CI<br>*/
	public char getCwemrEffectiveAcyTsCi() {
		return readChar(Pos.CWEMR_EFFECTIVE_ACY_TS_CI);
	}

	public void setCwemrEffectiveAcyTs(String cwemrEffectiveAcyTs) {
		writeString(Pos.CWEMR_EFFECTIVE_ACY_TS, cwemrEffectiveAcyTs, Len.CWEMR_EFFECTIVE_ACY_TS);
	}

	/**Original name: CWEMR-EFFECTIVE-ACY-TS<br>*/
	public String getCwemrEffectiveAcyTs() {
		return readString(Pos.CWEMR_EFFECTIVE_ACY_TS, Len.CWEMR_EFFECTIVE_ACY_TS);
	}

	public void setCwemrExpirationAcyTsCi(char cwemrExpirationAcyTsCi) {
		writeChar(Pos.CWEMR_EXPIRATION_ACY_TS_CI, cwemrExpirationAcyTsCi);
	}

	/**Original name: CWEMR-EXPIRATION-ACY-TS-CI<br>*/
	public char getCwemrExpirationAcyTsCi() {
		return readChar(Pos.CWEMR_EXPIRATION_ACY_TS_CI);
	}

	public void setCwemrExpirationAcyTsNi(char cwemrExpirationAcyTsNi) {
		writeChar(Pos.CWEMR_EXPIRATION_ACY_TS_NI, cwemrExpirationAcyTsNi);
	}

	/**Original name: CWEMR-EXPIRATION-ACY-TS-NI<br>*/
	public char getCwemrExpirationAcyTsNi() {
		return readChar(Pos.CWEMR_EXPIRATION_ACY_TS_NI);
	}

	public void setCwemrExpirationAcyTs(String cwemrExpirationAcyTs) {
		writeString(Pos.CWEMR_EXPIRATION_ACY_TS, cwemrExpirationAcyTs, Len.CWEMR_EXPIRATION_ACY_TS);
	}

	/**Original name: CWEMR-EXPIRATION-ACY-TS<br>*/
	public String getCwemrExpirationAcyTs() {
		return readString(Pos.CWEMR_EXPIRATION_ACY_TS, Len.CWEMR_EXPIRATION_ACY_TS);
	}

	public void setCwemrCiemEmailAdrTxtCi(char cwemrCiemEmailAdrTxtCi) {
		writeChar(Pos.CWEMR_CIEM_EMAIL_ADR_TXT_CI, cwemrCiemEmailAdrTxtCi);
	}

	/**Original name: CWEMR-CIEM-EMAIL-ADR-TXT-CI<br>*/
	public char getCwemrCiemEmailAdrTxtCi() {
		return readChar(Pos.CWEMR_CIEM_EMAIL_ADR_TXT_CI);
	}

	public void setCwemrCiemEmailAdrTxt(String cwemrCiemEmailAdrTxt) {
		writeString(Pos.CWEMR_CIEM_EMAIL_ADR_TXT, cwemrCiemEmailAdrTxt, Len.CWEMR_CIEM_EMAIL_ADR_TXT);
	}

	/**Original name: CWEMR-CIEM-EMAIL-ADR-TXT<br>*/
	public String getCwemrCiemEmailAdrTxt() {
		return readString(Pos.CWEMR_CIEM_EMAIL_ADR_TXT, Len.CWEMR_CIEM_EMAIL_ADR_TXT);
	}

	public void setCwemrMoreRowsSw(char cwemrMoreRowsSw) {
		writeChar(Pos.CWEMR_MORE_ROWS_SW, cwemrMoreRowsSw);
	}

	/**Original name: CWEMR-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	public char getCwemrMoreRowsSw() {
		return readChar(Pos.CWEMR_MORE_ROWS_SW);
	}

	public void setCwemrEmailTypeCdDesc(String cwemrEmailTypeCdDesc) {
		writeString(Pos.CWEMR_EMAIL_TYPE_CD_DESC, cwemrEmailTypeCdDesc, Len.CWEMR_EMAIL_TYPE_CD_DESC);
	}

	/**Original name: CWEMR-EMAIL-TYPE-CD-DESC<br>*/
	public String getCwemrEmailTypeCdDesc() {
		return readString(Pos.CWEMR_EMAIL_TYPE_CD_DESC, Len.CWEMR_EMAIL_TYPE_CD_DESC);
	}

	public void setCw04rClientPhoneRowFormatted(int cw04rClientPhoneRowIdx, String data) {
		int position = Pos.cw04rClientPhoneRow(cw04rClientPhoneRowIdx - 1);
		writeString(position, data, Len.CW04R_CLIENT_PHONE_ROW);
	}

	public void setCw04rClientPhoneChkSumFormatted(int cw04rClientPhoneChkSumIdx, String cw04rClientPhoneChkSum) {
		int position = Pos.cw04rClientPhoneChkSum(cw04rClientPhoneChkSumIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw04rClientPhoneChkSum, Len.CW04R_CLIENT_PHONE_CHK_SUM), Len.CW04R_CLIENT_PHONE_CHK_SUM);
	}

	/**Original name: CW04R-CLIENT-PHONE-CHK-SUM<br>*/
	public int getCw04rClientPhoneChkSum(int cw04rClientPhoneChkSumIdx) {
		int position = Pos.cw04rClientPhoneChkSum(cw04rClientPhoneChkSumIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW04R_CLIENT_PHONE_CHK_SUM);
	}

	public void setCw04rClientIdKcre(int cw04rClientIdKcreIdx, String cw04rClientIdKcre) {
		int position = Pos.cw04rClientIdKcre(cw04rClientIdKcreIdx - 1);
		writeString(position, cw04rClientIdKcre, Len.CW04R_CLIENT_ID_KCRE);
	}

	/**Original name: CW04R-CLIENT-ID-KCRE<br>*/
	public String getCw04rClientIdKcre(int cw04rClientIdKcreIdx) {
		int position = Pos.cw04rClientIdKcre(cw04rClientIdKcreIdx - 1);
		return readString(position, Len.CW04R_CLIENT_ID_KCRE);
	}

	public void setCw04rCiphPhnSeqNbrKcre(int cw04rCiphPhnSeqNbrKcreIdx, String cw04rCiphPhnSeqNbrKcre) {
		int position = Pos.cw04rCiphPhnSeqNbrKcre(cw04rCiphPhnSeqNbrKcreIdx - 1);
		writeString(position, cw04rCiphPhnSeqNbrKcre, Len.CW04R_CIPH_PHN_SEQ_NBR_KCRE);
	}

	/**Original name: CW04R-CIPH-PHN-SEQ-NBR-KCRE<br>*/
	public String getCw04rCiphPhnSeqNbrKcre(int cw04rCiphPhnSeqNbrKcreIdx) {
		int position = Pos.cw04rCiphPhnSeqNbrKcre(cw04rCiphPhnSeqNbrKcreIdx - 1);
		return readString(position, Len.CW04R_CIPH_PHN_SEQ_NBR_KCRE);
	}

	public void setCw04rTransProcessDt(int cw04rTransProcessDtIdx, String cw04rTransProcessDt) {
		int position = Pos.cw04rTransProcessDt(cw04rTransProcessDtIdx - 1);
		writeString(position, cw04rTransProcessDt, Len.CW04R_TRANS_PROCESS_DT);
	}

	/**Original name: CW04R-TRANS-PROCESS-DT<br>*/
	public String getCw04rTransProcessDt(int cw04rTransProcessDtIdx) {
		int position = Pos.cw04rTransProcessDt(cw04rTransProcessDtIdx - 1);
		return readString(position, Len.CW04R_TRANS_PROCESS_DT);
	}

	public void setCw04rClientIdCi(int cw04rClientIdCiIdx, char cw04rClientIdCi) {
		int position = Pos.cw04rClientIdCi(cw04rClientIdCiIdx - 1);
		writeChar(position, cw04rClientIdCi);
	}

	/**Original name: CW04R-CLIENT-ID-CI<br>*/
	public char getCw04rClientIdCi(int cw04rClientIdCiIdx) {
		int position = Pos.cw04rClientIdCi(cw04rClientIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rClientId(int cw04rClientIdIdx, String cw04rClientId) {
		int position = Pos.cw04rClientId(cw04rClientIdIdx - 1);
		writeString(position, cw04rClientId, Len.CW04R_CLIENT_ID);
	}

	/**Original name: CW04R-CLIENT-ID<br>*/
	public String getCw04rClientId(int cw04rClientIdIdx) {
		int position = Pos.cw04rClientId(cw04rClientIdIdx - 1);
		return readString(position, Len.CW04R_CLIENT_ID);
	}

	public void setCw04rCiphPhnSeqNbrCi(int cw04rCiphPhnSeqNbrCiIdx, char cw04rCiphPhnSeqNbrCi) {
		int position = Pos.cw04rCiphPhnSeqNbrCi(cw04rCiphPhnSeqNbrCiIdx - 1);
		writeChar(position, cw04rCiphPhnSeqNbrCi);
	}

	/**Original name: CW04R-CIPH-PHN-SEQ-NBR-CI<br>*/
	public char getCw04rCiphPhnSeqNbrCi(int cw04rCiphPhnSeqNbrCiIdx) {
		int position = Pos.cw04rCiphPhnSeqNbrCi(cw04rCiphPhnSeqNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rCiphPhnSeqNbrSign(int cw04rCiphPhnSeqNbrSignIdx, char cw04rCiphPhnSeqNbrSign) {
		int position = Pos.cw04rCiphPhnSeqNbrSign(cw04rCiphPhnSeqNbrSignIdx - 1);
		writeChar(position, cw04rCiphPhnSeqNbrSign);
	}

	/**Original name: CW04R-CIPH-PHN-SEQ-NBR-SIGN<br>*/
	public char getCw04rCiphPhnSeqNbrSign(int cw04rCiphPhnSeqNbrSignIdx) {
		int position = Pos.cw04rCiphPhnSeqNbrSign(cw04rCiphPhnSeqNbrSignIdx - 1);
		return readChar(position);
	}

	public void setCw04rCiphPhnSeqNbrFormatted(int cw04rCiphPhnSeqNbrIdx, String cw04rCiphPhnSeqNbr) {
		int position = Pos.cw04rCiphPhnSeqNbr(cw04rCiphPhnSeqNbrIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw04rCiphPhnSeqNbr, Len.CW04R_CIPH_PHN_SEQ_NBR), Len.CW04R_CIPH_PHN_SEQ_NBR);
	}

	/**Original name: CW04R-CIPH-PHN-SEQ-NBR<br>*/
	public int getCw04rCiphPhnSeqNbr(int cw04rCiphPhnSeqNbrIdx) {
		int position = Pos.cw04rCiphPhnSeqNbr(cw04rCiphPhnSeqNbrIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW04R_CIPH_PHN_SEQ_NBR);
	}

	public void setCw04rHistoryVldNbrCi(int cw04rHistoryVldNbrCiIdx, char cw04rHistoryVldNbrCi) {
		int position = Pos.cw04rHistoryVldNbrCi(cw04rHistoryVldNbrCiIdx - 1);
		writeChar(position, cw04rHistoryVldNbrCi);
	}

	/**Original name: CW04R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw04rHistoryVldNbrCi(int cw04rHistoryVldNbrCiIdx) {
		int position = Pos.cw04rHistoryVldNbrCi(cw04rHistoryVldNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rHistoryVldNbrSign(int cw04rHistoryVldNbrSignIdx, char cw04rHistoryVldNbrSign) {
		int position = Pos.cw04rHistoryVldNbrSign(cw04rHistoryVldNbrSignIdx - 1);
		writeChar(position, cw04rHistoryVldNbrSign);
	}

	/**Original name: CW04R-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw04rHistoryVldNbrSign(int cw04rHistoryVldNbrSignIdx) {
		int position = Pos.cw04rHistoryVldNbrSign(cw04rHistoryVldNbrSignIdx - 1);
		return readChar(position);
	}

	public void setCw04rHistoryVldNbrFormatted(int cw04rHistoryVldNbrIdx, String cw04rHistoryVldNbr) {
		int position = Pos.cw04rHistoryVldNbr(cw04rHistoryVldNbrIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw04rHistoryVldNbr, Len.CW04R_HISTORY_VLD_NBR), Len.CW04R_HISTORY_VLD_NBR);
	}

	/**Original name: CW04R-HISTORY-VLD-NBR<br>*/
	public int getCw04rHistoryVldNbr(int cw04rHistoryVldNbrIdx) {
		int position = Pos.cw04rHistoryVldNbr(cw04rHistoryVldNbrIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW04R_HISTORY_VLD_NBR);
	}

	public void setCw04rCiphEffDtCi(int cw04rCiphEffDtCiIdx, char cw04rCiphEffDtCi) {
		int position = Pos.cw04rCiphEffDtCi(cw04rCiphEffDtCiIdx - 1);
		writeChar(position, cw04rCiphEffDtCi);
	}

	/**Original name: CW04R-CIPH-EFF-DT-CI<br>*/
	public char getCw04rCiphEffDtCi(int cw04rCiphEffDtCiIdx) {
		int position = Pos.cw04rCiphEffDtCi(cw04rCiphEffDtCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rCiphEffDt(int cw04rCiphEffDtIdx, String cw04rCiphEffDt) {
		int position = Pos.cw04rCiphEffDt(cw04rCiphEffDtIdx - 1);
		writeString(position, cw04rCiphEffDt, Len.CW04R_CIPH_EFF_DT);
	}

	/**Original name: CW04R-CIPH-EFF-DT<br>*/
	public String getCw04rCiphEffDt(int cw04rCiphEffDtIdx) {
		int position = Pos.cw04rCiphEffDt(cw04rCiphEffDtIdx - 1);
		return readString(position, Len.CW04R_CIPH_EFF_DT);
	}

	public void setCw04rCiphPhnNbrCi(int cw04rCiphPhnNbrCiIdx, char cw04rCiphPhnNbrCi) {
		int position = Pos.cw04rCiphPhnNbrCi(cw04rCiphPhnNbrCiIdx - 1);
		writeChar(position, cw04rCiphPhnNbrCi);
	}

	/**Original name: CW04R-CIPH-PHN-NBR-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw04rCiphPhnNbrCi(int cw04rCiphPhnNbrCiIdx) {
		int position = Pos.cw04rCiphPhnNbrCi(cw04rCiphPhnNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rCiphPhnNbr(int cw04rCiphPhnNbrIdx, String cw04rCiphPhnNbr) {
		int position = Pos.cw04rCiphPhnNbr(cw04rCiphPhnNbrIdx - 1);
		writeString(position, cw04rCiphPhnNbr, Len.CW04R_CIPH_PHN_NBR);
	}

	/**Original name: CW04R-CIPH-PHN-NBR<br>*/
	public String getCw04rCiphPhnNbr(int cw04rCiphPhnNbrIdx) {
		int position = Pos.cw04rCiphPhnNbr(cw04rCiphPhnNbrIdx - 1);
		return readString(position, Len.CW04R_CIPH_PHN_NBR);
	}

	public void setCw04rPhnTypCdCi(int cw04rPhnTypCdCiIdx, char cw04rPhnTypCdCi) {
		int position = Pos.cw04rPhnTypCdCi(cw04rPhnTypCdCiIdx - 1);
		writeChar(position, cw04rPhnTypCdCi);
	}

	/**Original name: CW04R-PHN-TYP-CD-CI<br>*/
	public char getCw04rPhnTypCdCi(int cw04rPhnTypCdCiIdx) {
		int position = Pos.cw04rPhnTypCdCi(cw04rPhnTypCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rPhnTypCd(int cw04rPhnTypCdIdx, String cw04rPhnTypCd) {
		int position = Pos.cw04rPhnTypCd(cw04rPhnTypCdIdx - 1);
		writeString(position, cw04rPhnTypCd, Len.CW04R_PHN_TYP_CD);
	}

	/**Original name: CW04R-PHN-TYP-CD<br>*/
	public String getCw04rPhnTypCd(int cw04rPhnTypCdIdx) {
		int position = Pos.cw04rPhnTypCd(cw04rPhnTypCdIdx - 1);
		return readString(position, Len.CW04R_PHN_TYP_CD);
	}

	public void setCw04rCiphXrfIdCi(int cw04rCiphXrfIdCiIdx, char cw04rCiphXrfIdCi) {
		int position = Pos.cw04rCiphXrfIdCi(cw04rCiphXrfIdCiIdx - 1);
		writeChar(position, cw04rCiphXrfIdCi);
	}

	/**Original name: CW04R-CIPH-XRF-ID-CI<br>*/
	public char getCw04rCiphXrfIdCi(int cw04rCiphXrfIdCiIdx) {
		int position = Pos.cw04rCiphXrfIdCi(cw04rCiphXrfIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rCiphXrfIdNi(int cw04rCiphXrfIdNiIdx, char cw04rCiphXrfIdNi) {
		int position = Pos.cw04rCiphXrfIdNi(cw04rCiphXrfIdNiIdx - 1);
		writeChar(position, cw04rCiphXrfIdNi);
	}

	/**Original name: CW04R-CIPH-XRF-ID-NI<br>*/
	public char getCw04rCiphXrfIdNi(int cw04rCiphXrfIdNiIdx) {
		int position = Pos.cw04rCiphXrfIdNi(cw04rCiphXrfIdNiIdx - 1);
		return readChar(position);
	}

	public void setCw04rCiphXrfId(int cw04rCiphXrfIdIdx, String cw04rCiphXrfId) {
		int position = Pos.cw04rCiphXrfId(cw04rCiphXrfIdIdx - 1);
		writeString(position, cw04rCiphXrfId, Len.CW04R_CIPH_XRF_ID);
	}

	/**Original name: CW04R-CIPH-XRF-ID<br>*/
	public String getCw04rCiphXrfId(int cw04rCiphXrfIdIdx) {
		int position = Pos.cw04rCiphXrfId(cw04rCiphXrfIdIdx - 1);
		return readString(position, Len.CW04R_CIPH_XRF_ID);
	}

	public void setCw04rUserIdCi(int cw04rUserIdCiIdx, char cw04rUserIdCi) {
		int position = Pos.cw04rUserIdCi(cw04rUserIdCiIdx - 1);
		writeChar(position, cw04rUserIdCi);
	}

	/**Original name: CW04R-USER-ID-CI<br>*/
	public char getCw04rUserIdCi(int cw04rUserIdCiIdx) {
		int position = Pos.cw04rUserIdCi(cw04rUserIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rUserId(int cw04rUserIdIdx, String cw04rUserId) {
		int position = Pos.cw04rUserId(cw04rUserIdIdx - 1);
		writeString(position, cw04rUserId, Len.CW04R_USER_ID);
	}

	/**Original name: CW04R-USER-ID<br>*/
	public String getCw04rUserId(int cw04rUserIdIdx) {
		int position = Pos.cw04rUserId(cw04rUserIdIdx - 1);
		return readString(position, Len.CW04R_USER_ID);
	}

	public void setCw04rStatusCdCi(int cw04rStatusCdCiIdx, char cw04rStatusCdCi) {
		int position = Pos.cw04rStatusCdCi(cw04rStatusCdCiIdx - 1);
		writeChar(position, cw04rStatusCdCi);
	}

	/**Original name: CW04R-STATUS-CD-CI<br>*/
	public char getCw04rStatusCdCi(int cw04rStatusCdCiIdx) {
		int position = Pos.cw04rStatusCdCi(cw04rStatusCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rStatusCd(int cw04rStatusCdIdx, char cw04rStatusCd) {
		int position = Pos.cw04rStatusCd(cw04rStatusCdIdx - 1);
		writeChar(position, cw04rStatusCd);
	}

	/**Original name: CW04R-STATUS-CD<br>*/
	public char getCw04rStatusCd(int cw04rStatusCdIdx) {
		int position = Pos.cw04rStatusCd(cw04rStatusCdIdx - 1);
		return readChar(position);
	}

	public void setCw04rTerminalIdCi(int cw04rTerminalIdCiIdx, char cw04rTerminalIdCi) {
		int position = Pos.cw04rTerminalIdCi(cw04rTerminalIdCiIdx - 1);
		writeChar(position, cw04rTerminalIdCi);
	}

	/**Original name: CW04R-TERMINAL-ID-CI<br>*/
	public char getCw04rTerminalIdCi(int cw04rTerminalIdCiIdx) {
		int position = Pos.cw04rTerminalIdCi(cw04rTerminalIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rTerminalId(int cw04rTerminalIdIdx, String cw04rTerminalId) {
		int position = Pos.cw04rTerminalId(cw04rTerminalIdIdx - 1);
		writeString(position, cw04rTerminalId, Len.CW04R_TERMINAL_ID);
	}

	/**Original name: CW04R-TERMINAL-ID<br>*/
	public String getCw04rTerminalId(int cw04rTerminalIdIdx) {
		int position = Pos.cw04rTerminalId(cw04rTerminalIdIdx - 1);
		return readString(position, Len.CW04R_TERMINAL_ID);
	}

	public void setCw04rCiphExpDtCi(int cw04rCiphExpDtCiIdx, char cw04rCiphExpDtCi) {
		int position = Pos.cw04rCiphExpDtCi(cw04rCiphExpDtCiIdx - 1);
		writeChar(position, cw04rCiphExpDtCi);
	}

	/**Original name: CW04R-CIPH-EXP-DT-CI<br>*/
	public char getCw04rCiphExpDtCi(int cw04rCiphExpDtCiIdx) {
		int position = Pos.cw04rCiphExpDtCi(cw04rCiphExpDtCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rCiphExpDt(int cw04rCiphExpDtIdx, String cw04rCiphExpDt) {
		int position = Pos.cw04rCiphExpDt(cw04rCiphExpDtIdx - 1);
		writeString(position, cw04rCiphExpDt, Len.CW04R_CIPH_EXP_DT);
	}

	/**Original name: CW04R-CIPH-EXP-DT<br>*/
	public String getCw04rCiphExpDt(int cw04rCiphExpDtIdx) {
		int position = Pos.cw04rCiphExpDt(cw04rCiphExpDtIdx - 1);
		return readString(position, Len.CW04R_CIPH_EXP_DT);
	}

	public void setCw04rCiphEffAcyTsCi(int cw04rCiphEffAcyTsCiIdx, char cw04rCiphEffAcyTsCi) {
		int position = Pos.cw04rCiphEffAcyTsCi(cw04rCiphEffAcyTsCiIdx - 1);
		writeChar(position, cw04rCiphEffAcyTsCi);
	}

	/**Original name: CW04R-CIPH-EFF-ACY-TS-CI<br>*/
	public char getCw04rCiphEffAcyTsCi(int cw04rCiphEffAcyTsCiIdx) {
		int position = Pos.cw04rCiphEffAcyTsCi(cw04rCiphEffAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rCiphEffAcyTs(int cw04rCiphEffAcyTsIdx, String cw04rCiphEffAcyTs) {
		int position = Pos.cw04rCiphEffAcyTs(cw04rCiphEffAcyTsIdx - 1);
		writeString(position, cw04rCiphEffAcyTs, Len.CW04R_CIPH_EFF_ACY_TS);
	}

	/**Original name: CW04R-CIPH-EFF-ACY-TS<br>*/
	public String getCw04rCiphEffAcyTs(int cw04rCiphEffAcyTsIdx) {
		int position = Pos.cw04rCiphEffAcyTs(cw04rCiphEffAcyTsIdx - 1);
		return readString(position, Len.CW04R_CIPH_EFF_ACY_TS);
	}

	public void setCw04rCiphExpAcyTsCi(int cw04rCiphExpAcyTsCiIdx, char cw04rCiphExpAcyTsCi) {
		int position = Pos.cw04rCiphExpAcyTsCi(cw04rCiphExpAcyTsCiIdx - 1);
		writeChar(position, cw04rCiphExpAcyTsCi);
	}

	/**Original name: CW04R-CIPH-EXP-ACY-TS-CI<br>*/
	public char getCw04rCiphExpAcyTsCi(int cw04rCiphExpAcyTsCiIdx) {
		int position = Pos.cw04rCiphExpAcyTsCi(cw04rCiphExpAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCw04rCiphExpAcyTs(int cw04rCiphExpAcyTsIdx, String cw04rCiphExpAcyTs) {
		int position = Pos.cw04rCiphExpAcyTs(cw04rCiphExpAcyTsIdx - 1);
		writeString(position, cw04rCiphExpAcyTs, Len.CW04R_CIPH_EXP_ACY_TS);
	}

	/**Original name: CW04R-CIPH-EXP-ACY-TS<br>*/
	public String getCw04rCiphExpAcyTs(int cw04rCiphExpAcyTsIdx) {
		int position = Pos.cw04rCiphExpAcyTs(cw04rCiphExpAcyTsIdx - 1);
		return readString(position, Len.CW04R_CIPH_EXP_ACY_TS);
	}

	public void setCw04rMoreRowsSw(int cw04rMoreRowsSwIdx, char cw04rMoreRowsSw) {
		int position = Pos.cw04rMoreRowsSw(cw04rMoreRowsSwIdx - 1);
		writeChar(position, cw04rMoreRowsSw);
	}

	/**Original name: CW04R-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	public char getCw04rMoreRowsSw(int cw04rMoreRowsSwIdx) {
		int position = Pos.cw04rMoreRowsSw(cw04rMoreRowsSwIdx - 1);
		return readChar(position);
	}

	public void setCw04rPhnTypDesc(int cw04rPhnTypDescIdx, String cw04rPhnTypDesc) {
		int position = Pos.cw04rPhnTypDesc(cw04rPhnTypDescIdx - 1);
		writeString(position, cw04rPhnTypDesc, Len.CW04R_PHN_TYP_DESC);
	}

	/**Original name: CW04R-PHN-TYP-DESC<br>*/
	public String getCw04rPhnTypDesc(int cw04rPhnTypDescIdx) {
		int position = Pos.cw04rPhnTypDesc(cw04rPhnTypDescIdx - 1);
		return readString(position, Len.CW04R_PHN_TYP_DESC);
	}

	public void setCw08rCltObjRelationRowFormatted(int cw08rCltObjRelationRowIdx, String data) {
		int position = Pos.cw08rCltObjRelationRow(cw08rCltObjRelationRowIdx - 1);
		writeString(position, data, Len.CW08R_CLT_OBJ_RELATION_ROW);
	}

	public void setCw08rCltObjRelationCsumFormatted(int cw08rCltObjRelationCsumIdx, String cw08rCltObjRelationCsum) {
		int position = Pos.cw08rCltObjRelationCsum(cw08rCltObjRelationCsumIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw08rCltObjRelationCsum, Len.CW08R_CLT_OBJ_RELATION_CSUM), Len.CW08R_CLT_OBJ_RELATION_CSUM);
	}

	/**Original name: CW08R-CLT-OBJ-RELATION-CSUM<br>*/
	public int getCw08rCltObjRelationCsum(int cw08rCltObjRelationCsumIdx) {
		int position = Pos.cw08rCltObjRelationCsum(cw08rCltObjRelationCsumIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW08R_CLT_OBJ_RELATION_CSUM);
	}

	public void setCw08rTchObjectKeyKcre(int cw08rTchObjectKeyKcreIdx, String cw08rTchObjectKeyKcre) {
		int position = Pos.cw08rTchObjectKeyKcre(cw08rTchObjectKeyKcreIdx - 1);
		writeString(position, cw08rTchObjectKeyKcre, Len.CW08R_TCH_OBJECT_KEY_KCRE);
	}

	/**Original name: CW08R-TCH-OBJECT-KEY-KCRE<br>*/
	public String getCw08rTchObjectKeyKcre(int cw08rTchObjectKeyKcreIdx) {
		int position = Pos.cw08rTchObjectKeyKcre(cw08rTchObjectKeyKcreIdx - 1);
		return readString(position, Len.CW08R_TCH_OBJECT_KEY_KCRE);
	}

	public void setCw08rHistoryVldNbrKcre(int cw08rHistoryVldNbrKcreIdx, String cw08rHistoryVldNbrKcre) {
		int position = Pos.cw08rHistoryVldNbrKcre(cw08rHistoryVldNbrKcreIdx - 1);
		writeString(position, cw08rHistoryVldNbrKcre, Len.CW08R_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CW08R-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCw08rHistoryVldNbrKcre(int cw08rHistoryVldNbrKcreIdx) {
		int position = Pos.cw08rHistoryVldNbrKcre(cw08rHistoryVldNbrKcreIdx - 1);
		return readString(position, Len.CW08R_HISTORY_VLD_NBR_KCRE);
	}

	public void setCw08rCiorEffDtKcre(int cw08rCiorEffDtKcreIdx, String cw08rCiorEffDtKcre) {
		int position = Pos.cw08rCiorEffDtKcre(cw08rCiorEffDtKcreIdx - 1);
		writeString(position, cw08rCiorEffDtKcre, Len.CW08R_CIOR_EFF_DT_KCRE);
	}

	/**Original name: CW08R-CIOR-EFF-DT-KCRE<br>*/
	public String getCw08rCiorEffDtKcre(int cw08rCiorEffDtKcreIdx) {
		int position = Pos.cw08rCiorEffDtKcre(cw08rCiorEffDtKcreIdx - 1);
		return readString(position, Len.CW08R_CIOR_EFF_DT_KCRE);
	}

	public void setCw08rObjSysIdKcre(int cw08rObjSysIdKcreIdx, String cw08rObjSysIdKcre) {
		int position = Pos.cw08rObjSysIdKcre(cw08rObjSysIdKcreIdx - 1);
		writeString(position, cw08rObjSysIdKcre, Len.CW08R_OBJ_SYS_ID_KCRE);
	}

	/**Original name: CW08R-OBJ-SYS-ID-KCRE<br>*/
	public String getCw08rObjSysIdKcre(int cw08rObjSysIdKcreIdx) {
		int position = Pos.cw08rObjSysIdKcre(cw08rObjSysIdKcreIdx - 1);
		return readString(position, Len.CW08R_OBJ_SYS_ID_KCRE);
	}

	public void setCw08rCiorObjSeqNbrKcre(int cw08rCiorObjSeqNbrKcreIdx, String cw08rCiorObjSeqNbrKcre) {
		int position = Pos.cw08rCiorObjSeqNbrKcre(cw08rCiorObjSeqNbrKcreIdx - 1);
		writeString(position, cw08rCiorObjSeqNbrKcre, Len.CW08R_CIOR_OBJ_SEQ_NBR_KCRE);
	}

	/**Original name: CW08R-CIOR-OBJ-SEQ-NBR-KCRE<br>*/
	public String getCw08rCiorObjSeqNbrKcre(int cw08rCiorObjSeqNbrKcreIdx) {
		int position = Pos.cw08rCiorObjSeqNbrKcre(cw08rCiorObjSeqNbrKcreIdx - 1);
		return readString(position, Len.CW08R_CIOR_OBJ_SEQ_NBR_KCRE);
	}

	public void setCw08rClientIdKcre(int cw08rClientIdKcreIdx, String cw08rClientIdKcre) {
		int position = Pos.cw08rClientIdKcre(cw08rClientIdKcreIdx - 1);
		writeString(position, cw08rClientIdKcre, Len.CW08R_CLIENT_ID_KCRE);
	}

	/**Original name: CW08R-CLIENT-ID-KCRE<br>*/
	public String getCw08rClientIdKcre(int cw08rClientIdKcreIdx) {
		int position = Pos.cw08rClientIdKcre(cw08rClientIdKcreIdx - 1);
		return readString(position, Len.CW08R_CLIENT_ID_KCRE);
	}

	public void setCw08rTransProcessDt(int cw08rTransProcessDtIdx, String cw08rTransProcessDt) {
		int position = Pos.cw08rTransProcessDt(cw08rTransProcessDtIdx - 1);
		writeString(position, cw08rTransProcessDt, Len.CW08R_TRANS_PROCESS_DT);
	}

	/**Original name: CW08R-TRANS-PROCESS-DT<br>*/
	public String getCw08rTransProcessDt(int cw08rTransProcessDtIdx) {
		int position = Pos.cw08rTransProcessDt(cw08rTransProcessDtIdx - 1);
		return readString(position, Len.CW08R_TRANS_PROCESS_DT);
	}

	public void setCw08rTchObjectKeyCi(int cw08rTchObjectKeyCiIdx, char cw08rTchObjectKeyCi) {
		int position = Pos.cw08rTchObjectKeyCi(cw08rTchObjectKeyCiIdx - 1);
		writeChar(position, cw08rTchObjectKeyCi);
	}

	/**Original name: CW08R-TCH-OBJECT-KEY-CI<br>*/
	public char getCw08rTchObjectKeyCi(int cw08rTchObjectKeyCiIdx) {
		int position = Pos.cw08rTchObjectKeyCi(cw08rTchObjectKeyCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rTchObjectKey(int cw08rTchObjectKeyIdx, String cw08rTchObjectKey) {
		int position = Pos.cw08rTchObjectKey(cw08rTchObjectKeyIdx - 1);
		writeString(position, cw08rTchObjectKey, Len.CW08R_TCH_OBJECT_KEY);
	}

	/**Original name: CW08R-TCH-OBJECT-KEY<br>*/
	public String getCw08rTchObjectKey(int cw08rTchObjectKeyIdx) {
		int position = Pos.cw08rTchObjectKey(cw08rTchObjectKeyIdx - 1);
		return readString(position, Len.CW08R_TCH_OBJECT_KEY);
	}

	public void setCw08rHistoryVldNbrCi(int cw08rHistoryVldNbrCiIdx, char cw08rHistoryVldNbrCi) {
		int position = Pos.cw08rHistoryVldNbrCi(cw08rHistoryVldNbrCiIdx - 1);
		writeChar(position, cw08rHistoryVldNbrCi);
	}

	/**Original name: CW08R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw08rHistoryVldNbrCi(int cw08rHistoryVldNbrCiIdx) {
		int position = Pos.cw08rHistoryVldNbrCi(cw08rHistoryVldNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rHistoryVldNbrSign(int cw08rHistoryVldNbrSignIdx, char cw08rHistoryVldNbrSign) {
		int position = Pos.cw08rHistoryVldNbrSign(cw08rHistoryVldNbrSignIdx - 1);
		writeChar(position, cw08rHistoryVldNbrSign);
	}

	/**Original name: CW08R-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw08rHistoryVldNbrSign(int cw08rHistoryVldNbrSignIdx) {
		int position = Pos.cw08rHistoryVldNbrSign(cw08rHistoryVldNbrSignIdx - 1);
		return readChar(position);
	}

	public void setCw08rHistoryVldNbrFormatted(int cw08rHistoryVldNbrIdx, String cw08rHistoryVldNbr) {
		int position = Pos.cw08rHistoryVldNbr(cw08rHistoryVldNbrIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw08rHistoryVldNbr, Len.CW08R_HISTORY_VLD_NBR), Len.CW08R_HISTORY_VLD_NBR);
	}

	/**Original name: CW08R-HISTORY-VLD-NBR<br>*/
	public int getCw08rHistoryVldNbr(int cw08rHistoryVldNbrIdx) {
		int position = Pos.cw08rHistoryVldNbr(cw08rHistoryVldNbrIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW08R_HISTORY_VLD_NBR);
	}

	public void setCw08rCiorEffDtCi(int cw08rCiorEffDtCiIdx, char cw08rCiorEffDtCi) {
		int position = Pos.cw08rCiorEffDtCi(cw08rCiorEffDtCiIdx - 1);
		writeChar(position, cw08rCiorEffDtCi);
	}

	/**Original name: CW08R-CIOR-EFF-DT-CI<br>*/
	public char getCw08rCiorEffDtCi(int cw08rCiorEffDtCiIdx) {
		int position = Pos.cw08rCiorEffDtCi(cw08rCiorEffDtCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rCiorEffDt(int cw08rCiorEffDtIdx, String cw08rCiorEffDt) {
		int position = Pos.cw08rCiorEffDt(cw08rCiorEffDtIdx - 1);
		writeString(position, cw08rCiorEffDt, Len.CW08R_CIOR_EFF_DT);
	}

	/**Original name: CW08R-CIOR-EFF-DT<br>*/
	public String getCw08rCiorEffDt(int cw08rCiorEffDtIdx) {
		int position = Pos.cw08rCiorEffDt(cw08rCiorEffDtIdx - 1);
		return readString(position, Len.CW08R_CIOR_EFF_DT);
	}

	public void setCw08rObjSysIdCi(int cw08rObjSysIdCiIdx, char cw08rObjSysIdCi) {
		int position = Pos.cw08rObjSysIdCi(cw08rObjSysIdCiIdx - 1);
		writeChar(position, cw08rObjSysIdCi);
	}

	/**Original name: CW08R-OBJ-SYS-ID-CI<br>*/
	public char getCw08rObjSysIdCi(int cw08rObjSysIdCiIdx) {
		int position = Pos.cw08rObjSysIdCi(cw08rObjSysIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rObjSysId(int cw08rObjSysIdIdx, String cw08rObjSysId) {
		int position = Pos.cw08rObjSysId(cw08rObjSysIdIdx - 1);
		writeString(position, cw08rObjSysId, Len.CW08R_OBJ_SYS_ID);
	}

	/**Original name: CW08R-OBJ-SYS-ID<br>*/
	public String getCw08rObjSysId(int cw08rObjSysIdIdx) {
		int position = Pos.cw08rObjSysId(cw08rObjSysIdIdx - 1);
		return readString(position, Len.CW08R_OBJ_SYS_ID);
	}

	public void setCw08rCiorObjSeqNbrCi(int cw08rCiorObjSeqNbrCiIdx, char cw08rCiorObjSeqNbrCi) {
		int position = Pos.cw08rCiorObjSeqNbrCi(cw08rCiorObjSeqNbrCiIdx - 1);
		writeChar(position, cw08rCiorObjSeqNbrCi);
	}

	/**Original name: CW08R-CIOR-OBJ-SEQ-NBR-CI<br>*/
	public char getCw08rCiorObjSeqNbrCi(int cw08rCiorObjSeqNbrCiIdx) {
		int position = Pos.cw08rCiorObjSeqNbrCi(cw08rCiorObjSeqNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rCiorObjSeqNbrSign(int cw08rCiorObjSeqNbrSignIdx, char cw08rCiorObjSeqNbrSign) {
		int position = Pos.cw08rCiorObjSeqNbrSign(cw08rCiorObjSeqNbrSignIdx - 1);
		writeChar(position, cw08rCiorObjSeqNbrSign);
	}

	/**Original name: CW08R-CIOR-OBJ-SEQ-NBR-SIGN<br>*/
	public char getCw08rCiorObjSeqNbrSign(int cw08rCiorObjSeqNbrSignIdx) {
		int position = Pos.cw08rCiorObjSeqNbrSign(cw08rCiorObjSeqNbrSignIdx - 1);
		return readChar(position);
	}

	public void setCw08rCiorObjSeqNbrFormatted(int cw08rCiorObjSeqNbrIdx, String cw08rCiorObjSeqNbr) {
		int position = Pos.cw08rCiorObjSeqNbr(cw08rCiorObjSeqNbrIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw08rCiorObjSeqNbr, Len.CW08R_CIOR_OBJ_SEQ_NBR), Len.CW08R_CIOR_OBJ_SEQ_NBR);
	}

	/**Original name: CW08R-CIOR-OBJ-SEQ-NBR<br>*/
	public int getCw08rCiorObjSeqNbr(int cw08rCiorObjSeqNbrIdx) {
		int position = Pos.cw08rCiorObjSeqNbr(cw08rCiorObjSeqNbrIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW08R_CIOR_OBJ_SEQ_NBR);
	}

	public String getCw08rCiorObjSeqNbrFormatted(int cw08rCiorObjSeqNbrIdx) {
		int position = Pos.cw08rCiorObjSeqNbr(cw08rCiorObjSeqNbrIdx - 1);
		return readFixedString(position, Len.CW08R_CIOR_OBJ_SEQ_NBR);
	}

	public void setCw08rClientIdCi(int cw08rClientIdCiIdx, char cw08rClientIdCi) {
		int position = Pos.cw08rClientIdCi(cw08rClientIdCiIdx - 1);
		writeChar(position, cw08rClientIdCi);
	}

	/**Original name: CW08R-CLIENT-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw08rClientIdCi(int cw08rClientIdCiIdx) {
		int position = Pos.cw08rClientIdCi(cw08rClientIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rClientId(int cw08rClientIdIdx, String cw08rClientId) {
		int position = Pos.cw08rClientId(cw08rClientIdIdx - 1);
		writeString(position, cw08rClientId, Len.CW08R_CLIENT_ID);
	}

	/**Original name: CW08R-CLIENT-ID<br>*/
	public String getCw08rClientId(int cw08rClientIdIdx) {
		int position = Pos.cw08rClientId(cw08rClientIdIdx - 1);
		return readString(position, Len.CW08R_CLIENT_ID);
	}

	public void setCw08rRltTypCdCi(int cw08rRltTypCdCiIdx, char cw08rRltTypCdCi) {
		int position = Pos.cw08rRltTypCdCi(cw08rRltTypCdCiIdx - 1);
		writeChar(position, cw08rRltTypCdCi);
	}

	/**Original name: CW08R-RLT-TYP-CD-CI<br>*/
	public char getCw08rRltTypCdCi(int cw08rRltTypCdCiIdx) {
		int position = Pos.cw08rRltTypCdCi(cw08rRltTypCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rRltTypCd(int cw08rRltTypCdIdx, String cw08rRltTypCd) {
		int position = Pos.cw08rRltTypCd(cw08rRltTypCdIdx - 1);
		writeString(position, cw08rRltTypCd, Len.CW08R_RLT_TYP_CD);
	}

	/**Original name: CW08R-RLT-TYP-CD<br>*/
	public String getCw08rRltTypCd(int cw08rRltTypCdIdx) {
		int position = Pos.cw08rRltTypCd(cw08rRltTypCdIdx - 1);
		return readString(position, Len.CW08R_RLT_TYP_CD);
	}

	public void setCw08rObjCdCi(int cw08rObjCdCiIdx, char cw08rObjCdCi) {
		int position = Pos.cw08rObjCdCi(cw08rObjCdCiIdx - 1);
		writeChar(position, cw08rObjCdCi);
	}

	/**Original name: CW08R-OBJ-CD-CI<br>*/
	public char getCw08rObjCdCi(int cw08rObjCdCiIdx) {
		int position = Pos.cw08rObjCdCi(cw08rObjCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rObjCd(int cw08rObjCdIdx, String cw08rObjCd) {
		int position = Pos.cw08rObjCd(cw08rObjCdIdx - 1);
		writeString(position, cw08rObjCd, Len.CW08R_OBJ_CD);
	}

	/**Original name: CW08R-OBJ-CD<br>*/
	public String getCw08rObjCd(int cw08rObjCdIdx) {
		int position = Pos.cw08rObjCd(cw08rObjCdIdx - 1);
		return readString(position, Len.CW08R_OBJ_CD);
	}

	public void setCw08rCiorShwObjKeyCi(int cw08rCiorShwObjKeyCiIdx, char cw08rCiorShwObjKeyCi) {
		int position = Pos.cw08rCiorShwObjKeyCi(cw08rCiorShwObjKeyCiIdx - 1);
		writeChar(position, cw08rCiorShwObjKeyCi);
	}

	/**Original name: CW08R-CIOR-SHW-OBJ-KEY-CI<br>*/
	public char getCw08rCiorShwObjKeyCi(int cw08rCiorShwObjKeyCiIdx) {
		int position = Pos.cw08rCiorShwObjKeyCi(cw08rCiorShwObjKeyCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rCiorShwObjKey(int cw08rCiorShwObjKeyIdx, String cw08rCiorShwObjKey) {
		int position = Pos.cw08rCiorShwObjKey(cw08rCiorShwObjKeyIdx - 1);
		writeString(position, cw08rCiorShwObjKey, Len.CW08R_CIOR_SHW_OBJ_KEY);
	}

	/**Original name: CW08R-CIOR-SHW-OBJ-KEY<br>*/
	public String getCw08rCiorShwObjKey(int cw08rCiorShwObjKeyIdx) {
		int position = Pos.cw08rCiorShwObjKey(cw08rCiorShwObjKeyIdx - 1);
		return readString(position, Len.CW08R_CIOR_SHW_OBJ_KEY);
	}

	public String getCw08rCiorShwObjKeyFormatted(int cw08rCiorShwObjKeyIdx) {
		return Functions.padBlanks(getCw08rCiorShwObjKey(cw08rCiorShwObjKeyIdx), Len.CW08R_CIOR_SHW_OBJ_KEY);
	}

	public void setCw08rAdrSeqNbrCi(int cw08rAdrSeqNbrCiIdx, char cw08rAdrSeqNbrCi) {
		int position = Pos.cw08rAdrSeqNbrCi(cw08rAdrSeqNbrCiIdx - 1);
		writeChar(position, cw08rAdrSeqNbrCi);
	}

	/**Original name: CW08R-ADR-SEQ-NBR-CI<br>*/
	public char getCw08rAdrSeqNbrCi(int cw08rAdrSeqNbrCiIdx) {
		int position = Pos.cw08rAdrSeqNbrCi(cw08rAdrSeqNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rAdrSeqNbrSign(int cw08rAdrSeqNbrSignIdx, char cw08rAdrSeqNbrSign) {
		int position = Pos.cw08rAdrSeqNbrSign(cw08rAdrSeqNbrSignIdx - 1);
		writeChar(position, cw08rAdrSeqNbrSign);
	}

	/**Original name: CW08R-ADR-SEQ-NBR-SIGN<br>*/
	public char getCw08rAdrSeqNbrSign(int cw08rAdrSeqNbrSignIdx) {
		int position = Pos.cw08rAdrSeqNbrSign(cw08rAdrSeqNbrSignIdx - 1);
		return readChar(position);
	}

	public void setCw08rAdrSeqNbrFormatted(int cw08rAdrSeqNbrIdx, String cw08rAdrSeqNbr) {
		int position = Pos.cw08rAdrSeqNbr(cw08rAdrSeqNbrIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw08rAdrSeqNbr, Len.CW08R_ADR_SEQ_NBR), Len.CW08R_ADR_SEQ_NBR);
	}

	/**Original name: CW08R-ADR-SEQ-NBR<br>*/
	public int getCw08rAdrSeqNbr(int cw08rAdrSeqNbrIdx) {
		int position = Pos.cw08rAdrSeqNbr(cw08rAdrSeqNbrIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW08R_ADR_SEQ_NBR);
	}

	public void setCw08rUserIdCi(int cw08rUserIdCiIdx, char cw08rUserIdCi) {
		int position = Pos.cw08rUserIdCi(cw08rUserIdCiIdx - 1);
		writeChar(position, cw08rUserIdCi);
	}

	/**Original name: CW08R-USER-ID-CI<br>*/
	public char getCw08rUserIdCi(int cw08rUserIdCiIdx) {
		int position = Pos.cw08rUserIdCi(cw08rUserIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rUserId(int cw08rUserIdIdx, String cw08rUserId) {
		int position = Pos.cw08rUserId(cw08rUserIdIdx - 1);
		writeString(position, cw08rUserId, Len.CW08R_USER_ID);
	}

	/**Original name: CW08R-USER-ID<br>*/
	public String getCw08rUserId(int cw08rUserIdIdx) {
		int position = Pos.cw08rUserId(cw08rUserIdIdx - 1);
		return readString(position, Len.CW08R_USER_ID);
	}

	public void setCw08rStatusCdCi(int cw08rStatusCdCiIdx, char cw08rStatusCdCi) {
		int position = Pos.cw08rStatusCdCi(cw08rStatusCdCiIdx - 1);
		writeChar(position, cw08rStatusCdCi);
	}

	/**Original name: CW08R-STATUS-CD-CI<br>*/
	public char getCw08rStatusCdCi(int cw08rStatusCdCiIdx) {
		int position = Pos.cw08rStatusCdCi(cw08rStatusCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rStatusCd(int cw08rStatusCdIdx, char cw08rStatusCd) {
		int position = Pos.cw08rStatusCd(cw08rStatusCdIdx - 1);
		writeChar(position, cw08rStatusCd);
	}

	/**Original name: CW08R-STATUS-CD<br>*/
	public char getCw08rStatusCd(int cw08rStatusCdIdx) {
		int position = Pos.cw08rStatusCd(cw08rStatusCdIdx - 1);
		return readChar(position);
	}

	public void setCw08rTerminalIdCi(int cw08rTerminalIdCiIdx, char cw08rTerminalIdCi) {
		int position = Pos.cw08rTerminalIdCi(cw08rTerminalIdCiIdx - 1);
		writeChar(position, cw08rTerminalIdCi);
	}

	/**Original name: CW08R-TERMINAL-ID-CI<br>*/
	public char getCw08rTerminalIdCi(int cw08rTerminalIdCiIdx) {
		int position = Pos.cw08rTerminalIdCi(cw08rTerminalIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rTerminalId(int cw08rTerminalIdIdx, String cw08rTerminalId) {
		int position = Pos.cw08rTerminalId(cw08rTerminalIdIdx - 1);
		writeString(position, cw08rTerminalId, Len.CW08R_TERMINAL_ID);
	}

	/**Original name: CW08R-TERMINAL-ID<br>*/
	public String getCw08rTerminalId(int cw08rTerminalIdIdx) {
		int position = Pos.cw08rTerminalId(cw08rTerminalIdIdx - 1);
		return readString(position, Len.CW08R_TERMINAL_ID);
	}

	public void setCw08rCiorExpDtCi(int cw08rCiorExpDtCiIdx, char cw08rCiorExpDtCi) {
		int position = Pos.cw08rCiorExpDtCi(cw08rCiorExpDtCiIdx - 1);
		writeChar(position, cw08rCiorExpDtCi);
	}

	/**Original name: CW08R-CIOR-EXP-DT-CI<br>*/
	public char getCw08rCiorExpDtCi(int cw08rCiorExpDtCiIdx) {
		int position = Pos.cw08rCiorExpDtCi(cw08rCiorExpDtCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rCiorExpDt(int cw08rCiorExpDtIdx, String cw08rCiorExpDt) {
		int position = Pos.cw08rCiorExpDt(cw08rCiorExpDtIdx - 1);
		writeString(position, cw08rCiorExpDt, Len.CW08R_CIOR_EXP_DT);
	}

	/**Original name: CW08R-CIOR-EXP-DT<br>*/
	public String getCw08rCiorExpDt(int cw08rCiorExpDtIdx) {
		int position = Pos.cw08rCiorExpDt(cw08rCiorExpDtIdx - 1);
		return readString(position, Len.CW08R_CIOR_EXP_DT);
	}

	public void setCw08rCiorEffAcyTsCi(int cw08rCiorEffAcyTsCiIdx, char cw08rCiorEffAcyTsCi) {
		int position = Pos.cw08rCiorEffAcyTsCi(cw08rCiorEffAcyTsCiIdx - 1);
		writeChar(position, cw08rCiorEffAcyTsCi);
	}

	/**Original name: CW08R-CIOR-EFF-ACY-TS-CI<br>*/
	public char getCw08rCiorEffAcyTsCi(int cw08rCiorEffAcyTsCiIdx) {
		int position = Pos.cw08rCiorEffAcyTsCi(cw08rCiorEffAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rCiorEffAcyTs(int cw08rCiorEffAcyTsIdx, String cw08rCiorEffAcyTs) {
		int position = Pos.cw08rCiorEffAcyTs(cw08rCiorEffAcyTsIdx - 1);
		writeString(position, cw08rCiorEffAcyTs, Len.CW08R_CIOR_EFF_ACY_TS);
	}

	/**Original name: CW08R-CIOR-EFF-ACY-TS<br>*/
	public String getCw08rCiorEffAcyTs(int cw08rCiorEffAcyTsIdx) {
		int position = Pos.cw08rCiorEffAcyTs(cw08rCiorEffAcyTsIdx - 1);
		return readString(position, Len.CW08R_CIOR_EFF_ACY_TS);
	}

	public void setCw08rCiorExpAcyTsCi(int cw08rCiorExpAcyTsCiIdx, char cw08rCiorExpAcyTsCi) {
		int position = Pos.cw08rCiorExpAcyTsCi(cw08rCiorExpAcyTsCiIdx - 1);
		writeChar(position, cw08rCiorExpAcyTsCi);
	}

	/**Original name: CW08R-CIOR-EXP-ACY-TS-CI<br>*/
	public char getCw08rCiorExpAcyTsCi(int cw08rCiorExpAcyTsCiIdx) {
		int position = Pos.cw08rCiorExpAcyTsCi(cw08rCiorExpAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCw08rCiorExpAcyTs(int cw08rCiorExpAcyTsIdx, String cw08rCiorExpAcyTs) {
		int position = Pos.cw08rCiorExpAcyTs(cw08rCiorExpAcyTsIdx - 1);
		writeString(position, cw08rCiorExpAcyTs, Len.CW08R_CIOR_EXP_ACY_TS);
	}

	/**Original name: CW08R-CIOR-EXP-ACY-TS<br>*/
	public String getCw08rCiorExpAcyTs(int cw08rCiorExpAcyTsIdx) {
		int position = Pos.cw08rCiorExpAcyTs(cw08rCiorExpAcyTsIdx - 1);
		return readString(position, Len.CW08R_CIOR_EXP_ACY_TS);
	}

	public void setCw08rAppType(int cw08rAppTypeIdx, char cw08rAppType) {
		int position = Pos.cw08rAppType(cw08rAppTypeIdx - 1);
		writeChar(position, cw08rAppType);
	}

	/**Original name: CW08R-APP-TYPE<br>*/
	public char getCw08rAppType(int cw08rAppTypeIdx) {
		int position = Pos.cw08rAppType(cw08rAppTypeIdx - 1);
		return readChar(position);
	}

	public void setCw08rBusObjNm(int cw08rBusObjNmIdx, String cw08rBusObjNm) {
		int position = Pos.cw08rBusObjNm(cw08rBusObjNmIdx - 1);
		writeString(position, cw08rBusObjNm, Len.CW08R_BUS_OBJ_NM);
	}

	/**Original name: CW08R-BUS-OBJ-NM<br>*/
	public String getCw08rBusObjNm(int cw08rBusObjNmIdx) {
		int position = Pos.cw08rBusObjNm(cw08rBusObjNmIdx - 1);
		return readString(position, Len.CW08R_BUS_OBJ_NM);
	}

	public void setCw08rObjDesc(int cw08rObjDescIdx, String cw08rObjDesc) {
		int position = Pos.cw08rObjDesc(cw08rObjDescIdx - 1);
		writeString(position, cw08rObjDesc, Len.CW08R_OBJ_DESC);
	}

	/**Original name: CW08R-OBJ-DESC<br>*/
	public String getCw08rObjDesc(int cw08rObjDescIdx) {
		int position = Pos.cw08rObjDesc(cw08rObjDescIdx - 1);
		return readString(position, Len.CW08R_OBJ_DESC);
	}

	public void setCw11rCltRefRelationRowFormatted(String data) {
		writeString(Pos.CW11R_CLT_REF_RELATION_ROW, data, Len.CW11R_CLT_REF_RELATION_ROW);
	}

	public void setCw11rCltRefRelationCsumFormatted(String cw11rCltRefRelationCsum) {
		writeString(Pos.CW11R_CLT_REF_RELATION_CSUM, Trunc.toUnsignedNumeric(cw11rCltRefRelationCsum, Len.CW11R_CLT_REF_RELATION_CSUM),
				Len.CW11R_CLT_REF_RELATION_CSUM);
	}

	/**Original name: CW11R-CLT-REF-RELATION-CSUM<br>*/
	public int getCw11rCltRefRelationCsum() {
		return readNumDispUnsignedInt(Pos.CW11R_CLT_REF_RELATION_CSUM, Len.CW11R_CLT_REF_RELATION_CSUM);
	}

	public void setCw11rClientIdKcre(String cw11rClientIdKcre) {
		writeString(Pos.CW11R_CLIENT_ID_KCRE, cw11rClientIdKcre, Len.CW11R_CLIENT_ID_KCRE);
	}

	/**Original name: CW11R-CLIENT-ID-KCRE<br>*/
	public String getCw11rClientIdKcre() {
		return readString(Pos.CW11R_CLIENT_ID_KCRE, Len.CW11R_CLIENT_ID_KCRE);
	}

	public void setCw11rCirfRefSeqNbrKcre(String cw11rCirfRefSeqNbrKcre) {
		writeString(Pos.CW11R_CIRF_REF_SEQ_NBR_KCRE, cw11rCirfRefSeqNbrKcre, Len.CW11R_CIRF_REF_SEQ_NBR_KCRE);
	}

	/**Original name: CW11R-CIRF-REF-SEQ-NBR-KCRE<br>*/
	public String getCw11rCirfRefSeqNbrKcre() {
		return readString(Pos.CW11R_CIRF_REF_SEQ_NBR_KCRE, Len.CW11R_CIRF_REF_SEQ_NBR_KCRE);
	}

	public void setCw11rHistoryVldNbrKcre(String cw11rHistoryVldNbrKcre) {
		writeString(Pos.CW11R_HISTORY_VLD_NBR_KCRE, cw11rHistoryVldNbrKcre, Len.CW11R_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CW11R-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCw11rHistoryVldNbrKcre() {
		return readString(Pos.CW11R_HISTORY_VLD_NBR_KCRE, Len.CW11R_HISTORY_VLD_NBR_KCRE);
	}

	public void setCw11rCirfEffDtKcre(String cw11rCirfEffDtKcre) {
		writeString(Pos.CW11R_CIRF_EFF_DT_KCRE, cw11rCirfEffDtKcre, Len.CW11R_CIRF_EFF_DT_KCRE);
	}

	/**Original name: CW11R-CIRF-EFF-DT-KCRE<br>*/
	public String getCw11rCirfEffDtKcre() {
		return readString(Pos.CW11R_CIRF_EFF_DT_KCRE, Len.CW11R_CIRF_EFF_DT_KCRE);
	}

	public void setCw11rTransProcessDt(String cw11rTransProcessDt) {
		writeString(Pos.CW11R_TRANS_PROCESS_DT, cw11rTransProcessDt, Len.CW11R_TRANS_PROCESS_DT);
	}

	/**Original name: CW11R-TRANS-PROCESS-DT<br>*/
	public String getCw11rTransProcessDt() {
		return readString(Pos.CW11R_TRANS_PROCESS_DT, Len.CW11R_TRANS_PROCESS_DT);
	}

	public void setCw11rClientIdCi(char cw11rClientIdCi) {
		writeChar(Pos.CW11R_CLIENT_ID_CI, cw11rClientIdCi);
	}

	/**Original name: CW11R-CLIENT-ID-CI<br>*/
	public char getCw11rClientIdCi() {
		return readChar(Pos.CW11R_CLIENT_ID_CI);
	}

	public void setCw11rClientId(String cw11rClientId) {
		writeString(Pos.CW11R_CLIENT_ID, cw11rClientId, Len.CW11R_CLIENT_ID);
	}

	/**Original name: CW11R-CLIENT-ID<br>*/
	public String getCw11rClientId() {
		return readString(Pos.CW11R_CLIENT_ID, Len.CW11R_CLIENT_ID);
	}

	public void setCw11rCirfRefSeqNbrCi(char cw11rCirfRefSeqNbrCi) {
		writeChar(Pos.CW11R_CIRF_REF_SEQ_NBR_CI, cw11rCirfRefSeqNbrCi);
	}

	/**Original name: CW11R-CIRF-REF-SEQ-NBR-CI<br>*/
	public char getCw11rCirfRefSeqNbrCi() {
		return readChar(Pos.CW11R_CIRF_REF_SEQ_NBR_CI);
	}

	public void setCw11rCirfRefSeqNbrSign(char cw11rCirfRefSeqNbrSign) {
		writeChar(Pos.CW11R_CIRF_REF_SEQ_NBR_SIGN, cw11rCirfRefSeqNbrSign);
	}

	/**Original name: CW11R-CIRF-REF-SEQ-NBR-SIGN<br>*/
	public char getCw11rCirfRefSeqNbrSign() {
		return readChar(Pos.CW11R_CIRF_REF_SEQ_NBR_SIGN);
	}

	public void setCw11rCirfRefSeqNbrFormatted(String cw11rCirfRefSeqNbr) {
		writeString(Pos.CW11R_CIRF_REF_SEQ_NBR, Trunc.toUnsignedNumeric(cw11rCirfRefSeqNbr, Len.CW11R_CIRF_REF_SEQ_NBR), Len.CW11R_CIRF_REF_SEQ_NBR);
	}

	/**Original name: CW11R-CIRF-REF-SEQ-NBR<br>*/
	public int getCw11rCirfRefSeqNbr() {
		return readNumDispUnsignedInt(Pos.CW11R_CIRF_REF_SEQ_NBR, Len.CW11R_CIRF_REF_SEQ_NBR);
	}

	public void setCw11rHistoryVldNbrCi(char cw11rHistoryVldNbrCi) {
		writeChar(Pos.CW11R_HISTORY_VLD_NBR_CI, cw11rHistoryVldNbrCi);
	}

	/**Original name: CW11R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw11rHistoryVldNbrCi() {
		return readChar(Pos.CW11R_HISTORY_VLD_NBR_CI);
	}

	public void setCw11rHistoryVldNbrSign(char cw11rHistoryVldNbrSign) {
		writeChar(Pos.CW11R_HISTORY_VLD_NBR_SIGN, cw11rHistoryVldNbrSign);
	}

	/**Original name: CW11R-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw11rHistoryVldNbrSign() {
		return readChar(Pos.CW11R_HISTORY_VLD_NBR_SIGN);
	}

	public void setCw11rHistoryVldNbrFormatted(String cw11rHistoryVldNbr) {
		writeString(Pos.CW11R_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cw11rHistoryVldNbr, Len.CW11R_HISTORY_VLD_NBR), Len.CW11R_HISTORY_VLD_NBR);
	}

	/**Original name: CW11R-HISTORY-VLD-NBR<br>*/
	public int getCw11rHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CW11R_HISTORY_VLD_NBR, Len.CW11R_HISTORY_VLD_NBR);
	}

	public void setCw11rCirfEffDtCi(char cw11rCirfEffDtCi) {
		writeChar(Pos.CW11R_CIRF_EFF_DT_CI, cw11rCirfEffDtCi);
	}

	/**Original name: CW11R-CIRF-EFF-DT-CI<br>*/
	public char getCw11rCirfEffDtCi() {
		return readChar(Pos.CW11R_CIRF_EFF_DT_CI);
	}

	public void setCw11rCirfEffDt(String cw11rCirfEffDt) {
		writeString(Pos.CW11R_CIRF_EFF_DT, cw11rCirfEffDt, Len.CW11R_CIRF_EFF_DT);
	}

	/**Original name: CW11R-CIRF-EFF-DT<br>*/
	public String getCw11rCirfEffDt() {
		return readString(Pos.CW11R_CIRF_EFF_DT, Len.CW11R_CIRF_EFF_DT);
	}

	public void setCw11rCirfRefIdCi(char cw11rCirfRefIdCi) {
		writeChar(Pos.CW11R_CIRF_REF_ID_CI, cw11rCirfRefIdCi);
	}

	/**Original name: CW11R-CIRF-REF-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw11rCirfRefIdCi() {
		return readChar(Pos.CW11R_CIRF_REF_ID_CI);
	}

	public void setCw11rCirfRefId(String cw11rCirfRefId) {
		writeString(Pos.CW11R_CIRF_REF_ID, cw11rCirfRefId, Len.CW11R_CIRF_REF_ID);
	}

	/**Original name: CW11R-CIRF-REF-ID<br>*/
	public String getCw11rCirfRefId() {
		return readString(Pos.CW11R_CIRF_REF_ID, Len.CW11R_CIRF_REF_ID);
	}

	public void setCw11rRefTypCdCi(char cw11rRefTypCdCi) {
		writeChar(Pos.CW11R_REF_TYP_CD_CI, cw11rRefTypCdCi);
	}

	/**Original name: CW11R-REF-TYP-CD-CI<br>*/
	public char getCw11rRefTypCdCi() {
		return readChar(Pos.CW11R_REF_TYP_CD_CI);
	}

	public void setCw11rRefTypCd(String cw11rRefTypCd) {
		writeString(Pos.CW11R_REF_TYP_CD, cw11rRefTypCd, Len.CW11R_REF_TYP_CD);
	}

	/**Original name: CW11R-REF-TYP-CD<br>*/
	public String getCw11rRefTypCd() {
		return readString(Pos.CW11R_REF_TYP_CD, Len.CW11R_REF_TYP_CD);
	}

	public void setCw11rCirfExpDtCi(char cw11rCirfExpDtCi) {
		writeChar(Pos.CW11R_CIRF_EXP_DT_CI, cw11rCirfExpDtCi);
	}

	/**Original name: CW11R-CIRF-EXP-DT-CI<br>*/
	public char getCw11rCirfExpDtCi() {
		return readChar(Pos.CW11R_CIRF_EXP_DT_CI);
	}

	public void setCw11rCirfExpDt(String cw11rCirfExpDt) {
		writeString(Pos.CW11R_CIRF_EXP_DT, cw11rCirfExpDt, Len.CW11R_CIRF_EXP_DT);
	}

	/**Original name: CW11R-CIRF-EXP-DT<br>*/
	public String getCw11rCirfExpDt() {
		return readString(Pos.CW11R_CIRF_EXP_DT, Len.CW11R_CIRF_EXP_DT);
	}

	public void setCw11rUserIdCi(char cw11rUserIdCi) {
		writeChar(Pos.CW11R_USER_ID_CI, cw11rUserIdCi);
	}

	/**Original name: CW11R-USER-ID-CI<br>*/
	public char getCw11rUserIdCi() {
		return readChar(Pos.CW11R_USER_ID_CI);
	}

	public void setCw11rUserId(String cw11rUserId) {
		writeString(Pos.CW11R_USER_ID, cw11rUserId, Len.CW11R_USER_ID);
	}

	/**Original name: CW11R-USER-ID<br>*/
	public String getCw11rUserId() {
		return readString(Pos.CW11R_USER_ID, Len.CW11R_USER_ID);
	}

	public void setCw11rStatusCdCi(char cw11rStatusCdCi) {
		writeChar(Pos.CW11R_STATUS_CD_CI, cw11rStatusCdCi);
	}

	/**Original name: CW11R-STATUS-CD-CI<br>*/
	public char getCw11rStatusCdCi() {
		return readChar(Pos.CW11R_STATUS_CD_CI);
	}

	public void setCw11rStatusCd(char cw11rStatusCd) {
		writeChar(Pos.CW11R_STATUS_CD, cw11rStatusCd);
	}

	/**Original name: CW11R-STATUS-CD<br>*/
	public char getCw11rStatusCd() {
		return readChar(Pos.CW11R_STATUS_CD);
	}

	public void setCw11rTerminalIdCi(char cw11rTerminalIdCi) {
		writeChar(Pos.CW11R_TERMINAL_ID_CI, cw11rTerminalIdCi);
	}

	/**Original name: CW11R-TERMINAL-ID-CI<br>*/
	public char getCw11rTerminalIdCi() {
		return readChar(Pos.CW11R_TERMINAL_ID_CI);
	}

	public void setCw11rTerminalId(String cw11rTerminalId) {
		writeString(Pos.CW11R_TERMINAL_ID, cw11rTerminalId, Len.CW11R_TERMINAL_ID);
	}

	/**Original name: CW11R-TERMINAL-ID<br>*/
	public String getCw11rTerminalId() {
		return readString(Pos.CW11R_TERMINAL_ID, Len.CW11R_TERMINAL_ID);
	}

	public void setCw11rCirfEffAcyTsCi(char cw11rCirfEffAcyTsCi) {
		writeChar(Pos.CW11R_CIRF_EFF_ACY_TS_CI, cw11rCirfEffAcyTsCi);
	}

	/**Original name: CW11R-CIRF-EFF-ACY-TS-CI<br>*/
	public char getCw11rCirfEffAcyTsCi() {
		return readChar(Pos.CW11R_CIRF_EFF_ACY_TS_CI);
	}

	public void setCw11rCirfEffAcyTs(String cw11rCirfEffAcyTs) {
		writeString(Pos.CW11R_CIRF_EFF_ACY_TS, cw11rCirfEffAcyTs, Len.CW11R_CIRF_EFF_ACY_TS);
	}

	/**Original name: CW11R-CIRF-EFF-ACY-TS<br>*/
	public String getCw11rCirfEffAcyTs() {
		return readString(Pos.CW11R_CIRF_EFF_ACY_TS, Len.CW11R_CIRF_EFF_ACY_TS);
	}

	public void setCw11rCirfExpAcyTsCi(char cw11rCirfExpAcyTsCi) {
		writeChar(Pos.CW11R_CIRF_EXP_ACY_TS_CI, cw11rCirfExpAcyTsCi);
	}

	/**Original name: CW11R-CIRF-EXP-ACY-TS-CI<br>*/
	public char getCw11rCirfExpAcyTsCi() {
		return readChar(Pos.CW11R_CIRF_EXP_ACY_TS_CI);
	}

	public void setCw11rCirfExpAcyTs(String cw11rCirfExpAcyTs) {
		writeString(Pos.CW11R_CIRF_EXP_ACY_TS, cw11rCirfExpAcyTs, Len.CW11R_CIRF_EXP_ACY_TS);
	}

	/**Original name: CW11R-CIRF-EXP-ACY-TS<br>*/
	public String getCw11rCirfExpAcyTs() {
		return readString(Pos.CW11R_CIRF_EXP_ACY_TS, Len.CW11R_CIRF_EXP_ACY_TS);
	}

	public void setCw11rMoreRowsSw(char cw11rMoreRowsSw) {
		writeChar(Pos.CW11R_MORE_ROWS_SW, cw11rMoreRowsSw);
	}

	/**Original name: CW11R-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	public char getCw11rMoreRowsSw() {
		return readChar(Pos.CW11R_MORE_ROWS_SW);
	}

	public void setCw27rClientTaxRowFormatted(String data) {
		writeString(Pos.CW27R_CLIENT_TAX_ROW, data, Len.CW27R_CLIENT_TAX_ROW);
	}

	public void setCw27rClientTaxChkSumFormatted(String cw27rClientTaxChkSum) {
		writeString(Pos.CW27R_CLIENT_TAX_CHK_SUM, Trunc.toUnsignedNumeric(cw27rClientTaxChkSum, Len.CW27R_CLIENT_TAX_CHK_SUM),
				Len.CW27R_CLIENT_TAX_CHK_SUM);
	}

	/**Original name: CW27R-CLIENT-TAX-CHK-SUM<br>*/
	public int getCw27rClientTaxChkSum() {
		return readNumDispUnsignedInt(Pos.CW27R_CLIENT_TAX_CHK_SUM, Len.CW27R_CLIENT_TAX_CHK_SUM);
	}

	public void setCw27rClientIdKcre(String cw27rClientIdKcre) {
		writeString(Pos.CW27R_CLIENT_ID_KCRE, cw27rClientIdKcre, Len.CW27R_CLIENT_ID_KCRE);
	}

	/**Original name: CW27R-CLIENT-ID-KCRE<br>*/
	public String getCw27rClientIdKcre() {
		return readString(Pos.CW27R_CLIENT_ID_KCRE, Len.CW27R_CLIENT_ID_KCRE);
	}

	public void setCw27rCitxTaxSeqNbrKcre(String cw27rCitxTaxSeqNbrKcre) {
		writeString(Pos.CW27R_CITX_TAX_SEQ_NBR_KCRE, cw27rCitxTaxSeqNbrKcre, Len.CW27R_CITX_TAX_SEQ_NBR_KCRE);
	}

	/**Original name: CW27R-CITX-TAX-SEQ-NBR-KCRE<br>*/
	public String getCw27rCitxTaxSeqNbrKcre() {
		return readString(Pos.CW27R_CITX_TAX_SEQ_NBR_KCRE, Len.CW27R_CITX_TAX_SEQ_NBR_KCRE);
	}

	public void setCw27rTransProcessDt(String cw27rTransProcessDt) {
		writeString(Pos.CW27R_TRANS_PROCESS_DT, cw27rTransProcessDt, Len.CW27R_TRANS_PROCESS_DT);
	}

	/**Original name: CW27R-TRANS-PROCESS-DT<br>*/
	public String getCw27rTransProcessDt() {
		return readString(Pos.CW27R_TRANS_PROCESS_DT, Len.CW27R_TRANS_PROCESS_DT);
	}

	public void setCw27rClientIdCi(char cw27rClientIdCi) {
		writeChar(Pos.CW27R_CLIENT_ID_CI, cw27rClientIdCi);
	}

	/**Original name: CW27R-CLIENT-ID-CI<br>*/
	public char getCw27rClientIdCi() {
		return readChar(Pos.CW27R_CLIENT_ID_CI);
	}

	public void setCw27rClientId(String cw27rClientId) {
		writeString(Pos.CW27R_CLIENT_ID, cw27rClientId, Len.CW27R_CLIENT_ID);
	}

	/**Original name: CW27R-CLIENT-ID<br>*/
	public String getCw27rClientId() {
		return readString(Pos.CW27R_CLIENT_ID, Len.CW27R_CLIENT_ID);
	}

	public void setCw27rCitxTaxSeqNbrCi(char cw27rCitxTaxSeqNbrCi) {
		writeChar(Pos.CW27R_CITX_TAX_SEQ_NBR_CI, cw27rCitxTaxSeqNbrCi);
	}

	/**Original name: CW27R-CITX-TAX-SEQ-NBR-CI<br>*/
	public char getCw27rCitxTaxSeqNbrCi() {
		return readChar(Pos.CW27R_CITX_TAX_SEQ_NBR_CI);
	}

	public void setCw27rCitxTaxSeqNbrSign(char cw27rCitxTaxSeqNbrSign) {
		writeChar(Pos.CW27R_CITX_TAX_SEQ_NBR_SIGN, cw27rCitxTaxSeqNbrSign);
	}

	/**Original name: CW27R-CITX-TAX-SEQ-NBR-SIGN<br>*/
	public char getCw27rCitxTaxSeqNbrSign() {
		return readChar(Pos.CW27R_CITX_TAX_SEQ_NBR_SIGN);
	}

	public void setCw27rCitxTaxSeqNbrFormatted(String cw27rCitxTaxSeqNbr) {
		writeString(Pos.CW27R_CITX_TAX_SEQ_NBR, Trunc.toUnsignedNumeric(cw27rCitxTaxSeqNbr, Len.CW27R_CITX_TAX_SEQ_NBR), Len.CW27R_CITX_TAX_SEQ_NBR);
	}

	/**Original name: CW27R-CITX-TAX-SEQ-NBR<br>*/
	public int getCw27rCitxTaxSeqNbr() {
		return readNumDispUnsignedInt(Pos.CW27R_CITX_TAX_SEQ_NBR, Len.CW27R_CITX_TAX_SEQ_NBR);
	}

	public void setCw27rHistoryVldNbrCi(char cw27rHistoryVldNbrCi) {
		writeChar(Pos.CW27R_HISTORY_VLD_NBR_CI, cw27rHistoryVldNbrCi);
	}

	/**Original name: CW27R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw27rHistoryVldNbrCi() {
		return readChar(Pos.CW27R_HISTORY_VLD_NBR_CI);
	}

	public void setCw27rHistoryVldNbrSign(char cw27rHistoryVldNbrSign) {
		writeChar(Pos.CW27R_HISTORY_VLD_NBR_SIGN, cw27rHistoryVldNbrSign);
	}

	/**Original name: CW27R-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw27rHistoryVldNbrSign() {
		return readChar(Pos.CW27R_HISTORY_VLD_NBR_SIGN);
	}

	public void setCw27rHistoryVldNbrFormatted(String cw27rHistoryVldNbr) {
		writeString(Pos.CW27R_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cw27rHistoryVldNbr, Len.CW27R_HISTORY_VLD_NBR), Len.CW27R_HISTORY_VLD_NBR);
	}

	/**Original name: CW27R-HISTORY-VLD-NBR<br>*/
	public int getCw27rHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CW27R_HISTORY_VLD_NBR, Len.CW27R_HISTORY_VLD_NBR);
	}

	public void setCw27rEffectiveDtCi(char cw27rEffectiveDtCi) {
		writeChar(Pos.CW27R_EFFECTIVE_DT_CI, cw27rEffectiveDtCi);
	}

	/**Original name: CW27R-EFFECTIVE-DT-CI<br>*/
	public char getCw27rEffectiveDtCi() {
		return readChar(Pos.CW27R_EFFECTIVE_DT_CI);
	}

	public void setCw27rEffectiveDt(String cw27rEffectiveDt) {
		writeString(Pos.CW27R_EFFECTIVE_DT, cw27rEffectiveDt, Len.CW27R_EFFECTIVE_DT);
	}

	/**Original name: CW27R-EFFECTIVE-DT<br>*/
	public String getCw27rEffectiveDt() {
		return readString(Pos.CW27R_EFFECTIVE_DT, Len.CW27R_EFFECTIVE_DT);
	}

	public void setCw27rCitxTaxIdCi(char cw27rCitxTaxIdCi) {
		writeChar(Pos.CW27R_CITX_TAX_ID_CI, cw27rCitxTaxIdCi);
	}

	/**Original name: CW27R-CITX-TAX-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw27rCitxTaxIdCi() {
		return readChar(Pos.CW27R_CITX_TAX_ID_CI);
	}

	public void setCw27rCitxTaxId(String cw27rCitxTaxId) {
		writeString(Pos.CW27R_CITX_TAX_ID, cw27rCitxTaxId, Len.CW27R_CITX_TAX_ID);
	}

	/**Original name: CW27R-CITX-TAX-ID<br>*/
	public String getCw27rCitxTaxId() {
		return readString(Pos.CW27R_CITX_TAX_ID, Len.CW27R_CITX_TAX_ID);
	}

	public void setCw27rTaxTypeCdCi(char cw27rTaxTypeCdCi) {
		writeChar(Pos.CW27R_TAX_TYPE_CD_CI, cw27rTaxTypeCdCi);
	}

	/**Original name: CW27R-TAX-TYPE-CD-CI<br>*/
	public char getCw27rTaxTypeCdCi() {
		return readChar(Pos.CW27R_TAX_TYPE_CD_CI);
	}

	public void setCw27rTaxTypeCd(String cw27rTaxTypeCd) {
		writeString(Pos.CW27R_TAX_TYPE_CD, cw27rTaxTypeCd, Len.CW27R_TAX_TYPE_CD);
	}

	/**Original name: CW27R-TAX-TYPE-CD<br>*/
	public String getCw27rTaxTypeCd() {
		return readString(Pos.CW27R_TAX_TYPE_CD, Len.CW27R_TAX_TYPE_CD);
	}

	public void setCw27rCitxTaxStCdCi(char cw27rCitxTaxStCdCi) {
		writeChar(Pos.CW27R_CITX_TAX_ST_CD_CI, cw27rCitxTaxStCdCi);
	}

	/**Original name: CW27R-CITX-TAX-ST-CD-CI<br>*/
	public char getCw27rCitxTaxStCdCi() {
		return readChar(Pos.CW27R_CITX_TAX_ST_CD_CI);
	}

	public void setCw27rCitxTaxStCdNi(char cw27rCitxTaxStCdNi) {
		writeChar(Pos.CW27R_CITX_TAX_ST_CD_NI, cw27rCitxTaxStCdNi);
	}

	/**Original name: CW27R-CITX-TAX-ST-CD-NI<br>*/
	public char getCw27rCitxTaxStCdNi() {
		return readChar(Pos.CW27R_CITX_TAX_ST_CD_NI);
	}

	public void setCw27rCitxTaxStCd(String cw27rCitxTaxStCd) {
		writeString(Pos.CW27R_CITX_TAX_ST_CD, cw27rCitxTaxStCd, Len.CW27R_CITX_TAX_ST_CD);
	}

	/**Original name: CW27R-CITX-TAX-ST-CD<br>*/
	public String getCw27rCitxTaxStCd() {
		return readString(Pos.CW27R_CITX_TAX_ST_CD, Len.CW27R_CITX_TAX_ST_CD);
	}

	public void setCw27rCitxTaxCtrCdCi(char cw27rCitxTaxCtrCdCi) {
		writeChar(Pos.CW27R_CITX_TAX_CTR_CD_CI, cw27rCitxTaxCtrCdCi);
	}

	/**Original name: CW27R-CITX-TAX-CTR-CD-CI<br>*/
	public char getCw27rCitxTaxCtrCdCi() {
		return readChar(Pos.CW27R_CITX_TAX_CTR_CD_CI);
	}

	public void setCw27rCitxTaxCtrCdNi(char cw27rCitxTaxCtrCdNi) {
		writeChar(Pos.CW27R_CITX_TAX_CTR_CD_NI, cw27rCitxTaxCtrCdNi);
	}

	/**Original name: CW27R-CITX-TAX-CTR-CD-NI<br>*/
	public char getCw27rCitxTaxCtrCdNi() {
		return readChar(Pos.CW27R_CITX_TAX_CTR_CD_NI);
	}

	public void setCw27rCitxTaxCtrCd(String cw27rCitxTaxCtrCd) {
		writeString(Pos.CW27R_CITX_TAX_CTR_CD, cw27rCitxTaxCtrCd, Len.CW27R_CITX_TAX_CTR_CD);
	}

	/**Original name: CW27R-CITX-TAX-CTR-CD<br>*/
	public String getCw27rCitxTaxCtrCd() {
		return readString(Pos.CW27R_CITX_TAX_CTR_CD, Len.CW27R_CITX_TAX_CTR_CD);
	}

	public void setCw27rUserIdCi(char cw27rUserIdCi) {
		writeChar(Pos.CW27R_USER_ID_CI, cw27rUserIdCi);
	}

	/**Original name: CW27R-USER-ID-CI<br>*/
	public char getCw27rUserIdCi() {
		return readChar(Pos.CW27R_USER_ID_CI);
	}

	public void setCw27rUserId(String cw27rUserId) {
		writeString(Pos.CW27R_USER_ID, cw27rUserId, Len.CW27R_USER_ID);
	}

	/**Original name: CW27R-USER-ID<br>*/
	public String getCw27rUserId() {
		return readString(Pos.CW27R_USER_ID, Len.CW27R_USER_ID);
	}

	public void setCw27rStatusCdCi(char cw27rStatusCdCi) {
		writeChar(Pos.CW27R_STATUS_CD_CI, cw27rStatusCdCi);
	}

	/**Original name: CW27R-STATUS-CD-CI<br>*/
	public char getCw27rStatusCdCi() {
		return readChar(Pos.CW27R_STATUS_CD_CI);
	}

	public void setCw27rStatusCd(char cw27rStatusCd) {
		writeChar(Pos.CW27R_STATUS_CD, cw27rStatusCd);
	}

	/**Original name: CW27R-STATUS-CD<br>*/
	public char getCw27rStatusCd() {
		return readChar(Pos.CW27R_STATUS_CD);
	}

	public void setCw27rTerminalIdCi(char cw27rTerminalIdCi) {
		writeChar(Pos.CW27R_TERMINAL_ID_CI, cw27rTerminalIdCi);
	}

	/**Original name: CW27R-TERMINAL-ID-CI<br>*/
	public char getCw27rTerminalIdCi() {
		return readChar(Pos.CW27R_TERMINAL_ID_CI);
	}

	public void setCw27rTerminalId(String cw27rTerminalId) {
		writeString(Pos.CW27R_TERMINAL_ID, cw27rTerminalId, Len.CW27R_TERMINAL_ID);
	}

	/**Original name: CW27R-TERMINAL-ID<br>*/
	public String getCw27rTerminalId() {
		return readString(Pos.CW27R_TERMINAL_ID, Len.CW27R_TERMINAL_ID);
	}

	public void setCw27rExpirationDtCi(char cw27rExpirationDtCi) {
		writeChar(Pos.CW27R_EXPIRATION_DT_CI, cw27rExpirationDtCi);
	}

	/**Original name: CW27R-EXPIRATION-DT-CI<br>*/
	public char getCw27rExpirationDtCi() {
		return readChar(Pos.CW27R_EXPIRATION_DT_CI);
	}

	public void setCw27rExpirationDt(String cw27rExpirationDt) {
		writeString(Pos.CW27R_EXPIRATION_DT, cw27rExpirationDt, Len.CW27R_EXPIRATION_DT);
	}

	/**Original name: CW27R-EXPIRATION-DT<br>*/
	public String getCw27rExpirationDt() {
		return readString(Pos.CW27R_EXPIRATION_DT, Len.CW27R_EXPIRATION_DT);
	}

	public void setCw27rEffectiveAcyTsCi(char cw27rEffectiveAcyTsCi) {
		writeChar(Pos.CW27R_EFFECTIVE_ACY_TS_CI, cw27rEffectiveAcyTsCi);
	}

	/**Original name: CW27R-EFFECTIVE-ACY-TS-CI<br>*/
	public char getCw27rEffectiveAcyTsCi() {
		return readChar(Pos.CW27R_EFFECTIVE_ACY_TS_CI);
	}

	public void setCw27rEffectiveAcyTs(String cw27rEffectiveAcyTs) {
		writeString(Pos.CW27R_EFFECTIVE_ACY_TS, cw27rEffectiveAcyTs, Len.CW27R_EFFECTIVE_ACY_TS);
	}

	/**Original name: CW27R-EFFECTIVE-ACY-TS<br>*/
	public String getCw27rEffectiveAcyTs() {
		return readString(Pos.CW27R_EFFECTIVE_ACY_TS, Len.CW27R_EFFECTIVE_ACY_TS);
	}

	public void setCw27rExpirationAcyTsCi(char cw27rExpirationAcyTsCi) {
		writeChar(Pos.CW27R_EXPIRATION_ACY_TS_CI, cw27rExpirationAcyTsCi);
	}

	/**Original name: CW27R-EXPIRATION-ACY-TS-CI<br>*/
	public char getCw27rExpirationAcyTsCi() {
		return readChar(Pos.CW27R_EXPIRATION_ACY_TS_CI);
	}

	public void setCw27rExpirationAcyTsNi(char cw27rExpirationAcyTsNi) {
		writeChar(Pos.CW27R_EXPIRATION_ACY_TS_NI, cw27rExpirationAcyTsNi);
	}

	/**Original name: CW27R-EXPIRATION-ACY-TS-NI<br>*/
	public char getCw27rExpirationAcyTsNi() {
		return readChar(Pos.CW27R_EXPIRATION_ACY_TS_NI);
	}

	public void setCw27rExpirationAcyTs(String cw27rExpirationAcyTs) {
		writeString(Pos.CW27R_EXPIRATION_ACY_TS, cw27rExpirationAcyTs, Len.CW27R_EXPIRATION_ACY_TS);
	}

	/**Original name: CW27R-EXPIRATION-ACY-TS<br>*/
	public String getCw27rExpirationAcyTs() {
		return readString(Pos.CW27R_EXPIRATION_ACY_TS, Len.CW27R_EXPIRATION_ACY_TS);
	}

	public void setCw27rMoreRowsSw(char cw27rMoreRowsSw) {
		writeChar(Pos.CW27R_MORE_ROWS_SW, cw27rMoreRowsSw);
	}

	/**Original name: CW27R-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	public char getCw27rMoreRowsSw() {
		return readChar(Pos.CW27R_MORE_ROWS_SW);
	}

	public void setCw27rTaxTypeDesc(String cw27rTaxTypeDesc) {
		writeString(Pos.CW27R_TAX_TYPE_DESC, cw27rTaxTypeDesc, Len.CW27R_TAX_TYPE_DESC);
	}

	/**Original name: CW27R-TAX-TYPE-DESC<br>*/
	public String getCw27rTaxTypeDesc() {
		return readString(Pos.CW27R_TAX_TYPE_DESC, Len.CW27R_TAX_TYPE_DESC);
	}

	public void setCw45rCltMutTerStcRowFormatted(String data) {
		writeString(Pos.CW45R_CLT_MUT_TER_STC_ROW, data, Len.CW45R_CLT_MUT_TER_STC_ROW);
	}

	public void setCw45rCltMutTerStcCsumFormatted(String cw45rCltMutTerStcCsum) {
		writeString(Pos.CW45R_CLT_MUT_TER_STC_CSUM, Trunc.toUnsignedNumeric(cw45rCltMutTerStcCsum, Len.CW45R_CLT_MUT_TER_STC_CSUM),
				Len.CW45R_CLT_MUT_TER_STC_CSUM);
	}

	/**Original name: CW45R-CLT-MUT-TER-STC-CSUM<br>*/
	public int getCw45rCltMutTerStcCsum() {
		return readNumDispUnsignedInt(Pos.CW45R_CLT_MUT_TER_STC_CSUM, Len.CW45R_CLT_MUT_TER_STC_CSUM);
	}

	public void setCw45rTerCltIdKcre(String cw45rTerCltIdKcre) {
		writeString(Pos.CW45R_TER_CLT_ID_KCRE, cw45rTerCltIdKcre, Len.CW45R_TER_CLT_ID_KCRE);
	}

	/**Original name: CW45R-TER-CLT-ID-KCRE<br>*/
	public String getCw45rTerCltIdKcre() {
		return readString(Pos.CW45R_TER_CLT_ID_KCRE, Len.CW45R_TER_CLT_ID_KCRE);
	}

	public void setCw45rTerLevelCdKcre(String cw45rTerLevelCdKcre) {
		writeString(Pos.CW45R_TER_LEVEL_CD_KCRE, cw45rTerLevelCdKcre, Len.CW45R_TER_LEVEL_CD_KCRE);
	}

	/**Original name: CW45R-TER-LEVEL-CD-KCRE<br>*/
	public String getCw45rTerLevelCdKcre() {
		return readString(Pos.CW45R_TER_LEVEL_CD_KCRE, Len.CW45R_TER_LEVEL_CD_KCRE);
	}

	public void setCw45rTerNbrKcre(String cw45rTerNbrKcre) {
		writeString(Pos.CW45R_TER_NBR_KCRE, cw45rTerNbrKcre, Len.CW45R_TER_NBR_KCRE);
	}

	/**Original name: CW45R-TER-NBR-KCRE<br>*/
	public String getCw45rTerNbrKcre() {
		return readString(Pos.CW45R_TER_NBR_KCRE, Len.CW45R_TER_NBR_KCRE);
	}

	public void setCw45rTransProcessDt(String cw45rTransProcessDt) {
		writeString(Pos.CW45R_TRANS_PROCESS_DT, cw45rTransProcessDt, Len.CW45R_TRANS_PROCESS_DT);
	}

	/**Original name: CW45R-TRANS-PROCESS-DT<br>*/
	public String getCw45rTransProcessDt() {
		return readString(Pos.CW45R_TRANS_PROCESS_DT, Len.CW45R_TRANS_PROCESS_DT);
	}

	public void setCw45rTerCltId(String cw45rTerCltId) {
		writeString(Pos.CW45R_TER_CLT_ID, cw45rTerCltId, Len.CW45R_TER_CLT_ID);
	}

	/**Original name: CW45R-TER-CLT-ID<br>*/
	public String getCw45rTerCltId() {
		return readString(Pos.CW45R_TER_CLT_ID, Len.CW45R_TER_CLT_ID);
	}

	public void setCw45rTerLevelCd(String cw45rTerLevelCd) {
		writeString(Pos.CW45R_TER_LEVEL_CD, cw45rTerLevelCd, Len.CW45R_TER_LEVEL_CD);
	}

	/**Original name: CW45R-TER-LEVEL-CD<br>*/
	public String getCw45rTerLevelCd() {
		return readString(Pos.CW45R_TER_LEVEL_CD, Len.CW45R_TER_LEVEL_CD);
	}

	public void setCw45rTerNbr(String cw45rTerNbr) {
		writeString(Pos.CW45R_TER_NBR, cw45rTerNbr, Len.CW45R_TER_NBR);
	}

	/**Original name: CW45R-TER-NBR<br>*/
	public String getCw45rTerNbr() {
		return readString(Pos.CW45R_TER_NBR, Len.CW45R_TER_NBR);
	}

	public void setCw45rTerCltIdCi(char cw45rTerCltIdCi) {
		writeChar(Pos.CW45R_TER_CLT_ID_CI, cw45rTerCltIdCi);
	}

	/**Original name: CW45R-TER-CLT-ID-CI<br>*/
	public char getCw45rTerCltIdCi() {
		return readChar(Pos.CW45R_TER_CLT_ID_CI);
	}

	public void setCw45rTerLevelCdCi(char cw45rTerLevelCdCi) {
		writeChar(Pos.CW45R_TER_LEVEL_CD_CI, cw45rTerLevelCdCi);
	}

	/**Original name: CW45R-TER-LEVEL-CD-CI<br>*/
	public char getCw45rTerLevelCdCi() {
		return readChar(Pos.CW45R_TER_LEVEL_CD_CI);
	}

	public void setCw45rTerNbrCi(char cw45rTerNbrCi) {
		writeChar(Pos.CW45R_TER_NBR_CI, cw45rTerNbrCi);
	}

	/**Original name: CW45R-TER-NBR-CI<br>*/
	public char getCw45rTerNbrCi() {
		return readChar(Pos.CW45R_TER_NBR_CI);
	}

	public void setCw45rMrTerNbrCi(char cw45rMrTerNbrCi) {
		writeChar(Pos.CW45R_MR_TER_NBR_CI, cw45rMrTerNbrCi);
	}

	/**Original name: CW45R-MR-TER-NBR-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw45rMrTerNbrCi() {
		return readChar(Pos.CW45R_MR_TER_NBR_CI);
	}

	public void setCw45rMrTerNbr(String cw45rMrTerNbr) {
		writeString(Pos.CW45R_MR_TER_NBR, cw45rMrTerNbr, Len.CW45R_MR_TER_NBR);
	}

	/**Original name: CW45R-MR-TER-NBR<br>*/
	public String getCw45rMrTerNbr() {
		return readString(Pos.CW45R_MR_TER_NBR, Len.CW45R_MR_TER_NBR);
	}

	public void setCw45rMrTerCltIdCi(char cw45rMrTerCltIdCi) {
		writeChar(Pos.CW45R_MR_TER_CLT_ID_CI, cw45rMrTerCltIdCi);
	}

	/**Original name: CW45R-MR-TER-CLT-ID-CI<br>*/
	public char getCw45rMrTerCltIdCi() {
		return readChar(Pos.CW45R_MR_TER_CLT_ID_CI);
	}

	public void setCw45rMrTerCltId(String cw45rMrTerCltId) {
		writeString(Pos.CW45R_MR_TER_CLT_ID, cw45rMrTerCltId, Len.CW45R_MR_TER_CLT_ID);
	}

	/**Original name: CW45R-MR-TER-CLT-ID<br>*/
	public String getCw45rMrTerCltId() {
		return readString(Pos.CW45R_MR_TER_CLT_ID, Len.CW45R_MR_TER_CLT_ID);
	}

	public void setCw45rMrNmPfxCi(char cw45rMrNmPfxCi) {
		writeChar(Pos.CW45R_MR_NM_PFX_CI, cw45rMrNmPfxCi);
	}

	/**Original name: CW45R-MR-NM-PFX-CI<br>*/
	public char getCw45rMrNmPfxCi() {
		return readChar(Pos.CW45R_MR_NM_PFX_CI);
	}

	public void setCw45rMrNmPfx(String cw45rMrNmPfx) {
		writeString(Pos.CW45R_MR_NM_PFX, cw45rMrNmPfx, Len.CW45R_MR_NM_PFX);
	}

	/**Original name: CW45R-MR-NM-PFX<br>*/
	public String getCw45rMrNmPfx() {
		return readString(Pos.CW45R_MR_NM_PFX, Len.CW45R_MR_NM_PFX);
	}

	public void setCw45rMrFstNmCi(char cw45rMrFstNmCi) {
		writeChar(Pos.CW45R_MR_FST_NM_CI, cw45rMrFstNmCi);
	}

	/**Original name: CW45R-MR-FST-NM-CI<br>*/
	public char getCw45rMrFstNmCi() {
		return readChar(Pos.CW45R_MR_FST_NM_CI);
	}

	public void setCw45rMrFstNm(String cw45rMrFstNm) {
		writeString(Pos.CW45R_MR_FST_NM, cw45rMrFstNm, Len.CW45R_MR_FST_NM);
	}

	/**Original name: CW45R-MR-FST-NM<br>*/
	public String getCw45rMrFstNm() {
		return readString(Pos.CW45R_MR_FST_NM, Len.CW45R_MR_FST_NM);
	}

	public void setCw45rMrMdlNmCi(char cw45rMrMdlNmCi) {
		writeChar(Pos.CW45R_MR_MDL_NM_CI, cw45rMrMdlNmCi);
	}

	/**Original name: CW45R-MR-MDL-NM-CI<br>*/
	public char getCw45rMrMdlNmCi() {
		return readChar(Pos.CW45R_MR_MDL_NM_CI);
	}

	public void setCw45rMrMdlNm(String cw45rMrMdlNm) {
		writeString(Pos.CW45R_MR_MDL_NM, cw45rMrMdlNm, Len.CW45R_MR_MDL_NM);
	}

	/**Original name: CW45R-MR-MDL-NM<br>*/
	public String getCw45rMrMdlNm() {
		return readString(Pos.CW45R_MR_MDL_NM, Len.CW45R_MR_MDL_NM);
	}

	public void setCw45rMrLstNmCi(char cw45rMrLstNmCi) {
		writeChar(Pos.CW45R_MR_LST_NM_CI, cw45rMrLstNmCi);
	}

	/**Original name: CW45R-MR-LST-NM-CI<br>*/
	public char getCw45rMrLstNmCi() {
		return readChar(Pos.CW45R_MR_LST_NM_CI);
	}

	public void setCw45rMrLstNm(String cw45rMrLstNm) {
		writeString(Pos.CW45R_MR_LST_NM, cw45rMrLstNm, Len.CW45R_MR_LST_NM);
	}

	/**Original name: CW45R-MR-LST-NM<br>*/
	public String getCw45rMrLstNm() {
		return readString(Pos.CW45R_MR_LST_NM, Len.CW45R_MR_LST_NM);
	}

	public void setCw45rMrNmCi(char cw45rMrNmCi) {
		writeChar(Pos.CW45R_MR_NM_CI, cw45rMrNmCi);
	}

	/**Original name: CW45R-MR-NM-CI<br>*/
	public char getCw45rMrNmCi() {
		return readChar(Pos.CW45R_MR_NM_CI);
	}

	public void setCw45rMrNm(String cw45rMrNm) {
		writeString(Pos.CW45R_MR_NM, cw45rMrNm, Len.CW45R_MR_NM);
	}

	/**Original name: CW45R-MR-NM<br>*/
	public String getCw45rMrNm() {
		return readString(Pos.CW45R_MR_NM, Len.CW45R_MR_NM);
	}

	public void setCw45rMrNmSfxCi(char cw45rMrNmSfxCi) {
		writeChar(Pos.CW45R_MR_NM_SFX_CI, cw45rMrNmSfxCi);
	}

	/**Original name: CW45R-MR-NM-SFX-CI<br>*/
	public char getCw45rMrNmSfxCi() {
		return readChar(Pos.CW45R_MR_NM_SFX_CI);
	}

	public void setCw45rMrNmSfx(String cw45rMrNmSfx) {
		writeString(Pos.CW45R_MR_NM_SFX, cw45rMrNmSfx, Len.CW45R_MR_NM_SFX);
	}

	/**Original name: CW45R-MR-NM-SFX<br>*/
	public String getCw45rMrNmSfx() {
		return readString(Pos.CW45R_MR_NM_SFX, Len.CW45R_MR_NM_SFX);
	}

	public void setCw45rMrCltIdCi(char cw45rMrCltIdCi) {
		writeChar(Pos.CW45R_MR_CLT_ID_CI, cw45rMrCltIdCi);
	}

	/**Original name: CW45R-MR-CLT-ID-CI<br>*/
	public char getCw45rMrCltIdCi() {
		return readChar(Pos.CW45R_MR_CLT_ID_CI);
	}

	public void setCw45rMrCltId(String cw45rMrCltId) {
		writeString(Pos.CW45R_MR_CLT_ID, cw45rMrCltId, Len.CW45R_MR_CLT_ID);
	}

	/**Original name: CW45R-MR-CLT-ID<br>*/
	public String getCw45rMrCltId() {
		return readString(Pos.CW45R_MR_CLT_ID, Len.CW45R_MR_CLT_ID);
	}

	public void setCw45rCsrTerNbrCi(char cw45rCsrTerNbrCi) {
		writeChar(Pos.CW45R_CSR_TER_NBR_CI, cw45rCsrTerNbrCi);
	}

	/**Original name: CW45R-CSR-TER-NBR-CI<br>*/
	public char getCw45rCsrTerNbrCi() {
		return readChar(Pos.CW45R_CSR_TER_NBR_CI);
	}

	public void setCw45rCsrTerNbr(String cw45rCsrTerNbr) {
		writeString(Pos.CW45R_CSR_TER_NBR, cw45rCsrTerNbr, Len.CW45R_CSR_TER_NBR);
	}

	/**Original name: CW45R-CSR-TER-NBR<br>*/
	public String getCw45rCsrTerNbr() {
		return readString(Pos.CW45R_CSR_TER_NBR, Len.CW45R_CSR_TER_NBR);
	}

	public void setCw45rCsrTerCltIdCi(char cw45rCsrTerCltIdCi) {
		writeChar(Pos.CW45R_CSR_TER_CLT_ID_CI, cw45rCsrTerCltIdCi);
	}

	/**Original name: CW45R-CSR-TER-CLT-ID-CI<br>*/
	public char getCw45rCsrTerCltIdCi() {
		return readChar(Pos.CW45R_CSR_TER_CLT_ID_CI);
	}

	public void setCw45rCsrTerCltId(String cw45rCsrTerCltId) {
		writeString(Pos.CW45R_CSR_TER_CLT_ID, cw45rCsrTerCltId, Len.CW45R_CSR_TER_CLT_ID);
	}

	/**Original name: CW45R-CSR-TER-CLT-ID<br>*/
	public String getCw45rCsrTerCltId() {
		return readString(Pos.CW45R_CSR_TER_CLT_ID, Len.CW45R_CSR_TER_CLT_ID);
	}

	public void setCw45rCsrNmPfxCi(char cw45rCsrNmPfxCi) {
		writeChar(Pos.CW45R_CSR_NM_PFX_CI, cw45rCsrNmPfxCi);
	}

	/**Original name: CW45R-CSR-NM-PFX-CI<br>*/
	public char getCw45rCsrNmPfxCi() {
		return readChar(Pos.CW45R_CSR_NM_PFX_CI);
	}

	public void setCw45rCsrNmPfx(String cw45rCsrNmPfx) {
		writeString(Pos.CW45R_CSR_NM_PFX, cw45rCsrNmPfx, Len.CW45R_CSR_NM_PFX);
	}

	/**Original name: CW45R-CSR-NM-PFX<br>*/
	public String getCw45rCsrNmPfx() {
		return readString(Pos.CW45R_CSR_NM_PFX, Len.CW45R_CSR_NM_PFX);
	}

	public void setCw45rCsrFstNmCi(char cw45rCsrFstNmCi) {
		writeChar(Pos.CW45R_CSR_FST_NM_CI, cw45rCsrFstNmCi);
	}

	/**Original name: CW45R-CSR-FST-NM-CI<br>*/
	public char getCw45rCsrFstNmCi() {
		return readChar(Pos.CW45R_CSR_FST_NM_CI);
	}

	public void setCw45rCsrFstNm(String cw45rCsrFstNm) {
		writeString(Pos.CW45R_CSR_FST_NM, cw45rCsrFstNm, Len.CW45R_CSR_FST_NM);
	}

	/**Original name: CW45R-CSR-FST-NM<br>*/
	public String getCw45rCsrFstNm() {
		return readString(Pos.CW45R_CSR_FST_NM, Len.CW45R_CSR_FST_NM);
	}

	public void setCw45rCsrMdlNmCi(char cw45rCsrMdlNmCi) {
		writeChar(Pos.CW45R_CSR_MDL_NM_CI, cw45rCsrMdlNmCi);
	}

	/**Original name: CW45R-CSR-MDL-NM-CI<br>*/
	public char getCw45rCsrMdlNmCi() {
		return readChar(Pos.CW45R_CSR_MDL_NM_CI);
	}

	public void setCw45rCsrMdlNm(String cw45rCsrMdlNm) {
		writeString(Pos.CW45R_CSR_MDL_NM, cw45rCsrMdlNm, Len.CW45R_CSR_MDL_NM);
	}

	/**Original name: CW45R-CSR-MDL-NM<br>*/
	public String getCw45rCsrMdlNm() {
		return readString(Pos.CW45R_CSR_MDL_NM, Len.CW45R_CSR_MDL_NM);
	}

	public void setCw45rCsrLstNmCi(char cw45rCsrLstNmCi) {
		writeChar(Pos.CW45R_CSR_LST_NM_CI, cw45rCsrLstNmCi);
	}

	/**Original name: CW45R-CSR-LST-NM-CI<br>*/
	public char getCw45rCsrLstNmCi() {
		return readChar(Pos.CW45R_CSR_LST_NM_CI);
	}

	public void setCw45rCsrLstNm(String cw45rCsrLstNm) {
		writeString(Pos.CW45R_CSR_LST_NM, cw45rCsrLstNm, Len.CW45R_CSR_LST_NM);
	}

	/**Original name: CW45R-CSR-LST-NM<br>*/
	public String getCw45rCsrLstNm() {
		return readString(Pos.CW45R_CSR_LST_NM, Len.CW45R_CSR_LST_NM);
	}

	public void setCw45rCsrNmCi(char cw45rCsrNmCi) {
		writeChar(Pos.CW45R_CSR_NM_CI, cw45rCsrNmCi);
	}

	/**Original name: CW45R-CSR-NM-CI<br>*/
	public char getCw45rCsrNmCi() {
		return readChar(Pos.CW45R_CSR_NM_CI);
	}

	public void setCw45rCsrNm(String cw45rCsrNm) {
		writeString(Pos.CW45R_CSR_NM, cw45rCsrNm, Len.CW45R_CSR_NM);
	}

	/**Original name: CW45R-CSR-NM<br>*/
	public String getCw45rCsrNm() {
		return readString(Pos.CW45R_CSR_NM, Len.CW45R_CSR_NM);
	}

	public void setCw45rCsrNmSfxCi(char cw45rCsrNmSfxCi) {
		writeChar(Pos.CW45R_CSR_NM_SFX_CI, cw45rCsrNmSfxCi);
	}

	/**Original name: CW45R-CSR-NM-SFX-CI<br>*/
	public char getCw45rCsrNmSfxCi() {
		return readChar(Pos.CW45R_CSR_NM_SFX_CI);
	}

	public void setCw45rCsrNmSfx(String cw45rCsrNmSfx) {
		writeString(Pos.CW45R_CSR_NM_SFX, cw45rCsrNmSfx, Len.CW45R_CSR_NM_SFX);
	}

	/**Original name: CW45R-CSR-NM-SFX<br>*/
	public String getCw45rCsrNmSfx() {
		return readString(Pos.CW45R_CSR_NM_SFX, Len.CW45R_CSR_NM_SFX);
	}

	public void setCw45rCsrCltIdCi(char cw45rCsrCltIdCi) {
		writeChar(Pos.CW45R_CSR_CLT_ID_CI, cw45rCsrCltIdCi);
	}

	/**Original name: CW45R-CSR-CLT-ID-CI<br>*/
	public char getCw45rCsrCltIdCi() {
		return readChar(Pos.CW45R_CSR_CLT_ID_CI);
	}

	public void setCw45rCsrCltId(String cw45rCsrCltId) {
		writeString(Pos.CW45R_CSR_CLT_ID, cw45rCsrCltId, Len.CW45R_CSR_CLT_ID);
	}

	/**Original name: CW45R-CSR-CLT-ID<br>*/
	public String getCw45rCsrCltId() {
		return readString(Pos.CW45R_CSR_CLT_ID, Len.CW45R_CSR_CLT_ID);
	}

	public void setCw45rSmrTerNbrCi(char cw45rSmrTerNbrCi) {
		writeChar(Pos.CW45R_SMR_TER_NBR_CI, cw45rSmrTerNbrCi);
	}

	/**Original name: CW45R-SMR-TER-NBR-CI<br>*/
	public char getCw45rSmrTerNbrCi() {
		return readChar(Pos.CW45R_SMR_TER_NBR_CI);
	}

	public void setCw45rSmrTerNbr(String cw45rSmrTerNbr) {
		writeString(Pos.CW45R_SMR_TER_NBR, cw45rSmrTerNbr, Len.CW45R_SMR_TER_NBR);
	}

	/**Original name: CW45R-SMR-TER-NBR<br>*/
	public String getCw45rSmrTerNbr() {
		return readString(Pos.CW45R_SMR_TER_NBR, Len.CW45R_SMR_TER_NBR);
	}

	public void setCw45rSmrTerCltIdCi(char cw45rSmrTerCltIdCi) {
		writeChar(Pos.CW45R_SMR_TER_CLT_ID_CI, cw45rSmrTerCltIdCi);
	}

	/**Original name: CW45R-SMR-TER-CLT-ID-CI<br>*/
	public char getCw45rSmrTerCltIdCi() {
		return readChar(Pos.CW45R_SMR_TER_CLT_ID_CI);
	}

	public void setCw45rSmrTerCltId(String cw45rSmrTerCltId) {
		writeString(Pos.CW45R_SMR_TER_CLT_ID, cw45rSmrTerCltId, Len.CW45R_SMR_TER_CLT_ID);
	}

	/**Original name: CW45R-SMR-TER-CLT-ID<br>*/
	public String getCw45rSmrTerCltId() {
		return readString(Pos.CW45R_SMR_TER_CLT_ID, Len.CW45R_SMR_TER_CLT_ID);
	}

	public void setCw45rSmrNmPfxCi(char cw45rSmrNmPfxCi) {
		writeChar(Pos.CW45R_SMR_NM_PFX_CI, cw45rSmrNmPfxCi);
	}

	/**Original name: CW45R-SMR-NM-PFX-CI<br>*/
	public char getCw45rSmrNmPfxCi() {
		return readChar(Pos.CW45R_SMR_NM_PFX_CI);
	}

	public void setCw45rSmrNmPfx(String cw45rSmrNmPfx) {
		writeString(Pos.CW45R_SMR_NM_PFX, cw45rSmrNmPfx, Len.CW45R_SMR_NM_PFX);
	}

	/**Original name: CW45R-SMR-NM-PFX<br>*/
	public String getCw45rSmrNmPfx() {
		return readString(Pos.CW45R_SMR_NM_PFX, Len.CW45R_SMR_NM_PFX);
	}

	public void setCw45rSmrFstNmCi(char cw45rSmrFstNmCi) {
		writeChar(Pos.CW45R_SMR_FST_NM_CI, cw45rSmrFstNmCi);
	}

	/**Original name: CW45R-SMR-FST-NM-CI<br>*/
	public char getCw45rSmrFstNmCi() {
		return readChar(Pos.CW45R_SMR_FST_NM_CI);
	}

	public void setCw45rSmrFstNm(String cw45rSmrFstNm) {
		writeString(Pos.CW45R_SMR_FST_NM, cw45rSmrFstNm, Len.CW45R_SMR_FST_NM);
	}

	/**Original name: CW45R-SMR-FST-NM<br>*/
	public String getCw45rSmrFstNm() {
		return readString(Pos.CW45R_SMR_FST_NM, Len.CW45R_SMR_FST_NM);
	}

	public void setCw45rSmrMdlNmCi(char cw45rSmrMdlNmCi) {
		writeChar(Pos.CW45R_SMR_MDL_NM_CI, cw45rSmrMdlNmCi);
	}

	/**Original name: CW45R-SMR-MDL-NM-CI<br>*/
	public char getCw45rSmrMdlNmCi() {
		return readChar(Pos.CW45R_SMR_MDL_NM_CI);
	}

	public void setCw45rSmrMdlNm(String cw45rSmrMdlNm) {
		writeString(Pos.CW45R_SMR_MDL_NM, cw45rSmrMdlNm, Len.CW45R_SMR_MDL_NM);
	}

	/**Original name: CW45R-SMR-MDL-NM<br>*/
	public String getCw45rSmrMdlNm() {
		return readString(Pos.CW45R_SMR_MDL_NM, Len.CW45R_SMR_MDL_NM);
	}

	public void setCw45rSmrLstNmCi(char cw45rSmrLstNmCi) {
		writeChar(Pos.CW45R_SMR_LST_NM_CI, cw45rSmrLstNmCi);
	}

	/**Original name: CW45R-SMR-LST-NM-CI<br>*/
	public char getCw45rSmrLstNmCi() {
		return readChar(Pos.CW45R_SMR_LST_NM_CI);
	}

	public void setCw45rSmrLstNm(String cw45rSmrLstNm) {
		writeString(Pos.CW45R_SMR_LST_NM, cw45rSmrLstNm, Len.CW45R_SMR_LST_NM);
	}

	/**Original name: CW45R-SMR-LST-NM<br>*/
	public String getCw45rSmrLstNm() {
		return readString(Pos.CW45R_SMR_LST_NM, Len.CW45R_SMR_LST_NM);
	}

	public void setCw45rSmrNmCi(char cw45rSmrNmCi) {
		writeChar(Pos.CW45R_SMR_NM_CI, cw45rSmrNmCi);
	}

	/**Original name: CW45R-SMR-NM-CI<br>*/
	public char getCw45rSmrNmCi() {
		return readChar(Pos.CW45R_SMR_NM_CI);
	}

	public void setCw45rSmrNm(String cw45rSmrNm) {
		writeString(Pos.CW45R_SMR_NM, cw45rSmrNm, Len.CW45R_SMR_NM);
	}

	/**Original name: CW45R-SMR-NM<br>*/
	public String getCw45rSmrNm() {
		return readString(Pos.CW45R_SMR_NM, Len.CW45R_SMR_NM);
	}

	public void setCw45rSmrNmSfxCi(char cw45rSmrNmSfxCi) {
		writeChar(Pos.CW45R_SMR_NM_SFX_CI, cw45rSmrNmSfxCi);
	}

	/**Original name: CW45R-SMR-NM-SFX-CI<br>*/
	public char getCw45rSmrNmSfxCi() {
		return readChar(Pos.CW45R_SMR_NM_SFX_CI);
	}

	public void setCw45rSmrNmSfx(String cw45rSmrNmSfx) {
		writeString(Pos.CW45R_SMR_NM_SFX, cw45rSmrNmSfx, Len.CW45R_SMR_NM_SFX);
	}

	/**Original name: CW45R-SMR-NM-SFX<br>*/
	public String getCw45rSmrNmSfx() {
		return readString(Pos.CW45R_SMR_NM_SFX, Len.CW45R_SMR_NM_SFX);
	}

	public void setCw45rSmrCltIdCi(char cw45rSmrCltIdCi) {
		writeChar(Pos.CW45R_SMR_CLT_ID_CI, cw45rSmrCltIdCi);
	}

	/**Original name: CW45R-SMR-CLT-ID-CI<br>*/
	public char getCw45rSmrCltIdCi() {
		return readChar(Pos.CW45R_SMR_CLT_ID_CI);
	}

	public void setCw45rSmrCltId(String cw45rSmrCltId) {
		writeString(Pos.CW45R_SMR_CLT_ID, cw45rSmrCltId, Len.CW45R_SMR_CLT_ID);
	}

	/**Original name: CW45R-SMR-CLT-ID<br>*/
	public String getCw45rSmrCltId() {
		return readString(Pos.CW45R_SMR_CLT_ID, Len.CW45R_SMR_CLT_ID);
	}

	public void setCw45rDmmTerNbrCi(char cw45rDmmTerNbrCi) {
		writeChar(Pos.CW45R_DMM_TER_NBR_CI, cw45rDmmTerNbrCi);
	}

	/**Original name: CW45R-DMM-TER-NBR-CI<br>*/
	public char getCw45rDmmTerNbrCi() {
		return readChar(Pos.CW45R_DMM_TER_NBR_CI);
	}

	public void setCw45rDmmTerNbr(String cw45rDmmTerNbr) {
		writeString(Pos.CW45R_DMM_TER_NBR, cw45rDmmTerNbr, Len.CW45R_DMM_TER_NBR);
	}

	/**Original name: CW45R-DMM-TER-NBR<br>*/
	public String getCw45rDmmTerNbr() {
		return readString(Pos.CW45R_DMM_TER_NBR, Len.CW45R_DMM_TER_NBR);
	}

	public void setCw45rDmmTerCltIdCi(char cw45rDmmTerCltIdCi) {
		writeChar(Pos.CW45R_DMM_TER_CLT_ID_CI, cw45rDmmTerCltIdCi);
	}

	/**Original name: CW45R-DMM-TER-CLT-ID-CI<br>*/
	public char getCw45rDmmTerCltIdCi() {
		return readChar(Pos.CW45R_DMM_TER_CLT_ID_CI);
	}

	public void setCw45rDmmTerCltId(String cw45rDmmTerCltId) {
		writeString(Pos.CW45R_DMM_TER_CLT_ID, cw45rDmmTerCltId, Len.CW45R_DMM_TER_CLT_ID);
	}

	/**Original name: CW45R-DMM-TER-CLT-ID<br>*/
	public String getCw45rDmmTerCltId() {
		return readString(Pos.CW45R_DMM_TER_CLT_ID, Len.CW45R_DMM_TER_CLT_ID);
	}

	public void setCw45rDmmNmPfxCi(char cw45rDmmNmPfxCi) {
		writeChar(Pos.CW45R_DMM_NM_PFX_CI, cw45rDmmNmPfxCi);
	}

	/**Original name: CW45R-DMM-NM-PFX-CI<br>*/
	public char getCw45rDmmNmPfxCi() {
		return readChar(Pos.CW45R_DMM_NM_PFX_CI);
	}

	public void setCw45rDmmNmPfx(String cw45rDmmNmPfx) {
		writeString(Pos.CW45R_DMM_NM_PFX, cw45rDmmNmPfx, Len.CW45R_DMM_NM_PFX);
	}

	/**Original name: CW45R-DMM-NM-PFX<br>*/
	public String getCw45rDmmNmPfx() {
		return readString(Pos.CW45R_DMM_NM_PFX, Len.CW45R_DMM_NM_PFX);
	}

	public void setCw45rDmmFstNmCi(char cw45rDmmFstNmCi) {
		writeChar(Pos.CW45R_DMM_FST_NM_CI, cw45rDmmFstNmCi);
	}

	/**Original name: CW45R-DMM-FST-NM-CI<br>*/
	public char getCw45rDmmFstNmCi() {
		return readChar(Pos.CW45R_DMM_FST_NM_CI);
	}

	public void setCw45rDmmFstNm(String cw45rDmmFstNm) {
		writeString(Pos.CW45R_DMM_FST_NM, cw45rDmmFstNm, Len.CW45R_DMM_FST_NM);
	}

	/**Original name: CW45R-DMM-FST-NM<br>*/
	public String getCw45rDmmFstNm() {
		return readString(Pos.CW45R_DMM_FST_NM, Len.CW45R_DMM_FST_NM);
	}

	public void setCw45rDmmMdlNmCi(char cw45rDmmMdlNmCi) {
		writeChar(Pos.CW45R_DMM_MDL_NM_CI, cw45rDmmMdlNmCi);
	}

	/**Original name: CW45R-DMM-MDL-NM-CI<br>*/
	public char getCw45rDmmMdlNmCi() {
		return readChar(Pos.CW45R_DMM_MDL_NM_CI);
	}

	public void setCw45rDmmMdlNm(String cw45rDmmMdlNm) {
		writeString(Pos.CW45R_DMM_MDL_NM, cw45rDmmMdlNm, Len.CW45R_DMM_MDL_NM);
	}

	/**Original name: CW45R-DMM-MDL-NM<br>*/
	public String getCw45rDmmMdlNm() {
		return readString(Pos.CW45R_DMM_MDL_NM, Len.CW45R_DMM_MDL_NM);
	}

	public void setCw45rDmmLstNmCi(char cw45rDmmLstNmCi) {
		writeChar(Pos.CW45R_DMM_LST_NM_CI, cw45rDmmLstNmCi);
	}

	/**Original name: CW45R-DMM-LST-NM-CI<br>*/
	public char getCw45rDmmLstNmCi() {
		return readChar(Pos.CW45R_DMM_LST_NM_CI);
	}

	public void setCw45rDmmLstNm(String cw45rDmmLstNm) {
		writeString(Pos.CW45R_DMM_LST_NM, cw45rDmmLstNm, Len.CW45R_DMM_LST_NM);
	}

	/**Original name: CW45R-DMM-LST-NM<br>*/
	public String getCw45rDmmLstNm() {
		return readString(Pos.CW45R_DMM_LST_NM, Len.CW45R_DMM_LST_NM);
	}

	public void setCw45rDmmNmCi(char cw45rDmmNmCi) {
		writeChar(Pos.CW45R_DMM_NM_CI, cw45rDmmNmCi);
	}

	/**Original name: CW45R-DMM-NM-CI<br>*/
	public char getCw45rDmmNmCi() {
		return readChar(Pos.CW45R_DMM_NM_CI);
	}

	public void setCw45rDmmNm(String cw45rDmmNm) {
		writeString(Pos.CW45R_DMM_NM, cw45rDmmNm, Len.CW45R_DMM_NM);
	}

	/**Original name: CW45R-DMM-NM<br>*/
	public String getCw45rDmmNm() {
		return readString(Pos.CW45R_DMM_NM, Len.CW45R_DMM_NM);
	}

	public void setCw45rDmmNmSfxCi(char cw45rDmmNmSfxCi) {
		writeChar(Pos.CW45R_DMM_NM_SFX_CI, cw45rDmmNmSfxCi);
	}

	/**Original name: CW45R-DMM-NM-SFX-CI<br>*/
	public char getCw45rDmmNmSfxCi() {
		return readChar(Pos.CW45R_DMM_NM_SFX_CI);
	}

	public void setCw45rDmmNmSfx(String cw45rDmmNmSfx) {
		writeString(Pos.CW45R_DMM_NM_SFX, cw45rDmmNmSfx, Len.CW45R_DMM_NM_SFX);
	}

	/**Original name: CW45R-DMM-NM-SFX<br>*/
	public String getCw45rDmmNmSfx() {
		return readString(Pos.CW45R_DMM_NM_SFX, Len.CW45R_DMM_NM_SFX);
	}

	public void setCw45rDmmCltIdCi(char cw45rDmmCltIdCi) {
		writeChar(Pos.CW45R_DMM_CLT_ID_CI, cw45rDmmCltIdCi);
	}

	/**Original name: CW45R-DMM-CLT-ID-CI<br>*/
	public char getCw45rDmmCltIdCi() {
		return readChar(Pos.CW45R_DMM_CLT_ID_CI);
	}

	public void setCw45rDmmCltId(String cw45rDmmCltId) {
		writeString(Pos.CW45R_DMM_CLT_ID, cw45rDmmCltId, Len.CW45R_DMM_CLT_ID);
	}

	/**Original name: CW45R-DMM-CLT-ID<br>*/
	public String getCw45rDmmCltId() {
		return readString(Pos.CW45R_DMM_CLT_ID, Len.CW45R_DMM_CLT_ID);
	}

	public void setCw45rRmmTerNbrCi(char cw45rRmmTerNbrCi) {
		writeChar(Pos.CW45R_RMM_TER_NBR_CI, cw45rRmmTerNbrCi);
	}

	/**Original name: CW45R-RMM-TER-NBR-CI<br>*/
	public char getCw45rRmmTerNbrCi() {
		return readChar(Pos.CW45R_RMM_TER_NBR_CI);
	}

	public void setCw45rRmmTerNbr(String cw45rRmmTerNbr) {
		writeString(Pos.CW45R_RMM_TER_NBR, cw45rRmmTerNbr, Len.CW45R_RMM_TER_NBR);
	}

	/**Original name: CW45R-RMM-TER-NBR<br>*/
	public String getCw45rRmmTerNbr() {
		return readString(Pos.CW45R_RMM_TER_NBR, Len.CW45R_RMM_TER_NBR);
	}

	public void setCw45rRmmTerCltIdCi(char cw45rRmmTerCltIdCi) {
		writeChar(Pos.CW45R_RMM_TER_CLT_ID_CI, cw45rRmmTerCltIdCi);
	}

	/**Original name: CW45R-RMM-TER-CLT-ID-CI<br>*/
	public char getCw45rRmmTerCltIdCi() {
		return readChar(Pos.CW45R_RMM_TER_CLT_ID_CI);
	}

	public void setCw45rRmmTerCltId(String cw45rRmmTerCltId) {
		writeString(Pos.CW45R_RMM_TER_CLT_ID, cw45rRmmTerCltId, Len.CW45R_RMM_TER_CLT_ID);
	}

	/**Original name: CW45R-RMM-TER-CLT-ID<br>*/
	public String getCw45rRmmTerCltId() {
		return readString(Pos.CW45R_RMM_TER_CLT_ID, Len.CW45R_RMM_TER_CLT_ID);
	}

	public void setCw45rRmmNmPfxCi(char cw45rRmmNmPfxCi) {
		writeChar(Pos.CW45R_RMM_NM_PFX_CI, cw45rRmmNmPfxCi);
	}

	/**Original name: CW45R-RMM-NM-PFX-CI<br>*/
	public char getCw45rRmmNmPfxCi() {
		return readChar(Pos.CW45R_RMM_NM_PFX_CI);
	}

	public void setCw45rRmmNmPfx(String cw45rRmmNmPfx) {
		writeString(Pos.CW45R_RMM_NM_PFX, cw45rRmmNmPfx, Len.CW45R_RMM_NM_PFX);
	}

	/**Original name: CW45R-RMM-NM-PFX<br>*/
	public String getCw45rRmmNmPfx() {
		return readString(Pos.CW45R_RMM_NM_PFX, Len.CW45R_RMM_NM_PFX);
	}

	public void setCw45rRmmFstNmCi(char cw45rRmmFstNmCi) {
		writeChar(Pos.CW45R_RMM_FST_NM_CI, cw45rRmmFstNmCi);
	}

	/**Original name: CW45R-RMM-FST-NM-CI<br>*/
	public char getCw45rRmmFstNmCi() {
		return readChar(Pos.CW45R_RMM_FST_NM_CI);
	}

	public void setCw45rRmmFstNm(String cw45rRmmFstNm) {
		writeString(Pos.CW45R_RMM_FST_NM, cw45rRmmFstNm, Len.CW45R_RMM_FST_NM);
	}

	/**Original name: CW45R-RMM-FST-NM<br>*/
	public String getCw45rRmmFstNm() {
		return readString(Pos.CW45R_RMM_FST_NM, Len.CW45R_RMM_FST_NM);
	}

	public void setCw45rRmmMdlNmCi(char cw45rRmmMdlNmCi) {
		writeChar(Pos.CW45R_RMM_MDL_NM_CI, cw45rRmmMdlNmCi);
	}

	/**Original name: CW45R-RMM-MDL-NM-CI<br>*/
	public char getCw45rRmmMdlNmCi() {
		return readChar(Pos.CW45R_RMM_MDL_NM_CI);
	}

	public void setCw45rRmmMdlNm(String cw45rRmmMdlNm) {
		writeString(Pos.CW45R_RMM_MDL_NM, cw45rRmmMdlNm, Len.CW45R_RMM_MDL_NM);
	}

	/**Original name: CW45R-RMM-MDL-NM<br>*/
	public String getCw45rRmmMdlNm() {
		return readString(Pos.CW45R_RMM_MDL_NM, Len.CW45R_RMM_MDL_NM);
	}

	public void setCw45rRmmLstNmCi(char cw45rRmmLstNmCi) {
		writeChar(Pos.CW45R_RMM_LST_NM_CI, cw45rRmmLstNmCi);
	}

	/**Original name: CW45R-RMM-LST-NM-CI<br>*/
	public char getCw45rRmmLstNmCi() {
		return readChar(Pos.CW45R_RMM_LST_NM_CI);
	}

	public void setCw45rRmmLstNm(String cw45rRmmLstNm) {
		writeString(Pos.CW45R_RMM_LST_NM, cw45rRmmLstNm, Len.CW45R_RMM_LST_NM);
	}

	/**Original name: CW45R-RMM-LST-NM<br>*/
	public String getCw45rRmmLstNm() {
		return readString(Pos.CW45R_RMM_LST_NM, Len.CW45R_RMM_LST_NM);
	}

	public void setCw45rRmmNmCi(char cw45rRmmNmCi) {
		writeChar(Pos.CW45R_RMM_NM_CI, cw45rRmmNmCi);
	}

	/**Original name: CW45R-RMM-NM-CI<br>*/
	public char getCw45rRmmNmCi() {
		return readChar(Pos.CW45R_RMM_NM_CI);
	}

	public void setCw45rRmmNm(String cw45rRmmNm) {
		writeString(Pos.CW45R_RMM_NM, cw45rRmmNm, Len.CW45R_RMM_NM);
	}

	/**Original name: CW45R-RMM-NM<br>*/
	public String getCw45rRmmNm() {
		return readString(Pos.CW45R_RMM_NM, Len.CW45R_RMM_NM);
	}

	public void setCw45rRmmNmSfxCi(char cw45rRmmNmSfxCi) {
		writeChar(Pos.CW45R_RMM_NM_SFX_CI, cw45rRmmNmSfxCi);
	}

	/**Original name: CW45R-RMM-NM-SFX-CI<br>*/
	public char getCw45rRmmNmSfxCi() {
		return readChar(Pos.CW45R_RMM_NM_SFX_CI);
	}

	public void setCw45rRmmNmSfx(String cw45rRmmNmSfx) {
		writeString(Pos.CW45R_RMM_NM_SFX, cw45rRmmNmSfx, Len.CW45R_RMM_NM_SFX);
	}

	/**Original name: CW45R-RMM-NM-SFX<br>*/
	public String getCw45rRmmNmSfx() {
		return readString(Pos.CW45R_RMM_NM_SFX, Len.CW45R_RMM_NM_SFX);
	}

	public void setCw45rRmmCltIdCi(char cw45rRmmCltIdCi) {
		writeChar(Pos.CW45R_RMM_CLT_ID_CI, cw45rRmmCltIdCi);
	}

	/**Original name: CW45R-RMM-CLT-ID-CI<br>*/
	public char getCw45rRmmCltIdCi() {
		return readChar(Pos.CW45R_RMM_CLT_ID_CI);
	}

	public void setCw45rRmmCltId(String cw45rRmmCltId) {
		writeString(Pos.CW45R_RMM_CLT_ID, cw45rRmmCltId, Len.CW45R_RMM_CLT_ID);
	}

	/**Original name: CW45R-RMM-CLT-ID<br>*/
	public String getCw45rRmmCltId() {
		return readString(Pos.CW45R_RMM_CLT_ID, Len.CW45R_RMM_CLT_ID);
	}

	public void setCw45rDfoTerNbrCi(char cw45rDfoTerNbrCi) {
		writeChar(Pos.CW45R_DFO_TER_NBR_CI, cw45rDfoTerNbrCi);
	}

	/**Original name: CW45R-DFO-TER-NBR-CI<br>*/
	public char getCw45rDfoTerNbrCi() {
		return readChar(Pos.CW45R_DFO_TER_NBR_CI);
	}

	public void setCw45rDfoTerNbr(String cw45rDfoTerNbr) {
		writeString(Pos.CW45R_DFO_TER_NBR, cw45rDfoTerNbr, Len.CW45R_DFO_TER_NBR);
	}

	/**Original name: CW45R-DFO-TER-NBR<br>*/
	public String getCw45rDfoTerNbr() {
		return readString(Pos.CW45R_DFO_TER_NBR, Len.CW45R_DFO_TER_NBR);
	}

	public void setCw45rDfoTerCltIdCi(char cw45rDfoTerCltIdCi) {
		writeChar(Pos.CW45R_DFO_TER_CLT_ID_CI, cw45rDfoTerCltIdCi);
	}

	/**Original name: CW45R-DFO-TER-CLT-ID-CI<br>*/
	public char getCw45rDfoTerCltIdCi() {
		return readChar(Pos.CW45R_DFO_TER_CLT_ID_CI);
	}

	public void setCw45rDfoTerCltId(String cw45rDfoTerCltId) {
		writeString(Pos.CW45R_DFO_TER_CLT_ID, cw45rDfoTerCltId, Len.CW45R_DFO_TER_CLT_ID);
	}

	/**Original name: CW45R-DFO-TER-CLT-ID<br>*/
	public String getCw45rDfoTerCltId() {
		return readString(Pos.CW45R_DFO_TER_CLT_ID, Len.CW45R_DFO_TER_CLT_ID);
	}

	public void setCw45rDfoNmPfxCi(char cw45rDfoNmPfxCi) {
		writeChar(Pos.CW45R_DFO_NM_PFX_CI, cw45rDfoNmPfxCi);
	}

	/**Original name: CW45R-DFO-NM-PFX-CI<br>*/
	public char getCw45rDfoNmPfxCi() {
		return readChar(Pos.CW45R_DFO_NM_PFX_CI);
	}

	public void setCw45rDfoNmPfx(String cw45rDfoNmPfx) {
		writeString(Pos.CW45R_DFO_NM_PFX, cw45rDfoNmPfx, Len.CW45R_DFO_NM_PFX);
	}

	/**Original name: CW45R-DFO-NM-PFX<br>*/
	public String getCw45rDfoNmPfx() {
		return readString(Pos.CW45R_DFO_NM_PFX, Len.CW45R_DFO_NM_PFX);
	}

	public void setCw45rDfoFstNmCi(char cw45rDfoFstNmCi) {
		writeChar(Pos.CW45R_DFO_FST_NM_CI, cw45rDfoFstNmCi);
	}

	/**Original name: CW45R-DFO-FST-NM-CI<br>*/
	public char getCw45rDfoFstNmCi() {
		return readChar(Pos.CW45R_DFO_FST_NM_CI);
	}

	public void setCw45rDfoFstNm(String cw45rDfoFstNm) {
		writeString(Pos.CW45R_DFO_FST_NM, cw45rDfoFstNm, Len.CW45R_DFO_FST_NM);
	}

	/**Original name: CW45R-DFO-FST-NM<br>*/
	public String getCw45rDfoFstNm() {
		return readString(Pos.CW45R_DFO_FST_NM, Len.CW45R_DFO_FST_NM);
	}

	public void setCw45rDfoMdlNmCi(char cw45rDfoMdlNmCi) {
		writeChar(Pos.CW45R_DFO_MDL_NM_CI, cw45rDfoMdlNmCi);
	}

	/**Original name: CW45R-DFO-MDL-NM-CI<br>*/
	public char getCw45rDfoMdlNmCi() {
		return readChar(Pos.CW45R_DFO_MDL_NM_CI);
	}

	public void setCw45rDfoMdlNm(String cw45rDfoMdlNm) {
		writeString(Pos.CW45R_DFO_MDL_NM, cw45rDfoMdlNm, Len.CW45R_DFO_MDL_NM);
	}

	/**Original name: CW45R-DFO-MDL-NM<br>*/
	public String getCw45rDfoMdlNm() {
		return readString(Pos.CW45R_DFO_MDL_NM, Len.CW45R_DFO_MDL_NM);
	}

	public void setCw45rDfoLstNmCi(char cw45rDfoLstNmCi) {
		writeChar(Pos.CW45R_DFO_LST_NM_CI, cw45rDfoLstNmCi);
	}

	/**Original name: CW45R-DFO-LST-NM-CI<br>*/
	public char getCw45rDfoLstNmCi() {
		return readChar(Pos.CW45R_DFO_LST_NM_CI);
	}

	public void setCw45rDfoLstNm(String cw45rDfoLstNm) {
		writeString(Pos.CW45R_DFO_LST_NM, cw45rDfoLstNm, Len.CW45R_DFO_LST_NM);
	}

	/**Original name: CW45R-DFO-LST-NM<br>*/
	public String getCw45rDfoLstNm() {
		return readString(Pos.CW45R_DFO_LST_NM, Len.CW45R_DFO_LST_NM);
	}

	public void setCw45rDfoNmCi(char cw45rDfoNmCi) {
		writeChar(Pos.CW45R_DFO_NM_CI, cw45rDfoNmCi);
	}

	/**Original name: CW45R-DFO-NM-CI<br>*/
	public char getCw45rDfoNmCi() {
		return readChar(Pos.CW45R_DFO_NM_CI);
	}

	public void setCw45rDfoNm(String cw45rDfoNm) {
		writeString(Pos.CW45R_DFO_NM, cw45rDfoNm, Len.CW45R_DFO_NM);
	}

	/**Original name: CW45R-DFO-NM<br>*/
	public String getCw45rDfoNm() {
		return readString(Pos.CW45R_DFO_NM, Len.CW45R_DFO_NM);
	}

	public void setCw45rDfoNmSfxCi(char cw45rDfoNmSfxCi) {
		writeChar(Pos.CW45R_DFO_NM_SFX_CI, cw45rDfoNmSfxCi);
	}

	/**Original name: CW45R-DFO-NM-SFX-CI<br>*/
	public char getCw45rDfoNmSfxCi() {
		return readChar(Pos.CW45R_DFO_NM_SFX_CI);
	}

	public void setCw45rDfoNmSfx(String cw45rDfoNmSfx) {
		writeString(Pos.CW45R_DFO_NM_SFX, cw45rDfoNmSfx, Len.CW45R_DFO_NM_SFX);
	}

	/**Original name: CW45R-DFO-NM-SFX<br>*/
	public String getCw45rDfoNmSfx() {
		return readString(Pos.CW45R_DFO_NM_SFX, Len.CW45R_DFO_NM_SFX);
	}

	public void setCw45rDfoCltIdCi(char cw45rDfoCltIdCi) {
		writeChar(Pos.CW45R_DFO_CLT_ID_CI, cw45rDfoCltIdCi);
	}

	/**Original name: CW45R-DFO-CLT-ID-CI<br>*/
	public char getCw45rDfoCltIdCi() {
		return readChar(Pos.CW45R_DFO_CLT_ID_CI);
	}

	public void setCw45rDfoCltId(String cw45rDfoCltId) {
		writeString(Pos.CW45R_DFO_CLT_ID, cw45rDfoCltId, Len.CW45R_DFO_CLT_ID);
	}

	/**Original name: CW45R-DFO-CLT-ID<br>*/
	public String getCw45rDfoCltId() {
		return readString(Pos.CW45R_DFO_CLT_ID, Len.CW45R_DFO_CLT_ID);
	}

	public void setCw46rCltAgcTerStcRowFormatted(String data) {
		writeString(Pos.CW46R_CLT_AGC_TER_STC_ROW, data, Len.CW46R_CLT_AGC_TER_STC_ROW);
	}

	public void setCw46rCltAgcTerStcCsumFormatted(String cw46rCltAgcTerStcCsum) {
		writeString(Pos.CW46R_CLT_AGC_TER_STC_CSUM, Trunc.toUnsignedNumeric(cw46rCltAgcTerStcCsum, Len.CW46R_CLT_AGC_TER_STC_CSUM),
				Len.CW46R_CLT_AGC_TER_STC_CSUM);
	}

	/**Original name: CW46R-CLT-AGC-TER-STC-CSUM<br>*/
	public int getCw46rCltAgcTerStcCsum() {
		return readNumDispUnsignedInt(Pos.CW46R_CLT_AGC_TER_STC_CSUM, Len.CW46R_CLT_AGC_TER_STC_CSUM);
	}

	public void setCw46rTerCltIdKcre(String cw46rTerCltIdKcre) {
		writeString(Pos.CW46R_TER_CLT_ID_KCRE, cw46rTerCltIdKcre, Len.CW46R_TER_CLT_ID_KCRE);
	}

	/**Original name: CW46R-TER-CLT-ID-KCRE<br>*/
	public String getCw46rTerCltIdKcre() {
		return readString(Pos.CW46R_TER_CLT_ID_KCRE, Len.CW46R_TER_CLT_ID_KCRE);
	}

	public void setCw46rTerLevelCdKcre(String cw46rTerLevelCdKcre) {
		writeString(Pos.CW46R_TER_LEVEL_CD_KCRE, cw46rTerLevelCdKcre, Len.CW46R_TER_LEVEL_CD_KCRE);
	}

	/**Original name: CW46R-TER-LEVEL-CD-KCRE<br>*/
	public String getCw46rTerLevelCdKcre() {
		return readString(Pos.CW46R_TER_LEVEL_CD_KCRE, Len.CW46R_TER_LEVEL_CD_KCRE);
	}

	public void setCw46rTransProcessDt(String cw46rTransProcessDt) {
		writeString(Pos.CW46R_TRANS_PROCESS_DT, cw46rTransProcessDt, Len.CW46R_TRANS_PROCESS_DT);
	}

	/**Original name: CW46R-TRANS-PROCESS-DT<br>*/
	public String getCw46rTransProcessDt() {
		return readString(Pos.CW46R_TRANS_PROCESS_DT, Len.CW46R_TRANS_PROCESS_DT);
	}

	public void setCw46rTerCltId(String cw46rTerCltId) {
		writeString(Pos.CW46R_TER_CLT_ID, cw46rTerCltId, Len.CW46R_TER_CLT_ID);
	}

	/**Original name: CW46R-TER-CLT-ID<br>*/
	public String getCw46rTerCltId() {
		return readString(Pos.CW46R_TER_CLT_ID, Len.CW46R_TER_CLT_ID);
	}

	public void setCw46rTerLevelCd(String cw46rTerLevelCd) {
		writeString(Pos.CW46R_TER_LEVEL_CD, cw46rTerLevelCd, Len.CW46R_TER_LEVEL_CD);
	}

	/**Original name: CW46R-TER-LEVEL-CD<br>*/
	public String getCw46rTerLevelCd() {
		return readString(Pos.CW46R_TER_LEVEL_CD, Len.CW46R_TER_LEVEL_CD);
	}

	public void setCw46rTerCltIdCi(char cw46rTerCltIdCi) {
		writeChar(Pos.CW46R_TER_CLT_ID_CI, cw46rTerCltIdCi);
	}

	/**Original name: CW46R-TER-CLT-ID-CI<br>*/
	public char getCw46rTerCltIdCi() {
		return readChar(Pos.CW46R_TER_CLT_ID_CI);
	}

	public void setCw46rTerLevelCdCi(char cw46rTerLevelCdCi) {
		writeChar(Pos.CW46R_TER_LEVEL_CD_CI, cw46rTerLevelCdCi);
	}

	/**Original name: CW46R-TER-LEVEL-CD-CI<br>*/
	public char getCw46rTerLevelCdCi() {
		return readChar(Pos.CW46R_TER_LEVEL_CD_CI);
	}

	public void setCw46rTerNbrCi(char cw46rTerNbrCi) {
		writeChar(Pos.CW46R_TER_NBR_CI, cw46rTerNbrCi);
	}

	/**Original name: CW46R-TER-NBR-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw46rTerNbrCi() {
		return readChar(Pos.CW46R_TER_NBR_CI);
	}

	public void setCw46rTerNbr(String cw46rTerNbr) {
		writeString(Pos.CW46R_TER_NBR, cw46rTerNbr, Len.CW46R_TER_NBR);
	}

	/**Original name: CW46R-TER-NBR<br>*/
	public String getCw46rTerNbr() {
		return readString(Pos.CW46R_TER_NBR, Len.CW46R_TER_NBR);
	}

	public void setCw46rBrnTerNbrCi(char cw46rBrnTerNbrCi) {
		writeChar(Pos.CW46R_BRN_TER_NBR_CI, cw46rBrnTerNbrCi);
	}

	/**Original name: CW46R-BRN-TER-NBR-CI<br>*/
	public char getCw46rBrnTerNbrCi() {
		return readChar(Pos.CW46R_BRN_TER_NBR_CI);
	}

	public void setCw46rBrnTerNbr(String cw46rBrnTerNbr) {
		writeString(Pos.CW46R_BRN_TER_NBR, cw46rBrnTerNbr, Len.CW46R_BRN_TER_NBR);
	}

	/**Original name: CW46R-BRN-TER-NBR<br>*/
	public String getCw46rBrnTerNbr() {
		return readString(Pos.CW46R_BRN_TER_NBR, Len.CW46R_BRN_TER_NBR);
	}

	public void setCw46rBrnTerCltIdCi(char cw46rBrnTerCltIdCi) {
		writeChar(Pos.CW46R_BRN_TER_CLT_ID_CI, cw46rBrnTerCltIdCi);
	}

	/**Original name: CW46R-BRN-TER-CLT-ID-CI<br>*/
	public char getCw46rBrnTerCltIdCi() {
		return readChar(Pos.CW46R_BRN_TER_CLT_ID_CI);
	}

	public void setCw46rBrnTerCltId(String cw46rBrnTerCltId) {
		writeString(Pos.CW46R_BRN_TER_CLT_ID, cw46rBrnTerCltId, Len.CW46R_BRN_TER_CLT_ID);
	}

	/**Original name: CW46R-BRN-TER-CLT-ID<br>*/
	public String getCw46rBrnTerCltId() {
		return readString(Pos.CW46R_BRN_TER_CLT_ID, Len.CW46R_BRN_TER_CLT_ID);
	}

	public void setCw46rBrnNmCi(char cw46rBrnNmCi) {
		writeChar(Pos.CW46R_BRN_NM_CI, cw46rBrnNmCi);
	}

	/**Original name: CW46R-BRN-NM-CI<br>*/
	public char getCw46rBrnNmCi() {
		return readChar(Pos.CW46R_BRN_NM_CI);
	}

	public void setCw46rBrnNm(String cw46rBrnNm) {
		writeString(Pos.CW46R_BRN_NM, cw46rBrnNm, Len.CW46R_BRN_NM);
	}

	/**Original name: CW46R-BRN-NM<br>*/
	public String getCw46rBrnNm() {
		return readString(Pos.CW46R_BRN_NM, Len.CW46R_BRN_NM);
	}

	public void setCw46rBrnCltIdCi(char cw46rBrnCltIdCi) {
		writeChar(Pos.CW46R_BRN_CLT_ID_CI, cw46rBrnCltIdCi);
	}

	/**Original name: CW46R-BRN-CLT-ID-CI<br>*/
	public char getCw46rBrnCltIdCi() {
		return readChar(Pos.CW46R_BRN_CLT_ID_CI);
	}

	public void setCw46rBrnCltId(String cw46rBrnCltId) {
		writeString(Pos.CW46R_BRN_CLT_ID, cw46rBrnCltId, Len.CW46R_BRN_CLT_ID);
	}

	/**Original name: CW46R-BRN-CLT-ID<br>*/
	public String getCw46rBrnCltId() {
		return readString(Pos.CW46R_BRN_CLT_ID, Len.CW46R_BRN_CLT_ID);
	}

	public void setCw46rAgcTerNbrCi(char cw46rAgcTerNbrCi) {
		writeChar(Pos.CW46R_AGC_TER_NBR_CI, cw46rAgcTerNbrCi);
	}

	/**Original name: CW46R-AGC-TER-NBR-CI<br>*/
	public char getCw46rAgcTerNbrCi() {
		return readChar(Pos.CW46R_AGC_TER_NBR_CI);
	}

	public void setCw46rAgcTerNbr(String cw46rAgcTerNbr) {
		writeString(Pos.CW46R_AGC_TER_NBR, cw46rAgcTerNbr, Len.CW46R_AGC_TER_NBR);
	}

	/**Original name: CW46R-AGC-TER-NBR<br>*/
	public String getCw46rAgcTerNbr() {
		return readString(Pos.CW46R_AGC_TER_NBR, Len.CW46R_AGC_TER_NBR);
	}

	public void setCw46rAgcTerCltIdCi(char cw46rAgcTerCltIdCi) {
		writeChar(Pos.CW46R_AGC_TER_CLT_ID_CI, cw46rAgcTerCltIdCi);
	}

	/**Original name: CW46R-AGC-TER-CLT-ID-CI<br>*/
	public char getCw46rAgcTerCltIdCi() {
		return readChar(Pos.CW46R_AGC_TER_CLT_ID_CI);
	}

	public void setCw46rAgcTerCltId(String cw46rAgcTerCltId) {
		writeString(Pos.CW46R_AGC_TER_CLT_ID, cw46rAgcTerCltId, Len.CW46R_AGC_TER_CLT_ID);
	}

	/**Original name: CW46R-AGC-TER-CLT-ID<br>*/
	public String getCw46rAgcTerCltId() {
		return readString(Pos.CW46R_AGC_TER_CLT_ID, Len.CW46R_AGC_TER_CLT_ID);
	}

	public void setCw46rAgcNmCi(char cw46rAgcNmCi) {
		writeChar(Pos.CW46R_AGC_NM_CI, cw46rAgcNmCi);
	}

	/**Original name: CW46R-AGC-NM-CI<br>*/
	public char getCw46rAgcNmCi() {
		return readChar(Pos.CW46R_AGC_NM_CI);
	}

	public void setCw46rAgcNm(String cw46rAgcNm) {
		writeString(Pos.CW46R_AGC_NM, cw46rAgcNm, Len.CW46R_AGC_NM);
	}

	/**Original name: CW46R-AGC-NM<br>*/
	public String getCw46rAgcNm() {
		return readString(Pos.CW46R_AGC_NM, Len.CW46R_AGC_NM);
	}

	public void setCw46rAgcCltIdCi(char cw46rAgcCltIdCi) {
		writeChar(Pos.CW46R_AGC_CLT_ID_CI, cw46rAgcCltIdCi);
	}

	/**Original name: CW46R-AGC-CLT-ID-CI<br>*/
	public char getCw46rAgcCltIdCi() {
		return readChar(Pos.CW46R_AGC_CLT_ID_CI);
	}

	public void setCw46rAgcCltId(String cw46rAgcCltId) {
		writeString(Pos.CW46R_AGC_CLT_ID, cw46rAgcCltId, Len.CW46R_AGC_CLT_ID);
	}

	/**Original name: CW46R-AGC-CLT-ID<br>*/
	public String getCw46rAgcCltId() {
		return readString(Pos.CW46R_AGC_CLT_ID, Len.CW46R_AGC_CLT_ID);
	}

	public void setCw46rSmrTerNbrCi(char cw46rSmrTerNbrCi) {
		writeChar(Pos.CW46R_SMR_TER_NBR_CI, cw46rSmrTerNbrCi);
	}

	/**Original name: CW46R-SMR-TER-NBR-CI<br>*/
	public char getCw46rSmrTerNbrCi() {
		return readChar(Pos.CW46R_SMR_TER_NBR_CI);
	}

	public void setCw46rSmrTerNbr(String cw46rSmrTerNbr) {
		writeString(Pos.CW46R_SMR_TER_NBR, cw46rSmrTerNbr, Len.CW46R_SMR_TER_NBR);
	}

	/**Original name: CW46R-SMR-TER-NBR<br>*/
	public String getCw46rSmrTerNbr() {
		return readString(Pos.CW46R_SMR_TER_NBR, Len.CW46R_SMR_TER_NBR);
	}

	public void setCw46rSmrTerCltIdCi(char cw46rSmrTerCltIdCi) {
		writeChar(Pos.CW46R_SMR_TER_CLT_ID_CI, cw46rSmrTerCltIdCi);
	}

	/**Original name: CW46R-SMR-TER-CLT-ID-CI<br>*/
	public char getCw46rSmrTerCltIdCi() {
		return readChar(Pos.CW46R_SMR_TER_CLT_ID_CI);
	}

	public void setCw46rSmrTerCltId(String cw46rSmrTerCltId) {
		writeString(Pos.CW46R_SMR_TER_CLT_ID, cw46rSmrTerCltId, Len.CW46R_SMR_TER_CLT_ID);
	}

	/**Original name: CW46R-SMR-TER-CLT-ID<br>*/
	public String getCw46rSmrTerCltId() {
		return readString(Pos.CW46R_SMR_TER_CLT_ID, Len.CW46R_SMR_TER_CLT_ID);
	}

	public void setCw46rSmrNmCi(char cw46rSmrNmCi) {
		writeChar(Pos.CW46R_SMR_NM_CI, cw46rSmrNmCi);
	}

	/**Original name: CW46R-SMR-NM-CI<br>*/
	public char getCw46rSmrNmCi() {
		return readChar(Pos.CW46R_SMR_NM_CI);
	}

	public void setCw46rSmrNm(String cw46rSmrNm) {
		writeString(Pos.CW46R_SMR_NM, cw46rSmrNm, Len.CW46R_SMR_NM);
	}

	/**Original name: CW46R-SMR-NM<br>*/
	public String getCw46rSmrNm() {
		return readString(Pos.CW46R_SMR_NM, Len.CW46R_SMR_NM);
	}

	public void setCw46rSmrCltIdCi(char cw46rSmrCltIdCi) {
		writeChar(Pos.CW46R_SMR_CLT_ID_CI, cw46rSmrCltIdCi);
	}

	/**Original name: CW46R-SMR-CLT-ID-CI<br>*/
	public char getCw46rSmrCltIdCi() {
		return readChar(Pos.CW46R_SMR_CLT_ID_CI);
	}

	public void setCw46rSmrCltId(String cw46rSmrCltId) {
		writeString(Pos.CW46R_SMR_CLT_ID, cw46rSmrCltId, Len.CW46R_SMR_CLT_ID);
	}

	/**Original name: CW46R-SMR-CLT-ID<br>*/
	public String getCw46rSmrCltId() {
		return readString(Pos.CW46R_SMR_CLT_ID, Len.CW46R_SMR_CLT_ID);
	}

	public void setCw46rAmmTerNbrCi(char cw46rAmmTerNbrCi) {
		writeChar(Pos.CW46R_AMM_TER_NBR_CI, cw46rAmmTerNbrCi);
	}

	/**Original name: CW46R-AMM-TER-NBR-CI<br>*/
	public char getCw46rAmmTerNbrCi() {
		return readChar(Pos.CW46R_AMM_TER_NBR_CI);
	}

	public void setCw46rAmmTerNbr(String cw46rAmmTerNbr) {
		writeString(Pos.CW46R_AMM_TER_NBR, cw46rAmmTerNbr, Len.CW46R_AMM_TER_NBR);
	}

	/**Original name: CW46R-AMM-TER-NBR<br>*/
	public String getCw46rAmmTerNbr() {
		return readString(Pos.CW46R_AMM_TER_NBR, Len.CW46R_AMM_TER_NBR);
	}

	public void setCw46rAmmTerCltIdCi(char cw46rAmmTerCltIdCi) {
		writeChar(Pos.CW46R_AMM_TER_CLT_ID_CI, cw46rAmmTerCltIdCi);
	}

	/**Original name: CW46R-AMM-TER-CLT-ID-CI<br>*/
	public char getCw46rAmmTerCltIdCi() {
		return readChar(Pos.CW46R_AMM_TER_CLT_ID_CI);
	}

	public void setCw46rAmmTerCltId(String cw46rAmmTerCltId) {
		writeString(Pos.CW46R_AMM_TER_CLT_ID, cw46rAmmTerCltId, Len.CW46R_AMM_TER_CLT_ID);
	}

	/**Original name: CW46R-AMM-TER-CLT-ID<br>*/
	public String getCw46rAmmTerCltId() {
		return readString(Pos.CW46R_AMM_TER_CLT_ID, Len.CW46R_AMM_TER_CLT_ID);
	}

	public void setCw46rAmmNmPfxCi(char cw46rAmmNmPfxCi) {
		writeChar(Pos.CW46R_AMM_NM_PFX_CI, cw46rAmmNmPfxCi);
	}

	/**Original name: CW46R-AMM-NM-PFX-CI<br>*/
	public char getCw46rAmmNmPfxCi() {
		return readChar(Pos.CW46R_AMM_NM_PFX_CI);
	}

	public void setCw46rAmmNmPfx(String cw46rAmmNmPfx) {
		writeString(Pos.CW46R_AMM_NM_PFX, cw46rAmmNmPfx, Len.CW46R_AMM_NM_PFX);
	}

	/**Original name: CW46R-AMM-NM-PFX<br>*/
	public String getCw46rAmmNmPfx() {
		return readString(Pos.CW46R_AMM_NM_PFX, Len.CW46R_AMM_NM_PFX);
	}

	public void setCw46rAmmFstNmCi(char cw46rAmmFstNmCi) {
		writeChar(Pos.CW46R_AMM_FST_NM_CI, cw46rAmmFstNmCi);
	}

	/**Original name: CW46R-AMM-FST-NM-CI<br>*/
	public char getCw46rAmmFstNmCi() {
		return readChar(Pos.CW46R_AMM_FST_NM_CI);
	}

	public void setCw46rAmmFstNm(String cw46rAmmFstNm) {
		writeString(Pos.CW46R_AMM_FST_NM, cw46rAmmFstNm, Len.CW46R_AMM_FST_NM);
	}

	/**Original name: CW46R-AMM-FST-NM<br>*/
	public String getCw46rAmmFstNm() {
		return readString(Pos.CW46R_AMM_FST_NM, Len.CW46R_AMM_FST_NM);
	}

	public void setCw46rAmmMdlNmCi(char cw46rAmmMdlNmCi) {
		writeChar(Pos.CW46R_AMM_MDL_NM_CI, cw46rAmmMdlNmCi);
	}

	/**Original name: CW46R-AMM-MDL-NM-CI<br>*/
	public char getCw46rAmmMdlNmCi() {
		return readChar(Pos.CW46R_AMM_MDL_NM_CI);
	}

	public void setCw46rAmmMdlNm(String cw46rAmmMdlNm) {
		writeString(Pos.CW46R_AMM_MDL_NM, cw46rAmmMdlNm, Len.CW46R_AMM_MDL_NM);
	}

	/**Original name: CW46R-AMM-MDL-NM<br>*/
	public String getCw46rAmmMdlNm() {
		return readString(Pos.CW46R_AMM_MDL_NM, Len.CW46R_AMM_MDL_NM);
	}

	public void setCw46rAmmLstNmCi(char cw46rAmmLstNmCi) {
		writeChar(Pos.CW46R_AMM_LST_NM_CI, cw46rAmmLstNmCi);
	}

	/**Original name: CW46R-AMM-LST-NM-CI<br>*/
	public char getCw46rAmmLstNmCi() {
		return readChar(Pos.CW46R_AMM_LST_NM_CI);
	}

	public void setCw46rAmmLstNm(String cw46rAmmLstNm) {
		writeString(Pos.CW46R_AMM_LST_NM, cw46rAmmLstNm, Len.CW46R_AMM_LST_NM);
	}

	/**Original name: CW46R-AMM-LST-NM<br>*/
	public String getCw46rAmmLstNm() {
		return readString(Pos.CW46R_AMM_LST_NM, Len.CW46R_AMM_LST_NM);
	}

	public void setCw46rAmmNmCi(char cw46rAmmNmCi) {
		writeChar(Pos.CW46R_AMM_NM_CI, cw46rAmmNmCi);
	}

	/**Original name: CW46R-AMM-NM-CI<br>*/
	public char getCw46rAmmNmCi() {
		return readChar(Pos.CW46R_AMM_NM_CI);
	}

	public void setCw46rAmmNm(String cw46rAmmNm) {
		writeString(Pos.CW46R_AMM_NM, cw46rAmmNm, Len.CW46R_AMM_NM);
	}

	/**Original name: CW46R-AMM-NM<br>*/
	public String getCw46rAmmNm() {
		return readString(Pos.CW46R_AMM_NM, Len.CW46R_AMM_NM);
	}

	public void setCw46rAmmNmSfxCi(char cw46rAmmNmSfxCi) {
		writeChar(Pos.CW46R_AMM_NM_SFX_CI, cw46rAmmNmSfxCi);
	}

	/**Original name: CW46R-AMM-NM-SFX-CI<br>*/
	public char getCw46rAmmNmSfxCi() {
		return readChar(Pos.CW46R_AMM_NM_SFX_CI);
	}

	public void setCw46rAmmNmSfx(String cw46rAmmNmSfx) {
		writeString(Pos.CW46R_AMM_NM_SFX, cw46rAmmNmSfx, Len.CW46R_AMM_NM_SFX);
	}

	/**Original name: CW46R-AMM-NM-SFX<br>*/
	public String getCw46rAmmNmSfx() {
		return readString(Pos.CW46R_AMM_NM_SFX, Len.CW46R_AMM_NM_SFX);
	}

	public void setCw46rAmmCltIdCi(char cw46rAmmCltIdCi) {
		writeChar(Pos.CW46R_AMM_CLT_ID_CI, cw46rAmmCltIdCi);
	}

	/**Original name: CW46R-AMM-CLT-ID-CI<br>*/
	public char getCw46rAmmCltIdCi() {
		return readChar(Pos.CW46R_AMM_CLT_ID_CI);
	}

	public void setCw46rAmmCltId(String cw46rAmmCltId) {
		writeString(Pos.CW46R_AMM_CLT_ID, cw46rAmmCltId, Len.CW46R_AMM_CLT_ID);
	}

	/**Original name: CW46R-AMM-CLT-ID<br>*/
	public String getCw46rAmmCltId() {
		return readString(Pos.CW46R_AMM_CLT_ID, Len.CW46R_AMM_CLT_ID);
	}

	public void setCw46rAfmTerNbrCi(char cw46rAfmTerNbrCi) {
		writeChar(Pos.CW46R_AFM_TER_NBR_CI, cw46rAfmTerNbrCi);
	}

	/**Original name: CW46R-AFM-TER-NBR-CI<br>*/
	public char getCw46rAfmTerNbrCi() {
		return readChar(Pos.CW46R_AFM_TER_NBR_CI);
	}

	public void setCw46rAfmTerNbr(String cw46rAfmTerNbr) {
		writeString(Pos.CW46R_AFM_TER_NBR, cw46rAfmTerNbr, Len.CW46R_AFM_TER_NBR);
	}

	/**Original name: CW46R-AFM-TER-NBR<br>*/
	public String getCw46rAfmTerNbr() {
		return readString(Pos.CW46R_AFM_TER_NBR, Len.CW46R_AFM_TER_NBR);
	}

	public void setCw46rAfmTerCltIdCi(char cw46rAfmTerCltIdCi) {
		writeChar(Pos.CW46R_AFM_TER_CLT_ID_CI, cw46rAfmTerCltIdCi);
	}

	/**Original name: CW46R-AFM-TER-CLT-ID-CI<br>*/
	public char getCw46rAfmTerCltIdCi() {
		return readChar(Pos.CW46R_AFM_TER_CLT_ID_CI);
	}

	public void setCw46rAfmTerCltId(String cw46rAfmTerCltId) {
		writeString(Pos.CW46R_AFM_TER_CLT_ID, cw46rAfmTerCltId, Len.CW46R_AFM_TER_CLT_ID);
	}

	/**Original name: CW46R-AFM-TER-CLT-ID<br>*/
	public String getCw46rAfmTerCltId() {
		return readString(Pos.CW46R_AFM_TER_CLT_ID, Len.CW46R_AFM_TER_CLT_ID);
	}

	public void setCw46rAfmNmPfxCi(char cw46rAfmNmPfxCi) {
		writeChar(Pos.CW46R_AFM_NM_PFX_CI, cw46rAfmNmPfxCi);
	}

	/**Original name: CW46R-AFM-NM-PFX-CI<br>*/
	public char getCw46rAfmNmPfxCi() {
		return readChar(Pos.CW46R_AFM_NM_PFX_CI);
	}

	public void setCw46rAfmNmPfx(String cw46rAfmNmPfx) {
		writeString(Pos.CW46R_AFM_NM_PFX, cw46rAfmNmPfx, Len.CW46R_AFM_NM_PFX);
	}

	/**Original name: CW46R-AFM-NM-PFX<br>*/
	public String getCw46rAfmNmPfx() {
		return readString(Pos.CW46R_AFM_NM_PFX, Len.CW46R_AFM_NM_PFX);
	}

	public void setCw46rAfmFstNmCi(char cw46rAfmFstNmCi) {
		writeChar(Pos.CW46R_AFM_FST_NM_CI, cw46rAfmFstNmCi);
	}

	/**Original name: CW46R-AFM-FST-NM-CI<br>*/
	public char getCw46rAfmFstNmCi() {
		return readChar(Pos.CW46R_AFM_FST_NM_CI);
	}

	public void setCw46rAfmFstNm(String cw46rAfmFstNm) {
		writeString(Pos.CW46R_AFM_FST_NM, cw46rAfmFstNm, Len.CW46R_AFM_FST_NM);
	}

	/**Original name: CW46R-AFM-FST-NM<br>*/
	public String getCw46rAfmFstNm() {
		return readString(Pos.CW46R_AFM_FST_NM, Len.CW46R_AFM_FST_NM);
	}

	public void setCw46rAfmMdlNmCi(char cw46rAfmMdlNmCi) {
		writeChar(Pos.CW46R_AFM_MDL_NM_CI, cw46rAfmMdlNmCi);
	}

	/**Original name: CW46R-AFM-MDL-NM-CI<br>*/
	public char getCw46rAfmMdlNmCi() {
		return readChar(Pos.CW46R_AFM_MDL_NM_CI);
	}

	public void setCw46rAfmMdlNm(String cw46rAfmMdlNm) {
		writeString(Pos.CW46R_AFM_MDL_NM, cw46rAfmMdlNm, Len.CW46R_AFM_MDL_NM);
	}

	/**Original name: CW46R-AFM-MDL-NM<br>*/
	public String getCw46rAfmMdlNm() {
		return readString(Pos.CW46R_AFM_MDL_NM, Len.CW46R_AFM_MDL_NM);
	}

	public void setCw46rAfmLstNmCi(char cw46rAfmLstNmCi) {
		writeChar(Pos.CW46R_AFM_LST_NM_CI, cw46rAfmLstNmCi);
	}

	/**Original name: CW46R-AFM-LST-NM-CI<br>*/
	public char getCw46rAfmLstNmCi() {
		return readChar(Pos.CW46R_AFM_LST_NM_CI);
	}

	public void setCw46rAfmLstNm(String cw46rAfmLstNm) {
		writeString(Pos.CW46R_AFM_LST_NM, cw46rAfmLstNm, Len.CW46R_AFM_LST_NM);
	}

	/**Original name: CW46R-AFM-LST-NM<br>*/
	public String getCw46rAfmLstNm() {
		return readString(Pos.CW46R_AFM_LST_NM, Len.CW46R_AFM_LST_NM);
	}

	public void setCw46rAfmNmCi(char cw46rAfmNmCi) {
		writeChar(Pos.CW46R_AFM_NM_CI, cw46rAfmNmCi);
	}

	/**Original name: CW46R-AFM-NM-CI<br>*/
	public char getCw46rAfmNmCi() {
		return readChar(Pos.CW46R_AFM_NM_CI);
	}

	public void setCw46rAfmNm(String cw46rAfmNm) {
		writeString(Pos.CW46R_AFM_NM, cw46rAfmNm, Len.CW46R_AFM_NM);
	}

	/**Original name: CW46R-AFM-NM<br>*/
	public String getCw46rAfmNm() {
		return readString(Pos.CW46R_AFM_NM, Len.CW46R_AFM_NM);
	}

	public void setCw46rAfmNmSfxCi(char cw46rAfmNmSfxCi) {
		writeChar(Pos.CW46R_AFM_NM_SFX_CI, cw46rAfmNmSfxCi);
	}

	/**Original name: CW46R-AFM-NM-SFX-CI<br>*/
	public char getCw46rAfmNmSfxCi() {
		return readChar(Pos.CW46R_AFM_NM_SFX_CI);
	}

	public void setCw46rAfmNmSfx(String cw46rAfmNmSfx) {
		writeString(Pos.CW46R_AFM_NM_SFX, cw46rAfmNmSfx, Len.CW46R_AFM_NM_SFX);
	}

	/**Original name: CW46R-AFM-NM-SFX<br>*/
	public String getCw46rAfmNmSfx() {
		return readString(Pos.CW46R_AFM_NM_SFX, Len.CW46R_AFM_NM_SFX);
	}

	public void setCw46rAfmCltIdCi(char cw46rAfmCltIdCi) {
		writeChar(Pos.CW46R_AFM_CLT_ID_CI, cw46rAfmCltIdCi);
	}

	/**Original name: CW46R-AFM-CLT-ID-CI<br>*/
	public char getCw46rAfmCltIdCi() {
		return readChar(Pos.CW46R_AFM_CLT_ID_CI);
	}

	public void setCw46rAfmCltId(String cw46rAfmCltId) {
		writeString(Pos.CW46R_AFM_CLT_ID, cw46rAfmCltId, Len.CW46R_AFM_CLT_ID);
	}

	/**Original name: CW46R-AFM-CLT-ID<br>*/
	public String getCw46rAfmCltId() {
		return readString(Pos.CW46R_AFM_CLT_ID, Len.CW46R_AFM_CLT_ID);
	}

	public void setCw46rAvpTerNbrCi(char cw46rAvpTerNbrCi) {
		writeChar(Pos.CW46R_AVP_TER_NBR_CI, cw46rAvpTerNbrCi);
	}

	/**Original name: CW46R-AVP-TER-NBR-CI<br>*/
	public char getCw46rAvpTerNbrCi() {
		return readChar(Pos.CW46R_AVP_TER_NBR_CI);
	}

	public void setCw46rAvpTerNbr(String cw46rAvpTerNbr) {
		writeString(Pos.CW46R_AVP_TER_NBR, cw46rAvpTerNbr, Len.CW46R_AVP_TER_NBR);
	}

	/**Original name: CW46R-AVP-TER-NBR<br>*/
	public String getCw46rAvpTerNbr() {
		return readString(Pos.CW46R_AVP_TER_NBR, Len.CW46R_AVP_TER_NBR);
	}

	public void setCw46rAvpTerCltIdCi(char cw46rAvpTerCltIdCi) {
		writeChar(Pos.CW46R_AVP_TER_CLT_ID_CI, cw46rAvpTerCltIdCi);
	}

	/**Original name: CW46R-AVP-TER-CLT-ID-CI<br>*/
	public char getCw46rAvpTerCltIdCi() {
		return readChar(Pos.CW46R_AVP_TER_CLT_ID_CI);
	}

	public void setCw46rAvpTerCltId(String cw46rAvpTerCltId) {
		writeString(Pos.CW46R_AVP_TER_CLT_ID, cw46rAvpTerCltId, Len.CW46R_AVP_TER_CLT_ID);
	}

	/**Original name: CW46R-AVP-TER-CLT-ID<br>*/
	public String getCw46rAvpTerCltId() {
		return readString(Pos.CW46R_AVP_TER_CLT_ID, Len.CW46R_AVP_TER_CLT_ID);
	}

	public void setCw46rAvpNmPfxCi(char cw46rAvpNmPfxCi) {
		writeChar(Pos.CW46R_AVP_NM_PFX_CI, cw46rAvpNmPfxCi);
	}

	/**Original name: CW46R-AVP-NM-PFX-CI<br>*/
	public char getCw46rAvpNmPfxCi() {
		return readChar(Pos.CW46R_AVP_NM_PFX_CI);
	}

	public void setCw46rAvpNmPfx(String cw46rAvpNmPfx) {
		writeString(Pos.CW46R_AVP_NM_PFX, cw46rAvpNmPfx, Len.CW46R_AVP_NM_PFX);
	}

	/**Original name: CW46R-AVP-NM-PFX<br>*/
	public String getCw46rAvpNmPfx() {
		return readString(Pos.CW46R_AVP_NM_PFX, Len.CW46R_AVP_NM_PFX);
	}

	public void setCw46rAvpFstNmCi(char cw46rAvpFstNmCi) {
		writeChar(Pos.CW46R_AVP_FST_NM_CI, cw46rAvpFstNmCi);
	}

	/**Original name: CW46R-AVP-FST-NM-CI<br>*/
	public char getCw46rAvpFstNmCi() {
		return readChar(Pos.CW46R_AVP_FST_NM_CI);
	}

	public void setCw46rAvpFstNm(String cw46rAvpFstNm) {
		writeString(Pos.CW46R_AVP_FST_NM, cw46rAvpFstNm, Len.CW46R_AVP_FST_NM);
	}

	/**Original name: CW46R-AVP-FST-NM<br>*/
	public String getCw46rAvpFstNm() {
		return readString(Pos.CW46R_AVP_FST_NM, Len.CW46R_AVP_FST_NM);
	}

	public void setCw46rAvpMdlNmCi(char cw46rAvpMdlNmCi) {
		writeChar(Pos.CW46R_AVP_MDL_NM_CI, cw46rAvpMdlNmCi);
	}

	/**Original name: CW46R-AVP-MDL-NM-CI<br>*/
	public char getCw46rAvpMdlNmCi() {
		return readChar(Pos.CW46R_AVP_MDL_NM_CI);
	}

	public void setCw46rAvpMdlNm(String cw46rAvpMdlNm) {
		writeString(Pos.CW46R_AVP_MDL_NM, cw46rAvpMdlNm, Len.CW46R_AVP_MDL_NM);
	}

	/**Original name: CW46R-AVP-MDL-NM<br>*/
	public String getCw46rAvpMdlNm() {
		return readString(Pos.CW46R_AVP_MDL_NM, Len.CW46R_AVP_MDL_NM);
	}

	public void setCw46rAvpLstNmCi(char cw46rAvpLstNmCi) {
		writeChar(Pos.CW46R_AVP_LST_NM_CI, cw46rAvpLstNmCi);
	}

	/**Original name: CW46R-AVP-LST-NM-CI<br>*/
	public char getCw46rAvpLstNmCi() {
		return readChar(Pos.CW46R_AVP_LST_NM_CI);
	}

	public void setCw46rAvpLstNm(String cw46rAvpLstNm) {
		writeString(Pos.CW46R_AVP_LST_NM, cw46rAvpLstNm, Len.CW46R_AVP_LST_NM);
	}

	/**Original name: CW46R-AVP-LST-NM<br>*/
	public String getCw46rAvpLstNm() {
		return readString(Pos.CW46R_AVP_LST_NM, Len.CW46R_AVP_LST_NM);
	}

	public void setCw46rAvpNmCi(char cw46rAvpNmCi) {
		writeChar(Pos.CW46R_AVP_NM_CI, cw46rAvpNmCi);
	}

	/**Original name: CW46R-AVP-NM-CI<br>*/
	public char getCw46rAvpNmCi() {
		return readChar(Pos.CW46R_AVP_NM_CI);
	}

	public void setCw46rAvpNm(String cw46rAvpNm) {
		writeString(Pos.CW46R_AVP_NM, cw46rAvpNm, Len.CW46R_AVP_NM);
	}

	/**Original name: CW46R-AVP-NM<br>*/
	public String getCw46rAvpNm() {
		return readString(Pos.CW46R_AVP_NM, Len.CW46R_AVP_NM);
	}

	public void setCw46rAvpNmSfxCi(char cw46rAvpNmSfxCi) {
		writeChar(Pos.CW46R_AVP_NM_SFX_CI, cw46rAvpNmSfxCi);
	}

	/**Original name: CW46R-AVP-NM-SFX-CI<br>*/
	public char getCw46rAvpNmSfxCi() {
		return readChar(Pos.CW46R_AVP_NM_SFX_CI);
	}

	public void setCw46rAvpNmSfx(String cw46rAvpNmSfx) {
		writeString(Pos.CW46R_AVP_NM_SFX, cw46rAvpNmSfx, Len.CW46R_AVP_NM_SFX);
	}

	/**Original name: CW46R-AVP-NM-SFX<br>*/
	public String getCw46rAvpNmSfx() {
		return readString(Pos.CW46R_AVP_NM_SFX, Len.CW46R_AVP_NM_SFX);
	}

	public void setCw46rAvpCltIdCi(char cw46rAvpCltIdCi) {
		writeChar(Pos.CW46R_AVP_CLT_ID_CI, cw46rAvpCltIdCi);
	}

	/**Original name: CW46R-AVP-CLT-ID-CI<br>*/
	public char getCw46rAvpCltIdCi() {
		return readChar(Pos.CW46R_AVP_CLT_ID_CI);
	}

	public void setCw46rAvpCltId(String cw46rAvpCltId) {
		writeString(Pos.CW46R_AVP_CLT_ID, cw46rAvpCltId, Len.CW46R_AVP_CLT_ID);
	}

	/**Original name: CW46R-AVP-CLT-ID<br>*/
	public String getCw46rAvpCltId() {
		return readString(Pos.CW46R_AVP_CLT_ID, Len.CW46R_AVP_CLT_ID);
	}

	public String getlFwRespCltUwBpoFormatted() {
		return readFixedString(Pos.L_FW_RESP_CLT_UW_BPO, Len.L_FW_RESP_CLT_UW_BPO);
	}

	public void setMua02CltCltRelationCsumFormatted(String mua02CltCltRelationCsum) {
		writeString(Pos.MUA02_CLT_CLT_RELATION_CSUM, Trunc.toUnsignedNumeric(mua02CltCltRelationCsum, Len.MUA02_CLT_CLT_RELATION_CSUM),
				Len.MUA02_CLT_CLT_RELATION_CSUM);
	}

	/**Original name: MUA02-CLT-CLT-RELATION-CSUM<br>*/
	public int getMua02CltCltRelationCsum() {
		return readNumDispUnsignedInt(Pos.MUA02_CLT_CLT_RELATION_CSUM, Len.MUA02_CLT_CLT_RELATION_CSUM);
	}

	public void setMua02ClientIdKcre(String mua02ClientIdKcre) {
		writeString(Pos.MUA02_CLIENT_ID_KCRE, mua02ClientIdKcre, Len.MUA02_CLIENT_ID_KCRE);
	}

	/**Original name: MUA02-CLIENT-ID-KCRE<br>*/
	public String getMua02ClientIdKcre() {
		return readString(Pos.MUA02_CLIENT_ID_KCRE, Len.MUA02_CLIENT_ID_KCRE);
	}

	public void setMua02CltTypCdKcre(String mua02CltTypCdKcre) {
		writeString(Pos.MUA02_CLT_TYP_CD_KCRE, mua02CltTypCdKcre, Len.MUA02_CLT_TYP_CD_KCRE);
	}

	/**Original name: MUA02-CLT-TYP-CD-KCRE<br>*/
	public String getMua02CltTypCdKcre() {
		return readString(Pos.MUA02_CLT_TYP_CD_KCRE, Len.MUA02_CLT_TYP_CD_KCRE);
	}

	public void setMua02HistoryVldNbrKcre(String mua02HistoryVldNbrKcre) {
		writeString(Pos.MUA02_HISTORY_VLD_NBR_KCRE, mua02HistoryVldNbrKcre, Len.MUA02_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: MUA02-HISTORY-VLD-NBR-KCRE<br>*/
	public String getMua02HistoryVldNbrKcre() {
		return readString(Pos.MUA02_HISTORY_VLD_NBR_KCRE, Len.MUA02_HISTORY_VLD_NBR_KCRE);
	}

	public void setMua02CicrXrfIdKcre(String mua02CicrXrfIdKcre) {
		writeString(Pos.MUA02_CICR_XRF_ID_KCRE, mua02CicrXrfIdKcre, Len.MUA02_CICR_XRF_ID_KCRE);
	}

	/**Original name: MUA02-CICR-XRF-ID-KCRE<br>*/
	public String getMua02CicrXrfIdKcre() {
		return readString(Pos.MUA02_CICR_XRF_ID_KCRE, Len.MUA02_CICR_XRF_ID_KCRE);
	}

	public void setMua02XrfTypCdKcre(String mua02XrfTypCdKcre) {
		writeString(Pos.MUA02_XRF_TYP_CD_KCRE, mua02XrfTypCdKcre, Len.MUA02_XRF_TYP_CD_KCRE);
	}

	/**Original name: MUA02-XRF-TYP-CD-KCRE<br>*/
	public String getMua02XrfTypCdKcre() {
		return readString(Pos.MUA02_XRF_TYP_CD_KCRE, Len.MUA02_XRF_TYP_CD_KCRE);
	}

	public void setMua02CicrEffDtKcre(String mua02CicrEffDtKcre) {
		writeString(Pos.MUA02_CICR_EFF_DT_KCRE, mua02CicrEffDtKcre, Len.MUA02_CICR_EFF_DT_KCRE);
	}

	/**Original name: MUA02-CICR-EFF-DT-KCRE<br>*/
	public String getMua02CicrEffDtKcre() {
		return readString(Pos.MUA02_CICR_EFF_DT_KCRE, Len.MUA02_CICR_EFF_DT_KCRE);
	}

	public void setMua02TransProcessDt(String mua02TransProcessDt) {
		writeString(Pos.MUA02_TRANS_PROCESS_DT, mua02TransProcessDt, Len.MUA02_TRANS_PROCESS_DT);
	}

	/**Original name: MUA02-TRANS-PROCESS-DT<br>*/
	public String getMua02TransProcessDt() {
		return readString(Pos.MUA02_TRANS_PROCESS_DT, Len.MUA02_TRANS_PROCESS_DT);
	}

	public void setMua02ClientIdCi(char mua02ClientIdCi) {
		writeChar(Pos.MUA02_CLIENT_ID_CI, mua02ClientIdCi);
	}

	/**Original name: MUA02-CLIENT-ID-CI<br>*/
	public char getMua02ClientIdCi() {
		return readChar(Pos.MUA02_CLIENT_ID_CI);
	}

	public void setMua02ClientId(String mua02ClientId) {
		writeString(Pos.MUA02_CLIENT_ID, mua02ClientId, Len.MUA02_CLIENT_ID);
	}

	/**Original name: MUA02-CLIENT-ID<br>*/
	public String getMua02ClientId() {
		return readString(Pos.MUA02_CLIENT_ID, Len.MUA02_CLIENT_ID);
	}

	public void setMua02CltTypCdCi(char mua02CltTypCdCi) {
		writeChar(Pos.MUA02_CLT_TYP_CD_CI, mua02CltTypCdCi);
	}

	/**Original name: MUA02-CLT-TYP-CD-CI<br>*/
	public char getMua02CltTypCdCi() {
		return readChar(Pos.MUA02_CLT_TYP_CD_CI);
	}

	public void setMua02CltTypCd(String mua02CltTypCd) {
		writeString(Pos.MUA02_CLT_TYP_CD, mua02CltTypCd, Len.MUA02_CLT_TYP_CD);
	}

	/**Original name: MUA02-CLT-TYP-CD<br>*/
	public String getMua02CltTypCd() {
		return readString(Pos.MUA02_CLT_TYP_CD, Len.MUA02_CLT_TYP_CD);
	}

	public void setMua02HistoryVldNbrCi(char mua02HistoryVldNbrCi) {
		writeChar(Pos.MUA02_HISTORY_VLD_NBR_CI, mua02HistoryVldNbrCi);
	}

	/**Original name: MUA02-HISTORY-VLD-NBR-CI<br>*/
	public char getMua02HistoryVldNbrCi() {
		return readChar(Pos.MUA02_HISTORY_VLD_NBR_CI);
	}

	public void setMua02HistoryVldNbrSign(char mua02HistoryVldNbrSign) {
		writeChar(Pos.MUA02_HISTORY_VLD_NBR_SIGN, mua02HistoryVldNbrSign);
	}

	/**Original name: MUA02-HISTORY-VLD-NBR-SIGN<br>*/
	public char getMua02HistoryVldNbrSign() {
		return readChar(Pos.MUA02_HISTORY_VLD_NBR_SIGN);
	}

	public void setMua02HistoryVldNbrFormatted(String mua02HistoryVldNbr) {
		writeString(Pos.MUA02_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(mua02HistoryVldNbr, Len.MUA02_HISTORY_VLD_NBR), Len.MUA02_HISTORY_VLD_NBR);
	}

	/**Original name: MUA02-HISTORY-VLD-NBR<br>*/
	public int getMua02HistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.MUA02_HISTORY_VLD_NBR, Len.MUA02_HISTORY_VLD_NBR);
	}

	public void setMua02CicrXrfIdCi(char mua02CicrXrfIdCi) {
		writeChar(Pos.MUA02_CICR_XRF_ID_CI, mua02CicrXrfIdCi);
	}

	/**Original name: MUA02-CICR-XRF-ID-CI<br>*/
	public char getMua02CicrXrfIdCi() {
		return readChar(Pos.MUA02_CICR_XRF_ID_CI);
	}

	public void setMua02CicrXrfId(String mua02CicrXrfId) {
		writeString(Pos.MUA02_CICR_XRF_ID, mua02CicrXrfId, Len.MUA02_CICR_XRF_ID);
	}

	/**Original name: MUA02-CICR-XRF-ID<br>*/
	public String getMua02CicrXrfId() {
		return readString(Pos.MUA02_CICR_XRF_ID, Len.MUA02_CICR_XRF_ID);
	}

	public void setMua02XrfTypCdCi(char mua02XrfTypCdCi) {
		writeChar(Pos.MUA02_XRF_TYP_CD_CI, mua02XrfTypCdCi);
	}

	/**Original name: MUA02-XRF-TYP-CD-CI<br>*/
	public char getMua02XrfTypCdCi() {
		return readChar(Pos.MUA02_XRF_TYP_CD_CI);
	}

	public void setMua02XrfTypCd(String mua02XrfTypCd) {
		writeString(Pos.MUA02_XRF_TYP_CD, mua02XrfTypCd, Len.MUA02_XRF_TYP_CD);
	}

	/**Original name: MUA02-XRF-TYP-CD<br>*/
	public String getMua02XrfTypCd() {
		return readString(Pos.MUA02_XRF_TYP_CD, Len.MUA02_XRF_TYP_CD);
	}

	public void setMua02CicrEffDtCi(char mua02CicrEffDtCi) {
		writeChar(Pos.MUA02_CICR_EFF_DT_CI, mua02CicrEffDtCi);
	}

	/**Original name: MUA02-CICR-EFF-DT-CI<br>*/
	public char getMua02CicrEffDtCi() {
		return readChar(Pos.MUA02_CICR_EFF_DT_CI);
	}

	public void setMua02CicrEffDt(String mua02CicrEffDt) {
		writeString(Pos.MUA02_CICR_EFF_DT, mua02CicrEffDt, Len.MUA02_CICR_EFF_DT);
	}

	/**Original name: MUA02-CICR-EFF-DT<br>*/
	public String getMua02CicrEffDt() {
		return readString(Pos.MUA02_CICR_EFF_DT, Len.MUA02_CICR_EFF_DT);
	}

	public void setMua02CicrExpDtCi(char mua02CicrExpDtCi) {
		writeChar(Pos.MUA02_CICR_EXP_DT_CI, mua02CicrExpDtCi);
	}

	/**Original name: MUA02-CICR-EXP-DT-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getMua02CicrExpDtCi() {
		return readChar(Pos.MUA02_CICR_EXP_DT_CI);
	}

	public void setMua02CicrExpDt(String mua02CicrExpDt) {
		writeString(Pos.MUA02_CICR_EXP_DT, mua02CicrExpDt, Len.MUA02_CICR_EXP_DT);
	}

	/**Original name: MUA02-CICR-EXP-DT<br>*/
	public String getMua02CicrExpDt() {
		return readString(Pos.MUA02_CICR_EXP_DT, Len.MUA02_CICR_EXP_DT);
	}

	public void setMua02CicrNbrOprStrsCi(char mua02CicrNbrOprStrsCi) {
		writeChar(Pos.MUA02_CICR_NBR_OPR_STRS_CI, mua02CicrNbrOprStrsCi);
	}

	/**Original name: MUA02-CICR-NBR-OPR-STRS-CI<br>*/
	public char getMua02CicrNbrOprStrsCi() {
		return readChar(Pos.MUA02_CICR_NBR_OPR_STRS_CI);
	}

	public void setMua02CicrNbrOprStrsNi(char mua02CicrNbrOprStrsNi) {
		writeChar(Pos.MUA02_CICR_NBR_OPR_STRS_NI, mua02CicrNbrOprStrsNi);
	}

	/**Original name: MUA02-CICR-NBR-OPR-STRS-NI<br>*/
	public char getMua02CicrNbrOprStrsNi() {
		return readChar(Pos.MUA02_CICR_NBR_OPR_STRS_NI);
	}

	public void setMua02CicrNbrOprStrs(String mua02CicrNbrOprStrs) {
		writeString(Pos.MUA02_CICR_NBR_OPR_STRS, mua02CicrNbrOprStrs, Len.MUA02_CICR_NBR_OPR_STRS);
	}

	/**Original name: MUA02-CICR-NBR-OPR-STRS<br>*/
	public String getMua02CicrNbrOprStrs() {
		return readString(Pos.MUA02_CICR_NBR_OPR_STRS, Len.MUA02_CICR_NBR_OPR_STRS);
	}

	public void setMua02UserIdCi(char mua02UserIdCi) {
		writeChar(Pos.MUA02_USER_ID_CI, mua02UserIdCi);
	}

	/**Original name: MUA02-USER-ID-CI<br>*/
	public char getMua02UserIdCi() {
		return readChar(Pos.MUA02_USER_ID_CI);
	}

	public void setMua02UserId(String mua02UserId) {
		writeString(Pos.MUA02_USER_ID, mua02UserId, Len.MUA02_USER_ID);
	}

	/**Original name: MUA02-USER-ID<br>*/
	public String getMua02UserId() {
		return readString(Pos.MUA02_USER_ID, Len.MUA02_USER_ID);
	}

	public void setMua02StatusCdCi(char mua02StatusCdCi) {
		writeChar(Pos.MUA02_STATUS_CD_CI, mua02StatusCdCi);
	}

	/**Original name: MUA02-STATUS-CD-CI<br>*/
	public char getMua02StatusCdCi() {
		return readChar(Pos.MUA02_STATUS_CD_CI);
	}

	public void setMua02StatusCd(char mua02StatusCd) {
		writeChar(Pos.MUA02_STATUS_CD, mua02StatusCd);
	}

	/**Original name: MUA02-STATUS-CD<br>*/
	public char getMua02StatusCd() {
		return readChar(Pos.MUA02_STATUS_CD);
	}

	public void setMua02TerminalIdCi(char mua02TerminalIdCi) {
		writeChar(Pos.MUA02_TERMINAL_ID_CI, mua02TerminalIdCi);
	}

	/**Original name: MUA02-TERMINAL-ID-CI<br>*/
	public char getMua02TerminalIdCi() {
		return readChar(Pos.MUA02_TERMINAL_ID_CI);
	}

	public void setMua02TerminalId(String mua02TerminalId) {
		writeString(Pos.MUA02_TERMINAL_ID, mua02TerminalId, Len.MUA02_TERMINAL_ID);
	}

	/**Original name: MUA02-TERMINAL-ID<br>*/
	public String getMua02TerminalId() {
		return readString(Pos.MUA02_TERMINAL_ID, Len.MUA02_TERMINAL_ID);
	}

	public void setMua02CicrEffAcyTsCi(char mua02CicrEffAcyTsCi) {
		writeChar(Pos.MUA02_CICR_EFF_ACY_TS_CI, mua02CicrEffAcyTsCi);
	}

	/**Original name: MUA02-CICR-EFF-ACY-TS-CI<br>*/
	public char getMua02CicrEffAcyTsCi() {
		return readChar(Pos.MUA02_CICR_EFF_ACY_TS_CI);
	}

	public void setMua02CicrEffAcyTs(String mua02CicrEffAcyTs) {
		writeString(Pos.MUA02_CICR_EFF_ACY_TS, mua02CicrEffAcyTs, Len.MUA02_CICR_EFF_ACY_TS);
	}

	/**Original name: MUA02-CICR-EFF-ACY-TS<br>*/
	public String getMua02CicrEffAcyTs() {
		return readString(Pos.MUA02_CICR_EFF_ACY_TS, Len.MUA02_CICR_EFF_ACY_TS);
	}

	public void setMua02CicrExpAcyTsCi(char mua02CicrExpAcyTsCi) {
		writeChar(Pos.MUA02_CICR_EXP_ACY_TS_CI, mua02CicrExpAcyTsCi);
	}

	/**Original name: MUA02-CICR-EXP-ACY-TS-CI<br>*/
	public char getMua02CicrExpAcyTsCi() {
		return readChar(Pos.MUA02_CICR_EXP_ACY_TS_CI);
	}

	public void setMua02CicrExpAcyTs(String mua02CicrExpAcyTs) {
		writeString(Pos.MUA02_CICR_EXP_ACY_TS, mua02CicrExpAcyTs, Len.MUA02_CICR_EXP_ACY_TS);
	}

	/**Original name: MUA02-CICR-EXP-ACY-TS<br>*/
	public String getMua02CicrExpAcyTs() {
		return readString(Pos.MUA02_CICR_EXP_ACY_TS, Len.MUA02_CICR_EXP_ACY_TS);
	}

	public void setMua02CicrLegEntCdCi(char mua02CicrLegEntCdCi) {
		writeChar(Pos.MUA02_CICR_LEG_ENT_CD_CI, mua02CicrLegEntCdCi);
	}

	/**Original name: MUA02-CICR-LEG-ENT-CD-CI<br>*/
	public char getMua02CicrLegEntCdCi() {
		return readChar(Pos.MUA02_CICR_LEG_ENT_CD_CI);
	}

	public void setMua02CicrLegEntCd(String mua02CicrLegEntCd) {
		writeString(Pos.MUA02_CICR_LEG_ENT_CD, mua02CicrLegEntCd, Len.MUA02_CICR_LEG_ENT_CD);
	}

	/**Original name: MUA02-CICR-LEG-ENT-CD<br>*/
	public String getMua02CicrLegEntCd() {
		return readString(Pos.MUA02_CICR_LEG_ENT_CD, Len.MUA02_CICR_LEG_ENT_CD);
	}

	public void setMua02CicrFstNmCi(char mua02CicrFstNmCi) {
		writeChar(Pos.MUA02_CICR_FST_NM_CI, mua02CicrFstNmCi);
	}

	/**Original name: MUA02-CICR-FST-NM-CI<br>*/
	public char getMua02CicrFstNmCi() {
		return readChar(Pos.MUA02_CICR_FST_NM_CI);
	}

	public void setMua02CicrFstNm(String mua02CicrFstNm) {
		writeString(Pos.MUA02_CICR_FST_NM, mua02CicrFstNm, Len.MUA02_CICR_FST_NM);
	}

	/**Original name: MUA02-CICR-FST-NM<br>*/
	public String getMua02CicrFstNm() {
		return readString(Pos.MUA02_CICR_FST_NM, Len.MUA02_CICR_FST_NM);
	}

	public void setMua02CicrLstNmCi(char mua02CicrLstNmCi) {
		writeChar(Pos.MUA02_CICR_LST_NM_CI, mua02CicrLstNmCi);
	}

	/**Original name: MUA02-CICR-LST-NM-CI<br>*/
	public char getMua02CicrLstNmCi() {
		return readChar(Pos.MUA02_CICR_LST_NM_CI);
	}

	public void setMua02CicrLstNm(String mua02CicrLstNm) {
		writeString(Pos.MUA02_CICR_LST_NM, mua02CicrLstNm, Len.MUA02_CICR_LST_NM);
	}

	/**Original name: MUA02-CICR-LST-NM<br>*/
	public String getMua02CicrLstNm() {
		return readString(Pos.MUA02_CICR_LST_NM, Len.MUA02_CICR_LST_NM);
	}

	public void setMua02CicrMdlNmCi(char mua02CicrMdlNmCi) {
		writeChar(Pos.MUA02_CICR_MDL_NM_CI, mua02CicrMdlNmCi);
	}

	/**Original name: MUA02-CICR-MDL-NM-CI<br>*/
	public char getMua02CicrMdlNmCi() {
		return readChar(Pos.MUA02_CICR_MDL_NM_CI);
	}

	public void setMua02CicrMdlNm(String mua02CicrMdlNm) {
		writeString(Pos.MUA02_CICR_MDL_NM, mua02CicrMdlNm, Len.MUA02_CICR_MDL_NM);
	}

	/**Original name: MUA02-CICR-MDL-NM<br>*/
	public String getMua02CicrMdlNm() {
		return readString(Pos.MUA02_CICR_MDL_NM, Len.MUA02_CICR_MDL_NM);
	}

	public void setMua02NmPfxCi(char mua02NmPfxCi) {
		writeChar(Pos.MUA02_NM_PFX_CI, mua02NmPfxCi);
	}

	/**Original name: MUA02-NM-PFX-CI<br>*/
	public char getMua02NmPfxCi() {
		return readChar(Pos.MUA02_NM_PFX_CI);
	}

	public void setMua02NmPfx(String mua02NmPfx) {
		writeString(Pos.MUA02_NM_PFX, mua02NmPfx, Len.MUA02_NM_PFX);
	}

	/**Original name: MUA02-NM-PFX<br>*/
	public String getMua02NmPfx() {
		return readString(Pos.MUA02_NM_PFX, Len.MUA02_NM_PFX);
	}

	public void setMua02NmSfxCi(char mua02NmSfxCi) {
		writeChar(Pos.MUA02_NM_SFX_CI, mua02NmSfxCi);
	}

	/**Original name: MUA02-NM-SFX-CI<br>*/
	public char getMua02NmSfxCi() {
		return readChar(Pos.MUA02_NM_SFX_CI);
	}

	public void setMua02NmSfx(String mua02NmSfx) {
		writeString(Pos.MUA02_NM_SFX, mua02NmSfx, Len.MUA02_NM_SFX);
	}

	/**Original name: MUA02-NM-SFX<br>*/
	public String getMua02NmSfx() {
		return readString(Pos.MUA02_NM_SFX, Len.MUA02_NM_SFX);
	}

	public void setMua02CicrLngNmCi(char mua02CicrLngNmCi) {
		writeChar(Pos.MUA02_CICR_LNG_NM_CI, mua02CicrLngNmCi);
	}

	/**Original name: MUA02-CICR-LNG-NM-CI<br>*/
	public char getMua02CicrLngNmCi() {
		return readChar(Pos.MUA02_CICR_LNG_NM_CI);
	}

	public void setMua02CicrLngNmNi(char mua02CicrLngNmNi) {
		writeChar(Pos.MUA02_CICR_LNG_NM_NI, mua02CicrLngNmNi);
	}

	/**Original name: MUA02-CICR-LNG-NM-NI<br>*/
	public char getMua02CicrLngNmNi() {
		return readChar(Pos.MUA02_CICR_LNG_NM_NI);
	}

	public void setMua02CicrLngNm(String mua02CicrLngNm) {
		writeString(Pos.MUA02_CICR_LNG_NM, mua02CicrLngNm, Len.MUA02_CICR_LNG_NM);
	}

	/**Original name: MUA02-CICR-LNG-NM<br>*/
	public String getMua02CicrLngNm() {
		return readString(Pos.MUA02_CICR_LNG_NM, Len.MUA02_CICR_LNG_NM);
	}

	public void setMua02CicrRelChgSw(char mua02CicrRelChgSw) {
		writeChar(Pos.MUA02_CICR_REL_CHG_SW, mua02CicrRelChgSw);
	}

	/**Original name: MUA02-CICR-REL-CHG-SW<br>*/
	public char getMua02CicrRelChgSw() {
		return readChar(Pos.MUA02_CICR_REL_CHG_SW);
	}

	public void setMua02ClientDes(String mua02ClientDes) {
		writeString(Pos.MUA02_CLIENT_DES, mua02ClientDes, Len.MUA02_CLIENT_DES);
	}

	/**Original name: MUA02-CLIENT-DES<br>*/
	public String getMua02ClientDes() {
		return readString(Pos.MUA02_CLIENT_DES, Len.MUA02_CLIENT_DES);
	}

	public void setCw48rFedBusinessTypRowFormatted(int cw48rFedBusinessTypRowIdx, String data) {
		int position = Pos.cw48rFedBusinessTypRow(cw48rFedBusinessTypRowIdx - 1);
		writeString(position, data, Len.CW48R_FED_BUSINESS_TYP_ROW);
	}

	public void setCw48rFedBusinessTypCsumFormatted(int cw48rFedBusinessTypCsumIdx, String cw48rFedBusinessTypCsum) {
		int position = Pos.cw48rFedBusinessTypCsum(cw48rFedBusinessTypCsumIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw48rFedBusinessTypCsum, Len.CW48R_FED_BUSINESS_TYP_CSUM), Len.CW48R_FED_BUSINESS_TYP_CSUM);
	}

	/**Original name: CW48R-FED-BUSINESS-TYP-CSUM<br>*/
	public int getCw48rFedBusinessTypCsum(int cw48rFedBusinessTypCsumIdx) {
		int position = Pos.cw48rFedBusinessTypCsum(cw48rFedBusinessTypCsumIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW48R_FED_BUSINESS_TYP_CSUM);
	}

	public void setCw48rClientIdKcre(int cw48rClientIdKcreIdx, String cw48rClientIdKcre) {
		int position = Pos.cw48rClientIdKcre(cw48rClientIdKcreIdx - 1);
		writeString(position, cw48rClientIdKcre, Len.CW48R_CLIENT_ID_KCRE);
	}

	/**Original name: CW48R-CLIENT-ID-KCRE<br>*/
	public String getCw48rClientIdKcre(int cw48rClientIdKcreIdx) {
		int position = Pos.cw48rClientIdKcre(cw48rClientIdKcreIdx - 1);
		return readString(position, Len.CW48R_CLIENT_ID_KCRE);
	}

	public void setCw48rTobCdKcre(int cw48rTobCdKcreIdx, String cw48rTobCdKcre) {
		int position = Pos.cw48rTobCdKcre(cw48rTobCdKcreIdx - 1);
		writeString(position, cw48rTobCdKcre, Len.CW48R_TOB_CD_KCRE);
	}

	/**Original name: CW48R-TOB-CD-KCRE<br>*/
	public String getCw48rTobCdKcre(int cw48rTobCdKcreIdx) {
		int position = Pos.cw48rTobCdKcre(cw48rTobCdKcreIdx - 1);
		return readString(position, Len.CW48R_TOB_CD_KCRE);
	}

	public void setCw48rHistoryVldNbrKcre(int cw48rHistoryVldNbrKcreIdx, String cw48rHistoryVldNbrKcre) {
		int position = Pos.cw48rHistoryVldNbrKcre(cw48rHistoryVldNbrKcreIdx - 1);
		writeString(position, cw48rHistoryVldNbrKcre, Len.CW48R_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CW48R-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCw48rHistoryVldNbrKcre(int cw48rHistoryVldNbrKcreIdx) {
		int position = Pos.cw48rHistoryVldNbrKcre(cw48rHistoryVldNbrKcreIdx - 1);
		return readString(position, Len.CW48R_HISTORY_VLD_NBR_KCRE);
	}

	public void setCw48rEffectiveDtKcre(int cw48rEffectiveDtKcreIdx, String cw48rEffectiveDtKcre) {
		int position = Pos.cw48rEffectiveDtKcre(cw48rEffectiveDtKcreIdx - 1);
		writeString(position, cw48rEffectiveDtKcre, Len.CW48R_EFFECTIVE_DT_KCRE);
	}

	/**Original name: CW48R-EFFECTIVE-DT-KCRE<br>*/
	public String getCw48rEffectiveDtKcre(int cw48rEffectiveDtKcreIdx) {
		int position = Pos.cw48rEffectiveDtKcre(cw48rEffectiveDtKcreIdx - 1);
		return readString(position, Len.CW48R_EFFECTIVE_DT_KCRE);
	}

	public void setCw48rTransProcessDt(int cw48rTransProcessDtIdx, String cw48rTransProcessDt) {
		int position = Pos.cw48rTransProcessDt(cw48rTransProcessDtIdx - 1);
		writeString(position, cw48rTransProcessDt, Len.CW48R_TRANS_PROCESS_DT);
	}

	/**Original name: CW48R-TRANS-PROCESS-DT<br>*/
	public String getCw48rTransProcessDt(int cw48rTransProcessDtIdx) {
		int position = Pos.cw48rTransProcessDt(cw48rTransProcessDtIdx - 1);
		return readString(position, Len.CW48R_TRANS_PROCESS_DT);
	}

	public void setCw48rClientId(int cw48rClientIdIdx, String cw48rClientId) {
		int position = Pos.cw48rClientId(cw48rClientIdIdx - 1);
		writeString(position, cw48rClientId, Len.CW48R_CLIENT_ID);
	}

	/**Original name: CW48R-CLIENT-ID<br>*/
	public String getCw48rClientId(int cw48rClientIdIdx) {
		int position = Pos.cw48rClientId(cw48rClientIdIdx - 1);
		return readString(position, Len.CW48R_CLIENT_ID);
	}

	public void setCw48rTobCd(int cw48rTobCdIdx, String cw48rTobCd) {
		int position = Pos.cw48rTobCd(cw48rTobCdIdx - 1);
		writeString(position, cw48rTobCd, Len.CW48R_TOB_CD);
	}

	/**Original name: CW48R-TOB-CD<br>*/
	public String getCw48rTobCd(int cw48rTobCdIdx) {
		int position = Pos.cw48rTobCd(cw48rTobCdIdx - 1);
		return readString(position, Len.CW48R_TOB_CD);
	}

	public void setCw48rHistoryVldNbrSign(int cw48rHistoryVldNbrSignIdx, char cw48rHistoryVldNbrSign) {
		int position = Pos.cw48rHistoryVldNbrSign(cw48rHistoryVldNbrSignIdx - 1);
		writeChar(position, cw48rHistoryVldNbrSign);
	}

	/**Original name: CW48R-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw48rHistoryVldNbrSign(int cw48rHistoryVldNbrSignIdx) {
		int position = Pos.cw48rHistoryVldNbrSign(cw48rHistoryVldNbrSignIdx - 1);
		return readChar(position);
	}

	public void setCw48rHistoryVldNbrFormatted(int cw48rHistoryVldNbrIdx, String cw48rHistoryVldNbr) {
		int position = Pos.cw48rHistoryVldNbr(cw48rHistoryVldNbrIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw48rHistoryVldNbr, Len.CW48R_HISTORY_VLD_NBR), Len.CW48R_HISTORY_VLD_NBR);
	}

	/**Original name: CW48R-HISTORY-VLD-NBR<br>*/
	public int getCw48rHistoryVldNbr(int cw48rHistoryVldNbrIdx) {
		int position = Pos.cw48rHistoryVldNbr(cw48rHistoryVldNbrIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW48R_HISTORY_VLD_NBR);
	}

	public void setCw48rEffectiveDt(int cw48rEffectiveDtIdx, String cw48rEffectiveDt) {
		int position = Pos.cw48rEffectiveDt(cw48rEffectiveDtIdx - 1);
		writeString(position, cw48rEffectiveDt, Len.CW48R_EFFECTIVE_DT);
	}

	/**Original name: CW48R-EFFECTIVE-DT<br>*/
	public String getCw48rEffectiveDt(int cw48rEffectiveDtIdx) {
		int position = Pos.cw48rEffectiveDt(cw48rEffectiveDtIdx - 1);
		return readString(position, Len.CW48R_EFFECTIVE_DT);
	}

	public void setCw48rClientIdCi(int cw48rClientIdCiIdx, char cw48rClientIdCi) {
		int position = Pos.cw48rClientIdCi(cw48rClientIdCiIdx - 1);
		writeChar(position, cw48rClientIdCi);
	}

	/**Original name: CW48R-CLIENT-ID-CI<br>*/
	public char getCw48rClientIdCi(int cw48rClientIdCiIdx) {
		int position = Pos.cw48rClientIdCi(cw48rClientIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rTobCdCi(int cw48rTobCdCiIdx, char cw48rTobCdCi) {
		int position = Pos.cw48rTobCdCi(cw48rTobCdCiIdx - 1);
		writeChar(position, cw48rTobCdCi);
	}

	/**Original name: CW48R-TOB-CD-CI<br>*/
	public char getCw48rTobCdCi(int cw48rTobCdCiIdx) {
		int position = Pos.cw48rTobCdCi(cw48rTobCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rHistoryVldNbrCi(int cw48rHistoryVldNbrCiIdx, char cw48rHistoryVldNbrCi) {
		int position = Pos.cw48rHistoryVldNbrCi(cw48rHistoryVldNbrCiIdx - 1);
		writeChar(position, cw48rHistoryVldNbrCi);
	}

	/**Original name: CW48R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw48rHistoryVldNbrCi(int cw48rHistoryVldNbrCiIdx) {
		int position = Pos.cw48rHistoryVldNbrCi(cw48rHistoryVldNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rEffectiveDtCi(int cw48rEffectiveDtCiIdx, char cw48rEffectiveDtCi) {
		int position = Pos.cw48rEffectiveDtCi(cw48rEffectiveDtCiIdx - 1);
		writeChar(position, cw48rEffectiveDtCi);
	}

	/**Original name: CW48R-EFFECTIVE-DT-CI<br>*/
	public char getCw48rEffectiveDtCi(int cw48rEffectiveDtCiIdx) {
		int position = Pos.cw48rEffectiveDtCi(cw48rEffectiveDtCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rTobSicTypCdCi(int cw48rTobSicTypCdCiIdx, char cw48rTobSicTypCdCi) {
		int position = Pos.cw48rTobSicTypCdCi(cw48rTobSicTypCdCiIdx - 1);
		writeChar(position, cw48rTobSicTypCdCi);
	}

	/**Original name: CW48R-TOB-SIC-TYP-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw48rTobSicTypCdCi(int cw48rTobSicTypCdCiIdx) {
		int position = Pos.cw48rTobSicTypCdCi(cw48rTobSicTypCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rTobSicTypCd(int cw48rTobSicTypCdIdx, String cw48rTobSicTypCd) {
		int position = Pos.cw48rTobSicTypCd(cw48rTobSicTypCdIdx - 1);
		writeString(position, cw48rTobSicTypCd, Len.CW48R_TOB_SIC_TYP_CD);
	}

	/**Original name: CW48R-TOB-SIC-TYP-CD<br>*/
	public String getCw48rTobSicTypCd(int cw48rTobSicTypCdIdx) {
		int position = Pos.cw48rTobSicTypCd(cw48rTobSicTypCdIdx - 1);
		return readString(position, Len.CW48R_TOB_SIC_TYP_CD);
	}

	public void setCw48rTobPtyCdCi(int cw48rTobPtyCdCiIdx, char cw48rTobPtyCdCi) {
		int position = Pos.cw48rTobPtyCdCi(cw48rTobPtyCdCiIdx - 1);
		writeChar(position, cw48rTobPtyCdCi);
	}

	/**Original name: CW48R-TOB-PTY-CD-CI<br>*/
	public char getCw48rTobPtyCdCi(int cw48rTobPtyCdCiIdx) {
		int position = Pos.cw48rTobPtyCdCi(cw48rTobPtyCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rTobPtyCd(int cw48rTobPtyCdIdx, String cw48rTobPtyCd) {
		int position = Pos.cw48rTobPtyCd(cw48rTobPtyCdIdx - 1);
		writeString(position, cw48rTobPtyCd, Len.CW48R_TOB_PTY_CD);
	}

	/**Original name: CW48R-TOB-PTY-CD<br>*/
	public String getCw48rTobPtyCd(int cw48rTobPtyCdIdx) {
		int position = Pos.cw48rTobPtyCd(cw48rTobPtyCdIdx - 1);
		return readString(position, Len.CW48R_TOB_PTY_CD);
	}

	public void setCw48rUserIdCi(int cw48rUserIdCiIdx, char cw48rUserIdCi) {
		int position = Pos.cw48rUserIdCi(cw48rUserIdCiIdx - 1);
		writeChar(position, cw48rUserIdCi);
	}

	/**Original name: CW48R-USER-ID-CI<br>*/
	public char getCw48rUserIdCi(int cw48rUserIdCiIdx) {
		int position = Pos.cw48rUserIdCi(cw48rUserIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rUserId(int cw48rUserIdIdx, String cw48rUserId) {
		int position = Pos.cw48rUserId(cw48rUserIdIdx - 1);
		writeString(position, cw48rUserId, Len.CW48R_USER_ID);
	}

	/**Original name: CW48R-USER-ID<br>*/
	public String getCw48rUserId(int cw48rUserIdIdx) {
		int position = Pos.cw48rUserId(cw48rUserIdIdx - 1);
		return readString(position, Len.CW48R_USER_ID);
	}

	public void setCw48rStatusCdCi(int cw48rStatusCdCiIdx, char cw48rStatusCdCi) {
		int position = Pos.cw48rStatusCdCi(cw48rStatusCdCiIdx - 1);
		writeChar(position, cw48rStatusCdCi);
	}

	/**Original name: CW48R-STATUS-CD-CI<br>*/
	public char getCw48rStatusCdCi(int cw48rStatusCdCiIdx) {
		int position = Pos.cw48rStatusCdCi(cw48rStatusCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rStatusCd(int cw48rStatusCdIdx, char cw48rStatusCd) {
		int position = Pos.cw48rStatusCd(cw48rStatusCdIdx - 1);
		writeChar(position, cw48rStatusCd);
	}

	/**Original name: CW48R-STATUS-CD<br>*/
	public char getCw48rStatusCd(int cw48rStatusCdIdx) {
		int position = Pos.cw48rStatusCd(cw48rStatusCdIdx - 1);
		return readChar(position);
	}

	public void setCw48rTerminalIdCi(int cw48rTerminalIdCiIdx, char cw48rTerminalIdCi) {
		int position = Pos.cw48rTerminalIdCi(cw48rTerminalIdCiIdx - 1);
		writeChar(position, cw48rTerminalIdCi);
	}

	/**Original name: CW48R-TERMINAL-ID-CI<br>*/
	public char getCw48rTerminalIdCi(int cw48rTerminalIdCiIdx) {
		int position = Pos.cw48rTerminalIdCi(cw48rTerminalIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rTerminalId(int cw48rTerminalIdIdx, String cw48rTerminalId) {
		int position = Pos.cw48rTerminalId(cw48rTerminalIdIdx - 1);
		writeString(position, cw48rTerminalId, Len.CW48R_TERMINAL_ID);
	}

	/**Original name: CW48R-TERMINAL-ID<br>*/
	public String getCw48rTerminalId(int cw48rTerminalIdIdx) {
		int position = Pos.cw48rTerminalId(cw48rTerminalIdIdx - 1);
		return readString(position, Len.CW48R_TERMINAL_ID);
	}

	public void setCw48rExpirationDtCi(int cw48rExpirationDtCiIdx, char cw48rExpirationDtCi) {
		int position = Pos.cw48rExpirationDtCi(cw48rExpirationDtCiIdx - 1);
		writeChar(position, cw48rExpirationDtCi);
	}

	/**Original name: CW48R-EXPIRATION-DT-CI<br>*/
	public char getCw48rExpirationDtCi(int cw48rExpirationDtCiIdx) {
		int position = Pos.cw48rExpirationDtCi(cw48rExpirationDtCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rExpirationDt(int cw48rExpirationDtIdx, String cw48rExpirationDt) {
		int position = Pos.cw48rExpirationDt(cw48rExpirationDtIdx - 1);
		writeString(position, cw48rExpirationDt, Len.CW48R_EXPIRATION_DT);
	}

	/**Original name: CW48R-EXPIRATION-DT<br>*/
	public String getCw48rExpirationDt(int cw48rExpirationDtIdx) {
		int position = Pos.cw48rExpirationDt(cw48rExpirationDtIdx - 1);
		return readString(position, Len.CW48R_EXPIRATION_DT);
	}

	public void setCw48rEffectiveAcyTsCi(int cw48rEffectiveAcyTsCiIdx, char cw48rEffectiveAcyTsCi) {
		int position = Pos.cw48rEffectiveAcyTsCi(cw48rEffectiveAcyTsCiIdx - 1);
		writeChar(position, cw48rEffectiveAcyTsCi);
	}

	/**Original name: CW48R-EFFECTIVE-ACY-TS-CI<br>*/
	public char getCw48rEffectiveAcyTsCi(int cw48rEffectiveAcyTsCiIdx) {
		int position = Pos.cw48rEffectiveAcyTsCi(cw48rEffectiveAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rEffectiveAcyTs(int cw48rEffectiveAcyTsIdx, String cw48rEffectiveAcyTs) {
		int position = Pos.cw48rEffectiveAcyTs(cw48rEffectiveAcyTsIdx - 1);
		writeString(position, cw48rEffectiveAcyTs, Len.CW48R_EFFECTIVE_ACY_TS);
	}

	/**Original name: CW48R-EFFECTIVE-ACY-TS<br>*/
	public String getCw48rEffectiveAcyTs(int cw48rEffectiveAcyTsIdx) {
		int position = Pos.cw48rEffectiveAcyTs(cw48rEffectiveAcyTsIdx - 1);
		return readString(position, Len.CW48R_EFFECTIVE_ACY_TS);
	}

	public void setCw48rExpirationAcyTsCi(int cw48rExpirationAcyTsCiIdx, char cw48rExpirationAcyTsCi) {
		int position = Pos.cw48rExpirationAcyTsCi(cw48rExpirationAcyTsCiIdx - 1);
		writeChar(position, cw48rExpirationAcyTsCi);
	}

	/**Original name: CW48R-EXPIRATION-ACY-TS-CI<br>*/
	public char getCw48rExpirationAcyTsCi(int cw48rExpirationAcyTsCiIdx) {
		int position = Pos.cw48rExpirationAcyTsCi(cw48rExpirationAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCw48rExpirationAcyTsNi(int cw48rExpirationAcyTsNiIdx, char cw48rExpirationAcyTsNi) {
		int position = Pos.cw48rExpirationAcyTsNi(cw48rExpirationAcyTsNiIdx - 1);
		writeChar(position, cw48rExpirationAcyTsNi);
	}

	/**Original name: CW48R-EXPIRATION-ACY-TS-NI<br>*/
	public char getCw48rExpirationAcyTsNi(int cw48rExpirationAcyTsNiIdx) {
		int position = Pos.cw48rExpirationAcyTsNi(cw48rExpirationAcyTsNiIdx - 1);
		return readChar(position);
	}

	public void setCw48rExpirationAcyTs(int cw48rExpirationAcyTsIdx, String cw48rExpirationAcyTs) {
		int position = Pos.cw48rExpirationAcyTs(cw48rExpirationAcyTsIdx - 1);
		writeString(position, cw48rExpirationAcyTs, Len.CW48R_EXPIRATION_ACY_TS);
	}

	/**Original name: CW48R-EXPIRATION-ACY-TS<br>*/
	public String getCw48rExpirationAcyTs(int cw48rExpirationAcyTsIdx) {
		int position = Pos.cw48rExpirationAcyTs(cw48rExpirationAcyTsIdx - 1);
		return readString(position, Len.CW48R_EXPIRATION_ACY_TS);
	}

	public void setCw48rTobCdDesc(int cw48rTobCdDescIdx, String cw48rTobCdDesc) {
		int position = Pos.cw48rTobCdDesc(cw48rTobCdDescIdx - 1);
		writeString(position, cw48rTobCdDesc, Len.CW48R_TOB_CD_DESC);
	}

	/**Original name: CW48R-TOB-CD-DESC<br>*/
	public String getCw48rTobCdDesc(int cw48rTobCdDescIdx) {
		int position = Pos.cw48rTobCdDesc(cw48rTobCdDescIdx - 1);
		return readString(position, Len.CW48R_TOB_CD_DESC);
	}

	public void setCw49rFedSegInfoRowFormatted(String data) {
		writeString(Pos.CW49R_FED_SEG_INFO_ROW, data, Len.CW49R_FED_SEG_INFO_ROW);
	}

	public void setCw49rFedSegInfoCsumFormatted(String cw49rFedSegInfoCsum) {
		writeString(Pos.CW49R_FED_SEG_INFO_CSUM, Trunc.toUnsignedNumeric(cw49rFedSegInfoCsum, Len.CW49R_FED_SEG_INFO_CSUM),
				Len.CW49R_FED_SEG_INFO_CSUM);
	}

	/**Original name: CW49R-FED-SEG-INFO-CSUM<br>*/
	public int getCw49rFedSegInfoCsum() {
		return readNumDispUnsignedInt(Pos.CW49R_FED_SEG_INFO_CSUM, Len.CW49R_FED_SEG_INFO_CSUM);
	}

	public void setCw49rClientIdKcre(String cw49rClientIdKcre) {
		writeString(Pos.CW49R_CLIENT_ID_KCRE, cw49rClientIdKcre, Len.CW49R_CLIENT_ID_KCRE);
	}

	/**Original name: CW49R-CLIENT-ID-KCRE<br>*/
	public String getCw49rClientIdKcre() {
		return readString(Pos.CW49R_CLIENT_ID_KCRE, Len.CW49R_CLIENT_ID_KCRE);
	}

	public void setCw49rHistoryVldNbrKcre(String cw49rHistoryVldNbrKcre) {
		writeString(Pos.CW49R_HISTORY_VLD_NBR_KCRE, cw49rHistoryVldNbrKcre, Len.CW49R_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CW49R-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCw49rHistoryVldNbrKcre() {
		return readString(Pos.CW49R_HISTORY_VLD_NBR_KCRE, Len.CW49R_HISTORY_VLD_NBR_KCRE);
	}

	public void setCw49rEffectiveDtKcre(String cw49rEffectiveDtKcre) {
		writeString(Pos.CW49R_EFFECTIVE_DT_KCRE, cw49rEffectiveDtKcre, Len.CW49R_EFFECTIVE_DT_KCRE);
	}

	/**Original name: CW49R-EFFECTIVE-DT-KCRE<br>*/
	public String getCw49rEffectiveDtKcre() {
		return readString(Pos.CW49R_EFFECTIVE_DT_KCRE, Len.CW49R_EFFECTIVE_DT_KCRE);
	}

	public void setCw49rTransProcessDt(String cw49rTransProcessDt) {
		writeString(Pos.CW49R_TRANS_PROCESS_DT, cw49rTransProcessDt, Len.CW49R_TRANS_PROCESS_DT);
	}

	/**Original name: CW49R-TRANS-PROCESS-DT<br>*/
	public String getCw49rTransProcessDt() {
		return readString(Pos.CW49R_TRANS_PROCESS_DT, Len.CW49R_TRANS_PROCESS_DT);
	}

	public void setCw49rClientId(String cw49rClientId) {
		writeString(Pos.CW49R_CLIENT_ID, cw49rClientId, Len.CW49R_CLIENT_ID);
	}

	/**Original name: CW49R-CLIENT-ID<br>*/
	public String getCw49rClientId() {
		return readString(Pos.CW49R_CLIENT_ID, Len.CW49R_CLIENT_ID);
	}

	public void setCw49rHistoryVldNbrSignedFormatted(String cw49rHistoryVldNbrSigned) {
		writeString(Pos.CW49R_HISTORY_VLD_NBR_SIGNED, PicFormatter.display("-9(5)").format(cw49rHistoryVldNbrSigned).toString(),
				Len.CW49R_HISTORY_VLD_NBR_SIGNED);
	}

	/**Original name: CW49R-HISTORY-VLD-NBR-SIGNED<br>*/
	public long getCw49rHistoryVldNbrSigned() {
		return PicParser.display("-9(5)").parseLong(readString(Pos.CW49R_HISTORY_VLD_NBR_SIGNED, Len.CW49R_HISTORY_VLD_NBR_SIGNED));
	}

	public void setCw49rEffectiveDt(String cw49rEffectiveDt) {
		writeString(Pos.CW49R_EFFECTIVE_DT, cw49rEffectiveDt, Len.CW49R_EFFECTIVE_DT);
	}

	/**Original name: CW49R-EFFECTIVE-DT<br>*/
	public String getCw49rEffectiveDt() {
		return readString(Pos.CW49R_EFFECTIVE_DT, Len.CW49R_EFFECTIVE_DT);
	}

	public void setCw49rClientIdCi(char cw49rClientIdCi) {
		writeChar(Pos.CW49R_CLIENT_ID_CI, cw49rClientIdCi);
	}

	/**Original name: CW49R-CLIENT-ID-CI<br>*/
	public char getCw49rClientIdCi() {
		return readChar(Pos.CW49R_CLIENT_ID_CI);
	}

	public void setCw49rHistoryVldNbrCi(char cw49rHistoryVldNbrCi) {
		writeChar(Pos.CW49R_HISTORY_VLD_NBR_CI, cw49rHistoryVldNbrCi);
	}

	/**Original name: CW49R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw49rHistoryVldNbrCi() {
		return readChar(Pos.CW49R_HISTORY_VLD_NBR_CI);
	}

	public void setCw49rEffectiveDtCi(char cw49rEffectiveDtCi) {
		writeChar(Pos.CW49R_EFFECTIVE_DT_CI, cw49rEffectiveDtCi);
	}

	/**Original name: CW49R-EFFECTIVE-DT-CI<br>*/
	public char getCw49rEffectiveDtCi() {
		return readChar(Pos.CW49R_EFFECTIVE_DT_CI);
	}

	public void setCw49rSegCdCi(char cw49rSegCdCi) {
		writeChar(Pos.CW49R_SEG_CD_CI, cw49rSegCdCi);
	}

	/**Original name: CW49R-SEG-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw49rSegCdCi() {
		return readChar(Pos.CW49R_SEG_CD_CI);
	}

	public void setCw49rSegCd(String cw49rSegCd) {
		writeString(Pos.CW49R_SEG_CD, cw49rSegCd, Len.CW49R_SEG_CD);
	}

	/**Original name: CW49R-SEG-CD<br>*/
	public String getCw49rSegCd() {
		return readString(Pos.CW49R_SEG_CD, Len.CW49R_SEG_CD);
	}

	public void setCw49rUserIdCi(char cw49rUserIdCi) {
		writeChar(Pos.CW49R_USER_ID_CI, cw49rUserIdCi);
	}

	/**Original name: CW49R-USER-ID-CI<br>*/
	public char getCw49rUserIdCi() {
		return readChar(Pos.CW49R_USER_ID_CI);
	}

	public void setCw49rUserId(String cw49rUserId) {
		writeString(Pos.CW49R_USER_ID, cw49rUserId, Len.CW49R_USER_ID);
	}

	/**Original name: CW49R-USER-ID<br>*/
	public String getCw49rUserId() {
		return readString(Pos.CW49R_USER_ID, Len.CW49R_USER_ID);
	}

	public void setCw49rStatusCdCi(char cw49rStatusCdCi) {
		writeChar(Pos.CW49R_STATUS_CD_CI, cw49rStatusCdCi);
	}

	/**Original name: CW49R-STATUS-CD-CI<br>*/
	public char getCw49rStatusCdCi() {
		return readChar(Pos.CW49R_STATUS_CD_CI);
	}

	public void setCw49rStatusCd(char cw49rStatusCd) {
		writeChar(Pos.CW49R_STATUS_CD, cw49rStatusCd);
	}

	/**Original name: CW49R-STATUS-CD<br>*/
	public char getCw49rStatusCd() {
		return readChar(Pos.CW49R_STATUS_CD);
	}

	public void setCw49rTerminalIdCi(char cw49rTerminalIdCi) {
		writeChar(Pos.CW49R_TERMINAL_ID_CI, cw49rTerminalIdCi);
	}

	/**Original name: CW49R-TERMINAL-ID-CI<br>*/
	public char getCw49rTerminalIdCi() {
		return readChar(Pos.CW49R_TERMINAL_ID_CI);
	}

	public void setCw49rTerminalId(String cw49rTerminalId) {
		writeString(Pos.CW49R_TERMINAL_ID, cw49rTerminalId, Len.CW49R_TERMINAL_ID);
	}

	/**Original name: CW49R-TERMINAL-ID<br>*/
	public String getCw49rTerminalId() {
		return readString(Pos.CW49R_TERMINAL_ID, Len.CW49R_TERMINAL_ID);
	}

	public void setCw49rExpirationDtCi(char cw49rExpirationDtCi) {
		writeChar(Pos.CW49R_EXPIRATION_DT_CI, cw49rExpirationDtCi);
	}

	/**Original name: CW49R-EXPIRATION-DT-CI<br>*/
	public char getCw49rExpirationDtCi() {
		return readChar(Pos.CW49R_EXPIRATION_DT_CI);
	}

	public void setCw49rExpirationDt(String cw49rExpirationDt) {
		writeString(Pos.CW49R_EXPIRATION_DT, cw49rExpirationDt, Len.CW49R_EXPIRATION_DT);
	}

	/**Original name: CW49R-EXPIRATION-DT<br>*/
	public String getCw49rExpirationDt() {
		return readString(Pos.CW49R_EXPIRATION_DT, Len.CW49R_EXPIRATION_DT);
	}

	public void setCw49rEffectiveAcyTsCi(char cw49rEffectiveAcyTsCi) {
		writeChar(Pos.CW49R_EFFECTIVE_ACY_TS_CI, cw49rEffectiveAcyTsCi);
	}

	/**Original name: CW49R-EFFECTIVE-ACY-TS-CI<br>*/
	public char getCw49rEffectiveAcyTsCi() {
		return readChar(Pos.CW49R_EFFECTIVE_ACY_TS_CI);
	}

	public void setCw49rEffectiveAcyTs(String cw49rEffectiveAcyTs) {
		writeString(Pos.CW49R_EFFECTIVE_ACY_TS, cw49rEffectiveAcyTs, Len.CW49R_EFFECTIVE_ACY_TS);
	}

	/**Original name: CW49R-EFFECTIVE-ACY-TS<br>*/
	public String getCw49rEffectiveAcyTs() {
		return readString(Pos.CW49R_EFFECTIVE_ACY_TS, Len.CW49R_EFFECTIVE_ACY_TS);
	}

	public void setCw49rExpirationAcyTsCi(char cw49rExpirationAcyTsCi) {
		writeChar(Pos.CW49R_EXPIRATION_ACY_TS_CI, cw49rExpirationAcyTsCi);
	}

	/**Original name: CW49R-EXPIRATION-ACY-TS-CI<br>*/
	public char getCw49rExpirationAcyTsCi() {
		return readChar(Pos.CW49R_EXPIRATION_ACY_TS_CI);
	}

	public void setCw49rExpirationAcyTsNi(char cw49rExpirationAcyTsNi) {
		writeChar(Pos.CW49R_EXPIRATION_ACY_TS_NI, cw49rExpirationAcyTsNi);
	}

	/**Original name: CW49R-EXPIRATION-ACY-TS-NI<br>*/
	public char getCw49rExpirationAcyTsNi() {
		return readChar(Pos.CW49R_EXPIRATION_ACY_TS_NI);
	}

	public void setCw49rExpirationAcyTs(String cw49rExpirationAcyTs) {
		writeString(Pos.CW49R_EXPIRATION_ACY_TS, cw49rExpirationAcyTs, Len.CW49R_EXPIRATION_ACY_TS);
	}

	/**Original name: CW49R-EXPIRATION-ACY-TS<br>*/
	public String getCw49rExpirationAcyTs() {
		return readString(Pos.CW49R_EXPIRATION_ACY_TS, Len.CW49R_EXPIRATION_ACY_TS);
	}

	public void setCw49rSegDes(String cw49rSegDes) {
		writeString(Pos.CW49R_SEG_DES, cw49rSegDes, Len.CW49R_SEG_DES);
	}

	/**Original name: CW49R-SEG-DES<br>*/
	public String getCw49rSegDes() {
		return readString(Pos.CW49R_SEG_DES, Len.CW49R_SEG_DES);
	}

	public void setCw40rFedAccountInfoRowFormatted(String data) {
		writeString(Pos.CW40R_FED_ACCOUNT_INFO_ROW, data, Len.CW40R_FED_ACCOUNT_INFO_ROW);
	}

	public void setCw40rFedAccountInfoCsumFormatted(String cw40rFedAccountInfoCsum) {
		writeString(Pos.CW40R_FED_ACCOUNT_INFO_CSUM, Trunc.toUnsignedNumeric(cw40rFedAccountInfoCsum, Len.CW40R_FED_ACCOUNT_INFO_CSUM),
				Len.CW40R_FED_ACCOUNT_INFO_CSUM);
	}

	/**Original name: CW40R-FED-ACCOUNT-INFO-CSUM<br>*/
	public int getCw40rFedAccountInfoCsum() {
		return readNumDispUnsignedInt(Pos.CW40R_FED_ACCOUNT_INFO_CSUM, Len.CW40R_FED_ACCOUNT_INFO_CSUM);
	}

	public void setCw40rCiaiActTchKeyKcre(String cw40rCiaiActTchKeyKcre) {
		writeString(Pos.CW40R_CIAI_ACT_TCH_KEY_KCRE, cw40rCiaiActTchKeyKcre, Len.CW40R_CIAI_ACT_TCH_KEY_KCRE);
	}

	/**Original name: CW40R-CIAI-ACT-TCH-KEY-KCRE<br>*/
	public String getCw40rCiaiActTchKeyKcre() {
		return readString(Pos.CW40R_CIAI_ACT_TCH_KEY_KCRE, Len.CW40R_CIAI_ACT_TCH_KEY_KCRE);
	}

	public void setCw40rHistoryVldNbrKcre(String cw40rHistoryVldNbrKcre) {
		writeString(Pos.CW40R_HISTORY_VLD_NBR_KCRE, cw40rHistoryVldNbrKcre, Len.CW40R_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CW40R-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCw40rHistoryVldNbrKcre() {
		return readString(Pos.CW40R_HISTORY_VLD_NBR_KCRE, Len.CW40R_HISTORY_VLD_NBR_KCRE);
	}

	public void setCw40rEffectiveDtKcre(String cw40rEffectiveDtKcre) {
		writeString(Pos.CW40R_EFFECTIVE_DT_KCRE, cw40rEffectiveDtKcre, Len.CW40R_EFFECTIVE_DT_KCRE);
	}

	/**Original name: CW40R-EFFECTIVE-DT-KCRE<br>*/
	public String getCw40rEffectiveDtKcre() {
		return readString(Pos.CW40R_EFFECTIVE_DT_KCRE, Len.CW40R_EFFECTIVE_DT_KCRE);
	}

	public void setCw40rTransProcessDt(String cw40rTransProcessDt) {
		writeString(Pos.CW40R_TRANS_PROCESS_DT, cw40rTransProcessDt, Len.CW40R_TRANS_PROCESS_DT);
	}

	/**Original name: CW40R-TRANS-PROCESS-DT<br>*/
	public String getCw40rTransProcessDt() {
		return readString(Pos.CW40R_TRANS_PROCESS_DT, Len.CW40R_TRANS_PROCESS_DT);
	}

	public void setCw40rCiaiActTchKey(String cw40rCiaiActTchKey) {
		writeString(Pos.CW40R_CIAI_ACT_TCH_KEY, cw40rCiaiActTchKey, Len.CW40R_CIAI_ACT_TCH_KEY);
	}

	/**Original name: CW40R-CIAI-ACT-TCH-KEY<br>*/
	public String getCw40rCiaiActTchKey() {
		return readString(Pos.CW40R_CIAI_ACT_TCH_KEY, Len.CW40R_CIAI_ACT_TCH_KEY);
	}

	public void setCw40rHistoryVldNbrSign(char cw40rHistoryVldNbrSign) {
		writeChar(Pos.CW40R_HISTORY_VLD_NBR_SIGN, cw40rHistoryVldNbrSign);
	}

	/**Original name: CW40R-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw40rHistoryVldNbrSign() {
		return readChar(Pos.CW40R_HISTORY_VLD_NBR_SIGN);
	}

	public void setCw40rHistoryVldNbrFormatted(String cw40rHistoryVldNbr) {
		writeString(Pos.CW40R_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cw40rHistoryVldNbr, Len.CW40R_HISTORY_VLD_NBR), Len.CW40R_HISTORY_VLD_NBR);
	}

	/**Original name: CW40R-HISTORY-VLD-NBR<br>*/
	public int getCw40rHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CW40R_HISTORY_VLD_NBR, Len.CW40R_HISTORY_VLD_NBR);
	}

	public void setCw40rEffectiveDt(String cw40rEffectiveDt) {
		writeString(Pos.CW40R_EFFECTIVE_DT, cw40rEffectiveDt, Len.CW40R_EFFECTIVE_DT);
	}

	/**Original name: CW40R-EFFECTIVE-DT<br>*/
	public String getCw40rEffectiveDt() {
		return readString(Pos.CW40R_EFFECTIVE_DT, Len.CW40R_EFFECTIVE_DT);
	}

	public void setCw40rCiaiActTchKeyCi(char cw40rCiaiActTchKeyCi) {
		writeChar(Pos.CW40R_CIAI_ACT_TCH_KEY_CI, cw40rCiaiActTchKeyCi);
	}

	/**Original name: CW40R-CIAI-ACT-TCH-KEY-CI<br>*/
	public char getCw40rCiaiActTchKeyCi() {
		return readChar(Pos.CW40R_CIAI_ACT_TCH_KEY_CI);
	}

	public void setCw40rHistoryVldNbrCi(char cw40rHistoryVldNbrCi) {
		writeChar(Pos.CW40R_HISTORY_VLD_NBR_CI, cw40rHistoryVldNbrCi);
	}

	/**Original name: CW40R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw40rHistoryVldNbrCi() {
		return readChar(Pos.CW40R_HISTORY_VLD_NBR_CI);
	}

	public void setCw40rEffectiveDtCi(char cw40rEffectiveDtCi) {
		writeChar(Pos.CW40R_EFFECTIVE_DT_CI, cw40rEffectiveDtCi);
	}

	/**Original name: CW40R-EFFECTIVE-DT-CI<br>*/
	public char getCw40rEffectiveDtCi() {
		return readChar(Pos.CW40R_EFFECTIVE_DT_CI);
	}

	public void setCw40rOfcLocCdCi(char cw40rOfcLocCdCi) {
		writeChar(Pos.CW40R_OFC_LOC_CD_CI, cw40rOfcLocCdCi);
	}

	/**Original name: CW40R-OFC-LOC-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw40rOfcLocCdCi() {
		return readChar(Pos.CW40R_OFC_LOC_CD_CI);
	}

	public void setCw40rOfcLocCd(String cw40rOfcLocCd) {
		writeString(Pos.CW40R_OFC_LOC_CD, cw40rOfcLocCd, Len.CW40R_OFC_LOC_CD);
	}

	/**Original name: CW40R-OFC-LOC-CD<br>*/
	public String getCw40rOfcLocCd() {
		return readString(Pos.CW40R_OFC_LOC_CD, Len.CW40R_OFC_LOC_CD);
	}

	public String getCw40rOfcLocCdFormatted() {
		return Functions.padBlanks(getCw40rOfcLocCd(), Len.CW40R_OFC_LOC_CD);
	}

	public void setCw40rCiaiAuthnticLossCi(char cw40rCiaiAuthnticLossCi) {
		writeChar(Pos.CW40R_CIAI_AUTHNTIC_LOSS_CI, cw40rCiaiAuthnticLossCi);
	}

	/**Original name: CW40R-CIAI-AUTHNTIC-LOSS-CI<br>*/
	public char getCw40rCiaiAuthnticLossCi() {
		return readChar(Pos.CW40R_CIAI_AUTHNTIC_LOSS_CI);
	}

	public void setCw40rCiaiAuthnticLoss(char cw40rCiaiAuthnticLoss) {
		writeChar(Pos.CW40R_CIAI_AUTHNTIC_LOSS, cw40rCiaiAuthnticLoss);
	}

	/**Original name: CW40R-CIAI-AUTHNTIC-LOSS<br>*/
	public char getCw40rCiaiAuthnticLoss() {
		return readChar(Pos.CW40R_CIAI_AUTHNTIC_LOSS);
	}

	public void setCw40rFedStCdCi(char cw40rFedStCdCi) {
		writeChar(Pos.CW40R_FED_ST_CD_CI, cw40rFedStCdCi);
	}

	/**Original name: CW40R-FED-ST-CD-CI<br>*/
	public char getCw40rFedStCdCi() {
		return readChar(Pos.CW40R_FED_ST_CD_CI);
	}

	public void setCw40rFedStCd(String cw40rFedStCd) {
		writeString(Pos.CW40R_FED_ST_CD, cw40rFedStCd, Len.CW40R_FED_ST_CD);
	}

	/**Original name: CW40R-FED-ST-CD<br>*/
	public String getCw40rFedStCd() {
		return readString(Pos.CW40R_FED_ST_CD, Len.CW40R_FED_ST_CD);
	}

	public void setCw40rFedCountyCdCi(char cw40rFedCountyCdCi) {
		writeChar(Pos.CW40R_FED_COUNTY_CD_CI, cw40rFedCountyCdCi);
	}

	/**Original name: CW40R-FED-COUNTY-CD-CI<br>*/
	public char getCw40rFedCountyCdCi() {
		return readChar(Pos.CW40R_FED_COUNTY_CD_CI);
	}

	public void setCw40rFedCountyCd(String cw40rFedCountyCd) {
		writeString(Pos.CW40R_FED_COUNTY_CD, cw40rFedCountyCd, Len.CW40R_FED_COUNTY_CD);
	}

	/**Original name: CW40R-FED-COUNTY-CD<br>*/
	public String getCw40rFedCountyCd() {
		return readString(Pos.CW40R_FED_COUNTY_CD, Len.CW40R_FED_COUNTY_CD);
	}

	public void setCw40rFedTownCdCi(char cw40rFedTownCdCi) {
		writeChar(Pos.CW40R_FED_TOWN_CD_CI, cw40rFedTownCdCi);
	}

	/**Original name: CW40R-FED-TOWN-CD-CI<br>*/
	public char getCw40rFedTownCdCi() {
		return readChar(Pos.CW40R_FED_TOWN_CD_CI);
	}

	public void setCw40rFedTownCd(String cw40rFedTownCd) {
		writeString(Pos.CW40R_FED_TOWN_CD, cw40rFedTownCd, Len.CW40R_FED_TOWN_CD);
	}

	/**Original name: CW40R-FED-TOWN-CD<br>*/
	public String getCw40rFedTownCd() {
		return readString(Pos.CW40R_FED_TOWN_CD, Len.CW40R_FED_TOWN_CD);
	}

	public void setCw40rUserIdCi(char cw40rUserIdCi) {
		writeChar(Pos.CW40R_USER_ID_CI, cw40rUserIdCi);
	}

	/**Original name: CW40R-USER-ID-CI<br>*/
	public char getCw40rUserIdCi() {
		return readChar(Pos.CW40R_USER_ID_CI);
	}

	public void setCw40rUserId(String cw40rUserId) {
		writeString(Pos.CW40R_USER_ID, cw40rUserId, Len.CW40R_USER_ID);
	}

	/**Original name: CW40R-USER-ID<br>*/
	public String getCw40rUserId() {
		return readString(Pos.CW40R_USER_ID, Len.CW40R_USER_ID);
	}

	public void setCw40rStatusCdCi(char cw40rStatusCdCi) {
		writeChar(Pos.CW40R_STATUS_CD_CI, cw40rStatusCdCi);
	}

	/**Original name: CW40R-STATUS-CD-CI<br>*/
	public char getCw40rStatusCdCi() {
		return readChar(Pos.CW40R_STATUS_CD_CI);
	}

	public void setCw40rStatusCd(char cw40rStatusCd) {
		writeChar(Pos.CW40R_STATUS_CD, cw40rStatusCd);
	}

	/**Original name: CW40R-STATUS-CD<br>*/
	public char getCw40rStatusCd() {
		return readChar(Pos.CW40R_STATUS_CD);
	}

	public void setCw40rTerminalIdCi(char cw40rTerminalIdCi) {
		writeChar(Pos.CW40R_TERMINAL_ID_CI, cw40rTerminalIdCi);
	}

	/**Original name: CW40R-TERMINAL-ID-CI<br>*/
	public char getCw40rTerminalIdCi() {
		return readChar(Pos.CW40R_TERMINAL_ID_CI);
	}

	public void setCw40rTerminalId(String cw40rTerminalId) {
		writeString(Pos.CW40R_TERMINAL_ID, cw40rTerminalId, Len.CW40R_TERMINAL_ID);
	}

	/**Original name: CW40R-TERMINAL-ID<br>*/
	public String getCw40rTerminalId() {
		return readString(Pos.CW40R_TERMINAL_ID, Len.CW40R_TERMINAL_ID);
	}

	public void setCw40rExpirationDtCi(char cw40rExpirationDtCi) {
		writeChar(Pos.CW40R_EXPIRATION_DT_CI, cw40rExpirationDtCi);
	}

	/**Original name: CW40R-EXPIRATION-DT-CI<br>*/
	public char getCw40rExpirationDtCi() {
		return readChar(Pos.CW40R_EXPIRATION_DT_CI);
	}

	public void setCw40rExpirationDt(String cw40rExpirationDt) {
		writeString(Pos.CW40R_EXPIRATION_DT, cw40rExpirationDt, Len.CW40R_EXPIRATION_DT);
	}

	/**Original name: CW40R-EXPIRATION-DT<br>*/
	public String getCw40rExpirationDt() {
		return readString(Pos.CW40R_EXPIRATION_DT, Len.CW40R_EXPIRATION_DT);
	}

	public void setCw40rEffectiveAcyTsCi(char cw40rEffectiveAcyTsCi) {
		writeChar(Pos.CW40R_EFFECTIVE_ACY_TS_CI, cw40rEffectiveAcyTsCi);
	}

	/**Original name: CW40R-EFFECTIVE-ACY-TS-CI<br>*/
	public char getCw40rEffectiveAcyTsCi() {
		return readChar(Pos.CW40R_EFFECTIVE_ACY_TS_CI);
	}

	public void setCw40rEffectiveAcyTs(String cw40rEffectiveAcyTs) {
		writeString(Pos.CW40R_EFFECTIVE_ACY_TS, cw40rEffectiveAcyTs, Len.CW40R_EFFECTIVE_ACY_TS);
	}

	/**Original name: CW40R-EFFECTIVE-ACY-TS<br>*/
	public String getCw40rEffectiveAcyTs() {
		return readString(Pos.CW40R_EFFECTIVE_ACY_TS, Len.CW40R_EFFECTIVE_ACY_TS);
	}

	public void setCw40rExpirationAcyTsCi(char cw40rExpirationAcyTsCi) {
		writeChar(Pos.CW40R_EXPIRATION_ACY_TS_CI, cw40rExpirationAcyTsCi);
	}

	/**Original name: CW40R-EXPIRATION-ACY-TS-CI<br>*/
	public char getCw40rExpirationAcyTsCi() {
		return readChar(Pos.CW40R_EXPIRATION_ACY_TS_CI);
	}

	public void setCw40rExpirationAcyTsNi(char cw40rExpirationAcyTsNi) {
		writeChar(Pos.CW40R_EXPIRATION_ACY_TS_NI, cw40rExpirationAcyTsNi);
	}

	/**Original name: CW40R-EXPIRATION-ACY-TS-NI<br>*/
	public char getCw40rExpirationAcyTsNi() {
		return readChar(Pos.CW40R_EXPIRATION_ACY_TS_NI);
	}

	public void setCw40rExpirationAcyTs(String cw40rExpirationAcyTs) {
		writeString(Pos.CW40R_EXPIRATION_ACY_TS, cw40rExpirationAcyTs, Len.CW40R_EXPIRATION_ACY_TS);
	}

	/**Original name: CW40R-EXPIRATION-ACY-TS<br>*/
	public String getCw40rExpirationAcyTs() {
		return readString(Pos.CW40R_EXPIRATION_ACY_TS, Len.CW40R_EXPIRATION_ACY_TS);
	}

	public void setCw01rBusinessClientRowFormatted(String data) {
		writeString(Pos.CW01R_BUSINESS_CLIENT_ROW, data, Len.CW01R_BUSINESS_CLIENT_ROW);
	}

	public void setCw01rBusinessClientChkSumFormatted(String cw01rBusinessClientChkSum) {
		writeString(Pos.CW01R_BUSINESS_CLIENT_CHK_SUM, Trunc.toUnsignedNumeric(cw01rBusinessClientChkSum, Len.CW01R_BUSINESS_CLIENT_CHK_SUM),
				Len.CW01R_BUSINESS_CLIENT_CHK_SUM);
	}

	/**Original name: CW01R-BUSINESS-CLIENT-CHK-SUM<br>*/
	public int getCw01rBusinessClientChkSum() {
		return readNumDispUnsignedInt(Pos.CW01R_BUSINESS_CLIENT_CHK_SUM, Len.CW01R_BUSINESS_CLIENT_CHK_SUM);
	}

	public void setCw01rClientIdKcre(String cw01rClientIdKcre) {
		writeString(Pos.CW01R_CLIENT_ID_KCRE, cw01rClientIdKcre, Len.CW01R_CLIENT_ID_KCRE);
	}

	/**Original name: CW01R-CLIENT-ID-KCRE<br>*/
	public String getCw01rClientIdKcre() {
		return readString(Pos.CW01R_CLIENT_ID_KCRE, Len.CW01R_CLIENT_ID_KCRE);
	}

	public void setCw01rCibcBusSeqNbrKcre(String cw01rCibcBusSeqNbrKcre) {
		writeString(Pos.CW01R_CIBC_BUS_SEQ_NBR_KCRE, cw01rCibcBusSeqNbrKcre, Len.CW01R_CIBC_BUS_SEQ_NBR_KCRE);
	}

	/**Original name: CW01R-CIBC-BUS-SEQ-NBR-KCRE<br>*/
	public String getCw01rCibcBusSeqNbrKcre() {
		return readString(Pos.CW01R_CIBC_BUS_SEQ_NBR_KCRE, Len.CW01R_CIBC_BUS_SEQ_NBR_KCRE);
	}

	public void setCw01rTransProcessDt(String cw01rTransProcessDt) {
		writeString(Pos.CW01R_TRANS_PROCESS_DT, cw01rTransProcessDt, Len.CW01R_TRANS_PROCESS_DT);
	}

	/**Original name: CW01R-TRANS-PROCESS-DT<br>*/
	public String getCw01rTransProcessDt() {
		return readString(Pos.CW01R_TRANS_PROCESS_DT, Len.CW01R_TRANS_PROCESS_DT);
	}

	public void setCw01rClientIdCi(char cw01rClientIdCi) {
		writeChar(Pos.CW01R_CLIENT_ID_CI, cw01rClientIdCi);
	}

	/**Original name: CW01R-CLIENT-ID-CI<br>*/
	public char getCw01rClientIdCi() {
		return readChar(Pos.CW01R_CLIENT_ID_CI);
	}

	public void setCw01rClientId(String cw01rClientId) {
		writeString(Pos.CW01R_CLIENT_ID, cw01rClientId, Len.CW01R_CLIENT_ID);
	}

	/**Original name: CW01R-CLIENT-ID<br>*/
	public String getCw01rClientId() {
		return readString(Pos.CW01R_CLIENT_ID, Len.CW01R_CLIENT_ID);
	}

	public void setCw01rHistoryVldNbrCi(char cw01rHistoryVldNbrCi) {
		writeChar(Pos.CW01R_HISTORY_VLD_NBR_CI, cw01rHistoryVldNbrCi);
	}

	/**Original name: CW01R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw01rHistoryVldNbrCi() {
		return readChar(Pos.CW01R_HISTORY_VLD_NBR_CI);
	}

	public void setCw01rHistoryVldNbrSign(char cw01rHistoryVldNbrSign) {
		writeChar(Pos.CW01R_HISTORY_VLD_NBR_SIGN, cw01rHistoryVldNbrSign);
	}

	/**Original name: CW01R-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw01rHistoryVldNbrSign() {
		return readChar(Pos.CW01R_HISTORY_VLD_NBR_SIGN);
	}

	public void setCw01rHistoryVldNbrFormatted(String cw01rHistoryVldNbr) {
		writeString(Pos.CW01R_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cw01rHistoryVldNbr, Len.CW01R_HISTORY_VLD_NBR), Len.CW01R_HISTORY_VLD_NBR);
	}

	/**Original name: CW01R-HISTORY-VLD-NBR<br>*/
	public int getCw01rHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CW01R_HISTORY_VLD_NBR, Len.CW01R_HISTORY_VLD_NBR);
	}

	public void setCw01rCibcBusSeqNbrCi(char cw01rCibcBusSeqNbrCi) {
		writeChar(Pos.CW01R_CIBC_BUS_SEQ_NBR_CI, cw01rCibcBusSeqNbrCi);
	}

	/**Original name: CW01R-CIBC-BUS-SEQ-NBR-CI<br>*/
	public char getCw01rCibcBusSeqNbrCi() {
		return readChar(Pos.CW01R_CIBC_BUS_SEQ_NBR_CI);
	}

	public void setCw01rCibcBusSeqNbrSign(char cw01rCibcBusSeqNbrSign) {
		writeChar(Pos.CW01R_CIBC_BUS_SEQ_NBR_SIGN, cw01rCibcBusSeqNbrSign);
	}

	/**Original name: CW01R-CIBC-BUS-SEQ-NBR-SIGN<br>*/
	public char getCw01rCibcBusSeqNbrSign() {
		return readChar(Pos.CW01R_CIBC_BUS_SEQ_NBR_SIGN);
	}

	public void setCw01rCibcBusSeqNbrFormatted(String cw01rCibcBusSeqNbr) {
		writeString(Pos.CW01R_CIBC_BUS_SEQ_NBR, Trunc.toUnsignedNumeric(cw01rCibcBusSeqNbr, Len.CW01R_CIBC_BUS_SEQ_NBR), Len.CW01R_CIBC_BUS_SEQ_NBR);
	}

	/**Original name: CW01R-CIBC-BUS-SEQ-NBR<br>*/
	public int getCw01rCibcBusSeqNbr() {
		return readNumDispUnsignedInt(Pos.CW01R_CIBC_BUS_SEQ_NBR, Len.CW01R_CIBC_BUS_SEQ_NBR);
	}

	public void setCw01rCibcEffDtCi(char cw01rCibcEffDtCi) {
		writeChar(Pos.CW01R_CIBC_EFF_DT_CI, cw01rCibcEffDtCi);
	}

	/**Original name: CW01R-CIBC-EFF-DT-CI<br>*/
	public char getCw01rCibcEffDtCi() {
		return readChar(Pos.CW01R_CIBC_EFF_DT_CI);
	}

	public void setCw01rCibcEffDt(String cw01rCibcEffDt) {
		writeString(Pos.CW01R_CIBC_EFF_DT, cw01rCibcEffDt, Len.CW01R_CIBC_EFF_DT);
	}

	/**Original name: CW01R-CIBC-EFF-DT<br>*/
	public String getCw01rCibcEffDt() {
		return readString(Pos.CW01R_CIBC_EFF_DT, Len.CW01R_CIBC_EFF_DT);
	}

	public void setCw01rCibcExpDtCi(char cw01rCibcExpDtCi) {
		writeChar(Pos.CW01R_CIBC_EXP_DT_CI, cw01rCibcExpDtCi);
	}

	/**Original name: CW01R-CIBC-EXP-DT-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw01rCibcExpDtCi() {
		return readChar(Pos.CW01R_CIBC_EXP_DT_CI);
	}

	public void setCw01rCibcExpDt(String cw01rCibcExpDt) {
		writeString(Pos.CW01R_CIBC_EXP_DT, cw01rCibcExpDt, Len.CW01R_CIBC_EXP_DT);
	}

	/**Original name: CW01R-CIBC-EXP-DT<br>*/
	public String getCw01rCibcExpDt() {
		return readString(Pos.CW01R_CIBC_EXP_DT, Len.CW01R_CIBC_EXP_DT);
	}

	public void setCw01rGrsRevCdCi(char cw01rGrsRevCdCi) {
		writeChar(Pos.CW01R_GRS_REV_CD_CI, cw01rGrsRevCdCi);
	}

	/**Original name: CW01R-GRS-REV-CD-CI<br>*/
	public char getCw01rGrsRevCdCi() {
		return readChar(Pos.CW01R_GRS_REV_CD_CI);
	}

	public void setCw01rGrsRevCd(String cw01rGrsRevCd) {
		writeString(Pos.CW01R_GRS_REV_CD, cw01rGrsRevCd, Len.CW01R_GRS_REV_CD);
	}

	/**Original name: CW01R-GRS-REV-CD<br>*/
	public String getCw01rGrsRevCd() {
		return readString(Pos.CW01R_GRS_REV_CD, Len.CW01R_GRS_REV_CD);
	}

	public void setCw01rIdyTypCdCi(char cw01rIdyTypCdCi) {
		writeChar(Pos.CW01R_IDY_TYP_CD_CI, cw01rIdyTypCdCi);
	}

	/**Original name: CW01R-IDY-TYP-CD-CI<br>*/
	public char getCw01rIdyTypCdCi() {
		return readChar(Pos.CW01R_IDY_TYP_CD_CI);
	}

	public void setCw01rIdyTypCd(String cw01rIdyTypCd) {
		writeString(Pos.CW01R_IDY_TYP_CD, cw01rIdyTypCd, Len.CW01R_IDY_TYP_CD);
	}

	/**Original name: CW01R-IDY-TYP-CD<br>*/
	public String getCw01rIdyTypCd() {
		return readString(Pos.CW01R_IDY_TYP_CD, Len.CW01R_IDY_TYP_CD);
	}

	public void setCw01rCibcNbrEmpCi(char cw01rCibcNbrEmpCi) {
		writeChar(Pos.CW01R_CIBC_NBR_EMP_CI, cw01rCibcNbrEmpCi);
	}

	/**Original name: CW01R-CIBC-NBR-EMP-CI<br>*/
	public char getCw01rCibcNbrEmpCi() {
		return readChar(Pos.CW01R_CIBC_NBR_EMP_CI);
	}

	public void setCw01rCibcNbrEmpNi(char cw01rCibcNbrEmpNi) {
		writeChar(Pos.CW01R_CIBC_NBR_EMP_NI, cw01rCibcNbrEmpNi);
	}

	/**Original name: CW01R-CIBC-NBR-EMP-NI<br>*/
	public char getCw01rCibcNbrEmpNi() {
		return readChar(Pos.CW01R_CIBC_NBR_EMP_NI);
	}

	public void setCw01rCibcNbrEmpSign(char cw01rCibcNbrEmpSign) {
		writeChar(Pos.CW01R_CIBC_NBR_EMP_SIGN, cw01rCibcNbrEmpSign);
	}

	/**Original name: CW01R-CIBC-NBR-EMP-SIGN<br>*/
	public char getCw01rCibcNbrEmpSign() {
		return readChar(Pos.CW01R_CIBC_NBR_EMP_SIGN);
	}

	public void setCw01rCibcNbrEmpFormatted(String cw01rCibcNbrEmp) {
		writeString(Pos.CW01R_CIBC_NBR_EMP, Trunc.toUnsignedNumeric(cw01rCibcNbrEmp, Len.CW01R_CIBC_NBR_EMP), Len.CW01R_CIBC_NBR_EMP);
	}

	/**Original name: CW01R-CIBC-NBR-EMP<br>*/
	public long getCw01rCibcNbrEmp() {
		return readNumDispUnsignedLong(Pos.CW01R_CIBC_NBR_EMP, Len.CW01R_CIBC_NBR_EMP);
	}

	public String getCw01rCibcNbrEmpFormatted() {
		return readFixedString(Pos.CW01R_CIBC_NBR_EMP, Len.CW01R_CIBC_NBR_EMP);
	}

	public void setCw01rCibcStrDtCi(char cw01rCibcStrDtCi) {
		writeChar(Pos.CW01R_CIBC_STR_DT_CI, cw01rCibcStrDtCi);
	}

	/**Original name: CW01R-CIBC-STR-DT-CI<br>*/
	public char getCw01rCibcStrDtCi() {
		return readChar(Pos.CW01R_CIBC_STR_DT_CI);
	}

	public void setCw01rCibcStrDtNi(char cw01rCibcStrDtNi) {
		writeChar(Pos.CW01R_CIBC_STR_DT_NI, cw01rCibcStrDtNi);
	}

	/**Original name: CW01R-CIBC-STR-DT-NI<br>*/
	public char getCw01rCibcStrDtNi() {
		return readChar(Pos.CW01R_CIBC_STR_DT_NI);
	}

	public void setCw01rCibcStrDt(String cw01rCibcStrDt) {
		writeString(Pos.CW01R_CIBC_STR_DT, cw01rCibcStrDt, Len.CW01R_CIBC_STR_DT);
	}

	/**Original name: CW01R-CIBC-STR-DT<br>*/
	public String getCw01rCibcStrDt() {
		return readString(Pos.CW01R_CIBC_STR_DT, Len.CW01R_CIBC_STR_DT);
	}

	public void setCw01rUserIdCi(char cw01rUserIdCi) {
		writeChar(Pos.CW01R_USER_ID_CI, cw01rUserIdCi);
	}

	/**Original name: CW01R-USER-ID-CI<br>*/
	public char getCw01rUserIdCi() {
		return readChar(Pos.CW01R_USER_ID_CI);
	}

	public void setCw01rUserId(String cw01rUserId) {
		writeString(Pos.CW01R_USER_ID, cw01rUserId, Len.CW01R_USER_ID);
	}

	/**Original name: CW01R-USER-ID<br>*/
	public String getCw01rUserId() {
		return readString(Pos.CW01R_USER_ID, Len.CW01R_USER_ID);
	}

	public void setCw01rStatusCdCi(char cw01rStatusCdCi) {
		writeChar(Pos.CW01R_STATUS_CD_CI, cw01rStatusCdCi);
	}

	/**Original name: CW01R-STATUS-CD-CI<br>*/
	public char getCw01rStatusCdCi() {
		return readChar(Pos.CW01R_STATUS_CD_CI);
	}

	public void setCw01rStatusCd(char cw01rStatusCd) {
		writeChar(Pos.CW01R_STATUS_CD, cw01rStatusCd);
	}

	/**Original name: CW01R-STATUS-CD<br>*/
	public char getCw01rStatusCd() {
		return readChar(Pos.CW01R_STATUS_CD);
	}

	public void setCw01rTerminalIdCi(char cw01rTerminalIdCi) {
		writeChar(Pos.CW01R_TERMINAL_ID_CI, cw01rTerminalIdCi);
	}

	/**Original name: CW01R-TERMINAL-ID-CI<br>*/
	public char getCw01rTerminalIdCi() {
		return readChar(Pos.CW01R_TERMINAL_ID_CI);
	}

	public void setCw01rTerminalId(String cw01rTerminalId) {
		writeString(Pos.CW01R_TERMINAL_ID, cw01rTerminalId, Len.CW01R_TERMINAL_ID);
	}

	/**Original name: CW01R-TERMINAL-ID<br>*/
	public String getCw01rTerminalId() {
		return readString(Pos.CW01R_TERMINAL_ID, Len.CW01R_TERMINAL_ID);
	}

	public void setCw01rCibcEffAcyTsCi(char cw01rCibcEffAcyTsCi) {
		writeChar(Pos.CW01R_CIBC_EFF_ACY_TS_CI, cw01rCibcEffAcyTsCi);
	}

	/**Original name: CW01R-CIBC-EFF-ACY-TS-CI<br>*/
	public char getCw01rCibcEffAcyTsCi() {
		return readChar(Pos.CW01R_CIBC_EFF_ACY_TS_CI);
	}

	public void setCw01rCibcEffAcyTs(String cw01rCibcEffAcyTs) {
		writeString(Pos.CW01R_CIBC_EFF_ACY_TS, cw01rCibcEffAcyTs, Len.CW01R_CIBC_EFF_ACY_TS);
	}

	/**Original name: CW01R-CIBC-EFF-ACY-TS<br>*/
	public String getCw01rCibcEffAcyTs() {
		return readString(Pos.CW01R_CIBC_EFF_ACY_TS, Len.CW01R_CIBC_EFF_ACY_TS);
	}

	public void setCw01rCibcExpAcyTsCi(char cw01rCibcExpAcyTsCi) {
		writeChar(Pos.CW01R_CIBC_EXP_ACY_TS_CI, cw01rCibcExpAcyTsCi);
	}

	/**Original name: CW01R-CIBC-EXP-ACY-TS-CI<br>*/
	public char getCw01rCibcExpAcyTsCi() {
		return readChar(Pos.CW01R_CIBC_EXP_ACY_TS_CI);
	}

	public void setCw01rCibcExpAcyTs(String cw01rCibcExpAcyTs) {
		writeString(Pos.CW01R_CIBC_EXP_ACY_TS, cw01rCibcExpAcyTs, Len.CW01R_CIBC_EXP_ACY_TS);
	}

	/**Original name: CW01R-CIBC-EXP-ACY-TS<br>*/
	public String getCw01rCibcExpAcyTs() {
		return readString(Pos.CW01R_CIBC_EXP_ACY_TS, Len.CW01R_CIBC_EXP_ACY_TS);
	}

	public String getlFwRespRelCltObjRelFormatted() {
		return readFixedString(Pos.L_FW_RESP_REL_CLT_OBJ_REL, Len.L_FW_RESP_REL_CLT_OBJ_REL);
	}

	public void setCworcCltObjRelationCsumFormatted(String cworcCltObjRelationCsum) {
		writeString(Pos.CWORC_CLT_OBJ_RELATION_CSUM, Trunc.toUnsignedNumeric(cworcCltObjRelationCsum, Len.CWORC_CLT_OBJ_RELATION_CSUM),
				Len.CWORC_CLT_OBJ_RELATION_CSUM);
	}

	/**Original name: CWORC-CLT-OBJ-RELATION-CSUM<br>*/
	public int getCworcCltObjRelationCsum() {
		return readNumDispUnsignedInt(Pos.CWORC_CLT_OBJ_RELATION_CSUM, Len.CWORC_CLT_OBJ_RELATION_CSUM);
	}

	public void setCworcTchObjectKeyKcre(String cworcTchObjectKeyKcre) {
		writeString(Pos.CWORC_TCH_OBJECT_KEY_KCRE, cworcTchObjectKeyKcre, Len.CWORC_TCH_OBJECT_KEY_KCRE);
	}

	/**Original name: CWORC-TCH-OBJECT-KEY-KCRE<br>*/
	public String getCworcTchObjectKeyKcre() {
		return readString(Pos.CWORC_TCH_OBJECT_KEY_KCRE, Len.CWORC_TCH_OBJECT_KEY_KCRE);
	}

	public void setCworcHistoryVldNbrKcre(String cworcHistoryVldNbrKcre) {
		writeString(Pos.CWORC_HISTORY_VLD_NBR_KCRE, cworcHistoryVldNbrKcre, Len.CWORC_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CWORC-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCworcHistoryVldNbrKcre() {
		return readString(Pos.CWORC_HISTORY_VLD_NBR_KCRE, Len.CWORC_HISTORY_VLD_NBR_KCRE);
	}

	public void setCworcCiorEffDtKcre(String cworcCiorEffDtKcre) {
		writeString(Pos.CWORC_CIOR_EFF_DT_KCRE, cworcCiorEffDtKcre, Len.CWORC_CIOR_EFF_DT_KCRE);
	}

	/**Original name: CWORC-CIOR-EFF-DT-KCRE<br>*/
	public String getCworcCiorEffDtKcre() {
		return readString(Pos.CWORC_CIOR_EFF_DT_KCRE, Len.CWORC_CIOR_EFF_DT_KCRE);
	}

	public void setCworcObjSysIdKcre(String cworcObjSysIdKcre) {
		writeString(Pos.CWORC_OBJ_SYS_ID_KCRE, cworcObjSysIdKcre, Len.CWORC_OBJ_SYS_ID_KCRE);
	}

	/**Original name: CWORC-OBJ-SYS-ID-KCRE<br>*/
	public String getCworcObjSysIdKcre() {
		return readString(Pos.CWORC_OBJ_SYS_ID_KCRE, Len.CWORC_OBJ_SYS_ID_KCRE);
	}

	public void setCworcCiorObjSeqNbrKcre(String cworcCiorObjSeqNbrKcre) {
		writeString(Pos.CWORC_CIOR_OBJ_SEQ_NBR_KCRE, cworcCiorObjSeqNbrKcre, Len.CWORC_CIOR_OBJ_SEQ_NBR_KCRE);
	}

	/**Original name: CWORC-CIOR-OBJ-SEQ-NBR-KCRE<br>*/
	public String getCworcCiorObjSeqNbrKcre() {
		return readString(Pos.CWORC_CIOR_OBJ_SEQ_NBR_KCRE, Len.CWORC_CIOR_OBJ_SEQ_NBR_KCRE);
	}

	public void setCworcClientIdKcre(String cworcClientIdKcre) {
		writeString(Pos.CWORC_CLIENT_ID_KCRE, cworcClientIdKcre, Len.CWORC_CLIENT_ID_KCRE);
	}

	/**Original name: CWORC-CLIENT-ID-KCRE<br>*/
	public String getCworcClientIdKcre() {
		return readString(Pos.CWORC_CLIENT_ID_KCRE, Len.CWORC_CLIENT_ID_KCRE);
	}

	public void setCworcTransProcessDt(String cworcTransProcessDt) {
		writeString(Pos.CWORC_TRANS_PROCESS_DT, cworcTransProcessDt, Len.CWORC_TRANS_PROCESS_DT);
	}

	/**Original name: CWORC-TRANS-PROCESS-DT<br>*/
	public String getCworcTransProcessDt() {
		return readString(Pos.CWORC_TRANS_PROCESS_DT, Len.CWORC_TRANS_PROCESS_DT);
	}

	public void setCworcTchObjectKeyCi(char cworcTchObjectKeyCi) {
		writeChar(Pos.CWORC_TCH_OBJECT_KEY_CI, cworcTchObjectKeyCi);
	}

	/**Original name: CWORC-TCH-OBJECT-KEY-CI<br>*/
	public char getCworcTchObjectKeyCi() {
		return readChar(Pos.CWORC_TCH_OBJECT_KEY_CI);
	}

	public void setCworcTchObjectKey(String cworcTchObjectKey) {
		writeString(Pos.CWORC_TCH_OBJECT_KEY, cworcTchObjectKey, Len.CWORC_TCH_OBJECT_KEY);
	}

	/**Original name: CWORC-TCH-OBJECT-KEY<br>*/
	public String getCworcTchObjectKey() {
		return readString(Pos.CWORC_TCH_OBJECT_KEY, Len.CWORC_TCH_OBJECT_KEY);
	}

	public void setCworcHistoryVldNbrCi(char cworcHistoryVldNbrCi) {
		writeChar(Pos.CWORC_HISTORY_VLD_NBR_CI, cworcHistoryVldNbrCi);
	}

	/**Original name: CWORC-HISTORY-VLD-NBR-CI<br>*/
	public char getCworcHistoryVldNbrCi() {
		return readChar(Pos.CWORC_HISTORY_VLD_NBR_CI);
	}

	public void setCworcHistoryVldNbrSign(char cworcHistoryVldNbrSign) {
		writeChar(Pos.CWORC_HISTORY_VLD_NBR_SIGN, cworcHistoryVldNbrSign);
	}

	/**Original name: CWORC-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCworcHistoryVldNbrSign() {
		return readChar(Pos.CWORC_HISTORY_VLD_NBR_SIGN);
	}

	public void setCworcHistoryVldNbrFormatted(String cworcHistoryVldNbr) {
		writeString(Pos.CWORC_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cworcHistoryVldNbr, Len.CWORC_HISTORY_VLD_NBR), Len.CWORC_HISTORY_VLD_NBR);
	}

	/**Original name: CWORC-HISTORY-VLD-NBR<br>*/
	public int getCworcHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CWORC_HISTORY_VLD_NBR, Len.CWORC_HISTORY_VLD_NBR);
	}

	public void setCworcCiorEffDtCi(char cworcCiorEffDtCi) {
		writeChar(Pos.CWORC_CIOR_EFF_DT_CI, cworcCiorEffDtCi);
	}

	/**Original name: CWORC-CIOR-EFF-DT-CI<br>*/
	public char getCworcCiorEffDtCi() {
		return readChar(Pos.CWORC_CIOR_EFF_DT_CI);
	}

	public void setCworcCiorEffDt(String cworcCiorEffDt) {
		writeString(Pos.CWORC_CIOR_EFF_DT, cworcCiorEffDt, Len.CWORC_CIOR_EFF_DT);
	}

	/**Original name: CWORC-CIOR-EFF-DT<br>*/
	public String getCworcCiorEffDt() {
		return readString(Pos.CWORC_CIOR_EFF_DT, Len.CWORC_CIOR_EFF_DT);
	}

	public void setCworcObjSysIdCi(char cworcObjSysIdCi) {
		writeChar(Pos.CWORC_OBJ_SYS_ID_CI, cworcObjSysIdCi);
	}

	/**Original name: CWORC-OBJ-SYS-ID-CI<br>*/
	public char getCworcObjSysIdCi() {
		return readChar(Pos.CWORC_OBJ_SYS_ID_CI);
	}

	public void setCworcObjSysId(String cworcObjSysId) {
		writeString(Pos.CWORC_OBJ_SYS_ID, cworcObjSysId, Len.CWORC_OBJ_SYS_ID);
	}

	/**Original name: CWORC-OBJ-SYS-ID<br>*/
	public String getCworcObjSysId() {
		return readString(Pos.CWORC_OBJ_SYS_ID, Len.CWORC_OBJ_SYS_ID);
	}

	public void setCworcCiorObjSeqNbrCi(char cworcCiorObjSeqNbrCi) {
		writeChar(Pos.CWORC_CIOR_OBJ_SEQ_NBR_CI, cworcCiorObjSeqNbrCi);
	}

	/**Original name: CWORC-CIOR-OBJ-SEQ-NBR-CI<br>*/
	public char getCworcCiorObjSeqNbrCi() {
		return readChar(Pos.CWORC_CIOR_OBJ_SEQ_NBR_CI);
	}

	public void setCworcCiorObjSeqNbrSign(char cworcCiorObjSeqNbrSign) {
		writeChar(Pos.CWORC_CIOR_OBJ_SEQ_NBR_SIGN, cworcCiorObjSeqNbrSign);
	}

	/**Original name: CWORC-CIOR-OBJ-SEQ-NBR-SIGN<br>*/
	public char getCworcCiorObjSeqNbrSign() {
		return readChar(Pos.CWORC_CIOR_OBJ_SEQ_NBR_SIGN);
	}

	public void setCworcCiorObjSeqNbrFormatted(String cworcCiorObjSeqNbr) {
		writeString(Pos.CWORC_CIOR_OBJ_SEQ_NBR, Trunc.toUnsignedNumeric(cworcCiorObjSeqNbr, Len.CWORC_CIOR_OBJ_SEQ_NBR), Len.CWORC_CIOR_OBJ_SEQ_NBR);
	}

	/**Original name: CWORC-CIOR-OBJ-SEQ-NBR<br>*/
	public int getCworcCiorObjSeqNbr() {
		return readNumDispUnsignedInt(Pos.CWORC_CIOR_OBJ_SEQ_NBR, Len.CWORC_CIOR_OBJ_SEQ_NBR);
	}

	public void setCworcClientIdCi(char cworcClientIdCi) {
		writeChar(Pos.CWORC_CLIENT_ID_CI, cworcClientIdCi);
	}

	/**Original name: CWORC-CLIENT-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCworcClientIdCi() {
		return readChar(Pos.CWORC_CLIENT_ID_CI);
	}

	public void setCworcClientId(String cworcClientId) {
		writeString(Pos.CWORC_CLIENT_ID, cworcClientId, Len.CWORC_CLIENT_ID);
	}

	/**Original name: CWORC-CLIENT-ID<br>*/
	public String getCworcClientId() {
		return readString(Pos.CWORC_CLIENT_ID, Len.CWORC_CLIENT_ID);
	}

	public void setCworcRltTypCdCi(char cworcRltTypCdCi) {
		writeChar(Pos.CWORC_RLT_TYP_CD_CI, cworcRltTypCdCi);
	}

	/**Original name: CWORC-RLT-TYP-CD-CI<br>*/
	public char getCworcRltTypCdCi() {
		return readChar(Pos.CWORC_RLT_TYP_CD_CI);
	}

	public void setCworcRltTypCd(String cworcRltTypCd) {
		writeString(Pos.CWORC_RLT_TYP_CD, cworcRltTypCd, Len.CWORC_RLT_TYP_CD);
	}

	/**Original name: CWORC-RLT-TYP-CD<br>*/
	public String getCworcRltTypCd() {
		return readString(Pos.CWORC_RLT_TYP_CD, Len.CWORC_RLT_TYP_CD);
	}

	public void setCworcObjCdCi(char cworcObjCdCi) {
		writeChar(Pos.CWORC_OBJ_CD_CI, cworcObjCdCi);
	}

	/**Original name: CWORC-OBJ-CD-CI<br>*/
	public char getCworcObjCdCi() {
		return readChar(Pos.CWORC_OBJ_CD_CI);
	}

	public void setCworcObjCd(String cworcObjCd) {
		writeString(Pos.CWORC_OBJ_CD, cworcObjCd, Len.CWORC_OBJ_CD);
	}

	/**Original name: CWORC-OBJ-CD<br>*/
	public String getCworcObjCd() {
		return readString(Pos.CWORC_OBJ_CD, Len.CWORC_OBJ_CD);
	}

	public void setCworcCiorShwObjKeyCi(char cworcCiorShwObjKeyCi) {
		writeChar(Pos.CWORC_CIOR_SHW_OBJ_KEY_CI, cworcCiorShwObjKeyCi);
	}

	/**Original name: CWORC-CIOR-SHW-OBJ-KEY-CI<br>*/
	public char getCworcCiorShwObjKeyCi() {
		return readChar(Pos.CWORC_CIOR_SHW_OBJ_KEY_CI);
	}

	public void setCworcCiorShwObjKey(String cworcCiorShwObjKey) {
		writeString(Pos.CWORC_CIOR_SHW_OBJ_KEY, cworcCiorShwObjKey, Len.CWORC_CIOR_SHW_OBJ_KEY);
	}

	/**Original name: CWORC-CIOR-SHW-OBJ-KEY<br>*/
	public String getCworcCiorShwObjKey() {
		return readString(Pos.CWORC_CIOR_SHW_OBJ_KEY, Len.CWORC_CIOR_SHW_OBJ_KEY);
	}

	public void setCworcAdrSeqNbrCi(char cworcAdrSeqNbrCi) {
		writeChar(Pos.CWORC_ADR_SEQ_NBR_CI, cworcAdrSeqNbrCi);
	}

	/**Original name: CWORC-ADR-SEQ-NBR-CI<br>*/
	public char getCworcAdrSeqNbrCi() {
		return readChar(Pos.CWORC_ADR_SEQ_NBR_CI);
	}

	public void setCworcAdrSeqNbrSign(char cworcAdrSeqNbrSign) {
		writeChar(Pos.CWORC_ADR_SEQ_NBR_SIGN, cworcAdrSeqNbrSign);
	}

	/**Original name: CWORC-ADR-SEQ-NBR-SIGN<br>*/
	public char getCworcAdrSeqNbrSign() {
		return readChar(Pos.CWORC_ADR_SEQ_NBR_SIGN);
	}

	public void setCworcAdrSeqNbrFormatted(String cworcAdrSeqNbr) {
		writeString(Pos.CWORC_ADR_SEQ_NBR, Trunc.toUnsignedNumeric(cworcAdrSeqNbr, Len.CWORC_ADR_SEQ_NBR), Len.CWORC_ADR_SEQ_NBR);
	}

	/**Original name: CWORC-ADR-SEQ-NBR<br>*/
	public int getCworcAdrSeqNbr() {
		return readNumDispUnsignedInt(Pos.CWORC_ADR_SEQ_NBR, Len.CWORC_ADR_SEQ_NBR);
	}

	public void setCworcUserIdCi(char cworcUserIdCi) {
		writeChar(Pos.CWORC_USER_ID_CI, cworcUserIdCi);
	}

	/**Original name: CWORC-USER-ID-CI<br>*/
	public char getCworcUserIdCi() {
		return readChar(Pos.CWORC_USER_ID_CI);
	}

	public void setCworcUserId(String cworcUserId) {
		writeString(Pos.CWORC_USER_ID, cworcUserId, Len.CWORC_USER_ID);
	}

	/**Original name: CWORC-USER-ID<br>*/
	public String getCworcUserId() {
		return readString(Pos.CWORC_USER_ID, Len.CWORC_USER_ID);
	}

	public void setCworcStatusCdCi(char cworcStatusCdCi) {
		writeChar(Pos.CWORC_STATUS_CD_CI, cworcStatusCdCi);
	}

	/**Original name: CWORC-STATUS-CD-CI<br>*/
	public char getCworcStatusCdCi() {
		return readChar(Pos.CWORC_STATUS_CD_CI);
	}

	public void setCworcStatusCd(char cworcStatusCd) {
		writeChar(Pos.CWORC_STATUS_CD, cworcStatusCd);
	}

	/**Original name: CWORC-STATUS-CD<br>*/
	public char getCworcStatusCd() {
		return readChar(Pos.CWORC_STATUS_CD);
	}

	public void setCworcTerminalIdCi(char cworcTerminalIdCi) {
		writeChar(Pos.CWORC_TERMINAL_ID_CI, cworcTerminalIdCi);
	}

	/**Original name: CWORC-TERMINAL-ID-CI<br>*/
	public char getCworcTerminalIdCi() {
		return readChar(Pos.CWORC_TERMINAL_ID_CI);
	}

	public void setCworcTerminalId(String cworcTerminalId) {
		writeString(Pos.CWORC_TERMINAL_ID, cworcTerminalId, Len.CWORC_TERMINAL_ID);
	}

	/**Original name: CWORC-TERMINAL-ID<br>*/
	public String getCworcTerminalId() {
		return readString(Pos.CWORC_TERMINAL_ID, Len.CWORC_TERMINAL_ID);
	}

	public void setCworcCiorExpDtCi(char cworcCiorExpDtCi) {
		writeChar(Pos.CWORC_CIOR_EXP_DT_CI, cworcCiorExpDtCi);
	}

	/**Original name: CWORC-CIOR-EXP-DT-CI<br>*/
	public char getCworcCiorExpDtCi() {
		return readChar(Pos.CWORC_CIOR_EXP_DT_CI);
	}

	public void setCworcCiorExpDt(String cworcCiorExpDt) {
		writeString(Pos.CWORC_CIOR_EXP_DT, cworcCiorExpDt, Len.CWORC_CIOR_EXP_DT);
	}

	/**Original name: CWORC-CIOR-EXP-DT<br>*/
	public String getCworcCiorExpDt() {
		return readString(Pos.CWORC_CIOR_EXP_DT, Len.CWORC_CIOR_EXP_DT);
	}

	public void setCworcCiorEffAcyTsCi(char cworcCiorEffAcyTsCi) {
		writeChar(Pos.CWORC_CIOR_EFF_ACY_TS_CI, cworcCiorEffAcyTsCi);
	}

	/**Original name: CWORC-CIOR-EFF-ACY-TS-CI<br>*/
	public char getCworcCiorEffAcyTsCi() {
		return readChar(Pos.CWORC_CIOR_EFF_ACY_TS_CI);
	}

	public void setCworcCiorEffAcyTs(String cworcCiorEffAcyTs) {
		writeString(Pos.CWORC_CIOR_EFF_ACY_TS, cworcCiorEffAcyTs, Len.CWORC_CIOR_EFF_ACY_TS);
	}

	/**Original name: CWORC-CIOR-EFF-ACY-TS<br>*/
	public String getCworcCiorEffAcyTs() {
		return readString(Pos.CWORC_CIOR_EFF_ACY_TS, Len.CWORC_CIOR_EFF_ACY_TS);
	}

	public void setCworcCiorExpAcyTsCi(char cworcCiorExpAcyTsCi) {
		writeChar(Pos.CWORC_CIOR_EXP_ACY_TS_CI, cworcCiorExpAcyTsCi);
	}

	/**Original name: CWORC-CIOR-EXP-ACY-TS-CI<br>*/
	public char getCworcCiorExpAcyTsCi() {
		return readChar(Pos.CWORC_CIOR_EXP_ACY_TS_CI);
	}

	public void setCworcCiorExpAcyTs(String cworcCiorExpAcyTs) {
		writeString(Pos.CWORC_CIOR_EXP_ACY_TS, cworcCiorExpAcyTs, Len.CWORC_CIOR_EXP_ACY_TS);
	}

	/**Original name: CWORC-CIOR-EXP-ACY-TS<br>*/
	public String getCworcCiorExpAcyTs() {
		return readString(Pos.CWORC_CIOR_EXP_ACY_TS, Len.CWORC_CIOR_EXP_ACY_TS);
	}

	public void setCworcCiorLegEntCdCi(char cworcCiorLegEntCdCi) {
		writeChar(Pos.CWORC_CIOR_LEG_ENT_CD_CI, cworcCiorLegEntCdCi);
	}

	/**Original name: CWORC-CIOR-LEG-ENT-CD-CI<br>*/
	public char getCworcCiorLegEntCdCi() {
		return readChar(Pos.CWORC_CIOR_LEG_ENT_CD_CI);
	}

	public void setCworcCiorLegEntCd(String cworcCiorLegEntCd) {
		writeString(Pos.CWORC_CIOR_LEG_ENT_CD, cworcCiorLegEntCd, Len.CWORC_CIOR_LEG_ENT_CD);
	}

	/**Original name: CWORC-CIOR-LEG-ENT-CD<br>*/
	public String getCworcCiorLegEntCd() {
		return readString(Pos.CWORC_CIOR_LEG_ENT_CD, Len.CWORC_CIOR_LEG_ENT_CD);
	}

	public void setCworcCiorFstNmCi(char cworcCiorFstNmCi) {
		writeChar(Pos.CWORC_CIOR_FST_NM_CI, cworcCiorFstNmCi);
	}

	/**Original name: CWORC-CIOR-FST-NM-CI<br>*/
	public char getCworcCiorFstNmCi() {
		return readChar(Pos.CWORC_CIOR_FST_NM_CI);
	}

	public void setCworcCiorFstNm(String cworcCiorFstNm) {
		writeString(Pos.CWORC_CIOR_FST_NM, cworcCiorFstNm, Len.CWORC_CIOR_FST_NM);
	}

	/**Original name: CWORC-CIOR-FST-NM<br>*/
	public String getCworcCiorFstNm() {
		return readString(Pos.CWORC_CIOR_FST_NM, Len.CWORC_CIOR_FST_NM);
	}

	public void setCworcCiorLstNmCi(char cworcCiorLstNmCi) {
		writeChar(Pos.CWORC_CIOR_LST_NM_CI, cworcCiorLstNmCi);
	}

	/**Original name: CWORC-CIOR-LST-NM-CI<br>*/
	public char getCworcCiorLstNmCi() {
		return readChar(Pos.CWORC_CIOR_LST_NM_CI);
	}

	public void setCworcCiorLstNm(String cworcCiorLstNm) {
		writeString(Pos.CWORC_CIOR_LST_NM, cworcCiorLstNm, Len.CWORC_CIOR_LST_NM);
	}

	/**Original name: CWORC-CIOR-LST-NM<br>*/
	public String getCworcCiorLstNm() {
		return readString(Pos.CWORC_CIOR_LST_NM, Len.CWORC_CIOR_LST_NM);
	}

	public void setCworcCiorMdlNmCi(char cworcCiorMdlNmCi) {
		writeChar(Pos.CWORC_CIOR_MDL_NM_CI, cworcCiorMdlNmCi);
	}

	/**Original name: CWORC-CIOR-MDL-NM-CI<br>*/
	public char getCworcCiorMdlNmCi() {
		return readChar(Pos.CWORC_CIOR_MDL_NM_CI);
	}

	public void setCworcCiorMdlNm(String cworcCiorMdlNm) {
		writeString(Pos.CWORC_CIOR_MDL_NM, cworcCiorMdlNm, Len.CWORC_CIOR_MDL_NM);
	}

	/**Original name: CWORC-CIOR-MDL-NM<br>*/
	public String getCworcCiorMdlNm() {
		return readString(Pos.CWORC_CIOR_MDL_NM, Len.CWORC_CIOR_MDL_NM);
	}

	public void setCworcCiorNmPfxCi(char cworcCiorNmPfxCi) {
		writeChar(Pos.CWORC_CIOR_NM_PFX_CI, cworcCiorNmPfxCi);
	}

	/**Original name: CWORC-CIOR-NM-PFX-CI<br>*/
	public char getCworcCiorNmPfxCi() {
		return readChar(Pos.CWORC_CIOR_NM_PFX_CI);
	}

	public void setCworcCiorNmPfx(String cworcCiorNmPfx) {
		writeString(Pos.CWORC_CIOR_NM_PFX, cworcCiorNmPfx, Len.CWORC_CIOR_NM_PFX);
	}

	/**Original name: CWORC-CIOR-NM-PFX<br>*/
	public String getCworcCiorNmPfx() {
		return readString(Pos.CWORC_CIOR_NM_PFX, Len.CWORC_CIOR_NM_PFX);
	}

	public void setCworcCiorNmSfxCi(char cworcCiorNmSfxCi) {
		writeChar(Pos.CWORC_CIOR_NM_SFX_CI, cworcCiorNmSfxCi);
	}

	/**Original name: CWORC-CIOR-NM-SFX-CI<br>*/
	public char getCworcCiorNmSfxCi() {
		return readChar(Pos.CWORC_CIOR_NM_SFX_CI);
	}

	public void setCworcCiorNmSfx(String cworcCiorNmSfx) {
		writeString(Pos.CWORC_CIOR_NM_SFX, cworcCiorNmSfx, Len.CWORC_CIOR_NM_SFX);
	}

	/**Original name: CWORC-CIOR-NM-SFX<br>*/
	public String getCworcCiorNmSfx() {
		return readString(Pos.CWORC_CIOR_NM_SFX, Len.CWORC_CIOR_NM_SFX);
	}

	public void setCworcCiorLngNmCi(char cworcCiorLngNmCi) {
		writeChar(Pos.CWORC_CIOR_LNG_NM_CI, cworcCiorLngNmCi);
	}

	/**Original name: CWORC-CIOR-LNG-NM-CI<br>*/
	public char getCworcCiorLngNmCi() {
		return readChar(Pos.CWORC_CIOR_LNG_NM_CI);
	}

	public void setCworcCiorLngNmNi(char cworcCiorLngNmNi) {
		writeChar(Pos.CWORC_CIOR_LNG_NM_NI, cworcCiorLngNmNi);
	}

	/**Original name: CWORC-CIOR-LNG-NM-NI<br>*/
	public char getCworcCiorLngNmNi() {
		return readChar(Pos.CWORC_CIOR_LNG_NM_NI);
	}

	public void setCworcCiorLngNm(String cworcCiorLngNm) {
		writeString(Pos.CWORC_CIOR_LNG_NM, cworcCiorLngNm, Len.CWORC_CIOR_LNG_NM);
	}

	/**Original name: CWORC-CIOR-LNG-NM<br>*/
	public String getCworcCiorLngNm() {
		return readString(Pos.CWORC_CIOR_LNG_NM, Len.CWORC_CIOR_LNG_NM);
	}

	public void setCworcFilterType(String cworcFilterType) {
		writeString(Pos.CWORC_FILTER_TYPE, cworcFilterType, Len.CWORC_FILTER_TYPE);
	}

	/**Original name: CWORC-FILTER-TYPE<br>
	 * <pre>*  FIELDS AS INPUT TO BPO:</pre>*/
	public String getCworcFilterType() {
		return readString(Pos.CWORC_FILTER_TYPE, Len.CWORC_FILTER_TYPE);
	}

	public String getlFwRespCltCltRelFormatted(int lFwRespCltCltRelIdx) {
		int position = Pos.lFwRespCltCltRel(lFwRespCltCltRelIdx - 1);
		return readFixedString(position, Len.L_FW_RESP_CLT_CLT_REL);
	}

	public void setCw06rCltCltRelationCsumFormatted(int cw06rCltCltRelationCsumIdx, String cw06rCltCltRelationCsum) {
		int position = Pos.cw06rCltCltRelationCsum(cw06rCltCltRelationCsumIdx - 1);
		writeString(position, Trunc.toUnsignedNumeric(cw06rCltCltRelationCsum, Len.CW06R_CLT_CLT_RELATION_CSUM), Len.CW06R_CLT_CLT_RELATION_CSUM);
	}

	/**Original name: CW06R-CLT-CLT-RELATION-CSUM<br>*/
	public int getCw06rCltCltRelationCsum(int cw06rCltCltRelationCsumIdx) {
		int position = Pos.cw06rCltCltRelationCsum(cw06rCltCltRelationCsumIdx - 1);
		return readNumDispUnsignedInt(position, Len.CW06R_CLT_CLT_RELATION_CSUM);
	}

	public void setCw06rClientIdKcre(int cw06rClientIdKcreIdx, String cw06rClientIdKcre) {
		int position = Pos.cw06rClientIdKcre(cw06rClientIdKcreIdx - 1);
		writeString(position, cw06rClientIdKcre, Len.CW06R_CLIENT_ID_KCRE);
	}

	/**Original name: CW06R-CLIENT-ID-KCRE<br>*/
	public String getCw06rClientIdKcre(int cw06rClientIdKcreIdx) {
		int position = Pos.cw06rClientIdKcre(cw06rClientIdKcreIdx - 1);
		return readString(position, Len.CW06R_CLIENT_ID_KCRE);
	}

	public void setCw06rCltTypCdKcre(int cw06rCltTypCdKcreIdx, String cw06rCltTypCdKcre) {
		int position = Pos.cw06rCltTypCdKcre(cw06rCltTypCdKcreIdx - 1);
		writeString(position, cw06rCltTypCdKcre, Len.CW06R_CLT_TYP_CD_KCRE);
	}

	/**Original name: CW06R-CLT-TYP-CD-KCRE<br>*/
	public String getCw06rCltTypCdKcre(int cw06rCltTypCdKcreIdx) {
		int position = Pos.cw06rCltTypCdKcre(cw06rCltTypCdKcreIdx - 1);
		return readString(position, Len.CW06R_CLT_TYP_CD_KCRE);
	}

	public void setCw06rHistoryVldNbrKcre(int cw06rHistoryVldNbrKcreIdx, String cw06rHistoryVldNbrKcre) {
		int position = Pos.cw06rHistoryVldNbrKcre(cw06rHistoryVldNbrKcreIdx - 1);
		writeString(position, cw06rHistoryVldNbrKcre, Len.CW06R_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CW06R-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCw06rHistoryVldNbrKcre(int cw06rHistoryVldNbrKcreIdx) {
		int position = Pos.cw06rHistoryVldNbrKcre(cw06rHistoryVldNbrKcreIdx - 1);
		return readString(position, Len.CW06R_HISTORY_VLD_NBR_KCRE);
	}

	public void setCw06rCicrXrfIdKcre(int cw06rCicrXrfIdKcreIdx, String cw06rCicrXrfIdKcre) {
		int position = Pos.cw06rCicrXrfIdKcre(cw06rCicrXrfIdKcreIdx - 1);
		writeString(position, cw06rCicrXrfIdKcre, Len.CW06R_CICR_XRF_ID_KCRE);
	}

	/**Original name: CW06R-CICR-XRF-ID-KCRE<br>*/
	public String getCw06rCicrXrfIdKcre(int cw06rCicrXrfIdKcreIdx) {
		int position = Pos.cw06rCicrXrfIdKcre(cw06rCicrXrfIdKcreIdx - 1);
		return readString(position, Len.CW06R_CICR_XRF_ID_KCRE);
	}

	public void setCw06rXrfTypCdKcre(int cw06rXrfTypCdKcreIdx, String cw06rXrfTypCdKcre) {
		int position = Pos.cw06rXrfTypCdKcre(cw06rXrfTypCdKcreIdx - 1);
		writeString(position, cw06rXrfTypCdKcre, Len.CW06R_XRF_TYP_CD_KCRE);
	}

	/**Original name: CW06R-XRF-TYP-CD-KCRE<br>*/
	public String getCw06rXrfTypCdKcre(int cw06rXrfTypCdKcreIdx) {
		int position = Pos.cw06rXrfTypCdKcre(cw06rXrfTypCdKcreIdx - 1);
		return readString(position, Len.CW06R_XRF_TYP_CD_KCRE);
	}

	public void setCw06rCicrEffDtKcre(int cw06rCicrEffDtKcreIdx, String cw06rCicrEffDtKcre) {
		int position = Pos.cw06rCicrEffDtKcre(cw06rCicrEffDtKcreIdx - 1);
		writeString(position, cw06rCicrEffDtKcre, Len.CW06R_CICR_EFF_DT_KCRE);
	}

	/**Original name: CW06R-CICR-EFF-DT-KCRE<br>*/
	public String getCw06rCicrEffDtKcre(int cw06rCicrEffDtKcreIdx) {
		int position = Pos.cw06rCicrEffDtKcre(cw06rCicrEffDtKcreIdx - 1);
		return readString(position, Len.CW06R_CICR_EFF_DT_KCRE);
	}

	public void setCw06rTransProcessDt(int cw06rTransProcessDtIdx, String cw06rTransProcessDt) {
		int position = Pos.cw06rTransProcessDt(cw06rTransProcessDtIdx - 1);
		writeString(position, cw06rTransProcessDt, Len.CW06R_TRANS_PROCESS_DT);
	}

	/**Original name: CW06R-TRANS-PROCESS-DT<br>*/
	public String getCw06rTransProcessDt(int cw06rTransProcessDtIdx) {
		int position = Pos.cw06rTransProcessDt(cw06rTransProcessDtIdx - 1);
		return readString(position, Len.CW06R_TRANS_PROCESS_DT);
	}

	public void setCw06rClientIdCi(int cw06rClientIdCiIdx, char cw06rClientIdCi) {
		int position = Pos.cw06rClientIdCi(cw06rClientIdCiIdx - 1);
		writeChar(position, cw06rClientIdCi);
	}

	/**Original name: CW06R-CLIENT-ID-CI<br>*/
	public char getCw06rClientIdCi(int cw06rClientIdCiIdx) {
		int position = Pos.cw06rClientIdCi(cw06rClientIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rClientId(int cw06rClientIdIdx, String cw06rClientId) {
		int position = Pos.cw06rClientId(cw06rClientIdIdx - 1);
		writeString(position, cw06rClientId, Len.CW06R_CLIENT_ID);
	}

	/**Original name: CW06R-CLIENT-ID<br>*/
	public String getCw06rClientId(int cw06rClientIdIdx) {
		int position = Pos.cw06rClientId(cw06rClientIdIdx - 1);
		return readString(position, Len.CW06R_CLIENT_ID);
	}

	public void setCw06rCltTypCdCi(int cw06rCltTypCdCiIdx, char cw06rCltTypCdCi) {
		int position = Pos.cw06rCltTypCdCi(cw06rCltTypCdCiIdx - 1);
		writeChar(position, cw06rCltTypCdCi);
	}

	/**Original name: CW06R-CLT-TYP-CD-CI<br>*/
	public char getCw06rCltTypCdCi(int cw06rCltTypCdCiIdx) {
		int position = Pos.cw06rCltTypCdCi(cw06rCltTypCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rCltTypCd(int cw06rCltTypCdIdx, String cw06rCltTypCd) {
		int position = Pos.cw06rCltTypCd(cw06rCltTypCdIdx - 1);
		writeString(position, cw06rCltTypCd, Len.CW06R_CLT_TYP_CD);
	}

	/**Original name: CW06R-CLT-TYP-CD<br>*/
	public String getCw06rCltTypCd(int cw06rCltTypCdIdx) {
		int position = Pos.cw06rCltTypCd(cw06rCltTypCdIdx - 1);
		return readString(position, Len.CW06R_CLT_TYP_CD);
	}

	public void setCw06rHistoryVldNbrCi(int cw06rHistoryVldNbrCiIdx, char cw06rHistoryVldNbrCi) {
		int position = Pos.cw06rHistoryVldNbrCi(cw06rHistoryVldNbrCiIdx - 1);
		writeChar(position, cw06rHistoryVldNbrCi);
	}

	/**Original name: CW06R-HISTORY-VLD-NBR-CI<br>*/
	public char getCw06rHistoryVldNbrCi(int cw06rHistoryVldNbrCiIdx) {
		int position = Pos.cw06rHistoryVldNbrCi(cw06rHistoryVldNbrCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rHistoryVldNbrSignedFormatted(int cw06rHistoryVldNbrSignedIdx, String cw06rHistoryVldNbrSigned) {
		int position = Pos.cw06rHistoryVldNbrSigned(cw06rHistoryVldNbrSignedIdx - 1);
		writeString(position, PicFormatter.display("-9(5)").format(cw06rHistoryVldNbrSigned).toString(), Len.CW06R_HISTORY_VLD_NBR_SIGNED);
	}

	/**Original name: CW06R-HISTORY-VLD-NBR-SIGNED<br>*/
	public long getCw06rHistoryVldNbrSigned(int cw06rHistoryVldNbrSignedIdx) {
		int position = Pos.cw06rHistoryVldNbrSigned(cw06rHistoryVldNbrSignedIdx - 1);
		return PicParser.display("-9(5)").parseLong(readString(position, Len.CW06R_HISTORY_VLD_NBR_SIGNED));
	}

	public void setCw06rCicrXrfIdCi(int cw06rCicrXrfIdCiIdx, char cw06rCicrXrfIdCi) {
		int position = Pos.cw06rCicrXrfIdCi(cw06rCicrXrfIdCiIdx - 1);
		writeChar(position, cw06rCicrXrfIdCi);
	}

	/**Original name: CW06R-CICR-XRF-ID-CI<br>*/
	public char getCw06rCicrXrfIdCi(int cw06rCicrXrfIdCiIdx) {
		int position = Pos.cw06rCicrXrfIdCi(cw06rCicrXrfIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rCicrXrfId(int cw06rCicrXrfIdIdx, String cw06rCicrXrfId) {
		int position = Pos.cw06rCicrXrfId(cw06rCicrXrfIdIdx - 1);
		writeString(position, cw06rCicrXrfId, Len.CW06R_CICR_XRF_ID);
	}

	/**Original name: CW06R-CICR-XRF-ID<br>*/
	public String getCw06rCicrXrfId(int cw06rCicrXrfIdIdx) {
		int position = Pos.cw06rCicrXrfId(cw06rCicrXrfIdIdx - 1);
		return readString(position, Len.CW06R_CICR_XRF_ID);
	}

	public void setCw06rXrfTypCdCi(int cw06rXrfTypCdCiIdx, char cw06rXrfTypCdCi) {
		int position = Pos.cw06rXrfTypCdCi(cw06rXrfTypCdCiIdx - 1);
		writeChar(position, cw06rXrfTypCdCi);
	}

	/**Original name: CW06R-XRF-TYP-CD-CI<br>*/
	public char getCw06rXrfTypCdCi(int cw06rXrfTypCdCiIdx) {
		int position = Pos.cw06rXrfTypCdCi(cw06rXrfTypCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rXrfTypCd(int cw06rXrfTypCdIdx, String cw06rXrfTypCd) {
		int position = Pos.cw06rXrfTypCd(cw06rXrfTypCdIdx - 1);
		writeString(position, cw06rXrfTypCd, Len.CW06R_XRF_TYP_CD);
	}

	/**Original name: CW06R-XRF-TYP-CD<br>*/
	public String getCw06rXrfTypCd(int cw06rXrfTypCdIdx) {
		int position = Pos.cw06rXrfTypCd(cw06rXrfTypCdIdx - 1);
		return readString(position, Len.CW06R_XRF_TYP_CD);
	}

	public void setCw06rCicrEffDtCi(int cw06rCicrEffDtCiIdx, char cw06rCicrEffDtCi) {
		int position = Pos.cw06rCicrEffDtCi(cw06rCicrEffDtCiIdx - 1);
		writeChar(position, cw06rCicrEffDtCi);
	}

	/**Original name: CW06R-CICR-EFF-DT-CI<br>*/
	public char getCw06rCicrEffDtCi(int cw06rCicrEffDtCiIdx) {
		int position = Pos.cw06rCicrEffDtCi(cw06rCicrEffDtCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rCicrEffDt(int cw06rCicrEffDtIdx, String cw06rCicrEffDt) {
		int position = Pos.cw06rCicrEffDt(cw06rCicrEffDtIdx - 1);
		writeString(position, cw06rCicrEffDt, Len.CW06R_CICR_EFF_DT);
	}

	/**Original name: CW06R-CICR-EFF-DT<br>*/
	public String getCw06rCicrEffDt(int cw06rCicrEffDtIdx) {
		int position = Pos.cw06rCicrEffDt(cw06rCicrEffDtIdx - 1);
		return readString(position, Len.CW06R_CICR_EFF_DT);
	}

	public void setCw06rCicrExpDtCi(int cw06rCicrExpDtCiIdx, char cw06rCicrExpDtCi) {
		int position = Pos.cw06rCicrExpDtCi(cw06rCicrExpDtCiIdx - 1);
		writeChar(position, cw06rCicrExpDtCi);
	}

	/**Original name: CW06R-CICR-EXP-DT-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw06rCicrExpDtCi(int cw06rCicrExpDtCiIdx) {
		int position = Pos.cw06rCicrExpDtCi(cw06rCicrExpDtCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rCicrExpDt(int cw06rCicrExpDtIdx, String cw06rCicrExpDt) {
		int position = Pos.cw06rCicrExpDt(cw06rCicrExpDtIdx - 1);
		writeString(position, cw06rCicrExpDt, Len.CW06R_CICR_EXP_DT);
	}

	/**Original name: CW06R-CICR-EXP-DT<br>*/
	public String getCw06rCicrExpDt(int cw06rCicrExpDtIdx) {
		int position = Pos.cw06rCicrExpDt(cw06rCicrExpDtIdx - 1);
		return readString(position, Len.CW06R_CICR_EXP_DT);
	}

	public void setCw06rCicrNbrOprStrsCi(int cw06rCicrNbrOprStrsCiIdx, char cw06rCicrNbrOprStrsCi) {
		int position = Pos.cw06rCicrNbrOprStrsCi(cw06rCicrNbrOprStrsCiIdx - 1);
		writeChar(position, cw06rCicrNbrOprStrsCi);
	}

	/**Original name: CW06R-CICR-NBR-OPR-STRS-CI<br>*/
	public char getCw06rCicrNbrOprStrsCi(int cw06rCicrNbrOprStrsCiIdx) {
		int position = Pos.cw06rCicrNbrOprStrsCi(cw06rCicrNbrOprStrsCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rCicrNbrOprStrsNi(int cw06rCicrNbrOprStrsNiIdx, char cw06rCicrNbrOprStrsNi) {
		int position = Pos.cw06rCicrNbrOprStrsNi(cw06rCicrNbrOprStrsNiIdx - 1);
		writeChar(position, cw06rCicrNbrOprStrsNi);
	}

	/**Original name: CW06R-CICR-NBR-OPR-STRS-NI<br>*/
	public char getCw06rCicrNbrOprStrsNi(int cw06rCicrNbrOprStrsNiIdx) {
		int position = Pos.cw06rCicrNbrOprStrsNi(cw06rCicrNbrOprStrsNiIdx - 1);
		return readChar(position);
	}

	public void setCw06rCicrNbrOprStrs(int cw06rCicrNbrOprStrsIdx, String cw06rCicrNbrOprStrs) {
		int position = Pos.cw06rCicrNbrOprStrs(cw06rCicrNbrOprStrsIdx - 1);
		writeString(position, cw06rCicrNbrOprStrs, Len.CW06R_CICR_NBR_OPR_STRS);
	}

	/**Original name: CW06R-CICR-NBR-OPR-STRS<br>*/
	public String getCw06rCicrNbrOprStrs(int cw06rCicrNbrOprStrsIdx) {
		int position = Pos.cw06rCicrNbrOprStrs(cw06rCicrNbrOprStrsIdx - 1);
		return readString(position, Len.CW06R_CICR_NBR_OPR_STRS);
	}

	public void setCw06rUserIdCi(int cw06rUserIdCiIdx, char cw06rUserIdCi) {
		int position = Pos.cw06rUserIdCi(cw06rUserIdCiIdx - 1);
		writeChar(position, cw06rUserIdCi);
	}

	/**Original name: CW06R-USER-ID-CI<br>*/
	public char getCw06rUserIdCi(int cw06rUserIdCiIdx) {
		int position = Pos.cw06rUserIdCi(cw06rUserIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rUserId(int cw06rUserIdIdx, String cw06rUserId) {
		int position = Pos.cw06rUserId(cw06rUserIdIdx - 1);
		writeString(position, cw06rUserId, Len.CW06R_USER_ID);
	}

	/**Original name: CW06R-USER-ID<br>*/
	public String getCw06rUserId(int cw06rUserIdIdx) {
		int position = Pos.cw06rUserId(cw06rUserIdIdx - 1);
		return readString(position, Len.CW06R_USER_ID);
	}

	public void setCw06rStatusCdCi(int cw06rStatusCdCiIdx, char cw06rStatusCdCi) {
		int position = Pos.cw06rStatusCdCi(cw06rStatusCdCiIdx - 1);
		writeChar(position, cw06rStatusCdCi);
	}

	/**Original name: CW06R-STATUS-CD-CI<br>*/
	public char getCw06rStatusCdCi(int cw06rStatusCdCiIdx) {
		int position = Pos.cw06rStatusCdCi(cw06rStatusCdCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rStatusCd(int cw06rStatusCdIdx, char cw06rStatusCd) {
		int position = Pos.cw06rStatusCd(cw06rStatusCdIdx - 1);
		writeChar(position, cw06rStatusCd);
	}

	/**Original name: CW06R-STATUS-CD<br>*/
	public char getCw06rStatusCd(int cw06rStatusCdIdx) {
		int position = Pos.cw06rStatusCd(cw06rStatusCdIdx - 1);
		return readChar(position);
	}

	public void setCw06rTerminalIdCi(int cw06rTerminalIdCiIdx, char cw06rTerminalIdCi) {
		int position = Pos.cw06rTerminalIdCi(cw06rTerminalIdCiIdx - 1);
		writeChar(position, cw06rTerminalIdCi);
	}

	/**Original name: CW06R-TERMINAL-ID-CI<br>*/
	public char getCw06rTerminalIdCi(int cw06rTerminalIdCiIdx) {
		int position = Pos.cw06rTerminalIdCi(cw06rTerminalIdCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rTerminalId(int cw06rTerminalIdIdx, String cw06rTerminalId) {
		int position = Pos.cw06rTerminalId(cw06rTerminalIdIdx - 1);
		writeString(position, cw06rTerminalId, Len.CW06R_TERMINAL_ID);
	}

	/**Original name: CW06R-TERMINAL-ID<br>*/
	public String getCw06rTerminalId(int cw06rTerminalIdIdx) {
		int position = Pos.cw06rTerminalId(cw06rTerminalIdIdx - 1);
		return readString(position, Len.CW06R_TERMINAL_ID);
	}

	public void setCw06rCicrEffAcyTsCi(int cw06rCicrEffAcyTsCiIdx, char cw06rCicrEffAcyTsCi) {
		int position = Pos.cw06rCicrEffAcyTsCi(cw06rCicrEffAcyTsCiIdx - 1);
		writeChar(position, cw06rCicrEffAcyTsCi);
	}

	/**Original name: CW06R-CICR-EFF-ACY-TS-CI<br>*/
	public char getCw06rCicrEffAcyTsCi(int cw06rCicrEffAcyTsCiIdx) {
		int position = Pos.cw06rCicrEffAcyTsCi(cw06rCicrEffAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rCicrEffAcyTs(int cw06rCicrEffAcyTsIdx, String cw06rCicrEffAcyTs) {
		int position = Pos.cw06rCicrEffAcyTs(cw06rCicrEffAcyTsIdx - 1);
		writeString(position, cw06rCicrEffAcyTs, Len.CW06R_CICR_EFF_ACY_TS);
	}

	/**Original name: CW06R-CICR-EFF-ACY-TS<br>*/
	public String getCw06rCicrEffAcyTs(int cw06rCicrEffAcyTsIdx) {
		int position = Pos.cw06rCicrEffAcyTs(cw06rCicrEffAcyTsIdx - 1);
		return readString(position, Len.CW06R_CICR_EFF_ACY_TS);
	}

	public void setCw06rCicrExpAcyTsCi(int cw06rCicrExpAcyTsCiIdx, char cw06rCicrExpAcyTsCi) {
		int position = Pos.cw06rCicrExpAcyTsCi(cw06rCicrExpAcyTsCiIdx - 1);
		writeChar(position, cw06rCicrExpAcyTsCi);
	}

	/**Original name: CW06R-CICR-EXP-ACY-TS-CI<br>*/
	public char getCw06rCicrExpAcyTsCi(int cw06rCicrExpAcyTsCiIdx) {
		int position = Pos.cw06rCicrExpAcyTsCi(cw06rCicrExpAcyTsCiIdx - 1);
		return readChar(position);
	}

	public void setCw06rCicrExpAcyTs(int cw06rCicrExpAcyTsIdx, String cw06rCicrExpAcyTs) {
		int position = Pos.cw06rCicrExpAcyTs(cw06rCicrExpAcyTsIdx - 1);
		writeString(position, cw06rCicrExpAcyTs, Len.CW06R_CICR_EXP_ACY_TS);
	}

	/**Original name: CW06R-CICR-EXP-ACY-TS<br>*/
	public String getCw06rCicrExpAcyTs(int cw06rCicrExpAcyTsIdx) {
		int position = Pos.cw06rCicrExpAcyTs(cw06rCicrExpAcyTsIdx - 1);
		return readString(position, Len.CW06R_CICR_EXP_ACY_TS);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_CLIENT_TAB = L_FRAMEWORK_RESPONSE_AREA;
		public static final int CW02R_CLIENT_TAB_ROW = L_FW_RESP_CLIENT_TAB;
		public static final int CW02R_CLIENT_TAB_FIXED = CW02R_CLIENT_TAB_ROW;
		public static final int CW02R_CLIENT_TAB_CHK_SUM = CW02R_CLIENT_TAB_FIXED;
		public static final int CW02R_CLIENT_ID_KCRE = CW02R_CLIENT_TAB_CHK_SUM + Len.CW02R_CLIENT_TAB_CHK_SUM;
		public static final int CW02R_CLIENT_TAB_DATES = CW02R_CLIENT_ID_KCRE + Len.CW02R_CLIENT_ID_KCRE;
		public static final int CW02R_TRANS_PROCESS_DT = CW02R_CLIENT_TAB_DATES;
		public static final int CW02R_CLIENT_TAB_KEY = CW02R_TRANS_PROCESS_DT + Len.CW02R_TRANS_PROCESS_DT;
		public static final int CW02R_CLIENT_ID_CI = CW02R_CLIENT_TAB_KEY;
		public static final int CW02R_CLIENT_ID = CW02R_CLIENT_ID_CI + Len.CW02R_CLIENT_ID_CI;
		public static final int CW02R_HISTORY_VLD_NBR_CI = CW02R_CLIENT_ID + Len.CW02R_CLIENT_ID;
		public static final int CW02R_HISTORY_VLD_NBR_SIGN = CW02R_HISTORY_VLD_NBR_CI + Len.CW02R_HISTORY_VLD_NBR_CI;
		public static final int CW02R_HISTORY_VLD_NBR = CW02R_HISTORY_VLD_NBR_SIGN + Len.CW02R_HISTORY_VLD_NBR_SIGN;
		public static final int CW02R_CICL_EFF_DT_CI = CW02R_HISTORY_VLD_NBR + Len.CW02R_HISTORY_VLD_NBR;
		public static final int CW02R_CICL_EFF_DT = CW02R_CICL_EFF_DT_CI + Len.CW02R_CICL_EFF_DT_CI;
		public static final int CW02R_CLIENT_TAB_DATA = CW02R_CICL_EFF_DT + Len.CW02R_CICL_EFF_DT;
		public static final int CW02R_CICL_PRI_SUB_CD_CI = CW02R_CLIENT_TAB_DATA;
		public static final int CW02R_CICL_PRI_SUB_CD = CW02R_CICL_PRI_SUB_CD_CI + Len.CW02R_CICL_PRI_SUB_CD_CI;
		public static final int CW02R_CICL_FST_NM_CI = CW02R_CICL_PRI_SUB_CD + Len.CW02R_CICL_PRI_SUB_CD;
		public static final int CW02R_CICL_FST_NM = CW02R_CICL_FST_NM_CI + Len.CW02R_CICL_FST_NM_CI;
		public static final int CW02R_CICL_LST_NM_CI = CW02R_CICL_FST_NM + Len.CW02R_CICL_FST_NM;
		public static final int CW02R_CICL_LST_NM = CW02R_CICL_LST_NM_CI + Len.CW02R_CICL_LST_NM_CI;
		public static final int CW02R_CICL_MDL_NM_CI = CW02R_CICL_LST_NM + Len.CW02R_CICL_LST_NM;
		public static final int CW02R_CICL_MDL_NM = CW02R_CICL_MDL_NM_CI + Len.CW02R_CICL_MDL_NM_CI;
		public static final int CW02R_NM_PFX_CI = CW02R_CICL_MDL_NM + Len.CW02R_CICL_MDL_NM;
		public static final int CW02R_NM_PFX = CW02R_NM_PFX_CI + Len.CW02R_NM_PFX_CI;
		public static final int CW02R_NM_SFX_CI = CW02R_NM_PFX + Len.CW02R_NM_PFX;
		public static final int CW02R_NM_SFX = CW02R_NM_SFX_CI + Len.CW02R_NM_SFX_CI;
		public static final int CW02R_PRIMARY_PRO_DSN_CD_CI = CW02R_NM_SFX + Len.CW02R_NM_SFX;
		public static final int CW02R_PRIMARY_PRO_DSN_CD_NI = CW02R_PRIMARY_PRO_DSN_CD_CI + Len.CW02R_PRIMARY_PRO_DSN_CD_CI;
		public static final int CW02R_PRIMARY_PRO_DSN_CD = CW02R_PRIMARY_PRO_DSN_CD_NI + Len.CW02R_PRIMARY_PRO_DSN_CD_NI;
		public static final int CW02R_LEG_ENT_CD_CI = CW02R_PRIMARY_PRO_DSN_CD + Len.CW02R_PRIMARY_PRO_DSN_CD;
		public static final int CW02R_LEG_ENT_CD = CW02R_LEG_ENT_CD_CI + Len.CW02R_LEG_ENT_CD_CI;
		public static final int CW02R_CICL_SDX_CD_CI = CW02R_LEG_ENT_CD + Len.CW02R_LEG_ENT_CD;
		public static final int CW02R_CICL_SDX_CD = CW02R_CICL_SDX_CD_CI + Len.CW02R_CICL_SDX_CD_CI;
		public static final int CW02R_CICL_OGN_INCEPT_DT_CI = CW02R_CICL_SDX_CD + Len.CW02R_CICL_SDX_CD;
		public static final int CW02R_CICL_OGN_INCEPT_DT_NI = CW02R_CICL_OGN_INCEPT_DT_CI + Len.CW02R_CICL_OGN_INCEPT_DT_CI;
		public static final int CW02R_CICL_OGN_INCEPT_DT = CW02R_CICL_OGN_INCEPT_DT_NI + Len.CW02R_CICL_OGN_INCEPT_DT_NI;
		public static final int CW02R_CICL_ADD_NM_IND_CI = CW02R_CICL_OGN_INCEPT_DT + Len.CW02R_CICL_OGN_INCEPT_DT;
		public static final int CW02R_CICL_ADD_NM_IND_NI = CW02R_CICL_ADD_NM_IND_CI + Len.CW02R_CICL_ADD_NM_IND_CI;
		public static final int CW02R_CICL_ADD_NM_IND = CW02R_CICL_ADD_NM_IND_NI + Len.CW02R_CICL_ADD_NM_IND_NI;
		public static final int CW02R_CICL_DOB_DT_CI = CW02R_CICL_ADD_NM_IND + Len.CW02R_CICL_ADD_NM_IND;
		public static final int CW02R_CICL_DOB_DT = CW02R_CICL_DOB_DT_CI + Len.CW02R_CICL_DOB_DT_CI;
		public static final int CW02R_CICL_BIR_ST_CD_CI = CW02R_CICL_DOB_DT + Len.CW02R_CICL_DOB_DT;
		public static final int CW02R_CICL_BIR_ST_CD = CW02R_CICL_BIR_ST_CD_CI + Len.CW02R_CICL_BIR_ST_CD_CI;
		public static final int CW02R_GENDER_CD_CI = CW02R_CICL_BIR_ST_CD + Len.CW02R_CICL_BIR_ST_CD;
		public static final int CW02R_GENDER_CD = CW02R_GENDER_CD_CI + Len.CW02R_GENDER_CD_CI;
		public static final int CW02R_PRI_LGG_CD_CI = CW02R_GENDER_CD + Len.CW02R_GENDER_CD;
		public static final int CW02R_PRI_LGG_CD = CW02R_PRI_LGG_CD_CI + Len.CW02R_PRI_LGG_CD_CI;
		public static final int CW02R_USER_ID_CI = CW02R_PRI_LGG_CD + Len.CW02R_PRI_LGG_CD;
		public static final int CW02R_USER_ID = CW02R_USER_ID_CI + Len.CW02R_USER_ID_CI;
		public static final int CW02R_STATUS_CD_CI = CW02R_USER_ID + Len.CW02R_USER_ID;
		public static final int CW02R_STATUS_CD = CW02R_STATUS_CD_CI + Len.CW02R_STATUS_CD_CI;
		public static final int CW02R_TERMINAL_ID_CI = CW02R_STATUS_CD + Len.CW02R_STATUS_CD;
		public static final int CW02R_TERMINAL_ID = CW02R_TERMINAL_ID_CI + Len.CW02R_TERMINAL_ID_CI;
		public static final int CW02R_CICL_EXP_DT_CI = CW02R_TERMINAL_ID + Len.CW02R_TERMINAL_ID;
		public static final int CW02R_CICL_EXP_DT = CW02R_CICL_EXP_DT_CI + Len.CW02R_CICL_EXP_DT_CI;
		public static final int CW02R_CICL_EFF_ACY_TS_CI = CW02R_CICL_EXP_DT + Len.CW02R_CICL_EXP_DT;
		public static final int CW02R_CICL_EFF_ACY_TS = CW02R_CICL_EFF_ACY_TS_CI + Len.CW02R_CICL_EFF_ACY_TS_CI;
		public static final int CW02R_CICL_EXP_ACY_TS_CI = CW02R_CICL_EFF_ACY_TS + Len.CW02R_CICL_EFF_ACY_TS;
		public static final int CW02R_CICL_EXP_ACY_TS = CW02R_CICL_EXP_ACY_TS_CI + Len.CW02R_CICL_EXP_ACY_TS_CI;
		public static final int CW02R_STATUTORY_TLE_CD_CI = CW02R_CICL_EXP_ACY_TS + Len.CW02R_CICL_EXP_ACY_TS;
		public static final int CW02R_STATUTORY_TLE_CD = CW02R_STATUTORY_TLE_CD_CI + Len.CW02R_STATUTORY_TLE_CD_CI;
		public static final int CW02R_CICL_LNG_NM_CI = CW02R_STATUTORY_TLE_CD + Len.CW02R_STATUTORY_TLE_CD;
		public static final int CW02R_CICL_LNG_NM_NI = CW02R_CICL_LNG_NM_CI + Len.CW02R_CICL_LNG_NM_CI;
		public static final int CW02R_CICL_LNG_NM = CW02R_CICL_LNG_NM_NI + Len.CW02R_CICL_LNG_NM_NI;
		public static final int CW02R_LO_LST_NM_MCH_CD_CI = CW02R_CICL_LNG_NM + Len.CW02R_CICL_LNG_NM;
		public static final int CW02R_LO_LST_NM_MCH_CD_NI = CW02R_LO_LST_NM_MCH_CD_CI + Len.CW02R_LO_LST_NM_MCH_CD_CI;
		public static final int CW02R_LO_LST_NM_MCH_CD = CW02R_LO_LST_NM_MCH_CD_NI + Len.CW02R_LO_LST_NM_MCH_CD_NI;
		public static final int CW02R_HI_LST_NM_MCH_CD_CI = CW02R_LO_LST_NM_MCH_CD + Len.CW02R_LO_LST_NM_MCH_CD;
		public static final int CW02R_HI_LST_NM_MCH_CD_NI = CW02R_HI_LST_NM_MCH_CD_CI + Len.CW02R_HI_LST_NM_MCH_CD_CI;
		public static final int CW02R_HI_LST_NM_MCH_CD = CW02R_HI_LST_NM_MCH_CD_NI + Len.CW02R_HI_LST_NM_MCH_CD_NI;
		public static final int CW02R_LO_FST_NM_MCH_CD_CI = CW02R_HI_LST_NM_MCH_CD + Len.CW02R_HI_LST_NM_MCH_CD;
		public static final int CW02R_LO_FST_NM_MCH_CD_NI = CW02R_LO_FST_NM_MCH_CD_CI + Len.CW02R_LO_FST_NM_MCH_CD_CI;
		public static final int CW02R_LO_FST_NM_MCH_CD = CW02R_LO_FST_NM_MCH_CD_NI + Len.CW02R_LO_FST_NM_MCH_CD_NI;
		public static final int CW02R_HI_FST_NM_MCH_CD_CI = CW02R_LO_FST_NM_MCH_CD + Len.CW02R_LO_FST_NM_MCH_CD;
		public static final int CW02R_HI_FST_NM_MCH_CD_NI = CW02R_HI_FST_NM_MCH_CD_CI + Len.CW02R_HI_FST_NM_MCH_CD_CI;
		public static final int CW02R_HI_FST_NM_MCH_CD = CW02R_HI_FST_NM_MCH_CD_NI + Len.CW02R_HI_FST_NM_MCH_CD_NI;
		public static final int CW02R_CICL_ACQ_SRC_CD_CI = CW02R_HI_FST_NM_MCH_CD + Len.CW02R_HI_FST_NM_MCH_CD;
		public static final int CW02R_CICL_ACQ_SRC_CD_NI = CW02R_CICL_ACQ_SRC_CD_CI + Len.CW02R_CICL_ACQ_SRC_CD_CI;
		public static final int CW02R_CICL_ACQ_SRC_CD = CW02R_CICL_ACQ_SRC_CD_NI + Len.CW02R_CICL_ACQ_SRC_CD_NI;
		public static final int CW02R_LEG_ENT_DESC = CW02R_CICL_ACQ_SRC_CD + Len.CW02R_CICL_ACQ_SRC_CD;
		public static final int L_FW_RESP_CLIENT_EMAIL = cwcacrAdrTypDesc(L_FW_RESP_CLT_ADR_COMPOSITE_MAXOCCURS - 1) + Len.CWCACR_ADR_TYP_DESC;
		public static final int CWEMR_CLIENT_EMAIL_ROW = L_FW_RESP_CLIENT_EMAIL;
		public static final int CWEMR_CLIENT_EMAIL_FIXED = CWEMR_CLIENT_EMAIL_ROW;
		public static final int CWEMR_CLIENT_EMAIL_CSUM = CWEMR_CLIENT_EMAIL_FIXED;
		public static final int CWEMR_CLIENT_ID_KCRE = CWEMR_CLIENT_EMAIL_CSUM + Len.CWEMR_CLIENT_EMAIL_CSUM;
		public static final int CWEMR_CIEM_SEQ_NBR_KCRE = CWEMR_CLIENT_ID_KCRE + Len.CWEMR_CLIENT_ID_KCRE;
		public static final int CWEMR_HISTORY_VLD_NBR_KCRE = CWEMR_CIEM_SEQ_NBR_KCRE + Len.CWEMR_CIEM_SEQ_NBR_KCRE;
		public static final int CWEMR_EFFECTIVE_DT_KCRE = CWEMR_HISTORY_VLD_NBR_KCRE + Len.CWEMR_HISTORY_VLD_NBR_KCRE;
		public static final int CWEMR_CLIENT_EMAIL_DATES = CWEMR_EFFECTIVE_DT_KCRE + Len.CWEMR_EFFECTIVE_DT_KCRE;
		public static final int CWEMR_TRANS_PROCESS_DT = CWEMR_CLIENT_EMAIL_DATES;
		public static final int CWEMR_CLIENT_EMAIL_KEY = CWEMR_TRANS_PROCESS_DT + Len.CWEMR_TRANS_PROCESS_DT;
		public static final int CWEMR_CLIENT_ID_CI = CWEMR_CLIENT_EMAIL_KEY;
		public static final int CWEMR_CLIENT_ID = CWEMR_CLIENT_ID_CI + Len.CWEMR_CLIENT_ID_CI;
		public static final int CWEMR_CIEM_SEQ_NBR_CI = CWEMR_CLIENT_ID + Len.CWEMR_CLIENT_ID;
		public static final int CWEMR_CIEM_SEQ_NBR_SIGN = CWEMR_CIEM_SEQ_NBR_CI + Len.CWEMR_CIEM_SEQ_NBR_CI;
		public static final int CWEMR_CIEM_SEQ_NBR = CWEMR_CIEM_SEQ_NBR_SIGN + Len.CWEMR_CIEM_SEQ_NBR_SIGN;
		public static final int CWEMR_HISTORY_VLD_NBR_CI = CWEMR_CIEM_SEQ_NBR + Len.CWEMR_CIEM_SEQ_NBR;
		public static final int CWEMR_HISTORY_VLD_NBR_SIGN = CWEMR_HISTORY_VLD_NBR_CI + Len.CWEMR_HISTORY_VLD_NBR_CI;
		public static final int CWEMR_HISTORY_VLD_NBR = CWEMR_HISTORY_VLD_NBR_SIGN + Len.CWEMR_HISTORY_VLD_NBR_SIGN;
		public static final int CWEMR_EFFECTIVE_DT_CI = CWEMR_HISTORY_VLD_NBR + Len.CWEMR_HISTORY_VLD_NBR;
		public static final int CWEMR_EFFECTIVE_DT = CWEMR_EFFECTIVE_DT_CI + Len.CWEMR_EFFECTIVE_DT_CI;
		public static final int CWEMR_CLIENT_EMAIL_DATA = CWEMR_EFFECTIVE_DT + Len.CWEMR_EFFECTIVE_DT;
		public static final int CWEMR_EMAIL_TYPE_CD_CI = CWEMR_CLIENT_EMAIL_DATA;
		public static final int CWEMR_EMAIL_TYPE_CD = CWEMR_EMAIL_TYPE_CD_CI + Len.CWEMR_EMAIL_TYPE_CD_CI;
		public static final int CWEMR_USER_ID_CI = CWEMR_EMAIL_TYPE_CD + Len.CWEMR_EMAIL_TYPE_CD;
		public static final int CWEMR_USER_ID = CWEMR_USER_ID_CI + Len.CWEMR_USER_ID_CI;
		public static final int CWEMR_STATUS_CD_CI = CWEMR_USER_ID + Len.CWEMR_USER_ID;
		public static final int CWEMR_STATUS_CD = CWEMR_STATUS_CD_CI + Len.CWEMR_STATUS_CD_CI;
		public static final int CWEMR_TERMINAL_ID_CI = CWEMR_STATUS_CD + Len.CWEMR_STATUS_CD;
		public static final int CWEMR_TERMINAL_ID = CWEMR_TERMINAL_ID_CI + Len.CWEMR_TERMINAL_ID_CI;
		public static final int CWEMR_EXPIRATION_DT_CI = CWEMR_TERMINAL_ID + Len.CWEMR_TERMINAL_ID;
		public static final int CWEMR_EXPIRATION_DT = CWEMR_EXPIRATION_DT_CI + Len.CWEMR_EXPIRATION_DT_CI;
		public static final int CWEMR_EFFECTIVE_ACY_TS_CI = CWEMR_EXPIRATION_DT + Len.CWEMR_EXPIRATION_DT;
		public static final int CWEMR_EFFECTIVE_ACY_TS = CWEMR_EFFECTIVE_ACY_TS_CI + Len.CWEMR_EFFECTIVE_ACY_TS_CI;
		public static final int CWEMR_EXPIRATION_ACY_TS_CI = CWEMR_EFFECTIVE_ACY_TS + Len.CWEMR_EFFECTIVE_ACY_TS;
		public static final int CWEMR_EXPIRATION_ACY_TS_NI = CWEMR_EXPIRATION_ACY_TS_CI + Len.CWEMR_EXPIRATION_ACY_TS_CI;
		public static final int CWEMR_EXPIRATION_ACY_TS = CWEMR_EXPIRATION_ACY_TS_NI + Len.CWEMR_EXPIRATION_ACY_TS_NI;
		public static final int CWEMR_CIEM_EMAIL_ADR_TXT_CI = CWEMR_EXPIRATION_ACY_TS + Len.CWEMR_EXPIRATION_ACY_TS;
		public static final int CWEMR_CIEM_EMAIL_ADR_TXT = CWEMR_CIEM_EMAIL_ADR_TXT_CI + Len.CWEMR_CIEM_EMAIL_ADR_TXT_CI;
		public static final int CWEMR_MORE_ROWS_SW = CWEMR_CIEM_EMAIL_ADR_TXT + Len.CWEMR_CIEM_EMAIL_ADR_TXT;
		public static final int CWEMR_EMAIL_TYPE_CD_DESC = CWEMR_MORE_ROWS_SW + Len.CWEMR_MORE_ROWS_SW;
		public static final int L_FW_RESP_CLT_REF_RELATION = cw08rObjDesc(L_FW_RESP_CLT_OBJ_RELATION_MAXOCCURS - 1) + Len.CW08R_OBJ_DESC;
		public static final int CW11R_CLT_REF_RELATION_ROW = L_FW_RESP_CLT_REF_RELATION;
		public static final int CW11R_CLT_REF_RELATION_FIXED = CW11R_CLT_REF_RELATION_ROW;
		public static final int CW11R_CLT_REF_RELATION_CSUM = CW11R_CLT_REF_RELATION_FIXED;
		public static final int CW11R_CLIENT_ID_KCRE = CW11R_CLT_REF_RELATION_CSUM + Len.CW11R_CLT_REF_RELATION_CSUM;
		public static final int CW11R_CIRF_REF_SEQ_NBR_KCRE = CW11R_CLIENT_ID_KCRE + Len.CW11R_CLIENT_ID_KCRE;
		public static final int CW11R_HISTORY_VLD_NBR_KCRE = CW11R_CIRF_REF_SEQ_NBR_KCRE + Len.CW11R_CIRF_REF_SEQ_NBR_KCRE;
		public static final int CW11R_CIRF_EFF_DT_KCRE = CW11R_HISTORY_VLD_NBR_KCRE + Len.CW11R_HISTORY_VLD_NBR_KCRE;
		public static final int CW11R_CLT_REF_RELATION_DATES = CW11R_CIRF_EFF_DT_KCRE + Len.CW11R_CIRF_EFF_DT_KCRE;
		public static final int CW11R_TRANS_PROCESS_DT = CW11R_CLT_REF_RELATION_DATES;
		public static final int CW11R_CLT_REF_RELATION_KEY = CW11R_TRANS_PROCESS_DT + Len.CW11R_TRANS_PROCESS_DT;
		public static final int CW11R_CLIENT_ID_CI = CW11R_CLT_REF_RELATION_KEY;
		public static final int CW11R_CLIENT_ID = CW11R_CLIENT_ID_CI + Len.CW11R_CLIENT_ID_CI;
		public static final int CW11R_CIRF_REF_SEQ_NBR_CI = CW11R_CLIENT_ID + Len.CW11R_CLIENT_ID;
		public static final int CW11R_CIRF_REF_SEQ_NBR_SIGN = CW11R_CIRF_REF_SEQ_NBR_CI + Len.CW11R_CIRF_REF_SEQ_NBR_CI;
		public static final int CW11R_CIRF_REF_SEQ_NBR = CW11R_CIRF_REF_SEQ_NBR_SIGN + Len.CW11R_CIRF_REF_SEQ_NBR_SIGN;
		public static final int CW11R_HISTORY_VLD_NBR_CI = CW11R_CIRF_REF_SEQ_NBR + Len.CW11R_CIRF_REF_SEQ_NBR;
		public static final int CW11R_HISTORY_VLD_NBR_SIGN = CW11R_HISTORY_VLD_NBR_CI + Len.CW11R_HISTORY_VLD_NBR_CI;
		public static final int CW11R_HISTORY_VLD_NBR = CW11R_HISTORY_VLD_NBR_SIGN + Len.CW11R_HISTORY_VLD_NBR_SIGN;
		public static final int CW11R_CIRF_EFF_DT_CI = CW11R_HISTORY_VLD_NBR + Len.CW11R_HISTORY_VLD_NBR;
		public static final int CW11R_CIRF_EFF_DT = CW11R_CIRF_EFF_DT_CI + Len.CW11R_CIRF_EFF_DT_CI;
		public static final int CW11R_CLT_REF_RELATION_DATA = CW11R_CIRF_EFF_DT + Len.CW11R_CIRF_EFF_DT;
		public static final int CW11R_CIRF_REF_ID_CI = CW11R_CLT_REF_RELATION_DATA;
		public static final int CW11R_CIRF_REF_ID = CW11R_CIRF_REF_ID_CI + Len.CW11R_CIRF_REF_ID_CI;
		public static final int CW11R_REF_TYP_CD_CI = CW11R_CIRF_REF_ID + Len.CW11R_CIRF_REF_ID;
		public static final int CW11R_REF_TYP_CD = CW11R_REF_TYP_CD_CI + Len.CW11R_REF_TYP_CD_CI;
		public static final int CW11R_CIRF_EXP_DT_CI = CW11R_REF_TYP_CD + Len.CW11R_REF_TYP_CD;
		public static final int CW11R_CIRF_EXP_DT = CW11R_CIRF_EXP_DT_CI + Len.CW11R_CIRF_EXP_DT_CI;
		public static final int CW11R_USER_ID_CI = CW11R_CIRF_EXP_DT + Len.CW11R_CIRF_EXP_DT;
		public static final int CW11R_USER_ID = CW11R_USER_ID_CI + Len.CW11R_USER_ID_CI;
		public static final int CW11R_STATUS_CD_CI = CW11R_USER_ID + Len.CW11R_USER_ID;
		public static final int CW11R_STATUS_CD = CW11R_STATUS_CD_CI + Len.CW11R_STATUS_CD_CI;
		public static final int CW11R_TERMINAL_ID_CI = CW11R_STATUS_CD + Len.CW11R_STATUS_CD;
		public static final int CW11R_TERMINAL_ID = CW11R_TERMINAL_ID_CI + Len.CW11R_TERMINAL_ID_CI;
		public static final int CW11R_CIRF_EFF_ACY_TS_CI = CW11R_TERMINAL_ID + Len.CW11R_TERMINAL_ID;
		public static final int CW11R_CIRF_EFF_ACY_TS = CW11R_CIRF_EFF_ACY_TS_CI + Len.CW11R_CIRF_EFF_ACY_TS_CI;
		public static final int CW11R_CIRF_EXP_ACY_TS_CI = CW11R_CIRF_EFF_ACY_TS + Len.CW11R_CIRF_EFF_ACY_TS;
		public static final int CW11R_CIRF_EXP_ACY_TS = CW11R_CIRF_EXP_ACY_TS_CI + Len.CW11R_CIRF_EXP_ACY_TS_CI;
		public static final int CW11R_MORE_ROWS_SW = CW11R_CIRF_EXP_ACY_TS + Len.CW11R_CIRF_EXP_ACY_TS;
		public static final int L_FW_RESP_CLIENT_TAX = CW11R_MORE_ROWS_SW + Len.CW11R_MORE_ROWS_SW;
		public static final int CW27R_CLIENT_TAX_ROW = L_FW_RESP_CLIENT_TAX;
		public static final int CW27R_CLIENT_TAX_FIXED = CW27R_CLIENT_TAX_ROW;
		public static final int CW27R_CLIENT_TAX_CHK_SUM = CW27R_CLIENT_TAX_FIXED;
		public static final int CW27R_CLIENT_ID_KCRE = CW27R_CLIENT_TAX_CHK_SUM + Len.CW27R_CLIENT_TAX_CHK_SUM;
		public static final int CW27R_CITX_TAX_SEQ_NBR_KCRE = CW27R_CLIENT_ID_KCRE + Len.CW27R_CLIENT_ID_KCRE;
		public static final int CW27R_CLIENT_TAX_DATES = CW27R_CITX_TAX_SEQ_NBR_KCRE + Len.CW27R_CITX_TAX_SEQ_NBR_KCRE;
		public static final int CW27R_TRANS_PROCESS_DT = CW27R_CLIENT_TAX_DATES;
		public static final int CW27R_CLIENT_TAX_KEY = CW27R_TRANS_PROCESS_DT + Len.CW27R_TRANS_PROCESS_DT;
		public static final int CW27R_CLIENT_ID_CI = CW27R_CLIENT_TAX_KEY;
		public static final int CW27R_CLIENT_ID = CW27R_CLIENT_ID_CI + Len.CW27R_CLIENT_ID_CI;
		public static final int CW27R_CITX_TAX_SEQ_NBR_CI = CW27R_CLIENT_ID + Len.CW27R_CLIENT_ID;
		public static final int CW27R_CITX_TAX_SEQ_NBR_SIGN = CW27R_CITX_TAX_SEQ_NBR_CI + Len.CW27R_CITX_TAX_SEQ_NBR_CI;
		public static final int CW27R_CITX_TAX_SEQ_NBR = CW27R_CITX_TAX_SEQ_NBR_SIGN + Len.CW27R_CITX_TAX_SEQ_NBR_SIGN;
		public static final int CW27R_HISTORY_VLD_NBR_CI = CW27R_CITX_TAX_SEQ_NBR + Len.CW27R_CITX_TAX_SEQ_NBR;
		public static final int CW27R_HISTORY_VLD_NBR_SIGN = CW27R_HISTORY_VLD_NBR_CI + Len.CW27R_HISTORY_VLD_NBR_CI;
		public static final int CW27R_HISTORY_VLD_NBR = CW27R_HISTORY_VLD_NBR_SIGN + Len.CW27R_HISTORY_VLD_NBR_SIGN;
		public static final int CW27R_EFFECTIVE_DT_CI = CW27R_HISTORY_VLD_NBR + Len.CW27R_HISTORY_VLD_NBR;
		public static final int CW27R_EFFECTIVE_DT = CW27R_EFFECTIVE_DT_CI + Len.CW27R_EFFECTIVE_DT_CI;
		public static final int CW27R_CLIENT_TAX_DATA = CW27R_EFFECTIVE_DT + Len.CW27R_EFFECTIVE_DT;
		public static final int CW27R_CITX_TAX_ID_CI = CW27R_CLIENT_TAX_DATA;
		public static final int CW27R_CITX_TAX_ID = CW27R_CITX_TAX_ID_CI + Len.CW27R_CITX_TAX_ID_CI;
		public static final int CW27R_TAX_TYPE_CD_CI = CW27R_CITX_TAX_ID + Len.CW27R_CITX_TAX_ID;
		public static final int CW27R_TAX_TYPE_CD = CW27R_TAX_TYPE_CD_CI + Len.CW27R_TAX_TYPE_CD_CI;
		public static final int CW27R_CITX_TAX_ST_CD_CI = CW27R_TAX_TYPE_CD + Len.CW27R_TAX_TYPE_CD;
		public static final int CW27R_CITX_TAX_ST_CD_NI = CW27R_CITX_TAX_ST_CD_CI + Len.CW27R_CITX_TAX_ST_CD_CI;
		public static final int CW27R_CITX_TAX_ST_CD = CW27R_CITX_TAX_ST_CD_NI + Len.CW27R_CITX_TAX_ST_CD_NI;
		public static final int CW27R_CITX_TAX_CTR_CD_CI = CW27R_CITX_TAX_ST_CD + Len.CW27R_CITX_TAX_ST_CD;
		public static final int CW27R_CITX_TAX_CTR_CD_NI = CW27R_CITX_TAX_CTR_CD_CI + Len.CW27R_CITX_TAX_CTR_CD_CI;
		public static final int CW27R_CITX_TAX_CTR_CD = CW27R_CITX_TAX_CTR_CD_NI + Len.CW27R_CITX_TAX_CTR_CD_NI;
		public static final int CW27R_USER_ID_CI = CW27R_CITX_TAX_CTR_CD + Len.CW27R_CITX_TAX_CTR_CD;
		public static final int CW27R_USER_ID = CW27R_USER_ID_CI + Len.CW27R_USER_ID_CI;
		public static final int CW27R_STATUS_CD_CI = CW27R_USER_ID + Len.CW27R_USER_ID;
		public static final int CW27R_STATUS_CD = CW27R_STATUS_CD_CI + Len.CW27R_STATUS_CD_CI;
		public static final int CW27R_TERMINAL_ID_CI = CW27R_STATUS_CD + Len.CW27R_STATUS_CD;
		public static final int CW27R_TERMINAL_ID = CW27R_TERMINAL_ID_CI + Len.CW27R_TERMINAL_ID_CI;
		public static final int CW27R_EXPIRATION_DT_CI = CW27R_TERMINAL_ID + Len.CW27R_TERMINAL_ID;
		public static final int CW27R_EXPIRATION_DT = CW27R_EXPIRATION_DT_CI + Len.CW27R_EXPIRATION_DT_CI;
		public static final int CW27R_EFFECTIVE_ACY_TS_CI = CW27R_EXPIRATION_DT + Len.CW27R_EXPIRATION_DT;
		public static final int CW27R_EFFECTIVE_ACY_TS = CW27R_EFFECTIVE_ACY_TS_CI + Len.CW27R_EFFECTIVE_ACY_TS_CI;
		public static final int CW27R_EXPIRATION_ACY_TS_CI = CW27R_EFFECTIVE_ACY_TS + Len.CW27R_EFFECTIVE_ACY_TS;
		public static final int CW27R_EXPIRATION_ACY_TS_NI = CW27R_EXPIRATION_ACY_TS_CI + Len.CW27R_EXPIRATION_ACY_TS_CI;
		public static final int CW27R_EXPIRATION_ACY_TS = CW27R_EXPIRATION_ACY_TS_NI + Len.CW27R_EXPIRATION_ACY_TS_NI;
		public static final int CW27R_MORE_ROWS_SW = CW27R_EXPIRATION_ACY_TS + Len.CW27R_EXPIRATION_ACY_TS;
		public static final int CW27R_TAX_TYPE_DESC = CW27R_MORE_ROWS_SW + Len.CW27R_MORE_ROWS_SW;
		public static final int L_FW_RESP_CLT_MUT_TER_STC = CW27R_TAX_TYPE_DESC + Len.CW27R_TAX_TYPE_DESC;
		public static final int CW45R_CLT_MUT_TER_STC_ROW = L_FW_RESP_CLT_MUT_TER_STC;
		public static final int CW45R_CLT_MUT_TER_STC_FIXED = CW45R_CLT_MUT_TER_STC_ROW;
		public static final int CW45R_CLT_MUT_TER_STC_CSUM = CW45R_CLT_MUT_TER_STC_FIXED;
		public static final int CW45R_TER_CLT_ID_KCRE = CW45R_CLT_MUT_TER_STC_CSUM + Len.CW45R_CLT_MUT_TER_STC_CSUM;
		public static final int CW45R_TER_LEVEL_CD_KCRE = CW45R_TER_CLT_ID_KCRE + Len.CW45R_TER_CLT_ID_KCRE;
		public static final int CW45R_TER_NBR_KCRE = CW45R_TER_LEVEL_CD_KCRE + Len.CW45R_TER_LEVEL_CD_KCRE;
		public static final int CW45R_CLT_MUT_TER_STC_DATES = CW45R_TER_NBR_KCRE + Len.CW45R_TER_NBR_KCRE;
		public static final int CW45R_TRANS_PROCESS_DT = CW45R_CLT_MUT_TER_STC_DATES;
		public static final int CW45R_CLT_MUT_TER_STC_KEY = CW45R_TRANS_PROCESS_DT + Len.CW45R_TRANS_PROCESS_DT;
		public static final int CW45R_TER_CLT_ID = CW45R_CLT_MUT_TER_STC_KEY;
		public static final int CW45R_TER_LEVEL_CD = CW45R_TER_CLT_ID + Len.CW45R_TER_CLT_ID;
		public static final int CW45R_TER_NBR = CW45R_TER_LEVEL_CD + Len.CW45R_TER_LEVEL_CD;
		public static final int CW45R_CLT_MUT_TER_STC_KEY_CI = CW45R_TER_NBR + Len.CW45R_TER_NBR;
		public static final int CW45R_TER_CLT_ID_CI = CW45R_CLT_MUT_TER_STC_KEY_CI;
		public static final int CW45R_TER_LEVEL_CD_CI = CW45R_TER_CLT_ID_CI + Len.CW45R_TER_CLT_ID_CI;
		public static final int CW45R_TER_NBR_CI = CW45R_TER_LEVEL_CD_CI + Len.CW45R_TER_LEVEL_CD_CI;
		public static final int CW45R_CLT_MUT_TER_STC_DATA = CW45R_TER_NBR_CI + Len.CW45R_TER_NBR_CI;
		public static final int CW45R_MR_TER_NBR_CI = CW45R_CLT_MUT_TER_STC_DATA;
		public static final int CW45R_MR_TER_NBR = CW45R_MR_TER_NBR_CI + Len.CW45R_MR_TER_NBR_CI;
		public static final int CW45R_MR_TER_CLT_ID_CI = CW45R_MR_TER_NBR + Len.CW45R_MR_TER_NBR;
		public static final int CW45R_MR_TER_CLT_ID = CW45R_MR_TER_CLT_ID_CI + Len.CW45R_MR_TER_CLT_ID_CI;
		public static final int CW45R_MR_NM_PFX_CI = CW45R_MR_TER_CLT_ID + Len.CW45R_MR_TER_CLT_ID;
		public static final int CW45R_MR_NM_PFX = CW45R_MR_NM_PFX_CI + Len.CW45R_MR_NM_PFX_CI;
		public static final int CW45R_MR_FST_NM_CI = CW45R_MR_NM_PFX + Len.CW45R_MR_NM_PFX;
		public static final int CW45R_MR_FST_NM = CW45R_MR_FST_NM_CI + Len.CW45R_MR_FST_NM_CI;
		public static final int CW45R_MR_MDL_NM_CI = CW45R_MR_FST_NM + Len.CW45R_MR_FST_NM;
		public static final int CW45R_MR_MDL_NM = CW45R_MR_MDL_NM_CI + Len.CW45R_MR_MDL_NM_CI;
		public static final int CW45R_MR_LST_NM_CI = CW45R_MR_MDL_NM + Len.CW45R_MR_MDL_NM;
		public static final int CW45R_MR_LST_NM = CW45R_MR_LST_NM_CI + Len.CW45R_MR_LST_NM_CI;
		public static final int CW45R_MR_NM_CI = CW45R_MR_LST_NM + Len.CW45R_MR_LST_NM;
		public static final int CW45R_MR_NM = CW45R_MR_NM_CI + Len.CW45R_MR_NM_CI;
		public static final int CW45R_MR_NM_SFX_CI = CW45R_MR_NM + Len.CW45R_MR_NM;
		public static final int CW45R_MR_NM_SFX = CW45R_MR_NM_SFX_CI + Len.CW45R_MR_NM_SFX_CI;
		public static final int CW45R_MR_CLT_ID_CI = CW45R_MR_NM_SFX + Len.CW45R_MR_NM_SFX;
		public static final int CW45R_MR_CLT_ID = CW45R_MR_CLT_ID_CI + Len.CW45R_MR_CLT_ID_CI;
		public static final int CW45R_CSR_TER_NBR_CI = CW45R_MR_CLT_ID + Len.CW45R_MR_CLT_ID;
		public static final int CW45R_CSR_TER_NBR = CW45R_CSR_TER_NBR_CI + Len.CW45R_CSR_TER_NBR_CI;
		public static final int CW45R_CSR_TER_CLT_ID_CI = CW45R_CSR_TER_NBR + Len.CW45R_CSR_TER_NBR;
		public static final int CW45R_CSR_TER_CLT_ID = CW45R_CSR_TER_CLT_ID_CI + Len.CW45R_CSR_TER_CLT_ID_CI;
		public static final int CW45R_CSR_NM_PFX_CI = CW45R_CSR_TER_CLT_ID + Len.CW45R_CSR_TER_CLT_ID;
		public static final int CW45R_CSR_NM_PFX = CW45R_CSR_NM_PFX_CI + Len.CW45R_CSR_NM_PFX_CI;
		public static final int CW45R_CSR_FST_NM_CI = CW45R_CSR_NM_PFX + Len.CW45R_CSR_NM_PFX;
		public static final int CW45R_CSR_FST_NM = CW45R_CSR_FST_NM_CI + Len.CW45R_CSR_FST_NM_CI;
		public static final int CW45R_CSR_MDL_NM_CI = CW45R_CSR_FST_NM + Len.CW45R_CSR_FST_NM;
		public static final int CW45R_CSR_MDL_NM = CW45R_CSR_MDL_NM_CI + Len.CW45R_CSR_MDL_NM_CI;
		public static final int CW45R_CSR_LST_NM_CI = CW45R_CSR_MDL_NM + Len.CW45R_CSR_MDL_NM;
		public static final int CW45R_CSR_LST_NM = CW45R_CSR_LST_NM_CI + Len.CW45R_CSR_LST_NM_CI;
		public static final int CW45R_CSR_NM_CI = CW45R_CSR_LST_NM + Len.CW45R_CSR_LST_NM;
		public static final int CW45R_CSR_NM = CW45R_CSR_NM_CI + Len.CW45R_CSR_NM_CI;
		public static final int CW45R_CSR_NM_SFX_CI = CW45R_CSR_NM + Len.CW45R_CSR_NM;
		public static final int CW45R_CSR_NM_SFX = CW45R_CSR_NM_SFX_CI + Len.CW45R_CSR_NM_SFX_CI;
		public static final int CW45R_CSR_CLT_ID_CI = CW45R_CSR_NM_SFX + Len.CW45R_CSR_NM_SFX;
		public static final int CW45R_CSR_CLT_ID = CW45R_CSR_CLT_ID_CI + Len.CW45R_CSR_CLT_ID_CI;
		public static final int CW45R_SMR_TER_NBR_CI = CW45R_CSR_CLT_ID + Len.CW45R_CSR_CLT_ID;
		public static final int CW45R_SMR_TER_NBR = CW45R_SMR_TER_NBR_CI + Len.CW45R_SMR_TER_NBR_CI;
		public static final int CW45R_SMR_TER_CLT_ID_CI = CW45R_SMR_TER_NBR + Len.CW45R_SMR_TER_NBR;
		public static final int CW45R_SMR_TER_CLT_ID = CW45R_SMR_TER_CLT_ID_CI + Len.CW45R_SMR_TER_CLT_ID_CI;
		public static final int CW45R_SMR_NM_PFX_CI = CW45R_SMR_TER_CLT_ID + Len.CW45R_SMR_TER_CLT_ID;
		public static final int CW45R_SMR_NM_PFX = CW45R_SMR_NM_PFX_CI + Len.CW45R_SMR_NM_PFX_CI;
		public static final int CW45R_SMR_FST_NM_CI = CW45R_SMR_NM_PFX + Len.CW45R_SMR_NM_PFX;
		public static final int CW45R_SMR_FST_NM = CW45R_SMR_FST_NM_CI + Len.CW45R_SMR_FST_NM_CI;
		public static final int CW45R_SMR_MDL_NM_CI = CW45R_SMR_FST_NM + Len.CW45R_SMR_FST_NM;
		public static final int CW45R_SMR_MDL_NM = CW45R_SMR_MDL_NM_CI + Len.CW45R_SMR_MDL_NM_CI;
		public static final int CW45R_SMR_LST_NM_CI = CW45R_SMR_MDL_NM + Len.CW45R_SMR_MDL_NM;
		public static final int CW45R_SMR_LST_NM = CW45R_SMR_LST_NM_CI + Len.CW45R_SMR_LST_NM_CI;
		public static final int CW45R_SMR_NM_CI = CW45R_SMR_LST_NM + Len.CW45R_SMR_LST_NM;
		public static final int CW45R_SMR_NM = CW45R_SMR_NM_CI + Len.CW45R_SMR_NM_CI;
		public static final int CW45R_SMR_NM_SFX_CI = CW45R_SMR_NM + Len.CW45R_SMR_NM;
		public static final int CW45R_SMR_NM_SFX = CW45R_SMR_NM_SFX_CI + Len.CW45R_SMR_NM_SFX_CI;
		public static final int CW45R_SMR_CLT_ID_CI = CW45R_SMR_NM_SFX + Len.CW45R_SMR_NM_SFX;
		public static final int CW45R_SMR_CLT_ID = CW45R_SMR_CLT_ID_CI + Len.CW45R_SMR_CLT_ID_CI;
		public static final int CW45R_DMM_TER_NBR_CI = CW45R_SMR_CLT_ID + Len.CW45R_SMR_CLT_ID;
		public static final int CW45R_DMM_TER_NBR = CW45R_DMM_TER_NBR_CI + Len.CW45R_DMM_TER_NBR_CI;
		public static final int CW45R_DMM_TER_CLT_ID_CI = CW45R_DMM_TER_NBR + Len.CW45R_DMM_TER_NBR;
		public static final int CW45R_DMM_TER_CLT_ID = CW45R_DMM_TER_CLT_ID_CI + Len.CW45R_DMM_TER_CLT_ID_CI;
		public static final int CW45R_DMM_NM_PFX_CI = CW45R_DMM_TER_CLT_ID + Len.CW45R_DMM_TER_CLT_ID;
		public static final int CW45R_DMM_NM_PFX = CW45R_DMM_NM_PFX_CI + Len.CW45R_DMM_NM_PFX_CI;
		public static final int CW45R_DMM_FST_NM_CI = CW45R_DMM_NM_PFX + Len.CW45R_DMM_NM_PFX;
		public static final int CW45R_DMM_FST_NM = CW45R_DMM_FST_NM_CI + Len.CW45R_DMM_FST_NM_CI;
		public static final int CW45R_DMM_MDL_NM_CI = CW45R_DMM_FST_NM + Len.CW45R_DMM_FST_NM;
		public static final int CW45R_DMM_MDL_NM = CW45R_DMM_MDL_NM_CI + Len.CW45R_DMM_MDL_NM_CI;
		public static final int CW45R_DMM_LST_NM_CI = CW45R_DMM_MDL_NM + Len.CW45R_DMM_MDL_NM;
		public static final int CW45R_DMM_LST_NM = CW45R_DMM_LST_NM_CI + Len.CW45R_DMM_LST_NM_CI;
		public static final int CW45R_DMM_NM_CI = CW45R_DMM_LST_NM + Len.CW45R_DMM_LST_NM;
		public static final int CW45R_DMM_NM = CW45R_DMM_NM_CI + Len.CW45R_DMM_NM_CI;
		public static final int CW45R_DMM_NM_SFX_CI = CW45R_DMM_NM + Len.CW45R_DMM_NM;
		public static final int CW45R_DMM_NM_SFX = CW45R_DMM_NM_SFX_CI + Len.CW45R_DMM_NM_SFX_CI;
		public static final int CW45R_DMM_CLT_ID_CI = CW45R_DMM_NM_SFX + Len.CW45R_DMM_NM_SFX;
		public static final int CW45R_DMM_CLT_ID = CW45R_DMM_CLT_ID_CI + Len.CW45R_DMM_CLT_ID_CI;
		public static final int CW45R_RMM_TER_NBR_CI = CW45R_DMM_CLT_ID + Len.CW45R_DMM_CLT_ID;
		public static final int CW45R_RMM_TER_NBR = CW45R_RMM_TER_NBR_CI + Len.CW45R_RMM_TER_NBR_CI;
		public static final int CW45R_RMM_TER_CLT_ID_CI = CW45R_RMM_TER_NBR + Len.CW45R_RMM_TER_NBR;
		public static final int CW45R_RMM_TER_CLT_ID = CW45R_RMM_TER_CLT_ID_CI + Len.CW45R_RMM_TER_CLT_ID_CI;
		public static final int CW45R_RMM_NM_PFX_CI = CW45R_RMM_TER_CLT_ID + Len.CW45R_RMM_TER_CLT_ID;
		public static final int CW45R_RMM_NM_PFX = CW45R_RMM_NM_PFX_CI + Len.CW45R_RMM_NM_PFX_CI;
		public static final int CW45R_RMM_FST_NM_CI = CW45R_RMM_NM_PFX + Len.CW45R_RMM_NM_PFX;
		public static final int CW45R_RMM_FST_NM = CW45R_RMM_FST_NM_CI + Len.CW45R_RMM_FST_NM_CI;
		public static final int CW45R_RMM_MDL_NM_CI = CW45R_RMM_FST_NM + Len.CW45R_RMM_FST_NM;
		public static final int CW45R_RMM_MDL_NM = CW45R_RMM_MDL_NM_CI + Len.CW45R_RMM_MDL_NM_CI;
		public static final int CW45R_RMM_LST_NM_CI = CW45R_RMM_MDL_NM + Len.CW45R_RMM_MDL_NM;
		public static final int CW45R_RMM_LST_NM = CW45R_RMM_LST_NM_CI + Len.CW45R_RMM_LST_NM_CI;
		public static final int CW45R_RMM_NM_CI = CW45R_RMM_LST_NM + Len.CW45R_RMM_LST_NM;
		public static final int CW45R_RMM_NM = CW45R_RMM_NM_CI + Len.CW45R_RMM_NM_CI;
		public static final int CW45R_RMM_NM_SFX_CI = CW45R_RMM_NM + Len.CW45R_RMM_NM;
		public static final int CW45R_RMM_NM_SFX = CW45R_RMM_NM_SFX_CI + Len.CW45R_RMM_NM_SFX_CI;
		public static final int CW45R_RMM_CLT_ID_CI = CW45R_RMM_NM_SFX + Len.CW45R_RMM_NM_SFX;
		public static final int CW45R_RMM_CLT_ID = CW45R_RMM_CLT_ID_CI + Len.CW45R_RMM_CLT_ID_CI;
		public static final int CW45R_DFO_TER_NBR_CI = CW45R_RMM_CLT_ID + Len.CW45R_RMM_CLT_ID;
		public static final int CW45R_DFO_TER_NBR = CW45R_DFO_TER_NBR_CI + Len.CW45R_DFO_TER_NBR_CI;
		public static final int CW45R_DFO_TER_CLT_ID_CI = CW45R_DFO_TER_NBR + Len.CW45R_DFO_TER_NBR;
		public static final int CW45R_DFO_TER_CLT_ID = CW45R_DFO_TER_CLT_ID_CI + Len.CW45R_DFO_TER_CLT_ID_CI;
		public static final int CW45R_DFO_NM_PFX_CI = CW45R_DFO_TER_CLT_ID + Len.CW45R_DFO_TER_CLT_ID;
		public static final int CW45R_DFO_NM_PFX = CW45R_DFO_NM_PFX_CI + Len.CW45R_DFO_NM_PFX_CI;
		public static final int CW45R_DFO_FST_NM_CI = CW45R_DFO_NM_PFX + Len.CW45R_DFO_NM_PFX;
		public static final int CW45R_DFO_FST_NM = CW45R_DFO_FST_NM_CI + Len.CW45R_DFO_FST_NM_CI;
		public static final int CW45R_DFO_MDL_NM_CI = CW45R_DFO_FST_NM + Len.CW45R_DFO_FST_NM;
		public static final int CW45R_DFO_MDL_NM = CW45R_DFO_MDL_NM_CI + Len.CW45R_DFO_MDL_NM_CI;
		public static final int CW45R_DFO_LST_NM_CI = CW45R_DFO_MDL_NM + Len.CW45R_DFO_MDL_NM;
		public static final int CW45R_DFO_LST_NM = CW45R_DFO_LST_NM_CI + Len.CW45R_DFO_LST_NM_CI;
		public static final int CW45R_DFO_NM_CI = CW45R_DFO_LST_NM + Len.CW45R_DFO_LST_NM;
		public static final int CW45R_DFO_NM = CW45R_DFO_NM_CI + Len.CW45R_DFO_NM_CI;
		public static final int CW45R_DFO_NM_SFX_CI = CW45R_DFO_NM + Len.CW45R_DFO_NM;
		public static final int CW45R_DFO_NM_SFX = CW45R_DFO_NM_SFX_CI + Len.CW45R_DFO_NM_SFX_CI;
		public static final int CW45R_DFO_CLT_ID_CI = CW45R_DFO_NM_SFX + Len.CW45R_DFO_NM_SFX;
		public static final int CW45R_DFO_CLT_ID = CW45R_DFO_CLT_ID_CI + Len.CW45R_DFO_CLT_ID_CI;
		public static final int L_FW_RESP_CLT_AGC_TER_STC = CW45R_DFO_CLT_ID + Len.CW45R_DFO_CLT_ID;
		public static final int CW46R_CLT_AGC_TER_STC_ROW = L_FW_RESP_CLT_AGC_TER_STC;
		public static final int CW46R_CLT_AGC_TER_STC_FIXED = CW46R_CLT_AGC_TER_STC_ROW;
		public static final int CW46R_CLT_AGC_TER_STC_CSUM = CW46R_CLT_AGC_TER_STC_FIXED;
		public static final int CW46R_TER_CLT_ID_KCRE = CW46R_CLT_AGC_TER_STC_CSUM + Len.CW46R_CLT_AGC_TER_STC_CSUM;
		public static final int CW46R_TER_LEVEL_CD_KCRE = CW46R_TER_CLT_ID_KCRE + Len.CW46R_TER_CLT_ID_KCRE;
		public static final int CW46R_CLT_AGC_TER_STC_DATES = CW46R_TER_LEVEL_CD_KCRE + Len.CW46R_TER_LEVEL_CD_KCRE;
		public static final int CW46R_TRANS_PROCESS_DT = CW46R_CLT_AGC_TER_STC_DATES;
		public static final int CW46R_CLT_AGC_TER_STC_KEY = CW46R_TRANS_PROCESS_DT + Len.CW46R_TRANS_PROCESS_DT;
		public static final int CW46R_TER_CLT_ID = CW46R_CLT_AGC_TER_STC_KEY;
		public static final int CW46R_TER_LEVEL_CD = CW46R_TER_CLT_ID + Len.CW46R_TER_CLT_ID;
		public static final int CW46R_CLT_AGC_TER_STC_KEY_CI = CW46R_TER_LEVEL_CD + Len.CW46R_TER_LEVEL_CD;
		public static final int CW46R_TER_CLT_ID_CI = CW46R_CLT_AGC_TER_STC_KEY_CI;
		public static final int CW46R_TER_LEVEL_CD_CI = CW46R_TER_CLT_ID_CI + Len.CW46R_TER_CLT_ID_CI;
		public static final int CW46R_CLT_AGC_TER_STC_DATA = CW46R_TER_LEVEL_CD_CI + Len.CW46R_TER_LEVEL_CD_CI;
		public static final int CW46R_TER_NBR_CI = CW46R_CLT_AGC_TER_STC_DATA;
		public static final int CW46R_TER_NBR = CW46R_TER_NBR_CI + Len.CW46R_TER_NBR_CI;
		public static final int CW46R_BRN_TER_NBR_CI = CW46R_TER_NBR + Len.CW46R_TER_NBR;
		public static final int CW46R_BRN_TER_NBR = CW46R_BRN_TER_NBR_CI + Len.CW46R_BRN_TER_NBR_CI;
		public static final int CW46R_BRN_TER_CLT_ID_CI = CW46R_BRN_TER_NBR + Len.CW46R_BRN_TER_NBR;
		public static final int CW46R_BRN_TER_CLT_ID = CW46R_BRN_TER_CLT_ID_CI + Len.CW46R_BRN_TER_CLT_ID_CI;
		public static final int CW46R_BRN_NM_CI = CW46R_BRN_TER_CLT_ID + Len.CW46R_BRN_TER_CLT_ID;
		public static final int CW46R_BRN_NM = CW46R_BRN_NM_CI + Len.CW46R_BRN_NM_CI;
		public static final int CW46R_BRN_CLT_ID_CI = CW46R_BRN_NM + Len.CW46R_BRN_NM;
		public static final int CW46R_BRN_CLT_ID = CW46R_BRN_CLT_ID_CI + Len.CW46R_BRN_CLT_ID_CI;
		public static final int CW46R_AGC_TER_NBR_CI = CW46R_BRN_CLT_ID + Len.CW46R_BRN_CLT_ID;
		public static final int CW46R_AGC_TER_NBR = CW46R_AGC_TER_NBR_CI + Len.CW46R_AGC_TER_NBR_CI;
		public static final int CW46R_AGC_TER_CLT_ID_CI = CW46R_AGC_TER_NBR + Len.CW46R_AGC_TER_NBR;
		public static final int CW46R_AGC_TER_CLT_ID = CW46R_AGC_TER_CLT_ID_CI + Len.CW46R_AGC_TER_CLT_ID_CI;
		public static final int CW46R_AGC_NM_CI = CW46R_AGC_TER_CLT_ID + Len.CW46R_AGC_TER_CLT_ID;
		public static final int CW46R_AGC_NM = CW46R_AGC_NM_CI + Len.CW46R_AGC_NM_CI;
		public static final int CW46R_AGC_CLT_ID_CI = CW46R_AGC_NM + Len.CW46R_AGC_NM;
		public static final int CW46R_AGC_CLT_ID = CW46R_AGC_CLT_ID_CI + Len.CW46R_AGC_CLT_ID_CI;
		public static final int CW46R_SMR_TER_NBR_CI = CW46R_AGC_CLT_ID + Len.CW46R_AGC_CLT_ID;
		public static final int CW46R_SMR_TER_NBR = CW46R_SMR_TER_NBR_CI + Len.CW46R_SMR_TER_NBR_CI;
		public static final int CW46R_SMR_TER_CLT_ID_CI = CW46R_SMR_TER_NBR + Len.CW46R_SMR_TER_NBR;
		public static final int CW46R_SMR_TER_CLT_ID = CW46R_SMR_TER_CLT_ID_CI + Len.CW46R_SMR_TER_CLT_ID_CI;
		public static final int CW46R_SMR_NM_CI = CW46R_SMR_TER_CLT_ID + Len.CW46R_SMR_TER_CLT_ID;
		public static final int CW46R_SMR_NM = CW46R_SMR_NM_CI + Len.CW46R_SMR_NM_CI;
		public static final int CW46R_SMR_CLT_ID_CI = CW46R_SMR_NM + Len.CW46R_SMR_NM;
		public static final int CW46R_SMR_CLT_ID = CW46R_SMR_CLT_ID_CI + Len.CW46R_SMR_CLT_ID_CI;
		public static final int CW46R_AMM_TER_NBR_CI = CW46R_SMR_CLT_ID + Len.CW46R_SMR_CLT_ID;
		public static final int CW46R_AMM_TER_NBR = CW46R_AMM_TER_NBR_CI + Len.CW46R_AMM_TER_NBR_CI;
		public static final int CW46R_AMM_TER_CLT_ID_CI = CW46R_AMM_TER_NBR + Len.CW46R_AMM_TER_NBR;
		public static final int CW46R_AMM_TER_CLT_ID = CW46R_AMM_TER_CLT_ID_CI + Len.CW46R_AMM_TER_CLT_ID_CI;
		public static final int CW46R_AMM_NM_PFX_CI = CW46R_AMM_TER_CLT_ID + Len.CW46R_AMM_TER_CLT_ID;
		public static final int CW46R_AMM_NM_PFX = CW46R_AMM_NM_PFX_CI + Len.CW46R_AMM_NM_PFX_CI;
		public static final int CW46R_AMM_FST_NM_CI = CW46R_AMM_NM_PFX + Len.CW46R_AMM_NM_PFX;
		public static final int CW46R_AMM_FST_NM = CW46R_AMM_FST_NM_CI + Len.CW46R_AMM_FST_NM_CI;
		public static final int CW46R_AMM_MDL_NM_CI = CW46R_AMM_FST_NM + Len.CW46R_AMM_FST_NM;
		public static final int CW46R_AMM_MDL_NM = CW46R_AMM_MDL_NM_CI + Len.CW46R_AMM_MDL_NM_CI;
		public static final int CW46R_AMM_LST_NM_CI = CW46R_AMM_MDL_NM + Len.CW46R_AMM_MDL_NM;
		public static final int CW46R_AMM_LST_NM = CW46R_AMM_LST_NM_CI + Len.CW46R_AMM_LST_NM_CI;
		public static final int CW46R_AMM_NM_CI = CW46R_AMM_LST_NM + Len.CW46R_AMM_LST_NM;
		public static final int CW46R_AMM_NM = CW46R_AMM_NM_CI + Len.CW46R_AMM_NM_CI;
		public static final int CW46R_AMM_NM_SFX_CI = CW46R_AMM_NM + Len.CW46R_AMM_NM;
		public static final int CW46R_AMM_NM_SFX = CW46R_AMM_NM_SFX_CI + Len.CW46R_AMM_NM_SFX_CI;
		public static final int CW46R_AMM_CLT_ID_CI = CW46R_AMM_NM_SFX + Len.CW46R_AMM_NM_SFX;
		public static final int CW46R_AMM_CLT_ID = CW46R_AMM_CLT_ID_CI + Len.CW46R_AMM_CLT_ID_CI;
		public static final int CW46R_AFM_TER_NBR_CI = CW46R_AMM_CLT_ID + Len.CW46R_AMM_CLT_ID;
		public static final int CW46R_AFM_TER_NBR = CW46R_AFM_TER_NBR_CI + Len.CW46R_AFM_TER_NBR_CI;
		public static final int CW46R_AFM_TER_CLT_ID_CI = CW46R_AFM_TER_NBR + Len.CW46R_AFM_TER_NBR;
		public static final int CW46R_AFM_TER_CLT_ID = CW46R_AFM_TER_CLT_ID_CI + Len.CW46R_AFM_TER_CLT_ID_CI;
		public static final int CW46R_AFM_NM_PFX_CI = CW46R_AFM_TER_CLT_ID + Len.CW46R_AFM_TER_CLT_ID;
		public static final int CW46R_AFM_NM_PFX = CW46R_AFM_NM_PFX_CI + Len.CW46R_AFM_NM_PFX_CI;
		public static final int CW46R_AFM_FST_NM_CI = CW46R_AFM_NM_PFX + Len.CW46R_AFM_NM_PFX;
		public static final int CW46R_AFM_FST_NM = CW46R_AFM_FST_NM_CI + Len.CW46R_AFM_FST_NM_CI;
		public static final int CW46R_AFM_MDL_NM_CI = CW46R_AFM_FST_NM + Len.CW46R_AFM_FST_NM;
		public static final int CW46R_AFM_MDL_NM = CW46R_AFM_MDL_NM_CI + Len.CW46R_AFM_MDL_NM_CI;
		public static final int CW46R_AFM_LST_NM_CI = CW46R_AFM_MDL_NM + Len.CW46R_AFM_MDL_NM;
		public static final int CW46R_AFM_LST_NM = CW46R_AFM_LST_NM_CI + Len.CW46R_AFM_LST_NM_CI;
		public static final int CW46R_AFM_NM_CI = CW46R_AFM_LST_NM + Len.CW46R_AFM_LST_NM;
		public static final int CW46R_AFM_NM = CW46R_AFM_NM_CI + Len.CW46R_AFM_NM_CI;
		public static final int CW46R_AFM_NM_SFX_CI = CW46R_AFM_NM + Len.CW46R_AFM_NM;
		public static final int CW46R_AFM_NM_SFX = CW46R_AFM_NM_SFX_CI + Len.CW46R_AFM_NM_SFX_CI;
		public static final int CW46R_AFM_CLT_ID_CI = CW46R_AFM_NM_SFX + Len.CW46R_AFM_NM_SFX;
		public static final int CW46R_AFM_CLT_ID = CW46R_AFM_CLT_ID_CI + Len.CW46R_AFM_CLT_ID_CI;
		public static final int CW46R_AVP_TER_NBR_CI = CW46R_AFM_CLT_ID + Len.CW46R_AFM_CLT_ID;
		public static final int CW46R_AVP_TER_NBR = CW46R_AVP_TER_NBR_CI + Len.CW46R_AVP_TER_NBR_CI;
		public static final int CW46R_AVP_TER_CLT_ID_CI = CW46R_AVP_TER_NBR + Len.CW46R_AVP_TER_NBR;
		public static final int CW46R_AVP_TER_CLT_ID = CW46R_AVP_TER_CLT_ID_CI + Len.CW46R_AVP_TER_CLT_ID_CI;
		public static final int CW46R_AVP_NM_PFX_CI = CW46R_AVP_TER_CLT_ID + Len.CW46R_AVP_TER_CLT_ID;
		public static final int CW46R_AVP_NM_PFX = CW46R_AVP_NM_PFX_CI + Len.CW46R_AVP_NM_PFX_CI;
		public static final int CW46R_AVP_FST_NM_CI = CW46R_AVP_NM_PFX + Len.CW46R_AVP_NM_PFX;
		public static final int CW46R_AVP_FST_NM = CW46R_AVP_FST_NM_CI + Len.CW46R_AVP_FST_NM_CI;
		public static final int CW46R_AVP_MDL_NM_CI = CW46R_AVP_FST_NM + Len.CW46R_AVP_FST_NM;
		public static final int CW46R_AVP_MDL_NM = CW46R_AVP_MDL_NM_CI + Len.CW46R_AVP_MDL_NM_CI;
		public static final int CW46R_AVP_LST_NM_CI = CW46R_AVP_MDL_NM + Len.CW46R_AVP_MDL_NM;
		public static final int CW46R_AVP_LST_NM = CW46R_AVP_LST_NM_CI + Len.CW46R_AVP_LST_NM_CI;
		public static final int CW46R_AVP_NM_CI = CW46R_AVP_LST_NM + Len.CW46R_AVP_LST_NM;
		public static final int CW46R_AVP_NM = CW46R_AVP_NM_CI + Len.CW46R_AVP_NM_CI;
		public static final int CW46R_AVP_NM_SFX_CI = CW46R_AVP_NM + Len.CW46R_AVP_NM;
		public static final int CW46R_AVP_NM_SFX = CW46R_AVP_NM_SFX_CI + Len.CW46R_AVP_NM_SFX_CI;
		public static final int CW46R_AVP_CLT_ID_CI = CW46R_AVP_NM_SFX + Len.CW46R_AVP_NM_SFX;
		public static final int CW46R_AVP_CLT_ID = CW46R_AVP_CLT_ID_CI + Len.CW46R_AVP_CLT_ID_CI;
		public static final int L_FW_RESP_CLT_UW_BPO = CW46R_AVP_CLT_ID + Len.CW46R_AVP_CLT_ID;
		public static final int MUA02_CLT_CLT_RELATION_ROW = L_FW_RESP_CLT_UW_BPO;
		public static final int MUA02_CLT_CLT_RELATION_FIXED = MUA02_CLT_CLT_RELATION_ROW;
		public static final int MUA02_CLT_CLT_RELATION_CSUM = MUA02_CLT_CLT_RELATION_FIXED;
		public static final int MUA02_CLIENT_ID_KCRE = MUA02_CLT_CLT_RELATION_CSUM + Len.MUA02_CLT_CLT_RELATION_CSUM;
		public static final int MUA02_CLT_TYP_CD_KCRE = MUA02_CLIENT_ID_KCRE + Len.MUA02_CLIENT_ID_KCRE;
		public static final int MUA02_HISTORY_VLD_NBR_KCRE = MUA02_CLT_TYP_CD_KCRE + Len.MUA02_CLT_TYP_CD_KCRE;
		public static final int MUA02_CICR_XRF_ID_KCRE = MUA02_HISTORY_VLD_NBR_KCRE + Len.MUA02_HISTORY_VLD_NBR_KCRE;
		public static final int MUA02_XRF_TYP_CD_KCRE = MUA02_CICR_XRF_ID_KCRE + Len.MUA02_CICR_XRF_ID_KCRE;
		public static final int MUA02_CICR_EFF_DT_KCRE = MUA02_XRF_TYP_CD_KCRE + Len.MUA02_XRF_TYP_CD_KCRE;
		public static final int MUA02_CLT_CLT_RELATION_DATES = MUA02_CICR_EFF_DT_KCRE + Len.MUA02_CICR_EFF_DT_KCRE;
		public static final int MUA02_TRANS_PROCESS_DT = MUA02_CLT_CLT_RELATION_DATES;
		public static final int MUA02_CLT_CLT_RELATION_KEY = MUA02_TRANS_PROCESS_DT + Len.MUA02_TRANS_PROCESS_DT;
		public static final int MUA02_CLIENT_ID_CI = MUA02_CLT_CLT_RELATION_KEY;
		public static final int MUA02_CLIENT_ID = MUA02_CLIENT_ID_CI + Len.MUA02_CLIENT_ID_CI;
		public static final int MUA02_CLT_TYP_CD_CI = MUA02_CLIENT_ID + Len.MUA02_CLIENT_ID;
		public static final int MUA02_CLT_TYP_CD = MUA02_CLT_TYP_CD_CI + Len.MUA02_CLT_TYP_CD_CI;
		public static final int MUA02_HISTORY_VLD_NBR_CI = MUA02_CLT_TYP_CD + Len.MUA02_CLT_TYP_CD;
		public static final int MUA02_HISTORY_VLD_NBR_SIGN = MUA02_HISTORY_VLD_NBR_CI + Len.MUA02_HISTORY_VLD_NBR_CI;
		public static final int MUA02_HISTORY_VLD_NBR = MUA02_HISTORY_VLD_NBR_SIGN + Len.MUA02_HISTORY_VLD_NBR_SIGN;
		public static final int MUA02_CICR_XRF_ID_CI = MUA02_HISTORY_VLD_NBR + Len.MUA02_HISTORY_VLD_NBR;
		public static final int MUA02_CICR_XRF_ID = MUA02_CICR_XRF_ID_CI + Len.MUA02_CICR_XRF_ID_CI;
		public static final int MUA02_XRF_TYP_CD_CI = MUA02_CICR_XRF_ID + Len.MUA02_CICR_XRF_ID;
		public static final int MUA02_XRF_TYP_CD = MUA02_XRF_TYP_CD_CI + Len.MUA02_XRF_TYP_CD_CI;
		public static final int MUA02_CICR_EFF_DT_CI = MUA02_XRF_TYP_CD + Len.MUA02_XRF_TYP_CD;
		public static final int MUA02_CICR_EFF_DT = MUA02_CICR_EFF_DT_CI + Len.MUA02_CICR_EFF_DT_CI;
		public static final int MUA02_CLT_CLT_RELATION_DATA = MUA02_CICR_EFF_DT + Len.MUA02_CICR_EFF_DT;
		public static final int MUA02_CICR_EXP_DT_CI = MUA02_CLT_CLT_RELATION_DATA;
		public static final int MUA02_CICR_EXP_DT = MUA02_CICR_EXP_DT_CI + Len.MUA02_CICR_EXP_DT_CI;
		public static final int MUA02_CICR_NBR_OPR_STRS_CI = MUA02_CICR_EXP_DT + Len.MUA02_CICR_EXP_DT;
		public static final int MUA02_CICR_NBR_OPR_STRS_NI = MUA02_CICR_NBR_OPR_STRS_CI + Len.MUA02_CICR_NBR_OPR_STRS_CI;
		public static final int MUA02_CICR_NBR_OPR_STRS = MUA02_CICR_NBR_OPR_STRS_NI + Len.MUA02_CICR_NBR_OPR_STRS_NI;
		public static final int MUA02_USER_ID_CI = MUA02_CICR_NBR_OPR_STRS + Len.MUA02_CICR_NBR_OPR_STRS;
		public static final int MUA02_USER_ID = MUA02_USER_ID_CI + Len.MUA02_USER_ID_CI;
		public static final int MUA02_STATUS_CD_CI = MUA02_USER_ID + Len.MUA02_USER_ID;
		public static final int MUA02_STATUS_CD = MUA02_STATUS_CD_CI + Len.MUA02_STATUS_CD_CI;
		public static final int MUA02_TERMINAL_ID_CI = MUA02_STATUS_CD + Len.MUA02_STATUS_CD;
		public static final int MUA02_TERMINAL_ID = MUA02_TERMINAL_ID_CI + Len.MUA02_TERMINAL_ID_CI;
		public static final int MUA02_CICR_EFF_ACY_TS_CI = MUA02_TERMINAL_ID + Len.MUA02_TERMINAL_ID;
		public static final int MUA02_CICR_EFF_ACY_TS = MUA02_CICR_EFF_ACY_TS_CI + Len.MUA02_CICR_EFF_ACY_TS_CI;
		public static final int MUA02_CICR_EXP_ACY_TS_CI = MUA02_CICR_EFF_ACY_TS + Len.MUA02_CICR_EFF_ACY_TS;
		public static final int MUA02_CICR_EXP_ACY_TS = MUA02_CICR_EXP_ACY_TS_CI + Len.MUA02_CICR_EXP_ACY_TS_CI;
		public static final int MUA02_CICR_LEG_ENT_CD_CI = MUA02_CICR_EXP_ACY_TS + Len.MUA02_CICR_EXP_ACY_TS;
		public static final int MUA02_CICR_LEG_ENT_CD = MUA02_CICR_LEG_ENT_CD_CI + Len.MUA02_CICR_LEG_ENT_CD_CI;
		public static final int MUA02_CICR_FST_NM_CI = MUA02_CICR_LEG_ENT_CD + Len.MUA02_CICR_LEG_ENT_CD;
		public static final int MUA02_CICR_FST_NM = MUA02_CICR_FST_NM_CI + Len.MUA02_CICR_FST_NM_CI;
		public static final int MUA02_CICR_LST_NM_CI = MUA02_CICR_FST_NM + Len.MUA02_CICR_FST_NM;
		public static final int MUA02_CICR_LST_NM = MUA02_CICR_LST_NM_CI + Len.MUA02_CICR_LST_NM_CI;
		public static final int MUA02_CICR_MDL_NM_CI = MUA02_CICR_LST_NM + Len.MUA02_CICR_LST_NM;
		public static final int MUA02_CICR_MDL_NM = MUA02_CICR_MDL_NM_CI + Len.MUA02_CICR_MDL_NM_CI;
		public static final int MUA02_NM_PFX_CI = MUA02_CICR_MDL_NM + Len.MUA02_CICR_MDL_NM;
		public static final int MUA02_NM_PFX = MUA02_NM_PFX_CI + Len.MUA02_NM_PFX_CI;
		public static final int MUA02_NM_SFX_CI = MUA02_NM_PFX + Len.MUA02_NM_PFX;
		public static final int MUA02_NM_SFX = MUA02_NM_SFX_CI + Len.MUA02_NM_SFX_CI;
		public static final int MUA02_CICR_LNG_NM_CI = MUA02_NM_SFX + Len.MUA02_NM_SFX;
		public static final int MUA02_CICR_LNG_NM_NI = MUA02_CICR_LNG_NM_CI + Len.MUA02_CICR_LNG_NM_CI;
		public static final int MUA02_CICR_LNG_NM = MUA02_CICR_LNG_NM_NI + Len.MUA02_CICR_LNG_NM_NI;
		public static final int MUA02_CICR_REL_CHG_SW = MUA02_CICR_LNG_NM + Len.MUA02_CICR_LNG_NM;
		public static final int MUA02_CLIENT_DES = MUA02_CICR_REL_CHG_SW + Len.MUA02_CICR_REL_CHG_SW;
		public static final int L_FW_RESP_FED_SEG_INFO = cw48rTobCdDesc(L_FW_RESP_FED_BUSINESS_TYP_MAXOCCURS - 1) + Len.CW48R_TOB_CD_DESC;
		public static final int CW49R_FED_SEG_INFO_ROW = L_FW_RESP_FED_SEG_INFO;
		public static final int CW49R_FED_SEG_INFO_FIXED = CW49R_FED_SEG_INFO_ROW;
		public static final int CW49R_FED_SEG_INFO_CSUM = CW49R_FED_SEG_INFO_FIXED;
		public static final int CW49R_CLIENT_ID_KCRE = CW49R_FED_SEG_INFO_CSUM + Len.CW49R_FED_SEG_INFO_CSUM;
		public static final int CW49R_HISTORY_VLD_NBR_KCRE = CW49R_CLIENT_ID_KCRE + Len.CW49R_CLIENT_ID_KCRE;
		public static final int CW49R_EFFECTIVE_DT_KCRE = CW49R_HISTORY_VLD_NBR_KCRE + Len.CW49R_HISTORY_VLD_NBR_KCRE;
		public static final int CW49R_FED_SEG_INFO_DATES = CW49R_EFFECTIVE_DT_KCRE + Len.CW49R_EFFECTIVE_DT_KCRE;
		public static final int CW49R_TRANS_PROCESS_DT = CW49R_FED_SEG_INFO_DATES;
		public static final int CW49R_FED_SEG_INFO_KEY = CW49R_TRANS_PROCESS_DT + Len.CW49R_TRANS_PROCESS_DT;
		public static final int CW49R_CLIENT_ID = CW49R_FED_SEG_INFO_KEY;
		public static final int CW49R_HISTORY_VLD_NBR_SIGNED = CW49R_CLIENT_ID + Len.CW49R_CLIENT_ID;
		public static final int CW49R_EFFECTIVE_DT = CW49R_HISTORY_VLD_NBR_SIGNED + Len.CW49R_HISTORY_VLD_NBR_SIGNED;
		public static final int CW49R_FED_SEG_INFO_KEY_CI = CW49R_EFFECTIVE_DT + Len.CW49R_EFFECTIVE_DT;
		public static final int CW49R_CLIENT_ID_CI = CW49R_FED_SEG_INFO_KEY_CI;
		public static final int CW49R_HISTORY_VLD_NBR_CI = CW49R_CLIENT_ID_CI + Len.CW49R_CLIENT_ID_CI;
		public static final int CW49R_EFFECTIVE_DT_CI = CW49R_HISTORY_VLD_NBR_CI + Len.CW49R_HISTORY_VLD_NBR_CI;
		public static final int CW49R_FED_SEG_INFO_DATA = CW49R_EFFECTIVE_DT_CI + Len.CW49R_EFFECTIVE_DT_CI;
		public static final int CW49R_SEG_CD_CI = CW49R_FED_SEG_INFO_DATA;
		public static final int CW49R_SEG_CD = CW49R_SEG_CD_CI + Len.CW49R_SEG_CD_CI;
		public static final int CW49R_USER_ID_CI = CW49R_SEG_CD + Len.CW49R_SEG_CD;
		public static final int CW49R_USER_ID = CW49R_USER_ID_CI + Len.CW49R_USER_ID_CI;
		public static final int CW49R_STATUS_CD_CI = CW49R_USER_ID + Len.CW49R_USER_ID;
		public static final int CW49R_STATUS_CD = CW49R_STATUS_CD_CI + Len.CW49R_STATUS_CD_CI;
		public static final int CW49R_TERMINAL_ID_CI = CW49R_STATUS_CD + Len.CW49R_STATUS_CD;
		public static final int CW49R_TERMINAL_ID = CW49R_TERMINAL_ID_CI + Len.CW49R_TERMINAL_ID_CI;
		public static final int CW49R_EXPIRATION_DT_CI = CW49R_TERMINAL_ID + Len.CW49R_TERMINAL_ID;
		public static final int CW49R_EXPIRATION_DT = CW49R_EXPIRATION_DT_CI + Len.CW49R_EXPIRATION_DT_CI;
		public static final int CW49R_EFFECTIVE_ACY_TS_CI = CW49R_EXPIRATION_DT + Len.CW49R_EXPIRATION_DT;
		public static final int CW49R_EFFECTIVE_ACY_TS = CW49R_EFFECTIVE_ACY_TS_CI + Len.CW49R_EFFECTIVE_ACY_TS_CI;
		public static final int CW49R_EXPIRATION_ACY_TS_CI = CW49R_EFFECTIVE_ACY_TS + Len.CW49R_EFFECTIVE_ACY_TS;
		public static final int CW49R_EXPIRATION_ACY_TS_NI = CW49R_EXPIRATION_ACY_TS_CI + Len.CW49R_EXPIRATION_ACY_TS_CI;
		public static final int CW49R_EXPIRATION_ACY_TS = CW49R_EXPIRATION_ACY_TS_NI + Len.CW49R_EXPIRATION_ACY_TS_NI;
		public static final int CW49R_SEG_DES = CW49R_EXPIRATION_ACY_TS + Len.CW49R_EXPIRATION_ACY_TS;
		public static final int L_FW_RESP_FED_ACT_INFO = CW49R_SEG_DES + Len.CW49R_SEG_DES;
		public static final int CW40R_FED_ACCOUNT_INFO_ROW = L_FW_RESP_FED_ACT_INFO;
		public static final int CW40R_FED_ACCOUNT_INFO_FIXED = CW40R_FED_ACCOUNT_INFO_ROW;
		public static final int CW40R_FED_ACCOUNT_INFO_CSUM = CW40R_FED_ACCOUNT_INFO_FIXED;
		public static final int CW40R_CIAI_ACT_TCH_KEY_KCRE = CW40R_FED_ACCOUNT_INFO_CSUM + Len.CW40R_FED_ACCOUNT_INFO_CSUM;
		public static final int CW40R_HISTORY_VLD_NBR_KCRE = CW40R_CIAI_ACT_TCH_KEY_KCRE + Len.CW40R_CIAI_ACT_TCH_KEY_KCRE;
		public static final int CW40R_EFFECTIVE_DT_KCRE = CW40R_HISTORY_VLD_NBR_KCRE + Len.CW40R_HISTORY_VLD_NBR_KCRE;
		public static final int CW40R_FED_ACCOUNT_INFO_DATES = CW40R_EFFECTIVE_DT_KCRE + Len.CW40R_EFFECTIVE_DT_KCRE;
		public static final int CW40R_TRANS_PROCESS_DT = CW40R_FED_ACCOUNT_INFO_DATES;
		public static final int CW40R_FED_ACCOUNT_INFO_KEY = CW40R_TRANS_PROCESS_DT + Len.CW40R_TRANS_PROCESS_DT;
		public static final int CW40R_CIAI_ACT_TCH_KEY = CW40R_FED_ACCOUNT_INFO_KEY;
		public static final int CW40R_HISTORY_VLD_NBR_SIGN = CW40R_CIAI_ACT_TCH_KEY + Len.CW40R_CIAI_ACT_TCH_KEY;
		public static final int CW40R_HISTORY_VLD_NBR = CW40R_HISTORY_VLD_NBR_SIGN + Len.CW40R_HISTORY_VLD_NBR_SIGN;
		public static final int CW40R_EFFECTIVE_DT = CW40R_HISTORY_VLD_NBR + Len.CW40R_HISTORY_VLD_NBR;
		public static final int CW40R_FED_ACCOUNT_INFO_KEY_CI = CW40R_EFFECTIVE_DT + Len.CW40R_EFFECTIVE_DT;
		public static final int CW40R_CIAI_ACT_TCH_KEY_CI = CW40R_FED_ACCOUNT_INFO_KEY_CI;
		public static final int CW40R_HISTORY_VLD_NBR_CI = CW40R_CIAI_ACT_TCH_KEY_CI + Len.CW40R_CIAI_ACT_TCH_KEY_CI;
		public static final int CW40R_EFFECTIVE_DT_CI = CW40R_HISTORY_VLD_NBR_CI + Len.CW40R_HISTORY_VLD_NBR_CI;
		public static final int CW40R_FED_ACCOUNT_INFO_DATA = CW40R_EFFECTIVE_DT_CI + Len.CW40R_EFFECTIVE_DT_CI;
		public static final int CW40R_OFC_LOC_CD_CI = CW40R_FED_ACCOUNT_INFO_DATA;
		public static final int CW40R_OFC_LOC_CD = CW40R_OFC_LOC_CD_CI + Len.CW40R_OFC_LOC_CD_CI;
		public static final int CW40R_CIAI_AUTHNTIC_LOSS_CI = CW40R_OFC_LOC_CD + Len.CW40R_OFC_LOC_CD;
		public static final int CW40R_CIAI_AUTHNTIC_LOSS = CW40R_CIAI_AUTHNTIC_LOSS_CI + Len.CW40R_CIAI_AUTHNTIC_LOSS_CI;
		public static final int CW40R_FED_ST_CD_CI = CW40R_CIAI_AUTHNTIC_LOSS + Len.CW40R_CIAI_AUTHNTIC_LOSS;
		public static final int CW40R_FED_ST_CD = CW40R_FED_ST_CD_CI + Len.CW40R_FED_ST_CD_CI;
		public static final int CW40R_FED_COUNTY_CD_CI = CW40R_FED_ST_CD + Len.CW40R_FED_ST_CD;
		public static final int CW40R_FED_COUNTY_CD = CW40R_FED_COUNTY_CD_CI + Len.CW40R_FED_COUNTY_CD_CI;
		public static final int CW40R_FED_TOWN_CD_CI = CW40R_FED_COUNTY_CD + Len.CW40R_FED_COUNTY_CD;
		public static final int CW40R_FED_TOWN_CD = CW40R_FED_TOWN_CD_CI + Len.CW40R_FED_TOWN_CD_CI;
		public static final int CW40R_USER_ID_CI = CW40R_FED_TOWN_CD + Len.CW40R_FED_TOWN_CD;
		public static final int CW40R_USER_ID = CW40R_USER_ID_CI + Len.CW40R_USER_ID_CI;
		public static final int CW40R_STATUS_CD_CI = CW40R_USER_ID + Len.CW40R_USER_ID;
		public static final int CW40R_STATUS_CD = CW40R_STATUS_CD_CI + Len.CW40R_STATUS_CD_CI;
		public static final int CW40R_TERMINAL_ID_CI = CW40R_STATUS_CD + Len.CW40R_STATUS_CD;
		public static final int CW40R_TERMINAL_ID = CW40R_TERMINAL_ID_CI + Len.CW40R_TERMINAL_ID_CI;
		public static final int CW40R_EXPIRATION_DT_CI = CW40R_TERMINAL_ID + Len.CW40R_TERMINAL_ID;
		public static final int CW40R_EXPIRATION_DT = CW40R_EXPIRATION_DT_CI + Len.CW40R_EXPIRATION_DT_CI;
		public static final int CW40R_EFFECTIVE_ACY_TS_CI = CW40R_EXPIRATION_DT + Len.CW40R_EXPIRATION_DT;
		public static final int CW40R_EFFECTIVE_ACY_TS = CW40R_EFFECTIVE_ACY_TS_CI + Len.CW40R_EFFECTIVE_ACY_TS_CI;
		public static final int CW40R_EXPIRATION_ACY_TS_CI = CW40R_EFFECTIVE_ACY_TS + Len.CW40R_EFFECTIVE_ACY_TS;
		public static final int CW40R_EXPIRATION_ACY_TS_NI = CW40R_EXPIRATION_ACY_TS_CI + Len.CW40R_EXPIRATION_ACY_TS_CI;
		public static final int CW40R_EXPIRATION_ACY_TS = CW40R_EXPIRATION_ACY_TS_NI + Len.CW40R_EXPIRATION_ACY_TS_NI;
		public static final int L_FW_RESP_BUSINESS_CLT = CW40R_EXPIRATION_ACY_TS + Len.CW40R_EXPIRATION_ACY_TS;
		public static final int CW01R_BUSINESS_CLIENT_ROW = L_FW_RESP_BUSINESS_CLT;
		public static final int CW01R_BUSINESS_CLIENT_FIXED = CW01R_BUSINESS_CLIENT_ROW;
		public static final int CW01R_BUSINESS_CLIENT_CHK_SUM = CW01R_BUSINESS_CLIENT_FIXED;
		public static final int CW01R_CLIENT_ID_KCRE = CW01R_BUSINESS_CLIENT_CHK_SUM + Len.CW01R_BUSINESS_CLIENT_CHK_SUM;
		public static final int CW01R_CIBC_BUS_SEQ_NBR_KCRE = CW01R_CLIENT_ID_KCRE + Len.CW01R_CLIENT_ID_KCRE;
		public static final int CW01R_BUSINESS_CLIENT_DATES = CW01R_CIBC_BUS_SEQ_NBR_KCRE + Len.CW01R_CIBC_BUS_SEQ_NBR_KCRE;
		public static final int CW01R_TRANS_PROCESS_DT = CW01R_BUSINESS_CLIENT_DATES;
		public static final int CW01R_BUSINESS_CLIENT_KEY = CW01R_TRANS_PROCESS_DT + Len.CW01R_TRANS_PROCESS_DT;
		public static final int CW01R_CLIENT_ID_CI = CW01R_BUSINESS_CLIENT_KEY;
		public static final int CW01R_CLIENT_ID = CW01R_CLIENT_ID_CI + Len.CW01R_CLIENT_ID_CI;
		public static final int CW01R_HISTORY_VLD_NBR_CI = CW01R_CLIENT_ID + Len.CW01R_CLIENT_ID;
		public static final int CW01R_HISTORY_VLD_NBR_SIGN = CW01R_HISTORY_VLD_NBR_CI + Len.CW01R_HISTORY_VLD_NBR_CI;
		public static final int CW01R_HISTORY_VLD_NBR = CW01R_HISTORY_VLD_NBR_SIGN + Len.CW01R_HISTORY_VLD_NBR_SIGN;
		public static final int CW01R_CIBC_BUS_SEQ_NBR_CI = CW01R_HISTORY_VLD_NBR + Len.CW01R_HISTORY_VLD_NBR;
		public static final int CW01R_CIBC_BUS_SEQ_NBR_SIGN = CW01R_CIBC_BUS_SEQ_NBR_CI + Len.CW01R_CIBC_BUS_SEQ_NBR_CI;
		public static final int CW01R_CIBC_BUS_SEQ_NBR = CW01R_CIBC_BUS_SEQ_NBR_SIGN + Len.CW01R_CIBC_BUS_SEQ_NBR_SIGN;
		public static final int CW01R_CIBC_EFF_DT_CI = CW01R_CIBC_BUS_SEQ_NBR + Len.CW01R_CIBC_BUS_SEQ_NBR;
		public static final int CW01R_CIBC_EFF_DT = CW01R_CIBC_EFF_DT_CI + Len.CW01R_CIBC_EFF_DT_CI;
		public static final int CW01R_BUSINESS_CLIENT_DATA = CW01R_CIBC_EFF_DT + Len.CW01R_CIBC_EFF_DT;
		public static final int CW01R_CIBC_EXP_DT_CI = CW01R_BUSINESS_CLIENT_DATA;
		public static final int CW01R_CIBC_EXP_DT = CW01R_CIBC_EXP_DT_CI + Len.CW01R_CIBC_EXP_DT_CI;
		public static final int CW01R_GRS_REV_CD_CI = CW01R_CIBC_EXP_DT + Len.CW01R_CIBC_EXP_DT;
		public static final int CW01R_GRS_REV_CD = CW01R_GRS_REV_CD_CI + Len.CW01R_GRS_REV_CD_CI;
		public static final int CW01R_IDY_TYP_CD_CI = CW01R_GRS_REV_CD + Len.CW01R_GRS_REV_CD;
		public static final int CW01R_IDY_TYP_CD = CW01R_IDY_TYP_CD_CI + Len.CW01R_IDY_TYP_CD_CI;
		public static final int CW01R_CIBC_NBR_EMP_CI = CW01R_IDY_TYP_CD + Len.CW01R_IDY_TYP_CD;
		public static final int CW01R_CIBC_NBR_EMP_NI = CW01R_CIBC_NBR_EMP_CI + Len.CW01R_CIBC_NBR_EMP_CI;
		public static final int CW01R_CIBC_NBR_EMP_SIGN = CW01R_CIBC_NBR_EMP_NI + Len.CW01R_CIBC_NBR_EMP_NI;
		public static final int CW01R_CIBC_NBR_EMP = CW01R_CIBC_NBR_EMP_SIGN + Len.CW01R_CIBC_NBR_EMP_SIGN;
		public static final int CW01R_CIBC_STR_DT_CI = CW01R_CIBC_NBR_EMP + Len.CW01R_CIBC_NBR_EMP;
		public static final int CW01R_CIBC_STR_DT_NI = CW01R_CIBC_STR_DT_CI + Len.CW01R_CIBC_STR_DT_CI;
		public static final int CW01R_CIBC_STR_DT = CW01R_CIBC_STR_DT_NI + Len.CW01R_CIBC_STR_DT_NI;
		public static final int CW01R_USER_ID_CI = CW01R_CIBC_STR_DT + Len.CW01R_CIBC_STR_DT;
		public static final int CW01R_USER_ID = CW01R_USER_ID_CI + Len.CW01R_USER_ID_CI;
		public static final int CW01R_STATUS_CD_CI = CW01R_USER_ID + Len.CW01R_USER_ID;
		public static final int CW01R_STATUS_CD = CW01R_STATUS_CD_CI + Len.CW01R_STATUS_CD_CI;
		public static final int CW01R_TERMINAL_ID_CI = CW01R_STATUS_CD + Len.CW01R_STATUS_CD;
		public static final int CW01R_TERMINAL_ID = CW01R_TERMINAL_ID_CI + Len.CW01R_TERMINAL_ID_CI;
		public static final int CW01R_CIBC_EFF_ACY_TS_CI = CW01R_TERMINAL_ID + Len.CW01R_TERMINAL_ID;
		public static final int CW01R_CIBC_EFF_ACY_TS = CW01R_CIBC_EFF_ACY_TS_CI + Len.CW01R_CIBC_EFF_ACY_TS_CI;
		public static final int CW01R_CIBC_EXP_ACY_TS_CI = CW01R_CIBC_EFF_ACY_TS + Len.CW01R_CIBC_EFF_ACY_TS;
		public static final int CW01R_CIBC_EXP_ACY_TS = CW01R_CIBC_EXP_ACY_TS_CI + Len.CW01R_CIBC_EXP_ACY_TS_CI;
		public static final int L_FW_RESP_REL_CLT_OBJ_REL = CW01R_CIBC_EXP_ACY_TS + Len.CW01R_CIBC_EXP_ACY_TS;
		public static final int CWORC_CLT_OBJ_RELATION_ROW = L_FW_RESP_REL_CLT_OBJ_REL;
		public static final int CWORC_CLT_OBJ_RELATION_FIXED = CWORC_CLT_OBJ_RELATION_ROW;
		public static final int CWORC_CLT_OBJ_RELATION_CSUM = CWORC_CLT_OBJ_RELATION_FIXED;
		public static final int CWORC_TCH_OBJECT_KEY_KCRE = CWORC_CLT_OBJ_RELATION_CSUM + Len.CWORC_CLT_OBJ_RELATION_CSUM;
		public static final int CWORC_HISTORY_VLD_NBR_KCRE = CWORC_TCH_OBJECT_KEY_KCRE + Len.CWORC_TCH_OBJECT_KEY_KCRE;
		public static final int CWORC_CIOR_EFF_DT_KCRE = CWORC_HISTORY_VLD_NBR_KCRE + Len.CWORC_HISTORY_VLD_NBR_KCRE;
		public static final int CWORC_OBJ_SYS_ID_KCRE = CWORC_CIOR_EFF_DT_KCRE + Len.CWORC_CIOR_EFF_DT_KCRE;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_KCRE = CWORC_OBJ_SYS_ID_KCRE + Len.CWORC_OBJ_SYS_ID_KCRE;
		public static final int CWORC_CLIENT_ID_KCRE = CWORC_CIOR_OBJ_SEQ_NBR_KCRE + Len.CWORC_CIOR_OBJ_SEQ_NBR_KCRE;
		public static final int CWORC_CLT_OBJ_RELATION_DATES = CWORC_CLIENT_ID_KCRE + Len.CWORC_CLIENT_ID_KCRE;
		public static final int CWORC_TRANS_PROCESS_DT = CWORC_CLT_OBJ_RELATION_DATES;
		public static final int CWORC_CLT_OBJ_RELATION_KEY = CWORC_TRANS_PROCESS_DT + Len.CWORC_TRANS_PROCESS_DT;
		public static final int CWORC_TCH_OBJECT_KEY_CI = CWORC_CLT_OBJ_RELATION_KEY;
		public static final int CWORC_TCH_OBJECT_KEY = CWORC_TCH_OBJECT_KEY_CI + Len.CWORC_TCH_OBJECT_KEY_CI;
		public static final int CWORC_HISTORY_VLD_NBR_CI = CWORC_TCH_OBJECT_KEY + Len.CWORC_TCH_OBJECT_KEY;
		public static final int CWORC_HISTORY_VLD_NBR_SIGN = CWORC_HISTORY_VLD_NBR_CI + Len.CWORC_HISTORY_VLD_NBR_CI;
		public static final int CWORC_HISTORY_VLD_NBR = CWORC_HISTORY_VLD_NBR_SIGN + Len.CWORC_HISTORY_VLD_NBR_SIGN;
		public static final int CWORC_CIOR_EFF_DT_CI = CWORC_HISTORY_VLD_NBR + Len.CWORC_HISTORY_VLD_NBR;
		public static final int CWORC_CIOR_EFF_DT = CWORC_CIOR_EFF_DT_CI + Len.CWORC_CIOR_EFF_DT_CI;
		public static final int CWORC_OBJ_SYS_ID_CI = CWORC_CIOR_EFF_DT + Len.CWORC_CIOR_EFF_DT;
		public static final int CWORC_OBJ_SYS_ID = CWORC_OBJ_SYS_ID_CI + Len.CWORC_OBJ_SYS_ID_CI;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_CI = CWORC_OBJ_SYS_ID + Len.CWORC_OBJ_SYS_ID;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_SIGN = CWORC_CIOR_OBJ_SEQ_NBR_CI + Len.CWORC_CIOR_OBJ_SEQ_NBR_CI;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR = CWORC_CIOR_OBJ_SEQ_NBR_SIGN + Len.CWORC_CIOR_OBJ_SEQ_NBR_SIGN;
		public static final int CWORC_CLT_OBJ_RELATION_DATA = CWORC_CIOR_OBJ_SEQ_NBR + Len.CWORC_CIOR_OBJ_SEQ_NBR;
		public static final int CWORC_CLIENT_ID_CI = CWORC_CLT_OBJ_RELATION_DATA;
		public static final int CWORC_CLIENT_ID = CWORC_CLIENT_ID_CI + Len.CWORC_CLIENT_ID_CI;
		public static final int CWORC_RLT_TYP_CD_CI = CWORC_CLIENT_ID + Len.CWORC_CLIENT_ID;
		public static final int CWORC_RLT_TYP_CD = CWORC_RLT_TYP_CD_CI + Len.CWORC_RLT_TYP_CD_CI;
		public static final int CWORC_OBJ_CD_CI = CWORC_RLT_TYP_CD + Len.CWORC_RLT_TYP_CD;
		public static final int CWORC_OBJ_CD = CWORC_OBJ_CD_CI + Len.CWORC_OBJ_CD_CI;
		public static final int CWORC_CIOR_SHW_OBJ_KEY_CI = CWORC_OBJ_CD + Len.CWORC_OBJ_CD;
		public static final int CWORC_CIOR_SHW_OBJ_KEY = CWORC_CIOR_SHW_OBJ_KEY_CI + Len.CWORC_CIOR_SHW_OBJ_KEY_CI;
		public static final int CWORC_ADR_SEQ_NBR_CI = CWORC_CIOR_SHW_OBJ_KEY + Len.CWORC_CIOR_SHW_OBJ_KEY;
		public static final int CWORC_ADR_SEQ_NBR_SIGN = CWORC_ADR_SEQ_NBR_CI + Len.CWORC_ADR_SEQ_NBR_CI;
		public static final int CWORC_ADR_SEQ_NBR = CWORC_ADR_SEQ_NBR_SIGN + Len.CWORC_ADR_SEQ_NBR_SIGN;
		public static final int CWORC_USER_ID_CI = CWORC_ADR_SEQ_NBR + Len.CWORC_ADR_SEQ_NBR;
		public static final int CWORC_USER_ID = CWORC_USER_ID_CI + Len.CWORC_USER_ID_CI;
		public static final int CWORC_STATUS_CD_CI = CWORC_USER_ID + Len.CWORC_USER_ID;
		public static final int CWORC_STATUS_CD = CWORC_STATUS_CD_CI + Len.CWORC_STATUS_CD_CI;
		public static final int CWORC_TERMINAL_ID_CI = CWORC_STATUS_CD + Len.CWORC_STATUS_CD;
		public static final int CWORC_TERMINAL_ID = CWORC_TERMINAL_ID_CI + Len.CWORC_TERMINAL_ID_CI;
		public static final int CWORC_CIOR_EXP_DT_CI = CWORC_TERMINAL_ID + Len.CWORC_TERMINAL_ID;
		public static final int CWORC_CIOR_EXP_DT = CWORC_CIOR_EXP_DT_CI + Len.CWORC_CIOR_EXP_DT_CI;
		public static final int CWORC_CIOR_EFF_ACY_TS_CI = CWORC_CIOR_EXP_DT + Len.CWORC_CIOR_EXP_DT;
		public static final int CWORC_CIOR_EFF_ACY_TS = CWORC_CIOR_EFF_ACY_TS_CI + Len.CWORC_CIOR_EFF_ACY_TS_CI;
		public static final int CWORC_CIOR_EXP_ACY_TS_CI = CWORC_CIOR_EFF_ACY_TS + Len.CWORC_CIOR_EFF_ACY_TS;
		public static final int CWORC_CIOR_EXP_ACY_TS = CWORC_CIOR_EXP_ACY_TS_CI + Len.CWORC_CIOR_EXP_ACY_TS_CI;
		public static final int CWORC_CIOR_LEG_ENT_CD_CI = CWORC_CIOR_EXP_ACY_TS + Len.CWORC_CIOR_EXP_ACY_TS;
		public static final int CWORC_CIOR_LEG_ENT_CD = CWORC_CIOR_LEG_ENT_CD_CI + Len.CWORC_CIOR_LEG_ENT_CD_CI;
		public static final int CWORC_CIOR_FST_NM_CI = CWORC_CIOR_LEG_ENT_CD + Len.CWORC_CIOR_LEG_ENT_CD;
		public static final int CWORC_CIOR_FST_NM = CWORC_CIOR_FST_NM_CI + Len.CWORC_CIOR_FST_NM_CI;
		public static final int CWORC_CIOR_LST_NM_CI = CWORC_CIOR_FST_NM + Len.CWORC_CIOR_FST_NM;
		public static final int CWORC_CIOR_LST_NM = CWORC_CIOR_LST_NM_CI + Len.CWORC_CIOR_LST_NM_CI;
		public static final int CWORC_CIOR_MDL_NM_CI = CWORC_CIOR_LST_NM + Len.CWORC_CIOR_LST_NM;
		public static final int CWORC_CIOR_MDL_NM = CWORC_CIOR_MDL_NM_CI + Len.CWORC_CIOR_MDL_NM_CI;
		public static final int CWORC_CIOR_NM_PFX_CI = CWORC_CIOR_MDL_NM + Len.CWORC_CIOR_MDL_NM;
		public static final int CWORC_CIOR_NM_PFX = CWORC_CIOR_NM_PFX_CI + Len.CWORC_CIOR_NM_PFX_CI;
		public static final int CWORC_CIOR_NM_SFX_CI = CWORC_CIOR_NM_PFX + Len.CWORC_CIOR_NM_PFX;
		public static final int CWORC_CIOR_NM_SFX = CWORC_CIOR_NM_SFX_CI + Len.CWORC_CIOR_NM_SFX_CI;
		public static final int CWORC_CIOR_LNG_NM_CI = CWORC_CIOR_NM_SFX + Len.CWORC_CIOR_NM_SFX;
		public static final int CWORC_CIOR_LNG_NM_NI = CWORC_CIOR_LNG_NM_CI + Len.CWORC_CIOR_LNG_NM_CI;
		public static final int CWORC_CIOR_LNG_NM = CWORC_CIOR_LNG_NM_NI + Len.CWORC_CIOR_LNG_NM_NI;
		public static final int CWORC_CLT_OBJ_INPUT_DATA = CWORC_CIOR_LNG_NM + Len.CWORC_CIOR_LNG_NM;
		public static final int CWORC_FILTER_TYPE = CWORC_CLT_OBJ_INPUT_DATA;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespCltAdrComposite(int idx) {
			return CW02R_LEG_ENT_DESC + Len.CW02R_LEG_ENT_DESC + idx * Len.L_FW_RESP_CLT_ADR_COMPOSITE;
		}

		public static int cwcacrClientAddrComposite(int idx) {
			return lFwRespCltAdrComposite(idx);
		}

		public static int cwcacrClientAddrCompFixed(int idx) {
			return cwcacrClientAddrComposite(idx);
		}

		public static int cwcacrClientAddrCompChksum(int idx) {
			return cwcacrClientAddrCompFixed(idx);
		}

		public static int cwcacrAdrIdKcre(int idx) {
			return cwcacrClientAddrCompChksum(idx) + Len.CWCACR_CLIENT_ADDR_COMP_CHKSUM;
		}

		public static int cwcacrClientIdKcre(int idx) {
			return cwcacrAdrIdKcre(idx) + Len.CWCACR_ADR_ID_KCRE;
		}

		public static int cwcacrAdrSeqNbrKcre(int idx) {
			return cwcacrClientIdKcre(idx) + Len.CWCACR_CLIENT_ID_KCRE;
		}

		public static int cwcacrTchObjectKeyKcre(int idx) {
			return cwcacrAdrSeqNbrKcre(idx) + Len.CWCACR_ADR_SEQ_NBR_KCRE;
		}

		public static int cwcacrClientAddressKey(int idx) {
			return cwcacrTchObjectKeyKcre(idx) + Len.CWCACR_TCH_OBJECT_KEY_KCRE;
		}

		public static int cwcacrAdrIdCi(int idx) {
			return cwcacrClientAddressKey(idx);
		}

		public static int cwcacrAdrId(int idx) {
			return cwcacrAdrIdCi(idx) + Len.CWCACR_ADR_ID_CI;
		}

		public static int cwcacrCltAdrRelationKey(int idx) {
			return cwcacrAdrId(idx) + Len.CWCACR_ADR_ID;
		}

		public static int cwcacrClientIdCi(int idx) {
			return cwcacrCltAdrRelationKey(idx);
		}

		public static int cwcacrClientId(int idx) {
			return cwcacrClientIdCi(idx) + Len.CWCACR_CLIENT_ID_CI;
		}

		public static int cwcacrHistoryVldNbrCi(int idx) {
			return cwcacrClientId(idx) + Len.CWCACR_CLIENT_ID;
		}

		public static int cwcacrHistoryVldNbrSign(int idx) {
			return cwcacrHistoryVldNbrCi(idx) + Len.CWCACR_HISTORY_VLD_NBR_CI;
		}

		public static int cwcacrHistoryVldNbr(int idx) {
			return cwcacrHistoryVldNbrSign(idx) + Len.CWCACR_HISTORY_VLD_NBR_SIGN;
		}

		public static int cwcacrAdrSeqNbrCi(int idx) {
			return cwcacrHistoryVldNbr(idx) + Len.CWCACR_HISTORY_VLD_NBR;
		}

		public static int cwcacrAdrSeqNbrSign(int idx) {
			return cwcacrAdrSeqNbrCi(idx) + Len.CWCACR_ADR_SEQ_NBR_CI;
		}

		public static int cwcacrAdrSeqNbr(int idx) {
			return cwcacrAdrSeqNbrSign(idx) + Len.CWCACR_ADR_SEQ_NBR_SIGN;
		}

		public static int cwcacrCiarEffDtCi(int idx) {
			return cwcacrAdrSeqNbr(idx) + Len.CWCACR_ADR_SEQ_NBR;
		}

		public static int cwcacrCiarEffDt(int idx) {
			return cwcacrCiarEffDtCi(idx) + Len.CWCACR_CIAR_EFF_DT_CI;
		}

		public static int cwcacrClientAddrCompDates(int idx) {
			return cwcacrCiarEffDt(idx) + Len.CWCACR_CIAR_EFF_DT;
		}

		public static int cwcacrTransProcessDt(int idx) {
			return cwcacrClientAddrCompDates(idx);
		}

		public static int cwcacrClientAddrCompData(int idx) {
			return cwcacrTransProcessDt(idx) + Len.CWCACR_TRANS_PROCESS_DT;
		}

		public static int cwcacrUserIdCi(int idx) {
			return cwcacrClientAddrCompData(idx);
		}

		public static int cwcacrUserId(int idx) {
			return cwcacrUserIdCi(idx) + Len.CWCACR_USER_ID_CI;
		}

		public static int cwcacrTerminalIdCi(int idx) {
			return cwcacrUserId(idx) + Len.CWCACR_USER_ID;
		}

		public static int cwcacrTerminalId(int idx) {
			return cwcacrTerminalIdCi(idx) + Len.CWCACR_TERMINAL_ID_CI;
		}

		public static int cwcacrCicaAdr1Ci(int idx) {
			return cwcacrTerminalId(idx) + Len.CWCACR_TERMINAL_ID;
		}

		public static int cwcacrCicaAdr1(int idx) {
			return cwcacrCicaAdr1Ci(idx) + Len.CWCACR_CICA_ADR1_CI;
		}

		public static int cwcacrCicaAdr2Ci(int idx) {
			return cwcacrCicaAdr1(idx) + Len.CWCACR_CICA_ADR1;
		}

		public static int cwcacrCicaAdr2Ni(int idx) {
			return cwcacrCicaAdr2Ci(idx) + Len.CWCACR_CICA_ADR2_CI;
		}

		public static int cwcacrCicaAdr2(int idx) {
			return cwcacrCicaAdr2Ni(idx) + Len.CWCACR_CICA_ADR2_NI;
		}

		public static int cwcacrCicaCitNmCi(int idx) {
			return cwcacrCicaAdr2(idx) + Len.CWCACR_CICA_ADR2;
		}

		public static int cwcacrCicaCitNm(int idx) {
			return cwcacrCicaCitNmCi(idx) + Len.CWCACR_CICA_CIT_NM_CI;
		}

		public static int cwcacrCicaCtyCi(int idx) {
			return cwcacrCicaCitNm(idx) + Len.CWCACR_CICA_CIT_NM;
		}

		public static int cwcacrCicaCtyNi(int idx) {
			return cwcacrCicaCtyCi(idx) + Len.CWCACR_CICA_CTY_CI;
		}

		public static int cwcacrCicaCty(int idx) {
			return cwcacrCicaCtyNi(idx) + Len.CWCACR_CICA_CTY_NI;
		}

		public static int cwcacrStCdCi(int idx) {
			return cwcacrCicaCty(idx) + Len.CWCACR_CICA_CTY;
		}

		public static int cwcacrStCd(int idx) {
			return cwcacrStCdCi(idx) + Len.CWCACR_ST_CD_CI;
		}

		public static int cwcacrCicaPstCdCi(int idx) {
			return cwcacrStCd(idx) + Len.CWCACR_ST_CD;
		}

		public static int cwcacrCicaPstCd(int idx) {
			return cwcacrCicaPstCdCi(idx) + Len.CWCACR_CICA_PST_CD_CI;
		}

		public static int cwcacrCtrCdCi(int idx) {
			return cwcacrCicaPstCd(idx) + Len.CWCACR_CICA_PST_CD;
		}

		public static int cwcacrCtrCd(int idx) {
			return cwcacrCtrCdCi(idx) + Len.CWCACR_CTR_CD_CI;
		}

		public static int cwcacrCicaAddAdrIndCi(int idx) {
			return cwcacrCtrCd(idx) + Len.CWCACR_CTR_CD;
		}

		public static int cwcacrCicaAddAdrIndNi(int idx) {
			return cwcacrCicaAddAdrIndCi(idx) + Len.CWCACR_CICA_ADD_ADR_IND_CI;
		}

		public static int cwcacrCicaAddAdrInd(int idx) {
			return cwcacrCicaAddAdrIndNi(idx) + Len.CWCACR_CICA_ADD_ADR_IND_NI;
		}

		public static int cwcacrAddrStatusCdCi(int idx) {
			return cwcacrCicaAddAdrInd(idx) + Len.CWCACR_CICA_ADD_ADR_IND;
		}

		public static int cwcacrAddrStatusCd(int idx) {
			return cwcacrAddrStatusCdCi(idx) + Len.CWCACR_ADDR_STATUS_CD_CI;
		}

		public static int cwcacrAddNmIndCi(int idx) {
			return cwcacrAddrStatusCd(idx) + Len.CWCACR_ADDR_STATUS_CD;
		}

		public static int cwcacrAddNmInd(int idx) {
			return cwcacrAddNmIndCi(idx) + Len.CWCACR_ADD_NM_IND_CI;
		}

		public static int cwcacrAdrTypCdCi(int idx) {
			return cwcacrAddNmInd(idx) + Len.CWCACR_ADD_NM_IND;
		}

		public static int cwcacrAdrTypCd(int idx) {
			return cwcacrAdrTypCdCi(idx) + Len.CWCACR_ADR_TYP_CD_CI;
		}

		public static int cwcacrTchObjectKeyCi(int idx) {
			return cwcacrAdrTypCd(idx) + Len.CWCACR_ADR_TYP_CD;
		}

		public static int cwcacrTchObjectKey(int idx) {
			return cwcacrTchObjectKeyCi(idx) + Len.CWCACR_TCH_OBJECT_KEY_CI;
		}

		public static int cwcacrCiarExpDtCi(int idx) {
			return cwcacrTchObjectKey(idx) + Len.CWCACR_TCH_OBJECT_KEY;
		}

		public static int cwcacrCiarExpDt(int idx) {
			return cwcacrCiarExpDtCi(idx) + Len.CWCACR_CIAR_EXP_DT_CI;
		}

		public static int cwcacrCiarSerAdr1TxtCi(int idx) {
			return cwcacrCiarExpDt(idx) + Len.CWCACR_CIAR_EXP_DT;
		}

		public static int cwcacrCiarSerAdr1Txt(int idx) {
			return cwcacrCiarSerAdr1TxtCi(idx) + Len.CWCACR_CIAR_SER_ADR1_TXT_CI;
		}

		public static int cwcacrCiarSerCitNmCi(int idx) {
			return cwcacrCiarSerAdr1Txt(idx) + Len.CWCACR_CIAR_SER_ADR1_TXT;
		}

		public static int cwcacrCiarSerCitNm(int idx) {
			return cwcacrCiarSerCitNmCi(idx) + Len.CWCACR_CIAR_SER_CIT_NM_CI;
		}

		public static int cwcacrCiarSerStCdCi(int idx) {
			return cwcacrCiarSerCitNm(idx) + Len.CWCACR_CIAR_SER_CIT_NM;
		}

		public static int cwcacrCiarSerStCd(int idx) {
			return cwcacrCiarSerStCdCi(idx) + Len.CWCACR_CIAR_SER_ST_CD_CI;
		}

		public static int cwcacrCiarSerPstCdCi(int idx) {
			return cwcacrCiarSerStCd(idx) + Len.CWCACR_CIAR_SER_ST_CD;
		}

		public static int cwcacrCiarSerPstCd(int idx) {
			return cwcacrCiarSerPstCdCi(idx) + Len.CWCACR_CIAR_SER_PST_CD_CI;
		}

		public static int cwcacrRelStatusCdCi(int idx) {
			return cwcacrCiarSerPstCd(idx) + Len.CWCACR_CIAR_SER_PST_CD;
		}

		public static int cwcacrRelStatusCd(int idx) {
			return cwcacrRelStatusCdCi(idx) + Len.CWCACR_REL_STATUS_CD_CI;
		}

		public static int cwcacrEffAcyTsCi(int idx) {
			return cwcacrRelStatusCd(idx) + Len.CWCACR_REL_STATUS_CD;
		}

		public static int cwcacrEffAcyTs(int idx) {
			return cwcacrEffAcyTsCi(idx) + Len.CWCACR_EFF_ACY_TS_CI;
		}

		public static int cwcacrCiarExpAcyTsCi(int idx) {
			return cwcacrEffAcyTs(idx) + Len.CWCACR_EFF_ACY_TS;
		}

		public static int cwcacrCiarExpAcyTs(int idx) {
			return cwcacrCiarExpAcyTsCi(idx) + Len.CWCACR_CIAR_EXP_ACY_TS_CI;
		}

		public static int cwcacrMoreRowsSw(int idx) {
			return cwcacrCiarExpAcyTs(idx) + Len.CWCACR_CIAR_EXP_ACY_TS;
		}

		public static int cwcacrCicaShwObjKeyCi(int idx) {
			return cwcacrMoreRowsSw(idx) + Len.CWCACR_MORE_ROWS_SW;
		}

		public static int cwcacrCicaShwObjKey(int idx) {
			return cwcacrCicaShwObjKeyCi(idx) + Len.CWCACR_CICA_SHW_OBJ_KEY_CI;
		}

		public static int cwcacrBusObjNm(int idx) {
			return cwcacrCicaShwObjKey(idx) + Len.CWCACR_CICA_SHW_OBJ_KEY;
		}

		public static int cwcacrStDesc(int idx) {
			return cwcacrBusObjNm(idx) + Len.CWCACR_BUS_OBJ_NM;
		}

		public static int cwcacrAdrTypDesc(int idx) {
			return cwcacrStDesc(idx) + Len.CWCACR_ST_DESC;
		}

		public static int lFwRespClientPhone(int idx) {
			return CWEMR_EMAIL_TYPE_CD_DESC + Len.CWEMR_EMAIL_TYPE_CD_DESC + idx * Len.L_FW_RESP_CLIENT_PHONE;
		}

		public static int cw04rClientPhoneRow(int idx) {
			return lFwRespClientPhone(idx);
		}

		public static int cw04rClientPhoneFixed(int idx) {
			return cw04rClientPhoneRow(idx);
		}

		public static int cw04rClientPhoneChkSum(int idx) {
			return cw04rClientPhoneFixed(idx);
		}

		public static int cw04rClientIdKcre(int idx) {
			return cw04rClientPhoneChkSum(idx) + Len.CW04R_CLIENT_PHONE_CHK_SUM;
		}

		public static int cw04rCiphPhnSeqNbrKcre(int idx) {
			return cw04rClientIdKcre(idx) + Len.CW04R_CLIENT_ID_KCRE;
		}

		public static int cw04rClientPhoneDates(int idx) {
			return cw04rCiphPhnSeqNbrKcre(idx) + Len.CW04R_CIPH_PHN_SEQ_NBR_KCRE;
		}

		public static int cw04rTransProcessDt(int idx) {
			return cw04rClientPhoneDates(idx);
		}

		public static int cw04rClientPhoneKey(int idx) {
			return cw04rTransProcessDt(idx) + Len.CW04R_TRANS_PROCESS_DT;
		}

		public static int cw04rClientIdCi(int idx) {
			return cw04rClientPhoneKey(idx);
		}

		public static int cw04rClientId(int idx) {
			return cw04rClientIdCi(idx) + Len.CW04R_CLIENT_ID_CI;
		}

		public static int cw04rCiphPhnSeqNbrCi(int idx) {
			return cw04rClientId(idx) + Len.CW04R_CLIENT_ID;
		}

		public static int cw04rCiphPhnSeqNbrSign(int idx) {
			return cw04rCiphPhnSeqNbrCi(idx) + Len.CW04R_CIPH_PHN_SEQ_NBR_CI;
		}

		public static int cw04rCiphPhnSeqNbr(int idx) {
			return cw04rCiphPhnSeqNbrSign(idx) + Len.CW04R_CIPH_PHN_SEQ_NBR_SIGN;
		}

		public static int cw04rHistoryVldNbrCi(int idx) {
			return cw04rCiphPhnSeqNbr(idx) + Len.CW04R_CIPH_PHN_SEQ_NBR;
		}

		public static int cw04rHistoryVldNbrSign(int idx) {
			return cw04rHistoryVldNbrCi(idx) + Len.CW04R_HISTORY_VLD_NBR_CI;
		}

		public static int cw04rHistoryVldNbr(int idx) {
			return cw04rHistoryVldNbrSign(idx) + Len.CW04R_HISTORY_VLD_NBR_SIGN;
		}

		public static int cw04rCiphEffDtCi(int idx) {
			return cw04rHistoryVldNbr(idx) + Len.CW04R_HISTORY_VLD_NBR;
		}

		public static int cw04rCiphEffDt(int idx) {
			return cw04rCiphEffDtCi(idx) + Len.CW04R_CIPH_EFF_DT_CI;
		}

		public static int cw04rClientPhoneData(int idx) {
			return cw04rCiphEffDt(idx) + Len.CW04R_CIPH_EFF_DT;
		}

		public static int cw04rCiphPhnNbrCi(int idx) {
			return cw04rClientPhoneData(idx);
		}

		public static int cw04rCiphPhnNbr(int idx) {
			return cw04rCiphPhnNbrCi(idx) + Len.CW04R_CIPH_PHN_NBR_CI;
		}

		public static int cw04rPhnTypCdCi(int idx) {
			return cw04rCiphPhnNbr(idx) + Len.CW04R_CIPH_PHN_NBR;
		}

		public static int cw04rPhnTypCd(int idx) {
			return cw04rPhnTypCdCi(idx) + Len.CW04R_PHN_TYP_CD_CI;
		}

		public static int cw04rCiphXrfIdCi(int idx) {
			return cw04rPhnTypCd(idx) + Len.CW04R_PHN_TYP_CD;
		}

		public static int cw04rCiphXrfIdNi(int idx) {
			return cw04rCiphXrfIdCi(idx) + Len.CW04R_CIPH_XRF_ID_CI;
		}

		public static int cw04rCiphXrfId(int idx) {
			return cw04rCiphXrfIdNi(idx) + Len.CW04R_CIPH_XRF_ID_NI;
		}

		public static int cw04rUserIdCi(int idx) {
			return cw04rCiphXrfId(idx) + Len.CW04R_CIPH_XRF_ID;
		}

		public static int cw04rUserId(int idx) {
			return cw04rUserIdCi(idx) + Len.CW04R_USER_ID_CI;
		}

		public static int cw04rStatusCdCi(int idx) {
			return cw04rUserId(idx) + Len.CW04R_USER_ID;
		}

		public static int cw04rStatusCd(int idx) {
			return cw04rStatusCdCi(idx) + Len.CW04R_STATUS_CD_CI;
		}

		public static int cw04rTerminalIdCi(int idx) {
			return cw04rStatusCd(idx) + Len.CW04R_STATUS_CD;
		}

		public static int cw04rTerminalId(int idx) {
			return cw04rTerminalIdCi(idx) + Len.CW04R_TERMINAL_ID_CI;
		}

		public static int cw04rCiphExpDtCi(int idx) {
			return cw04rTerminalId(idx) + Len.CW04R_TERMINAL_ID;
		}

		public static int cw04rCiphExpDt(int idx) {
			return cw04rCiphExpDtCi(idx) + Len.CW04R_CIPH_EXP_DT_CI;
		}

		public static int cw04rCiphEffAcyTsCi(int idx) {
			return cw04rCiphExpDt(idx) + Len.CW04R_CIPH_EXP_DT;
		}

		public static int cw04rCiphEffAcyTs(int idx) {
			return cw04rCiphEffAcyTsCi(idx) + Len.CW04R_CIPH_EFF_ACY_TS_CI;
		}

		public static int cw04rCiphExpAcyTsCi(int idx) {
			return cw04rCiphEffAcyTs(idx) + Len.CW04R_CIPH_EFF_ACY_TS;
		}

		public static int cw04rCiphExpAcyTs(int idx) {
			return cw04rCiphExpAcyTsCi(idx) + Len.CW04R_CIPH_EXP_ACY_TS_CI;
		}

		public static int cw04rMoreRowsSw(int idx) {
			return cw04rCiphExpAcyTs(idx) + Len.CW04R_CIPH_EXP_ACY_TS;
		}

		public static int cw04rPhnTypDesc(int idx) {
			return cw04rMoreRowsSw(idx) + Len.CW04R_MORE_ROWS_SW;
		}

		public static int lFwRespCltObjRelation(int idx) {
			return cw04rPhnTypDesc(L_FW_RESP_CLIENT_PHONE_MAXOCCURS - 1) + Len.CW04R_PHN_TYP_DESC + idx * Len.L_FW_RESP_CLT_OBJ_RELATION;
		}

		public static int cw08rCltObjRelationRow(int idx) {
			return lFwRespCltObjRelation(idx);
		}

		public static int cw08rCltObjRelationFixed(int idx) {
			return cw08rCltObjRelationRow(idx);
		}

		public static int cw08rCltObjRelationCsum(int idx) {
			return cw08rCltObjRelationFixed(idx);
		}

		public static int cw08rTchObjectKeyKcre(int idx) {
			return cw08rCltObjRelationCsum(idx) + Len.CW08R_CLT_OBJ_RELATION_CSUM;
		}

		public static int cw08rHistoryVldNbrKcre(int idx) {
			return cw08rTchObjectKeyKcre(idx) + Len.CW08R_TCH_OBJECT_KEY_KCRE;
		}

		public static int cw08rCiorEffDtKcre(int idx) {
			return cw08rHistoryVldNbrKcre(idx) + Len.CW08R_HISTORY_VLD_NBR_KCRE;
		}

		public static int cw08rObjSysIdKcre(int idx) {
			return cw08rCiorEffDtKcre(idx) + Len.CW08R_CIOR_EFF_DT_KCRE;
		}

		public static int cw08rCiorObjSeqNbrKcre(int idx) {
			return cw08rObjSysIdKcre(idx) + Len.CW08R_OBJ_SYS_ID_KCRE;
		}

		public static int cw08rClientIdKcre(int idx) {
			return cw08rCiorObjSeqNbrKcre(idx) + Len.CW08R_CIOR_OBJ_SEQ_NBR_KCRE;
		}

		public static int cw08rCltObjRelationDates(int idx) {
			return cw08rClientIdKcre(idx) + Len.CW08R_CLIENT_ID_KCRE;
		}

		public static int cw08rTransProcessDt(int idx) {
			return cw08rCltObjRelationDates(idx);
		}

		public static int cw08rCltObjRelationKey(int idx) {
			return cw08rTransProcessDt(idx) + Len.CW08R_TRANS_PROCESS_DT;
		}

		public static int cw08rTchObjectKeyCi(int idx) {
			return cw08rCltObjRelationKey(idx);
		}

		public static int cw08rTchObjectKey(int idx) {
			return cw08rTchObjectKeyCi(idx) + Len.CW08R_TCH_OBJECT_KEY_CI;
		}

		public static int cw08rHistoryVldNbrCi(int idx) {
			return cw08rTchObjectKey(idx) + Len.CW08R_TCH_OBJECT_KEY;
		}

		public static int cw08rHistoryVldNbrSign(int idx) {
			return cw08rHistoryVldNbrCi(idx) + Len.CW08R_HISTORY_VLD_NBR_CI;
		}

		public static int cw08rHistoryVldNbr(int idx) {
			return cw08rHistoryVldNbrSign(idx) + Len.CW08R_HISTORY_VLD_NBR_SIGN;
		}

		public static int cw08rCiorEffDtCi(int idx) {
			return cw08rHistoryVldNbr(idx) + Len.CW08R_HISTORY_VLD_NBR;
		}

		public static int cw08rCiorEffDt(int idx) {
			return cw08rCiorEffDtCi(idx) + Len.CW08R_CIOR_EFF_DT_CI;
		}

		public static int cw08rObjSysIdCi(int idx) {
			return cw08rCiorEffDt(idx) + Len.CW08R_CIOR_EFF_DT;
		}

		public static int cw08rObjSysId(int idx) {
			return cw08rObjSysIdCi(idx) + Len.CW08R_OBJ_SYS_ID_CI;
		}

		public static int cw08rCiorObjSeqNbrCi(int idx) {
			return cw08rObjSysId(idx) + Len.CW08R_OBJ_SYS_ID;
		}

		public static int cw08rCiorObjSeqNbrSign(int idx) {
			return cw08rCiorObjSeqNbrCi(idx) + Len.CW08R_CIOR_OBJ_SEQ_NBR_CI;
		}

		public static int cw08rCiorObjSeqNbr(int idx) {
			return cw08rCiorObjSeqNbrSign(idx) + Len.CW08R_CIOR_OBJ_SEQ_NBR_SIGN;
		}

		public static int cw08rCltObjRelationData(int idx) {
			return cw08rCiorObjSeqNbr(idx) + Len.CW08R_CIOR_OBJ_SEQ_NBR;
		}

		public static int cw08rClientIdCi(int idx) {
			return cw08rCltObjRelationData(idx);
		}

		public static int cw08rClientId(int idx) {
			return cw08rClientIdCi(idx) + Len.CW08R_CLIENT_ID_CI;
		}

		public static int cw08rRltTypCdCi(int idx) {
			return cw08rClientId(idx) + Len.CW08R_CLIENT_ID;
		}

		public static int cw08rRltTypCd(int idx) {
			return cw08rRltTypCdCi(idx) + Len.CW08R_RLT_TYP_CD_CI;
		}

		public static int cw08rObjCdCi(int idx) {
			return cw08rRltTypCd(idx) + Len.CW08R_RLT_TYP_CD;
		}

		public static int cw08rObjCd(int idx) {
			return cw08rObjCdCi(idx) + Len.CW08R_OBJ_CD_CI;
		}

		public static int cw08rCiorShwObjKeyCi(int idx) {
			return cw08rObjCd(idx) + Len.CW08R_OBJ_CD;
		}

		public static int cw08rCiorShwObjKey(int idx) {
			return cw08rCiorShwObjKeyCi(idx) + Len.CW08R_CIOR_SHW_OBJ_KEY_CI;
		}

		public static int cw08rAdrSeqNbrCi(int idx) {
			return cw08rCiorShwObjKey(idx) + Len.CW08R_CIOR_SHW_OBJ_KEY;
		}

		public static int cw08rAdrSeqNbrSign(int idx) {
			return cw08rAdrSeqNbrCi(idx) + Len.CW08R_ADR_SEQ_NBR_CI;
		}

		public static int cw08rAdrSeqNbr(int idx) {
			return cw08rAdrSeqNbrSign(idx) + Len.CW08R_ADR_SEQ_NBR_SIGN;
		}

		public static int cw08rUserIdCi(int idx) {
			return cw08rAdrSeqNbr(idx) + Len.CW08R_ADR_SEQ_NBR;
		}

		public static int cw08rUserId(int idx) {
			return cw08rUserIdCi(idx) + Len.CW08R_USER_ID_CI;
		}

		public static int cw08rStatusCdCi(int idx) {
			return cw08rUserId(idx) + Len.CW08R_USER_ID;
		}

		public static int cw08rStatusCd(int idx) {
			return cw08rStatusCdCi(idx) + Len.CW08R_STATUS_CD_CI;
		}

		public static int cw08rTerminalIdCi(int idx) {
			return cw08rStatusCd(idx) + Len.CW08R_STATUS_CD;
		}

		public static int cw08rTerminalId(int idx) {
			return cw08rTerminalIdCi(idx) + Len.CW08R_TERMINAL_ID_CI;
		}

		public static int cw08rCiorExpDtCi(int idx) {
			return cw08rTerminalId(idx) + Len.CW08R_TERMINAL_ID;
		}

		public static int cw08rCiorExpDt(int idx) {
			return cw08rCiorExpDtCi(idx) + Len.CW08R_CIOR_EXP_DT_CI;
		}

		public static int cw08rCiorEffAcyTsCi(int idx) {
			return cw08rCiorExpDt(idx) + Len.CW08R_CIOR_EXP_DT;
		}

		public static int cw08rCiorEffAcyTs(int idx) {
			return cw08rCiorEffAcyTsCi(idx) + Len.CW08R_CIOR_EFF_ACY_TS_CI;
		}

		public static int cw08rCiorExpAcyTsCi(int idx) {
			return cw08rCiorEffAcyTs(idx) + Len.CW08R_CIOR_EFF_ACY_TS;
		}

		public static int cw08rCiorExpAcyTs(int idx) {
			return cw08rCiorExpAcyTsCi(idx) + Len.CW08R_CIOR_EXP_ACY_TS_CI;
		}

		public static int cw08rAppType(int idx) {
			return cw08rCiorExpAcyTs(idx) + Len.CW08R_CIOR_EXP_ACY_TS;
		}

		public static int cw08rBusObjNm(int idx) {
			return cw08rAppType(idx) + Len.CW08R_APP_TYPE;
		}

		public static int cw08rObjDesc(int idx) {
			return cw08rBusObjNm(idx) + Len.CW08R_BUS_OBJ_NM;
		}

		public static int lFwRespFedBusinessTyp(int idx) {
			return MUA02_CLIENT_DES + Len.MUA02_CLIENT_DES + idx * Len.L_FW_RESP_FED_BUSINESS_TYP;
		}

		public static int cw48rFedBusinessTypRow(int idx) {
			return lFwRespFedBusinessTyp(idx);
		}

		public static int cw48rFedBusinessTypFixed(int idx) {
			return cw48rFedBusinessTypRow(idx);
		}

		public static int cw48rFedBusinessTypCsum(int idx) {
			return cw48rFedBusinessTypFixed(idx);
		}

		public static int cw48rClientIdKcre(int idx) {
			return cw48rFedBusinessTypCsum(idx) + Len.CW48R_FED_BUSINESS_TYP_CSUM;
		}

		public static int cw48rTobCdKcre(int idx) {
			return cw48rClientIdKcre(idx) + Len.CW48R_CLIENT_ID_KCRE;
		}

		public static int cw48rHistoryVldNbrKcre(int idx) {
			return cw48rTobCdKcre(idx) + Len.CW48R_TOB_CD_KCRE;
		}

		public static int cw48rEffectiveDtKcre(int idx) {
			return cw48rHistoryVldNbrKcre(idx) + Len.CW48R_HISTORY_VLD_NBR_KCRE;
		}

		public static int cw48rFedBusinessTypDates(int idx) {
			return cw48rEffectiveDtKcre(idx) + Len.CW48R_EFFECTIVE_DT_KCRE;
		}

		public static int cw48rTransProcessDt(int idx) {
			return cw48rFedBusinessTypDates(idx);
		}

		public static int cw48rFedBusinessTypKey(int idx) {
			return cw48rTransProcessDt(idx) + Len.CW48R_TRANS_PROCESS_DT;
		}

		public static int cw48rClientId(int idx) {
			return cw48rFedBusinessTypKey(idx);
		}

		public static int cw48rTobCd(int idx) {
			return cw48rClientId(idx) + Len.CW48R_CLIENT_ID;
		}

		public static int cw48rHistoryVldNbrSign(int idx) {
			return cw48rTobCd(idx) + Len.CW48R_TOB_CD;
		}

		public static int cw48rHistoryVldNbr(int idx) {
			return cw48rHistoryVldNbrSign(idx) + Len.CW48R_HISTORY_VLD_NBR_SIGN;
		}

		public static int cw48rEffectiveDt(int idx) {
			return cw48rHistoryVldNbr(idx) + Len.CW48R_HISTORY_VLD_NBR;
		}

		public static int cw48rFedBusinessTypKeyCi(int idx) {
			return cw48rEffectiveDt(idx) + Len.CW48R_EFFECTIVE_DT;
		}

		public static int cw48rClientIdCi(int idx) {
			return cw48rFedBusinessTypKeyCi(idx);
		}

		public static int cw48rTobCdCi(int idx) {
			return cw48rClientIdCi(idx) + Len.CW48R_CLIENT_ID_CI;
		}

		public static int cw48rHistoryVldNbrCi(int idx) {
			return cw48rTobCdCi(idx) + Len.CW48R_TOB_CD_CI;
		}

		public static int cw48rEffectiveDtCi(int idx) {
			return cw48rHistoryVldNbrCi(idx) + Len.CW48R_HISTORY_VLD_NBR_CI;
		}

		public static int cw48rFedBusinessTypData(int idx) {
			return cw48rEffectiveDtCi(idx) + Len.CW48R_EFFECTIVE_DT_CI;
		}

		public static int cw48rTobSicTypCdCi(int idx) {
			return cw48rFedBusinessTypData(idx);
		}

		public static int cw48rTobSicTypCd(int idx) {
			return cw48rTobSicTypCdCi(idx) + Len.CW48R_TOB_SIC_TYP_CD_CI;
		}

		public static int cw48rTobPtyCdCi(int idx) {
			return cw48rTobSicTypCd(idx) + Len.CW48R_TOB_SIC_TYP_CD;
		}

		public static int cw48rTobPtyCd(int idx) {
			return cw48rTobPtyCdCi(idx) + Len.CW48R_TOB_PTY_CD_CI;
		}

		public static int cw48rUserIdCi(int idx) {
			return cw48rTobPtyCd(idx) + Len.CW48R_TOB_PTY_CD;
		}

		public static int cw48rUserId(int idx) {
			return cw48rUserIdCi(idx) + Len.CW48R_USER_ID_CI;
		}

		public static int cw48rStatusCdCi(int idx) {
			return cw48rUserId(idx) + Len.CW48R_USER_ID;
		}

		public static int cw48rStatusCd(int idx) {
			return cw48rStatusCdCi(idx) + Len.CW48R_STATUS_CD_CI;
		}

		public static int cw48rTerminalIdCi(int idx) {
			return cw48rStatusCd(idx) + Len.CW48R_STATUS_CD;
		}

		public static int cw48rTerminalId(int idx) {
			return cw48rTerminalIdCi(idx) + Len.CW48R_TERMINAL_ID_CI;
		}

		public static int cw48rExpirationDtCi(int idx) {
			return cw48rTerminalId(idx) + Len.CW48R_TERMINAL_ID;
		}

		public static int cw48rExpirationDt(int idx) {
			return cw48rExpirationDtCi(idx) + Len.CW48R_EXPIRATION_DT_CI;
		}

		public static int cw48rEffectiveAcyTsCi(int idx) {
			return cw48rExpirationDt(idx) + Len.CW48R_EXPIRATION_DT;
		}

		public static int cw48rEffectiveAcyTs(int idx) {
			return cw48rEffectiveAcyTsCi(idx) + Len.CW48R_EFFECTIVE_ACY_TS_CI;
		}

		public static int cw48rExpirationAcyTsCi(int idx) {
			return cw48rEffectiveAcyTs(idx) + Len.CW48R_EFFECTIVE_ACY_TS;
		}

		public static int cw48rExpirationAcyTsNi(int idx) {
			return cw48rExpirationAcyTsCi(idx) + Len.CW48R_EXPIRATION_ACY_TS_CI;
		}

		public static int cw48rExpirationAcyTs(int idx) {
			return cw48rExpirationAcyTsNi(idx) + Len.CW48R_EXPIRATION_ACY_TS_NI;
		}

		public static int cw48rTobCdDesc(int idx) {
			return cw48rExpirationAcyTs(idx) + Len.CW48R_EXPIRATION_ACY_TS;
		}

		public static int lFwRespCltCltRel(int idx) {
			return CWORC_FILTER_TYPE + Len.CWORC_FILTER_TYPE + idx * Len.L_FW_RESP_CLT_CLT_REL;
		}

		public static int cw06rCltCltRelationRow(int idx) {
			return lFwRespCltCltRel(idx);
		}

		public static int cw06rCltCltRelationFixed(int idx) {
			return cw06rCltCltRelationRow(idx);
		}

		public static int cw06rCltCltRelationCsum(int idx) {
			return cw06rCltCltRelationFixed(idx);
		}

		public static int cw06rClientIdKcre(int idx) {
			return cw06rCltCltRelationCsum(idx) + Len.CW06R_CLT_CLT_RELATION_CSUM;
		}

		public static int cw06rCltTypCdKcre(int idx) {
			return cw06rClientIdKcre(idx) + Len.CW06R_CLIENT_ID_KCRE;
		}

		public static int cw06rHistoryVldNbrKcre(int idx) {
			return cw06rCltTypCdKcre(idx) + Len.CW06R_CLT_TYP_CD_KCRE;
		}

		public static int cw06rCicrXrfIdKcre(int idx) {
			return cw06rHistoryVldNbrKcre(idx) + Len.CW06R_HISTORY_VLD_NBR_KCRE;
		}

		public static int cw06rXrfTypCdKcre(int idx) {
			return cw06rCicrXrfIdKcre(idx) + Len.CW06R_CICR_XRF_ID_KCRE;
		}

		public static int cw06rCicrEffDtKcre(int idx) {
			return cw06rXrfTypCdKcre(idx) + Len.CW06R_XRF_TYP_CD_KCRE;
		}

		public static int cw06rCltCltRelationDates(int idx) {
			return cw06rCicrEffDtKcre(idx) + Len.CW06R_CICR_EFF_DT_KCRE;
		}

		public static int cw06rTransProcessDt(int idx) {
			return cw06rCltCltRelationDates(idx);
		}

		public static int cw06rCltCltRelationKey(int idx) {
			return cw06rTransProcessDt(idx) + Len.CW06R_TRANS_PROCESS_DT;
		}

		public static int cw06rClientIdCi(int idx) {
			return cw06rCltCltRelationKey(idx);
		}

		public static int cw06rClientId(int idx) {
			return cw06rClientIdCi(idx) + Len.CW06R_CLIENT_ID_CI;
		}

		public static int cw06rCltTypCdCi(int idx) {
			return cw06rClientId(idx) + Len.CW06R_CLIENT_ID;
		}

		public static int cw06rCltTypCd(int idx) {
			return cw06rCltTypCdCi(idx) + Len.CW06R_CLT_TYP_CD_CI;
		}

		public static int cw06rHistoryVldNbrCi(int idx) {
			return cw06rCltTypCd(idx) + Len.CW06R_CLT_TYP_CD;
		}

		public static int cw06rHistoryVldNbrSigned(int idx) {
			return cw06rHistoryVldNbrCi(idx) + Len.CW06R_HISTORY_VLD_NBR_CI;
		}

		public static int cw06rCicrXrfIdCi(int idx) {
			return cw06rHistoryVldNbrSigned(idx) + Len.CW06R_HISTORY_VLD_NBR_SIGNED;
		}

		public static int cw06rCicrXrfId(int idx) {
			return cw06rCicrXrfIdCi(idx) + Len.CW06R_CICR_XRF_ID_CI;
		}

		public static int cw06rXrfTypCdCi(int idx) {
			return cw06rCicrXrfId(idx) + Len.CW06R_CICR_XRF_ID;
		}

		public static int cw06rXrfTypCd(int idx) {
			return cw06rXrfTypCdCi(idx) + Len.CW06R_XRF_TYP_CD_CI;
		}

		public static int cw06rCicrEffDtCi(int idx) {
			return cw06rXrfTypCd(idx) + Len.CW06R_XRF_TYP_CD;
		}

		public static int cw06rCicrEffDt(int idx) {
			return cw06rCicrEffDtCi(idx) + Len.CW06R_CICR_EFF_DT_CI;
		}

		public static int cw06rCltCltRelationData(int idx) {
			return cw06rCicrEffDt(idx) + Len.CW06R_CICR_EFF_DT;
		}

		public static int cw06rCicrExpDtCi(int idx) {
			return cw06rCltCltRelationData(idx);
		}

		public static int cw06rCicrExpDt(int idx) {
			return cw06rCicrExpDtCi(idx) + Len.CW06R_CICR_EXP_DT_CI;
		}

		public static int cw06rCicrNbrOprStrsCi(int idx) {
			return cw06rCicrExpDt(idx) + Len.CW06R_CICR_EXP_DT;
		}

		public static int cw06rCicrNbrOprStrsNi(int idx) {
			return cw06rCicrNbrOprStrsCi(idx) + Len.CW06R_CICR_NBR_OPR_STRS_CI;
		}

		public static int cw06rCicrNbrOprStrs(int idx) {
			return cw06rCicrNbrOprStrsNi(idx) + Len.CW06R_CICR_NBR_OPR_STRS_NI;
		}

		public static int cw06rUserIdCi(int idx) {
			return cw06rCicrNbrOprStrs(idx) + Len.CW06R_CICR_NBR_OPR_STRS;
		}

		public static int cw06rUserId(int idx) {
			return cw06rUserIdCi(idx) + Len.CW06R_USER_ID_CI;
		}

		public static int cw06rStatusCdCi(int idx) {
			return cw06rUserId(idx) + Len.CW06R_USER_ID;
		}

		public static int cw06rStatusCd(int idx) {
			return cw06rStatusCdCi(idx) + Len.CW06R_STATUS_CD_CI;
		}

		public static int cw06rTerminalIdCi(int idx) {
			return cw06rStatusCd(idx) + Len.CW06R_STATUS_CD;
		}

		public static int cw06rTerminalId(int idx) {
			return cw06rTerminalIdCi(idx) + Len.CW06R_TERMINAL_ID_CI;
		}

		public static int cw06rCicrEffAcyTsCi(int idx) {
			return cw06rTerminalId(idx) + Len.CW06R_TERMINAL_ID;
		}

		public static int cw06rCicrEffAcyTs(int idx) {
			return cw06rCicrEffAcyTsCi(idx) + Len.CW06R_CICR_EFF_ACY_TS_CI;
		}

		public static int cw06rCicrExpAcyTsCi(int idx) {
			return cw06rCicrEffAcyTs(idx) + Len.CW06R_CICR_EFF_ACY_TS;
		}

		public static int cw06rCicrExpAcyTs(int idx) {
			return cw06rCicrExpAcyTsCi(idx) + Len.CW06R_CICR_EXP_ACY_TS_CI;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CW02R_CLIENT_TAB_CHK_SUM = 9;
		public static final int CW02R_CLIENT_ID_KCRE = 32;
		public static final int CW02R_TRANS_PROCESS_DT = 10;
		public static final int CW02R_CLIENT_ID_CI = 1;
		public static final int CW02R_CLIENT_ID = 20;
		public static final int CW02R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW02R_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW02R_HISTORY_VLD_NBR = 5;
		public static final int CW02R_CICL_EFF_DT_CI = 1;
		public static final int CW02R_CICL_EFF_DT = 10;
		public static final int CW02R_CICL_PRI_SUB_CD_CI = 1;
		public static final int CW02R_CICL_PRI_SUB_CD = 4;
		public static final int CW02R_CICL_FST_NM_CI = 1;
		public static final int CW02R_CICL_FST_NM = 30;
		public static final int CW02R_CICL_LST_NM_CI = 1;
		public static final int CW02R_CICL_LST_NM = 60;
		public static final int CW02R_CICL_MDL_NM_CI = 1;
		public static final int CW02R_CICL_MDL_NM = 30;
		public static final int CW02R_NM_PFX_CI = 1;
		public static final int CW02R_NM_PFX = 4;
		public static final int CW02R_NM_SFX_CI = 1;
		public static final int CW02R_NM_SFX = 4;
		public static final int CW02R_PRIMARY_PRO_DSN_CD_CI = 1;
		public static final int CW02R_PRIMARY_PRO_DSN_CD_NI = 1;
		public static final int CW02R_PRIMARY_PRO_DSN_CD = 4;
		public static final int CW02R_LEG_ENT_CD_CI = 1;
		public static final int CW02R_LEG_ENT_CD = 2;
		public static final int CW02R_CICL_SDX_CD_CI = 1;
		public static final int CW02R_CICL_SDX_CD = 8;
		public static final int CW02R_CICL_OGN_INCEPT_DT_CI = 1;
		public static final int CW02R_CICL_OGN_INCEPT_DT_NI = 1;
		public static final int CW02R_CICL_OGN_INCEPT_DT = 10;
		public static final int CW02R_CICL_ADD_NM_IND_CI = 1;
		public static final int CW02R_CICL_ADD_NM_IND_NI = 1;
		public static final int CW02R_CICL_ADD_NM_IND = 1;
		public static final int CW02R_CICL_DOB_DT_CI = 1;
		public static final int CW02R_CICL_DOB_DT = 10;
		public static final int CW02R_CICL_BIR_ST_CD_CI = 1;
		public static final int CW02R_CICL_BIR_ST_CD = 3;
		public static final int CW02R_GENDER_CD_CI = 1;
		public static final int CW02R_GENDER_CD = 1;
		public static final int CW02R_PRI_LGG_CD_CI = 1;
		public static final int CW02R_PRI_LGG_CD = 3;
		public static final int CW02R_USER_ID_CI = 1;
		public static final int CW02R_USER_ID = 8;
		public static final int CW02R_STATUS_CD_CI = 1;
		public static final int CW02R_STATUS_CD = 1;
		public static final int CW02R_TERMINAL_ID_CI = 1;
		public static final int CW02R_TERMINAL_ID = 8;
		public static final int CW02R_CICL_EXP_DT_CI = 1;
		public static final int CW02R_CICL_EXP_DT = 10;
		public static final int CW02R_CICL_EFF_ACY_TS_CI = 1;
		public static final int CW02R_CICL_EFF_ACY_TS = 26;
		public static final int CW02R_CICL_EXP_ACY_TS_CI = 1;
		public static final int CW02R_CICL_EXP_ACY_TS = 26;
		public static final int CW02R_STATUTORY_TLE_CD_CI = 1;
		public static final int CW02R_STATUTORY_TLE_CD = 3;
		public static final int CW02R_CICL_LNG_NM_CI = 1;
		public static final int CW02R_CICL_LNG_NM_NI = 1;
		public static final int CW02R_CICL_LNG_NM = 132;
		public static final int CW02R_LO_LST_NM_MCH_CD_CI = 1;
		public static final int CW02R_LO_LST_NM_MCH_CD_NI = 1;
		public static final int CW02R_LO_LST_NM_MCH_CD = 35;
		public static final int CW02R_HI_LST_NM_MCH_CD_CI = 1;
		public static final int CW02R_HI_LST_NM_MCH_CD_NI = 1;
		public static final int CW02R_HI_LST_NM_MCH_CD = 35;
		public static final int CW02R_LO_FST_NM_MCH_CD_CI = 1;
		public static final int CW02R_LO_FST_NM_MCH_CD_NI = 1;
		public static final int CW02R_LO_FST_NM_MCH_CD = 35;
		public static final int CW02R_HI_FST_NM_MCH_CD_CI = 1;
		public static final int CW02R_HI_FST_NM_MCH_CD_NI = 1;
		public static final int CW02R_HI_FST_NM_MCH_CD = 35;
		public static final int CW02R_CICL_ACQ_SRC_CD_CI = 1;
		public static final int CW02R_CICL_ACQ_SRC_CD_NI = 1;
		public static final int CW02R_CICL_ACQ_SRC_CD = 3;
		public static final int CW02R_LEG_ENT_DESC = 40;
		public static final int CWCACR_CLIENT_ADDR_COMP_CHKSUM = 9;
		public static final int CWCACR_ADR_ID_KCRE = 32;
		public static final int CWCACR_CLIENT_ID_KCRE = 32;
		public static final int CWCACR_ADR_SEQ_NBR_KCRE = 32;
		public static final int CWCACR_TCH_OBJECT_KEY_KCRE = 32;
		public static final int CWCACR_CLIENT_ADDR_COMP_FIXED = CWCACR_CLIENT_ADDR_COMP_CHKSUM + CWCACR_ADR_ID_KCRE + CWCACR_CLIENT_ID_KCRE
				+ CWCACR_ADR_SEQ_NBR_KCRE + CWCACR_TCH_OBJECT_KEY_KCRE;
		public static final int CWCACR_ADR_ID_CI = 1;
		public static final int CWCACR_ADR_ID = 20;
		public static final int CWCACR_CLIENT_ADDRESS_KEY = CWCACR_ADR_ID_CI + CWCACR_ADR_ID;
		public static final int CWCACR_CLIENT_ID_CI = 1;
		public static final int CWCACR_CLIENT_ID = 20;
		public static final int CWCACR_HISTORY_VLD_NBR_CI = 1;
		public static final int CWCACR_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CWCACR_HISTORY_VLD_NBR = 5;
		public static final int CWCACR_ADR_SEQ_NBR_CI = 1;
		public static final int CWCACR_ADR_SEQ_NBR_SIGN = 1;
		public static final int CWCACR_ADR_SEQ_NBR = 5;
		public static final int CWCACR_CIAR_EFF_DT_CI = 1;
		public static final int CWCACR_CIAR_EFF_DT = 10;
		public static final int CWCACR_CLT_ADR_RELATION_KEY = CWCACR_CLIENT_ID_CI + CWCACR_CLIENT_ID + CWCACR_HISTORY_VLD_NBR_CI
				+ CWCACR_HISTORY_VLD_NBR_SIGN + CWCACR_HISTORY_VLD_NBR + CWCACR_ADR_SEQ_NBR_CI + CWCACR_ADR_SEQ_NBR_SIGN + CWCACR_ADR_SEQ_NBR
				+ CWCACR_CIAR_EFF_DT_CI + CWCACR_CIAR_EFF_DT;
		public static final int CWCACR_TRANS_PROCESS_DT = 10;
		public static final int CWCACR_CLIENT_ADDR_COMP_DATES = CWCACR_TRANS_PROCESS_DT;
		public static final int CWCACR_USER_ID_CI = 1;
		public static final int CWCACR_USER_ID = 8;
		public static final int CWCACR_TERMINAL_ID_CI = 1;
		public static final int CWCACR_TERMINAL_ID = 8;
		public static final int CWCACR_CICA_ADR1_CI = 1;
		public static final int CWCACR_CICA_ADR1 = 45;
		public static final int CWCACR_CICA_ADR2_CI = 1;
		public static final int CWCACR_CICA_ADR2_NI = 1;
		public static final int CWCACR_CICA_ADR2 = 45;
		public static final int CWCACR_CICA_CIT_NM_CI = 1;
		public static final int CWCACR_CICA_CIT_NM = 30;
		public static final int CWCACR_CICA_CTY_CI = 1;
		public static final int CWCACR_CICA_CTY_NI = 1;
		public static final int CWCACR_CICA_CTY = 30;
		public static final int CWCACR_ST_CD_CI = 1;
		public static final int CWCACR_ST_CD = 3;
		public static final int CWCACR_CICA_PST_CD_CI = 1;
		public static final int CWCACR_CICA_PST_CD = 13;
		public static final int CWCACR_CTR_CD_CI = 1;
		public static final int CWCACR_CTR_CD = 4;
		public static final int CWCACR_CICA_ADD_ADR_IND_CI = 1;
		public static final int CWCACR_CICA_ADD_ADR_IND_NI = 1;
		public static final int CWCACR_CICA_ADD_ADR_IND = 1;
		public static final int CWCACR_ADDR_STATUS_CD_CI = 1;
		public static final int CWCACR_ADDR_STATUS_CD = 1;
		public static final int CWCACR_ADD_NM_IND_CI = 1;
		public static final int CWCACR_ADD_NM_IND = 1;
		public static final int CWCACR_ADR_TYP_CD_CI = 1;
		public static final int CWCACR_ADR_TYP_CD = 4;
		public static final int CWCACR_TCH_OBJECT_KEY_CI = 1;
		public static final int CWCACR_TCH_OBJECT_KEY = 20;
		public static final int CWCACR_CIAR_EXP_DT_CI = 10;
		public static final int CWCACR_CIAR_EXP_DT = 10;
		public static final int CWCACR_CIAR_SER_ADR1_TXT_CI = 1;
		public static final int CWCACR_CIAR_SER_ADR1_TXT = 8;
		public static final int CWCACR_CIAR_SER_CIT_NM_CI = 1;
		public static final int CWCACR_CIAR_SER_CIT_NM = 5;
		public static final int CWCACR_CIAR_SER_ST_CD_CI = 1;
		public static final int CWCACR_CIAR_SER_ST_CD = 3;
		public static final int CWCACR_CIAR_SER_PST_CD_CI = 1;
		public static final int CWCACR_CIAR_SER_PST_CD = 6;
		public static final int CWCACR_REL_STATUS_CD_CI = 1;
		public static final int CWCACR_REL_STATUS_CD = 1;
		public static final int CWCACR_EFF_ACY_TS_CI = 1;
		public static final int CWCACR_EFF_ACY_TS = 26;
		public static final int CWCACR_CIAR_EXP_ACY_TS_CI = 1;
		public static final int CWCACR_CIAR_EXP_ACY_TS = 26;
		public static final int CWCACR_MORE_ROWS_SW = 1;
		public static final int CWCACR_CICA_SHW_OBJ_KEY_CI = 1;
		public static final int CWCACR_CICA_SHW_OBJ_KEY = 30;
		public static final int CWCACR_BUS_OBJ_NM = 32;
		public static final int CWCACR_ST_DESC = 40;
		public static final int CWCACR_ADR_TYP_DESC = 40;
		public static final int CWCACR_CLIENT_ADDR_COMP_DATA = CWCACR_USER_ID_CI + CWCACR_USER_ID + CWCACR_TERMINAL_ID_CI + CWCACR_TERMINAL_ID
				+ CWCACR_CICA_ADR1_CI + CWCACR_CICA_ADR1 + CWCACR_CICA_ADR2_CI + CWCACR_CICA_ADR2_NI + CWCACR_CICA_ADR2 + CWCACR_CICA_CIT_NM_CI
				+ CWCACR_CICA_CIT_NM + CWCACR_CICA_CTY_CI + CWCACR_CICA_CTY_NI + CWCACR_CICA_CTY + CWCACR_ST_CD_CI + CWCACR_ST_CD
				+ CWCACR_CICA_PST_CD_CI + CWCACR_CICA_PST_CD + CWCACR_CTR_CD_CI + CWCACR_CTR_CD + CWCACR_CICA_ADD_ADR_IND_CI
				+ CWCACR_CICA_ADD_ADR_IND_NI + CWCACR_CICA_ADD_ADR_IND + CWCACR_ADDR_STATUS_CD_CI + CWCACR_ADDR_STATUS_CD + CWCACR_ADD_NM_IND_CI
				+ CWCACR_ADD_NM_IND + CWCACR_ADR_TYP_CD_CI + CWCACR_ADR_TYP_CD + CWCACR_TCH_OBJECT_KEY_CI + CWCACR_TCH_OBJECT_KEY
				+ CWCACR_CIAR_EXP_DT_CI + CWCACR_CIAR_EXP_DT + CWCACR_CIAR_SER_ADR1_TXT_CI + CWCACR_CIAR_SER_ADR1_TXT + CWCACR_CIAR_SER_CIT_NM_CI
				+ CWCACR_CIAR_SER_CIT_NM + CWCACR_CIAR_SER_ST_CD_CI + CWCACR_CIAR_SER_ST_CD + CWCACR_CIAR_SER_PST_CD_CI + CWCACR_CIAR_SER_PST_CD
				+ CWCACR_REL_STATUS_CD_CI + CWCACR_REL_STATUS_CD + CWCACR_EFF_ACY_TS_CI + CWCACR_EFF_ACY_TS + CWCACR_CIAR_EXP_ACY_TS_CI
				+ CWCACR_CIAR_EXP_ACY_TS + CWCACR_MORE_ROWS_SW + CWCACR_CICA_SHW_OBJ_KEY_CI + CWCACR_CICA_SHW_OBJ_KEY + CWCACR_BUS_OBJ_NM
				+ CWCACR_ST_DESC + CWCACR_ADR_TYP_DESC;
		public static final int CWCACR_CLIENT_ADDR_COMPOSITE = CWCACR_CLIENT_ADDR_COMP_FIXED + CWCACR_CLIENT_ADDRESS_KEY + CWCACR_CLT_ADR_RELATION_KEY
				+ CWCACR_CLIENT_ADDR_COMP_DATES + CWCACR_CLIENT_ADDR_COMP_DATA;
		public static final int L_FW_RESP_CLT_ADR_COMPOSITE = CWCACR_CLIENT_ADDR_COMPOSITE;
		public static final int CWEMR_CLIENT_EMAIL_CSUM = 9;
		public static final int CWEMR_CLIENT_ID_KCRE = 32;
		public static final int CWEMR_CIEM_SEQ_NBR_KCRE = 32;
		public static final int CWEMR_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CWEMR_EFFECTIVE_DT_KCRE = 32;
		public static final int CWEMR_TRANS_PROCESS_DT = 10;
		public static final int CWEMR_CLIENT_ID_CI = 1;
		public static final int CWEMR_CLIENT_ID = 20;
		public static final int CWEMR_CIEM_SEQ_NBR_CI = 1;
		public static final int CWEMR_CIEM_SEQ_NBR_SIGN = 1;
		public static final int CWEMR_CIEM_SEQ_NBR = 5;
		public static final int CWEMR_HISTORY_VLD_NBR_CI = 1;
		public static final int CWEMR_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CWEMR_HISTORY_VLD_NBR = 5;
		public static final int CWEMR_EFFECTIVE_DT_CI = 1;
		public static final int CWEMR_EFFECTIVE_DT = 10;
		public static final int CWEMR_EMAIL_TYPE_CD_CI = 1;
		public static final int CWEMR_EMAIL_TYPE_CD = 3;
		public static final int CWEMR_USER_ID_CI = 1;
		public static final int CWEMR_USER_ID = 8;
		public static final int CWEMR_STATUS_CD_CI = 1;
		public static final int CWEMR_STATUS_CD = 1;
		public static final int CWEMR_TERMINAL_ID_CI = 1;
		public static final int CWEMR_TERMINAL_ID = 8;
		public static final int CWEMR_EXPIRATION_DT_CI = 1;
		public static final int CWEMR_EXPIRATION_DT = 10;
		public static final int CWEMR_EFFECTIVE_ACY_TS_CI = 1;
		public static final int CWEMR_EFFECTIVE_ACY_TS = 26;
		public static final int CWEMR_EXPIRATION_ACY_TS_CI = 1;
		public static final int CWEMR_EXPIRATION_ACY_TS_NI = 1;
		public static final int CWEMR_EXPIRATION_ACY_TS = 26;
		public static final int CWEMR_CIEM_EMAIL_ADR_TXT_CI = 1;
		public static final int CWEMR_CIEM_EMAIL_ADR_TXT = 255;
		public static final int CWEMR_MORE_ROWS_SW = 1;
		public static final int CWEMR_EMAIL_TYPE_CD_DESC = 40;
		public static final int CW04R_CLIENT_PHONE_CHK_SUM = 9;
		public static final int CW04R_CLIENT_ID_KCRE = 32;
		public static final int CW04R_CIPH_PHN_SEQ_NBR_KCRE = 32;
		public static final int CW04R_CLIENT_PHONE_FIXED = CW04R_CLIENT_PHONE_CHK_SUM + CW04R_CLIENT_ID_KCRE + CW04R_CIPH_PHN_SEQ_NBR_KCRE;
		public static final int CW04R_TRANS_PROCESS_DT = 10;
		public static final int CW04R_CLIENT_PHONE_DATES = CW04R_TRANS_PROCESS_DT;
		public static final int CW04R_CLIENT_ID_CI = 1;
		public static final int CW04R_CLIENT_ID = 20;
		public static final int CW04R_CIPH_PHN_SEQ_NBR_CI = 1;
		public static final int CW04R_CIPH_PHN_SEQ_NBR_SIGN = 1;
		public static final int CW04R_CIPH_PHN_SEQ_NBR = 5;
		public static final int CW04R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW04R_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW04R_HISTORY_VLD_NBR = 5;
		public static final int CW04R_CIPH_EFF_DT_CI = 1;
		public static final int CW04R_CIPH_EFF_DT = 10;
		public static final int CW04R_CLIENT_PHONE_KEY = CW04R_CLIENT_ID_CI + CW04R_CLIENT_ID + CW04R_CIPH_PHN_SEQ_NBR_CI
				+ CW04R_CIPH_PHN_SEQ_NBR_SIGN + CW04R_CIPH_PHN_SEQ_NBR + CW04R_HISTORY_VLD_NBR_CI + CW04R_HISTORY_VLD_NBR_SIGN + CW04R_HISTORY_VLD_NBR
				+ CW04R_CIPH_EFF_DT_CI + CW04R_CIPH_EFF_DT;
		public static final int CW04R_CIPH_PHN_NBR_CI = 1;
		public static final int CW04R_CIPH_PHN_NBR = 21;
		public static final int CW04R_PHN_TYP_CD_CI = 1;
		public static final int CW04R_PHN_TYP_CD = 2;
		public static final int CW04R_CIPH_XRF_ID_CI = 1;
		public static final int CW04R_CIPH_XRF_ID_NI = 1;
		public static final int CW04R_CIPH_XRF_ID = 20;
		public static final int CW04R_USER_ID_CI = 1;
		public static final int CW04R_USER_ID = 8;
		public static final int CW04R_STATUS_CD_CI = 1;
		public static final int CW04R_STATUS_CD = 1;
		public static final int CW04R_TERMINAL_ID_CI = 1;
		public static final int CW04R_TERMINAL_ID = 8;
		public static final int CW04R_CIPH_EXP_DT_CI = 1;
		public static final int CW04R_CIPH_EXP_DT = 10;
		public static final int CW04R_CIPH_EFF_ACY_TS_CI = 1;
		public static final int CW04R_CIPH_EFF_ACY_TS = 26;
		public static final int CW04R_CIPH_EXP_ACY_TS_CI = 1;
		public static final int CW04R_CIPH_EXP_ACY_TS = 26;
		public static final int CW04R_MORE_ROWS_SW = 1;
		public static final int CW04R_PHN_TYP_DESC = 40;
		public static final int CW04R_CLIENT_PHONE_DATA = CW04R_CIPH_PHN_NBR_CI + CW04R_CIPH_PHN_NBR + CW04R_PHN_TYP_CD_CI + CW04R_PHN_TYP_CD
				+ CW04R_CIPH_XRF_ID_CI + CW04R_CIPH_XRF_ID_NI + CW04R_CIPH_XRF_ID + CW04R_USER_ID_CI + CW04R_USER_ID + CW04R_STATUS_CD_CI
				+ CW04R_STATUS_CD + CW04R_TERMINAL_ID_CI + CW04R_TERMINAL_ID + CW04R_CIPH_EXP_DT_CI + CW04R_CIPH_EXP_DT + CW04R_CIPH_EFF_ACY_TS_CI
				+ CW04R_CIPH_EFF_ACY_TS + CW04R_CIPH_EXP_ACY_TS_CI + CW04R_CIPH_EXP_ACY_TS + CW04R_MORE_ROWS_SW + CW04R_PHN_TYP_DESC;
		public static final int CW04R_CLIENT_PHONE_ROW = CW04R_CLIENT_PHONE_FIXED + CW04R_CLIENT_PHONE_DATES + CW04R_CLIENT_PHONE_KEY
				+ CW04R_CLIENT_PHONE_DATA;
		public static final int L_FW_RESP_CLIENT_PHONE = CW04R_CLIENT_PHONE_ROW;
		public static final int CW08R_CLT_OBJ_RELATION_CSUM = 9;
		public static final int CW08R_TCH_OBJECT_KEY_KCRE = 32;
		public static final int CW08R_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CW08R_CIOR_EFF_DT_KCRE = 32;
		public static final int CW08R_OBJ_SYS_ID_KCRE = 32;
		public static final int CW08R_CIOR_OBJ_SEQ_NBR_KCRE = 32;
		public static final int CW08R_CLIENT_ID_KCRE = 32;
		public static final int CW08R_CLT_OBJ_RELATION_FIXED = CW08R_CLT_OBJ_RELATION_CSUM + CW08R_TCH_OBJECT_KEY_KCRE + CW08R_HISTORY_VLD_NBR_KCRE
				+ CW08R_CIOR_EFF_DT_KCRE + CW08R_OBJ_SYS_ID_KCRE + CW08R_CIOR_OBJ_SEQ_NBR_KCRE + CW08R_CLIENT_ID_KCRE;
		public static final int CW08R_TRANS_PROCESS_DT = 10;
		public static final int CW08R_CLT_OBJ_RELATION_DATES = CW08R_TRANS_PROCESS_DT;
		public static final int CW08R_TCH_OBJECT_KEY_CI = 1;
		public static final int CW08R_TCH_OBJECT_KEY = 20;
		public static final int CW08R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW08R_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW08R_HISTORY_VLD_NBR = 5;
		public static final int CW08R_CIOR_EFF_DT_CI = 1;
		public static final int CW08R_CIOR_EFF_DT = 10;
		public static final int CW08R_OBJ_SYS_ID_CI = 1;
		public static final int CW08R_OBJ_SYS_ID = 4;
		public static final int CW08R_CIOR_OBJ_SEQ_NBR_CI = 1;
		public static final int CW08R_CIOR_OBJ_SEQ_NBR_SIGN = 1;
		public static final int CW08R_CIOR_OBJ_SEQ_NBR = 5;
		public static final int CW08R_CLT_OBJ_RELATION_KEY = CW08R_TCH_OBJECT_KEY_CI + CW08R_TCH_OBJECT_KEY + CW08R_HISTORY_VLD_NBR_CI
				+ CW08R_HISTORY_VLD_NBR_SIGN + CW08R_HISTORY_VLD_NBR + CW08R_CIOR_EFF_DT_CI + CW08R_CIOR_EFF_DT + CW08R_OBJ_SYS_ID_CI
				+ CW08R_OBJ_SYS_ID + CW08R_CIOR_OBJ_SEQ_NBR_CI + CW08R_CIOR_OBJ_SEQ_NBR_SIGN + CW08R_CIOR_OBJ_SEQ_NBR;
		public static final int CW08R_CLIENT_ID_CI = 1;
		public static final int CW08R_CLIENT_ID = 20;
		public static final int CW08R_RLT_TYP_CD_CI = 1;
		public static final int CW08R_RLT_TYP_CD = 4;
		public static final int CW08R_OBJ_CD_CI = 1;
		public static final int CW08R_OBJ_CD = 4;
		public static final int CW08R_CIOR_SHW_OBJ_KEY_CI = 1;
		public static final int CW08R_CIOR_SHW_OBJ_KEY = 30;
		public static final int CW08R_ADR_SEQ_NBR_CI = 1;
		public static final int CW08R_ADR_SEQ_NBR_SIGN = 1;
		public static final int CW08R_ADR_SEQ_NBR = 5;
		public static final int CW08R_USER_ID_CI = 1;
		public static final int CW08R_USER_ID = 8;
		public static final int CW08R_STATUS_CD_CI = 1;
		public static final int CW08R_STATUS_CD = 1;
		public static final int CW08R_TERMINAL_ID_CI = 1;
		public static final int CW08R_TERMINAL_ID = 8;
		public static final int CW08R_CIOR_EXP_DT_CI = 1;
		public static final int CW08R_CIOR_EXP_DT = 10;
		public static final int CW08R_CIOR_EFF_ACY_TS_CI = 1;
		public static final int CW08R_CIOR_EFF_ACY_TS = 26;
		public static final int CW08R_CIOR_EXP_ACY_TS_CI = 1;
		public static final int CW08R_CIOR_EXP_ACY_TS = 26;
		public static final int CW08R_APP_TYPE = 1;
		public static final int CW08R_BUS_OBJ_NM = 32;
		public static final int CW08R_OBJ_DESC = 40;
		public static final int CW08R_CLT_OBJ_RELATION_DATA = CW08R_CLIENT_ID_CI + CW08R_CLIENT_ID + CW08R_RLT_TYP_CD_CI + CW08R_RLT_TYP_CD
				+ CW08R_OBJ_CD_CI + CW08R_OBJ_CD + CW08R_CIOR_SHW_OBJ_KEY_CI + CW08R_CIOR_SHW_OBJ_KEY + CW08R_ADR_SEQ_NBR_CI + CW08R_ADR_SEQ_NBR_SIGN
				+ CW08R_ADR_SEQ_NBR + CW08R_USER_ID_CI + CW08R_USER_ID + CW08R_STATUS_CD_CI + CW08R_STATUS_CD + CW08R_TERMINAL_ID_CI
				+ CW08R_TERMINAL_ID + CW08R_CIOR_EXP_DT_CI + CW08R_CIOR_EXP_DT + CW08R_CIOR_EFF_ACY_TS_CI + CW08R_CIOR_EFF_ACY_TS
				+ CW08R_CIOR_EXP_ACY_TS_CI + CW08R_CIOR_EXP_ACY_TS + CW08R_APP_TYPE + CW08R_BUS_OBJ_NM + CW08R_OBJ_DESC;
		public static final int CW08R_CLT_OBJ_RELATION_ROW = CW08R_CLT_OBJ_RELATION_FIXED + CW08R_CLT_OBJ_RELATION_DATES + CW08R_CLT_OBJ_RELATION_KEY
				+ CW08R_CLT_OBJ_RELATION_DATA;
		public static final int L_FW_RESP_CLT_OBJ_RELATION = CW08R_CLT_OBJ_RELATION_ROW;
		public static final int CW11R_CLT_REF_RELATION_CSUM = 9;
		public static final int CW11R_CLIENT_ID_KCRE = 32;
		public static final int CW11R_CIRF_REF_SEQ_NBR_KCRE = 32;
		public static final int CW11R_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CW11R_CIRF_EFF_DT_KCRE = 32;
		public static final int CW11R_TRANS_PROCESS_DT = 10;
		public static final int CW11R_CLIENT_ID_CI = 1;
		public static final int CW11R_CLIENT_ID = 20;
		public static final int CW11R_CIRF_REF_SEQ_NBR_CI = 1;
		public static final int CW11R_CIRF_REF_SEQ_NBR_SIGN = 1;
		public static final int CW11R_CIRF_REF_SEQ_NBR = 5;
		public static final int CW11R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW11R_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW11R_HISTORY_VLD_NBR = 5;
		public static final int CW11R_CIRF_EFF_DT_CI = 1;
		public static final int CW11R_CIRF_EFF_DT = 10;
		public static final int CW11R_CIRF_REF_ID_CI = 1;
		public static final int CW11R_CIRF_REF_ID = 30;
		public static final int CW11R_REF_TYP_CD_CI = 1;
		public static final int CW11R_REF_TYP_CD = 4;
		public static final int CW11R_CIRF_EXP_DT_CI = 1;
		public static final int CW11R_CIRF_EXP_DT = 10;
		public static final int CW11R_USER_ID_CI = 1;
		public static final int CW11R_USER_ID = 8;
		public static final int CW11R_STATUS_CD_CI = 1;
		public static final int CW11R_STATUS_CD = 1;
		public static final int CW11R_TERMINAL_ID_CI = 1;
		public static final int CW11R_TERMINAL_ID = 8;
		public static final int CW11R_CIRF_EFF_ACY_TS_CI = 1;
		public static final int CW11R_CIRF_EFF_ACY_TS = 26;
		public static final int CW11R_CIRF_EXP_ACY_TS_CI = 1;
		public static final int CW11R_CIRF_EXP_ACY_TS = 26;
		public static final int CW11R_MORE_ROWS_SW = 1;
		public static final int CW27R_CLIENT_TAX_CHK_SUM = 9;
		public static final int CW27R_CLIENT_ID_KCRE = 32;
		public static final int CW27R_CITX_TAX_SEQ_NBR_KCRE = 32;
		public static final int CW27R_TRANS_PROCESS_DT = 10;
		public static final int CW27R_CLIENT_ID_CI = 1;
		public static final int CW27R_CLIENT_ID = 20;
		public static final int CW27R_CITX_TAX_SEQ_NBR_CI = 1;
		public static final int CW27R_CITX_TAX_SEQ_NBR_SIGN = 1;
		public static final int CW27R_CITX_TAX_SEQ_NBR = 5;
		public static final int CW27R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW27R_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW27R_HISTORY_VLD_NBR = 5;
		public static final int CW27R_EFFECTIVE_DT_CI = 1;
		public static final int CW27R_EFFECTIVE_DT = 10;
		public static final int CW27R_CITX_TAX_ID_CI = 1;
		public static final int CW27R_CITX_TAX_ID = 15;
		public static final int CW27R_TAX_TYPE_CD_CI = 1;
		public static final int CW27R_TAX_TYPE_CD = 3;
		public static final int CW27R_CITX_TAX_ST_CD_CI = 1;
		public static final int CW27R_CITX_TAX_ST_CD_NI = 1;
		public static final int CW27R_CITX_TAX_ST_CD = 3;
		public static final int CW27R_CITX_TAX_CTR_CD_CI = 1;
		public static final int CW27R_CITX_TAX_CTR_CD_NI = 1;
		public static final int CW27R_CITX_TAX_CTR_CD = 4;
		public static final int CW27R_USER_ID_CI = 1;
		public static final int CW27R_USER_ID = 8;
		public static final int CW27R_STATUS_CD_CI = 1;
		public static final int CW27R_STATUS_CD = 1;
		public static final int CW27R_TERMINAL_ID_CI = 1;
		public static final int CW27R_TERMINAL_ID = 8;
		public static final int CW27R_EXPIRATION_DT_CI = 1;
		public static final int CW27R_EXPIRATION_DT = 10;
		public static final int CW27R_EFFECTIVE_ACY_TS_CI = 1;
		public static final int CW27R_EFFECTIVE_ACY_TS = 26;
		public static final int CW27R_EXPIRATION_ACY_TS_CI = 1;
		public static final int CW27R_EXPIRATION_ACY_TS_NI = 1;
		public static final int CW27R_EXPIRATION_ACY_TS = 26;
		public static final int CW27R_MORE_ROWS_SW = 1;
		public static final int CW27R_TAX_TYPE_DESC = 40;
		public static final int CW45R_CLT_MUT_TER_STC_CSUM = 9;
		public static final int CW45R_TER_CLT_ID_KCRE = 32;
		public static final int CW45R_TER_LEVEL_CD_KCRE = 32;
		public static final int CW45R_TER_NBR_KCRE = 32;
		public static final int CW45R_TRANS_PROCESS_DT = 10;
		public static final int CW45R_TER_CLT_ID = 20;
		public static final int CW45R_TER_LEVEL_CD = 4;
		public static final int CW45R_TER_NBR = 4;
		public static final int CW45R_TER_CLT_ID_CI = 1;
		public static final int CW45R_TER_LEVEL_CD_CI = 1;
		public static final int CW45R_TER_NBR_CI = 1;
		public static final int CW45R_MR_TER_NBR_CI = 1;
		public static final int CW45R_MR_TER_NBR = 4;
		public static final int CW45R_MR_TER_CLT_ID_CI = 1;
		public static final int CW45R_MR_TER_CLT_ID = 20;
		public static final int CW45R_MR_NM_PFX_CI = 1;
		public static final int CW45R_MR_NM_PFX = 4;
		public static final int CW45R_MR_FST_NM_CI = 1;
		public static final int CW45R_MR_FST_NM = 30;
		public static final int CW45R_MR_MDL_NM_CI = 1;
		public static final int CW45R_MR_MDL_NM = 30;
		public static final int CW45R_MR_LST_NM_CI = 1;
		public static final int CW45R_MR_LST_NM = 60;
		public static final int CW45R_MR_NM_CI = 1;
		public static final int CW45R_MR_NM = 120;
		public static final int CW45R_MR_NM_SFX_CI = 1;
		public static final int CW45R_MR_NM_SFX = 4;
		public static final int CW45R_MR_CLT_ID_CI = 1;
		public static final int CW45R_MR_CLT_ID = 20;
		public static final int CW45R_CSR_TER_NBR_CI = 1;
		public static final int CW45R_CSR_TER_NBR = 4;
		public static final int CW45R_CSR_TER_CLT_ID_CI = 1;
		public static final int CW45R_CSR_TER_CLT_ID = 20;
		public static final int CW45R_CSR_NM_PFX_CI = 1;
		public static final int CW45R_CSR_NM_PFX = 4;
		public static final int CW45R_CSR_FST_NM_CI = 1;
		public static final int CW45R_CSR_FST_NM = 30;
		public static final int CW45R_CSR_MDL_NM_CI = 1;
		public static final int CW45R_CSR_MDL_NM = 30;
		public static final int CW45R_CSR_LST_NM_CI = 1;
		public static final int CW45R_CSR_LST_NM = 60;
		public static final int CW45R_CSR_NM_CI = 1;
		public static final int CW45R_CSR_NM = 120;
		public static final int CW45R_CSR_NM_SFX_CI = 1;
		public static final int CW45R_CSR_NM_SFX = 4;
		public static final int CW45R_CSR_CLT_ID_CI = 1;
		public static final int CW45R_CSR_CLT_ID = 20;
		public static final int CW45R_SMR_TER_NBR_CI = 1;
		public static final int CW45R_SMR_TER_NBR = 4;
		public static final int CW45R_SMR_TER_CLT_ID_CI = 1;
		public static final int CW45R_SMR_TER_CLT_ID = 20;
		public static final int CW45R_SMR_NM_PFX_CI = 1;
		public static final int CW45R_SMR_NM_PFX = 4;
		public static final int CW45R_SMR_FST_NM_CI = 1;
		public static final int CW45R_SMR_FST_NM = 30;
		public static final int CW45R_SMR_MDL_NM_CI = 1;
		public static final int CW45R_SMR_MDL_NM = 30;
		public static final int CW45R_SMR_LST_NM_CI = 1;
		public static final int CW45R_SMR_LST_NM = 60;
		public static final int CW45R_SMR_NM_CI = 1;
		public static final int CW45R_SMR_NM = 120;
		public static final int CW45R_SMR_NM_SFX_CI = 1;
		public static final int CW45R_SMR_NM_SFX = 4;
		public static final int CW45R_SMR_CLT_ID_CI = 1;
		public static final int CW45R_SMR_CLT_ID = 20;
		public static final int CW45R_DMM_TER_NBR_CI = 1;
		public static final int CW45R_DMM_TER_NBR = 4;
		public static final int CW45R_DMM_TER_CLT_ID_CI = 1;
		public static final int CW45R_DMM_TER_CLT_ID = 20;
		public static final int CW45R_DMM_NM_PFX_CI = 1;
		public static final int CW45R_DMM_NM_PFX = 4;
		public static final int CW45R_DMM_FST_NM_CI = 1;
		public static final int CW45R_DMM_FST_NM = 30;
		public static final int CW45R_DMM_MDL_NM_CI = 1;
		public static final int CW45R_DMM_MDL_NM = 30;
		public static final int CW45R_DMM_LST_NM_CI = 1;
		public static final int CW45R_DMM_LST_NM = 60;
		public static final int CW45R_DMM_NM_CI = 1;
		public static final int CW45R_DMM_NM = 120;
		public static final int CW45R_DMM_NM_SFX_CI = 1;
		public static final int CW45R_DMM_NM_SFX = 4;
		public static final int CW45R_DMM_CLT_ID_CI = 1;
		public static final int CW45R_DMM_CLT_ID = 20;
		public static final int CW45R_RMM_TER_NBR_CI = 1;
		public static final int CW45R_RMM_TER_NBR = 4;
		public static final int CW45R_RMM_TER_CLT_ID_CI = 1;
		public static final int CW45R_RMM_TER_CLT_ID = 20;
		public static final int CW45R_RMM_NM_PFX_CI = 1;
		public static final int CW45R_RMM_NM_PFX = 4;
		public static final int CW45R_RMM_FST_NM_CI = 1;
		public static final int CW45R_RMM_FST_NM = 30;
		public static final int CW45R_RMM_MDL_NM_CI = 1;
		public static final int CW45R_RMM_MDL_NM = 30;
		public static final int CW45R_RMM_LST_NM_CI = 1;
		public static final int CW45R_RMM_LST_NM = 60;
		public static final int CW45R_RMM_NM_CI = 1;
		public static final int CW45R_RMM_NM = 120;
		public static final int CW45R_RMM_NM_SFX_CI = 1;
		public static final int CW45R_RMM_NM_SFX = 4;
		public static final int CW45R_RMM_CLT_ID_CI = 1;
		public static final int CW45R_RMM_CLT_ID = 20;
		public static final int CW45R_DFO_TER_NBR_CI = 1;
		public static final int CW45R_DFO_TER_NBR = 4;
		public static final int CW45R_DFO_TER_CLT_ID_CI = 1;
		public static final int CW45R_DFO_TER_CLT_ID = 20;
		public static final int CW45R_DFO_NM_PFX_CI = 1;
		public static final int CW45R_DFO_NM_PFX = 4;
		public static final int CW45R_DFO_FST_NM_CI = 1;
		public static final int CW45R_DFO_FST_NM = 30;
		public static final int CW45R_DFO_MDL_NM_CI = 1;
		public static final int CW45R_DFO_MDL_NM = 30;
		public static final int CW45R_DFO_LST_NM_CI = 1;
		public static final int CW45R_DFO_LST_NM = 60;
		public static final int CW45R_DFO_NM_CI = 1;
		public static final int CW45R_DFO_NM = 120;
		public static final int CW45R_DFO_NM_SFX_CI = 1;
		public static final int CW45R_DFO_NM_SFX = 4;
		public static final int CW45R_DFO_CLT_ID_CI = 1;
		public static final int CW45R_DFO_CLT_ID = 20;
		public static final int CW46R_CLT_AGC_TER_STC_CSUM = 9;
		public static final int CW46R_TER_CLT_ID_KCRE = 32;
		public static final int CW46R_TER_LEVEL_CD_KCRE = 32;
		public static final int CW46R_TRANS_PROCESS_DT = 10;
		public static final int CW46R_TER_CLT_ID = 20;
		public static final int CW46R_TER_LEVEL_CD = 4;
		public static final int CW46R_TER_CLT_ID_CI = 1;
		public static final int CW46R_TER_LEVEL_CD_CI = 1;
		public static final int CW46R_TER_NBR_CI = 1;
		public static final int CW46R_TER_NBR = 4;
		public static final int CW46R_BRN_TER_NBR_CI = 1;
		public static final int CW46R_BRN_TER_NBR = 4;
		public static final int CW46R_BRN_TER_CLT_ID_CI = 1;
		public static final int CW46R_BRN_TER_CLT_ID = 20;
		public static final int CW46R_BRN_NM_CI = 1;
		public static final int CW46R_BRN_NM = 60;
		public static final int CW46R_BRN_CLT_ID_CI = 1;
		public static final int CW46R_BRN_CLT_ID = 20;
		public static final int CW46R_AGC_TER_NBR_CI = 1;
		public static final int CW46R_AGC_TER_NBR = 4;
		public static final int CW46R_AGC_TER_CLT_ID_CI = 1;
		public static final int CW46R_AGC_TER_CLT_ID = 20;
		public static final int CW46R_AGC_NM_CI = 1;
		public static final int CW46R_AGC_NM = 60;
		public static final int CW46R_AGC_CLT_ID_CI = 1;
		public static final int CW46R_AGC_CLT_ID = 20;
		public static final int CW46R_SMR_TER_NBR_CI = 1;
		public static final int CW46R_SMR_TER_NBR = 4;
		public static final int CW46R_SMR_TER_CLT_ID_CI = 1;
		public static final int CW46R_SMR_TER_CLT_ID = 20;
		public static final int CW46R_SMR_NM_CI = 1;
		public static final int CW46R_SMR_NM = 60;
		public static final int CW46R_SMR_CLT_ID_CI = 1;
		public static final int CW46R_SMR_CLT_ID = 20;
		public static final int CW46R_AMM_TER_NBR_CI = 1;
		public static final int CW46R_AMM_TER_NBR = 4;
		public static final int CW46R_AMM_TER_CLT_ID_CI = 1;
		public static final int CW46R_AMM_TER_CLT_ID = 20;
		public static final int CW46R_AMM_NM_PFX_CI = 1;
		public static final int CW46R_AMM_NM_PFX = 4;
		public static final int CW46R_AMM_FST_NM_CI = 1;
		public static final int CW46R_AMM_FST_NM = 30;
		public static final int CW46R_AMM_MDL_NM_CI = 1;
		public static final int CW46R_AMM_MDL_NM = 30;
		public static final int CW46R_AMM_LST_NM_CI = 1;
		public static final int CW46R_AMM_LST_NM = 60;
		public static final int CW46R_AMM_NM_CI = 1;
		public static final int CW46R_AMM_NM = 120;
		public static final int CW46R_AMM_NM_SFX_CI = 1;
		public static final int CW46R_AMM_NM_SFX = 4;
		public static final int CW46R_AMM_CLT_ID_CI = 1;
		public static final int CW46R_AMM_CLT_ID = 20;
		public static final int CW46R_AFM_TER_NBR_CI = 1;
		public static final int CW46R_AFM_TER_NBR = 4;
		public static final int CW46R_AFM_TER_CLT_ID_CI = 1;
		public static final int CW46R_AFM_TER_CLT_ID = 20;
		public static final int CW46R_AFM_NM_PFX_CI = 1;
		public static final int CW46R_AFM_NM_PFX = 4;
		public static final int CW46R_AFM_FST_NM_CI = 1;
		public static final int CW46R_AFM_FST_NM = 30;
		public static final int CW46R_AFM_MDL_NM_CI = 1;
		public static final int CW46R_AFM_MDL_NM = 30;
		public static final int CW46R_AFM_LST_NM_CI = 1;
		public static final int CW46R_AFM_LST_NM = 60;
		public static final int CW46R_AFM_NM_CI = 1;
		public static final int CW46R_AFM_NM = 120;
		public static final int CW46R_AFM_NM_SFX_CI = 1;
		public static final int CW46R_AFM_NM_SFX = 4;
		public static final int CW46R_AFM_CLT_ID_CI = 1;
		public static final int CW46R_AFM_CLT_ID = 20;
		public static final int CW46R_AVP_TER_NBR_CI = 1;
		public static final int CW46R_AVP_TER_NBR = 4;
		public static final int CW46R_AVP_TER_CLT_ID_CI = 1;
		public static final int CW46R_AVP_TER_CLT_ID = 20;
		public static final int CW46R_AVP_NM_PFX_CI = 1;
		public static final int CW46R_AVP_NM_PFX = 4;
		public static final int CW46R_AVP_FST_NM_CI = 1;
		public static final int CW46R_AVP_FST_NM = 30;
		public static final int CW46R_AVP_MDL_NM_CI = 1;
		public static final int CW46R_AVP_MDL_NM = 30;
		public static final int CW46R_AVP_LST_NM_CI = 1;
		public static final int CW46R_AVP_LST_NM = 60;
		public static final int CW46R_AVP_NM_CI = 1;
		public static final int CW46R_AVP_NM = 120;
		public static final int CW46R_AVP_NM_SFX_CI = 1;
		public static final int CW46R_AVP_NM_SFX = 4;
		public static final int CW46R_AVP_CLT_ID_CI = 1;
		public static final int CW46R_AVP_CLT_ID = 20;
		public static final int MUA02_CLT_CLT_RELATION_CSUM = 9;
		public static final int MUA02_CLIENT_ID_KCRE = 32;
		public static final int MUA02_CLT_TYP_CD_KCRE = 32;
		public static final int MUA02_HISTORY_VLD_NBR_KCRE = 32;
		public static final int MUA02_CICR_XRF_ID_KCRE = 32;
		public static final int MUA02_XRF_TYP_CD_KCRE = 32;
		public static final int MUA02_CICR_EFF_DT_KCRE = 32;
		public static final int MUA02_TRANS_PROCESS_DT = 10;
		public static final int MUA02_CLIENT_ID_CI = 1;
		public static final int MUA02_CLIENT_ID = 20;
		public static final int MUA02_CLT_TYP_CD_CI = 1;
		public static final int MUA02_CLT_TYP_CD = 4;
		public static final int MUA02_HISTORY_VLD_NBR_CI = 1;
		public static final int MUA02_HISTORY_VLD_NBR_SIGN = 1;
		public static final int MUA02_HISTORY_VLD_NBR = 5;
		public static final int MUA02_CICR_XRF_ID_CI = 1;
		public static final int MUA02_CICR_XRF_ID = 20;
		public static final int MUA02_XRF_TYP_CD_CI = 1;
		public static final int MUA02_XRF_TYP_CD = 4;
		public static final int MUA02_CICR_EFF_DT_CI = 1;
		public static final int MUA02_CICR_EFF_DT = 10;
		public static final int MUA02_CICR_EXP_DT_CI = 1;
		public static final int MUA02_CICR_EXP_DT = 10;
		public static final int MUA02_CICR_NBR_OPR_STRS_CI = 1;
		public static final int MUA02_CICR_NBR_OPR_STRS_NI = 1;
		public static final int MUA02_CICR_NBR_OPR_STRS = 3;
		public static final int MUA02_USER_ID_CI = 1;
		public static final int MUA02_USER_ID = 8;
		public static final int MUA02_STATUS_CD_CI = 1;
		public static final int MUA02_STATUS_CD = 1;
		public static final int MUA02_TERMINAL_ID_CI = 1;
		public static final int MUA02_TERMINAL_ID = 8;
		public static final int MUA02_CICR_EFF_ACY_TS_CI = 1;
		public static final int MUA02_CICR_EFF_ACY_TS = 26;
		public static final int MUA02_CICR_EXP_ACY_TS_CI = 1;
		public static final int MUA02_CICR_EXP_ACY_TS = 26;
		public static final int MUA02_CICR_LEG_ENT_CD_CI = 1;
		public static final int MUA02_CICR_LEG_ENT_CD = 2;
		public static final int MUA02_CICR_FST_NM_CI = 1;
		public static final int MUA02_CICR_FST_NM = 30;
		public static final int MUA02_CICR_LST_NM_CI = 1;
		public static final int MUA02_CICR_LST_NM = 60;
		public static final int MUA02_CICR_MDL_NM_CI = 1;
		public static final int MUA02_CICR_MDL_NM = 30;
		public static final int MUA02_NM_PFX_CI = 1;
		public static final int MUA02_NM_PFX = 4;
		public static final int MUA02_NM_SFX_CI = 1;
		public static final int MUA02_NM_SFX = 4;
		public static final int MUA02_CICR_LNG_NM_CI = 1;
		public static final int MUA02_CICR_LNG_NM_NI = 1;
		public static final int MUA02_CICR_LNG_NM = 132;
		public static final int MUA02_CICR_REL_CHG_SW = 1;
		public static final int MUA02_CLIENT_DES = 30;
		public static final int CW48R_FED_BUSINESS_TYP_CSUM = 9;
		public static final int CW48R_CLIENT_ID_KCRE = 32;
		public static final int CW48R_TOB_CD_KCRE = 32;
		public static final int CW48R_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CW48R_EFFECTIVE_DT_KCRE = 32;
		public static final int CW48R_FED_BUSINESS_TYP_FIXED = CW48R_FED_BUSINESS_TYP_CSUM + CW48R_CLIENT_ID_KCRE + CW48R_TOB_CD_KCRE
				+ CW48R_HISTORY_VLD_NBR_KCRE + CW48R_EFFECTIVE_DT_KCRE;
		public static final int CW48R_TRANS_PROCESS_DT = 10;
		public static final int CW48R_FED_BUSINESS_TYP_DATES = CW48R_TRANS_PROCESS_DT;
		public static final int CW48R_CLIENT_ID = 20;
		public static final int CW48R_TOB_CD = 4;
		public static final int CW48R_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW48R_HISTORY_VLD_NBR = 5;
		public static final int CW48R_EFFECTIVE_DT = 10;
		public static final int CW48R_FED_BUSINESS_TYP_KEY = CW48R_CLIENT_ID + CW48R_TOB_CD + CW48R_HISTORY_VLD_NBR_SIGN + CW48R_HISTORY_VLD_NBR
				+ CW48R_EFFECTIVE_DT;
		public static final int CW48R_CLIENT_ID_CI = 1;
		public static final int CW48R_TOB_CD_CI = 1;
		public static final int CW48R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW48R_EFFECTIVE_DT_CI = 1;
		public static final int CW48R_FED_BUSINESS_TYP_KEY_CI = CW48R_CLIENT_ID_CI + CW48R_TOB_CD_CI + CW48R_HISTORY_VLD_NBR_CI
				+ CW48R_EFFECTIVE_DT_CI;
		public static final int CW48R_TOB_SIC_TYP_CD_CI = 1;
		public static final int CW48R_TOB_SIC_TYP_CD = 3;
		public static final int CW48R_TOB_PTY_CD_CI = 1;
		public static final int CW48R_TOB_PTY_CD = 2;
		public static final int CW48R_USER_ID_CI = 1;
		public static final int CW48R_USER_ID = 8;
		public static final int CW48R_STATUS_CD_CI = 1;
		public static final int CW48R_STATUS_CD = 1;
		public static final int CW48R_TERMINAL_ID_CI = 1;
		public static final int CW48R_TERMINAL_ID = 8;
		public static final int CW48R_EXPIRATION_DT_CI = 1;
		public static final int CW48R_EXPIRATION_DT = 10;
		public static final int CW48R_EFFECTIVE_ACY_TS_CI = 1;
		public static final int CW48R_EFFECTIVE_ACY_TS = 26;
		public static final int CW48R_EXPIRATION_ACY_TS_CI = 1;
		public static final int CW48R_EXPIRATION_ACY_TS_NI = 1;
		public static final int CW48R_EXPIRATION_ACY_TS = 26;
		public static final int CW48R_TOB_CD_DESC = 40;
		public static final int CW48R_FED_BUSINESS_TYP_DATA = CW48R_TOB_SIC_TYP_CD_CI + CW48R_TOB_SIC_TYP_CD + CW48R_TOB_PTY_CD_CI + CW48R_TOB_PTY_CD
				+ CW48R_USER_ID_CI + CW48R_USER_ID + CW48R_STATUS_CD_CI + CW48R_STATUS_CD + CW48R_TERMINAL_ID_CI + CW48R_TERMINAL_ID
				+ CW48R_EXPIRATION_DT_CI + CW48R_EXPIRATION_DT + CW48R_EFFECTIVE_ACY_TS_CI + CW48R_EFFECTIVE_ACY_TS + CW48R_EXPIRATION_ACY_TS_CI
				+ CW48R_EXPIRATION_ACY_TS_NI + CW48R_EXPIRATION_ACY_TS + CW48R_TOB_CD_DESC;
		public static final int CW48R_FED_BUSINESS_TYP_ROW = CW48R_FED_BUSINESS_TYP_FIXED + CW48R_FED_BUSINESS_TYP_DATES + CW48R_FED_BUSINESS_TYP_KEY
				+ CW48R_FED_BUSINESS_TYP_KEY_CI + CW48R_FED_BUSINESS_TYP_DATA;
		public static final int L_FW_RESP_FED_BUSINESS_TYP = CW48R_FED_BUSINESS_TYP_ROW;
		public static final int CW49R_FED_SEG_INFO_CSUM = 9;
		public static final int CW49R_CLIENT_ID_KCRE = 32;
		public static final int CW49R_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CW49R_EFFECTIVE_DT_KCRE = 32;
		public static final int CW49R_TRANS_PROCESS_DT = 10;
		public static final int CW49R_CLIENT_ID = 20;
		public static final int CW49R_HISTORY_VLD_NBR_SIGNED = 6;
		public static final int CW49R_EFFECTIVE_DT = 10;
		public static final int CW49R_CLIENT_ID_CI = 1;
		public static final int CW49R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW49R_EFFECTIVE_DT_CI = 1;
		public static final int CW49R_SEG_CD_CI = 1;
		public static final int CW49R_SEG_CD = 3;
		public static final int CW49R_USER_ID_CI = 1;
		public static final int CW49R_USER_ID = 8;
		public static final int CW49R_STATUS_CD_CI = 1;
		public static final int CW49R_STATUS_CD = 1;
		public static final int CW49R_TERMINAL_ID_CI = 1;
		public static final int CW49R_TERMINAL_ID = 8;
		public static final int CW49R_EXPIRATION_DT_CI = 1;
		public static final int CW49R_EXPIRATION_DT = 10;
		public static final int CW49R_EFFECTIVE_ACY_TS_CI = 1;
		public static final int CW49R_EFFECTIVE_ACY_TS = 26;
		public static final int CW49R_EXPIRATION_ACY_TS_CI = 1;
		public static final int CW49R_EXPIRATION_ACY_TS_NI = 1;
		public static final int CW49R_EXPIRATION_ACY_TS = 26;
		public static final int CW49R_SEG_DES = 40;
		public static final int CW40R_FED_ACCOUNT_INFO_CSUM = 9;
		public static final int CW40R_CIAI_ACT_TCH_KEY_KCRE = 32;
		public static final int CW40R_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CW40R_EFFECTIVE_DT_KCRE = 32;
		public static final int CW40R_TRANS_PROCESS_DT = 10;
		public static final int CW40R_CIAI_ACT_TCH_KEY = 20;
		public static final int CW40R_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW40R_HISTORY_VLD_NBR = 5;
		public static final int CW40R_EFFECTIVE_DT = 10;
		public static final int CW40R_CIAI_ACT_TCH_KEY_CI = 1;
		public static final int CW40R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW40R_EFFECTIVE_DT_CI = 1;
		public static final int CW40R_OFC_LOC_CD_CI = 1;
		public static final int CW40R_OFC_LOC_CD = 2;
		public static final int CW40R_CIAI_AUTHNTIC_LOSS_CI = 1;
		public static final int CW40R_CIAI_AUTHNTIC_LOSS = 1;
		public static final int CW40R_FED_ST_CD_CI = 1;
		public static final int CW40R_FED_ST_CD = 3;
		public static final int CW40R_FED_COUNTY_CD_CI = 1;
		public static final int CW40R_FED_COUNTY_CD = 3;
		public static final int CW40R_FED_TOWN_CD_CI = 1;
		public static final int CW40R_FED_TOWN_CD = 4;
		public static final int CW40R_USER_ID_CI = 1;
		public static final int CW40R_USER_ID = 8;
		public static final int CW40R_STATUS_CD_CI = 1;
		public static final int CW40R_STATUS_CD = 1;
		public static final int CW40R_TERMINAL_ID_CI = 1;
		public static final int CW40R_TERMINAL_ID = 8;
		public static final int CW40R_EXPIRATION_DT_CI = 1;
		public static final int CW40R_EXPIRATION_DT = 10;
		public static final int CW40R_EFFECTIVE_ACY_TS_CI = 1;
		public static final int CW40R_EFFECTIVE_ACY_TS = 26;
		public static final int CW40R_EXPIRATION_ACY_TS_CI = 1;
		public static final int CW40R_EXPIRATION_ACY_TS_NI = 1;
		public static final int CW40R_EXPIRATION_ACY_TS = 26;
		public static final int CW01R_BUSINESS_CLIENT_CHK_SUM = 9;
		public static final int CW01R_CLIENT_ID_KCRE = 32;
		public static final int CW01R_CIBC_BUS_SEQ_NBR_KCRE = 32;
		public static final int CW01R_TRANS_PROCESS_DT = 10;
		public static final int CW01R_CLIENT_ID_CI = 1;
		public static final int CW01R_CLIENT_ID = 20;
		public static final int CW01R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW01R_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW01R_HISTORY_VLD_NBR = 5;
		public static final int CW01R_CIBC_BUS_SEQ_NBR_CI = 1;
		public static final int CW01R_CIBC_BUS_SEQ_NBR_SIGN = 1;
		public static final int CW01R_CIBC_BUS_SEQ_NBR = 5;
		public static final int CW01R_CIBC_EFF_DT_CI = 1;
		public static final int CW01R_CIBC_EFF_DT = 10;
		public static final int CW01R_CIBC_EXP_DT_CI = 1;
		public static final int CW01R_CIBC_EXP_DT = 10;
		public static final int CW01R_GRS_REV_CD_CI = 1;
		public static final int CW01R_GRS_REV_CD = 2;
		public static final int CW01R_IDY_TYP_CD_CI = 1;
		public static final int CW01R_IDY_TYP_CD = 10;
		public static final int CW01R_CIBC_NBR_EMP_CI = 1;
		public static final int CW01R_CIBC_NBR_EMP_NI = 1;
		public static final int CW01R_CIBC_NBR_EMP_SIGN = 1;
		public static final int CW01R_CIBC_NBR_EMP = 10;
		public static final int CW01R_CIBC_STR_DT_CI = 1;
		public static final int CW01R_CIBC_STR_DT_NI = 1;
		public static final int CW01R_CIBC_STR_DT = 10;
		public static final int CW01R_USER_ID_CI = 1;
		public static final int CW01R_USER_ID = 8;
		public static final int CW01R_STATUS_CD_CI = 1;
		public static final int CW01R_STATUS_CD = 1;
		public static final int CW01R_TERMINAL_ID_CI = 1;
		public static final int CW01R_TERMINAL_ID = 8;
		public static final int CW01R_CIBC_EFF_ACY_TS_CI = 1;
		public static final int CW01R_CIBC_EFF_ACY_TS = 26;
		public static final int CW01R_CIBC_EXP_ACY_TS_CI = 1;
		public static final int CW01R_CIBC_EXP_ACY_TS = 26;
		public static final int CWORC_CLT_OBJ_RELATION_CSUM = 9;
		public static final int CWORC_TCH_OBJECT_KEY_KCRE = 32;
		public static final int CWORC_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CWORC_CIOR_EFF_DT_KCRE = 32;
		public static final int CWORC_OBJ_SYS_ID_KCRE = 32;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_KCRE = 32;
		public static final int CWORC_CLIENT_ID_KCRE = 32;
		public static final int CWORC_TRANS_PROCESS_DT = 10;
		public static final int CWORC_TCH_OBJECT_KEY_CI = 1;
		public static final int CWORC_TCH_OBJECT_KEY = 20;
		public static final int CWORC_HISTORY_VLD_NBR_CI = 1;
		public static final int CWORC_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CWORC_HISTORY_VLD_NBR = 5;
		public static final int CWORC_CIOR_EFF_DT_CI = 1;
		public static final int CWORC_CIOR_EFF_DT = 10;
		public static final int CWORC_OBJ_SYS_ID_CI = 1;
		public static final int CWORC_OBJ_SYS_ID = 4;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_CI = 1;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_SIGN = 1;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR = 5;
		public static final int CWORC_CLIENT_ID_CI = 1;
		public static final int CWORC_CLIENT_ID = 20;
		public static final int CWORC_RLT_TYP_CD_CI = 1;
		public static final int CWORC_RLT_TYP_CD = 4;
		public static final int CWORC_OBJ_CD_CI = 1;
		public static final int CWORC_OBJ_CD = 4;
		public static final int CWORC_CIOR_SHW_OBJ_KEY_CI = 1;
		public static final int CWORC_CIOR_SHW_OBJ_KEY = 30;
		public static final int CWORC_ADR_SEQ_NBR_CI = 1;
		public static final int CWORC_ADR_SEQ_NBR_SIGN = 1;
		public static final int CWORC_ADR_SEQ_NBR = 5;
		public static final int CWORC_USER_ID_CI = 1;
		public static final int CWORC_USER_ID = 8;
		public static final int CWORC_STATUS_CD_CI = 1;
		public static final int CWORC_STATUS_CD = 1;
		public static final int CWORC_TERMINAL_ID_CI = 1;
		public static final int CWORC_TERMINAL_ID = 8;
		public static final int CWORC_CIOR_EXP_DT_CI = 1;
		public static final int CWORC_CIOR_EXP_DT = 10;
		public static final int CWORC_CIOR_EFF_ACY_TS_CI = 1;
		public static final int CWORC_CIOR_EFF_ACY_TS = 26;
		public static final int CWORC_CIOR_EXP_ACY_TS_CI = 1;
		public static final int CWORC_CIOR_EXP_ACY_TS = 26;
		public static final int CWORC_CIOR_LEG_ENT_CD_CI = 1;
		public static final int CWORC_CIOR_LEG_ENT_CD = 2;
		public static final int CWORC_CIOR_FST_NM_CI = 1;
		public static final int CWORC_CIOR_FST_NM = 30;
		public static final int CWORC_CIOR_LST_NM_CI = 1;
		public static final int CWORC_CIOR_LST_NM = 60;
		public static final int CWORC_CIOR_MDL_NM_CI = 1;
		public static final int CWORC_CIOR_MDL_NM = 30;
		public static final int CWORC_CIOR_NM_PFX_CI = 1;
		public static final int CWORC_CIOR_NM_PFX = 4;
		public static final int CWORC_CIOR_NM_SFX_CI = 1;
		public static final int CWORC_CIOR_NM_SFX = 4;
		public static final int CWORC_CIOR_LNG_NM_CI = 1;
		public static final int CWORC_CIOR_LNG_NM_NI = 1;
		public static final int CWORC_CIOR_LNG_NM = 132;
		public static final int CWORC_FILTER_TYPE = 2;
		public static final int CW06R_CLT_CLT_RELATION_CSUM = 9;
		public static final int CW06R_CLIENT_ID_KCRE = 32;
		public static final int CW06R_CLT_TYP_CD_KCRE = 32;
		public static final int CW06R_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CW06R_CICR_XRF_ID_KCRE = 32;
		public static final int CW06R_XRF_TYP_CD_KCRE = 32;
		public static final int CW06R_CICR_EFF_DT_KCRE = 32;
		public static final int CW06R_CLT_CLT_RELATION_FIXED = CW06R_CLT_CLT_RELATION_CSUM + CW06R_CLIENT_ID_KCRE + CW06R_CLT_TYP_CD_KCRE
				+ CW06R_HISTORY_VLD_NBR_KCRE + CW06R_CICR_XRF_ID_KCRE + CW06R_XRF_TYP_CD_KCRE + CW06R_CICR_EFF_DT_KCRE;
		public static final int CW06R_TRANS_PROCESS_DT = 10;
		public static final int CW06R_CLT_CLT_RELATION_DATES = CW06R_TRANS_PROCESS_DT;
		public static final int CW06R_CLIENT_ID_CI = 1;
		public static final int CW06R_CLIENT_ID = 20;
		public static final int CW06R_CLT_TYP_CD_CI = 1;
		public static final int CW06R_CLT_TYP_CD = 4;
		public static final int CW06R_HISTORY_VLD_NBR_CI = 1;
		public static final int CW06R_HISTORY_VLD_NBR_SIGNED = 6;
		public static final int CW06R_CICR_XRF_ID_CI = 1;
		public static final int CW06R_CICR_XRF_ID = 20;
		public static final int CW06R_XRF_TYP_CD_CI = 1;
		public static final int CW06R_XRF_TYP_CD = 4;
		public static final int CW06R_CICR_EFF_DT_CI = 1;
		public static final int CW06R_CICR_EFF_DT = 10;
		public static final int CW06R_CLT_CLT_RELATION_KEY = CW06R_CLIENT_ID_CI + CW06R_CLIENT_ID + CW06R_CLT_TYP_CD_CI + CW06R_CLT_TYP_CD
				+ CW06R_HISTORY_VLD_NBR_CI + CW06R_HISTORY_VLD_NBR_SIGNED + CW06R_CICR_XRF_ID_CI + CW06R_CICR_XRF_ID + CW06R_XRF_TYP_CD_CI
				+ CW06R_XRF_TYP_CD + CW06R_CICR_EFF_DT_CI + CW06R_CICR_EFF_DT;
		public static final int CW06R_CICR_EXP_DT_CI = 1;
		public static final int CW06R_CICR_EXP_DT = 10;
		public static final int CW06R_CICR_NBR_OPR_STRS_CI = 1;
		public static final int CW06R_CICR_NBR_OPR_STRS_NI = 1;
		public static final int CW06R_CICR_NBR_OPR_STRS = 3;
		public static final int CW06R_USER_ID_CI = 1;
		public static final int CW06R_USER_ID = 8;
		public static final int CW06R_STATUS_CD_CI = 1;
		public static final int CW06R_STATUS_CD = 1;
		public static final int CW06R_TERMINAL_ID_CI = 1;
		public static final int CW06R_TERMINAL_ID = 8;
		public static final int CW06R_CICR_EFF_ACY_TS_CI = 1;
		public static final int CW06R_CICR_EFF_ACY_TS = 26;
		public static final int CW06R_CICR_EXP_ACY_TS_CI = 1;
		public static final int CW06R_CICR_EXP_ACY_TS = 26;
		public static final int CW06R_CLT_CLT_RELATION_DATA = CW06R_CICR_EXP_DT_CI + CW06R_CICR_EXP_DT + CW06R_CICR_NBR_OPR_STRS_CI
				+ CW06R_CICR_NBR_OPR_STRS_NI + CW06R_CICR_NBR_OPR_STRS + CW06R_USER_ID_CI + CW06R_USER_ID + CW06R_STATUS_CD_CI + CW06R_STATUS_CD
				+ CW06R_TERMINAL_ID_CI + CW06R_TERMINAL_ID + CW06R_CICR_EFF_ACY_TS_CI + CW06R_CICR_EFF_ACY_TS + CW06R_CICR_EXP_ACY_TS_CI
				+ CW06R_CICR_EXP_ACY_TS;
		public static final int CW06R_CLT_CLT_RELATION_ROW = CW06R_CLT_CLT_RELATION_FIXED + CW06R_CLT_CLT_RELATION_DATES + CW06R_CLT_CLT_RELATION_KEY
				+ CW06R_CLT_CLT_RELATION_DATA;
		public static final int L_FW_RESP_CLT_CLT_REL = CW06R_CLT_CLT_RELATION_ROW;
		public static final int CW02R_CLIENT_TAB_FIXED = CW02R_CLIENT_TAB_CHK_SUM + CW02R_CLIENT_ID_KCRE;
		public static final int CW02R_CLIENT_TAB_DATES = CW02R_TRANS_PROCESS_DT;
		public static final int CW02R_CLIENT_TAB_KEY = CW02R_CLIENT_ID_CI + CW02R_CLIENT_ID + CW02R_HISTORY_VLD_NBR_CI + CW02R_HISTORY_VLD_NBR_SIGN
				+ CW02R_HISTORY_VLD_NBR + CW02R_CICL_EFF_DT_CI + CW02R_CICL_EFF_DT;
		public static final int CW02R_CLIENT_TAB_DATA = CW02R_CICL_PRI_SUB_CD_CI + CW02R_CICL_PRI_SUB_CD + CW02R_CICL_FST_NM_CI + CW02R_CICL_FST_NM
				+ CW02R_CICL_LST_NM_CI + CW02R_CICL_LST_NM + CW02R_CICL_MDL_NM_CI + CW02R_CICL_MDL_NM + CW02R_NM_PFX_CI + CW02R_NM_PFX
				+ CW02R_NM_SFX_CI + CW02R_NM_SFX + CW02R_PRIMARY_PRO_DSN_CD_CI + CW02R_PRIMARY_PRO_DSN_CD_NI + CW02R_PRIMARY_PRO_DSN_CD
				+ CW02R_LEG_ENT_CD_CI + CW02R_LEG_ENT_CD + CW02R_CICL_SDX_CD_CI + CW02R_CICL_SDX_CD + CW02R_CICL_OGN_INCEPT_DT_CI
				+ CW02R_CICL_OGN_INCEPT_DT_NI + CW02R_CICL_OGN_INCEPT_DT + CW02R_CICL_ADD_NM_IND_CI + CW02R_CICL_ADD_NM_IND_NI + CW02R_CICL_ADD_NM_IND
				+ CW02R_CICL_DOB_DT_CI + CW02R_CICL_DOB_DT + CW02R_CICL_BIR_ST_CD_CI + CW02R_CICL_BIR_ST_CD + CW02R_GENDER_CD_CI + CW02R_GENDER_CD
				+ CW02R_PRI_LGG_CD_CI + CW02R_PRI_LGG_CD + CW02R_USER_ID_CI + CW02R_USER_ID + CW02R_STATUS_CD_CI + CW02R_STATUS_CD
				+ CW02R_TERMINAL_ID_CI + CW02R_TERMINAL_ID + CW02R_CICL_EXP_DT_CI + CW02R_CICL_EXP_DT + CW02R_CICL_EFF_ACY_TS_CI
				+ CW02R_CICL_EFF_ACY_TS + CW02R_CICL_EXP_ACY_TS_CI + CW02R_CICL_EXP_ACY_TS + CW02R_STATUTORY_TLE_CD_CI + CW02R_STATUTORY_TLE_CD
				+ CW02R_CICL_LNG_NM_CI + CW02R_CICL_LNG_NM_NI + CW02R_CICL_LNG_NM + CW02R_LO_LST_NM_MCH_CD_CI + CW02R_LO_LST_NM_MCH_CD_NI
				+ CW02R_LO_LST_NM_MCH_CD + CW02R_HI_LST_NM_MCH_CD_CI + CW02R_HI_LST_NM_MCH_CD_NI + CW02R_HI_LST_NM_MCH_CD + CW02R_LO_FST_NM_MCH_CD_CI
				+ CW02R_LO_FST_NM_MCH_CD_NI + CW02R_LO_FST_NM_MCH_CD + CW02R_HI_FST_NM_MCH_CD_CI + CW02R_HI_FST_NM_MCH_CD_NI + CW02R_HI_FST_NM_MCH_CD
				+ CW02R_CICL_ACQ_SRC_CD_CI + CW02R_CICL_ACQ_SRC_CD_NI + CW02R_CICL_ACQ_SRC_CD + CW02R_LEG_ENT_DESC;
		public static final int CW02R_CLIENT_TAB_ROW = CW02R_CLIENT_TAB_FIXED + CW02R_CLIENT_TAB_DATES + CW02R_CLIENT_TAB_KEY + CW02R_CLIENT_TAB_DATA;
		public static final int L_FW_RESP_CLIENT_TAB = CW02R_CLIENT_TAB_ROW;
		public static final int CWEMR_CLIENT_EMAIL_FIXED = CWEMR_CLIENT_EMAIL_CSUM + CWEMR_CLIENT_ID_KCRE + CWEMR_CIEM_SEQ_NBR_KCRE
				+ CWEMR_HISTORY_VLD_NBR_KCRE + CWEMR_EFFECTIVE_DT_KCRE;
		public static final int CWEMR_CLIENT_EMAIL_DATES = CWEMR_TRANS_PROCESS_DT;
		public static final int CWEMR_CLIENT_EMAIL_KEY = CWEMR_CLIENT_ID_CI + CWEMR_CLIENT_ID + CWEMR_CIEM_SEQ_NBR_CI + CWEMR_CIEM_SEQ_NBR_SIGN
				+ CWEMR_CIEM_SEQ_NBR + CWEMR_HISTORY_VLD_NBR_CI + CWEMR_HISTORY_VLD_NBR_SIGN + CWEMR_HISTORY_VLD_NBR + CWEMR_EFFECTIVE_DT_CI
				+ CWEMR_EFFECTIVE_DT;
		public static final int CWEMR_CLIENT_EMAIL_DATA = CWEMR_EMAIL_TYPE_CD_CI + CWEMR_EMAIL_TYPE_CD + CWEMR_USER_ID_CI + CWEMR_USER_ID
				+ CWEMR_STATUS_CD_CI + CWEMR_STATUS_CD + CWEMR_TERMINAL_ID_CI + CWEMR_TERMINAL_ID + CWEMR_EXPIRATION_DT_CI + CWEMR_EXPIRATION_DT
				+ CWEMR_EFFECTIVE_ACY_TS_CI + CWEMR_EFFECTIVE_ACY_TS + CWEMR_EXPIRATION_ACY_TS_CI + CWEMR_EXPIRATION_ACY_TS_NI
				+ CWEMR_EXPIRATION_ACY_TS + CWEMR_CIEM_EMAIL_ADR_TXT_CI + CWEMR_CIEM_EMAIL_ADR_TXT + CWEMR_MORE_ROWS_SW + CWEMR_EMAIL_TYPE_CD_DESC;
		public static final int CWEMR_CLIENT_EMAIL_ROW = CWEMR_CLIENT_EMAIL_FIXED + CWEMR_CLIENT_EMAIL_DATES + CWEMR_CLIENT_EMAIL_KEY
				+ CWEMR_CLIENT_EMAIL_DATA;
		public static final int L_FW_RESP_CLIENT_EMAIL = CWEMR_CLIENT_EMAIL_ROW;
		public static final int CW11R_CLT_REF_RELATION_FIXED = CW11R_CLT_REF_RELATION_CSUM + CW11R_CLIENT_ID_KCRE + CW11R_CIRF_REF_SEQ_NBR_KCRE
				+ CW11R_HISTORY_VLD_NBR_KCRE + CW11R_CIRF_EFF_DT_KCRE;
		public static final int CW11R_CLT_REF_RELATION_DATES = CW11R_TRANS_PROCESS_DT;
		public static final int CW11R_CLT_REF_RELATION_KEY = CW11R_CLIENT_ID_CI + CW11R_CLIENT_ID + CW11R_CIRF_REF_SEQ_NBR_CI
				+ CW11R_CIRF_REF_SEQ_NBR_SIGN + CW11R_CIRF_REF_SEQ_NBR + CW11R_HISTORY_VLD_NBR_CI + CW11R_HISTORY_VLD_NBR_SIGN + CW11R_HISTORY_VLD_NBR
				+ CW11R_CIRF_EFF_DT_CI + CW11R_CIRF_EFF_DT;
		public static final int CW11R_CLT_REF_RELATION_DATA = CW11R_CIRF_REF_ID_CI + CW11R_CIRF_REF_ID + CW11R_REF_TYP_CD_CI + CW11R_REF_TYP_CD
				+ CW11R_CIRF_EXP_DT_CI + CW11R_CIRF_EXP_DT + CW11R_USER_ID_CI + CW11R_USER_ID + CW11R_STATUS_CD_CI + CW11R_STATUS_CD
				+ CW11R_TERMINAL_ID_CI + CW11R_TERMINAL_ID + CW11R_CIRF_EFF_ACY_TS_CI + CW11R_CIRF_EFF_ACY_TS + CW11R_CIRF_EXP_ACY_TS_CI
				+ CW11R_CIRF_EXP_ACY_TS + CW11R_MORE_ROWS_SW;
		public static final int CW11R_CLT_REF_RELATION_ROW = CW11R_CLT_REF_RELATION_FIXED + CW11R_CLT_REF_RELATION_DATES + CW11R_CLT_REF_RELATION_KEY
				+ CW11R_CLT_REF_RELATION_DATA;
		public static final int L_FW_RESP_CLT_REF_RELATION = CW11R_CLT_REF_RELATION_ROW;
		public static final int CW27R_CLIENT_TAX_FIXED = CW27R_CLIENT_TAX_CHK_SUM + CW27R_CLIENT_ID_KCRE + CW27R_CITX_TAX_SEQ_NBR_KCRE;
		public static final int CW27R_CLIENT_TAX_DATES = CW27R_TRANS_PROCESS_DT;
		public static final int CW27R_CLIENT_TAX_KEY = CW27R_CLIENT_ID_CI + CW27R_CLIENT_ID + CW27R_CITX_TAX_SEQ_NBR_CI + CW27R_CITX_TAX_SEQ_NBR_SIGN
				+ CW27R_CITX_TAX_SEQ_NBR + CW27R_HISTORY_VLD_NBR_CI + CW27R_HISTORY_VLD_NBR_SIGN + CW27R_HISTORY_VLD_NBR + CW27R_EFFECTIVE_DT_CI
				+ CW27R_EFFECTIVE_DT;
		public static final int CW27R_CLIENT_TAX_DATA = CW27R_CITX_TAX_ID_CI + CW27R_CITX_TAX_ID + CW27R_TAX_TYPE_CD_CI + CW27R_TAX_TYPE_CD
				+ CW27R_CITX_TAX_ST_CD_CI + CW27R_CITX_TAX_ST_CD_NI + CW27R_CITX_TAX_ST_CD + CW27R_CITX_TAX_CTR_CD_CI + CW27R_CITX_TAX_CTR_CD_NI
				+ CW27R_CITX_TAX_CTR_CD + CW27R_USER_ID_CI + CW27R_USER_ID + CW27R_STATUS_CD_CI + CW27R_STATUS_CD + CW27R_TERMINAL_ID_CI
				+ CW27R_TERMINAL_ID + CW27R_EXPIRATION_DT_CI + CW27R_EXPIRATION_DT + CW27R_EFFECTIVE_ACY_TS_CI + CW27R_EFFECTIVE_ACY_TS
				+ CW27R_EXPIRATION_ACY_TS_CI + CW27R_EXPIRATION_ACY_TS_NI + CW27R_EXPIRATION_ACY_TS + CW27R_MORE_ROWS_SW + CW27R_TAX_TYPE_DESC;
		public static final int CW27R_CLIENT_TAX_ROW = CW27R_CLIENT_TAX_FIXED + CW27R_CLIENT_TAX_DATES + CW27R_CLIENT_TAX_KEY + CW27R_CLIENT_TAX_DATA;
		public static final int L_FW_RESP_CLIENT_TAX = CW27R_CLIENT_TAX_ROW;
		public static final int CW45R_CLT_MUT_TER_STC_FIXED = CW45R_CLT_MUT_TER_STC_CSUM + CW45R_TER_CLT_ID_KCRE + CW45R_TER_LEVEL_CD_KCRE
				+ CW45R_TER_NBR_KCRE;
		public static final int CW45R_CLT_MUT_TER_STC_DATES = CW45R_TRANS_PROCESS_DT;
		public static final int CW45R_CLT_MUT_TER_STC_KEY = CW45R_TER_CLT_ID + CW45R_TER_LEVEL_CD + CW45R_TER_NBR;
		public static final int CW45R_CLT_MUT_TER_STC_KEY_CI = CW45R_TER_CLT_ID_CI + CW45R_TER_LEVEL_CD_CI + CW45R_TER_NBR_CI;
		public static final int CW45R_CLT_MUT_TER_STC_DATA = CW45R_MR_TER_NBR_CI + CW45R_MR_TER_NBR + CW45R_MR_TER_CLT_ID_CI + CW45R_MR_TER_CLT_ID
				+ CW45R_MR_NM_PFX_CI + CW45R_MR_NM_PFX + CW45R_MR_FST_NM_CI + CW45R_MR_FST_NM + CW45R_MR_MDL_NM_CI + CW45R_MR_MDL_NM
				+ CW45R_MR_LST_NM_CI + CW45R_MR_LST_NM + CW45R_MR_NM_CI + CW45R_MR_NM + CW45R_MR_NM_SFX_CI + CW45R_MR_NM_SFX + CW45R_MR_CLT_ID_CI
				+ CW45R_MR_CLT_ID + CW45R_CSR_TER_NBR_CI + CW45R_CSR_TER_NBR + CW45R_CSR_TER_CLT_ID_CI + CW45R_CSR_TER_CLT_ID + CW45R_CSR_NM_PFX_CI
				+ CW45R_CSR_NM_PFX + CW45R_CSR_FST_NM_CI + CW45R_CSR_FST_NM + CW45R_CSR_MDL_NM_CI + CW45R_CSR_MDL_NM + CW45R_CSR_LST_NM_CI
				+ CW45R_CSR_LST_NM + CW45R_CSR_NM_CI + CW45R_CSR_NM + CW45R_CSR_NM_SFX_CI + CW45R_CSR_NM_SFX + CW45R_CSR_CLT_ID_CI + CW45R_CSR_CLT_ID
				+ CW45R_SMR_TER_NBR_CI + CW45R_SMR_TER_NBR + CW45R_SMR_TER_CLT_ID_CI + CW45R_SMR_TER_CLT_ID + CW45R_SMR_NM_PFX_CI + CW45R_SMR_NM_PFX
				+ CW45R_SMR_FST_NM_CI + CW45R_SMR_FST_NM + CW45R_SMR_MDL_NM_CI + CW45R_SMR_MDL_NM + CW45R_SMR_LST_NM_CI + CW45R_SMR_LST_NM
				+ CW45R_SMR_NM_CI + CW45R_SMR_NM + CW45R_SMR_NM_SFX_CI + CW45R_SMR_NM_SFX + CW45R_SMR_CLT_ID_CI + CW45R_SMR_CLT_ID
				+ CW45R_DMM_TER_NBR_CI + CW45R_DMM_TER_NBR + CW45R_DMM_TER_CLT_ID_CI + CW45R_DMM_TER_CLT_ID + CW45R_DMM_NM_PFX_CI + CW45R_DMM_NM_PFX
				+ CW45R_DMM_FST_NM_CI + CW45R_DMM_FST_NM + CW45R_DMM_MDL_NM_CI + CW45R_DMM_MDL_NM + CW45R_DMM_LST_NM_CI + CW45R_DMM_LST_NM
				+ CW45R_DMM_NM_CI + CW45R_DMM_NM + CW45R_DMM_NM_SFX_CI + CW45R_DMM_NM_SFX + CW45R_DMM_CLT_ID_CI + CW45R_DMM_CLT_ID
				+ CW45R_RMM_TER_NBR_CI + CW45R_RMM_TER_NBR + CW45R_RMM_TER_CLT_ID_CI + CW45R_RMM_TER_CLT_ID + CW45R_RMM_NM_PFX_CI + CW45R_RMM_NM_PFX
				+ CW45R_RMM_FST_NM_CI + CW45R_RMM_FST_NM + CW45R_RMM_MDL_NM_CI + CW45R_RMM_MDL_NM + CW45R_RMM_LST_NM_CI + CW45R_RMM_LST_NM
				+ CW45R_RMM_NM_CI + CW45R_RMM_NM + CW45R_RMM_NM_SFX_CI + CW45R_RMM_NM_SFX + CW45R_RMM_CLT_ID_CI + CW45R_RMM_CLT_ID
				+ CW45R_DFO_TER_NBR_CI + CW45R_DFO_TER_NBR + CW45R_DFO_TER_CLT_ID_CI + CW45R_DFO_TER_CLT_ID + CW45R_DFO_NM_PFX_CI + CW45R_DFO_NM_PFX
				+ CW45R_DFO_FST_NM_CI + CW45R_DFO_FST_NM + CW45R_DFO_MDL_NM_CI + CW45R_DFO_MDL_NM + CW45R_DFO_LST_NM_CI + CW45R_DFO_LST_NM
				+ CW45R_DFO_NM_CI + CW45R_DFO_NM + CW45R_DFO_NM_SFX_CI + CW45R_DFO_NM_SFX + CW45R_DFO_CLT_ID_CI + CW45R_DFO_CLT_ID;
		public static final int CW45R_CLT_MUT_TER_STC_ROW = CW45R_CLT_MUT_TER_STC_FIXED + CW45R_CLT_MUT_TER_STC_DATES + CW45R_CLT_MUT_TER_STC_KEY
				+ CW45R_CLT_MUT_TER_STC_KEY_CI + CW45R_CLT_MUT_TER_STC_DATA;
		public static final int L_FW_RESP_CLT_MUT_TER_STC = CW45R_CLT_MUT_TER_STC_ROW;
		public static final int CW46R_CLT_AGC_TER_STC_FIXED = CW46R_CLT_AGC_TER_STC_CSUM + CW46R_TER_CLT_ID_KCRE + CW46R_TER_LEVEL_CD_KCRE;
		public static final int CW46R_CLT_AGC_TER_STC_DATES = CW46R_TRANS_PROCESS_DT;
		public static final int CW46R_CLT_AGC_TER_STC_KEY = CW46R_TER_CLT_ID + CW46R_TER_LEVEL_CD;
		public static final int CW46R_CLT_AGC_TER_STC_KEY_CI = CW46R_TER_CLT_ID_CI + CW46R_TER_LEVEL_CD_CI;
		public static final int CW46R_CLT_AGC_TER_STC_DATA = CW46R_TER_NBR_CI + CW46R_TER_NBR + CW46R_BRN_TER_NBR_CI + CW46R_BRN_TER_NBR
				+ CW46R_BRN_TER_CLT_ID_CI + CW46R_BRN_TER_CLT_ID + CW46R_BRN_NM_CI + CW46R_BRN_NM + CW46R_BRN_CLT_ID_CI + CW46R_BRN_CLT_ID
				+ CW46R_AGC_TER_NBR_CI + CW46R_AGC_TER_NBR + CW46R_AGC_TER_CLT_ID_CI + CW46R_AGC_TER_CLT_ID + CW46R_AGC_NM_CI + CW46R_AGC_NM
				+ CW46R_AGC_CLT_ID_CI + CW46R_AGC_CLT_ID + CW46R_SMR_TER_NBR_CI + CW46R_SMR_TER_NBR + CW46R_SMR_TER_CLT_ID_CI + CW46R_SMR_TER_CLT_ID
				+ CW46R_SMR_NM_CI + CW46R_SMR_NM + CW46R_SMR_CLT_ID_CI + CW46R_SMR_CLT_ID + CW46R_AMM_TER_NBR_CI + CW46R_AMM_TER_NBR
				+ CW46R_AMM_TER_CLT_ID_CI + CW46R_AMM_TER_CLT_ID + CW46R_AMM_NM_PFX_CI + CW46R_AMM_NM_PFX + CW46R_AMM_FST_NM_CI + CW46R_AMM_FST_NM
				+ CW46R_AMM_MDL_NM_CI + CW46R_AMM_MDL_NM + CW46R_AMM_LST_NM_CI + CW46R_AMM_LST_NM + CW46R_AMM_NM_CI + CW46R_AMM_NM
				+ CW46R_AMM_NM_SFX_CI + CW46R_AMM_NM_SFX + CW46R_AMM_CLT_ID_CI + CW46R_AMM_CLT_ID + CW46R_AFM_TER_NBR_CI + CW46R_AFM_TER_NBR
				+ CW46R_AFM_TER_CLT_ID_CI + CW46R_AFM_TER_CLT_ID + CW46R_AFM_NM_PFX_CI + CW46R_AFM_NM_PFX + CW46R_AFM_FST_NM_CI + CW46R_AFM_FST_NM
				+ CW46R_AFM_MDL_NM_CI + CW46R_AFM_MDL_NM + CW46R_AFM_LST_NM_CI + CW46R_AFM_LST_NM + CW46R_AFM_NM_CI + CW46R_AFM_NM
				+ CW46R_AFM_NM_SFX_CI + CW46R_AFM_NM_SFX + CW46R_AFM_CLT_ID_CI + CW46R_AFM_CLT_ID + CW46R_AVP_TER_NBR_CI + CW46R_AVP_TER_NBR
				+ CW46R_AVP_TER_CLT_ID_CI + CW46R_AVP_TER_CLT_ID + CW46R_AVP_NM_PFX_CI + CW46R_AVP_NM_PFX + CW46R_AVP_FST_NM_CI + CW46R_AVP_FST_NM
				+ CW46R_AVP_MDL_NM_CI + CW46R_AVP_MDL_NM + CW46R_AVP_LST_NM_CI + CW46R_AVP_LST_NM + CW46R_AVP_NM_CI + CW46R_AVP_NM
				+ CW46R_AVP_NM_SFX_CI + CW46R_AVP_NM_SFX + CW46R_AVP_CLT_ID_CI + CW46R_AVP_CLT_ID;
		public static final int CW46R_CLT_AGC_TER_STC_ROW = CW46R_CLT_AGC_TER_STC_FIXED + CW46R_CLT_AGC_TER_STC_DATES + CW46R_CLT_AGC_TER_STC_KEY
				+ CW46R_CLT_AGC_TER_STC_KEY_CI + CW46R_CLT_AGC_TER_STC_DATA;
		public static final int L_FW_RESP_CLT_AGC_TER_STC = CW46R_CLT_AGC_TER_STC_ROW;
		public static final int MUA02_CLT_CLT_RELATION_FIXED = MUA02_CLT_CLT_RELATION_CSUM + MUA02_CLIENT_ID_KCRE + MUA02_CLT_TYP_CD_KCRE
				+ MUA02_HISTORY_VLD_NBR_KCRE + MUA02_CICR_XRF_ID_KCRE + MUA02_XRF_TYP_CD_KCRE + MUA02_CICR_EFF_DT_KCRE;
		public static final int MUA02_CLT_CLT_RELATION_DATES = MUA02_TRANS_PROCESS_DT;
		public static final int MUA02_CLT_CLT_RELATION_KEY = MUA02_CLIENT_ID_CI + MUA02_CLIENT_ID + MUA02_CLT_TYP_CD_CI + MUA02_CLT_TYP_CD
				+ MUA02_HISTORY_VLD_NBR_CI + MUA02_HISTORY_VLD_NBR_SIGN + MUA02_HISTORY_VLD_NBR + MUA02_CICR_XRF_ID_CI + MUA02_CICR_XRF_ID
				+ MUA02_XRF_TYP_CD_CI + MUA02_XRF_TYP_CD + MUA02_CICR_EFF_DT_CI + MUA02_CICR_EFF_DT;
		public static final int MUA02_CLT_CLT_RELATION_DATA = MUA02_CICR_EXP_DT_CI + MUA02_CICR_EXP_DT + MUA02_CICR_NBR_OPR_STRS_CI
				+ MUA02_CICR_NBR_OPR_STRS_NI + MUA02_CICR_NBR_OPR_STRS + MUA02_USER_ID_CI + MUA02_USER_ID + MUA02_STATUS_CD_CI + MUA02_STATUS_CD
				+ MUA02_TERMINAL_ID_CI + MUA02_TERMINAL_ID + MUA02_CICR_EFF_ACY_TS_CI + MUA02_CICR_EFF_ACY_TS + MUA02_CICR_EXP_ACY_TS_CI
				+ MUA02_CICR_EXP_ACY_TS + MUA02_CICR_LEG_ENT_CD_CI + MUA02_CICR_LEG_ENT_CD + MUA02_CICR_FST_NM_CI + MUA02_CICR_FST_NM
				+ MUA02_CICR_LST_NM_CI + MUA02_CICR_LST_NM + MUA02_CICR_MDL_NM_CI + MUA02_CICR_MDL_NM + MUA02_NM_PFX_CI + MUA02_NM_PFX
				+ MUA02_NM_SFX_CI + MUA02_NM_SFX + MUA02_CICR_LNG_NM_CI + MUA02_CICR_LNG_NM_NI + MUA02_CICR_LNG_NM + MUA02_CICR_REL_CHG_SW
				+ MUA02_CLIENT_DES;
		public static final int MUA02_CLT_CLT_RELATION_ROW = MUA02_CLT_CLT_RELATION_FIXED + MUA02_CLT_CLT_RELATION_DATES + MUA02_CLT_CLT_RELATION_KEY
				+ MUA02_CLT_CLT_RELATION_DATA;
		public static final int L_FW_RESP_CLT_UW_BPO = MUA02_CLT_CLT_RELATION_ROW;
		public static final int CW49R_FED_SEG_INFO_FIXED = CW49R_FED_SEG_INFO_CSUM + CW49R_CLIENT_ID_KCRE + CW49R_HISTORY_VLD_NBR_KCRE
				+ CW49R_EFFECTIVE_DT_KCRE;
		public static final int CW49R_FED_SEG_INFO_DATES = CW49R_TRANS_PROCESS_DT;
		public static final int CW49R_FED_SEG_INFO_KEY = CW49R_CLIENT_ID + CW49R_HISTORY_VLD_NBR_SIGNED + CW49R_EFFECTIVE_DT;
		public static final int CW49R_FED_SEG_INFO_KEY_CI = CW49R_CLIENT_ID_CI + CW49R_HISTORY_VLD_NBR_CI + CW49R_EFFECTIVE_DT_CI;
		public static final int CW49R_FED_SEG_INFO_DATA = CW49R_SEG_CD_CI + CW49R_SEG_CD + CW49R_USER_ID_CI + CW49R_USER_ID + CW49R_STATUS_CD_CI
				+ CW49R_STATUS_CD + CW49R_TERMINAL_ID_CI + CW49R_TERMINAL_ID + CW49R_EXPIRATION_DT_CI + CW49R_EXPIRATION_DT
				+ CW49R_EFFECTIVE_ACY_TS_CI + CW49R_EFFECTIVE_ACY_TS + CW49R_EXPIRATION_ACY_TS_CI + CW49R_EXPIRATION_ACY_TS_NI
				+ CW49R_EXPIRATION_ACY_TS + CW49R_SEG_DES;
		public static final int CW49R_FED_SEG_INFO_ROW = CW49R_FED_SEG_INFO_FIXED + CW49R_FED_SEG_INFO_DATES + CW49R_FED_SEG_INFO_KEY
				+ CW49R_FED_SEG_INFO_KEY_CI + CW49R_FED_SEG_INFO_DATA;
		public static final int L_FW_RESP_FED_SEG_INFO = CW49R_FED_SEG_INFO_ROW;
		public static final int CW40R_FED_ACCOUNT_INFO_FIXED = CW40R_FED_ACCOUNT_INFO_CSUM + CW40R_CIAI_ACT_TCH_KEY_KCRE + CW40R_HISTORY_VLD_NBR_KCRE
				+ CW40R_EFFECTIVE_DT_KCRE;
		public static final int CW40R_FED_ACCOUNT_INFO_DATES = CW40R_TRANS_PROCESS_DT;
		public static final int CW40R_FED_ACCOUNT_INFO_KEY = CW40R_CIAI_ACT_TCH_KEY + CW40R_HISTORY_VLD_NBR_SIGN + CW40R_HISTORY_VLD_NBR
				+ CW40R_EFFECTIVE_DT;
		public static final int CW40R_FED_ACCOUNT_INFO_KEY_CI = CW40R_CIAI_ACT_TCH_KEY_CI + CW40R_HISTORY_VLD_NBR_CI + CW40R_EFFECTIVE_DT_CI;
		public static final int CW40R_FED_ACCOUNT_INFO_DATA = CW40R_OFC_LOC_CD_CI + CW40R_OFC_LOC_CD + CW40R_CIAI_AUTHNTIC_LOSS_CI
				+ CW40R_CIAI_AUTHNTIC_LOSS + CW40R_FED_ST_CD_CI + CW40R_FED_ST_CD + CW40R_FED_COUNTY_CD_CI + CW40R_FED_COUNTY_CD
				+ CW40R_FED_TOWN_CD_CI + CW40R_FED_TOWN_CD + CW40R_USER_ID_CI + CW40R_USER_ID + CW40R_STATUS_CD_CI + CW40R_STATUS_CD
				+ CW40R_TERMINAL_ID_CI + CW40R_TERMINAL_ID + CW40R_EXPIRATION_DT_CI + CW40R_EXPIRATION_DT + CW40R_EFFECTIVE_ACY_TS_CI
				+ CW40R_EFFECTIVE_ACY_TS + CW40R_EXPIRATION_ACY_TS_CI + CW40R_EXPIRATION_ACY_TS_NI + CW40R_EXPIRATION_ACY_TS;
		public static final int CW40R_FED_ACCOUNT_INFO_ROW = CW40R_FED_ACCOUNT_INFO_FIXED + CW40R_FED_ACCOUNT_INFO_DATES + CW40R_FED_ACCOUNT_INFO_KEY
				+ CW40R_FED_ACCOUNT_INFO_KEY_CI + CW40R_FED_ACCOUNT_INFO_DATA;
		public static final int L_FW_RESP_FED_ACT_INFO = CW40R_FED_ACCOUNT_INFO_ROW;
		public static final int CW01R_BUSINESS_CLIENT_FIXED = CW01R_BUSINESS_CLIENT_CHK_SUM + CW01R_CLIENT_ID_KCRE + CW01R_CIBC_BUS_SEQ_NBR_KCRE;
		public static final int CW01R_BUSINESS_CLIENT_DATES = CW01R_TRANS_PROCESS_DT;
		public static final int CW01R_BUSINESS_CLIENT_KEY = CW01R_CLIENT_ID_CI + CW01R_CLIENT_ID + CW01R_HISTORY_VLD_NBR_CI
				+ CW01R_HISTORY_VLD_NBR_SIGN + CW01R_HISTORY_VLD_NBR + CW01R_CIBC_BUS_SEQ_NBR_CI + CW01R_CIBC_BUS_SEQ_NBR_SIGN
				+ CW01R_CIBC_BUS_SEQ_NBR + CW01R_CIBC_EFF_DT_CI + CW01R_CIBC_EFF_DT;
		public static final int CW01R_BUSINESS_CLIENT_DATA = CW01R_CIBC_EXP_DT_CI + CW01R_CIBC_EXP_DT + CW01R_GRS_REV_CD_CI + CW01R_GRS_REV_CD
				+ CW01R_IDY_TYP_CD_CI + CW01R_IDY_TYP_CD + CW01R_CIBC_NBR_EMP_CI + CW01R_CIBC_NBR_EMP_NI + CW01R_CIBC_NBR_EMP_SIGN
				+ CW01R_CIBC_NBR_EMP + CW01R_CIBC_STR_DT_CI + CW01R_CIBC_STR_DT_NI + CW01R_CIBC_STR_DT + CW01R_USER_ID_CI + CW01R_USER_ID
				+ CW01R_STATUS_CD_CI + CW01R_STATUS_CD + CW01R_TERMINAL_ID_CI + CW01R_TERMINAL_ID + CW01R_CIBC_EFF_ACY_TS_CI + CW01R_CIBC_EFF_ACY_TS
				+ CW01R_CIBC_EXP_ACY_TS_CI + CW01R_CIBC_EXP_ACY_TS;
		public static final int CW01R_BUSINESS_CLIENT_ROW = CW01R_BUSINESS_CLIENT_FIXED + CW01R_BUSINESS_CLIENT_DATES + CW01R_BUSINESS_CLIENT_KEY
				+ CW01R_BUSINESS_CLIENT_DATA;
		public static final int L_FW_RESP_BUSINESS_CLT = CW01R_BUSINESS_CLIENT_ROW;
		public static final int CWORC_CLT_OBJ_RELATION_FIXED = CWORC_CLT_OBJ_RELATION_CSUM + CWORC_TCH_OBJECT_KEY_KCRE + CWORC_HISTORY_VLD_NBR_KCRE
				+ CWORC_CIOR_EFF_DT_KCRE + CWORC_OBJ_SYS_ID_KCRE + CWORC_CIOR_OBJ_SEQ_NBR_KCRE + CWORC_CLIENT_ID_KCRE;
		public static final int CWORC_CLT_OBJ_RELATION_DATES = CWORC_TRANS_PROCESS_DT;
		public static final int CWORC_CLT_OBJ_RELATION_KEY = CWORC_TCH_OBJECT_KEY_CI + CWORC_TCH_OBJECT_KEY + CWORC_HISTORY_VLD_NBR_CI
				+ CWORC_HISTORY_VLD_NBR_SIGN + CWORC_HISTORY_VLD_NBR + CWORC_CIOR_EFF_DT_CI + CWORC_CIOR_EFF_DT + CWORC_OBJ_SYS_ID_CI
				+ CWORC_OBJ_SYS_ID + CWORC_CIOR_OBJ_SEQ_NBR_CI + CWORC_CIOR_OBJ_SEQ_NBR_SIGN + CWORC_CIOR_OBJ_SEQ_NBR;
		public static final int CWORC_CLT_OBJ_RELATION_DATA = CWORC_CLIENT_ID_CI + CWORC_CLIENT_ID + CWORC_RLT_TYP_CD_CI + CWORC_RLT_TYP_CD
				+ CWORC_OBJ_CD_CI + CWORC_OBJ_CD + CWORC_CIOR_SHW_OBJ_KEY_CI + CWORC_CIOR_SHW_OBJ_KEY + CWORC_ADR_SEQ_NBR_CI + CWORC_ADR_SEQ_NBR_SIGN
				+ CWORC_ADR_SEQ_NBR + CWORC_USER_ID_CI + CWORC_USER_ID + CWORC_STATUS_CD_CI + CWORC_STATUS_CD + CWORC_TERMINAL_ID_CI
				+ CWORC_TERMINAL_ID + CWORC_CIOR_EXP_DT_CI + CWORC_CIOR_EXP_DT + CWORC_CIOR_EFF_ACY_TS_CI + CWORC_CIOR_EFF_ACY_TS
				+ CWORC_CIOR_EXP_ACY_TS_CI + CWORC_CIOR_EXP_ACY_TS + CWORC_CIOR_LEG_ENT_CD_CI + CWORC_CIOR_LEG_ENT_CD + CWORC_CIOR_FST_NM_CI
				+ CWORC_CIOR_FST_NM + CWORC_CIOR_LST_NM_CI + CWORC_CIOR_LST_NM + CWORC_CIOR_MDL_NM_CI + CWORC_CIOR_MDL_NM + CWORC_CIOR_NM_PFX_CI
				+ CWORC_CIOR_NM_PFX + CWORC_CIOR_NM_SFX_CI + CWORC_CIOR_NM_SFX + CWORC_CIOR_LNG_NM_CI + CWORC_CIOR_LNG_NM_NI + CWORC_CIOR_LNG_NM;
		public static final int CWORC_CLT_OBJ_INPUT_DATA = CWORC_FILTER_TYPE;
		public static final int CWORC_CLT_OBJ_RELATION_ROW = CWORC_CLT_OBJ_RELATION_FIXED + CWORC_CLT_OBJ_RELATION_DATES + CWORC_CLT_OBJ_RELATION_KEY
				+ CWORC_CLT_OBJ_RELATION_DATA + CWORC_CLT_OBJ_INPUT_DATA;
		public static final int L_FW_RESP_REL_CLT_OBJ_REL = CWORC_CLT_OBJ_RELATION_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_CLIENT_TAB
				+ LFrameworkResponseArea.L_FW_RESP_CLT_ADR_COMPOSITE_MAXOCCURS * L_FW_RESP_CLT_ADR_COMPOSITE + L_FW_RESP_CLIENT_EMAIL
				+ LFrameworkResponseArea.L_FW_RESP_CLIENT_PHONE_MAXOCCURS * L_FW_RESP_CLIENT_PHONE
				+ LFrameworkResponseArea.L_FW_RESP_CLT_OBJ_RELATION_MAXOCCURS * L_FW_RESP_CLT_OBJ_RELATION + L_FW_RESP_CLT_REF_RELATION
				+ L_FW_RESP_CLIENT_TAX + L_FW_RESP_CLT_MUT_TER_STC + L_FW_RESP_CLT_AGC_TER_STC + L_FW_RESP_CLT_UW_BPO
				+ LFrameworkResponseArea.L_FW_RESP_FED_BUSINESS_TYP_MAXOCCURS * L_FW_RESP_FED_BUSINESS_TYP + L_FW_RESP_FED_SEG_INFO
				+ L_FW_RESP_FED_ACT_INFO + L_FW_RESP_BUSINESS_CLT + L_FW_RESP_REL_CLT_OBJ_REL
				+ LFrameworkResponseArea.L_FW_RESP_CLT_CLT_REL_MAXOCCURS * L_FW_RESP_CLT_CLT_REL;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CW49R_HISTORY_VLD_NBR_SIGNED = 5;
			public static final int CW06R_HISTORY_VLD_NBR_SIGNED = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
