/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalUowPrcSeqV;

/**Original name: DCLHAL-UOW-PRC-SEQ<br>
 * Variable: DCLHAL-UOW-PRC-SEQ from copybook HALLGUPS<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalUowPrcSeq implements IHalUowPrcSeqV {

	//==== PROPERTIES ====
	//Original name: HUPS-UOW-NM
	private String hupsUowNm = DefaultValues.stringVal(Len.HUPS_UOW_NM);
	//Original name: UOW-SEQ-NBR
	private short uowSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: ROOT-BOBJ-NM
	private String rootBobjNm = DefaultValues.stringVal(Len.ROOT_BOBJ_NM);
	//Original name: HUPS-BRN-PRC-CD
	private char hupsBrnPrcCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public byte[] getDclhalUowPrcSeqBytes() {
		byte[] buffer = new byte[Len.DCLHAL_UOW_PRC_SEQ];
		return getDclhalUowPrcSeqBytes(buffer, 1);
	}

	public byte[] getDclhalUowPrcSeqBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, hupsUowNm, Len.HUPS_UOW_NM);
		position += Len.HUPS_UOW_NM;
		MarshalByte.writeBinaryShort(buffer, position, uowSeqNbr);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, rootBobjNm, Len.ROOT_BOBJ_NM);
		position += Len.ROOT_BOBJ_NM;
		MarshalByte.writeChar(buffer, position, hupsBrnPrcCd);
		return buffer;
	}

	@Override
	public void setHupsUowNm(String hupsUowNm) {
		this.hupsUowNm = Functions.subString(hupsUowNm, Len.HUPS_UOW_NM);
	}

	@Override
	public String getHupsUowNm() {
		return this.hupsUowNm;
	}

	public String getHupsUowNmFormatted() {
		return Functions.padBlanks(getHupsUowNm(), Len.HUPS_UOW_NM);
	}

	@Override
	public void setUowSeqNbr(short uowSeqNbr) {
		this.uowSeqNbr = uowSeqNbr;
	}

	@Override
	public short getUowSeqNbr() {
		return this.uowSeqNbr;
	}

	@Override
	public void setRootBobjNm(String rootBobjNm) {
		this.rootBobjNm = Functions.subString(rootBobjNm, Len.ROOT_BOBJ_NM);
	}

	@Override
	public String getRootBobjNm() {
		return this.rootBobjNm;
	}

	@Override
	public void setHupsBrnPrcCd(char hupsBrnPrcCd) {
		this.hupsBrnPrcCd = hupsBrnPrcCd;
	}

	@Override
	public char getHupsBrnPrcCd() {
		return this.hupsBrnPrcCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HUPS_UOW_NM = 32;
		public static final int ROOT_BOBJ_NM = 32;
		public static final int UOW_SEQ_NBR = 2;
		public static final int HUPS_BRN_PRC_CD = 1;
		public static final int DCLHAL_UOW_PRC_SEQ = HUPS_UOW_NM + UOW_SEQ_NBR + ROOT_BOBJ_NM + HUPS_BRN_PRC_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
