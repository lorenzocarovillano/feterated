/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.ICltObjRelationV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [CLT_OBJ_RELATION_V]
 * 
 */
public class CltObjRelationVDao extends BaseSqlDao<ICltObjRelationV> {

	private Cursor wsObjCursor1;
	private final IRowMapper<ICltObjRelationV> fetchWsObjCursor1Rm = buildNamedRowMapper(ICltObjRelationV.class, "tchObjectKey", "historyVldNbr",
			"ciorEffDt", "objSysId", "ciorObjSeqNbr", "clientId", "rltTypCd", "objCd", "ciorShwObjKey", "adrSeqNbr", "userId", "statusCd",
			"terminalId", "ciorExpDt", "ciorEffAcyTs", "ciorExpAcyTs");

	public CltObjRelationVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ICltObjRelationV> getToClass() {
		return ICltObjRelationV.class;
	}

	public DbAccessStatus openWsObjCursor1(String cw08hCiorShwObjKey) {
		wsObjCursor1 = buildQuery("openWsObjCursor1").bind("cw08hCiorShwObjKey", cw08hCiorShwObjKey).open();
		return dbStatus;
	}

	public DbAccessStatus closeWsObjCursor1() {
		return closeCursor(wsObjCursor1);
	}

	public ICltObjRelationV fetchWsObjCursor1(ICltObjRelationV iCltObjRelationV) {
		return fetch(wsObjCursor1, iCltObjRelationV, fetchWsObjCursor1Rm);
	}
}
