/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-01-MAX-ROWS-RETURNED-MSG<br>
 * Variable: EA-01-MAX-ROWS-RETURNED-MSG from program XZ0B90S0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01MaxRowsReturnedMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-MAX-ROWS-RETURNED-MSG
	private String flr1 = "The maximum";
	//Original name: FILLER-EA-01-MAX-ROWS-RETURNED-MSG-1
	private String flr2 = "number of";
	//Original name: FILLER-EA-01-MAX-ROWS-RETURNED-MSG-2
	private String flr3 = "policies";
	//Original name: FILLER-EA-01-MAX-ROWS-RETURNED-MSG-3
	private char flr4 = '<';
	//Original name: EA-01-MAX-ROWS-RETURNED
	private String ea01MaxRowsReturned = DefaultValues.stringVal(Len.EA01_MAX_ROWS_RETURNED);
	//Original name: FILLER-EA-01-MAX-ROWS-RETURNED-MSG-4
	private char flr5 = '>';
	//Original name: FILLER-EA-01-MAX-ROWS-RETURNED-MSG-5
	private String flr6 = "have been";
	//Original name: FILLER-EA-01-MAX-ROWS-RETURNED-MSG-6
	private String flr7 = "located for";
	//Original name: FILLER-EA-01-MAX-ROWS-RETURNED-MSG-7
	private String flr8 = "this search.";

	//==== METHODS ====
	public String getEa01MaxRowsReturnedMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01MaxRowsReturnedMsgBytes());
	}

	public byte[] getEa01MaxRowsReturnedMsgBytes() {
		byte[] buffer = new byte[Len.EA01_MAX_ROWS_RETURNED_MSG];
		return getEa01MaxRowsReturnedMsgBytes(buffer, 1);
	}

	public byte[] getEa01MaxRowsReturnedMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeChar(buffer, position, flr4);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ea01MaxRowsReturned, Len.EA01_MAX_ROWS_RETURNED);
		position += Len.EA01_MAX_ROWS_RETURNED;
		MarshalByte.writeChar(buffer, position, flr5);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR1);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public char getFlr4() {
		return this.flr4;
	}

	public void setEa01MaxRowsReturned(long ea01MaxRowsReturned) {
		this.ea01MaxRowsReturned = PicFormatter.display("Z(2)9").format(ea01MaxRowsReturned).toString();
	}

	public long getEa01MaxRowsReturned() {
		return PicParser.display("Z(2)9").parseLong(this.ea01MaxRowsReturned);
	}

	public char getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA01_MAX_ROWS_RETURNED = 3;
		public static final int FLR1 = 12;
		public static final int FLR2 = 10;
		public static final int FLR3 = 9;
		public static final int FLR4 = 1;
		public static final int EA01_MAX_ROWS_RETURNED_MSG = EA01_MAX_ROWS_RETURNED + 3 * FLR1 + 2 * FLR2 + FLR3 + 2 * FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
