/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-PARAGRAPH-NAMES<br>
 * Variable: CF-PARAGRAPH-NAMES from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfParagraphNamesXz001000 {

	//==== PROPERTIES ====
	//Original name: CF-PN-3100
	private String pn3100 = "3100";
	//Original name: CF-PN-4000
	private String pn4000 = "4000";
	//Original name: CF-PN-9100
	private String pn9100 = "9100";
	//Original name: CF-PN-9200
	private String pn9200 = "9200";
	//Original name: CF-PN-9300
	private String pn9300 = "9300";
	//Original name: CF-PN-9600
	private String pn9600 = "9600";
	//Original name: CF-PN-9610
	private String pn9610 = "9610";

	//==== METHODS ====
	public String getPn3100() {
		return this.pn3100;
	}

	public String getPn4000() {
		return this.pn4000;
	}

	public String getPn9100() {
		return this.pn9100;
	}

	public String getPn9200() {
		return this.pn9200;
	}

	public String getPn9300() {
		return this.pn9300;
	}

	public String getPn9600() {
		return this.pn9600;
	}

	public String getPn9610() {
		return this.pn9610;
	}
}
