/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.UidgErridErrorCode;

/**Original name: UIDG-ERROR-ID-OUTPUT<br>
 * Variable: UIDG-ERROR-ID-OUTPUT from program HALOUIDG<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class UidgErrorIdOutput {

	//==== PROPERTIES ====
	//Original name: UIDG-GENERATED-ERROR-REF
	private String generatedErrorRef = DefaultValues.stringVal(Len.GENERATED_ERROR_REF);
	//Original name: UIDG-ERRID-ERROR-CODE
	private UidgErridErrorCode erridErrorCode = new UidgErridErrorCode();
	//Original name: UIDG-ERRID-ERROR-SQLCD
	private String erridErrorSqlcd = DefaultValues.stringVal(Len.ERRID_ERROR_SQLCD);
	//Original name: FILLER-UIDG-ERROR-ID-OUTPUT
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setUidgGeneratedId(String uidgGeneratedId) {
		int position = 1;
		byte[] buffer = getUidgErrorIdOutputBytes();
		MarshalByte.writeString(buffer, position, uidgGeneratedId, Len.UIDG_GENERATED_ID);
		setUidgErrorIdOutputBytes(buffer);
	}

	/**Original name: UIDG-GENERATED-ID<br>
	 * <pre>*       05  UIDG-GENERATED-ID             PIC  X(20).</pre>*/
	public String getUidgGeneratedId() {
		int position = 1;
		return MarshalByte.readString(getUidgErrorIdOutputBytes(), position, Len.UIDG_GENERATED_ID);
	}

	public String getUidgGeneratedIdFormatted() {
		int position = 1;
		return MarshalByte.readFixedString(getUidgErrorIdOutputBytes(), position, Len.UIDG_GENERATED_ID);
	}

	public void setUidgErrorIdOutputBytes(byte[] buffer) {
		setUidgErrorIdOutputBytes(buffer, 1);
	}

	/**Original name: UIDG-ERROR-ID-OUTPUT<br>*/
	public byte[] getUidgErrorIdOutputBytes() {
		byte[] buffer = new byte[Len.UIDG_ERROR_ID_OUTPUT];
		return getUidgErrorIdOutputBytes(buffer, 1);
	}

	public void setUidgErrorIdOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		generatedErrorRef = MarshalByte.readString(buffer, position, Len.GENERATED_ERROR_REF);
		position += Len.GENERATED_ERROR_REF;
		erridErrorCode.setErridErrorCode(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		erridErrorSqlcd = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.ERRID_ERROR_SQLCD), Len.ERRID_ERROR_SQLCD);
		position += Len.ERRID_ERROR_SQLCD;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getUidgErrorIdOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, generatedErrorRef, Len.GENERATED_ERROR_REF);
		position += Len.GENERATED_ERROR_REF;
		MarshalByte.writeChar(buffer, position, erridErrorCode.getErridErrorCode());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, erridErrorSqlcd, Len.ERRID_ERROR_SQLCD);
		position += Len.ERRID_ERROR_SQLCD;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void initUidgErrorIdOutputSpaces() {
		generatedErrorRef = "";
		erridErrorCode.setErridErrorCode(Types.SPACE_CHAR);
		erridErrorSqlcd = "";
		flr1 = "";
	}

	public void setGeneratedErrorRef(String generatedErrorRef) {
		this.generatedErrorRef = Functions.subString(generatedErrorRef, Len.GENERATED_ERROR_REF);
	}

	public String getGeneratedErrorRef() {
		return this.generatedErrorRef;
	}

	public String getUidgGeneratedErrorRefFormatted() {
		return Functions.padBlanks(getGeneratedErrorRef(), Len.GENERATED_ERROR_REF);
	}

	public void setUidgErridErrorSqlcd(long uidgErridErrorSqlcd) {
		this.erridErrorSqlcd = PicFormatter.display("--(3)9").format(uidgErridErrorSqlcd).toString();
	}

	public long getUidgErridErrorSqlcd() {
		return PicParser.display("--(3)9").parseLong(this.erridErrorSqlcd);
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public UidgErridErrorCode getErridErrorCode() {
		return erridErrorCode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int GENERATED_ERROR_REF = 10;
		public static final int ERRID_ERROR_SQLCD = 5;
		public static final int FLR1 = 16;
		public static final int UIDG_ERROR_ID_OUTPUT = GENERATED_ERROR_REF + UidgErridErrorCode.Len.ERRID_ERROR_CODE + ERRID_ERROR_SQLCD + FLR1;
		public static final int UIDG_GENERATED_ID = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int UIDG_GENERATED_ID = 32;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
