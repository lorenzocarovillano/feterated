/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: HALLUBOC<br>
 * Variable: HALLUBOC from copybook HALLUBOC<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Halluboc {

	//==== PROPERTIES ====
	//Original name: UBOC-COMM-INFO
	private UbocCommInfo commInfo = new UbocCommInfo();
	//Original name: UBOC-APP-DATA-BUFFER-LENGTH
	private String appDataBufferLength = DefaultValues.stringVal(Len.APP_DATA_BUFFER_LENGTH);
	//Original name: UBOC-APP-DATA-BUFFER
	private String appDataBuffer = DefaultValues.stringVal(Len.APP_DATA_BUFFER);

	//==== METHODS ====
	public void setRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		commInfo.setCommInfoBytes(buffer, position);
		position += UbocCommInfo.Len.COMM_INFO;
		setExtraDataBytes(buffer, position);
	}

	public void setExtraDataBytes(byte[] buffer, int offset) {
		int position = offset;
		appDataBufferLength = MarshalByte.readFixedString(buffer, position, Len.APP_DATA_BUFFER_LENGTH);
		position += Len.APP_DATA_BUFFER_LENGTH;
		appDataBuffer = MarshalByte.readString(buffer, position, Len.APP_DATA_BUFFER);
	}

	public void setAppDataBuffer(String appDataBuffer) {
		this.appDataBuffer = Functions.subString(appDataBuffer, Len.APP_DATA_BUFFER);
	}

	public String getAppDataBuffer() {
		return this.appDataBuffer;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int APP_DATA_BUFFER_LENGTH = 4;
		public static final int APP_DATA_BUFFER = 5000;
		public static final int EXTRA_DATA = APP_DATA_BUFFER_LENGTH + APP_DATA_BUFFER;
		public static final int RECORD = UbocCommInfo.Len.COMM_INFO + EXTRA_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
