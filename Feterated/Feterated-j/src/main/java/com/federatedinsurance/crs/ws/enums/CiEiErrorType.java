/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: CI-EI-ERROR-TYPE<br>
 * Variable: CI-EI-ERROR-TYPE from copybook TS54701<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CiEiErrorType {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char REGION_SWAPPED = '1';
	public static final char SYSTEM_WARNING = '2';
	public static final char DPL_RESP_CAPTURED = '3';

	//==== METHODS ====
	public void setErrorType(char errorType) {
		this.value = errorType;
	}

	public char getErrorType() {
		return this.value;
	}

	public boolean isRegionSwapped() {
		return value == REGION_SWAPPED;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_TYPE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
