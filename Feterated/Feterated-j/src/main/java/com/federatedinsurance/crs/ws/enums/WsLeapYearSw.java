/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-LEAP-YEAR-SW<br>
 * Variable: WS-LEAP-YEAR-SW from program XPIODAT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsLeapYearSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char YES = 'Y';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setWsLeapYearSw(char wsLeapYearSw) {
		this.value = wsLeapYearSw;
	}

	public void setWsLeapYearSwFormatted(String wsLeapYearSw) {
		setWsLeapYearSw(Functions.charAt(wsLeapYearSw, Types.CHAR_SIZE));
	}

	public char getWsLeapYearSw() {
		return this.value;
	}

	public boolean isYes() {
		return value == YES;
	}

	public boolean isNo() {
		return value == NO;
	}
}
