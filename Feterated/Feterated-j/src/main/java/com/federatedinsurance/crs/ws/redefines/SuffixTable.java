/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: SUFFIX-TABLE<br>
 * Variable: SUFFIX-TABLE from program CIWBNSRB<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SuffixTable extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int SUFFIX_FLD_MAXOCCURS = 18;

	//==== CONSTRUCTORS ====
	public SuffixTable() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.SUFFIX_TABLE;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "II", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "III", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "IV", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "DDS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "ESQ", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "JR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "PHD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "SR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "I", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "V", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "VI", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "VII", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "VIII", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CAPT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CONG", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "GOV", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "VPRE", Len.FLR1);
	}

	/**Original name: SUFFIX-STD<br>*/
	public String getSuffixStd(int suffixStdIdx) {
		int position = Pos.suffixStd(suffixStdIdx - 1);
		return readString(position, Len.SUFFIX_STD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int SUFFIX_TABLE = 1;
		public static final int FLR1 = SUFFIX_TABLE;
		public static final int FLR2 = FLR1 + Len.FLR1;
		public static final int FLR3 = FLR2 + Len.FLR1;
		public static final int FLR4 = FLR3 + Len.FLR1;
		public static final int FLR5 = FLR4 + Len.FLR1;
		public static final int FLR6 = FLR5 + Len.FLR1;
		public static final int FLR7 = FLR6 + Len.FLR1;
		public static final int FLR8 = FLR7 + Len.FLR1;
		public static final int FLR9 = FLR8 + Len.FLR1;
		public static final int FLR10 = FLR9 + Len.FLR1;
		public static final int FLR11 = FLR10 + Len.FLR1;
		public static final int FLR12 = FLR11 + Len.FLR1;
		public static final int FLR13 = FLR12 + Len.FLR1;
		public static final int FLR14 = FLR13 + Len.FLR1;
		public static final int FLR15 = FLR14 + Len.FLR1;
		public static final int FLR16 = FLR15 + Len.FLR1;
		public static final int FLR17 = FLR16 + Len.FLR1;
		public static final int FLR18 = FLR17 + Len.FLR1;
		public static final int SUF_TABLE_REDEF = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int suffixFld(int idx) {
			return SUF_TABLE_REDEF + idx * Len.SUFFIX_FLD;
		}

		public static int suffixStd(int idx) {
			return suffixFld(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 4;
		public static final int SUFFIX_STD = 4;
		public static final int SUFFIX_FLD = SUFFIX_STD;
		public static final int SUFFIX_TABLE = 18 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
