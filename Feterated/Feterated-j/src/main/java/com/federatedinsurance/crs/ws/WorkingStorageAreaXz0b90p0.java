/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.WsMsgStaCd;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0B90P0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0b90p0 {

	//==== PROPERTIES ====
	//Original name: WS-ROW-COUNT
	private short rowCount = ((short) 0);
	public static final short NO_POLS_FOUND = ((short) 0);
	//Original name: WS-MAX-ROWS
	private String maxRows = DefaultValues.stringVal(Len.MAX_ROWS);
	//Original name: WS-POL-TO-CHECK
	private String polToCheck = DefaultValues.stringVal(Len.POL_TO_CHECK);
	//Original name: WS-EIBRESP-CD
	private short eibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0B90P0";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-LIST-KEY
	private String busObjNmListKey = "XZ_GET_NOT_CER_POL_LIST_KEY";
	//Original name: WS-BUS-OBJ-NM-LIST-DTL
	private String busObjNmListDtl = "XZ_GET_NOT_CER_POL_LIST_DTL";
	//Original name: WS-MSG-STA-CD
	private WsMsgStaCd msgStaCd = new WsMsgStaCd();

	//==== METHODS ====
	public void setRowCount(short rowCount) {
		this.rowCount = rowCount;
	}

	public short getRowCount() {
		return this.rowCount;
	}

	public void setNoPolsFound() {
		rowCount = NO_POLS_FOUND;
	}

	public void setMaxRows(long maxRows) {
		this.maxRows = PicFormatter.display("Z(3)9-").format(maxRows).toString();
	}

	public long getMaxRows() {
		return PicParser.display("Z(3)9-").parseLong(this.maxRows);
	}

	public String getMaxRowsFormatted() {
		return this.maxRows;
	}

	public String getMaxRowsAsString() {
		return getMaxRowsFormatted();
	}

	public void setPolToCheck(String polToCheck) {
		this.polToCheck = Functions.subString(polToCheck, Len.POL_TO_CHECK);
	}

	public String getPolToCheck() {
		return this.polToCheck;
	}

	public String getPolToCheckFormatted() {
		return Functions.padBlanks(getPolToCheck(), Len.POL_TO_CHECK);
	}

	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd = eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public String getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public String getEibrespCdAsString() {
		return getEibrespCdFormatted();
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd = eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public String getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public String getEibresp2CdAsString() {
		return getEibresp2CdFormatted();
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNmListKey() {
		return this.busObjNmListKey;
	}

	public String getBusObjNmListDtl() {
		return this.busObjNmListDtl;
	}

	public WsMsgStaCd getMsgStaCd() {
		return msgStaCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_ROWS = 5;
		public static final int POL_TO_CHECK = 25;
		public static final int EIBRESP_CD = 4;
		public static final int EIBRESP2_CD = 4;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
