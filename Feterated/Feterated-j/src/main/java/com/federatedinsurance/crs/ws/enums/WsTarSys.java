/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TAR-SYS<br>
 * Variable: WS-TAR-SYS from program XZC03090<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTarSys {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.TAR_SYS);
	public static final String DEV1 = "DEV1";
	public static final String DEV2 = "DEV2";
	public static final String DEV4 = "DEV4";
	public static final String BETA1 = "BETA1";
	public static final String BETA2 = "BETA2";
	public static final String BETA4 = "BETA4";
	public static final String EDUC1 = "EDUC1";
	public static final String EDUC2 = "EDUC2";
	public static final String PROD = "PROD";

	//==== METHODS ====
	public void setTarSys(String tarSys) {
		this.value = Functions.subString(tarSys, Len.TAR_SYS);
	}

	public String getTarSys() {
		return this.value;
	}

	public void setDev1() {
		value = DEV1;
	}

	public void setDev2() {
		value = DEV2;
	}

	public void setDev4() {
		value = DEV4;
	}

	public void setBeta1() {
		value = BETA1;
	}

	public void setBeta2() {
		value = BETA2;
	}

	public void setBeta4() {
		value = BETA4;
	}

	public void setEduc1() {
		value = EDUC1;
	}

	public void setEduc2() {
		value = EDUC2;
	}

	public void setProd() {
		value = PROD;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TAR_SYS = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
