/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-INV-POL-NBR<br>
 * Variable: EA-01-INV-POL-NBR from program XZ0D0002<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01InvPolNbr {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-INV-POL-NBR
	private String flr1 = "Policy number";
	//Original name: FILLER-EA-01-INV-POL-NBR-1
	private String flr2 = " of";
	//Original name: EA-01-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: FILLER-EA-01-INV-POL-NBR-2
	private String flr3 = " is not in a";
	//Original name: FILLER-EA-01-INV-POL-NBR-3
	private String flr4 = "valid format of";
	//Original name: FILLER-EA-01-INV-POL-NBR-4
	private String flr5 = " #######";
	//Original name: FILLER-EA-01-INV-POL-NBR-5
	private String flr6 = " for account";
	//Original name: EA-01-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-01-INV-POL-NBR-6
	private String flr7 = ", process time";
	//Original name: FILLER-EA-01-INV-POL-NBR-7
	private String flr8 = "stamp date";
	//Original name: EA-01-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-01-INV-POL-NBR-8
	private char flr9 = '.';

	//==== METHODS ====
	public String getEa01InvPolNbrFormatted() {
		return MarshalByteExt.bufferToStr(getEa01InvPolNbrBytes());
	}

	public byte[] getEa01InvPolNbrBytes() {
		byte[] buffer = new byte[Len.EA01_INV_POL_NBR];
		return getEa01InvPolNbrBytes(buffer, 1);
	}

	public byte[] getEa01InvPolNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		position += Len.FLR8;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr9);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr9() {
		return this.flr9;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 13;
		public static final int FLR2 = 4;
		public static final int FLR4 = 15;
		public static final int FLR5 = 8;
		public static final int FLR7 = 14;
		public static final int FLR8 = 11;
		public static final int FLR9 = 1;
		public static final int EA01_INV_POL_NBR = POL_NBR + CSR_ACT_NBR + NOT_PRC_TS + 3 * FLR1 + FLR2 + FLR4 + FLR5 + FLR7 + FLR8 + FLR9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
