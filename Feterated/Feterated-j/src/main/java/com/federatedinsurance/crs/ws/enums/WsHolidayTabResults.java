/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-HOLIDAY-TAB-RESULTS<br>
 * Variable: WS-HOLIDAY-TAB-RESULTS from program XPIODAT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsHolidayTabResults {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char A_HOLIDAY = 'Y';
	public static final char NOT_A_HOLIDAY = 'N';

	//==== METHODS ====
	public void setWsHolidayTabResults(char wsHolidayTabResults) {
		this.value = wsHolidayTabResults;
	}

	public char getWsHolidayTabResults() {
		return this.value;
	}

	public boolean isaHoliday() {
		return value == A_HOLIDAY;
	}

	public void setaHoliday() {
		value = A_HOLIDAY;
	}

	public boolean isNotAHoliday() {
		return value == NOT_A_HOLIDAY;
	}

	public void setNotAHoliday() {
		value = NOT_A_HOLIDAY;
	}
}
