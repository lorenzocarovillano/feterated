/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLFED-SEG-DES-V<br>
 * Variable: DCLFED-SEG-DES-V from copybook MUH00337<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclfedSegDesV {

	//==== PROPERTIES ====
	//Original name: FED-SEG-CD
	private String fedSegCd = DefaultValues.stringVal(Len.FED_SEG_CD);
	//Original name: CTR-NBR-CD
	private short ctrNbrCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: FED-SEG-DES
	private String fedSegDes = DefaultValues.stringVal(Len.FED_SEG_DES);

	//==== METHODS ====
	public void setFedSegCd(String fedSegCd) {
		this.fedSegCd = Functions.subString(fedSegCd, Len.FED_SEG_CD);
	}

	public String getFedSegCd() {
		return this.fedSegCd;
	}

	public String getFedSegCdFormatted() {
		return Functions.padBlanks(getFedSegCd(), Len.FED_SEG_CD);
	}

	public void setCtrNbrCd(short ctrNbrCd) {
		this.ctrNbrCd = ctrNbrCd;
	}

	public short getCtrNbrCd() {
		return this.ctrNbrCd;
	}

	public void setFedSegDes(String fedSegDes) {
		this.fedSegDes = Functions.subString(fedSegDes, Len.FED_SEG_DES);
	}

	public String getFedSegDes() {
		return this.fedSegDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FED_SEG_CD = 10;
		public static final int FED_SEG_DES = 40;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
