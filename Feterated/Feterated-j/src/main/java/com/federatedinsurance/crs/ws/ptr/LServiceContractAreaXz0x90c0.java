/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90C0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90c0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90c0() {
	}

	public LServiceContractAreaXz0x90c0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiTkNotPrcTs(String iTkNotPrcTs) {
		writeString(Pos.I_TK_NOT_PRC_TS, iTkNotPrcTs, Len.I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9CI-TK-NOT-PRC-TS<br>*/
	public String getiTkNotPrcTs() {
		return readString(Pos.I_TK_NOT_PRC_TS, Len.I_TK_NOT_PRC_TS);
	}

	public void setiCsrActNbr(String iCsrActNbr) {
		writeString(Pos.I_CSR_ACT_NBR, iCsrActNbr, Len.I_CSR_ACT_NBR);
	}

	/**Original name: XZT9CI-CSR-ACT-NBR<br>*/
	public String getiCsrActNbr() {
		return readString(Pos.I_CSR_ACT_NBR, Len.I_CSR_ACT_NBR);
	}

	public void setiUserid(String iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT9CI-USERID<br>*/
	public String getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	public String getiUseridFormatted() {
		return Functions.padBlanks(getiUserid(), Len.I_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT9C0_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_TECHNICAL_KEY = XZT9C0_SERVICE_INPUTS;
		public static final int I_TK_NOT_PRC_TS = I_TECHNICAL_KEY;
		public static final int I_CSR_ACT_NBR = I_TK_NOT_PRC_TS + Len.I_TK_NOT_PRC_TS;
		public static final int I_USERID = I_CSR_ACT_NBR + Len.I_CSR_ACT_NBR;
		public static final int XZT9C0_SERVICE_OUTPUTS = I_USERID + Len.I_USERID;
		public static final int FLR1 = XZT9C0_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_TK_NOT_PRC_TS = 26;
		public static final int I_CSR_ACT_NBR = 9;
		public static final int I_USERID = 8;
		public static final int I_TECHNICAL_KEY = I_TK_NOT_PRC_TS;
		public static final int XZT9C0_SERVICE_INPUTS = I_TECHNICAL_KEY + I_CSR_ACT_NBR + I_USERID;
		public static final int FLR1 = 1;
		public static final int XZT9C0_SERVICE_OUTPUTS = FLR1;
		public static final int L_SERVICE_CONTRACT_AREA = XZT9C0_SERVICE_INPUTS + XZT9C0_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
