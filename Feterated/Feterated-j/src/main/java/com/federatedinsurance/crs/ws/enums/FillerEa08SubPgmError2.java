/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FILLER-EA-08-SUB-PGM-ERROR-2<br>
 * Variable: FILLER-EA-08-SUB-PGM-ERROR-2 from program FNC02090<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FillerEa08SubPgmError2 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.FLR3);
	public static final String WARNING = "WARNING";
	public static final String FATAL = "  FATAL";

	//==== METHODS ====
	public String getFlr3() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR3 = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
