/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.federatedinsurance.crs.copy.Halluboc;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALOEPAL<br>
 * Generated as a class for rule WS.<br>*/
public class HaloepalData {

	//==== PROPERTIES ====
	//Original name: W-RESPONSE
	private int wResponse = DefaultValues.BIN_INT_VAL;
	//Original name: W-RESPONSE2
	private int wResponse2 = DefaultValues.BIN_INT_VAL;
	//Original name: HALLUBOC
	private Halluboc halluboc = new Halluboc();

	//==== METHODS ====
	public void setwResponse(int wResponse) {
		this.wResponse = wResponse;
	}

	public int getwResponse() {
		return this.wResponse;
	}

	public void setwResponse2(int wResponse2) {
		this.wResponse2 = wResponse2;
	}

	public int getwResponse2() {
		return this.wResponse2;
	}

	public void setWsEpalLinkFormatted(String data) {
		byte[] buffer = new byte[Len.WS_EPAL_LINK];
		MarshalByte.writeString(buffer, 1, data, Len.WS_EPAL_LINK);
		setWsEpalLinkBytes(buffer, 1);
	}

	public void setWsEpalLinkBytes(byte[] buffer, int offset) {
		int position = offset;
		halluboc.setRecordBytes(buffer, position);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_EIBRESP_DISPLAY = 10;
		public static final int WS_EIBRESP2_DISPLAY = 11;
		public static final int WS_ERROR_MESSAGE = 128;
		public static final int WS_EPAL_LINK = Halluboc.Len.RECORD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
