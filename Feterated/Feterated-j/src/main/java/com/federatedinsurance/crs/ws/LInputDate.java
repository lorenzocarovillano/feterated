/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: L-INPUT-DATE<br>
 * Variable: L-INPUT-DATE from program HALRVDT1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LInputDate extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: L-INPUT-YYYY
	private String lInputYyyy = DefaultValues.stringVal(Len.L_INPUT_YYYY);
	//Original name: L-INPUT-DELIM1
	private char lInputDelim1 = DefaultValues.CHAR_VAL;
	public static final char VALID_L_INPUT_DELIM1 = '-';
	//Original name: L-INPUT-MM
	private short lInputMm = DefaultValues.SHORT_VAL;
	//Original name: L-INPUT-DELIM2
	private char lInputDelim2 = DefaultValues.CHAR_VAL;
	public static final char VALID_L_INPUT_DELIM2 = '-';
	//Original name: L-INPUT-DD
	private String lInputDd = DefaultValues.stringVal(Len.L_INPUT_DD);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_INPUT_DATE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setlInputDateBytes(buf);
	}

	public void setlInputDateBytes(byte[] buffer) {
		setlInputDateBytes(buffer, 1);
	}

	public byte[] getlInputDateBytes() {
		byte[] buffer = new byte[Len.L_INPUT_DATE];
		return getlInputDateBytes(buffer, 1);
	}

	public void setlInputDateBytes(byte[] buffer, int offset) {
		int position = offset;
		lInputYyyy = MarshalByte.readFixedString(buffer, position, Len.L_INPUT_YYYY);
		position += Len.L_INPUT_YYYY;
		lInputDelim1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		lInputMm = MarshalByte.readShort(buffer, position, Len.L_INPUT_MM, SignType.NO_SIGN);
		position += Len.L_INPUT_MM;
		lInputDelim2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		lInputDd = MarshalByte.readFixedString(buffer, position, Len.L_INPUT_DD);
	}

	public byte[] getlInputDateBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, lInputYyyy, Len.L_INPUT_YYYY);
		position += Len.L_INPUT_YYYY;
		MarshalByte.writeChar(buffer, position, lInputDelim1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeShort(buffer, position, lInputMm, Len.L_INPUT_MM, SignType.NO_SIGN);
		position += Len.L_INPUT_MM;
		MarshalByte.writeChar(buffer, position, lInputDelim2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, lInputDd, Len.L_INPUT_DD);
		return buffer;
	}

	public short getlInputYyyy() {
		return NumericDisplay.asShort(this.lInputYyyy);
	}

	public String getlInputYyyyFormatted() {
		return this.lInputYyyy;
	}

	public void setlInputDelim1(char lInputDelim1) {
		this.lInputDelim1 = lInputDelim1;
	}

	public char getlInputDelim1() {
		return this.lInputDelim1;
	}

	public boolean isValidLInputDelim1() {
		return lInputDelim1 == VALID_L_INPUT_DELIM1;
	}

	public void setlInputMm(short lInputMm) {
		this.lInputMm = lInputMm;
	}

	public short getlInputMm() {
		return this.lInputMm;
	}

	public void setlInputDelim2(char lInputDelim2) {
		this.lInputDelim2 = lInputDelim2;
	}

	public char getlInputDelim2() {
		return this.lInputDelim2;
	}

	public boolean isValidLInputDelim2() {
		return lInputDelim2 == VALID_L_INPUT_DELIM2;
	}

	public short getlInputDd() {
		return NumericDisplay.asShort(this.lInputDd);
	}

	public String getlInputDdFormatted() {
		return this.lInputDd;
	}

	@Override
	public byte[] serialize() {
		return getlInputDateBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int L_INPUT_YYYY = 4;
		public static final int L_INPUT_DELIM1 = 1;
		public static final int L_INPUT_MM = 2;
		public static final int L_INPUT_DELIM2 = 1;
		public static final int L_INPUT_DD = 2;
		public static final int L_INPUT_DATE = L_INPUT_YYYY + L_INPUT_DELIM1 + L_INPUT_MM + L_INPUT_DELIM2 + L_INPUT_DD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
