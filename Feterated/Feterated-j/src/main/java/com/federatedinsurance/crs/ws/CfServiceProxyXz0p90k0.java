/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-SERVICE-PROXY<br>
 * Variable: CF-SERVICE-PROXY from program XZ0P90K0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfServiceProxyXz0p90k0 {

	//==== PROPERTIES ====
	//Original name: CF-SP-ADD-ACT-NOT-SVC
	private String addActNotSvc = "XZ0X0006";
	//Original name: CF-SP-GET-INSURED-DETAIL-SVC
	private String getInsuredDetailSvc = "MU0X0004";
	//Original name: CF-SP-PREPARE-INS-POL-SVC
	private String prepareInsPolSvc = "XZ0X90H0";
	//Original name: CF-SP-PREPARE-NOT-SVC
	private String prepareNotSvc = "XZ0X90J0";
	//Original name: CF-SP-PREPARE-TTY-SVC
	private String prepareTtySvc = "XZ0X90A0";
	//Original name: CF-SP-UPD-NOT-STA-SVC
	private String updNotStaSvc = "XZ0X90M0";

	//==== METHODS ====
	public String getAddActNotSvc() {
		return this.addActNotSvc;
	}

	public String getGetInsuredDetailSvc() {
		return this.getInsuredDetailSvc;
	}

	public String getPrepareInsPolSvc() {
		return this.prepareInsPolSvc;
	}

	public String getPrepareNotSvc() {
		return this.prepareNotSvc;
	}

	public String getPrepareTtySvc() {
		return this.prepareTtySvc;
	}

	public String getUpdNotStaSvc() {
		return this.updNotStaSvc;
	}
}
