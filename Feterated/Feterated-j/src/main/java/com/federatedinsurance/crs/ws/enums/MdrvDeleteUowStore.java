/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: MDRV-DELETE-UOW-STORE<br>
 * Variable: MDRV-DELETE-UOW-STORE from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvDeleteUowStore {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char DELETE_STORE = 'Y';
	public static final char NO_DELETE_STORE = 'N';

	//==== METHODS ====
	public void setDeleteUowStore(char deleteUowStore) {
		this.value = deleteUowStore;
	}

	public char getDeleteUowStore() {
		return this.value;
	}

	public void setDeleteStore() {
		value = DELETE_STORE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DELETE_UOW_STORE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
