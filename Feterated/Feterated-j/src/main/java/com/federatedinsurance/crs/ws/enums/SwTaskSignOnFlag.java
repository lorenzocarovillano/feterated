/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-TASK-SIGN-ON-FLAG<br>
 * Variable: SW-TASK-SIGN-ON-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwTaskSignOnFlag {

	//==== PROPERTIES ====
	private char value = '0';
	public static final char ROOT = '0';
	public static final char SUB = '1';

	//==== METHODS ====
	public void setTaskSignOnFlag(char taskSignOnFlag) {
		this.value = taskSignOnFlag;
	}

	public char getTaskSignOnFlag() {
		return this.value;
	}

	public boolean isSub() {
		return value == SUB;
	}

	public void setSub() {
		value = SUB;
	}
}
