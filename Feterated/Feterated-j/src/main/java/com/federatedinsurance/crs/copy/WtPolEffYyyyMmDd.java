/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WT-POL-EFF-YYYY-MM-DD<br>
 * Variable: WT-POL-EFF-YYYY-MM-DD from copybook XZC00401<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WtPolEffYyyyMmDd {

	//==== PROPERTIES ====
	//Original name: WT-POL-EFF-CC
	private String cc = DefaultValues.stringVal(Len.CC);
	//Original name: WT-POL-EFF-YY
	private String yy = DefaultValues.stringVal(Len.YY);
	//Original name: FILLER-WT-POL-EFF-YYYY-MM-DD
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: WT-POL-EFF-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: FILLER-WT-POL-EFF-YYYY-MM-DD-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: WT-POL-EFF-DD
	private String dd = DefaultValues.stringVal(Len.DD);

	//==== METHODS ====
	public void setPolEffYyyyMmDdFormatted(String data) {
		byte[] buffer = new byte[Len.POL_EFF_YYYY_MM_DD];
		MarshalByte.writeString(buffer, 1, data, Len.POL_EFF_YYYY_MM_DD);
		setPolEffYyyyMmDdBytes(buffer, 1);
	}

	public void setPolEffYyyyMmDdBytes(byte[] buffer, int offset) {
		int position = offset;
		setYyyyBytes(buffer, position);
		position += Len.YYYY;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mm = MarshalByte.readString(buffer, position, Len.MM);
		position += Len.MM;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dd = MarshalByte.readString(buffer, position, Len.DD);
	}

	public byte[] getPolEffYyyyMmDdBytes(byte[] buffer, int offset) {
		int position = offset;
		getYyyyBytes(buffer, position);
		position += Len.YYYY;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position += Len.MM;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dd, Len.DD);
		return buffer;
	}

	public void initPolEffYyyyMmDdSpaces() {
		initYyyySpaces();
		flr1 = Types.SPACE_CHAR;
		mm = "";
		flr2 = Types.SPACE_CHAR;
		dd = "";
	}

	public void setYyyyBytes(byte[] buffer, int offset) {
		int position = offset;
		cc = MarshalByte.readString(buffer, position, Len.CC);
		position += Len.CC;
		yy = MarshalByte.readString(buffer, position, Len.YY);
	}

	public byte[] getYyyyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cc, Len.CC);
		position += Len.CC;
		MarshalByte.writeString(buffer, position, yy, Len.YY);
		return buffer;
	}

	public void initYyyySpaces() {
		cc = "";
		yy = "";
	}

	public void setCc(String cc) {
		this.cc = Functions.subString(cc, Len.CC);
	}

	public String getCc() {
		return this.cc;
	}

	public void setYy(String yy) {
		this.yy = Functions.subString(yy, Len.YY);
	}

	public String getYy() {
		return this.yy;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setMm(String mm) {
		this.mm = Functions.subString(mm, Len.MM);
	}

	public String getMm() {
		return this.mm;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setDd(String dd) {
		this.dd = Functions.subString(dd, Len.DD);
	}

	public String getDd() {
		return this.dd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CC = 2;
		public static final int YY = 2;
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int YYYY = CC + YY;
		public static final int FLR1 = 1;
		public static final int POL_EFF_YYYY_MM_DD = YYYY + MM + DD + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
