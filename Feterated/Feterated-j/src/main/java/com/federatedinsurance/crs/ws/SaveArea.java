/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.federatedinsurance.crs.ws.enums.SaCicsApplId;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program TS548099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class SaveArea extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: SA-PIPE-TOKEN
	private int pipeToken = 0;
	//Original name: SA-USER-TOKEN
	private int userToken = 0;
	//Original name: SA-CICS-APPL-ID
	private SaCicsApplId cicsApplId = new SaCicsApplId();
	//Original name: SA-LIST-OF-REGIONS-ATTEMPTED
	private String listOfRegionsAttempted = DefaultValues.stringVal(Len.LIST_OF_REGIONS_ATTEMPTED);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.SAVE_AREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setSaveAreaBytes(buf);
	}

	public String getSaveAreaFormatted() {
		return MarshalByteExt.bufferToStr(getSaveAreaBytes());
	}

	public void setSaveAreaBytes(byte[] buffer) {
		setSaveAreaBytes(buffer, 1);
	}

	public byte[] getSaveAreaBytes() {
		byte[] buffer = new byte[Len.SAVE_AREA];
		return getSaveAreaBytes(buffer, 1);
	}

	public void setSaveAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		pipeToken = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		userToken = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		cicsApplId.setCicsApplId(MarshalByte.readString(buffer, position, SaCicsApplId.Len.CICS_APPL_ID));
		position += SaCicsApplId.Len.CICS_APPL_ID;
		listOfRegionsAttempted = MarshalByte.readString(buffer, position, Len.LIST_OF_REGIONS_ATTEMPTED);
	}

	public byte[] getSaveAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryInt(buffer, position, pipeToken);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, userToken);
		position += Types.INT_SIZE;
		MarshalByte.writeString(buffer, position, cicsApplId.getCicsApplId(), SaCicsApplId.Len.CICS_APPL_ID);
		position += SaCicsApplId.Len.CICS_APPL_ID;
		MarshalByte.writeString(buffer, position, listOfRegionsAttempted, Len.LIST_OF_REGIONS_ATTEMPTED);
		return buffer;
	}

	public void setPipeToken(int pipeToken) {
		this.pipeToken = pipeToken;
	}

	public int getPipeToken() {
		return this.pipeToken;
	}

	public void setUserToken(int userToken) {
		this.userToken = userToken;
	}

	public void setUserTokenFromBuffer(byte[] buffer) {
		userToken = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getUserToken() {
		return this.userToken;
	}

	public String getUserTokenFormatted() {
		return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.BINARY)).format(getUserToken()).toString();
	}

	public void setListOfRegionsAttempted(String listOfRegionsAttempted) {
		this.listOfRegionsAttempted = Functions.subString(listOfRegionsAttempted, Len.LIST_OF_REGIONS_ATTEMPTED);
	}

	public String getListOfRegionsAttempted() {
		return this.listOfRegionsAttempted;
	}

	public SaCicsApplId getCicsApplId() {
		return cicsApplId;
	}

	@Override
	public byte[] serialize() {
		return getSaveAreaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PIPE_TOKEN = 4;
		public static final int USER_TOKEN = 4;
		public static final int LIST_OF_REGIONS_ATTEMPTED = 30;
		public static final int SAVE_AREA = PIPE_TOKEN + USER_TOKEN + SaCicsApplId.Len.CICS_APPL_ID + LIST_OF_REGIONS_ATTEMPTED;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
