/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotTyp1;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [ACT_NOT, ACT_NOT_TYP]
 * 
 */
public class ActNotTyp1Dao extends BaseSqlDao<IActNotTyp1> {

	private Cursor xmlReqCsr;
	private final IRowMapper<IActNotTyp1> fetchXmlReqCsrRm = buildNamedRowMapper(IActNotTyp1.class, "csrActNbr", "notPrcTs", "actNotTypCd", "notDt",
			"empIdObj", "actTypCdObj", "docDes");

	public ActNotTyp1Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotTyp1> getToClass() {
		return IActNotTyp1.class;
	}

	public DbAccessStatus openXmlReqCsr(String tNotStaCd, char yInd) {
		xmlReqCsr = buildQuery("openXmlReqCsr").bind("tNotStaCd", tNotStaCd).bind("yInd", String.valueOf(yInd)).open();
		return dbStatus;
	}

	public IActNotTyp1 fetchXmlReqCsr(IActNotTyp1 iActNotTyp1) {
		return fetch(xmlReqCsr, iActNotTyp1, fetchXmlReqCsrRm);
	}

	public DbAccessStatus closeXmlReqCsr() {
		return closeCursor(xmlReqCsr);
	}
}
