/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZH00016<br>
 * Copybook: XZH00016 from copybook XZH00016<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Xzh00016 {

	//==== PROPERTIES ====
	//Original name: CLT-RLT-TYP-CD
	private String cltRltTypCd = DefaultValues.stringVal(Len.CLT_RLT_TYP_CD);
	//Original name: REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);

	//==== METHODS ====
	public void setCltRltTypCd(String cltRltTypCd) {
		this.cltRltTypCd = Functions.subString(cltRltTypCd, Len.CLT_RLT_TYP_CD);
	}

	public String getCltRltTypCd() {
		return this.cltRltTypCd;
	}

	public String getCltRltTypCdFormatted() {
		return Functions.padBlanks(getCltRltTypCd(), Len.CLT_RLT_TYP_CD);
	}

	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	public String getRecTypCd() {
		return this.recTypCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLT_RLT_TYP_CD = 4;
		public static final int REC_TYP_CD = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
