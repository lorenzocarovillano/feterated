/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZT9R0-SERVICE-OUTPUTS<br>
 * Variable: XZT9R0-SERVICE-OUTPUTS from copybook XZ0Z90R0<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzt9r0ServiceOutputs {

	//==== PROPERTIES ====
	//Original name: XZT9RO-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZT9RO-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZT9RO-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZT9RO-CNC-DATA
	private Xzt9roCncData cncData = new Xzt9roCncData();
	//Original name: XZT9RO-UW-TMN-FLG
	private char uwTmnFlg = DefaultValues.CHAR_VAL;
	//Original name: XZT9RO-TMN-EMP-ID
	private String tmnEmpId = DefaultValues.stringVal(Len.TMN_EMP_ID);
	//Original name: XZT9RO-TMN-EMP-NM
	private String tmnEmpNm = DefaultValues.stringVal(Len.TMN_EMP_NM);
	//Original name: XZT9RO-TMN-PRC-DT
	private String tmnPrcDt = DefaultValues.stringVal(Len.TMN_PRC_DT);
	//Original name: FILLER-DFHCOMMAREA-XZT9R0-SERVICE-OUTPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setXzt9r0ServiceOutputsBytes(byte[] buffer) {
		setXzt9r0ServiceOutputsBytes(buffer, 1);
	}

	public void setXzt9r0ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		setPolKeysBytes(buffer, position);
		position += Len.POL_KEYS;
		cncData.setCncDataBytes(buffer, position);
		position += Xzt9roCncData.Len.CNC_DATA;
		setTmnDataBytes(buffer, position);
		position += Len.TMN_DATA;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzt9r0ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		getPolKeysBytes(buffer, position);
		position += Len.POL_KEYS;
		cncData.getCncDataBytes(buffer, position);
		position += Xzt9roCncData.Len.CNC_DATA;
		getTmnDataBytes(buffer, position);
		position += Len.TMN_DATA;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setPolKeysBytes(byte[] buffer, int offset) {
		int position = offset;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		polExpDt = MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
	}

	public byte[] getPolKeysBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		return buffer;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setTmnDataBytes(byte[] buffer, int offset) {
		int position = offset;
		uwTmnFlg = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tmnEmpId = MarshalByte.readString(buffer, position, Len.TMN_EMP_ID);
		position += Len.TMN_EMP_ID;
		tmnEmpNm = MarshalByte.readString(buffer, position, Len.TMN_EMP_NM);
		position += Len.TMN_EMP_NM;
		tmnPrcDt = MarshalByte.readString(buffer, position, Len.TMN_PRC_DT);
	}

	public byte[] getTmnDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, uwTmnFlg);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tmnEmpId, Len.TMN_EMP_ID);
		position += Len.TMN_EMP_ID;
		MarshalByte.writeString(buffer, position, tmnEmpNm, Len.TMN_EMP_NM);
		position += Len.TMN_EMP_NM;
		MarshalByte.writeString(buffer, position, tmnPrcDt, Len.TMN_PRC_DT);
		return buffer;
	}

	public void setUwTmnFlg(char uwTmnFlg) {
		this.uwTmnFlg = uwTmnFlg;
	}

	public char getUwTmnFlg() {
		return this.uwTmnFlg;
	}

	public void setTmnEmpId(String tmnEmpId) {
		this.tmnEmpId = Functions.subString(tmnEmpId, Len.TMN_EMP_ID);
	}

	public String getTmnEmpId() {
		return this.tmnEmpId;
	}

	public void setTmnEmpNm(String tmnEmpNm) {
		this.tmnEmpNm = Functions.subString(tmnEmpNm, Len.TMN_EMP_NM);
	}

	public String getTmnEmpNm() {
		return this.tmnEmpNm;
	}

	public void setTmnPrcDt(String tmnPrcDt) {
		this.tmnPrcDt = Functions.subString(tmnPrcDt, Len.TMN_PRC_DT);
	}

	public String getTmnPrcDt() {
		return this.tmnPrcDt;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public Xzt9roCncData getCncData() {
		return cncData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 9;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int POL_KEYS = POL_NBR + POL_EFF_DT + POL_EXP_DT;
		public static final int UW_TMN_FLG = 1;
		public static final int TMN_EMP_ID = 6;
		public static final int TMN_EMP_NM = 67;
		public static final int TMN_PRC_DT = 10;
		public static final int TMN_DATA = UW_TMN_FLG + TMN_EMP_ID + TMN_EMP_NM + TMN_PRC_DT;
		public static final int FLR1 = 800;
		public static final int XZT9R0_SERVICE_OUTPUTS = POL_KEYS + Xzt9roCncData.Len.CNC_DATA + TMN_DATA + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
