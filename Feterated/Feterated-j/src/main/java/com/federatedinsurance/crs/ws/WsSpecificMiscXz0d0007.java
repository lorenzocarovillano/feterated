/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsChildrenXz0d0004;

/**Original name: WS-SPECIFIC-MISC<br>
 * Variable: WS-SPECIFIC-MISC from program XZ0D0007<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSpecificMiscXz0d0007 {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0D0007";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-APPLICATION-LOK-TAB
	private String applicationLokTab = "XXX";
	//Original name: WS-APPLICATION-LOK-NM
	private String applicationLokNm = "CRS";
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "ACT_NOT_FRM_REC";
	//Original name: WS-PARAGRAPH-NM
	private String paragraphNm = "";
	/**Original name: WS-CHILDREN<br>
	 * <pre>* LIST ALL DIRECT AND FOREIGN CHILDREN OF THIS BDO HERE.</pre>*/
	private WsChildrenXz0d0004 children = new WsChildrenXz0d0004();

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getApplicationLokTab() {
		return this.applicationLokTab;
	}

	public String getApplicationLokNm() {
		return this.applicationLokNm;
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setParagraphNm(String paragraphNm) {
		this.paragraphNm = Functions.subString(paragraphNm, Len.PARAGRAPH_NM);
	}

	public String getParagraphNm() {
		return this.paragraphNm;
	}

	public WsChildrenXz0d0004 getChildren() {
		return children;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUS_OBJ_NM = 32;
		public static final int PROGRAM_NAME = 8;
		public static final int PARAGRAPH_NM = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
