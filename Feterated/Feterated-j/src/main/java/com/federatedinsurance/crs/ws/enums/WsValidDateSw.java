/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-VALID-DATE-SW<br>
 * Variable: WS-VALID-DATE-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsValidDateSw extends SerializableParameter {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char VALID_DATE = 'Y';
	public static final char NOT_VALID_DATE = 'N';

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.VALID_DATE_SW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setValidDateSwFromBuffer(buf);
	}

	public void setValidDateSw(char validDateSw) {
		this.value = validDateSw;
	}

	public void setValidDateSwFromBuffer(byte[] buffer) {
		value = MarshalByte.readChar(buffer, 1);
	}

	public char getValidDateSw() {
		return this.value;
	}

	public boolean isValidDate() {
		return value == VALID_DATE;
	}

	public boolean isWsNotValidDate() {
		return value == NOT_VALID_DATE;
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.chToBuffer(getValidDateSw());
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VALID_DATE_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int VALID_DATE_SW = 1;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int VALID_DATE_SW = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
