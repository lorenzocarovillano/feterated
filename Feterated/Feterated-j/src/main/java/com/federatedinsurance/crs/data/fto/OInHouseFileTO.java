/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: O-IN-HOUSE-FILE<br>
 * File: O-IN-HOUSE-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OInHouseFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: IHO-CARRIAGE-CONTROL
	private String ihoCarriageControl = DefaultValues.stringVal(Len.IHO_CARRIAGE_CONTROL);
	//Original name: IHO-DATA
	private String ihoData = DefaultValues.stringVal(Len.IHO_DATA);

	//==== METHODS ====
	public void setoInHouseRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		ihoCarriageControl = MarshalByte.readFixedString(buffer, position, Len.IHO_CARRIAGE_CONTROL);
		position += Len.IHO_CARRIAGE_CONTROL;
		ihoData = MarshalByte.readString(buffer, position, Len.IHO_DATA);
	}

	public byte[] getoInHouseRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ihoCarriageControl, Len.IHO_CARRIAGE_CONTROL);
		position += Len.IHO_CARRIAGE_CONTROL;
		MarshalByte.writeString(buffer, position, ihoData, Len.IHO_DATA);
		return buffer;
	}

	public void setIhoCarriageControlFormatted(String ihoCarriageControl) {
		this.ihoCarriageControl = Trunc.toUnsignedNumeric(ihoCarriageControl, Len.IHO_CARRIAGE_CONTROL);
	}

	public short getIhoCarriageControl() {
		return NumericDisplay.asShort(this.ihoCarriageControl);
	}

	public void setIhoData(String ihoData) {
		this.ihoData = Functions.subString(ihoData, Len.IHO_DATA);
	}

	public String getIhoData() {
		return this.ihoData;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoInHouseRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoInHouseRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_IN_HOUSE_RECORD;
	}

	@Override
	public IBuffer copy() {
		OInHouseFileTO copyTO = new OInHouseFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int IHO_CARRIAGE_CONTROL = 1;
		public static final int IHO_DATA = 132;
		public static final int O_IN_HOUSE_RECORD = IHO_CARRIAGE_CONTROL + IHO_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
