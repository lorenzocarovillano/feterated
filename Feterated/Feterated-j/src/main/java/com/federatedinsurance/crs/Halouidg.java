/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import static java.lang.Math.abs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalouidgGenericDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.HalouidgData;
import com.federatedinsurance.crs.ws.WGeneralWorkfields;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.SqlFunctions;
import com.modernsystems.programs.Programs;

/**Original name: HALOUIDG<br>
 * <pre>AUTHOR.       MYND.
 * DATE-WRITTEN. APR 2000.
 * ****************************************************************
 *                                                                *
 *   PROGRAM TITLE      - GENERATOR OF ANY OF A VARIETY OF RANDOM *
 *                        IDENTIFIERS, EG. POLICY ID, CLIENT ID   *
 *                        ETC.                                    *
 *                                                                *
 *   PLATFORM           - HOST                                    *
 *                                                                *
 *   OPERATING SYSTEM   - MVS                                     *
 *                                                                *
 *   LANGUAGE           - COBOL II                                *
 *                                                                *
 *   PURPOSE            - TO GENERATE A POLICY ID OR OTHER        *
 *                        IDENTIFIER IN THE SAME WAY AS S3.       *
 *                                                                *
 *   PROGRAM INITIATION - THIS PROGRAM IS STARTED IN THE FOLLOWING*
 *                        WAYS:                                   *
 *                                                                *
 *                        STARTED BY A LINK FROM ANY MODULE       *
 *                        REQUIRING ID GENERATION.                *
 *                                                                *
 *   DATA ACCESS METHODS - PASSED INFORMATION IN THE COMMAREA OR  *
 *                         THROUGH LINKAGE.                       *
 *                                                                *
 *                                                                *
 * ****************************************************************
 * ****************************************************************
 *                 M A I N T E N A N C E    L O G
 *   SI#       DATE      PRGMR   DESCRIPTION
 *   --------- --------  ------  ----------------------------------
 *   SAVANNAH  01MAR00   JAF     CREATED.
 *   15863     24AUG01   18448   UTILIZE AN INFRASTRUCTURE TABLE TO
 *                               GET SECONDS.
 *   16178     31AUG01   18448   ADD CODE FOR PROCESSING THE DWS
 *                               UIDG-DSBBATCH-ID REQUEST.
 *   17241     13NOV01   03539   REPLACE REFERENCES TO IAP WITH
 *                               COMPARABLE INFRASTRUCTURE CODE.
 *   17330     11JAN02   18448   CHANGE ID ALGORITHM TO GENERATE IDS
 *    (GA)                       THAT WILL SPREAD MORE WIDELY ACROSS
 *                               DB2 PAGES. (RETRO'D BACK FROM
 *                               BF 2.2 GA).
 *   18687     16JAN02   18448   REMOVE APP-SPECIFIC CODE AND MOVE
 *                               TO NEW APP-OWNED ID BUILDER UTILS.
 * ****************************************************************</pre>*/
public class Halouidg extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*   05  WS-S3DT-BREAKDOWN.
	 * *       10  WS-S3DT-YYYY-MM-DD.
	 * *           15  WS-S3DT-YYYY         PIC 9(04).
	 * *           15  FILLER               PIC X(01).
	 * *           15  WS-S3DT-MM           PIC 9(02).
	 * *           15  FILLER               PIC X(01).
	 * *           15  WS-S3DT-DD           PIC 9(02).
	 * *       10  FILLER                   PIC X(01).
	 * *       10  WS-S3TIME-HH-MM-SS.
	 * *           15  WS-S3TIME-HH         PIC 9(02).
	 * *           15  FILLER               PIC X(01).
	 * *           15  WS-S3TIME-MM         PIC 9(02).
	 * *           15  FILLER               PIC X(01).
	 * *           15  WS-S3TIME-SS         PIC 9(02).
	 * *           15  FILLER               PIC X(01).
	 * *           15  WS-S3TIME-MSA        PIC 9(04).
	 * *           15  WS-S3TIME-MSB        PIC 9(02).
	 * 01  MSG-XPXCDAT-ERROR.
	 * *   05  FILLER                    PIC X(20)
	 * *          VALUE 'DATE ROUTINE FAILED.'.
	 * *   COPY XPXLDAT.
	 * ****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK*
	 * **CSC *                                     *BUSINESS FRAMEWORK*
	 * **CSC *  GENERAL DB2 DEFINITIONS            *BUSINESS FRAMEWORK*
	 * ****************************************************************</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ExecContext execContext = null;
	private HalouidgGenericDao halouidgGenericDao = new HalouidgGenericDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private HalouidgData ws = new HalouidgData();
	//Original name: DFHCOMMAREA
	private Dfhcommarea dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, Dfhcommarea dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainLine();
		returnToCallingProgram();
		return 0;
	}

	public static Halouidg getInstance() {
		return (Programs.getInstance(Halouidg.class));
	}

	/**Original name: 0010-MAIN-LINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *     CONTROL OF PROCESSING                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainLine() {
		// COB_CODE: PERFORM 0100-INITIALIZATION.
		initialization();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 0010-RETURN-TO-CALLING-PROGRAM
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 0010-RETURN-TO-CALLING-PROGRAM
			returnToCallingProgram();
		}
		//** 16178      PERFORM 0125-PROCESS-ID-TYPE.
		//** 16178      IF ID-PROCESSING-COMPLETE
		//** 16178          GO TO 0010-RETURN-TO-CALLING-PROGRAM
		//** 16178      END-IF.
		// COB_CODE: PERFORM 0150-GENERATE-ID.
		generateId();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 0010-RETURN-TO-CALLING-PROGRAM
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 0010-RETURN-TO-CALLING-PROGRAM
			returnToCallingProgram();
		}
	}

	/**Original name: 0010-RETURN-TO-CALLING-PROGRAM<br>
	 * <pre>**  PERFORM 1000-CHECK-ID.</pre>*/
	private void returnToCallingProgram() {
		// COB_CODE: MOVE LENGTH OF W-HALOUIDG-COMMAREA
		//                         TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.setAppDataBufferLength(((short) HalouidgData.Len.W_HALOUIDG_COMMAREA));
		// COB_CODE: MOVE W-HALOUIDG-COMMAREA TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.setAppDataBuffer(ws.getwHalouidgCommareaFormatted());
		// COB_CODE: EXEC CICS
		//              RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 0100-INITIALIZATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *     INITIALIZATION OF WORKFIELDS, AND ANY OTHER INITIAL         *
	 *     PROCESSING.                                                 *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void initialization() {
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER
		//                        TO W-HALOUIDG-COMMAREA.
		ws.setwHalouidgCommareaFormatted(dfhcommarea.getAppDataBufferFormatted());
		// COB_CODE: INITIALIZE WS-ESTO-INFO.
		initWsEstoInfo();
		// COB_CODE: MOVE SPACES TO UIDG-CA-OUTPUT.
		ws.getHalluidg().initUidgCaOutputSpaces();
		// COB_CODE: IF UBOC-MSG-ID EQUAL SPACES OR LOW-VALUES
		//               GO TO 0100-INITIALIZATION-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: MOVE '**UIDG*FAILURE*MSGID*IS*SPACES**' TO UBOC-MSG-ID
			dfhcommarea.getCommInfo().setUbocMsgId("**UIDG*FAILURE*MSGID*IS*SPACES**");
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET COMA-MSG-ID-BLANK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE '0100-INITIALIZATION'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-INITIALIZATION");
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-INITIALIZATION-X
			return;
		}
	}

	/**Original name: 0150-GENERATE-ID_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *     GENERATE THE UNIQUE OBJECT IDENTIFIER                       *
	 *                                                                 *
	 * *****************************************************************
	 * ************** BEGINNING OF CODE REMOVAL ****************
	 * **  PERFORM 0500-GET-CURRENT-S3-TS.
	 * **
	 * **  IF UBOC-UOW-LOGGABLE-ERRORS
	 * **      GO TO 0150-GENERATE-ID-X
	 * **  END-IF.
	 * **
	 * **  MOVE DATE-OUTPUT(1:10)      TO W-DATE.
	 * **  MOVE DATE-OUTPUT(12:2)      TO W-HOURS.
	 * **  MOVE DATE-OUTPUT(15:2)      TO W-MINUTES.
	 * **  MOVE DATE-OUTPUT(18:2)      TO W-SECONDS.
	 * **  MOVE DATE-OUTPUT(21:2)      TO W-HUNDREDTHS.
	 * **  MOVE DATE-OUTPUT(23:4)      TO W-MILLISECS.
	 * **
	 * **ADD UNIT NBR TO HUNDREDTHS TO AVOID DUPLICATION ON
	 * **              A MULTI-UNIT OR MULTI-CLIENT INSERT
	 * **  ADD UIDG-UNIT-NBR TO W-HUNDREDTHS-NUM.
	 * **
	 * **************** END OF CODE REMOVAL ***************
	 * **************** NEW PROCESSING ********************
	 * * GET THE CURRENT DB2 TIMESTAMP.
	 * ****************************************************</pre>*/
	private void generateId() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ID-BUILDING-OKAY TO TRUE.
		ws.getwGeneralWorkfields().getIdBuildingErr().setOkay();
		// COB_CODE: EXEC SQL
		//               SET :W-CURRENT-TIMESTAMP = CURRENT TIMESTAMP
		//           END-EXEC.
		ws.getwCurrentTimestampNum().setwCurrentTimestamp(SqlFunctions.currentTimestamp());
		// COB_CODE: EVALUATE TRUE
		//             WHEN ERD-SQL-GOOD
		//               PERFORM 0155-BUILD-WITH-1970-SECS
		//             WHEN OTHER
		//               PERFORM 0160-BUILD-WITH-CICS-TS
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: PERFORM 0155-BUILD-WITH-1970-SECS
			buildWith1970Secs();
			break;

		default:// COB_CODE: PERFORM 0160-BUILD-WITH-CICS-TS
			buildWithCicsTs();
			break;
		}
		// COB_CODE: IF ID-BUILDING-FAILED
		//               GO TO 0150-GENERATE-ID-X
		//           END-IF.
		if (ws.getwGeneralWorkfields().getIdBuildingErr().isFailed()) {
			// COB_CODE: GO TO 0150-GENERATE-ID-X
			return;
		}
		//* REVERSE THE RESULT, PLACING THE MOST VOLATILE CHARACTERS
		//* AT THE FRONT.
		// COB_CODE: PERFORM VARYING W-CHAR-INDEX FROM 1 BY +1
		//              UNTIL W-CHAR-INDEX > 8
		//                                  (16 - W-CHAR-INDEX + 1:1)
		//           END-PERFORM.
		ws.getwGeneralWorkfields().setCharIndex(((short) 1));
		while (!(ws.getwGeneralWorkfields().getCharIndex() > 8)) {
			// COB_CODE: MOVE W-16-CHAR-TS-ID(W-CHAR-INDEX:1)
			//                      TO W-TEMP-DIGIT
			ws.getwGeneralWorkfields().setTempDigitFormatted(ws.getwGeneralWorkfields().getW16CharTsIdFormatted()
					.substring((ws.getwGeneralWorkfields().getCharIndex()) - 1, ws.getwGeneralWorkfields().getCharIndex()));
			// COB_CODE: MOVE W-16-CHAR-TS-ID
			//                            (16 - W-CHAR-INDEX + 1 : 1)
			//                      TO W-16-CHAR-TS-ID
			//                            (W-CHAR-INDEX:1)
			ws.getwGeneralWorkfields().setW16CharTsId(Functions.setSubstring(
					ws.getwGeneralWorkfields().getW16CharTsId(), ws.getwGeneralWorkfields().getW16CharTsIdFormatted()
							.substring((16 - ws.getwGeneralWorkfields().getCharIndex() + 1) - 1, 16 - ws.getwGeneralWorkfields().getCharIndex() + 1),
					ws.getwGeneralWorkfields().getCharIndex(), 1));
			// COB_CODE: MOVE W-TEMP-DIGIT
			//                      TO W-16-CHAR-TS-ID
			//                            (16 - W-CHAR-INDEX + 1:1)
			ws.getwGeneralWorkfields().setW16CharTsId(Functions.setSubstring(ws.getwGeneralWorkfields().getW16CharTsId(),
					ws.getwGeneralWorkfields().getTempDigitFormatted(), 16 - ws.getwGeneralWorkfields().getCharIndex() + 1, 1));
			ws.getwGeneralWorkfields().setCharIndex(Trunc.toShort(ws.getwGeneralWorkfields().getCharIndex() + 1, 2));
		}
		//* CONVERT TO BASE 36 PRODUCING UP TO A 10 CHARACTER STRING.
		// COB_CODE: MOVE W-16-CHAR-TS-ID TO W2000-INPUT-BASE10.
		ws.getW2000Workfields().setInputBase10Formatted(ws.getwGeneralWorkfields().getW16CharTsIdFormatted());
		// COB_CODE: PERFORM 2000-CONVERT-TO-36.
		method2000010();
		//* CREATE A 32 BYTE RANDOM STRING USING THE UOW MSG ID FROM
		//* HALLUBOC.  IF THE UOW MSG ID CONTAINS EMBEDDED SPACES, ONLY
		//* THE CHARACTERS TO THE LEFT OF THE FIRST SPACE WILL BE USED.
		//* UOW MSG IDS WITH EMBEDDED SPACES ARE NOT RECOMMENDED.
		// COB_CODE: SET START-OF-UBOC-MSG-ID TO TRUE.
		ws.getwGeneralWorkfields().getUbocMsgIdScan().setStartOfUbocMsgId();
		// COB_CODE: PERFORM VARYING W-BACKEND-CNTR
		//             FROM 1 BY 1
		//             UNTIL END-OF-UBOC-MSG-ID
		//               END-IF
		//           END-PERFORM.
		ws.getwGeneralWorkfields().setBackendCntr(((short) 1));
		while (!ws.getwGeneralWorkfields().getUbocMsgIdScan().isEndOfUbocMsgId()) {
			// COB_CODE: IF UBOC-MSG-ID(W-BACKEND-CNTR:1) EQUAL SPACE
			//             OR W-BACKEND-CNTR >= (LENGTH OF UBOC-MSG-ID)
			//               SET END-OF-UBOC-MSG-ID TO TRUE
			//           END-IF
			if (Conditions
					.eq(dfhcommarea.getCommInfo().getUbocMsgIdFormatted().substring((ws.getwGeneralWorkfields().getBackendCntr()) - 1,
							ws.getwGeneralWorkfields().getBackendCntr()), "")
					|| ws.getwGeneralWorkfields().getBackendCntr() >= UbocCommInfo.Len.UBOC_MSG_ID) {
				// COB_CODE: SET END-OF-UBOC-MSG-ID TO TRUE
				ws.getwGeneralWorkfields().getUbocMsgIdScan().setEndOfUbocMsgId();
			}
			ws.getwGeneralWorkfields().setBackendCntr(Trunc.toShort(ws.getwGeneralWorkfields().getBackendCntr() + 1, 2));
		}
		// COB_CODE: SUBTRACT 1 FROM W-BACKEND-CNTR.
		ws.getwGeneralWorkfields().setBackendCntr(Trunc.toShort(abs(ws.getwGeneralWorkfields().getBackendCntr() - 1), 2));
		// COB_CODE: MOVE 0 TO W-FRONTEND-CNTR
		//                     W-SCRAMBLED-CNTR.
		ws.getwGeneralWorkfields().setFrontendCntr(((short) 0));
		ws.getwGeneralWorkfields().setScrambledCntr(((short) 0));
		// COB_CODE: MOVE SPACES TO W-32-CHAR-UBOC-ID
		//                          W-SCRAMBLED-MSG-ID.
		ws.getwGeneralWorkfields().setW32CharUbocId("");
		ws.getwGeneralWorkfields().setScrambledMsgId("");
		// COB_CODE: MOVE UBOC-MSG-ID TO W-SCRAMBLED-MSG-ID.
		ws.getwGeneralWorkfields().setScrambledMsgId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: SET MSG-ID-NOT-PROCESSED TO TRUE.
		ws.getwGeneralWorkfields().getMsgIdProcessing().setNotProcessed();
		// COB_CODE: SET SCRAMBLE-INCOMPLETE TO TRUE.
		ws.getwGeneralWorkfields().getScrambleProcess().setIncomplete();
		// COB_CODE: PERFORM UNTIL SCRAMBLE-COMPLETE
		//               END-IF
		//           END-PERFORM.
		while (!ws.getwGeneralWorkfields().getScrambleProcess().isComplete()) {
			// COB_CODE: PERFORM 0151-SCRAMBLE-MSG-ID
			//             UNTIL SCRAMBLE-COMPLETE OR MSG-ID-PROCESSED
			while (!(ws.getwGeneralWorkfields().getScrambleProcess().isComplete() || ws.getwGeneralWorkfields().getMsgIdProcessing().isProcessed())) {
				scrambleMsgId();
			}
			// COB_CODE: IF SCRAMBLE-INCOMPLETE
			//               SET MSG-ID-NOT-PROCESSED TO TRUE
			//           END-IF
			if (ws.getwGeneralWorkfields().getScrambleProcess().isIncomplete()) {
				// COB_CODE: MOVE SPACES TO W-SCRAMBLED-MSG-ID
				ws.getwGeneralWorkfields().setScrambledMsgId("");
				// COB_CODE: MOVE W-32-CHAR-UBOC-ID TO W-SCRAMBLED-MSG-ID
				ws.getwGeneralWorkfields().setScrambledMsgId(ws.getwGeneralWorkfields().getW32CharUbocId());
				// COB_CODE: MOVE 0 TO W-FRONTEND-CNTR
				ws.getwGeneralWorkfields().setFrontendCntr(((short) 0));
				// COB_CODE: MOVE W-SCRAMBLED-CNTR TO W-BACKEND-CNTR
				ws.getwGeneralWorkfields().setBackendCntr(ws.getwGeneralWorkfields().getScrambledCntr());
				// COB_CODE: SUBTRACT 1 FROM W-SCRAMBLED-CNTR
				ws.getwGeneralWorkfields().setScrambledCntr(Trunc.toShort(abs(ws.getwGeneralWorkfields().getScrambledCntr() - 1), 2));
				// COB_CODE: SET MSG-ID-NOT-PROCESSED TO TRUE
				ws.getwGeneralWorkfields().getMsgIdProcessing().setNotProcessed();
			}
		}
		//* APPEND ALL OR A PORTION OF THE 32 BYTE 'SHUFFLED MSG ID' STRING
		//* ONTO THE BASE 36 REVERSED SECONDS/MICROSECS STRING (WHICH WILL
		//* BE 10 OR FEWER CHARS) GIVING THE FINAL 32 BYTE RANDOM, UNIQUE
		//* ID.
		// COB_CODE: MOVE SPACES TO W-32-CHAR-GEND-ID.
		ws.getwGeneralWorkfields().setW32CharGendId("");
		// COB_CODE: STRING W2000-OUTPUT-BASE36 DELIMITED BY SPACE
		//                  W-32-CHAR-UBOC-ID DELIMITED BY SIZE
		//             INTO W-32-CHAR-GEND-ID
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(WGeneralWorkfields.Len.W32_CHAR_GEND_ID,
				Functions.substringBefore(ws.getW2000Workfields().getOutputBase36Formatted(), Types.SPACE_STRING),
				ws.getwGeneralWorkfields().getW32CharUbocIdFormatted());
		ws.getwGeneralWorkfields().setW32CharGendId(concatUtil.replaceInString(ws.getwGeneralWorkfields().getW32CharGendIdFormatted()));
		// COB_CODE: MOVE W-32-CHAR-GEND-ID TO UIDG-GENERATED-ID.
		ws.getHalluidg().getUidgErrorIdOutput().setUidgGeneratedId(ws.getwGeneralWorkfields().getW32CharGendId());
	}

	/**Original name: 0151-SCRAMBLE-MSG-ID_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SCRAMBLE THE INPUT VALUE BY MOVING THE CHARACTERS IN A 'FRONT
	 *  THEN BACK' ALTERNATING FASHION, THEREBY CREATING A NEW OUTPUT
	 *  VALUE.
	 * *****************************************************************</pre>*/
	private void scrambleMsgId() {
		// COB_CODE: ADD +1 TO W-SCRAMBLED-CNTR.
		ws.getwGeneralWorkfields().setScrambledCntr(Trunc.toShort(1 + ws.getwGeneralWorkfields().getScrambledCntr(), 2));
		// COB_CODE: ADD +1 TO W-FRONTEND-CNTR.
		ws.getwGeneralWorkfields().setFrontendCntr(Trunc.toShort(1 + ws.getwGeneralWorkfields().getFrontendCntr(), 2));
		// COB_CODE: PERFORM 0152-SCRAMBLE-COMPLETE-CHECK.
		scrambleCompleteCheck();
		// COB_CODE: IF MSG-ID-NOT-PROCESSED OR SCRAMBLE-INCOMPLETE
		//               END-IF
		//           END-IF.
		if (ws.getwGeneralWorkfields().getMsgIdProcessing().isNotProcessed() || ws.getwGeneralWorkfields().getScrambleProcess().isIncomplete()) {
			// COB_CODE: MOVE W-SCRAMBLED-MSG-ID(W-FRONTEND-CNTR:1)
			//             TO W-32-CHAR-UBOC-ID(W-SCRAMBLED-CNTR:1)
			ws.getwGeneralWorkfields()
					.setW32CharUbocId(Functions.setSubstring(
							ws.getwGeneralWorkfields().getW32CharUbocId(), ws.getwGeneralWorkfields().getScrambledMsgIdFormatted()
									.substring((ws.getwGeneralWorkfields().getFrontendCntr()) - 1, ws.getwGeneralWorkfields().getFrontendCntr()),
							ws.getwGeneralWorkfields().getScrambledCntr(), 1));
			// COB_CODE: ADD +1 TO W-SCRAMBLED-CNTR
			ws.getwGeneralWorkfields().setScrambledCntr(Trunc.toShort(1 + ws.getwGeneralWorkfields().getScrambledCntr(), 2));
			// COB_CODE: SUBTRACT 1 FROM W-BACKEND-CNTR
			ws.getwGeneralWorkfields().setBackendCntr(Trunc.toShort(abs(ws.getwGeneralWorkfields().getBackendCntr() - 1), 2));
			// COB_CODE: PERFORM 0152-SCRAMBLE-COMPLETE-CHECK
			scrambleCompleteCheck();
			// COB_CODE: IF MSG-ID-NOT-PROCESSED OR SCRAMBLE-INCOMPLETE
			//                 TO W-32-CHAR-UBOC-ID(W-SCRAMBLED-CNTR:1)
			//           END-IF
			if (ws.getwGeneralWorkfields().getMsgIdProcessing().isNotProcessed() || ws.getwGeneralWorkfields().getScrambleProcess().isIncomplete()) {
				// COB_CODE: MOVE W-SCRAMBLED-MSG-ID(W-BACKEND-CNTR:1)
				//             TO W-32-CHAR-UBOC-ID(W-SCRAMBLED-CNTR:1)
				ws.getwGeneralWorkfields()
						.setW32CharUbocId(Functions.setSubstring(
								ws.getwGeneralWorkfields().getW32CharUbocId(), ws.getwGeneralWorkfields().getScrambledMsgIdFormatted()
										.substring((ws.getwGeneralWorkfields().getBackendCntr()) - 1, ws.getwGeneralWorkfields().getBackendCntr()),
								ws.getwGeneralWorkfields().getScrambledCntr(), 1));
			}
		}
	}

	/**Original name: 0152-SCRAMBLE-COMPLETE-CHECK_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DETERMINE IF CREATION OF A 32 CHARACTER SCRAMBLED ID IS COMPLETE
	 * *****************************************************************</pre>*/
	private void scrambleCompleteCheck() {
		// COB_CODE: IF W-SCRAMBLED-CNTR > (LENGTH OF W-32-CHAR-UBOC-ID)
		//               GO TO 0152-SCRAMBLE-COMPLETE-CHECK-X
		//           END-IF.
		if (ws.getwGeneralWorkfields().getScrambledCntr() > WGeneralWorkfields.Len.W32_CHAR_UBOC_ID) {
			// COB_CODE: SET SCRAMBLE-COMPLETE TO TRUE
			ws.getwGeneralWorkfields().getScrambleProcess().setComplete();
			// COB_CODE: GO TO 0152-SCRAMBLE-COMPLETE-CHECK-X
			return;
		}
		// COB_CODE: IF W-FRONTEND-CNTR = W-BACKEND-CNTR
		//               SET MSG-ID-PROCESSED TO TRUE
		//           END-IF.
		if (ws.getwGeneralWorkfields().getFrontendCntr() == ws.getwGeneralWorkfields().getBackendCntr()) {
			// COB_CODE: SET MSG-ID-PROCESSED TO TRUE
			ws.getwGeneralWorkfields().getMsgIdProcessing().setProcessed();
		}
	}

	/**Original name: 0155-BUILD-WITH-1970-SECS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  USING THE SECONDS SINCE 1970 AND THE MICROSECONDS OF THE      *
	 *  CURRENT TIMESTAMP, CREATE A 16 BYTE NUMBER.                   *
	 * ****************************************************************
	 * * GET THE NUMBER OF SECONDS SINCE 1970.  ADD CURRENT HR/MN/SEC
	 * * TO THE SECONDS SINCE 1970 GIVING THE A 10 DIGIT FIELD.</pre>*/
	private void buildWith1970Secs() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//               SET :W-SECONDS-SINCE-1970 =
		//                      (DAYS(CURRENT DATE)
		//                          - DAYS('1970-01-01')) * 86400
		//           END-EXEC.
		ws.getwGeneralWorkfields().setSecondsSince1970(halouidgGenericDao.setHostVarRec(ws.getwGeneralWorkfields().getSecondsSince1970()));
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 0155-BUILD-WITH-1970-SECS-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: PERFORM 0160-BUILD-WITH-CICS-TS
			buildWithCicsTs();
			// COB_CODE: GO TO 0155-BUILD-WITH-1970-SECS-X
			return;
		}
		// COB_CODE: COMPUTE W-SECONDS-SINCE-1970 =
		//                   W-SECONDS-SINCE-1970 +
		//                        W-HOURS-NUM * 3600.
		ws.getwGeneralWorkfields().setSecondsSince1970(
				Trunc.toInt(ws.getwGeneralWorkfields().getSecondsSince1970() + ws.getwCurrentTimestampNum().getHoursNum() * 3600, 9));
		// COB_CODE: COMPUTE W-SECONDS-SINCE-1970 =
		//                   W-SECONDS-SINCE-1970 +
		//                        W-MINUTES-NUM * 60.
		ws.getwGeneralWorkfields().setSecondsSince1970(
				Trunc.toInt(ws.getwGeneralWorkfields().getSecondsSince1970() + ws.getwCurrentTimestampNum().getMinutesNum() * 60, 9));
		// COB_CODE: COMPUTE W-SECONDS-SINCE-1970 =
		//                   W-SECONDS-SINCE-1970 +
		//                        W-SECONDS-NUM.
		ws.getwGeneralWorkfields()
				.setSecondsSince1970(Trunc.toInt(ws.getwGeneralWorkfields().getSecondsSince1970() + ws.getwCurrentTimestampNum().getSecondsNum(), 9));
		// COB_CODE: MOVE W-SECONDS-SINCE-1970 TO W-SECONDS-SINCE-1970-NUM.
		ws.getwGeneralWorkfields().setSecondsSince1970Num(TruncAbs.toLong(ws.getwGeneralWorkfields().getSecondsSince1970(), 10));
		//* ADD THE PASSED IN UNIT NUMBER TO THE MICROSECONDS.
		// COB_CODE: ADD UIDG-UNIT-NBR TO W-MICROS-NUM.
		ws.getwCurrentTimestampNum()
				.setMicrosNum(Trunc.toInt(abs(ws.getHalluidg().getUidgUnitNbr() + ws.getwCurrentTimestampNum().getMicrosNum()), 6));
		//* APPEND THE MICROSECONDS FROM THE CURRENT TIMESTAMP TO THE
		//* SECONDS SINCE 1970 GIVING A 16 CHARACTER STRING.
		// COB_CODE: MOVE SPACES TO W-16-CHAR-TS-ID.
		ws.getwGeneralWorkfields().setW16CharTsId("");
		// COB_CODE: STRING W-SECONDS-SINCE-1970-NUM
		//                  W-MICROS-NUM
		//             DELIMITED BY SIZE
		//             INTO W-16-CHAR-TS-ID.
		concatUtil = ConcatUtil.buildString(WGeneralWorkfields.Len.W16_CHAR_TS_ID, ws.getwGeneralWorkfields().getSecondsSince1970NumAsString(),
				ws.getwCurrentTimestampNum().getMicrosNumAsString());
		ws.getwGeneralWorkfields().setW16CharTsId(concatUtil.replaceInString(ws.getwGeneralWorkfields().getW16CharTsIdFormatted()));
	}

	/**Original name: 0160-BUILD-WITH-CICS-TS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  IF THE ID GENERATOR HAS BEEN CALLED FROM ERROR LOGGING, USE CICS
	 *  TO BUILD AN ID IF THIS MODULE (HALOUIDG) IS NOT BOUND.  IF THIS
	 *  MODULE HAS BEEN CALLED BY SOMETHING OTHER THAN ERROR LOGGING AND
	 *  HAS RECEIVED AN SQL ERROR, THEN CALL ERROR LOGGING TO STORE THE
	 *  ERROR.
	 * *****************************************************************</pre>*/
	private void buildWithCicsTs() {
		// COB_CODE: IF NOT UIDG-ERROR-ID
		//               GO TO 0160-BUILD-WITH-CICS-TS-X
		//           END-IF.
		if (!ws.getHalluidg().getUidgIdType().isErrorId()) {
			// COB_CODE: SET ID-BUILDING-FAILED       TO TRUE
			ws.getwGeneralWorkfields().getIdBuildingErr().setFailed();
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SET-CURRENT-TS  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2SetCurrentTs();
			// COB_CODE: MOVE 'CURRENT TIMESTAMP'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("CURRENT TIMESTAMP");
			// COB_CODE: MOVE '0160-BUILD-WITH-CICS-TS'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0160-BUILD-WITH-CICS-TS");
			// COB_CODE: MOVE 'GETTING TIMESTAMP FROM DB2 FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("GETTING TIMESTAMP FROM DB2 FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0160-BUILD-WITH-CICS-TS-X
			return;
		}
		//* GET ABSTIME FROM CICS.  THIS IS THE NUMBER OF MILLISECONDS
		//* SINCE 01/01/1900.  THIS WILL BE USED TO BUILD THE ID PASSED
		//* BACK TO ERROR LOGGING.
		// COB_CODE: EXEC CICS ASKTIME
		//                     ABSTIME  (W-ABSTIME)
		//           END-EXEC.
		ws.getwGeneralWorkfields().setAbstime((execContext.updateTime()).toLong());
		// COB_CODE: MOVE W-ABSTIME TO W-ABSTIME-NUM.
		ws.getwGeneralWorkfields().setAbstimeNum(TruncAbs.toLong(ws.getwGeneralWorkfields().getAbstime(), 16));
		// COB_CODE: MOVE W-ABSTIME-NUM TO W-16-CHAR-TS-ID.
		ws.getwGeneralWorkfields().setW16CharTsId(ws.getwGeneralWorkfields().getAbstimeNumFormatted());
	}

	/**Original name: 2000-010<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *     CONVERTS W-INPUT-BASE10 TO BASE 36, LEFT JUSTIFIED IN       *
	 *     W-OUTPUT-BASE36.                                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void method2000010() {
		int counter = 0;
		// COB_CODE: MOVE SPACES         TO W2000-OUTPUT-BASE36.
		ws.getW2000Workfields().setOutputBase36("");
		// COB_CODE: MOVE 10             TO W2000-INDEX.
		ws.getW2000Workfields().setIndex2(((short) 10));
		// COB_CODE: MOVE W2000-INPUT-BASE10 TO W2000-QUOTIENT.
		ws.getW2000Workfields().setQuotient(ws.getW2000Workfields().getInputBase10());
		// COB_CODE: PERFORM UNTIL W2000-QUOTIENT < 36
		//              SUBTRACT 1 FROM W2000-INDEX
		//           END-PERFORM.
		while (!(ws.getW2000Workfields().getQuotient() < 36)) {
			// COB_CODE: DIVIDE 36 INTO W2000-QUOTIENT
			//              GIVING W2000-QUOTIENT
			//              REMAINDER W2000-REMAINDER
			//           END-DIVIDE
			ws.getW2000Workfields().setRemainder(ws.getW2000Workfields().getQuotient() % 36);
			ws.getW2000Workfields().setQuotient(ws.getW2000Workfields().getQuotient() / 36);
			// COB_CODE: MOVE W2000-REF-DIGITS( W2000-REMAINDER + 1 : 1 )
			//                               TO W2000-OUTPUT-BASE36(W2000-INDEX:1)
			ws.getW2000Workfields()
					.setOutputBase36(Functions.setSubstring(ws.getW2000Workfields().getOutputBase36(),
							ws.getW2000Workfields().getRefDigitsFormatted().substring((((int) (ws.getW2000Workfields().getRemainder() + 1))) - 1,
									((int) (ws.getW2000Workfields().getRemainder() + 1))),
							ws.getW2000Workfields().getIndex2(), 1));
			// COB_CODE: SUBTRACT 1 FROM W2000-INDEX
			ws.getW2000Workfields().setIndex2(Trunc.toShort(abs(ws.getW2000Workfields().getIndex2() - 1), 2));
		}
		// COB_CODE: MOVE W2000-REF-DIGITS( W2000-QUOTIENT + 1 : 1 )
		//                                  TO W2000-OUTPUT-BASE36(W2000-INDEX:1)
		ws.getW2000Workfields()
				.setOutputBase36(Functions.setSubstring(ws.getW2000Workfields().getOutputBase36(), ws.getW2000Workfields().getRefDigitsFormatted()
						.substring((((int) (ws.getW2000Workfields().getQuotient() + 1))) - 1, ((int) (ws.getW2000Workfields().getQuotient() + 1))),
						ws.getW2000Workfields().getIndex2(), 1));
		//* LEFT-JUSTIFY THE RESULTING VALUE.
		// COB_CODE: PERFORM 10 TIMES
		//              END-IF
		//           END-PERFORM.
		counter = 10;
		for (int i = 1; i <= counter; i++) {
			// COB_CODE: IF W2000-OUTPUT-BASE36(1:1) = SPACES
			//                                        TO W2000-OUTPUT-BASE36(1:10)
			//           END-IF
			if (Conditions.eq(ws.getW2000Workfields().getOutputBase36Formatted().substring((1) - 1, 1), "")) {
				// COB_CODE: MOVE W2000-OUTPUT-BASE36(2:9)
				//                                    TO W2000-OUTPUT-BASE36(1:10)
				ws.getW2000Workfields().setOutputBase36(Functions.setSubstring(ws.getW2000Workfields().getOutputBase36(),
						ws.getW2000Workfields().getOutputBase36Formatted().substring((2) - 1, 10), 1, 10));
			}
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsSpecificMisc().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsSpecificMisc().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALOUIDG", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibrespDsply(ws.getWsSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initWsEstoInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
