/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: HSDD-DFL-OFS-PER<br>
 * Variable: HSDD-DFL-OFS-PER from copybook HALLGSDD<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HsddDflOfsPer {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.DFL_OFS_PER);
	public static final String HOURS = "HOURS";
	public static final String DAYS = "DAYS";
	public static final String WEEKS = "WEEKS";
	public static final String MONTHS = "MONTHS";
	public static final String YEARS = "YEARS";

	//==== METHODS ====
	public void setDflOfsPer(String dflOfsPer) {
		this.value = Functions.subString(dflOfsPer, Len.DFL_OFS_PER);
	}

	public String getDflOfsPer() {
		return this.value;
	}

	public String getDflOfsPerFormatted() {
		return Functions.padBlanks(getDflOfsPer(), Len.DFL_OFS_PER);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DFL_OFS_PER = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
