/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclrecTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xzc050c1;
import com.federatedinsurance.crs.ws.enums.TtThirdPartiesXz0b9081;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9081<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9081Data {

	//==== PROPERTIES ====
	public static final int TT_THIRD_PARTIES_MAXOCCURS = 2500;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b9081 constantFields = new ConstantFieldsXz0b9081();
	//Original name: EA-02-NOTHING-FOUND-MSG
	private Ea02NothingFoundMsgXz0b9081 ea02NothingFoundMsg = new Ea02NothingFoundMsgXz0b9081();
	//Original name: EA-03-MAX-CERTS-FOUND-MSG
	private Ea03MaxCertsFoundMsg ea03MaxCertsFoundMsg = new Ea03MaxCertsFoundMsg();
	//Original name: SS-CI
	private short ssCi = DefaultValues.BIN_SHORT_VAL;
	//Original name: SWITCHES
	private SwitchesXz0b9081 switches = new SwitchesXz0b9081();
	//Original name: TT-THIRD-PARTIES
	private LazyArrayCopy<TtThirdPartiesXz0b9081> ttThirdParties = new LazyArrayCopy<>(new TtThirdPartiesXz0b9081(), 1,
			TT_THIRD_PARTIES_MAXOCCURS);
	//Original name: IX-TT
	private int ixTt = 1;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9081 workingStorageArea = new WorkingStorageAreaXz0b9081();
	//Original name: WS-XZ0A9081-ROW
	private WsXz0a9081Row wsXz0a9081Row = new WsXz0a9081Row();
	//Original name: XZC050C1
	private Xzc050c1 xzc050c1 = new Xzc050c1();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: DCLREC-TYP
	private DclrecTyp dclrecTyp = new DclrecTyp();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public void setSsCi(short ssCi) {
		this.ssCi = ssCi;
	}

	public short getSsCi() {
		return this.ssCi;
	}

	public void initTableOfThirdPartiesHighValues() {
		getTtThirdPartiesObj().fill(new TtThirdPartiesXz0b9081().initTtThirdPartiesHighValues());
	}

	public void setIxTt(int ixTt) {
		this.ixTt = ixTt;
	}

	public int getIxTt() {
		return this.ixTt;
	}

	public void setWsXzc050c1RowBytes(byte[] buffer) {
		setWsXzc050c1RowBytes(buffer, 1);
	}

	/**Original name: WS-XZC050C1-ROW<br>
	 * <pre>**  LAYOUT FOR CALLING GetCertificateList.</pre>*/
	public byte[] getWsXzc050c1RowBytes() {
		byte[] buffer = new byte[Len.WS_XZC050C1_ROW];
		return getWsXzc050c1RowBytes(buffer, 1);
	}

	public void setWsXzc050c1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc050c1.setXzc050ProgramInputBytes(buffer, position);
		position += Xzc050c1.Len.XZC050_PROGRAM_INPUT;
		xzc050c1.setXzc050ProgramOutputBytes(buffer, position);
	}

	public byte[] getWsXzc050c1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc050c1.getXzc050ProgramInputBytes(buffer, position);
		position += Xzc050c1.Len.XZC050_PROGRAM_INPUT;
		xzc050c1.getXzc050ProgramOutputBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public ConstantFieldsXz0b9081 getConstantFields() {
		return constantFields;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclrecTyp getDclrecTyp() {
		return dclrecTyp;
	}

	public Ea02NothingFoundMsgXz0b9081 getEa02NothingFoundMsg() {
		return ea02NothingFoundMsg;
	}

	public Ea03MaxCertsFoundMsg getEa03MaxCertsFoundMsg() {
		return ea03MaxCertsFoundMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public SwitchesXz0b9081 getSwitches() {
		return switches;
	}

	public TtThirdPartiesXz0b9081 getTtThirdParties(int idx) {
		return ttThirdParties.get(idx - 1);
	}

	public LazyArrayCopy<TtThirdPartiesXz0b9081> getTtThirdPartiesObj() {
		return ttThirdParties;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b9081 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9081Row getWsXz0a9081Row() {
		return wsXz0a9081Row;
	}

	public Xzc050c1 getXzc050c1() {
		return xzc050c1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_XZC050C1_ROW = Xzc050c1.Len.XZC050_PROGRAM_INPUT + Xzc050c1.Len.XZC050_PROGRAM_OUTPUT;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
