/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: HALRRESP-REC-FOUND-SW<br>
 * Variable: HALRRESP-REC-FOUND-SW from copybook HALLRESP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HalrrespRecFoundSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FOUND = 'Y';
	public static final char NOT_FOUND = 'N';

	//==== METHODS ====
	public void setRecFoundSw(char recFoundSw) {
		this.value = recFoundSw;
	}

	public char getRecFoundSw() {
		return this.value;
	}

	public void setFound() {
		value = FOUND;
	}

	public boolean isHalrrespRecNotFound() {
		return value == NOT_FOUND;
	}

	public void setNotFound() {
		value = NOT_FOUND;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_FOUND_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
