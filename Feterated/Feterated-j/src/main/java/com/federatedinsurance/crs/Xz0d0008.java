/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.storage.KeyType;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpAccessStatus;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.ActNotPolDao;
import com.federatedinsurance.crs.commons.data.dao.ActNotPolRecDao;
import com.federatedinsurance.crs.commons.data.dao.ActNotRecDao;
import com.federatedinsurance.crs.commons.data.dao.HalBoAudTgrVDao;
import com.federatedinsurance.crs.commons.data.dao.HalBoMduXrfVDao;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.commons.data.dao.HalUowObjHierVDao;
import com.federatedinsurance.crs.commons.data.dao.Xz0d0008GenericDao;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.Xzc008ActNotPolRecRow;
import com.federatedinsurance.crs.ws.UbocRecord;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsHalrlomgLinkage1;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsSpecificMiscXz0d0008;
import com.federatedinsurance.crs.ws.Xz0d0008Data;
import com.federatedinsurance.crs.ws.enums.UbocPassThruAction;
import com.federatedinsurance.crs.ws.enums.WsCursorSelectionSw;
import com.federatedinsurance.crs.ws.enums.WsUmtHdrSw;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0D0008<br>
 * <pre>AUTHOR.  DAWN POSSEHL.
 * DATE-WRITTEN. 23 SEP 2010.
 * ****************************************************************
 * *
 * * PROGRAM TITLE - ACT_NOT_POL_REC
 * *                 BDO (BUSINESS DATA OBJECT)
 * *
 * * PLATFORM - BUSINESS FRAMEWORK
 * *
 * * OPERATING SYSTEM - MVS
 * *
 * * LANGUAGE - COBOL
 * *
 * * SKELETON VERSION - S21PHASE2INF
 * *
 * * COMPONENT VERSION - 5.1  JUN 09, 2006
 * *
 * * SKELETON TEMPLATE USED FOR GENERATION - BDOSKEL
 * *
 * * PURPOSE -  DATA BUSINESS OBJECT TO READ, DELETE, INSERT,
 * *            OR UPDATE VIEW ACT_NOT_POL_REC
 * *            OF TABLE ACT_NOT_POL_REC
 * *
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED IN THE
 * *                       FOLLOWING WAYS:
 * *
 * *                       XZ0D0008 IS STARTED BY A LINK FROM
 * *                       PROGRAM HALOMCM WHEN CALLED AS A
 * *                       PRIMARY BUSINESS OBJECT, OR CAN BE
 * *                       CALLED AS A SECONDARY BUSINESS OBJECT
 * *                       BY A PRIMARY BUSINESS OBJECT AS
 * *                       DEFINED IN THE BUS_OBJ_HIERACHY TABLE.
 * *
 * * DATA ACCESS METHODS - UMT STORAGE RECORDS
 * *
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G
 * *
 * * CASE #    DATE      PROG               DESCRIPTION
 * * ------- ----------- ------- --------------------------------
 * * CHECK                       GENERATED TAG TO IDENTIFY AREAS
 * *                              THAT MAY NEED TO BE CHANGED
 * * SAMPLE                      GENERATED SAMPLE CODE
 * * NOTE                        GENERATED NOTES
 * *
 * * 02570   23 SEP 2010 E404DLP GENERATED
 * * PP02500 25 JUL 2012 E404BPO RECOMPILE FOR XZ0H0003 CHANGE
 * ****************************************************************</pre>*/
public class Xz0d0008 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>* DCLGEN USED FOR ACCESSING DATA SECURITY.
	 * *   EXEC SQL
	 * *       INCLUDE HALLGDPS
	 * *   END-EXEC.
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS
	 *        05  CIDP-TABLE-NAME                PIC X(18).
	 *        05  CIDP-TABLE-ROW                 PIC X(4982).</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private HalBoAudTgrVDao halBoAudTgrVDao = new HalBoAudTgrVDao(dbAccessStatus);
	private HalUowObjHierVDao halUowObjHierVDao = new HalUowObjHierVDao(dbAccessStatus);
	private HalBoMduXrfVDao halBoMduXrfVDao = new HalBoMduXrfVDao(dbAccessStatus);
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	private ActNotPolRecDao actNotPolRecDao = new ActNotPolRecDao(dbAccessStatus);
	private Xz0d0008GenericDao xz0d0008GenericDao = new Xz0d0008GenericDao(dbAccessStatus);
	private ActNotPolDao actNotPolDao = new ActNotPolDao(dbAccessStatus);
	private ActNotRecDao actNotRecDao = new ActNotRecDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0d0008Data ws = new Xz0d0008Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private WsHalrlomgLinkage1 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, WsHalrlomgLinkage1 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		returnToFrontEnd();
		return 0;
	}

	public static Xz0d0008 getInstance() {
		return (Programs.getInstance(Xz0d0008.class));
	}

	/**Original name: 0000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   THE 0000-MAIN PARAGRAPH IS RESPONSIBLE FOR CONTROLLING THE
	 *   PROCESSING OF THE FUNCTION PASSED TO IT.
	 * ****************************************************************
	 * **  PERFORM 0001-INITIALIZE-MODULE.                              00200000</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM MONB-PERF-MON-BEGIN.
		monbPerfMonBegin();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		//*** PERFORM 0100-INITIALIZE-WARN-MSG.
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		//*** PERFORM 0110-VALIDATE-COMMAREA.
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// COB_CODE: PERFORM 0200-INITIALIZE.
		initialize();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// COB_CODE: IF CASCADING-DELETE OF UBOC-COMM-INFO
		//               END-IF
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isCascadingDelete()) {
			// COB_CODE: PERFORM 0330-PROCESS-PASSED-REQUEST
			processPassedRequest();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0000-RETURN-TO-FRONT-END
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
				returnToFrontEnd();
			}
		}
		// COB_CODE: PERFORM 0300-PROCESS-REQ-MSG-UMT.
		processReqMsgUmt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// COB_CODE: IF WS-RESP-DATA-ROWS-WRIT
		//               END-IF
		//           END-IF.
		if (ws.getWsBdoSwitches().getRespDataRowsWritSw().isWsRespDataRowsWrit()) {
			// COB_CODE: PERFORM 4020-WRITE-RESP-HDR-REC
			writeRespHdrRec();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0000-RETURN-TO-FRONT-END
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
				returnToFrontEnd();
			}
		}
		//** GENERALLY SPEAKING, FETCH UOWS SHOULD NOT INCLUDE THE SIMPLE
		//** EDIT HUB.  HOWEVER, AS A PROTECTION, A CHECK HAS BEEN ADDED TO
		//** SKIP HIERARCHY PROCESSING AT THIS POINT IF THE EXPLICIT ACTION
		//** OF FETCH-AND-SKIP-HIER-REQUEST IS SET WITHIN A UOW THAT
		//** INCLUDES THE SIMPLE EDIT HUB.  HALCDPDC DOES NOT SUPPORT A WAY
		//** FOR WS-IUD-PROCESSED TO BE TRUE AND HAVE
		//** FETCH-AND-SKIP-HIER-REQUEST ALSO SET TO TRUE.  THEREFORE,THIS
		//** CHECK HAS NOT BEEN ADDED TO THE SECOND CONDITION FOR CALLING
		//** HIERARCHY PROCESSING.  ADDITIONALLY, CASCADING-DELETE REQUESTS
		//** ARE NOT PROCESSED DURING THE SIMPLE EDIT PHASE.
		//    IF (SIMPLE-EDIT-AND-PRIM-KEYS OF UBOC-COMM-INFO
		// COB_CODE: IF ((SIMPLE-EDIT-AND-PRIM-KEYS OF UBOC-COMM-INFO
		//             OR BYPASS-SIMPLE-EDIT-STEP OF UBOC-COMM-INFO)
		//               AND NOT FETCH-AND-SKIP-HIER-REQUEST OF UBOC-COMM-INFO)
		//             OR (WS-IUD-PROCESSED
		//                 AND NOT CASCADING-DELETE OF UBOC-COMM-INFO)
		//               END-IF
		//           END-IF.
		if ((dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isSimpleEditAndPrimKeys()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isBypassSimpleEditStep())
				&& !dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchAndSkipHierRequest()
				|| ws.getWsBdoSwitches().getIudProcessedSw().isWsIudProcessed()
						&& !dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isCascadingDelete()) {
			// COB_CODE: PERFORM 5000-PROCESS-HIER-TABLE
			processHierTable();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0000-RETURN-TO-FRONT-END
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
				returnToFrontEnd();
			}
		}
		// COB_CODE: IF WS-AUDIT-REQUIRED
		//               END-IF
		//           END-IF.
		if (ws.getWsBdoSwitches().getAuditTriggerSw().isWsAuditRequired()) {
			// COB_CODE: PERFORM 6000-AUDIT-TRAIL
			auditTrail();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0000-RETURN-TO-FRONT-END
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
				returnToFrontEnd();
			}
		}
	}

	/**Original name: 0000-RETURN-TO-FRONT-END<br>
	 * <pre>*   PERFORM 19999-TERMINATE.</pre>*/
	private void returnToFrontEnd() {
		// COB_CODE: PERFORM 9999-TERMINATE-MODULE.
		terminateModule();
		// COB_CODE: EXEC CICS
		//                RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 0200-INITIALIZE_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A) INITIALIZE THE UMT RECORD SEQ NBRS OF THE UOW RESPONSE
	 *     UMTS TO ZERO. THEY WILL BE INCREMENTED WHEN WRITTEN TO.
	 *  B) SET 'NO ERRORS' TO TRUE.
	 *  C) RETRIEVE THE SERIES 3 DATE FOR USE BY THE CURSORS.
	 *  D) SET DUMMY CHARACTERS USED TO DETERMINE IF FRONT-END HAS
	 *     CHANGED VALUE IN A FIELD.
	 *  E) PERFORM ANY OTHER BDO SPECIFIC INITIALIZATION.
	 * ***************************************************************</pre>*/
	private void initialize() {
		// COB_CODE: MOVE ZERO TO UDAT-REC-SEQ.
		ws.getHalludat().setRecSeq(0);
		// COB_CODE: SET WS-RESP-DATA-ROWS-NOT-WRIT TO TRUE.
		ws.getWsBdoSwitches().getRespDataRowsWritSw().setWsRespDataRowsNotWrit();
		// COB_CODE: SET WS-FIRST-RESP-DATA-REC TO TRUE.
		ws.getWsBdoSwitches().getRespDataRecordSw().setWsFirstRespDataRec();
		// COB_CODE: SET WS-IUD-NOT-PROCESSED TO TRUE.
		ws.getWsBdoSwitches().getIudProcessedSw().setWsIudNotProcessed();
		// COB_CODE: MOVE SPACES TO WS-DATA-UMT-AREA.
		ws.initWsDataUmtAreaSpaces();
		// COB_CODE: MOVE SPACES TO WS-HDR-UMT-AREA.
		ws.initWsHdrUmtAreaSpaces();
		// COB_CODE: INITIALIZE WS-MSG-UMT-AREA.
		initWsMsgUmtArea();
		//**  INITIALIZE WS-WARNING-UMT-AREA.
		//**  INITIALIZE WS-NLBE-UMT-AREA.
		//**  MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		//**  MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		//* INITIALISE UIDG-UNIT-NBR. THIS IS USED BY THE UNIVERSAL
		//* ID GENERATOR HALOUIDG TO ENSURE THAT DUPLICATE IDS
		//* AREN'T GENERATED IF IDS FOR THE SAME TYPE OF OBJECT ARE
		//* GENERATED IN QUICK SUCCESSION.
		// COB_CODE: MOVE 0 TO UIDG-UNIT-NBR.
		ws.getHalluidg().setUidgUnitNbr(((short) 0));
		// COB_CODE: SET READ-FOR-MSTR-SWT-NO TO TRUE.
		ws.getWsBdoSwitches().getReadForMstrSwtInd().setReadForMstrSwtNo();
		// COB_CODE: SET MASTER-SWITCH-NOT-SET TO TRUE.
		ws.getWsBdoSwitches().getMasterSwitchInd().setNotSet();
		// COB_CODE: PERFORM 0210-GET-CUR-TIMESTAMP.
		getCurTimestamp();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0200-INITIALIZE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0200-INITIALIZE-X
			return;
		}
		// COB_CODE: IF UBOC-APPLY-AUDITS
		//               END-IF
		//           ELSE
		//               SET WS-AUDIT-NOT-REQUIRED TO TRUE
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocAuditProcessingInfo().getApplyAuditsSw().isUbocApplyAudits()) {
			// COB_CODE: PERFORM 0250-CHECK-IF-AUDIT-TRIGGER
			checkIfAuditTrigger();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//             GO TO 0200-INITIALIZE-X
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0200-INITIALIZE-X
				return;
			}
		} else {
			// COB_CODE: SET WS-AUDIT-NOT-REQUIRED TO TRUE
			ws.getWsBdoSwitches().getAuditTriggerSw().setWsAuditNotRequired();
		}
		// COB_CODE: PERFORM 10200-INITIALIZE.
		initialize1();
	}

	/**Original name: 0210-GET-CUR-TIMESTAMP_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  GET CURRENT TIMESTAMP.
	 * ****************************************************************</pre>*/
	private void getCurTimestamp() {
		Xpiodat xpiodat = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF UBOC-EXCEED-TS-SET
		//               GO TO 0210-GET-CUR-TIMESTAMP-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocApplicationTsSw().isUbocExceedTsSet()) {
			// COB_CODE: MOVE UBOC-APPLICATION-TS TO WS-SE3-CUR-ISO-DATE-TIME
			ws.setWsSe3CurIsoDateTimeFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocApplicationTsFormatted());
			// COB_CODE: GO TO 0210-GET-CUR-TIMESTAMP-X
			return;
		}
		// COB_CODE: MOVE SPACES TO DATE-STRUCTURE.
		ws.getDateStructure().initDateStructureSpaces();
		// COB_CODE: MOVE 'C'    TO DATE-FUNCTION.
		ws.getDateStructure().setDateFunctionFormatted("C");
		// COB_CODE: MOVE SPACES TO DATE-LANGUAGE.
		ws.getDateStructure().setDateLanguage("");
		// COB_CODE: MOVE 'S3'   TO DATE-INP-FORMAT.
		ws.getDateStructure().setDateInpFormat("S3");
		// COB_CODE: MOVE 'B'    TO DATE-OUT-FORMAT.
		ws.getDateStructure().setDateOutFormat("B");
		// COB_CODE: MOVE SPACES TO DATE-INPUT.
		ws.getDateStructure().setDateInput("");
		//*************************************************************         ***
		//                                                            *         ...
		//    XPXCDAT - COPYBOOK USED TO LINK TO XPIODAT, THE ON-LINE *    04/11/01
		//              COBOL DATE ROUTINE.                           *    XPXCDAT
		//                                                            *       LV004
		// IR REF    DATE   RELEASE            DESCRIPTION            *    00006
		// XXXXXX  DDMMMYY    X.X    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX *    00007
		// XXXXXX  16JAN03  DNF     CHANGED TO R70 STRUTURE. FEDERATED*    00008
		// 002607  06JUL00  IND9176 ADDED CODE IN ORDER TO MAKE IT A  *    00008
		//                          DYNAMIC CALL TO XPIODAT.          *    00009
		//*************************************************************    00010
		//    MOVE LENGTH OF DATE-STRUCTURE TO DATE-STRUCTURE-LENGTH.      00012
		//**  EXEC CICS LINK                                               00014
		//**       PROGRAM  ('XPIODAT')                                    00015
		//**       COMMAREA (DATE-STRUCTURE)                               00016
		//**       LENGTH   (DATE-STRUCTURE-LENGTH)                        00017
		//**       NOHANDLE                                                00018
		//**       END-EXEC.                                               00019
		//**                                                               00020
		//    MOVE 'XPIODAT ' TO WS-XPIODAT-MODULE.                        00021
		//    CALL WS-XPIODAT-MODULE USING                                 00022
		//         DFHEIBLK                                                00023
		//         DFHCOMMAREA                                             00024
		//         DATE-STRUCTURE.                                         00025
		// COB_CODE: CALL 'XPIODAT' USING DATE-STRUCTURE.
		xpiodat = Xpiodat.getInstance();
		xpiodat.run(ws.getDateStructure());
		// COB_CODE: IF DATE-RETURN-CODE NOT EQUAL 'OK'
		//           OR DATE-OUTPUT EQUAL SPACES
		//               GO TO 0210-GET-CUR-TIMESTAMP-X
		//           END-IF.
		if (!Conditions.eq(ws.getDateStructure().getDateReturnCode(), "OK") || Characters.EQ_SPACE.test(ws.getDateStructure().getDateOutput())) {
			// COB_CODE: SET WS-LOG-ERROR       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-IAP-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalIapFailed();
			// COB_CODE: SET IAP-DATE-RETURN-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setIapDateReturnError();
			// COB_CODE: MOVE '0210-GET-CUR-TIMESTAMP' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0210-GET-CUR-TIMESTAMP");
			// COB_CODE: MOVE 'IAP DATE CALL FAILED' TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("IAP DATE CALL FAILED");
			// COB_CODE: STRING 'IAP RETURN CODE: '  DATE-RETURN-CODE ';'
			//                  'EXTENDED ERROR: '   DATE-EXTENDED-ERROR ';'
			//                  'OUTPUT: '           DATE-OUTPUT ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "IAP RETURN CODE: ", ws.getDateStructure().getDateReturnCodeFormatted(), ";", "EXTENDED ERROR: ",
							ws.getDateStructure().getScratchAreaRedef().getDateExtendedErrorFormatted(), ";", "OUTPUT: ",
							ws.getDateStructure().getDateOutputFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0210-GET-CUR-TIMESTAMP-X
			return;
		}
		// COB_CODE: MOVE DATE-OUTPUT TO WS-SE3-CUR-ISO-DATE-TIME.
		ws.setWsSe3CurIsoDateTimeFormatted(ws.getDateStructure().getDateOutputFormatted());
		// COB_CODE: MOVE DATE-OUTPUT TO UBOC-APPLICATION-TS.
		dfhcommarea.getUbocRecord().getCommInfo().setUbocApplicationTs(ws.getDateStructure().getDateOutput());
		// COB_CODE: SET UBOC-EXCEED-TS-SET TO TRUE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocApplicationTsSw().setUbocExceedTsSet();
	}

	/**Original name: 0230-GET-MAX-UMT-HDR-SEQ_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  OBTAIN MAX SEQ FROM RESPONSE HEADER.
	 * ****************************************************************</pre>*/
	private void getMaxUmtHdrSeq() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-UMT-HDR-MISSING TO TRUE.
		ws.getWsBdoSwitches().getUmtHdrSw().setMissing();
		// COB_CODE: MOVE WS-BUS-OBJ-NM TO UHDR-BUS-OBJ-NM.
		ws.getHalluhdr().setBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: MOVE UBOC-MSG-ID   TO UHDR-ID.
		ws.getHalluhdr().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: EXEC CICS
		//                READ FILE (UBOC-UOW-RESP-HEADER-STORE)
		//                INTO      (UHDR-COMMON)
		//                RIDFLD    (UHDR-KEY)
		//                KEYLENGTH (LENGTH OF UHDR-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Halluhdr.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalluhdr().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//                    WHEN DFHRESP(NORMAL)
		//                        MOVE UHDR-MSG-BUS-OBJ-COUNT TO UDAT-REC-SEQ
		//           ** IF NO HEADER RECORD IS FOUND, THEN SET THE WORKING STORAGE
		//           ** UP FOR ONE TO BE WRITTEN. THEN SET THE WORKING STORAGE.
		//                    WHEN DFHRESP(NOTFND)
		//                          TO UHDR-UOW-BUFFER-LENGTH
		//                    WHEN OTHER
		//                        GO TO 0230-GET-MAX-UMT-HDR-SEQ-X
		//                END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET WS-UMT-HDR-EXISTS TO TRUE
			ws.getWsBdoSwitches().getUmtHdrSw().setExists();
			// COB_CODE: MOVE UHDR-MSG-BUS-OBJ-COUNT TO UDAT-REC-SEQ
			ws.getHalludat().setRecSeqFormatted(ws.getHalluhdr().getMsgBusObjCountFormatted());
			//* IF NO HEADER RECORD IS FOUND, THEN SET THE WORKING STORAGE
			//* UP FOR ONE TO BE WRITTEN. THEN SET THE WORKING STORAGE.
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: MOVE ZERO          TO UHDR-MSG-BUS-OBJ-COUNT
			ws.getHalluhdr().setMsgBusObjCount(0);
			// COB_CODE: MOVE ZERO          TO UDAT-REC-SEQ
			ws.getHalludat().setRecSeq(0);
			// COB_CODE: MOVE UBOC-MSG-ID   TO UHDR-ID
			ws.getHalluhdr().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
			// COB_CODE: MOVE WS-BUS-OBJ-NM TO UHDR-MSG-BUS-OBJ-NM
			ws.getHalluhdr().setMsgBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
			// COB_CODE: PERFORM 10230-GET-MAX-UMT-HDR-SEQ
			getMaxUmtHdrSeq1();
			// COB_CODE: ADD LENGTH OF WS-BUS-OBJ-NM
			//             TO UHDR-MSG-BUS-OBJ-DATA-LEN
			ws.getHalluhdr().setMsgBusObjDataLen(((short) (WsSpecificMiscXz0d0008.Len.BUS_OBJ_NM + ws.getHalluhdr().getMsgBusObjDataLen())));
			// COB_CODE: MOVE LENGTH OF UHDR-UOW-MESSAGE-BUFFER
			//             TO UHDR-UOW-BUFFER-LENGTH
			ws.getHalluhdr().setUowBufferLength(((short) Halluhdr.Len.UOW_MESSAGE_BUFFER));
		} else {
			// COB_CODE: SET WS-LOG-ERROR                       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '0230-GET-MAX-UMT-HDR-SEQ'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0230-GET-MAX-UMT-HDR-SEQ");
			// COB_CODE: MOVE 'READ RESP HEADER UMT FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ RESP HEADER UMT FAILED");
			// COB_CODE: STRING 'UHDR-ID='         UHDR-ID         ';'
			//                  'UHDR-BUS-OBJ-NM=' UHDR-BUS-OBJ-NM ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "UHDR-ID=", ws.getHalluhdr().getIdFormatted(),
					";", "UHDR-BUS-OBJ-NM=", ws.getHalluhdr().getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0230-GET-MAX-UMT-HDR-SEQ-X
			return;
		}
	}

	/**Original name: 0250-CHECK-IF-AUDIT-TRIGGER_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CHECK IF AUDIT REQUIRED.
	 * ****************************************************************</pre>*/
	private void checkIfAuditTrigger() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-UOW-NAME TO HBAT-UOW-NM.
		ws.getDclhalBoAudTgrV().setUowNm(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE WS-BUS-OBJ-NM TO HBAT-BUS-OBJ-NM.
		ws.getDclhalBoAudTgrV().setBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: EXEC SQL
		//               SELECT HBAT_TGR_IND,
		//                      HBAT_MODULE_NM
		//               INTO  :HBAT-TGR-IND,
		//                     :HBAT-MODULE-NM
		//               FROM   HAL_BO_AUD_TGR_V
		//               WHERE UOW_NM           = :HBAT-UOW-NM
		//                 AND HBAT_BUS_OBJ_NM  = :HBAT-BUS-OBJ-NM
		//           END-EXEC.
		halBoAudTgrVDao.selectRec(ws.getDclhalBoAudTgrV().getUowNm(), ws.getDclhalBoAudTgrV().getBusObjNm(), ws.getDclhalBoAudTgrV());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   MOVE HBAT-TGR-IND TO WS-AUDIT-TRIGGER-SW
		//               WHEN ERD-SQL-NOT-FOUND
		//                   SET WS-AUDIT-NOT-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   GO TO 0250-CHECK-IF-AUDIT-TRIGGER-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: MOVE HBAT-TGR-IND TO WS-AUDIT-TRIGGER-SW
			ws.getWsBdoSwitches().getAuditTriggerSw().setAuditTriggerSw(ws.getDclhalBoAudTgrV().getTgrInd());
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-AUDIT-NOT-REQUIRED TO TRUE
			ws.getWsBdoSwitches().getAuditTriggerSw().setWsAuditNotRequired();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_BO_AUD_TGR_V'
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_AUD_TGR_V");
			// COB_CODE: MOVE '0250-CHECK-IF-AUDIT-TRIGGER'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0250-CHECK-IF-AUDIT-TRIGGER");
			// COB_CODE: MOVE 'SELECT FROM AUDIT TRIGGER TABLE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT FROM AUDIT TRIGGER TABLE FAILED");
			// COB_CODE: STRING 'HBAT-UOW-NM='     HBAT-UOW-NM     ';'
			//                  'HBAT-BUS-OBJ-NM=' HBAT-BUS-OBJ-NM ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "HBAT-UOW-NM=",
					ws.getDclhalBoAudTgrV().getUowNmFormatted(), ";", "HBAT-BUS-OBJ-NM=", ws.getDclhalBoAudTgrV().getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0250-CHECK-IF-AUDIT-TRIGGER-X
			return;
		}
	}

	/**Original name: 0300-PROCESS-REQ-MSG-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  SET UP FOR UMT READ IS IN INITIALIZATION - MOVED MSG ID AND
	 *  BUS OBJ NAME TO UMT COPYBOOK. THERE CAN BE MULTIPLE CALLS
	 *  TO THIS BUS OBJ ON THE MSG REQUEST UMT WHICH WILL VARY BY
	 *  SEQUENCE NUMBER.
	 * ****************************************************************</pre>*/
	private void processReqMsgUmt() {
		// COB_CODE: SET STAY-IN-MSG-UMT-LOOP  TO TRUE.
		ws.getWsBdoSwitches().getMsgUmtLoopSw().setStayInMsgUmtLoop();
		// COB_CODE: SET FIRST-READ-OF-REQ-UMT TO TRUE.
		ws.getWsBdoSwitches().getReqUmtReadSw().setFirstReadOfReqUmt();
		// COB_CODE: SET REQ-UMT-REC-NOT-FOUND TO TRUE.
		ws.getWsBdoSwitches().getReqUmtRecSw().setReqUmtRecNotFound();
		// COB_CODE: MOVE UBOC-MSG-ID   TO URQM-ID.
		ws.getUrqmCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE WS-BUS-OBJ-NM TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: PERFORM 0310-READ-REQ-MSG-UMT
		//               VARYING URQM-REC-SEQ
		//               FROM 1 BY 1
		//               UNTIL UBOC-HALT-AND-RETURN
		//                 OR EXIT-MSG-UMT-LOOP.
		ws.getUrqmCommon().setRecSeq(1);
		while (!(dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsBdoSwitches().getMsgUmtLoopSw().isExitMsgUmtLoop())) {
			readReqMsgUmt();
			ws.getUrqmCommon().setRecSeq(Trunc.toInt(ws.getUrqmCommon().getRecSeq() + 1, 5));
		}
	}

	/**Original name: 0310-READ-REQ-MSG-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ NEXT REQUEST UMT RECORD.
	 * ****************************************************************</pre>*/
	private void readReqMsgUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//                READ FILE (UBOC-UOW-REQ-MSG-STORE)
		//                INTO      (URQM-COMMON)
		//                RIDFLD    (URQM-KEY)
		//                KEYLENGTH (LENGTH OF URQM-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, UrqmCommon.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-EVALUATE
		//               WHEN DFHRESP(NOTFND)
		//                   SET EXIT-MSG-UMT-LOOP TO TRUE
		//               WHEN OTHER
		//                   GO TO 0310-READ-REQ-MSG-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET REQ-UMT-REC-FOUND TO TRUE
			ws.getWsBdoSwitches().getReqUmtRecSw().setReqUmtRecFound();
			// COB_CODE: SET SUBSEQUENT-READ-OF-REQ-UMT TO TRUE
			ws.getWsBdoSwitches().getReqUmtReadSw().setSubsequentReadOfReqUmt();
			// COB_CODE: PERFORM 10310-READ-REQ-MSG-UMT
			readReqMsgUmt1();
			// COB_CODE: SET WS-NOT-REQ-UMT-ROW-UPDATED TO TRUE
			ws.getWsBdoSwitches().getReqUmtRowUpdatedSw().setWsNotReqUmtRowUpdated();
			// COB_CODE: EVALUATE TRUE
			//               WHEN SIMPLE-EDIT-AND-PRIM-KEYS OF UBOC-COMM-INFO
			//               WHEN BYPASS-SIMPLE-EDIT-STEP OF UBOC-COMM-INFO
			//                   PERFORM 1000-INTTAB-ED-AND-PRIM-KEYS
			//               WHEN CASCADING-DELETE OF UBOC-COMM-INFO
			//                   PERFORM 0350-IGNORE-OTHER-REQUESTS
			//               WHEN OTHER
			//                   PERFORM 0320-PROCESS-UMT-REQUEST
			//           END-EVALUATE
			switch (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().getUbocPassThruAction()) {

			case UbocPassThruAction.SIMPLE_EDIT_AND_PRIM_KEYS:
			case UbocPassThruAction.BYPASS_SIMPLE_EDIT_STEP:// COB_CODE: PERFORM 1000-INTTAB-ED-AND-PRIM-KEYS
				inttabEdAndPrimKeys();
				break;

			case UbocPassThruAction.CASCADING_DELETE:// COB_CODE: PERFORM 0350-IGNORE-OTHER-REQUESTS
				ignoreOtherRequests();
				break;

			default:// COB_CODE: PERFORM 0320-PROCESS-UMT-REQUEST
				processUmtRequest();
				break;
			}
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: IF FIRST-READ-OF-REQ-UMT
			//               PERFORM 0330-PROCESS-PASSED-REQUEST
			//           END-IF
			if (ws.getWsBdoSwitches().getReqUmtReadSw().isFirstReadOfReqUmt()) {
				// COB_CODE: PERFORM 0330-PROCESS-PASSED-REQUEST
				processPassedRequest();
			}
			// COB_CODE: SET EXIT-MSG-UMT-LOOP TO TRUE
			ws.getWsBdoSwitches().getMsgUmtLoopSw().setExitMsgUmtLoop();
		} else {
			// COB_CODE: SET WS-LOG-ERROR                       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '0310-READ-REQ-MSG-UMT'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0310-READ-REQ-MSG-UMT");
			// COB_CODE: MOVE 'READ REQ MSG STORE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ REQ MSG STORE FAILED");
			// COB_CODE: STRING 'URQM-ID='      URQM-ID      ';'
			//                  'URQM-BUS-OBJ=' URQM-BUS-OBJ ';'
			//                  'URQM-REC-SEQ=' URQM-REC-SEQ ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "URQM-ID=", ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(),
							";", "URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0310-READ-REQ-MSG-UMT-X
			return;
		}
	}

	/**Original name: 0320-PROCESS-UMT-REQUEST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PROCESS UMT RECORD.
	 * ****************************************************************</pre>*/
	private void processUmtRequest() {
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN FILTER-ONLY-REQUEST OF URQM-COMMON
		//                        PERFORM 0340-BUILD-FILTERED-REQUEST
		//                    WHEN FETCH-DATA-REQUEST  OF URQM-COMMON
		//                        PERFORM 2000-PROCESS-FETCH-REQUEST
		//                    WHEN FETCH-ALL-DATA-REQUEST OF URQM-COMMON
		//                        PERFORM 2000-PROCESS-FETCH-REQUEST
		//                    WHEN FETCH-AND-SKIP-HIER-REQUEST OF URQM-COMMON
		//                        PERFORM 2000-PROCESS-FETCH-REQUEST
		//                    WHEN FETCH-WITH-EXACT-KEY-REQUEST OF URQM-COMMON
		//                        PERFORM 2000-PROCESS-FETCH-REQUEST
		//                    WHEN FETCH-WITH-EXACT-KEY-ISS-REQ OF URQM-COMMON
		//                        PERFORM 2000-PROCESS-FETCH-REQUEST
		//                    WHEN FETCH-WITH-EXACT-KEY-PND-REQ OF URQM-COMMON
		//                        PERFORM 2000-PROCESS-FETCH-REQUEST
		//                    WHEN FETCH-ISSUED-ROWS-ONLY-REQ OF URQM-COMMON
		//                        PERFORM 2000-PROCESS-FETCH-REQUEST
		//                    WHEN FETCH-PENDING-DATA-REQUEST  OF URQM-COMMON
		//                        PERFORM 2000-PROCESS-FETCH-REQUEST
		//                    WHEN INSERT-DATA-REQUEST OF URQM-COMMON
		//                      OR INSERT-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//                        PERFORM 2100-PROCESS-INS-REQ
		//                    WHEN UPDATE-DATA-REQUEST OF URQM-COMMON
		//                      OR UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//                        PERFORM 2200-PROCESS-UPD-REQ
		//                    WHEN CHANGE-DATA-REQUEST OF URQM-COMMON
		//                      OR CHANGE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//                        PERFORM 2200-PROCESS-UPD-REQ
		//                    WHEN DELETE-DATA-REQUEST OF URQM-COMMON
		//                        PERFORM 2300-PROCESS-DEL-REQ
		//                    WHEN IGNORE-AND-RETURN-REQUEST OF URQM-COMMON
		//                        PERFORM 2400-PROCESS-IGN-AND-RET-REQ
		//                    WHEN CORRECT-DATA-REQUEST OF URQM-COMMON
		//                      OR CORRECT-AND-RETURN-DATA-REQ OF URQM-COMMON
		//                        PERFORM 2200-PROCESS-UPD-REQ
		//                    WHEN IGNORE-REQUEST OF URQM-COMMON
		//                        PERFORM 2600-PROCESS-IGN-REQ
		//                    WHEN OTHER
		//                        PERFORM 2700-PROC-ADDL-ACTIONS-URQM
		//           *            SET WS-LOG-ERROR                            TO TRUE
		//           *            SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO  TO TRUE
		//           *            SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
		//           *            SET BUSP-INV-ACTION-CODE OF WS-ESTO-INFO    TO TRUE
		//           *            MOVE '0320-PROCESS-UMT-REQUEST'
		//           *              TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
		//           *            MOVE 'INVALID ACTION CODE'
		//           *              TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           *            STRING 'URQM-ACTION-CODE OF URQM-COMMON='
		//           *                   URQM-ACTION-CODE OF URQM-COMMON ';'
		//           *                DELIMITED BY SIZE
		//           *                INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
		//           *            PERFORM 9000-LOG-WARNING-OR-ERROR
		//           *            GO TO 0320-PROCESS-UMT-REQUEST-X
		//                    END-EVALUATE.
		if (ws.getUrqmCommon().getActionCode().isFilterOnlyRequest()) {
			// COB_CODE: SET FETCH-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchDataRequest();
			// COB_CODE: PERFORM 0340-BUILD-FILTERED-REQUEST
			buildFilteredRequest();
		} else if (ws.getUrqmCommon().getActionCode().isFetchDataRequest()) {
			// COB_CODE: SET FETCH-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchDataRequest();
			// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST
			processFetchRequest();
		} else if (ws.getUrqmCommon().getActionCode().isFetchAllDataRequest()) {
			// COB_CODE: SET FETCH-ALL-DATA-REQUEST OF UBOC-COMM-INFO
			//             TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchAllDataRequest();
			// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST
			processFetchRequest();
		} else if (ws.getUrqmCommon().getActionCode().isFetchAndSkipHierRequest()) {
			// COB_CODE: SET FETCH-AND-SKIP-HIER-REQUEST OF UBOC-COMM-INFO
			//             TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchAndSkipHierRequest();
			// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST
			processFetchRequest();
		} else if (ws.getUrqmCommon().getActionCode().isFetchWithExactKeyRequest()) {
			// COB_CODE: SET FETCH-WITH-EXACT-KEY-REQUEST OF UBOC-COMM-INFO
			//             TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchWithExactKeyRequest();
			// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST
			processFetchRequest();
		} else if (ws.getUrqmCommon().getActionCode().isFetchWithExactKeyIssReq()) {
			// COB_CODE: SET FETCH-WITH-EXACT-KEY-ISS-REQ OF UBOC-COMM-INFO
			//             TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchWithExactKeyIssReq();
			// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST
			processFetchRequest();
		} else if (ws.getUrqmCommon().getActionCode().isFetchWithExactKeyPndReq()) {
			// COB_CODE: SET FETCH-WITH-EXACT-KEY-PND-REQ OF UBOC-COMM-INFO
			//             TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchWithExactKeyPndReq();
			// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST
			processFetchRequest();
		} else if (ws.getUrqmCommon().getActionCode().isFetchIssuedRowsOnlyReq()) {
			// COB_CODE: SET FETCH-ISSUED-ROWS-ONLY-REQ OF UBOC-COMM-INFO
			//             TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchIssuedRowsOnlyReq();
			// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST
			processFetchRequest();
		} else if (ws.getUrqmCommon().getActionCode().isFetchPendingDataRequest()) {
			// COB_CODE: SET FETCH-PENDING-DATA-REQUEST
			//            OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchPendingDataRequest();
			// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST
			processFetchRequest();
		} else if (ws.getUrqmCommon().getActionCode().isInsertDataRequest() || ws.getUrqmCommon().getActionCode().isInsertAndReturnDataRequest()) {
			// COB_CODE: SET INSERT-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setInsertDataRequest();
			// COB_CODE: PERFORM 2100-PROCESS-INS-REQ
			processInsReq();
		} else if (ws.getUrqmCommon().getActionCode().isUpdateDataRequest() || ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest()) {
			// COB_CODE: SET UPDATE-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setUpdateDataRequest();
			// COB_CODE: PERFORM 2200-PROCESS-UPD-REQ
			processUpdReq();
		} else if (ws.getUrqmCommon().getActionCode().isChangeDataRequest() || ws.getUrqmCommon().getActionCode().isChangeAndReturnDataRequest()) {
			// COB_CODE: SET CHANGE-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setChangeDataRequest();
			// COB_CODE: PERFORM 2200-PROCESS-UPD-REQ
			processUpdReq();
		} else if (ws.getUrqmCommon().getActionCode().isDeleteDataRequest()) {
			// COB_CODE: SET DELETE-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setDeleteDataRequest();
			// COB_CODE: PERFORM 2300-PROCESS-DEL-REQ
			processDelReq();
		} else if (ws.getUrqmCommon().getActionCode().isIgnoreAndReturnRequest()) {
			// COB_CODE: PERFORM 2400-PROCESS-IGN-AND-RET-REQ
			processIgnAndRetReq();
		} else if (ws.getUrqmCommon().getActionCode().isCorrectDataRequest() || ws.getUrqmCommon().getActionCode().isCorrectAndReturnDataReq()) {
			// COB_CODE: SET CORRECT-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setCorrectDataRequest();
			// COB_CODE: PERFORM 2200-PROCESS-UPD-REQ
			processUpdReq();
		} else if (ws.getUrqmCommon().getActionCode().isIgnoreRequest()) {
			// COB_CODE: PERFORM 2600-PROCESS-IGN-REQ
			processIgnReq();
		} else {
			// COB_CODE: PERFORM 2700-PROC-ADDL-ACTIONS-URQM
			procAddlActionsUrqm();
			//            SET WS-LOG-ERROR                            TO TRUE
			//            SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO  TO TRUE
			//            SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
			//            SET BUSP-INV-ACTION-CODE OF WS-ESTO-INFO    TO TRUE
			//            MOVE '0320-PROCESS-UMT-REQUEST'
			//              TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//            MOVE 'INVALID ACTION CODE'
			//              TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			//            STRING 'URQM-ACTION-CODE OF URQM-COMMON='
			//                   URQM-ACTION-CODE OF URQM-COMMON ';'
			//                DELIMITED BY SIZE
			//                INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//            PERFORM 9000-LOG-WARNING-OR-ERROR
			//            GO TO 0320-PROCESS-UMT-REQUEST-X
		}
	}

	/**Original name: 0330-PROCESS-PASSED-REQUEST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  IF NO MESSAGE WAS PASSED IN THE REQUEST UMT, A VALID MESSAGE
	 *  SHOULD HAVE BEEN PASSED IN THRU LINKAGE.  IF THIS IS A REQUEST
	 *  TYPE SUCH AS UPDATE THAT DOES NOT REQUIRE ACTION ON THE PART OF
	 *  EVERY BDO IN THE HIERARCHY, THEN THIS BDO SHOULD PROCEED TO
	 *  CALL ITS SUBORDINATE BUSINESS OBJECTS.
	 * ****************************************************************</pre>*/
	private void processPassedRequest() {
		// COB_CODE: IF UBOC-APP-DATA-BUFFER EQUAL SPACES OR LOW-VALUES
		//               GO TO 0330-PROCESS-PASSED-REQUEST-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getAppDataBuffer())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getAppDataBuffer(), UbocRecord.Len.APP_DATA_BUFFER)) {
			// COB_CODE: SET WS-IUD-PROCESSED TO TRUE
			ws.getWsBdoSwitches().getIudProcessedSw().setWsIudProcessed();
			// COB_CODE: GO TO 0330-PROCESS-PASSED-REQUEST-X
			return;
		}
		//* IF FETCH CRITERIA WAS PASSED VIA UBOC LINKAGE, IT WILL ONLY
		//* CONTAIN THE MAIN KEY. MOVE THE PASSED IN MAIN KEY TO THE
		//* DCLGEN FIELD. SET THE EFFECTIVE AND EXPIRATION DATES.
		// COB_CODE: PERFORM 10330-PROCESS-PASSED-REQUEST.
		processPassedRequest1();
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN FETCH-DATA-REQUEST OF UBOC-COMM-INFO
		//                      OR FETCH-WITH-EXACT-KEY-REQUEST OF UBOC-COMM-INFO
		//                      OR FETCH-WITH-EXACT-KEY-ISS-REQ OF UBOC-COMM-INFO
		//                      OR FETCH-WITH-EXACT-KEY-PND-REQ OF UBOC-COMM-INFO
		//                      OR FETCH-ISSUED-ROWS-ONLY-REQ OF UBOC-COMM-INFO
		//                      OR FETCH-PENDING-DATA-REQUEST OF UBOC-COMM-INFO
		//                      OR FETCH-ALL-DATA-REQUEST OF UBOC-COMM-INFO
		//                      OR FETCH-AND-SKIP-HIER-REQUEST OF UBOC-COMM-INFO
		//                        PERFORM 2000-PROCESS-FETCH-REQUEST
		//                    WHEN CASCADING-DELETE OF UBOC-COMM-INFO
		//                        PERFORM 2300-PROCESS-DEL-REQ
		//                    WHEN OTHER
		//                        PERFORM 2705-PROC-ADDL-ACTIONS-UBOC
		//           *            SET WS-LOG-ERROR                            TO TRUE
		//           *            SET EFAL-BUS-LOGIC-FAILURE  OF WS-ESTO-INFO TO TRUE
		//           *            SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
		//           *            SET BUSP-INV-ACTION-CODE OF WS-ESTO-INFO    TO TRUE
		//           *            MOVE '0330-PROCESS-PASSED-REQUEST'
		//           *              TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
		//           *            MOVE 'INVALID ACTION CODE'
		//           *              TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           *            STRING 'UBOC-PASS-THRU-ACTION OF UBOC-COMM-INFO='
		//           *                   UBOC-PASS-THRU-ACTION OF UBOC-COMM-INFO ';'
		//           *                DELIMITED BY SIZE
		//           *                INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
		//           *            PERFORM 9000-LOG-WARNING-OR-ERROR
		//           *            GO TO 0330-PROCESS-PASSED-REQUEST-X
		//                END-EVALUATE.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyIssReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyPndReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchIssuedRowsOnlyReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchPendingDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchAllDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchAndSkipHierRequest()) {
			// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST
			processFetchRequest();
		} else if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isCascadingDelete()) {
			// COB_CODE: PERFORM 2300-PROCESS-DEL-REQ
			processDelReq();
		} else {
			// COB_CODE: PERFORM 2705-PROC-ADDL-ACTIONS-UBOC
			procAddlActionsUboc();
			//            SET WS-LOG-ERROR                            TO TRUE
			//            SET EFAL-BUS-LOGIC-FAILURE  OF WS-ESTO-INFO TO TRUE
			//            SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
			//            SET BUSP-INV-ACTION-CODE OF WS-ESTO-INFO    TO TRUE
			//            MOVE '0330-PROCESS-PASSED-REQUEST'
			//              TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//            MOVE 'INVALID ACTION CODE'
			//              TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			//            STRING 'UBOC-PASS-THRU-ACTION OF UBOC-COMM-INFO='
			//                   UBOC-PASS-THRU-ACTION OF UBOC-COMM-INFO ';'
			//                DELIMITED BY SIZE
			//                INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//            PERFORM 9000-LOG-WARNING-OR-ERROR
			//            GO TO 0330-PROCESS-PASSED-REQUEST-X
		}
	}

	/**Original name: 0340-BUILD-FILTERED-REQUEST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  IF ONLY FILTER INFO WAS PASSED IN THE UMT, THE PRIMARY KEY
	 *  INFO SHOULD HAVE BEEN PASSED IN THRU LINKAGE. VERIFY THAT
	 *  THE FILTER INFO IS VALID. IF IT IS VALID, BUILD THE FETCH
	 *  REQUEST BASED ON THE PASSED IN PRIMARY KEY INFO AND THE
	 *  FILTER INFO.
	 * ****************************************************************</pre>*/
	private void buildFilteredRequest() {
		// COB_CODE: IF (UBOC-APP-DATA-BUFFER EQUAL SPACES OR LOW-VALUES)
		//             AND (URQM-REC-SEQ = 1)
		//               GO TO 0340-BUILD-FILTERED-REQUEST-X
		//           END-IF.
		if ((Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getAppDataBuffer())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getAppDataBuffer(), UbocRecord.Len.APP_DATA_BUFFER))
				&& ws.getUrqmCommon().getRecSeq() == 1) {
			// COB_CODE: SET WS-LOG-ERROR                             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-UOW-DATA-BUFF-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspUowDataBuffBlank();
			// COB_CODE: MOVE '0340-BUILD-FILTERED-REQUEST'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0340-BUILD-FILTERED-REQUEST");
			// COB_CODE: MOVE 'HALLUBOC DATA BUFFER IS EMPTY.'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("HALLUBOC DATA BUFFER IS EMPTY.");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0340-BUILD-FILTERED-REQUEST-X
			return;
		}
		// COB_CODE: PERFORM 10340-BUILD-FILTERED-REQUEST.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=1320, because the code is unreachable.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0340-BUILD-FILTERED-REQUEST-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0340-BUILD-FILTERED-REQUEST-X
			return;
		}
		//* CALL FETCH ROUTINE.
		// COB_CODE: PERFORM 2000-PROCESS-FETCH-REQUEST.
		processFetchRequest();
	}

	/**Original name: 0350-IGNORE-OTHER-REQUESTS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  IF THE REQUEST MESSAGE CONTAINS ANOTHER ROW FOR THIS KEY,
	 *  SET ITS ACTION TO IGNORE AND REWRITE THE UMT ROW.
	 * ****************************************************************</pre>*/
	private void ignoreOtherRequests() {
		// COB_CODE: SET WS-REQ-UMT-ROW-UPDATED TO TRUE.
		ws.getWsBdoSwitches().getReqUmtRowUpdatedSw().setWsReqUmtRowUpdated();
		// COB_CODE: SET WS-DATA-CHANGED TO TRUE.
		ws.getWsBdoSwitches().getDataChangedSw().setWsDataChanged();
		// COB_CODE: PERFORM 10350-CHECK-KEYS-FOR-CASCADE.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=1345, because the code is unreachable.
		// COB_CODE: IF WS-NOT-REQ-UMT-ROW-UPDATED
		//              GO TO 0350-IGNORE-OTHER-REQUESTS-X
		//           END-IF.
		if (ws.getWsBdoSwitches().getReqUmtRowUpdatedSw().isWsNotReqUmtRowUpdated()) {
			// COB_CODE: GO TO 0350-IGNORE-OTHER-REQUESTS-X
			return;
		}
		// COB_CODE: PERFORM 1860-ITE-UPDATE-MESSAGE-UMT.
		iteUpdateMessageUmt();
	}

	/**Original name: 1000-INTTAB-ED-AND-PRIM-KEYS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF INTRA-TABLE EDITS.
	 *  THIS PROCESSING CONSISTS OF MORE THAN BASIC EDITS.
	 * ****************************************************************</pre>*/
	private void inttabEdAndPrimKeys() {
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN INSERT-DATA-REQUEST OF URQM-COMMON
		//                      OR INSERT-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//                        PERFORM 1100-ITE-INSERT
		//                    WHEN UPDATE-DATA-REQUEST OF URQM-COMMON
		//                      OR UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//                      OR CHANGE-DATA-REQUEST OF URQM-COMMON
		//                      OR CHANGE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//                      OR CORRECT-DATA-REQUEST OF URQM-COMMON
		//                      OR CORRECT-AND-RETURN-DATA-REQ OF URQM-COMMON
		//                        PERFORM 1200-ITE-UPDATE
		//                    WHEN DELETE-DATA-REQUEST OF URQM-COMMON
		//                        PERFORM 1300-ITE-DELETE
		//                    WHEN IGNORE-REQUEST OF URQM-COMMON
		//                      OR IGNORE-AND-RETURN-REQUEST OF URQM-COMMON
		//                      OR FETCH-DATA-REQUEST OF URQM-COMMON
		//                      OR FETCH-WITH-EXACT-KEY-REQUEST OF URQM-COMMON
		//                      OR FETCH-WITH-EXACT-KEY-ISS-REQ OF URQM-COMMON
		//                      OR FETCH-WITH-EXACT-KEY-PND-REQ OF URQM-COMMON
		//                      OR FETCH-ISSUED-ROWS-ONLY-REQ OF URQM-COMMON
		//                      OR FETCH-PENDING-DATA-REQUEST OF URQM-COMMON
		//                      OR FETCH-ALL-DATA-REQUEST OF URQM-COMMON
		//                      OR FETCH-AND-SKIP-HIER-REQUEST OF URQM-COMMON
		//                        CONTINUE
		//                    WHEN OTHER
		//                        PERFORM 1400-ITE-ADDL-ACTIONS
		//           *            SET WS-LOG-ERROR                            TO TRUE
		//           *            SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO  TO TRUE
		//           *            SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
		//           *            SET BUSP-INV-ACTION-CODE OF WS-ESTO-INFO    TO TRUE
		//           *            MOVE '1000-INTTAB-ED-AND-PRIM-KEYS'
		//           *              TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
		//           *            MOVE 'INVALID ACTION CODE'
		//           *              TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           *            STRING 'URQM-ACTION-CODE OF URQM-COMMON='
		//           *                   URQM-ACTION-CODE OF URQM-COMMON ';'
		//           *              DELIMITED BY SIZE
		//           *              INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
		//           *            PERFORM 9000-LOG-WARNING-OR-ERROR
		//           *            GO TO 1000-INTTAB-ED-AND-PRIM-KEYS-X
		//                END-EVALUATE.
		if (ws.getUrqmCommon().getActionCode().isInsertDataRequest() || ws.getUrqmCommon().getActionCode().isInsertAndReturnDataRequest()) {
			// COB_CODE: PERFORM 1100-ITE-INSERT
			iteInsert();
		} else if (ws.getUrqmCommon().getActionCode().isUpdateDataRequest() || ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isChangeDataRequest() || ws.getUrqmCommon().getActionCode().isChangeAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isCorrectDataRequest() || ws.getUrqmCommon().getActionCode().isCorrectAndReturnDataReq()) {
			// COB_CODE: PERFORM 1200-ITE-UPDATE
			iteUpdate();
		} else if (ws.getUrqmCommon().getActionCode().isDeleteDataRequest()) {
			// COB_CODE: PERFORM 1300-ITE-DELETE
			iteDelete();
		} else if (ws.getUrqmCommon().getActionCode().isIgnoreRequest() || ws.getUrqmCommon().getActionCode().isIgnoreAndReturnRequest()
				|| ws.getUrqmCommon().getActionCode().isFetchDataRequest() || ws.getUrqmCommon().getActionCode().isFetchWithExactKeyRequest()
				|| ws.getUrqmCommon().getActionCode().isFetchWithExactKeyIssReq() || ws.getUrqmCommon().getActionCode().isFetchWithExactKeyPndReq()
				|| ws.getUrqmCommon().getActionCode().isFetchIssuedRowsOnlyReq() || ws.getUrqmCommon().getActionCode().isFetchPendingDataRequest()
				|| ws.getUrqmCommon().getActionCode().isFetchAllDataRequest() || ws.getUrqmCommon().getActionCode().isFetchAndSkipHierRequest()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: PERFORM 1400-ITE-ADDL-ACTIONS
			iteAddlActions();
			//            SET WS-LOG-ERROR                            TO TRUE
			//            SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO  TO TRUE
			//            SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
			//            SET BUSP-INV-ACTION-CODE OF WS-ESTO-INFO    TO TRUE
			//            MOVE '1000-INTTAB-ED-AND-PRIM-KEYS'
			//              TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//            MOVE 'INVALID ACTION CODE'
			//              TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			//            STRING 'URQM-ACTION-CODE OF URQM-COMMON='
			//                   URQM-ACTION-CODE OF URQM-COMMON ';'
			//              DELIMITED BY SIZE
			//              INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//            PERFORM 9000-LOG-WARNING-OR-ERROR
			//            GO TO 1000-INTTAB-ED-AND-PRIM-KEYS-X
		}
	}

	/**Original name: 1100-ITE-INSERT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF INTRA-TABLE EDIT IN INSERT MODE.
	 * ****************************************************************</pre>*/
	private void iteInsert() {
		// COB_CODE: PERFORM 1110-BUILD-PRIM-KEY-REPL-VALS.
		buildPrimKeyReplVals();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1100-ITE-INSERT-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1100-ITE-INSERT-X
			return;
		}
		// COB_CODE: IF UBOC-APPLY-DATA-PRIV
		//               PERFORM 7000-CHECK-AUTHORIZATION
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().isUbocApplyDataPriv()) {
			// COB_CODE: PERFORM 7000-CHECK-AUTHORIZATION
			rng7000CheckAuthorization();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1100-ITE-INSERT-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1100-ITE-INSERT-X
			return;
		}
		// COB_CODE: PERFORM 1840-SIMPLE-INTERNAL-EDITS.
		simpleInternalEdits();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1100-ITE-INSERT-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1100-ITE-INSERT-X
			return;
		}
		// COB_CODE: PERFORM 1860-ITE-UPDATE-MESSAGE-UMT.
		iteUpdateMessageUmt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1100-ITE-INSERT-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1100-ITE-INSERT-X
			return;
		}
	}

	/**Original name: 1110-BUILD-PRIM-KEY-REPL-VALS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DETERMINE IF THE PRIMARY KEY(S) HAVE BEEN SUPPLIED BY THE
	 *  FRONT-END, BUILT BY A PARENT, OR IF THIS MODULE NEEDS TO
	 *  GENERATE THEM. IF THIS MODULE NEEDS TO GENERATE PRIMARY
	 *  KEY(S) THEN PERFORM PROCESSING TO GENERATE THEM AND THEN
	 *  STORE ON KEY REPLACEMENT UMT.
	 * ****************************************************************</pre>*/
	private void buildPrimKeyReplVals() {
		// COB_CODE: PERFORM 11110-BLD-PRIM-KEY-REPL-VALS.
		bldPrimKeyReplVals();
		// COB_CODE: MOVE SPACES TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE ZERO   TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) 0));
	}

	/**Original name: 1200-ITE-UPDATE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF INTRA-TABLE EDIT IN UPDATE MODE.
	 * ****************************************************************</pre>*/
	private void iteUpdate() {
		// COB_CODE: SET WS-NOT-REALLY-IS-A-DELETE TO TRUE.
		ws.getWsBdoSwitches().getReallyIsADeleteSw().setWsNotReallyIsADelete();
		// COB_CODE: PERFORM 1210-REALLY-IS-A-DELETE.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=1479, because the code is unreachable.
		// COB_CODE: IF WS-REALLY-IS-A-DELETE
		//               GO TO 1200-ITE-UPDATE-X
		//           END-IF.
		if (ws.getWsBdoSwitches().getReallyIsADeleteSw().isWsReallyIsADelete()) {
			// COB_CODE: SET WS-REQ-UMT-ROW-UPDATED TO TRUE
			ws.getWsBdoSwitches().getReqUmtRowUpdatedSw().setWsReqUmtRowUpdated();
			// COB_CODE: PERFORM 1300-ITE-DELETE
			iteDelete();
			// COB_CODE: GO TO 1200-ITE-UPDATE-X
			return;
		}
		// COB_CODE: IF UBOC-APPLY-DATA-PRIV
		//               PERFORM 7000-CHECK-AUTHORIZATION
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().isUbocApplyDataPriv()) {
			// COB_CODE: PERFORM 7000-CHECK-AUTHORIZATION
			rng7000CheckAuthorization();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1200-ITE-UPDATE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1200-ITE-UPDATE-X
			return;
		}
		// COB_CODE: PERFORM 1820-ITE-READ-EXISTING-ROW.
		iteReadExistingRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1200-ITE-UPDATE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1200-ITE-UPDATE-X
			return;
		}
		// COB_CODE: PERFORM 1220-ITE-REPL-RETAINED-VALS.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=1498, because the code is unreachable.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1200-ITE-UPDATE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1200-ITE-UPDATE-X
			return;
		}
		// COB_CODE: PERFORM 1230-ITE-HAS-DATA-CHANGED.
		iteHasDataChanged();
		// COB_CODE: IF WS-DATA-NOT-CHANGED
		//               PERFORM 1240-DATA-NOT-CHANGED
		//           ELSE
		//               PERFORM 1840-SIMPLE-INTERNAL-EDITS
		//           END-IF.
		if (ws.getWsBdoSwitches().getDataChangedSw().isWsDataNotChanged()) {
			// COB_CODE: PERFORM 1240-DATA-NOT-CHANGED
			dataNotChanged();
		} else {
			// COB_CODE: PERFORM 1840-SIMPLE-INTERNAL-EDITS
			simpleInternalEdits();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1200-ITE-UPDATE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1200-ITE-UPDATE-X
			return;
		}
		// COB_CODE: PERFORM 1860-ITE-UPDATE-MESSAGE-UMT.
		iteUpdateMessageUmt();
	}

	/**Original name: 1230-ITE-HAS-DATA-CHANGED_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DETERMINE IF THE DATA HAS BEEN CHANGED BY THE FRONT-END
	 *  BY COMPARING DATA SENT FROM FRONT-END WITH DATA FROM DATABASE.
	 *  RETAINED VALUES WILL ALREADY BE SUBSTITUTED BY NOW.
	 * ****************************************************************</pre>*/
	private void iteHasDataChanged() {
		// COB_CODE: SET WS-DATA-NOT-CHANGED TO TRUE.
		ws.getWsBdoSwitches().getDataChangedSw().setWsDataNotChanged();
		// COB_CODE: PERFORM 11230-ITE-HAS-DATA-CHANGED.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=1571, because the code is unreachable.
	}

	/**Original name: 1240-DATA-NOT-CHANGED_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  UPDATE PROCESSING IF NO DATA HAS CHANGED.
	 * ****************************************************************</pre>*/
	private void dataNotChanged() {
		// COB_CODE: SET WS-REQ-UMT-ROW-UPDATED TO TRUE.
		ws.getWsBdoSwitches().getReqUmtRowUpdatedSw().setWsReqUmtRowUpdated();
	}

	/**Original name: 1300-ITE-DELETE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF INTRA-TABLE EDIT IN DELETE MODE.
	 * ****************************************************************</pre>*/
	private void iteDelete() {
		// COB_CODE: PERFORM 1840-SIMPLE-INTERNAL-EDITS.
		simpleInternalEdits();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1300-ITE-DELETE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1300-ITE-DELETE-X
			return;
		}
		// COB_CODE: PERFORM 1820-ITE-READ-EXISTING-ROW.
		iteReadExistingRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1300-ITE-DELETE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1300-ITE-DELETE-X
			return;
		}
		// COB_CODE: PERFORM 1860-ITE-UPDATE-MESSAGE-UMT.
		iteUpdateMessageUmt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1300-ITE-DELETE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1300-ITE-DELETE-X
			return;
		}
	}

	/**Original name: 1400-ITE-ADDL-ACTIONS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CALL APPLICATION-SPECIFIC SECTION THAT RESIDES IN THE
	 *  SKELETONS TO PROCESSING THE INCOMING ACTION.  HALCDPDC CAN
	 *  ONLY INTERPRET THE BASIC ACTIONS AND THEIR NEED FOR SIMPLE
	 *  EDIT PROCESSING.  HOWEVER, APPLICATIONS MAY HAVE A NEED FOR
	 *  NON-STANDARD ACTION REQUESTS.  THESE REQUESTS MUST BE
	 *  ADDRESSED WITHIN THE BDOS OF EACH APPLICATION.
	 * ****************************************************************</pre>*/
	private void iteAddlActions() {
		// COB_CODE: PERFORM 11400-ITE-ADDL-ACTIONS.
		iteAddlActions1();
	}

	/**Original name: 1820-ITE-READ-EXISTING-ROW_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ EXISTING ROW FROM DATABASE.
	 * ****************************************************************</pre>*/
	private void iteReadExistingRow() {
		// COB_CODE: PERFORM 2820-READ-ROW.
		readRow();
	}

	/**Original name: 1840-SIMPLE-INTERNAL-EDITS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PERFORM SIMPLE EDITING ON EACH FIELD OF OBJECT BEING PROCESSED.
	 * ****************************************************************</pre>*/
	private void simpleInternalEdits() {
		// COB_CODE: IF BYPASS-SIMPLE-EDIT-STEP OF UBOC-COMM-INFO
		//               GO TO 1840-SIMPLE-INTERNAL-EDITS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isBypassSimpleEditStep()) {
			// COB_CODE: GO TO 1840-SIMPLE-INTERNAL-EDITS-X
			return;
		}
		// COB_CODE: PERFORM 11840-SIMPLE-INTERNAL-EDITS.
		simpleInternalEdits1();
	}

	/**Original name: 1860-ITE-UPDATE-MESSAGE-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  IF THE REQUEST UMT ROW BEING PROCESSED HAS BEEN UPDATED,
	 *  THEN WRITE UPDATED ROW BACK TO REQUEST UMT.
	 * ****************************************************************
	 * * CHECK IF REWRITE OF REQUEST UMT ROW IS REQUIRED.</pre>*/
	private void iteUpdateMessageUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF WS-NOT-REQ-UMT-ROW-UPDATED
		//               GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
		//           END-IF.
		if (ws.getWsBdoSwitches().getReqUmtRowUpdatedSw().isWsNotReqUmtRowUpdated()) {
			// COB_CODE: GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
			return;
		}
		//* REREAD THE REQUEST UMT ROW TO GET UPDATE INTENT.
		// COB_CODE: EXEC CICS
		//                READ FILE (UBOC-UOW-REQ-MSG-STORE)
		//                INTO      (URQM-COMMON)
		//                RIDFLD    (URQM-KEY)
		//                KEYLENGTH (LENGTH OF URQM-KEY)
		//                UPDATE
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, UrqmCommon.Len.KEY, true);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR                       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '1860-ITE-UPDATE-MESSAGE-UMT'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1860-ITE-UPDATE-MESSAGE-UMT");
			// COB_CODE: MOVE 'RE-READ REQ MSG STORE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("RE-READ REQ MSG STORE FAILED");
			// COB_CODE: STRING 'URQM-ID='      URQM-ID      ';'
			//                  'URQM-BUS-OBJ=' URQM-BUS-OBJ ';'
			//                  'URQM-REC-SEQ=' URQM-REC-SEQ ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "URQM-ID=", ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(),
							";", "URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
			return;
		}
		//* REWRITE THE REQUEST UMT ROW.
		// COB_CODE: IF (UPDATE-DATA-REQUEST OF URQM-COMMON
		//             OR UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CHANGE-DATA-REQUEST OF URQM-COMMON
		//             OR CHANGE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CORRECT-DATA-REQUEST OF URQM-COMMON
		//             OR CORRECT-AND-RETURN-DATA-REQ OF URQM-COMMON)
		//             AND WS-REALLY-IS-A-DELETE
		//               SET DELETE-DATA-REQUEST OF URQM-COMMON TO TRUE
		//           END-IF.
		if ((ws.getUrqmCommon().getActionCode().isUpdateDataRequest() || ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isChangeDataRequest() || ws.getUrqmCommon().getActionCode().isChangeAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isCorrectDataRequest() || ws.getUrqmCommon().getActionCode().isCorrectAndReturnDataReq())
				&& ws.getWsBdoSwitches().getReallyIsADeleteSw().isWsReallyIsADelete()) {
			// COB_CODE: SET DELETE-DATA-REQUEST OF URQM-COMMON TO TRUE
			ws.getUrqmCommon().getActionCode().setDeleteDataRequest();
		}
		// COB_CODE: IF (UPDATE-DATA-REQUEST OF URQM-COMMON
		//             OR CHANGE-DATA-REQUEST OF URQM-COMMON
		//             OR CORRECT-DATA-REQUEST OF URQM-COMMON)
		//             AND WS-DATA-NOT-CHANGED
		//               SET IGNORE-REQUEST OF URQM-COMMON TO TRUE
		//           END-IF.
		if ((ws.getUrqmCommon().getActionCode().isUpdateDataRequest() || ws.getUrqmCommon().getActionCode().isChangeDataRequest()
				|| ws.getUrqmCommon().getActionCode().isCorrectDataRequest()) && ws.getWsBdoSwitches().getDataChangedSw().isWsDataNotChanged()) {
			// COB_CODE: SET IGNORE-REQUEST OF URQM-COMMON TO TRUE
			ws.getUrqmCommon().getActionCode().setIgnoreRequest();
		}
		// COB_CODE: IF (UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CHANGE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CORRECT-AND-RETURN-DATA-REQ OF URQM-COMMON)
		//             AND WS-DATA-NOT-CHANGED
		//               SET IGNORE-AND-RETURN-REQUEST OF URQM-COMMON TO TRUE
		//           END-IF.
		if ((ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest() || ws.getUrqmCommon().getActionCode().isChangeAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isCorrectAndReturnDataReq()) && ws.getWsBdoSwitches().getDataChangedSw().isWsDataNotChanged()) {
			// COB_CODE: SET IGNORE-AND-RETURN-REQUEST OF URQM-COMMON TO TRUE
			ws.getUrqmCommon().getActionCode().setIgnoreAndReturnRequest();
		}
		// COB_CODE: IF CASCADING-DELETE OF UBOC-COMM-INFO
		//               SET IGNORE-REQUEST OF URQM-COMMON TO TRUE
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isCascadingDelete()) {
			// COB_CODE: SET IGNORE-REQUEST OF URQM-COMMON TO TRUE
			ws.getUrqmCommon().getActionCode().setIgnoreRequest();
		}
		// COB_CODE: PERFORM 11860-ITE-UPDATE-MESSAGE-UMT.
		iteUpdateMessageUmt1();
		//* CHECK IF REWRITE OF REQUEST UMT ROW IS STILL REQUIRED
		//* AFTER CALL TO 11860-.
		// COB_CODE: IF WS-NOT-REQ-UMT-ROW-UPDATED
		//              GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
		//           END-IF.
		if (ws.getWsBdoSwitches().getReqUmtRowUpdatedSw().isWsNotReqUmtRowUpdated()) {
			// COB_CODE: EXEC CICS
			//                UNLOCK
			//                FILE   (UBOC-UOW-REQ-MSG-STORE)
			//                RESP   (WS-RESPONSE-CODE)
			//                RESP2  (WS-RESPONSE-CODE2)
			//           END-EXEC
			iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStoreFormatted());
			if (iRowDAO != null) {
				iRowDAO.unlock();
			}
			ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
			ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
			// COB_CODE: EVALUATE WS-RESPONSE-CODE
			//               WHEN DFHRESP(NORMAL)
			//                   CONTINUE
			//               WHEN OTHER
			//                   GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
			//           END-EVALUATE
			if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
				// COB_CODE: CONTINUE
				//continue
			} else {
				// COB_CODE: SET WS-LOG-ERROR TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
				// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
				// COB_CODE: SET ETRA-CICS-FUNCTION OF WS-ESTO-INFO TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsFunction();
				// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
				//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore());
				// COB_CODE: MOVE '1860-ITE-UPDATE-MESSAGE-UMT'
				//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1860-ITE-UPDATE-MESSAGE-UMT");
				// COB_CODE: MOVE 'UNLOCK REQ MSG STORE FAILED'
				//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UNLOCK REQ MSG STORE FAILED");
				// COB_CODE: STRING 'UMT='
				//                   UBOC-UOW-REQ-MSG-STORE ';'
				//                  'URQM-ID='      URQM-ID      ';'
				//                  'URQM-BUS-OBJ=' URQM-BUS-OBJ ';'
				//                  'URQM-REC-SEQ=' URQM-REC-SEQ ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
						new String[] { "UMT=", dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStoreFormatted(), ";", "URQM-ID=",
								ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(), ";",
								"URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
				return;
			}
			// COB_CODE: GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
			return;
		}
		// COB_CODE: EXEC CICS
		//                REWRITE
		//                FILE   (UBOC-UOW-REQ-MSG-STORE)
		//                FROM   (URQM-COMMON)
		//                LENGTH (LENGTH OF URQM-COMMON)
		//                RESP   (WS-RESPONSE-CODE)
		//                RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getUrqmCommon().getUrqmCommonBytes());
			iRowDAO.update(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR                          TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-REWRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsRewriteUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '1860-ITE-UPDATE-MESSAGE-UMT'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1860-ITE-UPDATE-MESSAGE-UMT");
			// COB_CODE: MOVE 'REWRITE REQ MSG STORE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("REWRITE REQ MSG STORE FAILED");
			// COB_CODE: STRING 'URQM-ID='      URQM-ID      ';'
			//                  'URQM-BUS-OBJ=' URQM-BUS-OBJ ';'
			//                  'URQM-REC-SEQ=' URQM-REC-SEQ ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "URQM-ID=", ws.getUrqmCommon().getIdFormatted(), ";", "URQM-BUS-OBJ=", ws.getUrqmCommon().getBusObjFormatted(),
							";", "URQM-REC-SEQ=", ws.getUrqmCommon().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1860-ITE-UPDATE-MESSAGE-UMT-X
			return;
		}
	}

	/**Original name: 2000-PROCESS-FETCH-REQUEST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DETERMINE APPROPRIATE FETCH CURSOR.
	 *  CALL SECTION THAT UTILIZES CURSOR.
	 * ****************************************************************</pre>*/
	private void processFetchRequest() {
		// COB_CODE: PERFORM 2005-BUILD-SEARCH.
		buildSearch();
		// COB_CODE: PERFORM 2010-PICK-FETCH-CURSOR.
		pickFetchCursor();
		// COB_CODE: PERFORM 2015-PROCESS-FETCH-REQUEST.
		processFetchRequest1();
	}

	/**Original name: 2005-BUILD-SEARCH_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  BUILD SEARCH.
	 * ****************************************************************</pre>*/
	private void buildSearch() {
		// COB_CODE: PERFORM 12005-BUILD-SEARCH.
		buildSearch1();
	}

	/**Original name: 2010-PICK-FETCH-CURSOR_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PICK FETCH CURSOR.
	 * ****************************************************************</pre>*/
	private void pickFetchCursor() {
		// COB_CODE: PERFORM 12010-PICK-FETCH-CURSOR.
		pickFetchCursor1();
	}

	/**Original name: 2015-PROCESS-FETCH-REQUEST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PROCESS FETCH REQUEST.
	 * ****************************************************************</pre>*/
	private void processFetchRequest1() {
		// COB_CODE: PERFORM 12015-PROCESS-FETCH-REQUEST.
		processFetchRequest2();
	}

	/**Original name: 2100-PROCESS-INS-REQ_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF AN INSERT REQUEST.
	 * ****************************************************************</pre>*/
	private void processInsReq() {
		// COB_CODE: SET WS-IUD-PROCESSED TO TRUE.
		ws.getWsBdoSwitches().getIudProcessedSw().setWsIudProcessed();
		// COB_CODE: PERFORM 2110-SETUP-KEYS.
		setupKeys();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-PROCESS-INS-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-PROCESS-INS-REQ-X
			return;
		}
		// COB_CODE: PERFORM 2120-CHECK-PARENTS-EXIST.
		checkParentsExist();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-PROCESS-INS-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-PROCESS-INS-REQ-X
			return;
		}
		// COB_CODE: PERFORM 2830-SETUP-DATA.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=1914, because the code is unreachable.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-PROCESS-INS-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-PROCESS-INS-REQ-X
			return;
		}
		// COB_CODE: PERFORM 2130-INS-ROW-TO-INS.
		insRowToIns();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-PROCESS-INS-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-PROCESS-INS-REQ-X
			return;
		}
		// AN INSERT REQUEST WILL EXIT THE PROCESSING BEFORE WRITING THE
		// RECORD TO THE RESPONSE UMT.
		//    IF INSERT-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		// COB_CODE: PERFORM 2140-WRI-TO-RESP-UMT-AFT-INS.
		wriToRespUmtAftIns();
		//    END-IF.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-PROCESS-INS-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-PROCESS-INS-REQ-X
			return;
		}
	}

	/**Original name: 2110-SETUP-KEYS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  SETS UP ALL THE KEYS FOR THE ROW PRIOR TO INSERTION.
	 *  KEY COMPONENTS MAY BE SOURCED AS FOLLOWS:
	 *  A. THE KEY MAY SIMPLY BE SUPPLIED BY THE FRONT-END.
	 *  B. THE KEY MAY NEED TO BE GENERATED BY THIS DATA OBJECT
	 *      (I.E. A SEQUENCE NUMBER).
	 *  C. IF THE KEY COMPONENT IS UNKNOWN BY THE FRONT-END BECAUSE
	 *      IT IS ASSOCIATED WITH AN OBJECT THAT IS TO BE INSERTED,
	 *      THEN THE KEY IS GENERATED BY THE OWNING OBJECT IN THE
	 *      BACK-END AND STORED BY A 'KEY REPLACEMENT' STORE
	 *      PROCESS. IT IS THEN RETRIEVED TO BE USED IN THIS SECTION
	 *      BY A CORRESPONDING 'KEY REPLACEMENT' RETRIEVE PROCESS.
	 *      FOR (C) SCENARIO ABOVE, IF THE KEY IS A PRIMARY KEY
	 *      THEN THIS PROCESS WILL TAKE PLACE DURING 'SIMPLE EDIT'.
	 *      FOR A NON-PRIMARY KEY, IT WILL TAKE PLACE HERE, PRIOR TO
	 *      THE INSERT.
	 * ****************************************************************</pre>*/
	private void setupKeys() {
		// COB_CODE: PERFORM 12110-SETUP-KEYS.
		setupKeys1();
	}

	/**Original name: 2120-CHECK-PARENTS-EXIST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PRIOR TO AN INSERT, CHECK THAT CORRESPONDING PARENT ROWS
	 *  EXIST ON THE DATABASE. THIS IS USER MAINTENANCE OF THE
	 *  REFERENTIAL INTEGRITY OF THE SERIES 3 DATABASE.
	 * ****************************************************************</pre>*/
	private void checkParentsExist() {
		// COB_CODE: PERFORM 12120-CHECK-PARENTS-EXIST.
		checkParentsExist1();
	}

	/**Original name: 2130-INS-ROW-TO-INS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  INSERT A ROW.
	 * ****************************************************************</pre>*/
	private void insRowToIns() {
		// COB_CODE: PERFORM 12130-INS-ROW-TO-INS.
		insRowToIns1();
	}

	/**Original name: 2140-WRI-TO-RESP-UMT-AFT-INS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  IF ACTION CODE IS INSERT-AND-RETURN-DATA-REQUEST, WRITE ROW
	 *  TO THE RESPONSE UMT THAT WAS JUST INSERTED IN THE DATABASE.
	 * ****************************************************************</pre>*/
	private void wriToRespUmtAftIns() {
		// COB_CODE: PERFORM 3000-PROCESS-RECORD.
		processRecord();
	}

	/**Original name: 2200-PROCESS-UPD-REQ_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF AN UPDATE REQUEST.
	 * ****************************************************************
	 * * CARRY OUT LOCK AUTHENTICATION PROCESSING.</pre>*/
	private void processUpdReq() {
		// COB_CODE: PERFORM 3030-AUTHENTICATE-ACCESS-LOK.
		authenticateAccessLok();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2200-PROCESS-UPD-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2200-PROCESS-UPD-REQ-X
			return;
		}
		// COB_CODE: SET WS-IUD-PROCESSED TO TRUE.
		ws.getWsBdoSwitches().getIudProcessedSw().setWsIudProcessed();
		// COB_CODE: IF WS-IS-HISTORIZED
		//               PERFORM 2203-PROCESS-UPD-REQ-HIST
		//           ELSE
		//               PERFORM 2206-PROCESS-UPD-REQ-NOTHIST
		//           END-IF.
		if (ws.getWsBdoSwitches().getHistorizedSw().isWsIsHistorized()) {
			// COB_CODE: PERFORM 2203-PROCESS-UPD-REQ-HIST
			processUpdReqHist();
		} else {
			// COB_CODE: PERFORM 2206-PROCESS-UPD-REQ-NOTHIST
			processUpdReqNothist();
		}
	}

	/**Original name: 2203-PROCESS-UPD-REQ-HIST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF AN UPDATE REQUEST.
	 * ****************************************************************</pre>*/
	private void processUpdReqHist() {
		// COB_CODE: SET WS-IUD-PROCESSED TO TRUE.
		ws.getWsBdoSwitches().getIudProcessedSw().setWsIudProcessed();
		// COB_CODE: PERFORM 2810-CHECK-KEY-CHECK-SUM.
		checkKeyCheckSum();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2203-PROCESS-UPD-REQ-HIST-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2203-PROCESS-UPD-REQ-HIST-X
			return;
		}
		// COB_CODE: PERFORM 12203-PROCESS-UPD-REQ-HIST.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=2061, because the code is unreachable.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2203-PROCESS-UPD-REQ-HIST-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2203-PROCESS-UPD-REQ-HIST-X
			return;
		}
		// COB_CODE: IF UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CHANGE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CORRECT-AND-RETURN-DATA-REQ OF URQM-COMMON
		//               PERFORM 2260-WRI-TO-RESP-UMT-AFT-UPD
		//           END-IF.
		if (ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest() || ws.getUrqmCommon().getActionCode().isChangeAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isCorrectAndReturnDataReq()) {
			// COB_CODE: PERFORM 2260-WRI-TO-RESP-UMT-AFT-UPD
			wriToRespUmtAftUpd();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2203-PROCESS-UPD-REQ-HIST-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2203-PROCESS-UPD-REQ-HIST-X
			return;
		}
	}

	/**Original name: 2206-PROCESS-UPD-REQ-NOTHIST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF AN UPDATE REQUEST. A NON-HISTORIZED
	 *  UPDATE AND A NON-HISTORIZED CHANGE ARE IDENTICAL.
	 *  NOTE: NOT HISTORIZED BUSINESS OBJECT ONLY.
	 * ****************************************************************</pre>*/
	private void processUpdReqNothist() {
		// COB_CODE: PERFORM 2810-CHECK-KEY-CHECK-SUM.
		checkKeyCheckSum();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2206-PROCESS-UPD-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2206-PROCESS-UPD-REQ-X
			return;
		}
		// COB_CODE: PERFORM 2830-SETUP-DATA.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=2097, because the code is unreachable.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2206-PROCESS-UPD-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2206-PROCESS-UPD-REQ-X
			return;
		}
		// COB_CODE: PERFORM 2250-UPD-ROW-TO-UPD.
		updRowToUpd();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2206-PROCESS-UPD-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2206-PROCESS-UPD-REQ-X
			return;
		}
		// COB_CODE: IF UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CHANGE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CORRECT-AND-RETURN-DATA-REQ OF URQM-COMMON
		//               PERFORM 2260-WRI-TO-RESP-UMT-AFT-UPD
		//           END-IF.
		if (ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest() || ws.getUrqmCommon().getActionCode().isChangeAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isCorrectAndReturnDataReq()) {
			// COB_CODE: PERFORM 2260-WRI-TO-RESP-UMT-AFT-UPD
			wriToRespUmtAftUpd();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2206-PROCESS-UPD-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2206-PROCESS-UPD-REQ-X
			return;
		}
	}

	/**Original name: 2250-UPD-ROW-TO-UPD_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PROCESS CORRECTION.
	 * ****************************************************************</pre>*/
	private void updRowToUpd() {
		// COB_CODE: PERFORM 2110-SETUP-KEYS.
		setupKeys();
		// COB_CODE: PERFORM 12250-UPD-ROW-TO-UPD.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=2149, because the code is unreachable.
	}

	/**Original name: 2260-WRI-TO-RESP-UMT-AFT-UPD_FIRST_SENTENCES<br>
	 * <pre>****************************************************************       00
	 *                                                                        00
	 *  IF ACTION CODE IS UPDATE-AND-RETURN-DATA-REQUEST, WRITE ROW
	 *  TO THE RESPONSE UMT THAT WAS JUST UPDATED IN THE DATABASE.
	 *                                                                        00
	 * ****************************************************************       00</pre>*/
	private void wriToRespUmtAftUpd() {
		// COB_CODE: PERFORM 3000-PROCESS-RECORD.
		processRecord();
	}

	/**Original name: 2300-PROCESS-DEL-REQ_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF A DELETE REQUEST.
	 * ****************************************************************</pre>*/
	private void processDelReq() {
		// COB_CODE: SET WS-IUD-PROCESSED TO TRUE.
		ws.getWsBdoSwitches().getIudProcessedSw().setWsIudProcessed();
		//* CARRY OUT LOCK AUTHENTICATION PROCESSING.
		// COB_CODE: PERFORM 3030-AUTHENTICATE-ACCESS-LOK.
		authenticateAccessLok();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2300-PROCESS-DEL-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2300-PROCESS-DEL-REQ-X
			return;
		}
		// COB_CODE: IF WS-IS-HISTORIZED
		//               PERFORM 2303-PROCESS-DEL-REQ-HIST
		//           ELSE
		//               PERFORM 2306-PROCESS-DEL-REQ-NOTHIST
		//           END-IF.
		if (ws.getWsBdoSwitches().getHistorizedSw().isWsIsHistorized()) {
			// COB_CODE: PERFORM 2303-PROCESS-DEL-REQ-HIST
			processDelReqHist();
		} else {
			// COB_CODE: PERFORM 2306-PROCESS-DEL-REQ-NOTHIST
			processDelReqNothist();
		}
		// COB_CODE: IF DELETE-DATA-REQUEST OF UBOC-COMM-INFO
		//               SET DELETE-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isDeleteDataRequest()) {
			// COB_CODE: PERFORM 2350-START-CASCADING-DELETE
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=2213, because the code is unreachable.
			// COB_CODE: SET DELETE-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setDeleteDataRequest();
		}
		// COB_CODE: IF CASCADING-DELETE OF UBOC-COMM-INFO
		//               MOVE 1 TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isCascadingDelete()) {
			// COB_CODE: PERFORM 2350-START-CASCADING-DELETE
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=2218, because the code is unreachable.
			// COB_CODE: MOVE SPACES TO UBOC-APP-DATA-BUFFER
			dfhcommarea.getUbocRecord().setAppDataBuffer("");
			// COB_CODE: MOVE 1 TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) 1));
		}
	}

	/**Original name: 2303-PROCESS-DEL-REQ-HIST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF A DELETE REQUEST.
	 *  NOTE: HISTORIZED BUSINESS OBJECT ONLY.
	 * ****************************************************************</pre>*/
	private void processDelReqHist() {
		// COB_CODE: IF CASCADING-DELETE OF UBOC-COMM-INFO
		//                GO TO 2303-PROCESS-DEL-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isCascadingDelete()) {
			// COB_CODE: PERFORM 2360-DELETE-MULTI-ROWS
			deleteMultiRows();
			// COB_CODE: GO TO 2303-PROCESS-DEL-REQ-X
			return;
		}
		// COB_CODE: PERFORM 2810-CHECK-KEY-CHECK-SUM
		checkKeyCheckSum();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2303-PROCESS-DEL-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2303-PROCESS-DEL-REQ-X
			return;
		}
		// COB_CODE: PERFORM 12303-PROCESS-DEL-REQ-HIST.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=2248, because the code is unreachable.
	}

	/**Original name: 2306-PROCESS-DEL-REQ-NOTHIST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROL PROCESSING OF A DELETE REQUEST.
	 *  NOTE: NOT HISTORIZED BUSINESS OBJECT ONLY.
	 * ****************************************************************</pre>*/
	private void processDelReqNothist() {
		// COB_CODE: IF CASCADING-DELETE OF UBOC-COMM-INFO
		//                GO TO 2306-PROCESS-DEL-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isCascadingDelete()) {
			// COB_CODE: PERFORM 2360-DELETE-MULTI-ROWS
			deleteMultiRows();
			// COB_CODE: GO TO 2306-PROCESS-DEL-REQ-X
			return;
		}
		// COB_CODE: PERFORM 2810-CHECK-KEY-CHECK-SUM.
		checkKeyCheckSum();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2306-PROCESS-DEL-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2306-PROCESS-DEL-REQ-X
			return;
		}
		// COB_CODE: PERFORM 12306-PROCESS-DEL-REQ-NOTHIST.
		processDelReqNothist1();
	}

	/**Original name: 2360-DELETE-MULTI-ROWS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  INVOKE DELETE PROCESSING OF RELATED SUBORDINATE BDOS. IF A
	 *  TABLE IS SUBORDINATE TO THIS TABLE IN THE SIII/S3+ DATA MODEL,
	 *  THE BDO THAT PROCESSES IT SHOULD BE REFERENCED IN THIS SECTION.
	 * ****************************************************************</pre>*/
	private void deleteMultiRows() {
		// COB_CODE: PERFORM 12360-DELETE-MULTI-ROWS.
		deleteMultiRows1();
	}

	/**Original name: 2400-PROCESS-IGN-AND-RET-REQ_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  FOR STATELESS FRONT-ENDS, ROW DATA SHOULD ALWAYS BE RETURNED.
	 * ****************************************************************
	 * * SINCE THE HIER SHOULD STILL BE CALLED, THIS FLAG MUST BE SET.  16860000</pre>*/
	private void processIgnAndRetReq() {
		// COB_CODE: SET WS-IUD-PROCESSED TO TRUE.
		ws.getWsBdoSwitches().getIudProcessedSw().setWsIudProcessed();
		//* CALL DATA PRIVACY TO RE-ESTABLISH THE COLUMN INDICATORS BEFORE
		//* REWRITING THE ROW TO THE RESPONSE UMT.  IF ANY OF THE INPUTS
		//* TO THE DP RULES HAVE CHANGED SINCE THE ORIGINAL DOWNLOAD OF
		//* THE ROW, THE COLUMN INDICATORS MAY NOT BE THE SAME AS FOR THE
		//* ORIGINAL DOWNLOAD.
		// COB_CODE: IF UBOC-APPLY-DATA-PRIV
		//               PERFORM 7000-CHECK-AUTHORIZATION
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().isUbocApplyDataPriv()) {
			// COB_CODE: PERFORM 7000-CHECK-AUTHORIZATION
			rng7000CheckAuthorization();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2400-PROCESS-IGN-AND-RET-REQ-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2400-PROCESS-IGN-AND-RET-REQ-X
			return;
		}
		// COB_CODE: PERFORM 4000-WRITE-RESP-DATA-REC.
		writeRespDataRec();
	}

	/**Original name: 2600-PROCESS-IGN-REQ_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PROCESS IGNORE REQUEST.
	 * ****************************************************************
	 * * SINCE THE HIER SHOULD STILL BE CALLED, THIS FLAG MUST BE SET.</pre>*/
	private void processIgnReq() {
		// COB_CODE: SET WS-IUD-PROCESSED TO TRUE.
		ws.getWsBdoSwitches().getIudProcessedSw().setWsIudProcessed();
	}

	/**Original name: 2700-PROC-ADDL-ACTIONS-URQM_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  INVOKED FROM READ OF UOW REQUEST MESSAGE UMT ROW PROCESSING.
	 *  CALL SECTION IN THE SKELETONS THAT PROCESSES NON-STANDARD
	 *  ACTIONS.  HALCDPDC ONLY PROCESSES STANDARD ACTIONS THAT ARE
	 *  NOT APPLICATIONS SPECIFIC.  ANY NON-STANDARD, APPLICATION-
	 *  SPECIFIC ACTIONS MUST BE INTERPRETTED BY THE APPLICATION BDOS.
	 * ****************************************************************
	 * * THE EQUIVALENT UBOC ACTION MUST BE SET IN THE BDO TO ENSURE
	 * * PROPER PROCESSING.</pre>*/
	private void procAddlActionsUrqm() {
		// COB_CODE: PERFORM 12700-PROC-ADDL-ACTIONS-URQM.
		procAddlActionsUrqm1();
	}

	/**Original name: 2705-PROC-ADDL-ACTIONS-UBOC_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  INVOKED FROM 'PASSED IN UBOC' PROCESSING.
	 *  CALL SECTION IN THE SKELETONS THAT PROCESSES NON-STANDARD
	 *  ACTIONS.  HALCDPDC ONLY PROCESSES STANDARD ACTIONS THAT ARE
	 *  NOT APPLICATIONS SPECIFIC.  ANY NON-STANDARD, APPLICATION-
	 *  SPECIFIC ACTIONS MUST BE INTERPRETTED BY THE APPLICATION BDOS.
	 * ****************************************************************</pre>*/
	private void procAddlActionsUboc() {
		// COB_CODE: PERFORM 12705-PROC-ADDL-ACTIONS-UBOC.
		procAddlActionsUboc1();
	}

	/**Original name: 2810-CHECK-KEY-CHECK-SUM_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CHECK THAT THE CHECK SUM WHICH WAS CALCULATED FROM THE KEY
	 *  FOR THIS OBJECT AND SENT TO THE FRONT-END AS PART OF THE FETCH
	 *  REQUEST AND NOW RETURNED TO THE BACK-END CORRESPONDS TO THE
	 *  KEY SENT FROM THE FRONT-END. THIS IS AN ATTEMPT TO ENSURE THAT
	 *  THE KEY HASN'T BEEN CHANGED BY THE FRONT-END.
	 * ****************************************************************</pre>*/
	private void checkKeyCheckSum() {
		// COB_CODE: PERFORM 12810-CHECK-KEY-CHECK-SUM.
		checkKeyCheckSum1();
	}

	/**Original name: 2820-READ-ROW_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  FETCH A ROW WITH A GIVEN KEY.
	 *  NOTE: HISTORIZED AND NOT HISTORIZED BUSINESS OBJECT.
	 * ****************************************************************</pre>*/
	private void readRow() {
		// COB_CODE: PERFORM 12820-READ-ROW.
		readRow1();
	}

	/**Original name: 3000-PROCESS-RECORD_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CONTROLS THE PROCESSING OF THE RETURN OF A ROW TO FRONT-END
	 *  (FETCH, INSERT WITH RETURN, UPDATE WITH RETURN).
	 * ****************************************************************</pre>*/
	private void processRecord() {
		// COB_CODE: PERFORM 3005-REFORMAT-DATA.
		reformatData();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-PROCESS-RECORD-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-PROCESS-RECORD-X
			return;
		}
		// COB_CODE: IF UBOC-NO-DATA-PRIVACY
		//               SET DPER-DATA-PRIV-CHECK-OK TO TRUE
		//           ELSE
		//               END-IF
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().isUbocNoDataPrivacy()) {
			// COB_CODE: SET DPER-DATA-PRIV-CHECK-OK TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocDataPrivRetCode().setDataPrivCheckOk();
		} else {
			// COB_CODE: PERFORM 7000-CHECK-AUTHORIZATION
			rng7000CheckAuthorization();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3000-PROCESS-RECORD-X
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3000-PROCESS-RECORD-X
				return;
			}
		}
		// COB_CODE: IF DPER-ROW-EXCLUDED
		//               GO TO 3000-PROCESS-RECORD-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocDataPrivRetCode().isRowExcluded()) {
			// COB_CODE: GO TO 3000-PROCESS-RECORD-X
			return;
		}
		//* OLD COMMENT
		//* IF PROCESSING A FETCH REQUEST (E.G. NOT A *-AND-RETURN
		//* REQUEST) THEN CARRY OUT LOCK CREATION PROCESSING.              18280000
		//* NEW COMMENT
		//* IF PROCESSING A TYPE OF FETCH REQUEST OR AN INSERT-AND-RETURN
		//* REQUEST THEN CARRY OUT LOCK CREATION PROCESSING.               18280000
		//*   IF NOT (INSERT-AND-RETURN-DATA-REQUEST OF URQM-COMMON        18300000
		//*     OR UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		// COB_CODE: IF NOT (UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CHANGE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CORRECT-AND-RETURN-DATA-REQ    OF URQM-COMMON)
		//               END-IF
		//           END-IF.
		if (!(ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest() || ws.getUrqmCommon().getActionCode().isChangeAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isCorrectAndReturnDataReq())) {
			// COB_CODE: PERFORM 3020-CREATE-ACCESS-LOCK
			createAccessLock();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3000-PROCESS-RECORD-X
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3000-PROCESS-RECORD-X
				return;
			}
		}
		// COB_CODE: IF INSERT-DATA-REQUEST OF URQM-COMMON
		//               GO TO 3000-PROCESS-RECORD-X
		//           END-IF.
		if (ws.getUrqmCommon().getActionCode().isInsertDataRequest()) {
			// COB_CODE: GO TO 3000-PROCESS-RECORD-X
			return;
		}
		// COB_CODE: PERFORM 3050-DETERMINE-CHECK-SUM.
		determineCheckSum();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-PROCESS-RECORD-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-PROCESS-RECORD-X
			return;
		}
		// COB_CODE: PERFORM 4000-WRITE-RESP-DATA-REC.
		writeRespDataRec();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-PROCESS-RECORD-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-PROCESS-RECORD-X
			return;
		}
		//** ONLY THE HIERARCHY PROCESSING FLAVORS OF FETCH SHOULD INVOKE
		//** HIERARCHY PROCESSING BETWEEN THE PROCESSING OF EACH REQUEST
		//** ROW.  THE OTHER ACTIONS REFERENCED BELOW EITHER DO NOT INVOKE
		//** HIERARCHY PROCESSING OR INVOKE IT AFTER ALL OF THE BDO'S
		//** REQUEST ROWS HAVE BEEN PROCESSED.
		// COB_CODE: IF NOT (INSERT-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR FETCH-AND-SKIP-HIER-REQUEST OF URQM-COMMON
		//             OR FETCH-AND-SKIP-HIER-REQUEST OF UBOC-COMM-INFO
		//             OR UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CHANGE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR CORRECT-AND-RETURN-DATA-REQ    OF URQM-COMMON)
		//               PERFORM 5000-PROCESS-HIER-TABLE
		//           END-IF.
		if (!(ws.getUrqmCommon().getActionCode().isInsertAndReturnDataRequest() || ws.getUrqmCommon().getActionCode().isFetchAndSkipHierRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchAndSkipHierRequest()
				|| ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isChangeAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isCorrectAndReturnDataReq())) {
			// COB_CODE: PERFORM 5000-PROCESS-HIER-TABLE
			processHierTable();
		}
	}

	/**Original name: 3005-REFORMAT-DATA_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  REFORMAT FETCHED ROW FOR DOWNLOAD.
	 * ****************************************************************</pre>*/
	private void reformatData() {
		// COB_CODE: PERFORM 13005-REFORMAT-DATA.
		reformatData1();
	}

	/**Original name: 3020-CREATE-ACCESS-LOCK_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DETERMINE THE LOCK KEY INFORMATION AND SET UP PARAMETERS
	 *  BEFORE CALLING THE HALRLODR LOCKING DRIVER MODULE FOR CREATE.
	 * ****************************************************************
	 * * 13020-SECTION WILL SET UP:
	 * *       1. THE LOCKING TECHNICAL KEY
	 * *       2. THE LOCKING APPLICATION ID</pre>*/
	private void createAccessLock() {
		Halrlodr halrlodr = null;
		// COB_CODE: INITIALIZE HALRLODR-INPUT-LINKAGE.
		initInputLinkage();
		// COB_CODE: PERFORM 13020-GENERATE-LOCK-KEY-INFO.
		generateLockKeyInfo();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3020-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3020-CREATE-ACCESS-LOCK-X
			return;
		}
		//* 13025-SECTION WILL SET UP:
		//*       1. THE FETCHED ROW IN UBOC-APP-DATA-BUFFER
		//*       2. THE LENGTH OF THE DATA IN UBOC-APP-DATA-BUFFER-LENGTH
		// COB_CODE: INITIALIZE UBOC-EXTRA-DATA.
		initUbocExtraData();
		// COB_CODE: PERFORM 13025-SETUP-DATA-ROW-FOR-LOK.
		setupDataRowForLok();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3020-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3020-CREATE-ACCESS-LOCK-X
			return;
		}
		//* SET UP REMAINING PARAMETERS FOR HALRLODR.
		//17238      IF INSERT-AND-RETURN-DATA-REQUEST OF URQM-COMMON      19090000
		//17238          SET HALRLODR-CREATE-INSRET-LOCK TO TRUE           19090000
		//17238      ELSE                                                  19090000
		//               SET HALRLODR-CREATE-ACCESS-LOCK TO TRUE           19100000
		//17238      END-IF.                                               19100000
		// COB_CODE: EVALUATE TRUE
		//               WHEN INSERT-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//                   SET HALRLODR-CREATE-INSRET-LOCK TO TRUE
		//               WHEN INSERT-DATA-REQUEST OF URQM-COMMON
		//                   SET HALRLODR-VERIFY-INSERT-ACCESS TO TRUE
		//               WHEN OTHER
		//                   SET HALRLODR-CREATE-ACCESS-LOCK TO TRUE
		//           END-EVALUATE.
		switch (ws.getUrqmCommon().getActionCode().getUbocPassThruAction()) {

		case UbocPassThruAction.INSERT_AND_RETURN_DATA_REQUEST:// COB_CODE: SET HALRLODR-CREATE-INSRET-LOCK TO TRUE
			ws.getHalrlodrLockDriverStorage().getFunction().setHalrlodrCreateInsretLock();
			break;

		case UbocPassThruAction.INSERT_DATA_REQUEST:// COB_CODE: SET HALRLODR-VERIFY-INSERT-ACCESS TO TRUE
			ws.getHalrlodrLockDriverStorage().getFunction().setHalrlodrVerifyInsertAccess();
			break;

		default:// COB_CODE: SET HALRLODR-CREATE-ACCESS-LOCK TO TRUE
			ws.getHalrlodrLockDriverStorage().getFunction().setHalrlodrCreateAccessLock();
			break;
		}
		// COB_CODE: MOVE WS-BUS-OBJ-NM        TO HALRLODR-BUS-OBJ-NM.
		ws.getHalrlodrLockDriverStorage().setBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: CALL WS-HALRLODR-NAME USING
		//                 DFHEIBLK,
		//                 DFHCOMMAREA,
		//                 UBOC-RECORD,
		//                 HALRLODR-INPUT-LINKAGE.
		halrlodr = Halrlodr.getInstance();
		halrlodr.run(execContext, dfhcommarea, dfhcommarea, ws.getHalrlodrLockDriverStorage());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3020-CREATE-ACCESS-LOCK-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3020-CREATE-ACCESS-LOCK-X
			return;
		}
	}

	/**Original name: 3030-AUTHENTICATE-ACCESS-LOK_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DETERMINE THE LOCK KEY INFORMATION AND SET UP PARAMETERS
	 *  BEFORE CALLING THE HALRLODR LOCKING DRIVER MODULE FOR
	 *  AUTHENTICATION.
	 * ****************************************************************</pre>*/
	private void authenticateAccessLok() {
		Halrlodr halrlodr = null;
		// COB_CODE: INITIALIZE HALRLODR-INPUT-LINKAGE.
		initInputLinkage();
		// COB_CODE: PERFORM 13020-GENERATE-LOCK-KEY-INFO.
		generateLockKeyInfo();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3030-AUTHENTICATE-ACCESS-LOK-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3030-AUTHENTICATE-ACCESS-LOK-X
			return;
		}
		//* 13020-SECTION WILL SET UP:
		//*       1. THE LOCKING TECHNICAL KEY
		//*       2. THE LOCKING APPLICATION ID
		//*       3. MOVE THE FETCHED ROW TO UBOC-APP-DATA-BUFFER
		// COB_CODE: SET HALRLODR-AUTH-ACCESS-LOCK TO TRUE.
		ws.getHalrlodrLockDriverStorage().getFunction().setHalrlodrAuthAccessLock();
		// COB_CODE: MOVE WS-BUS-OBJ-NM              TO HALRLODR-BUS-OBJ-NM.
		ws.getHalrlodrLockDriverStorage().setBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: CALL WS-HALRLODR-NAME USING
		//                 DFHEIBLK,
		//                 DFHCOMMAREA,
		//                 UBOC-RECORD,
		//                 HALRLODR-INPUT-LINKAGE.
		halrlodr = Halrlodr.getInstance();
		halrlodr.run(execContext, dfhcommarea, dfhcommarea, ws.getHalrlodrLockDriverStorage());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3030-AUTHENTICATE-ACCESS-LOK-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3030-AUTHENTICATE-ACCESS-LOK-X
			return;
		}
	}

	/**Original name: 3050-DETERMINE-CHECK-SUM_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DETERMINE CHECK SUM BASED ON KEY OF ROW.
	 *  THIS IS THEN SENT WITH THE ROW AND USED TO ENSURE THE
	 *  FRONT-END HASN'T CHANGED THE KEY.
	 * ****************************************************************</pre>*/
	private void determineCheckSum() {
		// COB_CODE: PERFORM 13050-DETERMINE-CHECK-SUM.
		determineCheckSum1();
	}

	/**Original name: 4000-WRITE-RESP-DATA-REC_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  IF THIS IS THE FIRST RESP DATA ROW TO BE WRITTEN, GET THE
	 *  NEXT AVAILABLE RECORD SEQUENCE NUMBER FOR THIS BDO'S RESP ROWS.
	 *  WRITE RESPONSE DATA RECORD.
	 * ****************************************************************</pre>*/
	private void writeRespDataRec() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF WS-FIRST-RESP-DATA-REC
		//               END-IF
		//           END-IF.
		if (ws.getWsBdoSwitches().getRespDataRecordSw().isWsFirstRespDataRec()) {
			// COB_CODE: PERFORM 0230-GET-MAX-UMT-HDR-SEQ
			getMaxUmtHdrSeq();
			// COB_CODE: SET WS-NEXT-RESP-DATA-REC TO TRUE
			ws.getWsBdoSwitches().getRespDataRecordSw().setWsNextRespDataRec();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 4000-WRITE-RESP-DATA-REC-X
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 4000-WRITE-RESP-DATA-REC-X
				return;
			}
		}
		// COB_CODE: IF UBOC-STORE-TYPE-UMT
		//               PERFORM 4005-WR-RESP-UMT-DATA-REC
		//           ELSE
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocStoreTypeCd().isUbocStoreTypeUmt()) {
			// COB_CODE: PERFORM 4005-WR-RESP-UMT-DATA-REC
			wrRespUmtDataRec();
		} else {
			// COB_CODE: SET WS-LOG-ERROR                            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INVALID-STORE-TYPE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidStoreType();
			// COB_CODE: MOVE '4000-WRITE-RESP-DATA-REC'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4000-WRITE-RESP-DATA-REC");
			// COB_CODE: MOVE 'INVALID STORE TYPE'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID STORE TYPE");
			// COB_CODE: STRING 'UBOC-STORE-TYPE-CD=' UBOC-STORE-TYPE-CD ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UBOC-STORE-TYPE-CD=",
					String.valueOf(dfhcommarea.getUbocRecord().getCommInfo().getUbocStoreTypeCd().getUbocStoreTypeCd()), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 4005-WR-RESP-UMT-DATA-REC_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  WRITE THE CURRENT ROW TO THE RESPONSE DATA UMT (HALLUDAT).
	 * ****************************************************************</pre>*/
	private void wrRespUmtDataRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID TO UDAT-ID.
		ws.getHalludat().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE WS-BUS-OBJ-NM TO UDAT-BUS-OBJ-NM.
		ws.getHalludat().setBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: ADD 1 TO UDAT-REC-SEQ.
		ws.getHalludat().setRecSeq(Trunc.toInt(1 + ws.getHalludat().getRecSeq(), 5));
		// COB_CODE: ADD 1 TO UHDR-MSG-BUS-OBJ-COUNT.
		ws.getHalluhdr().setMsgBusObjCount(Trunc.toInt(1 + ws.getHalluhdr().getMsgBusObjCount(), 5));
		//* KEEP A RECORD THAT A RESPONSE DATA RECORD HAS BEEN WRITTEN
		//* IN THIS PASS OF THE MODULE. AT THE END OF PROCESSING,
		//* THIS SWITCH WILL CAUSE THE RESPONSE HEADER RECORD TO BE
		//* UPDATED WITH A NEW COUNT (OR FOR A COUNT TO BE CREATED).
		// COB_CODE: SET WS-RESP-DATA-ROWS-WRIT TO TRUE.
		ws.getWsBdoSwitches().getRespDataRowsWritSw().setWsRespDataRowsWrit();
		// COB_CODE: PERFORM 14005-WR-RESP-UMT-DATA-REC-A.
		wrRespUmtDataRecA();
		// COB_CODE: ADD LENGTH OF WS-BUS-OBJ-NM
		//               TO UDAT-UOW-BUFFER-LENGTH.
		ws.getHalludat().setUowBufferLength(((short) (WsSpecificMiscXz0d0008.Len.BUS_OBJ_NM + ws.getHalludat().getUowBufferLength())));
		// COB_CODE: MOVE WS-BUS-OBJ-NM  TO UDAT-UOW-BUS-OBJ-NM.
		ws.getHalludat().setUowBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: PERFORM 14005-WR-RESP-UMT-DATA-REC-B.
		wrRespUmtDataRecB();
		// COB_CODE: EXEC CICS
		//               WRITE FILE (UBOC-UOW-RESP-DATA-STORE)
		//               FROM       (WS-DATA-UMT-AREA)
		//               LENGTH     (LENGTH OF WS-DATA-UMT-AREA)
		//               RIDFLD     (UDAT-KEY)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsDataUmtAreaBytes());
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 4005-WR-RESP-UMT-DATA-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '4005-WR-RESP-UMT-DATA-REC'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4005-WR-RESP-UMT-DATA-REC");
			// COB_CODE: MOVE 'WRITE TO UOW RESP DATA STORE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW RESP DATA STORE FAILED");
			// COB_CODE: STRING 'UDAT-ID='         UDAT-ID         ';'
			//                  'UDAT-BUS-OBJ-NM=' UDAT-BUS-OBJ-NM ';'
			//                  'UDAT-REC-SEQ='    UDAT-REC-SEQ    ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";", "UDAT-BUS-OBJ-NM=", ws.getHalludat().getBusObjNmFormatted(),
							";", "UDAT-REC-SEQ=", ws.getHalludat().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4005-WR-RESP-UMT-DATA-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-DATA-ROWS.
		dfhcommarea.getUbocRecord().getCommInfo()
				.setUbocNbrDataRows(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrDataRows(), 9));
	}

	/**Original name: 4020-WRITE-RESP-HDR-REC_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  WRITE A RESPONSE HEADER.
	 * ****************************************************************</pre>*/
	private void writeRespHdrRec() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF UBOC-STORE-TYPE-UMT
		//               PERFORM 4025-WRITE-RESP-UMT-HDR-REC
		//           ELSE
		//               GO TO 4020-WRITE-RESP-HDR-REC-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocStoreTypeCd().isUbocStoreTypeUmt()) {
			// COB_CODE: PERFORM 4025-WRITE-RESP-UMT-HDR-REC
			writeRespUmtHdrRec();
		} else {
			// COB_CODE: SET WS-LOG-ERROR                            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INVALID-STORE-TYPE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidStoreType();
			// COB_CODE: MOVE '4020-WRITE-RESP-HDR-REC'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4020-WRITE-RESP-HDR-REC");
			// COB_CODE: MOVE 'INVALID STORE TYPE'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID STORE TYPE");
			// COB_CODE: STRING 'UBOC-STORE-TYPE-CD' UBOC-STORE-TYPE-CD ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UBOC-STORE-TYPE-CD",
					String.valueOf(dfhcommarea.getUbocRecord().getCommInfo().getUbocStoreTypeCd().getUbocStoreTypeCd()), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4020-WRITE-RESP-HDR-REC-X
			return;
		}
	}

	/**Original name: 4025-WRITE-RESP-UMT-HDR-REC_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  WRITE THE HEADER ROW TO THE RESPONSE HEADER UMT (HALLUHDR).
	 *  IF A HEADER ROW ALREADY EXISTS, READ IT WITH UPDATE OPTION
	 *  AND REWRITE IT; OTHERWISE INITIALIZE THE HEADER RECORD AND
	 *  WRITE A NEW HEADER ROW.
	 * ****************************************************************</pre>*/
	private void writeRespUmtHdrRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EVALUATE TRUE
		//               WHEN WS-UMT-HDR-MISSING
		//                   END-EXEC
		//               WHEN WS-UMT-HDR-EXISTS
		//                   END-EXEC
		//               WHEN OTHER
		//                   GO TO 4025-WRITE-RESP-UMT-HDR-REC-X
		//           END-EVALUATE.
		switch (ws.getWsBdoSwitches().getUmtHdrSw().getUmtHdrSw()) {

		case WsUmtHdrSw.MISSING:// COB_CODE: EXEC CICS
			//               WRITE FILE (UBOC-UOW-RESP-HEADER-STORE)
			//               FROM       (UHDR-COMMON)
			//               LENGTH     (LENGTH OF UHDR-COMMON)
			//               RIDFLD     (UHDR-KEY)
			//               RESP       (WS-RESPONSE-CODE)
			//               RESP2      (WS-RESPONSE-CODE2)
			//           END-EXEC
			iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStoreFormatted());
			if (iRowDAO != null) {
				iRowData = iRowDAO.createTO(ws.getHalluhdr().getCommonBytes());
				iRowData.setKey(ws.getHalluhdr().getKeyBytes());
				iRowDAO.insert(iRowData);
			}
			ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
			ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
			break;

		case WsUmtHdrSw.EXISTS:// COB_CODE: PERFORM 4030-READUPD-RESP-UMT-HDR
			readupdRespUmtHdr();
			//* READ WILL OVERWRITE COUNT SO RE-LOAD IT FROM UDAT-REC-SEQ.
			// COB_CODE: MOVE UDAT-REC-SEQ TO UHDR-MSG-BUS-OBJ-COUNT
			ws.getHalluhdr().setMsgBusObjCountFormatted(ws.getHalludat().getRecSeqFormatted());
			// COB_CODE: EXEC CICS
			//               REWRITE FILE (UBOC-UOW-RESP-HEADER-STORE)
			//               FROM         (UHDR-COMMON)
			//               LENGTH       (LENGTH OF UHDR-COMMON)
			//               RESP         (WS-RESPONSE-CODE)
			//               RESP2        (WS-RESPONSE-CODE2)
			//           END-EXEC
			iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStoreFormatted());
			if (iRowDAO != null) {
				iRowData = iRowDAO.createTO(ws.getHalluhdr().getCommonBytes());
				iRowDAO.update(iRowData);
			}
			ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
			ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-HDR-EXIST-CANT-TELL OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspHdrExistCantTell();
			// COB_CODE: MOVE '4025-WRITE-RESP-UMT-HDR-REC'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4025-WRITE-RESP-UMT-HDR-REC");
			// COB_CODE: MOVE 'CANT DETERMINE IF HEADER EXISTS'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CANT DETERMINE IF HEADER EXISTS");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4025-WRITE-RESP-UMT-HDR-REC-X
			return;
		}
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 4025-WRITE-RESP-UMT-HDR-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '4025-WRITE-RESP-UMT-HDR-REC'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4025-WRITE-RESP-UMT-HDR-REC");
			// COB_CODE: MOVE 'WRITE TO UOW RESP HEADER STORE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW RESP HEADER STORE FAILED");
			// COB_CODE: STRING 'UHDR-ID='         UHDR-ID         ';'
			//                  'UHDR-BUS-OBJ-NM=' UHDR-BUS-OBJ-NM ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "UHDR-ID=", ws.getHalluhdr().getIdFormatted(),
					";", "UHDR-BUS-OBJ-NM=", ws.getHalluhdr().getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4025-WRITE-RESP-UMT-HDR-REC-X
			return;
		}
		// COB_CODE: IF WS-UMT-HDR-MISSING
		//               ADD 1 TO UBOC-NBR-HDR-ROWS
		//           END-IF.
		if (ws.getWsBdoSwitches().getUmtHdrSw().isMissing()) {
			// COB_CODE: ADD 1 TO UBOC-NBR-HDR-ROWS
			dfhcommarea.getUbocRecord().getCommInfo()
					.setUbocNbrHdrRows(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrHdrRows(), 9));
		}
	}

	/**Original name: 4030-READUPD-RESP-UMT-HDR_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ THE RESPONSE HEADER UMT (HALLUHDR) FOR THIS BDO FOR
	 *  SUBSEQUENT REWRITE.
	 *  THE HEADER ROW HAS ALREADY BEEN READ DURING PROGRAM
	 *  INITIALIZATION BUT AN UPDATE LOCK WAS NOT PLACED ON
	 *  THE UMT DUE TO POTENTIAL SUBORDINATE BDOS ATTEMPTING TO
	 *  UPDATE THE SAME UMT.
	 * ****************************************************************</pre>*/
	private void readupdRespUmtHdr() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//                READ FILE (UBOC-UOW-RESP-HEADER-STORE)
		//                INTO      (UHDR-COMMON)
		//                RIDFLD    (UHDR-KEY)
		//                KEYLENGTH (LENGTH OF UHDR-KEY)
		//                UPDATE
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Halluhdr.Len.KEY, true);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalluhdr().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* SINCE WE'VE ALREADY READ THIS HEADER RECORD, WE SHOULD ALWAYS
		//* EXPECT A NORMAL RETURN CODE FROM THIS READ.
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 4030-READUPD-RESP-UMT-HDR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR                       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '4030-READUPD-RESP-UMT-HDR'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4030-READUPD-RESP-UMT-HDR");
			// COB_CODE: MOVE 'READ (UPD) UOW RESP HEADER STORE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ (UPD) UOW RESP HEADER STORE FAILED");
			// COB_CODE: STRING 'UHDR-ID='         UHDR-ID         ';'
			//                  'UHDR-BUS-OBJ-NM=' UHDR-BUS-OBJ-NM ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "UHDR-ID=", ws.getHalluhdr().getIdFormatted(),
					";", "UHDR-BUS-OBJ-NM=", ws.getHalluhdr().getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4030-READUPD-RESP-UMT-HDR-X
			return;
		}
	}

	/**Original name: 5000-PROCESS-HIER-TABLE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ THE UOW BUS OBJ HIERARCHY TABLE AND RETRIEVE ALL THE
	 *  CHILDREN OF THIS BUSINESS OBJECT WITHIN THIS UOW.
	 * ****************************************************************</pre>*/
	private void processHierTable() {
		// COB_CODE: PERFORM 5005-OPEN-HIER-CURSOR.
		openHierCursor();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 5000-PROCESS-HIER-TABLE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 5000-PROCESS-HIER-TABLE-X
			return;
		}
		// COB_CODE: MOVE UBOC-PASS-THRU-ACTION TO WS-ORIGINAL-ACTION.
		ws.getWsBdoWorkFields().setOriginalAction(dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().getUbocPassThruAction());
		// COB_CODE: PERFORM 5010-PROCESS-CHILD
		//             UNTIL UBOC-HALT-AND-RETURN
		//                OR WS-END-OF-CURSOR0.
		while (!(dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsBdoSwitches().getEndOfCursor0Sw().isWsEndOfCursor0())) {
			processChild();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 5000-PROCESS-HIER-TABLE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 5000-PROCESS-HIER-TABLE-X
			return;
		}
		// COB_CODE: PERFORM 5015-CLOSE-HIER-CURSOR.
		closeHierCursor();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 5000-PROCESS-HIER-TABLE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 5000-PROCESS-HIER-TABLE-X
			return;
		}
	}

	/**Original name: 5005-OPEN-HIER-CURSOR_FIRST_SENTENCES<br>
	 * <pre>**************************************************************** *
	 *  OPEN HIERARCHY CURSOR
	 * **************************************************************** *</pre>*/
	private void openHierCursor() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-UOW-NAME          TO HUOH-UOW-NM.
		ws.getDclhalUowObjHierV().setUowNm(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-PRIMARY-BUS-OBJ   TO HUOH-ROOT-BOBJ-NM.
		ws.getDclhalUowObjHierV().setRootBobjNm(dfhcommarea.getUbocRecord().getCommInfo().getUbocPrimaryBusObj());
		// COB_CODE: MOVE WS-BUS-OBJ-NM          TO HUOH-PNT-BOBJ-NM.
		ws.getDclhalUowObjHierV().setPntBobjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: EXEC SQL
		//              OPEN BO-HIER-CURS0
		//           END-EXEC.
		halUowObjHierVDao.openBoHierCurs0(ws.getDclhalUowObjHierV().getUowNm(), ws.getDclhalUowObjHierV().getRootBobjNm(),
				ws.getDclhalUowObjHierV().getPntBobjNm());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 5005-OPEN-HIER-CURSOR-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_UOW_OBJ_HIER'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_OBJ_HIER");
			// COB_CODE: MOVE '5005-OPEN-HIER-CURSOR'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5005-OPEN-HIER-CURSOR");
			// COB_CODE: MOVE 'OPEN CURS0 FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CURS0 FAILED");
			// COB_CODE: STRING 'HUOH-UOW-NM='       HUOH-UOW-NM       ';'
			//                  'HUOH-ROOT-BOBJ-NM=' HUOH-ROOT-BOBJ-NM ';'
			//                  'HUOH-PNT-BOBJ-NM='  HUOH-PNT-BOBJ-NM  ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "HUOH-UOW-NM=", ws.getDclhalUowObjHierV().getUowNmFormatted(), ";", "HUOH-ROOT-BOBJ-NM=",
							ws.getDclhalUowObjHierV().getRootBobjNmFormatted(), ";", "HUOH-PNT-BOBJ-NM=",
							ws.getDclhalUowObjHierV().getPntBobjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5005-OPEN-HIER-CURSOR-X
			return;
		}
		// COB_CODE: SET WS-NOT-END-OF-CURSOR0 TO TRUE.
		ws.getWsBdoSwitches().getEndOfCursor0Sw().setWsNotEndOfCursor0();
		// COB_CODE: IF   FETCH-DATA-REQUEST OF UBOC-COMM-INFO
		//             OR FETCH-WITH-EXACT-KEY-REQUEST OF UBOC-COMM-INFO
		//             OR FETCH-WITH-EXACT-KEY-ISS-REQ OF UBOC-COMM-INFO
		//             OR FETCH-WITH-EXACT-KEY-PND-REQ OF UBOC-COMM-INFO
		//             OR FETCH-ISSUED-ROWS-ONLY-REQ   OF UBOC-COMM-INFO
		//             OR FETCH-PENDING-DATA-REQUEST   OF UBOC-COMM-INFO
		//             OR FETCH-ALL-DATA-REQUEST       OF UBOC-COMM-INFO
		//               PERFORM 5020-CHECK-MSTR-SWT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyIssReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyPndReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchIssuedRowsOnlyReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchPendingDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchAllDataRequest()) {
			// COB_CODE: PERFORM 5020-CHECK-MSTR-SWT
			checkMstrSwt();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 5005-OPEN-HIER-CURSOR-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 5005-OPEN-HIER-CURSOR-X
			return;
		}
	}

	/**Original name: 5010-PROCESS-CHILD_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  FETCH CHILD OF THIS BUSINESS OBJECT WITHIN THIS UOW.
	 * ****************************************************************</pre>*/
	private void processChild() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET CALL-CHILD TO TRUE.
		ws.getWsNotSpecificMisc().getProcessChildSw().setCallChild();
		// COB_CODE: EXEC SQL
		//              FETCH BO-HIER-CURS0
		//              INTO :HUOH-HIER-SEQ-NBR,
		//                   :HUOH-PNT-BOBJ-NM,
		//                   :HUOH-CHD-BOBJ-NM
		//           END-EXEC.
		halUowObjHierVDao.fetchBoHierCurs0(ws.getDclhalUowObjHierV());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 5010-PROCESS-CHILD-X
		//               WHEN OTHER
		//                   GO TO 5010-PROCESS-CHILD-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CURSOR0 TO TRUE
			ws.getWsBdoSwitches().getEndOfCursor0Sw().setWsEndOfCursor0();
			// COB_CODE: GO TO 5010-PROCESS-CHILD-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'HAL_UOW_OBJ_HIER'
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_OBJ_HIER");
			// COB_CODE: MOVE '5010-PROCESS-CHILD'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5010-PROCESS-CHILD");
			// COB_CODE: MOVE 'FETCH CURS0 FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH CURS0 FAILED");
			// COB_CODE: STRING 'HUOH-UOW-NM'         HUOH-UOW-NM          ';'
			//                  'HUOH-ROOT-BOBJ-NM'   HUOH-ROOT-BOBJ-NM    ';'
			//                  'HUOH-PNT-BOBJ-NM'    HUOH-PNT-BOBJ-NM     ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "HUOH-UOW-NM", ws.getDclhalUowObjHierV().getUowNmFormatted(), ";", "HUOH-ROOT-BOBJ-NM",
							ws.getDclhalUowObjHierV().getRootBobjNmFormatted(), ";", "HUOH-PNT-BOBJ-NM",
							ws.getDclhalUowObjHierV().getPntBobjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5010-PROCESS-CHILD-X
			return;
		}
		// COB_CODE: MOVE HUOH-CHD-BOBJ-NM TO WS-CHILD-BUS-OBJ-NM
		//                                    WS-CHILDREN.
		ws.getWsNotSpecificMisc().setChildBusObjNm(ws.getDclhalUowObjHierV().getChdBobjNm());
		ws.getWsSpecificMisc().getChildren().setChildren(ws.getDclhalUowObjHierV().getChdBobjNm());
		// COB_CODE: PERFORM 5400-READ-BUS-OBJ-XREF.
		readBusObjXref();
		// COB_CODE: EVALUATE TRUE
		//               WHEN FETCH-DATA-REQUEST OF UBOC-COMM-INFO
		//                 OR FETCH-WITH-EXACT-KEY-REQUEST OF UBOC-COMM-INFO
		//                 OR FETCH-WITH-EXACT-KEY-ISS-REQ OF UBOC-COMM-INFO
		//                 OR FETCH-WITH-EXACT-KEY-PND-REQ OF UBOC-COMM-INFO
		//                 OR FETCH-ISSUED-ROWS-ONLY-REQ   OF UBOC-COMM-INFO
		//                 OR FETCH-PENDING-DATA-REQUEST   OF UBOC-COMM-INFO
		//                 OR FETCH-ALL-DATA-REQUEST       OF UBOC-COMM-INFO
		//                   END-IF
		//               WHEN DELETE-DATA-REQUEST OF UBOC-COMM-INFO
		//                 OR CASCADING-DELETE OF UBOC-COMM-INFO
		//                   END-IF
		//               WHEN OTHER
		//                   PERFORM 5200-LINK-TO-CHILD-MODULE
		//           END-EVALUATE.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyIssReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyPndReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchIssuedRowsOnlyReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchPendingDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchAllDataRequest()) {
			// COB_CODE: IF MASTER-SWITCH-SET
			//               PERFORM 5200-LINK-TO-CHILD-MODULE
			//           ELSE
			//               END-IF
			//           END-IF
			if (ws.getWsBdoSwitches().getMasterSwitchInd().isSetFld()) {
				// COB_CODE: PERFORM 5200-LINK-TO-CHILD-MODULE
				linkToChildModule();
			} else {
				// COB_CODE: SET MATCHING-SWITCH-NOT-FOUND TO TRUE
				ws.getWsBdoSwitches().getMatchingSwitchSw().setNotFound();
				// COB_CODE: PERFORM 5100-READ-SWITCH-UMT
				readSwitchUmt();
				// COB_CODE: IF MATCHING-SWITCH-FOUND
				//             AND NOT UBOC-HALT-AND-RETURN
				//               PERFORM 5200-LINK-TO-CHILD-MODULE
				//           END-IF
				if (ws.getWsBdoSwitches().getMatchingSwitchSw().isFound()
						&& !dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
					// COB_CODE: PERFORM 5200-LINK-TO-CHILD-MODULE
					linkToChildModule();
				}
			}
		} else if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isDeleteDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isCascadingDelete()) {
			// COB_CODE: IF NOT WS-DIRECT-S3-CHILDREN
			//               PERFORM 5200-LINK-TO-CHILD-MODULE
			//           END-IF
			if (!ws.getWsSpecificMisc().getChildren().isDirectS3Children()) {
				// COB_CODE: SET NO-PASS-THRU-ACTION OF UBOC-COMM-INFO
				//             TO TRUE
				dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setNoPassThruAction();
				// COB_CODE: PERFORM 5200-LINK-TO-CHILD-MODULE
				linkToChildModule();
			}
		} else {
			// COB_CODE: PERFORM 5200-LINK-TO-CHILD-MODULE
			linkToChildModule();
		}
		// COB_CODE: MOVE WS-ORIGINAL-ACTION TO UBOC-PASS-THRU-ACTION.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setUbocPassThruAction(ws.getWsBdoWorkFields().getOriginalAction());
	}

	/**Original name: 5015-CLOSE-HIER-CURSOR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CLOSE HIERARCHY CURSOR
	 * *****************************************************************</pre>*/
	private void closeHierCursor() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//               CLOSE BO-HIER-CURS0
		//           END-EXEC.
		halUowObjHierVDao.closeBoHierCurs0();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 5015-CLOSE-HIER-CURSOR-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING                     TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_UOW_OBJ_HIER'
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_OBJ_HIER");
			// COB_CODE: MOVE '5015-CLOSE-HIER-CURSOR'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5015-CLOSE-HIER-CURSOR");
			// COB_CODE: MOVE 'CLOSE CURS0 FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CURS0 FAILED");
			// COB_CODE: STRING 'HUOH-UOW-NM='       HUOH-UOW-NM       ';'
			//                  'HUOH-ROOT-BOBJ-NM=' HUOH-ROOT-BOBJ-NM ';'
			//                  'HUOH-PNT-BOBJ-NM='  HUOH-PNT-BOBJ-NM  ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "HUOH-UOW-NM=", ws.getDclhalUowObjHierV().getUowNmFormatted(), ";", "HUOH-ROOT-BOBJ-NM=",
							ws.getDclhalUowObjHierV().getRootBobjNmFormatted(), ";", "HUOH-PNT-BOBJ-NM=",
							ws.getDclhalUowObjHierV().getPntBobjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5015-CLOSE-HIER-CURSOR-X
			return;
		}
	}

	/**Original name: 5020-CHECK-MSTR-SWT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  LOOK FOR A MASTER SWITCH OF 'RETURN ALL' TO INDICATE THAT ALL
	 *  MODULES LISTED IN THE HIERARCHY TABLE SHOULD BE CALLED.
	 * ****************************************************************</pre>*/
	private void checkMstrSwt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF READ-FOR-MSTR-SWT-YES
		//               GO TO 5020-CHECK-MSTR-SWT-X
		//           END-IF.
		if (ws.getWsBdoSwitches().getReadForMstrSwtInd().isYes()) {
			// COB_CODE: GO TO 5020-CHECK-MSTR-SWT-X
			return;
		}
		// COB_CODE: MOVE UBOC-MSG-ID TO USW-ID.
		ws.getHallusw().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE 'RETURN ALL' TO USW-BUS-OBJ-SWITCH
		//                                WS-SWITCH-OBJECT.
		ws.getHallusw().setBusObjSwitch("RETURN ALL");
		ws.getWsBdoWorkFields().setSwitchObject("RETURN ALL");
		// COB_CODE: IF UBOC-UOW-REQ-SWITCHES-TSQ EQUAL SPACES
		//               CONTINUE
		//           ELSE
		//               GO TO 5020-CHECK-MSTR-SWT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsq())) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET MASTER-SWITCH-NOT-SET TO TRUE
			ws.getWsBdoSwitches().getMasterSwitchInd().setNotSet();
			// COB_CODE: SET READ-FOR-MSTR-SWT-YES TO TRUE
			ws.getWsBdoSwitches().getReadForMstrSwtInd().setYes();
			// COB_CODE: SET WS-READING-FOR-MASTER TO TRUE
			ws.getWsBdoSwitches().getSwitchesTypeSw().setMaster();
			// COB_CODE: PERFORM 5150-CHECK-SWITCH-TSQ
			checkSwitchTsq();
			// COB_CODE: GO TO 5020-CHECK-MSTR-SWT-X
			return;
		}
		// COB_CODE: EXEC CICS
		//                READ FILE (UBOC-UOW-REQ-SWITCHES-STORE)
		//                INTO      (USW-COMMON)
		//                RIDFLD    (USW-KEY)
		//                KEYLENGTH (LENGTH OF USW-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHallusw().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Hallusw.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHallusw().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    SET READ-FOR-MSTR-SWT-YES TO TRUE
		//               WHEN DFHRESP(NOTFND)
		//                    SET READ-FOR-MSTR-SWT-YES TO TRUE
		//               WHEN OTHER
		//                    GO TO 5020-CHECK-MSTR-SWT-X
		//              END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET MASTER-SWITCH-SET     TO TRUE
			ws.getWsBdoSwitches().getMasterSwitchInd().setSetFld();
			// COB_CODE: SET READ-FOR-MSTR-SWT-YES TO TRUE
			ws.getWsBdoSwitches().getReadForMstrSwtInd().setYes();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET MASTER-SWITCH-NOT-SET TO TRUE
			ws.getWsBdoSwitches().getMasterSwitchInd().setNotSet();
			// COB_CODE: SET READ-FOR-MSTR-SWT-YES TO TRUE
			ws.getWsBdoSwitches().getReadForMstrSwtInd().setYes();
		} else {
			// COB_CODE: SET WS-LOG-ERROR                       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-SWITCHES-STORE
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore());
			// COB_CODE: MOVE '5020-CHECK-MSTR-SWT'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5020-CHECK-MSTR-SWT");
			// COB_CODE: MOVE 'READ UOW REQ SWITCHES STORE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ UOW REQ SWITCHES STORE FAILED");
			// COB_CODE: STRING 'USW-ID='             USW-ID             ';'
			//                  'USW-BUS-OBJ-SWITCH=' USW-BUS-OBJ-SWITCH ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "USW-ID=", ws.getHallusw().getIdFormatted(),
					";", "USW-BUS-OBJ-SWITCH=", ws.getHallusw().getBusObjSwitchFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5020-CHECK-MSTR-SWT-X
			return;
		}
	}

	/**Original name: 5100-READ-SWITCH-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ SWITCH UMT ENTRY.
	 * ****************************************************************</pre>*/
	private void readSwitchUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID TO USW-ID.
		ws.getHallusw().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: PERFORM 15100-BUILD-SWITCH-BUFFER.
		buildSwitchBuffer();
		// COB_CODE: MOVE USW-BUS-OBJ-SWITCH TO WS-SWITCH-OBJECT.
		ws.getWsBdoWorkFields().setSwitchObject(ws.getHallusw().getBusObjSwitch());
		// COB_CODE: IF UBOC-UOW-REQ-SWITCHES-TSQ EQUAL SPACES
		//               CONTINUE
		//           ELSE
		//               GO TO 5100-READ-SWITCH-UMT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsq())) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET MATCHING-SWITCH-NOT-FOUND TO TRUE
			ws.getWsBdoSwitches().getMatchingSwitchSw().setNotFound();
			// COB_CODE: SET WS-READING-FOR-OBJECT TO TRUE
			ws.getWsBdoSwitches().getSwitchesTypeSw().setObjectFld();
			// COB_CODE: PERFORM 5150-CHECK-SWITCH-TSQ
			checkSwitchTsq();
			// COB_CODE: GO TO 5100-READ-SWITCH-UMT-X
			return;
		}
		// COB_CODE: EXEC CICS
		//                READ FILE (UBOC-UOW-REQ-SWITCHES-STORE)
		//                INTO      (USW-COMMON)
		//                RIDFLD    (USW-KEY)
		//                KEYLENGTH (LENGTH OF USW-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHallusw().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Hallusw.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHallusw().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   SET MATCHING-SWITCH-FOUND TO TRUE
		//               WHEN DFHRESP(NOTFND)
		//                   SET MATCHING-SWITCH-NOT-FOUND TO TRUE
		//               WHEN OTHER
		//                   GO TO 5100-READ-SWITCH-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET MATCHING-SWITCH-FOUND TO TRUE
			ws.getWsBdoSwitches().getMatchingSwitchSw().setFound();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET MATCHING-SWITCH-NOT-FOUND TO TRUE
			ws.getWsBdoSwitches().getMatchingSwitchSw().setNotFound();
		} else {
			// COB_CODE: SET WS-LOG-ERROR                       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-SWITCHES-STORE
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore());
			// COB_CODE: MOVE '5100-READ-SWITCH-UMT'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5100-READ-SWITCH-UMT");
			// COB_CODE: MOVE 'READ UOW REQ SWITCHES STORE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ UOW REQ SWITCHES STORE FAILED");
			// COB_CODE: STRING 'USW-ID='             USW-ID             ';'
			//                  'USW-BUS-OBJ-SWITCH=' USW-BUS-OBJ-SWITCH ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "USW-ID=", ws.getHallusw().getIdFormatted(),
					";", "USW-BUS-OBJ-SWITCH=", ws.getHallusw().getBusObjSwitchFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5100-READ-SWITCH-UMT-X
			return;
		}
	}

	/**Original name: 5150-CHECK-SWITCH-TSQ_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ ROWS FROM THE UOW SWITCHES TSQ
	 * ****************************************************************</pre>*/
	private void checkSwitchTsq() {
		// COB_CODE: SET WS-START-OF-SWITCHES-TSQ TO TRUE.
		ws.getWsBdoSwitches().getSwitchesTsqSw().setStartOfSwitchesTsq();
		// COB_CODE: PERFORM 5155-READ-ALL-SWITCH-Q-ROWS
		//              VARYING WS-SWITCH-TSQ-CNT
		//              FROM 1 BY 1
		//              UNTIL UBOC-HALT-AND-RETURN
		//                 OR WS-END-OF-SWITCHES-TSQ.
		ws.getWsBdoWorkFields().setSwitchTsqCnt(((short) 1));
		while (!(dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsBdoSwitches().getSwitchesTsqSw().isEndOfSwitchesTsq())) {
			readAllSwitchQRows();
			ws.getWsBdoWorkFields().setSwitchTsqCnt(Trunc.toShort(ws.getWsBdoWorkFields().getSwitchTsqCnt() + 1, 4));
		}
	}

	/**Original name: 5155-READ-ALL-SWITCH-Q-ROWS_FIRST_SENTENCES<br>*/
	private void readAllSwitchQRows() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS
		//                READQ TS QNAME(UBOC-UOW-REQ-SWITCHES-TSQ)
		//                INTO          (USW-COMMON)
		//                ITEM          (WS-SWITCH-TSQ-CNT)
		//                RESP          (WS-RESPONSE-CODE)
		//                RESP2         (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(Hallusw.Len.COMMON);
		TsQueueManager.read(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsqFormatted(),
				ws.getWsBdoWorkFields().getSwitchTsqCnt(), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.getHallusw().setCommonBytes(tsQueueData.getData());
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//             WHEN DFHRESP(NORMAL)
		//               CONTINUE
		//             WHEN DFHRESP(QIDERR)
		//             WHEN DFHRESP(ITEMERR)
		//               GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
		//             WHEN OTHER
		//               GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.QIDERR)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ITEMERR)) {
			// COB_CODE: SET WS-END-OF-SWITCHES-TSQ TO TRUE
			ws.getWsBdoSwitches().getSwitchesTsqSw().setEndOfSwitchesTsq();
			// COB_CODE: GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadTsq();
			// COB_CODE: MOVE UBOC-UOW-LOCK-PROC-TSQ
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowLockProcTsq());
			// COB_CODE: MOVE '5155-READ-ALL-SWITCH-Q-ROWS'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5155-READ-ALL-SWITCH-Q-ROWS");
			// COB_CODE: MOVE 'READ OF SWITCHES TSQ FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ OF SWITCHES TSQ FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
			return;
		}
		// COB_CODE: IF (UBOC-MSG-ID EQUAL USW-ID)
		//             AND (WS-SWITCH-OBJECT EQUAL USW-BUS-OBJ-SWITCH)
		//               GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
		//           END-IF.
		if (Conditions.eq(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId(), ws.getHallusw().getId())
				&& Conditions.eq(ws.getWsBdoWorkFields().getSwitchObject(), ws.getHallusw().getBusObjSwitch())) {
			// COB_CODE: IF WS-READING-FOR-OBJECT
			//               SET MATCHING-SWITCH-FOUND TO TRUE
			//           ELSE
			//               SET MASTER-SWITCH-SET     TO TRUE
			//           END-IF
			if (ws.getWsBdoSwitches().getSwitchesTypeSw().isObjectFld()) {
				// COB_CODE: SET MATCHING-SWITCH-FOUND TO TRUE
				ws.getWsBdoSwitches().getMatchingSwitchSw().setFound();
			} else {
				// COB_CODE: SET MASTER-SWITCH-SET     TO TRUE
				ws.getWsBdoSwitches().getMasterSwitchInd().setSetFld();
			}
			// COB_CODE: SET WS-END-OF-SWITCHES-TSQ TO TRUE
			ws.getWsBdoSwitches().getSwitchesTsqSw().setEndOfSwitchesTsq();
			// COB_CODE: GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
			return;
		}
	}

	/**Original name: 5200-LINK-TO-CHILD-MODULE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  LINK TO CHILD MODULE.
	 *  HALLUBOC IS THE COMMON LINKAGE COPYBOOK FOR BUS OBJ TO
	 *  BUS OBJ COMMUNICATION.
	 * ****************************************************************</pre>*/
	private void linkToChildModule() {
		// COB_CODE: PERFORM 5205-CALL-OR-BYPASS-CHILD.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=3379, because the code is unreachable.
		// COB_CODE: IF BYPASS-CHILD
		//               GO TO 5200-LINK-TO-CHILD-MODULE-X
		//           END-IF.
		if (ws.getWsNotSpecificMisc().getProcessChildSw().isBypassChild()) {
			// COB_CODE: GO TO 5200-LINK-TO-CHILD-MODULE-X
			return;
		}
		// COB_CODE: IF FETCH-DATA-REQUEST OF UBOC-COMM-INFO
		//             OR FETCH-WITH-EXACT-KEY-REQUEST OF UBOC-COMM-INFO
		//             OR FETCH-WITH-EXACT-KEY-ISS-REQ OF UBOC-COMM-INFO
		//             OR FETCH-WITH-EXACT-KEY-PND-REQ OF UBOC-COMM-INFO
		//             OR FETCH-ISSUED-ROWS-ONLY-REQ   OF UBOC-COMM-INFO
		//             OR FETCH-PENDING-DATA-REQUEST   OF UBOC-COMM-INFO
		//             OR FETCH-ALL-DATA-REQUEST       OF UBOC-COMM-INFO
		//             OR DELETE-DATA-REQUEST          OF UBOC-COMM-INFO
		//             OR CASCADING-DELETE             OF UBOC-COMM-INFO
		//               PERFORM 5210-BUILD-CHILD-BUFFER
		//           ELSE
		//               MOVE SPACES TO UBOC-APP-DATA-BUFFER
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyIssReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyPndReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchIssuedRowsOnlyReq()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchPendingDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchAllDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isDeleteDataRequest()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isCascadingDelete()) {
			// COB_CODE: PERFORM 5210-BUILD-CHILD-BUFFER
			buildChildBuffer();
		} else {
			// COB_CODE: MOVE SPACES TO UBOC-APP-DATA-BUFFER
			dfhcommarea.getUbocRecord().setAppDataBuffer("");
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 5200-LINK-TO-CHILD-MODULE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 5200-LINK-TO-CHILD-MODULE-X
			return;
		}
		// COB_CODE: EXEC CICS LINK
		//              PROGRAM  (WS-CHILD-BUS-OBJ-MDU)
		//              COMMAREA (UBOC-RECORD)
		//              LENGTH   (LENGTH OF UBOC-RECORD)
		//              RESP     (WS-RESPONSE-CODE)
		//              RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0D0008", execContext).commarea(dfhcommarea.getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD)
				.link(ws.getWsNotSpecificMisc().getChildBusObjMdu());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 5200-LINK-TO-CHILD-MODULE-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			//******      MOVE WS-CALL-MODULE-NM
			// COB_CODE: MOVE WS-CHILD-BUS-OBJ-MDU
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsNotSpecificMisc().getChildBusObjMdu());
			// COB_CODE: MOVE '5200-LINK-TO-CHILD-MODULE'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5200-LINK-TO-CHILD-MODULE");
			// COB_CODE: MOVE 'LINK TO CHILD MODULE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO CHILD MODULE FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5200-LINK-TO-CHILD-MODULE-X
			return;
		}
	}

	/**Original name: 5210-BUILD-CHILD-BUFFER_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  BUILD FILE LAYOUT OF THE CHILD TO BE CALLED.
	 * ****************************************************************
	 * * GENERALLY, A PARENT CANNOT BUILD AN EXACT KEY FOR A CHILD.</pre>*/
	private void buildChildBuffer() {
		// COB_CODE: IF FETCH-WITH-EXACT-KEY-REQUEST OF UBOC-COMM-INFO
		//               SET FETCH-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyRequest()) {
			// COB_CODE: SET FETCH-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchDataRequest();
		}
		// COB_CODE: IF FETCH-WITH-EXACT-KEY-ISS-REQ OF UBOC-COMM-INFO
		//                 OF UBOC-COMM-INFO TO TRUE
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyIssReq()) {
			// COB_CODE: SET FETCH-ISSUED-ROWS-ONLY-REQ
			//             OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchIssuedRowsOnlyReq();
		}
		// COB_CODE: IF FETCH-WITH-EXACT-KEY-PND-REQ OF UBOC-COMM-INFO
		//               SET FETCH-PENDING-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyPndReq()) {
			// COB_CODE: SET FETCH-PENDING-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchPendingDataRequest();
		}
		// COB_CODE: IF DELETE-DATA-REQUEST OF UBOC-COMM-INFO
		//               SET CASCADING-DELETE OF UBOC-COMM-INFO TO TRUE
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isDeleteDataRequest()) {
			// COB_CODE: SET CASCADING-DELETE OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setCascadingDelete();
		}
		// COB_CODE: IF FETCH-ALL-DATA-REQUEST OF UBOC-COMM-INFO
		//               SET FETCH-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchAllDataRequest()) {
			// COB_CODE: SET FETCH-DATA-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().setFetchDataRequest();
		}
		// COB_CODE: PERFORM 15210-BUILD-CHILD-BUFFER.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=3481, because the code is unreachable.
	}

	/**Original name: 5400-READ-BUS-OBJ-XREF_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  RETRIEVE THE BUS OBJ MODULE FOR THE BUSINESS OBJECT NAME
	 * *****************************************************************</pre>*/
	private void readBusObjXref() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE WS-CHILD-BUS-OBJ-NM TO HBMX-BUS-OBJ-NM.
		ws.getDclhalBoMduXrfV().setBusObjNm(ws.getWsNotSpecificMisc().getChildBusObjNm());
		// COB_CODE: EXEC SQL
		//               SELECT HBMX_BOBJ_MDU_NM
		//               INTO  :HBMX-BOBJ-MDU-NM
		//               FROM   HAL_BO_MDU_XRF_V
		//               WHERE BUS_OBJ_NM = :HBMX-BUS-OBJ-NM
		//           END-EXEC.
		ws.getDclhalBoMduXrfV()
				.setBobjMduNm(halBoMduXrfVDao.selectByHbmxBusObjNm(ws.getDclhalBoMduXrfV().getBusObjNm(), ws.getDclhalBoMduXrfV().getBobjMduNm()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 5400-READ-BUS-OBJ-XREF-X
		//               WHEN OTHER
		//                   GO TO 5400-READ-BUS-OBJ-XREF-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-LOG-ERROR                             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQD-DATA-NOT-FOUND OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspReqdDataNotFound();
			// COB_CODE: MOVE '5400-READ-BUS-OBJ-XREF'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5400-READ-BUS-OBJ-XREF");
			// COB_CODE: MOVE 'EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5400-READ-BUS-OBJ-XREF-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF");
			// COB_CODE: MOVE '5400-READ-BUS-OBJ-XREF'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5400-READ-BUS-OBJ-XREF");
			// COB_CODE: MOVE 'SELECT FROM OBJ XREF TABLE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT FROM OBJ XREF TABLE FAILED");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5400-READ-BUS-OBJ-XREF-X
			return;
		}
		//* CHECK MODULE NAME PRESENT IN OBJ XREF ROW RETURNED
		// COB_CODE: IF HBMX-BOBJ-MDU-NM = SPACES
		//               GO TO 5400-READ-BUS-OBJ-XREF-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclhalBoMduXrfV().getBobjMduNm())) {
			// COB_CODE: SET WS-LOG-ERROR                              TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE    OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED   OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUIRED-FIELD-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequiredFieldBlank();
			// COB_CODE: MOVE '5400-READ-BUS-OBJ-XREF'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5400-READ-BUS-OBJ-XREF");
			// COB_CODE: MOVE 'MODULE NAME ON OBJ XREF ROW IS BLANK'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MODULE NAME ON OBJ XREF ROW IS BLANK");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//                  'HBMX-BOBJ-MDU-NM=' HBMX-BOBJ-MDU-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";", "HBMX-BOBJ-MDU-NM=", ws.getDclhalBoMduXrfV().getBobjMduNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5400-READ-BUS-OBJ-XREF-X
			return;
		}
		//* USE MODULE NAME FROM OBJ XREF ROW
		// COB_CODE: MOVE HBMX-BOBJ-MDU-NM TO WS-CHILD-BUS-OBJ-MDU.
		ws.getWsNotSpecificMisc().setChildBusObjMdu(ws.getDclhalBoMduXrfV().getBobjMduNm());
	}

	/**Original name: 6000-AUDIT-TRAIL_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   CALL THE AUDIT MODULE.
	 * ****************************************************************</pre>*/
	private void auditTrail() {
		// COB_CODE: PERFORM 6100-SETUP-AUDIT-FIELDS.
		setupAuditFields();
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM  (HBAT-MODULE-NM)
		//                COMMAREA (UBOC-RECORD)
		//                LENGTH   (LENGTH OF UBOC-RECORD)
		//                RESP     (WS-RESPONSE-CODE)
		//                RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0D0008", execContext).commarea(dfhcommarea.getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD)
				.link(ws.getDclhalBoAudTgrV().getModuleNm());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 6000-AUDIT-TRAIL-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE HBAT-MODULE-NM
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getDclhalBoAudTgrV().getModuleNm());
			// COB_CODE: MOVE '6000-AUDIT-TRAIL'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6000-AUDIT-TRAIL");
			// COB_CODE: MOVE 'LINK TO AUDIT MODULE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO AUDIT MODULE FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 6000-AUDIT-TRAIL-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 6000-AUDIT-TRAIL-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 6000-AUDIT-TRAIL-X
			return;
		}
	}

	/**Original name: 6100-SETUP-AUDIT-FIELDS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  SET UP THE LINKAGE FOR THE AUDIT RECORD.
	 * ****************************************************************</pre>*/
	private void setupAuditFields() {
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO UBOC-AUDT-BUS-OBJ-NM.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocAuditProcessingInfo().setAudtBusObjNm(ws.getWsSpecificMisc().getProgramName());
		// COB_CODE: MOVE SPACES TO UBOC-AUDT-EVENT-DATA.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocAuditProcessingInfo().setAudtEventData("");
		// COB_CODE: PERFORM 16100-SETUP-AUDIT-FIELDS.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=3621, because the code is unreachable.
	}

	/**Original name: 7000-CHECK-AUTHORIZATION_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  LINK TO REQUIRED DATA PRIVACY MODULE.
	 * ****************************************************************</pre>*/
	private void checkAuthorization() {
		// COB_CODE: INITIALIZE UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer("");
		// COB_CODE: INITIALIZE WS-DATA-PRIVACY-INFO.
		initWsDataPrivacyInfo();
		// COB_CODE: PERFORM 17000-CHECK-AUTHORIZATION-A.
		checkAuthorizationA();
		// COB_CODE: MOVE WS-DATA-PRIVACY-INFO TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getWsDataPrivacyInfoFormatted());
		// COB_CODE: MOVE LENGTH OF WS-DATA-PRIVACY-INFO
		//             TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) Xz0d0008Data.Len.WS_DATA_PRIVACY_INFO));
	}

	/**Original name: CSDF-CALL-DATA-PRIVACY<br>
	 * <pre>* ADD CODE TO READ FOR A GENERATED DP MODULE FOR THE BDO ON
	 * * HAL_BO_MDU_XREF.  IF ONE EXISTS, CALL IT WITH THE LINKAGE
	 * * ESTABLISHED ABOVE.
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  CONTROL PART OF DATA PRIVACY       *BUSINESS FRAMEWORK**
	 * **CSC *  (THIS DOES NOT VARY BETWEEN BDOS)  *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 JAN. 03, 2002          *BUSINESS FRAMEWORK**
	 * **CSC *  18384                              *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * **********************************************************
	 *  READ THE HAL_MDU_XRF TABLE FOR THE SET DEFAULT/ DATA    *
	 *  PRIVACY MODULE AFFILIATED WITH THIS BDO, AND LINK TO IT *
	 * **********************************************************</pre>*/
	private void csdfCallDataPrivacy() {
		// COB_CODE: MOVE WS-BUS-OBJ-NM TO HBMX-BUS-OBJ-NM.
		ws.getDclhalBoMduXrfV().setBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: EXEC SQL
		//             SELECT HBMX_DP_DFL_MDU_NM
		//                INTO :HBMX-DP-DFL-MDU-NM
		//                FROM HAL_BO_MDU_XRF_V
		//               WHERE  BUS_OBJ_NM  = :HBMX-BUS-OBJ-NM
		//                 AND  HBMX_DP_DFL_MDU_NM <> ' '
		//           END-EXEC.
		ws.getDclhalBoMduXrfV()
				.setDpDflMduNm(halBoMduXrfVDao.selectByHbmxBusObjNm1(ws.getDclhalBoMduXrfV().getBusObjNm(), ws.getDclhalBoMduXrfV().getDpDflMduNm()));
		// COB_CODE: EVALUATE SQLCODE
		//              WHEN ZERO
		//                  CONTINUE
		//              WHEN 100
		//                 GO TO CSDF-CALL-DATA-PRIVACY-X
		//              WHEN OTHER
		//                 GO TO CSDF-CALL-DATA-PRIVACY-X
		//           END-EVALUATE.
		if (sqlca.getSqlcode() == 0) {
			// COB_CODE: CONTINUE
			//continue
		} else if (sqlca.getSqlcode() == 100) {
			// COB_CODE: SET DPER-DATA-PRIV-CHECK-OK TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocDataPrivRetCode().setDataPrivCheckOk();
			// COB_CODE: GO TO CSDF-CALL-DATA-PRIVACY-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF_V");
			// COB_CODE: MOVE 'CSDF-CALL-DATA-PRIVACY'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("CSDF-CALL-DATA-PRIVACY");
			// COB_CODE: MOVE 'UNEXPECTED DB2 RETURN CODE'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UNEXPECTED DB2 RETURN CODE");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO CSDF-CALL-DATA-PRIVACY-X
			return;
		}
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (HBMX-DP-DFL-MDU-NM)
		//               COMMAREA (UBOC-RECORD)
		//               LENGTH   (LENGTH OF UBOC-RECORD)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0D0008", execContext).commarea(dfhcommarea.getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD)
				.link(ws.getDclhalBoMduXrfV().getDpDflMduNm());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                   WHEN 0
		//                      CONTINUE
		//                   WHEN OTHER
		//                      GO TO CSDF-CALL-DATA-PRIVACY-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE HBMX-DP-DFL-MDU-NM
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getDclhalBoMduXrfV().getDpDflMduNm());
			// COB_CODE: MOVE 'CSDF-CALL-DATA-PRIVACY'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("CSDF-CALL-DATA-PRIVACY");
			// COB_CODE: MOVE 'ERROR LINKING TO DP/DEFAULT MODULE'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR LINKING TO DP/DEFAULT MODULE");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO CSDF-CALL-DATA-PRIVACY-X
			return;
		}
	}

	/**Original name: CSDF-CALL-DATA-PRIVACY-X<br>*/
	private void csdfCallDataPrivacyX() {
		// COB_CODE: EXIT.
		//exit
		//* THE OLD DATA PRIVACY PROCESSING HAS BEEN REPLACED BY THE
		//* DATA PRIVACY/SET DEFAULTS PROCESSING.  THEREFORE, THIS CALL
		//* IS NO LONGER NEEDED.
		//**  EXEC SQL
		//**      INCLUDE HALCSDP
		//**  END-EXEC.
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER TO WS-DATA-PRIVACY-INFO.
		ws.setWsDataPrivacyInfoFormatted(dfhcommarea.getUbocRecord().getAppDataBufferFormatted());
		// COB_CODE: PERFORM 17000-CHECK-AUTHORIZATION-B.
		checkAuthorizationB();
		// COB_CODE: INITIALIZE UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE 0 TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) 0));
	}

	/**Original name: 9999-TERMINATE-MODULE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PERFORM ANY REQUIRED TERMINATION PROCESSING. THIS
	 *  INCLUDES THE CALL TO THE PERFORMANCE MONITORING MODULE.
	 * ****************************************************************</pre>*/
	private void terminateModule() {
		// COB_CODE: PERFORM 19999-TERMINATE.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=3678, because the code is unreachable.
		//* COMMENTED OUT.  NOW USING PERFORMANCE MONITORING 'END' CODE
		//* COPYBOOK TO HANDLE THIS PROCESSING.
		//**** SET UP PERFORMANCE MODULE PARMS HERE.
		//                                                                 27050000
		//    SET HALRMON-END-FUNCTION   TO TRUE.                          27060000
		//    MOVE WS-BUS-OBJ-NM         TO HALRMON-BUS-OBJ-NM.            27070000
		//    MOVE WS-PROGRAM-NAME       TO HALRMON-BUS-MOD-NM.            27080000
		//    MOVE SPACES                TO HALRMON-INFO-LABEL.            27090000
		//    MOVE +0                    TO HALRMON-INFO-LENGTH.           27100000
		//    MOVE SPACES                TO HALRMON-DUMMY-INFO-TEXT.       27110000
		//                                                                 27120000
		//    CALL WS-HALRMON-NAME USING                                   27130000
		//          DFHEIBLK,                                              27140000
		//          DFHCOMMAREA,                                           27150000
		//          UBOC-RECORD,                                           27160000
		//          HALRMON-INPUT-LINKAGE,                                 27170000
		//          HALRMON-DUMMY-INFO-TEXT.                               27180000
		//* INVOKE PERFORMANCE MONITORING 'END' PROCESSING.                27030000
		// COB_CODE: PERFORM MONE-PERF-MON-END.
		monePerfMonEnd();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9999-TERMINATE-MODULE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9999-TERMINATE-MODULE-X
			return;
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning()
				&& dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsSpecificMisc().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsSpecificMisc().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0D0008", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWsSpecificMisc().getProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getUbocRecord().getCommInfo()
				.setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWsSpecificMisc().getProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getUbocRecord().getCommInfo()
				.setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWsSpecificMisc().getApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: MONB-PERF-MON-BEGIN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL PERFORMANCE MONITORING API IN 'BEGIN' MODE                *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void monbPerfMonBegin() {
		Halrmon halrmon = null;
		// COB_CODE: SET HALRMON-BEGIN-FUNCTION TO TRUE.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().getFunction().setBeginFunction();
		// COB_CODE: MOVE WS-BUS-OBJ-NM         TO HALRMON-BUS-OBJ-NM.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().setBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: MOVE WS-PROGRAM-NAME       TO HALRMON-BUS-MOD-NM.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().setBusModNm(ws.getWsSpecificMisc().getProgramName());
		// COB_CODE: MOVE SPACES                TO HALRMON-INFO-LABEL.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().setInfoLabel("");
		// COB_CODE: MOVE +0                    TO HALRMON-INFO-LENGTH.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().setInfoLength(((short) 0));
		// COB_CODE: MOVE SPACES                TO HALRMON-DUMMY-INFO-TEXT.
		ws.getHalrmonPerfMonitorStorage().setDummyInfoText(Types.SPACE_CHAR);
		// COB_CODE: CALL WS-HALRMON-NAME USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                HALRMON-INPUT-LINKAGE
		//                HALRMON-DUMMY-INFO-TEXT.
		halrmon = Halrmon.getInstance();
		halrmon.run(execContext, dfhcommarea, dfhcommarea, ws.getHalrmonPerfMonitorStorage().getInputLinkage(), ws.getHalrmonPerfMonitorStorage());
	}

	/**Original name: MONE-PERF-MON-END_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL PERFORMANCE MONITORING API IN 'END' MODE                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void monePerfMonEnd() {
		Halrmon halrmon = null;
		// COB_CODE: SET HALRMON-END-FUNCTION TO TRUE.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().getFunction().setEndFunction();
		// COB_CODE: MOVE WS-BUS-OBJ-NM         TO HALRMON-BUS-OBJ-NM.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().setBusObjNm(ws.getWsSpecificMisc().getBusObjNm());
		// COB_CODE: MOVE WS-PROGRAM-NAME       TO HALRMON-BUS-MOD-NM.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().setBusModNm(ws.getWsSpecificMisc().getProgramName());
		// COB_CODE: MOVE SPACES                TO HALRMON-INFO-LABEL.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().setInfoLabel("");
		// COB_CODE: MOVE +0                    TO HALRMON-INFO-LENGTH.
		ws.getHalrmonPerfMonitorStorage().getInputLinkage().setInfoLength(((short) 0));
		// COB_CODE: MOVE SPACES                TO HALRMON-DUMMY-INFO-TEXT.
		ws.getHalrmonPerfMonitorStorage().setDummyInfoText(Types.SPACE_CHAR);
		// COB_CODE: CALL WS-HALRMON-NAME USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                HALRMON-INPUT-LINKAGE
		//                HALRMON-DUMMY-INFO-TEXT.
		halrmon = Halrmon.getInstance();
		halrmon.run(execContext, dfhcommarea, dfhcommarea, ws.getHalrmonPerfMonitorStorage().getInputLinkage(), ws.getHalrmonPerfMonitorStorage());
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowKeyReplaceStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 10200-INITIALIZE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  BDO SPECIFIC INITIALIZATION.
	 * ****************************************************************
	 * * IF HISTORY IS KEPT WHEN THIS BUSINESS OBJECT IS UPDATED ON
	 * * THE SERIES 3 DATABASE THEN SET WS-IS-HISTORIZED TO TRUE.
	 * * IF NOT, THEN SET WS-IS-NOT-HISTORIZED TO TRUE.
	 * * SET THE HISTORIZED FLAG.</pre>*/
	private void initialize1() {
		// COB_CODE: SET WS-IS-NOT-HISTORIZED    TO TRUE.
		ws.getWsBdoSwitches().getHistorizedSw().setWsIsNotHistorized();
	}

	/**Original name: 10230-GET-MAX-UMT-HDR-SEQ_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  MOVE LENGTH OF LAYOUT ROW TO UHDR-MSG-BUS-OBJ-DATA-LEN.
	 * ****************************************************************</pre>*/
	private void getMaxUmtHdrSeq1() {
		// COB_CODE: MOVE LENGTH OF XZC008-ACT-NOT-POL-REC-ROW
		//                                       TO UHDR-MSG-BUS-OBJ-DATA-LEN.
		ws.getHalluhdr().setMsgBusObjDataLen(((short) Xzc008ActNotPolRecRow.Len.XZC008_ACT_NOT_POL_REC_ROW));
	}

	/**Original name: 10310-READ-REQ-MSG-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  MOVE URWM DATA TO ROW LAYOUT.
	 * ****************************************************************</pre>*/
	private void readReqMsgUmt1() {
		// COB_CODE: MOVE URQM-MSG-DATA(1:URQM-BUS-OBJ-DATA-LENGTH)
		//                                       TO XZC008-ACT-NOT-POL-REC-ROW.
		ws.getXzc008ActNotPolRecRow().setXzc008ActNotPolRecRowFormatted(
				ws.getUrqmCommon().getMsgDataFormatted().substring((1) - 1, ws.getUrqmCommon().getBusObjDataLength()));
	}

	/**Original name: 10330-PROCESS-PASSED-REQUEST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  MOVE UBOC APP DATA BUFFER TO ROW LAYOUT.
	 * ****************************************************************</pre>*/
	private void processPassedRequest1() {
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER
		//               (1:LENGTH OF XZC008-ACT-NOT-POL-REC-ROW)
		//                                       TO XZC008-ACT-NOT-POL-REC-ROW.
		ws.getXzc008ActNotPolRecRow().setXzc008ActNotPolRecRowFormatted(
				dfhcommarea.getUbocRecord().getAppDataBufferFormatted().substring((1) - 1, Xzc008ActNotPolRecRow.Len.XZC008_ACT_NOT_POL_REC_ROW));
	}

	/**Original name: 11110-BLD-PRIM-KEY-REPL-VALS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  OBTAIN THE VALUES OF EACH COMPONENT OF THE PRIMARY KEY.
	 *  PRIMARY KEY VALUES MAY BE SOURCED AS FOLLOWS:
	 *  A. SUPPLIED BY THE FRONT END.
	 *  B. BUILT BY A PARENT BDO AND THUS RETRIEVABLE FROM THE
	 *     KEY REPLACEMENT UMT.
	 *  C. GENERATED BY THIS MODULE. GENERATED KEYS SHOULD BE STORED
	 *     ON KEY REPLACEMENT UMT FOR POSSIBLE SUBSEQUENT USE BY
	 *     A CHILD.
	 *     THERE ARE 2 VARIATIONS OF GENERATED KEY:
	 *     I.  KEY GENERATED IN THIS MODULE WHICH BEARS NO RELATION
	 *         TO PREVIOUS OR SUBSEQUENT KEYS (E.G. A TECHNICAL KEY).
	 *     II. KEY GENERATED WHICH IS RELATED TO A PREVIOUS OR
	 *         SUBSEQUENT KEY (E.G. A SEQUENCE NUMBER).
	 *         THE LAST SEQUENT NUMBER ALLOCATED IS ITSELF STORED
	 *         ON THE KEY REPLACEMENT UMT.
	 * ****************************************************************
	 * * OBTAIN CSR-ACT-NBR</pre>*/
	private void bldPrimKeyReplVals() {
		// COB_CODE: IF XZC008-CSR-ACT-NBR-KCRE NOT = SPACES
		//             OR
		//              XZC008-CSR-ACT-NBR = SPACES
		//               PERFORM 11115-OBT-CSR-ACT-NBR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getCsrActNbrKcre())
				|| Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getCsrActNbr())) {
			// COB_CODE: PERFORM 11115-OBT-CSR-ACT-NBR
			obtCsrActNbr();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11110-BLD-PRIM-KEY-REPL-VALS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11110-BLD-PRIM-KEY-REPL-VALS-X
			return;
		}
		//* OBTAIN NOT-PRC-TS
		// COB_CODE: IF XZC008-NOT-PRC-TS-KCRE NOT = SPACES
		//             OR
		//              XZC008-NOT-PRC-TS = SPACES
		//               PERFORM 11115-OBT-NOT-PRC-TS
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getNotPrcTsKcre())
				|| Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getNotPrcTs())) {
			// COB_CODE: PERFORM 11115-OBT-NOT-PRC-TS
			obtNotPrcTs();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11110-BLD-PRIM-KEY-REPL-VALS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11110-BLD-PRIM-KEY-REPL-VALS-X
			return;
		}
		//* OBTAIN POL-NBR
		// COB_CODE: IF XZC008-POL-NBR-KCRE NOT = SPACES
		//             OR
		//              XZC008-POL-NBR = SPACES
		//               PERFORM 11115-OBT-POL-NBR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getPolNbrKcre())
				|| Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getPolNbr())) {
			// COB_CODE: PERFORM 11115-OBT-POL-NBR
			obtPolNbr();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11110-BLD-PRIM-KEY-REPL-VALS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11110-BLD-PRIM-KEY-REPL-VALS-X
			return;
		}
		//* OBTAIN REC-SEQ-NBR
		// COB_CODE: IF XZC008-REC-SEQ-NBR-KCRE NOT = SPACES
		//             OR
		//              XZC008-REC-SEQ-NBR = SPACES
		//               PERFORM 11115-OBT-REC-SEQ-NBR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getRecSeqNbrKcre())
				|| Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getRecSeqNbrFormatted())) {
			// COB_CODE: PERFORM 11115-OBT-REC-SEQ-NBR
			obtRecSeqNbr();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11110-BLD-PRIM-KEY-REPL-VALS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11110-BLD-PRIM-KEY-REPL-VALS-X
			return;
		}
	}

	/**Original name: 11115-OBT-CSR-ACT-NBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  OBTAIN PRIMARY KEY: CSR-ACT-NBR                                *
	 *  VALUE SUPPLIED BY FRONT END                                    *
	 *  ERROR LOGGED IF KEY FIELD IS BLANK OR -KCRE IS POPULATED       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void obtCsrActNbr() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
		//                                       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
		// COB_CODE: MOVE '11115-OBT-CSR-ACT-NBR'
		//                                       TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11115-OBT-CSR-ACT-NBR");
		// COB_CODE: MOVE 'XZC008-CSR-ACT-NBR MUST BE FRONT END SUPPLIED'
		//                                       TO EFAL-ERR-COMMENT.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("XZC008-CSR-ACT-NBR MUST BE FRONT END SUPPLIED");
		// COB_CODE: STRING 'XZC008-CSR-ACT-NBR-KCRE='
		//                   XZC008-CSR-ACT-NBR-KCRE ';'
		//                  'XZC008-CSR-ACT-NBR='
		//                   XZC008-CSR-ACT-NBR ';'
		//               DELIMITED BY SIZE
		//               INTO EFAL-OBJ-DATA-KEY
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
				new String[] { "XZC008-CSR-ACT-NBR-KCRE=", ws.getXzc008ActNotPolRecRow().getCsrActNbrKcreFormatted(), ";", "XZC008-CSR-ACT-NBR=",
						ws.getXzc008ActNotPolRecRow().getCsrActNbrFormatted(), ";" });
		ws.getWsEstoInfo().getEstoDetailBuffer()
				.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 11115-OBT-NOT-PRC-TS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  OBTAIN PRIMARY KEY: NOT-PRC-TS                                 *
	 *  VALUE SUPPLIED BY FRONT END                                    *
	 *  ERROR LOGGED IF KEY FIELD IS BLANK OR -KCRE IS POPULATED       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void obtNotPrcTs() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
		//                                       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
		// COB_CODE: MOVE '11115-OBT-NOT-PRC-TS'
		//                                       TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11115-OBT-NOT-PRC-TS");
		// COB_CODE: MOVE 'XZC008-NOT-PRC-TS MUST BE FRONT END SUPPLIED'
		//                                       TO EFAL-ERR-COMMENT.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("XZC008-NOT-PRC-TS MUST BE FRONT END SUPPLIED");
		// COB_CODE: STRING 'XZC008-NOT-PRC-TS-KCRE='
		//                   XZC008-NOT-PRC-TS-KCRE ';'
		//                  'XZC008-NOT-PRC-TS='
		//                   XZC008-NOT-PRC-TS ';'
		//               DELIMITED BY SIZE
		//               INTO EFAL-OBJ-DATA-KEY
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
				new String[] { "XZC008-NOT-PRC-TS-KCRE=", ws.getXzc008ActNotPolRecRow().getNotPrcTsKcreFormatted(), ";", "XZC008-NOT-PRC-TS=",
						ws.getXzc008ActNotPolRecRow().getNotPrcTsFormatted(), ";" });
		ws.getWsEstoInfo().getEstoDetailBuffer()
				.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 11115-OBT-POL-NBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  OBTAIN PRIMARY KEY: POL-NBR                                    *
	 *  VALUE SUPPLIED BY FRONT END                                    *
	 *  ERROR LOGGED IF KEY FIELD IS BLANK OR -KCRE IS POPULATED       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void obtPolNbr() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
		//                                       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
		// COB_CODE: MOVE '11115-OBT-POL-NBR'
		//                                       TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11115-OBT-POL-NBR");
		// COB_CODE: MOVE 'XZC008-POL-NBR MUST BE FRONT END SUPPLIED'
		//                                       TO EFAL-ERR-COMMENT.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("XZC008-POL-NBR MUST BE FRONT END SUPPLIED");
		// COB_CODE: STRING 'XZC008-POL-NBR-KCRE='
		//                   XZC008-POL-NBR-KCRE ';'
		//                  'XZC008-POL-NBR='
		//                   XZC008-POL-NBR ';'
		//               DELIMITED BY SIZE
		//               INTO EFAL-OBJ-DATA-KEY
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
				new String[] { "XZC008-POL-NBR-KCRE=", ws.getXzc008ActNotPolRecRow().getPolNbrKcreFormatted(), ";", "XZC008-POL-NBR=",
						ws.getXzc008ActNotPolRecRow().getPolNbrFormatted(), ";" });
		ws.getWsEstoInfo().getEstoDetailBuffer()
				.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 11115-OBT-REC-SEQ-NBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  OBTAIN PRIMARY KEY: REC-SEQ-NBR                                *
	 *  VALUE SUPPLIED BY FRONT END                                    *
	 *  ERROR LOGGED IF KEY FIELD IS BLANK OR -KCRE IS POPULATED       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void obtRecSeqNbr() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
		//                                       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
		// COB_CODE: MOVE '11115-OBT-REC-SEQ-NBR'
		//                                       TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11115-OBT-REC-SEQ-NBR");
		// COB_CODE: MOVE 'XZC008-REC-SEQ-NBR MUST BE FRONT END SUPPLIED'
		//                                       TO EFAL-ERR-COMMENT.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("XZC008-REC-SEQ-NBR MUST BE FRONT END SUPPLIED");
		// COB_CODE: STRING 'XZC008-REC-SEQ-NBR-KCRE='
		//                   XZC008-REC-SEQ-NBR-KCRE ';'
		//                  'XZC008-REC-SEQ-NBR='
		//                   XZC008-REC-SEQ-NBR ';'
		//               DELIMITED BY SIZE
		//               INTO EFAL-OBJ-DATA-KEY
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
				new String[] { "XZC008-REC-SEQ-NBR-KCRE=", ws.getXzc008ActNotPolRecRow().getRecSeqNbrKcreFormatted(), ";", "XZC008-REC-SEQ-NBR=",
						ws.getXzc008ActNotPolRecRow().getRecSeqNbrAsString(), ";" });
		ws.getWsEstoInfo().getEstoDetailBuffer()
				.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 11400-ITE-ADDL-ACTIONS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  APPLICATIONS REQUIRING SIMPLE-EDITS TO BE PERFORMED PRIOR TO
	 *  INVOKING AN APPLICATION SPECIFIC ACTION CODE.
	 *  THIS PARAGRAPH WILL BE INVOKED FROM PARAGRAPH
	 *  1400-ITE-ADDL-ACTIONS.
	 * ****************************************************************
	 *  STANDARD DEFAULT CODE.</pre>*/
	private void iteAddlActions1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET BUSP-INV-ACTION-CODE    TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvActionCode();
		// COB_CODE: MOVE '11400-ITE-ADDL-ACTIONS'
		//                                       TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11400-ITE-ADDL-ACTIONS");
		// COB_CODE: MOVE 'INVALID ACTION CODE'  TO EFAL-ERR-COMMENT.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID ACTION CODE");
		// COB_CODE: STRING 'URQM-ACTION-CODE OF URQM-COMMON='
		//                   URQM-ACTION-CODE OF URQM-COMMON ';'
		//               DELIMITED BY SIZE
		//               INTO EFAL-OBJ-DATA-KEY
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-ACTION-CODE OF URQM-COMMON=",
				ws.getUrqmCommon().getActionCode().getActionCodeFormatted(), ";");
		ws.getWsEstoInfo().getEstoDetailBuffer()
				.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 11840-SIMPLE-INTERNAL-EDITS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PERFORM SIMPLE EDITING ON EACH FIELD OF OBJECT BEING PROCESSED.
	 * ****************************************************************
	 * ****************************************************************
	 *  CERTAIN BDOS REQUIRE ADDITIONAL VALUES FROM PARENT OR RELATED
	 *  BDOS TO PERFORM SOME OF THE SIMPLE EDITING.  AN EXAMPLE WOULD
	 *  BE THAT MANY UWS TABLES REQUIRE THE LINE OF BUSINESS TO READ
	 *  SUPPORT TABLES; HOWEVER, THIS FIELD IS NOT CONTAINED IN EVERY
	 *  BDO.  THIS PERFORM WILL INVOKE A SECTION THAT WILL RETRIEVE
	 *  ANY REQUIRED EXTERNAL VALUES.
	 * ****************************************************************</pre>*/
	private void simpleInternalEdits1() {
		// COB_CODE: PERFORM 11840-SE-GET-EXTERNAL-VALS.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=4126, because the code is unreachable.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11840-SIMPLE-INTERNAL-EDITS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11840-SIMPLE-INTERNAL-EDITS-X
			return;
		}
		// COB_CODE: IF UPDATE-DATA-REQUEST OF URQM-COMMON
		//             OR
		//              UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR
		//              DELETE-DATA-REQUEST OF URQM-COMMON
		//               PERFORM 11840-SE-TRANS-PROCESS-DT
		//           END-IF.
		if (ws.getUrqmCommon().getActionCode().isUpdateDataRequest() || ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest()
				|| ws.getUrqmCommon().getActionCode().isDeleteDataRequest()) {
			// COB_CODE: PERFORM 11840-SE-TRANS-PROCESS-DT
			seTransProcessDt();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11840-SIMPLE-INTERNAL-EDITS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11840-SIMPLE-INTERNAL-EDITS-X
			return;
		}
		// COB_CODE: PERFORM 11840-SE-CSR-ACT-NBR.
		seCsrActNbr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11840-SIMPLE-INTERNAL-EDITS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11840-SIMPLE-INTERNAL-EDITS-X
			return;
		}
		// COB_CODE: PERFORM 11840-SE-NOT-PRC-TS.
		seNotPrcTs();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11840-SIMPLE-INTERNAL-EDITS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11840-SIMPLE-INTERNAL-EDITS-X
			return;
		}
		// COB_CODE: PERFORM 11840-SE-POL-NBR.
		sePolNbr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11840-SIMPLE-INTERNAL-EDITS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11840-SIMPLE-INTERNAL-EDITS-X
			return;
		}
		// COB_CODE: PERFORM 11840-SE-REC-SEQ-NBR.
		seRecSeqNbr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11840-SIMPLE-INTERNAL-EDITS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11840-SIMPLE-INTERNAL-EDITS-X
			return;
		}
		//****************************************************************
		//
		// THIS AREA IS FOR THE PROGRAMMER TO CODE THE PERFORM FOR ANY
		// GLOBAL SIMPLE EDITS THAT NEED TO BE EXECUTED AGAINST THE
		// TABLE. ONLY THE PERFORM OF THE SECTION SHOULD BE CODED HERE.
		// THE NEW GLOBAL SIMPLE EDIT SECTIONS THEMSELVES SHOULD BE HAND
		// CODED BELOW AFTER THE GENERATED SIMPLE EDIT SECTIONS.
		//
		//****************************************************************
		// COB_CODE: PERFORM 11840-SE-GLOBAL-TAB-EDIT.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=XZ0D0008.CBL:line=4177, because the code is unreachable.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 11840-SIMPLE-INTERNAL-EDITS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 11840-SIMPLE-INTERNAL-EDITS-X
			return;
		}
	}

	/**Original name: 11840-SE-TRANS-PROCESS-DT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PERFORM SIMPLE EDITING ON COLUMN TRANS-PROCESS-DT              *
	 *                                                                 *
	 * *****************************************************************
	 * * FIELD: TRANS-PROCESS-DT   (DATE, NOT NULLABLE, NOT SIGNED)
	 * * TRANS-PROCESS-DT IS OPTIONALLY SUPPLIED BY FRONT END</pre>*/
	private void seTransProcessDt() {
		Halrvdt1 halrvdt1 = null;
		StringParam xzc008TransProcessDt = null;
		// COB_CODE: IF XZC008-TRANS-PROCESS-DT = SPACES
		//               GO TO 11840-SE-TRANS-PROCESS-DT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getTransProcessDt())) {
			// COB_CODE: GO TO 11840-SE-TRANS-PROCESS-DT-X
			return;
		}
		//* VALIDATE COLUMN
		// COB_CODE: CALL 'HALRVDT1' USING XZC008-TRANS-PROCESS-DT
		//                                 WS-VALID-DATE-SW.
		halrvdt1 = Halrvdt1.getInstance();
		xzc008TransProcessDt = new StringParam(ws.getXzc008ActNotPolRecRow().getTransProcessDt(), Xzc008ActNotPolRecRow.Len.TRANS_PROCESS_DT);
		halrvdt1.run(xzc008TransProcessDt, ws.getWsBdoSwitches().getValidDateSw());
		ws.getXzc008ActNotPolRecRow().setTransProcessDt(xzc008TransProcessDt.getString());
		// COB_CODE: IF WS-NOT-VALID-DATE
		//               PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
		//           END-IF.
		if (ws.getWsBdoSwitches().getValidDateSw().isWsNotValidDate()) {
			// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'  TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile("ACT_NOT_POL_REC");
			// COB_CODE: MOVE 'TRANS_PROCESS_DT' TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("TRANS_PROCESS_DT");
			// COB_CODE: MOVE 'GEN_DATFMT'       TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("GEN_DATFMT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE XZN008-TRANS-PROCESS-DT
			//                                   TO WS-NONLOG-ERR-COL1-NAME
			ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Name(ws.getXz0n0008().getTransProcessDt());
			// COB_CODE: MOVE XZC008-TRANS-PROCESS-DT
			//                                   TO WS-NONLOG-ERR-COL1-VALUE
			ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(ws.getXzc008ActNotPolRecRow().getTransProcessDt());
			// COB_CODE: MOVE XZN008-PROCESSING-CONTEXT-TEXT
			//                                   TO WS-NONLOG-ERR-CONTEXT-TEXT
			ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(ws.getXz0n0008().getProcessingContextTextFormatted());
			// COB_CODE: MOVE XZC008-ACT-NOT-POL-REC-KEY
			//                                   TO WS-NONLOG-ERR-CONTEXT-VALUE
			ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(ws.getXzc008ActNotPolRecRow().getActNotPolRecKeyFormatted());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
		}
	}

	/**Original name: 11840-SE-CSR-ACT-NBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PERFORM SIMPLE EDITING ON COLUMN CSR-ACT-NBR                   *
	 *                                                                 *
	 * *****************************************************************
	 * * COLUMN: CSR_ACT_NBR        (CHAR, NOT NULLABLE, NOT SIGNED)
	 * * VALIDATE COLUMN</pre>*/
	private void seCsrActNbr() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF XZC008-CSR-ACT-NBR = SPACES
		//               GO TO 11840-SE-CSR-ACT-NBR-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getCsrActNbr())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '11840-SE-CSR-ACT-NBR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11840-SE-CSR-ACT-NBR");
			// COB_CODE: MOVE 'CSR-ACT-NBR INV FORMAT OR CONTENTS'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CSR-ACT-NBR INV FORMAT OR CONTENTS");
			// COB_CODE: STRING 'XZC008-CSR-ACT-NBR='
			//                  XZC008-CSR-ACT-NBR ';'
			//                  'XZC008-ACT-NOT-POL-REC-ROW='
			//                  XZC008-ACT-NOT-POL-REC-ROW ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZC008-CSR-ACT-NBR=", ws.getXzc008ActNotPolRecRow().getCsrActNbrFormatted(), ";", "XZC008-ACT-NOT-POL-REC-ROW=",
							ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 11840-SE-CSR-ACT-NBR-X
			return;
		}
	}

	/**Original name: 11840-SE-NOT-PRC-TS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PERFORM SIMPLE EDITING ON COLUMN NOT-PRC-TS                    *
	 *                                                                 *
	 * *****************************************************************
	 * * COLUMN: NOT_PRC_TS         (TIMESTMP, NOT NULLABLE, NOT SIGNED)
	 * * VALIDATE COLUMN</pre>*/
	private void seNotPrcTs() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF XZC008-NOT-PRC-TS = SPACES
		//               GO TO 11840-SE-NOT-PRC-TS-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getNotPrcTs())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '11840-SE-NOT-PRC-TS'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11840-SE-NOT-PRC-TS");
			// COB_CODE: MOVE 'NOT-PRC-TS INV FORMAT OR CONTENTS'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NOT-PRC-TS INV FORMAT OR CONTENTS");
			// COB_CODE: STRING 'XZC008-NOT-PRC-TS='
			//                  XZC008-NOT-PRC-TS ';'
			//                  'XZC008-ACT-NOT-POL-REC-ROW='
			//                  XZC008-ACT-NOT-POL-REC-ROW ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZC008-NOT-PRC-TS=", ws.getXzc008ActNotPolRecRow().getNotPrcTsFormatted(), ";", "XZC008-ACT-NOT-POL-REC-ROW=",
							ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 11840-SE-NOT-PRC-TS-X
			return;
		}
	}

	/**Original name: 11840-SE-POL-NBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PERFORM SIMPLE EDITING ON COLUMN POL-NBR                       *
	 *                                                                 *
	 * *****************************************************************
	 * * COLUMN: POL_NBR            (CHAR, NOT NULLABLE, NOT SIGNED)
	 * * VALIDATE COLUMN</pre>*/
	private void sePolNbr() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF XZC008-POL-NBR = SPACES
		//               GO TO 11840-SE-POL-NBR-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getPolNbr())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '11840-SE-POL-NBR' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11840-SE-POL-NBR");
			// COB_CODE: MOVE 'POL-NBR INV FORMAT OR CONTENTS'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("POL-NBR INV FORMAT OR CONTENTS");
			// COB_CODE: STRING 'XZC008-POL-NBR='
			//                  XZC008-POL-NBR ';'
			//                  'XZC008-ACT-NOT-POL-REC-ROW='
			//                  XZC008-ACT-NOT-POL-REC-ROW ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZC008-POL-NBR=", ws.getXzc008ActNotPolRecRow().getPolNbrFormatted(), ";", "XZC008-ACT-NOT-POL-REC-ROW=",
							ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 11840-SE-POL-NBR-X
			return;
		}
	}

	/**Original name: 11840-SE-REC-SEQ-NBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PERFORM SIMPLE EDITING ON COLUMN REC-SEQ-NBR                   *
	 *                                                                 *
	 * *****************************************************************
	 * * COLUMN: REC_SEQ_NBR        (SMALLINT, NOT NULLABLE, SIGNED)
	 * * VALIDATE SIGN</pre>*/
	private void seRecSeqNbr() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT (XZC008-REC-SEQ-NBR-SIGN = '+' OR '-')
		//               GO TO 11840-SE-REC-SEQ-NBR-X
		//           END-IF.
		if (!(ws.getXzc008ActNotPolRecRow().getRecSeqNbrSign() == '+' || ws.getXzc008ActNotPolRecRow().getRecSeqNbrSign() == '-')) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INVALID-SIGN   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSign();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '11840-SE-REC-SEQ-NBR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11840-SE-REC-SEQ-NBR");
			// COB_CODE: MOVE 'REC-SEQ-NBR SIGN INVALID'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("REC-SEQ-NBR SIGN INVALID");
			// COB_CODE: STRING 'XZC008-REC-SEQ-NBR-SIGN='
			//                  XZC008-REC-SEQ-NBR-SIGN ';'
			//                  'XZC008-ACT-NOT-POL-REC-ROW='
			//                  XZC008-ACT-NOT-POL-REC-ROW ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZC008-REC-SEQ-NBR-SIGN=", String.valueOf(ws.getXzc008ActNotPolRecRow().getRecSeqNbrSign()), ";",
							"XZC008-ACT-NOT-POL-REC-ROW=", ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 11840-SE-REC-SEQ-NBR-X
			return;
		}
		//* VALIDATE COLUMN
		// COB_CODE: IF XZC008-REC-SEQ-NBR NOT NUMERIC
		//               GO TO 11840-SE-REC-SEQ-NBR-X
		//           END-IF.
		if (!Functions.isNumber(ws.getXzc008ActNotPolRecRow().getRecSeqNbrFormatted())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '11840-SE-REC-SEQ-NBR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("11840-SE-REC-SEQ-NBR");
			// COB_CODE: MOVE 'REC-SEQ-NBR INV FORMAT OR CONTENTS'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("REC-SEQ-NBR INV FORMAT OR CONTENTS");
			// COB_CODE: STRING 'XZC008-REC-SEQ-NBR='
			//                  '(NON NUMERIC)' ';'
			//                  'XZC008-ACT-NOT-POL-REC-ROW='
			//                  XZC008-ACT-NOT-POL-REC-ROW ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "XZC008-REC-SEQ-NBR=", "(NON NUMERIC)", ";",
					"XZC008-ACT-NOT-POL-REC-ROW=", ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 11840-SE-REC-SEQ-NBR-X
			return;
		}
	}

	/**Original name: 11860-ITE-UPDATE-MESSAGE-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  MOVE ROW TO URQM MSG DATA.
	 * ****************************************************************</pre>*/
	private void iteUpdateMessageUmt1() {
		// COB_CODE: MOVE XZC008-ACT-NOT-POL-REC-ROW
		//                                       TO URQM-MSG-DATA
		//                                          (1:URQM-BUS-OBJ-DATA-LENGTH).
		ws.getUrqmCommon().setMsgDataSubstring(ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted(), 1,
				ws.getUrqmCommon().getBusObjDataLength());
	}

	/**Original name: 12005-BUILD-SEARCH_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  POPULATE KNOWN SEARCH FIELDS.
	 * ****************************************************************</pre>*/
	private void buildSearch1() {
		// COB_CODE: IF XZC008-TRANS-PROCESS-DT = SPACES
		//                                       TO XZC008-TRANS-PROCESS-DT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getTransProcessDt())) {
			// COB_CODE: MOVE WS-SE3-CUR-ISO-DATE
			//                                   TO XZC008-TRANS-PROCESS-DT
			ws.getXzc008ActNotPolRecRow().setTransProcessDt(ws.getWsSe3CurIsoDate());
		}
		//* IF THIS IS A FILTER-ONLY REQUEST, THE WORKING STORAGE
		//* ROW CANNOT BE CLEARED OTHERWISE YOU WILL LOSE THE
		//* FILTER VALUES. IF THIS IS NOT A FILTER-ONLY REQUEST,
		//* THEN THE WORKING STORAGE ROW SHOULD BE CLEARED.
		// COB_CODE: IF NOT FILTER-ONLY-REQUEST OF URQM-COMMON
		//               MOVE SPACES             TO XZH008-ACT-NOT-POL-REC-ROW
		//           END-IF.
		if (!ws.getUrqmCommon().getActionCode().isFilterOnlyRequest()) {
			// COB_CODE: MOVE SPACES             TO XZH008-ACT-NOT-POL-REC-ROW
			ws.getXzh008ActNotPolRecRow().initXzh008ActNotPolRecRowSpaces();
		}
		// COB_CODE: MOVE XZC008-CSR-ACT-NBR     TO XZH008-CSR-ACT-NBR.
		ws.getXzh008ActNotPolRecRow().setCsrActNbr(ws.getXzc008ActNotPolRecRow().getCsrActNbr());
		// COB_CODE: MOVE XZC008-NOT-PRC-TS      TO XZH008-NOT-PRC-TS.
		ws.getXzh008ActNotPolRecRow().setNotPrcTs(ws.getXzc008ActNotPolRecRow().getNotPrcTs());
		// COB_CODE: MOVE XZC008-POL-NBR         TO XZH008-POL-NBR.
		ws.getXzh008ActNotPolRecRow().setPolNbr(ws.getXzc008ActNotPolRecRow().getPolNbr());
		// COB_CODE: MOVE XZC008-REC-SEQ-NBR     TO XZH008-REC-SEQ-NBR.
		ws.getXzh008ActNotPolRecRow().setRecSeqNbr(Trunc.toShort(ws.getXzc008ActNotPolRecRow().getRecSeqNbr(), 4));
	}

	/**Original name: 12010-PICK-FETCH-CURSOR_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PICK A FETCH CURSOR.
	 * ****************************************************************</pre>*/
	private void pickFetchCursor1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET INVALID-RETRIEVE-CURSOR TO TRUE.
		ws.getWsBdoSwitches().getCursorSelectionSw().setInvalidRetrieveCursor();
		//* THE FOLLOWING IS USED TO VERIFY THAT FIELDS COMMON TO
		//* ALL CURSORS ARE POPULATED. CODE VALIDATIONS FOR ALL
		//* COMMON FIELDS HERE.
		// COB_CODE: IF XZC008-CSR-ACT-NBR = SPACES
		//               GO TO 12010-PICK-FETCH-CURSOR-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getCsrActNbr())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE '12010-PICK-FETCH-CURSOR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12010-PICK-FETCH-CURSOR");
			// COB_CODE: STRING 'REQUIRED KEY FIELDS FOR FETCH SEARCH '
			//                  'ARE NOT POPULATED.'
			//               DELIMITED BY SIZE
			//               INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "REQUIRED KEY FIELDS FOR FETCH SEARCH ", "ARE NOT POPULATED.");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'XZC008-CSR-ACT-NBR='
			//                  XZC008-CSR-ACT-NBR ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZC008-CSR-ACT-NBR=",
					ws.getXzc008ActNotPolRecRow().getCsrActNbrFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12010-PICK-FETCH-CURSOR-X
			return;
		}
		// COB_CODE: IF XZC008-NOT-PRC-TS = SPACES
		//               GO TO 12010-PICK-FETCH-CURSOR-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getNotPrcTs())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE '12010-PICK-FETCH-CURSOR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12010-PICK-FETCH-CURSOR");
			// COB_CODE: STRING 'REQUIRED KEY FIELDS FOR FETCH SEARCH '
			//                  'ARE NOT POPULATED.'
			//               DELIMITED BY SIZE
			//               INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "REQUIRED KEY FIELDS FOR FETCH SEARCH ", "ARE NOT POPULATED.");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'XZC008-NOT-PRC-TS='
			//                  XZC008-NOT-PRC-TS ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZC008-NOT-PRC-TS=",
					ws.getXzc008ActNotPolRecRow().getNotPrcTsFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12010-PICK-FETCH-CURSOR-X
			return;
		}
		// COB_CODE: IF XZC008-POL-NBR = SPACES
		//               GO TO 12010-PICK-FETCH-CURSOR-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getPolNbr())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE '12010-PICK-FETCH-CURSOR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12010-PICK-FETCH-CURSOR");
			// COB_CODE: STRING 'REQUIRED KEY FIELDS FOR FETCH SEARCH '
			//                  'ARE NOT POPULATED.'
			//               DELIMITED BY SIZE
			//               INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "REQUIRED KEY FIELDS FOR FETCH SEARCH ", "ARE NOT POPULATED.");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'XZC008-POL-NBR='
			//                  XZC008-POL-NBR ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZC008-POL-NBR=",
					ws.getXzc008ActNotPolRecRow().getPolNbrFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12010-PICK-FETCH-CURSOR-X
			return;
		}
		// COB_CODE: IF XZC008-REC-SEQ-NBR NOT NUMERIC
		//               GO TO 12010-PICK-FETCH-CURSOR-X
		//           END-IF.
		if (!Functions.isNumber(ws.getXzc008ActNotPolRecRow().getRecSeqNbrFormatted())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE '12010-PICK-FETCH-CURSOR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12010-PICK-FETCH-CURSOR");
			// COB_CODE: STRING 'REQUIRED KEY FIELDS FOR FETCH SEARCH '
			//                  'ARE NOT POPULATED.'
			//               DELIMITED BY SIZE
			//               INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "REQUIRED KEY FIELDS FOR FETCH SEARCH ", "ARE NOT POPULATED.");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'XZC008-REC-SEQ-NBR='
			//                  XZC008-REC-SEQ-NBR ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZC008-REC-SEQ-NBR=",
					ws.getXzc008ActNotPolRecRow().getRecSeqNbrAsString(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12010-PICK-FETCH-CURSOR-X
			return;
		}
		//* DETERMINE CURSOR.
		//* IF EXACT KEY SEARCH IS SPECIFIED, USE CURSOR1.
		//* MAKE SURE THAT ALL KEY FIELDS HAVE BEEN SUPPLIED.
		// COB_CODE: IF FETCH-WITH-EXACT-KEY-REQUEST OF UBOC-COMM-INFO
		//               GO TO 12010-PICK-FETCH-CURSOR-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isFetchWithExactKeyRequest()) {
			// COB_CODE: IF XZC008-CSR-ACT-NBR        = SPACES
			//             OR XZC008-NOT-PRC-TS       = SPACES
			//             OR XZC008-POL-NBR          = SPACES
			//             OR XZC008-REC-SEQ-NBR      NOT NUMERIC
			//               GO TO 12010-PICK-FETCH-CURSOR-X
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getCsrActNbr())
					|| Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getNotPrcTs())
					|| Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getPolNbr())
					|| !Functions.isNumber(ws.getXzc008ActNotPolRecRow().getRecSeqNbrFormatted())) {
				// COB_CODE: SET WS-LOG-ERROR    TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
				// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
				//                               TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
				// COB_CODE: MOVE '12010-PICK-FETCH-CURSOR'
				//                               TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12010-PICK-FETCH-CURSOR");
				// COB_CODE: STRING 'REQUIRED KEY FIELDS FOR EXACT SEARCH '
				//                  'ARE NOT POPULATED.'
				//               DELIMITED BY SIZE
				//               INTO EFAL-ERR-COMMENT
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "REQUIRED KEY FIELDS FOR EXACT SEARCH ",
						"ARE NOT POPULATED.");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
				// COB_CODE: STRING 'XZC008-ACT-NOT-POL-REC-ROW='
				//                   XZC008-ACT-NOT-POL-REC-ROW ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZC008-ACT-NOT-POL-REC-ROW=",
						ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 12010-PICK-FETCH-CURSOR-X
				return;
			}
			// COB_CODE: SET RETRIEVE-WITH-CURSOR1
			//                                   TO TRUE
			ws.getWsBdoSwitches().getCursorSelectionSw().setRetrieveWithCursor1();
			// COB_CODE: GO TO 12010-PICK-FETCH-CURSOR-X
			return;
		}
	}

	/**Original name: 12015-PROCESS-FETCH-REQUEST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CALL SECTION THAT UTILIZES APPROPRIATE CURSOR.
	 * ****************************************************************</pre>*/
	private void processFetchRequest2() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EVALUATE TRUE
		//               WHEN RETRIEVE-WITH-CURSOR1
		//                   PERFORM 12020-PROCESS-CURSOR1
		//               WHEN OTHER
		//                   GO TO 12015-PROCESS-FETCH-REQUEST-X
		//           END-EVALUATE.
		switch (ws.getWsBdoSwitches().getCursorSelectionSw().getCursorSelectionSwFormatted()) {

		case WsCursorSelectionSw.RETRIEVE_WITH_CURSOR1:// COB_CODE: PERFORM 12020-PROCESS-CURSOR1
			processCursor1();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INVALID-FETCH-CURSOR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidFetchCursor();
			// COB_CODE: MOVE '12015-PROCESS-FETCH-REQUEST'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12015-PROCESS-FETCH-REQUEST");
			// COB_CODE: MOVE 'INVALID FETCH CRITERIA SPECIFIED. NO CURSOR.'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID FETCH CRITERIA SPECIFIED. NO CURSOR.");
			// COB_CODE: STRING 'WS-CURSOR-SELECTION-SW='
			//                   WS-CURSOR-SELECTION-SW ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "WS-CURSOR-SELECTION-SW=",
					ws.getWsBdoSwitches().getCursorSelectionSw().getCursorSelectionSwAsString(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12015-PROCESS-FETCH-REQUEST-X
			return;
		}
	}

	/**Original name: 12020-PROCESS-CURSOR1_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  OPEN CURSOR USED TO READ ACT_NOT_POL_REC
	 *  WITH ACCT NBR, NOTIFY PROCESS TS, POL NBR, AND REC SEQ NBR
	 *  RETRIEVE THE APPROPRIATE ROWS BASED ON THIS CURSOR.
	 *  CLOSE THE CURSOR.
	 * ****************************************************************</pre>*/
	private void processCursor1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//              OPEN ACT_NOT_POL_REC_CSR
		//           END-EXEC.
		actNotPolRecDao.openActNotPolRecCsr(ws.getXzh008ActNotPolRecRow().getCsrActNbr(), ws.getXzh008ActNotPolRecRow().getNotPrcTs(),
				ws.getXzh008ActNotPolRecRow().getPolNbr(), ws.getXzh008ActNotPolRecRow().getRecSeqNbr());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 12020-PROCESS-CURSOR1-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'        TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '12020-PROCESS-CURSOR1'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12020-PROCESS-CURSOR1");
			// COB_CODE: MOVE 'OPEN CURSOR1 FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CURSOR1 FAILED");
			// COB_CODE: MOVE XZH008-REC-SEQ-NBR TO WS-DATA-KEY-DISP1
			ws.getWsNotSpecificMisc().setDataKeyDisp1(ws.getXzh008ActNotPolRecRow().getRecSeqNbr());
			// COB_CODE: STRING 'XZH008-CSR-ACT-NBR='
			//                  XZH008-CSR-ACT-NBR ';'
			//                  'XZH008-NOT-PRC-TS='
			//                  XZH008-NOT-PRC-TS ';'
			//                  'XZH008-POL-NBR='
			//                  XZH008-POL-NBR ';'
			//                  'XZH008-REC-SEQ-NBR='
			//                  WS-DATA-KEY-DISP1 ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZH008-CSR-ACT-NBR=", ws.getXzh008ActNotPolRecRow().getCsrActNbrFormatted(), ";", "XZH008-NOT-PRC-TS=",
							ws.getXzh008ActNotPolRecRow().getNotPrcTsFormatted(), ";", "XZH008-POL-NBR=",
							ws.getXzh008ActNotPolRecRow().getPolNbrFormatted(), ";", "XZH008-REC-SEQ-NBR=",
							ws.getWsNotSpecificMisc().getDataKeyDisp1AsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12020-PROCESS-CURSOR1-X
			return;
		}
		// COB_CODE: SET WS-NOT-END-OF-CURSOR1   TO TRUE.
		ws.getWsBdoSwitches().getEndOfCursor1Sw().setNotEndOfCursor1();
		// COB_CODE: SET WS-THE-FIRST-FETCH      TO TRUE.
		ws.getWsBdoSwitches().getFirstFetchSw().setTheFirstFetch();
		// COB_CODE: PERFORM 12025-FETCH-CURSOR1
		//               UNTIL UBOC-HALT-AND-RETURN
		//                   OR WS-END-OF-CURSOR1.
		while (!(dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsBdoSwitches().getEndOfCursor1Sw().isEndOfCursor1())) {
			fetchCursor1();
		}
		// COB_CODE: EXEC SQL
		//              CLOSE ACT_NOT_POL_REC_CSR
		//           END-EXEC.
		actNotPolRecDao.closeActNotPolRecCsr();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 12020-PROCESS-CURSOR1-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING      TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'        TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '12020-PROCESS-CURSOR1'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12020-PROCESS-CURSOR1");
			// COB_CODE: MOVE 'CLOSE CURSOR1 FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CURSOR1 FAILED");
			// COB_CODE: MOVE XZH008-REC-SEQ-NBR TO WS-DATA-KEY-DISP1
			ws.getWsNotSpecificMisc().setDataKeyDisp1(ws.getXzh008ActNotPolRecRow().getRecSeqNbr());
			// COB_CODE: STRING 'XZH008-CSR-ACT-NBR='
			//                  XZH008-CSR-ACT-NBR ';'
			//                  'XZH008-NOT-PRC-TS='
			//                  XZH008-NOT-PRC-TS ';'
			//                  'XZH008-POL-NBR='
			//                  XZH008-POL-NBR ';'
			//                  'XZH008-REC-SEQ-NBR='
			//                  WS-DATA-KEY-DISP1 ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZH008-CSR-ACT-NBR=", ws.getXzh008ActNotPolRecRow().getCsrActNbrFormatted(), ";", "XZH008-NOT-PRC-TS=",
							ws.getXzh008ActNotPolRecRow().getNotPrcTsFormatted(), ";", "XZH008-POL-NBR=",
							ws.getXzh008ActNotPolRecRow().getPolNbrFormatted(), ";", "XZH008-REC-SEQ-NBR=",
							ws.getWsNotSpecificMisc().getDataKeyDisp1AsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12020-PROCESS-CURSOR1-X
			return;
		}
	}

	/**Original name: 12025-FETCH-CURSOR1_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  FETCH ROW DATA USING CURSOR1 WITH ???
	 * ****************************************************************</pre>*/
	private void fetchCursor1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//               FETCH ACT_NOT_POL_FRM_CSR
		//                INTO :XZH008-CSR-ACT-NBR
		//                   , :XZH008-NOT-PRC-TS
		//                   , :XZH008-POL-NBR
		//                   , :XZH008-REC-SEQ-NBR
		//           END-EXEC.
		xz0d0008GenericDao.fetchRec(ws.getXzh008ActNotPolRecRow());
		//* IF SECURITY CHECK REQUESTED, CALL SECURITY/FILTER PROGRAM
		//* WITH ENTIRE ROW OF DATA.
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 12025-FETCH-CURSOR1-X
		//               WHEN OTHER
		//                   GO TO 12025-FETCH-CURSOR1-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CURSOR1
			//                               TO TRUE
			ws.getWsBdoSwitches().getEndOfCursor1Sw().setEndOfCursor1();
			// COB_CODE: IF WS-THE-FIRST-FETCH
			//               PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			//           END-IF
			if (ws.getWsBdoSwitches().getFirstFetchSw().isWsTheFirstFetch()) {
				// COB_CODE: SET WS-NON-LOGGABLE-WARNING
				//                           TO TRUE
				ws.getWsNonLoggableWarnOrErrSw().setWarning();
				// COB_CODE: MOVE WS-BUS-OBJ-NM
				//                           TO UWRN-FAILED-TABLE-OR-FILE
				ws.getUwrnCommon().setFailedTableOrFile(ws.getWsSpecificMisc().getBusObjNm());
				// COB_CODE: MOVE 'GEN_ALLNFD'
				//                           TO UWRN-WARNING-CODE
				ws.getUwrnCommon().setWarningCode("GEN_ALLNFD");
				// COB_CODE: MOVE WS-CURSOR-SELECTION-SW
				//                           TO UWRN-FAILED-COLUMN-OR-FIELD
				ws.getUwrnCommon().setFailedColumnOrField(ws.getWsBdoSwitches().getCursorSelectionSw().getCursorSelectionSwFormatted());
				// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
				procNonLogWrnOrErr();
			}
			// COB_CODE: GO TO 12025-FETCH-CURSOR1-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'    TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '12025-FETCH-CURSOR1'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12025-FETCH-CURSOR1");
			// COB_CODE: MOVE 'FETCH FROM CURSOR1 FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH FROM CURSOR1 FAILED");
			// COB_CODE: MOVE XZH008-REC-SEQ-NBR
			//                               TO WS-DATA-KEY-DISP1
			ws.getWsNotSpecificMisc().setDataKeyDisp1(ws.getXzh008ActNotPolRecRow().getRecSeqNbr());
			// COB_CODE: STRING 'XZH008-CSR-ACT-NBR='
			//                  XZH008-CSR-ACT-NBR ';'
			//                  'XZH008-NOT-PRC-TS='
			//                  XZH008-NOT-PRC-TS ';'
			//                  'XZH008-POL-NBR='
			//                  XZH008-POL-NBR ';'
			//                  'XZH008-REC-SEQ-NBR='
			//                  WS-DATA-KEY-DISP1 ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZH008-CSR-ACT-NBR=", ws.getXzh008ActNotPolRecRow().getCsrActNbrFormatted(), ";", "XZH008-NOT-PRC-TS=",
							ws.getXzh008ActNotPolRecRow().getNotPrcTsFormatted(), ";", "XZH008-POL-NBR=",
							ws.getXzh008ActNotPolRecRow().getPolNbrFormatted(), ";", "XZH008-REC-SEQ-NBR=",
							ws.getWsNotSpecificMisc().getDataKeyDisp1AsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12025-FETCH-CURSOR1-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 12025-FETCH-CURSOR1-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 12025-FETCH-CURSOR1-X
			return;
		}
		// COB_CODE: PERFORM 3000-PROCESS-RECORD.
		processRecord();
		// COB_CODE: SET WS-NOT-THE-FIRST-FETCH  TO TRUE.
		ws.getWsBdoSwitches().getFirstFetchSw().setNotTheFirstFetch();
	}

	/**Original name: 12110-SETUP-KEYS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  SETS UP ALL THE KEYS FOR THE ROW.
	 *  KEY COMPONENTS MAY BE SOURCED AS FOLLOWS:
	 *  A. THE KEY MAY SIMPLY BE SUPPLIED BY THE FRONT-END.
	 *  B. THE KEY MAY NEED TO BE GENERATED BY THIS DATA OBJECT
	 *      (E.G. A SEQUENCE NUMBER).
	 *  C. IF THE KEY COMPONENT IS UNKNOWN BY THE FRONT END BECAUSE
	 *      IT IS ASSOCIATED WITH AN OBJECT THAT IS TO BE INSERTED,
	 *      THEN THE KEY IS GENERATED BY THE OWNING OBJECT IN THE
	 *      BACK END AND STORED AWAY BY A 'KEY REPLACEMENT' STORE
	 *      PROCESS. IT IS THEN RETRIEVED TO BE USED IN THIS SECTION
	 *      BY A CORRESPONDING 'KEY REPLACEMENT' RETRIEVE PROCESS.
	 *      FOR THE ABOVE SCENARIO, IF THE KEY IS A PRIMARY KEY
	 *      THEN THIS PROCESS WILL TAKE PLACE DURING 'SIMPLE EDIT'.
	 *      FOR A NON-PRIMARY KEY, IT WILL TAKE PLACE HERE.
	 * ****************************************************************
	 *  DETERMINE KEY FIELD: CSR_ACT_NBR        (TYPE CHAR)</pre>*/
	private void setupKeys1() {
		// COB_CODE: MOVE XZC008-CSR-ACT-NBR     TO XZH008-CSR-ACT-NBR.
		ws.getXzh008ActNotPolRecRow().setCsrActNbr(ws.getXzc008ActNotPolRecRow().getCsrActNbr());
		// DETERMINE KEY FIELD: NOT_PRC_TS         (TYPE TIMESTMP)
		// COB_CODE: MOVE XZC008-NOT-PRC-TS      TO XZH008-NOT-PRC-TS.
		ws.getXzh008ActNotPolRecRow().setNotPrcTs(ws.getXzc008ActNotPolRecRow().getNotPrcTs());
		// DETERMINE KEY FIELD: POL_NBR            (TYPE CHAR)
		// COB_CODE: MOVE XZC008-POL-NBR         TO XZH008-POL-NBR.
		ws.getXzh008ActNotPolRecRow().setPolNbr(ws.getXzc008ActNotPolRecRow().getPolNbr());
		// DETERMINE KEY FIELD: REC_SEQ_NBR        (TYPE SMALLINT)
		// COB_CODE: MOVE XZC008-REC-SEQ-NBR     TO XZH008-REC-SEQ-NBR.
		ws.getXzh008ActNotPolRecRow().setRecSeqNbr(Trunc.toShort(ws.getXzc008ActNotPolRecRow().getRecSeqNbr(), 4));
		// COB_CODE: IF XZC008-REC-SEQ-NBR-SIGN = '-'
		//                        XZH008-REC-SEQ-NBR
		//           END-IF.
		if (ws.getXzc008ActNotPolRecRow().getRecSeqNbrSign() == '-') {
			// COB_CODE: SUBTRACT XZH008-REC-SEQ-NBR FROM 0 GIVING
			//                    XZH008-REC-SEQ-NBR
			ws.getXzh008ActNotPolRecRow().setRecSeqNbr(Trunc.toShort(0 - ws.getXzh008ActNotPolRecRow().getRecSeqNbr(), 4));
		}
	}

	/**Original name: 12120-CHECK-PARENTS-EXIST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PRIOR TO AN INSERT, CHECK THAT CORRESPONDING PARENT ROWS
	 *  EXIST ON THE DATABASE FOR:
	 *  A. DIRECT PARENT(S) (USUALLY 1)
	 *  B. FOREIGN KEY(S) WHERE THE KEY IS SUPPLIED (NOT FROM KEY CRE)
	 *  THIS IS USER MAINTENANCE OF THE
	 *  REFERENTIAL INTEGRITY OF THE SERIES 3 DATABASE.
	 * ****************************************************************
	 * * CHECK FOR PRESENCE OF IMMEDIATE PARENT.</pre>*/
	private void checkParentsExist1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE XZC008-CSR-ACT-NBR     TO XZH002-CSR-ACT-NBR.
		ws.getXzh002ActNotPolRow().setCsrActNbr(ws.getXzc008ActNotPolRecRow().getCsrActNbr());
		// COB_CODE: MOVE XZC008-NOT-PRC-TS      TO XZH002-NOT-PRC-TS.
		ws.getXzh002ActNotPolRow().setNotPrcTs(ws.getXzc008ActNotPolRecRow().getNotPrcTs());
		// COB_CODE: MOVE XZC008-POL-NBR         TO XZH002-POL-NBR.
		ws.getXzh002ActNotPolRow().setPolNbr(ws.getXzc008ActNotPolRecRow().getPolNbr());
		// COB_CODE: EXEC SQL
		//              SELECT CSR_ACT_NBR
		//                INTO :XZH002-CSR-ACT-NBR
		//                FROM ACT_NOT_POL
		//               WHERE CSR_ACT_NBR = :XZH002-CSR-ACT-NBR
		//                 AND NOT_PRC_TS  = :XZH002-NOT-PRC-TS
		//                 AND POL_NBR     = :XZH002-POL-NBR
		//           END-EXEC.
		ws.getXzh002ActNotPolRow().setCsrActNbr(actNotPolDao.selectRec5(ws.getXzh002ActNotPolRow().getCsrActNbr(),
				ws.getXzh002ActNotPolRow().getNotPrcTs(), ws.getXzh002ActNotPolRow().getPolNbr(), ws.getXzh002ActNotPolRow().getCsrActNbr()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 12120-CHECK-PARENTS-EXIST-X
		//               WHEN OTHER
		//                   GO TO 12120-CHECK-PARENTS-EXIST-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-PARENT-MISSING
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspParentMissing();
			// COB_CODE: MOVE '12120-CHECK-PARENTS-EXIST'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12120-CHECK-PARENTS-EXIST");
			// COB_CODE: MOVE 'PARENT NOT FOUND IN ACT_NOT_POL'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("PARENT NOT FOUND IN ACT_NOT_POL");
			// COB_CODE: STRING 'XZC008-CSR-ACT-NBR='
			//                   XZC008-CSR-ACT-NBR ';'
			//                  'XZC008-NOT-PRC-TS='
			//                   XZC008-NOT-PRC-TS ';'
			//                  'XZC008-POL-NBR='
			//                   XZC008-POL-NBR ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZC008-CSR-ACT-NBR=", ws.getXzc008ActNotPolRecRow().getCsrActNbrFormatted(), ";", "XZC008-NOT-PRC-TS=",
							ws.getXzc008ActNotPolRecRow().getNotPrcTsFormatted(), ";", "XZC008-POL-NBR=",
							ws.getXzc008ActNotPolRecRow().getPolNbrFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12120-CHECK-PARENTS-EXIST-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '12120-CHECK-PARENTS-EXIST'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12120-CHECK-PARENTS-EXIST");
			// COB_CODE: MOVE 'SELECT ACT_NOT_POL PARENT FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT ACT_NOT_POL PARENT FAILED");
			// COB_CODE: STRING 'XZC008-CSR-ACT-NBR='
			//                   XZC008-CSR-ACT-NBR ';'
			//                  'XZC008-NOT-PRC-TS='
			//                   XZC008-NOT-PRC-TS ';'
			//                  'XZC008-POL-NBR='
			//                   XZC008-POL-NBR ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZC008-CSR-ACT-NBR=", ws.getXzc008ActNotPolRecRow().getCsrActNbrFormatted(), ";", "XZC008-NOT-PRC-TS=",
							ws.getXzc008ActNotPolRecRow().getNotPrcTsFormatted(), ";", "XZC008-POL-NBR=",
							ws.getXzc008ActNotPolRecRow().getPolNbrFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12120-CHECK-PARENTS-EXIST-X
			return;
		}
		//* IF THIS TABLE HAS AN ADDITIONAL PARENT RELATIONSHIP OR
		//* THE ROW CONTAINS A FOREIGN KEY, CHECK FOR APPROPRIATE
		//* DATABASE INTEGRITY HERE. ONLY PERFORM FOREIGN KEY CHECKS HERE
		//* IF THE FOREIGN KEY FIELD CONTAINS AN ACTUAL VALUE AND THE
		//* ASSOCIATED -KEYCRE VALUE IS EMPTY.  IF THE -KEYCRE VALUE
		//* ASSOCIATED WITH THE FOREIGN KEY IS POPULATED, DO NOT PERFORM
		//* ANY FOREIGN KEY CHECKS AT THIS POINT.
		//* CHECK FOR PRESENCE OF ADDITIONAL PARENT.
		// COB_CODE: MOVE XZC008-CSR-ACT-NBR     TO XZH003-CSR-ACT-NBR.
		ws.getXzh003ActNotRecRow().setCsrActNbr(ws.getXzc008ActNotPolRecRow().getCsrActNbr());
		// COB_CODE: MOVE XZC008-NOT-PRC-TS      TO XZH003-NOT-PRC-TS.
		ws.getXzh003ActNotRecRow().setNotPrcTs(ws.getXzc008ActNotPolRecRow().getNotPrcTs());
		// COB_CODE: MOVE XZC008-REC-SEQ-NBR     TO XZH003-REC-SEQ-NBR.
		ws.getXzh003ActNotRecRow().setRecSeqNbr(Trunc.toShort(ws.getXzc008ActNotPolRecRow().getRecSeqNbr(), 4));
		// COB_CODE: EXEC SQL
		//              SELECT CSR_ACT_NBR
		//                INTO :XZH003-CSR-ACT-NBR
		//                FROM ACT_NOT_REC
		//               WHERE CSR_ACT_NBR = :XZH003-CSR-ACT-NBR
		//                 AND NOT_PRC_TS  = :XZH003-NOT-PRC-TS
		//                 AND REC_SEQ_NBR = :XZH003-REC-SEQ-NBR
		//           END-EXEC.
		ws.getXzh003ActNotRecRow().setCsrActNbr(actNotRecDao.selectRec4(ws.getXzh003ActNotRecRow().getCsrActNbr(),
				ws.getXzh003ActNotRecRow().getNotPrcTs(), ws.getXzh003ActNotRecRow().getRecSeqNbr(), ws.getXzh003ActNotRecRow().getCsrActNbr()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 12120-CHECK-PARENTS-EXIST-X
		//               WHEN OTHER
		//                   GO TO 12120-CHECK-PARENTS-EXIST-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-PARENT-MISSING
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspParentMissing();
			// COB_CODE: MOVE '12120-CHECK-PARENTS-EXIST'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12120-CHECK-PARENTS-EXIST");
			// COB_CODE: MOVE 'PARENT NOT FOUND IN ACT_NOT_REC'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("PARENT NOT FOUND IN ACT_NOT_REC");
			// COB_CODE: MOVE XZC008-REC-SEQ-NBR
			//                               TO WS-DATA-KEY-DISP1
			ws.getWsNotSpecificMisc().setDataKeyDisp1(ws.getXzc008ActNotPolRecRow().getRecSeqNbr());
			// COB_CODE: STRING 'XZC008-CSR-ACT-NBR='
			//                   XZC008-CSR-ACT-NBR ';'
			//                  'XZC008-NOT-PRC-TS='
			//                   XZC008-NOT-PRC-TS  ';'
			//                  'XZC008-REC-SEQ-NBR='
			//                   WS-DATA-KEY-DISP1 ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZC008-CSR-ACT-NBR=", ws.getXzc008ActNotPolRecRow().getCsrActNbrFormatted(), ";", "XZC008-NOT-PRC-TS=",
							ws.getXzc008ActNotPolRecRow().getNotPrcTsFormatted(), ";", "XZC008-REC-SEQ-NBR=",
							ws.getWsNotSpecificMisc().getDataKeyDisp1AsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12120-CHECK-PARENTS-EXIST-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '12120-CHECK-PARENTS-EXIST'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12120-CHECK-PARENTS-EXIST");
			// COB_CODE: MOVE 'SELECT ACT_NOT_REC PARENT FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT ACT_NOT_REC PARENT FAILED");
			// COB_CODE: MOVE XZC008-REC-SEQ-NBR
			//                               TO WS-DATA-KEY-DISP1
			ws.getWsNotSpecificMisc().setDataKeyDisp1(ws.getXzc008ActNotPolRecRow().getRecSeqNbr());
			// COB_CODE: STRING 'XZC008-CSR-ACT-NBR='
			//                   XZC008-CSR-ACT-NBR ';'
			//                  'XZC008-NOT-PRC-TS='
			//                   XZC008-NOT-PRC-TS  ';'
			//                  'XZC008-REC-SEQ-NBR='
			//                   WS-DATA-KEY-DISP1 ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZC008-CSR-ACT-NBR=", ws.getXzc008ActNotPolRecRow().getCsrActNbrFormatted(), ";", "XZC008-NOT-PRC-TS=",
							ws.getXzc008ActNotPolRecRow().getNotPrcTsFormatted(), ";", "XZC008-REC-SEQ-NBR=",
							ws.getWsNotSpecificMisc().getDataKeyDisp1AsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12120-CHECK-PARENTS-EXIST-X
			return;
		}
	}

	/**Original name: 12130-INS-ROW-TO-INS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  INSERT A ROW.
	 *  NOTE: HISTORIZED AND NOT HISTORIZED BUSINESS OBJECT.
	 * ****************************************************************</pre>*/
	private void insRowToIns1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 12135-INS-ROW-TO-INS.
		insRowToIns2();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-INSERT     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Insert();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '12130-INS-ROW-TO-INS'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12130-INS-ROW-TO-INS");
			// COB_CODE: MOVE 'INSERT INTO ACT_NOT_POL_REC FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INSERT INTO ACT_NOT_POL_REC FAILED");
			// COB_CODE: STRING 'XZH008-ACT-NOT-POL-REC-ROW='
			//                  XZH008-ACT-NOT-POL-REC-ROW
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZH008-ACT-NOT-POL-REC-ROW=",
					ws.getXzh008ActNotPolRecRow().getXzh008ActNotPolRecRowFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 12135-INS-ROW-TO-INS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  INSERT A ROW.
	 *  NOTE: HISTORIZED AND NOT HISTORIZED BUSINESS OBJECT.
	 * ****************************************************************</pre>*/
	private void insRowToIns2() {
		// COB_CODE: EXEC SQL
		//               INSERT INTO ACT_NOT_POL_REC
		//                      (CSR_ACT_NBR
		//                     , NOT_PRC_TS
		//                     , POL_NBR
		//                     , REC_SEQ_NBR)
		//               VALUES (:XZH008-CSR-ACT-NBR
		//                     , :XZH008-NOT-PRC-TS
		//                     , :XZH008-POL-NBR
		//                     , :XZH008-REC-SEQ-NBR)
		//           END-EXEC.
		actNotPolRecDao.insertRec(ws.getXzh008ActNotPolRecRow().getCsrActNbr(), ws.getXzh008ActNotPolRecRow().getNotPrcTs(),
				ws.getXzh008ActNotPolRecRow().getPolNbr(), ws.getXzh008ActNotPolRecRow().getRecSeqNbr());
	}

	/**Original name: 12306-PROCESS-DEL-REQ-NOTHIST_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  THIS SECTION IS USED TO PERFORM DELETE PROCESSING FOR
	 *  APPLICATIONS THAT DO NOT HISTORIZE.
	 *  NOTE: NON-HISTORIZED BUSINESS OBJECT.
	 * ****************************************************************
	 * * THIS CODE WILL INVOKE THE PROCESSING REQUIRED FOR A
	 * * PHYSICAL DELETE.</pre>*/
	private void processDelReqNothist1() {
		// COB_CODE: PERFORM 12810-CHECK-KEY-CHECK-SUM.
		checkKeyCheckSum1();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 12306-PROC-DEL-REQ-NOTHIST-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 12306-PROC-DEL-REQ-NOTHIST-X
			return;
		}
		// COB_CODE: PERFORM 12320-DEL-ROW-TO-DEL.
		delRowToDel();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 12306-PROC-DEL-REQ-NOTHIST-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 12306-PROC-DEL-REQ-NOTHIST-X
			return;
		}
	}

	/**Original name: 12320-DEL-ROW-TO-DEL_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  UPDATE A ROW TO MARK AS DELETED.
	 *  NOTE: SOME NON-HISTORIZED BDOS CHANGE THE ROW'S STATUS VERSUS
	 *        ACTUALLY DELETING THE ROW.  IF THAT IS THE CASE FOR THIS
	 *        BDO, PLACE THE UPDATE LOGIC FOR THE MODIFIED ROW IN THIS
	 *        SECTION. IF THIS IS A BDO THAT NEEDS THE CAPABILITY TO
	 *        BOTH PHYSICALLY AND LOGICALLY DELETE, PLACE THE LOGIC TO
	 *        DETERMINE THE PATHWAY HERE AND CREATE 12325- SECTIONS TO
	 *        PERFORM THE REQUIRED PROCESSING.
	 *  NOTE: HISTORIZED AND NOT HISTORIZED BUSINESS OBJECT
	 * ****************************************************************</pre>*/
	private void delRowToDel() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 12110-SETUP-KEYS.
		setupKeys1();
		// COB_CODE: EXEC SQL
		//              DELETE FROM ACT_NOT_POL_REC
		//              WHERE CSR_ACT_NBR        = :XZH008-CSR-ACT-NBR
		//                AND NOT_PRC_TS         = :XZH008-NOT-PRC-TS
		//                AND POL_NBR            = :XZH008-POL-NBR
		//                AND REC_SEQ_NBR        = :XZH008-REC-SEQ-NBR
		//           END-EXEC.
		actNotPolRecDao.deleteRec(ws.getXzh008ActNotPolRecRow().getCsrActNbr(), ws.getXzh008ActNotPolRecRow().getNotPrcTs(),
				ws.getXzh008ActNotPolRecRow().getPolNbr(), ws.getXzh008ActNotPolRecRow().getRecSeqNbr());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'        TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '12320-DEL-ROW-TO-DEL'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12320-DEL-ROW-TO-DEL");
			// COB_CODE: MOVE 'DELETE OF ACT_NOT_POL_REC FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DELETE OF ACT_NOT_POL_REC FAILED");
			// COB_CODE: STRING 'XZH008-ACT-NOT-POL-REC-ROW='
			//                  XZH008-ACT-NOT-POL-REC-ROW
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZH008-ACT-NOT-POL-REC-ROW=",
					ws.getXzh008ActNotPolRecRow().getXzh008ActNotPolRecRowFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 12360-DELETE-MULTI-ROWS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DELETE MULTIPLE ROWS USING A PARTIAL KEY.
	 *  IF THIS BDO IS NOT IMPACTED BY A CASCADING DELETE REQUEST,
	 *  THIS SECTION CAN BE EMPTY. HOWEVER, THE SECTION ITSELF MUST
	 *  BE PRESENT OR THE COMPILE WILL FAIL.
	 * ****************************************************************
	 * * VALIDATE THE DELETE CRITERIA.</pre>*/
	private void deleteMultiRows1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF XZC008-CSR-ACT-NBR = SPACES
		//               GO TO 12360-DELETE-MULTI-ROWS-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getCsrActNbr())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE '12360-DELETE-MULTI-ROWS'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12360-DELETE-MULTI-ROWS");
			// COB_CODE: STRING 'REQUIRED KEY FIELDS FOR DELETE SEARCH '
			//                  'ARE NOT POPULATED.'
			//               DELIMITED BY SIZE
			//               INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "REQUIRED KEY FIELDS FOR DELETE SEARCH ",
					"ARE NOT POPULATED.");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'XZC008-CSR-ACT-NBR='
			//                   XZH008-CSR-ACT-NBR ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZC008-CSR-ACT-NBR=",
					ws.getXzh008ActNotPolRecRow().getCsrActNbrFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12360-DELETE-MULTI-ROWS-X
			return;
		}
		// COB_CODE: IF XZC008-NOT-PRC-TS = SPACES
		//               GO TO 12360-DELETE-MULTI-ROWS-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc008ActNotPolRecRow().getNotPrcTs())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET BUSP-INV-KEY-FIELD-CONTENTS
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvKeyFieldContents();
			// COB_CODE: MOVE '12360-DELETE-MULTI-ROWS'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12360-DELETE-MULTI-ROWS");
			// COB_CODE: STRING 'REQUIRED KEY FIELDS FOR DELETE SEARCH '
			//                  'ARE NOT POPULATED.'
			//               DELIMITED BY SIZE
			//               INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "REQUIRED KEY FIELDS FOR DELETE SEARCH ",
					"ARE NOT POPULATED.");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'XZC008-NOT-PRC-TS='
			//                   XZC008-NOT-PRC-TS ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZC008-NOT-PRC-TS=",
					ws.getXzc008ActNotPolRecRow().getNotPrcTsFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12360-DELETE-MULTI-ROWS-X
			return;
		}
		//* ADD CODE HERE TO VERIFY ANY ADDITIONAL FIELDS THAT ARE
		//* NEEDED TO PROCESS A DELETE WITH A PARTIAL KEY.
		// COB_CODE: PERFORM 12110-SETUP-KEYS.
		setupKeys1();
		//* EXECUTE THE PARTIAL KEY DELETION PROCESS.
		//* PHYSICALLY DELETES ROWS FROM THE DATABASE.
		// COB_CODE: PERFORM 12370-PHYSICAL-DELETE-ROWS.
		physicalDeleteRows();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 12360-DELETE-MULTI-ROWS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 12360-DELETE-MULTI-ROWS-X
			return;
		}
	}

	/**Original name: 12370-PHYSICAL-DELETE-ROWS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PHYSICALLY DELETE MULTIPLE ROWS USING A PARTIAL KEY.
	 * ****************************************************************
	 * * DEPENDING ON THE KEYS BEING PASSED, PERFORM THE
	 * * APPROPRIATE PARTIAL KEY DELETE.</pre>*/
	private void physicalDeleteRows() {
		ConcatUtil concatUtil = null;
		// COB_CODE:      IF XZH008-REC-SEQ-NBR > 0
		//           **       DELETE COMING FROM ACT_NOT_REC
		//                    PERFORM 12370-DEL-WITH-REC-SEQ-NBR
		//                ELSE
		//                    END-IF
		//                END-IF.
		if (ws.getXzh008ActNotPolRecRow().getRecSeqNbr() > 0) {
			//*       DELETE COMING FROM ACT_NOT_REC
			// COB_CODE: PERFORM 12370-DEL-WITH-REC-SEQ-NBR
			delWithRecSeqNbr();
		} else if (!Characters.EQ_SPACE.test(ws.getXzh008ActNotPolRecRow().getPolNbr())) {
			// COB_CODE:          IF XZH008-POL-NBR NOT = SPACES
			//           **           DELETE COMING FROM ACT_NOT_POL
			//                        PERFORM 12370-DEL-WITH-POL-NBR
			//                    ELSE
			//           **           DELETE COMING FROM ACT_NOT
			//                        PERFORM 12370-DEL-WITH-ACT-NBR
			//                    END-IF
			//*           DELETE COMING FROM ACT_NOT_POL
			// COB_CODE: PERFORM 12370-DEL-WITH-POL-NBR
			delWithPolNbr();
		} else {
			//*           DELETE COMING FROM ACT_NOT
			// COB_CODE: PERFORM 12370-DEL-WITH-ACT-NBR
			delWithActNbr();
		}
		//* IF NO ROWS WERE FOUND TO DELETE FOR A PARTICULAR CHILD IN
		//* A CASCADING DELETE SITUATION, THAT IS OKAY
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//             AND
		//              NOT ERD-SQL-NOT-FOUND
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!sqlca.isErdSqlGood() && !sqlca.isErdSqlNotFound()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE WS-PARAGRAPH-NM    TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph(ws.getWsSpecificMisc().getParagraphNm());
			// COB_CODE: MOVE 'DELETE OF ACT_NOT_POL_REC FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DELETE OF ACT_NOT_POL_REC FAILED");
			// COB_CODE: STRING 'XZH008-ACT-NOT-POL-REC-ROW='
			//                  XZH008-ACT-NOT-POL-REC-ROW
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZH008-ACT-NOT-POL-REC-ROW=",
					ws.getXzh008ActNotPolRecRow().getXzh008ActNotPolRecRowFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 12370-DEL-WITH-REC-SEQ-NBR_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PHYSICALLY DELETE MULTIPLE ROWS USING THIS PARTIAL KEY.
	 *  ACCT NBR, NOTIFY PROCESS TS, AND REC SEQ NUMBER.
	 * ****************************************************************</pre>*/
	private void delWithRecSeqNbr() {
		// COB_CODE: MOVE '12370-DEL-WITH-REC-SEQ-NBR'
		//                                       TO WS-PARAGRAPH-NM.
		ws.getWsSpecificMisc().setParagraphNm("12370-DEL-WITH-REC-SEQ-NBR");
		// COB_CODE: EXEC SQL
		//              DELETE FROM ACT_NOT_POL_REC
		//              WHERE CSR_ACT_NBR        = :XZH008-CSR-ACT-NBR
		//                AND NOT_PRC_TS         = :XZH008-NOT-PRC-TS
		//                AND REC_SEQ_NBR        = :XZH008-REC-SEQ-NBR
		//           END-EXEC.
		actNotPolRecDao.deleteRec1(ws.getXzh008ActNotPolRecRow().getCsrActNbr(), ws.getXzh008ActNotPolRecRow().getNotPrcTs(),
				ws.getXzh008ActNotPolRecRow().getRecSeqNbr());
	}

	/**Original name: 12370-DEL-WITH-POL-NBR_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PHYSICALLY DELETE MULTIPLE ROWS USING THIS PARTIAL KEY.
	 *  ACCT NBR, NOTIFY PROCESS TS, AND POLICY NUMBER.
	 * ****************************************************************</pre>*/
	private void delWithPolNbr() {
		// COB_CODE: MOVE '12370-DEL-WITH-POL-NBR'
		//                                       TO WS-PARAGRAPH-NM.
		ws.getWsSpecificMisc().setParagraphNm("12370-DEL-WITH-POL-NBR");
		// COB_CODE: EXEC SQL
		//              DELETE FROM ACT_NOT_POL_REC
		//              WHERE CSR_ACT_NBR        = :XZH008-CSR-ACT-NBR
		//                AND NOT_PRC_TS         = :XZH008-NOT-PRC-TS
		//                AND POL_NBR            = :XZH008-POL-NBR
		//           END-EXEC.
		actNotPolRecDao.deleteRec2(ws.getXzh008ActNotPolRecRow().getCsrActNbr(), ws.getXzh008ActNotPolRecRow().getNotPrcTs(),
				ws.getXzh008ActNotPolRecRow().getPolNbr());
	}

	/**Original name: 12370-DEL-WITH-ACT-NBR_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PHYSICALLY DELETE MULTIPLE ROWS USING THIS PARTIAL KEY.
	 *  ACCT NBR AND NOTIFY PROCESS TS.
	 * ****************************************************************</pre>*/
	private void delWithActNbr() {
		// COB_CODE: MOVE '12370-DEL-WITH-ACT-NBR'
		//                                       TO WS-PARAGRAPH-NM.
		ws.getWsSpecificMisc().setParagraphNm("12370-DEL-WITH-ACT-NBR");
		// COB_CODE: EXEC SQL
		//              DELETE FROM ACT_NOT_POL_REC
		//              WHERE CSR_ACT_NBR        = :XZH008-CSR-ACT-NBR
		//                AND NOT_PRC_TS         = :XZH008-NOT-PRC-TS
		//           END-EXEC.
		actNotPolRecDao.deleteRec3(ws.getXzh008ActNotPolRecRow().getCsrActNbr(), ws.getXzh008ActNotPolRecRow().getNotPrcTs());
	}

	/**Original name: 12700-PROC-ADDL-ACTIONS-URQM_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  TO PROCESS ADDITIONAL ACTIONS THAT ARE ONLY RELATED TO A
	 *  SPECIFIC APPLICATION.
	 *  INVOKED WHEN MESSAGE ROW RETRIEVED FROM UOW REQ MESSAGE UMT.
	 *  EXAMPLE: UWS NEEDS THE BDOS TO BE ABLE TO ACCEPT AN ACTION CODE
	 *  OF QUOTE_ACCEPT OR POLICY_COPY. THESE ACTIONS REQUIRE SPECIAL
	 *  SECTIONS TO BE ADDED TO THE BDOS AND THIS PARAGRAPH WILL ALLOW
	 *  THE COMMON INFRASTRUCTURE CODE (HALCDPDC) TO HAND OFF THESE
	 *  SPECIAL ACTIONS SO THAT THEY CAN BE PROCESSED.
	 * ****************************************************************
	 *  STANDARD DEFAULT CODE.
	 *  IF THIS BDO DOES NOT NEED TO PROCESS ADDITIONAL ACTIONS, LEAVE
	 *  THE FOLLOWING CODE IN PLACE.  HOWEVER, IF THE 'EVALUATE' ABOVE
	 *  IS REQUIRED, DELETE THIS CODE.</pre>*/
	private void procAddlActionsUrqm1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET BUSP-INV-ACTION-CODE    TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvActionCode();
		// COB_CODE: MOVE '12700-PROC-ADDL-ACTIONS-URQM'
		//                                       TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12700-PROC-ADDL-ACTIONS-URQM");
		// COB_CODE: MOVE 'INVALID ACTION CODE'  TO EFAL-ERR-COMMENT.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID ACTION CODE");
		// COB_CODE: STRING 'URQM-ACTION-CODE OF URQM-COMMON='
		//                   URQM-ACTION-CODE OF URQM-COMMON ';'
		//               DELIMITED BY SIZE
		//               INTO EFAL-OBJ-DATA-KEY
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-ACTION-CODE OF URQM-COMMON=",
				ws.getUrqmCommon().getActionCode().getActionCodeFormatted(), ";");
		ws.getWsEstoInfo().getEstoDetailBuffer()
				.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 12705-PROC-ADDL-ACTIONS-UBOC_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  TO PROCESS ADDITIONAL ACTIONS THAT ARE ONLY RELATED TO A
	 *  SPECIFIC APPLICATION.
	 *  INVOKED WHEN MESSAGE ROW RETRIEVED FROM LINKAGE.
	 *  EXAMPLE: UWS NEEDS THE BDOS TO BE ABLE TO ACCEPT AN ACTION CODE
	 *  OF QUOTE_ACCEPT OR POLICY_COPY. THESE ACTIONS REQUIRE SPECIAL
	 *  SECTIONS TO BE ADDED TO THE BDOS AND THIS PARAGRAPH WILL ALLOW
	 *  THE COMMON INFRASTRUCTURE CODE (HALCDPDC) TO HAND OFF THESE
	 *  SPECIAL ACTIONS SO THAT THEY CAN BE PROCESSED.
	 * ****************************************************************
	 *  STANDARD DEFAULT CODE.
	 *  IF THIS BDO DOES NOT NEED TO PROCESS ADDITIONAL ACTIONS, LEAVE
	 *  THE FOLLOWING CODE IN PLACE.  HOWEVER, IF THE 'EVALUATE' ABOVE
	 *  IS REQUIRED, DELETE THIS CODE.</pre>*/
	private void procAddlActionsUboc1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET BUSP-INV-ACTION-CODE    TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvActionCode();
		// COB_CODE: MOVE '12705-PROC-ADDL-ACTIONS-UBOC'
		//                                       TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12705-PROC-ADDL-ACTIONS-UBOC");
		// COB_CODE: MOVE 'INVALID ACTION CODE'  TO EFAL-ERR-COMMENT.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID ACTION CODE");
		// COB_CODE: STRING 'UBOC-PASS-THRU-ACTION='
		//                   UBOC-PASS-THRU-ACTION ';'
		//               DELIMITED BY SIZE
		//               INTO EFAL-OBJ-DATA-KEY
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UBOC-PASS-THRU-ACTION=",
				dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().getActionCodeFormatted(), ";");
		ws.getWsEstoInfo().getEstoDetailBuffer()
				.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 12810-CHECK-KEY-CHECK-SUM_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CHECK THAT THE CHECK SUM WHICH WAS CALCULATED FROM THE KEY
	 *  FOR THIS OBJECT AND SENT TO THE FRONT END AS PART OF THE FETCH
	 *  REQUEST AND NOW RETURNED TO THE BACK END CORRESPONDS TO THE
	 *  KEY SENT FROM THE FRONT END.
	 *  THIS IS AN ATTEMPT TO ENSURE THAT THE KEY HASN'T BEEN
	 *  CHANGED BY THE FRONT END.
	 * ****************************************************************
	 * * MOVE CONCATENATED KEY AND LENGTH.</pre>*/
	private void checkKeyCheckSum1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE LENGTH OF XZC008-ACT-NOT-POL-REC-KEY
		//                                       TO HALOUCHS-LENGTH.
		ws.getHalluchs().setLength2(((short) Xzc008ActNotPolRecRow.Len.ACT_NOT_POL_REC_KEY));
		// COB_CODE: MOVE XZC008-ACT-NOT-POL-REC-KEY
		//                                       TO HALOUCHS-STRING.
		ws.getHalluchs().setStringFldBytes(ws.getXzc008ActNotPolRecRow().getActNotPolRecKeyBytes());
		// COB_CODE: MOVE SPACES                 TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE LENGTH OF HALOUCHS-LINKAGE
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) Xz0d0008Data.Len.HALOUCHS_LINKAGE));
		// COB_CODE: MOVE HALOUCHS-LINKAGE       TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getHalouchsLinkageFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  ('HALOUCHS')
		//               COMMAREA (UBOC-RECORD)
		//               LENGTH   (LENGTH OF UBOC-RECORD)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0D0008", execContext).commarea(dfhcommarea.getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD).link("HALOUCHS",
				new Halouchs());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 12810-CHECK-KEY-CHECK-SUM-X
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE 'HALOUCHS'         TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALOUCHS");
			// COB_CODE: MOVE '12810-CHECK-KEY-CHECK-SUM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12810-CHECK-KEY-CHECK-SUM");
			// COB_CODE: MOVE 'LINK TO HALOUCHS FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO HALOUCHS FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12810-CHECK-KEY-CHECK-SUM-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 12810-CHECK-KEY-CHECK-SUM-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 12810-CHECK-KEY-CHECK-SUM-X
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER   TO HALOUCHS-LINKAGE.
		ws.setHalouchsLinkageFormatted(dfhcommarea.getUbocRecord().getAppDataBufferFormatted());
		// COB_CODE: IF XZC008-ACT-NOT-POL-REC-CSUM NOT = HALOUCHS-CHECK-SUM
		//               GO TO 12810-CHECK-KEY-CHECK-SUM-X
		//           END-IF.
		if (ws.getXzc008ActNotPolRecRow().getActNotPolRecCsum() != ws.getHalluchs().getCheckSum()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-KEY-CHANGED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspKeyChanged();
			// COB_CODE: MOVE '12810-CHECK-KEY-CHECK-SUM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12810-CHECK-KEY-CHECK-SUM");
			// COB_CODE: MOVE 'CHECKSUM/KEY CHANGED BY FRONT END'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CHECKSUM/KEY CHANGED BY FRONT END");
			// COB_CODE: STRING 'XZC008-ACT-NOT-POL-REC-CSUM='
			//                   XZC008-ACT-NOT-POL-REC-CSUM ';'
			//                  'HALOUCHS-CHECK-SUM=' HALOUCHS-CHECK-SUM ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "XZC008-ACT-NOT-POL-REC-CSUM=", ws.getXzc008ActNotPolRecRow().getActNotPolRecCsumAsString(), ";",
							"HALOUCHS-CHECK-SUM=", ws.getHalluchs().getCheckSumAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12810-CHECK-KEY-CHECK-SUM-X
			return;
		}
	}

	/**Original name: 12820-READ-ROW_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ EXISTING ROW FROM DATABASE.
	 *  NOTE: HISTORIZED AND NOT HISTORIZED BUSINESS OBJECT.
	 * ****************************************************************</pre>*/
	private void readRow1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 12110-SETUP-KEYS.
		setupKeys1();
		// COB_CODE: EXEC SQL
		//              SELECT CSR_ACT_NBR
		//                   , NOT_PRC_TS
		//                   , POL_NBR
		//                   , REC_SEQ_NBR
		//              INTO   :XZH008-CSR-ACT-NBR
		//                   , :XZH008-NOT-PRC-TS
		//                   , :XZH008-POL-NBR
		//                   , :XZH008-REC-SEQ-NBR
		//              FROM ACT_NOT_POL_REC
		//              WHERE CSR_ACT_NBR        = :XZH008-CSR-ACT-NBR
		//                AND NOT_PRC_TS         = :XZH008-NOT-PRC-TS
		//                AND POL_NBR            = :XZH008-POL-NBR
		//                AND REC_SEQ_NBR        = :XZH008-REC-SEQ-NBR
		//           END-EXEC.
		actNotPolRecDao.selectRec(ws.getXzh008ActNotPolRecRow().getCsrActNbr(), ws.getXzh008ActNotPolRecRow().getNotPrcTs(),
				ws.getXzh008ActNotPolRecRow().getPolNbr(), ws.getXzh008ActNotPolRecRow().getRecSeqNbr(), ws.getXzh008ActNotPolRecRow());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 12820-READ-ROW-X
		//               WHEN OTHER
		//                   GO TO 12820-READ-ROW-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-EXI-ROW-NOT-FOUND
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspExiRowNotFound();
			// COB_CODE: MOVE '12820-READ-ROW'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12820-READ-ROW");
			// COB_CODE: MOVE 'EXISTING ROW NOT FOUND'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("EXISTING ROW NOT FOUND");
			// COB_CODE: STRING 'XZH008-ACT-NOT-POL-REC-ROW='
			//                   XZH008-ACT-NOT-POL-REC-ROW
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZH008-ACT-NOT-POL-REC-ROW=",
					ws.getXzh008ActNotPolRecRow().getXzh008ActNotPolRecRowFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12820-READ-ROW-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT_POL_REC'
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_POL_REC");
			// COB_CODE: MOVE '12820-READ-ROW'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("12820-READ-ROW");
			// COB_CODE: MOVE 'SELECT EXISTING ROW FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT EXISTING ROW FAILED");
			// COB_CODE: STRING 'XZH008-ACT-NOT-POL-REC-ROW='
			//                   XZH008-ACT-NOT-POL-REC-ROW
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "XZH008-ACT-NOT-POL-REC-ROW=",
					ws.getXzh008ActNotPolRecRow().getXzh008ActNotPolRecRowFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 12820-READ-ROW-X
			return;
		}
	}

	/**Original name: 13005-REFORMAT-DATA_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  REFORMAT ROW FOR DOWNLOAD.
	 * ****************************************************************</pre>*/
	private void reformatData1() {
		// COB_CODE: IF INSERT-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//             OR
		//              UPDATE-AND-RETURN-DATA-REQUEST OF URQM-COMMON
		//               CONTINUE
		//           ELSE
		//               MOVE SPACES             TO XZC008-ACT-NOT-POL-REC-FIXED
		//           END-IF.
		if (ws.getUrqmCommon().getActionCode().isInsertAndReturnDataRequest() || ws.getUrqmCommon().getActionCode().isUpdateAndReturnDataRequest()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: MOVE SPACES             TO XZC008-ACT-NOT-POL-REC-FIXED
			ws.getXzc008ActNotPolRecRow().initActNotPolRecFixedSpaces();
		}
		// COB_CODE: MOVE SPACES                 TO XZC008-ACT-NOT-POL-REC-DATA.
		ws.getXzc008ActNotPolRecRow().initActNotPolRecDataSpaces();
		//* COLUMN: CSR_ACT_NBR        (CHAR, NOT NULLABLE, NOT SIGNED)
		// COB_CODE: MOVE COM-IS-PRIM-KEY-COL-IND
		//                                       TO XZC008-CSR-ACT-NBR-CI.
		ws.getXzc008ActNotPolRecRow().setCsrActNbrCi(ws.getHallcom().getIsPrimKeyColInd());
		// COB_CODE: MOVE XZH008-CSR-ACT-NBR     TO XZC008-CSR-ACT-NBR.
		ws.getXzc008ActNotPolRecRow().setCsrActNbr(ws.getXzh008ActNotPolRecRow().getCsrActNbr());
		//* COLUMN: NOT_PRC_TS         (TIMESTMP, NOT NULLABLE, NOT SIGNED)
		// COB_CODE: MOVE COM-IS-PRIM-KEY-COL-IND
		//                                       TO XZC008-NOT-PRC-TS-CI.
		ws.getXzc008ActNotPolRecRow().setNotPrcTsCi(ws.getHallcom().getIsPrimKeyColInd());
		// COB_CODE: MOVE XZH008-NOT-PRC-TS      TO XZC008-NOT-PRC-TS.
		ws.getXzc008ActNotPolRecRow().setNotPrcTs(ws.getXzh008ActNotPolRecRow().getNotPrcTs());
		//* COLUMN: POL_NBR            (CHAR, NOT NULLABLE, NOT SIGNED)
		// COB_CODE: MOVE COM-IS-PRIM-KEY-COL-IND
		//                                       TO XZC008-POL-NBR-CI.
		ws.getXzc008ActNotPolRecRow().setPolNbrCi(ws.getHallcom().getIsPrimKeyColInd());
		// COB_CODE: MOVE XZH008-POL-NBR         TO XZC008-POL-NBR.
		ws.getXzc008ActNotPolRecRow().setPolNbr(ws.getXzh008ActNotPolRecRow().getPolNbr());
		//* COLUMN: REC_SEQ_NBR        (SMALLINT, NOT NULLABLE, SIGNED)
		// COB_CODE: MOVE COM-IS-PRIM-KEY-COL-IND
		//                                       TO XZC008-REC-SEQ-NBR-CI.
		ws.getXzc008ActNotPolRecRow().setRecSeqNbrCi(ws.getHallcom().getIsPrimKeyColInd());
		// COB_CODE: IF XZH008-REC-SEQ-NBR < 0
		//               MOVE '-'                TO XZC008-REC-SEQ-NBR-SIGN
		//           ELSE
		//               MOVE '+'                TO XZC008-REC-SEQ-NBR-SIGN
		//           END-IF.
		if (ws.getXzh008ActNotPolRecRow().getRecSeqNbr() < 0) {
			// COB_CODE: MOVE '-'                TO XZC008-REC-SEQ-NBR-SIGN
			ws.getXzc008ActNotPolRecRow().setRecSeqNbrSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+'                TO XZC008-REC-SEQ-NBR-SIGN
			ws.getXzc008ActNotPolRecRow().setRecSeqNbrSignFormatted("+");
		}
		// COB_CODE: MOVE XZH008-REC-SEQ-NBR     TO XZC008-REC-SEQ-NBR.
		ws.getXzc008ActNotPolRecRow().setRecSeqNbr(TruncAbs.toInt(ws.getXzh008ActNotPolRecRow().getRecSeqNbr(), 5));
	}

	/**Original name: 13020-GENERATE-LOCK-KEY-INFO_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  GENERATE LOCK KEY INFO.
	 * ****************************************************************
	 * * VERIFY THAT THE PROPER APPLICATION LOCK NAME IS BEING SET.</pre>*/
	private void generateLockKeyInfo() {
		// COB_CODE: MOVE WS-APPLICATION-LOK-NM  TO HALRLODR-APP-ID.
		ws.getHalrlodrLockDriverStorage().setAppId(ws.getWsSpecificMisc().getApplicationLokNm());
		//* VERIFY THAT THE PROPER APPLICATION LOCK TABLE ID IS BEING SET.
		// COB_CODE: MOVE WS-APPLICATION-LOK-TAB TO HALRLODR-TABLE-NM.
		ws.getHalrlodrLockDriverStorage().setTableNm(ws.getWsSpecificMisc().getApplicationLokTab());
		//* PROVIDE THE APPROPRIATE TECHNICAL KEY USED FOR LOCKING.
		// COB_CODE: MOVE XZC008-ACT-NOT-POL-REC-KEY
		//                                       TO HALRLODR-TCH-KEY.
		ws.getHalrlodrLockDriverStorage().setTchKey(ws.getXzc008ActNotPolRecRow().getActNotPolRecKeyFormatted());
	}

	/**Original name: 13025-SETUP-DATA-ROW-FOR-LOK_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  SET UP DATA FOR ROW LOCKING.
	 * ****************************************************************</pre>*/
	private void setupDataRowForLok() {
		// COB_CODE: MOVE XZC008-ACT-NOT-POL-REC-ROW
		//                                       TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted());
		// COB_CODE: MOVE LENGTH OF XZC008-ACT-NOT-POL-REC-ROW
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) Xzc008ActNotPolRecRow.Len.XZC008_ACT_NOT_POL_REC_ROW));
	}

	/**Original name: 13050-DETERMINE-CHECK-SUM_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DETERMINE CHECK SUM BASED ON KEY OF ROW.
	 *  THIS IS THEN SENT WITH THE ROW AND USED TO ENSURE THE
	 *  FRONT END HASN'T CHANGED THE KEY.
	 * *****************************************************************</pre>*/
	private void determineCheckSum1() {
		// COB_CODE: MOVE LENGTH OF XZC008-ACT-NOT-POL-REC-KEY
		//                                       TO HALOUCHS-LENGTH.
		ws.getHalluchs().setLength2(((short) Xzc008ActNotPolRecRow.Len.ACT_NOT_POL_REC_KEY));
		// COB_CODE: MOVE XZC008-ACT-NOT-POL-REC-KEY
		//                                       TO HALOUCHS-STRING.
		ws.getHalluchs().setStringFldBytes(ws.getXzc008ActNotPolRecRow().getActNotPolRecKeyBytes());
		// COB_CODE: MOVE SPACES                 TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE LENGTH OF HALOUCHS-LINKAGE
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) Xz0d0008Data.Len.HALOUCHS_LINKAGE));
		// COB_CODE: MOVE HALOUCHS-LINKAGE       TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getHalouchsLinkageFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  ('HALOUCHS')
		//               COMMAREA (UBOC-RECORD)
		//               LENGTH   (LENGTH OF UBOC-RECORD)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0D0008", execContext).commarea(dfhcommarea.getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD).link("HALOUCHS",
				new Halouchs());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 13050-DETERMINE-CHECK-SUM-X
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE 'HALOUCHS'         TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALOUCHS");
			// COB_CODE: MOVE '13050-DETERMINE-CHECK-SUM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("13050-DETERMINE-CHECK-SUM");
			// COB_CODE: MOVE 'LINK TO HALOUCHS FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO HALOUCHS FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 13050-DETERMINE-CHECK-SUM-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 13050-DETERMINE-CHECK-SUM-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 13050-DETERMINE-CHECK-SUM-X
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER   TO HALOUCHS-LINKAGE.
		ws.setHalouchsLinkageFormatted(dfhcommarea.getUbocRecord().getAppDataBufferFormatted());
		// COB_CODE: MOVE HALOUCHS-CHECK-SUM     TO XZC008-ACT-NOT-POL-REC-CSUM.
		ws.getXzc008ActNotPolRecRow().setActNotPolRecCsumFormatted(ws.getHalluchs().getCheckSumFormatted());
	}

	/**Original name: 14005-WR-RESP-UMT-DATA-REC-A_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  MOVE LENGTH OF ROW TO UDAT-UOW-BUFFER-LENGTH.
	 * ****************************************************************</pre>*/
	private void wrRespUmtDataRecA() {
		// COB_CODE: MOVE LENGTH OF XZC008-ACT-NOT-POL-REC-ROW
		//                                       TO UDAT-UOW-BUFFER-LENGTH.
		ws.getHalludat().setUowBufferLength(((short) Xzc008ActNotPolRecRow.Len.XZC008_ACT_NOT_POL_REC_ROW));
	}

	/**Original name: 14005-WR-RESP-UMT-DATA-REC-B_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  MOVE PARTICULAR ROW TO UDAT-UOW-BUS-OBJ-DATA.
	 * ****************************************************************</pre>*/
	private void wrRespUmtDataRecB() {
		// COB_CODE: MOVE XZC008-ACT-NOT-POL-REC-ROW
		//                                       TO UDAT-UOW-BUS-OBJ-DATA.
		ws.getHalludat().setUowBusObjData(ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted());
	}

	/**Original name: 15100-BUILD-SWITCH-BUFFER_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  BUILD SWITCH KEY FROM HUOH_SBO_BOBJ_NM. IF HUOH_SBO_BOBJ_NM
	 *  IS A SPECIAL CASE, THEN SUBSTITUTE WITH REQUIRED VALUE.
	 * ****************************************************************</pre>*/
	private void buildSwitchBuffer() {
		// COB_CODE: MOVE WS-CHILD-BUS-OBJ-NM    TO USW-BUS-OBJ-SWITCH.
		ws.getHallusw().setBusObjSwitch(ws.getWsNotSpecificMisc().getChildBusObjNm());
	}

	/**Original name: 17000-CHECK-AUTHORIZATION-A_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  LINK TO REQUIRED DATA PRIVACY MODULE:
	 *  MOVE TABLE NAME AND ROW DETAILS.
	 * ****************************************************************</pre>*/
	private void checkAuthorizationA() {
		// COB_CODE: MOVE 'ACT_NOT_POL_REC'      TO CIDP-TABLE-NAME.
		ws.getCidpTableInfo().setTableName("ACT_NOT_POL_REC");
		// COB_CODE: MOVE XZC008-ACT-NOT-POL-REC-ROW
		//                                       TO CIDP-TABLE-ROW.
		ws.getCidpTableInfo().setTableRow(ws.getXzc008ActNotPolRecRow().getXzc008ActNotPolRecRowFormatted());
	}

	/**Original name: 17000-CHECK-AUTHORIZATION-B_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  LINK TO REQUIRED DATA PRIVACY MODULE:
	 *  MOVE TABLE ROW.
	 * ****************************************************************</pre>*/
	private void checkAuthorizationB() {
		// COB_CODE: MOVE CIDP-TABLE-ROW         TO XZC008-ACT-NOT-POL-REC-ROW.
		ws.getXzc008ActNotPolRecRow().setXzc008ActNotPolRecRowFormatted(ws.getCidpTableInfo().getTableRowFormatted());
	}

	/**Original name: RNG_7000-CHECK-AUTHORIZATION_FIRST_SENTENCES-_-7000-CHECK-AUTHORIZATION-X<br>*/
	private void rng7000CheckAuthorization() {
		checkAuthorization();
		csdfCallDataPrivacy();
		csdfCallDataPrivacyX();
	}

	public void initWsMsgUmtArea() {
		ws.getUrqmCommon().setId("");
		ws.getUrqmCommon().setBusObj("");
		ws.getUrqmCommon().setRecSeqFormatted("00000");
		ws.getUrqmCommon().setUowBufferLengthFormatted("0000");
		ws.getUrqmCommon().setMsgBusObjNm("");
		ws.getUrqmCommon().getActionCode().setUbocPassThruAction("");
		ws.getUrqmCommon().setBusObjDataLengthFormatted("0000");
		ws.getUrqmCommon().setMsgData("");
	}

	public void initInputLinkage() {
		ws.getHalrlodrLockDriverStorage().getFunction().setFunction("");
		ws.getHalrlodrLockDriverStorage().setTchKey("");
		ws.getHalrlodrLockDriverStorage().setAppId("");
		ws.getHalrlodrLockDriverStorage().setTableNm("");
		ws.getHalrlodrLockDriverStorage().setBusObjNm("");
	}

	public void initUbocExtraData() {
		dfhcommarea.getUbocRecord().setAppDataBufferLengthFormatted("0000");
		dfhcommarea.getUbocRecord().setAppDataBuffer("");
	}

	public void initWsDataPrivacyInfo() {
		ws.getCidpTableInfo().setTableName("");
		ws.getCidpTableInfo().setTableRowLength(((short) 0));
		ws.getCidpTableInfo().setTableRow("");
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
