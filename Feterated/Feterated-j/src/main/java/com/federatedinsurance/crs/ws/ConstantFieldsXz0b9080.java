/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0B9080<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0b9080 {

	//==== PROPERTIES ====
	//Original name: CF-SP-GET-TTY-LIST-SVC
	private String spGetTtyListSvc = "XZC08090";
	//Original name: CF-LOSS-PAYEE
	private String lossPayee = "LPEMG";
	//Original name: CF-CI-CICS-CHANNEL
	private String ciCicsChannel = "XZPOLINFCHN";
	//Original name: CF-CI-SVC-IN-CONTAINER
	private String ciSvcInContainer = "XZPOLSVCIN";
	//Original name: CF-CI-SVC-OUT-CONTAINER
	private String ciSvcOutContainer = "XZPOLSVCOUT";

	//==== METHODS ====
	public String getSpGetTtyListSvc() {
		return this.spGetTtyListSvc;
	}

	public String getLossPayee() {
		return this.lossPayee;
	}

	public String getCiCicsChannel() {
		return this.ciCicsChannel;
	}

	public String getCiCicsChannelFormatted() {
		return Functions.padBlanks(getCiCicsChannel(), Len.CI_CICS_CHANNEL);
	}

	public String getCiSvcInContainer() {
		return this.ciSvcInContainer;
	}

	public String getCiSvcInContainerFormatted() {
		return Functions.padBlanks(getCiSvcInContainer(), Len.CI_SVC_IN_CONTAINER);
	}

	public String getCiSvcOutContainer() {
		return this.ciSvcOutContainer;
	}

	public String getCiSvcOutContainerFormatted() {
		return Functions.padBlanks(getCiSvcOutContainer(), Len.CI_SVC_OUT_CONTAINER);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CI_SVC_IN_CONTAINER = 16;
		public static final int CI_CICS_CHANNEL = 16;
		public static final int CI_SVC_OUT_CONTAINER = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
