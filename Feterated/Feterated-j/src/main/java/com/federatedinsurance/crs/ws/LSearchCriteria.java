/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.LScCrt1Cop;
import com.federatedinsurance.crs.ws.enums.LScCrt1Typ;
import com.federatedinsurance.crs.ws.enums.LScCrt2Typ;
import com.federatedinsurance.crs.ws.enums.LScEmpNonempCd;
import com.federatedinsurance.crs.ws.enums.LScNewSerInd;
import com.federatedinsurance.crs.ws.enums.LScSerTyp;
import com.federatedinsurance.crs.ws.enums.LScSpePrcInd;
import com.federatedinsurance.crs.ws.enums.LScStaCd;
import com.federatedinsurance.crs.ws.enums.LScUsrIdCd;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-SEARCH-CRITERIA<br>
 * Variable: L-SEARCH-CRITERIA from copybook TT008001<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LSearchCriteria extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: L-SC-USER
	private String user = DefaultValues.stringVal(Len.USER);
	//Original name: L-SC-NEW-SER-IND
	private LScNewSerInd newSerInd = new LScNewSerInd();
	//Original name: L-SC-SPE-PRC-IND
	private LScSpePrcInd spePrcInd = new LScSpePrcInd();
	//Original name: L-SC-SER-TYP
	private LScSerTyp serTyp = new LScSerTyp();
	//Original name: L-SC-STA-CD
	private LScStaCd staCd = new LScStaCd();
	//Original name: L-SC-EMP-NONEMP-CD
	private LScEmpNonempCd empNonempCd = new LScEmpNonempCd();
	//Original name: L-SC-USR-ID-CD
	private LScUsrIdCd usrIdCd = new LScUsrIdCd();
	//Original name: L-SC-CRT1-TYP
	private LScCrt1Typ crt1Typ = new LScCrt1Typ();
	//Original name: L-SC-CRT1-COP
	private LScCrt1Cop crt1Cop = new LScCrt1Cop();
	//Original name: L-SC-CRT1-VAL
	private String crt1Val = DefaultValues.stringVal(Len.CRT1_VAL);
	public static final String CRT1_VAL_N_A = "";
	//Original name: L-SC-CRT2-TYP
	private LScCrt2Typ crt2Typ = new LScCrt2Typ();
	//Original name: L-SC-CRT2-COP
	private LScCrt1Cop crt2Cop = new LScCrt1Cop();
	//Original name: L-SC-CRT2-VAL
	private String crt2Val = DefaultValues.stringVal(Len.CRT2_VAL);
	public static final String CRT2_VAL_N_A = "";
	//Original name: L-SC-CRT3-TYP
	private LScCrt2Typ crt3Typ = new LScCrt2Typ();
	//Original name: L-SC-CRT3-COP
	private LScCrt1Cop crt3Cop = new LScCrt1Cop();
	//Original name: L-SC-CRT3-VAL
	private String crt3Val = DefaultValues.stringVal(Len.CRT3_VAL);
	public static final String CRT3_VAL_N_A = "";
	//Original name: L-SC-CRT4-TYP
	private LScCrt2Typ crt4Typ = new LScCrt2Typ();
	//Original name: L-SC-CRT4-COP
	private LScCrt1Cop crt4Cop = new LScCrt1Cop();
	//Original name: L-SC-CRT4-VAL
	private String crt4Val = DefaultValues.stringVal(Len.CRT4_VAL);
	public static final String CRT4_VAL_N_A = "";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.SEARCH_CRITERIA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setSearchCriteriaBytes(buf);
	}

	public void setSearchCriteriaBytes(byte[] buffer) {
		setSearchCriteriaBytes(buffer, 1);
	}

	public byte[] getSearchCriteriaBytes() {
		byte[] buffer = new byte[Len.SEARCH_CRITERIA];
		return getSearchCriteriaBytes(buffer, 1);
	}

	public void setSearchCriteriaBytes(byte[] buffer, int offset) {
		int position = offset;
		user = MarshalByte.readString(buffer, position, Len.USER);
		position += Len.USER;
		newSerInd.setNewSerInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		spePrcInd.setSpePrcInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		serTyp.setSerTyp(MarshalByte.readString(buffer, position, LScSerTyp.Len.SER_TYP));
		position += LScSerTyp.Len.SER_TYP;
		staCd.setStaCd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		empNonempCd.setEmpNonempCd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		usrIdCd.setUsrIdCd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		crt1Typ.setCrt1Typ(MarshalByte.readString(buffer, position, LScCrt1Typ.Len.CRT1_TYP));
		position += LScCrt1Typ.Len.CRT1_TYP;
		crt1Cop.setCrt1Cop(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		crt1Val = MarshalByte.readString(buffer, position, Len.CRT1_VAL);
		position += Len.CRT1_VAL;
		crt2Typ.setCrt2Typ(MarshalByte.readString(buffer, position, LScCrt2Typ.Len.CRT2_TYP));
		position += LScCrt2Typ.Len.CRT2_TYP;
		crt2Cop.setCrt1Cop(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		crt2Val = MarshalByte.readString(buffer, position, Len.CRT2_VAL);
		position += Len.CRT2_VAL;
		crt3Typ.setCrt2Typ(MarshalByte.readString(buffer, position, LScCrt2Typ.Len.CRT2_TYP));
		position += LScCrt2Typ.Len.CRT2_TYP;
		crt3Cop.setCrt1Cop(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		crt3Val = MarshalByte.readString(buffer, position, Len.CRT3_VAL);
		position += Len.CRT3_VAL;
		crt4Typ.setCrt2Typ(MarshalByte.readString(buffer, position, LScCrt2Typ.Len.CRT2_TYP));
		position += LScCrt2Typ.Len.CRT2_TYP;
		crt4Cop.setCrt1Cop(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		crt4Val = MarshalByte.readString(buffer, position, Len.CRT4_VAL);
	}

	public byte[] getSearchCriteriaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, user, Len.USER);
		position += Len.USER;
		MarshalByte.writeChar(buffer, position, newSerInd.getNewSerInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, spePrcInd.getSpePrcInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, serTyp.getSerTyp(), LScSerTyp.Len.SER_TYP);
		position += LScSerTyp.Len.SER_TYP;
		MarshalByte.writeChar(buffer, position, staCd.getStaCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, empNonempCd.getEmpNonempCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, usrIdCd.getUsrIdCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, crt1Typ.getCrt1Typ(), LScCrt1Typ.Len.CRT1_TYP);
		position += LScCrt1Typ.Len.CRT1_TYP;
		MarshalByte.writeChar(buffer, position, crt1Cop.getCrt1Cop());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, crt1Val, Len.CRT1_VAL);
		position += Len.CRT1_VAL;
		MarshalByte.writeString(buffer, position, crt2Typ.getCrt2Typ(), LScCrt2Typ.Len.CRT2_TYP);
		position += LScCrt2Typ.Len.CRT2_TYP;
		MarshalByte.writeChar(buffer, position, crt2Cop.getCrt1Cop());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, crt2Val, Len.CRT2_VAL);
		position += Len.CRT2_VAL;
		MarshalByte.writeString(buffer, position, crt3Typ.getCrt2Typ(), LScCrt2Typ.Len.CRT2_TYP);
		position += LScCrt2Typ.Len.CRT2_TYP;
		MarshalByte.writeChar(buffer, position, crt3Cop.getCrt1Cop());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, crt3Val, Len.CRT3_VAL);
		position += Len.CRT3_VAL;
		MarshalByte.writeString(buffer, position, crt4Typ.getCrt2Typ(), LScCrt2Typ.Len.CRT2_TYP);
		position += LScCrt2Typ.Len.CRT2_TYP;
		MarshalByte.writeChar(buffer, position, crt4Cop.getCrt1Cop());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, crt4Val, Len.CRT4_VAL);
		return buffer;
	}

	public void setUser(String user) {
		this.user = Functions.subString(user, Len.USER);
	}

	public String getUser() {
		return this.user;
	}

	public void setCrt1Val(String crt1Val) {
		this.crt1Val = Functions.subString(crt1Val, Len.CRT1_VAL);
	}

	public String getCrt1Val() {
		return this.crt1Val;
	}

	public void setCrt2Val(String crt2Val) {
		this.crt2Val = Functions.subString(crt2Val, Len.CRT2_VAL);
	}

	public String getCrt2Val() {
		return this.crt2Val;
	}

	public void setCrt3Val(String crt3Val) {
		this.crt3Val = Functions.subString(crt3Val, Len.CRT3_VAL);
	}

	public String getCrt3Val() {
		return this.crt3Val;
	}

	public void setCrt4Val(String crt4Val) {
		this.crt4Val = Functions.subString(crt4Val, Len.CRT4_VAL);
	}

	public String getCrt4Val() {
		return this.crt4Val;
	}

	public LScCrt1Cop getCrt1Cop() {
		return crt1Cop;
	}

	public LScCrt1Typ getCrt1Typ() {
		return crt1Typ;
	}

	public LScEmpNonempCd getEmpNonempCd() {
		return empNonempCd;
	}

	public LScSerTyp getSerTyp() {
		return serTyp;
	}

	public LScStaCd getStaCd() {
		return staCd;
	}

	public LScUsrIdCd getUsrIdCd() {
		return usrIdCd;
	}

	@Override
	public byte[] serialize() {
		return getSearchCriteriaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int USER = 8;
		public static final int CRT1_VAL = 70;
		public static final int CRT2_VAL = 70;
		public static final int CRT3_VAL = 70;
		public static final int CRT4_VAL = 70;
		public static final int SEARCH_CRITERIA = USER + LScNewSerInd.Len.NEW_SER_IND + LScSpePrcInd.Len.SPE_PRC_IND + LScSerTyp.Len.SER_TYP
				+ LScStaCd.Len.STA_CD + LScEmpNonempCd.Len.EMP_NONEMP_CD + LScUsrIdCd.Len.USR_ID_CD + LScCrt1Typ.Len.CRT1_TYP
				+ LScCrt1Cop.Len.CRT1_COP + CRT1_VAL + LScCrt2Typ.Len.CRT2_TYP + LScCrt1Cop.Len.CRT1_COP + CRT2_VAL + LScCrt2Typ.Len.CRT2_TYP
				+ LScCrt1Cop.Len.CRT1_COP + CRT3_VAL + LScCrt2Typ.Len.CRT2_TYP + LScCrt1Cop.Len.CRT1_COP + CRT4_VAL;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
