/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.ActNotPol1Dao;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.copy.Xzy800GetNotDayRqrRow;
import com.federatedinsurance.crs.ws.UbocRecord;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsHalrlomgLinkage1;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsXz0a0004Row;
import com.federatedinsurance.crs.ws.Xz0b0004Data;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0B0004<br>
 * <pre>AUTHOR.       FEDERATED INSURANCE.
 * DATE-WRITTEN. 04 AUG 2008.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - GET_RESCIND_IND_BPO                         **
 *                     XREF OBJ NM: XZGET_RESCIND_IND_BPO        *
 *                     UOW        : XZ_GET_RESCIND_IND           *
 *                     OPERATIONS : GetRescindInd                *
 * *                                                             **
 * * PURPOSE -  READ RESCIND INDICATOR ROWS AND RETURN 'Y' OR 'N'**
 * *                                                             **
 * * PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS **
 * *                       LINKED TO BY THE FRAMEWORK DRIVER.    **
 * *                                                             **
 * *                                                             **
 * * DATA ACCESS METHODS - UMT STORAGE RECORDS                   **
 * *                       DB2 DATABASE                          **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * * NOTE: THIS LOG FOR INFRASTRUCTURE USE ONLY FOR TEMPLATE     **
 * *       VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     **
 * *       APPLICATION CODING.                                   **
 * *     T E M P L A T E   M A I N T E N A N C E   L O G         **
 * * CASE#     DATE       PROG       DESCRIPTION                 **
 * * --------  ---------  --------   ----------------------------**
 * * TS129     06/13/2006 E404LJL    TEMPLATE CREATED            **
 * * YJ249     04/27/2007 E404NEM    STDS CHGS                   **
 * * TS130     12/28/2007 E404JSP    Changed a few bugs          **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * -------  ----------  --------   ----------------------------**
 * * TO07614  08/04/2008  E404JWL    INITIAL PROGRAM             **
 * * TO07614  09/11/2008  E404GRK    Revised Logic               **
 * * TO07614  03/06/2009  E404KXS    Added Variable Timeframe    **
 * * TO07614  03/31/2009  E404KXS    Added call to utility pgm   **
 * * TO07614  05/18/2009  E404KXS    Check for accrued expiration**
 * *                                 time                        **
 * *CP081-01  10/12/2012  E404AKW    RECOMPILE FOR XZ0Y8000 CHGS **
 * ****************************************************************</pre>*/
public class Xz0b0004 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS
	 * ******************************************************************
	 *  THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 13      **
	 * ******************************************************************</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ActNotPol1Dao actNotPol1Dao = new ActNotPol1Dao(dbAccessStatus);
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0b0004Data ws = new Xz0b0004Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private WsHalrlomgLinkage1 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, WsHalrlomgLinkage1 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0b0004 getInstance() {
		return (Programs.getInstance(Xz0b0004.class));
	}

	/**Original name: 1000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   MAIN PROCESSING CONTROL                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINING-HOUSEKEEPING.
		beginingHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-PROCESS-RESCIND-IND.
		processRescindInd();
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *  INITIALIZATION CONTROL                                        *
	 *                                                                *
	 * ****************************************************************
	 * * INITIALIZE ERROR/WARNING STORAGE</pre>*/
	private void beginingHousekeeping() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//* VALIDATE UBOC IN COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2100-READ-REQ-UMT-ROW.
		readReqUmtRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-READ-REQ-UMT-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR THE BPO INPUT ROW                     *
	 * *****************************************************************</pre>*/
	private void readReqUmtRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: INITIALIZE WS-XZ0A0004-ROW.
		initWsXz0a0004Row();
		// COB_CODE: SET HALRURQA-READ-FUNC      TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM          TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNm());
		// COB_CODE: MOVE +1                     TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(1);
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-XZ0A0004-ROW.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsXz0a0004Row());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: IF HALRURQA-REC-NOT-FOUND
		//               GO TO 2100-EXIT
		//           END-IF.
		if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUEST-MSG-MISSING
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequestMsgMissing();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '2100-READ-REQ-UMT-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-READ-REQ-UMT-ROW");
			// COB_CODE: MOVE 'NO RECORD FOUND ON REQ MSG STORE'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO RECORD FOUND ON REQ MSG STORE");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HALRURQA-BUS-OBJ-NM='
			//                  HALRURQA-BUS-OBJ-NM
			//                  '; HALRURQA-REC-SEQ='
			//                  HALRURQA-REC-SEQ
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HALRURQA-BUS-OBJ-NM=").append(ws.getWsHalrurqaLinkage().getBusObjNmFormatted())
							.append("; HALRURQA-REC-SEQ=").append(ws.getWsHalrurqaLinkage().getRecSeqFormatted()).toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
	}

	/**Original name: 3000-PROCESS-RESCIND-IND_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DETERMINE IF THE RESCIND IS VALID. IT CAN ONLY BE VALID        *
	 *  IF WE HAVE PROCESSED AN IMPENDING CANCELLATION WITHIN THE      *
	 *  VALID NOTIFICATION TIMEFRAME.                                  *
	 * *****************************************************************
	 * * GET REQUIRED NOTICE DAYS FOR DETERMINING TIMEFRAME</pre>*/
	private void processRescindInd() {
		// COB_CODE: PERFORM 3100-GET-NBR-OF-NOT-DAY.
		getNbrOfNotDay();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* DETERMINE IF RESCIND IS VALID
		// COB_CODE: PERFORM 3200-PROCESS-RESCIND-IND.
		processRescindInd1();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3100-GET-NBR-OF-NOT-DAY_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE SUBROUTINE TO GET THE NUMBER OF NOTIFICATION DAYS.    *
	 * *****************************************************************</pre>*/
	private void getNbrOfNotDay() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE WS-XZ0Y8000-ROW.
		initWsXz0y8000Row();
		// COB_CODE: MOVE CF-ACT-NOT-TYP-CD-IMP  TO XZY800-ACT-NOT-TYP-CD.
		ws.getXzy800GetNotDayRqrRow().setActNotTypCd(ws.getCfActNotTypCdImp());
		// COB_CODE: MOVE XZA004-POLICY-NUMBER   TO XZY800-POL-NBR.
		ws.getXzy800GetNotDayRqrRow().setPolNbr(ws.getWsXz0a0004Row().getPolicyNumber());
		// COB_CODE: MOVE XZA004-POLICY-EFF-DT   TO XZY800-POL-EFF-DT.
		ws.getXzy800GetNotDayRqrRow().setPolEffDt(ws.getWsXz0a0004Row().getPolicyEffDt());
		// COB_CODE: MOVE XZA004-USERID          TO XZY800-USERID.
		ws.getXzy800GetNotDayRqrRow().setUserid(ws.getWsXz0a0004Row().getUserid());
		// COB_CODE: MOVE SPACES                 TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE LENGTH OF WS-XZ0Y8000-ROW
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) Xz0b0004Data.Len.WS_XZ0Y8000_ROW));
		// COB_CODE: MOVE WS-XZ0Y8000-ROW        TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getWsXz0y8000RowFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-GET-NOT-DAY-RQR-UTY-PGM)
		//               COMMAREA(UBOC-RECORD)
		//               LENGTH(LENGTH OF UBOC-RECORD)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0B0004", execContext).commarea(dfhcommarea.getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD)
				.link(ws.getCfGetNotDayRqrUtyPgm(), new Xz0u8000());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE CF-GET-NOT-DAY-RQR-UTY-PGM
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getCfGetNotDayRqrUtyPgm());
			// COB_CODE: MOVE '3100-GET-NBR-OF-NOT-DAY'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3100-GET-NBR-OF-NOT-DAY");
			// COB_CODE: MOVE 'LINK TO GET NOT DAYS REQUIRED UTILITY FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO GET NOT DAYS REQUIRED UTILITY FAILED");
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZA004-ACCOUNT-NUMBER
			//                  '; ACT-NOT-TYP-CD='
			//                  CF-ACT-NOT-TYP-CD-IMP
			//                  '; POL-NBR='
			//                  XZA004-POLICY-NUMBER
			//                  '; POL-EFF-DT='
			//                  XZA004-POLICY-EFF-DT
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "CSR-ACT-NBR=", ws.getWsXz0a0004Row().getAccountNumberFormatted(), "; ACT-NOT-TYP-CD=",
							ws.getCfActNotTypCdImpFormatted(), "; POL-NBR=", ws.getWsXz0a0004Row().getPolicyNumberFormatted(), "; POL-EFF-DT=",
							ws.getWsXz0a0004Row().getPolicyEffDtFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER   TO WS-XZ0Y8000-ROW.
		ws.setWsXz0y8000RowFormatted(dfhcommarea.getUbocRecord().getAppDataBufferFormatted());
		// COB_CODE: IF XZY800-NLBE-OCC
		//               GO TO 3100-EXIT
		//           END-IF.
		if (ws.getXzy800GetNotDayRqrRow().getNlbeInd().isNlbeOcc()) {
			// COB_CODE: SET UBOC-HALT-AND-RETURN
			//                                   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setBusErr();
			// COB_CODE: MOVE 'NOT_DAY_REQ'      TO NLBE-FAILED-TABLE-OR-FILE
			//                                      NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedTableOrFile("NOT_DAY_REQ");
			ws.getNlbeCommon().setFailedColumnOrField("NOT_DAY_REQ");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO NLBE-ERROR-CODE
			ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE XZY800-NON-LOG-ERR-MSG
			//                                   TO WS-NONLOG-ERR-ALLTXT-TEXT
			ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getXzy800GetNotDayRqrRow().getNonLogErrMsg());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: IF XZY800-WARNINGS-OCC
		//               PERFORM 3110-OUTPUT-WARNINGS
		//           END-IF.
		if (ws.getXzy800GetNotDayRqrRow().getWarningInd().isXzy800WarningsOcc()) {
			// COB_CODE: PERFORM 3110-OUTPUT-WARNINGS
			rng3110OutputWarnings();
		}
	}

	/**Original name: 3110-OUTPUT-WARNINGS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LOOP THROUGH ALL OF THE WARNINGS THAT WERE PASSED BACK FROM    *
	 *  THE UTILITY AND CREATE WARNING MESSAGES.                       *
	 *      NOTE:  THE UTILITY'S WARNING AREA IS A DIFFERENT SIZE THAN *
	 *  THE PPC'S WARNING AREA, THUS THE DIFFERENT WARNING SECTION.    *
	 * *****************************************************************</pre>*/
	private void outputWarnings() {
		// COB_CODE: MOVE +0                     TO SS-WNG-IDX.
		ws.setSsWngIdx(((short) 0));
	}

	/**Original name: 3110-A<br>*/
	private String a() {
		// COB_CODE: ADD +1                      TO SS-WNG-IDX.
		ws.setSsWngIdx(Trunc.toShort(1 + ws.getSsWngIdx(), 4));
		// COB_CODE: IF XZY800-WARN-MSG(SS-WNG-IDX) = SPACES
		//               GO TO 3110-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzy800GetNotDayRqrRow().getWarnings(ws.getSsWngIdx()).getXzy800WarnMsg())) {
			// COB_CODE: GO TO 3110-EXIT
			return "";
		}
		// COB_CODE: SET WS-NON-LOGGABLE-WARNING TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setWarning();
		// COB_CODE: MOVE 'NOT_DAY_REQ'          TO NLBE-FAILED-TABLE-OR-FILE
		//                                          NLBE-FAILED-COLUMN-OR-FIELD.
		ws.getNlbeCommon().setFailedTableOrFile("NOT_DAY_REQ");
		ws.getNlbeCommon().setFailedColumnOrField("NOT_DAY_REQ");
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE XZY800-WARN-MSG(SS-WNG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getXzy800GetNotDayRqrRow().getWarnings(ws.getSsWngIdx()).getXzy800WarnMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-WNG-IDX-MAX
		//               GO TO 3110-EXIT
		//           END-IF.
		if (ws.isSsWngIdxMax()) {
			// COB_CODE: GO TO 3110-EXIT
			return "";
		}
		// COB_CODE: GO TO 3110-A.
		return "3110-A";
	}

	/**Original name: 3200-PROCESS-RESCIND-IND_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CONTROL FETCH DATA FROM SELECTED CURSOR.                       *
	 * *****************************************************************</pre>*/
	private void processRescindInd1() {
		// COB_CODE: INITIALIZE DCLACT-NOT
		//                      DCLACT-NOT-POL.
		initDclactNot();
		initDclactNotPol();
		// COB_CODE: MOVE XZA004-ACCOUNT-NUMBER  TO CSR-ACT-NBR OF DCLACT-NOT.
		ws.getDclactNot().setCsrActNbr(ws.getWsXz0a0004Row().getAccountNumber());
		// COB_CODE: MOVE XZA004-POLICY-NUMBER   TO POL-NBR OF DCLACT-NOT-POL.
		ws.getDclactNotPol().setPolNbr(ws.getWsXz0a0004Row().getPolicyNumber());
		// COB_CODE: MOVE XZA004-POLICY-CANC-DT  TO WS-POLICY-CANC-DT.
		ws.getWorkingStorageArea().setPolicyCancDt(ws.getWsXz0a0004Row().getPolicyCancDt());
		// COB_CODE: MOVE XZY800-NBR-OF-NOT-DAY  TO WS-NBR-OF-NOT-DAY.
		ws.getWorkingStorageArea().setNbrOfNotDay(Trunc.toShort(ws.getXzy800GetNotDayRqrRow().getNbrOfNotDay(), 4));
		// COB_CODE: EXEC SQL
		//               SELECT A.NOT_PRC_TS
		//                 INTO :DCLACT-NOT.NOT-PRC-TS
		//                 FROM ACT_NOT A,
		//                      ACT_NOT_POL B
		//                WHERE A.CSR_ACT_NBR = :DCLACT-NOT.CSR-ACT-NBR
		//                  AND A.ACT_NOT_TYP_CD = :CF-ACT-NOT-TYP-CD-IMP
		//                  AND A.CSR_ACT_NBR = B.CSR_ACT_NBR
		//                  AND A.NOT_PRC_TS = B.NOT_PRC_TS
		//                  AND B.POL_NBR = :DCLACT-NOT-POL.POL-NBR
		//                  AND (DATE(B.NOT_PRC_TS) BETWEEN
		//                      DATE(:WS-POLICY-CANC-DT)
		//                       - :WS-NBR-OF-NOT-DAY DAYS
		//                        AND
		//                      :WS-POLICY-CANC-DT
		//                   OR B.NOT_EFF_DT = :WS-POLICY-CANC-DT)
		//                FETCH FIRST 1 ROW ONLY
		//           END-EXEC.
		ws.getDclactNot().setNotPrcTs(actNotPol1Dao.selectRec(ws, ws.getDclactNot().getNotPrcTs()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   SET WS-RI-YES       TO TRUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   SET WS-RI-NO        TO TRUE
		//               WHEN OTHER
		//                   GO TO 3200-EXIT
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET WS-RI-YES       TO TRUE
			ws.getWorkingStorageArea().getRescindInd().setYes();
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-RI-NO        TO TRUE
			ws.getWorkingStorageArea().getRescindInd().setNo();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED    OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT    OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'RESCIND IND'  TO EFAL-ERR-OBJECT-NAME
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("RESCIND IND");
			// COB_CODE: MOVE '3200-PROCESS-RESCIND-IND'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3200-PROCESS-RESCIND-IND");
			// COB_CODE: MOVE 'ACT_NOT & ACT_NOT_POL SELECT FAILED'
			//                               TO EFAL-ERR-COMMENT
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ACT_NOT & ACT_NOT_POL SELECT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3200-EXIT
			return;
		}
		// COB_CODE: PERFORM 3210-WRITE-RESPONSE-ROW.
		writeResponseRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3200-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3200-EXIT
			return;
		}
	}

	/**Original name: 3210-WRITE-RESPONSE-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  WRITE RESCIND IND TO RESPONSE UMT                              *
	 * *****************************************************************</pre>*/
	private void writeResponseRow() {
		Halrresp halrresp = null;
		// COB_CODE: INITIALIZE WS-XZ0A0004-ROW.
		initWsXz0a0004Row();
		// COB_CODE: MOVE WS-RESCIND-IND         TO XZA004-RESCIND-IND.
		ws.getWsXz0a0004Row().setRescindInd(ws.getWorkingStorageArea().getRescindInd().getRescindInd());
		// COB_CODE: SET HALRRESP-WRITE-FUNC     TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM          TO HALRRESP-BUS-OBJ-NM.
		ws.getWsHalrrespLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNm());
		// COB_CODE: MOVE LENGTH OF WS-XZ0A0004-ROW
		//                                       TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(((short) WsXz0a0004Row.Len.WS_XZ0A0004_ROW));
		// COB_CODE: CALL HALRRESP-HALRRESP-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRRESP-LINKAGE
		//                WS-XZ0A0004-ROW.
		halrresp = Halrresp.getInstance();
		halrresp.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrrespLinkage(), ws.getWsXz0a0004Row());
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowKeyReplaceStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning()
				&& dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWorkingStorageArea().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0B0004", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getUbocRecord().getCommInfo()
				.setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getUbocRecord().getCommInfo()
				.setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWorkingStorageArea().getApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: RNG_3110-OUTPUT-WARNINGS_FIRST_SENTENCES-_-3110-EXIT<br>*/
	private void rng3110OutputWarnings() {
		String retcode = "";
		boolean goto3110A = false;
		outputWarnings();
		do {
			goto3110A = false;
			retcode = a();
		} while (retcode.equals("3110-A"));
	}

	public void initWsXz0a0004Row() {
		ws.getWsXz0a0004Row().setPrimaryFunction("");
		ws.getWsXz0a0004Row().setAccountNumber("");
		ws.getWsXz0a0004Row().setOwnerState("");
		ws.getWsXz0a0004Row().setPolicyNumber("");
		ws.getWsXz0a0004Row().setPolicyEffDt("");
		ws.getWsXz0a0004Row().setPolicyExpDt("");
		ws.getWsXz0a0004Row().setPolicyType("");
		ws.getWsXz0a0004Row().setPolicyCancDt("");
		ws.getWsXz0a0004Row().setRescindInd(Types.SPACE_CHAR);
		ws.getWsXz0a0004Row().setUserid("");
	}

	public void initWsXz0y8000Row() {
		ws.getXzy800GetNotDayRqrRow().setPolPriRskStAbb("");
		ws.getXzy800GetNotDayRqrRow().setActNotTypCd("");
		ws.getXzy800GetNotDayRqrRow().setPolTypCd("");
		ws.getXzy800GetNotDayRqrRow().setMinPolEffDt("");
		ws.getXzy800GetNotDayRqrRow().setPolNbr("");
		ws.getXzy800GetNotDayRqrRow().setPolEffDt("");
		ws.getXzy800GetNotDayRqrRow().setNbrOfNotDay(0);
		ws.getXzy800GetNotDayRqrRow().setUserid("");
		ws.getXzy800GetNotDayRqrRow().getNlbeInd().setNlbeInd(Types.SPACE_CHAR);
		ws.getXzy800GetNotDayRqrRow().getWarningInd().setWarningInd(Types.SPACE_CHAR);
		ws.getXzy800GetNotDayRqrRow().setNonLogErrMsg("");
		for (int idx0 = 1; idx0 <= Xzy800GetNotDayRqrRow.WARNINGS_MAXOCCURS; idx0++) {
			ws.getXzy800GetNotDayRqrRow().getWarnings(idx0).setXzy800WarnMsg("");
		}
	}

	public void initDclactNot() {
		ws.getDclactNot().setCsrActNbr("");
		ws.getDclactNot().setNotPrcTs("");
		ws.getDclactNot().setActNotTypCd("");
		ws.getDclactNot().setNotDt("");
		ws.getDclactNot().setActOwnCltId("");
		ws.getDclactNot().setActOwnAdrId("");
		ws.getDclactNot().setEmpId("");
		ws.getDclactNot().setStaMdfTs("");
		ws.getDclactNot().setActNotStaCd("");
		ws.getDclactNot().setPdcNbr("");
		ws.getDclactNot().setPdcNmLen(((short) 0));
		ws.getDclactNot().setPdcNmText("");
		ws.getDclactNot().setSegCd("");
		ws.getDclactNot().setActTypCd("");
		ws.getDclactNot().setTotFeeAmt(new AfDecimal(0, 10, 2));
		ws.getDclactNot().setStAbb("");
		ws.getDclactNot().setAddCncDay(((short) 0));
		ws.getDclactNot().setReaDesLen(((short) 0));
		ws.getDclactNot().setReaDesText("");
		ws.getDclactNot().setCerHldNotInd(Types.SPACE_CHAR);
	}

	public void initDclactNotPol() {
		ws.getDclactNotPol().setCsrActNbr("");
		ws.getDclactNotPol().setNotPrcTs("");
		ws.getDclactNotPol().setPolNbr("");
		ws.getDclactNotPol().setPolTypCd("");
		ws.getDclactNotPol().setPolPriRskStAbb("");
		ws.getDclactNotPol().setNotEffDt("");
		ws.getDclactNotPol().setPolEffDt("");
		ws.getDclactNotPol().setPolExpDt("");
		ws.getDclactNotPol().setPolDueAmt(new AfDecimal(0, 10, 2));
		ws.getDclactNotPol().setNinCltId("");
		ws.getDclactNotPol().setNinAdrId("");
		ws.getDclactNotPol().setWfStartedInd(Types.SPACE_CHAR);
		ws.getDclactNotPol().setPolBilStaCd(Types.SPACE_CHAR);
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
