/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ACT_NOT_POL, ACT_NOT_POL_FRM]
 * 
 */
public interface IActNotPolFrm extends BaseSqlTo {

	/**
	 * Host Variable CSR-ACT-NBR
	 * 
	 */
	String getCsrActNbr();

	void setCsrActNbr(String csrActNbr);

	/**
	 * Host Variable NOT-PRC-TS
	 * 
	 */
	String getNotPrcTs();

	void setNotPrcTs(String notPrcTs);

	/**
	 * Host Variable POL-NBR
	 * 
	 */
	String getPolNbr();

	void setPolNbr(String polNbr);

	/**
	 * Host Variable POL-TYP-CD
	 * 
	 */
	String getPolTypCd();

	void setPolTypCd(String polTypCd);

	/**
	 * Host Variable POL-PRI-RSK-ST-ABB
	 * 
	 */
	String getPolPriRskStAbb();

	void setPolPriRskStAbb(String polPriRskStAbb);

	/**
	 * Host Variable NOT-EFF-DT
	 * 
	 */
	String getNotEffDt();

	void setNotEffDt(String notEffDt);

	/**
	 * Nullable property for NOT-EFF-DT
	 * 
	 */
	String getNotEffDtObj();

	void setNotEffDtObj(String notEffDtObj);

	/**
	 * Host Variable POL-EFF-DT
	 * 
	 */
	String getPolEffDt();

	void setPolEffDt(String polEffDt);

	/**
	 * Host Variable POL-EXP-DT
	 * 
	 */
	String getPolExpDt();

	void setPolExpDt(String polExpDt);

	/**
	 * Host Variable POL-DUE-AMT
	 * 
	 */
	AfDecimal getPolDueAmt();

	void setPolDueAmt(AfDecimal polDueAmt);

	/**
	 * Nullable property for POL-DUE-AMT
	 * 
	 */
	AfDecimal getPolDueAmtObj();

	void setPolDueAmtObj(AfDecimal polDueAmtObj);

	/**
	 * Host Variable NIN-CLT-ID
	 * 
	 */
	String getNinCltId();

	void setNinCltId(String ninCltId);

	/**
	 * Host Variable NIN-ADR-ID
	 * 
	 */
	String getNinAdrId();

	void setNinAdrId(String ninAdrId);

	/**
	 * Host Variable WF-STARTED-IND
	 * 
	 */
	char getWfStartedInd();

	void setWfStartedInd(char wfStartedInd);

	/**
	 * Nullable property for WF-STARTED-IND
	 * 
	 */
	Character getWfStartedIndObj();

	void setWfStartedIndObj(Character wfStartedIndObj);

	/**
	 * Host Variable POL-BIL-STA-CD
	 * 
	 */
	char getPolBilStaCd();

	void setPolBilStaCd(char polBilStaCd);

	/**
	 * Nullable property for POL-BIL-STA-CD
	 * 
	 */
	Character getPolBilStaCdObj();

	void setPolBilStaCdObj(Character polBilStaCdObj);
};
