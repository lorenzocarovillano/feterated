/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalBoMduXrfVDao;
import com.federatedinsurance.crs.commons.data.dao.HalUowPrcSeqVDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.ws.DfhcommareaHalouieh;
import com.federatedinsurance.crs.ws.HalouiehData;
import com.federatedinsurance.crs.ws.UbocCommInfoCaws002;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: HALOUIEH<br>
 * <pre>AUTHOR.  RICHARD SYME.
 * DATE-WRITTEN. MAY 2000.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  INTRA EDIT HUB                             **
 * *                                                             **
 * * PLATFORM - I-BASE                                           **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -                                                   **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED IN THE FOLLOW-**
 * *                       ING WAYS:                             **
 * *                                                             **
 * *                       1)LINK FROM THE MESSAGE CONTROL       **
 * *                         MODULE (HALOMCM).                   **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #       DATE     PROG    DESCRIPTION                     **
 * * --------  --------  ------  --------------------------------**
 * * SAVANNAH  09MAY00   RDS     NEW PROGRAM.                    **
 * * C15776    14AUG2001 99361   REMOVE BUS_OBJ_NM_MDU COLUMN    **
 * *                             FROM HAL_UOW_PRC_SEQ TABLE.     **
 * * 17241     13NOV01   03539   REPLACE REFERENCES TO IAP WITH  **
 * *                             COMPARABLE INFRASTRUCTURE CODE. **
 * * 17543O    25FEB02   18448   ADD FOR READ ONLY TO CURSOR FOR **
 * *                             PERFORMANCE REASONS SINCE THE   **
 * *                             ROWS ARE NOT ALTERED.           **
 * * 25389     19AUG02   18448   ADD CODE TO DETERMINE IF EVERY  **
 * *                             STEP OF THE SIMPLE EDITS AND    **
 * *                             PRIM KEYS PHASE SHOULD BE       **
 * *                             EXECUTED OR IF THE EDITS STEP   **
 * *                             SHOULD BE BYPASSED.             **
 * ****************************************************************</pre>*/
public class Halouieh extends Program {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private HalUowPrcSeqVDao halUowPrcSeqVDao = new HalUowPrcSeqVDao(dbAccessStatus);
	private HalBoMduXrfVDao halBoMduXrfVDao = new HalBoMduXrfVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private HalouiehData ws = new HalouiehData();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaHalouieh dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaHalouieh dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		main1();
		exitModule();
		return 0;
	}

	public static Halouieh getInstance() {
		return (Programs.getInstance(Halouieh.class));
	}

	/**Original name: 0000-MAIN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   THE 0000-MAIN PARAGRAPH IS RESPONSIBLE FOR CONTROLLING THE    *
	 *   PROCESSING OF THE FUNCTION PASSED TO IT.                      *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void main1() {
		// COB_CODE: IF NOT UBOC-HALT-AND-RETURN
		//              PERFORM 0050-INITIALIZE
		//           END-IF.
		if (!dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 0050-INITIALIZE
			initialize();
		}
		// COB_CODE: IF NOT UBOC-HALT-AND-RETURN
		//              PERFORM 0100-OPEN-PST-CURSOR
		//           END-IF.
		if (!dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 0100-OPEN-PST-CURSOR
			openPstCursor();
		}
		// COB_CODE: IF NOT UBOC-HALT-AND-RETURN
		//              PERFORM 0200-PROCESS-EDITS
		//           END-IF.
		if (!dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 0200-PROCESS-EDITS
			processEdits();
		}
		// COB_CODE: IF NOT UBOC-HALT-AND-RETURN
		//              PERFORM 0500-CLOSE-CURSOR
		//           END-IF.
		if (!dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 0500-CLOSE-CURSOR
			closeCursor();
		}
	}

	/**Original name: EXIT-MODULE<br>*/
	private void exitModule() {
		// COB_CODE: EXEC CICS
		//                RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 0050-INITIALIZE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  INITIALIZE THE ERROR PROCESSING VARIABLES.                     *
	 *  READ THE REQUEST UMT FOR A BYPASS_SIMPLE_EDIT_STEP MESSAGE.    *
	 *  ONLY ONE ROW FOR BYPASS_SIMPLE_EDIT_STEP SHOULD BE FOUND.      *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void initialize() {
		Halrurqa halrurqa = null;
		// COB_CODE: INITIALIZE ESTO-STORE-INFO
		//                      ESTO-RETURN-INFO.
		initEstoStoreInfo();
		initEstoReturnInfo();
		//* SEARCH FOR A BYPASS RECORD IN THE MESSAGE.
		// COB_CODE: SET HALRURQA-READ-FUNC     TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE WS-BYPASS-SE-URQM-LIT TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWsWorkFields().getBypassSeUrqmLit());
		// COB_CODE: MOVE 1                     TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(1);
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-GENERIC-REC.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsGenericRec());
		//* IF NORMAL, SET ACTION TO BYPASS-SIMPLE-EDIT-STEP.
		//* IF NOTFND, SET ACTION TO SIMPLE-EDIT-AND-PRIM-KEYS.
		//* OTHERWISE WE HAVE AN UNEXPECTED ERROR.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0050-INITIALIZE-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0050-INITIALIZE-X
			return;
		}
		// COB_CODE: IF HALRURQA-REC-FOUND
		//               SET WS-BYPASS-SE-STEP TO TRUE
		//           ELSE
		//               SET WS-SE-AND-PRIM-KEYS TO TRUE
		//           END-IF.
		if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecFound()) {
			// COB_CODE: SET WS-BYPASS-SE-STEP TO TRUE
			ws.getWsBoActionSw().setBypassSeStep();
		} else {
			// COB_CODE: SET WS-SE-AND-PRIM-KEYS TO TRUE
			ws.getWsBoActionSw().setSeAndPrimKeys();
		}
	}

	/**Original name: 0100-OPEN-PST-CURSOR_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  A>  INITIALISE CURSOR SWITCH                                  *
	 *  B>  MOVE UOW NAME AND DATA PROCESS BRANCH TYPE TO             *
	 *      DCLGEN FOR CUR1 CURSOR                                    *
	 *  C>  OPEN CUR1                                                 *
	 * ****************************************************************</pre>*/
	private void openPstCursor() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-NOT-END-OF-CUR1   TO TRUE.
		ws.getWsEndOfCur1Sw().setNotEndOfCur1();
		// COB_CODE: MOVE UBOC-UOW-NAME       TO HUPS-UOW-NM.
		ws.getDclhalUowPrcSeq().setHupsUowNm(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: SET PST-DATA-PROC-BRANCH TO TRUE.
		ws.getHallpst().getBrnchProcTyp().setPstDataProcBranch();
		// COB_CODE: MOVE PST-BRNCH-PROC-TYP  TO HUPS-BRN-PRC-CD.
		ws.getDclhalUowPrcSeq().setHupsBrnPrcCd(ws.getHallpst().getBrnchProcTyp().getBrnchProcTyp());
		// COB_CODE: EXEC SQL
		//               OPEN CUR1
		//           END-EXEC.
		halUowPrcSeqVDao.openCur1(ws.getDclhalUowPrcSeq().getHupsUowNm(), ws.getDclhalUowPrcSeq().getHupsBrnPrcCd());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//              GO TO 0100-OPEN-PST-CURSOR-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_UOW_PRC_SEQ_V'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_PRC_SEQ_V");
			// COB_CODE: MOVE '0100-OPEN-PST-CURSOR'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-OPEN-PST-CURSOR");
			// COB_CODE: MOVE 'OPEN CUR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CUR FAILED");
			// COB_CODE: STRING 'HUPS-BRN-PRC-CD    = ' HUPS-BRN-PRC-CD    ';'
			//                  'HUPS-UOW-NM      = '     HUPS-UOW-NM      ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "HUPS-BRN-PRC-CD    = ", String.valueOf(ws.getDclhalUowPrcSeq().getHupsBrnPrcCd()), ";", "HUPS-UOW-NM      = ",
							ws.getDclhalUowPrcSeq().getHupsUowNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-OPEN-PST-CURSOR-X
			return;
		}
	}

	/**Original name: 0200-PROCESS-EDITS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  A>  FETCH CURSOR UNTIL THERE ARE NO MORE ROWS ON              *
	 *      HAL_UOW_PRC_SEQ_V OR UNTIL THERE IS A LOGGABLE ERROR      *
	 * ****************************************************************</pre>*/
	private void processEdits() {
		// COB_CODE: PERFORM 0300-FETCH-CURSOR
		//             UNTIL UBOC-UOW-LOGGABLE-ERRORS
		//                OR WS-END-OF-CUR1.
		while (!(dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()
				|| ws.getWsEndOfCur1Sw().isEndOfCur1())) {
			fetchCursor();
		}
	}

	/**Original name: 0300-FETCH-CURSOR_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  A>  FETCH OBJECT MODULE NAME                                  *
	 * ****************************************************************
	 * *           , :HUPS-BUS-OBJ-MDU-NM</pre>*/
	private void fetchCursor() {
		ConcatUtil concatUtil = null;
		// COB_CODE:      EXEC SQL
		//                    FETCH CUR1
		//                     INTO :ROOT-BOBJ-NM
		//                        , :UOW-SEQ-NBR
		//           **           , :HUPS-BUS-OBJ-MDU-NM
		//                END-EXEC.
		halUowPrcSeqVDao.fetchCur1(ws.getDclhalUowPrcSeq());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   MOVE PST-ROOT-BUS-OBJ TO UBOC-PRIMARY-BUS-OBJ
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0300-FETCH-CURSOR-X
		//               WHEN OTHER
		//                   GO TO 0300-FETCH-CURSOR-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: IF WS-BYPASS-SE-STEP
			//                 TO TRUE
			//           ELSE
			//                 TO TRUE
			//           END-IF
			if (ws.getWsBoActionSw().isBypassSeStep()) {
				// COB_CODE: SET BYPASS-SIMPLE-EDIT-STEP OF UBOC-COMM-INFO
				//             TO TRUE
				dfhcommarea.getCommInfo().getUbocPassThruAction().setBypassSimpleEditStep();
			} else {
				// COB_CODE: SET SIMPLE-EDIT-AND-PRIM-KEYS OF UBOC-COMM-INFO
				//             TO TRUE
				dfhcommarea.getCommInfo().getUbocPassThruAction().setSimpleEditAndPrimKeys();
			}
			// COB_CODE: MOVE ROOT-BOBJ-NM     TO PST-ROOT-BUS-OBJ
			//                                    WS-BUS-OBJ-NM
			ws.getHallpst().setRootBusObj(ws.getDclhalUowPrcSeq().getRootBobjNm());
			ws.getWsWorkFields().setBusObjNm(ws.getDclhalUowPrcSeq().getRootBobjNm());
			// COB_CODE: PERFORM 0600-READ-BOBJ-MDU-XRF
			readBobjMduXrf();
			//*           MOVE HUPS-BUS-OBJ-MDU-NM TO PST-OBJ-MODULE
			// COB_CODE: MOVE PST-ROOT-BUS-OBJ TO UBOC-PRIMARY-BUS-OBJ
			dfhcommarea.getCommInfo().setUbocPrimaryBusObj(ws.getHallpst().getRootBusObj());
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CUR1 TO TRUE
			ws.getWsEndOfCur1Sw().setEndOfCur1();
			// COB_CODE: GO TO 0300-FETCH-CURSOR-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'HAL_UOW_PRC_SEQ_V'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_PRC_SEQ_V");
			// COB_CODE: MOVE '0300-FETCH-CURSOR'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0300-FETCH-CURSOR");
			// COB_CODE: MOVE 'FETCH CUR FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH CUR FAILED");
			// COB_CODE: STRING 'BRNCH-PROC-TYP = '    HUPS-BRN-PRC-CD    ';'
			//                  'HUPS-UOW-NM      = '    HUPS-UOW-NM      ';'
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "BRNCH-PROC-TYP = ", String.valueOf(ws.getDclhalUowPrcSeq().getHupsBrnPrcCd()), ";", "HUPS-UOW-NM      = ",
							ws.getDclhalUowPrcSeq().getHupsUowNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0300-FETCH-CURSOR-X
			return;
		}
		// COB_CODE: IF NOT UBOC-UOW-LOGGABLE-ERRORS
		//              PERFORM 0400-LINK-TO-PST-OBJ-MOD
		//           END-IF.
		if (!dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: PERFORM 0400-LINK-TO-PST-OBJ-MOD
			linkToPstObjMod();
		}
	}

	/**Original name: 0400-LINK-TO-PST-OBJ-MOD_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  A>  LINK TO OBJECT MODULE                                      *
	 *                                                                 *
	 * *****************************************************************
	 *          PROGRAM   (PST-OBJ-MODULE)</pre>*/
	private void linkToPstObjMod() {
		// COB_CODE:      EXEC CICS LINK
		//           *         PROGRAM   (PST-OBJ-MODULE)
		//                     PROGRAM   (WS-BUS-OBJ-MDU)
		//                     COMMAREA  (UBOC-COMM-INFO)
		//                     LENGTH    (LENGTH OF UBOC-COMM-INFO)
		//                     RESP      (WS-RESPONSE-CODE)
		//                     RESP2     (WS-RESPONSE-CODE2)
		//                END-EXEC.
		TpRunner.context("HALOUIEH", execContext).commarea(dfhcommarea.getCommInfo()).length(UbocCommInfoCaws002.Len.COMM_INFO)
				.link(ws.getWsWorkFields().getBusObjMdu());
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0400-LINK-TO-PST-OBJ-MOD-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkFields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK           TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			//            MOVE PST-OBJ-MODULE
			// COB_CODE: MOVE WS-BUS-OBJ-MDU
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsWorkFields().getBusObjMdu());
			// COB_CODE: MOVE '0400-LINK-TO-PST-OBJ-MOD'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0400-LINK-TO-PST-OBJ-MOD");
			// COB_CODE: MOVE 'CICS LINK TO BDO FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CICS LINK TO BDO FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0400-LINK-TO-PST-OBJ-MOD-X
			return;
		}
	}

	/**Original name: 0500-CLOSE-CURSOR_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  A>  CLOSE CURSOR                                              *
	 * ****************************************************************</pre>*/
	private void closeCursor() {
		// COB_CODE: EXEC SQL
		//               CLOSE CUR1
		//           END-EXEC.
		halUowPrcSeqVDao.closeCur1();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//              GO TO 0500-CLOSE-CURSOR-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING           TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_UOW_PRC_SEQ_V'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_PRC_SEQ_V");
			// COB_CODE: MOVE '0500-CLOSE-CURSOR'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0500-CLOSE-CURSOR");
			// COB_CODE: MOVE 'CLOSE CUR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CUR FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0500-CLOSE-CURSOR-X
			return;
		}
	}

	/**Original name: 0600-READ-BOBJ-MDU-XRF_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  RETRIEVE THE BUS OBJ MODULE FOR THE BUSINESS OBJECT NAME
	 * *****************************************************************</pre>*/
	private void readBobjMduXrf() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE WS-BUS-OBJ-NM TO HBMX-BUS-OBJ-NM.
		ws.getDclhalBoMduXrfV().setBusObjNm(ws.getWsWorkFields().getBusObjNm());
		// COB_CODE: EXEC SQL
		//               SELECT HBMX_BOBJ_MDU_NM
		//               INTO  :HBMX-BOBJ-MDU-NM
		//               FROM   HAL_BO_MDU_XRF_V
		//               WHERE BUS_OBJ_NM = :HBMX-BUS-OBJ-NM
		//           END-EXEC.
		ws.getDclhalBoMduXrfV()
				.setBobjMduNm(halBoMduXrfVDao.selectByHbmxBusObjNm(ws.getDclhalBoMduXrfV().getBusObjNm(), ws.getDclhalBoMduXrfV().getBobjMduNm()));
		// COB_CODE: EVALUATE TRUE
		//            WHEN ERD-SQL-GOOD
		//                 CONTINUE
		//            WHEN ERD-SQL-NOT-FOUND
		//                  GO TO 0600-READ-BOBJ-MDU-XRF-X
		//            WHEN OTHER
		//                 GO TO 0600-READ-BOBJ-MDU-XRF-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-LOG-ERROR                             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQD-DATA-NOT-FOUND OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspReqdDataNotFound();
			// COB_CODE: MOVE '0600-READ-BOBJ-MDU-XRF'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-READ-BOBJ-MDU-XRF");
			// COB_CODE: MOVE 'EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//             DELIMITED BY SIZE
			//            INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0600-READ-BOBJ-MDU-XRF-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF");
			// COB_CODE: MOVE '0600-READ-BOBJ-MDU-XRF'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-READ-BOBJ-MDU-XRF");
			// COB_CODE: MOVE 'SELECT FROM OBJ XREF TABLE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT FROM OBJ XREF TABLE FAILED");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0600-READ-BOBJ-MDU-XRF-X
			return;
		}
		//* CHECK MODULE NAME PRESENT IN OBJ XREF ROW RETURNED
		// COB_CODE: IF HBMX-BOBJ-MDU-NM = SPACES
		//               GO TO 0600-READ-BOBJ-MDU-XRF-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclhalBoMduXrfV().getBobjMduNm())) {
			// COB_CODE: SET WS-LOG-ERROR                              TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE    OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED   OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUIRED-FIELD-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequiredFieldBlank();
			// COB_CODE: MOVE '0600-READ-BOBJ-MDU-XRF'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-READ-BOBJ-MDU-XRF");
			// COB_CODE: MOVE 'MODULE NAME ON OBJ XREF ROW IS BLANK'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MODULE NAME ON OBJ XREF ROW IS BLANK");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//                  'HBMX-BOBJ-MDU-NM=' HBMX-BOBJ-MDU-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";", "HBMX-BOBJ-MDU-NM=", ws.getDclhalBoMduXrfV().getBobjMduNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0600-READ-BOBJ-MDU-XRF-X
			return;
		}
		//* USE MODULE NAME FROM OBJ XREF ROW
		// COB_CODE: MOVE HBMX-BOBJ-MDU-NM TO WS-BUS-OBJ-MDU.
		ws.getWsWorkFields().setBusObjMdu(ws.getDclhalBoMduXrfV().getBobjMduNm());
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsWorkFields().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsWorkFields().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsWorkFields().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsWorkFields().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsWorkFields().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsWorkFields().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALOUIEH", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsWorkFields().setResponseCode(execContext.getResp());
		ws.getWsWorkFields().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsWorkFields().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibrespDsply(ws.getWsWorkFields().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibresp2Dsply(ws.getWsWorkFields().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
