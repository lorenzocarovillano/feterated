/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.FillerEa08SubPgmError2;
import com.federatedinsurance.crs.ws.enums.FillerEa08SubPgmError7;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-08-SUB-PGM-ERROR<br>
 * Variable: EA-08-SUB-PGM-ERROR from program FNC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea08SubPgmError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-08-SUB-PGM-ERROR
	private String flr1 = "****";
	//Original name: FILLER-EA-08-SUB-PGM-ERROR-1
	private String flr2 = "FNC02090 -";
	//Original name: FILLER-EA-08-SUB-PGM-ERROR-2
	private FillerEa08SubPgmError2 flr3 = new FillerEa08SubPgmError2();
	//Original name: FILLER-EA-08-SUB-PGM-ERROR-3
	private String flr4 = "ERROR CALLING";
	//Original name: FILLER-EA-08-SUB-PGM-ERROR-4
	private String flr5 = "PGM:";
	//Original name: EA-08-PGM
	private String pgm = DefaultValues.stringVal(Len.PGM);
	//Original name: FILLER-EA-08-SUB-PGM-ERROR-5
	private String flr6 = "; RET CD:";
	//Original name: EA-08-RET-CD
	private String retCd = DefaultValues.stringVal(Len.RET_CD);
	//Original name: FILLER-EA-08-SUB-PGM-ERROR-6
	private String flr7 = "; MSG DES:";
	//Original name: FILLER-EA-08-SUB-PGM-ERROR-7
	private FillerEa08SubPgmError7 flr8 = new FillerEa08SubPgmError7();
	//Original name: FILLER-EA-08-SUB-PGM-ERROR-8
	private String flr9 = "; PGM INP:";
	//Original name: EA-08-PGM-INP
	private String pgmInp = DefaultValues.stringVal(Len.PGM_INP);

	//==== METHODS ====
	public String getEa08SubPgmErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa08SubPgmErrorBytes());
	}

	public byte[] getEa08SubPgmErrorBytes() {
		byte[] buffer = new byte[Len.EA08_SUB_PGM_ERROR];
		return getEa08SubPgmErrorBytes(buffer, 1);
	}

	public byte[] getEa08SubPgmErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3.getFlr3(), FillerEa08SubPgmError2.Len.FLR3);
		position += FillerEa08SubPgmError2.Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, pgm, Len.PGM);
		position += Len.PGM;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, retCd, Len.RET_CD);
		position += Len.RET_CD;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr8.getFlr8(), FillerEa08SubPgmError7.Len.FLR8);
		position += FillerEa08SubPgmError7.Len.FLR8;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, pgmInp, Len.PGM_INP);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setPgm(String pgm) {
		this.pgm = Functions.subString(pgm, Len.PGM);
	}

	public String getPgm() {
		return this.pgm;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setRetCd(int retCd) {
		this.retCd = NumericDisplay.asString(retCd, Len.RET_CD);
	}

	public int getRetCd() {
		return NumericDisplay.asInt(this.retCd);
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public String getPgmInp() {
		return this.pgmInp;
	}

	public FillerEa08SubPgmError7 getFlr8() {
		return flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PGM = 8;
		public static final int RET_CD = 8;
		public static final int PGM_INP = 30;
		public static final int FLR1 = 5;
		public static final int FLR2 = 11;
		public static final int FLR4 = 14;
		public static final int FLR6 = 10;
		public static final int EA08_SUB_PGM_ERROR = PGM + RET_CD + PGM_INP + FillerEa08SubPgmError7.Len.FLR8 + FillerEa08SubPgmError2.Len.FLR3
				+ 2 * FLR1 + 3 * FLR2 + FLR4 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
