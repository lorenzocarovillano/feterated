/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0R9090<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0r9090Data {

	//==== PROPERTIES ====
	//Original name: CF-BUS-OBJ-NM
	private String cfBusObjNm = "XZ_GET_NOT_CO_LIST";
	//Original name: FILLER-CF-INVALID-OPERATION
	private String flr1 = "RESPONSE MODULE";
	//Original name: FILLER-CF-INVALID-OPERATION-1
	private String flr2 = " - INVALID";
	//Original name: FILLER-CF-INVALID-OPERATION-2
	private String flr3 = "OPERATION";
	//Original name: FILLER-EA-01-INVALID-OPERATION-MSG
	private String flr4 = "XZ0R9090 -";
	//Original name: FILLER-EA-01-INVALID-OPERATION-MSG-1
	private String flr5 = "INVALID OPER:";
	//Original name: EA-01-INVALID-OPERATION-NAME
	private String ea01InvalidOperationName = DefaultValues.stringVal(Len.EA01_INVALID_OPERATION_NAME);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0r9090 workingStorageArea = new WorkingStorageAreaXz0r9090();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: IX-RS
	private int ixRs = 1;

	//==== METHODS ====
	public String getCfBusObjNm() {
		return this.cfBusObjNm;
	}

	public String getCfInvalidOperationFormatted() {
		return MarshalByteExt.bufferToStr(getCfInvalidOperationBytes());
	}

	/**Original name: CF-INVALID-OPERATION<br>*/
	public byte[] getCfInvalidOperationBytes() {
		byte[] buffer = new byte[Len.CF_INVALID_OPERATION];
		return getCfInvalidOperationBytes(buffer, 1);
	}

	public byte[] getCfInvalidOperationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getEa01InvalidOperationMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01InvalidOperationMsgBytes());
	}

	/**Original name: EA-01-INVALID-OPERATION-MSG<br>*/
	public byte[] getEa01InvalidOperationMsgBytes() {
		byte[] buffer = new byte[Len.EA01_INVALID_OPERATION_MSG];
		return getEa01InvalidOperationMsgBytes(buffer, 1);
	}

	public byte[] getEa01InvalidOperationMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea01InvalidOperationName, Len.EA01_INVALID_OPERATION_NAME);
		return buffer;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa01InvalidOperationName(String ea01InvalidOperationName) {
		this.ea01InvalidOperationName = Functions.subString(ea01InvalidOperationName, Len.EA01_INVALID_OPERATION_NAME);
	}

	public String getEa01InvalidOperationName() {
		return this.ea01InvalidOperationName;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public void setIxRs(int ixRs) {
		this.ixRs = ixRs;
	}

	public int getIxRs() {
		return this.ixRs;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public WorkingStorageAreaXz0r9090 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA01_INVALID_OPERATION_NAME = 32;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int FLR1 = 15;
		public static final int FLR2 = 11;
		public static final int FLR3 = 9;
		public static final int CF_INVALID_OPERATION = FLR1 + FLR2 + FLR3;
		public static final int FLR5 = 14;
		public static final int EA01_INVALID_OPERATION_MSG = EA01_INVALID_OPERATION_NAME + FLR2 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
