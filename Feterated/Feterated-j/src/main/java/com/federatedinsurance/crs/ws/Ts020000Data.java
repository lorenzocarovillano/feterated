/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.commons.data.to.IHalUowTransactV;
import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalMsgTransprt;
import com.federatedinsurance.crs.copy.DclhalUowPrcSeq;
import com.federatedinsurance.crs.copy.DclhalUowTransact;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallmdrv;
import com.federatedinsurance.crs.copy.Hallpst;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Halluidg;
import com.federatedinsurance.crs.copy.Hallukrp;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.MtcsMsgTransportInfo;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UsecInputOuputData;
import com.federatedinsurance.crs.copy.UtcsUowTransConfigInfo;
import com.federatedinsurance.crs.copy.UwrnCommon;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS020000<br>
 * Generated as a class for rule WS.<br>*/
public class Ts020000Data implements IHalUowTransactV {

	//==== PROPERTIES ====
	//Original name: CF-NLBE-ERROR-CODE
	private short cfNlbeErrorCode = ((short) 200);
	//Original name: CF-WARNING-ERROR-CODE
	private short cfWarningErrorCode = ((short) 100);
	//Original name: EA-01-FATAL-ERROR-MSG
	private Ea01FatalErrorMsg ea01FatalErrorMsg = new Ea01FatalErrorMsg();
	//Original name: SUBSCRIPTS
	private Subscripts subscripts = new Subscripts();
	//Original name: SWITCHES
	private Switches switches = new Switches();
	//Original name: TIME-ELAPSED-WORK
	private TimeElapsedWork timeElapsedWork = new TimeElapsedWork();
	//Original name: WORK-AREA
	private WorkArea workArea = new WorkArea();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: HALLUSW
	private Hallusw hallusw = new Hallusw();
	//Original name: HALLUHDR
	private Halluhdr halluhdr = new Halluhdr();
	//Original name: HALLUDAT
	private Halludat halludat = new Halludat();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: HALLUKRP
	private Hallukrp hallukrp = new Hallukrp();
	//Original name: USEC-INPUT-OUPUT-DATA
	private UsecInputOuputData usecInputOuputData = new UsecInputOuputData();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: MTCS-MSG-TRANSPORT-INFO
	private MtcsMsgTransportInfo mtcsMsgTransportInfo = new MtcsMsgTransportInfo();
	//Original name: UTCS-UOW-TRANS-CONFIG-INFO
	private UtcsUowTransConfigInfo utcsUowTransConfigInfo = new UtcsUowTransConfigInfo();
	//Original name: HALLPST
	private Hallpst hallpst = new Hallpst();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();
	//Original name: HALLMDRV
	private Hallmdrv hallmdrv = new Hallmdrv();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();
	//Original name: DCLHAL-MSG-TRANSPRT
	private DclhalMsgTransprt dclhalMsgTransprt = new DclhalMsgTransprt();
	//Original name: DCLHAL-UOW-TRANSACT
	private DclhalUowTransact dclhalUowTransact = new DclhalUowTransact();
	//Original name: DCLHAL-UOW-PRC-SEQ
	private DclhalUowPrcSeq dclhalUowPrcSeq = new DclhalUowPrcSeq();
	//Original name: WS-HUB-DATA
	private WsHubData wsHubData = new WsHubData();

	//==== METHODS ====
	public short getCfNlbeErrorCode() {
		return this.cfNlbeErrorCode;
	}

	public short getCfWarningErrorCode() {
		return this.cfWarningErrorCode;
	}

	public void setWsUrqmMsgBytes(byte[] buffer) {
		setWsUrqmMsgBytes(buffer, 1);
	}

	public void setWsUrqmMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		urqmCommon.setUrqmCommonBytes(buffer, position);
	}

	public void setWsUswMsgBytes(byte[] buffer) {
		setWsUswMsgBytes(buffer, 1);
	}

	public void setWsUswMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		hallusw.setCommonBytes(buffer, position);
	}

	public void setWsUhdrMsgBytes(byte[] buffer) {
		setWsUhdrMsgBytes(buffer, 1);
	}

	public void setWsUhdrMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		halluhdr.setCommonBytes(buffer, position);
	}

	public void setWsUdatMsgBytes(byte[] buffer) {
		setWsUdatMsgBytes(buffer, 1);
	}

	public void setWsUdatMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		halludat.setCommonBytes(buffer, position);
	}

	public void setWsUwrnMsgBytes(byte[] buffer) {
		setWsUwrnMsgBytes(buffer, 1);
	}

	public void setWsUwrnMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.setUwrnCommonBytes(buffer, position);
	}

	public void setHaloukrpLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.HALOUKRP_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.HALOUKRP_LINKAGE);
		setHaloukrpLinkageBytes(buffer, 1);
	}

	public String getHaloukrpLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHaloukrpLinkageBytes());
	}

	/**Original name: HALOUKRP-LINKAGE<br>
	 * <pre>* KEY REPLACEMENT MODULE COMMAREA COPYBOOK</pre>*/
	public byte[] getHaloukrpLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUKRP_LINKAGE];
		return getHaloukrpLinkageBytes(buffer, 1);
	}

	public void setHaloukrpLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallukrp.setInputFieldsBytes(buffer, position);
		position += Hallukrp.Len.INPUT_FIELDS;
		hallukrp.setInputOutputFieldsBytes(buffer, position);
	}

	public byte[] getHaloukrpLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallukrp.getInputFieldsBytes(buffer, position);
		position += Hallukrp.Len.INPUT_FIELDS;
		hallukrp.getInputOutputFieldsBytes(buffer, position);
		return buffer;
	}

	public void setWsUsecInfoFormatted(String data) {
		byte[] buffer = new byte[Len.WS_USEC_INFO];
		MarshalByte.writeString(buffer, 1, data, Len.WS_USEC_INFO);
		setWsUsecInfoBytes(buffer, 1);
	}

	public String getWsUsecInfoFormatted() {
		return usecInputOuputData.getUsecInputOuputDataFormatted();
	}

	public void setWsUsecInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		usecInputOuputData.setUsecInputOuputDataBytes(buffer, position);
	}

	public void initWsUsecInfoSpaces() {
		usecInputOuputData.initUsecInputOuputDataSpaces();
	}

	public void setWsNlbeMsgBytes(byte[] buffer) {
		setWsNlbeMsgBytes(buffer, 1);
	}

	public void setWsNlbeMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.setNlbeCommonBytes(buffer, position);
	}

	public void setWsMsgTransportInfoBytes(byte[] buffer) {
		setWsMsgTransportInfoBytes(buffer, 1);
	}

	public void setWsMsgTransportInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		mtcsMsgTransportInfo.setMtcsMsgTransportInfoBytes(buffer, position);
	}

	public void initWsMsgTransportInfoSpaces() {
		mtcsMsgTransportInfo.initMtcsMsgTransportInfoSpaces();
	}

	public void initWsUowTransactionInfoSpaces() {
		utcsUowTransConfigInfo.initUtcsUowTransConfigInfoSpaces();
	}

	public void setWsUowProcessSeqInfoBytes(byte[] buffer) {
		setWsUowProcessSeqInfoBytes(buffer, 1);
	}

	public void setWsUowProcessSeqInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		hallpst.setUowPrcSeqBytes(buffer, position);
	}

	public void setWsHalouidgLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.WS_HALOUIDG_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.WS_HALOUIDG_LINKAGE);
		setWsHalouidgLinkageBytes(buffer, 1);
	}

	public String getWsHalouidgLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getWsHalouidgLinkageBytes());
	}

	/**Original name: WS-HALOUIDG-LINKAGE<br>
	 * <pre>* RANDOM ID GENERATOR LINKAGE</pre>*/
	public byte[] getWsHalouidgLinkageBytes() {
		byte[] buffer = new byte[Len.WS_HALOUIDG_LINKAGE];
		return getWsHalouidgLinkageBytes(buffer, 1);
	}

	public void setWsHalouidgLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.setUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.setUidgCaOutputBytes(buffer, position);
	}

	public byte[] getWsHalouidgLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.getUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.getUidgCaOutputBytes(buffer, position);
		return buffer;
	}

	@Override
	public char getAuditInd() {
		return dclhalUowTransact.getAuditInd();
	}

	@Override
	public void setAuditInd(char auditInd) {
		this.dclhalUowTransact.setAuditInd(auditInd);
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalMsgTransprt getDclhalMsgTransprt() {
		return dclhalMsgTransprt;
	}

	public DclhalUowPrcSeq getDclhalUowPrcSeq() {
		return dclhalUowPrcSeq;
	}

	public DclhalUowTransact getDclhalUowTransact() {
		return dclhalUowTransact;
	}

	@Override
	public char getDtaPvcInd() {
		return dclhalUowTransact.getDtaPvcInd();
	}

	@Override
	public void setDtaPvcInd(char dtaPvcInd) {
		this.dclhalUowTransact.setDtaPvcInd(dtaPvcInd);
	}

	public Ea01FatalErrorMsg getEa01FatalErrorMsg() {
		return ea01FatalErrorMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallmdrv getHallmdrv() {
		return hallmdrv;
	}

	public Halludat getHalludat() {
		return halludat;
	}

	public Halluhdr getHalluhdr() {
		return halluhdr;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public Hallukrp getHallukrp() {
		return hallukrp;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	@Override
	public String getHutcUowNm() {
		return dclhalUowTransact.getUowNm();
	}

	@Override
	public void setHutcUowNm(String hutcUowNm) {
		this.dclhalUowTransact.setUowNm(hutcUowNm);
	}

	@Override
	public char getLokSgyCd() {
		return dclhalUowTransact.getLokSgyCd();
	}

	@Override
	public void setLokSgyCd(char lokSgyCd) {
		this.dclhalUowTransact.setLokSgyCd(lokSgyCd);
	}

	@Override
	public int getLokTmoItv() {
		return dclhalUowTransact.getLokTmoItv();
	}

	@Override
	public void setLokTmoItv(int lokTmoItv) {
		this.dclhalUowTransact.setLokTmoItv(lokTmoItv);
	}

	@Override
	public String getMcmMduNm() {
		return dclhalUowTransact.getMcmMduNm();
	}

	@Override
	public void setMcmMduNm(String mcmMduNm) {
		this.dclhalUowTransact.setMcmMduNm(mcmMduNm);
	}

	public MtcsMsgTransportInfo getMtcsMsgTransportInfo() {
		return mtcsMsgTransportInfo;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public char getSecInd() {
		return dclhalUowTransact.getSecInd();
	}

	@Override
	public void setSecInd(char secInd) {
		this.dclhalUowTransact.setSecInd(secInd);
	}

	@Override
	public String getSecMduNm() {
		return dclhalUowTransact.getSecMduNm();
	}

	@Override
	public void setSecMduNm(String secMduNm) {
		this.dclhalUowTransact.setSecMduNm(secMduNm);
	}

	@Override
	public String getSecMduNmObj() {
		if (dclhalUowTransact.getSecMduNmNi() >= 0) {
			return getSecMduNm();
		} else {
			return null;
		}
	}

	@Override
	public void setSecMduNmObj(String secMduNmObj) {
		if (secMduNmObj != null) {
			setSecMduNm(secMduNmObj);
			dclhalUowTransact.setSecMduNmNi(((short) 0));
		} else {
			dclhalUowTransact.setSecMduNmNi(((short) -1));
		}
	}

	public Subscripts getSubscripts() {
		return subscripts;
	}

	public Switches getSwitches() {
		return switches;
	}

	public TimeElapsedWork getTimeElapsedWork() {
		return timeElapsedWork;
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public UsecInputOuputData getUsecInputOuputData() {
		return usecInputOuputData;
	}

	public UtcsUowTransConfigInfo getUtcsUowTransConfigInfo() {
		return utcsUowTransConfigInfo;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkArea getWorkArea() {
		return workArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHubData getWsHubData() {
		return wsHubData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_HALOUIDG_LINKAGE = Halluidg.Len.UIDG_CA_INCOMING + Halluidg.Len.UIDG_CA_OUTPUT;
		public static final int WS_USEC_INFO = UsecInputOuputData.Len.USEC_INPUT_OUPUT_DATA;
		public static final int HALOUKRP_LINKAGE = Hallukrp.Len.INPUT_FIELDS + Hallukrp.Len.INPUT_OUTPUT_FIELDS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
