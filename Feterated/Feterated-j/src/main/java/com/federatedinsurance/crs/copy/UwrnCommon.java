/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: UWRN-COMMON<br>
 * Variable: UWRN-COMMON from copybook HALLUWRN<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class UwrnCommon {

	//==== PROPERTIES ====
	//Original name: UWRN-ID
	private String id = DefaultValues.stringVal(Len.ID);
	//Original name: UWRN-REC-SEQ
	private String recSeq = DefaultValues.stringVal(Len.REC_SEQ);
	//Original name: UWRN-FAILURE-TYPE
	private String failureType = "WARNINGS";
	//Original name: UWRN-FAILED-MODULE
	private String failedModule = DefaultValues.stringVal(Len.FAILED_MODULE);
	//Original name: UWRN-FAILED-TABLE-OR-FILE
	private String failedTableOrFile = DefaultValues.stringVal(Len.FAILED_TABLE_OR_FILE);
	//Original name: UWRN-FAILED-COLUMN-OR-FIELD
	private String failedColumnOrField = DefaultValues.stringVal(Len.FAILED_COLUMN_OR_FIELD);
	//Original name: UWRN-WARNING-CODE
	private String warningCode = DefaultValues.stringVal(Len.WARNING_CODE);
	/**Original name: UWRN-WARNING-TEXT<br>
	 * <pre>           10 UWRN-WARNING-TEXT              PIC X(100).</pre>*/
	private String warningText = DefaultValues.stringVal(Len.WARNING_TEXT);

	//==== METHODS ====
	public void setUwrnCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		setKeyBytes(buffer, position);
		position += Len.KEY;
		setUowWarningBufferBytes(buffer, position);
	}

	public byte[] getUwrnCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		getKeyBytes(buffer, position);
		position += Len.KEY;
		getUowWarningBufferBytes(buffer, position);
		return buffer;
	}

	public String getKeyFormatted() {
		return MarshalByteExt.bufferToStr(getKeyBytes());
	}

	public void setKeyBytes(byte[] buffer) {
		setKeyBytes(buffer, 1);
	}

	/**Original name: UWRN-KEY<br>*/
	public byte[] getKeyBytes() {
		byte[] buffer = new byte[Len.KEY];
		return getKeyBytes(buffer, 1);
	}

	public void setKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		id = MarshalByte.readString(buffer, position, Len.ID);
		position += Len.ID;
		recSeq = MarshalByte.readFixedString(buffer, position, Len.REC_SEQ);
	}

	public byte[] getKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, id, Len.ID);
		position += Len.ID;
		MarshalByte.writeString(buffer, position, recSeq, Len.REC_SEQ);
		return buffer;
	}

	public void setId(String id) {
		this.id = Functions.subString(id, Len.ID);
	}

	public String getId() {
		return this.id;
	}

	public String getIdFormatted() {
		return Functions.padBlanks(getId(), Len.ID);
	}

	public void setRecSeq(short recSeq) {
		this.recSeq = NumericDisplay.asString(recSeq, Len.REC_SEQ);
	}

	public void setRecSeqFormatted(String recSeq) {
		this.recSeq = Trunc.toUnsignedNumeric(recSeq, Len.REC_SEQ);
	}

	public short getRecSeq() {
		return NumericDisplay.asShort(this.recSeq);
	}

	public String getRecSeqFormatted() {
		return this.recSeq;
	}

	public String getRecSeqAsString() {
		return getRecSeqFormatted();
	}

	public void setUowWarningBufferBytes(byte[] buffer, int offset) {
		int position = offset;
		failureType = MarshalByte.readString(buffer, position, Len.FAILURE_TYPE);
		position += Len.FAILURE_TYPE;
		failedModule = MarshalByte.readString(buffer, position, Len.FAILED_MODULE);
		position += Len.FAILED_MODULE;
		failedTableOrFile = MarshalByte.readString(buffer, position, Len.FAILED_TABLE_OR_FILE);
		position += Len.FAILED_TABLE_OR_FILE;
		failedColumnOrField = MarshalByte.readString(buffer, position, Len.FAILED_COLUMN_OR_FIELD);
		position += Len.FAILED_COLUMN_OR_FIELD;
		warningCode = MarshalByte.readString(buffer, position, Len.WARNING_CODE);
		position += Len.WARNING_CODE;
		warningText = MarshalByte.readString(buffer, position, Len.WARNING_TEXT);
	}

	public byte[] getUowWarningBufferBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, failureType, Len.FAILURE_TYPE);
		position += Len.FAILURE_TYPE;
		MarshalByte.writeString(buffer, position, failedModule, Len.FAILED_MODULE);
		position += Len.FAILED_MODULE;
		MarshalByte.writeString(buffer, position, failedTableOrFile, Len.FAILED_TABLE_OR_FILE);
		position += Len.FAILED_TABLE_OR_FILE;
		MarshalByte.writeString(buffer, position, failedColumnOrField, Len.FAILED_COLUMN_OR_FIELD);
		position += Len.FAILED_COLUMN_OR_FIELD;
		MarshalByte.writeString(buffer, position, warningCode, Len.WARNING_CODE);
		position += Len.WARNING_CODE;
		MarshalByte.writeString(buffer, position, warningText, Len.WARNING_TEXT);
		return buffer;
	}

	public void setFailureType(String failureType) {
		this.failureType = Functions.subString(failureType, Len.FAILURE_TYPE);
	}

	public String getFailureType() {
		return this.failureType;
	}

	public void setFailedModule(String failedModule) {
		this.failedModule = Functions.subString(failedModule, Len.FAILED_MODULE);
	}

	public String getFailedModule() {
		return this.failedModule;
	}

	public void setFailedTableOrFile(String failedTableOrFile) {
		this.failedTableOrFile = Functions.subString(failedTableOrFile, Len.FAILED_TABLE_OR_FILE);
	}

	public String getFailedTableOrFile() {
		return this.failedTableOrFile;
	}

	public void setFailedColumnOrField(String failedColumnOrField) {
		this.failedColumnOrField = Functions.subString(failedColumnOrField, Len.FAILED_COLUMN_OR_FIELD);
	}

	public String getFailedColumnOrField() {
		return this.failedColumnOrField;
	}

	public void setWarningCode(String warningCode) {
		this.warningCode = Functions.subString(warningCode, Len.WARNING_CODE);
	}

	public String getWarningCode() {
		return this.warningCode;
	}

	public String getWarningCodeFormatted() {
		return Functions.padBlanks(getWarningCode(), Len.WARNING_CODE);
	}

	public void setWarningText(String warningText) {
		this.warningText = Functions.subString(warningText, Len.WARNING_TEXT);
	}

	public String getWarningText() {
		return this.warningText;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ID = 32;
		public static final int REC_SEQ = 3;
		public static final int FAILED_MODULE = 32;
		public static final int FAILED_TABLE_OR_FILE = 32;
		public static final int FAILED_COLUMN_OR_FIELD = 18;
		public static final int WARNING_CODE = 10;
		public static final int WARNING_TEXT = 500;
		public static final int FAILURE_TYPE = 32;
		public static final int KEY = ID + REC_SEQ;
		public static final int UOW_WARNING_BUFFER = FAILURE_TYPE + FAILED_MODULE + FAILED_TABLE_OR_FILE + FAILED_COLUMN_OR_FIELD + WARNING_CODE
				+ WARNING_TEXT;
		public static final int UWRN_COMMON = KEY + UOW_WARNING_BUFFER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
