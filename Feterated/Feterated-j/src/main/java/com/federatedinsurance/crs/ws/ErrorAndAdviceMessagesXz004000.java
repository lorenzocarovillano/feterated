/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXz004000 {

	//==== PROPERTIES ====
	//Original name: EA-01-PROGRAM-MSG
	private Ea01ProgramMsgXz004000 ea01ProgramMsg = new Ea01ProgramMsgXz004000();
	//Original name: EA-03-SUCCESSFUL-END
	private Ea03SuccessfulEndXz004000 ea03SuccessfulEnd = new Ea03SuccessfulEndXz004000();
	//Original name: EA-04-GENERIC-MSG
	private Ea04GenericMsg ea04GenericMsg = new Ea04GenericMsg();
	//Original name: EA-05-NO-NOTICES-TO-REPORT
	private Ea05NoNoticesToReport ea05NoNoticesToReport = new Ea05NoNoticesToReport();
	//Original name: EA-06-NOT-DT-CD-NOT-FOUND
	private Ea06NotDtCdNotFound ea06NotDtCdNotFound = new Ea06NotDtCdNotFound();
	//Original name: EA-07-IMP-NOT-NOT-FOUND
	private Ea07ImpNotNotFound ea07ImpNotNotFound = new Ea07ImpNotNotFound();
	//Original name: EA-08-ACT-NOT-FOUND-MSG
	private Ea08ActNotFoundMsg ea08ActNotFoundMsg = new Ea08ActNotFoundMsg();
	//Original name: EA-96-DATE-RANGE-MSG
	private Ea96DateRangeMsg ea96DateRangeMsg = new Ea96DateRangeMsg();

	//==== METHODS ====
	public Ea01ProgramMsgXz004000 getEa01ProgramMsg() {
		return ea01ProgramMsg;
	}

	public Ea03SuccessfulEndXz004000 getEa03SuccessfulEnd() {
		return ea03SuccessfulEnd;
	}

	public Ea04GenericMsg getEa04GenericMsg() {
		return ea04GenericMsg;
	}

	public Ea05NoNoticesToReport getEa05NoNoticesToReport() {
		return ea05NoNoticesToReport;
	}

	public Ea06NotDtCdNotFound getEa06NotDtCdNotFound() {
		return ea06NotDtCdNotFound;
	}

	public Ea07ImpNotNotFound getEa07ImpNotNotFound() {
		return ea07ImpNotNotFound;
	}

	public Ea08ActNotFoundMsg getEa08ActNotFoundMsg() {
		return ea08ActNotFoundMsg;
	}

	public Ea96DateRangeMsg getEa96DateRangeMsg() {
		return ea96DateRangeMsg;
	}
}
