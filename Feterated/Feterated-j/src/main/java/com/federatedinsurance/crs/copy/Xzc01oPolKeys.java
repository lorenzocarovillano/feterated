/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC01O-POL-KEYS<br>
 * Variable: XZC01O-POL-KEYS from copybook XZC010C1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzc01oPolKeys {

	//==== PROPERTIES ====
	//Original name: XZC01O-POL-NBR
	private String nbr = DefaultValues.stringVal(Len.NBR);
	//Original name: XZC01O-POL-EFF-DT
	private String effDt = DefaultValues.stringVal(Len.EFF_DT);
	//Original name: XZC01O-POL-EXP-DT
	private String expDt = DefaultValues.stringVal(Len.EXP_DT);

	//==== METHODS ====
	public void setPolKeysBytes(byte[] buffer, int offset) {
		int position = offset;
		nbr = MarshalByte.readString(buffer, position, Len.NBR);
		position += Len.NBR;
		effDt = MarshalByte.readString(buffer, position, Len.EFF_DT);
		position += Len.EFF_DT;
		expDt = MarshalByte.readString(buffer, position, Len.EXP_DT);
	}

	public byte[] getPolKeysBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, nbr, Len.NBR);
		position += Len.NBR;
		MarshalByte.writeString(buffer, position, effDt, Len.EFF_DT);
		position += Len.EFF_DT;
		MarshalByte.writeString(buffer, position, expDt, Len.EXP_DT);
		return buffer;
	}

	public void setNbr(String nbr) {
		this.nbr = Functions.subString(nbr, Len.NBR);
	}

	public String getNbr() {
		return this.nbr;
	}

	public void setEffDt(String effDt) {
		this.effDt = Functions.subString(effDt, Len.EFF_DT);
	}

	public String getEffDt() {
		return this.effDt;
	}

	public void setExpDt(String expDt) {
		this.expDt = Functions.subString(expDt, Len.EXP_DT);
	}

	public String getExpDt() {
		return this.expDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NBR = 9;
		public static final int EFF_DT = 10;
		public static final int EXP_DT = 10;
		public static final int POL_KEYS = NBR + EFF_DT + EXP_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
