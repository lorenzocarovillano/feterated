/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-CM-CICS-APPL-ID<br>
 * Variable: L-CM-CICS-APPL-ID from copybook TS54801<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LCmCicsApplId {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.CICS_APPL_ID);
	public static final String P2 = "CICS2";
	public static final String P3 = "CICS3";
	public static final String P4 = "CICS4";
	public static final String P5 = "CICS5";
	public static final String P6 = "CICS6";
	public static final String P7 = "CICS7";
	public static final String P8 = "CICS8";
	public static final String P9 = "CICS9";
	public static final String PM = "CICSM";
	public static final String D1 = "D1CICS";
	public static final String DE = "DECICS";
	public static final String B1 = "B1CICS";
	public static final String BA = "BACICS";
	public static final String EDUC1 = "E1CICS";
	public static final String EDUC3 = "E3CICS";

	//==== METHODS ====
	public void setCicsApplId(String cicsApplId) {
		this.value = Functions.subString(cicsApplId, Len.CICS_APPL_ID);
	}

	public String getCicsApplId() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICS_APPL_ID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
