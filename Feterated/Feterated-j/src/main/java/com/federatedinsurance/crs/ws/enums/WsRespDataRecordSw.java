/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-RESP-DATA-RECORD-SW<br>
 * Variable: WS-RESP-DATA-RECORD-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsRespDataRecordSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FIRST_RESP_DATA_REC = 'F';
	public static final char NEXT_RESP_DATA_REC = 'N';

	//==== METHODS ====
	public void setRespDataRecordSw(char respDataRecordSw) {
		this.value = respDataRecordSw;
	}

	public char getRespDataRecordSw() {
		return this.value;
	}

	public boolean isWsFirstRespDataRec() {
		return value == FIRST_RESP_DATA_REC;
	}

	public void setWsFirstRespDataRec() {
		value = FIRST_RESP_DATA_REC;
	}

	public void setWsNextRespDataRec() {
		value = NEXT_RESP_DATA_REC;
	}
}
