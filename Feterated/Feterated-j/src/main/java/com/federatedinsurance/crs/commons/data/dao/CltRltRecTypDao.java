/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.ICltRltRecTyp;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [CLT_RLT_REC_TYP]
 * 
 */
public class CltRltRecTypDao extends BaseSqlDao<ICltRltRecTyp> {

	public CltRltRecTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ICltRltRecTyp> getToClass() {
		return ICltRltRecTyp.class;
	}

	public String selectByCltRltTypCd(String cltRltTypCd, String dft) {
		return buildQuery("selectByCltRltTypCd").bind("cltRltTypCd", cltRltTypCd).scalarResultString(dft);
	}
}
