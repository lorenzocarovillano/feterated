/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLHAL-UOW-TRANSACT<br>
 * Variable: DCLHAL-UOW-TRANSACT from copybook HALLGUTC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalUowTransact {

	//==== PROPERTIES ====
	//Original name: HUTC-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HUTC-MCM-MDU-NM
	private String mcmMduNm = DefaultValues.stringVal(Len.MCM_MDU_NM);
	//Original name: HUTC-LOK-TMO-ITV
	private int lokTmoItv = DefaultValues.BIN_INT_VAL;
	//Original name: HUTC-LOK-SGY-CD
	private char lokSgyCd = DefaultValues.CHAR_VAL;
	//Original name: HUTC-SEC-IND
	private char secInd = DefaultValues.CHAR_VAL;
	//Original name: HUTC-SEC-MDU-NM-NI
	private short secMduNmNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: HUTC-SEC-MDU-NM
	private String secMduNm = DefaultValues.stringVal(Len.SEC_MDU_NM);
	//Original name: HUTC-DTA-PVC-IND
	private char dtaPvcInd = DefaultValues.CHAR_VAL;
	//Original name: HUTC-AUDIT-IND
	private char auditInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void initDclhalUowTransactSpaces() {
		uowNm = "";
		mcmMduNm = "";
		lokTmoItv = Types.INVALID_BINARY_INT_VAL;
		lokSgyCd = Types.SPACE_CHAR;
		secInd = Types.SPACE_CHAR;
		secMduNmNi = Types.INVALID_BINARY_SHORT_VAL;
		secMduNm = "";
		dtaPvcInd = Types.SPACE_CHAR;
		auditInd = Types.SPACE_CHAR;
	}

	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public String getUowNmFormatted() {
		return Functions.padBlanks(getUowNm(), Len.UOW_NM);
	}

	public void setMcmMduNm(String mcmMduNm) {
		this.mcmMduNm = Functions.subString(mcmMduNm, Len.MCM_MDU_NM);
	}

	public String getMcmMduNm() {
		return this.mcmMduNm;
	}

	public void setLokTmoItv(int lokTmoItv) {
		this.lokTmoItv = lokTmoItv;
	}

	public int getLokTmoItv() {
		return this.lokTmoItv;
	}

	public void setLokSgyCd(char lokSgyCd) {
		this.lokSgyCd = lokSgyCd;
	}

	public char getLokSgyCd() {
		return this.lokSgyCd;
	}

	public void setSecInd(char secInd) {
		this.secInd = secInd;
	}

	public char getSecInd() {
		return this.secInd;
	}

	public void setSecMduNmNi(short secMduNmNi) {
		this.secMduNmNi = secMduNmNi;
	}

	public short getSecMduNmNi() {
		return this.secMduNmNi;
	}

	public void setSecMduNm(String secMduNm) {
		this.secMduNm = Functions.subString(secMduNm, Len.SEC_MDU_NM);
	}

	public String getSecMduNm() {
		return this.secMduNm;
	}

	public void setDtaPvcInd(char dtaPvcInd) {
		this.dtaPvcInd = dtaPvcInd;
	}

	public char getDtaPvcInd() {
		return this.dtaPvcInd;
	}

	public void setAuditInd(char auditInd) {
		this.auditInd = auditInd;
	}

	public char getAuditInd() {
		return this.auditInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NM = 32;
		public static final int MCM_MDU_NM = 32;
		public static final int SEC_MDU_NM = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
