/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-TABLE-FORMATTER-MODULES<br>
 * Variable: CF-TABLE-FORMATTER-MODULES from program MU0R0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfTableFormatterModulesMu0r0004 {

	//==== PROPERTIES ====
	//Original name: CF-TF-CLIENT-TAB-FMT
	private String clientTabFmt = "CAWI002";
	//Original name: CF-TF-CLT-ADR-COMP-FMT
	private String cltAdrCompFmt = "CAWICAC";
	//Original name: CF-TF-CLT-PHONE-FMT
	private String cltPhoneFmt = "CAWI004";
	//Original name: CF-TF-CLT-EMAIL-FMT
	private String cltEmailFmt = "CAWI0EM";
	//Original name: CF-TF-CLT-OBJ-REL-FMT
	private String cltObjRelFmt = "CAWI008";
	//Original name: CF-TF-CLT-REF-REL-FMT
	private String cltRefRelFmt = "CAWI011";
	//Original name: CF-TF-CLIENT-TAX-FMT
	private String clientTaxFmt = "CAWI027";
	//Original name: CF-TF-CLT-MUT-TER-STC-FMT
	private String cltMutTerStcFmt = "CAWI045";
	//Original name: CF-TF-CLT-AGC-TER-STC-FMT
	private String cltAgcTerStcFmt = "CAWI046";
	//Original name: CF-TF-FED-BUS-TYP-FMT
	private String fedBusTypFmt = "CAWI048";
	//Original name: CF-TF-FED-SEG-INFO-FMT
	private String fedSegInfoFmt = "CAWI049";
	//Original name: CF-TF-FED-ACT-INFO-FMT
	private String fedActInfoFmt = "CAWI040";
	//Original name: CF-TF-BUS-CLT-FMT
	private String busCltFmt = "CAWI001";

	//==== METHODS ====
	public String getClientTabFmt() {
		return this.clientTabFmt;
	}

	public String getCltAdrCompFmt() {
		return this.cltAdrCompFmt;
	}

	public String getCltPhoneFmt() {
		return this.cltPhoneFmt;
	}

	public String getCltEmailFmt() {
		return this.cltEmailFmt;
	}

	public String getCltObjRelFmt() {
		return this.cltObjRelFmt;
	}

	public String getCltRefRelFmt() {
		return this.cltRefRelFmt;
	}

	public String getClientTaxFmt() {
		return this.clientTaxFmt;
	}

	public String getCltMutTerStcFmt() {
		return this.cltMutTerStcFmt;
	}

	public String getCltAgcTerStcFmt() {
		return this.cltAgcTerStcFmt;
	}

	public String getFedBusTypFmt() {
		return this.fedBusTypFmt;
	}

	public String getFedSegInfoFmt() {
		return this.fedSegInfoFmt;
	}

	public String getFedActInfoFmt() {
		return this.fedActInfoFmt;
	}

	public String getBusCltFmt() {
		return this.busCltFmt;
	}
}
