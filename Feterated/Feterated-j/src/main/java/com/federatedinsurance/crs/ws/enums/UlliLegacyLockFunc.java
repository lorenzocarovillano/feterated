/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ULLI-LEGACY-LOCK-FUNC<br>
 * Variable: ULLI-LEGACY-LOCK-FUNC from copybook HALLULLI<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UlliLegacyLockFunc {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CREATE_UPDATE_LOCK = 'C';
	public static final char VERIFY_UPDATE_LOCK = 'V';
	public static final char DELETE_UPDATE_LOCK = 'D';
	public static final char UPGRADE_LOCK = 'U';

	//==== METHODS ====
	public void setLegacyLockFunc(char legacyLockFunc) {
		this.value = legacyLockFunc;
	}

	public char getLegacyLockFunc() {
		return this.value;
	}

	public void setCreateUpdateLock() {
		value = CREATE_UPDATE_LOCK;
	}

	public boolean isVerifyUpdateLock() {
		return value == VERIFY_UPDATE_LOCK;
	}

	public void setVerifyUpdateLock() {
		value = VERIFY_UPDATE_LOCK;
	}

	public void setUpgradeLock() {
		value = UPGRADE_LOCK;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LEGACY_LOCK_FUNC = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
