/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ0P90H0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz0p90h0 {

	//==== PROPERTIES ====
	//Original name: SW-ACT-NOT-POL-ALC-FLAG
	private boolean actNotPolAlcFlag = false;
	//Original name: SW-ACT-NOT-REC-ALC-FLAG
	private boolean actNotRecAlcFlag = false;
	//Original name: SW-ADD-INSURED-FLAG
	private boolean addInsuredFlag = false;
	//Original name: SW-CHECK-TTY-POLICY-FLAG
	private boolean checkTtyPolicyFlag = false;
	//Original name: SW-FIRST-TIME-THRU-FLAG
	private boolean firstTimeThruFlag = true;
	//Original name: SW-GET-ACT-CLT-LIST-ALC-FLAG
	private boolean getActCltListAlcFlag = false;
	//Original name: SW-PROCESS-SORT-1-FLAG
	private boolean processSort1Flag = true;
	//Original name: SW-TTY-POL-FOUND-FLAG
	private boolean ttyPolFoundFlag = true;

	//==== METHODS ====
	public void setActNotPolAlcFlag(boolean actNotPolAlcFlag) {
		this.actNotPolAlcFlag = actNotPolAlcFlag;
	}

	public boolean isActNotPolAlcFlag() {
		return this.actNotPolAlcFlag;
	}

	public void setActNotRecAlcFlag(boolean actNotRecAlcFlag) {
		this.actNotRecAlcFlag = actNotRecAlcFlag;
	}

	public boolean isActNotRecAlcFlag() {
		return this.actNotRecAlcFlag;
	}

	public void setAddInsuredFlag(boolean addInsuredFlag) {
		this.addInsuredFlag = addInsuredFlag;
	}

	public boolean isAddInsuredFlag() {
		return this.addInsuredFlag;
	}

	public void setCheckTtyPolicyFlag(boolean checkTtyPolicyFlag) {
		this.checkTtyPolicyFlag = checkTtyPolicyFlag;
	}

	public boolean isCheckTtyPolicyFlag() {
		return this.checkTtyPolicyFlag;
	}

	public void setFirstTimeThruFlag(boolean firstTimeThruFlag) {
		this.firstTimeThruFlag = firstTimeThruFlag;
	}

	public boolean isFirstTimeThruFlag() {
		return this.firstTimeThruFlag;
	}

	public void setGetActCltListAlcFlag(boolean getActCltListAlcFlag) {
		this.getActCltListAlcFlag = getActCltListAlcFlag;
	}

	public boolean isGetActCltListAlcFlag() {
		return this.getActCltListAlcFlag;
	}

	public void setProcessSort1Flag(boolean processSort1Flag) {
		this.processSort1Flag = processSort1Flag;
	}

	public boolean isProcessSort1Flag() {
		return this.processSort1Flag;
	}

	public void setTtyPolFoundFlag(boolean ttyPolFoundFlag) {
		this.ttyPolFoundFlag = ttyPolFoundFlag;
	}

	public boolean isTtyPolFoundFlag() {
		return this.ttyPolFoundFlag;
	}
}
