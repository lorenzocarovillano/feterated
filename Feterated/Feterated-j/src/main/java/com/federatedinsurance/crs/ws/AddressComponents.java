/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADDRESS-COMPONENTS<br>
 * Variable: ADDRESS-COMPONENTS from program TS529099<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class AddressComponents {

	//==== PROPERTIES ====
	//Original name: AC-DISPLAY-NAME
	private String displayName = DefaultValues.stringVal(Len.DISPLAY_NAME);
	//Original name: AC-NAME-LINE-1
	private String nameLine1 = DefaultValues.stringVal(Len.NAME_LINE1);
	//Original name: AC-NAME-LINE-2
	private String nameLine2 = DefaultValues.stringVal(Len.NAME_LINE2);
	//Original name: AC-ADR-LINE-1
	private String adrLine1 = DefaultValues.stringVal(Len.ADR_LINE1);
	//Original name: AC-ADR-LINE-2
	private String adrLine2 = DefaultValues.stringVal(Len.ADR_LINE2);
	//Original name: AC-CITY
	private String city = DefaultValues.stringVal(Len.CITY);
	//Original name: AC-STATE-ABB
	private String stateAbb = DefaultValues.stringVal(Len.STATE_ABB);
	//Original name: AC-POSTAL-CODE
	private String postalCode = DefaultValues.stringVal(Len.POSTAL_CODE);

	//==== METHODS ====
	public void setAddressComponentsFormatted(String data) {
		byte[] buffer = new byte[Len.ADDRESS_COMPONENTS];
		MarshalByte.writeString(buffer, 1, data, Len.ADDRESS_COMPONENTS);
		setAddressComponentsBytes(buffer, 1);
	}

	public String getAddressComponentsFormatted() {
		return MarshalByteExt.bufferToStr(getAddressComponentsBytes());
	}

	public byte[] getAddressComponentsBytes() {
		byte[] buffer = new byte[Len.ADDRESS_COMPONENTS];
		return getAddressComponentsBytes(buffer, 1);
	}

	public void setAddressComponentsBytes(byte[] buffer, int offset) {
		int position = offset;
		displayName = MarshalByte.readString(buffer, position, Len.DISPLAY_NAME);
		position += Len.DISPLAY_NAME;
		nameLine1 = MarshalByte.readString(buffer, position, Len.NAME_LINE1);
		position += Len.NAME_LINE1;
		nameLine2 = MarshalByte.readString(buffer, position, Len.NAME_LINE2);
		position += Len.NAME_LINE2;
		adrLine1 = MarshalByte.readString(buffer, position, Len.ADR_LINE1);
		position += Len.ADR_LINE1;
		adrLine2 = MarshalByte.readString(buffer, position, Len.ADR_LINE2);
		position += Len.ADR_LINE2;
		city = MarshalByte.readString(buffer, position, Len.CITY);
		position += Len.CITY;
		stateAbb = MarshalByte.readString(buffer, position, Len.STATE_ABB);
		position += Len.STATE_ABB;
		postalCode = MarshalByte.readString(buffer, position, Len.POSTAL_CODE);
	}

	public byte[] getAddressComponentsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, displayName, Len.DISPLAY_NAME);
		position += Len.DISPLAY_NAME;
		MarshalByte.writeString(buffer, position, nameLine1, Len.NAME_LINE1);
		position += Len.NAME_LINE1;
		MarshalByte.writeString(buffer, position, nameLine2, Len.NAME_LINE2);
		position += Len.NAME_LINE2;
		MarshalByte.writeString(buffer, position, adrLine1, Len.ADR_LINE1);
		position += Len.ADR_LINE1;
		MarshalByte.writeString(buffer, position, adrLine2, Len.ADR_LINE2);
		position += Len.ADR_LINE2;
		MarshalByte.writeString(buffer, position, city, Len.CITY);
		position += Len.CITY;
		MarshalByte.writeString(buffer, position, stateAbb, Len.STATE_ABB);
		position += Len.STATE_ABB;
		MarshalByte.writeString(buffer, position, postalCode, Len.POSTAL_CODE);
		return buffer;
	}

	public void setDisplayName(String displayName) {
		this.displayName = Functions.subString(displayName, Len.DISPLAY_NAME);
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public String getDisplayNameFormatted() {
		return Functions.padBlanks(getDisplayName(), Len.DISPLAY_NAME);
	}

	public void setNameLine1(String nameLine1) {
		this.nameLine1 = Functions.subString(nameLine1, Len.NAME_LINE1);
	}

	public String getNameLine1() {
		return this.nameLine1;
	}

	public void setNameLine2(String nameLine2) {
		this.nameLine2 = Functions.subString(nameLine2, Len.NAME_LINE2);
	}

	public String getNameLine2() {
		return this.nameLine2;
	}

	public void setAdrLine1(String adrLine1) {
		this.adrLine1 = Functions.subString(adrLine1, Len.ADR_LINE1);
	}

	public String getAdrLine1() {
		return this.adrLine1;
	}

	public void setAdrLine2(String adrLine2) {
		this.adrLine2 = Functions.subString(adrLine2, Len.ADR_LINE2);
	}

	public String getAdrLine2() {
		return this.adrLine2;
	}

	public void setCity(String city) {
		this.city = Functions.subString(city, Len.CITY);
	}

	public String getCity() {
		return this.city;
	}

	public String getCityFormatted() {
		return Functions.padBlanks(getCity(), Len.CITY);
	}

	public void setStateAbb(String stateAbb) {
		this.stateAbb = Functions.subString(stateAbb, Len.STATE_ABB);
	}

	public String getStateAbb() {
		return this.stateAbb;
	}

	public String getStateAbbFormatted() {
		return Functions.padBlanks(getStateAbb(), Len.STATE_ABB);
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = Functions.subString(postalCode, Len.POSTAL_CODE);
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public String getPostalCodeFormatted() {
		return Functions.padBlanks(getPostalCode(), Len.POSTAL_CODE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DISPLAY_NAME = 120;
		public static final int NAME_LINE1 = 45;
		public static final int NAME_LINE2 = 45;
		public static final int ADR_LINE1 = 45;
		public static final int ADR_LINE2 = 45;
		public static final int CITY = 30;
		public static final int STATE_ABB = 3;
		public static final int POSTAL_CODE = 13;
		public static final int ADDRESS_COMPONENTS = DISPLAY_NAME + NAME_LINE1 + NAME_LINE2 + ADR_LINE1 + ADR_LINE2 + CITY + STATE_ABB + POSTAL_CODE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
