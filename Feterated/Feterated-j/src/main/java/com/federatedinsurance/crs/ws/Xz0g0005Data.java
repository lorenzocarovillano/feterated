/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0G0005<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0g0005Data {

	//==== PROPERTIES ====
	//Original name: CF-INITIALIZATION-VALUE
	private char cfInitializationValue = Types.SPACE_CHAR;
	/**Original name: CF-SERVICE-PROXY-PGM<br>
	 * <pre> This is the name of the proxy program.</pre>*/
	private String cfServiceProxyPgm = "XZ0X0005";
	//Original name: EA-01-CICS-LINK-ERROR
	private Ea01CicsLinkError ea01CicsLinkError = new Ea01CicsLinkError();
	//Original name: EA-02-GETMAIN-FREEMAIN-ERROR
	private Ea02GetmainFreemainError ea02GetmainFreemainError = new Ea02GetmainFreemainError();
	//Original name: WS-RESPONSE-CODE
	private int wsResponseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int wsResponseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: SERVICE-PROXY-CONTRACT
	private WsProxyProgramArea serviceProxyContract = new WsProxyProgramArea();

	//==== METHODS ====
	public char getCfInitializationValue() {
		return this.cfInitializationValue;
	}

	public String getCfServiceProxyPgm() {
		return this.cfServiceProxyPgm;
	}

	public void setWsResponseCode(int wsResponseCode) {
		this.wsResponseCode = wsResponseCode;
	}

	public int getWsResponseCode() {
		return this.wsResponseCode;
	}

	public void setWsResponseCode2(int wsResponseCode2) {
		this.wsResponseCode2 = wsResponseCode2;
	}

	public int getWsResponseCode2() {
		return this.wsResponseCode2;
	}

	public String getWsResponseCode2Formatted() {
		return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.BINARY)).format(getWsResponseCode2()).toString();
	}

	public Ea01CicsLinkError getEa01CicsLinkError() {
		return ea01CicsLinkError;
	}

	public Ea02GetmainFreemainError getEa02GetmainFreemainError() {
		return ea02GetmainFreemainError;
	}

	public WsProxyProgramArea getServiceProxyContract() {
		return serviceProxyContract;
	}
}
