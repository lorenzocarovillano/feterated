/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLREC-CO-TYP<br>
 * Variable: DCLREC-CO-TYP from copybook XZH00010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclrecCoTyp {

	//==== PROPERTIES ====
	//Original name: CO-CD
	private String coCd = DefaultValues.stringVal(Len.CO_CD);
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setCoCd(String coCd) {
		this.coCd = Functions.subString(coCd, Len.CO_CD);
	}

	public String getCoCd() {
		return this.coCd;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_TYP_CD = 5;
		public static final int CO_CD = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
