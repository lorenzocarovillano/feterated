/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalUowPrcSeqV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_UOW_PRC_SEQ_V]
 * 
 */
public class HalUowPrcSeqVDao extends BaseSqlDao<IHalUowPrcSeqV> {

	private Cursor cur1;
	private Cursor primarybocur;
	private final IRowMapper<IHalUowPrcSeqV> fetchCur1Rm = buildNamedRowMapper(IHalUowPrcSeqV.class, "rootBobjNm", "uowSeqNbr");
	private final IRowMapper<IHalUowPrcSeqV> fetchPrimarybocurRm = buildNamedRowMapper(IHalUowPrcSeqV.class, "hupsUowNm", "uowSeqNbr", "rootBobjNm",
			"hupsBrnPrcCd");
	private final IRowMapper<IHalUowPrcSeqV> selectRecRm = buildNamedRowMapper(IHalUowPrcSeqV.class, "hupsUowNm", "uowSeqNbr", "rootBobjNm");

	public HalUowPrcSeqVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalUowPrcSeqV> getToClass() {
		return IHalUowPrcSeqV.class;
	}

	public DbAccessStatus openCur1(String uowNm, char brnPrcCd) {
		cur1 = buildQuery("openCur1").bind("uowNm", uowNm).bind("brnPrcCd", String.valueOf(brnPrcCd)).open();
		return dbStatus;
	}

	public IHalUowPrcSeqV fetchCur1(IHalUowPrcSeqV iHalUowPrcSeqV) {
		return fetch(cur1, iHalUowPrcSeqV, fetchCur1Rm);
	}

	public DbAccessStatus closeCur1() {
		return closeCursor(cur1);
	}

	public DbAccessStatus openPrimarybocur(String hupsUowNm) {
		primarybocur = buildQuery("openPrimarybocur").bind("hupsUowNm", hupsUowNm).open();
		return dbStatus;
	}

	public DbAccessStatus closePrimarybocur() {
		return closeCursor(primarybocur);
	}

	public IHalUowPrcSeqV fetchPrimarybocur(IHalUowPrcSeqV iHalUowPrcSeqV) {
		return fetch(primarybocur, iHalUowPrcSeqV, fetchPrimarybocurRm);
	}

	public IHalUowPrcSeqV selectRec(String hupsUowNm, short waLastUowSeqNbr, IHalUowPrcSeqV iHalUowPrcSeqV) {
		return buildQuery("selectRec").bind("hupsUowNm", hupsUowNm).bind("waLastUowSeqNbr", waLastUowSeqNbr).rowMapper(selectRecRm)
				.singleResult(iHalUowPrcSeqV);
	}
}
