/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-CURRENT-DATE-TIME<br>
 * Variable: SA-CURRENT-DATE-TIME from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaCurrentDateTime {

	//==== PROPERTIES ====
	//Original name: SA-CD-YEAR
	private String year = DefaultValues.stringVal(Len.YEAR);
	//Original name: SA-CD-MONTH-DAY
	private String monthDay = DefaultValues.stringVal(Len.MONTH_DAY);
	//Original name: SA-CD-HOURS
	private String hours = DefaultValues.stringVal(Len.HOURS);
	//Original name: SA-CD-MINUTES
	private String minutes = DefaultValues.stringVal(Len.MINUTES);
	//Original name: SA-CD-SECONDS
	private String seconds = DefaultValues.stringVal(Len.SECONDS);
	//Original name: SA-CD-HUNDREDTHS-OF-SECOND
	private String hundredthsOfSecond = DefaultValues.stringVal(Len.HUNDREDTHS_OF_SECOND);
	//Original name: FILLER-SA-CURRENT-DATE-TIME
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setCurrentDateTimeFormatted(String data) {
		byte[] buffer = new byte[Len.CURRENT_DATE_TIME];
		MarshalByte.writeString(buffer, 1, data, Len.CURRENT_DATE_TIME);
		setCurrentDateTimeBytes(buffer, 1);
	}

	public void setCurrentDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		setDateFldBytes(buffer, position);
		position += Len.DATE_FLD;
		setTimeFldBytes(buffer, position);
		position += Len.TIME_FLD;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getCurrentDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		getDateFldBytes(buffer, position);
		position += Len.DATE_FLD;
		getTimeFldBytes(buffer, position);
		position += Len.TIME_FLD;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setDateFldBytes(byte[] buffer, int offset) {
		int position = offset;
		year = MarshalByte.readString(buffer, position, Len.YEAR);
		position += Len.YEAR;
		monthDay = MarshalByte.readString(buffer, position, Len.MONTH_DAY);
	}

	public byte[] getDateFldBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, year, Len.YEAR);
		position += Len.YEAR;
		MarshalByte.writeString(buffer, position, monthDay, Len.MONTH_DAY);
		return buffer;
	}

	public void setYear(String year) {
		this.year = Functions.subString(year, Len.YEAR);
	}

	public String getYear() {
		return this.year;
	}

	public void setMonthDay(String monthDay) {
		this.monthDay = Functions.subString(monthDay, Len.MONTH_DAY);
	}

	public String getMonthDay() {
		return this.monthDay;
	}

	public String getMonthDayFormatted() {
		return Functions.padBlanks(getMonthDay(), Len.MONTH_DAY);
	}

	public void setTimeFldBytes(byte[] buffer, int offset) {
		int position = offset;
		hours = MarshalByte.readString(buffer, position, Len.HOURS);
		position += Len.HOURS;
		minutes = MarshalByte.readString(buffer, position, Len.MINUTES);
		position += Len.MINUTES;
		seconds = MarshalByte.readString(buffer, position, Len.SECONDS);
		position += Len.SECONDS;
		hundredthsOfSecond = MarshalByte.readString(buffer, position, Len.HUNDREDTHS_OF_SECOND);
	}

	public byte[] getTimeFldBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, hours, Len.HOURS);
		position += Len.HOURS;
		MarshalByte.writeString(buffer, position, minutes, Len.MINUTES);
		position += Len.MINUTES;
		MarshalByte.writeString(buffer, position, seconds, Len.SECONDS);
		position += Len.SECONDS;
		MarshalByte.writeString(buffer, position, hundredthsOfSecond, Len.HUNDREDTHS_OF_SECOND);
		return buffer;
	}

	public void setHours(String hours) {
		this.hours = Functions.subString(hours, Len.HOURS);
	}

	public String getHours() {
		return this.hours;
	}

	public void setMinutes(String minutes) {
		this.minutes = Functions.subString(minutes, Len.MINUTES);
	}

	public String getMinutes() {
		return this.minutes;
	}

	public void setSeconds(String seconds) {
		this.seconds = Functions.subString(seconds, Len.SECONDS);
	}

	public String getSeconds() {
		return this.seconds;
	}

	public void setHundredthsOfSecond(String hundredthsOfSecond) {
		this.hundredthsOfSecond = Functions.subString(hundredthsOfSecond, Len.HUNDREDTHS_OF_SECOND);
	}

	public String getHundredthsOfSecond() {
		return this.hundredthsOfSecond;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YEAR = 4;
		public static final int MONTH_DAY = 4;
		public static final int DATE_FLD = YEAR + MONTH_DAY;
		public static final int HOURS = 2;
		public static final int MINUTES = 2;
		public static final int SECONDS = 2;
		public static final int HUNDREDTHS_OF_SECOND = 2;
		public static final int TIME_FLD = HOURS + MINUTES + SECONDS + HUNDREDTHS_OF_SECOND;
		public static final int FLR1 = 5;
		public static final int CURRENT_DATE_TIME = DATE_FLD + TIME_FLD + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
