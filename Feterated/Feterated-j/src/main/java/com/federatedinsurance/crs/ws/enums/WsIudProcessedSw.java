/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-IUD-PROCESSED-SW<br>
 * Variable: WS-IUD-PROCESSED-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsIudProcessedSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char PROCESSED = 'Y';
	public static final char NOT_PROCESSED = 'N';

	//==== METHODS ====
	public void setIudProcessedSw(char iudProcessedSw) {
		this.value = iudProcessedSw;
	}

	public char getIudProcessedSw() {
		return this.value;
	}

	public boolean isWsIudProcessed() {
		return value == PROCESSED;
	}

	public void setWsIudProcessed() {
		value = PROCESSED;
	}

	public void setWsIudNotProcessed() {
		value = NOT_PROCESSED;
	}
}
