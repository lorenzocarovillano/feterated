/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.occurs.Bxt03oFeeInf;
import com.federatedinsurance.crs.ws.occurs.Bxt03oPolicyInf;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: BXT03O-ACCOUNT-INFO<br>
 * Variable: BXT03O-ACCOUNT-INFO from copybook BX0T0003<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Bxt03oAccountInfo {

	//==== PROPERTIES ====
	public static final int FEE_INF_MAXOCCURS = 5;
	public static final int POLICY_INF_MAXOCCURS = 20;
	//Original name: BXT03O-BANKRUPTCY-IND
	private char bankruptcyInd = DefaultValues.CHAR_VAL;
	//Original name: BXT03O-FEE-INF
	private Bxt03oFeeInf[] feeInf = new Bxt03oFeeInf[FEE_INF_MAXOCCURS];
	//Original name: BXT03O-POLICY-INF
	private Bxt03oPolicyInf[] policyInf = new Bxt03oPolicyInf[POLICY_INF_MAXOCCURS];
	//Original name: BXT03O-PAYMENT-MADE-IND
	private char paymentMadeInd = DefaultValues.CHAR_VAL;
	//Original name: BXT03O-INVOICE-SENT-IND
	private char invoiceSentInd = DefaultValues.CHAR_VAL;
	/**Original name: BXT03O-POL-CNT-CI<br>
	 * <pre> POLICY COUNT EXCLUDES BONDS</pre>*/
	private char polCntCi = DefaultValues.CHAR_VAL;
	//Original name: BXT03O-POL-CNT
	private String polCnt = DefaultValues.stringVal(Len.POL_CNT);
	/**Original name: BXT03O-PCN-POL-CNT-CI<br>
	 * <pre> PCN POLICY COUNT EXCLUDES BONDS</pre>*/
	private char pcnPolCntCi = DefaultValues.CHAR_VAL;
	//Original name: BXT03O-PCN-POL-CNT
	private String pcnPolCnt = DefaultValues.stringVal(Len.PCN_POL_CNT);

	//==== CONSTRUCTORS ====
	public Bxt03oAccountInfo() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int feeInfIdx = 1; feeInfIdx <= FEE_INF_MAXOCCURS; feeInfIdx++) {
			feeInf[feeInfIdx - 1] = new Bxt03oFeeInf();
		}
		for (int policyInfIdx = 1; policyInfIdx <= POLICY_INF_MAXOCCURS; policyInfIdx++) {
			policyInf[policyInfIdx - 1] = new Bxt03oPolicyInf();
		}
	}

	public void setBxt03oAccountInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		bankruptcyInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		setFeeListBytes(buffer, position);
		position += Len.FEE_LIST;
		setPolicyListBytes(buffer, position);
		position += Len.POLICY_LIST;
		paymentMadeInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		invoiceSentInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polCntCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polCnt = MarshalByte.readFixedString(buffer, position, Len.POL_CNT);
		position += Len.POL_CNT;
		pcnPolCntCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pcnPolCnt = MarshalByte.readFixedString(buffer, position, Len.PCN_POL_CNT);
	}

	public byte[] getBxt03oAccountInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, bankruptcyInd);
		position += Types.CHAR_SIZE;
		getFeeListBytes(buffer, position);
		position += Len.FEE_LIST;
		getPolicyListBytes(buffer, position);
		position += Len.POLICY_LIST;
		MarshalByte.writeChar(buffer, position, paymentMadeInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, invoiceSentInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polCntCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, polCnt, Len.POL_CNT);
		position += Len.POL_CNT;
		MarshalByte.writeChar(buffer, position, pcnPolCntCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pcnPolCnt, Len.PCN_POL_CNT);
		return buffer;
	}

	public void setBankruptcyInd(char bankruptcyInd) {
		this.bankruptcyInd = bankruptcyInd;
	}

	public char getBankruptcyInd() {
		return this.bankruptcyInd;
	}

	public void setFeeListBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= FEE_INF_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				feeInf[idx - 1].setFeeInfBytes(buffer, position);
				position += Bxt03oFeeInf.Len.FEE_INF;
			} else {
				feeInf[idx - 1].initFeeInfSpaces();
				position += Bxt03oFeeInf.Len.FEE_INF;
			}
		}
	}

	public byte[] getFeeListBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= FEE_INF_MAXOCCURS; idx++) {
			feeInf[idx - 1].getFeeInfBytes(buffer, position);
			position += Bxt03oFeeInf.Len.FEE_INF;
		}
		return buffer;
	}

	public void setPolicyListBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POLICY_INF_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				policyInf[idx - 1].setPolicyInfBytes(buffer, position);
				position += Bxt03oPolicyInf.Len.POLICY_INF;
			} else {
				policyInf[idx - 1].initPolicyInfSpaces();
				position += Bxt03oPolicyInf.Len.POLICY_INF;
			}
		}
	}

	public byte[] getPolicyListBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POLICY_INF_MAXOCCURS; idx++) {
			policyInf[idx - 1].getPolicyInfBytes(buffer, position);
			position += Bxt03oPolicyInf.Len.POLICY_INF;
		}
		return buffer;
	}

	public void setPaymentMadeInd(char paymentMadeInd) {
		this.paymentMadeInd = paymentMadeInd;
	}

	public char getPaymentMadeInd() {
		return this.paymentMadeInd;
	}

	public void setInvoiceSentInd(char invoiceSentInd) {
		this.invoiceSentInd = invoiceSentInd;
	}

	public char getInvoiceSentInd() {
		return this.invoiceSentInd;
	}

	public void setPolCntCi(char polCntCi) {
		this.polCntCi = polCntCi;
	}

	public char getPolCntCi() {
		return this.polCntCi;
	}

	public void setPolCntFormatted(String polCnt) {
		this.polCnt = Trunc.toUnsignedNumeric(polCnt, Len.POL_CNT);
	}

	public int getPolCnt() {
		return NumericDisplay.asInt(this.polCnt);
	}

	public void setPcnPolCntCi(char pcnPolCntCi) {
		this.pcnPolCntCi = pcnPolCntCi;
	}

	public char getPcnPolCntCi() {
		return this.pcnPolCntCi;
	}

	public void setPcnPolCntFormatted(String pcnPolCnt) {
		this.pcnPolCnt = Trunc.toUnsignedNumeric(pcnPolCnt, Len.PCN_POL_CNT);
	}

	public int getPcnPolCnt() {
		return NumericDisplay.asInt(this.pcnPolCnt);
	}

	public Bxt03oFeeInf getFeeInf(int idx) {
		return feeInf[idx - 1];
	}

	public Bxt03oPolicyInf getPolicyInf(int idx) {
		return policyInf[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BANKRUPTCY_IND = 1;
		public static final int FEE_LIST = Bxt03oAccountInfo.FEE_INF_MAXOCCURS * Bxt03oFeeInf.Len.FEE_INF;
		public static final int POLICY_LIST = Bxt03oAccountInfo.POLICY_INF_MAXOCCURS * Bxt03oPolicyInf.Len.POLICY_INF;
		public static final int PAYMENT_MADE_IND = 1;
		public static final int INVOICE_SENT_IND = 1;
		public static final int POL_CNT_CI = 1;
		public static final int POL_CNT = 7;
		public static final int PCN_POL_CNT_CI = 1;
		public static final int PCN_POL_CNT = 7;
		public static final int BXT03O_ACCOUNT_INFO = BANKRUPTCY_IND + FEE_LIST + POLICY_LIST + PAYMENT_MADE_IND + INVOICE_SENT_IND + POL_CNT_CI
				+ POL_CNT + PCN_POL_CNT_CI + PCN_POL_CNT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
