/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-INBOUND-REQ-SW<br>
 * Variable: WS-INBOUND-REQ-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsInboundReqSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char INBOUND_REQ = 'Y';
	public static final char NOT_INBOUND_REQ = 'N';

	//==== METHODS ====
	public void setWsInboundReqSw(char wsInboundReqSw) {
		this.value = wsInboundReqSw;
	}

	public char getWsInboundReqSw() {
		return this.value;
	}

	public boolean isInboundReq() {
		return value == INBOUND_REQ;
	}

	public void setInboundReq() {
		value = INBOUND_REQ;
	}

	public void setNotInboundReq() {
		value = NOT_INBOUND_REQ;
	}
}
