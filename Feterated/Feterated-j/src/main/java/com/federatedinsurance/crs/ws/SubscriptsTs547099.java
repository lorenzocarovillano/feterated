/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsTs547099 {

	//==== PROPERTIES ====
	//Original name: SS-TP
	private short tp = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-TP-ORIG
	private short tpOrig = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-TP-REDIR
	private short tpRedir = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-TP-TEMP
	private short tpTemp = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setTp(short tp) {
		this.tp = tp;
	}

	public short getTp() {
		return this.tp;
	}

	public void setTpOrig(short tpOrig) {
		this.tpOrig = tpOrig;
	}

	public short getTpOrig() {
		return this.tpOrig;
	}

	public void setTpRedir(short tpRedir) {
		this.tpRedir = tpRedir;
	}

	public short getTpRedir() {
		return this.tpRedir;
	}

	public void setTpTemp(short tpTemp) {
		this.tpTemp = tpTemp;
	}

	public short getTpTemp() {
		return this.tpTemp;
	}
}
