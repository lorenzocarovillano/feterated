/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [REC_TYP]
 * 
 */
public interface IRecTyp extends BaseSqlTo {

	/**
	 * Host Variable REC-SHT-DES
	 * 
	 */
	String getShtDes();

	void setShtDes(String shtDes);

	/**
	 * Host Variable REC-LNG-DES
	 * 
	 */
	String getLngDes();

	void setLngDes(String lngDes);

	/**
	 * Host Variable REC-SR-ORD-NBR
	 * 
	 */
	short getSrOrdNbr();

	void setSrOrdNbr(short srOrdNbr);
};
