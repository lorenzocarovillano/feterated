/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: HALRURQA-FUNCTION<br>
 * Variable: HALRURQA-FUNCTION from copybook HALLURQA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HalrurqaFunction {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char READ_FUNC = 'R';
	public static final char WRITE_FUNC = 'W';
	public static final char UPDATE_FUNC = 'U';
	public static final char ALLDEL_FUNC = 'C';
	public static final char DELBO_FUNC = 'B';

	//==== METHODS ====
	public void setFunction(char function) {
		this.value = function;
	}

	public char getFunction() {
		return this.value;
	}

	public void setHalrrespReadFunc() {
		value = READ_FUNC;
	}

	public void setHalrurqaWriteFunc() {
		value = WRITE_FUNC;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FUNCTION = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
