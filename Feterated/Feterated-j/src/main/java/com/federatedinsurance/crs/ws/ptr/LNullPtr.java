/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-NULL-PTR<br>
 * Variable: L-NULL-PTR from program TS548099<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LNullPtr extends BytesClass {

	//==== CONSTRUCTORS ====
	public LNullPtr() {
	}

	public LNullPtr(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_NULL_PTR;
	}

	public int getlNullPtr() {
		return readBinaryInt(Pos.L_NULL_PTR);
	}

	public String getlNullPtrFormatted() {
		return "" + getlNullPtr();
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_NULL_PTR = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int L_NULL_PTR = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
