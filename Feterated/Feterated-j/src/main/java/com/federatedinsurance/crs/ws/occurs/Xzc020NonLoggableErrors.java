/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC020-NON-LOGGABLE-ERRORS<br>
 * Variables: XZC020-NON-LOGGABLE-ERRORS from copybook XZC020C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzc020NonLoggableErrors {

	//==== PROPERTIES ====
	//Original name: XZC020-NON-LOG-ERR-MSG
	private String xzc020NonLogErrMsg = DefaultValues.stringVal(Len.XZC020_NON_LOG_ERR_MSG);

	//==== METHODS ====
	public void setXzc020NonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc020NonLogErrMsg = MarshalByte.readString(buffer, position, Len.XZC020_NON_LOG_ERR_MSG);
	}

	public byte[] getXzc020NonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc020NonLogErrMsg, Len.XZC020_NON_LOG_ERR_MSG);
		return buffer;
	}

	public void initXzc020NonLoggableErrorsSpaces() {
		xzc020NonLogErrMsg = "";
	}

	public void setXzc020NonLogErrMsg(String xzc020NonLogErrMsg) {
		this.xzc020NonLogErrMsg = Functions.subString(xzc020NonLogErrMsg, Len.XZC020_NON_LOG_ERR_MSG);
	}

	public String getXzc020NonLogErrMsg() {
		return this.xzc020NonLogErrMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC020_NON_LOG_ERR_MSG = 500;
		public static final int XZC020_NON_LOGGABLE_ERRORS = XZC020_NON_LOG_ERR_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
