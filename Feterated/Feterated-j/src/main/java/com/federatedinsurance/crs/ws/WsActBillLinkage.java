/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.Bx0t0001;
import com.federatedinsurance.crs.copy.ProxyProgramCommon;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-ACT-BILL-LINKAGE<br>
 * Variable: WS-ACT-BILL-LINKAGE from program XZ0P90E0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsActBillLinkage extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: PROXY-PROGRAM-COMMON
	private ProxyProgramCommon proxyProgramCommon = new ProxyProgramCommon();
	//Original name: BX0T0001
	private Bx0t0001 bx0t0001 = new Bx0t0001();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_ACT_BILL_LINKAGE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsActBillLinkageBytes(buf);
	}

	public void setWsActBillLinkageBytes(byte[] buffer) {
		setWsActBillLinkageBytes(buffer, 1);
	}

	public byte[] getWsActBillLinkageBytes() {
		byte[] buffer = new byte[Len.WS_ACT_BILL_LINKAGE];
		return getWsActBillLinkageBytes(buffer, 1);
	}

	public void setWsActBillLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		proxyProgramCommon.setProxyProgramCommonBytes(buffer, position);
		position += ProxyProgramCommon.Len.PROXY_PROGRAM_COMMON;
		bx0t0001.setBxt001ServiceInputsBytes(buffer, position);
		position += Bx0t0001.Len.BXT001_SERVICE_INPUTS;
		bx0t0001.setBxt001ServiceOutputsBytes(buffer, position);
	}

	public byte[] getWsActBillLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		proxyProgramCommon.getProxyProgramCommonBytes(buffer, position);
		position += ProxyProgramCommon.Len.PROXY_PROGRAM_COMMON;
		bx0t0001.getBxt001ServiceInputsBytes(buffer, position);
		position += Bx0t0001.Len.BXT001_SERVICE_INPUTS;
		bx0t0001.getBxt001ServiceOutputsBytes(buffer, position);
		return buffer;
	}

	public Bx0t0001 getBx0t0001() {
		return bx0t0001;
	}

	public ProxyProgramCommon getProxyProgramCommon() {
		return proxyProgramCommon;
	}

	@Override
	public byte[] serialize() {
		return getWsActBillLinkageBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_ACT_BILL_LINKAGE = ProxyProgramCommon.Len.PROXY_PROGRAM_COMMON + Bx0t0001.Len.BXT001_SERVICE_INPUTS
				+ Bx0t0001.Len.BXT001_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
