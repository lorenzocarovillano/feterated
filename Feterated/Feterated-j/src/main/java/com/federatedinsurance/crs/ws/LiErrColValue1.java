/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LI-ERR-COL-VALUE1<br>
 * Variable: LI-ERR-COL-VALUE1 from program HALRPLAC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class LiErrColValue1 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LI-ERR-COL-VALUE1
	private String liErrColValue1 = "";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.LI_ERR_COL_VALUE1;
	}

	@Override
	public void deserialize(byte[] buf) {
		setLiErrColValue1FromBuffer(buf);
	}

	public void setLiErrColValue1(String liErrColValue1) {
		this.liErrColValue1 = Functions.subString(liErrColValue1, Len.LI_ERR_COL_VALUE1);
	}

	public void setLiErrColValue1FromBuffer(byte[] buffer, int offset) {
		setLiErrColValue1(MarshalByte.readString(buffer, offset, Len.LI_ERR_COL_VALUE1));
	}

	public void setLiErrColValue1FromBuffer(byte[] buffer) {
		setLiErrColValue1FromBuffer(buffer, 1);
	}

	public String getLiErrColValue1() {
		return this.liErrColValue1;
	}

	public String getLiErrColValue1Formatted() {
		return Functions.padBlanks(getLiErrColValue1(), Len.LI_ERR_COL_VALUE1);
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.strToBuffer(getLiErrColValue1(), Len.LI_ERR_COL_VALUE1);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LI_ERR_COL_VALUE1 = 75;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int LI_ERR_COL_VALUE1 = 75;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int LI_ERR_COL_VALUE1 = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
