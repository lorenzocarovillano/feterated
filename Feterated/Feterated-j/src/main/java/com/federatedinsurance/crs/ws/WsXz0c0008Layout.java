/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-XZ0C0008-LAYOUT<br>
 * Variable: WS-XZ0C0008-LAYOUT from program XZ0F0008<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0c0008Layout extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZC008-ACT-NOT-POL-REC-CSUM
	private String actNotPolRecCsum = DefaultValues.stringVal(Len.ACT_NOT_POL_REC_CSUM);
	//Original name: XZC008-CSR-ACT-NBR-KCRE
	private String csrActNbrKcre = DefaultValues.stringVal(Len.CSR_ACT_NBR_KCRE);
	//Original name: XZC008-NOT-PRC-TS-KCRE
	private String notPrcTsKcre = DefaultValues.stringVal(Len.NOT_PRC_TS_KCRE);
	//Original name: XZC008-POL-NBR-KCRE
	private String polNbrKcre = DefaultValues.stringVal(Len.POL_NBR_KCRE);
	//Original name: XZC008-REC-SEQ-NBR-KCRE
	private String recSeqNbrKcre = DefaultValues.stringVal(Len.REC_SEQ_NBR_KCRE);
	//Original name: XZC008-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC008-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC008-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC008-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZC008-REC-SEQ-NBR-SIGN
	private char recSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: XZC008-REC-SEQ-NBR
	private String recSeqNbr = DefaultValues.stringVal(Len.REC_SEQ_NBR);
	//Original name: XZC008-CSR-ACT-NBR-CI
	private char csrActNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC008-NOT-PRC-TS-CI
	private char notPrcTsCi = DefaultValues.CHAR_VAL;
	//Original name: XZC008-POL-NBR-CI
	private char polNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC008-REC-SEQ-NBR-CI
	private char recSeqNbrCi = DefaultValues.CHAR_VAL;
	//Original name: FILLER-XZC008-ACT-NOT-POL-REC-DATA
	private char flr1 = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0C0008_LAYOUT;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0c0008LayoutBytes(buf);
	}

	public void setWsXz0c0008LayoutFormatted(String data) {
		byte[] buffer = new byte[Len.WS_XZ0C0008_LAYOUT];
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0C0008_LAYOUT);
		setWsXz0c0008LayoutBytes(buffer, 1);
	}

	public String getWsXz0c0008LayoutFormatted() {
		return getActNotPolRecRowFormatted();
	}

	public void setWsXz0c0008LayoutBytes(byte[] buffer) {
		setWsXz0c0008LayoutBytes(buffer, 1);
	}

	public byte[] getWsXz0c0008LayoutBytes() {
		byte[] buffer = new byte[Len.WS_XZ0C0008_LAYOUT];
		return getWsXz0c0008LayoutBytes(buffer, 1);
	}

	public void setWsXz0c0008LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotPolRecRowBytes(buffer, position);
	}

	public byte[] getWsXz0c0008LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotPolRecRowBytes(buffer, position);
		return buffer;
	}

	public String getActNotPolRecRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotPolRecRowBytes());
	}

	/**Original name: XZC008-ACT-NOT-POL-REC-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0C0008 - ACT_NOT_POL_REC TABLE                               *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  PP02570 23 Sep 2010 E404DLP   GENERATED                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getActNotPolRecRowBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_POL_REC_ROW];
		return getActNotPolRecRowBytes(buffer, 1);
	}

	public void setActNotPolRecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotPolRecFixedBytes(buffer, position);
		position += Len.ACT_NOT_POL_REC_FIXED;
		setActNotPolRecDatesBytes(buffer, position);
		position += Len.ACT_NOT_POL_REC_DATES;
		setActNotPolRecKeyBytes(buffer, position);
		position += Len.ACT_NOT_POL_REC_KEY;
		setActNotPolRecKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_POL_REC_KEY_CI;
		setActNotPolRecDataBytes(buffer, position);
	}

	public byte[] getActNotPolRecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotPolRecFixedBytes(buffer, position);
		position += Len.ACT_NOT_POL_REC_FIXED;
		getActNotPolRecDatesBytes(buffer, position);
		position += Len.ACT_NOT_POL_REC_DATES;
		getActNotPolRecKeyBytes(buffer, position);
		position += Len.ACT_NOT_POL_REC_KEY;
		getActNotPolRecKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_POL_REC_KEY_CI;
		getActNotPolRecDataBytes(buffer, position);
		return buffer;
	}

	public void setActNotPolRecFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotPolRecCsum = MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_POL_REC_CSUM);
		position += Len.ACT_NOT_POL_REC_CSUM;
		csrActNbrKcre = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		notPrcTsKcre = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		polNbrKcre = MarshalByte.readString(buffer, position, Len.POL_NBR_KCRE);
		position += Len.POL_NBR_KCRE;
		recSeqNbrKcre = MarshalByte.readString(buffer, position, Len.REC_SEQ_NBR_KCRE);
	}

	public byte[] getActNotPolRecFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotPolRecCsum, Len.ACT_NOT_POL_REC_CSUM);
		position += Len.ACT_NOT_POL_REC_CSUM;
		MarshalByte.writeString(buffer, position, csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		MarshalByte.writeString(buffer, position, notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		MarshalByte.writeString(buffer, position, polNbrKcre, Len.POL_NBR_KCRE);
		position += Len.POL_NBR_KCRE;
		MarshalByte.writeString(buffer, position, recSeqNbrKcre, Len.REC_SEQ_NBR_KCRE);
		return buffer;
	}

	public void setActNotPolRecCsumFormatted(String actNotPolRecCsum) {
		this.actNotPolRecCsum = Trunc.toUnsignedNumeric(actNotPolRecCsum, Len.ACT_NOT_POL_REC_CSUM);
	}

	public int getActNotPolRecCsum() {
		return NumericDisplay.asInt(this.actNotPolRecCsum);
	}

	public void setCsrActNbrKcre(String csrActNbrKcre) {
		this.csrActNbrKcre = Functions.subString(csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
	}

	public String getCsrActNbrKcre() {
		return this.csrActNbrKcre;
	}

	public void setNotPrcTsKcre(String notPrcTsKcre) {
		this.notPrcTsKcre = Functions.subString(notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
	}

	public String getNotPrcTsKcre() {
		return this.notPrcTsKcre;
	}

	public void setPolNbrKcre(String polNbrKcre) {
		this.polNbrKcre = Functions.subString(polNbrKcre, Len.POL_NBR_KCRE);
	}

	public String getPolNbrKcre() {
		return this.polNbrKcre;
	}

	public void setRecSeqNbrKcre(String recSeqNbrKcre) {
		this.recSeqNbrKcre = Functions.subString(recSeqNbrKcre, Len.REC_SEQ_NBR_KCRE);
	}

	public String getRecSeqNbrKcre() {
		return this.recSeqNbrKcre;
	}

	public void setActNotPolRecDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getActNotPolRecDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setActNotPolRecKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		recSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recSeqNbr = MarshalByte.readFixedString(buffer, position, Len.REC_SEQ_NBR);
	}

	public byte[] getActNotPolRecKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeChar(buffer, position, recSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, recSeqNbr, Len.REC_SEQ_NBR);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setRecSeqNbrSign(char recSeqNbrSign) {
		this.recSeqNbrSign = recSeqNbrSign;
	}

	public char getRecSeqNbrSign() {
		return this.recSeqNbrSign;
	}

	public void setRecSeqNbrFormatted(String recSeqNbr) {
		this.recSeqNbr = Trunc.toUnsignedNumeric(recSeqNbr, Len.REC_SEQ_NBR);
	}

	public int getRecSeqNbr() {
		return NumericDisplay.asInt(this.recSeqNbr);
	}

	public void setActNotPolRecKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notPrcTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recSeqNbrCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotPolRecKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, recSeqNbrCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi = csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi = notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setPolNbrCi(char polNbrCi) {
		this.polNbrCi = polNbrCi;
	}

	public char getPolNbrCi() {
		return this.polNbrCi;
	}

	public void setRecSeqNbrCi(char recSeqNbrCi) {
		this.recSeqNbrCi = recSeqNbrCi;
	}

	public char getRecSeqNbrCi() {
		return this.recSeqNbrCi;
	}

	public void setActNotPolRecDataBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotPolRecDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		return buffer;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0c0008LayoutBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_POL_REC_CSUM = 9;
		public static final int CSR_ACT_NBR_KCRE = 32;
		public static final int NOT_PRC_TS_KCRE = 32;
		public static final int POL_NBR_KCRE = 32;
		public static final int REC_SEQ_NBR_KCRE = 32;
		public static final int ACT_NOT_POL_REC_FIXED = ACT_NOT_POL_REC_CSUM + CSR_ACT_NBR_KCRE + NOT_PRC_TS_KCRE + POL_NBR_KCRE + REC_SEQ_NBR_KCRE;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int ACT_NOT_POL_REC_DATES = TRANS_PROCESS_DT;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int POL_NBR = 25;
		public static final int REC_SEQ_NBR_SIGN = 1;
		public static final int REC_SEQ_NBR = 5;
		public static final int ACT_NOT_POL_REC_KEY = CSR_ACT_NBR + NOT_PRC_TS + POL_NBR + REC_SEQ_NBR_SIGN + REC_SEQ_NBR;
		public static final int CSR_ACT_NBR_CI = 1;
		public static final int NOT_PRC_TS_CI = 1;
		public static final int POL_NBR_CI = 1;
		public static final int REC_SEQ_NBR_CI = 1;
		public static final int ACT_NOT_POL_REC_KEY_CI = CSR_ACT_NBR_CI + NOT_PRC_TS_CI + POL_NBR_CI + REC_SEQ_NBR_CI;
		public static final int FLR1 = 1;
		public static final int ACT_NOT_POL_REC_DATA = FLR1;
		public static final int ACT_NOT_POL_REC_ROW = ACT_NOT_POL_REC_FIXED + ACT_NOT_POL_REC_DATES + ACT_NOT_POL_REC_KEY + ACT_NOT_POL_REC_KEY_CI
				+ ACT_NOT_POL_REC_DATA;
		public static final int WS_XZ0C0008_LAYOUT = ACT_NOT_POL_REC_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
