/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotFrm;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ACT_NOT_FRM]
 * 
 */
public class ActNotFrmDao extends BaseSqlDao<IActNotFrm> {

	private Cursor formCsr;
	private Cursor actNotFrmCsr;
	private final IRowMapper<IActNotFrm> fetchFormCsrRm = buildNamedRowMapper(IActNotFrm.class, "seqNbr", "nbr", "edtDt");
	private final IRowMapper<IActNotFrm> fetchActNotFrmCsrRm = buildNamedRowMapper(IActNotFrm.class, "csrActNbr", "notPrcTs", "frmSeqNbr", "frmNbr",
			"frmEdtDt");

	public ActNotFrmDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotFrm> getToClass() {
		return IActNotFrm.class;
	}

	public DbAccessStatus openFormCsr(String csrActNbr, String notPrcTs) {
		formCsr = buildQuery("openFormCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public IActNotFrm fetchFormCsr(IActNotFrm iActNotFrm) {
		return fetch(formCsr, iActNotFrm, fetchFormCsrRm);
	}

	public DbAccessStatus closeFormCsr() {
		return closeCursor(formCsr);
	}

	public IActNotFrm selectRec(String csrActNbr, String notPrcTs, IActNotFrm iActNotFrm) {
	}

	public DbAccessStatus openActNotFrmCsr(String csrActNbr, String notPrcTs, short frmSeqNbr) {
		actNotFrmCsr = buildQuery("openActNotFrmCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr).open();
		return dbStatus;
	}

	public DbAccessStatus closeActNotFrmCsr() {
		return closeCursor(actNotFrmCsr);
	}

	public IActNotFrm fetchActNotFrmCsr(IActNotFrm iActNotFrm) {
		return fetch(actNotFrmCsr, iActNotFrm, fetchActNotFrmCsrRm);
	}

	public DbAccessStatus insertRec(IActNotFrm iActNotFrm) {
		return buildQuery("insertRec").bind(iActNotFrm).executeInsert();
	}

	public DbAccessStatus deleteRec(String csrActNbr, String notPrcTs, short frmSeqNbr) {
		return buildQuery("deleteRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr).executeDelete();
	}

	public DbAccessStatus deleteRec1(String csrActNbr, String notPrcTs) {
		return buildQuery("deleteRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).executeDelete();
	}

	public IActNotFrm selectRec1(String csrActNbr, String notPrcTs, short frmSeqNbr, IActNotFrm iActNotFrm) {
		return buildQuery("selectRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr).singleResult(iActNotFrm);
	}

	public short selectRec2(String csrActNbr, String notPrcTs, short dft) {
		return buildQuery("selectRec2").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).scalarResultShort(dft);
	}

	public String selectRec3(String xzh006CsrActNbr, String notPrcTs, short frmSeqNbr, String dft) {
		return buildQuery("selectRec3").bind("xzh006CsrActNbr", xzh006CsrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr)
				.scalarResultString(dft);
	}

	public DbAccessStatus updateRec(IActNotFrm iActNotFrm) {
		return buildQuery("updateRec").bind(iActNotFrm).executeUpdate();
	}
}
