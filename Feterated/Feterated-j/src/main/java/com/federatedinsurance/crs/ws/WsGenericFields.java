/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-GENERIC-FIELDS<br>
 * Variable: WS-GENERIC-FIELDS from program CAWS002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsGenericFields {

	//==== PROPERTIES ====
	//Original name: WS-UOW-NM
	private String uowNm = "";
	//Original name: WS-SEC-GRP-NM
	private String secGrpNm = "";
	//Original name: WS-AUTH-USERID
	private String authUserid = "";
	//Original name: WS-SEC-ASC-TYP
	private String secAscTyp = "";
	//Original name: WS-ALL-UOWS-LIT
	private String allUowsLit = "";
	//Original name: WS-ALL-SEC-GRPS-LIT
	private String allSecGrpsLit = "";
	//Original name: WS-ALL-SEC-ASCS-LIT
	private String allSecAscsLit = "";
	//Original name: WS-DEFAULT-FLD-1
	private String defaultFld1 = "999";
	//Original name: WS-DEFAULT-FLD-2
	private String defaultFld2 = "999";
	//Original name: WS-DEFAULT-FLD-3
	private String defaultFld3 = "999";
	//Original name: WS-DEFAULT-FLD-4
	private String defaultFld4 = "999";
	//Original name: WS-DEFAULT-FLD-5
	private String defaultFld5 = "999";
	//Original name: WS-SUPPLIED-FLD-1
	private String suppliedFld1 = "";
	//Original name: WS-SUPPLIED-FLD-2
	private String suppliedFld2 = "";
	//Original name: WS-SUPPLIED-FLD-3
	private String suppliedFld3 = "";
	//Original name: WS-SUPPLIED-FLD-4
	private String suppliedFld4 = "";
	//Original name: WS-SUPPLIED-FLD-5
	private String suppliedFld5 = "";

	//==== METHODS ====
	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public String getUowNmFormatted() {
		return Functions.padBlanks(getUowNm(), Len.UOW_NM);
	}

	public void setSecGrpNm(String secGrpNm) {
		this.secGrpNm = Functions.subString(secGrpNm, Len.SEC_GRP_NM);
	}

	public String getSecGrpNm() {
		return this.secGrpNm;
	}

	public String getSecGrpNmFormatted() {
		return Functions.padBlanks(getSecGrpNm(), Len.SEC_GRP_NM);
	}

	public void setAuthUserid(String authUserid) {
		this.authUserid = Functions.subString(authUserid, Len.AUTH_USERID);
	}

	public String getAuthUserid() {
		return this.authUserid;
	}

	public void setSecAscTyp(String secAscTyp) {
		this.secAscTyp = Functions.subString(secAscTyp, Len.SEC_ASC_TYP);
	}

	public String getSecAscTyp() {
		return this.secAscTyp;
	}

	public void setAllUowsLit(String allUowsLit) {
		this.allUowsLit = Functions.subString(allUowsLit, Len.ALL_UOWS_LIT);
	}

	public String getAllUowsLit() {
		return this.allUowsLit;
	}

	public void setAllSecGrpsLit(String allSecGrpsLit) {
		this.allSecGrpsLit = Functions.subString(allSecGrpsLit, Len.ALL_SEC_GRPS_LIT);
	}

	public String getAllSecGrpsLit() {
		return this.allSecGrpsLit;
	}

	public void setAllSecAscsLit(String allSecAscsLit) {
		this.allSecAscsLit = Functions.subString(allSecAscsLit, Len.ALL_SEC_ASCS_LIT);
	}

	public String getAllSecAscsLit() {
		return this.allSecAscsLit;
	}

	public void setDefaultFld1(String defaultFld1) {
		this.defaultFld1 = Functions.subString(defaultFld1, Len.DEFAULT_FLD1);
	}

	public String getDefaultFld1() {
		return this.defaultFld1;
	}

	public void setDefaultFld2(String defaultFld2) {
		this.defaultFld2 = Functions.subString(defaultFld2, Len.DEFAULT_FLD2);
	}

	public String getDefaultFld2() {
		return this.defaultFld2;
	}

	public void setDefaultFld3(String defaultFld3) {
		this.defaultFld3 = Functions.subString(defaultFld3, Len.DEFAULT_FLD3);
	}

	public String getDefaultFld3() {
		return this.defaultFld3;
	}

	public void setDefaultFld4(String defaultFld4) {
		this.defaultFld4 = Functions.subString(defaultFld4, Len.DEFAULT_FLD4);
	}

	public String getDefaultFld4() {
		return this.defaultFld4;
	}

	public void setDefaultFld5(String defaultFld5) {
		this.defaultFld5 = Functions.subString(defaultFld5, Len.DEFAULT_FLD5);
	}

	public String getDefaultFld5() {
		return this.defaultFld5;
	}

	public void setSuppliedFld1(String suppliedFld1) {
		this.suppliedFld1 = Functions.subString(suppliedFld1, Len.SUPPLIED_FLD1);
	}

	public String getSuppliedFld1() {
		return this.suppliedFld1;
	}

	public void setSuppliedFld2(String suppliedFld2) {
		this.suppliedFld2 = Functions.subString(suppliedFld2, Len.SUPPLIED_FLD2);
	}

	public String getSuppliedFld2() {
		return this.suppliedFld2;
	}

	public void setSuppliedFld3(String suppliedFld3) {
		this.suppliedFld3 = Functions.subString(suppliedFld3, Len.SUPPLIED_FLD3);
	}

	public String getSuppliedFld3() {
		return this.suppliedFld3;
	}

	public void setSuppliedFld4(String suppliedFld4) {
		this.suppliedFld4 = Functions.subString(suppliedFld4, Len.SUPPLIED_FLD4);
	}

	public String getSuppliedFld4() {
		return this.suppliedFld4;
	}

	public void setSuppliedFld5(String suppliedFld5) {
		this.suppliedFld5 = Functions.subString(suppliedFld5, Len.SUPPLIED_FLD5);
	}

	public String getSuppliedFld5() {
		return this.suppliedFld5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NM = 32;
		public static final int SEC_GRP_NM = 8;
		public static final int AUTH_USERID = 32;
		public static final int SEC_ASC_TYP = 8;
		public static final int ALL_UOWS_LIT = 32;
		public static final int ALL_SEC_GRPS_LIT = 8;
		public static final int ALL_SEC_ASCS_LIT = 8;
		public static final int DEFAULT_FLD1 = 20;
		public static final int DEFAULT_FLD2 = 8;
		public static final int DEFAULT_FLD3 = 8;
		public static final int DEFAULT_FLD4 = 8;
		public static final int DEFAULT_FLD5 = 8;
		public static final int SUPPLIED_FLD1 = 20;
		public static final int SUPPLIED_FLD2 = 8;
		public static final int SUPPLIED_FLD3 = 8;
		public static final int SUPPLIED_FLD4 = 8;
		public static final int SUPPLIED_FLD5 = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
