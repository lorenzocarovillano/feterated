/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsOperationsCalled;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P0022<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p0022 {

	//==== PROPERTIES ====
	//Original name: WS-EIBRESP-CD
	private short wsEibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short wsEibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo wsErrorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "XZ0P0022";
	//Original name: WS-APPLICATION-NM
	private String wsApplicationNm = "CRS";
	//Original name: WS-OPERATIONS-CALLED
	private WsOperationsCalled wsOperationsCalled = new WsOperationsCalled();
	//Original name: WS-CURRENT-TIMESTAMP-X
	private WsCurrentTimestampX wsCurrentTimestampX = new WsCurrentTimestampX();
	//Original name: WS-PRODUCER-NBR
	private WsProducerNbr wsProducerNbr = new WsProducerNbr();

	//==== METHODS ====
	public void setWsEibrespCd(short wsEibrespCd) {
		this.wsEibrespCd = wsEibrespCd;
	}

	public short getWsEibrespCd() {
		return this.wsEibrespCd;
	}

	public String getWsEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getWsEibrespCd(), Len.WS_EIBRESP_CD);
	}

	public String getWsEibrespCdAsString() {
		return getWsEibrespCdFormatted();
	}

	public void setWsEibresp2Cd(short wsEibresp2Cd) {
		this.wsEibresp2Cd = wsEibresp2Cd;
	}

	public short getWsEibresp2Cd() {
		return this.wsEibresp2Cd;
	}

	public String getWsEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getWsEibresp2Cd(), Len.WS_EIBRESP2_CD);
	}

	public String getWsEibresp2CdAsString() {
		return getWsEibresp2CdFormatted();
	}

	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public String getWsApplicationNm() {
		return this.wsApplicationNm;
	}

	public WsCurrentTimestampX getWsCurrentTimestampX() {
		return wsCurrentTimestampX;
	}

	public WsErrorCheckInfo getWsErrorCheckInfo() {
		return wsErrorCheckInfo;
	}

	public WsOperationsCalled getWsOperationsCalled() {
		return wsOperationsCalled;
	}

	public WsProducerNbr getWsProducerNbr() {
		return wsProducerNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_EIBRESP_CD = 4;
		public static final int WS_EIBRESP2_CD = 4;
		public static final int WS_PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
