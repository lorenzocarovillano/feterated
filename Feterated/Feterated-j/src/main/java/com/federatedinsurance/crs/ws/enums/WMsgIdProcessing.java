/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: W-MSG-ID-PROCESSING<br>
 * Variable: W-MSG-ID-PROCESSING from program HALOUIDG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WMsgIdProcessing {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char NOT_PROCESSED = Types.SPACE_CHAR;
	public static final char PROCESSED = 'P';

	//==== METHODS ====
	public void setMsgIdProcessing(char msgIdProcessing) {
		this.value = msgIdProcessing;
	}

	public void setMsgIdProcessingFormatted(String msgIdProcessing) {
		setMsgIdProcessing(Functions.charAt(msgIdProcessing, Types.CHAR_SIZE));
	}

	public char getMsgIdProcessing() {
		return this.value;
	}

	public boolean isNotProcessed() {
		return value == NOT_PROCESSED;
	}

	public void setNotProcessed() {
		setMsgIdProcessingFormatted(String.valueOf(NOT_PROCESSED));
	}

	public boolean isProcessed() {
		return value == PROCESSED;
	}

	public void setProcessed() {
		value = PROCESSED;
	}
}
