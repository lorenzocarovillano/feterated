/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-SERVICE-CONTRACT-ATB<br>
 * Variable: WS-SERVICE-CONTRACT-ATB from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsServiceContractAtb {

	//==== PROPERTIES ====
	//Original name: WS-SC-INPUT-LEN
	private int inputLen = DefaultValues.BIN_INT_VAL;
	//Original name: WS-SC-OUTPUT-LEN
	private int outputLen = DefaultValues.BIN_INT_VAL;
	//Original name: WS-SC-INPUT-PTR
	private int inputPtr = DefaultValues.BIN_INT_VAL;
	//Original name: WS-SC-OUTPUT-PTR
	private int outputPtr = DefaultValues.BIN_INT_VAL;

	//==== METHODS ====
	public void setInputLen(int inputLen) {
		this.inputLen = inputLen;
	}

	public int getInputLen() {
		return this.inputLen;
	}

	public void setOutputLen(int outputLen) {
		this.outputLen = outputLen;
	}

	public int getOutputLen() {
		return this.outputLen;
	}

	public void setInputPtr(int inputPtr) {
		this.inputPtr = inputPtr;
	}

	public int getInputPtr() {
		return this.inputPtr;
	}

	public void setOutputPtr(int outputPtr) {
		this.outputPtr = outputPtr;
	}

	public int getOutputPtr() {
		return this.outputPtr;
	}
}
