/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.federatedinsurance.crs.ws.enums.TaReportNmbrDdName;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TA-REPORT-DATA<br>
 * Variables: TA-REPORT-DATA from program TS030099<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TaReportData implements ICopyable<TaReportData> {

	//==== PROPERTIES ====
	//Original name: TA-REPORT-NMBR-DD-NAME
	private TaReportNmbrDdName reportNmbrDdName = new TaReportNmbrDdName();
	//Original name: TA-OPEN-CLOSE-COUNT
	private short openCloseCount = ((short) 0);
	public static final short EXCESS_OPEN_CLOSE = ((short) 11);

	//==== CONSTRUCTORS ====
	public TaReportData() {
		init();
	}

	public TaReportData(TaReportData taReportData) {
		this();
		this.reportNmbrDdName = taReportData.reportNmbrDdName.copy();
		this.openCloseCount = taReportData.openCloseCount;
	}

	//==== METHODS ====
	public void init() {
		reportNmbrDdName.initReportNmbrDdNameHighValues();
	}

	public void setOpenCloseCount(short openCloseCount) {
		this.openCloseCount = openCloseCount;
	}

	public short getOpenCloseCount() {
		return this.openCloseCount;
	}

	public boolean isExcessOpenClose() {
		return openCloseCount == EXCESS_OPEN_CLOSE;
	}

	public TaReportNmbrDdName getReportNmbrDdName() {
		return reportNmbrDdName;
	}

	@Override
	public TaReportData copy() {
		return new TaReportData(this);
	}
}
