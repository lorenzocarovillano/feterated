/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R90P0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r90p0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A90P1_MAXOCCURS = 50;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r90p0() {
	}

	public LFrameworkResponseAreaXz0r90p0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza9p0MaxPolRows(short xza9p0MaxPolRows) {
		writeBinaryShort(Pos.XZA9P0_MAX_POL_ROWS, xza9p0MaxPolRows);
	}

	/**Original name: XZA9P0-MAX-POL-ROWS<br>*/
	public short getXza9p0MaxPolRows() {
		return readBinaryShort(Pos.XZA9P0_MAX_POL_ROWS);
	}

	public void setXza9p0CsrActNbr(String xza9p0CsrActNbr) {
		writeString(Pos.XZA9P0_CSR_ACT_NBR, xza9p0CsrActNbr, Len.XZA9P0_CSR_ACT_NBR);
	}

	/**Original name: XZA9P0-CSR-ACT-NBR<br>*/
	public String getXza9p0CsrActNbr() {
		return readString(Pos.XZA9P0_CSR_ACT_NBR, Len.XZA9P0_CSR_ACT_NBR);
	}

	public void setXza9p0NotPrcTs(String xza9p0NotPrcTs) {
		writeString(Pos.XZA9P0_NOT_PRC_TS, xza9p0NotPrcTs, Len.XZA9P0_NOT_PRC_TS);
	}

	/**Original name: XZA9P0-NOT-PRC-TS<br>*/
	public String getXza9p0NotPrcTs() {
		return readString(Pos.XZA9P0_NOT_PRC_TS, Len.XZA9P0_NOT_PRC_TS);
	}

	public void setXza9p0Userid(String xza9p0Userid) {
		writeString(Pos.XZA9P0_USERID, xza9p0Userid, Len.XZA9P0_USERID);
	}

	/**Original name: XZA9P0-USERID<br>*/
	public String getXza9p0Userid() {
		return readString(Pos.XZA9P0_USERID, Len.XZA9P0_USERID);
	}

	public String getlFwRespXz0a90p1Formatted(int lFwRespXz0a90p1Idx) {
		int position = Pos.lFwRespXz0a90p1(lFwRespXz0a90p1Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A90P1);
	}

	public void setXza9p1rPolNbr(int xza9p1rPolNbrIdx, String xza9p1rPolNbr) {
		int position = Pos.xza9p1PolNbr(xza9p1rPolNbrIdx - 1);
		writeString(position, xza9p1rPolNbr, Len.XZA9P1_POL_NBR);
	}

	/**Original name: XZA9P1R-POL-NBR<br>*/
	public String getXza9p1rPolNbr(int xza9p1rPolNbrIdx) {
		int position = Pos.xza9p1PolNbr(xza9p1rPolNbrIdx - 1);
		return readString(position, Len.XZA9P1_POL_NBR);
	}

	public void setXza9p1rPolTyp(int xza9p1rPolTypIdx, String xza9p1rPolTyp) {
		int position = Pos.xza9p1PolTyp(xza9p1rPolTypIdx - 1);
		writeString(position, xza9p1rPolTyp, Len.XZA9P1_POL_TYP);
	}

	/**Original name: XZA9P1R-POL-TYP<br>*/
	public String getXza9p1rPolTyp(int xza9p1rPolTypIdx) {
		int position = Pos.xza9p1PolTyp(xza9p1rPolTypIdx - 1);
		return readString(position, Len.XZA9P1_POL_TYP);
	}

	public void setXza9p1rPolEffDt(int xza9p1rPolEffDtIdx, String xza9p1rPolEffDt) {
		int position = Pos.xza9p1PolEffDt(xza9p1rPolEffDtIdx - 1);
		writeString(position, xza9p1rPolEffDt, Len.XZA9P1_POL_EFF_DT);
	}

	/**Original name: XZA9P1R-POL-EFF-DT<br>*/
	public String getXza9p1rPolEffDt(int xza9p1rPolEffDtIdx) {
		int position = Pos.xza9p1PolEffDt(xza9p1rPolEffDtIdx - 1);
		return readString(position, Len.XZA9P1_POL_EFF_DT);
	}

	public void setXza9p1rPolExpDt(int xza9p1rPolExpDtIdx, String xza9p1rPolExpDt) {
		int position = Pos.xza9p1PolExpDt(xza9p1rPolExpDtIdx - 1);
		writeString(position, xza9p1rPolExpDt, Len.XZA9P1_POL_EXP_DT);
	}

	/**Original name: XZA9P1R-POL-EXP-DT<br>*/
	public String getXza9p1rPolExpDt(int xza9p1rPolExpDtIdx) {
		int position = Pos.xza9p1PolExpDt(xza9p1rPolExpDtIdx - 1);
		return readString(position, Len.XZA9P1_POL_EXP_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_XZ0A90P0 = L_FRAMEWORK_RESPONSE_AREA;
		public static final int XZA9P0_GET_CERT_POL_LIST_KEY = L_FW_RESP_XZ0A90P0;
		public static final int XZA9P0_MAX_POL_ROWS = XZA9P0_GET_CERT_POL_LIST_KEY;
		public static final int XZA9P0_CSR_ACT_NBR = XZA9P0_MAX_POL_ROWS + Len.XZA9P0_MAX_POL_ROWS;
		public static final int XZA9P0_NOT_PRC_TS = XZA9P0_CSR_ACT_NBR + Len.XZA9P0_CSR_ACT_NBR;
		public static final int XZA9P0_USERID = XZA9P0_NOT_PRC_TS + Len.XZA9P0_NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a90p1(int idx) {
			return XZA9P0_USERID + Len.XZA9P0_USERID + idx * Len.L_FW_RESP_XZ0A90P1;
		}

		public static int xza9p1GetCertPolListDtl(int idx) {
			return lFwRespXz0a90p1(idx);
		}

		public static int xza9p1PolNbr(int idx) {
			return xza9p1GetCertPolListDtl(idx);
		}

		public static int xza9p1PolTyp(int idx) {
			return xza9p1PolNbr(idx) + Len.XZA9P1_POL_NBR;
		}

		public static int xza9p1PolEffDt(int idx) {
			return xza9p1PolTyp(idx) + Len.XZA9P1_POL_TYP;
		}

		public static int xza9p1PolExpDt(int idx) {
			return xza9p1PolEffDt(idx) + Len.XZA9P1_POL_EFF_DT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9P0_MAX_POL_ROWS = 2;
		public static final int XZA9P0_CSR_ACT_NBR = 9;
		public static final int XZA9P0_NOT_PRC_TS = 26;
		public static final int XZA9P0_USERID = 8;
		public static final int XZA9P1_POL_NBR = 25;
		public static final int XZA9P1_POL_TYP = 4;
		public static final int XZA9P1_POL_EFF_DT = 10;
		public static final int XZA9P1_POL_EXP_DT = 10;
		public static final int XZA9P1_GET_CERT_POL_LIST_DTL = XZA9P1_POL_NBR + XZA9P1_POL_TYP + XZA9P1_POL_EFF_DT + XZA9P1_POL_EXP_DT;
		public static final int L_FW_RESP_XZ0A90P1 = XZA9P1_GET_CERT_POL_LIST_DTL;
		public static final int XZA9P0_GET_CERT_POL_LIST_KEY = XZA9P0_MAX_POL_ROWS + XZA9P0_CSR_ACT_NBR + XZA9P0_NOT_PRC_TS + XZA9P0_USERID;
		public static final int L_FW_RESP_XZ0A90P0 = XZA9P0_GET_CERT_POL_LIST_KEY;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_XZ0A90P0
				+ LFrameworkResponseAreaXz0r90p0.L_FW_RESP_XZ0A90P1_MAXOCCURS * L_FW_RESP_XZ0A90P1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
