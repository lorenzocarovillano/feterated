/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-06-MAX-TBL-ENTRIES-MSG<br>
 * Variable: EA-06-MAX-TBL-ENTRIES-MSG from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea06MaxTblEntriesMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-06-MAX-TBL-ENTRIES-MSG
	private String flr1 = "XZ001000 -";
	//Original name: FILLER-EA-06-MAX-TBL-ENTRIES-MSG-1
	private String flr2 = "MAX OF";
	//Original name: EA-06-MAX-ENTRIES
	private String maxEntries = DefaultValues.stringVal(Len.MAX_ENTRIES);
	//Original name: FILLER-EA-06-MAX-TBL-ENTRIES-MSG-2
	private String flr3 = " HAS BEEN";
	//Original name: FILLER-EA-06-MAX-TBL-ENTRIES-MSG-3
	private String flr4 = "REACHED FOR";
	//Original name: FILLER-EA-06-MAX-TBL-ENTRIES-MSG-4
	private String flr5 = "TABLE OF";
	//Original name: FILLER-EA-06-MAX-TBL-ENTRIES-MSG-5
	private String flr6 = "POLICY";
	//Original name: FILLER-EA-06-MAX-TBL-ENTRIES-MSG-6
	private String flr7 = "NUMBERS.";
	//Original name: FILLER-EA-06-MAX-TBL-ENTRIES-MSG-7
	private String flr8 = " ACCOUNT NBR =";
	//Original name: EA-06-ACCOUNT-NBR
	private String accountNbr = DefaultValues.stringVal(Len.ACCOUNT_NBR);

	//==== METHODS ====
	public String getEa06MaxTblEntriesMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa06MaxTblEntriesMsgBytes());
	}

	public byte[] getEa06MaxTblEntriesMsgBytes() {
		byte[] buffer = new byte[Len.EA06_MAX_TBL_ENTRIES_MSG];
		return getEa06MaxTblEntriesMsgBytes(buffer, 1);
	}

	public byte[] getEa06MaxTblEntriesMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, maxEntries, Len.MAX_ENTRIES);
		position += Len.MAX_ENTRIES;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		position += Len.FLR8;
		MarshalByte.writeString(buffer, position, accountNbr, Len.ACCOUNT_NBR);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setMaxEntries(long maxEntries) {
		this.maxEntries = PicFormatter.display("++(3)9").format(maxEntries).toString();
	}

	public long getMaxEntries() {
		return PicParser.display("++(3)9").parseLong(this.maxEntries);
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setAccountNbr(String accountNbr) {
		this.accountNbr = Functions.subString(accountNbr, Len.ACCOUNT_NBR);
	}

	public String getAccountNbr() {
		return this.accountNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_ENTRIES = 5;
		public static final int ACCOUNT_NBR = 9;
		public static final int FLR1 = 11;
		public static final int FLR2 = 7;
		public static final int FLR3 = 10;
		public static final int FLR4 = 12;
		public static final int FLR5 = 9;
		public static final int FLR8 = 15;
		public static final int EA06_MAX_TBL_ENTRIES_MSG = MAX_ENTRIES + ACCOUNT_NBR + FLR1 + 2 * FLR2 + FLR3 + FLR4 + 2 * FLR5 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
