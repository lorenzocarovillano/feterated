/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-REPORT-CHANGED-FLAG<br>
 * Variable: SW-REPORT-CHANGED-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwReportChangedFlag {

	//==== PROPERTIES ====
	private char value = '0';
	public static final char NOT_CHANGED = '0';
	public static final char CHANGED = '1';

	//==== METHODS ====
	public void setReportChangedFlag(char reportChangedFlag) {
		this.value = reportChangedFlag;
	}

	public char getReportChangedFlag() {
		return this.value;
	}

	public void setNotChanged() {
		value = NOT_CHANGED;
	}

	public boolean isChanged() {
		return value == CHANGED;
	}

	public void setChanged() {
		value = CHANGED;
	}
}
