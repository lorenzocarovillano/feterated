/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X9081<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x9081 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x9081() {
	}

	public LFrameworkRequestAreaXz0x9081(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza981qMaxTtyRows(short xza981qMaxTtyRows) {
		writeBinaryShort(Pos.XZA981Q_MAX_TTY_ROWS, xza981qMaxTtyRows);
	}

	/**Original name: XZA981Q-MAX-TTY-ROWS<br>*/
	public short getXza981qMaxTtyRows() {
		return readBinaryShort(Pos.XZA981Q_MAX_TTY_ROWS);
	}

	public void setXza981qCsrActNbr(String xza981qCsrActNbr) {
		writeString(Pos.XZA981Q_CSR_ACT_NBR, xza981qCsrActNbr, Len.XZA981Q_CSR_ACT_NBR);
	}

	/**Original name: XZA981Q-CSR-ACT-NBR<br>*/
	public String getXza981qCsrActNbr() {
		return readString(Pos.XZA981Q_CSR_ACT_NBR, Len.XZA981Q_CSR_ACT_NBR);
	}

	public void setXza981qNotPrcTs(String xza981qNotPrcTs) {
		writeString(Pos.XZA981Q_NOT_PRC_TS, xza981qNotPrcTs, Len.XZA981Q_NOT_PRC_TS);
	}

	/**Original name: XZA981Q-NOT-PRC-TS<br>*/
	public String getXza981qNotPrcTs() {
		return readString(Pos.XZA981Q_NOT_PRC_TS, Len.XZA981Q_NOT_PRC_TS);
	}

	public void setXza981qUserid(String xza981qUserid) {
		writeString(Pos.XZA981Q_USERID, xza981qUserid, Len.XZA981Q_USERID);
	}

	/**Original name: XZA981Q-USERID<br>*/
	public String getXza981qUserid() {
		return readString(Pos.XZA981Q_USERID, Len.XZA981Q_USERID);
	}

	public void setXza981qClientId(String xza981qClientId) {
		writeString(Pos.XZA981Q_CLIENT_ID, xza981qClientId, Len.XZA981Q_CLIENT_ID);
	}

	/**Original name: XZA981Q-CLIENT-ID<br>*/
	public String getXza981qClientId() {
		return readString(Pos.XZA981Q_CLIENT_ID, Len.XZA981Q_CLIENT_ID);
	}

	public void setXza981qAdrId(String xza981qAdrId) {
		writeString(Pos.XZA981Q_ADR_ID, xza981qAdrId, Len.XZA981Q_ADR_ID);
	}

	/**Original name: XZA981Q-ADR-ID<br>*/
	public String getXza981qAdrId() {
		return readString(Pos.XZA981Q_ADR_ID, Len.XZA981Q_ADR_ID);
	}

	public void setXza981qRecTypCd(String xza981qRecTypCd) {
		writeString(Pos.XZA981Q_REC_TYP_CD, xza981qRecTypCd, Len.XZA981Q_REC_TYP_CD);
	}

	/**Original name: XZA981Q-REC-TYP-CD<br>*/
	public String getXza981qRecTypCd() {
		return readString(Pos.XZA981Q_REC_TYP_CD, Len.XZA981Q_REC_TYP_CD);
	}

	public void setXza981qRecTypDes(String xza981qRecTypDes) {
		writeString(Pos.XZA981Q_REC_TYP_DES, xza981qRecTypDes, Len.XZA981Q_REC_TYP_DES);
	}

	/**Original name: XZA981Q-REC-TYP-DES<br>*/
	public String getXza981qRecTypDes() {
		return readString(Pos.XZA981Q_REC_TYP_DES, Len.XZA981Q_REC_TYP_DES);
	}

	public void setXza981qName(String xza981qName) {
		writeString(Pos.XZA981Q_NAME, xza981qName, Len.XZA981Q_NAME);
	}

	/**Original name: XZA981Q-NAME<br>*/
	public String getXza981qName() {
		return readString(Pos.XZA981Q_NAME, Len.XZA981Q_NAME);
	}

	public void setXza981qAdrLin1(String xza981qAdrLin1) {
		writeString(Pos.XZA981Q_ADR_LIN1, xza981qAdrLin1, Len.XZA981Q_ADR_LIN1);
	}

	/**Original name: XZA981Q-ADR-LIN1<br>*/
	public String getXza981qAdrLin1() {
		return readString(Pos.XZA981Q_ADR_LIN1, Len.XZA981Q_ADR_LIN1);
	}

	public void setXza981qAdrLin2(String xza981qAdrLin2) {
		writeString(Pos.XZA981Q_ADR_LIN2, xza981qAdrLin2, Len.XZA981Q_ADR_LIN2);
	}

	/**Original name: XZA981Q-ADR-LIN2<br>*/
	public String getXza981qAdrLin2() {
		return readString(Pos.XZA981Q_ADR_LIN2, Len.XZA981Q_ADR_LIN2);
	}

	public void setXza981qCityNm(String xza981qCityNm) {
		writeString(Pos.XZA981Q_CITY_NM, xza981qCityNm, Len.XZA981Q_CITY_NM);
	}

	/**Original name: XZA981Q-CITY-NM<br>*/
	public String getXza981qCityNm() {
		return readString(Pos.XZA981Q_CITY_NM, Len.XZA981Q_CITY_NM);
	}

	public void setXza981qStateAbb(String xza981qStateAbb) {
		writeString(Pos.XZA981Q_STATE_ABB, xza981qStateAbb, Len.XZA981Q_STATE_ABB);
	}

	/**Original name: XZA981Q-STATE-ABB<br>*/
	public String getXza981qStateAbb() {
		return readString(Pos.XZA981Q_STATE_ABB, Len.XZA981Q_STATE_ABB);
	}

	public void setXza981qPstCd(String xza981qPstCd) {
		writeString(Pos.XZA981Q_PST_CD, xza981qPstCd, Len.XZA981Q_PST_CD);
	}

	/**Original name: XZA981Q-PST-CD<br>*/
	public String getXza981qPstCd() {
		return readString(Pos.XZA981Q_PST_CD, Len.XZA981Q_PST_CD);
	}

	public void setXza981qCerNbr(String xza981qCerNbr) {
		writeString(Pos.XZA981Q_CER_NBR, xza981qCerNbr, Len.XZA981Q_CER_NBR);
	}

	/**Original name: XZA981Q-CER-NBR<br>*/
	public String getXza981qCerNbr() {
		return readString(Pos.XZA981Q_CER_NBR, Len.XZA981Q_CER_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A9081 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA981Q_TTY_CERT_INFO_ROW = L_FW_REQ_XZ0A9081;
		public static final int XZA981Q_MAX_TTY_ROWS = XZA981Q_TTY_CERT_INFO_ROW;
		public static final int XZA981Q_CSR_ACT_NBR = XZA981Q_MAX_TTY_ROWS + Len.XZA981Q_MAX_TTY_ROWS;
		public static final int XZA981Q_NOT_PRC_TS = XZA981Q_CSR_ACT_NBR + Len.XZA981Q_CSR_ACT_NBR;
		public static final int XZA981Q_USERID = XZA981Q_NOT_PRC_TS + Len.XZA981Q_NOT_PRC_TS;
		public static final int XZA981Q_TTY_CERT_LIST = XZA981Q_USERID + Len.XZA981Q_USERID;
		public static final int XZA981Q_CLIENT_ID = XZA981Q_TTY_CERT_LIST;
		public static final int XZA981Q_ADR_ID = XZA981Q_CLIENT_ID + Len.XZA981Q_CLIENT_ID;
		public static final int XZA981Q_REC_TYP_CD = XZA981Q_ADR_ID + Len.XZA981Q_ADR_ID;
		public static final int XZA981Q_REC_TYP_DES = XZA981Q_REC_TYP_CD + Len.XZA981Q_REC_TYP_CD;
		public static final int XZA981Q_NAME = XZA981Q_REC_TYP_DES + Len.XZA981Q_REC_TYP_DES;
		public static final int XZA981Q_ADR_LIN1 = XZA981Q_NAME + Len.XZA981Q_NAME;
		public static final int XZA981Q_ADR_LIN2 = XZA981Q_ADR_LIN1 + Len.XZA981Q_ADR_LIN1;
		public static final int XZA981Q_CITY_NM = XZA981Q_ADR_LIN2 + Len.XZA981Q_ADR_LIN2;
		public static final int XZA981Q_STATE_ABB = XZA981Q_CITY_NM + Len.XZA981Q_CITY_NM;
		public static final int XZA981Q_PST_CD = XZA981Q_STATE_ABB + Len.XZA981Q_STATE_ABB;
		public static final int XZA981Q_CER_NBR = XZA981Q_PST_CD + Len.XZA981Q_PST_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA981Q_MAX_TTY_ROWS = 2;
		public static final int XZA981Q_CSR_ACT_NBR = 9;
		public static final int XZA981Q_NOT_PRC_TS = 26;
		public static final int XZA981Q_USERID = 8;
		public static final int XZA981Q_CLIENT_ID = 64;
		public static final int XZA981Q_ADR_ID = 64;
		public static final int XZA981Q_REC_TYP_CD = 5;
		public static final int XZA981Q_REC_TYP_DES = 13;
		public static final int XZA981Q_NAME = 120;
		public static final int XZA981Q_ADR_LIN1 = 45;
		public static final int XZA981Q_ADR_LIN2 = 45;
		public static final int XZA981Q_CITY_NM = 30;
		public static final int XZA981Q_STATE_ABB = 2;
		public static final int XZA981Q_PST_CD = 13;
		public static final int XZA981Q_CER_NBR = 25;
		public static final int XZA981Q_TTY_CERT_LIST = XZA981Q_CLIENT_ID + XZA981Q_ADR_ID + XZA981Q_REC_TYP_CD + XZA981Q_REC_TYP_DES + XZA981Q_NAME
				+ XZA981Q_ADR_LIN1 + XZA981Q_ADR_LIN2 + XZA981Q_CITY_NM + XZA981Q_STATE_ABB + XZA981Q_PST_CD + XZA981Q_CER_NBR;
		public static final int XZA981Q_TTY_CERT_INFO_ROW = XZA981Q_MAX_TTY_ROWS + XZA981Q_CSR_ACT_NBR + XZA981Q_NOT_PRC_TS + XZA981Q_USERID
				+ XZA981Q_TTY_CERT_LIST;
		public static final int L_FW_REQ_XZ0A9081 = XZA981Q_TTY_CERT_INFO_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A9081;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
