/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-UDAT-ROW-SW<br>
 * Variable: WS-UDAT-ROW-SW from program HALRRESP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsUdatRowSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NO_MORE = 'N';
	public static final char ROW_EXIST = 'Y';

	//==== METHODS ====
	public void setsUdatRowSw(char sUdatRowSw) {
		this.value = sUdatRowSw;
	}

	public char getsUdatRowSw() {
		return this.value;
	}

	public boolean isNoMore() {
		return value == NO_MORE;
	}

	public void setNoMore() {
		value = NO_MORE;
	}

	public void setRowExist() {
		value = ROW_EXIST;
	}
}
