/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XWC010-WARNINGS<br>
 * Variables: XWC010-WARNINGS from copybook XWC010C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xwc010Warnings {

	//==== PROPERTIES ====
	//Original name: XWC010-WARN-MSG
	private String xwc010WarnMsg = DefaultValues.stringVal(Len.XWC010_WARN_MSG);

	//==== METHODS ====
	public void setXwc010WarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		xwc010WarnMsg = MarshalByte.readString(buffer, position, Len.XWC010_WARN_MSG);
	}

	public byte[] getXwc010WarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xwc010WarnMsg, Len.XWC010_WARN_MSG);
		return buffer;
	}

	public void initXwc010WarningsSpaces() {
		xwc010WarnMsg = "";
	}

	public void setXwc010WarnMsg(String xwc010WarnMsg) {
		this.xwc010WarnMsg = Functions.subString(xwc010WarnMsg, Len.XWC010_WARN_MSG);
	}

	public String getXwc010WarnMsg() {
		return this.xwc010WarnMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XWC010_WARN_MSG = 500;
		public static final int XWC010_WARNINGS = XWC010_WARN_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
