/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LIO-MESS<br>
 * Variable: LIO-MESS from program HALRPLAC<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LioMess extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int LIO_MESS_STRING_X_MAXOCCURS = 500;
	/**Original name: LIO-MESS-STRING-X<br>
	 * <pre>                                     OCCURS 100</pre>*/
	private char[] lioMessStringX = new char[LIO_MESS_STRING_X_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public LioMess() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.LIO_MESS;
	}

	@Override
	public void deserialize(byte[] buf) {
		setLioMessBytes(buf);
	}

	public void init() {
		for (int lioMessStringXIdx = 1; lioMessStringXIdx <= LIO_MESS_STRING_X_MAXOCCURS; lioMessStringXIdx++) {
			setLioMessStringX(lioMessStringXIdx, DefaultValues.CHAR_VAL);
		}
	}

	public void setLioMessBytes(byte[] buffer) {
		setLioMessBytes(buffer, 1);
	}

	public byte[] getLioMessBytes() {
		byte[] buffer = new byte[Len.LIO_MESS];
		return getLioMessBytes(buffer, 1);
	}

	public void setLioMessBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= LIO_MESS_STRING_X_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setLioMessStringX(idx, MarshalByte.readChar(buffer, position));
				position += Types.CHAR_SIZE;
			} else {
				setLioMessStringX(idx, Types.SPACE_CHAR);
				position += Types.CHAR_SIZE;
			}
		}
	}

	public byte[] getLioMessBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= LIO_MESS_STRING_X_MAXOCCURS; idx++) {
			MarshalByte.writeChar(buffer, position, getLioMessStringX(idx));
			position += Types.CHAR_SIZE;
		}
		return buffer;
	}

	public void setLioMessStringX(int lioMessStringXIdx, char lioMessStringX) {
		this.lioMessStringX[lioMessStringXIdx - 1] = lioMessStringX;
	}

	public char getLioMessStringX(int lioMessStringXIdx) {
		return this.lioMessStringX[lioMessStringXIdx - 1];
	}

	@Override
	public byte[] serialize() {
		return getLioMessBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LIO_MESS_STRING_X = 1;
		public static final int LIO_MESS = LioMess.LIO_MESS_STRING_X_MAXOCCURS * LIO_MESS_STRING_X;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
