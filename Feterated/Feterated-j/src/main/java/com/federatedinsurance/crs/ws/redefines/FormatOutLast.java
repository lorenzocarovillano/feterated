/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: FORMAT-OUT-LAST<br>
 * Variable: FORMAT-OUT-LAST from program CIWBNSRB<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class FormatOutLast extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int FLR1_MAXOCCURS = 60;

	//==== CONSTRUCTORS ====
	public FormatOutLast() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.FORMAT_OUT_LAST;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "", Len.FORMAT_OUT_LAST);
	}

	public void setFormatOutLast(String formatOutLast) {
		writeString(Pos.FORMAT_OUT_LAST, formatOutLast, Len.FORMAT_OUT_LAST);
	}

	public void setFormatOutLastFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.FORMAT_OUT_LAST, Pos.FORMAT_OUT_LAST);
	}

	/**Original name: FORMAT-OUT-LAST<br>*/
	public String getFormatOutLast() {
		return readString(Pos.FORMAT_OUT_LAST, Len.FORMAT_OUT_LAST);
	}

	public String getFormatOutLastFormatted() {
		return Functions.padBlanks(getFormatOutLast(), Len.FORMAT_OUT_LAST);
	}

	public byte[] getFormatOutLastAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.FORMAT_OUT_LAST, Pos.FORMAT_OUT_LAST);
		return buffer;
	}

	/**Original name: FORMAT-OUT-IN-BYTE<br>*/
	public char getInByte(int inByteIdx) {
		int position = Pos.formatOutInByte(inByteIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int FORMAT_OUT_LAST = 1;
		public static final int FORMAT_OUT_BYTE = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int flr1(int idx) {
			return FORMAT_OUT_BYTE + idx * Len.FLR1;
		}

		public static int formatOutInByte(int idx) {
			return flr1(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int IN_BYTE = 1;
		public static final int FLR1 = IN_BYTE;
		public static final int FORMAT_OUT_LAST = 60;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
