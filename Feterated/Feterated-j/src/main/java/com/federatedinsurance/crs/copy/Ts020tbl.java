/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.TfActionCode;
import com.federatedinsurance.crs.ws.enums.TfRecFoundFlag;
import com.federatedinsurance.crs.ws.enums.TfRequestResponseFlag;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: TS020TBL<br>
 * Variable: TS020TBL from copybook TS020TBL<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ts020tbl {

	//==== PROPERTIES ====
	//Original name: TF-REQUEST-RESPONSE-FLAG
	private TfRequestResponseFlag tfRequestResponseFlag = new TfRequestResponseFlag();
	//Original name: TF-BUSINESS-OBJECT-NM
	private String tfBusinessObjectNm = DefaultValues.stringVal(Len.TF_BUSINESS_OBJECT_NM);
	//Original name: TF-ACTION-CODE
	private TfActionCode tfActionCode = new TfActionCode();
	//Original name: TF-REC-SEQ
	private String tfRecSeq = DefaultValues.stringVal(Len.TF_REC_SEQ);
	//Original name: TF-REC-FOUND-FLAG
	private TfRecFoundFlag tfRecFoundFlag = new TfRecFoundFlag();
	//Original name: TF-DATA-BUFFER
	private String tfDataBuffer = DefaultValues.stringVal(Len.TF_DATA_BUFFER);

	//==== METHODS ====
	public String getTableFormatterParmsFormatted() {
		return MarshalByteExt.bufferToStr(getTableFormatterParmsBytes());
	}

	/**Original name: TABLE-FORMATTER-PARMS<br>
	 * <pre>*************************************************************
	 *  TS020TBL - TABLE FORMATTER PARAMETERS                      *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TS129    22JUN2005 E404LJL    NEW                          *
	 *                                                             *
	 * *************************************************************
	 * * TABLE FORMATTER PARAMETERS</pre>*/
	public byte[] getTableFormatterParmsBytes() {
		byte[] buffer = new byte[Len.TABLE_FORMATTER_PARMS];
		return getTableFormatterParmsBytes(buffer, 1);
	}

	public void setTableFormatterParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		tfRequestResponseFlag.setTfRequestResponseFlag(MarshalByte.readString(buffer, position, TfRequestResponseFlag.Len.TF_REQUEST_RESPONSE_FLAG));
		position += TfRequestResponseFlag.Len.TF_REQUEST_RESPONSE_FLAG;
		tfBusinessObjectNm = MarshalByte.readString(buffer, position, Len.TF_BUSINESS_OBJECT_NM);
		position += Len.TF_BUSINESS_OBJECT_NM;
		tfActionCode.setTfActionCode(MarshalByte.readString(buffer, position, TfActionCode.Len.TF_ACTION_CODE));
		position += TfActionCode.Len.TF_ACTION_CODE;
		tfRecSeq = MarshalByte.readFixedString(buffer, position, Len.TF_REC_SEQ);
		position += Len.TF_REC_SEQ;
		tfRecFoundFlag.setTfRecFoundFlag(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		tfDataBuffer = MarshalByte.readString(buffer, position, Len.TF_DATA_BUFFER);
	}

	public byte[] getTableFormatterParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tfRequestResponseFlag.getTfRequestResponseFlag(),
				TfRequestResponseFlag.Len.TF_REQUEST_RESPONSE_FLAG);
		position += TfRequestResponseFlag.Len.TF_REQUEST_RESPONSE_FLAG;
		MarshalByte.writeString(buffer, position, tfBusinessObjectNm, Len.TF_BUSINESS_OBJECT_NM);
		position += Len.TF_BUSINESS_OBJECT_NM;
		MarshalByte.writeString(buffer, position, tfActionCode.getTfActionCode(), TfActionCode.Len.TF_ACTION_CODE);
		position += TfActionCode.Len.TF_ACTION_CODE;
		MarshalByte.writeString(buffer, position, tfRecSeq, Len.TF_REC_SEQ);
		position += Len.TF_REC_SEQ;
		MarshalByte.writeChar(buffer, position, tfRecFoundFlag.getTfRecFoundFlag());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tfDataBuffer, Len.TF_DATA_BUFFER);
		return buffer;
	}

	public void setTfBusinessObjectNm(String tfBusinessObjectNm) {
		this.tfBusinessObjectNm = Functions.subString(tfBusinessObjectNm, Len.TF_BUSINESS_OBJECT_NM);
	}

	public String getTfBusinessObjectNm() {
		return this.tfBusinessObjectNm;
	}

	public void setTfRecSeq(int tfRecSeq) {
		this.tfRecSeq = NumericDisplay.asString(tfRecSeq, Len.TF_REC_SEQ);
	}

	public void setTfRecSeqFormatted(String tfRecSeq) {
		this.tfRecSeq = Trunc.toUnsignedNumeric(tfRecSeq, Len.TF_REC_SEQ);
	}

	public int getTfRecSeq() {
		return NumericDisplay.asInt(this.tfRecSeq);
	}

	public String getTfRecSeqFormatted() {
		return this.tfRecSeq;
	}

	public void setTfDataBuffer(String tfDataBuffer) {
		this.tfDataBuffer = Functions.subString(tfDataBuffer, Len.TF_DATA_BUFFER);
	}

	public String getTfDataBuffer() {
		return this.tfDataBuffer;
	}

	public String getTfDataBufferFormatted() {
		return Functions.padBlanks(getTfDataBuffer(), Len.TF_DATA_BUFFER);
	}

	public TfActionCode getTfActionCode() {
		return tfActionCode;
	}

	public TfRecFoundFlag getTfRecFoundFlag() {
		return tfRecFoundFlag;
	}

	public TfRequestResponseFlag getTfRequestResponseFlag() {
		return tfRequestResponseFlag;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TF_BUSINESS_OBJECT_NM = 32;
		public static final int TF_REC_SEQ = 5;
		public static final int TF_DATA_BUFFER = 5000;
		public static final int TABLE_FORMATTER_PARMS = TfRequestResponseFlag.Len.TF_REQUEST_RESPONSE_FLAG + TF_BUSINESS_OBJECT_NM
				+ TfActionCode.Len.TF_ACTION_CODE + TF_REC_SEQ + TfRecFoundFlag.Len.TF_REC_FOUND_FLAG + TF_DATA_BUFFER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
