/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsFirstCharSw;
import com.federatedinsurance.crs.ws.enums.WsSoundexSw;
import com.federatedinsurance.crs.ws.occurs.WsTmpBuff;
import com.federatedinsurance.crs.ws.redefines.WsTranslateValues;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program CIWOSDX<br>
 * Generated as a class for rule WS.<br>*/
public class CiwosdxData {

	//==== PROPERTIES ====
	public static final int WS_TMP_BUFF_MAXOCCURS = 60;
	//Original name: WS-TMP-BUFF
	private WsTmpBuff[] wsTmpBuff = new WsTmpBuff[WS_TMP_BUFF_MAXOCCURS];
	//Original name: BUF-INDX
	private int bufIndx = 1;
	//Original name: WS-TMP-CTR
	private short wsTmpCtr = ((short) 0);
	//Original name: WS-SDX-CTR
	private short wsSdxCtr = ((short) 0);
	//Original name: WS-FIRST-CHAR-SW
	private WsFirstCharSw wsFirstCharSw = new WsFirstCharSw();
	//Original name: WS-SOUNDEX-SW
	private WsSoundexSw wsSoundexSw = new WsSoundexSw();
	//Original name: WS-PREV-CHAR
	private char wsPrevChar = Types.SPACE_CHAR;
	//Original name: WS-ONE-CHAR
	private char wsOneChar = Types.SPACE_CHAR;
	//Original name: WS-TALLY
	private int wsTally = 0;
	//Original name: WS-SDX-PASSED-PARMS
	private WsSdxPassedParms wsSdxPassedParms = new WsSdxPassedParms();
	//Original name: SDX-INDX
	private int sdxIndx = 1;
	//Original name: WS-ALPHABET
	private String wsAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	//Original name: WS-TRANSLATE-VALUES
	private WsTranslateValues wsTranslateValues = new WsTranslateValues();

	//==== CONSTRUCTORS ====
	public CiwosdxData() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int wsTmpBuffIdx = 1; wsTmpBuffIdx <= WS_TMP_BUFF_MAXOCCURS; wsTmpBuffIdx++) {
			wsTmpBuff[wsTmpBuffIdx - 1] = new WsTmpBuff();
		}
	}

	public void setWsTmpBufferFormatted(String data) {
		byte[] buffer = new byte[Len.WS_TMP_BUFFER];
		MarshalByte.writeString(buffer, 1, data, Len.WS_TMP_BUFFER);
		setWsTmpBufferBytes(buffer, 1);
	}

	public void setWsTmpBufferBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= WS_TMP_BUFF_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				wsTmpBuff[idx - 1].setWsTmpBuffBytes(buffer, position);
				position += WsTmpBuff.Len.WS_TMP_BUFF;
			} else {
				wsTmpBuff[idx - 1].initWsTmpBuffSpaces();
				position += WsTmpBuff.Len.WS_TMP_BUFF;
			}
		}
	}

	public void setBufIndx(int bufIndx) {
		this.bufIndx = bufIndx;
	}

	public int getBufIndx() {
		return this.bufIndx;
	}

	public void setWsTmpCtr(short wsTmpCtr) {
		this.wsTmpCtr = wsTmpCtr;
	}

	public short getWsTmpCtr() {
		return this.wsTmpCtr;
	}

	public void setWsSdxCtr(short wsSdxCtr) {
		this.wsSdxCtr = wsSdxCtr;
	}

	public short getWsSdxCtr() {
		return this.wsSdxCtr;
	}

	public void setWsPrevChar(char wsPrevChar) {
		this.wsPrevChar = wsPrevChar;
	}

	public void setWsPrevCharFormatted(String wsPrevChar) {
		setWsPrevChar(Functions.charAt(wsPrevChar, Types.CHAR_SIZE));
	}

	public char getWsPrevChar() {
		return this.wsPrevChar;
	}

	public void setWsOneChar(char wsOneChar) {
		this.wsOneChar = wsOneChar;
	}

	public char getWsOneChar() {
		return this.wsOneChar;
	}

	public void setWsTally(int wsTally) {
		this.wsTally = wsTally;
	}

	public int getWsTally() {
		return this.wsTally;
	}

	public void setSdxIndx(int sdxIndx) {
		this.sdxIndx = sdxIndx;
	}

	public int getSdxIndx() {
		return this.sdxIndx;
	}

	public String getWsAlphabet() {
		return this.wsAlphabet;
	}

	public String getWsAlphabetFormatted() {
		return Functions.padBlanks(getWsAlphabet(), Len.WS_ALPHABET);
	}

	public WsFirstCharSw getWsFirstCharSw() {
		return wsFirstCharSw;
	}

	public WsSdxPassedParms getWsSdxPassedParms() {
		return wsSdxPassedParms;
	}

	public WsSoundexSw getWsSoundexSw() {
		return wsSoundexSw;
	}

	public WsTmpBuff getWsTmpBuff(int idx) {
		return wsTmpBuff[idx - 1];
	}

	public WsTranslateValues getWsTranslateValues() {
		return wsTranslateValues;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SDX_CTR = 4;
		public static final int WS_TMP_BUFFER = CiwosdxData.WS_TMP_BUFF_MAXOCCURS * WsTmpBuff.Len.WS_TMP_BUFF;
		public static final int WS_ALPHABET = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
