/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-FORMATTED-ACT-NBR<br>
 * Variable: SA-FORMATTED-ACT-NBR from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaFormattedActNbr {

	//==== PROPERTIES ====
	//Original name: SA-FA-FIRST-3
	private String first3 = DefaultValues.stringVal(Len.FIRST3);
	//Original name: FILLER-SA-FORMATTED-ACT-NBR
	private char flr1 = '-';
	//Original name: SA-FA-SECOND-3
	private String second3 = DefaultValues.stringVal(Len.SECOND3);
	//Original name: FILLER-SA-FORMATTED-ACT-NBR-1
	private char flr2 = '-';
	//Original name: SA-FA-LAST-1
	private char last1 = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setFormattedActNbrFormatted(String data) {
		byte[] buffer = new byte[Len.FORMATTED_ACT_NBR];
		MarshalByte.writeString(buffer, 1, data, Len.FORMATTED_ACT_NBR);
		setFormattedActNbrBytes(buffer, 1);
	}

	public String getFormattedActNbrFormatted() {
		return MarshalByteExt.bufferToStr(getFormattedActNbrBytes());
	}

	public byte[] getFormattedActNbrBytes() {
		byte[] buffer = new byte[Len.FORMATTED_ACT_NBR];
		return getFormattedActNbrBytes(buffer, 1);
	}

	public void setFormattedActNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		first3 = MarshalByte.readString(buffer, position, Len.FIRST3);
		position += Len.FIRST3;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		second3 = MarshalByte.readString(buffer, position, Len.SECOND3);
		position += Len.SECOND3;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		last1 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getFormattedActNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, first3, Len.FIRST3);
		position += Len.FIRST3;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, second3, Len.SECOND3);
		position += Len.SECOND3;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, last1);
		return buffer;
	}

	public void setFirst3(String first3) {
		this.first3 = Functions.subString(first3, Len.FIRST3);
	}

	public String getFirst3() {
		return this.first3;
	}

	public String getFirst3Formatted() {
		return Functions.padBlanks(getFirst3(), Len.FIRST3);
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setSecond3(String second3) {
		this.second3 = Functions.subString(second3, Len.SECOND3);
	}

	public String getSecond3() {
		return this.second3;
	}

	public String getSecond3Formatted() {
		return Functions.padBlanks(getSecond3(), Len.SECOND3);
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setLast1(char last1) {
		this.last1 = last1;
	}

	public void setLast1Formatted(String last1) {
		setLast1(Functions.charAt(last1, Types.CHAR_SIZE));
	}

	public char getLast1() {
		return this.last1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FIRST3 = 3;
		public static final int SECOND3 = 3;
		public static final int FLR1 = 1;
		public static final int LAST1 = 1;
		public static final int FORMATTED_ACT_NBR = FIRST3 + SECOND3 + LAST1 + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
