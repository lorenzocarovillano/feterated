/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IFrmGrpTag;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [FRM_GRP, FRM_TAG]
 * 
 */
public class FrmGrpTagDao extends BaseSqlDao<IFrmGrpTag> {

	private Cursor tagCsr;
	private final IRowMapper<IFrmGrpTag> fetchTagCsrRm = buildNamedRowMapper(IFrmGrpTag.class, "edlFrmNm", "dtaGrpNm", "dtaGrpFldNbr", "tagNm",
			"jusCd", "spePrcCdObj", "lenNbr", "tagOccCnt", "delInd");

	public FrmGrpTagDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IFrmGrpTag> getToClass() {
		return IFrmGrpTag.class;
	}

	public DbAccessStatus openTagCsr() {
		tagCsr = buildQuery("openTagCsr").open();
		return dbStatus;
	}

	public IFrmGrpTag fetchTagCsr(IFrmGrpTag iFrmGrpTag) {
		return fetch(tagCsr, iFrmGrpTag, fetchTagCsrRm);
	}

	public DbAccessStatus closeTagCsr() {
		return closeCursor(tagCsr);
	}
}
