/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.PstBrnchProcTyp;

/**Original name: HALLPST<br>
 * Variable: HALLPST from copybook HALLPST<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Hallpst {

	//==== PROPERTIES ====
	//Original name: PST-UOW-NAME
	private String uowName = DefaultValues.stringVal(Len.UOW_NAME);
	//Original name: PST-UOW-SEQ-NBR
	private short uowSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: PST-ROOT-BUS-OBJ
	private String rootBusObj = DefaultValues.stringVal(Len.ROOT_BUS_OBJ);
	/**Original name: PST-BRNCH-PROC-TYP<br>
	 * <pre>*      05 PST-OBJ-MODULE       PIC X(32).</pre>*/
	private PstBrnchProcTyp brnchProcTyp = new PstBrnchProcTyp();

	//==== METHODS ====
	public void setUowPrcSeqBytes(byte[] buffer, int offset) {
		int position = offset;
		uowName = MarshalByte.readString(buffer, position, Len.UOW_NAME);
		position += Len.UOW_NAME;
		uowSeqNbr = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		rootBusObj = MarshalByte.readString(buffer, position, Len.ROOT_BUS_OBJ);
		position += Len.ROOT_BUS_OBJ;
		brnchProcTyp.setBrnchProcTyp(MarshalByte.readChar(buffer, position));
	}

	public void setUowName(String uowName) {
		this.uowName = Functions.subString(uowName, Len.UOW_NAME);
	}

	public String getUowName() {
		return this.uowName;
	}

	public void setUowSeqNbr(short uowSeqNbr) {
		this.uowSeqNbr = uowSeqNbr;
	}

	public short getUowSeqNbr() {
		return this.uowSeqNbr;
	}

	public void setRootBusObj(String rootBusObj) {
		this.rootBusObj = Functions.subString(rootBusObj, Len.ROOT_BUS_OBJ);
	}

	public String getRootBusObj() {
		return this.rootBusObj;
	}

	public PstBrnchProcTyp getBrnchProcTyp() {
		return brnchProcTyp;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NAME = 32;
		public static final int ROOT_BUS_OBJ = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
