/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.redefines.ScratchArea;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XPIODAT-LINKAGE<br>
 * Variable: XPIODAT-LINKAGE from program XPIODAT<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class XpiodatLinkage extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: DATE-FUNCTION
	private char dateFunction = DefaultValues.CHAR_VAL;
	/**Original name: DATE-LANGUAGE<br>
	 * <pre>              C = DATE CONVERSION
	 *               D = DATE DIFFERENCE</pre>*/
	private String dateLanguage = DefaultValues.stringVal(Len.DATE_LANGUAGE);
	//Original name: DATE-INP-FORMAT
	private String dateInpFormat = DefaultValues.stringVal(Len.DATE_INP_FORMAT);
	/**Original name: DATE-OUT-FORMAT<br>
	 * <pre>              J = YYYYDDD/HH:MM:SS
	 *               G = YYYY/MM/DD/HH:MM:SS
	 *               G1 = YYYY/MM/DD/HH:MM:SS
	 *               G7 = YYYY/MM/DD/HH:MM:SS
	 *               M = MM/DD/YYYY/HH:MM:SS
	 *               M1 = MM/DD/YYYY/HH:MM:SS
	 *               M7 = MM/DD/YYYY/HH:MM:SS
	 *               D = DD/MM/YYYY/HH:MM:SS
	 *               D1 = DD/MM/YYYY/HH:MM:SS
	 *               D7 = DD/MM/YYYY/HH:MM:SS
	 *               B = YYYY-MM-DD-HH.MM.SS.MMMMMM
	 *               SY= (SYSTEM)
	 *               S3= IAP SERIES III DATE
	 *               - ON WORKSTATION ONLY
	 *               ?  = (G/M/D) FROM OS/2 CONTROL PANEL
	 *               ?1 = (G1/M1/D1) FROM OS/2 CONTROL PANEL
	 *               ?7 = (G7/M7/D7) FROM OS/2 CONTROL PANEL</pre>*/
	private String dateOutFormat = DefaultValues.stringVal(Len.DATE_OUT_FORMAT);
	/**Original name: DATE-ADJ-FORMAT<br>
	 * <pre>              J = YYYYDDD HH:MM:SS
	 *               G = YYYY/MM/DD HH:MM:SS
	 *               M = MM/DD/YYYY HH:MM:SS
	 *               D = DD/MM/YYYY HH:MM:SS
	 *               W = DAY-OF-WEEK
	 *               AM= HH:MM:SS AM MONTH DD, YYYY
	 *               AD= HH:MM:SS AM DD MONTH YYYY
	 *               AS= HH:MM:SS AM DD MON YYYY
	 *               B = YYYY-MM-DD-HH.MM.SS.MMMMMM
	 *               - ON WORKSTATION ONLY
	 *               ?  = (G/M/D) FROM OS/2 CONTROL PANEL
	 *               ?1 = (G1/M1/D1) FROM OS/2 CONTROL PANEL
	 *               ?7 = (G7/M7/D7) FROM OS/2 CONTROL PANEL
	 *               THIS IS AN INPUT FIELD FOR DIFFERENCE CALULATIONS</pre>*/
	private String dateAdjFormat = DefaultValues.stringVal(Len.DATE_ADJ_FORMAT);
	/**Original name: DATE-RETURN-CODE<br>
	 * <pre>              HD= +/- HOURS
	 *               DD= +/- DAYS
	 *               WD= +/- WEEKS
	 *               MD= +/- MONTHS
	 *               YD= +/- YEARS
	 *               *B= ABOVE IN FULL WORD BINARY  (HOST ONLY)
	 *               THIS IS AN OUTPUT FIELD FOR DIFFERENCE CALULATIONS</pre>*/
	private String dateReturnCode = DefaultValues.stringVal(Len.DATE_RETURN_CODE);
	/**Original name: DATE-INPUT<br>
	 * <pre>              OK= GOOD NEWS
	 *               IF=INVALID FORMAT  (EDITING CHARACTERS)
	 *               ID=INVALID DATE    (>12 MONTHS,>31 DAYS)
	 *               IC=INVALID CODE    (DATE-XXX-FORMAT ERROR)
	 *               UC=UNSUPPORTED     (ON THIS PLATFORM)
	 *               OR=OUT OF RANGE FOR DIFFERENCE CALCULATIONS
	 *               XC=NOT IMPLEMENTED (YET)</pre>*/
	private String dateInput = DefaultValues.stringVal(Len.DATE_INPUT);
	/**Original name: DATE-OUTPUT<br>
	 * <pre>              DATE TO EDIT OR REFORMAT</pre>*/
	private String dateOutput = DefaultValues.stringVal(Len.DATE_OUTPUT);
	//Original name: DATE-ADJUST-SIGN
	private char dateAdjustSign = DefaultValues.CHAR_VAL;
	//Original name: DATE-ADJUST-NUMB
	private String dateAdjustNumb = DefaultValues.stringVal(Len.DATE_ADJUST_NUMB);
	/**Original name: DATE-DELIMITER-DATE<br>
	 * <pre>              ADD (OR SUBTRACT) THIS AMOUNT
	 *             DIFFERENCE FIELD FOR DIFFERENCE CALULATIONS</pre>*/
	private char dateDelimiterDate = DefaultValues.CHAR_VAL;
	/**Original name: DATE-DELIMITER-TIME<br>
	 * <pre>              / OR OTHER EDITING CHARACTER IN DATE
	 *               - ON WORKSTATION ONLY:
	 *               ? GET DELIMITER FROM OS/2 CONTROL PANEL</pre>*/
	private char dateDelimiterTime = DefaultValues.CHAR_VAL;
	/**Original name: DATE-AM-PM<br>
	 * <pre>              : OR OTHER EDITING CHARACTER IN TIME
	 *               - ON WORKSTATION ONLY:
	 *               ? GET DELIMITER FROM OS/2 CONTROL PANEL</pre>*/
	private String dateAmPm = DefaultValues.stringVal(Len.DATE_AM_PM);
	/**Original name: SCRATCH-AREA<br>
	 * <pre>              AMPM OR OTHER MORNING INDICATOR
	 *               SPACES FOR 24 HOUR CLOCK
	 *               - ON WORKSTATION ONLY:
	 *               ? GET INDICATOR FROM OS/2 CONTROL PANEL</pre>*/
	private ScratchArea scratchArea = new ScratchArea();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.XPIODAT_LINKAGE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setXpiodatLinkageBytes(buf);
	}

	public void setXpiodatLinkageBytes(byte[] buffer) {
		setXpiodatLinkageBytes(buffer, 1);
	}

	public byte[] getXpiodatLinkageBytes() {
		byte[] buffer = new byte[Len.XPIODAT_LINKAGE];
		return getXpiodatLinkageBytes(buffer, 1);
	}

	public void setXpiodatLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		dateFunction = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dateLanguage = MarshalByte.readString(buffer, position, Len.DATE_LANGUAGE);
		position += Len.DATE_LANGUAGE;
		dateInpFormat = MarshalByte.readString(buffer, position, Len.DATE_INP_FORMAT);
		position += Len.DATE_INP_FORMAT;
		dateOutFormat = MarshalByte.readString(buffer, position, Len.DATE_OUT_FORMAT);
		position += Len.DATE_OUT_FORMAT;
		dateAdjFormat = MarshalByte.readString(buffer, position, Len.DATE_ADJ_FORMAT);
		position += Len.DATE_ADJ_FORMAT;
		dateReturnCode = MarshalByte.readString(buffer, position, Len.DATE_RETURN_CODE);
		position += Len.DATE_RETURN_CODE;
		dateInput = MarshalByte.readString(buffer, position, Len.DATE_INPUT);
		position += Len.DATE_INPUT;
		dateOutput = MarshalByte.readString(buffer, position, Len.DATE_OUTPUT);
		position += Len.DATE_OUTPUT;
		setDateAdjustBytes(buffer, position);
		position += Len.DATE_ADJUST;
		dateDelimiterDate = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dateDelimiterTime = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dateAmPm = MarshalByte.readString(buffer, position, Len.DATE_AM_PM);
		position += Len.DATE_AM_PM;
		scratchArea.setScratchAreaFromBuffer(buffer, position);
	}

	public byte[] getXpiodatLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, dateFunction);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dateLanguage, Len.DATE_LANGUAGE);
		position += Len.DATE_LANGUAGE;
		MarshalByte.writeString(buffer, position, dateInpFormat, Len.DATE_INP_FORMAT);
		position += Len.DATE_INP_FORMAT;
		MarshalByte.writeString(buffer, position, dateOutFormat, Len.DATE_OUT_FORMAT);
		position += Len.DATE_OUT_FORMAT;
		MarshalByte.writeString(buffer, position, dateAdjFormat, Len.DATE_ADJ_FORMAT);
		position += Len.DATE_ADJ_FORMAT;
		MarshalByte.writeString(buffer, position, dateReturnCode, Len.DATE_RETURN_CODE);
		position += Len.DATE_RETURN_CODE;
		MarshalByte.writeString(buffer, position, dateInput, Len.DATE_INPUT);
		position += Len.DATE_INPUT;
		MarshalByte.writeString(buffer, position, dateOutput, Len.DATE_OUTPUT);
		position += Len.DATE_OUTPUT;
		getDateAdjustBytes(buffer, position);
		position += Len.DATE_ADJUST;
		MarshalByte.writeChar(buffer, position, dateDelimiterDate);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, dateDelimiterTime);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dateAmPm, Len.DATE_AM_PM);
		position += Len.DATE_AM_PM;
		scratchArea.getScratchAreaAsBuffer(buffer, position);
		return buffer;
	}

	public void setDateFunction(char dateFunction) {
		this.dateFunction = dateFunction;
	}

	public char getDateFunction() {
		return this.dateFunction;
	}

	public void setDateLanguage(String dateLanguage) {
		this.dateLanguage = Functions.subString(dateLanguage, Len.DATE_LANGUAGE);
	}

	public String getDateLanguage() {
		return this.dateLanguage;
	}

	public String getDateLanguageFormatted() {
		return Functions.padBlanks(getDateLanguage(), Len.DATE_LANGUAGE);
	}

	public String getDateBusinessIdFormatted() {
		return getDateLanguageFormatted();
	}

	/**Original name: DATE-BUSINESS-ID<br>
	 * <pre>              SPACES = ALWAYS</pre>*/
	public byte[] getDateBusinessIdBytes() {
		byte[] buffer = new byte[Len.DATE_BUSINESS_ID];
		return getDateBusinessIdBytes(buffer, 1);
	}

	public byte[] getDateBusinessIdBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, dateLanguage, Len.DATE_LANGUAGE);
		return buffer;
	}

	public void setDateInpFormat(String dateInpFormat) {
		this.dateInpFormat = Functions.subString(dateInpFormat, Len.DATE_INP_FORMAT);
	}

	public String getDateInpFormat() {
		return this.dateInpFormat;
	}

	public void setDateOutFormat(String dateOutFormat) {
		this.dateOutFormat = Functions.subString(dateOutFormat, Len.DATE_OUT_FORMAT);
	}

	public String getDateOutFormat() {
		return this.dateOutFormat;
	}

	public void setDateAdjFormat(String dateAdjFormat) {
		this.dateAdjFormat = Functions.subString(dateAdjFormat, Len.DATE_ADJ_FORMAT);
	}

	public String getDateAdjFormat() {
		return this.dateAdjFormat;
	}

	public void setDateReturnCode(String dateReturnCode) {
		this.dateReturnCode = Functions.subString(dateReturnCode, Len.DATE_RETURN_CODE);
	}

	public String getDateReturnCode() {
		return this.dateReturnCode;
	}

	public void setDateInput(String dateInput) {
		this.dateInput = Functions.subString(dateInput, Len.DATE_INPUT);
	}

	public String getDateInput() {
		return this.dateInput;
	}

	public String getDateInputFormatted() {
		return Functions.padBlanks(getDateInput(), Len.DATE_INPUT);
	}

	public void setDateOutput(String dateOutput) {
		this.dateOutput = Functions.subString(dateOutput, Len.DATE_OUTPUT);
	}

	public String getDateOutput() {
		return this.dateOutput;
	}

	public String getDateOutputFormatted() {
		return Functions.padBlanks(getDateOutput(), Len.DATE_OUTPUT);
	}

	/**Original name: DATE-ADJUST<br>
	 * <pre>              REFORMATTED DATE RETURNED
	 *               DATE FOR DIFFERENCE CALULATIONS</pre>*/
	public byte[] getDateAdjustBytes() {
		byte[] buffer = new byte[Len.DATE_ADJUST];
		return getDateAdjustBytes(buffer, 1);
	}

	public void setDateAdjustBytes(byte[] buffer, int offset) {
		int position = offset;
		dateAdjustSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dateAdjustNumb = MarshalByte.readFixedString(buffer, position, Len.DATE_ADJUST_NUMB);
	}

	public byte[] getDateAdjustBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, dateAdjustSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dateAdjustNumb, Len.DATE_ADJUST_NUMB);
		return buffer;
	}

	public void setDateAdjustSign(char dateAdjustSign) {
		this.dateAdjustSign = dateAdjustSign;
	}

	public char getDateAdjustSign() {
		return this.dateAdjustSign;
	}

	public void setDateAdjustNumb(int dateAdjustNumb) {
		this.dateAdjustNumb = NumericDisplay.asString(dateAdjustNumb, Len.DATE_ADJUST_NUMB);
	}

	public void setDateAdjustNumbFormatted(String dateAdjustNumb) {
		this.dateAdjustNumb = Trunc.toUnsignedNumeric(dateAdjustNumb, Len.DATE_ADJUST_NUMB);
	}

	public int getDateAdjustNumb() {
		return NumericDisplay.asInt(this.dateAdjustNumb);
	}

	public String getDateAdjustNumbFormatted() {
		return this.dateAdjustNumb;
	}

	public void setDateDelimiterDate(char dateDelimiterDate) {
		this.dateDelimiterDate = dateDelimiterDate;
	}

	public char getDateDelimiterDate() {
		return this.dateDelimiterDate;
	}

	public void setDateDelimiterTime(char dateDelimiterTime) {
		this.dateDelimiterTime = dateDelimiterTime;
	}

	public char getDateDelimiterTime() {
		return this.dateDelimiterTime;
	}

	public void setDateAmPm(String dateAmPm) {
		this.dateAmPm = Functions.subString(dateAmPm, Len.DATE_AM_PM);
	}

	public String getDateAmPm() {
		return this.dateAmPm;
	}

	public ScratchArea getScratchArea() {
		return scratchArea;
	}

	@Override
	public byte[] serialize() {
		return getXpiodatLinkageBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DATE_FUNCTION = 1;
		public static final int DATE_LANGUAGE = 3;
		public static final int DATE_INP_FORMAT = 2;
		public static final int DATE_OUT_FORMAT = 2;
		public static final int DATE_ADJ_FORMAT = 2;
		public static final int DATE_RETURN_CODE = 2;
		public static final int DATE_INPUT = 30;
		public static final int DATE_OUTPUT = 30;
		public static final int DATE_ADJUST_SIGN = 1;
		public static final int DATE_ADJUST_NUMB = 5;
		public static final int DATE_ADJUST = DATE_ADJUST_SIGN + DATE_ADJUST_NUMB;
		public static final int DATE_DELIMITER_DATE = 1;
		public static final int DATE_DELIMITER_TIME = 1;
		public static final int DATE_AM_PM = 4;
		public static final int XPIODAT_LINKAGE = DATE_FUNCTION + DATE_LANGUAGE + DATE_INP_FORMAT + DATE_OUT_FORMAT + DATE_ADJ_FORMAT
				+ DATE_RETURN_CODE + DATE_INPUT + DATE_OUTPUT + DATE_ADJUST + DATE_DELIMITER_DATE + DATE_DELIMITER_TIME + DATE_AM_PM
				+ ScratchArea.Len.SCRATCH_AREA;
		public static final int DATE_BUSINESS_ID = DATE_LANGUAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
