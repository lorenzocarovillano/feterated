/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-FD-REPORT-NMBR<br>
 * Variable: L-FD-REPORT-NMBR from program TS514099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LFdReportNmbr extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: FILLER-L-FD-REPORT-NMBR
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FD_REPORT_NMBR;
	}

	@Override
	public void deserialize(byte[] buf) {
		setlFdReportNmbrBytes(buf);
	}

	public String getlFdReportNmbrFormatted() {
		return getFlr1Formatted();
	}

	public void setlFdReportNmbrBytes(byte[] buffer) {
		setlFdReportNmbrBytes(buffer, 1);
	}

	public byte[] getlFdReportNmbrBytes() {
		byte[] buffer = new byte[Len.L_FD_REPORT_NMBR];
		return getlFdReportNmbrBytes(buffer, 1);
	}

	public void setlFdReportNmbrBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getlFdReportNmbrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr1Formatted() {
		return Functions.padBlanks(getFlr1(), Len.FLR1);
	}

	@Override
	public byte[] serialize() {
		return getlFdReportNmbrBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 8;
		public static final int L_FD_REPORT_NMBR = FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
