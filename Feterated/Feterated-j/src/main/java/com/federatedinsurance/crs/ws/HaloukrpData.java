/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallukrp;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsMoreKeyReplUmtRecsInd;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALOUKRP<br>
 * Generated as a class for rule WS.<br>*/
public class HaloukrpData {

	//==== PROPERTIES ====
	//Original name: WS-MORE-KEY-REPL-UMT-RECS-IND
	private WsMoreKeyReplUmtRecsInd wsMoreKeyReplUmtRecsInd = new WsMoreKeyReplUmtRecsInd();
	//Original name: WS-WORK-FIELDS
	private WsWorkFieldsHaloukrp wsWorkFields = new WsWorkFieldsHaloukrp();
	//Original name: WS-HALFUKRP-UBOC-MSG-ID
	private String wsHalfukrpUbocMsgId = DefaultValues.stringVal(Len.WS_HALFUKRP_UBOC_MSG_ID);
	//Original name: WS-HALFUKRP-BUS-OBJ-NM
	private String wsHalfukrpBusObjNm = DefaultValues.stringVal(Len.WS_HALFUKRP_BUS_OBJ_NM);
	/**Original name: WS-HALFUKRP-KEY-REPL-LABEL<br>
	 * <pre>      07  WS-HALFUKRP-KEY-REPL-LABEL PIC X(32).</pre>*/
	private String wsHalfukrpKeyReplLabel = DefaultValues.stringVal(Len.WS_HALFUKRP_KEY_REPL_LABEL);
	//Original name: WS-HALFUKRP-KEY-REPL-KEY-LEN
	private int wsHalfukrpKeyReplKeyLen = DefaultValues.BIN_INT_VAL;
	//Original name: WS-HALFUKRP-KEY-REPL-KEY
	private String wsHalfukrpKeyReplKey = DefaultValues.stringVal(Len.WS_HALFUKRP_KEY_REPL_KEY);
	//Original name: HALLUKRP
	private Hallukrp hallukrp = new Hallukrp();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void setWsHalfukrpRecBytes(byte[] buffer) {
		setWsHalfukrpRecBytes(buffer, 1);
	}

	/**Original name: WS-HALFUKRP-REC<br>
	 * <pre> KEY REPLACEMENT UMT RECORD</pre>*/
	public byte[] getWsHalfukrpRecBytes() {
		byte[] buffer = new byte[Len.WS_HALFUKRP_REC];
		return getWsHalfukrpRecBytes(buffer, 1);
	}

	public void setWsHalfukrpRecBytes(byte[] buffer, int offset) {
		int position = offset;
		setWsHalfukrpKeyBytes(buffer, position);
		position += Len.WS_HALFUKRP_KEY;
		setWsHalfukrpDataBytes(buffer, position);
	}

	public byte[] getWsHalfukrpRecBytes(byte[] buffer, int offset) {
		int position = offset;
		getWsHalfukrpKeyBytes(buffer, position);
		position += Len.WS_HALFUKRP_KEY;
		getWsHalfukrpDataBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-HALFUKRP-KEY<br>*/
	public byte[] getWsHalfukrpKeyBytes() {
		byte[] buffer = new byte[Len.WS_HALFUKRP_KEY];
		return getWsHalfukrpKeyBytes(buffer, 1);
	}

	public void setWsHalfukrpKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		wsHalfukrpUbocMsgId = MarshalByte.readString(buffer, position, Len.WS_HALFUKRP_UBOC_MSG_ID);
		position += Len.WS_HALFUKRP_UBOC_MSG_ID;
		wsHalfukrpBusObjNm = MarshalByte.readString(buffer, position, Len.WS_HALFUKRP_BUS_OBJ_NM);
		position += Len.WS_HALFUKRP_BUS_OBJ_NM;
		wsHalfukrpKeyReplLabel = MarshalByte.readString(buffer, position, Len.WS_HALFUKRP_KEY_REPL_LABEL);
	}

	public byte[] getWsHalfukrpKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, wsHalfukrpUbocMsgId, Len.WS_HALFUKRP_UBOC_MSG_ID);
		position += Len.WS_HALFUKRP_UBOC_MSG_ID;
		MarshalByte.writeString(buffer, position, wsHalfukrpBusObjNm, Len.WS_HALFUKRP_BUS_OBJ_NM);
		position += Len.WS_HALFUKRP_BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, wsHalfukrpKeyReplLabel, Len.WS_HALFUKRP_KEY_REPL_LABEL);
		return buffer;
	}

	public void setWsHalfukrpUbocMsgId(String wsHalfukrpUbocMsgId) {
		this.wsHalfukrpUbocMsgId = Functions.subString(wsHalfukrpUbocMsgId, Len.WS_HALFUKRP_UBOC_MSG_ID);
	}

	public String getWsHalfukrpUbocMsgId() {
		return this.wsHalfukrpUbocMsgId;
	}

	public String getWsHalfukrpUbocMsgIdFormatted() {
		return Functions.padBlanks(getWsHalfukrpUbocMsgId(), Len.WS_HALFUKRP_UBOC_MSG_ID);
	}

	public void setWsHalfukrpBusObjNm(String wsHalfukrpBusObjNm) {
		this.wsHalfukrpBusObjNm = Functions.subString(wsHalfukrpBusObjNm, Len.WS_HALFUKRP_BUS_OBJ_NM);
	}

	public String getWsHalfukrpBusObjNm() {
		return this.wsHalfukrpBusObjNm;
	}

	public String getWsHalfukrpBusObjNmFormatted() {
		return Functions.padBlanks(getWsHalfukrpBusObjNm(), Len.WS_HALFUKRP_BUS_OBJ_NM);
	}

	public void setWsHalfukrpKeyReplLabel(String wsHalfukrpKeyReplLabel) {
		this.wsHalfukrpKeyReplLabel = Functions.subString(wsHalfukrpKeyReplLabel, Len.WS_HALFUKRP_KEY_REPL_LABEL);
	}

	public String getWsHalfukrpKeyReplLabel() {
		return this.wsHalfukrpKeyReplLabel;
	}

	public String getWsHalfukrpKeyReplLabelFormatted() {
		return Functions.padBlanks(getWsHalfukrpKeyReplLabel(), Len.WS_HALFUKRP_KEY_REPL_LABEL);
	}

	public void setWsHalfukrpDataBytes(byte[] buffer, int offset) {
		int position = offset;
		wsHalfukrpKeyReplKeyLen = MarshalByte.readBinaryUnsignedShort(buffer, position);
		position += Types.SHORT_SIZE;
		wsHalfukrpKeyReplKey = MarshalByte.readString(buffer, position, Len.WS_HALFUKRP_KEY_REPL_KEY);
	}

	public byte[] getWsHalfukrpDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryUnsignedShort(buffer, position, wsHalfukrpKeyReplKeyLen);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, wsHalfukrpKeyReplKey, Len.WS_HALFUKRP_KEY_REPL_KEY);
		return buffer;
	}

	public void setWsHalfukrpKeyReplKeyLen(int wsHalfukrpKeyReplKeyLen) {
		this.wsHalfukrpKeyReplKeyLen = wsHalfukrpKeyReplKeyLen;
	}

	public int getWsHalfukrpKeyReplKeyLen() {
		return this.wsHalfukrpKeyReplKeyLen;
	}

	public void setWsHalfukrpKeyReplKey(String wsHalfukrpKeyReplKey) {
		this.wsHalfukrpKeyReplKey = Functions.subString(wsHalfukrpKeyReplKey, Len.WS_HALFUKRP_KEY_REPL_KEY);
	}

	public String getWsHalfukrpKeyReplKey() {
		return this.wsHalfukrpKeyReplKey;
	}

	public void setHaloukrpLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.HALOUKRP_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.HALOUKRP_LINKAGE);
		setHaloukrpLinkageBytes(buffer, 1);
	}

	public String getHaloukrpLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHaloukrpLinkageBytes());
	}

	/**Original name: HALOUKRP-LINKAGE<br>
	 * <pre> HALOUKRP LINKAGE</pre>*/
	public byte[] getHaloukrpLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUKRP_LINKAGE];
		return getHaloukrpLinkageBytes(buffer, 1);
	}

	public void setHaloukrpLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallukrp.setInputFieldsBytes(buffer, position);
		position += Hallukrp.Len.INPUT_FIELDS;
		hallukrp.setInputOutputFieldsBytes(buffer, position);
	}

	public byte[] getHaloukrpLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallukrp.getInputFieldsBytes(buffer, position);
		position += Hallukrp.Len.INPUT_FIELDS;
		hallukrp.getInputOutputFieldsBytes(buffer, position);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallukrp getHallukrp() {
		return hallukrp;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsMoreKeyReplUmtRecsInd getWsMoreKeyReplUmtRecsInd() {
		return wsMoreKeyReplUmtRecsInd;
	}

	public WsWorkFieldsHaloukrp getWsWorkFields() {
		return wsWorkFields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_HALFUKRP_UBOC_MSG_ID = 32;
		public static final int WS_HALFUKRP_BUS_OBJ_NM = 32;
		public static final int WS_HALFUKRP_KEY_REPL_LABEL = 150;
		public static final int WS_HALFUKRP_KEY_REPL_KEY = 40;
		public static final int WS_APPLID = 8;
		public static final int HALOUKRP_LINKAGE = Hallukrp.Len.INPUT_FIELDS + Hallukrp.Len.INPUT_OUTPUT_FIELDS;
		public static final int WS_HALFUKRP_KEY = WS_HALFUKRP_UBOC_MSG_ID + WS_HALFUKRP_BUS_OBJ_NM + WS_HALFUKRP_KEY_REPL_LABEL;
		public static final int WS_HALFUKRP_KEY_REPL_KEY_LEN = 2;
		public static final int WS_HALFUKRP_DATA = WS_HALFUKRP_KEY_REPL_KEY_LEN + WS_HALFUKRP_KEY_REPL_KEY;
		public static final int WS_HALFUKRP_REC = WS_HALFUKRP_KEY + WS_HALFUKRP_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
