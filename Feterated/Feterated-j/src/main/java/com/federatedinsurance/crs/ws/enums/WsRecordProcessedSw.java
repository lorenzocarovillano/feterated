/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-RECORD-PROCESSED-SW<br>
 * Variable: WS-RECORD-PROCESSED-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsRecordProcessedSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char PROCESSED = 'Y';
	public static final char NOT_PROCESSED = 'N';

	//==== METHODS ====
	public void setWsRecordProcessedSw(char wsRecordProcessedSw) {
		this.value = wsRecordProcessedSw;
	}

	public char getWsRecordProcessedSw() {
		return this.value;
	}

	public boolean isProcessed() {
		return value == PROCESSED;
	}

	public void setProcessed() {
		value = PROCESSED;
	}

	public boolean isNotProcessed() {
		return value == NOT_PROCESSED;
	}

	public void setNotProcessed() {
		value = NOT_PROCESSED;
	}
}
