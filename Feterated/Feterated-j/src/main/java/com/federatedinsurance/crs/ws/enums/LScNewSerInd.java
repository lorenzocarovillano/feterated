/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: L-SC-NEW-SER-IND<br>
 * Variable: L-SC-NEW-SER-IND from copybook TT008001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LScNewSerInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NEW_SER = 'Y';
	public static final char NOT_NEW_SER = 'N';

	//==== METHODS ====
	public void setNewSerInd(char newSerInd) {
		this.value = newSerInd;
	}

	public char getNewSerInd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NEW_SER_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
