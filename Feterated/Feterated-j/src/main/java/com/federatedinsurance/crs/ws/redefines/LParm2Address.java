/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-PARM2-ADDRESS<br>
 * Variable: L-PARM2-ADDRESS from program TS030099<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LParm2Address extends BytesClass {

	//==== CONSTRUCTORS ====
	public LParm2Address() {
	}

	public LParm2Address(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_PARM2_ADDRESS;
	}

	public void setlParm2Address(int lParm2Address) {
		writeBinaryInt(Pos.L_PARM2_ADDRESS, lParm2Address);
	}

	/**Original name: L-PARM2-ADDRESS<br>*/
	public int getlParm2Address() {
		return readBinaryInt(Pos.L_PARM2_ADDRESS);
	}

	public int getlParm2Pointer() {
		return readBinaryInt(Pos.L_PARM2_POINTER);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_PARM2_ADDRESS = 1;
		public static final int L_PARM2_POINTER = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int L_PARM2_ADDRESS = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
