/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-CALL-ADD-ACT-NOT-WRD-SVC-FG<br>
 * Variable: SW-CALL-ADD-ACT-NOT-WRD-SVC-FG from program XZ0P9000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwCallAddActNotWrdSvcFg {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CALL_ADD_ACT_NOT_WRD_SVC = 'Y';
	public static final char NO_CALL_ADD_ACT_NOT_WRD_SVC = 'N';

	//==== METHODS ====
	public void setCallAddActNotWrdSvcFg(char callAddActNotWrdSvcFg) {
		this.value = callAddActNotWrdSvcFg;
	}

	public char getCallAddActNotWrdSvcFg() {
		return this.value;
	}

	public boolean isCallAddActNotWrdSvc() {
		return value == CALL_ADD_ACT_NOT_WRD_SVC;
	}

	public void setCallAddActNotWrdSvc() {
		value = CALL_ADD_ACT_NOT_WRD_SVC;
	}

	public void setNoCallAddActNotWrdSvc() {
		value = NO_CALL_ADD_ACT_NOT_WRD_SVC;
	}
}
