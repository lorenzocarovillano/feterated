/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CWEMF-CLIENT-EMAIL-ROW<br>
 * Variable: CWEMF-CLIENT-EMAIL-ROW from copybook CAWLF0EM<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class CwemfClientEmailRow {

	//==== PROPERTIES ====
	//Original name: CWEMF-CLIENT-EMAIL-CSUM
	private String clientEmailCsum = DefaultValues.stringVal(Len.CLIENT_EMAIL_CSUM);
	//Original name: CWEMF-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CWEMF-CIEM-SEQ-NBR-KCRE
	private String ciemSeqNbrKcre = DefaultValues.stringVal(Len.CIEM_SEQ_NBR_KCRE);
	//Original name: CWEMF-HISTORY-VLD-NBR-KCRE
	private String historyVldNbrKcre = DefaultValues.stringVal(Len.HISTORY_VLD_NBR_KCRE);
	//Original name: CWEMF-EFFECTIVE-DT-KCRE
	private String effectiveDtKcre = DefaultValues.stringVal(Len.EFFECTIVE_DT_KCRE);
	//Original name: CWEMF-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CWEMF-CLIENT-EMAIL-KEY
	private Cw01fBusinessClientKey clientEmailKey = new Cw01fBusinessClientKey();
	//Original name: CWEMF-CLIENT-EMAIL-DATA
	private CwemfClientEmailData clientEmailData = new CwemfClientEmailData();

	//==== METHODS ====
	public void setCwemfClientEmailRowFormatted(String data) {
		byte[] buffer = new byte[Len.CWEMF_CLIENT_EMAIL_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CWEMF_CLIENT_EMAIL_ROW);
		setCwemfClientEmailRowBytes(buffer, 1);
	}

	public String getCwemfClientEmailRowFormatted() {
		return MarshalByteExt.bufferToStr(getCwemfClientEmailRowBytes());
	}

	public byte[] getCwemfClientEmailRowBytes() {
		byte[] buffer = new byte[Len.CWEMF_CLIENT_EMAIL_ROW];
		return getCwemfClientEmailRowBytes(buffer, 1);
	}

	public void setCwemfClientEmailRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setClientEmailFixedBytes(buffer, position);
		position += Len.CLIENT_EMAIL_FIXED;
		setClientEmailDatesBytes(buffer, position);
		position += Len.CLIENT_EMAIL_DATES;
		clientEmailKey.setBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		clientEmailData.setClientEmailDataBytes(buffer, position);
	}

	public byte[] getCwemfClientEmailRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getClientEmailFixedBytes(buffer, position);
		position += Len.CLIENT_EMAIL_FIXED;
		getClientEmailDatesBytes(buffer, position);
		position += Len.CLIENT_EMAIL_DATES;
		clientEmailKey.getBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		clientEmailData.getClientEmailDataBytes(buffer, position);
		return buffer;
	}

	public void setClientEmailFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		clientEmailCsum = MarshalByte.readFixedString(buffer, position, Len.CLIENT_EMAIL_CSUM);
		position += Len.CLIENT_EMAIL_CSUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		ciemSeqNbrKcre = MarshalByte.readString(buffer, position, Len.CIEM_SEQ_NBR_KCRE);
		position += Len.CIEM_SEQ_NBR_KCRE;
		historyVldNbrKcre = MarshalByte.readString(buffer, position, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		effectiveDtKcre = MarshalByte.readString(buffer, position, Len.EFFECTIVE_DT_KCRE);
	}

	public byte[] getClientEmailFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientEmailCsum, Len.CLIENT_EMAIL_CSUM);
		position += Len.CLIENT_EMAIL_CSUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		MarshalByte.writeString(buffer, position, ciemSeqNbrKcre, Len.CIEM_SEQ_NBR_KCRE);
		position += Len.CIEM_SEQ_NBR_KCRE;
		MarshalByte.writeString(buffer, position, historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		MarshalByte.writeString(buffer, position, effectiveDtKcre, Len.EFFECTIVE_DT_KCRE);
		return buffer;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setCiemSeqNbrKcre(String ciemSeqNbrKcre) {
		this.ciemSeqNbrKcre = Functions.subString(ciemSeqNbrKcre, Len.CIEM_SEQ_NBR_KCRE);
	}

	public String getCiemSeqNbrKcre() {
		return this.ciemSeqNbrKcre;
	}

	public void setHistoryVldNbrKcre(String historyVldNbrKcre) {
		this.historyVldNbrKcre = Functions.subString(historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
	}

	public String getHistoryVldNbrKcre() {
		return this.historyVldNbrKcre;
	}

	public void setEffectiveDtKcre(String effectiveDtKcre) {
		this.effectiveDtKcre = Functions.subString(effectiveDtKcre, Len.EFFECTIVE_DT_KCRE);
	}

	public String getEffectiveDtKcre() {
		return this.effectiveDtKcre;
	}

	public void setClientEmailDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getClientEmailDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_EMAIL_CSUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int CIEM_SEQ_NBR_KCRE = 32;
		public static final int HISTORY_VLD_NBR_KCRE = 32;
		public static final int EFFECTIVE_DT_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLIENT_EMAIL_FIXED = CLIENT_EMAIL_CSUM + CLIENT_ID_KCRE + CIEM_SEQ_NBR_KCRE + HISTORY_VLD_NBR_KCRE
				+ EFFECTIVE_DT_KCRE;
		public static final int CLIENT_EMAIL_DATES = TRANS_PROCESS_DT;
		public static final int CWEMF_CLIENT_EMAIL_ROW = CLIENT_EMAIL_FIXED + CLIENT_EMAIL_DATES + Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY
				+ CwemfClientEmailData.Len.CLIENT_EMAIL_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
