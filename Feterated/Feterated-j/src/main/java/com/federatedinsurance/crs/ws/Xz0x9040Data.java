/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0X9040<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0x9040Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0x9040 constantFields = new ConstantFieldsXz0x9040();
	//Original name: SS-FL
	private short ssFl = DefaultValues.BIN_SHORT_VAL;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0x9040 workingStorageArea = new WorkingStorageAreaXz0x9040();
	//Original name: MAIN-DRIVER-DATA
	private DfhcommareaTs020000 mainDriverData = new DfhcommareaTs020000();
	//Original name: IX-RS
	private int ixRs = 1;

	//==== METHODS ====
	public void setSsFl(short ssFl) {
		this.ssFl = ssFl;
	}

	public short getSsFl() {
		return this.ssFl;
	}

	public void setIxRs(int ixRs) {
		this.ixRs = ixRs;
	}

	public int getIxRs() {
		return this.ixRs;
	}

	public ConstantFieldsXz0x9040 getConstantFields() {
		return constantFields;
	}

	public DfhcommareaTs020000 getMainDriverData() {
		return mainDriverData;
	}

	public WorkingStorageAreaXz0x9040 getWorkingStorageArea() {
		return workingStorageArea;
	}
}
