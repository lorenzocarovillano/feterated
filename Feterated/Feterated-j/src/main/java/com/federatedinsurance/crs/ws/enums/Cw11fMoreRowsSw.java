/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: CW11F-MORE-ROWS-SW<br>
 * Variable: CW11F-MORE-ROWS-SW from copybook CAWLF011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Cw11fMoreRowsSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char MORE_THAN_ONE_ROW = 'Y';
	public static final char NOT_MORE_THAN_ONE_ROW = 'N';

	//==== METHODS ====
	public void setMoreRowsSw(char moreRowsSw) {
		this.value = moreRowsSw;
	}

	public char getMoreRowsSw() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MORE_ROWS_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
