/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXzc02090 {

	//==== PROPERTIES ====
	//Original name: EA-01-INVALID-POLICY-TERM
	private Ea01InvalidPolicyTerm ea01InvalidPolicyTerm = new Ea01InvalidPolicyTerm();
	//Original name: EA-02-INVALID-POL-EFF-DT
	private Ea02InvalidPolEffDt ea02InvalidPolEffDt = new Ea02InvalidPolEffDt();
	//Original name: EA-03-INVALID-POL-EXP-DT
	private Ea03InvalidPolExpDt ea03InvalidPolExpDt = new Ea03InvalidPolExpDt();
	//Original name: EA-04-INVALID-POLICY
	private Ea04InvalidPolicy ea04InvalidPolicy = new Ea04InvalidPolicy();
	//Original name: EA-10-DB2-ERROR-ON-SELECT
	private Ea10Db2ErrorOnSelectXzc02090 ea10Db2ErrorOnSelect = new Ea10Db2ErrorOnSelectXzc02090();
	//Original name: EA-20-CICS-LINK-ERROR
	private Ea20CicsLinkError ea20CicsLinkError = new Ea20CicsLinkError();
	//Original name: EA-21-PEOPLE-SEARCH-ERROR
	private Ea21PeopleSearchError ea21PeopleSearchError = new Ea21PeopleSearchError();

	//==== METHODS ====
	public Ea01InvalidPolicyTerm getEa01InvalidPolicyTerm() {
		return ea01InvalidPolicyTerm;
	}

	public Ea02InvalidPolEffDt getEa02InvalidPolEffDt() {
		return ea02InvalidPolEffDt;
	}

	public Ea03InvalidPolExpDt getEa03InvalidPolExpDt() {
		return ea03InvalidPolExpDt;
	}

	public Ea04InvalidPolicy getEa04InvalidPolicy() {
		return ea04InvalidPolicy;
	}

	public Ea10Db2ErrorOnSelectXzc02090 getEa10Db2ErrorOnSelect() {
		return ea10Db2ErrorOnSelect;
	}

	public Ea20CicsLinkError getEa20CicsLinkError() {
		return ea20CicsLinkError;
	}

	public Ea21PeopleSearchError getEa21PeopleSearchError() {
		return ea21PeopleSearchError;
	}
}
