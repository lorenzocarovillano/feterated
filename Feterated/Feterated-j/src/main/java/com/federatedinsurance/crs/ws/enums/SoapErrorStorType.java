/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SOAP-ERROR-STOR-TYPE<br>
 * Variable: SOAP-ERROR-STOR-TYPE from copybook TS570CB1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SoapErrorStorType {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.ERROR_STOR_TYPE);
	public static final String CAREA = "CO";
	public static final String TSQ = "TS";
	public static final String TDQ = "TD";
	public static final String IVORYFILE = "IF";

	//==== METHODS ====
	public void setErrorStorType(String errorStorType) {
		this.value = Functions.subString(errorStorType, Len.ERROR_STOR_TYPE);
	}

	public String getErrorStorType() {
		return this.value;
	}

	public void setSoapErrorStorTypeCarea() {
		value = CAREA;
	}

	public void setTdq() {
		value = TDQ;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_STOR_TYPE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
