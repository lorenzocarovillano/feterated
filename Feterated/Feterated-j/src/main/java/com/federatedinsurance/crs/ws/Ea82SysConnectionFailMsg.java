/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-82-SYS-CONNECTION-FAIL-MSG<br>
 * Variable: EA-82-SYS-CONNECTION-FAIL-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea82SysConnectionFailMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-82-SYS-CONNECTION-FAIL-MSG
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-82-SYS-CONNECTION-FAIL-MSG-1
	private String flr2 = "AN ATTEMPT TO";
	//Original name: FILLER-EA-82-SYS-CONNECTION-FAIL-MSG-2
	private String flr3 = "CONNECT TO THE";
	//Original name: FILLER-EA-82-SYS-CONNECTION-FAIL-MSG-3
	private String flr4 = "TARGET CICS";
	//Original name: FILLER-EA-82-SYS-CONNECTION-FAIL-MSG-4
	private String flr5 = "SYSTEM FAILED.";
	//Original name: FILLER-EA-82-SYS-CONNECTION-FAIL-MSG-5
	private String flr6 = "REGIONS TRIED:";
	//Original name: EA-82-REGIONS-ATTEMPTED
	private String ea82RegionsAttempted = DefaultValues.stringVal(Len.EA82_REGIONS_ATTEMPTED);

	//==== METHODS ====
	public String getEa82SysConnectionFailMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa82SysConnectionFailMsgBytes());
	}

	public byte[] getEa82SysConnectionFailMsgBytes() {
		byte[] buffer = new byte[Len.EA82_SYS_CONNECTION_FAIL_MSG];
		return getEa82SysConnectionFailMsgBytes(buffer, 1);
	}

	public byte[] getEa82SysConnectionFailMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea82RegionsAttempted, Len.EA82_REGIONS_ATTEMPTED);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setEa82RegionsAttempted(String ea82RegionsAttempted) {
		this.ea82RegionsAttempted = Functions.subString(ea82RegionsAttempted, Len.EA82_REGIONS_ATTEMPTED);
	}

	public String getEa82RegionsAttempted() {
		return this.ea82RegionsAttempted;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA82_REGIONS_ATTEMPTED = 30;
		public static final int FLR1 = 11;
		public static final int FLR2 = 14;
		public static final int FLR3 = 15;
		public static final int FLR4 = 12;
		public static final int FLR5 = 16;
		public static final int EA82_SYS_CONNECTION_FAIL_MSG = EA82_REGIONS_ATTEMPTED + FLR1 + FLR2 + 2 * FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
