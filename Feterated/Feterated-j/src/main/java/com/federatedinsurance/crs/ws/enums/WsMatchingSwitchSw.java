/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-MATCHING-SWITCH-SW<br>
 * Variable: WS-MATCHING-SWITCH-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMatchingSwitchSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FOUND = 'Y';
	public static final char NOT_FOUND = 'N';

	//==== METHODS ====
	public void setMatchingSwitchSw(char matchingSwitchSw) {
		this.value = matchingSwitchSw;
	}

	public char getMatchingSwitchSw() {
		return this.value;
	}

	public boolean isFound() {
		return value == FOUND;
	}

	public void setFound() {
		value = FOUND;
	}

	public void setNotFound() {
		value = NOT_FOUND;
	}
}
