/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-FIRST-FETCH-SW<br>
 * Variable: WS-FIRST-FETCH-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFirstFetchSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char THE_FIRST_FETCH = 'Y';
	public static final char NOT_THE_FIRST_FETCH = 'N';

	//==== METHODS ====
	public void setFirstFetchSw(char firstFetchSw) {
		this.value = firstFetchSw;
	}

	public char getFirstFetchSw() {
		return this.value;
	}

	public boolean isWsTheFirstFetch() {
		return value == THE_FIRST_FETCH;
	}

	public void setTheFirstFetch() {
		value = THE_FIRST_FETCH;
	}

	public void setNotTheFirstFetch() {
		value = NOT_THE_FIRST_FETCH;
	}
}
