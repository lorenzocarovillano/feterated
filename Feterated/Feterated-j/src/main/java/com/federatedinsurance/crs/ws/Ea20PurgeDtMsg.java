/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-20-PURGE-DT-MSG<br>
 * Variable: EA-20-PURGE-DT-MSG from program XZ400000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea20PurgeDtMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-20-PURGE-DT-MSG
	private String flr1 = "XZ400000";
	//Original name: FILLER-EA-20-PURGE-DT-MSG-1
	private String flr2 = "COPY-PGE-DT";
	//Original name: FILLER-EA-20-PURGE-DT-MSG-2
	private String flr3 = "VALUE =";
	//Original name: EA-20-COPY-PGE-DT
	private String ea20CopyPgeDt = DefaultValues.stringVal(Len.EA20_COPY_PGE_DT);

	//==== METHODS ====
	public String getEa20PurgeDtMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa20PurgeDtMsgBytes());
	}

	public byte[] getEa20PurgeDtMsgBytes() {
		byte[] buffer = new byte[Len.EA20_PURGE_DT_MSG];
		return getEa20PurgeDtMsgBytes(buffer, 1);
	}

	public byte[] getEa20PurgeDtMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea20CopyPgeDt, Len.EA20_COPY_PGE_DT);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEa20CopyPgeDt(String ea20CopyPgeDt) {
		this.ea20CopyPgeDt = Functions.subString(ea20CopyPgeDt, Len.EA20_COPY_PGE_DT);
	}

	public String getEa20CopyPgeDt() {
		return this.ea20CopyPgeDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA20_COPY_PGE_DT = 10;
		public static final int FLR1 = 9;
		public static final int FLR2 = 12;
		public static final int FLR3 = 8;
		public static final int EA20_PURGE_DT_MSG = EA20_COPY_PGE_DT + FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
