/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-NOTHING-FOUND-MSG<br>
 * Variable: EA-02-NOTHING-FOUND-MSG from program XZ0B9080<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02NothingFoundMsgXz0b9080 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG
	private String flr1 = "No third";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-1
	private String flr2 = "parties";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-2
	private String flr3 = "found for";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-3
	private String flr4 = "search criteria";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-4
	private String flr5 = " entered:";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-5
	private String flr6 = "Account =";
	//Original name: EA-02-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-6
	private String flr7 = "; Notification";
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-7
	private String flr8 = "TimeStamp =";
	//Original name: EA-02-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-02-NOTHING-FOUND-MSG-8
	private char flr9 = '.';

	//==== METHODS ====
	public String getEa02NothingFoundMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02NothingFoundMsgBytes());
	}

	public byte[] getEa02NothingFoundMsgBytes() {
		byte[] buffer = new byte[Len.EA02_NOTHING_FOUND_MSG];
		return getEa02NothingFoundMsgBytes(buffer, 1);
	}

	public byte[] getEa02NothingFoundMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		position += Len.FLR8;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr9);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr9() {
		return this.flr9;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 9;
		public static final int FLR2 = 8;
		public static final int FLR3 = 10;
		public static final int FLR4 = 15;
		public static final int FLR8 = 12;
		public static final int FLR9 = 1;
		public static final int EA02_NOTHING_FOUND_MSG = CSR_ACT_NBR + NOT_PRC_TS + FLR1 + FLR2 + 3 * FLR3 + 2 * FLR4 + FLR8 + FLR9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
