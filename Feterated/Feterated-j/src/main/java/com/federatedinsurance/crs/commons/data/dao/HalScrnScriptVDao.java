/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalScrnScriptV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_SCRN_SCRIPT_V]
 * 
 */
public class HalScrnScriptVDao extends BaseSqlDao<IHalScrnScriptV> {

	private Cursor scriptcur;
	private final IRowMapper<IHalScrnScriptV> fetchScriptcurRm = buildNamedRowMapper(IHalScrnScriptV.class, "uowNm", "secGrpNm", "hssvContext",
			"hssvScrnFldId", "hssvSeqNbr", "hssvNewLinInd", "hssvScriptTxt");

	public HalScrnScriptVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalScrnScriptV> getToClass() {
		return IHalScrnScriptV.class;
	}

	public DbAccessStatus openScriptcur(String hssvhUowNm, String hssvhSecGrpNm, String wsFullContext, String wsContextFirst10ForCsr) {
		scriptcur = buildQuery("openScriptcur").bind("hssvhUowNm", hssvhUowNm).bind("hssvhSecGrpNm", hssvhSecGrpNm)
				.bind("wsFullContext", wsFullContext).bind("wsContextFirst10ForCsr", wsContextFirst10ForCsr).open();
		return dbStatus;
	}

	public DbAccessStatus closeScriptcur() {
		return closeCursor(scriptcur);
	}

	public IHalScrnScriptV fetchScriptcur(IHalScrnScriptV iHalScrnScriptV) {
		return fetch(scriptcur, iHalScrnScriptV, fetchScriptcurRm);
	}
}
