/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IXz0p0021Generic;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [XZ0P0021_Generic]
 * 
 */
public class Xz0p0021GenericDao extends BaseSqlDao<IXz0p0021Generic> {

	public Xz0p0021GenericDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IXz0p0021Generic> getToClass() {
		return IXz0p0021Generic.class;
	}

	public String setHostVarRec(String polExpDt, short notDayRqr, String dft) {
		return buildQuery("setHostVarRec").bind("polExpDt", polExpDt).bind("notDayRqr", notDayRqr).scalarResultString(dft);
	}

	public String setHostVarByWsCptNotDt(String wsCptNotDt, String dft) {
		return buildQuery("setHostVarByWsCptNotDt").bind("wsCptNotDt", wsCptNotDt).scalarResultString(dft);
	}

	public String setHostVarByWsCptNotDt1(String wsCptNotDt, String dft) {
		return buildQuery("setHostVarByWsCptNotDt1").bind("wsCptNotDt", wsCptNotDt).scalarResultString(dft);
	}
}
