/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DUMP-PARAMETERS<br>
 * Variable: DUMP-PARAMETERS from program XZ001000<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DumpParameters extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: DP-DUMP-TITLE
	private String dumpTitle = "XZ001000 DUMP";
	//Original name: DP-OPTIONS
	private String options = "";
	//Original name: DP-FEEDBACK-CODE
	private String feedbackCode = DefaultValues.stringVal(Len.FEEDBACK_CODE);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DUMP_PARAMETERS;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDumpParametersBytes(buf);
	}

	public String getDumpParametersFormatted() {
		return MarshalByteExt.bufferToStr(getDumpParametersBytes());
	}

	public void setDumpParametersBytes(byte[] buffer) {
		setDumpParametersBytes(buffer, 1);
	}

	public byte[] getDumpParametersBytes() {
		byte[] buffer = new byte[Len.DUMP_PARAMETERS];
		return getDumpParametersBytes(buffer, 1);
	}

	public void setDumpParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		dumpTitle = MarshalByte.readString(buffer, position, Len.DUMP_TITLE);
		position += Len.DUMP_TITLE;
		options = MarshalByte.readString(buffer, position, Len.OPTIONS);
		position += Len.OPTIONS;
		feedbackCode = MarshalByte.readString(buffer, position, Len.FEEDBACK_CODE);
	}

	public byte[] getDumpParametersBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, dumpTitle, Len.DUMP_TITLE);
		position += Len.DUMP_TITLE;
		MarshalByte.writeString(buffer, position, options, Len.OPTIONS);
		position += Len.OPTIONS;
		MarshalByte.writeString(buffer, position, feedbackCode, Len.FEEDBACK_CODE);
		return buffer;
	}

	public void setDumpTitle(String dumpTitle) {
		this.dumpTitle = Functions.subString(dumpTitle, Len.DUMP_TITLE);
	}

	public String getDumpTitle() {
		return this.dumpTitle;
	}

	public void setOptions(String options) {
		this.options = Functions.subString(options, Len.OPTIONS);
	}

	public String getOptions() {
		return this.options;
	}

	public String getOptionsFormatted() {
		return Functions.padBlanks(getOptions(), Len.OPTIONS);
	}

	public void setFeedbackCode(String feedbackCode) {
		this.feedbackCode = Functions.subString(feedbackCode, Len.FEEDBACK_CODE);
	}

	public String getFeedbackCode() {
		return this.feedbackCode;
	}

	public String getFeedbackCodeFormatted() {
		return Functions.padBlanks(getFeedbackCode(), Len.FEEDBACK_CODE);
	}

	@Override
	public byte[] serialize() {
		return getDumpParametersBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DUMP_TITLE = 80;
		public static final int OPTIONS = 255;
		public static final int FEEDBACK_CODE = 12;
		public static final int DUMP_PARAMETERS = DUMP_TITLE + OPTIONS + FEEDBACK_CODE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
