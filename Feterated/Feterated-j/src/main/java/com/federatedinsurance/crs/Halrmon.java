/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.LHallrmonData;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;

/**Original name: HALRMON<br>
 * <pre>AUTHOR.
 *                   A.ABRAMS
 * DATE-WRITTEN.
 *                   JAN 2001.
 * DATE-COMPILED.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  SHELL PROGRAM TO BE REPLACED BY ACTUAL     **
 * *                  PROGRAM AT A LATER DATE.                   **
 * *                                                             **
 * * PLATFORM - IBM MAINFRAME                                    **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -        ALLOW TESTING OF CALLING BDO.              **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED AS FOLLOWS:   **
 * *                       1) STATICALLY CALLED BY A DATA OBJECT **
 * *                                                             **
 * * DATA ACCESS METHODS - DATA PASSED IN LINKAGE                **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #      DATE      PRGMR   DESCRIPTION                     **
 * * --------- --------- ------  --------------------------------**
 * * SAVANNAH  02JAN01   AJA     NEW ROUTINE.                    **
 * * 17543D    09NOV01   MM      IMPROVEMENTS FOR CLARITY.       **
 * ****************************************************************</pre>*/
public class Halrmon extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: L-HALLUBOC-DATA
	private Dfhcommarea lHallubocData;
	//Original name: L-HALLRMON-DATA
	private LHallrmonData lHallrmonData;

	//==== METHODS ====
	/**Original name: 0000-MAIN-X<br>*/
	public long execute(Dfhcommarea lHallubocData, LHallrmonData lHallrmonData) {
		this.lHallubocData = lHallubocData;
		this.lHallrmonData = lHallrmonData;
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Halrmon getInstance() {
		return (Programs.getInstance(Halrmon.class));
	}
}
