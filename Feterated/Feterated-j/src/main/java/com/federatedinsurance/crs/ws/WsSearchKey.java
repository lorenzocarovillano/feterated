/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-SEARCH-KEY<br>
 * Variable: WS-SEARCH-KEY from program HALOESTO<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSearchKey {

	//==== PROPERTIES ====
	//Original name: WS-SEARCH-GUID
	private String guid = DefaultValues.stringVal(Len.GUID);
	//Original name: WS-SEARCH-RECORDING-LEVEL
	private String recordingLevel = DefaultValues.stringVal(Len.RECORDING_LEVEL);
	//Original name: WS-SEARCH-SEQ-NUM
	private String seqNum = DefaultValues.stringVal(Len.SEQ_NUM);

	//==== METHODS ====
	public void setWsSearchKeyBytes(byte[] buffer) {
		setWsSearchKeyBytes(buffer, 1);
	}

	public byte[] getWsSearchKeyBytes() {
		byte[] buffer = new byte[Len.WS_SEARCH_KEY];
		return getWsSearchKeyBytes(buffer, 1);
	}

	public void setWsSearchKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		guid = MarshalByte.readString(buffer, position, Len.GUID);
		position += Len.GUID;
		recordingLevel = MarshalByte.readString(buffer, position, Len.RECORDING_LEVEL);
		position += Len.RECORDING_LEVEL;
		seqNum = MarshalByte.readFixedString(buffer, position, Len.SEQ_NUM);
	}

	public byte[] getWsSearchKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, guid, Len.GUID);
		position += Len.GUID;
		MarshalByte.writeString(buffer, position, recordingLevel, Len.RECORDING_LEVEL);
		position += Len.RECORDING_LEVEL;
		MarshalByte.writeString(buffer, position, seqNum, Len.SEQ_NUM);
		return buffer;
	}

	public void setGuid(String guid) {
		this.guid = Functions.subString(guid, Len.GUID);
	}

	public String getGuid() {
		return this.guid;
	}

	public void setRecordingLevel(String recordingLevel) {
		this.recordingLevel = Functions.subString(recordingLevel, Len.RECORDING_LEVEL);
	}

	public String getRecordingLevel() {
		return this.recordingLevel;
	}

	public void setSeqNum(int seqNum) {
		this.seqNum = NumericDisplay.asString(seqNum, Len.SEQ_NUM);
	}

	public int getSeqNum() {
		return NumericDisplay.asInt(this.seqNum);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int GUID = 32;
		public static final int RECORDING_LEVEL = 32;
		public static final int SEQ_NUM = 5;
		public static final int WS_SEARCH_KEY = GUID + RECORDING_LEVEL + SEQ_NUM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
