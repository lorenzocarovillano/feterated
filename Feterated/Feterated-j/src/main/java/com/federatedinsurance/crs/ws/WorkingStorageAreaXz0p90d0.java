/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.WsBillingRegion;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P90D0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p90d0 {

	//==== PROPERTIES ====
	//Original name: WS-MAX-POL-ROWS
	private short maxPolRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-EIBRESP-CD
	private short eibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0P90D0";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-NOT-BIL-OF-DTS
	private String busObjNmNotBilOfDts = "XZ_NOT_BIL_OF_POL_CAN_DTS";
	//Original name: WS-GET-POLICY-LIST
	private String getPolicyList = "GetPolicyListByNotification";
	//Original name: WS-UPD-NOT-STA
	private String updNotSta = "UpdateNotificationStatus";
	//Original name: WS-SQL-CD
	private String sqlCd = DefaultValues.stringVal(Len.SQL_CD);
	/**Original name: WS-BILLING-REGION<br>
	 * <pre>* !WARNING! DEV AND BETA BILLING REGIONS MAY CHANGE!
	 * * VERIFY THE FOLLOWING BEFORE TESTING!</pre>*/
	private WsBillingRegion billingRegion = new WsBillingRegion();

	//==== METHODS ====
	public void setMaxPolRows(short maxPolRows) {
		this.maxPolRows = maxPolRows;
	}

	public short getMaxPolRows() {
		return this.maxPolRows;
	}

	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd = eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public String getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public String getEibrespCdAsString() {
		return getEibrespCdFormatted();
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd = eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public String getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public String getEibresp2CdAsString() {
		return getEibresp2CdFormatted();
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNmNotBilOfDts() {
		return this.busObjNmNotBilOfDts;
	}

	public String getGetPolicyList() {
		return this.getPolicyList;
	}

	public String getUpdNotSta() {
		return this.updNotSta;
	}

	public void setSqlCd(long sqlCd) {
		this.sqlCd = PicFormatter.display("Z(7)9-").format(sqlCd).toString();
	}

	public long getSqlCd() {
		return PicParser.display("Z(7)9-").parseLong(this.sqlCd);
	}

	public String getSqlCdFormatted() {
		return this.sqlCd;
	}

	public String getSqlCdAsString() {
		return getSqlCdFormatted();
	}

	public WsBillingRegion getBillingRegion() {
		return billingRegion;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SQL_CD = 9;
		public static final int EIBRESP_CD = 4;
		public static final int EIBRESP2_CD = 4;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
