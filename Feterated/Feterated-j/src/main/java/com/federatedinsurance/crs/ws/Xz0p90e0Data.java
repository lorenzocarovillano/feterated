/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ActNotRecXz0p90e0;
import com.federatedinsurance.crs.commons.data.to.IActNot;
import com.federatedinsurance.crs.commons.data.to.IFrmRecAtcTyp;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotFrm;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclactNotRec;
import com.federatedinsurance.crs.copy.DclfrmRecAtcTyp;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xzh008ActNotPolRecRow;
import com.federatedinsurance.crs.ws.enums.TaAdinsRecipients;
import com.federatedinsurance.crs.ws.enums.TcCertPolicies;
import com.federatedinsurance.crs.ws.enums.TiInsRecipients;
import com.federatedinsurance.crs.ws.enums.TlLpemgRecipients;
import com.federatedinsurance.crs.ws.enums.TnAniRecipients;
import com.federatedinsurance.crs.ws.enums.TrCertRecipients;
import com.federatedinsurance.crs.ws.enums.TtTtyPolicies;
import com.federatedinsurance.crs.ws.enums.TuUniquePolicyNins;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0P90E0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0p90e0Data implements IActNot, IFrmRecAtcTyp {

	//==== PROPERTIES ====
	public static final int TC_CERT_POLICIES_MAXOCCURS = 100;
	public static final int TT_TTY_POLICIES_MAXOCCURS = 100;
	public static final int TU_UNIQUE_POLICY_NINS_MAXOCCURS = 100;
	public static final int TN_ANI_RECIPIENTS_MAXOCCURS = 4500;
	public static final int TA_ADINS_RECIPIENTS_MAXOCCURS = 4500;
	public static final int TR_CERT_RECIPIENTS_MAXOCCURS = 4500;
	public static final int TI_INS_RECIPIENTS_MAXOCCURS = 4500;
	public static final int TL_LPEMG_RECIPIENTS_MAXOCCURS = 4500;
	private ActNotRecXz0p90e0 actNotRecXz0p90e0 = new ActNotRecXz0p90e0(this);
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0p90e0 constantFields = new ConstantFieldsXz0p90e0();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXz0p90e0 errorAndAdviceMessages = new ErrorAndAdviceMessagesXz0p90e0();
	//Original name: ES-01-FATAL-ERROR-MSG
	private Es01FatalErrorMsg es01FatalErrorMsg = new Es01FatalErrorMsg();
	/**Original name: NI-ACT-TYP-CD<br>
	 * <pre>*  NULL-INDICATORS.</pre>*/
	private short niActTypCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-CER-HLD-NOT-IND
	private short niCerHldNotInd = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-FRM-SPE-ATC-CD
	private short niFrmSpeAtcCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-REC-CLT-ID
	private short niRecCltId = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-REC-ADR-ID
	private short niRecAdrId = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-SEG-CD
	private short niSegCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-STA-CD-CNT
	private short niStaCdCnt = DefaultValues.BIN_SHORT_VAL;
	//Original name: SAVE-AREA
	private SaveAreaXz0p90e0 saveArea = new SaveAreaXz0p90e0();
	//Original name: SUBSCRIPTS
	private SubscriptsXz0p0022 subscripts = new SubscriptsXz0p0022();
	//Original name: SWITCHES
	private SwitchesXz0p90e0 switches = new SwitchesXz0p90e0();
	//Original name: TC-CERT-POLICIES
	private TcCertPolicies[] tcCertPolicies = new TcCertPolicies[TC_CERT_POLICIES_MAXOCCURS];
	//Original name: IX-TC
	private int ixTc = 1;
	//Original name: TT-TTY-POLICIES
	private TtTtyPolicies[] ttTtyPolicies = new TtTtyPolicies[TT_TTY_POLICIES_MAXOCCURS];
	//Original name: IX-TT
	private int ixTt = 1;
	//Original name: TU-UNIQUE-POLICY-NINS
	private TuUniquePolicyNins[] tuUniquePolicyNins = new TuUniquePolicyNins[TU_UNIQUE_POLICY_NINS_MAXOCCURS];
	//Original name: IX-TU
	private int ixTu = 1;
	//Original name: IX-TP
	private int ixTp = 1;
	//Original name: TN-ANI-RECIPIENTS
	private LazyArrayCopy<TnAniRecipients> tnAniRecipients = new LazyArrayCopy<>(new TnAniRecipients(), 1,
			TN_ANI_RECIPIENTS_MAXOCCURS);
	//Original name: IX-TN
	private int ixTn = 1;
	//Original name: IX-TM
	private int ixTm = 1;
	//Original name: TA-ADINS-RECIPIENTS
	private LazyArrayCopy<TaAdinsRecipients> taAdinsRecipients = new LazyArrayCopy<>(new TaAdinsRecipients(), 1,
			TA_ADINS_RECIPIENTS_MAXOCCURS);
	//Original name: IX-TA
	private int ixTa = 1;
	//Original name: TR-CERT-RECIPIENTS
	private LazyArrayCopy<TrCertRecipients> trCertRecipients = new LazyArrayCopy<>(new TrCertRecipients(), 1,
			TR_CERT_RECIPIENTS_MAXOCCURS);
	//Original name: IX-TR
	private int ixTr = 1;
	//Original name: TI-INS-RECIPIENTS
	private LazyArrayCopy<TiInsRecipients> tiInsRecipients = new LazyArrayCopy<>(new TiInsRecipients(), 1,
			TI_INS_RECIPIENTS_MAXOCCURS);
	//Original name: IX-TI
	private int ixTi = 1;
	//Original name: TL-LPEMG-RECIPIENTS
	private LazyArrayCopy<TlLpemgRecipients> tlLpemgRecipients = new LazyArrayCopy<>(new TlLpemgRecipients(), 1,
			TL_LPEMG_RECIPIENTS_MAXOCCURS);
	//Original name: IX-TL
	private int ixTl = 1;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0p90e0 workingStorageArea = new WorkingStorageAreaXz0p90e0();
	//Original name: WS-XZ0Y90E0-ROW
	private WsXz0y90a0Row wsXz0y90e0Row = new WsXz0y90a0Row();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: DCLACT-NOT-FRM
	private DclactNotFrm dclactNotFrm = new DclactNotFrm();
	//Original name: DCLACT-NOT-REC
	private DclactNotRec dclactNotRec = new DclactNotRec();
	//Original name: DCLACT-NOT-POL-REC
	private Xzh008ActNotPolRecRow dclactNotPolRec = new Xzh008ActNotPolRecRow();
	//Original name: DCLFRM-REC-ATC-TYP
	private DclfrmRecAtcTyp dclfrmRecAtcTyp = new DclfrmRecAtcTyp();
	//Original name: WS-XZC030C1-ROW
	private DfhcommareaXzc03090 wsXzc030c1Row = new DfhcommareaXzc03090();
	//Original name: WS-PROXY-PROGRAM-AREA
	private WsProxyProgramArea wsProxyProgramArea = new WsProxyProgramArea();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-XZ0T0001-LINKAGE
	private DfhcommareaXz0g0001 wsXz0t0001Linkage = new DfhcommareaXz0g0001();
	//Original name: WS-ACT-BILL-LINKAGE
	private WsActBillLinkage wsActBillLinkage = new WsActBillLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== CONSTRUCTORS ====
	public Xz0p90e0Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int tcCertPoliciesIdx = 1; tcCertPoliciesIdx <= TC_CERT_POLICIES_MAXOCCURS; tcCertPoliciesIdx++) {
			tcCertPolicies[tcCertPoliciesIdx - 1] = new TcCertPolicies();
		}
		for (int ttTtyPoliciesIdx = 1; ttTtyPoliciesIdx <= TT_TTY_POLICIES_MAXOCCURS; ttTtyPoliciesIdx++) {
			ttTtyPolicies[ttTtyPoliciesIdx - 1] = new TtTtyPolicies();
		}
		for (int tuUniquePolicyNinsIdx = 1; tuUniquePolicyNinsIdx <= TU_UNIQUE_POLICY_NINS_MAXOCCURS; tuUniquePolicyNinsIdx++) {
			tuUniquePolicyNins[tuUniquePolicyNinsIdx - 1] = new TuUniquePolicyNins();
		}
	}

	public void setNiActTypCd(short niActTypCd) {
		this.niActTypCd = niActTypCd;
	}

	public short getNiActTypCd() {
		return this.niActTypCd;
	}

	public void setNiCerHldNotInd(short niCerHldNotInd) {
		this.niCerHldNotInd = niCerHldNotInd;
	}

	public short getNiCerHldNotInd() {
		return this.niCerHldNotInd;
	}

	public void setNiFrmSpeAtcCd(short niFrmSpeAtcCd) {
		this.niFrmSpeAtcCd = niFrmSpeAtcCd;
	}

	public short getNiFrmSpeAtcCd() {
		return this.niFrmSpeAtcCd;
	}

	public void setNiRecCltId(short niRecCltId) {
		this.niRecCltId = niRecCltId;
	}

	public short getNiRecCltId() {
		return this.niRecCltId;
	}

	public void setNiRecAdrId(short niRecAdrId) {
		this.niRecAdrId = niRecAdrId;
	}

	public short getNiRecAdrId() {
		return this.niRecAdrId;
	}

	public void setNiSegCd(short niSegCd) {
		this.niSegCd = niSegCd;
	}

	public short getNiSegCd() {
		return this.niSegCd;
	}

	public short getNiStaCdCnt() {
		return this.niStaCdCnt;
	}

	public void initTableOfCertPoliciesHighValues() {
		for (int idx = 1; idx <= TC_CERT_POLICIES_MAXOCCURS; idx++) {
			tcCertPolicies[idx - 1].initTcCertPoliciesHighValues();
		}
	}

	public void setIxTc(int ixTc) {
		this.ixTc = ixTc;
	}

	public int getIxTc() {
		return this.ixTc;
	}

	public void initTableOfTtyPoliciesHighValues() {
		for (int idx = 1; idx <= TT_TTY_POLICIES_MAXOCCURS; idx++) {
			ttTtyPolicies[idx - 1].initTtTtyPoliciesHighValues();
		}
	}

	public void setIxTt(int ixTt) {
		this.ixTt = ixTt;
	}

	public int getIxTt() {
		return this.ixTt;
	}

	public void initTableOfUniquePolicyNinsHighValues() {
		for (int idx = 1; idx <= TU_UNIQUE_POLICY_NINS_MAXOCCURS; idx++) {
			tuUniquePolicyNins[idx - 1].initTuUniquePolicyNinsHighValues();
		}
	}

	public void setIxTu(int ixTu) {
		this.ixTu = ixTu;
	}

	public int getIxTu() {
		return this.ixTu;
	}

	public void setIxTp(int ixTp) {
		this.ixTp = ixTp;
	}

	public int getIxTp() {
		return this.ixTp;
	}

	public void initTableOfAniRecipientsHighValues() {
		getTnAniRecipientsObj().fill(new TnAniRecipients().initTnAniRecipientsHighValues());
	}

	public void setIxTn(int ixTn) {
		this.ixTn = ixTn;
	}

	public int getIxTn() {
		return this.ixTn;
	}

	public void setIxTm(int ixTm) {
		this.ixTm = ixTm;
	}

	public int getIxTm() {
		return this.ixTm;
	}

	public void initTableOfAdinsRecipientsHighValues() {
		getTaAdinsRecipientsObj().fill(new TaAdinsRecipients().initTaAdinsRecipientsHighValues());
	}

	public void setIxTa(int ixTa) {
		this.ixTa = ixTa;
	}

	public int getIxTa() {
		return this.ixTa;
	}

	public void initTableOfCertRecipientsHighValues() {
		getTrCertRecipientsObj().fill(new TrCertRecipients().initTrCertRecipientsHighValues());
	}

	public void setIxTr(int ixTr) {
		this.ixTr = ixTr;
	}

	public int getIxTr() {
		return this.ixTr;
	}

	public void initTableOfInsRecipientsHighValues() {
		getTiInsRecipientsObj().fill(new TiInsRecipients().initTiInsRecipientsHighValues());
	}

	public void setIxTi(int ixTi) {
		this.ixTi = ixTi;
	}

	public int getIxTi() {
		return this.ixTi;
	}

	public void initTableOfLpemgRecipientsHighValues() {
		getTlLpemgRecipientsObj().fill(new TlLpemgRecipients().initTlLpemgRecipientsHighValues());
	}

	public void setIxTl(int ixTl) {
		this.ixTl = ixTl;
	}

	public int getIxTl() {
		return this.ixTl;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public ActNotRecXz0p90e0 getActNotRecXz0p90e0() {
		return actNotRecXz0p90e0;
	}

	@Override
	public String getActNotStaCd() {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public String getActNotTypCd() {
		return dclactNot.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.dclactNot.setActNotTypCd(actNotTypCd);
	}

	@Override
	public String getActOwnAdrId() {
		return dclactNot.getActOwnAdrId();
	}

	@Override
	public void setActOwnAdrId(String actOwnAdrId) {
		this.dclactNot.setActOwnAdrId(actOwnAdrId);
	}

	@Override
	public String getActOwnCltId() {
		return dclactNot.getActOwnCltId();
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		this.dclactNot.setActOwnCltId(actOwnCltId);
	}

	@Override
	public String getActTypCd() {
		return dclactNot.getActTypCd();
	}

	@Override
	public void setActTypCd(String actTypCd) {
		this.dclactNot.setActTypCd(actTypCd);
	}

	@Override
	public String getActTypCdObj() {
		if (getNiActTypCd() >= 0) {
			return getActTypCd();
		} else {
			return null;
		}
	}

	@Override
	public void setActTypCdObj(String actTypCdObj) {
		if (actTypCdObj != null) {
			setActTypCd(actTypCdObj);
			setNiActTypCd(((short) 0));
		} else {
			setNiActTypCd(((short) -1));
		}
	}

	@Override
	public short getAddCncDay() {
		throw new FieldNotMappedException("addCncDay");
	}

	@Override
	public void setAddCncDay(short addCncDay) {
		throw new FieldNotMappedException("addCncDay");
	}

	@Override
	public Short getAddCncDayObj() {
		return (getAddCncDay());
	}

	@Override
	public void setAddCncDayObj(Short addCncDayObj) {
		setAddCncDay((addCncDayObj));
	}

	@Override
	public char getAddInsRecInd() {
		return dclfrmRecAtcTyp.getAddInsRecInd();
	}

	@Override
	public void setAddInsRecInd(char addInsRecInd) {
		this.dclfrmRecAtcTyp.setAddInsRecInd(addInsRecInd);
	}

	@Override
	public char getAniRecInd() {
		return dclfrmRecAtcTyp.getAniRecInd();
	}

	@Override
	public void setAniRecInd(char aniRecInd) {
		this.dclfrmRecAtcTyp.setAniRecInd(aniRecInd);
	}

	@Override
	public char getCerHldNotInd() {
		return dclactNot.getCerHldNotInd();
	}

	@Override
	public void setCerHldNotInd(char cerHldNotInd) {
		this.dclactNot.setCerHldNotInd(cerHldNotInd);
	}

	@Override
	public Character getCerHldNotIndObj() {
		if (getNiCerHldNotInd() >= 0) {
			return (getCerHldNotInd());
		} else {
			return null;
		}
	}

	@Override
	public void setCerHldNotIndObj(Character cerHldNotIndObj) {
		if (cerHldNotIndObj != null) {
			setCerHldNotInd((cerHldNotIndObj));
			setNiCerHldNotInd(((short) 0));
		} else {
			setNiCerHldNotInd(((short) -1));
		}
	}

	@Override
	public char getCerRecInd() {
		return dclfrmRecAtcTyp.getCerRecInd();
	}

	@Override
	public void setCerRecInd(char cerRecInd) {
		this.dclfrmRecAtcTyp.setCerRecInd(cerRecInd);
	}

	public ConstantFieldsXz0p90e0 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotFrm getDclactNotFrm() {
		return dclactNotFrm;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public Xzh008ActNotPolRecRow getDclactNotPolRec() {
		return dclactNotPolRec;
	}

	public DclactNotRec getDclactNotRec() {
		return dclactNotRec;
	}

	public DclfrmRecAtcTyp getDclfrmRecAtcTyp() {
		return dclfrmRecAtcTyp;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	@Override
	public String getEmpId() {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public void setEmpId(String empId) {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	public ErrorAndAdviceMessagesXz0p90e0 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public Es01FatalErrorMsg getEs01FatalErrorMsg() {
		return es01FatalErrorMsg;
	}

	@Override
	public String getFrmEdtDt() {
		return dclfrmRecAtcTyp.getFrmEdtDt();
	}

	@Override
	public void setFrmEdtDt(String frmEdtDt) {
		this.dclfrmRecAtcTyp.setFrmEdtDt(frmEdtDt);
	}

	@Override
	public String getFrmNbr() {
		return dclfrmRecAtcTyp.getFrmNbr();
	}

	@Override
	public void setFrmNbr(String frmNbr) {
		this.dclfrmRecAtcTyp.setFrmNbr(frmNbr);
	}

	@Override
	public String getFrmSpeAtcCd() {
		return dclfrmRecAtcTyp.getFrmSpeAtcCd();
	}

	@Override
	public void setFrmSpeAtcCd(String frmSpeAtcCd) {
		this.dclfrmRecAtcTyp.setFrmSpeAtcCd(frmSpeAtcCd);
	}

	@Override
	public String getFrmSpeAtcCdObj() {
		if (getNiFrmSpeAtcCd() >= 0) {
			return getFrmSpeAtcCd();
		} else {
			return null;
		}
	}

	@Override
	public void setFrmSpeAtcCdObj(String frmSpeAtcCdObj) {
		if (frmSpeAtcCdObj != null) {
			setFrmSpeAtcCd(frmSpeAtcCdObj);
			setNiFrmSpeAtcCd(((short) 0));
		} else {
			setNiFrmSpeAtcCd(((short) -1));
		}
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	@Override
	public char getInsdRecInd() {
		return dclfrmRecAtcTyp.getInsdRecInd();
	}

	@Override
	public void setInsdRecInd(char insdRecInd) {
		this.dclfrmRecAtcTyp.setInsdRecInd(insdRecInd);
	}

	@Override
	public char getLpeMgeRecInd() {
		return dclfrmRecAtcTyp.getLpeMgeRecInd();
	}

	@Override
	public void setLpeMgeRecInd(char lpeMgeRecInd) {
		this.dclfrmRecAtcTyp.setLpeMgeRecInd(lpeMgeRecInd);
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotDt() {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public void setNotDt(String notDt) {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getPdcNbr() {
		throw new FieldNotMappedException("pdcNbr");
	}

	@Override
	public void setPdcNbr(String pdcNbr) {
		throw new FieldNotMappedException("pdcNbr");
	}

	@Override
	public String getPdcNbrObj() {
		return getPdcNbr();
	}

	@Override
	public void setPdcNbrObj(String pdcNbrObj) {
		setPdcNbr(pdcNbrObj);
	}

	@Override
	public String getPdcNm() {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public void setPdcNm(String pdcNm) {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public String getPdcNmObj() {
		return getPdcNm();
	}

	@Override
	public void setPdcNmObj(String pdcNmObj) {
		setPdcNm(pdcNmObj);
	}

	@Override
	public String getReaDes() {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public void setReaDes(String reaDes) {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public String getReaDesObj() {
		return getReaDes();
	}

	@Override
	public void setReaDesObj(String reaDesObj) {
		setReaDes(reaDesObj);
	}

	@Override
	public String getSaNotPrcTsTime() {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	@Override
	public void setSaNotPrcTsTime(String saNotPrcTsTime) {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	public SaveAreaXz0p90e0 getSaveArea() {
		return saveArea;
	}

	@Override
	public String getSegCd() {
		return dclactNot.getSegCd();
	}

	@Override
	public void setSegCd(String segCd) {
		this.dclactNot.setSegCd(segCd);
	}

	@Override
	public String getSegCdObj() {
		if (getNiSegCd() >= 0) {
			return getSegCd();
		} else {
			return null;
		}
	}

	@Override
	public void setSegCdObj(String segCdObj) {
		if (segCdObj != null) {
			setSegCd(segCdObj);
			setNiSegCd(((short) 0));
		} else {
			setNiSegCd(((short) -1));
		}
	}

	@Override
	public String getStAbb() {
		throw new FieldNotMappedException("stAbb");
	}

	@Override
	public void setStAbb(String stAbb) {
		throw new FieldNotMappedException("stAbb");
	}

	@Override
	public String getStAbbObj() {
		return getStAbb();
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		setStAbb(stAbbObj);
	}

	@Override
	public String getStaMdfTs() {
		throw new FieldNotMappedException("staMdfTs");
	}

	@Override
	public void setStaMdfTs(String staMdfTs) {
		throw new FieldNotMappedException("staMdfTs");
	}

	public SubscriptsXz0p0022 getSubscripts() {
		return subscripts;
	}

	public SwitchesXz0p90e0 getSwitches() {
		return switches;
	}

	public TaAdinsRecipients getTaAdinsRecipients(int idx) {
		return taAdinsRecipients.get(idx - 1);
	}

	public LazyArrayCopy<TaAdinsRecipients> getTaAdinsRecipientsObj() {
		return taAdinsRecipients;
	}

	public TcCertPolicies getTcCertPolicies(int idx) {
		return tcCertPolicies[idx - 1];
	}

	public TiInsRecipients getTiInsRecipients(int idx) {
		return tiInsRecipients.get(idx - 1);
	}

	public LazyArrayCopy<TiInsRecipients> getTiInsRecipientsObj() {
		return tiInsRecipients;
	}

	public TlLpemgRecipients getTlLpemgRecipients(int idx) {
		return tlLpemgRecipients.get(idx - 1);
	}

	public LazyArrayCopy<TlLpemgRecipients> getTlLpemgRecipientsObj() {
		return tlLpemgRecipients;
	}

	public TnAniRecipients getTnAniRecipients(int idx) {
		return tnAniRecipients.get(idx - 1);
	}

	public LazyArrayCopy<TnAniRecipients> getTnAniRecipientsObj() {
		return tnAniRecipients;
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		return getTotFeeAmt().toString();
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
	}

	public TrCertRecipients getTrCertRecipients(int idx) {
		return trCertRecipients.get(idx - 1);
	}

	public LazyArrayCopy<TrCertRecipients> getTrCertRecipientsObj() {
		return trCertRecipients;
	}

	public TtTtyPolicies getTtTtyPolicies(int idx) {
		return ttTtyPolicies[idx - 1];
	}

	public TuUniquePolicyNins getTuUniquePolicyNins(int idx) {
		return tuUniquePolicyNins[idx - 1];
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0p90e0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsActBillLinkage getWsActBillLinkage() {
		return wsActBillLinkage;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsProxyProgramArea getWsProxyProgramArea() {
		return wsProxyProgramArea;
	}

	public DfhcommareaXz0g0001 getWsXz0t0001Linkage() {
		return wsXz0t0001Linkage;
	}

	public WsXz0y90a0Row getWsXz0y90e0Row() {
		return wsXz0y90e0Row;
	}

	public DfhcommareaXzc03090 getWsXzc030c1Row() {
		return wsXzc030c1Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
