/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-DSP-DELIMITERS<br>
 * Variable: SA-DSP-DELIMITERS from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaDspDelimiters {

	//==== PROPERTIES ====
	//Original name: SA-DD-FST-SPR
	private String fstSpr = DefaultValues.stringVal(Len.FST_SPR);
	//Original name: SA-DD-MDL-SPR
	private String mdlSpr = DefaultValues.stringVal(Len.MDL_SPR);
	//Original name: SA-DD-LST-SPR
	private String lstSpr = DefaultValues.stringVal(Len.LST_SPR);

	//==== METHODS ====
	public void setFstSpr(String fstSpr) {
		this.fstSpr = Functions.subString(fstSpr, Len.FST_SPR);
	}

	public String getFstSpr() {
		return this.fstSpr;
	}

	public String getFstSprFormatted() {
		return Functions.padBlanks(getFstSpr(), Len.FST_SPR);
	}

	public void setMdlSpr(String mdlSpr) {
		this.mdlSpr = Functions.subString(mdlSpr, Len.MDL_SPR);
	}

	public String getMdlSpr() {
		return this.mdlSpr;
	}

	public String getMdlSprFormatted() {
		return Functions.padBlanks(getMdlSpr(), Len.MDL_SPR);
	}

	public void setLstSpr(String lstSpr) {
		this.lstSpr = Functions.subString(lstSpr, Len.LST_SPR);
	}

	public String getLstSpr() {
		return this.lstSpr;
	}

	public String getLstSprFormatted() {
		return Functions.padBlanks(getLstSpr(), Len.LST_SPR);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FST_SPR = 2;
		public static final int MDL_SPR = 2;
		public static final int LST_SPR = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
