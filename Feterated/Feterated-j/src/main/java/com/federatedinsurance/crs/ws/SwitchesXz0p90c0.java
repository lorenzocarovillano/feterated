/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ0P90C0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz0p90c0 {

	//==== PROPERTIES ====
	//Original name: SW-CERT-INFO-ADDED-FLAG
	private boolean certInfoAddedFlag = false;
	//Original name: SW-PRC-CER-HLD-REC-FLAG
	private boolean prcCerHldRecFlag = false;
	//Original name: SW-DUPLICATE-FOUND-FLAG
	private boolean duplicateFoundFlag = false;
	//Original name: SW-RECIPIENTS-FOUND-FLAG
	private boolean recipientsFoundFlag = false;
	//Original name: SW-PRI-POL-FOUND-FLAG
	private boolean priPolFoundFlag = false;

	//==== METHODS ====
	public void setCertInfoAddedFlag(boolean certInfoAddedFlag) {
		this.certInfoAddedFlag = certInfoAddedFlag;
	}

	public boolean isCertInfoAddedFlag() {
		return this.certInfoAddedFlag;
	}

	public void setPrcCerHldRecFlag(boolean prcCerHldRecFlag) {
		this.prcCerHldRecFlag = prcCerHldRecFlag;
	}

	public boolean isPrcCerHldRecFlag() {
		return this.prcCerHldRecFlag;
	}

	public void setDuplicateFoundFlag(boolean duplicateFoundFlag) {
		this.duplicateFoundFlag = duplicateFoundFlag;
	}

	public boolean isDuplicateFoundFlag() {
		return this.duplicateFoundFlag;
	}

	public void setRecipientsFoundFlag(boolean recipientsFoundFlag) {
		this.recipientsFoundFlag = recipientsFoundFlag;
	}

	public boolean isRecipientsFoundFlag() {
		return this.recipientsFoundFlag;
	}

	public void setPriPolFoundFlag(boolean priPolFoundFlag) {
		this.priPolFoundFlag = priPolFoundFlag;
	}

	public boolean isPriPolFoundFlag() {
		return this.priPolFoundFlag;
	}
}
