/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [FRM_PRC_TYP]
 * 
 */
public interface IFrmPrcTyp extends BaseSqlTo {

	/**
	 * Host Variable FRM-NBR
	 * 
	 */
	String getFrmNbr();

	void setFrmNbr(String frmNbr);

	/**
	 * Host Variable FRM-EDT-DT
	 * 
	 */
	String getFrmEdtDt();

	void setFrmEdtDt(String frmEdtDt);

	/**
	 * Host Variable ACT-NOT-TYP-CD
	 * 
	 */
	String getActNotTypCd();

	void setActNotTypCd(String actNotTypCd);

	/**
	 * Host Variable EDL-FRM-NM
	 * 
	 */
	String getEdlFrmNm();

	void setEdlFrmNm(String edlFrmNm);

	/**
	 * Host Variable FRM-DES
	 * 
	 */
	String getFrmDes();

	void setFrmDes(String frmDes);

	/**
	 * Host Variable SPE-PRC-CD
	 * 
	 */
	String getSpePrcCd();

	void setSpePrcCd(String spePrcCd);

	/**
	 * Nullable property for SPE-PRC-CD
	 * 
	 */
	String getSpePrcCdObj();

	void setSpePrcCdObj(String spePrcCdObj);

	/**
	 * Host Variable DTN-CD
	 * 
	 */
	short getDtnCd();

	void setDtnCd(short dtnCd);
};
