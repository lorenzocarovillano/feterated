/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-05-PARM-ERR-MSG<br>
 * Variable: EA-05-PARM-ERR-MSG from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea05ParmErrMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-05-PARM-ERR-MSG
	private String flr1 = "XZ001000 -";
	//Original name: FILLER-EA-05-PARM-ERR-MSG-1
	private String flr2 = "INPUT PARM";
	//Original name: FILLER-EA-05-PARM-ERR-MSG-2
	private String flr3 = "ERROR.";
	//Original name: FILLER-EA-05-PARM-ERR-MSG-3
	private String flr4 = " PARAMETER:";
	//Original name: EA-05-PARAMETER
	private String ea05Parameter = DefaultValues.stringVal(Len.EA05_PARAMETER);
	//Original name: FILLER-EA-05-PARM-ERR-MSG-4
	private String flr5 = " THIS SHOULD BE";
	//Original name: FILLER-EA-05-PARM-ERR-MSG-5
	private String flr6 = " A VALID CICS.";

	//==== METHODS ====
	public String getEa05ParmErrMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa05ParmErrMsgBytes());
	}

	public byte[] getEa05ParmErrMsgBytes() {
		byte[] buffer = new byte[Len.EA05_PARM_ERR_MSG];
		return getEa05ParmErrMsgBytes(buffer, 1);
	}

	public byte[] getEa05ParmErrMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, ea05Parameter, Len.EA05_PARAMETER);
		position += Len.EA05_PARAMETER;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setEa05Parameter(String ea05Parameter) {
		this.ea05Parameter = Functions.subString(ea05Parameter, Len.EA05_PARAMETER);
	}

	public String getEa05Parameter() {
		return this.ea05Parameter;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA05_PARAMETER = 80;
		public static final int FLR1 = 11;
		public static final int FLR3 = 6;
		public static final int FLR4 = 12;
		public static final int FLR5 = 15;
		public static final int FLR6 = 14;
		public static final int EA05_PARM_ERR_MSG = EA05_PARAMETER + 2 * FLR1 + FLR3 + FLR4 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
