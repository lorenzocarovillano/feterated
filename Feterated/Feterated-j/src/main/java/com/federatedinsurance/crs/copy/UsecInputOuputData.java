/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.UsecAction;
import com.federatedinsurance.crs.ws.enums.UsecResponseStorageSw;
import com.federatedinsurance.crs.ws.enums.UsecReturnCode;
import com.federatedinsurance.crs.ws.enums.UsecUowAuthorityLevel;

/**Original name: USEC-INPUT-OUPUT-DATA<br>
 * Variable: USEC-INPUT-OUPUT-DATA from copybook HALLUSEC<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class UsecInputOuputData {

	//==== PROPERTIES ====
	//Original name: USEC-UOW-NAME
	private String uowName = DefaultValues.stringVal(Len.UOW_NAME);
	//Original name: USEC-ACTION
	private UsecAction action = new UsecAction();
	//Original name: USEC-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: USEC-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: USEC-UOW-AUTHORITY-LEVEL
	private UsecUowAuthorityLevel uowAuthorityLevel = new UsecUowAuthorityLevel();
	//Original name: USEC-S3-AUTHORITY-NBR-SIGN
	private char s3AuthorityNbrSign = DefaultValues.CHAR_VAL;
	//Original name: USEC-S3-AUTHORITY-NBR-NUM
	private String s3AuthorityNbrNum = DefaultValues.stringVal(Len.S3_AUTHORITY_NBR_NUM);
	//Original name: USEC-GROUP-ID
	private String groupId = DefaultValues.stringVal(Len.GROUP_ID);
	//Original name: USEC-USR-FLD-TEXT
	private String usrFldText = DefaultValues.stringVal(Len.USR_FLD_TEXT);
	//Original name: USEC-FLD-TEXT
	private String fldText = DefaultValues.stringVal(Len.FLD_TEXT);
	//Original name: USEC-UOW-USER-ASSOCIATION-TYPE
	private String uowUserAssociationType = DefaultValues.stringVal(Len.UOW_USER_ASSOCIATION_TYPE);
	//Original name: USEC-RESPONSE-STORAGE-SW
	private UsecResponseStorageSw responseStorageSw = new UsecResponseStorageSw();
	/**Original name: USEC-SMCA-PM-ERROR<br>
	 * <pre>*** SUCCESS/FAILURE RETURN INFORMATION</pre>*/
	private String smcaPmError = DefaultValues.stringVal(Len.SMCA_PM_ERROR);
	//Original name: USEC-RETURN-CODE
	private UsecReturnCode returnCode = new UsecReturnCode();

	//==== METHODS ====
	public String getUsecInputOuputDataFormatted() {
		return MarshalByteExt.bufferToStr(getUsecInputOuputDataBytes());
	}

	public byte[] getUsecInputOuputDataBytes() {
		byte[] buffer = new byte[Len.USEC_INPUT_OUPUT_DATA];
		return getUsecInputOuputDataBytes(buffer, 1);
	}

	public void setUsecInputOuputDataBytes(byte[] buffer, int offset) {
		int position = offset;
		uowName = MarshalByte.readString(buffer, position, Len.UOW_NAME);
		position += Len.UOW_NAME;
		action.value = MarshalByte.readFixedString(buffer, position, UsecAction.Len.ACTION);
		position += UsecAction.Len.ACTION;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		clientId = MarshalByte.readString(buffer, position, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		uowAuthorityLevel.value = MarshalByte.readFixedString(buffer, position, UsecUowAuthorityLevel.Len.UOW_AUTHORITY_LEVEL);
		position += UsecUowAuthorityLevel.Len.UOW_AUTHORITY_LEVEL;
		setS3AuthorityNbrBytes(buffer, position);
		position += Len.S3_AUTHORITY_NBR;
		groupId = MarshalByte.readString(buffer, position, Len.GROUP_ID);
		position += Len.GROUP_ID;
		usrFldText = MarshalByte.readString(buffer, position, Len.USR_FLD_TEXT);
		position += Len.USR_FLD_TEXT;
		fldText = MarshalByte.readString(buffer, position, Len.FLD_TEXT);
		position += Len.FLD_TEXT;
		uowUserAssociationType = MarshalByte.readString(buffer, position, Len.UOW_USER_ASSOCIATION_TYPE);
		position += Len.UOW_USER_ASSOCIATION_TYPE;
		responseStorageSw.value = MarshalByte.readFixedString(buffer, position, UsecResponseStorageSw.Len.RESPONSE_STORAGE_SW);
		position += UsecResponseStorageSw.Len.RESPONSE_STORAGE_SW;
		smcaPmError = MarshalByte.readString(buffer, position, Len.SMCA_PM_ERROR);
		position += Len.SMCA_PM_ERROR;
		returnCode.value = MarshalByte.readFixedString(buffer, position, UsecReturnCode.Len.RETURN_CODE);
	}

	public byte[] getUsecInputOuputDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, uowName, Len.UOW_NAME);
		position += Len.UOW_NAME;
		MarshalByte.writeString(buffer, position, action.value, UsecAction.Len.ACTION);
		position += UsecAction.Len.ACTION;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeString(buffer, position, uowAuthorityLevel.value, UsecUowAuthorityLevel.Len.UOW_AUTHORITY_LEVEL);
		position += UsecUowAuthorityLevel.Len.UOW_AUTHORITY_LEVEL;
		getS3AuthorityNbrBytes(buffer, position);
		position += Len.S3_AUTHORITY_NBR;
		MarshalByte.writeString(buffer, position, groupId, Len.GROUP_ID);
		position += Len.GROUP_ID;
		MarshalByte.writeString(buffer, position, usrFldText, Len.USR_FLD_TEXT);
		position += Len.USR_FLD_TEXT;
		MarshalByte.writeString(buffer, position, fldText, Len.FLD_TEXT);
		position += Len.FLD_TEXT;
		MarshalByte.writeString(buffer, position, uowUserAssociationType, Len.UOW_USER_ASSOCIATION_TYPE);
		position += Len.UOW_USER_ASSOCIATION_TYPE;
		MarshalByte.writeString(buffer, position, responseStorageSw.value, UsecResponseStorageSw.Len.RESPONSE_STORAGE_SW);
		position += UsecResponseStorageSw.Len.RESPONSE_STORAGE_SW;
		MarshalByte.writeString(buffer, position, smcaPmError, Len.SMCA_PM_ERROR);
		position += Len.SMCA_PM_ERROR;
		MarshalByte.writeString(buffer, position, returnCode.value, UsecReturnCode.Len.RETURN_CODE);
		return buffer;
	}

	public void initUsecInputOuputDataSpaces() {
		uowName = "";
		action.setAction(Types.INVALID_SHORT_VAL);
		userId = "";
		clientId = "";
		uowAuthorityLevel.setUowAuthorityLevel(Types.INVALID_INT_VAL);
		initS3AuthorityNbrSpaces();
		groupId = "";
		usrFldText = "";
		fldText = "";
		uowUserAssociationType = "";
		responseStorageSw.setResponseStorageSw(Types.INVALID_SHORT_VAL);
		smcaPmError = "";
		returnCode.setReturnCode(Types.INVALID_SHORT_VAL);
	}

	public void setUowName(String uowName) {
		this.uowName = Functions.subString(uowName, Len.UOW_NAME);
	}

	public String getUowName() {
		return this.uowName;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public String getUserIdFormatted() {
		return Functions.padBlanks(getUserId(), Len.USER_ID);
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setS3AuthorityNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		s3AuthorityNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		s3AuthorityNbrNum = MarshalByte.readFixedString(buffer, position, Len.S3_AUTHORITY_NBR_NUM);
	}

	public byte[] getS3AuthorityNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, s3AuthorityNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, s3AuthorityNbrNum, Len.S3_AUTHORITY_NBR_NUM);
		return buffer;
	}

	public void initS3AuthorityNbrSpaces() {
		s3AuthorityNbrSign = Types.SPACE_CHAR;
		s3AuthorityNbrNum = "";
	}

	public void setS3AuthorityNbrSign(char s3AuthorityNbrSign) {
		this.s3AuthorityNbrSign = s3AuthorityNbrSign;
	}

	public char getS3AuthorityNbrSign() {
		return this.s3AuthorityNbrSign;
	}

	public void setGroupId(String groupId) {
		this.groupId = Functions.subString(groupId, Len.GROUP_ID);
	}

	public String getGroupId() {
		return this.groupId;
	}

	public void setUsrFldText(String usrFldText) {
		this.usrFldText = Functions.subString(usrFldText, Len.USR_FLD_TEXT);
	}

	public String getUsrFldText() {
		return this.usrFldText;
	}

	public void setFldText(String fldText) {
		this.fldText = Functions.subString(fldText, Len.FLD_TEXT);
	}

	public String getFldText() {
		return this.fldText;
	}

	public void setUowUserAssociationType(String uowUserAssociationType) {
		this.uowUserAssociationType = Functions.subString(uowUserAssociationType, Len.UOW_USER_ASSOCIATION_TYPE);
	}

	public String getUowUserAssociationType() {
		return this.uowUserAssociationType;
	}

	public void setSmcaPmError(String smcaPmError) {
		this.smcaPmError = Functions.subString(smcaPmError, Len.SMCA_PM_ERROR);
	}

	public String getSmcaPmError() {
		return this.smcaPmError;
	}

	public UsecAction getAction() {
		return action;
	}

	public UsecResponseStorageSw getResponseStorageSw() {
		return responseStorageSw;
	}

	public UsecReturnCode getReturnCode() {
		return returnCode;
	}

	public UsecUowAuthorityLevel getUowAuthorityLevel() {
		return uowAuthorityLevel;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NAME = 32;
		public static final int USER_ID = 32;
		public static final int CLIENT_ID = 20;
		public static final int S3_AUTHORITY_NBR_NUM = 5;
		public static final int GROUP_ID = 8;
		public static final int USR_FLD_TEXT = 20;
		public static final int FLD_TEXT = 20;
		public static final int UOW_USER_ASSOCIATION_TYPE = 8;
		public static final int SMCA_PM_ERROR = 76;
		public static final int S3_AUTHORITY_NBR_SIGN = 1;
		public static final int S3_AUTHORITY_NBR = S3_AUTHORITY_NBR_SIGN + S3_AUTHORITY_NBR_NUM;
		public static final int USEC_INPUT_OUPUT_DATA = UOW_NAME + UsecAction.Len.ACTION + USER_ID + CLIENT_ID
				+ UsecUowAuthorityLevel.Len.UOW_AUTHORITY_LEVEL + S3_AUTHORITY_NBR + GROUP_ID + USR_FLD_TEXT + FLD_TEXT + UOW_USER_ASSOCIATION_TYPE
				+ UsecResponseStorageSw.Len.RESPONSE_STORAGE_SW + SMCA_PM_ERROR + UsecReturnCode.Len.RETURN_CODE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
