/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [FRM_REC_ATC_TYP]
 * 
 */
public interface IFrmRecAtcTyp extends BaseSqlTo {

	/**
	 * Host Variable FRM-NBR
	 * 
	 */
	String getFrmNbr();

	void setFrmNbr(String frmNbr);

	/**
	 * Host Variable FRM-EDT-DT
	 * 
	 */
	String getFrmEdtDt();

	void setFrmEdtDt(String frmEdtDt);

	/**
	 * Host Variable FRM-SPE-ATC-CD
	 * 
	 */
	String getFrmSpeAtcCd();

	void setFrmSpeAtcCd(String frmSpeAtcCd);

	/**
	 * Nullable property for FRM-SPE-ATC-CD
	 * 
	 */
	String getFrmSpeAtcCdObj();

	void setFrmSpeAtcCdObj(String frmSpeAtcCdObj);

	/**
	 * Host Variable INSD-REC-IND
	 * 
	 */
	char getInsdRecInd();

	void setInsdRecInd(char insdRecInd);

	/**
	 * Host Variable ADD-INS-REC-IND
	 * 
	 */
	char getAddInsRecInd();

	void setAddInsRecInd(char addInsRecInd);

	/**
	 * Host Variable LPE-MGE-REC-IND
	 * 
	 */
	char getLpeMgeRecInd();

	void setLpeMgeRecInd(char lpeMgeRecInd);

	/**
	 * Host Variable CER-REC-IND
	 * 
	 */
	char getCerRecInd();

	void setCerRecInd(char cerRecInd);

	/**
	 * Host Variable ANI-REC-IND
	 * 
	 */
	char getAniRecInd();

	void setAniRecInd(char aniRecInd);
};
