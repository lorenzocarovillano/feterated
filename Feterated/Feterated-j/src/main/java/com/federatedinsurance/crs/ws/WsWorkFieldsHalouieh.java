/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-WORK-FIELDS<br>
 * Variable: WS-WORK-FIELDS from program HALOUIEH<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkFieldsHalouieh {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "HALOUIEH";
	//Original name: WS-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "";
	//Original name: WS-BUS-OBJ-MDU
	private String busObjMdu = "";
	//Original name: WS-BYPASS-SE-URQM-LIT
	private String bypassSeUrqmLit = "BYPASS_SIMPLE_EDIT_STEP";

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setBusObjMdu(String busObjMdu) {
		this.busObjMdu = Functions.subString(busObjMdu, Len.BUS_OBJ_MDU);
	}

	public String getBusObjMdu() {
		return this.busObjMdu;
	}

	public String getBypassSeUrqmLit() {
		return this.bypassSeUrqmLit;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUS_OBJ_NM = 32;
		public static final int BUS_OBJ_MDU = 32;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
