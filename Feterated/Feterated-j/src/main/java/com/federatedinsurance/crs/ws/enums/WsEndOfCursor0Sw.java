/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-END-OF-CURSOR0-SW<br>
 * Variable: WS-END-OF-CURSOR0-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEndOfCursor0Sw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_OF_CURSOR0 = 'Y';
	public static final char NOT_END_OF_CURSOR0 = 'N';

	//==== METHODS ====
	public void setEndOfCursor0Sw(char endOfCursor0Sw) {
		this.value = endOfCursor0Sw;
	}

	public char getEndOfCursor0Sw() {
		return this.value;
	}

	public boolean isWsEndOfCursor0() {
		return value == END_OF_CURSOR0;
	}

	public void setWsEndOfCursor0() {
		value = END_OF_CURSOR0;
	}

	public void setWsNotEndOfCursor0() {
		value = NOT_END_OF_CURSOR0;
	}
}
