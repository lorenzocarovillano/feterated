/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLFED-TOB-CODE-V<br>
 * Variable: DCLFED-TOB-CODE-V from copybook MUH00104<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclfedTobCodeV {

	//==== PROPERTIES ====
	//Original name: TOB-CD
	private String tobCd = DefaultValues.stringVal(Len.TOB_CD);
	//Original name: CTR-NBR-CD
	private short ctrNbrCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: TOB-DES
	private String tobDes = DefaultValues.stringVal(Len.TOB_DES);

	//==== METHODS ====
	public void setTobCd(String tobCd) {
		this.tobCd = Functions.subString(tobCd, Len.TOB_CD);
	}

	public String getTobCd() {
		return this.tobCd;
	}

	public String getTobCdFormatted() {
		return Functions.padBlanks(getTobCd(), Len.TOB_CD);
	}

	public void setCtrNbrCd(short ctrNbrCd) {
		this.ctrNbrCd = ctrNbrCd;
	}

	public short getCtrNbrCd() {
		return this.ctrNbrCd;
	}

	public void setTobDes(String tobDes) {
		this.tobDes = Functions.subString(tobDes, Len.TOB_DES);
	}

	public String getTobDes() {
		return this.tobDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TOB_CD = 10;
		public static final int TOB_DES = 40;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
