/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.WtTrnCd;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XZC00401<br>
 * Variable: XZC00401 from copybook XZC00401<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc00401 {

	//==== PROPERTIES ====
	/**Original name: WT-POL-NBR<br>
	 * <pre>****************************************************************
	 *     NEW MAINTENANCE HISTORY
	 * ****************************************************************
	 *     INFO    CHANGE
	 *     NMBR     DATE    III  DESCRIPTION
	 *     *****  ********  ***  **************************************
	 *     22674  02/18/19  JAL  NEW JOB
	 * ****************************************************************
	 * 01  WCPOLS-TRIGGER-RECORD.</pre>*/
	public String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: WT-POL-EFF-YYYY-MM-DD
	private WtPolEffYyyyMmDd polEffYyyyMmDd = new WtPolEffYyyyMmDd();
	//Original name: WT-ISS-ACY-TS
	private String issAcyTs = DefaultValues.stringVal(Len.ISS_ACY_TS);
	//Original name: WT-TRN-PRC-YYYY-MM-DD
	private WtPolEffYyyyMmDd trnPrcYyyyMmDd = new WtPolEffYyyyMmDd();
	//Original name: WT-TRN-EFF-YYYY-MM-DD
	private WtPolEffYyyyMmDd trnEffYyyyMmDd = new WtPolEffYyyyMmDd();
	//Original name: WT-TRN-CD
	private WtTrnCd trnCd = new WtTrnCd();
	//Original name: WT-WC-PRC-MM-DD-YYYY
	private WtWcPrcMmDdYyyy wcPrcMmDdYyyy = new WtWcPrcMmDdYyyy();
	//Original name: WT-TRN-CRS-INIT-IND
	private char trnCrsInitInd = DefaultValues.CHAR_VAL;
	//Original name: WT-TRN-CRS-SEQ-NBR
	public String trnCrsSeqNbr = DefaultValues.stringVal(Len.TRN_CRS_SEQ_NBR);
	//Original name: FILLER-WC-TRIGGER-RECORD
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void initXzc00401Spaces() {
		polNbr = "";
		polEffYyyyMmDd.initPolEffYyyyMmDdSpaces();
		issAcyTs = "";
		trnPrcYyyyMmDd.initPolEffYyyyMmDdSpaces();
		trnEffYyyyMmDd.initPolEffYyyyMmDdSpaces();
		trnCd.setTrnCd("");
		wcPrcMmDdYyyy.initWcPrcMmDdYyyySpaces();
		trnCrsInitInd = Types.SPACE_CHAR;
		trnCrsSeqNbr = "";
		flr1 = "";
	}

	public void setPolNbrFormatted(String polNbr) {
		this.polNbr = Trunc.toUnsignedNumeric(polNbr, Len.POL_NBR);
	}

	public int getPolNbr() {
		return NumericDisplay.asInt(this.polNbr);
	}

	public void setIssAcyTs(String issAcyTs) {
		this.issAcyTs = Functions.subString(issAcyTs, Len.ISS_ACY_TS);
	}

	public String getIssAcyTs() {
		return this.issAcyTs;
	}

	public void setTrnCrsInitInd(char trnCrsInitInd) {
		this.trnCrsInitInd = trnCrsInitInd;
	}

	public char getTrnCrsInitInd() {
		return this.trnCrsInitInd;
	}

	public void setTrnCrsSeqNbr(short trnCrsSeqNbr) {
		this.trnCrsSeqNbr = NumericDisplay.asString(trnCrsSeqNbr, Len.TRN_CRS_SEQ_NBR);
	}

	public short getTrnCrsSeqNbr() {
		return NumericDisplay.asShort(this.trnCrsSeqNbr);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public WtPolEffYyyyMmDd getPolEffYyyyMmDd() {
		return polEffYyyyMmDd;
	}

	public WtTrnCd getTrnCd() {
		return trnCd;
	}

	public WtPolEffYyyyMmDd getTrnEffYyyyMmDd() {
		return trnEffYyyyMmDd;
	}

	public WtPolEffYyyyMmDd getTrnPrcYyyyMmDd() {
		return trnPrcYyyyMmDd;
	}

	public WtWcPrcMmDdYyyy getWcPrcMmDdYyyy() {
		return wcPrcMmDdYyyy;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 7;
		public static final int ISS_ACY_TS = 26;
		public static final int TRN_CRS_SEQ_NBR = 2;
		public static final int FLR1 = 2;
		public static final int TRN_CRS_INIT_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
