/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-SPECIFIC-WORK-AREAS<br>
 * Variable: WS-SPECIFIC-WORK-AREAS from program HALOUSDH<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSpecificWorkAreasHalousdh {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "HALOUSDH";
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "SET_DEFAULTS_HUB";
	//Original name: WS-DATA-PRIV-CTX-LIT
	private String dataPrivCtxLit = "DATA_PRIVACY_CONTEXT";
	//Original name: WS-FUNCTIONS-ROW-NAME
	private String functionsRowName = "FUNCTIONS";
	//Original name: WS-SCRNSCRPT-ROW-NAME
	private String scrnscrptRowName = "SCREEN_SCRIPT";

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public String getDataPrivCtxLit() {
		return this.dataPrivCtxLit;
	}

	public String getFunctionsRowName() {
		return this.functionsRowName;
	}

	public String getScrnscrptRowName() {
		return this.scrnscrptRowName;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
