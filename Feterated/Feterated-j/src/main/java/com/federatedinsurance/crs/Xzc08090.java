/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.IAfSet;
import com.bphx.ctu.af.lang.collection.creation.CollectionCreator;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpAccessStatus;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.Ivoryh;
import com.federatedinsurance.crs.copy.Xz08coServiceOutputs;
import com.federatedinsurance.crs.copy.Xzc080ci;
import com.federatedinsurance.crs.copy.Xzc080co;
import com.federatedinsurance.crs.ws.DefaultComm;
import com.federatedinsurance.crs.ws.SaveAreaXzc08090;
import com.federatedinsurance.crs.ws.Ts571cb1;
import com.federatedinsurance.crs.ws.Xzc08090Data;
import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZC08090<br>
 * <pre>AUTHOR.       Tim Johnson.
 * DATE-WRITTEN. AUG 2018.
 * ****************************************************************
 *                                                               **
 *   PROGRAM TITLE - getCncNoticeEligibleAddlInterestList        **
 *                   INTERFACE PROGRAM                           **
 *   PURPOSE -  CALLS A CALLABLE WEB SERVICE TO INTERFACE WITH   **
 *              A BROKERED WEB SERVICE TO RETRIEVE ADDITIONAL    **
 *              INSURED INFORMATION                              **
 *                                                               **
 *   PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS **
 *                         LINKED TO BY AN APPLICATION PROGRAM.  **
 *                                                               **
 *   DATA ACCESS METHODS - CICS LINKAGE                          **
 *                         DB2 DATABASE                          **
 *                                                               **
 * ****************************************************************
 *                 M A I N T E N A N C E    L O G                **
 *                                                               **
 *   SI #          DATE      PROG             DESCRIPTION        **
 *   --------   ----------  --------   --------------------------**
 *   20163      08/24/2018    TJJ      NEW
 * ****************************************************************</pre>*/
public class Xzc08090 extends Program {

	//==== PROPERTIES ====
	//Original name: DEFAULT-COMM
	private DefaultComm defaultComm;
	private ExecContext execContext = null;
	private IAfSet<String> channelSet = CollectionCreator.getStringFactory().createSet();
	//Original name: WORKING-STORAGE
	private Xzc08090Data ws = new Xzc08090Data();

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DefaultComm defaultComm) {
		this.execContext = execContext;
		this.defaultComm = defaultComm;
		mainline();
		programExit();
		return 0;
	}

	public static Xzc08090 getInstance() {
		return (Programs.getInstance(Xzc08090.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>*****************************************************************
	 *   MAIN PROCESSING CONTROL
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF XZC08O-RC-OK
		//                  THRU 3000-EXIT
		//           END-IF.
		if (ws.getXzc080co().getRetCd().isOk()) {
			// COB_CODE: PERFORM 3000-GET-ADDL-ITS
			//              THRU 3000-EXIT
			getAddlIts();
		}
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>*****************************************************************
	 *  INITIALIZE OUTPUT AND VERIFY INPUT.
	 * *****************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: SET XZC08O-RC-OK            TO TRUE.
		ws.getXzc080co().getRetCd().setOk();
		// COB_CODE: PERFORM 2100-CAPTURE-CHANNEL-NAME
		//              THRU 2100-EXIT.
		captureChannelName();
		// COB_CODE: PERFORM 2200-CAPTURE-INPUT
		//              THRU 2200-EXIT.
		captureInput();
		// COB_CODE: PERFORM 2300-VERIFY-INPUT
		//              THRU 2300-EXIT.
		verifyInput();
		// COB_CODE: PERFORM 2400-DET-CICS-ENV-INF
		//              THRU 2400-EXIT.
		detCicsEnvInf();
	}

	/**Original name: 2100-CAPTURE-CHANNEL-NAME<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE NAME OF THE CHANNEL PASSED TO THIS PROGRAM       *
	 * ***************************************************************</pre>*/
	private void captureChannelName() {
		// COB_CODE: MOVE SPACES                 TO SA-CHN-NM.
		ws.getSaveArea().setChnNm("");
		// COB_CODE: EXEC CICS ASSIGN
		//               CHANNEL   (SA-CHN-NM)
		//               RESP      (SA-RESPONSE-CODE)
		//               RESP2     (SA-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getSaveArea().setChnNm(execContext.getCurrentChannel());
		execContext.clearStatus();
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             OR
		//              SA-CHN-NM = SPACES
		//                  THRU 9100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL
				|| Characters.EQ_SPACE.test(ws.getSaveArea().getChnNm())) {
			// COB_CODE: MOVE CF-PN-2100         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2100());
			// COB_CODE: PERFORM 9100-END-WITH-FATAL-INP-ERR
			//              THRU 9100-EXIT
			endWithFatalInpErr();
		}
	}

	/**Original name: 2200-CAPTURE-INPUT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE SERVICE INPUTS FROM THE INPUT CHANNEL            *
	 * ***************************************************************</pre>*/
	private void captureInput() {
		TpOutputData tsOutputData = null;
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CI-SVC-IN-CONTAINER)
		//               CHANNEL    (SA-CHN-NM)
		//               INTO       (SERVICE-INPUTS)
		//               FLENGTH    (LENGTH OF SERVICE-INPUTS)
		//               RESP       (SA-RESPONSE-CODE)
		//               RESP2      (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc08090Data.Len.SERVICE_INPUTS);
		Channel.read(execContext, ws.getSaveArea().getChnNmFormatted(), ws.getConstantFields().getContainerInfo().getSvcInContainerFormatted(),
				tsOutputData);
		ws.setServiceInputsBytes(tsOutputData.getData());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-2200         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2200());
			// COB_CODE: PERFORM 9100-END-WITH-FATAL-INP-ERR
			//              THRU 9100-EXIT
			endWithFatalInpErr();
		}
	}

	/**Original name: 2300-VERIFY-INPUT<br>
	 * <pre>*****************************************************************
	 *  VERIFY THAT THE CONSUMER PASSED IN VALID INPUTS.
	 * *****************************************************************</pre>*/
	private void verifyInput() {
		// COB_CODE: IF XZC08I-POL-NBR = SPACES
		//             OR
		//              XZC08I-POL-NBR = LOW-VALUES
		//             OR
		//              XZC08I-TRM-EXP-DT = SPACES
		//             OR
		//              XZC08I-TRM-EXP-DT = LOW-VALUES
		//                  THRU 9100-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc080ci().getPolNbr()) || Characters.EQ_LOW.test(ws.getXzc080ci().getPolNbr(), Xzc080ci.Len.POL_NBR)
				|| Characters.EQ_SPACE.test(ws.getXzc080ci().getTrmExpDt()) || Characters.EQ_LOW.test(ws.getXzc080ci().getTrmExpDtFormatted())) {
			// COB_CODE: MOVE CF-PN-2300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2300());
			// COB_CODE: PERFORM 9100-END-WITH-FATAL-INP-ERR
			//              THRU 9100-EXIT
			endWithFatalInpErr();
		}
	}

	/**Original name: 2400-DET-CICS-ENV-INF<br>
	 * <pre>*****************************************************************
	 *  GET THE CURRENT CICS REGION TO DETERMINE TARGET SYSTEM.
	 * *****************************************************************</pre>*/
	private void detCicsEnvInf() {
		// COB_CODE: EXEC CICS ASSIGN
		//               APPLID(SA-CICS-APPLID)
		//           END-EXEC.
		ws.getSaveArea().getCicsApplid().setCicsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: EVALUATE TRUE
		//               WHEN SA-CA-PROD
		//                   SET SA-TS-PROD      TO TRUE
		//               WHEN SA-CA-DEV1
		//                   SET SA-TS-DEV1      TO TRUE
		//               WHEN SA-CA-DEV2
		//                   SET SA-TS-DEV2      TO TRUE
		//               WHEN SA-CA-DEV6
		//                   SET SA-TS-DEV6      TO TRUE
		//               WHEN SA-CA-BETA1
		//                   SET SA-TS-BETA1     TO TRUE
		//               WHEN SA-CA-BETA2
		//                   SET SA-TS-BETA2     TO TRUE
		//               WHEN SA-CA-BETA8
		//                   SET SA-TS-BETA8     TO TRUE
		//               WHEN SA-CA-EDUC1
		//                   SET SA-TS-EDUC1     TO TRUE
		//               WHEN SA-CA-EDUC2
		//                   SET SA-TS-EDUC2     TO TRUE
		//               WHEN OTHER
		//                      THRU 9150-EXIT
		//           END-EVALUATE.
		if (ws.getSaveArea().getCicsApplid().isProd()) {
			// COB_CODE: SET SA-TS-PROD      TO TRUE
			ws.getSaveArea().getTarSys().setProd();
		} else if (ws.getSaveArea().getCicsApplid().isDev1()) {
			// COB_CODE: SET SA-TS-DEV1      TO TRUE
			ws.getSaveArea().getTarSys().setDev1();
		} else if (ws.getSaveArea().getCicsApplid().isDev2()) {
			// COB_CODE: SET SA-TS-DEV2      TO TRUE
			ws.getSaveArea().getTarSys().setDev2();
		} else if (ws.getSaveArea().getCicsApplid().isDev6()) {
			// COB_CODE: SET SA-TS-DEV6      TO TRUE
			ws.getSaveArea().getTarSys().setDev6();
		} else if (ws.getSaveArea().getCicsApplid().isBeta1()) {
			// COB_CODE: SET SA-TS-BETA1     TO TRUE
			ws.getSaveArea().getTarSys().setBeta1();
		} else if (ws.getSaveArea().getCicsApplid().isBeta2()) {
			// COB_CODE: SET SA-TS-BETA2     TO TRUE
			ws.getSaveArea().getTarSys().setBeta2();
		} else if (ws.getSaveArea().getCicsApplid().isBeta8()) {
			// COB_CODE: SET SA-TS-BETA8     TO TRUE
			ws.getSaveArea().getTarSys().setBeta8();
		} else if (ws.getSaveArea().getCicsApplid().isEduc1()) {
			// COB_CODE: SET SA-TS-EDUC1     TO TRUE
			ws.getSaveArea().getTarSys().setEduc1();
		} else if (ws.getSaveArea().getCicsApplid().isEduc2()) {
			// COB_CODE: SET SA-TS-EDUC2     TO TRUE
			ws.getSaveArea().getTarSys().setEduc2();
		} else {
			// COB_CODE: MOVE CF-PN-2400     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2400());
			// COB_CODE: PERFORM 9150-END-WITH-CICS-APPLID-ERR
			//              THRU 9150-EXIT
			endWithCicsApplidErr();
		}
	}

	/**Original name: 3000-GET-ADDL-ITS<br>
	 * <pre>*****************************************************************
	 *  CALL THE CRSPolicyInformationCallable SERVICE TO RETRIEVE
	 *  THE POLICY DETAILS.
	 * *****************************************************************</pre>*/
	private void getAddlIts() {
		// COB_CODE: PERFORM 3100-PRP-WEB-SVC-CALL
		//              THRU 3100-EXIT.
		prpWebSvcCall();
		// COB_CODE: IF NOT XZC08O-RC-OK
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!ws.getXzc080co().getRetCd().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3200-MOVE-WEB-SVC-INPUT
		//              THRU 3200-EXIT.
		moveWebSvcInput();
		// COB_CODE: IF NOT XZC08O-RC-OK
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!ws.getXzc080co().getRetCd().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3300-SETUP-CONTAINERS
		//              THRU 3300-EXIT.
		setupContainers();
		// COB_CODE: IF NOT XZC08O-RC-OK
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!ws.getXzc080co().getRetCd().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3400-LINK-TO-GIICALS
		//              THRU 3400-EXIT.
		linkToGiicals();
		// COB_CODE: IF NOT XZC08O-RC-OK
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!ws.getXzc080co().getRetCd().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3500-MOVE-WEB-SVC-OUTPUT
		//              THRU 3500-EXIT.
		rng3500MoveWebSvcOutput();
		// COB_CODE: PERFORM 9000-MOVE-TO-CONTAINER
		//              THRU 9000-EXIT.
		moveToContainer();
	}

	/**Original name: 3100-PRP-WEB-SVC-CALL<br>
	 * <pre>*****************************************************************
	 *  INITIAL SETUP FOR CALLING IVORY WEB SERVICE PROGRAM
	 * *****************************************************************</pre>*/
	private void prpWebSvcCall() {
		// COB_CODE: MOVE LOW-VALUES             TO NON-STATIC-FIELDS
		//                                          CALLABLE-INPUTS
		//                                          CALLABLE-OUTPUTS.
		ws.getIvoryh().getNonStaticFields().initNonStaticFieldsLowValues();
		ws.initCallableInputsLowValues();
		ws.initCallableOutputsLowValues();
		// COB_CODE: MOVE CF-SOAP-SERVICE        TO SOAP-SERVICE IN IVORYH.
		ws.getIvoryh().getNonStaticFields().setService(ws.getConstantFields().getSoapService());
		// COB_CODE: MOVE CF-SOAP-OPERATION      TO SOAP-OPERATION IN IVORYH.
		ws.getIvoryh().getNonStaticFields().setOperation(ws.getConstantFields().getSoapOperation());
		// COB_CODE: MOVE CF-LV-ZLINUX-SRV       TO SOAP-LAYOUT-VERSION.
		ws.getIvoryh().setSoapLayoutVersion(ws.getConstantFields().getLvZlinuxSrv());
		// COB_CODE: SET SOAP-TRACE-STOR-CHARSET-EBCDIC
		//               SOAP-REQUEST-STOR-TYPE-CHCONT
		//               SOAP-RESPONSE-STOR-TYPE-CHCONT
		//               SOAP-TRACE-STOR-TYPE-TDQ
		//               SOAP-ERROR-STOR-TYPE-TDQ
		//                                       TO TRUE.
		ws.getIvoryh().getNonStaticFields().getTraceStorCharset().setEbcdic();
		ws.getIvoryh().getNonStaticFields().getRequestStorType().setChcont();
		ws.getIvoryh().getNonStaticFields().getResponseStorType().setChcont();
		ws.getIvoryh().getNonStaticFields().getTraceStorType().setTdq();
		ws.getIvoryh().getNonStaticFields().getErrorStorType().setTdq();
		// COB_CODE: MOVE CF-CI-INP-CONTAINER    TO SOAP-REQUEST-STORE-VALUE.
		ws.getIvoryh().getNonStaticFields().setRequestStoreValue(ws.getConstantFields().getContainerInfo().getInpContainer());
		// COB_CODE: MOVE CF-CI-OUP-CONTAINER    TO SOAP-RESPONSE-STORE-VALUE.
		ws.getIvoryh().getNonStaticFields().setResponseStoreValue(ws.getConstantFields().getContainerInfo().getOupContainer());
		// COB_CODE: MOVE LENGTH OF CALLABLE-INPUTS
		//                                       TO SOAP-REQUEST-LENGTH.
		ws.getIvoryh().getNonStaticFields().setRequestLength(Xzc08090Data.Len.CALLABLE_INPUTS);
		// COB_CODE: MOVE LENGTH OF CALLABLE-OUTPUTS
		//                                       TO SOAP-RESPONSE-LENGTH.
		ws.getIvoryh().getNonStaticFields().setResponseLength(Xzc08090Data.Len.CALLABLE_OUTPUTS);
		// COB_CODE: MOVE CF-TRACE-STOR-VALUE    TO SOAP-TRACE-STOR-VALUE
		//                                          SOAP-ERROR-STOR-VALUE.
		ws.getIvoryh().getNonStaticFields().setTraceStorValue(ws.getConstantFields().getTraceStorValue());
		ws.getIvoryh().getNonStaticFields().setErrorStorValue(ws.getConstantFields().getTraceStorValue());
	}

	/**Original name: 3200-MOVE-WEB-SVC-INPUT<br>
	 * <pre>*****************************************************************
	 *  SETUP INPUT FOR THE SERVICE CALL TO IVORY
	 * *****************************************************************</pre>*/
	private void moveWebSvcInput() {
		// COB_CODE: INITIALIZE CALLABLE-INPUTS.
		initCallableInputs();
		// COB_CODE: PERFORM 3210-RETRIEVE-SVC-URL
		//              THRU 3210-EXIT.
		retrieveSvcUrl();
		// COB_CODE: MOVE XZC08I-POL-NBR         TO XZ08CI-POL-NBR.
		ws.getXz08ci1i().setPolNbr(ws.getXzc080ci().getPolNbr());
		// COB_CODE: MOVE XZC08I-TRM-EXP-DT      TO XZ08CI-TRM-EXP-DT.
		ws.getXz08ci1i().setTrmExpDt(ws.getXzc080ci().getTrmExpDt());
		// COB_CODE: MOVE SA-WEB-SVC-URL         TO XZ08CI-URI.
		ws.getXz08ci1i().setUri(ws.getSaveArea().getWebSvcUrl());
		// COB_CODE: PERFORM 3220-RETRIEVE-USR-ID-PWD
		//              THRU 3220-EXIT.
		retrieveUsrIdPwd();
		// COB_CODE: MOVE SA-UC-USR-ID           TO XZ08CI-USR-ID.
		ws.getXz08ci1i().setUsrId(ws.getSaveArea().getUcUsrId());
		// COB_CODE: MOVE SA-UC-PWD              TO XZ08CI-PWD.
		ws.getXz08ci1i().setPwd(ws.getSaveArea().getUcPwd());
		// COB_CODE: MOVE SA-TAR-SYS             TO XZ08CI-TAR-SYS.
		ws.getXz08ci1i().setTarSys(ws.getSaveArea().getTarSys().getTarSys());
	}

	/**Original name: 3210-RETRIEVE-SVC-URL<br>
	 * <pre>*****************************************************************
	 *  CALL THE WEB SERVICE URI LOOKUP COMMON ROUTINE
	 * *****************************************************************</pre>*/
	private void retrieveSvcUrl() {
		// COB_CODE: INITIALIZE URI-LKU-LINKAGE.
		initTs571cb1();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO UL-SERVICE-MNEMONIC.
		ws.getTs571cb1().setServiceMnemonic(ws.getConstantFields().getWebSvcId());
		// CALL WEB SERVICE LOOKUP INTERFACE PROGRAM
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (CF-WEB-SVC-LKU-PGM)
		//               COMMAREA (URI-LKU-LINKAGE)
		//               LENGTH   (LENGTH OF URI-LKU-LINKAGE)
		//               RESP     (SA-RESPONSE-CODE)
		//               RESP2    (SA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC08090", execContext).commarea(ws.getTs571cb1()).length(Ts571cb1.Len.TS571CB1)
				.link(ws.getConstantFields().getWebSvcLkuPgm(), new Ts571099());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// CHECK FOR ERROR FROM CICS LINK
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3210         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3210());
			// COB_CODE: MOVE CF-WEB-SVC-LKU-PGM TO EA-03-FAILED-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getConstantFields().getWebSvcLkuPgm());
			// COB_CODE: PERFORM 9110-END-WITH-CICS-LINK-ERROR
			//              THRU 9110-EXIT
			endWithCicsLinkError();
		}
		// CHECK IF RETURNING URI IS NOT VALID
		// COB_CODE: IF UL-URI = SPACES
		//             OR
		//              UL-URI = LOW-VALUES
		//                  THRU 9130-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getTs571cb1().getUri()) || Characters.EQ_LOW.test(ws.getTs571cb1().getUri(), Ts571cb1.Len.URI)) {
			// COB_CODE: MOVE CF-PN-3210         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3210());
			// COB_CODE: PERFORM 9130-END-WITH-WEB-URI-ERR
			//              THRU 9130-EXIT
			endWithWebUriErr();
		}
		// COB_CODE: MOVE UL-URI                 TO SA-WEB-SVC-URL.
		ws.getSaveArea().setWebSvcUrl(ws.getTs571cb1().getUri());
	}

	/**Original name: 3220-RETRIEVE-USR-ID-PWD<br>
	 * <pre>*****************************************************************
	 *  READ THE QUEUE TO GET THE USERID AND PASSWORD.
	 * *****************************************************************</pre>*/
	private void retrieveUsrIdPwd() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS
		//               READQ TS
		//               ITEM  (1)
		//               QNAME ('UIDPCRS1')
		//               INTO  (SA-USR-CREDENTIALS)
		//               RESP  (SA-RESPONSE-CODE)
		//               RESP2 (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(SaveAreaXzc08090.Len.USR_CREDENTIALS);
		TsQueueManager.read(execContext, "UIDPCRS1", ((short) 1), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.getSaveArea().setUsrCredentialsBytes(tsQueueData.getData());
		}
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9140-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3220         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3220());
			// COB_CODE: PERFORM 9140-END-WITH-CICS-READQ-ERROR
			//              THRU 9140-EXIT
			endWithCicsReadqError();
		}
	}

	/**Original name: 3300-SETUP-CONTAINERS<br>
	 * <pre>*****************************************************************
	 *  SETUP THE CONTAINERS FOR CALLING GIICALS
	 * *****************************************************************</pre>*/
	private void setupContainers() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE CF-PN-3300             TO EA-01-PARAGRAPH-NBR.
		ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-IVORYH-CONTAINER)
		//               CHANNEL    (SA-CHN-NM)
		//               FROM       (IVORYH)
		//               FLENGTH    (LENGTH OF IVORYH)
		//               RESP       (SA-RESPONSE-CODE)
		//               RESP2      (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Ivoryh.Len.IVORYH);
		tsOutputData.setData(ws.getIvoryh().getIvoryhBytes());
		Channel.write(execContext, ws.getSaveArea().getChnNmFormatted(), ws.getConstantFields().getContainerInfo().getIvoryhContainerFormatted(),
				tsOutputData);
		channelSet.add(ws.getSaveArea().getChnNmFormatted());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9160-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9160-END-WITH-CONTAINER-ERRORS
			//              THRU 9160-EXIT
			endWithContainerErrors();
		}
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-INP-CONTAINER)
		//               CHANNEL    (SA-CHN-NM)
		//               FROM       (CALLABLE-INPUTS)
		//               FLENGTH    (LENGTH OF CALLABLE-INPUTS)
		//               RESP       (SA-RESPONSE-CODE)
		//               RESP2      (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc08090Data.Len.CALLABLE_INPUTS);
		tsOutputData.setData(ws.getCallableInputsBytes());
		Channel.write(execContext, ws.getSaveArea().getChnNmFormatted(), ws.getConstantFields().getContainerInfo().getInpContainerFormatted(),
				tsOutputData);
		channelSet.add(ws.getSaveArea().getChnNmFormatted());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9160-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9160-END-WITH-CONTAINER-ERRORS
			//              THRU 9160-EXIT
			endWithContainerErrors();
		}
		// COB_CODE: INITIALIZE CALLABLE-OUTPUTS.
		initCallableOutputs();
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-OUP-CONTAINER)
		//               CHANNEL    (SA-CHN-NM)
		//               FROM       (CALLABLE-OUTPUTS)
		//               FLENGTH    (LENGTH OF CALLABLE-OUTPUTS)
		//               RESP       (SA-RESPONSE-CODE)
		//               RESP2      (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc08090Data.Len.CALLABLE_OUTPUTS);
		tsOutputData.setData(ws.getCallableOutputsBytes());
		Channel.write(execContext, ws.getSaveArea().getChnNmFormatted(), ws.getConstantFields().getContainerInfo().getOupContainerFormatted(),
				tsOutputData);
		channelSet.add(ws.getSaveArea().getChnNmFormatted());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9160-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9160-END-WITH-CONTAINER-ERRORS
			//              THRU 9160-EXIT
			endWithContainerErrors();
		}
	}

	/**Original name: 3400-LINK-TO-GIICALS<br>
	 * <pre>*****************************************************************
	 *  LINK TO THE IVORY PROCESSING PROGRAM
	 * *****************************************************************</pre>*/
	private void linkToGiicals() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE CF-PN-3400             TO EA-01-PARAGRAPH-NBR
		ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3400());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM (CF-CALLABLE-SVC-PROGRAM)
		//               CHANNEL (SA-CHN-NM)
		//               RESP    (SA-RESPONSE-CODE)
		//               RESP2   (SA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC08090", execContext).channel(ws.getSaveArea().getChnNmFormatted()).link(ws.getConstantFields().getCallableSvcProgram());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-CALLABLE-SVC-PROGRAM
			//                                   TO EA-03-FAILED-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getConstantFields().getCallableSvcProgram());
			// COB_CODE: PERFORM 9110-END-WITH-CICS-LINK-ERROR
			//              THRU 9110-EXIT
			endWithCicsLinkError();
		}
		//    In order to do any further error checking, we must get the
		//    containers.
		// COB_CODE: EXEC CICS GET
		//               CONTAINER (CF-CI-IVORYH-CONTAINER)
		//               CHANNEL   (SA-CHN-NM)
		//               INTO      (IVORYH)
		//               RESP      (SA-RESPONSE-CODE)
		//               RESP2     (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Ivoryh.Len.IVORYH);
		Channel.read(execContext, ws.getSaveArea().getChnNmFormatted(), ws.getConstantFields().getContainerInfo().getIvoryhContainerFormatted(),
				tsOutputData);
		ws.getIvoryh().setIvoryhBytes(tsOutputData.getData());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9160-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9160-END-WITH-CONTAINER-ERRORS
			//              THRU 9160-EXIT
			endWithContainerErrors();
		}
		// COB_CODE: EXEC CICS GET
		//               CONTAINER (CF-CI-OUP-CONTAINER)
		//               CHANNEL   (SA-CHN-NM)
		//               INTO      (CALLABLE-OUTPUTS)
		//               RESP      (SA-RESPONSE-CODE)
		//               RESP2     (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc08090Data.Len.CALLABLE_OUTPUTS);
		Channel.read(execContext, ws.getSaveArea().getChnNmFormatted(), ws.getConstantFields().getContainerInfo().getOupContainerFormatted(),
				tsOutputData);
		ws.setCallableOutputsBytes(tsOutputData.getData());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9160-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9160-END-WITH-CONTAINER-ERRORS
			//              THRU 9160-EXIT
			endWithContainerErrors();
		}
		// COB_CODE: IF SOAP-RETURNCODE NOT = ZERO
		//                  THRU 9120-EXIT
		//           END-IF.
		if (ws.getIvoryh().getNonStaticFields().getReturncode() != 0) {
			// COB_CODE: PERFORM 9120-END-WITH-SOAP-FAULT-ERROR
			//              THRU 9120-EXIT
			endWithSoapFaultError();
		}
	}

	/**Original name: 3500-MOVE-WEB-SVC-OUTPUT<br>
	 * <pre>*****************************************************************
	 *  MOVE POLICY DETAILS TO THE OUTPUT AREA
	 * *****************************************************************</pre>*/
	private String moveWebSvcOutput() {
		// COB_CODE: INITIALIZE XZC08O-SERVICE-OUTPUTS.
		initServiceOutputs();
		// COB_CODE: MOVE XZ08CO-RET-CD          TO XZC08O-RET-CD.
		ws.getXzc080co().getRetCd().setRetCd(ws.getXz08coServiceOutputs().getXz08coRetCd().getRetCd());
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN XZ08CO-RC-OK
		//           *            THIS WOULD ONLY HAPPEN if THERE IS SOME TYPE OF
		//           *            SOAP FAULT/SYSTEM ERROR WHERE IT NEVER REACHED THE
		//           *            SERVICE.
		//                        CONTINUE
		//                    WHEN XZ08CO-RC-FAULT
		//                        GO TO 3500-EXIT
		//                    WHEN XZ08CO-RC-SYSTEM-ERROR
		//                        GO TO 3500-EXIT
		//                    WHEN XZ08CO-RC-WARNING
		//                           THRU 3520-EXIT
		//                    WHEN OTHER
		//                        CONTINUE
		//                END-EVALUATE.
		switch (ws.getXz08coServiceOutputs().getXz08coRetCd().getRetCd()) {

		case Xz08coRetCd.OK://            THIS WOULD ONLY HAPPEN if THERE IS SOME TYPE OF
			//            SOAP FAULT/SYSTEM ERROR WHERE IT NEVER REACHED THE
			//            SERVICE.
			// COB_CODE: IF NOT XZ08C0-EI-NO-ERR
			//               GO TO 3500-EXIT
			//           END-IF
			if (!ws.getXz08coServiceOutputs().getXz08c0ErrInd().isNoErr()) {
				// COB_CODE: SET XZ08CO-RC-SYSTEM-ERROR
				//                           TO TRUE
				ws.getXz08coServiceOutputs().getXz08coRetCd().setSystemError();
				// COB_CODE: PERFORM 3510-PRC-FATAL-MSGS
				//              THRU 3510-EXIT
				rng3510PrcFatalMsgs();
				// COB_CODE: GO TO 3500-EXIT
				return "3500-EXIT";
			}
			// COB_CODE: CONTINUE
			//continue
			break;

		case Xz08coRetCd.FAULT:// COB_CODE: PERFORM 3510-PRC-FATAL-MSGS
			//              THRU 3510-EXIT
			rng3510PrcFatalMsgs();
			// COB_CODE: GO TO 3500-EXIT
			return "3500-EXIT";

		case Xz08coRetCd.SYSTEM_ERROR:// COB_CODE: PERFORM 3510-PRC-FATAL-MSGS
			//              THRU 3510-EXIT
			rng3510PrcFatalMsgs();
			// COB_CODE: GO TO 3500-EXIT
			return "3500-EXIT";

		case Xz08coRetCd.WARNING:// COB_CODE: PERFORM 3520-PRC-WNG-MSGS
			//              THRU 3520-EXIT
			rng3520PrcWngMsgs();
			break;

		default:// COB_CODE: CONTINUE
			//continue
			break;
		}
		// COB_CODE: MOVE +1                     TO SS-AI.
		ws.setSsAi(((short) 1));
		return "";
	}

	/**Original name: 3500-A<br>*/
	private String a() {
		// COB_CODE: IF SS-AI >= CF-MAX-ADDL-ITS
		//             OR
		//              XZ08CO-CTC-ID(SS-AI) = SPACES
		//               GO TO 3500-EXIT
		//           END-IF.
		if (ws.getSsAi() >= ws.getConstantFields().getMaxAddlIts()
				|| Characters.EQ_SPACE.test(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcId())) {
			// COB_CODE: GO TO 3500-EXIT
			return "";
		}
		// COB_CODE: MOVE XZ08CO-CTC-ID(SS-AI)   TO XZC08O-CTC-ID(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).setCtcId(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcId());
		// COB_CODE: MOVE XZ08CO-CTC-NM(SS-AI)   TO XZC08O-CTC-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getCtcNm()
				.setCtcNmBytes(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcNm().getCtcNmBytes());
		// COB_CODE: MOVE XZ08CO-CN-DSP-NM(SS-AI)
		//                                       TO XZC08O-CN-DSP-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getCtcNm()
				.setDspNm(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcNm().getDspNm());
		// COB_CODE: MOVE XZ08CO-CN-SRT-NM(SS-AI)
		//                                       TO XZC08O-CN-SRT-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getCtcNm()
				.setSrtNm(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcNm().getSrtNm());
		// COB_CODE: MOVE XZ08CO-CN-CMP-NM(SS-AI)
		//                                       TO XZC08O-CN-CMP-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getCtcNm()
				.setCmpNm(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcNm().getCmpNm());
		// COB_CODE: MOVE XZ08CO-CN-LST-NM(SS-AI)
		//                                       TO XZC08O-CN-LST-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getCtcNm()
				.setLstNm(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcNm().getLstNm());
		// COB_CODE: MOVE XZ08CO-CN-FST-NM(SS-AI)
		//                                       TO XZC08O-CN-FST-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getCtcNm()
				.setFstNm(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcNm().getFstNm());
		// COB_CODE: MOVE XZ08CO-CN-MDL-NM(SS-AI)
		//                                       TO XZC08O-CN-MDL-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getCtcNm()
				.setMdlNm(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcNm().getMdlNm());
		// COB_CODE: MOVE XZ08CO-CN-PFX(SS-AI)   TO XZC08O-CN-PFX(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getCtcNm()
				.setPfx(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcNm().getPfx());
		// COB_CODE: MOVE XZ08CO-CN-SFX(SS-AI)   TO XZC08O-CN-SFX(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getCtcNm()
				.setSfx(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getCtcNm().getSfx());
		// COB_CODE: MOVE XZ08CO-ADR-ID(SS-AI)   TO XZC08O-ADR-ID(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setAdrId(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getAdrId());
		// COB_CODE: MOVE XZ08CO-ADR-SEQ-NBR(SS-AI)
		//                                       TO XZC08O-ADR-SEQ-NBR(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setAdrSeqNbr(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getAdrSeqNbr());
		// COB_CODE: MOVE XZ08CO-ADR-1(SS-AI)    TO XZC08O-ADR-1(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setAdr1(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getAdr1());
		// COB_CODE: MOVE XZ08CO-ADR-2(SS-AI)    TO XZC08O-ADR-2(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setAdr2(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getAdr2());
		// COB_CODE: MOVE XZ08CO-CIT-NM(SS-AI)   TO XZC08O-CIT-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setCitNm(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getCitNm());
		// COB_CODE: MOVE XZ08CO-CTY-NM(SS-AI)   TO XZC08O-CTY-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setCtyNm(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getCtyNm());
		// COB_CODE: MOVE XZ08CO-ST(SS-AI)       TO XZC08O-ST(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setXzc08oStBytes(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getStBytes());
		// COB_CODE: MOVE XZ08CO-ST-ABB(SS-AI)   TO XZC08O-ST-ABB(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setStAbb(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getStAbb());
		// COB_CODE: MOVE XZ08CO-ST-NM(SS-AI)    TO XZC08O-ST-NM(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setStNm(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getStNm());
		// COB_CODE: MOVE XZ08CO-PST-CD(SS-AI)   TO XZC08O-PST-CD(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setPstCd(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getPstCd());
		// COB_CODE: MOVE XZ08CO-RLT-TYP(SS-AI)  TO XZC08O-RLT-TYP(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setXzc08oRltTypBytes(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getRltTypBytes());
		// COB_CODE: MOVE XZ08CO-RT-CD(SS-AI)    TO XZC08O-RT-CD(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setRtCd(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getRtCd());
		// COB_CODE: MOVE XZ08CO-RT-DES(SS-AI)   TO XZC08O-RT-DES(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setRtDes(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getRtDes());
		// COB_CODE: MOVE XZ08CO-POL-LVL-IND(SS-AI)
		//                                       TO XZC08O-POL-LVL-IND(SS-AI).
		ws.getXzc080co().getAddlItsInfo(ws.getSsAi()).getAdr()
				.setPolLvlInd(ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(ws.getSsAi()).getAdr().getPolLvlInd());
		// COB_CODE: ADD +1                      TO SS-AI.
		ws.setSsAi(Trunc.toShort(1 + ws.getSsAi(), 4));
		// COB_CODE: GO TO 3500-A.
		return "3500-A";
	}

	/**Original name: 3510-PRC-FATAL-MSGS<br>
	 * <pre>*****************************************************************
	 *  PROCESS FATAL ERRORS
	 * *****************************************************************</pre>*/
	private void prcFatalMsgs() {
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
	}

	/**Original name: 3510-A<br>*/
	private String a1() {
		// COB_CODE: IF SS-ER >= CF-MAX-ERR
		//             OR
		//              XZ08CO-ERR-MSG(SS-ER) = SPACES
		//               GO TO 3510-EXIT
		//           END-IF.
		if (ws.getSsEr() >= ws.getConstantFields().getMaxErr()
				|| Characters.EQ_SPACE.test(ws.getXz08coServiceOutputs().getXz08coErrMsg(ws.getSsEr()))) {
			// COB_CODE: GO TO 3510-EXIT
			return "";
		}
		// COB_CODE: MOVE XZ08CO-ERR-MSG(SS-ER)  TO XZC08O-ERR-MSG(SS-ER).
		ws.getXzc080co().setErrMsg(ws.getSsEr(), ws.getXz08coServiceOutputs().getXz08coErrMsg(ws.getSsEr()));
		// COB_CODE: ADD +1                      TO SS-ER.
		ws.setSsEr(Trunc.toShort(1 + ws.getSsEr(), 4));
		// COB_CODE: GO TO 3510-A.
		return "3510-A";
	}

	/**Original name: 3520-PRC-WNG-MSGS<br>
	 * <pre>*****************************************************************
	 *  PROCESS WARNINGS
	 * *****************************************************************</pre>*/
	private void prcWngMsgs() {
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
	}

	/**Original name: 3520-A<br>*/
	private String a2() {
		// COB_CODE: IF SS-ER >= CF-MAX-ERR
		//             OR
		//              XZ08CO-WNG-MSG(SS-ER) = SPACES
		//               GO TO 3520-EXIT
		//           END-IF.
		if (ws.getSsEr() >= ws.getConstantFields().getMaxErr()
				|| Characters.EQ_SPACE.test(ws.getXz08coServiceOutputs().getXz08coWngMsg(ws.getSsEr()))) {
			// COB_CODE: GO TO 3520-EXIT
			return "";
		}
		// COB_CODE: MOVE XZ08CO-WNG-MSG(SS-ER)  TO XZC08O-WNG-MSG(SS-ER).
		ws.getXzc080co().setWngMsg(ws.getSsEr(), ws.getXz08coServiceOutputs().getXz08coWngMsg(ws.getSsEr()));
		// COB_CODE: ADD +1                      TO SS-ER.
		ws.setSsEr(Trunc.toShort(1 + ws.getSsEr(), 4));
		// COB_CODE: GO TO 3520-A.
		return "3520-A";
	}

	/**Original name: 9000-MOVE-TO-CONTAINER<br>
	 * <pre>***************************************************************
	 *  OUTPUT THE RETURN FOR THE CONSUMER TO CAPTURE                *
	 * ***************************************************************</pre>*/
	private void moveToContainer() {
		TpOutputData tsOutputData = null;
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-SVC-OUT-CONTAINER)
		//               CHANNEL    (SA-CHN-NM)
		//               FROM       (SERVICE-OUTPUTS)
		//               FLENGTH    (LENGTH OF SERVICE-OUTPUTS)
		//               RESP       (SA-RESPONSE-CODE)
		//               RESP2      (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc08090Data.Len.SERVICE_OUTPUTS);
		tsOutputData.setData(ws.getServiceOutputsBytes());
		Channel.write(execContext, ws.getSaveArea().getChnNmFormatted(), ws.getConstantFields().getContainerInfo().getSvcOutContainerFormatted(),
				tsOutputData);
		channelSet.add(ws.getSaveArea().getChnNmFormatted());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
	}

	/**Original name: 9100-END-WITH-FATAL-INP-ERR<br>
	 * <pre>*****************************************************************
	 *  INVALID INPUT ERROR
	 * *****************************************************************</pre>*/
	private void endWithFatalInpErr() {
		// COB_CODE: SET XZC08O-RC-SYSTEM-ERROR  TO TRUE.
		ws.getXzc080co().getRetCd().setSystemError();
		// COB_CODE: MOVE XZC08I-POL-NBR         TO EA-02-POL-NBR.
		ws.getErrorAndAdviceMessages().getEa02InvalidInput().setPolNbr(ws.getXzc080ci().getPolNbr());
		// COB_CODE: MOVE XZC08I-TRM-EXP-DT      TO EA-02-EXP-DT.
		ws.getErrorAndAdviceMessages().getEa02InvalidInput().setExpDt(ws.getXzc080ci().getTrmExpDt());
		// COB_CODE: MOVE EA-02-INVALID-INPUT    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa02InvalidInput().getEa02InvalidInputFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC08O-ERR-MSG(SS-ER).
		ws.getXzc080co().setErrMsg(ws.getSsEr(), ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9000-MOVE-TO-CONTAINER
		//              THRU 9000-EXIT.
		moveToContainer();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9110-END-WITH-CICS-LINK-ERROR<br>
	 * <pre>*****************************************************************
	 *  CICS LINK ERROR
	 * *****************************************************************</pre>*/
	private void endWithCicsLinkError() {
		// COB_CODE: SET XZC08O-RC-SYSTEM-ERROR  TO TRUE.
		ws.getXzc080co().getRetCd().setSystemError();
		// COB_CODE: MOVE SA-RESPONSE-CODE       TO EA-03-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode(ws.getSaveArea().getResponseCode());
		// COB_CODE: MOVE SA-RESPONSE-CODE2      TO EA-03-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode2(ws.getSaveArea().getResponseCode2());
		// COB_CODE: MOVE EA-03-CICS-LINK-ERROR  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa03CicsLinkError().getEa03CicsLinkErrorFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC08O-ERR-MSG(SS-ER).
		ws.getXzc080co().setErrMsg(ws.getSsEr(), ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9000-MOVE-TO-CONTAINER
		//              THRU 9000-EXIT.
		moveToContainer();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9120-END-WITH-SOAP-FAULT-ERROR<br>
	 * <pre>*****************************************************************
	 *  SOAP FAULT ERROR
	 * *****************************************************************</pre>*/
	private void endWithSoapFaultError() {
		// COB_CODE: SET XZC08O-RC-FAULT         TO TRUE.
		ws.getXzc080co().getRetCd().setFault();
		// COB_CODE: MOVE SOAP-RETURNCODE        TO EA-04-SOAP-RETURN-CODE.
		ws.getErrorAndAdviceMessages().getEa04SoapFault().setEa04SoapReturnCode(((int) (ws.getIvoryh().getNonStaticFields().getReturncode())));
		// COB_CODE: MOVE EA-04-SOAP-FAULT       TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error().setErrorMessage(ws.getErrorAndAdviceMessages().getEa04SoapFault().getEa04SoapFaultFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC08O-ERR-MSG(SS-ER).
		ws.getXzc080co().setErrMsg(ws.getSsEr(), ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9000-MOVE-TO-CONTAINER
		//              THRU 9000-EXIT.
		moveToContainer();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9130-END-WITH-WEB-URI-ERR<br>
	 * <pre>*****************************************************************
	 *  WEB URI ERROR
	 * *****************************************************************</pre>*/
	private void endWithWebUriErr() {
		// COB_CODE: SET XZC08O-RC-FAULT         TO TRUE.
		ws.getXzc080co().getRetCd().setFault();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO EA-05-WEB-SVC-ID.
		ws.getErrorAndAdviceMessages().getEa05WebUriFault().setEa05WebSvcId(ws.getConstantFields().getWebSvcId());
		// COB_CODE: MOVE EA-05-WEB-URI-FAULT    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa05WebUriFault().getEa05WebUriFaultFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC08O-ERR-MSG(SS-ER).
		ws.getXzc080co().setErrMsg(ws.getSsEr(), ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9000-MOVE-TO-CONTAINER
		//              THRU 9000-EXIT.
		moveToContainer();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9140-END-WITH-CICS-READQ-ERROR<br>
	 * <pre>*****************************************************************
	 *  CICS READQ ERROR
	 * *****************************************************************</pre>*/
	private void endWithCicsReadqError() {
		// COB_CODE: SET XZC08O-RC-SYSTEM-ERROR  TO TRUE.
		ws.getXzc080co().getRetCd().setSystemError();
		// COB_CODE: MOVE SA-RESPONSE-CODE       TO EA-07-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa07ReadqError().setCode(ws.getSaveArea().getResponseCode());
		// COB_CODE: MOVE SA-RESPONSE-CODE2      TO EA-07-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa07ReadqError().setCode2(ws.getSaveArea().getResponseCode2());
		// COB_CODE: MOVE EA-07-READQ-ERROR      TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa07ReadqError().getEa07ReadqErrorFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC08O-ERR-MSG(SS-ER).
		ws.getXzc080co().setErrMsg(ws.getSsEr(), ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9000-MOVE-TO-CONTAINER
		//              THRU 9000-EXIT.
		moveToContainer();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9150-END-WITH-CICS-APPLID-ERR<br>
	 * <pre>*****************************************************************
	 *  TARGET SYSTEM NOT FOUND ERROR
	 * *****************************************************************</pre>*/
	private void endWithCicsApplidErr() {
		// COB_CODE: SET XZC08O-RC-SYSTEM-ERROR  TO TRUE.
		ws.getXzc080co().getRetCd().setSystemError();
		// COB_CODE: MOVE SA-CICS-APPLID         TO EA-08-CICS-RGN.
		ws.getErrorAndAdviceMessages().getEa08TarSysNotFnd().setEa08CicsRgn(ws.getSaveArea().getCicsApplid().getCicsApplid());
		// COB_CODE: MOVE EA-08-TAR-SYS-NOT-FND  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa08TarSysNotFnd().getEa08TarSysNotFndFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC08O-ERR-MSG(SS-ER).
		ws.getXzc080co().setErrMsg(ws.getSsEr(), ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9000-MOVE-TO-CONTAINER
		//              THRU 9000-EXIT.
		moveToContainer();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9160-END-WITH-CONTAINER-ERRORS<br>
	 * <pre>**********************************************************
	 *   REPORT ON CONTAINER ERRORS, SET PARAGRAPH BEFOREHAND   *
	 *         MOVE CF-PN-9999         TO EA-01-PARAGRAPH-NBR   *
	 * **********************************************************</pre>*/
	private void endWithContainerErrors() {
		// COB_CODE: SET XZC08O-RC-SYSTEM-ERROR  TO TRUE.
		ws.getXzc080co().getRetCd().setSystemError();
		// COB_CODE: MOVE SA-RESPONSE-CODE       TO EA-06-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode(ws.getSaveArea().getResponseCode());
		// COB_CODE: MOVE SA-RESPONSE-CODE2      TO EA-06-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode2(ws.getSaveArea().getResponseCode2());
		// COB_CODE: MOVE EA-06-CONTAINER-ERROR  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa06ContainerError().getEa06ContainerErrorFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC08O-ERR-MSG(SS-ER).
		ws.getXzc080co().setErrMsg(ws.getSsEr(), ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9000-MOVE-TO-CONTAINER
		//              THRU 9000-EXIT.
		moveToContainer();
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: RNG_3500-MOVE-WEB-SVC-OUTPUT-_-3500-EXIT<br>*/
	private void rng3500MoveWebSvcOutput() {
		String retcode = "";
		boolean goto3500A = false;
		boolean goto3500Exit = false;
		retcode = moveWebSvcOutput();
		if (!retcode.equals("3500-EXIT")) {
			do {
				goto3500A = false;
				retcode = a();
			} while (retcode.equals("3500-A"));
		}
		goto3500Exit = false;
	}

	/**Original name: RNG_3510-PRC-FATAL-MSGS-_-3510-EXIT<br>*/
	private void rng3510PrcFatalMsgs() {
		String retcode = "";
		boolean goto3510A = false;
		prcFatalMsgs();
		do {
			goto3510A = false;
			retcode = a1();
		} while (retcode.equals("3510-A"));
	}

	/**Original name: RNG_3520-PRC-WNG-MSGS-_-3520-EXIT<br>*/
	private void rng3520PrcWngMsgs() {
		String retcode = "";
		boolean goto3520A = false;
		prcWngMsgs();
		do {
			goto3520A = false;
			retcode = a2();
		} while (retcode.equals("3520-A"));
	}

	public void initCallableInputs() {
		ws.getXz08ci1i().setUri("");
		ws.getXz08ci1i().setUsrId("");
		ws.getXz08ci1i().setPwd("");
		ws.getXz08ci1i().setTarSys("");
		ws.getXz08ci1i().setPolNbr("");
		ws.getXz08ci1i().setTrmExpDt("");
	}

	public void initTs571cb1() {
		ws.getTs571cb1().setServiceMnemonic("");
		ws.getTs571cb1().setUri("");
		ws.getTs571cb1().setCicsSysid("");
		ws.getTs571cb1().setServicePrefix("");
	}

	public void initCallableOutputs() {
		ws.getXz08coServiceOutputs().getXz08coRetCd().setRetCd(((short) 0));
		for (int idx0 = 1; idx0 <= Xz08coServiceOutputs.XZ08CO_ERR_MSG_MAXOCCURS; idx0++) {
			ws.getXz08coServiceOutputs().setXz08coErrMsg(idx0, "");
		}
		for (int idx0 = 1; idx0 <= Xz08coServiceOutputs.XZ08CO_WNG_MSG_MAXOCCURS; idx0++) {
			ws.getXz08coServiceOutputs().setXz08coWngMsg(idx0, "");
		}
		for (int idx0 = 1; idx0 <= Xz08coServiceOutputs.XZ08CO_ADDL_ITS_INFO_MAXOCCURS; idx0++) {
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).setCtcId("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getCtcNm().setDspNm("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getCtcNm().setSrtNm("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getCtcNm().setCmpNm("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getCtcNm().setLstNm("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getCtcNm().setFstNm("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getCtcNm().setMdlNm("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getCtcNm().setPfx("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getCtcNm().setSfx("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setAdrId("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setAdrSeqNbr(0);
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setAdr1("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setAdr2("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setCitNm("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setCtyNm("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setStAbb("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setStNm("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setPstCd("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setRtCd("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setRtDes("");
			ws.getXz08coServiceOutputs().getXz08coAddlItsInfo(idx0).getAdr().setPolLvlInd(Types.SPACE_CHAR);
		}
		ws.getXz08coServiceOutputs().getXz08c0ErrInd().setXz08c0ErrInd("");
		ws.getXz08coServiceOutputs().setXz08c0FaultCd("");
		ws.getXz08coServiceOutputs().setXz08c0FaultString("");
		ws.getXz08coServiceOutputs().setXz08c0FaultActor("");
		ws.getXz08coServiceOutputs().setXz08c0FaultDetail("");
		ws.getXz08coServiceOutputs().setXz08c0NonSoapErrTxt("");
	}

	public void initServiceOutputs() {
		ws.getXzc080co().getRetCd().setRetCd(((short) 0));
		for (int idx0 = 1; idx0 <= Xzc080co.ERR_MSG_MAXOCCURS; idx0++) {
			ws.getXzc080co().setErrMsg(idx0, "");
		}
		for (int idx0 = 1; idx0 <= Xzc080co.WNG_MSG_MAXOCCURS; idx0++) {
			ws.getXzc080co().setWngMsg(idx0, "");
		}
		for (int idx0 = 1; idx0 <= Xzc080co.ADDL_ITS_INFO_MAXOCCURS; idx0++) {
			ws.getXzc080co().getAddlItsInfo(idx0).setCtcId("");
			ws.getXzc080co().getAddlItsInfo(idx0).getCtcNm().setDspNm("");
			ws.getXzc080co().getAddlItsInfo(idx0).getCtcNm().setSrtNm("");
			ws.getXzc080co().getAddlItsInfo(idx0).getCtcNm().setCmpNm("");
			ws.getXzc080co().getAddlItsInfo(idx0).getCtcNm().setLstNm("");
			ws.getXzc080co().getAddlItsInfo(idx0).getCtcNm().setFstNm("");
			ws.getXzc080co().getAddlItsInfo(idx0).getCtcNm().setMdlNm("");
			ws.getXzc080co().getAddlItsInfo(idx0).getCtcNm().setPfx("");
			ws.getXzc080co().getAddlItsInfo(idx0).getCtcNm().setSfx("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setAdrId("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setAdrSeqNbr(0);
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setAdr1("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setAdr2("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setCitNm("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setCtyNm("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setStAbb("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setStNm("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setPstCd("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setRtCd("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setRtDes("");
			ws.getXzc080co().getAddlItsInfo(idx0).getAdr().setPolLvlInd(Types.SPACE_CHAR);
		}
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
