/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ08CO-ADR<br>
 * Variable: XZ08CO-ADR from copybook XZ08CI1O<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz08coAdr {

	//==== PROPERTIES ====
	//Original name: XZ08CO-ADR-ID
	private String adrId = DefaultValues.stringVal(Len.ADR_ID);
	//Original name: XZ08CO-ADR-SEQ-NBR
	private int adrSeqNbr = DefaultValues.INT_VAL;
	//Original name: XZ08CO-ADR-1
	private String adr1 = DefaultValues.stringVal(Len.ADR1);
	//Original name: XZ08CO-ADR-2
	private String adr2 = DefaultValues.stringVal(Len.ADR2);
	//Original name: XZ08CO-CIT-NM
	private String citNm = DefaultValues.stringVal(Len.CIT_NM);
	//Original name: XZ08CO-CTY-NM
	private String ctyNm = DefaultValues.stringVal(Len.CTY_NM);
	//Original name: XZ08CO-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZ08CO-ST-NM
	private String stNm = DefaultValues.stringVal(Len.ST_NM);
	//Original name: XZ08CO-PST-CD
	private String pstCd = DefaultValues.stringVal(Len.PST_CD);
	//Original name: XZ08CO-RT-CD
	private String rtCd = DefaultValues.stringVal(Len.RT_CD);
	//Original name: XZ08CO-RT-DES
	private String rtDes = DefaultValues.stringVal(Len.RT_DES);
	//Original name: XZ08CO-POL-LVL-IND
	private char polLvlInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setAdrBytes(byte[] buffer, int offset) {
		int position = offset;
		adrId = MarshalByte.readString(buffer, position, Len.ADR_ID);
		position += Len.ADR_ID;
		adrSeqNbr = MarshalByte.readInt(buffer, position, Len.ADR_SEQ_NBR);
		position += Len.ADR_SEQ_NBR;
		adr1 = MarshalByte.readString(buffer, position, Len.ADR1);
		position += Len.ADR1;
		adr2 = MarshalByte.readString(buffer, position, Len.ADR2);
		position += Len.ADR2;
		citNm = MarshalByte.readString(buffer, position, Len.CIT_NM);
		position += Len.CIT_NM;
		ctyNm = MarshalByte.readString(buffer, position, Len.CTY_NM);
		position += Len.CTY_NM;
		setStBytes(buffer, position);
		position += Len.ST;
		pstCd = MarshalByte.readString(buffer, position, Len.PST_CD);
		position += Len.PST_CD;
		setRltTypBytes(buffer, position);
		position += Len.RLT_TYP;
		polLvlInd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getAdrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, adrId, Len.ADR_ID);
		position += Len.ADR_ID;
		MarshalByte.writeInt(buffer, position, adrSeqNbr, Len.ADR_SEQ_NBR);
		position += Len.ADR_SEQ_NBR;
		MarshalByte.writeString(buffer, position, adr1, Len.ADR1);
		position += Len.ADR1;
		MarshalByte.writeString(buffer, position, adr2, Len.ADR2);
		position += Len.ADR2;
		MarshalByte.writeString(buffer, position, citNm, Len.CIT_NM);
		position += Len.CIT_NM;
		MarshalByte.writeString(buffer, position, ctyNm, Len.CTY_NM);
		position += Len.CTY_NM;
		getStBytes(buffer, position);
		position += Len.ST;
		MarshalByte.writeString(buffer, position, pstCd, Len.PST_CD);
		position += Len.PST_CD;
		getRltTypBytes(buffer, position);
		position += Len.RLT_TYP;
		MarshalByte.writeChar(buffer, position, polLvlInd);
		return buffer;
	}

	public void initAdrLowValues() {
		adrId = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.ADR_ID);
		adrSeqNbr = Types.LOW_INT_VAL;
		adr1 = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.ADR1);
		adr2 = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.ADR2);
		citNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CIT_NM);
		ctyNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CTY_NM);
		initStLowValues();
		pstCd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.PST_CD);
		initRltTypLowValues();
		polLvlInd = Types.LOW_CHAR_VAL;
	}

	public void initAdrSpaces() {
		adrId = "";
		adrSeqNbr = Types.INVALID_INT_VAL;
		adr1 = "";
		adr2 = "";
		citNm = "";
		ctyNm = "";
		initStSpaces();
		pstCd = "";
		initRltTypSpaces();
		polLvlInd = Types.SPACE_CHAR;
	}

	public void setAdrId(String adrId) {
		this.adrId = Functions.subString(adrId, Len.ADR_ID);
	}

	public String getAdrId() {
		return this.adrId;
	}

	public void setAdrSeqNbr(int adrSeqNbr) {
		this.adrSeqNbr = adrSeqNbr;
	}

	public int getAdrSeqNbr() {
		return this.adrSeqNbr;
	}

	public void setAdr1(String adr1) {
		this.adr1 = Functions.subString(adr1, Len.ADR1);
	}

	public String getAdr1() {
		return this.adr1;
	}

	public void setAdr2(String adr2) {
		this.adr2 = Functions.subString(adr2, Len.ADR2);
	}

	public String getAdr2() {
		return this.adr2;
	}

	public void setCitNm(String citNm) {
		this.citNm = Functions.subString(citNm, Len.CIT_NM);
	}

	public String getCitNm() {
		return this.citNm;
	}

	public void setCtyNm(String ctyNm) {
		this.ctyNm = Functions.subString(ctyNm, Len.CTY_NM);
	}

	public String getCtyNm() {
		return this.ctyNm;
	}

	public void setXzc08oStBytes(byte[] buffer) {
		setStBytes(buffer, 1);
	}

	/**Original name: XZ08CO-ST<br>*/
	public byte[] getStBytes() {
		byte[] buffer = new byte[Len.ST];
		return getStBytes(buffer, 1);
	}

	public void setStBytes(byte[] buffer, int offset) {
		int position = offset;
		stAbb = MarshalByte.readString(buffer, position, Len.ST_ABB);
		position += Len.ST_ABB;
		stNm = MarshalByte.readString(buffer, position, Len.ST_NM);
	}

	public byte[] getStBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeString(buffer, position, stNm, Len.ST_NM);
		return buffer;
	}

	public void initStLowValues() {
		stAbb = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.ST_ABB);
		stNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.ST_NM);
	}

	public void initStSpaces() {
		stAbb = "";
		stNm = "";
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public void setStNm(String stNm) {
		this.stNm = Functions.subString(stNm, Len.ST_NM);
	}

	public String getStNm() {
		return this.stNm;
	}

	public void setPstCd(String pstCd) {
		this.pstCd = Functions.subString(pstCd, Len.PST_CD);
	}

	public String getPstCd() {
		return this.pstCd;
	}

	public void setXzc08oRltTypBytes(byte[] buffer) {
		setRltTypBytes(buffer, 1);
	}

	/**Original name: XZ08CO-RLT-TYP<br>*/
	public byte[] getRltTypBytes() {
		byte[] buffer = new byte[Len.RLT_TYP];
		return getRltTypBytes(buffer, 1);
	}

	public void setRltTypBytes(byte[] buffer, int offset) {
		int position = offset;
		rtCd = MarshalByte.readString(buffer, position, Len.RT_CD);
		position += Len.RT_CD;
		rtDes = MarshalByte.readString(buffer, position, Len.RT_DES);
	}

	public byte[] getRltTypBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, rtCd, Len.RT_CD);
		position += Len.RT_CD;
		MarshalByte.writeString(buffer, position, rtDes, Len.RT_DES);
		return buffer;
	}

	public void initRltTypLowValues() {
		rtCd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.RT_CD);
		rtDes = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.RT_DES);
	}

	public void initRltTypSpaces() {
		rtCd = "";
		rtDes = "";
	}

	public void setRtCd(String rtCd) {
		this.rtCd = Functions.subString(rtCd, Len.RT_CD);
	}

	public String getRtCd() {
		return this.rtCd;
	}

	public void setRtDes(String rtDes) {
		this.rtDes = Functions.subString(rtDes, Len.RT_DES);
	}

	public String getRtDes() {
		return this.rtDes;
	}

	public void setPolLvlInd(char polLvlInd) {
		this.polLvlInd = polLvlInd;
	}

	public char getPolLvlInd() {
		return this.polLvlInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ADR_ID = 20;
		public static final int ADR1 = 45;
		public static final int ADR2 = 45;
		public static final int CIT_NM = 30;
		public static final int CTY_NM = 30;
		public static final int ST_ABB = 3;
		public static final int ST_NM = 25;
		public static final int PST_CD = 13;
		public static final int RT_CD = 4;
		public static final int RT_DES = 35;
		public static final int ADR_SEQ_NBR = 5;
		public static final int ST = ST_ABB + ST_NM;
		public static final int RLT_TYP = RT_CD + RT_DES;
		public static final int POL_LVL_IND = 1;
		public static final int ADR = ADR_ID + ADR_SEQ_NBR + ADR1 + ADR2 + CIT_NM + CTY_NM + ST + PST_CD + RLT_TYP + POL_LVL_IND;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
