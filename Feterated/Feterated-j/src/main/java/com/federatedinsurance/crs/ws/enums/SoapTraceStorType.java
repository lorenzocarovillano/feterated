/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SOAP-TRACE-STOR-TYPE<br>
 * Variable: SOAP-TRACE-STOR-TYPE from copybook TS570CB1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SoapTraceStorType {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.TRACE_STOR_TYPE);
	public static final String TSQ = "TS";
	public static final String TDQ = "TD";
	public static final String IVORYFILE = "IF";

	//==== METHODS ====
	public void setTraceStorType(String traceStorType) {
		this.value = Functions.subString(traceStorType, Len.TRACE_STOR_TYPE);
	}

	public String getTraceStorType() {
		return this.value;
	}

	public void setTdq() {
		value = TDQ;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRACE_STOR_TYPE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
