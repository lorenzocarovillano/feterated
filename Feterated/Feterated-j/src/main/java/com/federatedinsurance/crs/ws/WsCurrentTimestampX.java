/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-CURRENT-TIMESTAMP-X<br>
 * Variable: WS-CURRENT-TIMESTAMP-X from program XZ0P0022<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsCurrentTimestampX {

	//==== PROPERTIES ====
	//Original name: WS-CURRENT-DATE
	private String dateFld = DefaultValues.stringVal(Len.DATE_FLD);
	//Original name: FILLER-WS-CURRENT-TIMESTAMP-X
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setWsCurrentTimestamp(String wsCurrentTimestamp) {
		int position = 1;
		byte[] buffer = getWsCurrentTimestampXBytes();
		MarshalByte.writeString(buffer, position, wsCurrentTimestamp, Len.WS_CURRENT_TIMESTAMP);
		setWsCurrentTimestampXBytes(buffer);
	}

	/**Original name: WS-CURRENT-TIMESTAMP<br>*/
	public String getWsCurrentTimestamp() {
		int position = 1;
		return MarshalByte.readString(getWsCurrentTimestampXBytes(), position, Len.WS_CURRENT_TIMESTAMP);
	}

	public void setWsCurrentTimestampXBytes(byte[] buffer) {
		setWsCurrentTimestampXBytes(buffer, 1);
	}

	/**Original name: WS-CURRENT-TIMESTAMP-X<br>*/
	public byte[] getWsCurrentTimestampXBytes() {
		byte[] buffer = new byte[Len.WS_CURRENT_TIMESTAMP_X];
		return getWsCurrentTimestampXBytes(buffer, 1);
	}

	public void setWsCurrentTimestampXBytes(byte[] buffer, int offset) {
		int position = offset;
		dateFld = MarshalByte.readString(buffer, position, Len.DATE_FLD);
		position += Len.DATE_FLD;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getWsCurrentTimestampXBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, dateFld, Len.DATE_FLD);
		position += Len.DATE_FLD;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setDateFld(String dateFld) {
		this.dateFld = Functions.subString(dateFld, Len.DATE_FLD);
	}

	public String getDateFld() {
		return this.dateFld;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DATE_FLD = 10;
		public static final int FLR1 = 16;
		public static final int WS_CURRENT_TIMESTAMP_X = DATE_FLD + FLR1;
		public static final int WS_CURRENT_TIMESTAMP = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_CURRENT_TIMESTAMP = 26;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
