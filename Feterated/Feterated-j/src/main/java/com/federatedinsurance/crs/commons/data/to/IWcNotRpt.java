/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [WC_NOT_RPT]
 * 
 */
public interface IWcNotRpt extends BaseSqlTo {

	/**
	 * Host Variable POL-NBR
	 * 
	 */
	String getPolNbr();

	void setPolNbr(String polNbr);

	/**
	 * Host Variable POL-EFF-DT
	 * 
	 */
	String getPolEffDt();

	void setPolEffDt(String polEffDt);

	/**
	 * Host Variable STA-MDF-TS
	 * 
	 */
	String getStaMdfTs();

	void setStaMdfTs(String staMdfTs);

	/**
	 * Host Variable NOT-EFF-DT
	 * 
	 */
	String getNotEffDt();

	void setNotEffDt(String notEffDt);

	/**
	 * Nullable property for NOT-EFF-DT
	 * 
	 */
	String getNotEffDtObj();

	void setNotEffDtObj(String notEffDtObj);

	/**
	 * Host Variable NOT-TYP-CD
	 * 
	 */
	String getNotTypCd();

	void setNotTypCd(String notTypCd);

	/**
	 * Nullable property for NOT-TYP-CD
	 * 
	 */
	String getNotTypCdObj();

	void setNotTypCdObj(String notTypCdObj);
};
