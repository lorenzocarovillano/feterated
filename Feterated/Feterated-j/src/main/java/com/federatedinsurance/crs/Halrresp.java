/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import static java.lang.Math.abs;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.storage.KeyType;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.ws.DefaultComm;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.HalrrespData;
import com.federatedinsurance.crs.ws.LiHalrrespLinkage;
import com.federatedinsurance.crs.ws.LioUowBusObjData;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.enums.HalrurqaFunction;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: HALRRESP<br>
 * <pre>AUTHOR.       CSC.
 * DATE-WRITTEN. 02 JULY 2001.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - RESPONSE UMT (UHRD/UDAT) ACCESS MODULE.     **
 * *                                                             **
 * * PLATFORM - I-BASE                                           **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -  RESPONSE UMT (UHDR/UDAT) ACCESS MODULE.          **
 * *                                                             **
 * *            UDAT DATA RESPONSE UMT IS MANIPULATED            **
 * *            WITH UHDR HEADER RESPONSE UMT KEPT IN LINE.     **
 * *                                                             **
 * *            FUNCTIONS CURRENTLY AVAILABLE:                   **
 * *                                                             **
 * *            READ      - READ A SPECIFIC RECORD.              **
 * *            WRITE     - WRITE A DATA UMT RECORD.             **
 * *            UPDATE    - UPDATE A SPECIFIC RECORD.            **
 * *            ALLDEL    - DELETE ALL UDAT/UHDR ROWS FOR MSGID. **
 * *            DELBO     - DELETE ALL UDAT/UHDR FOR A BUS OBJ.  **
 * *                                                             **
 * * HOW TO USE THIS ROUTINE:                                    **
 * *                                                             **
 * * 1. READ FUNCTION - READ A SPECIFIC RECORD                   **
 * *                                                             **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRRESP-READ-FUNC TO TRUE.                       **
 * *       MOVE WS-POLICY-TAB-V   TO HALRRESP-BUS-OBJ-NM.        **
 * *       MOVE 2                 TO HALRRESP-REC-SEQ.           **
 * *                                                             **
 * *       CALL HALRRESP-HALRRESP-LIT USING                      **
 * *            DFHEIBLK                                         **
 * *            DFHCOMMAREA                                      **
 * *            UBOC-RECORD                                      **
 * *            WS-HALRRESP-LINKAGE                              **
 * *            PB01C-POLICY-TAB-ROW.                            **
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *                                                             **
 * *      (IF RECORD FOUND:)                                     **
 * *       HALRRESP-REC-FOUND        - SET TO TRUE               **
 * *       HALRRESP-BUS-OBJ-DATA-LEN - SET TO LENGTH OF DATA     **
 * *                                   OBJECT RETURNED           **
 * *       RECORD LAYOUT (3RD PARAM) - CONTAINS RECORD DATA      **
 * *                                                             **
 * *      (IF RECORD NOT FOUND:)                                 **
 * *       HALRRESP-REC-NOT-FOUND    - SET TO TRUE               **
 * *       HALRRESP-BUS-OBJ-DATA-LEN - SET TO 0                  **
 * *       RECORD LAYOUT (3RD PARAM) - CONTAINS ALL '?'          **
 * *                                                             **
 * *                                                             **
 * * 2. WRITE FUNCTION - WRITE A NEW RECORD                      **
 * *                                                             **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRRESP-WRITE-FUNC TO TRUE.                       *
 * *       MOVE WS-POLICY-TAB-V    TO HALRRESP-BUS-OBJ-NM.        *
 * *       MOVE LENGTH OF PB01C-POLICY-TAB-ROW                    *
 * *                               TO HALRRESP-BUS-OBJ-DATA-LEN.  *
 * *                                                              *
 * *       CALL HALRRESP-HALRRESP-LIT USING                       *
 * *            DFHEIBLK                                          *
 * *            DFHCOMMAREA                                       *
 * *            UBOC-RECORD                                       *
 * *            WS-HALRRESP-LINKAGE                               *
 * *            PB01C-POLICY-TAB-ROW.                             *
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *                                                             **
 * *       HALRRESP-REC-SEQ          - SET TO REC-SEQ USED FOR   **
 * *                                   WRITE                     **
 * *                                                             **
 * *                                                             **
 * * 3. UPDATE FUNCTION - UPDATE A SPECIFIC RECORD               **
 * *                                                             **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRRESP-UPDATE-FUNC TO TRUE.                     **
 * *       MOVE WS-POLICY-TAB-V   TO HALRRESP-BUS-OBJ-NM.        **
 * *       MOVE 2                 TO HALRRESP-REC-SEQ.           **
 * *       MOVE 2.5               TO PB01C-FLAT-COM-AMT.         **
 * *                                                             **
 * *       CALL HALRRESP-HALRRESP-LIT USING                      **
 * *            DFHEIBLK                                         **
 * *            DFHCOMMAREA                                      **
 * *            UBOC-RECORD                                      **
 * *            WS-HALRRESP-LINKAGE                              **
 * *            PB01C-POLICY-TAB-ROW.                            **
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *                                                             **
 * *       (NONE)                                                **
 * *                                                             **
 * *                                                             **
 * * 4. ALLDEL FUNCTION - DELETE ALL ROWS FOR A SPECIFIC MSG ID  **
 * *                                                             **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRRESP-ALLDEL-FUNC TO TRUE.                     **
 * *                                                             **
 * *       CALL HALRRESP-HALRRESP-LIT USING                      **
 * *            DFHEIBLK                                         **
 * *            DFHCOMMAREA                                      **
 * *            UBOC-RECORD                                      **
 * *            WS-HALRRESP-LINKAGE.                             **
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *                                                             **
 * *       (NONE)                                                **
 * *                                                             **
 * * 5. DELBO  FUNCTION - DELETE ALL ROWS FOR A SPECIFIC BUS OBJ **
 * *                      IN THE MESSAGE.                        **
 * *                                                             **
 * *    A. SAMPLE CALL:                                          **
 * *                                                             **
 * *       SET HALRRESP-DELBO-FUNC TO TRUE.                      **
 * *       MOVE WS-POLICY-TAB-V    TO HALRRESP-BUS-OBJ-NM.       **
 * *                                                             **
 * *       CALL HALRRESP-HALRRESP-LIT USING                      **
 * *            DFHEIBLK                                         **
 * *            DFHCOMMAREA                                      **
 * *            UBOC-RECORD                                      **
 * *            WS-HALRRESP-LINKAGE.                             **
 * *                                                             **
 * *       UBOC-HALT-AND-RETURN SET IF FATAL ERROR ENCOUNTERED.  **
 * *                                                             **
 * *    B. DATA RETURNED:                                        **
 * *                                                             **
 * *       (NONE)                                                **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED IN THE FOLLOW-**
 * *                       WAYS:                                 **
 * *                                                             **
 * *                       THIS MODULE IS DYNAMICALLY CALLED     **
 * *                       BY ANY MODULE REQUIRING ITS SERVICES. **
 * *                                                             **
 * * DATA ACCESS METHODS - UMT (VSAM), DB2.                      **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * --------  ---------  --------   ----------------------------**
 * * 15756     02 JUL 00  JAF (NU)   NEW                         **
 * * 17543I    11 FEB 02  18448      RETROFIT UK CODE FOR ALLDEL.**
 * *                                 ADD CODE FOR DELBO.         **
 * * 17241     20 FEB 02  18448      REMOVE REFERENCES TO IAP.   **
 * * 21245     23 APR 02  18448      ONLY DELETE SPECIFIED BO.   **
 * *                                 END BROWSE EVEN IF ERROR HAS**
 * *                                 OCCURRED.                   **
 * * 55282     08 AUG 05  18448      USE UBOC-HALT-AND-RETURN NOT**
 * *                                 "NOT UBOC-UOW-OK".          **
 * ****************************************************************</pre>*/
public class Halrresp extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>****************************************************************
	 * **MYND*  START OF:                             *SAVANNAH 2.0****
	 * **MYND*                                        *SAVANNAH 2.0****
	 * **MYND*  GENERIC         WORKING-STORAGE       *SAVANNAH 2.0****
	 * **MYND*  (SPECIFIC TO BPOS)                    *SAVANNAH 2.0****
	 * ****************************************************************
	 * * REPLACE HALLJWSA WITH HALLJWSB.
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NO IAP REFERENCES)                *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 OCT. 16, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *  CASE 17241  PRGMR AICI448          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private ExecContext execContext = null;
	//Original name: DEFAULT-COMM
	private DefaultComm defaultComm;
	//Original name: WORKING-STORAGE
	private HalrrespData ws = new HalrrespData();
	//Original name: LI-UBOC
	private Dfhcommarea liUboc;
	//Original name: LI-HALRRESP-LINKAGE
	private LiHalrrespLinkage liHalrrespLinkage;
	//Original name: LIO-UOW-BUS-OBJ-DATA
	private LioUowBusObjData lioUowBusObjData = new LioUowBusObjData();

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DefaultComm defaultComm, Dfhcommarea liUboc, LiHalrrespLinkage liHalrrespLinkage,
			LioUowBusObjData lioUowBusObjData) {
		this.execContext = execContext;
		this.defaultComm = defaultComm;
		this.liUboc = liUboc;
		this.liHalrrespLinkage = liHalrrespLinkage;
		this.lioUowBusObjData = lioUowBusObjData;
		registerArgListeners();
		mainline();
		mainlineX();
		deleteArgListeners();
		return 0;
	}

	public static Halrresp getInstance() {
		return (Programs.getInstance(Halrresp.class));
	}

	/**Original name: 0000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  MAIN PROCESSING CONTROL                                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 0100-INITIALIZATION.
		initialization();
		//*   IF NOT UBOC-UOW-OK
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-MAINLINE-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-MAINLINE-X
			mainlineX();
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN HALRRESP-READ-FUNC
		//                   PERFORM 0200-READ-FUNC
		//               WHEN HALRRESP-UPDATE-FUNC
		//                   PERFORM 0300-UPDATE-FUNC
		//               WHEN HALRRESP-WRITE-FUNC
		//                   PERFORM 0400-WRITE-FUNC
		//               WHEN HALRRESP-ALLDEL-FUNC
		//                   PERFORM 0500-ALLDEL-FUNC
		//               WHEN HALRRESP-DELBO-FUNC
		//                   PERFORM 0600-DELBO-FUNC
		//               WHEN OTHER
		//                   GO TO 0000-MAINLINE-X
		//           END-EVALUATE.
		switch (liHalrrespLinkage.getFunction().getFunction()) {

		case HalrurqaFunction.READ_FUNC:// COB_CODE: PERFORM 0200-READ-FUNC
			readFunc();
			break;

		case HalrurqaFunction.UPDATE_FUNC:// COB_CODE: PERFORM 0300-UPDATE-FUNC
			updateFunc();
			break;

		case HalrurqaFunction.WRITE_FUNC:// COB_CODE: PERFORM 0400-WRITE-FUNC
			writeFunc();
			break;

		case HalrurqaFunction.ALLDEL_FUNC:// COB_CODE: PERFORM 0500-ALLDEL-FUNC
			alldelFunc();
			break;

		case HalrurqaFunction.DELBO_FUNC:// COB_CODE: PERFORM 0600-DELBO-FUNC
			delboFunc();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INV-ACTION-CODE    OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvActionCode();
			// COB_CODE: MOVE '0000-MAINLINE'
			//                TO EFAL-ERR-PARAGRAPH  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0000-MAINLINE");
			// COB_CODE: MOVE 'INVALID FUNCTION PASSED IN LINKAGE'
			//                TO EFAL-ERR-COMMENT    OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID FUNCTION PASSED IN LINKAGE");
			// COB_CODE: STRING 'HALRRESP-FUNCTION='
			//                  HALRRESP-FUNCTION                     ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HALRRESP-FUNCTION=",
					String.valueOf(liHalrrespLinkage.getFunction().getFunction()), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0000-MAINLINE-X
			mainlineX();
			break;
		}
		//*   PERFORM 0900-TERMINATION.
		// COB_CODE: PERFORM 9999-TERMINATION.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=HALRRESP.CBL:line=468, because the code is unreachable.
	}

	/**Original name: 0000-MAINLINE-X<br>*/
	private void mainlineX() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 0100-INITIALIZATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  INITIALIZATION.                                                *
	 *                                                                 *
	 * *****************************************************************
	 * * INIT WARN/ERR W-S</pre>*/
	private void initialization() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		//* VALIDATE COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(liUboc.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(liUboc.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowRespHeaderStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowRespDataStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowRespWarningsStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(liUboc.getCommInfo().getUbocUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(liUboc.getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 0200-READ-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  READ FUNCTION: READ A SPECIFIC RESPONSE DATA RECORD            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readFunc() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID         TO UDAT-ID.
		ws.getHalludat().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM TO UDAT-BUS-OBJ-NM.
		ws.getHalludat().setBusObjNm(liHalrrespLinkage.getBusObjNm());
		// COB_CODE: MOVE HALRRESP-REC-SEQ    TO UDAT-REC-SEQ.
		ws.getHalludat().setRecSeqFormatted(liHalrrespLinkage.getRecSeqFormatted());
		// COB_CODE: EXEC CICS
		//                READ FILE  (UBOC-UOW-RESP-DATA-STORE)
		//                INTO       (UDAT-COMMON)
		//                RIDFLD     (UDAT-KEY)
		//                KEYLENGTH  (LENGTH OF UDAT-KEY)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Halludat.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalludat().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   MOVE UDAT-UOW-BUS-OBJ-DATA TO LIO-UOW-BUS-OBJ-DATA
		//               WHEN DFHRESP(NOTFND)
		//                   MOVE ALL '?' TO LIO-UOW-BUS-OBJ-DATA
		//               WHEN OTHER
		//                   GO TO 0200-READ-FUNC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET HALRRESP-REC-FOUND TO TRUE
			liHalrrespLinkage.getRecFoundSw().setFound();
			// COB_CODE: SUBTRACT LENGTH OF UDAT-UOW-BUS-OBJ-NM
			//               FROM UDAT-UOW-BUFFER-LENGTH
			//             GIVING HALRRESP-BUS-OBJ-DATA-LEN
			liHalrrespLinkage.setBusObjDataLen(((short) (abs(ws.getHalludat().getUowBufferLength() - Halludat.Len.UOW_BUS_OBJ_NM))));
			// COB_CODE: MOVE UDAT-UOW-BUS-OBJ-DATA TO LIO-UOW-BUS-OBJ-DATA
			lioUowBusObjData.setLioUowBusObjDataFormatted(ws.getHalludat().getUowBusObjDataFormatted());
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET HALRRESP-REC-NOT-FOUND TO TRUE
			liHalrrespLinkage.getRecFoundSw().setNotFound();
			// COB_CODE: MOVE 0       TO HALRRESP-BUS-OBJ-DATA-LEN
			liHalrrespLinkage.setBusObjDataLen(((short) 0));
			// COB_CODE: MOVE ALL '?' TO LIO-UOW-BUS-OBJ-DATA
			lioUowBusObjData.setLioUowBusObjDataFormatted(LiteralGenerator.create("?", lioUowBusObjData.getLioUowBusObjDataSize()));
		} else {
			// COB_CODE: SET WS-LOG-ERROR       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '0200-READ-FUNC'
			//                                  TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0200-READ-FUNC");
			// COB_CODE: MOVE 'READ RESP DATA UMT FAILED'
			//                                  TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ RESP DATA UMT FAILED");
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  'UDAT-BUS-OBJ-NM='    UDAT-BUS-OBJ-NM      ';'
			//                  'UDAT-REC-SEQ='       UDAT-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";", "UDAT-BUS-OBJ-NM=", ws.getHalludat().getBusObjNmFormatted(),
							";", "UDAT-REC-SEQ=", ws.getHalludat().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0200-READ-FUNC-X
			return;
		}
	}

	/**Original name: 0300-UPDATE-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  UPDATE FUNCTION: UPDATE A SPECIFIC RESPONSE DATA RECORD        *
	 *                   AFTER FIRST RE-READING THE RECORD WITH        *
	 *                   UPDATE INTENT TO GET LOCK                     *
	 *                                                                 *
	 * *****************************************************************
	 * * RE-READ THE RECORD WITH UPDATE INTENT TO GET LOCK
	 * *   PERFORM 0320-READ-RESP-DATA-REC.</pre>*/
	private void updateFunc() {
		// COB_CODE: PERFORM 1000-READ-RESP-DATA-REC.
		readRespDataRec();
		//*   IF NOT UBOC-UOW-OK
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0300-UPDATE-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0300-UPDATE-FUNC-X
			return;
		}
		//* UPDATE (RE-WRITE) RESPONSE DATA RECORD
		// COB_CODE: PERFORM 0340-UPDATE-RESP-DATA-REC.
		updateRespDataRec();
		//*   IF NOT UBOC-UOW-OK
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0300-UPDATE-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0300-UPDATE-FUNC-X
			return;
		}
	}

	/**Original name: 0340-UPDATE-RESP-DATA-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  UPDATE (RE-WRITE) RESPONSE DATA RECORD.                        *
	 *                                                                 *
	 * *****************************************************************
	 * * FOLLOWING SO THAT LENGTH OF DATA IN LINKAGE IS CALCULATED
	 * * CORRECTLY:</pre>*/
	private void updateRespDataRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SUBTRACT LENGTH OF UDAT-UOW-BUS-OBJ-NM
		//               FROM UDAT-UOW-BUFFER-LENGTH
		//             GIVING HALRRESP-BUS-OBJ-DATA-LEN
		liHalrrespLinkage.setBusObjDataLen(((short) (abs(ws.getHalludat().getUowBufferLength() - Halludat.Len.UOW_BUS_OBJ_NM))));
		// COB_CODE: MOVE LIO-UOW-BUS-OBJ-DATA TO UDAT-UOW-BUS-OBJ-DATA.
		ws.getHalludat().setUowBusObjData(lioUowBusObjData.getLioUowBusObjDataFormatted());
		// COB_CODE: EXEC CICS
		//               REWRITE
		//               FILE   (UBOC-UOW-RESP-DATA-STORE)
		//               FROM   (UDAT-COMMON)
		//               LENGTH (LENGTH OF UDAT-COMMON)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getHalludat().getCommonBytes());
			iRowDAO.update(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0340-UPDATE-RESP-DATA-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '0340-UPDATE-RESP-DATA-REC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0340-UPDATE-RESP-DATA-REC");
			// COB_CODE: MOVE 'REWRITE OF UOW RESP DATA STORE FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("REWRITE OF UOW RESP DATA STORE FAILED");
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  'UDAT-BUS-OBJ-NM='    UDAT-BUS-OBJ-NM      ';'
			//                  'UDAT-REC-SEQ='       UDAT-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";", "UDAT-BUS-OBJ-NM=", ws.getHalludat().getBusObjNmFormatted(),
							";", "UDAT-REC-SEQ=", ws.getHalludat().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0340-UPDATE-RESP-DATA-REC-X
			return;
		}
	}

	/**Original name: 0400-WRITE-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  WRITE FUNCTION: WRITE A DATA RESPONSE RECORD KEEPING           *
	 *                  HEADER RESPONSE RECORD IN LINE                 *
	 *                                                                 *
	 * *****************************************************************
	 * *   PERFORM 0420-READ-RESP-HDR-REC.</pre>*/
	private void writeFunc() {
		// COB_CODE: PERFORM 1020-READ-RESP-HDR-REC.
		readRespHdrRec();
		//*   IF NOT UBOC-UOW-OK
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0400-WRITE-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0400-WRITE-FUNC-X
			return;
		}
		// COB_CODE: PERFORM 0430-INSERT-RESP-DATA-REC.
		insertRespDataRec();
		//*   IF NOT UBOC-UOW-OK
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0400-WRITE-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0400-WRITE-FUNC-X
			return;
		}
		// COB_CODE: PERFORM 0440-WRI-UPD-RESP-HDR-REC.
		wriUpdRespHdrRec();
		//*   IF NOT UBOC-UOW-OK
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0400-WRITE-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0400-WRITE-FUNC-X
			return;
		}
	}

	/**Original name: 0430-INSERT-RESP-DATA-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  INSERT RECORD INTO RESPONSE DATA UMT.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void insertRespDataRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID         TO UDAT-ID.
		ws.getHalludat().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM TO UDAT-BUS-OBJ-NM.
		ws.getHalludat().setBusObjNm(liHalrrespLinkage.getBusObjNm());
		// COB_CODE: IF W0420-HDR-EXISTS
		//               ADD 1 UHDR-MSG-BUS-OBJ-COUNT GIVING UDAT-REC-SEQ
		//           ELSE
		//               MOVE 1 TO UDAT-REC-SEQ
		//           END-IF.
		if (ws.getWs0420WorkAreas().getW0420HdrExistsSw().isExists()) {
			// COB_CODE: ADD 1 UHDR-MSG-BUS-OBJ-COUNT GIVING UDAT-REC-SEQ
			ws.getHalludat().setRecSeq(Trunc.toInt(1 + ws.getHalluhdr().getMsgBusObjCount(), 5));
		} else {
			// COB_CODE: MOVE 1 TO UDAT-REC-SEQ
			ws.getHalludat().setRecSeq(1);
		}
		// COB_CODE: ADD LENGTH OF HALRRESP-BUS-OBJ-NM
		//               HALRRESP-BUS-OBJ-DATA-LEN
		//               GIVING UDAT-UOW-BUFFER-LENGTH.
		ws.getHalludat().setUowBufferLength(((short) (LiHalrrespLinkage.Len.BUS_OBJ_NM + liHalrrespLinkage.getBusObjDataLen0())));
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM  TO UDAT-UOW-BUS-OBJ-NM.
		ws.getHalludat().setUowBusObjNm(liHalrrespLinkage.getBusObjNm());
		// COB_CODE: MOVE LIO-UOW-BUS-OBJ-DATA TO UDAT-UOW-BUS-OBJ-DATA.
		ws.getHalludat().setUowBusObjData(lioUowBusObjData.getLioUowBusObjDataFormatted());
		// COB_CODE: EXEC CICS
		//               WRITE FILE(UBOC-UOW-RESP-DATA-STORE)
		//               FROM   (UDAT-COMMON)
		//               LENGTH (LENGTH OF UDAT-COMMON)
		//               RIDFLD (UDAT-KEY)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getHalludat().getCommonBytes());
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   MOVE UDAT-REC-SEQ TO HALRRESP-REC-SEQ
		//               WHEN OTHER
		//                   GO TO 0430-INSERT-RESP-DATA-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: MOVE UDAT-REC-SEQ TO HALRRESP-REC-SEQ
			liHalrrespLinkage.setRecSeqFormatted(ws.getHalludat().getRecSeqFormatted());
		} else {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '0430-INSERT-RESP-DATA-REC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0430-INSERT-RESP-DATA-REC");
			// COB_CODE: MOVE 'WRITE TO UOW RESP DATA STORE FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW RESP DATA STORE FAILED");
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  'UDAT-BUS-OBJ-NM='    UDAT-BUS-OBJ-NM      ';'
			//                  'UDAT-REC-SEQ='       UDAT-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";", "UDAT-BUS-OBJ-NM=", ws.getHalludat().getBusObjNmFormatted(),
							";", "UDAT-REC-SEQ=", ws.getHalludat().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0430-INSERT-RESP-DATA-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-DATA-ROWS.
		liUboc.getCommInfo().setUbocNbrDataRows(Trunc.toInt(1 + liUboc.getCommInfo().getUbocNbrDataRows(), 9));
	}

	/**Original name: 0440-WRI-UPD-RESP-HDR-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONTROLS THE                                                   *
	 *  UPDATE OR WRITE (DEPENDING ON PRE-EXISTENCE) OF THE            *
	 *  RESPONSE DATA HEADER ASSOCIATED WITH THE DATA RECORD           *
	 *  JUST WRITTEN.                                                  *
	 *  MAX SEQ NUMBER RECORDED ON THE HEADER.                         *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void wriUpdRespHdrRec() {
		// COB_CODE: IF W0420-HDR-EXISTS
		//               PERFORM 0445-UPDATE-OLD-HDR
		//           ELSE
		//               PERFORM 0450-WRITE-NEW-HDR
		//           END-IF.
		if (ws.getWs0420WorkAreas().getW0420HdrExistsSw().isExists()) {
			// COB_CODE: PERFORM 0445-UPDATE-OLD-HDR
			updateOldHdr();
		} else {
			// COB_CODE: PERFORM 0450-WRITE-NEW-HDR
			writeNewHdr();
		}
	}

	/**Original name: 0445-UPDATE-OLD-HDR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  UPDATE EXISTING RESP HDR RECORD.                               *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void updateOldHdr() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: ADD 1 TO UHDR-MSG-BUS-OBJ-COUNT.
		ws.getHalluhdr().setMsgBusObjCount(Trunc.toInt(1 + ws.getHalluhdr().getMsgBusObjCount(), 5));
		// COB_CODE: EXEC CICS
		//               REWRITE FILE(UBOC-UOW-RESP-HEADER-STORE)
		//               FROM   (UHDR-COMMON)
		//               LENGTH (LENGTH OF UHDR-COMMON)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getHalluhdr().getCommonBytes());
			iRowDAO.update(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0445-UPDATE-OLD-HDR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '0445-UPDATE-OLD-HDR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0445-UPDATE-OLD-HDR");
			// COB_CODE: MOVE 'UPDATE OF UOW RESP HEADER STORE FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF UOW RESP HEADER STORE FAILED");
			// COB_CODE: STRING 'UHDR-ID='            UHDR-ID              ';'
			//                  'UHDR-BUS-OBJ-NM='    UHDR-BUS-OBJ-NM      ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "UHDR-ID=", ws.getHalluhdr().getIdFormatted(),
					";", "UHDR-BUS-OBJ-NM=", ws.getHalluhdr().getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0445-UPDATE-OLD-HDR-X
			return;
		}
	}

	/**Original name: 0450-WRITE-NEW-HDR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  WRITE NEW RESP HDR RECORD.                                     *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNewHdr() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID               TO UHDR-ID.
		ws.getHalluhdr().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM       TO UHDR-BUS-OBJ-NM.
		ws.getHalluhdr().setBusObjNm(liHalrrespLinkage.getBusObjNm());
		// COB_CODE: MOVE LENGTH OF UHDR-UOW-MESSAGE-BUFFER
		//                                          TO UHDR-UOW-BUFFER-LENGTH.
		ws.getHalluhdr().setUowBufferLength(((short) Halluhdr.Len.UOW_MESSAGE_BUFFER));
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM       TO UHDR-MSG-BUS-OBJ-NM.
		ws.getHalluhdr().setMsgBusObjNm(liHalrrespLinkage.getBusObjNm());
		// COB_CODE: MOVE 1                         TO UHDR-MSG-BUS-OBJ-COUNT.
		ws.getHalluhdr().setMsgBusObjCount(1);
		// COB_CODE: ADD LENGTH OF HALRRESP-BUS-OBJ-NM
		//               HALRRESP-BUS-OBJ-DATA-LEN
		//                                      GIVING UHDR-MSG-BUS-OBJ-DATA-LEN.
		ws.getHalluhdr().setMsgBusObjDataLen(((short) (LiHalrrespLinkage.Len.BUS_OBJ_NM + liHalrrespLinkage.getBusObjDataLen0())));
		// COB_CODE: EXEC CICS
		//               WRITE FILE(UBOC-UOW-RESP-HEADER-STORE)
		//               FROM   (UHDR-COMMON)
		//               LENGTH (LENGTH OF UHDR-COMMON)
		//               RIDFLD (UHDR-KEY)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getHalluhdr().getCommonBytes());
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0450-WRITE-NEW-HDR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '0450-WRITE-NEW-HDR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0450-WRITE-NEW-HDR");
			// COB_CODE: MOVE 'WRITE TO UOW RESP HEADER STORE FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW RESP HEADER STORE FAILED");
			// COB_CODE: STRING 'UHDR-ID='            UHDR-ID              ';'
			//                  'UHDR-BUS-OBJ-NM='    UHDR-BUS-OBJ-NM      ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "UHDR-ID=", ws.getHalluhdr().getIdFormatted(),
					";", "UHDR-BUS-OBJ-NM=", ws.getHalluhdr().getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0450-WRITE-NEW-HDR-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-HDR-ROWS.
		liUboc.getCommInfo().setUbocNbrHdrRows(Trunc.toInt(1 + liUboc.getCommInfo().getUbocNbrHdrRows(), 9));
	}

	/**Original name: 0500-ALLDEL-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ALLDEL FUNCTION: DELETE ALL RESPONSE UMT ROWS FOR A GIVEN MSGID*
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void alldelFunc() {
		// COB_CODE: SET  WS-UHDR-STBROWSE-NOT-OK TO TRUE.
		ws.getWs0420WorkAreas().getsUhdrBrowseSw().setNotOk();
		// COB_CODE: SET  WS-UDAT-STBROWSE-NOT-OK TO TRUE.
		ws.getWs0420WorkAreas().getsUdatBrowseSw().setNotOk();
		// COB_CODE: SET  WS-UHDR-ROW-EXIST       TO TRUE.
		ws.getWs0420WorkAreas().getsUhdrRowSw().setRowExist();
		// COB_CODE: SET  WS-UDAT-ROW-EXIST       TO TRUE.
		ws.getWs0420WorkAreas().getsUdatRowSw().setRowExist();
		// COB_CODE: MOVE UBOC-MSG-ID         TO UDAT-ID.
		ws.getHalludat().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE LOW-VALUES          TO UDAT-BUS-OBJ-NM.
		ws.getHalludat().setBusObjNm(LiteralGenerator.create(Types.LOW_CHAR_VAL, Halludat.Len.BUS_OBJ_NM));
		// COB_CODE: MOVE 0                   TO UDAT-REC-SEQ.
		ws.getHalludat().setRecSeq(0);
		// COB_CODE: PERFORM 0510-UDAT-STARTBR.
		udatStartbr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0500-ALLDEL-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0500-ALLDEL-FUNC-X
			return;
		}
		//* READ AND DELETE ALL RESPONSE UMT ROWS
		// COB_CODE:      IF  WS-UDAT-STBROWSE-OK
		//                    END-IF
		//           *17543I         PERFORM 0520-UDAT-ENDBR
		//                END-IF.
		if (ws.getWs0420WorkAreas().getsUdatBrowseSw().isOk()) {
			// COB_CODE: PERFORM 0515-READ-AND-DELETE-UDAT
			//             UNTIL WS-UDAT-NO-MORE
			//               OR  UBOC-HALT-AND-RETURN
			while (!(ws.getWs0420WorkAreas().getsUdatRowSw().isNoMore()
					|| liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn())) {
				readAndDeleteUdat();
			}
			// COB_CODE: PERFORM 0520-UDAT-ENDBR
			udatEndbr();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//              GO TO 0500-ALLDEL-FUNC-X
			//           END-IF
			if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0500-ALLDEL-FUNC-X
				return;
			}
			//17543I         PERFORM 0520-UDAT-ENDBR
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0500-ALLDEL-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0500-ALLDEL-FUNC-X
			return;
		}
		//* DELETE ALL ROWS ON UHDR
		// COB_CODE: MOVE UBOC-MSG-ID         TO UHDR-ID.
		ws.getHalluhdr().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: PERFORM 0550-UHDR-STARTBR.
		uhdrStartbr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//           OR NOT WS-UHDR-STBROWSE-OK
		//               GO TO 0500-ALLDEL-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| !ws.getWs0420WorkAreas().getsUhdrBrowseSw().isOk()) {
			// COB_CODE: GO TO 0500-ALLDEL-FUNC-X
			return;
		}
		//* READ ALL THE ROWS AND DELETE
		// COB_CODE: PERFORM 0555-READ-AND-DELETE-UHDR
		//               UNTIL WS-UHDR-NO-MORE
		//                 OR  UBOC-HALT-AND-RETURN.
		while (!(ws.getWs0420WorkAreas().getsUhdrRowSw().isNoMore()
				|| liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn())) {
			readAndDeleteUhdr();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0500-ALLDEL-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0500-ALLDEL-FUNC-X
			return;
		}
		// COB_CODE: PERFORM 0550-UHDR-ENDBR.
		uhdrEndbr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0500-ALLDEL-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0500-ALLDEL-FUNC-X
			return;
		}
	}

	/**Original name: 0510-UDAT-STARTBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ALLDEL FUNCTION: START BROWSING THE UDAT FILE USING A PARTIAL  *
	 *                   KEY                                           *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void udatStartbr() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (UBOC-UOW-RESP-DATA-STORE)
		//               RIDFLD       (UDAT-KEY)
		//               KEYLENGTH    (LENGTH OF UDAT-ID)
		//               GENERIC
		//               RESP         (WS-RESPONSE-CODE)
		//               RESP2        (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, Halludat.Len.ID);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    SET WS-UDAT-STBROWSE-OK TO TRUE
		//              WHEN DFHRESP(NOTFND)
		//                   SET WS-UDAT-NO-MORE TO TRUE
		//               WHEN OTHER
		//                   GO TO 0510-UDAT-STARTBR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET WS-UDAT-STBROWSE-OK TO TRUE
			ws.getWs0420WorkAreas().getsUdatBrowseSw().setOk();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET WS-UDAT-NO-MORE TO TRUE
			ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
		} else {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '0510-UDAT-STARTBR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0510-UDAT-STARTBR");
			// COB_CODE: MOVE 'BAD RETURN CODE FROM STARTBR UMT'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD RETURN CODE FROM STARTBR UMT");
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0510-UDAT-STARTBR-X
			return;
		}
	}

	/**Original name: 0515-READ-AND-DELETE-UDAT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ALLDEL FUNCTION: READ NEXT AND THEN DELETE THE ROW USING A     *
	 *                   PARTIAL KEY                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readAndDeleteUdat() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (UBOC-UOW-RESP-DATA-STORE)
		//               INTO          (WS-DATA-UMT-AREA)
		//               RIDFLD        (UDAT-KEY)
		//               KEYLENGTH     (LENGTH OF UDAT-KEY)
		//               RESP          (WS-RESPONSE-CODE)
		//               RESP2         (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, Halludat.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalludat().setKeyBytes(iRowData.getKey());
				ws.setWsDataUmtAreaBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN DFHRESP(ENDFILE)
		//                    GO TO 0515-READ-AND-DELETE-UDAT-X
		//               WHEN OTHER
		//                   GO TO 0515-READ-AND-DELETE-UDAT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF UDAT-ID NOT EQUAL UBOC-MSG-ID
			//               GO TO 0515-READ-AND-DELETE-UDAT-X
			//           ELSE
			//               CONTINUE
			//           END-IF
			if (!Conditions.eq(ws.getHalludat().getId(), liUboc.getCommInfo().getUbocMsgId())) {
				// COB_CODE: SET WS-UDAT-NO-MORE TO TRUE
				ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
				// COB_CODE: GO TO 0515-READ-AND-DELETE-UDAT-X
				return;
			} else {
				// COB_CODE: CONTINUE
				//continue
			}
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ENDFILE) {
			// COB_CODE: SET WS-UDAT-NO-MORE TO TRUE
			ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
			// COB_CODE: GO TO 0515-READ-AND-DELETE-UDAT-X
			return;
		} else {
			// COB_CODE: SET WS-UDAT-NO-MORE       TO TRUE
			ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
			// COB_CODE: SET WS-LOG-ERROR          TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'UDAT-KEY = '                    UDAT-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY = ", ws.getHalludat().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0515-READ-AND-DELETE-UDAT'
			//              TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0515-READ-AND-DELETE-UDAT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNXT UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNXT UMT");
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0515-READ-AND-DELETE-UDAT-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETE
		//               FILE   (UBOC-UOW-RESP-DATA-STORE)
		//               RIDFLD (UDAT-KEY)
		//               KEYLENGTH(LENGTH OF UDAT-KEY)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowDAO.delete(iRowData, Halludat.Len.KEY);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    SUBTRACT +1 FROM UBOC-NBR-DATA-ROWS
		//               WHEN OTHER
		//                   GO TO 0515-READ-AND-DELETE-UDAT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SUBTRACT +1 FROM UBOC-NBR-DATA-ROWS
			liUboc.getCommInfo().setUbocNbrDataRows(Trunc.toInt(abs(liUboc.getCommInfo().getUbocNbrDataRows() - 1), 9));
		} else {
			// COB_CODE: SET WS-UDAT-NO-MORE      TO TRUE
			ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '0515-READ-AND-DELETE-UDAT'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0515-READ-AND-DELETE-UDAT");
			// COB_CODE: MOVE 'DELETE OF RESP UOW DATA STORE FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DELETE OF RESP UOW DATA STORE FAILED");
			// COB_CODE: STRING 'UDAT-KEY='           UDAT-KEY             ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY=", ws.getHalludat().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0515-READ-AND-DELETE-UDAT-X
			return;
		}
	}

	/**Original name: 0520-UDAT-ENDBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ALLDEL FUNCTION: END   BROWSING THE UDAT FILE USING A PARTIAL  *
	 *                   KEY                                           *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void udatEndbr() {
		IRowDAO iRowDAO = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (UBOC-UOW-RESP-DATA-STORE)
		//               RESP         (WS-RESPONSE-CODE)
		//               RESP2        (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    CONTINUE
		//               WHEN OTHER
		//                   GO TO 0520-UDAT-ENDBR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '0520-UDAT-ENDBR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0520-UDAT-ENDBR");
			// COB_CODE: MOVE 'BAD RETURN CODE FROM ENDBR UMT'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD RETURN CODE FROM ENDBR UMT");
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0520-UDAT-ENDBR-X
			return;
		}
	}

	/**Original name: 0550-UHDR-STARTBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ALLDEL FUNCTION: START BROWSING THE UHDR FILE USING A PARTIAL  *
	 *                   KEY                                           *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void uhdrStartbr() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (UBOC-UOW-RESP-HEADER-STORE)
		//               RIDFLD       (UHDR-KEY)
		//               KEYLENGTH    (LENGTH OF UHDR-ID)
		//               GENERIC
		//               RESP         (WS-RESPONSE-CODE)
		//               RESP2        (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, Halluhdr.Len.ID);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    SET WS-UHDR-STBROWSE-OK TO TRUE
		//              WHEN DFHRESP(NOTFND)
		//                   SET WS-UHDR-NO-MORE TO TRUE
		//               WHEN OTHER
		//                   GO TO 0550-UHDR-STARTBR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET WS-UHDR-STBROWSE-OK TO TRUE
			ws.getWs0420WorkAreas().getsUhdrBrowseSw().setOk();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET WS-UHDR-NO-MORE TO TRUE
			ws.getWs0420WorkAreas().getsUhdrRowSw().setNoMore();
		} else {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '0550-UHDR-STARTBR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0550-UHDR-STARTBR");
			// COB_CODE: MOVE 'BAD RETURN CODE FROM STARTBR UMT'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD RETURN CODE FROM STARTBR UMT");
			// COB_CODE: STRING 'UHDR-ID='            UHDR-ID              ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-ID=", ws.getHalluhdr().getIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0550-UHDR-STARTBR-X
			return;
		}
	}

	/**Original name: 0555-READ-AND-DELETE-UHDR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ALLDEL FUNCTION: READ NEXT AND THEN DELETE THE ROW USING A     *
	 *                   PARTIAL KEY                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readAndDeleteUhdr() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (UBOC-UOW-RESP-HEADER-STORE)
		//               INTO          (WS-HDR-UMT-AREA)
		//               RIDFLD        (UHDR-KEY)
		//               KEYLENGTH     (LENGTH OF UHDR-KEY)
		//               RESP          (WS-RESPONSE-CODE)
		//               RESP2         (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, Halluhdr.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalluhdr().setKeyBytes(iRowData.getKey());
				ws.setWsHdrUmtAreaBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN DFHRESP(ENDFILE)
		//                   GO TO 0555-READ-AND-DELETE-UHDR-X
		//               WHEN OTHER
		//                   GO TO 0555-READ-AND-DELETE-UHDR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF UHDR-ID NOT EQUAL UBOC-MSG-ID
			//               GO TO 0555-READ-AND-DELETE-UHDR-X
			//           ELSE
			//               CONTINUE
			//           END-IF
			if (!Conditions.eq(ws.getHalluhdr().getId(), liUboc.getCommInfo().getUbocMsgId())) {
				// COB_CODE: SET WS-UHDR-NO-MORE TO TRUE
				ws.getWs0420WorkAreas().getsUhdrRowSw().setNoMore();
				// COB_CODE: GO TO 0555-READ-AND-DELETE-UHDR-X
				return;
			} else {
				// COB_CODE: CONTINUE
				//continue
			}
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ENDFILE) {
			// COB_CODE: SET WS-UHDR-NO-MORE TO TRUE
			ws.getWs0420WorkAreas().getsUhdrRowSw().setNoMore();
			// COB_CODE: GO TO 0555-READ-AND-DELETE-UHDR-X
			return;
		} else {
			// COB_CODE: SET WS-UHDR-NO-MORE       TO TRUE
			ws.getWs0420WorkAreas().getsUhdrRowSw().setNoMore();
			// COB_CODE: SET WS-LOG-ERROR          TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'UHDR-KEY = '                    UHDR-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-KEY = ", ws.getHalluhdr().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0555-READ-AND-DELETE-UHDR'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0555-READ-AND-DELETE-UHDR");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNXT UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNXT UMT");
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0555-READ-AND-DELETE-UHDR-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETE
		//               FILE   (UBOC-UOW-RESP-HEADER-STORE)
		//               RIDFLD (UHDR-KEY)
		//               KEYLENGTH(LENGTH OF UHDR-KEY)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowDAO.delete(iRowData, Halluhdr.Len.KEY);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    SUBTRACT +1 FROM UBOC-NBR-HDR-ROWS
		//               WHEN OTHER
		//                   GO TO 0555-READ-AND-DELETE-UHDR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SUBTRACT +1 FROM UBOC-NBR-HDR-ROWS
			liUboc.getCommInfo().setUbocNbrHdrRows(Trunc.toInt(abs(liUboc.getCommInfo().getUbocNbrHdrRows() - 1), 9));
		} else {
			// COB_CODE: SET WS-UHDR-NO-MORE      TO TRUE
			ws.getWs0420WorkAreas().getsUhdrRowSw().setNoMore();
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '0555-READ-AND-DELETE-UHDR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0555-READ-AND-DELETE-UHDR");
			// COB_CODE: MOVE 'DELETE OF RESP UOW DATA STORE FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DELETE OF RESP UOW DATA STORE FAILED");
			// COB_CODE: STRING 'UHDR-KEY='           UHDR-KEY             ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-KEY=", ws.getHalluhdr().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0555-READ-AND-DELETE-UHDR-X
			return;
		}
	}

	/**Original name: 0550-UHDR-ENDBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ALLDEL FUNCTION: END   BROWSING THE UHDR FILE USING A PARTIAL  *
	 *                   KEY                                           *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void uhdrEndbr() {
		IRowDAO iRowDAO = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (UBOC-UOW-RESP-HEADER-STORE)
		//               RESP         (WS-RESPONSE-CODE)
		//               RESP2        (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    CONTINUE
		//               WHEN OTHER
		//                   GO TO 0550-UHDR-ENDBR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '0550-UHDR-ENDBR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0550-UHDR-ENDBR");
			// COB_CODE: MOVE 'BAD RETURN CODE FROM ENDBR UMT'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD RETURN CODE FROM ENDBR UMT");
			// COB_CODE: STRING 'UHDR-ID='            UHDR-ID              ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-ID=", ws.getHalluhdr().getIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0550-UHDR-ENDBR-X
			return;
		}
	}

	/**Original name: 0600-DELBO-FUNC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DELBO FUNCTION: DELETE ALL RESPONSE UMT ROWS FOR A GIVEN MSGID
	 *       AND BUSINESS OBJECT NAME.
	 * *****************************************************************</pre>*/
	private void delboFunc() {
		// COB_CODE: SET  WS-UHDR-STBROWSE-NOT-OK TO TRUE.
		ws.getWs0420WorkAreas().getsUhdrBrowseSw().setNotOk();
		// COB_CODE: SET  WS-UDAT-STBROWSE-NOT-OK TO TRUE.
		ws.getWs0420WorkAreas().getsUdatBrowseSw().setNotOk();
		// COB_CODE: SET  WS-UHDR-ROW-EXIST       TO TRUE.
		ws.getWs0420WorkAreas().getsUhdrRowSw().setRowExist();
		// COB_CODE: SET  WS-UDAT-ROW-EXIST       TO TRUE.
		ws.getWs0420WorkAreas().getsUdatRowSw().setRowExist();
		// COB_CODE: MOVE UBOC-MSG-ID         TO UDAT-ID.
		ws.getHalludat().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM TO UDAT-BUS-OBJ-NM.
		ws.getHalludat().setBusObjNm(liHalrrespLinkage.getBusObjNm());
		// COB_CODE: MOVE 0                   TO UDAT-REC-SEQ.
		ws.getHalludat().setRecSeq(0);
		// COB_CODE: PERFORM 0610-UDAT-STARTBR.
		udatStartbr1();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0600-DELBO-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0600-DELBO-FUNC-X
			return;
		}
		//* READ AND DELETE ALL RESPONSE UMT ROWS FOR THE BUS OBJ
		// COB_CODE:      IF  WS-UDAT-STBROWSE-OK
		//                    END-IF
		//           **17543I         PERFORM 0620-UDAT-ENDBR
		//                END-IF.
		if (ws.getWs0420WorkAreas().getsUdatBrowseSw().isOk()) {
			// COB_CODE: PERFORM 0615-READ-AND-DELETE-UDAT
			//             UNTIL WS-UDAT-NO-MORE
			//               OR  UBOC-HALT-AND-RETURN
			while (!(ws.getWs0420WorkAreas().getsUdatRowSw().isNoMore()
					|| liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn())) {
				readAndDeleteUdat1();
			}
			// COB_CODE: PERFORM 0620-UDAT-ENDBR
			udatEndbr1();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//              GO TO 0600-DELBO-FUNC-X
			//           END-IF
			if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0600-DELBO-FUNC-X
				return;
			}
			//*17543I         PERFORM 0620-UDAT-ENDBR
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0600-DELBO-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0600-DELBO-FUNC-X
			return;
		}
		//* DELETE ROW ON UHDR FOR THE BUS OBJ
		// COB_CODE: PERFORM 1020-READ-RESP-HDR-REC.
		readRespHdrRec();
		//*   IF NOT UBOC-UOW-OK
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0600-DELBO-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0600-DELBO-FUNC-X
			return;
		}
		// COB_CODE: PERFORM 1030-DELETE-HEADER-REC
		deleteHeaderRec();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0600-DELBO-FUNC-X
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0600-DELBO-FUNC-X
			return;
		}
	}

	/**Original name: 0610-UDAT-STARTBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DELBO FUNCTION: START BROWSING THE UDAT FILE USING A PARTIAL
	 *                   KEY OF THE MSG ID AND THE BUS OBJ NAME
	 * *****************************************************************</pre>*/
	private void udatStartbr1() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (UBOC-UOW-RESP-DATA-STORE)
		//               RIDFLD       (UDAT-KEY)
		//               KEYLENGTH    (LENGTH OF UDAT-KEY)
		//               GTEQ
		//               RESP         (WS-RESPONSE-CODE)
		//               RESP2        (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, Halludat.Len.KEY);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    SET WS-UDAT-STBROWSE-OK TO TRUE
		//              WHEN DFHRESP(NOTFND)
		//                   GO TO 0610-UDAT-STARTBR-X
		//               WHEN OTHER
		//                   GO TO 0610-UDAT-STARTBR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET WS-UDAT-STBROWSE-OK TO TRUE
			ws.getWs0420WorkAreas().getsUdatBrowseSw().setOk();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET WS-UDAT-NO-MORE TO TRUE
			ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
			// COB_CODE: GO TO 0610-UDAT-STARTBR-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '0610-UDAT-STARTBR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0610-UDAT-STARTBR");
			// COB_CODE: MOVE 'BAD RETURN CODE FROM STARTBR UMT'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD RETURN CODE FROM STARTBR UMT");
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0610-UDAT-STARTBR-X
			return;
		}
	}

	/**Original name: 0615-READ-AND-DELETE-UDAT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DELBO FUNCTION: READ NEXT AND THEN DELETE THE ROW USING A
	 *                   PARTIAL KEY OF MSG ID AND BUS OBJ NAME
	 * *****************************************************************</pre>*/
	private void readAndDeleteUdat1() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (UBOC-UOW-RESP-DATA-STORE)
		//               INTO          (WS-DATA-UMT-AREA)
		//               RIDFLD        (UDAT-KEY)
		//               KEYLENGTH     (LENGTH OF UDAT-KEY)
		//               RESP          (WS-RESPONSE-CODE)
		//               RESP2         (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, Halludat.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalludat().setKeyBytes(iRowData.getKey());
				ws.setWsDataUmtAreaBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//                    WHEN DFHRESP(NORMAL)
		//                        END-IF
		//           *17543I             IF UDAT-ID NOT EQUAL UBOC-MSG-ID
		//           *17543I                 SET WS-UDAT-NO-MORE TO TRUE
		//           *17543I                 GO TO 0615-READ-AND-DELETE-UDAT-X
		//           *17543I             ELSE
		//           *17543I                 CONTINUE
		//           *17543I             END-IF
		//                    WHEN DFHRESP(ENDFILE)
		//                         GO TO 0615-READ-AND-DELETE-UDAT-X
		//                    WHEN OTHER
		//                        GO TO 0615-READ-AND-DELETE-UDAT-X
		//                END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF UDAT-ID EQUAL UBOC-MSG-ID
			//             AND UDAT-BUS-OBJ-NM EQUAL HALRRESP-BUS-OBJ-NM
			//               CONTINUE
			//           ELSE
			//               GO TO 0615-READ-AND-DELETE-UDAT-X
			//           END-IF
			if (Conditions.eq(ws.getHalludat().getId(), liUboc.getCommInfo().getUbocMsgId())
					&& Conditions.eq(ws.getHalludat().getBusObjNm(), liHalrrespLinkage.getBusObjNm())) {
				// COB_CODE: CONTINUE
				//continue
			} else {
				// COB_CODE: SET WS-UDAT-NO-MORE TO TRUE
				ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
				// COB_CODE: GO TO 0615-READ-AND-DELETE-UDAT-X
				return;
			}
			//17543I             IF UDAT-ID NOT EQUAL UBOC-MSG-ID
			//17543I                 SET WS-UDAT-NO-MORE TO TRUE
			//17543I                 GO TO 0615-READ-AND-DELETE-UDAT-X
			//17543I             ELSE
			//17543I                 CONTINUE
			//17543I             END-IF
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ENDFILE) {
			// COB_CODE: SET WS-UDAT-NO-MORE TO TRUE
			ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
			// COB_CODE: GO TO 0615-READ-AND-DELETE-UDAT-X
			return;
		} else {
			// COB_CODE: SET WS-UDAT-NO-MORE       TO TRUE
			ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
			// COB_CODE: SET WS-LOG-ERROR          TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'UDAT-KEY = '                    UDAT-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY = ", ws.getHalludat().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '0615-READ-AND-DELETE-UDAT'
			//              TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0615-READ-AND-DELETE-UDAT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNXT UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNXT UMT");
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0615-READ-AND-DELETE-UDAT-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETE
		//               FILE      (UBOC-UOW-RESP-DATA-STORE)
		//               RIDFLD    (UDAT-KEY)
		//               KEYLENGTH (LENGTH OF UDAT-KEY)
		//               RESP      (WS-RESPONSE-CODE)
		//               RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowDAO.delete(iRowData, Halludat.Len.KEY);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    SUBTRACT +1 FROM UBOC-NBR-DATA-ROWS
		//               WHEN OTHER
		//                   GO TO 0615-READ-AND-DELETE-UDAT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SUBTRACT +1 FROM UBOC-NBR-DATA-ROWS
			liUboc.getCommInfo().setUbocNbrDataRows(Trunc.toInt(abs(liUboc.getCommInfo().getUbocNbrDataRows() - 1), 9));
		} else {
			// COB_CODE: SET WS-UDAT-NO-MORE      TO TRUE
			ws.getWs0420WorkAreas().getsUdatRowSw().setNoMore();
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '0615-READ-AND-DELETE-UDAT'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0615-READ-AND-DELETE-UDAT");
			// COB_CODE: MOVE 'DELETE OF RESP UOW DATA STORE FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DELETE OF RESP UOW DATA STORE FAILED");
			// COB_CODE: STRING 'UDAT-KEY='           UDAT-KEY             ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY=", ws.getHalludat().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0615-READ-AND-DELETE-UDAT-X
			return;
		}
	}

	/**Original name: 0620-UDAT-ENDBR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DELBO FUNCTION: END BROWSING THE UDAT FILE USING A PARTIAL
	 *                   KEY OF MSG ID AND BUS OBJ NAME.
	 * *****************************************************************</pre>*/
	private void udatEndbr1() {
		IRowDAO iRowDAO = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               ENDBR FILE   (UBOC-UOW-RESP-DATA-STORE)
		//               RESP         (WS-RESPONSE-CODE)
		//               RESP2        (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    CONTINUE
		//               WHEN OTHER
		//                   GO TO 0620-UDAT-ENDBR-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '0620-UDAT-ENDBR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0620-UDAT-ENDBR");
			// COB_CODE: MOVE 'BAD RETURN CODE FROM ENDBR UMT'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD RETURN CODE FROM ENDBR UMT");
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0620-UDAT-ENDBR-X
			return;
		}
	}

	/**Original name: 1000-READ-RESP-DATA-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  READ A RESPONSE DATA RECORD WITH UPDATE INTENT.                *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readRespDataRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID         TO UDAT-ID.
		ws.getHalludat().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM TO UDAT-BUS-OBJ-NM.
		ws.getHalludat().setBusObjNm(liHalrrespLinkage.getBusObjNm());
		// COB_CODE: MOVE HALRRESP-REC-SEQ    TO UDAT-REC-SEQ.
		ws.getHalludat().setRecSeqFormatted(liHalrrespLinkage.getRecSeqFormatted());
		// COB_CODE: EXEC CICS
		//                READ FILE  (UBOC-UOW-RESP-DATA-STORE)
		//                INTO       (UDAT-COMMON)
		//                RIDFLD     (UDAT-KEY)
		//                KEYLENGTH  (LENGTH OF UDAT-KEY)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//                UPDATE
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Halludat.Len.KEY, true);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalludat().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 1000-READ-RESP-DATA-REC-X
		//               WHEN OTHER
		//                   GO TO 1000-READ-RESP-DATA-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET WS-LOG-ERROR                            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE  OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-EXI-ROW-NOT-FOUND  OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspExiRowNotFound();
			// COB_CODE: MOVE '1000-READ-RESP-DATA-REC'
			//                TO EFAL-ERR-PARAGRAPH  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1000-READ-RESP-DATA-REC");
			// COB_CODE: MOVE 'EXISTING UDAT REC NOT FOUND'
			//                TO EFAL-ERR-COMMENT    OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("EXISTING UDAT REC NOT FOUND");
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  'UDAT-BUS-OBJ-NM='    UDAT-BUS-OBJ-NM      ';'
			//                  'UDAT-REC-SEQ='       UDAT-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";", "UDAT-BUS-OBJ-NM=", ws.getHalludat().getBusObjNmFormatted(),
							";", "UDAT-REC-SEQ=", ws.getHalludat().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1000-READ-RESP-DATA-REC-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-DATA-STORE
			//                                  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespDataStore());
			// COB_CODE: MOVE '1000-READ-RESP-DATA-REC'
			//                                  TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1000-READ-RESP-DATA-REC");
			// COB_CODE: MOVE 'READ RESP DATA UMT FAILED'
			//                                  TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ RESP DATA UMT FAILED");
			// COB_CODE: STRING 'UDAT-ID='            UDAT-ID              ';'
			//                  'UDAT-BUS-OBJ-NM='    UDAT-BUS-OBJ-NM      ';'
			//                  'UDAT-REC-SEQ='       UDAT-REC-SEQ         ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UDAT-ID=", ws.getHalludat().getIdFormatted(), ";", "UDAT-BUS-OBJ-NM=", ws.getHalludat().getBusObjNmFormatted(),
							";", "UDAT-REC-SEQ=", ws.getHalludat().getRecSeqAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1000-READ-RESP-DATA-REC-X
			return;
		}
	}

	/**Original name: 1020-READ-RESP-HDR-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  ATTEMPT TO READ THE RESPONSE HEADER RECORD CORRESPONDING       *
	 *  TO THE BUSINESS OBJECT DATA NAME ASSOCIATED WITH THE RESPONSE  *
	 *  DATA RECORD.                                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readRespHdrRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID         TO UHDR-ID.
		ws.getHalluhdr().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM TO UHDR-BUS-OBJ-NM.
		ws.getHalluhdr().setBusObjNm(liHalrrespLinkage.getBusObjNm());
		// COB_CODE: EXEC CICS
		//                READ FILE  (UBOC-UOW-RESP-HEADER-STORE)
		//                INTO       (UHDR-COMMON)
		//                RIDFLD     (UHDR-KEY)
		//                KEYLENGTH  (LENGTH OF UHDR-KEY)
		//                UPDATE
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Halluhdr.Len.KEY, true);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalluhdr().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   SET W0420-HDR-EXISTS TO TRUE
		//               WHEN DFHRESP(NOTFND)
		//                   SET W0420-HDR-NOT-EXISTS TO TRUE
		//               WHEN OTHER
		//                   GO TO 1020-READ-RESP-HDR-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET W0420-HDR-EXISTS TO TRUE
			ws.getWs0420WorkAreas().getW0420HdrExistsSw().setExists();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET W0420-HDR-NOT-EXISTS TO TRUE
			ws.getWs0420WorkAreas().getW0420HdrExistsSw().setNotExists();
		} else {
			// COB_CODE: SET WS-LOG-ERROR       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//                                  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '1020-READ-RESP-HDR-REC'
			//                                  TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1020-READ-RESP-HDR-REC");
			// COB_CODE: MOVE 'READ RESP HEADER UMT FAILED'
			//                                  TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ RESP HEADER UMT FAILED");
			// COB_CODE: STRING 'UHDR-ID='            UHDR-ID              ';'
			//                  'UHDR-BUS-OBJ-NM='    UHDR-BUS-OBJ-NM      ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "UHDR-ID=", ws.getHalluhdr().getIdFormatted(),
					";", "UHDR-BUS-OBJ-NM=", ws.getHalluhdr().getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1020-READ-RESP-HDR-REC-X
			return;
		}
	}

	/**Original name: 1030-DELETE-HEADER-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  DELETE A RESPONSE HEADER RECORD                                *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void deleteHeaderRec() {
		IRowDAO iRowDAO = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID         TO UHDR-ID.
		ws.getHalluhdr().setId(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE HALRRESP-BUS-OBJ-NM TO UHDR-BUS-OBJ-NM.
		ws.getHalluhdr().setBusObjNm(liHalrrespLinkage.getBusObjNm());
		// COB_CODE: EXEC CICS
		//                DELETE
		//                FILE    (UBOC-UOW-RESP-HEADER-STORE)
		//                RESP    (WS-RESPONSE-CODE)
		//                RESP2   (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, liUboc.getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.delete(null);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   SUBTRACT +1 FROM UBOC-NBR-HDR-ROWS
		//               WHEN OTHER
		//                   GO TO 1030-DELETE-HEADER-REC-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SUBTRACT +1 FROM UBOC-NBR-HDR-ROWS
			liUboc.getCommInfo().setUbocNbrHdrRows(Trunc.toInt(abs(liUboc.getCommInfo().getUbocNbrHdrRows() - 1), 9));
		} else {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//                                  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(liUboc.getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '1030-DELETE-HEADER-REC'
			//                                  TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1030-DELETE-HEADER-REC");
			// COB_CODE: MOVE 'DELETE RESP HEADER UMT FAILED'
			//                                  TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DELETE RESP HEADER UMT FAILED");
			// COB_CODE: STRING 'UHDR-ID='            UHDR-ID              ';'
			//                  'UHDR-BUS-OBJ-NM='    UHDR-BUS-OBJ-NM      ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "UHDR-ID=", ws.getHalluhdr().getIdFormatted(),
					";", "UHDR-BUS-OBJ-NM=", ws.getHalluhdr().getBusObjNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1030-DELETE-HEADER-REC-X
			return;
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(liUboc.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(liUboc.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(liUboc.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(liUboc.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALRRESP", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			liUboc.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			liUboc.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			liUboc.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}

	public void deleteArgListeners() {
		liHalrrespLinkage.getBusObjDataLen().deleteListener(lioUowBusObjData.getFillerLioUowBusObjDataListener());
	}

	public void registerArgListeners() {
		liHalrrespLinkage.getBusObjDataLen().addListener(lioUowBusObjData.getFillerLioUowBusObjDataListener());
		liHalrrespLinkage.getBusObjDataLen().notifyListeners();
	}
}
