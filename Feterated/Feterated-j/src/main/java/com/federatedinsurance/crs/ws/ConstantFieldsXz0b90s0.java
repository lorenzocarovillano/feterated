/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0B90S0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0b90s0 {

	//==== PROPERTIES ====
	//Original name: CF-SN-GET-POL-TRM-LIS-BY-ACT
	private String snGetPolTrmLisByAct = "XZC06090";
	//Original name: CF-PN-GET-PND-CNC-STATUS
	private String pnGetPndCncStatus = "XZC01090";
	//Original name: CF-PN-GET-POL-TMN-STATUS
	private String pnGetPolTmnStatus = "XZC02090";
	//Original name: CF-YES
	private char yes = 'Y';
	//Original name: CF-NIN
	private String nin = "NIN";
	//Original name: CF-CANCELLED-POL-STA-CD
	private char cancelledPolStaCd = '9';
	//Original name: CF-TERMINATED-POL-STA-CD
	private char terminatedPolStaCd = '8';
	//Original name: CF-NEW-BIZ-TRS-TYP-CD
	private char newBizTrsTypCd = 'N';
	//Original name: CF-RNL-TRS-TYP-CD
	private char rnlTrsTypCd = 'R';
	//Original name: CF-CONTAINER-INFO
	private CfContainerInfoXz0b90s0 containerInfo = new CfContainerInfoXz0b90s0();

	//==== METHODS ====
	public String getSnGetPolTrmLisByAct() {
		return this.snGetPolTrmLisByAct;
	}

	public String getPnGetPndCncStatus() {
		return this.pnGetPndCncStatus;
	}

	public String getPnGetPolTmnStatus() {
		return this.pnGetPolTmnStatus;
	}

	public char getYes() {
		return this.yes;
	}

	public String getNin() {
		return this.nin;
	}

	public char getCancelledPolStaCd() {
		return this.cancelledPolStaCd;
	}

	public char getTerminatedPolStaCd() {
		return this.terminatedPolStaCd;
	}

	public char getNewBizTrsTypCd() {
		return this.newBizTrsTypCd;
	}

	public char getRnlTrsTypCd() {
		return this.rnlTrsTypCd;
	}

	public CfContainerInfoXz0b90s0 getContainerInfo() {
		return containerInfo;
	}
}
