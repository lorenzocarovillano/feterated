/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXz001000 {

	//==== PROPERTIES ====
	//Original name: EA-01-PROGRAM-MSG
	private Ea01ProgramMsg ea01ProgramMsg = new Ea01ProgramMsg();
	//Original name: EA-03-SUCCESSFUL-END
	private Ea03SuccessfulEnd ea03SuccessfulEnd = new Ea03SuccessfulEnd();
	//Original name: EA-04-CICS-PARM-MSG
	private Ea04CicsParmMsg ea04CicsParmMsg = new Ea04CicsParmMsg();
	//Original name: EA-05-PARM-ERR-MSG
	private Ea05ParmErrMsg ea05ParmErrMsg = new Ea05ParmErrMsg();
	//Original name: EA-06-MAX-TBL-ENTRIES-MSG
	private Ea06MaxTblEntriesMsg ea06MaxTblEntriesMsg = new Ea06MaxTblEntriesMsg();
	//Original name: EA-10-OPEN-CURSOR-ERROR
	private Ea10OpenCursorError ea10OpenCursorError = new Ea10OpenCursorError();
	//Original name: EA-11-FETCH-CURSOR-ERROR
	private Ea11FetchCursorError ea11FetchCursorError = new Ea11FetchCursorError();
	//Original name: EA-12-CLOSE-CURSOR-ERROR
	private Ea12CloseCursorError ea12CloseCursorError = new Ea12CloseCursorError();
	//Original name: EA-13-COMMIT-DB2-ERROR
	private Ea13CommitDb2Error ea13CommitDb2Error = new Ea13CommitDb2Error();
	//Original name: EA-14-ROLLBACK-DB2-ERROR
	private Ea14RollbackDb2Error ea14RollbackDb2Error = new Ea14RollbackDb2Error();
	//Original name: EA-15-DB2-UPDATE-ERROR
	private Ea15Db2UpdateError ea15Db2UpdateError = new Ea15Db2UpdateError();
	//Original name: EA-20-WF-ERROR-MSG
	private Ea20WfErrorMsg ea20WfErrorMsg = new Ea20WfErrorMsg();
	//Original name: EA-21-WF-RETURN-MSG
	private String ea21WfReturnMsg = DefaultValues.stringVal(Len.EA21_WF_RETURN_MSG);
	//Original name: EA-22-WF-PARM-MSG
	private Ea22WfParmMsg ea22WfParmMsg = new Ea22WfParmMsg();

	//==== METHODS ====
	public void setEa21WfReturnMsg(String ea21WfReturnMsg) {
		this.ea21WfReturnMsg = Functions.subString(ea21WfReturnMsg, Len.EA21_WF_RETURN_MSG);
	}

	public String getEa21WfReturnMsg() {
		return this.ea21WfReturnMsg;
	}

	public Ea01ProgramMsg getEa01ProgramMsg() {
		return ea01ProgramMsg;
	}

	public Ea03SuccessfulEnd getEa03SuccessfulEnd() {
		return ea03SuccessfulEnd;
	}

	public Ea04CicsParmMsg getEa04CicsParmMsg() {
		return ea04CicsParmMsg;
	}

	public Ea05ParmErrMsg getEa05ParmErrMsg() {
		return ea05ParmErrMsg;
	}

	public Ea06MaxTblEntriesMsg getEa06MaxTblEntriesMsg() {
		return ea06MaxTblEntriesMsg;
	}

	public Ea10OpenCursorError getEa10OpenCursorError() {
		return ea10OpenCursorError;
	}

	public Ea11FetchCursorError getEa11FetchCursorError() {
		return ea11FetchCursorError;
	}

	public Ea12CloseCursorError getEa12CloseCursorError() {
		return ea12CloseCursorError;
	}

	public Ea13CommitDb2Error getEa13CommitDb2Error() {
		return ea13CommitDb2Error;
	}

	public Ea14RollbackDb2Error getEa14RollbackDb2Error() {
		return ea14RollbackDb2Error;
	}

	public Ea15Db2UpdateError getEa15Db2UpdateError() {
		return ea15Db2UpdateError;
	}

	public Ea20WfErrorMsg getEa20WfErrorMsg() {
		return ea20WfErrorMsg;
	}

	public Ea22WfParmMsg getEa22WfParmMsg() {
		return ea22WfParmMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA21_WF_RETURN_MSG = 132;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
