/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalLokObjHdrV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [HAL_LOK_OBJ_HDR_V]
 * 
 */
public class HalLokObjHdrVDao extends BaseSqlDao<IHalLokObjHdrV> {

	public HalLokObjHdrVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalLokObjHdrV> getToClass() {
		return IHalLokObjHdrV.class;
	}

	public DbAccessStatus insertRec(String tchKey, String appId) {
		return buildQuery("insertRec").bind("tchKey", tchKey).bind("appId", appId).executeInsert();
	}

	public DbAccessStatus deleteRec(String tchKey, String appId) {
		return buildQuery("deleteRec").bind("tchKey", tchKey).bind("appId", appId).executeDelete();
	}

	public DbAccessStatus updateRec(String tchKey, String appId) {
		return buildQuery("updateRec").bind("tchKey", tchKey).bind("appId", appId).executeUpdate();
	}
}
