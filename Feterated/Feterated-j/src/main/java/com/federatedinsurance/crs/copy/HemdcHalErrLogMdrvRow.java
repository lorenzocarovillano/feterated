/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: HEMDC-HAL-ERR-LOG-MDRV-ROW<br>
 * Variable: HEMDC-HAL-ERR-LOG-MDRV-ROW from copybook HALLCEMD<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class HemdcHalErrLogMdrvRow {

	//==== PROPERTIES ====
	//Original name: HEMDC-HAL-ERR-LOG-MDRV-CHK-SUM
	private String halErrLogMdrvChkSum = DefaultValues.stringVal(Len.HAL_ERR_LOG_MDRV_CHK_SUM);
	//Original name: HEMDC-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: HEMDC-UOW-ID-CI
	private char uowIdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-UOW-ID
	private String uowId = DefaultValues.stringVal(Len.UOW_ID);
	//Original name: HEMDC-FAIL-TS-CI
	private char failTsCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-FAIL-TS
	private String failTs = DefaultValues.stringVal(Len.FAIL_TS);
	//Original name: HEMDC-HAL-ERR-LOG-MDRV-DATA
	private HemdcHalErrLogMdrvData halErrLogMdrvData = new HemdcHalErrLogMdrvData();

	//==== METHODS ====
	public String getHemdcHalErrLogMdrvRowFormatted() {
		return MarshalByteExt.bufferToStr(getHemdcHalErrLogMdrvRowBytes());
	}

	public byte[] getHemdcHalErrLogMdrvRowBytes() {
		byte[] buffer = new byte[Len.HEMDC_HAL_ERR_LOG_MDRV_ROW];
		return getHemdcHalErrLogMdrvRowBytes(buffer, 1);
	}

	public byte[] getHemdcHalErrLogMdrvRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getHalErrLogMdrvFixedBytes(buffer, position);
		position += Len.HAL_ERR_LOG_MDRV_FIXED;
		getHalErrLogMdrvDatesBytes(buffer, position);
		position += Len.HAL_ERR_LOG_MDRV_DATES;
		getHalErrLogMdrvKeyBytes(buffer, position);
		position += Len.HAL_ERR_LOG_MDRV_KEY;
		halErrLogMdrvData.getHalErrLogMdrvDataBytes(buffer, position);
		return buffer;
	}

	public void initHemdcHalErrLogMdrvRowSpaces() {
		initHalErrLogMdrvFixedSpaces();
		initHalErrLogMdrvDatesSpaces();
		initHalErrLogMdrvKeySpaces();
		halErrLogMdrvData.initHalErrLogMdrvDataSpaces();
	}

	public byte[] getHalErrLogMdrvFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, halErrLogMdrvChkSum, Len.HAL_ERR_LOG_MDRV_CHK_SUM);
		return buffer;
	}

	public void initHalErrLogMdrvFixedSpaces() {
		halErrLogMdrvChkSum = "";
	}

	public byte[] getHalErrLogMdrvDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void initHalErrLogMdrvDatesSpaces() {
		transProcessDt = "";
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public byte[] getHalErrLogMdrvKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, uowIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, uowId, Len.UOW_ID);
		position += Len.UOW_ID;
		MarshalByte.writeChar(buffer, position, failTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, failTs, Len.FAIL_TS);
		return buffer;
	}

	public void initHalErrLogMdrvKeySpaces() {
		uowIdCi = Types.SPACE_CHAR;
		uowId = "";
		failTsCi = Types.SPACE_CHAR;
		failTs = "";
	}

	public char getUowIdCi() {
		return this.uowIdCi;
	}

	public void setUowId(String uowId) {
		this.uowId = Functions.subString(uowId, Len.UOW_ID);
	}

	public String getUowId() {
		return this.uowId;
	}

	public char getFailTsCi() {
		return this.failTsCi;
	}

	public void setFailTs(String failTs) {
		this.failTs = Functions.subString(failTs, Len.FAIL_TS);
	}

	public String getFailTs() {
		return this.failTs;
	}

	public HemdcHalErrLogMdrvData getHalErrLogMdrvData() {
		return halErrLogMdrvData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HAL_ERR_LOG_MDRV_CHK_SUM = 9;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int UOW_ID = 32;
		public static final int FAIL_TS = 26;
		public static final int HAL_ERR_LOG_MDRV_FIXED = HAL_ERR_LOG_MDRV_CHK_SUM;
		public static final int HAL_ERR_LOG_MDRV_DATES = TRANS_PROCESS_DT;
		public static final int UOW_ID_CI = 1;
		public static final int FAIL_TS_CI = 1;
		public static final int HAL_ERR_LOG_MDRV_KEY = UOW_ID_CI + UOW_ID + FAIL_TS_CI + FAIL_TS;
		public static final int HEMDC_HAL_ERR_LOG_MDRV_ROW = HAL_ERR_LOG_MDRV_FIXED + HAL_ERR_LOG_MDRV_DATES + HAL_ERR_LOG_MDRV_KEY
				+ HemdcHalErrLogMdrvData.Len.HAL_ERR_LOG_MDRV_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
