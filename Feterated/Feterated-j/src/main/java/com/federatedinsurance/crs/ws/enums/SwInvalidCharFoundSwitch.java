/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-INVALID-CHAR-FOUND-SWITCH<br>
 * Variable: SW-INVALID-CHAR-FOUND-SWITCH from program CAWRVDCH<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwInvalidCharFoundSwitch {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NOT_FOUND = 'N';
	public static final char FOUND = 'Y';

	//==== METHODS ====
	public void setSwInvalidCharFoundSwitch(char swInvalidCharFoundSwitch) {
		this.value = swInvalidCharFoundSwitch;
	}

	public char getSwInvalidCharFoundSwitch() {
		return this.value;
	}

	public void setNotFound() {
		value = NOT_FOUND;
	}

	public boolean isFound() {
		return value == FOUND;
	}

	public void setFound() {
		value = FOUND;
	}
}
