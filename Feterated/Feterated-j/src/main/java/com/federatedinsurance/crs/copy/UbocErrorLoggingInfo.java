/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.UbocErrorLoggingLvlSw;
import com.federatedinsurance.crs.ws.enums.UbocErrorsLoggedSw;

/**Original name: UBOC-ERROR-LOGGING-INFO<br>
 * Variable: UBOC-ERROR-LOGGING-INFO from copybook HALLUBOC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class UbocErrorLoggingInfo {

	//==== PROPERTIES ====
	//Original name: UBOC-ERRORS-LOGGED-SW
	private UbocErrorsLoggedSw errorsLoggedSw = new UbocErrorsLoggedSw();
	//Original name: UBOC-ERROR-LOGGING-LVL-SW
	private UbocErrorLoggingLvlSw errorLoggingLvlSw = new UbocErrorLoggingLvlSw();
	//Original name: UBOC-ERR-LOG-SQLCODE-DSPLY
	private String errLogSqlcodeDsply = DefaultValues.stringVal(Len.ERR_LOG_SQLCODE_DSPLY);
	//Original name: UBOC-ERR-LOG-EIBRESP-DSPLY
	private String errLogEibrespDsply = DefaultValues.stringVal(Len.ERR_LOG_EIBRESP_DSPLY);
	//Original name: UBOC-ERR-LOG-EIBRESP2-DSPLY
	private String errLogEibresp2Dsply = DefaultValues.stringVal(Len.ERR_LOG_EIBRESP2_DSPLY);

	//==== METHODS ====
	public byte[] getUbocErrorLoggingInfoBytes() {
		byte[] buffer = new byte[Len.UBOC_ERROR_LOGGING_INFO];
		return getUbocErrorLoggingInfoBytes(buffer, 1);
	}

	public void setUbocErrorLoggingInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		errorsLoggedSw.setErrorsLoggedSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		errorLoggingLvlSw.setErrorLoggingLvlSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		errLogSqlcodeDsply = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.ERR_LOG_SQLCODE_DSPLY), Len.ERR_LOG_SQLCODE_DSPLY);
		position += Len.ERR_LOG_SQLCODE_DSPLY;
		errLogEibrespDsply = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.ERR_LOG_EIBRESP_DSPLY), Len.ERR_LOG_EIBRESP_DSPLY);
		position += Len.ERR_LOG_EIBRESP_DSPLY;
		errLogEibresp2Dsply = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.ERR_LOG_EIBRESP2_DSPLY), Len.ERR_LOG_EIBRESP2_DSPLY);
	}

	public byte[] getUbocErrorLoggingInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, errorsLoggedSw.getErrorsLoggedSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, errorLoggingLvlSw.getErrorLoggingLvlSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, errLogSqlcodeDsply, Len.ERR_LOG_SQLCODE_DSPLY);
		position += Len.ERR_LOG_SQLCODE_DSPLY;
		MarshalByte.writeString(buffer, position, errLogEibrespDsply, Len.ERR_LOG_EIBRESP_DSPLY);
		position += Len.ERR_LOG_EIBRESP_DSPLY;
		MarshalByte.writeString(buffer, position, errLogEibresp2Dsply, Len.ERR_LOG_EIBRESP2_DSPLY);
		return buffer;
	}

	public void setErrLogSqlcodeDsply(long errLogSqlcodeDsply) {
		this.errLogSqlcodeDsply = PicFormatter.display("-Z(8)9").format(errLogSqlcodeDsply).toString();
	}

	public void setUbocErrLogSqlcodeDsplyFormatted(String ubocErrLogSqlcodeDsply) {
		this.errLogSqlcodeDsply = PicFormatter.display("-Z(8)9").format(ubocErrLogSqlcodeDsply).toString();
	}

	public long getErrLogSqlcodeDsply() {
		return PicParser.display("-Z(8)9").parseLong(this.errLogSqlcodeDsply);
	}

	public void setErrLogEibrespDsply(long errLogEibrespDsply) {
		this.errLogEibrespDsply = PicFormatter.display("-Z(8)9").format(errLogEibrespDsply).toString();
	}

	public void setUbocErrLogEibrespDsplyFormatted(String ubocErrLogEibrespDsply) {
		this.errLogEibrespDsply = PicFormatter.display("-Z(8)9").format(ubocErrLogEibrespDsply).toString();
	}

	public long getErrLogEibrespDsply() {
		return PicParser.display("-Z(8)9").parseLong(this.errLogEibrespDsply);
	}

	public void setErrLogEibresp2Dsply(long errLogEibresp2Dsply) {
		this.errLogEibresp2Dsply = PicFormatter.display("-Z(8)9").format(errLogEibresp2Dsply).toString();
	}

	public void setUbocErrLogEibresp2DsplyFormatted(String ubocErrLogEibresp2Dsply) {
		this.errLogEibresp2Dsply = PicFormatter.display("-Z(8)9").format(ubocErrLogEibresp2Dsply).toString();
	}

	public long getErrLogEibresp2Dsply() {
		return PicParser.display("-Z(8)9").parseLong(this.errLogEibresp2Dsply);
	}

	public UbocErrorLoggingLvlSw getErrorLoggingLvlSw() {
		return errorLoggingLvlSw;
	}

	public UbocErrorsLoggedSw getErrorsLoggedSw() {
		return errorsLoggedSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERR_LOG_SQLCODE_DSPLY = 10;
		public static final int ERR_LOG_EIBRESP_DSPLY = 10;
		public static final int ERR_LOG_EIBRESP2_DSPLY = 10;
		public static final int UBOC_ERROR_LOGGING_INFO = UbocErrorsLoggedSw.Len.ERRORS_LOGGED_SW + UbocErrorLoggingLvlSw.Len.ERROR_LOGGING_LVL_SW
				+ ERR_LOG_SQLCODE_DSPLY + ERR_LOG_EIBRESP_DSPLY + ERR_LOG_EIBRESP2_DSPLY;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
