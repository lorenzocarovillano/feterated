/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-61-USR-INVALID-APPL-ID-MSG<br>
 * Variable: EA-61-USR-INVALID-APPL-ID-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea61UsrInvalidApplIdMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-61-USR-INVALID-APPL-ID-MSG
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-61-USR-INVALID-APPL-ID-MSG-1
	private String flr2 = "INVALID CICS";
	//Original name: FILLER-EA-61-USR-INVALID-APPL-ID-MSG-2
	private String flr3 = "REGION ID.  (";
	//Original name: EA-61-APPL-ID
	private String ea61ApplId = DefaultValues.stringVal(Len.EA61_APPL_ID);
	//Original name: FILLER-EA-61-USR-INVALID-APPL-ID-MSG-3
	private char flr4 = ')';

	//==== METHODS ====
	public String getEa61UsrInvalidApplIdMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa61UsrInvalidApplIdMsgBytes());
	}

	public byte[] getEa61UsrInvalidApplIdMsgBytes() {
		byte[] buffer = new byte[Len.EA61_USR_INVALID_APPL_ID_MSG];
		return getEa61UsrInvalidApplIdMsgBytes(buffer, 1);
	}

	public byte[] getEa61UsrInvalidApplIdMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, ea61ApplId, Len.EA61_APPL_ID);
		position += Len.EA61_APPL_ID;
		MarshalByte.writeChar(buffer, position, flr4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEa61ApplId(String ea61ApplId) {
		this.ea61ApplId = Functions.subString(ea61ApplId, Len.EA61_APPL_ID);
	}

	public String getEa61ApplId() {
		return this.ea61ApplId;
	}

	public char getFlr4() {
		return this.flr4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA61_APPL_ID = 8;
		public static final int FLR1 = 11;
		public static final int FLR2 = 13;
		public static final int FLR4 = 1;
		public static final int EA61_USR_INVALID_APPL_ID_MSG = EA61_APPL_ID + FLR1 + 2 * FLR2 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
