/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-OPERATION-NAME<br>
 * Variable: WS-OPERATION-NAME from program XZ0R0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsOperationNameXz0r0008 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.OPERATION_NAME);
	public static final String ADD_ACT_NOT_POL = "AddAccountNotificationPolicy";
	public static final String UPD_ACT_NOT_POL = "UpdateAccountNotificationPolicy";
	private static final String[] VALID_OPERATION = new String[] { "AddAccountNotificationPolicy", "UpdateAccountNotificationPolicy" };

	//==== METHODS ====
	public void setOperationName(String operationName) {
		this.value = Functions.subString(operationName, Len.OPERATION_NAME);
	}

	public String getOperationName() {
		return this.value;
	}

	public boolean isValidOperation() {
		return ArrayUtils.contains(VALID_OPERATION, value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPERATION_NAME = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
