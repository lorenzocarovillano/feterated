/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPol1;
import com.federatedinsurance.crs.commons.data.to.ISysdummy1;
import com.federatedinsurance.crs.copy.DclactNot;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0G0001<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0g0001Data implements ISysdummy1, IActNotPol1 {

	//==== PROPERTIES ====
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0g0001 constantFields = new ConstantFieldsXz0g0001();
	//Original name: EA-03-MISSING-INPUT
	private Ea03MissingInput ea03MissingInput = new Ea03MissingInput();
	//Original name: EA-05-DB2-ERROR-ON-SELECT
	private Ea05Db2ErrorOnSelect ea05Db2ErrorOnSelect = new Ea05Db2ErrorOnSelect();
	//Original name: SA-PCN-CNT
	private short saPcnCnt = DefaultValues.BIN_SHORT_VAL;
	//Original name: SA-PARAGRAPH-NBR
	private String saParagraphNbr = DefaultValues.stringVal(Len.SA_PARAGRAPH_NBR);
	//Original name: SA-CURRENT-DT
	private String saCurrentDt = DefaultValues.stringVal(Len.SA_CURRENT_DT);
	//Original name: SA-ONE-YEAR-AGO-DT
	private String saOneYearAgoDt = DefaultValues.stringVal(Len.SA_ONE_YEAR_AGO_DT);

	//==== METHODS ====
	public void setSaPcnCnt(short saPcnCnt) {
		this.saPcnCnt = saPcnCnt;
	}

	public short getSaPcnCnt() {
		return this.saPcnCnt;
	}

	public void setSaParagraphNbr(String saParagraphNbr) {
		this.saParagraphNbr = Functions.subString(saParagraphNbr, Len.SA_PARAGRAPH_NBR);
	}

	public String getSaParagraphNbr() {
		return this.saParagraphNbr;
	}

	public void setSaCurrentDt(String saCurrentDt) {
		this.saCurrentDt = Functions.subString(saCurrentDt, Len.SA_CURRENT_DT);
	}

	public String getSaCurrentDt() {
		return this.saCurrentDt;
	}

	public void setSaOneYearAgoDt(String saOneYearAgoDt) {
		this.saOneYearAgoDt = Functions.subString(saOneYearAgoDt, Len.SA_ONE_YEAR_AGO_DT);
	}

	public String getSaOneYearAgoDt() {
		return this.saOneYearAgoDt;
	}

	@Override
	public String getActNotStaCd() {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public String getActNotTypCd() {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public String getActOwnCltId() {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public String getCfActNotImpending() {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public void setCfActNotImpending(String cfActNotImpending) {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public String getCfActNotNonpay() {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public void setCfActNotNonpay(String cfActNotNonpay) {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public String getCfActNotRescind() {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public void setCfActNotRescind(String cfActNotRescind) {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public String getCfActNotTypCdImp() {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	@Override
	public void setCfActNotTypCdImp(String cfActNotTypCdImp) {
		throw new FieldNotMappedException("cfActNotTypCdImp");
	}

	public ConstantFieldsXz0g0001 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public String getCurrentDt() {
		return getSaCurrentDt();
	}

	@Override
	public void setCurrentDt(String currentDt) {
		this.setSaCurrentDt(currentDt);
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public Ea03MissingInput getEa03MissingInput() {
		return ea03MissingInput;
	}

	public Ea05Db2ErrorOnSelect getEa05Db2ErrorOnSelect() {
		return ea05Db2ErrorOnSelect;
	}

	@Override
	public String getEmpId() {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public void setEmpId(String empId) {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	@Override
	public String getNotDt() {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public void setNotDt(String notDt) {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public String getNotEffDt() {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getOneYearAgoDt() {
		return getSaOneYearAgoDt();
	}

	@Override
	public void setOneYearAgoDt(String oneYearAgoDt) {
		this.setSaOneYearAgoDt(oneYearAgoDt);
	}

	@Override
	public String getPolEffDt() {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public String getPolExpDt() {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public String getPolNbr() {
		throw new FieldNotMappedException("polNbr");
	}

	@Override
	public void setPolNbr(String polNbr) {
		throw new FieldNotMappedException("polNbr");
	}

	@Override
	public String getPolTypCd() {
		throw new FieldNotMappedException("polTypCd");
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		throw new FieldNotMappedException("polTypCd");
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		return getTotFeeAmt().toString();
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
	}

	@Override
	public short getWsNbrOfNotDay() {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	@Override
	public void setWsNbrOfNotDay(short wsNbrOfNotDay) {
		throw new FieldNotMappedException("wsNbrOfNotDay");
	}

	@Override
	public String getWsPolicyCancDt() {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}

	@Override
	public void setWsPolicyCancDt(String wsPolicyCancDt) {
		throw new FieldNotMappedException("wsPolicyCancDt");
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_CURRENT_DT = 10;
		public static final int SA_ONE_YEAR_AGO_DT = 10;
		public static final int SA_PARAGRAPH_NBR = 5;
		public static final int SA_USER_ID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
