/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-HALRLOMG-LINKAGE-1<br>
 * Variable: WS-HALRLOMG-LINKAGE-1 from program HALRLOMG<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsHalrlomgLinkage1 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: UBOC-RECORD
	private UbocRecord ubocRecord = new UbocRecord();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_HALRLOMG_LINKAGE1;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsHalrlomgLinkage1Bytes(buf);
	}

	public String getWsHallubocCommsAreaFormatted() {
		return ubocRecord.getUbocRecordFormatted();
	}

	public void setWsHalrlomgLinkage1Bytes(byte[] buffer) {
		setWsHalrlomgLinkage1Bytes(buffer, 1);
	}

	public byte[] getWsHalrlomgLinkage1Bytes() {
		byte[] buffer = new byte[Len.WS_HALRLOMG_LINKAGE1];
		return getWsHalrlomgLinkage1Bytes(buffer, 1);
	}

	public void setWsHalrlomgLinkage1Bytes(byte[] buffer, int offset) {
		int position = offset;
		ubocRecord.setUbocRecordBytes(buffer, position);
	}

	public byte[] getWsHalrlomgLinkage1Bytes(byte[] buffer, int offset) {
		int position = offset;
		ubocRecord.getUbocRecordBytes(buffer, position);
		return buffer;
	}

	public UbocRecord getUbocRecord() {
		return ubocRecord;
	}

	@Override
	public byte[] serialize() {
		return getWsHalrlomgLinkage1Bytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_HALRLOMG_LINKAGE1 = UbocRecord.Len.UBOC_RECORD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
