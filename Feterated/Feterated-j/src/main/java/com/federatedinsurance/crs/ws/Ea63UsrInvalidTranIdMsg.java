/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-63-USR-INVALID-TRAN-ID-MSG<br>
 * Variable: EA-63-USR-INVALID-TRAN-ID-MSG from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea63UsrInvalidTranIdMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-63-USR-INVALID-TRAN-ID-MSG
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-63-USR-INVALID-TRAN-ID-MSG-1
	private String flr2 = "INVALID TRAN ID";
	//Original name: FILLER-EA-63-USR-INVALID-TRAN-ID-MSG-2
	private String flr3 = " (";
	//Original name: EA-63-TRAN-ID
	private String ea63TranId = DefaultValues.stringVal(Len.EA63_TRAN_ID);
	//Original name: FILLER-EA-63-USR-INVALID-TRAN-ID-MSG-3
	private String flr4 = ") WAS PASSED TO";
	//Original name: FILLER-EA-63-USR-INVALID-TRAN-ID-MSG-4
	private String flr5 = "CICS.";

	//==== METHODS ====
	public String getEa63UsrInvalidTranIdMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa63UsrInvalidTranIdMsgBytes());
	}

	public byte[] getEa63UsrInvalidTranIdMsgBytes() {
		byte[] buffer = new byte[Len.EA63_USR_INVALID_TRAN_ID_MSG];
		return getEa63UsrInvalidTranIdMsgBytes(buffer, 1);
	}

	public byte[] getEa63UsrInvalidTranIdMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea63TranId, Len.EA63_TRAN_ID);
		position += Len.EA63_TRAN_ID;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEa63TranId(String ea63TranId) {
		this.ea63TranId = Functions.subString(ea63TranId, Len.EA63_TRAN_ID);
	}

	public String getEa63TranId() {
		return this.ea63TranId;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA63_TRAN_ID = 4;
		public static final int FLR1 = 11;
		public static final int FLR2 = 15;
		public static final int FLR3 = 2;
		public static final int FLR5 = 5;
		public static final int EA63_USR_INVALID_TRAN_ID_MSG = EA63_TRAN_ID + FLR1 + 2 * FLR2 + FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
