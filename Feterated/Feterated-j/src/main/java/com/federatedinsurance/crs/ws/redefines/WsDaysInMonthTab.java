/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WS-DAYS-IN-MONTH-TAB<br>
 * Variable: WS-DAYS-IN-MONTH-TAB from program HALRVDT1<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsDaysInMonthTab extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int MONTH_MAXOCCURS = 12;

	//==== CONSTRUCTORS ====
	public WsDaysInMonthTab() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_DAYS_IN_MONTH_TAB;
	}

	@Override
	public void init() {
		int position = 1;
		writeShort(position, ((short) 31), Len.Int.JAN, SignType.NO_SIGN);
		position += Len.JAN;
		writeShort(position, ((short) 28), Len.Int.FEB, SignType.NO_SIGN);
		position += Len.FEB;
		writeShort(position, ((short) 31), Len.Int.MAR, SignType.NO_SIGN);
		position += Len.MAR;
		writeShort(position, ((short) 30), Len.Int.APR, SignType.NO_SIGN);
		position += Len.APR;
		writeShort(position, ((short) 31), Len.Int.MAY, SignType.NO_SIGN);
		position += Len.MAY;
		writeShort(position, ((short) 30), Len.Int.JUN, SignType.NO_SIGN);
		position += Len.JUN;
		writeShort(position, ((short) 31), Len.Int.JUL, SignType.NO_SIGN);
		position += Len.JUL;
		writeShort(position, ((short) 31), Len.Int.AUG, SignType.NO_SIGN);
		position += Len.AUG;
		writeShort(position, ((short) 30), Len.Int.SEP, SignType.NO_SIGN);
		position += Len.SEP;
		writeShort(position, ((short) 31), Len.Int.OCT, SignType.NO_SIGN);
		position += Len.OCT;
		writeShort(position, ((short) 30), Len.Int.NOV, SignType.NO_SIGN);
		position += Len.NOV;
		writeShort(position, ((short) 31), Len.Int.DEC_FLD, SignType.NO_SIGN);
	}

	/**Original name: WS-DAYS-IN-MONTH<br>*/
	public short getMonth(int monthIdx) {
		int position = Pos.wsDaysInMonth(monthIdx - 1);
		return readNumDispUnsignedShort(position, Len.MONTH);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_DAYS_IN_MONTH_TAB = 1;
		public static final int JAN = WS_DAYS_IN_MONTH_TAB;
		public static final int FEB = JAN + Len.JAN;
		public static final int MAR = FEB + Len.FEB;
		public static final int APR = MAR + Len.MAR;
		public static final int MAY = APR + Len.APR;
		public static final int JUN = MAY + Len.MAY;
		public static final int JUL = JUN + Len.JUN;
		public static final int AUG = JUL + Len.JUL;
		public static final int SEP = AUG + Len.AUG;
		public static final int OCT = SEP + Len.SEP;
		public static final int NOV = OCT + Len.OCT;
		public static final int DEC_FLD = NOV + Len.NOV;
		public static final int FILLER_WS_WORK_FIELDS = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int wsDaysInMonth(int idx) {
			return FILLER_WS_WORK_FIELDS + idx * Len.MONTH;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int JAN = 2;
		public static final int FEB = 2;
		public static final int MAR = 2;
		public static final int APR = 2;
		public static final int MAY = 2;
		public static final int JUN = 2;
		public static final int JUL = 2;
		public static final int AUG = 2;
		public static final int SEP = 2;
		public static final int OCT = 2;
		public static final int NOV = 2;
		public static final int MONTH = 2;
		public static final int DEC_FLD = 2;
		public static final int WS_DAYS_IN_MONTH_TAB = JAN + FEB + MAR + APR + MAY + JUN + JUL + AUG + SEP + OCT + NOV + DEC_FLD;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int JAN = 2;
			public static final int FEB = 2;
			public static final int MAR = 2;
			public static final int APR = 2;
			public static final int MAY = 2;
			public static final int JUN = 2;
			public static final int JUL = 2;
			public static final int AUG = 2;
			public static final int SEP = 2;
			public static final int OCT = 2;
			public static final int NOV = 2;
			public static final int DEC_FLD = 2;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
