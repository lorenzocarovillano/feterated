/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: L-RD-ACTION-INDICATOR<br>
 * Variable: L-RD-ACTION-INDICATOR from program TS030199<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LRdActionIndicator {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CONNECT = 'C';
	public static final char DISCONNECT = 'D';
	public static final char RETRIEVE = 'R';

	//==== METHODS ====
	public void setActionIndicator(char actionIndicator) {
		this.value = actionIndicator;
	}

	public char getActionIndicator() {
		return this.value;
	}

	public void setRdAiConnect() {
		value = CONNECT;
	}

	public void setRdAiDisconnect() {
		value = DISCONNECT;
	}

	public void setRdAiRetrieve() {
		value = RETRIEVE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACTION_INDICATOR = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
