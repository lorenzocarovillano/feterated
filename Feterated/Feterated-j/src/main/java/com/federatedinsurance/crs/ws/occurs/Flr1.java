/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: FILLER-RI-OFFICE-LOCATION-DATA<br>
 * Variables: FILLER-RI-OFFICE-LOCATION-DATA from program TS030099<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Flr1 {

	//==== PROPERTIES ====
	//Original name: RI-REPORT-OFFICE-LOCATION
	private String location = DefaultValues.stringVal(Len.LOCATION);
	//Original name: RI-REPORT-OFFICE-LOCATION-FLAG
	private char locationFlag = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setFlr1Bytes(byte[] buffer, int offset) {
		int position = offset;
		location = MarshalByte.readString(buffer, position, Len.LOCATION);
		position += Len.LOCATION;
		locationFlag = MarshalByte.readChar(buffer, position);
	}

	public void initFlr1Spaces() {
		location = "";
		locationFlag = Types.SPACE_CHAR;
	}

	public void setLocation(String location) {
		this.location = Functions.subString(location, Len.LOCATION);
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocationFlag(char locationFlag) {
		this.locationFlag = locationFlag;
	}

	public char getLocationFlag() {
		return this.locationFlag;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LOCATION = 2;
		public static final int LOCATION_FLAG = 1;
		public static final int FLR1 = LOCATION + LOCATION_FLAG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
