/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: HSDD-DFL-TYP-IND<br>
 * Variable: HSDD-DFL-TYP-IND from copybook HALLGSDD<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HsddDflTypInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CHAR_FLD = 'C';
	public static final char NUM = 'N';
	public static final char DATE_FLD = 'D';
	public static final char TS = 'T';
	public static final char LINK = 'L';

	//==== METHODS ====
	public void setDflTypInd(char dflTypInd) {
		this.value = dflTypInd;
	}

	public char getDflTypInd() {
		return this.value;
	}

	public boolean isLink() {
		return value == LINK;
	}
}
