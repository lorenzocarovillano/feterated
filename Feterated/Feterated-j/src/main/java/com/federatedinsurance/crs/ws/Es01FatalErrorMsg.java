/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: ES-01-FATAL-ERROR-MSG<br>
 * Variable: ES-01-FATAL-ERROR-MSG from program XZ0B9080<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Es01FatalErrorMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-ES-01-FATAL-ERROR-MSG
	private String flr1 = "";
	//Original name: ES-01-FAILED-MODULE
	private String failedModule = DefaultValues.stringVal(Len.FAILED_MODULE);
	//Original name: FILLER-ES-01-FATAL-ERROR-MSG-1
	private String flr2 = "";
	//Original name: ES-01-FAILED-PARAGRAPH
	private String failedParagraph = DefaultValues.stringVal(Len.FAILED_PARAGRAPH);
	//Original name: FILLER-ES-01-FATAL-ERROR-MSG-2
	private String flr3 = "";
	//Original name: ES-01-SQLCODE-DISPLAY
	private String sqlcodeDisplay = DefaultValues.stringVal(Len.SQLCODE_DISPLAY);
	//Original name: FILLER-ES-01-FATAL-ERROR-MSG-3
	private String flr4 = "";
	//Original name: ES-01-EIBRESP-DISPLAY
	private String eibrespDisplay = DefaultValues.stringVal(Len.EIBRESP_DISPLAY);
	//Original name: FILLER-ES-01-FATAL-ERROR-MSG-4
	private String flr5 = "";
	//Original name: ES-01-EIBRESP2-DISPLAY
	private String eibresp2Display = DefaultValues.stringVal(Len.EIBRESP2_DISPLAY);

	//==== METHODS ====
	public void setEs01FatalErrorMsgFormatted(String data) {
		byte[] buffer = new byte[Len.ES01_FATAL_ERROR_MSG];
		MarshalByte.writeString(buffer, 1, data, Len.ES01_FATAL_ERROR_MSG);
		setEs01FatalErrorMsgBytes(buffer, 1);
	}

	public void setEs01FatalErrorMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		failedModule = MarshalByte.readString(buffer, position, Len.FAILED_MODULE);
		position += Len.FAILED_MODULE;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR2);
		position += Len.FLR2;
		failedParagraph = MarshalByte.readString(buffer, position, Len.FAILED_PARAGRAPH);
		position += Len.FAILED_PARAGRAPH;
		flr3 = MarshalByte.readString(buffer, position, Len.FLR3);
		position += Len.FLR3;
		sqlcodeDisplay = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.SQLCODE_DISPLAY), Len.SQLCODE_DISPLAY);
		position += Len.SQLCODE_DISPLAY;
		flr4 = MarshalByte.readString(buffer, position, Len.FLR4);
		position += Len.FLR4;
		eibrespDisplay = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.EIBRESP_DISPLAY), Len.EIBRESP_DISPLAY);
		position += Len.EIBRESP_DISPLAY;
		flr5 = MarshalByte.readString(buffer, position, Len.FLR5);
		position += Len.FLR5;
		eibresp2Display = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.EIBRESP2_DISPLAY), Len.EIBRESP2_DISPLAY);
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setFailedModule(String failedModule) {
		this.failedModule = Functions.subString(failedModule, Len.FAILED_MODULE);
	}

	public String getFailedModule() {
		return this.failedModule;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR2);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setFailedParagraph(String failedParagraph) {
		this.failedParagraph = Functions.subString(failedParagraph, Len.FAILED_PARAGRAPH);
	}

	public String getFailedParagraph() {
		return this.failedParagraph;
	}

	public void setFlr3(String flr3) {
		this.flr3 = Functions.subString(flr3, Len.FLR3);
	}

	public String getFlr3() {
		return this.flr3;
	}

	public long getSqlcodeDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.sqlcodeDisplay);
	}

	public void setFlr4(String flr4) {
		this.flr4 = Functions.subString(flr4, Len.FLR4);
	}

	public String getFlr4() {
		return this.flr4;
	}

	public long getEibrespDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.eibrespDisplay);
	}

	public void setFlr5(String flr5) {
		this.flr5 = Functions.subString(flr5, Len.FLR5);
	}

	public String getFlr5() {
		return this.flr5;
	}

	public long getEibresp2Display() {
		return PicParser.display("-Z(8)9").parseLong(this.eibresp2Display);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FAILED_MODULE = 8;
		public static final int FAILED_PARAGRAPH = 30;
		public static final int SQLCODE_DISPLAY = 10;
		public static final int EIBRESP_DISPLAY = 10;
		public static final int EIBRESP2_DISPLAY = 10;
		public static final int FLR1 = 75;
		public static final int FLR2 = 23;
		public static final int FLR3 = 14;
		public static final int FLR4 = 19;
		public static final int FLR5 = 20;
		public static final int ES01_FATAL_ERROR_MSG = FAILED_MODULE + FAILED_PARAGRAPH + SQLCODE_DISPLAY + EIBRESP_DISPLAY + EIBRESP2_DISPLAY + FLR1
				+ FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
