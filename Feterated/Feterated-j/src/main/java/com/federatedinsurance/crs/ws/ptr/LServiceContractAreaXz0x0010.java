/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0010<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0010 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0010() {
	}

	public LServiceContractAreaXz0x0010(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt10iTkNotPrcTs(String xzt10iTkNotPrcTs) {
		writeString(Pos.XZT10I_TK_NOT_PRC_TS, xzt10iTkNotPrcTs, Len.XZT10I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT10I-TK-NOT-PRC-TS<br>*/
	public String getXzt10iTkNotPrcTs() {
		return readString(Pos.XZT10I_TK_NOT_PRC_TS, Len.XZT10I_TK_NOT_PRC_TS);
	}

	public void setXzt10iTkStWrdSeqCd(String xzt10iTkStWrdSeqCd) {
		writeString(Pos.XZT10I_TK_ST_WRD_SEQ_CD, xzt10iTkStWrdSeqCd, Len.XZT10I_TK_ST_WRD_SEQ_CD);
	}

	/**Original name: XZT10I-TK-ST-WRD-SEQ-CD<br>*/
	public String getXzt10iTkStWrdSeqCd() {
		return readString(Pos.XZT10I_TK_ST_WRD_SEQ_CD, Len.XZT10I_TK_ST_WRD_SEQ_CD);
	}

	public void setXzt10iCsrActNbr(String xzt10iCsrActNbr) {
		writeString(Pos.XZT10I_CSR_ACT_NBR, xzt10iCsrActNbr, Len.XZT10I_CSR_ACT_NBR);
	}

	/**Original name: XZT10I-CSR-ACT-NBR<br>*/
	public String getXzt10iCsrActNbr() {
		return readString(Pos.XZT10I_CSR_ACT_NBR, Len.XZT10I_CSR_ACT_NBR);
	}

	public void setXzt10iPolNbr(String xzt10iPolNbr) {
		writeString(Pos.XZT10I_POL_NBR, xzt10iPolNbr, Len.XZT10I_POL_NBR);
	}

	/**Original name: XZT10I-POL-NBR<br>*/
	public String getXzt10iPolNbr() {
		return readString(Pos.XZT10I_POL_NBR, Len.XZT10I_POL_NBR);
	}

	public void setXzt10iUserid(String xzt10iUserid) {
		writeString(Pos.XZT10I_USERID, xzt10iUserid, Len.XZT10I_USERID);
	}

	/**Original name: XZT10I-USERID<br>*/
	public String getXzt10iUserid() {
		return readString(Pos.XZT10I_USERID, Len.XZT10I_USERID);
	}

	public String getXzt10iUseridFormatted() {
		return Functions.padBlanks(getXzt10iUserid(), Len.XZT10I_USERID);
	}

	public void setXzt10oTkNotPrcTs(String xzt10oTkNotPrcTs) {
		writeString(Pos.XZT10O_TK_NOT_PRC_TS, xzt10oTkNotPrcTs, Len.XZT10O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT10O-TK-NOT-PRC-TS<br>*/
	public String getXzt10oTkNotPrcTs() {
		return readString(Pos.XZT10O_TK_NOT_PRC_TS, Len.XZT10O_TK_NOT_PRC_TS);
	}

	public void setXzt10oTkStWrdSeqCd(String xzt10oTkStWrdSeqCd) {
		writeString(Pos.XZT10O_TK_ST_WRD_SEQ_CD, xzt10oTkStWrdSeqCd, Len.XZT10O_TK_ST_WRD_SEQ_CD);
	}

	/**Original name: XZT10O-TK-ST-WRD-SEQ-CD<br>*/
	public String getXzt10oTkStWrdSeqCd() {
		return readString(Pos.XZT10O_TK_ST_WRD_SEQ_CD, Len.XZT10O_TK_ST_WRD_SEQ_CD);
	}

	public void setXzt10oTkActNotWrdCsumFormatted(String xzt10oTkActNotWrdCsum) {
		writeString(Pos.XZT10O_TK_ACT_NOT_WRD_CSUM, Trunc.toUnsignedNumeric(xzt10oTkActNotWrdCsum, Len.XZT10O_TK_ACT_NOT_WRD_CSUM),
				Len.XZT10O_TK_ACT_NOT_WRD_CSUM);
	}

	/**Original name: XZT10O-TK-ACT-NOT-WRD-CSUM<br>*/
	public int getXzt10oTkActNotWrdCsum() {
		return readNumDispUnsignedInt(Pos.XZT10O_TK_ACT_NOT_WRD_CSUM, Len.XZT10O_TK_ACT_NOT_WRD_CSUM);
	}

	public void setXzt10oCsrActNbr(String xzt10oCsrActNbr) {
		writeString(Pos.XZT10O_CSR_ACT_NBR, xzt10oCsrActNbr, Len.XZT10O_CSR_ACT_NBR);
	}

	/**Original name: XZT10O-CSR-ACT-NBR<br>*/
	public String getXzt10oCsrActNbr() {
		return readString(Pos.XZT10O_CSR_ACT_NBR, Len.XZT10O_CSR_ACT_NBR);
	}

	public void setXzt10oPolNbr(String xzt10oPolNbr) {
		writeString(Pos.XZT10O_POL_NBR, xzt10oPolNbr, Len.XZT10O_POL_NBR);
	}

	/**Original name: XZT10O-POL-NBR<br>*/
	public String getXzt10oPolNbr() {
		return readString(Pos.XZT10O_POL_NBR, Len.XZT10O_POL_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT010_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT10I_TECHNICAL_KEY = XZT010_SERVICE_INPUTS;
		public static final int XZT10I_TK_NOT_PRC_TS = XZT10I_TECHNICAL_KEY;
		public static final int XZT10I_TK_ST_WRD_SEQ_CD = XZT10I_TK_NOT_PRC_TS + Len.XZT10I_TK_NOT_PRC_TS;
		public static final int XZT10I_CSR_ACT_NBR = XZT10I_TK_ST_WRD_SEQ_CD + Len.XZT10I_TK_ST_WRD_SEQ_CD;
		public static final int XZT10I_POL_NBR = XZT10I_CSR_ACT_NBR + Len.XZT10I_CSR_ACT_NBR;
		public static final int XZT10I_USERID = XZT10I_POL_NBR + Len.XZT10I_POL_NBR;
		public static final int XZT010_SERVICE_OUTPUTS = XZT10I_USERID + Len.XZT10I_USERID;
		public static final int XZT10O_TECHNICAL_KEY = XZT010_SERVICE_OUTPUTS;
		public static final int XZT10O_TK_NOT_PRC_TS = XZT10O_TECHNICAL_KEY;
		public static final int XZT10O_TK_ST_WRD_SEQ_CD = XZT10O_TK_NOT_PRC_TS + Len.XZT10O_TK_NOT_PRC_TS;
		public static final int XZT10O_TK_ACT_NOT_WRD_CSUM = XZT10O_TK_ST_WRD_SEQ_CD + Len.XZT10O_TK_ST_WRD_SEQ_CD;
		public static final int XZT10O_CSR_ACT_NBR = XZT10O_TK_ACT_NOT_WRD_CSUM + Len.XZT10O_TK_ACT_NOT_WRD_CSUM;
		public static final int XZT10O_POL_NBR = XZT10O_CSR_ACT_NBR + Len.XZT10O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT10I_TK_NOT_PRC_TS = 26;
		public static final int XZT10I_TK_ST_WRD_SEQ_CD = 5;
		public static final int XZT10I_CSR_ACT_NBR = 9;
		public static final int XZT10I_POL_NBR = 25;
		public static final int XZT10I_USERID = 8;
		public static final int XZT10O_TK_NOT_PRC_TS = 26;
		public static final int XZT10O_TK_ST_WRD_SEQ_CD = 5;
		public static final int XZT10O_TK_ACT_NOT_WRD_CSUM = 9;
		public static final int XZT10O_CSR_ACT_NBR = 9;
		public static final int XZT10I_TECHNICAL_KEY = XZT10I_TK_NOT_PRC_TS + XZT10I_TK_ST_WRD_SEQ_CD;
		public static final int XZT010_SERVICE_INPUTS = XZT10I_TECHNICAL_KEY + XZT10I_CSR_ACT_NBR + XZT10I_POL_NBR + XZT10I_USERID;
		public static final int XZT10O_TECHNICAL_KEY = XZT10O_TK_NOT_PRC_TS + XZT10O_TK_ST_WRD_SEQ_CD + XZT10O_TK_ACT_NOT_WRD_CSUM;
		public static final int XZT10O_POL_NBR = 25;
		public static final int XZT010_SERVICE_OUTPUTS = XZT10O_TECHNICAL_KEY + XZT10O_CSR_ACT_NBR + XZT10O_POL_NBR;
		public static final int L_SERVICE_CONTRACT_AREA = XZT010_SERVICE_INPUTS + XZT010_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
