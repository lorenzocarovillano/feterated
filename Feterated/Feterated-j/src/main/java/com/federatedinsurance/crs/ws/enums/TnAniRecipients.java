/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TN-ANI-RECIPIENTS<br>
 * Variable: TN-ANI-RECIPIENTS from program XZ0P90E0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TnAniRecipients implements ICopyable<TnAniRecipients> {

	//==== PROPERTIES ====
	public static final int POLICY_NBRS_MAXOCCURS = 100;
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TN_ANI_RECIPIENTS);
	//Original name: TN-REC-SEQ-NBR
	private int recSeqNbr = DefaultValues.INT_VAL;
	//Original name: TN-POLICY-NBRS
	private TnPolicyNbrs[] policyNbrs = new TnPolicyNbrs[POLICY_NBRS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public TnAniRecipients() {
		init();
	}

	public TnAniRecipients(TnAniRecipients tnAniRecipients) {
		this();
		this.recSeqNbr = tnAniRecipients.recSeqNbr;
		for (int policyNbrsIdx = 1; policyNbrsIdx <= TnAniRecipients.POLICY_NBRS_MAXOCCURS; policyNbrsIdx++) {
			this.policyNbrs[policyNbrsIdx - 1] = tnAniRecipients.policyNbrs[policyNbrsIdx - 1].copy();
		}
	}

	//==== METHODS ====
	public void init() {
		for (int policyNbrsIdx = 1; policyNbrsIdx <= POLICY_NBRS_MAXOCCURS; policyNbrsIdx++) {
			policyNbrs[policyNbrsIdx - 1] = new TnPolicyNbrs();
		}
	}

	public String getTnAniRecipientsFormatted() {
		return MarshalByteExt.bufferToStr(getTnAniRecipientsBytes());
	}

	public byte[] getTnAniRecipientsBytes() {
		byte[] buffer = new byte[Len.TN_ANI_RECIPIENTS];
		return getTnAniRecipientsBytes(buffer, 1);
	}

	public byte[] getTnAniRecipientsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeInt(buffer, position, recSeqNbr, Len.REC_SEQ_NBR);
		position += Len.REC_SEQ_NBR;
		for (int idx = 1; idx <= POLICY_NBRS_MAXOCCURS; idx++) {
			policyNbrs[idx - 1].getPolicyNbrsBytes(buffer, position);
			position += TnPolicyNbrs.Len.POLICY_NBRS;
		}
		return buffer;
	}

	public TnAniRecipients initTnAniRecipientsHighValues() {
		recSeqNbr = Types.HIGH_INT_VAL;
		for (int idx = 1; idx <= POLICY_NBRS_MAXOCCURS; idx++) {
			policyNbrs[idx - 1].initPolicyNbrsHighValues();
		}
		return this;
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTnAniRecipientsFormatted()).equals(END_OF_TABLE);
	}

	public void setRecSeqNbr(int recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	public int getRecSeqNbr() {
		return this.recSeqNbr;
	}

	public TnPolicyNbrs getPolicyNbrs(int idx) {
		return policyNbrs[idx - 1];
	}

	@Override
	public TnAniRecipients copy() {
		return new TnAniRecipients(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_SEQ_NBR = 5;
		public static final int TN_ANI_RECIPIENTS = REC_SEQ_NBR + TnAniRecipients.POLICY_NBRS_MAXOCCURS * TnPolicyNbrs.Len.POLICY_NBRS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
