/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalLokObjDetV;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WS-TSQ-FIELDS<br>
 * Variable: WS-TSQ-FIELDS from program HALRLOMG<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class WsTsqFields implements IHalLokObjDetV {

	//==== PROPERTIES ====
	//Original name: WS-SESSION-ID
	private String sessionId = DefaultValues.stringVal(Len.SESSION_ID);
	//Original name: WS-TCH-KEY
	private String tchKey = DefaultValues.stringVal(Len.TCH_KEY);
	//Original name: WS-APP-ID
	private String appId = DefaultValues.stringVal(Len.APP_ID);
	//Original name: WS-TABLE-NM
	private String tableNm = DefaultValues.stringVal(Len.TABLE_NM);
	//Original name: WS-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: WS-LOK-TYPE-CD
	private char lokTypeCd = DefaultValues.CHAR_VAL;
	//Original name: WS-LOKDET-ROW-LGTH
	private short lokdetRowLgth = ((short) 219);

	//==== METHODS ====
	public String getLokdetRowFormatted() {
		return MarshalByteExt.bufferToStr(getLokdetRowBytes());
	}

	/**Original name: WS-LOKDET-ROW<br>*/
	public byte[] getLokdetRowBytes() {
		byte[] buffer = new byte[Len.LOKDET_ROW];
		return getLokdetRowBytes(buffer, 1);
	}

	public byte[] getLokdetRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
		position += Len.SESSION_ID;
		MarshalByte.writeString(buffer, position, tchKey, Len.TCH_KEY);
		position += Len.TCH_KEY;
		MarshalByte.writeString(buffer, position, appId, Len.APP_ID);
		position += Len.APP_ID;
		MarshalByte.writeString(buffer, position, tableNm, Len.TABLE_NM);
		position += Len.TABLE_NM;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, lokTypeCd);
		return buffer;
	}

	@Override
	public void setSessionId(String sessionId) {
		this.sessionId = Functions.subString(sessionId, Len.SESSION_ID);
	}

	@Override
	public String getSessionId() {
		return this.sessionId;
	}

	@Override
	public void setTchKey(String tchKey) {
		this.tchKey = Functions.subString(tchKey, Len.TCH_KEY);
	}

	@Override
	public String getTchKey() {
		return this.tchKey;
	}

	@Override
	public void setAppId(String appId) {
		this.appId = Functions.subString(appId, Len.APP_ID);
	}

	@Override
	public String getAppId() {
		return this.appId;
	}

	@Override
	public void setTableNm(String tableNm) {
		this.tableNm = Functions.subString(tableNm, Len.TABLE_NM);
	}

	@Override
	public String getTableNm() {
		return this.tableNm;
	}

	@Override
	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	@Override
	public String getUserId() {
		return this.userId;
	}

	@Override
	public void setLokTypeCd(char lokTypeCd) {
		this.lokTypeCd = lokTypeCd;
	}

	@Override
	public char getLokTypeCd() {
		return this.lokTypeCd;
	}

	public short getLokdetRowLgth() {
		return this.lokdetRowLgth;
	}

	@Override
	public String getUpdByMsgId() {
		throw new FieldNotMappedException("updByMsgId");
	}

	@Override
	public void setUpdByMsgId(String updByMsgId) {
		throw new FieldNotMappedException("updByMsgId");
	}

	@Override
	public String getUserid() {
		throw new FieldNotMappedException("userid");
	}

	@Override
	public void setUserid(String userid) {
		throw new FieldNotMappedException("userid");
	}

	@Override
	public short getWsTmoValue() {
		throw new FieldNotMappedException("wsTmoValue");
	}

	@Override
	public void setWsTmoValue(short wsTmoValue) {
		throw new FieldNotMappedException("wsTmoValue");
	}

	@Override
	public String getZappedBy() {
		throw new FieldNotMappedException("zappedBy");
	}

	@Override
	public void setZappedBy(String zappedBy) {
		throw new FieldNotMappedException("zappedBy");
	}

	@Override
	public char getZappedInd() {
		throw new FieldNotMappedException("zappedInd");
	}

	@Override
	public void setZappedInd(char zappedInd) {
		throw new FieldNotMappedException("zappedInd");
	}

	@Override
	public String getZappedTs() {
		throw new FieldNotMappedException("zappedTs");
	}

	@Override
	public void setZappedTs(String zappedTs) {
		throw new FieldNotMappedException("zappedTs");
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SESSION_ID = 32;
		public static final int TCH_KEY = 126;
		public static final int APP_ID = 10;
		public static final int TABLE_NM = 18;
		public static final int USER_ID = 32;
		public static final int LOK_TYPE_CD = 1;
		public static final int LOKDET_ROW = SESSION_ID + TCH_KEY + APP_ID + TABLE_NM + USER_ID + LOK_TYPE_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
