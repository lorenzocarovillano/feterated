/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.ProxyProgramCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0P90R0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0p90r0Data {

	//==== PROPERTIES ====
	//Original name: CF-PN-GET-PND-CNC-STATUS
	private String cfPnGetPndCncStatus = "XZC01090";
	//Original name: CF-PN-GET-POL-TMN-STATUS
	private String cfPnGetPolTmnStatus = "XZC02090";
	//Original name: SS-MSG-IDX
	private short ssMsgIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short SS_MSG_IDX_MAX = ((short) 10);
	//Original name: SS-WNG-IDX
	private short ssWngIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short SS_WNG_IDX_MAX = ((short) 10);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0p90r0 workingStorageArea = new WorkingStorageAreaXz0p90r0();
	//Original name: WS-XZC010C1-ROW
	private DfhcommareaXzc01090 wsXzc010c1Row = new DfhcommareaXzc01090();
	//Original name: WS-XZC020C1-ROW
	private DfhcommareaXzc02090 wsXzc020c1Row = new DfhcommareaXzc02090();
	//Original name: WS-XZ0Y90R0-ROW
	private WsXz0y90r0Row wsXz0y90r0Row = new WsXz0y90r0Row();
	//Original name: PROXY-PROGRAM-COMMON
	private ProxyProgramCommon proxyProgramCommon = new ProxyProgramCommon();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public String getCfPnGetPndCncStatus() {
		return this.cfPnGetPndCncStatus;
	}

	public String getCfPnGetPolTmnStatus() {
		return this.cfPnGetPolTmnStatus;
	}

	public void setSsMsgIdx(short ssMsgIdx) {
		this.ssMsgIdx = ssMsgIdx;
	}

	public short getSsMsgIdx() {
		return this.ssMsgIdx;
	}

	public boolean isSsMsgIdxMax() {
		return ssMsgIdx == SS_MSG_IDX_MAX;
	}

	public void setSsWngIdx(short ssWngIdx) {
		this.ssWngIdx = ssWngIdx;
	}

	public short getSsWngIdx() {
		return this.ssWngIdx;
	}

	public boolean isSsWngIdxMax() {
		return ssWngIdx == SS_WNG_IDX_MAX;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public ProxyProgramCommon getProxyProgramCommon() {
		return proxyProgramCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0p90r0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0y90r0Row getWsXz0y90r0Row() {
		return wsXz0y90r0Row;
	}

	public DfhcommareaXzc01090 getWsXzc010c1Row() {
		return wsXzc010c1Row;
	}

	public DfhcommareaXzc02090 getWsXzc020c1Row() {
		return wsXzc020c1Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
