/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9020<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9020 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT92O_REC_LIST_MAXOCCURS = 750;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9020() {
	}

	public LServiceContractAreaXz0x9020(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt92iTkNotPrcTs(String xzt92iTkNotPrcTs) {
		writeString(Pos.XZT92I_TK_NOT_PRC_TS, xzt92iTkNotPrcTs, Len.XZT92I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT92I-TK-NOT-PRC-TS<br>*/
	public String getXzt92iTkNotPrcTs() {
		return readString(Pos.XZT92I_TK_NOT_PRC_TS, Len.XZT92I_TK_NOT_PRC_TS);
	}

	public void setXzt92iTkFrmSeqNbr(int xzt92iTkFrmSeqNbr) {
		writeInt(Pos.XZT92I_TK_FRM_SEQ_NBR, xzt92iTkFrmSeqNbr, Len.Int.XZT92I_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT92I-TK-FRM-SEQ-NBR<br>*/
	public int getXzt92iTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT92I_TK_FRM_SEQ_NBR, Len.XZT92I_TK_FRM_SEQ_NBR);
	}

	public void setXzt92iTkRecallRecSeqNbr(int xzt92iTkRecallRecSeqNbr) {
		writeInt(Pos.XZT92I_TK_RECALL_REC_SEQ_NBR, xzt92iTkRecallRecSeqNbr, Len.Int.XZT92I_TK_RECALL_REC_SEQ_NBR);
	}

	/**Original name: XZT92I-TK-RECALL-REC-SEQ-NBR<br>*/
	public int getXzt92iTkRecallRecSeqNbr() {
		return readNumDispInt(Pos.XZT92I_TK_RECALL_REC_SEQ_NBR, Len.XZT92I_TK_RECALL_REC_SEQ_NBR);
	}

	public void setXzt92iCsrActNbr(String xzt92iCsrActNbr) {
		writeString(Pos.XZT92I_CSR_ACT_NBR, xzt92iCsrActNbr, Len.XZT92I_CSR_ACT_NBR);
	}

	/**Original name: XZT92I-CSR-ACT-NBR<br>*/
	public String getXzt92iCsrActNbr() {
		return readString(Pos.XZT92I_CSR_ACT_NBR, Len.XZT92I_CSR_ACT_NBR);
	}

	public void setXzt92iUserid(String xzt92iUserid) {
		writeString(Pos.XZT92I_USERID, xzt92iUserid, Len.XZT92I_USERID);
	}

	/**Original name: XZT92I-USERID<br>*/
	public String getXzt92iUserid() {
		return readString(Pos.XZT92I_USERID, Len.XZT92I_USERID);
	}

	public String getXzt92iUseridFormatted() {
		return Functions.padBlanks(getXzt92iUserid(), Len.XZT92I_USERID);
	}

	public void setXzt92oTkNotPrcTs(String xzt92oTkNotPrcTs) {
		writeString(Pos.XZT92O_TK_NOT_PRC_TS, xzt92oTkNotPrcTs, Len.XZT92O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT92O-TK-NOT-PRC-TS<br>*/
	public String getXzt92oTkNotPrcTs() {
		return readString(Pos.XZT92O_TK_NOT_PRC_TS, Len.XZT92O_TK_NOT_PRC_TS);
	}

	public void setXzt92oTkFrmSeqNbr(int xzt92oTkFrmSeqNbr) {
		writeInt(Pos.XZT92O_TK_FRM_SEQ_NBR, xzt92oTkFrmSeqNbr, Len.Int.XZT92O_TK_FRM_SEQ_NBR);
	}

	/**Original name: XZT92O-TK-FRM-SEQ-NBR<br>*/
	public int getXzt92oTkFrmSeqNbr() {
		return readNumDispInt(Pos.XZT92O_TK_FRM_SEQ_NBR, Len.XZT92O_TK_FRM_SEQ_NBR);
	}

	public void setXzt92oTkRecallRecSeqNbr(int xzt92oTkRecallRecSeqNbr) {
		writeInt(Pos.XZT92O_TK_RECALL_REC_SEQ_NBR, xzt92oTkRecallRecSeqNbr, Len.Int.XZT92O_TK_RECALL_REC_SEQ_NBR);
	}

	/**Original name: XZT92O-TK-RECALL-REC-SEQ-NBR<br>*/
	public int getXzt92oTkRecallRecSeqNbr() {
		return readNumDispInt(Pos.XZT92O_TK_RECALL_REC_SEQ_NBR, Len.XZT92O_TK_RECALL_REC_SEQ_NBR);
	}

	public void setXzt92oCsrActNbr(String xzt92oCsrActNbr) {
		writeString(Pos.XZT92O_CSR_ACT_NBR, xzt92oCsrActNbr, Len.XZT92O_CSR_ACT_NBR);
	}

	/**Original name: XZT92O-CSR-ACT-NBR<br>*/
	public String getXzt92oCsrActNbr() {
		return readString(Pos.XZT92O_CSR_ACT_NBR, Len.XZT92O_CSR_ACT_NBR);
	}

	public void setXzt92oTkRecSortOrdNbr(int xzt92oTkRecSortOrdNbrIdx, int xzt92oTkRecSortOrdNbr) {
		int position = Pos.xzt92oTkRecSortOrdNbr(xzt92oTkRecSortOrdNbrIdx - 1);
		writeInt(position, xzt92oTkRecSortOrdNbr, Len.Int.XZT92O_TK_REC_SORT_ORD_NBR);
	}

	/**Original name: XZT92O-TK-REC-SORT-ORD-NBR<br>*/
	public int getXzt92oTkRecSortOrdNbr(int xzt92oTkRecSortOrdNbrIdx) {
		int position = Pos.xzt92oTkRecSortOrdNbr(xzt92oTkRecSortOrdNbrIdx - 1);
		return readNumDispInt(position, Len.XZT92O_TK_REC_SORT_ORD_NBR);
	}

	public void setXzt92oTkRecSeqNbr(int xzt92oTkRecSeqNbrIdx, int xzt92oTkRecSeqNbr) {
		int position = Pos.xzt92oTkRecSeqNbr(xzt92oTkRecSeqNbrIdx - 1);
		writeInt(position, xzt92oTkRecSeqNbr, Len.Int.XZT92O_TK_REC_SEQ_NBR);
	}

	/**Original name: XZT92O-TK-REC-SEQ-NBR<br>*/
	public int getXzt92oTkRecSeqNbr(int xzt92oTkRecSeqNbrIdx) {
		int position = Pos.xzt92oTkRecSeqNbr(xzt92oTkRecSeqNbrIdx - 1);
		return readNumDispInt(position, Len.XZT92O_TK_REC_SEQ_NBR);
	}

	public void setXzt92oRecTypCd(int xzt92oRecTypCdIdx, String xzt92oRecTypCd) {
		int position = Pos.xzt92oRecTypCd(xzt92oRecTypCdIdx - 1);
		writeString(position, xzt92oRecTypCd, Len.XZT92O_REC_TYP_CD);
	}

	/**Original name: XZT92O-REC-TYP-CD<br>*/
	public String getXzt92oRecTypCd(int xzt92oRecTypCdIdx) {
		int position = Pos.xzt92oRecTypCd(xzt92oRecTypCdIdx - 1);
		return readString(position, Len.XZT92O_REC_TYP_CD);
	}

	public void setXzt92oRecTypDes(int xzt92oRecTypDesIdx, String xzt92oRecTypDes) {
		int position = Pos.xzt92oRecTypDes(xzt92oRecTypDesIdx - 1);
		writeString(position, xzt92oRecTypDes, Len.XZT92O_REC_TYP_DES);
	}

	/**Original name: XZT92O-REC-TYP-DES<br>*/
	public String getXzt92oRecTypDes(int xzt92oRecTypDesIdx) {
		int position = Pos.xzt92oRecTypDes(xzt92oRecTypDesIdx - 1);
		return readString(position, Len.XZT92O_REC_TYP_DES);
	}

	public void setXzt92oCerNbr(int xzt92oCerNbrIdx, String xzt92oCerNbr) {
		int position = Pos.xzt92oCerNbr(xzt92oCerNbrIdx - 1);
		writeString(position, xzt92oCerNbr, Len.XZT92O_CER_NBR);
	}

	/**Original name: XZT92O-CER-NBR<br>*/
	public String getXzt92oCerNbr(int xzt92oCerNbrIdx) {
		int position = Pos.xzt92oCerNbr(xzt92oCerNbrIdx - 1);
		return readString(position, Len.XZT92O_CER_NBR);
	}

	public void setXzt92oNmAdrLin1(int xzt92oNmAdrLin1Idx, String xzt92oNmAdrLin1) {
		int position = Pos.xzt92oNmAdrLin1(xzt92oNmAdrLin1Idx - 1);
		writeString(position, xzt92oNmAdrLin1, Len.XZT92O_NM_ADR_LIN1);
	}

	/**Original name: XZT92O-NM-ADR-LIN-1<br>*/
	public String getXzt92oNmAdrLin1(int xzt92oNmAdrLin1Idx) {
		int position = Pos.xzt92oNmAdrLin1(xzt92oNmAdrLin1Idx - 1);
		return readString(position, Len.XZT92O_NM_ADR_LIN1);
	}

	public void setXzt92oNmAdrLin2(int xzt92oNmAdrLin2Idx, String xzt92oNmAdrLin2) {
		int position = Pos.xzt92oNmAdrLin2(xzt92oNmAdrLin2Idx - 1);
		writeString(position, xzt92oNmAdrLin2, Len.XZT92O_NM_ADR_LIN2);
	}

	/**Original name: XZT92O-NM-ADR-LIN-2<br>*/
	public String getXzt92oNmAdrLin2(int xzt92oNmAdrLin2Idx) {
		int position = Pos.xzt92oNmAdrLin2(xzt92oNmAdrLin2Idx - 1);
		return readString(position, Len.XZT92O_NM_ADR_LIN2);
	}

	public void setXzt92oNmAdrLin3(int xzt92oNmAdrLin3Idx, String xzt92oNmAdrLin3) {
		int position = Pos.xzt92oNmAdrLin3(xzt92oNmAdrLin3Idx - 1);
		writeString(position, xzt92oNmAdrLin3, Len.XZT92O_NM_ADR_LIN3);
	}

	/**Original name: XZT92O-NM-ADR-LIN-3<br>*/
	public String getXzt92oNmAdrLin3(int xzt92oNmAdrLin3Idx) {
		int position = Pos.xzt92oNmAdrLin3(xzt92oNmAdrLin3Idx - 1);
		return readString(position, Len.XZT92O_NM_ADR_LIN3);
	}

	public void setXzt92oNmAdrLin4(int xzt92oNmAdrLin4Idx, String xzt92oNmAdrLin4) {
		int position = Pos.xzt92oNmAdrLin4(xzt92oNmAdrLin4Idx - 1);
		writeString(position, xzt92oNmAdrLin4, Len.XZT92O_NM_ADR_LIN4);
	}

	/**Original name: XZT92O-NM-ADR-LIN-4<br>*/
	public String getXzt92oNmAdrLin4(int xzt92oNmAdrLin4Idx) {
		int position = Pos.xzt92oNmAdrLin4(xzt92oNmAdrLin4Idx - 1);
		return readString(position, Len.XZT92O_NM_ADR_LIN4);
	}

	public void setXzt92oNmAdrLin5(int xzt92oNmAdrLin5Idx, String xzt92oNmAdrLin5) {
		int position = Pos.xzt92oNmAdrLin5(xzt92oNmAdrLin5Idx - 1);
		writeString(position, xzt92oNmAdrLin5, Len.XZT92O_NM_ADR_LIN5);
	}

	/**Original name: XZT92O-NM-ADR-LIN-5<br>*/
	public String getXzt92oNmAdrLin5(int xzt92oNmAdrLin5Idx) {
		int position = Pos.xzt92oNmAdrLin5(xzt92oNmAdrLin5Idx - 1);
		return readString(position, Len.XZT92O_NM_ADR_LIN5);
	}

	public void setXzt92oNmAdrLin6(int xzt92oNmAdrLin6Idx, String xzt92oNmAdrLin6) {
		int position = Pos.xzt92oNmAdrLin6(xzt92oNmAdrLin6Idx - 1);
		writeString(position, xzt92oNmAdrLin6, Len.XZT92O_NM_ADR_LIN6);
	}

	/**Original name: XZT92O-NM-ADR-LIN-6<br>*/
	public String getXzt92oNmAdrLin6(int xzt92oNmAdrLin6Idx) {
		int position = Pos.xzt92oNmAdrLin6(xzt92oNmAdrLin6Idx - 1);
		return readString(position, Len.XZT92O_NM_ADR_LIN6);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT902_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT92I_TECHNICAL_KEY = XZT902_SERVICE_INPUTS;
		public static final int XZT92I_TK_NOT_PRC_TS = XZT92I_TECHNICAL_KEY;
		public static final int XZT92I_TK_FRM_SEQ_NBR = XZT92I_TK_NOT_PRC_TS + Len.XZT92I_TK_NOT_PRC_TS;
		public static final int XZT92I_TK_RECALL_REC_SEQ_NBR = XZT92I_TK_FRM_SEQ_NBR + Len.XZT92I_TK_FRM_SEQ_NBR;
		public static final int XZT92I_CSR_ACT_NBR = XZT92I_TK_RECALL_REC_SEQ_NBR + Len.XZT92I_TK_RECALL_REC_SEQ_NBR;
		public static final int XZT92I_USERID = XZT92I_CSR_ACT_NBR + Len.XZT92I_CSR_ACT_NBR;
		public static final int XZT902_SERVICE_OUTPUTS = XZT92I_USERID + Len.XZT92I_USERID;
		public static final int XZT92O_TECHNICAL_KEY = XZT902_SERVICE_OUTPUTS;
		public static final int XZT92O_TK_NOT_PRC_TS = XZT92O_TECHNICAL_KEY;
		public static final int XZT92O_TK_FRM_SEQ_NBR = XZT92O_TK_NOT_PRC_TS + Len.XZT92O_TK_NOT_PRC_TS;
		public static final int XZT92O_TK_RECALL_REC_SEQ_NBR = XZT92O_TK_FRM_SEQ_NBR + Len.XZT92O_TK_FRM_SEQ_NBR;
		public static final int XZT92O_CSR_ACT_NBR = XZT92O_TK_RECALL_REC_SEQ_NBR + Len.XZT92O_TK_RECALL_REC_SEQ_NBR;
		public static final int XZT92O_REC_LIST_TBL = XZT92O_CSR_ACT_NBR + Len.XZT92O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt92oRecList(int idx) {
			return XZT92O_REC_LIST_TBL + idx * Len.XZT92O_REC_LIST;
		}

		public static int xzt92oRecTechnicalKey(int idx) {
			return xzt92oRecList(idx);
		}

		public static int xzt92oTkRecSortOrdNbr(int idx) {
			return xzt92oRecTechnicalKey(idx);
		}

		public static int xzt92oTkRecSeqNbr(int idx) {
			return xzt92oTkRecSortOrdNbr(idx) + Len.XZT92O_TK_REC_SORT_ORD_NBR;
		}

		public static int xzt92oRecTyp(int idx) {
			return xzt92oTkRecSeqNbr(idx) + Len.XZT92O_TK_REC_SEQ_NBR;
		}

		public static int xzt92oRecTypCd(int idx) {
			return xzt92oRecTyp(idx);
		}

		public static int xzt92oRecTypDes(int idx) {
			return xzt92oRecTypCd(idx) + Len.XZT92O_REC_TYP_CD;
		}

		public static int xzt92oCerNbr(int idx) {
			return xzt92oRecTypDes(idx) + Len.XZT92O_REC_TYP_DES;
		}

		public static int xzt92oNmAdrLin1(int idx) {
			return xzt92oCerNbr(idx) + Len.XZT92O_CER_NBR;
		}

		public static int xzt92oNmAdrLin2(int idx) {
			return xzt92oNmAdrLin1(idx) + Len.XZT92O_NM_ADR_LIN1;
		}

		public static int xzt92oNmAdrLin3(int idx) {
			return xzt92oNmAdrLin2(idx) + Len.XZT92O_NM_ADR_LIN2;
		}

		public static int xzt92oNmAdrLin4(int idx) {
			return xzt92oNmAdrLin3(idx) + Len.XZT92O_NM_ADR_LIN3;
		}

		public static int xzt92oNmAdrLin5(int idx) {
			return xzt92oNmAdrLin4(idx) + Len.XZT92O_NM_ADR_LIN4;
		}

		public static int xzt92oNmAdrLin6(int idx) {
			return xzt92oNmAdrLin5(idx) + Len.XZT92O_NM_ADR_LIN5;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT92I_TK_NOT_PRC_TS = 26;
		public static final int XZT92I_TK_FRM_SEQ_NBR = 5;
		public static final int XZT92I_TK_RECALL_REC_SEQ_NBR = 5;
		public static final int XZT92I_CSR_ACT_NBR = 9;
		public static final int XZT92I_USERID = 8;
		public static final int XZT92O_TK_NOT_PRC_TS = 26;
		public static final int XZT92O_TK_FRM_SEQ_NBR = 5;
		public static final int XZT92O_TK_RECALL_REC_SEQ_NBR = 5;
		public static final int XZT92O_CSR_ACT_NBR = 9;
		public static final int XZT92O_TK_REC_SORT_ORD_NBR = 5;
		public static final int XZT92O_TK_REC_SEQ_NBR = 5;
		public static final int XZT92O_REC_TECHNICAL_KEY = XZT92O_TK_REC_SORT_ORD_NBR + XZT92O_TK_REC_SEQ_NBR;
		public static final int XZT92O_REC_TYP_CD = 5;
		public static final int XZT92O_REC_TYP_DES = 30;
		public static final int XZT92O_REC_TYP = XZT92O_REC_TYP_CD + XZT92O_REC_TYP_DES;
		public static final int XZT92O_CER_NBR = 25;
		public static final int XZT92O_NM_ADR_LIN1 = 45;
		public static final int XZT92O_NM_ADR_LIN2 = 45;
		public static final int XZT92O_NM_ADR_LIN3 = 45;
		public static final int XZT92O_NM_ADR_LIN4 = 45;
		public static final int XZT92O_NM_ADR_LIN5 = 45;
		public static final int XZT92O_NM_ADR_LIN6 = 45;
		public static final int XZT92O_REC_LIST = XZT92O_REC_TECHNICAL_KEY + XZT92O_REC_TYP + XZT92O_CER_NBR + XZT92O_NM_ADR_LIN1 + XZT92O_NM_ADR_LIN2
				+ XZT92O_NM_ADR_LIN3 + XZT92O_NM_ADR_LIN4 + XZT92O_NM_ADR_LIN5 + XZT92O_NM_ADR_LIN6;
		public static final int XZT92I_TECHNICAL_KEY = XZT92I_TK_NOT_PRC_TS + XZT92I_TK_FRM_SEQ_NBR + XZT92I_TK_RECALL_REC_SEQ_NBR;
		public static final int XZT902_SERVICE_INPUTS = XZT92I_TECHNICAL_KEY + XZT92I_CSR_ACT_NBR + XZT92I_USERID;
		public static final int XZT92O_TECHNICAL_KEY = XZT92O_TK_NOT_PRC_TS + XZT92O_TK_FRM_SEQ_NBR + XZT92O_TK_RECALL_REC_SEQ_NBR;
		public static final int XZT92O_REC_LIST_TBL = LServiceContractAreaXz0x9020.XZT92O_REC_LIST_MAXOCCURS * XZT92O_REC_LIST;
		public static final int XZT902_SERVICE_OUTPUTS = XZT92O_TECHNICAL_KEY + XZT92O_CSR_ACT_NBR + XZT92O_REC_LIST_TBL;
		public static final int L_SERVICE_CONTRACT_AREA = XZT902_SERVICE_INPUTS + XZT902_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT92I_TK_FRM_SEQ_NBR = 5;
			public static final int XZT92I_TK_RECALL_REC_SEQ_NBR = 5;
			public static final int XZT92O_TK_FRM_SEQ_NBR = 5;
			public static final int XZT92O_TK_RECALL_REC_SEQ_NBR = 5;
			public static final int XZT92O_TK_REC_SORT_ORD_NBR = 5;
			public static final int XZT92O_TK_REC_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
