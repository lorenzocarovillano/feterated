/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0F0005<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0f0005 {

	//==== PROPERTIES ====
	//Original name: WS-BUS-OBJ-NAME
	private String busObjName = "ACT_NOT_WRD";

	//==== METHODS ====
	public String getBusObjName() {
		return this.busObjName;
	}
}
