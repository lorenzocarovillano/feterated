/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UBOC-UOW-LOCK-STRATEGY-CD<br>
 * Variable: UBOC-UOW-LOCK-STRATEGY-CD from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocUowLockStrategyCd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char LOCK_OPTIMISTIC = 'O';
	public static final char LOCK_PESSIMISTIC = 'P';
	public static final char LOCK_MIXED = 'M';
	public static final char LOCK_GROUP_PESSI = 'G';
	public static final char NO_LOCK_NEEDED = 'N';

	//==== METHODS ====
	public void setUbocUowLockStrategyCd(char ubocUowLockStrategyCd) {
		this.value = ubocUowLockStrategyCd;
	}

	public char getUbocUowLockStrategyCd() {
		return this.value;
	}

	public void setUbocUowLockPessimistic() {
		value = LOCK_PESSIMISTIC;
	}

	public boolean isNoLockAction() {
		return value == NO_LOCK_NEEDED;
	}

	public void setUbocUowNoLockNeeded() {
		value = NO_LOCK_NEEDED;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_UOW_LOCK_STRATEGY_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
