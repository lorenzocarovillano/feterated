/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: ESTO-DETAIL-BUFFER<br>
 * Variable: ESTO-DETAIL-BUFFER from program HALOUIDG<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class EstoDetailBuffer extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final String EFAL_S3_SAVARCH = "SAVARCH";
	public static final String EFAL_S3_SAVANNAH = "SAVANNAH";
	public static final String EFAL_S3_HEAL = "HEAL";
	public static final String EFAL_MAINFRAME = "MAINFRAME";
	public static final String EFAL_SERVER = "SERVER";
	public static final char EFAL_LOGGABLE_WARNING = 'W';
	public static final char EFAL_BUS_LOGIC_FAILURE = 'B';
	public static final char EFAL_SYSTEM_ERROR = 'S';
	public static final String EFAL_NO_FAILED_ACTION = "";
	public static final String EFAL_DB2_FAILED = "DB2";
	public static final String EFAL_CICS_FAILED = "CICS";
	public static final String EFAL_IAP_FAILED = "IAP";
	public static final String EFAL_LOCKING_FAILED = "LOCKING";
	public static final String EFAL_COMMAREA_FAILED = "COMMAREA";
	public static final String EFAL_SECURITY_FAILED = "SECURITY";
	public static final String EFAL_DATA_PRIVACY_FAILED = "DATAPRIV";
	public static final String EFAL_AUDIT_PROCESS_FAILED = "AUDIT";
	public static final String EFAL_BUS_PROCESS_FAILED = "BUSPROC";
	public static final String EFAL_ABEND_ENCOUNTERED = "ABEND";
	public static final String EFAL_TRANSFER_FAILED = "TRANSFER";
	public static final String ETRA_DB2_INSERT = "INSERT";
	public static final String ETRA_DB2_UPDATE = "UPDATE";
	public static final String ETRA_DB2_DELETE = "DELETE";
	public static final String ETRA_DB2_SELECT = "SELECT";
	public static final String ETRA_DB2_OPEN_CSR = "OPEN CSR";
	public static final String ETRA_DB2_FETCH_CSR = "FETCH CSR";
	public static final String ETRA_DB2_CLOSE_CSR = "CLOSE CSR";
	public static final String ETRA_DB2_POSITION_CSR = "POSITION CSR";
	public static final String ETRA_DB2_SET_CURRENT_TS = "SET CURRENT TS";
	public static final String ETRA_DB2_SET_ENVIRONMENT = "SET ENVIRONMENT";
	public static final String ETRA_CICS_READ_TSQ = "READ TSQ";
	public static final String ETRA_CICS_UPDATE_TSQ = "UPDATE TSQ";
	public static final String ETRA_CICS_WRITE_TSQ = "WRITE TSQ";
	public static final String ETRA_CICS_DELETE_TSQ = "DELETE TSQ";
	public static final String ETRA_CICS_READ_VSAM = "READ VSAM";
	public static final String ETRA_CICS_UPDATE_VSAM = "UPDATE VSAM";
	public static final String ETRA_CICS_WRITE_VSAM = "WRITE VSAM";
	public static final String ETRA_CICS_DELETE_VSAM = "DELETE VSAM";
	public static final String ETRA_CICS_STARTBR_UMT = "STARTBR UMT";
	public static final String ETRA_CICS_READNXT_UMT = "READNXT UMT";
	public static final String ETRA_CICS_ENDBR_UMT = "ENDBR UMT";
	public static final String ETRA_CICS_READ_UMT = "READ UMT";
	public static final String ETRA_CICS_UPDATE_UMT = "UPDATE UMT";
	public static final String ETRA_CICS_WRITE_UMT = "WRITE UMT";
	public static final String ETRA_CICS_REWRITE_UMT = "REWRITE UMT";
	public static final String ETRA_CICS_DELETE_UMT = "DELETE UMT";
	public static final String ETRA_CICS_LINK = "LINK";
	public static final String ETRA_CICS_START = "START";
	public static final String ETRA_CICS_XCTL = "XCTL";
	public static final String ETRA_CICS_FUNCTION = "FUNCTION";
	public static final String ETRA_CICS_RETRIEVE = "CICS RETRIEVE";
	public static final String ETRA_CICS_FORMATTIME = "CICS FORMATTIME";
	public static final String ETRA_CICS_GETMAIN = "CICS GETMAIN";
	public static final String ETRA_CICS_FREEMAIN = "CICS FREEMAIN";
	public static final String ETRA_CICS_WEB_EXTRACT = "CICS WEB EXTRACT";
	public static final String ETRA_CICS_WEB_READ = "CICS WEB READ";
	public static final String ETRA_CICS_WEB_SEND = "CICS WEB SEND";
	public static final String ETRA_CICS_WEB_RECEIVE = "CICS WEB RECEIVE";
	public static final String ETRA_CICS_WEB_WRITE = "CICS WEB WRITE";
	public static final String ETRA_CICS_DOC_CREATE = "CICS DOCUMENT CREATE";
	public static final String ETRA_COBOL_CALL = "COBOL CALL";
	public static final String COMA_FIELD_NOT_VALUED = "FIELD NOT VALUED";
	public static final String COMA_MSG_ID_BLANK = "MSG ID BLANK";
	public static final String COMA_UOW_NAME_NOT_FILLED = "UNIT OF WORK NOT FILLED";
	public static final String COMA_UOW_MESS_BUFF_NOT_FILLED = "MESSAGE BUFFER NOT FILLED";
	public static final String COMA_REC_SEQ_INVALID = "REC SEQ NOT NUMERIC";
	public static final String COMA_TOTAL_MSG_LENGTH_INVALID = "TOTAL MSG LENGTH NOT NUMERIC";
	public static final String COMA_UOW_NUM_CHKS_INVALID = "UOW NUM CHUNKS NOT NUMERIC";
	public static final String COMA_UOW_BUFFER_LEN_INVALID = "UOW BUFFER LENGTH NOT NUMERIC";
	public static final String COMA_UOW_NAME_BLANK = "UOW NAME BLANK";
	public static final String COMA_USERID_BLANK = "USERID BLANK";
	public static final String COMA_AUTH_USER_CLIENTID_BLANK = "AUTH USER CLIENTID BLANK";
	public static final String COMA_UOW_REQ_MSG_STORE_BLANK = "UOW REQ MSG STORE BLANK";
	public static final String COMA_UOW_REQ_SWIT_STORE_BLANK = "UOW REQ SWITCHES STORE BLANK";
	public static final String COMA_UOW_RESP_HDR_STORE_BLANK = "UOW RESP HEADER STORE BLANK";
	public static final String COMA_UOW_RESP_DATA_STORE_BLANK = "UOW RESP DATA STORE BLANK";
	public static final String COMA_UOW_RESP_WARN_STORE_BLANK = "UOW RESP WARNINGS STORE BLANK";
	public static final String COMA_UOW_RESP_NLBE_STORE_BLANK = "UOW RSP NLBE STORE";
	public static final String COMA_UOW_KEY_REPL_STORE_BLANK = "UOW KEY REPLACE STORE BLANK";
	public static final String COMA_DEL_TIME_BLANK = "LOKS DEL TIME BLANK";
	public static final String COMA_LOCK_TYPE_BLANK = "LOCK TYPE BLANK";
	public static final String COMA_AUDT_BUS_OBJ_NM_BLANK = "AUDIT BUS OBJ NAME BLANK";
	public static final String COMA_SESSION_ID_BLANK = "SESSION IS BLANK";
	public static final String COMA_BAD_SYNCPOINT_FLAG = "INVALID SYNCPOINT FLAG";
	public static final String COMA_BAD_REQ_MSG_PROC_FLAG = "INVALID REQ MSG PROC FLAG";
	public static final String COMA_BAD_RESP_MSG_PROC_FLAG = "INVALID RESP MSG PROC FLAG";
	public static final String BUSP_STORE_NAME_BLANK = "STORE NME BLANK";
	public static final String BUSP_UOW_MODULE_BLANK = "UOW MOD BLANK";
	public static final String BUSP_UOW_TRANSACT_NOT_FOUND = "UOW TRANSACTION ROW NOT FOUND";
	public static final String BUSP_LOCK_ACTION_BLANK = "LOCK ACTION BLANK";
	public static final String BUSP_CN_LVL_SEC_IND_BLANK = "CN LVL SEC IND BLANK";
	public static final String BUSP_CONT_LVL_SEC_MODULE_BLANK = "CNT LVL SEC MOD BLANK";
	public static final String BUSP_INVALID_SEC_MODULE = "SECURITY MOD INVALID";
	public static final String BUSP_DATA_PRIV_IND_BLANK = "DATA PRIVACY IND BLANK";
	public static final String BUSP_NO_MESS_ROWS_INV = "NO MESSAGE ROWS";
	public static final String BUSP_HDR_EXIST_CANT_TELL = "HDR EXIST CANT TELL";
	public static final String BUSP_INV_ACTION_CODE = "INV ACT CODE";
	public static final String BUSP_INVALID_SEC_ACTION = "INVALID SEC ACTION";
	public static final String BUSP_PARENT_MISSING = "PARENT MISSING";
	public static final String BUSP_KEY_CHANGED = "KEY CHANGED";
	public static final String BUSP_EXI_ROW_NOT_FOUND = "EXI ROW NOT FND";
	public static final String BUSP_INVALID_STORE_TYPE = "INV STO TYPE";
	public static final String BUSP_INV_PARAM_LEN = "INV PARA LEN";
	public static final String BUSP_KEY_AND_LABEL_SUPP = "KEY & LAB SUPP";
	public static final String BUSP_INVALID_FUNCTION = "INV FUNC";
	public static final String BUSP_UOW_NOT_IN_SUPP_TAB = "UOW NOT ON SUP TAB";
	public static final String BUSP_STORE_TYPE_INV = "SPEC. STORE TYPE INV";
	public static final String BUSP_UOW_DATA_BUFF_BLANK = "UOW DATA BUFF BLANK";
	public static final String BUSP_INVALID_FILTER = "UOW FILTER INVALID";
	public static final String BUSP_INVALID_FETCH_CURSOR = "INVALID FETCH CURSOR";
	public static final String BUSP_UOW_REQ_UMT_EMPTY = "UOW REQ UMT EMPTY";
	public static final String BUSP_UOW_HDR_DATA_MISS_MATCH = "UOW HDR DATA MMATCH";
	public static final String BUSP_INVALID_UMT_REC_CNT = "INVALID UMT REC CNT";
	public static final String BUSP_DUPL_KEY_GENERATED = "DUPL. KEY GENERATED";
	public static final String BUSP_REQUEST_MSG_MISSING = "REQ MSG MISSING";
	public static final String BUSP_INVALID_SUB_BUS_OBJ = "INV SUB BUS OBJ";
	public static final String BUSP_INVALID_NULL_IND = "INV NULL IND";
	public static final String BUSP_INVALID_SIGN = "INVALID SIGN";
	public static final String BUSP_INVALID_COLUMN_IND = "INV COL IND";
	public static final String BUSP_REQUIRED_FIELD_BLANK = "REQUIRED FIELD BLANK";
	public static final String BUSP_INV_KEY_FIELD_CONTENTS = "INV KEY FIE CONTS";
	public static final String BUSP_INVALID_SUPPLIED_VALUE = "INV SUPPLIED VALUE";
	public static final String BUSP_LIMIT_EXCEEDED = "LIMIT EXCEEDED";
	public static final String BUSP_REQD_DATA_NOT_FOUND = "REQUIRED DATA NOT FOUND";
	public static final String BUSP_POL_ISSUE_FAILED = "POLICY ISSUE FAILED";
	public static final String BUSP_TRIGGER_WRITE_FAILED = "TRIGGER WRITE FAILED";
	public static final String BUSP_INV_LINE_OF_BUS = "INVALID LINE OF BUSINESS";
	public static final String BUSP_EFF_DATE_OUT_OF_RANGE = "EFFECTIVE DATE OUT OF RANGE";
	public static final String BUSP_EXTRNL_VAL_NOTFND = "EXTERNAL VALUE NOT FOUND";
	public static final String BUSP_LEGACY_PROCESS_FAILED = "FAILURE IN LEGACY PROCESS";
	public static final String BUSP_UBOC_LINKAGE_CORRUPTED = "UBOC LINKAGE CORRUPTED";
	public static final String BUSP_INVALID_HTTP_METHOD = "INVALID HTTP METHOD";
	public static final String BUSP_INVALID_CONTENT_LEN = "INVALID HTTP CONTENT LENGTH";
	public static final String SECU_SEC_SYS_ACCESS_DENIED = "SEC SYS ACCESS DENIED";
	public static final String SECU_SEC_UOW_ACCESS_DENIED = "SEC UOW ACCESS DENIED";
	public static final String SECU_SEC_INVALID_RETURN = "SEC INVALID RETURN";
	public static final String DATP_UNKNOWN_ASSOC_TYPE = "DATA PRIV UNKNOWN ASSOC TYPE";
	public static final String DATP_UNKNOWN_CONTEXT = "DATA PRIV UNKNOWN CONTEXT";
	public static final String IAP_MAX_SCH_ATTEMPTS_EXCEEDED = "MAX DFR ATTEMPTS";
	public static final String IAP_DATE_RETURN_ERROR = "DATE ROUTINE ERROR";
	public static final String IAP_FAILED_DFR_ACY_SCHEDULE = "FAILED TO SCHED DFR - DEL UMT";
	public static final String IAP_XPIOCHK_CALL_ERROR = "FAILURE IN CALL TO XPIOCHK";
	public static final String IAP_TSQ_NAME_GENERATION_ERROR = "FAILURE IN TSQNAMES CALL";
	public static final String LOK_BCMOCHK_CALL_ERROR = "FAILURE IN CALL TO BCMOCHK";
	public static final String LOK_INVALID_LEGACY_RET = "INVALID LEGACY LOCK RETURN";
	public static final String ETRA_ABEND_ENCOUNTERED = "ABEND ENCOUNTERED";
	public static final String FRAM_OPERATION_NAME_BLANK = "OPERATION NAME IS BLANK";

	//==== CONSTRUCTORS ====
	public EstoDetailBuffer() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.ESTO_DETAIL_BUFFER;
	}

	public void setEstoDetailBuffer(String estoDetailBuffer) {
		writeString(Pos.ESTO_DETAIL_BUFFER, estoDetailBuffer, Len.ESTO_DETAIL_BUFFER);
	}

	public void setEstoDetailBufferFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.ESTO_DETAIL_BUFFER, Pos.ESTO_DETAIL_BUFFER);
	}

	/**Original name: ESTO-DETAIL-BUFFER<br>*/
	public String getEstoDetailBuffer() {
		return readString(Pos.ESTO_DETAIL_BUFFER, Len.ESTO_DETAIL_BUFFER);
	}

	public byte[] getEstoDetailBufferAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.ESTO_DETAIL_BUFFER, Pos.ESTO_DETAIL_BUFFER);
		return buffer;
	}

	public void setEfalFailLvlGuid(String efalFailLvlGuid) {
		writeString(Pos.EFAL_FAIL_LVL_GUID, efalFailLvlGuid, Len.EFAL_FAIL_LVL_GUID);
	}

	/**Original name: EFAL-FAIL-LVL-GUID<br>*/
	public String getEfalFailLvlGuid() {
		return readString(Pos.EFAL_FAIL_LVL_GUID, Len.EFAL_FAIL_LVL_GUID);
	}

	public void setEfalFailLvlErrTimestamp(String efalFailLvlErrTimestamp) {
		writeString(Pos.EFAL_FAIL_LVL_ERR_TIMESTAMP, efalFailLvlErrTimestamp, Len.EFAL_FAIL_LVL_ERR_TIMESTAMP);
	}

	/**Original name: EFAL-FAIL-LVL-ERR-TIMESTAMP<br>*/
	public String getEfalFailLvlErrTimestamp() {
		return readString(Pos.EFAL_FAIL_LVL_ERR_TIMESTAMP, Len.EFAL_FAIL_LVL_ERR_TIMESTAMP);
	}

	public void setEfalFailedApplication(String efalFailedApplication) {
		writeString(Pos.EFAL_FAILED_APPLICATION, efalFailedApplication, Len.EFAL_FAILED_APPLICATION);
	}

	/**Original name: EFAL-FAILED-APPLICATION<br>*/
	public String getEfalFailedApplication() {
		return readString(Pos.EFAL_FAILED_APPLICATION, Len.EFAL_FAILED_APPLICATION);
	}

	public String getEfalFailedApplicationFormatted() {
		return Functions.padBlanks(getEfalFailedApplication(), Len.EFAL_FAILED_APPLICATION);
	}

	public void setEfalS3Savarch() {
		setEfalFailedApplication(EFAL_S3_SAVARCH);
	}

	public void setEfalS3Savannah() {
		setEfalFailedApplication(EFAL_S3_SAVANNAH);
	}

	public void setEfalFailedPlatform(String efalFailedPlatform) {
		writeString(Pos.EFAL_FAILED_PLATFORM, efalFailedPlatform, Len.EFAL_FAILED_PLATFORM);
	}

	/**Original name: EFAL-FAILED-PLATFORM<br>*/
	public String getEfalFailedPlatform() {
		return readString(Pos.EFAL_FAILED_PLATFORM, Len.EFAL_FAILED_PLATFORM);
	}

	public String getEfalFailedPlatformFormatted() {
		return Functions.padBlanks(getEfalFailedPlatform(), Len.EFAL_FAILED_PLATFORM);
	}

	public void setEfalMainframe() {
		setEfalFailedPlatform(EFAL_MAINFRAME);
	}

	public void setEfalFailureType(char efalFailureType) {
		writeChar(Pos.EFAL_FAILURE_TYPE, efalFailureType);
	}

	/**Original name: EFAL-FAILURE-TYPE<br>*/
	public char getEfalFailureType() {
		return readChar(Pos.EFAL_FAILURE_TYPE);
	}

	public void setEfalLoggableWarning() {
		setEfalFailureType(EFAL_LOGGABLE_WARNING);
	}

	public void setEfalBusLogicFailure() {
		setEfalFailureType(EFAL_BUS_LOGIC_FAILURE);
	}

	public void setEfalSystemError() {
		setEfalFailureType(EFAL_SYSTEM_ERROR);
	}

	public void setEfalFailedActionType(String efalFailedActionType) {
		writeString(Pos.EFAL_FAILED_ACTION_TYPE, efalFailedActionType, Len.EFAL_FAILED_ACTION_TYPE);
	}

	/**Original name: EFAL-FAILED-ACTION-TYPE<br>*/
	public String getEfalFailedActionType() {
		return readString(Pos.EFAL_FAILED_ACTION_TYPE, Len.EFAL_FAILED_ACTION_TYPE);
	}

	public String getEfalFailedActionTypeFormatted() {
		return Functions.padBlanks(getEfalFailedActionType(), Len.EFAL_FAILED_ACTION_TYPE);
	}

	public boolean isEfalDb2Failed() {
		return getEfalFailedActionType().equals(EFAL_DB2_FAILED);
	}

	public void setEfalDb2Failed() {
		setEfalFailedActionType(EFAL_DB2_FAILED);
	}

	public boolean isEfalCicsFailed() {
		return getEfalFailedActionType().equals(EFAL_CICS_FAILED);
	}

	public void setEfalCicsFailed() {
		setEfalFailedActionType(EFAL_CICS_FAILED);
	}

	public void setEfalIapFailed() {
		setEfalFailedActionType(EFAL_IAP_FAILED);
	}

	public void setEfalLockingFailed() {
		setEfalFailedActionType(EFAL_LOCKING_FAILED);
	}

	public void setEfalCommareaFailed() {
		setEfalFailedActionType(EFAL_COMMAREA_FAILED);
	}

	public void setEfalSecurityFailed() {
		setEfalFailedActionType(EFAL_SECURITY_FAILED);
	}

	public void setEfalDataPrivacyFailed() {
		setEfalFailedActionType(EFAL_DATA_PRIVACY_FAILED);
	}

	public void setEfalBusProcessFailed() {
		setEfalFailedActionType(EFAL_BUS_PROCESS_FAILED);
	}

	public void setEfalTransferFailed() {
		setEfalFailedActionType(EFAL_TRANSFER_FAILED);
	}

	public void setEfalFailedModule(String efalFailedModule) {
		writeString(Pos.EFAL_FAILED_MODULE, efalFailedModule, Len.EFAL_FAILED_MODULE);
	}

	/**Original name: EFAL-FAILED-MODULE<br>*/
	public String getEfalFailedModule() {
		return readString(Pos.EFAL_FAILED_MODULE, Len.EFAL_FAILED_MODULE);
	}

	public void setEfalErrParagraph(String efalErrParagraph) {
		writeString(Pos.EFAL_ERR_PARAGRAPH, efalErrParagraph, Len.EFAL_ERR_PARAGRAPH);
	}

	/**Original name: EFAL-ERR-PARAGRAPH<br>*/
	public String getEfalErrParagraph() {
		return readString(Pos.EFAL_ERR_PARAGRAPH, Len.EFAL_ERR_PARAGRAPH);
	}

	public void setEfalErrObjectName(String efalErrObjectName) {
		writeString(Pos.EFAL_ERR_OBJECT_NAME, efalErrObjectName, Len.EFAL_ERR_OBJECT_NAME);
	}

	/**Original name: EFAL-ERR-OBJECT-NAME<br>*/
	public String getEfalErrObjectName() {
		return readString(Pos.EFAL_ERR_OBJECT_NAME, Len.EFAL_ERR_OBJECT_NAME);
	}

	public void setEfalDb2ErrSqlcodeSign(char efalDb2ErrSqlcodeSign) {
		writeChar(Pos.EFAL_DB2_ERR_SQLCODE_SIGN, efalDb2ErrSqlcodeSign);
	}

	public void setEfalDb2ErrSqlcodeSignFormatted(String efalDb2ErrSqlcodeSign) {
		setEfalDb2ErrSqlcodeSign(Functions.charAt(efalDb2ErrSqlcodeSign, Types.CHAR_SIZE));
	}

	/**Original name: EFAL-DB2-ERR-SQLCODE-SIGN<br>*/
	public char getEfalDb2ErrSqlcodeSign() {
		return readChar(Pos.EFAL_DB2_ERR_SQLCODE_SIGN);
	}

	public void setEfalDb2ErrSqlcode(long efalDb2ErrSqlcode) {
		writeLong(Pos.EFAL_DB2_ERR_SQLCODE, efalDb2ErrSqlcode, Len.Int.EFAL_DB2_ERR_SQLCODE, SignType.NO_SIGN);
	}

	public void setEfalDb2ErrSqlcodeFormatted(String efalDb2ErrSqlcode) {
		writeString(Pos.EFAL_DB2_ERR_SQLCODE, Trunc.toUnsignedNumeric(efalDb2ErrSqlcode, Len.EFAL_DB2_ERR_SQLCODE), Len.EFAL_DB2_ERR_SQLCODE);
	}

	/**Original name: EFAL-DB2-ERR-SQLCODE<br>*/
	public long getEfalDb2ErrSqlcode() {
		return readNumDispUnsignedLong(Pos.EFAL_DB2_ERR_SQLCODE, Len.EFAL_DB2_ERR_SQLCODE);
	}

	public String getEfalDb2ErrSqlcodeFormatted() {
		return readFixedString(Pos.EFAL_DB2_ERR_SQLCODE, Len.EFAL_DB2_ERR_SQLCODE);
	}

	public void setEfalCicsErrRespSign(char efalCicsErrRespSign) {
		writeChar(Pos.EFAL_CICS_ERR_RESP_SIGN, efalCicsErrRespSign);
	}

	public void setEfalCicsErrRespSignFormatted(String efalCicsErrRespSign) {
		setEfalCicsErrRespSign(Functions.charAt(efalCicsErrRespSign, Types.CHAR_SIZE));
	}

	/**Original name: EFAL-CICS-ERR-RESP-SIGN<br>*/
	public char getEfalCicsErrRespSign() {
		return readChar(Pos.EFAL_CICS_ERR_RESP_SIGN);
	}

	public void setEfalCicsErrResp(long efalCicsErrResp) {
		writeLong(Pos.EFAL_CICS_ERR_RESP, efalCicsErrResp, Len.Int.EFAL_CICS_ERR_RESP, SignType.NO_SIGN);
	}

	public void setEfalCicsErrRespFormatted(String efalCicsErrResp) {
		writeString(Pos.EFAL_CICS_ERR_RESP, Trunc.toUnsignedNumeric(efalCicsErrResp, Len.EFAL_CICS_ERR_RESP), Len.EFAL_CICS_ERR_RESP);
	}

	/**Original name: EFAL-CICS-ERR-RESP<br>*/
	public long getEfalCicsErrResp() {
		return readNumDispUnsignedLong(Pos.EFAL_CICS_ERR_RESP, Len.EFAL_CICS_ERR_RESP);
	}

	public String getEfalCicsErrRespFormatted() {
		return readFixedString(Pos.EFAL_CICS_ERR_RESP, Len.EFAL_CICS_ERR_RESP);
	}

	public void setEfalErrComment(String efalErrComment) {
		writeString(Pos.EFAL_ERR_COMMENT, efalErrComment, Len.EFAL_ERR_COMMENT);
	}

	/**Original name: EFAL-ERR-COMMENT<br>*/
	public String getEfalErrComment() {
		return readString(Pos.EFAL_ERR_COMMENT, Len.EFAL_ERR_COMMENT);
	}

	public String getEfalErrCommentFormatted() {
		return Functions.padBlanks(getEfalErrComment(), Len.EFAL_ERR_COMMENT);
	}

	public void setEfalUnitOfWork(String efalUnitOfWork) {
		writeString(Pos.EFAL_UNIT_OF_WORK, efalUnitOfWork, Len.EFAL_UNIT_OF_WORK);
	}

	/**Original name: EFAL-UNIT-OF-WORK<br>*/
	public String getEfalUnitOfWork() {
		return readString(Pos.EFAL_UNIT_OF_WORK, Len.EFAL_UNIT_OF_WORK);
	}

	public String getEfalUnitOfWorkFormatted() {
		return Functions.padBlanks(getEfalUnitOfWork(), Len.EFAL_UNIT_OF_WORK);
	}

	public void setEfalFailedLocationId(String efalFailedLocationId) {
		writeString(Pos.EFAL_FAILED_LOCATION_ID, efalFailedLocationId, Len.EFAL_FAILED_LOCATION_ID);
	}

	/**Original name: EFAL-FAILED-LOCATION-ID<br>*/
	public String getEfalFailedLocationId() {
		return readString(Pos.EFAL_FAILED_LOCATION_ID, Len.EFAL_FAILED_LOCATION_ID);
	}

	public String getEfalFailedLocationIdFormatted() {
		return Functions.padBlanks(getEfalFailedLocationId(), Len.EFAL_FAILED_LOCATION_ID);
	}

	public void setEfalDb2ErrSqlerrmc(String efalDb2ErrSqlerrmc) {
		writeString(Pos.EFAL_DB2_ERR_SQLERRMC, efalDb2ErrSqlerrmc, Len.EFAL_DB2_ERR_SQLERRMC);
	}

	/**Original name: EFAL-DB2-ERR-SQLERRMC<br>*/
	public String getEfalDb2ErrSqlerrmc() {
		return readString(Pos.EFAL_DB2_ERR_SQLERRMC, Len.EFAL_DB2_ERR_SQLERRMC);
	}

	public String getEfalDb2ErrSqlerrmcFormatted() {
		return Functions.padBlanks(getEfalDb2ErrSqlerrmc(), Len.EFAL_DB2_ERR_SQLERRMC);
	}

	public void setEfalCicsErrResp2Sign(char efalCicsErrResp2Sign) {
		writeChar(Pos.EFAL_CICS_ERR_RESP2_SIGN, efalCicsErrResp2Sign);
	}

	public void setEfalCicsErrResp2SignFormatted(String efalCicsErrResp2Sign) {
		setEfalCicsErrResp2Sign(Functions.charAt(efalCicsErrResp2Sign, Types.CHAR_SIZE));
	}

	/**Original name: EFAL-CICS-ERR-RESP2-SIGN<br>*/
	public char getEfalCicsErrResp2Sign() {
		return readChar(Pos.EFAL_CICS_ERR_RESP2_SIGN);
	}

	public void setEfalCicsErrResp2(long efalCicsErrResp2) {
		writeLong(Pos.EFAL_CICS_ERR_RESP2, efalCicsErrResp2, Len.Int.EFAL_CICS_ERR_RESP2, SignType.NO_SIGN);
	}

	public void setEfalCicsErrResp2Formatted(String efalCicsErrResp2) {
		writeString(Pos.EFAL_CICS_ERR_RESP2, Trunc.toUnsignedNumeric(efalCicsErrResp2, Len.EFAL_CICS_ERR_RESP2), Len.EFAL_CICS_ERR_RESP2);
	}

	/**Original name: EFAL-CICS-ERR-RESP2<br>*/
	public long getEfalCicsErrResp2() {
		return readNumDispUnsignedLong(Pos.EFAL_CICS_ERR_RESP2, Len.EFAL_CICS_ERR_RESP2);
	}

	public String getEfalCicsErrResp2Formatted() {
		return readFixedString(Pos.EFAL_CICS_ERR_RESP2, Len.EFAL_CICS_ERR_RESP2);
	}

	public void setEfalLogonUserid(String efalLogonUserid) {
		writeString(Pos.EFAL_LOGON_USERID, efalLogonUserid, Len.EFAL_LOGON_USERID);
	}

	/**Original name: EFAL-LOGON-USERID<br>*/
	public String getEfalLogonUserid() {
		return readString(Pos.EFAL_LOGON_USERID, Len.EFAL_LOGON_USERID);
	}

	public void setEfalSecSysIdSign(char efalSecSysIdSign) {
		writeChar(Pos.EFAL_SEC_SYS_ID_SIGN, efalSecSysIdSign);
	}

	public void setEfalSecSysIdSignFormatted(String efalSecSysIdSign) {
		setEfalSecSysIdSign(Functions.charAt(efalSecSysIdSign, Types.CHAR_SIZE));
	}

	/**Original name: EFAL-SEC-SYS-ID-SIGN<br>*/
	public char getEfalSecSysIdSign() {
		return readChar(Pos.EFAL_SEC_SYS_ID_SIGN);
	}

	public void setEfalSecSysId(long efalSecSysId) {
		writeLong(Pos.EFAL_SEC_SYS_ID, efalSecSysId, Len.Int.EFAL_SEC_SYS_ID, SignType.NO_SIGN);
	}

	public void setEfalSecSysIdFormatted(String efalSecSysId) {
		writeString(Pos.EFAL_SEC_SYS_ID, Trunc.toUnsignedNumeric(efalSecSysId, Len.EFAL_SEC_SYS_ID), Len.EFAL_SEC_SYS_ID);
	}

	/**Original name: EFAL-SEC-SYS-ID<br>*/
	public long getEfalSecSysId() {
		return readNumDispUnsignedLong(Pos.EFAL_SEC_SYS_ID, Len.EFAL_SEC_SYS_ID);
	}

	public String getEfalSecSysIdFormatted() {
		return readFixedString(Pos.EFAL_SEC_SYS_ID, Len.EFAL_SEC_SYS_ID);
	}

	public void setEfalObjDataKey(String efalObjDataKey) {
		writeString(Pos.EFAL_OBJ_DATA_KEY, efalObjDataKey, Len.EFAL_OBJ_DATA_KEY);
	}

	/**Original name: EFAL-OBJ-DATA-KEY<br>*/
	public String getEfalObjDataKey() {
		return readString(Pos.EFAL_OBJ_DATA_KEY, Len.EFAL_OBJ_DATA_KEY);
	}

	public String getEfalObjDataKeyFormatted() {
		return Functions.padBlanks(getEfalObjDataKey(), Len.EFAL_OBJ_DATA_KEY);
	}

	public void setEfalActionBuffer(String efalActionBuffer) {
		writeString(Pos.EFAL_ACTION_BUFFER, efalActionBuffer, Len.EFAL_ACTION_BUFFER);
	}

	/**Original name: EFAL-ACTION-BUFFER<br>*/
	public String getEfalActionBuffer() {
		return readString(Pos.EFAL_ACTION_BUFFER, Len.EFAL_ACTION_BUFFER);
	}

	public void setEtraErrAction(String etraErrAction) {
		writeString(Pos.ETRA_ERR_ACTION, etraErrAction, Len.ETRA_ERR_ACTION);
	}

	/**Original name: ETRA-ERR-ACTION<br>*/
	public String getEtraErrAction() {
		return readString(Pos.ETRA_ERR_ACTION, Len.ETRA_ERR_ACTION);
	}

	public void setEtraDb2Insert() {
		setEtraErrAction(ETRA_DB2_INSERT);
	}

	public void setEtraDb2Update() {
		setEtraErrAction(ETRA_DB2_UPDATE);
	}

	public void setEtraDb2Delete() {
		setEtraErrAction(ETRA_DB2_DELETE);
	}

	public void setEtraDb2Select() {
		setEtraErrAction(ETRA_DB2_SELECT);
	}

	public void setEtraDb2OpenCsr() {
		setEtraErrAction(ETRA_DB2_OPEN_CSR);
	}

	public void setEtraDb2FetchCsr() {
		setEtraErrAction(ETRA_DB2_FETCH_CSR);
	}

	public void setEtraDb2CloseCsr() {
		setEtraErrAction(ETRA_DB2_CLOSE_CSR);
	}

	public void setEtraDb2SetCurrentTs() {
		setEtraErrAction(ETRA_DB2_SET_CURRENT_TS);
	}

	public void setEtraCicsReadTsq() {
		setEtraErrAction(ETRA_CICS_READ_TSQ);
	}

	public void setEtraCicsUpdateTsq() {
		setEtraErrAction(ETRA_CICS_UPDATE_TSQ);
	}

	public void setEtraCicsWriteTsq() {
		setEtraErrAction(ETRA_CICS_WRITE_TSQ);
	}

	public void setEtraCicsDeleteTsq() {
		setEtraErrAction(ETRA_CICS_DELETE_TSQ);
	}

	public void setEtraCicsStartbrUmt() {
		setEtraErrAction(ETRA_CICS_STARTBR_UMT);
	}

	public void setEtraCicsReadnxtUmt() {
		setEtraErrAction(ETRA_CICS_READNXT_UMT);
	}

	public void setEtraCicsEndbrUmt() {
		setEtraErrAction(ETRA_CICS_ENDBR_UMT);
	}

	public void setEtraCicsReadUmt() {
		setEtraErrAction(ETRA_CICS_READ_UMT);
	}

	public void setEtraCicsWriteUmt() {
		setEtraErrAction(ETRA_CICS_WRITE_UMT);
	}

	public void setEtraCicsRewriteUmt() {
		setEtraErrAction(ETRA_CICS_REWRITE_UMT);
	}

	public void setEtraCicsDeleteUmt() {
		setEtraErrAction(ETRA_CICS_DELETE_UMT);
	}

	public void setEtraCicsLink() {
		setEtraErrAction(ETRA_CICS_LINK);
	}

	public void setEtraCicsFunction() {
		setEtraErrAction(ETRA_CICS_FUNCTION);
	}

	public void setEtraCicsRetrieve() {
		setEtraErrAction(ETRA_CICS_RETRIEVE);
	}

	public void setEtraCicsWebRead() {
		setEtraErrAction(ETRA_CICS_WEB_READ);
	}

	public void setEtraCicsWebReceive() {
		setEtraErrAction(ETRA_CICS_WEB_RECEIVE);
	}

	public void setEtraCicsWebWrite() {
		setEtraErrAction(ETRA_CICS_WEB_WRITE);
	}

	public void setEtraCobolCall() {
		setEtraErrAction(ETRA_COBOL_CALL);
	}

	public void setComaMsgIdBlank() {
		setEtraErrAction(COMA_MSG_ID_BLANK);
	}

	public void setComaUowNameBlank() {
		setEtraErrAction(COMA_UOW_NAME_BLANK);
	}

	public void setComaUseridBlank() {
		setEtraErrAction(COMA_USERID_BLANK);
	}

	public void setComaAuthUserClientidBlank() {
		setEtraErrAction(COMA_AUTH_USER_CLIENTID_BLANK);
	}

	public void setComaUowReqMsgStoreBlank() {
		setEtraErrAction(COMA_UOW_REQ_MSG_STORE_BLANK);
	}

	public void setComaUowReqSwitStoreBlank() {
		setEtraErrAction(COMA_UOW_REQ_SWIT_STORE_BLANK);
	}

	public void setComaUowRespHdrStoreBlank() {
		setEtraErrAction(COMA_UOW_RESP_HDR_STORE_BLANK);
	}

	public void setComaUowRespDataStoreBlank() {
		setEtraErrAction(COMA_UOW_RESP_DATA_STORE_BLANK);
	}

	public void setComaUowRespWarnStoreBlank() {
		setEtraErrAction(COMA_UOW_RESP_WARN_STORE_BLANK);
	}

	public void setComaUowKeyReplStoreBlank() {
		setEtraErrAction(COMA_UOW_KEY_REPL_STORE_BLANK);
	}

	public void setComaLockTypeBlank() {
		setEtraErrAction(COMA_LOCK_TYPE_BLANK);
	}

	public void setComaSessionIdBlank() {
		setEtraErrAction(COMA_SESSION_ID_BLANK);
	}

	public void setBuspStoreNameBlank() {
		setEtraErrAction(BUSP_STORE_NAME_BLANK);
	}

	public void setBuspUowModuleBlank() {
		setEtraErrAction(BUSP_UOW_MODULE_BLANK);
	}

	public void setBuspLockActionBlank() {
		setEtraErrAction(BUSP_LOCK_ACTION_BLANK);
	}

	public void setBuspCnLvlSecIndBlank() {
		setEtraErrAction(BUSP_CN_LVL_SEC_IND_BLANK);
	}

	public void setBuspContLvlSecModuleBlank() {
		setEtraErrAction(BUSP_CONT_LVL_SEC_MODULE_BLANK);
	}

	public void setBuspDataPrivIndBlank() {
		setEtraErrAction(BUSP_DATA_PRIV_IND_BLANK);
	}

	public void setBuspHdrExistCantTell() {
		setEtraErrAction(BUSP_HDR_EXIST_CANT_TELL);
	}

	public void setBuspInvActionCode() {
		setEtraErrAction(BUSP_INV_ACTION_CODE);
	}

	public void setBuspParentMissing() {
		setEtraErrAction(BUSP_PARENT_MISSING);
	}

	public void setBuspKeyChanged() {
		setEtraErrAction(BUSP_KEY_CHANGED);
	}

	public void setBuspExiRowNotFound() {
		setEtraErrAction(BUSP_EXI_ROW_NOT_FOUND);
	}

	public void setBuspInvalidStoreType() {
		setEtraErrAction(BUSP_INVALID_STORE_TYPE);
	}

	public void setBuspInvParamLen() {
		setEtraErrAction(BUSP_INV_PARAM_LEN);
	}

	public void setBuspKeyAndLabelSupp() {
		setEtraErrAction(BUSP_KEY_AND_LABEL_SUPP);
	}

	public void setBuspInvalidFunction() {
		setEtraErrAction(BUSP_INVALID_FUNCTION);
	}

	public void setBuspUowDataBuffBlank() {
		setEtraErrAction(BUSP_UOW_DATA_BUFF_BLANK);
	}

	public void setBuspInvalidFetchCursor() {
		setEtraErrAction(BUSP_INVALID_FETCH_CURSOR);
	}

	public void setBuspInvalidUmtRecCnt() {
		setEtraErrAction(BUSP_INVALID_UMT_REC_CNT);
	}

	public void setBuspDuplKeyGenerated() {
		setEtraErrAction(BUSP_DUPL_KEY_GENERATED);
	}

	public void setBuspRequestMsgMissing() {
		setEtraErrAction(BUSP_REQUEST_MSG_MISSING);
	}

	public void setBuspInvalidSubBusObj() {
		setEtraErrAction(BUSP_INVALID_SUB_BUS_OBJ);
	}

	public void setBuspInvalidNullInd() {
		setEtraErrAction(BUSP_INVALID_NULL_IND);
	}

	public void setBuspInvalidSign() {
		setEtraErrAction(BUSP_INVALID_SIGN);
	}

	public void setBuspInvalidColumnInd() {
		setEtraErrAction(BUSP_INVALID_COLUMN_IND);
	}

	public void setBuspRequiredFieldBlank() {
		setEtraErrAction(BUSP_REQUIRED_FIELD_BLANK);
	}

	public void setBuspInvKeyFieldContents() {
		setEtraErrAction(BUSP_INV_KEY_FIELD_CONTENTS);
	}

	public void setBuspInvalidSuppliedValue() {
		setEtraErrAction(BUSP_INVALID_SUPPLIED_VALUE);
	}

	public void setBuspLimitExceeded() {
		setEtraErrAction(BUSP_LIMIT_EXCEEDED);
	}

	public void setBuspReqdDataNotFound() {
		setEtraErrAction(BUSP_REQD_DATA_NOT_FOUND);
	}

	public void setBuspUbocLinkageCorrupted() {
		setEtraErrAction(BUSP_UBOC_LINKAGE_CORRUPTED);
	}

	public void setSecuSecSysAccessDenied() {
		setEtraErrAction(SECU_SEC_SYS_ACCESS_DENIED);
	}

	public void setSecuSecUowAccessDenied() {
		setEtraErrAction(SECU_SEC_UOW_ACCESS_DENIED);
	}

	public void setIapDateReturnError() {
		setEtraErrAction(IAP_DATE_RETURN_ERROR);
	}

	public void setLokInvalidLegacyRet() {
		setEtraErrAction(LOK_INVALID_LEGACY_RET);
	}

	public void setFramOperationNameBlank() {
		setEtraErrAction(FRAM_OPERATION_NAME_BLANK);
	}

	public void setEfalEtraPriorityLevelSign(char efalEtraPriorityLevelSign) {
		writeChar(Pos.EFAL_ETRA_PRIORITY_LEVEL_SIGN, efalEtraPriorityLevelSign);
	}

	public void setEfalEtraPriorityLevelSignFormatted(String efalEtraPriorityLevelSign) {
		setEfalEtraPriorityLevelSign(Functions.charAt(efalEtraPriorityLevelSign, Types.CHAR_SIZE));
	}

	/**Original name: EFAL-ETRA-PRIORITY-LEVEL-SIGN<br>*/
	public char getEfalEtraPriorityLevelSign() {
		return readChar(Pos.EFAL_ETRA_PRIORITY_LEVEL_SIGN);
	}

	public void setEfalEtraPriorityLevel(int efalEtraPriorityLevel) {
		writeInt(Pos.EFAL_ETRA_PRIORITY_LEVEL, efalEtraPriorityLevel, Len.Int.EFAL_ETRA_PRIORITY_LEVEL, SignType.NO_SIGN);
	}

	public void setEfalEtraPriorityLevelFormatted(String efalEtraPriorityLevel) {
		writeString(Pos.EFAL_ETRA_PRIORITY_LEVEL, Trunc.toUnsignedNumeric(efalEtraPriorityLevel, Len.EFAL_ETRA_PRIORITY_LEVEL),
				Len.EFAL_ETRA_PRIORITY_LEVEL);
	}

	/**Original name: EFAL-ETRA-PRIORITY-LEVEL<br>*/
	public int getEfalEtraPriorityLevel() {
		return readNumDispUnsignedInt(Pos.EFAL_ETRA_PRIORITY_LEVEL, Len.EFAL_ETRA_PRIORITY_LEVEL);
	}

	public String getEfalEtraPriorityLevelFormatted() {
		return readFixedString(Pos.EFAL_ETRA_PRIORITY_LEVEL, Len.EFAL_ETRA_PRIORITY_LEVEL);
	}

	public void setEfalEtraErrorRef(String efalEtraErrorRef) {
		writeString(Pos.EFAL_ETRA_ERROR_REF, efalEtraErrorRef, Len.EFAL_ETRA_ERROR_REF);
	}

	/**Original name: EFAL-ETRA-ERROR-REF<br>*/
	public String getEfalEtraErrorRef() {
		return readString(Pos.EFAL_ETRA_ERROR_REF, Len.EFAL_ETRA_ERROR_REF);
	}

	public String getEfalEtraErrorRefFormatted() {
		return Functions.padBlanks(getEfalEtraErrorRef(), Len.EFAL_ETRA_ERROR_REF);
	}

	public void setEfalEtraErrorTxt(String efalEtraErrorTxt) {
		writeString(Pos.EFAL_ETRA_ERROR_TXT, efalEtraErrorTxt, Len.EFAL_ETRA_ERROR_TXT);
	}

	/**Original name: EFAL-ETRA-ERROR-TXT<br>*/
	public String getEfalEtraErrorTxt() {
		return readString(Pos.EFAL_ETRA_ERROR_TXT, Len.EFAL_ETRA_ERROR_TXT);
	}

	public String getEfalEtraErrorTxtFormatted() {
		return Functions.padBlanks(getEfalEtraErrorTxt(), Len.EFAL_ETRA_ERROR_TXT);
	}

	/**Original name: EMCM-MCM-LVL-GUID<br>*/
	public String getEmcmMcmLvlGuid() {
		return readString(Pos.EMCM_MCM_LVL_GUID, Len.EMCM_MCM_LVL_GUID);
	}

	public void setEmcmMcmLvlErrTimestamp(String emcmMcmLvlErrTimestamp) {
		writeString(Pos.EMCM_MCM_LVL_ERR_TIMESTAMP, emcmMcmLvlErrTimestamp, Len.EMCM_MCM_LVL_ERR_TIMESTAMP);
	}

	/**Original name: EMCM-MCM-LVL-ERR-TIMESTAMP<br>*/
	public String getEmcmMcmLvlErrTimestamp() {
		return readString(Pos.EMCM_MCM_LVL_ERR_TIMESTAMP, Len.EMCM_MCM_LVL_ERR_TIMESTAMP);
	}

	/**Original name: EMCM-MSG-CNTL-MOD<br>*/
	public String getEmcmMsgCntlMod() {
		return readString(Pos.EMCM_MSG_CNTL_MOD, Len.EMCM_MSG_CNTL_MOD);
	}

	/**Original name: EMCM-PRIMARY-BUS-OBJ<br>*/
	public String getEmcmPrimaryBusObj() {
		return readString(Pos.EMCM_PRIMARY_BUS_OBJ, Len.EMCM_PRIMARY_BUS_OBJ);
	}

	/**Original name: EMCM-STORE-TYPE<br>*/
	public char getEmcmStoreType() {
		return readChar(Pos.EMCM_STORE_TYPE);
	}

	/**Original name: EMCM-PASS-THRU-ACTION<br>*/
	public String getEmcmPassThruAction() {
		return readString(Pos.EMCM_PASS_THRU_ACTION, Len.EMCM_PASS_THRU_ACTION);
	}

	/**Original name: EMCM-UOW-REQ-MSG-ROWS-SIGN<br>
	 * <pre>* UOW LEVEL REQUEST UMT ROW COUNTS OF INCOMING ROWS</pre>*/
	public char getEmcmUowReqMsgRowsSign() {
		return readChar(Pos.EMCM_UOW_REQ_MSG_ROWS_SIGN);
	}

	public String getEmcmUowReqMsgRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_REQ_MSG_ROWS, Len.EMCM_UOW_REQ_MSG_ROWS);
	}

	/**Original name: EMCM-UOW-REQ-SWITCH-ROWS-SIGN<br>*/
	public char getEmcmUowReqSwitchRowsSign() {
		return readChar(Pos.EMCM_UOW_REQ_SWITCH_ROWS_SIGN);
	}

	public String getEmcmUowReqSwitchRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_REQ_SWITCH_ROWS, Len.EMCM_UOW_REQ_SWITCH_ROWS);
	}

	/**Original name: EMCM-UOW-RESP-HDR-ROWS-SIGN<br>
	 * <pre>* UOW LEVEL RESPONSE UMT ROW COUNTS</pre>*/
	public char getEmcmUowRespHdrRowsSign() {
		return readChar(Pos.EMCM_UOW_RESP_HDR_ROWS_SIGN);
	}

	public String getEmcmUowRespHdrRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_RESP_HDR_ROWS, Len.EMCM_UOW_RESP_HDR_ROWS);
	}

	/**Original name: EMCM-UOW-RESP-DATA-ROWS-SIGN<br>*/
	public char getEmcmUowRespDataRowsSign() {
		return readChar(Pos.EMCM_UOW_RESP_DATA_ROWS_SIGN);
	}

	public String getEmcmUowRespDataRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_RESP_DATA_ROWS, Len.EMCM_UOW_RESP_DATA_ROWS);
	}

	/**Original name: EMCM-UOW-RESP-WARN-ROWS-SIGN<br>*/
	public char getEmcmUowRespWarnRowsSign() {
		return readChar(Pos.EMCM_UOW_RESP_WARN_ROWS_SIGN);
	}

	public String getEmcmUowRespWarnRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_RESP_WARN_ROWS, Len.EMCM_UOW_RESP_WARN_ROWS);
	}

	/**Original name: EMCM-UOW-RESP-NLBE-ROWS-SIGN<br>*/
	public char getEmcmUowRespNlbeRowsSign() {
		return readChar(Pos.EMCM_UOW_RESP_NLBE_ROWS_SIGN);
	}

	public String getEmcmUowRespNlbeRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_RESP_NLBE_ROWS, Len.EMCM_UOW_RESP_NLBE_ROWS);
	}

	/**Original name: EMCM-UOW-PROC-HDR-ROWS-SIGN<br>
	 * <pre>* COUNTS OF UOW LEVEL RESPONSE UMT ROWS THAT HAVE BEEN
	 * * PROCESSED.</pre>*/
	public char getEmcmUowProcHdrRowsSign() {
		return readChar(Pos.EMCM_UOW_PROC_HDR_ROWS_SIGN);
	}

	public String getEmcmUowProcHdrRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_PROC_HDR_ROWS, Len.EMCM_UOW_PROC_HDR_ROWS);
	}

	/**Original name: EMCM-UOW-PROC-DATA-ROWS-SIGN<br>*/
	public char getEmcmUowProcDataRowsSign() {
		return readChar(Pos.EMCM_UOW_PROC_DATA_ROWS_SIGN);
	}

	public String getEmcmUowProcDataRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_PROC_DATA_ROWS, Len.EMCM_UOW_PROC_DATA_ROWS);
	}

	/**Original name: EMCM-UOW-PROC-WARN-ROWS-SIGN<br>*/
	public char getEmcmUowProcWarnRowsSign() {
		return readChar(Pos.EMCM_UOW_PROC_WARN_ROWS_SIGN);
	}

	public String getEmcmUowProcWarnRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_PROC_WARN_ROWS, Len.EMCM_UOW_PROC_WARN_ROWS);
	}

	/**Original name: EMCM-UOW-PROC-NLBE-ROWS-SIGN<br>*/
	public char getEmcmUowProcNlbeRowsSign() {
		return readChar(Pos.EMCM_UOW_PROC_NLBE_ROWS_SIGN);
	}

	public String getEmcmUowProcNlbeRowsFormatted() {
		return readFixedString(Pos.EMCM_UOW_PROC_NLBE_ROWS, Len.EMCM_UOW_PROC_NLBE_ROWS);
	}

	/**Original name: EMCM-LOCK-TYPE<br>
	 * <pre>* MCM LOCKING INFO</pre>*/
	public String getEmcmLockType() {
		return readString(Pos.EMCM_LOCK_TYPE, Len.EMCM_LOCK_TYPE);
	}

	/**Original name: EMCM-UOW-LOCK-ACTION-CD<br>*/
	public char getEmcmUowLockActionCd() {
		return readChar(Pos.EMCM_UOW_LOCK_ACTION_CD);
	}

	/**Original name: EMCM-FLDR-LOKS-DEL-TIME-SIGN<br>*/
	public char getEmcmFldrLoksDelTimeSign() {
		return readChar(Pos.EMCM_FLDR_LOKS_DEL_TIME_SIGN);
	}

	public String getEmcmFolderLoksDelTimeFormatted() {
		return readFixedString(Pos.EMCM_FOLDER_LOKS_DEL_TIME, Len.EMCM_FOLDER_LOKS_DEL_TIME);
	}

	/**Original name: EMCM-LOKS-UOW-CONTINUE-SW<br>*/
	public char getEmcmLoksUowContinueSw() {
		return readChar(Pos.EMCM_LOKS_UOW_CONTINUE_SW);
	}

	/**Original name: EMCM-APPLY-DATA-PRIVACY-SW<br>*/
	public char getEmcmApplyDataPrivacySw() {
		return readChar(Pos.EMCM_APPLY_DATA_PRIVACY_SW);
	}

	/**Original name: EMCM-SECURITY-CONTEXT-NI<br>*/
	public char getEmcmSecurityContextNi() {
		return readChar(Pos.EMCM_SECURITY_CONTEXT_NI);
	}

	/**Original name: EMCM-SECURITY-CONTEXT<br>*/
	public String getEmcmSecurityContext() {
		return readString(Pos.EMCM_SECURITY_CONTEXT, Len.EMCM_SECURITY_CONTEXT);
	}

	/**Original name: EMCM-SEC-AUT-NBR-SIGN<br>*/
	public char getEmcmSecAutNbrSign() {
		return readChar(Pos.EMCM_SEC_AUT_NBR_SIGN);
	}

	public String getEmcmSecAutNbrFormatted() {
		return readFixedString(Pos.EMCM_SEC_AUT_NBR, Len.EMCM_SEC_AUT_NBR);
	}

	/**Original name: EMCM-SEC-ASSOC-TYPE-NI<br>*/
	public char getEmcmSecAssocTypeNi() {
		return readChar(Pos.EMCM_SEC_ASSOC_TYPE_NI);
	}

	/**Original name: EMCM-SEC-ASSOCIATION-TYPE<br>*/
	public String getEmcmSecAssociationType() {
		return readString(Pos.EMCM_SEC_ASSOCIATION_TYPE, Len.EMCM_SEC_ASSOCIATION_TYPE);
	}

	/**Original name: EMCM-DATA-PRIVACY-RETCODE-NI<br>*/
	public char getEmcmDataPrivacyRetcodeNi() {
		return readChar(Pos.EMCM_DATA_PRIVACY_RETCODE_NI);
	}

	/**Original name: EMCM-DATA-PRIVACY-RETCODE<br>*/
	public String getEmcmDataPrivacyRetcode() {
		return readString(Pos.EMCM_DATA_PRIVACY_RETCODE, Len.EMCM_DATA_PRIVACY_RETCODE);
	}

	/**Original name: EMCM-APPLY-AUDITS-SW<br>*/
	public char getEmcmApplyAuditsSw() {
		return readChar(Pos.EMCM_APPLY_AUDITS_SW);
	}

	/**Original name: EMCM-AUDT-BUS-OBJ-NM-NI<br>*/
	public char getEmcmAudtBusObjNmNi() {
		return readChar(Pos.EMCM_AUDT_BUS_OBJ_NM_NI);
	}

	/**Original name: EMCM-AUDT-BUS-OBJ-NM<br>*/
	public String getEmcmAudtBusObjNm() {
		return readString(Pos.EMCM_AUDT_BUS_OBJ_NM, Len.EMCM_AUDT_BUS_OBJ_NM);
	}

	/**Original name: EMCM-AUDT-EVENT-DATA-NI<br>*/
	public char getEmcmAudtEventDataNi() {
		return readChar(Pos.EMCM_AUDT_EVENT_DATA_NI);
	}

	/**Original name: EMCM-AUDT-EVENT-DATA<br>*/
	public String getEmcmAudtEventData() {
		return readString(Pos.EMCM_AUDT_EVENT_DATA, Len.EMCM_AUDT_EVENT_DATA);
	}

	public String getEmcmAudtEventDataFormatted() {
		return Functions.padBlanks(getEmcmAudtEventData(), Len.EMCM_AUDT_EVENT_DATA);
	}

	/**Original name: EMCM-HAL-EMCM-KEEP-CLR<br>*/
	public char getEmcmHalEmcmKeepClr() {
		return readChar(Pos.EMCM_HAL_EMCM_KEEP_CLR);
	}

	/**Original name: EMDR-MDRV-LVL-GUID<br>*/
	public String getEmdrMdrvLvlGuid() {
		return readString(Pos.EMDR_MDRV_LVL_GUID, Len.EMDR_MDRV_LVL_GUID);
	}

	public void setEmdrMdrvLvlErrTimestamp(String emdrMdrvLvlErrTimestamp) {
		writeString(Pos.EMDR_MDRV_LVL_ERR_TIMESTAMP, emdrMdrvLvlErrTimestamp, Len.EMDR_MDRV_LVL_ERR_TIMESTAMP);
	}

	/**Original name: EMDR-MDRV-LVL-ERR-TIMESTAMP<br>*/
	public String getEmdrMdrvLvlErrTimestamp() {
		return readString(Pos.EMDR_MDRV_LVL_ERR_TIMESTAMP, Len.EMDR_MDRV_LVL_ERR_TIMESTAMP);
	}

	/**Original name: EMDR-TERMINAL-ID<br>*/
	public String getEmdrTerminalId() {
		return readString(Pos.EMDR_TERMINAL_ID, Len.EMDR_TERMINAL_ID);
	}

	/**Original name: EMDR-MSG-TRANSFER-CD<br>*/
	public char getEmdrMsgTransferCd() {
		return readChar(Pos.EMDR_MSG_TRANSFER_CD);
	}

	/**Original name: EMDR-CURRENT-CHUNK-SIGN<br>*/
	public char getEmdrCurrentChunkSign() {
		return readChar(Pos.EMDR_CURRENT_CHUNK_SIGN);
	}

	public String getEmdrCurrentChunkFormatted() {
		return readFixedString(Pos.EMDR_CURRENT_CHUNK, Len.EMDR_CURRENT_CHUNK);
	}

	/**Original name: EMDR-DEL-STORE-SW<br>*/
	public char getEmdrDelStoreSw() {
		return readChar(Pos.EMDR_DEL_STORE_SW);
	}

	/**Original name: EMDR-RETURN-WARNS-SW<br>*/
	public char getEmdrReturnWarnsSw() {
		return readChar(Pos.EMDR_RETURN_WARNS_SW);
	}

	/**Original name: EMDR-TOTAL-MSG-LENGTH-SIGN<br>*/
	public char getEmdrTotalMsgLengthSign() {
		return readChar(Pos.EMDR_TOTAL_MSG_LENGTH_SIGN);
	}

	public String getEmdrTotalMsgLengthFormatted() {
		return readFixedString(Pos.EMDR_TOTAL_MSG_LENGTH, Len.EMDR_TOTAL_MSG_LENGTH);
	}

	/**Original name: EMDR-CURRENT-BUFFER-LEN-SIGN<br>*/
	public char getEmdrCurrentBufferLenSign() {
		return readChar(Pos.EMDR_CURRENT_BUFFER_LEN_SIGN);
	}

	public String getEmdrCurrentBufferLengthFormatted() {
		return readFixedString(Pos.EMDR_CURRENT_BUFFER_LENGTH, Len.EMDR_CURRENT_BUFFER_LENGTH);
	}

	/**Original name: EMDR-TOTAL-NBR-CHUNKS-SIGN<br>*/
	public char getEmdrTotalNbrChunksSign() {
		return readChar(Pos.EMDR_TOTAL_NBR_CHUNKS_SIGN);
	}

	public String getEmdrTotalNbrChunksFormatted() {
		return readFixedString(Pos.EMDR_TOTAL_NBR_CHUNKS, Len.EMDR_TOTAL_NBR_CHUNKS);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int ESTO_DETAIL_BUFFER = 1;
		public static final int EDTL_FAILURE_DETAIL = 1;
		public static final int EFAL_FAILURE_RECORD_INFO = EDTL_FAILURE_DETAIL;
		public static final int EFAL_FAIL_LVL_GUID = EFAL_FAILURE_RECORD_INFO;
		public static final int EFAL_FAIL_LVL_ERR_TIMESTAMP = EFAL_FAIL_LVL_GUID + Len.EFAL_FAIL_LVL_GUID;
		public static final int EFAL_FAILED_APPLICATION = EFAL_FAIL_LVL_ERR_TIMESTAMP + Len.EFAL_FAIL_LVL_ERR_TIMESTAMP;
		public static final int EFAL_FAILED_PLATFORM = EFAL_FAILED_APPLICATION + Len.EFAL_FAILED_APPLICATION;
		public static final int EFAL_FAILURE_TYPE = EFAL_FAILED_PLATFORM + Len.EFAL_FAILED_PLATFORM;
		public static final int EFAL_FAILED_ACTION_TYPE = EFAL_FAILURE_TYPE + Len.EFAL_FAILURE_TYPE;
		public static final int EFAL_FAILED_MODULE = EFAL_FAILED_ACTION_TYPE + Len.EFAL_FAILED_ACTION_TYPE;
		public static final int EFAL_ERR_PARAGRAPH = EFAL_FAILED_MODULE + Len.EFAL_FAILED_MODULE;
		public static final int EFAL_ERR_OBJECT_NAME = EFAL_ERR_PARAGRAPH + Len.EFAL_ERR_PARAGRAPH;
		public static final int EFAL_DB2_ERR_SQLCODE_SIGN = EFAL_ERR_OBJECT_NAME + Len.EFAL_ERR_OBJECT_NAME;
		public static final int EFAL_DB2_ERR_SQLCODE = EFAL_DB2_ERR_SQLCODE_SIGN + Len.EFAL_DB2_ERR_SQLCODE_SIGN;
		public static final int EFAL_CICS_ERR_RESP_SIGN = EFAL_DB2_ERR_SQLCODE + Len.EFAL_DB2_ERR_SQLCODE;
		public static final int EFAL_CICS_ERR_RESP = EFAL_CICS_ERR_RESP_SIGN + Len.EFAL_CICS_ERR_RESP_SIGN;
		public static final int EFAL_ERR_COMMENT = EFAL_CICS_ERR_RESP + Len.EFAL_CICS_ERR_RESP;
		public static final int EFAL_HAL_ERR_LOG_FAIL_DATA = EFAL_ERR_COMMENT + Len.EFAL_ERR_COMMENT;
		public static final int EFAL_UNIT_OF_WORK = EFAL_HAL_ERR_LOG_FAIL_DATA;
		public static final int EFAL_FAILED_LOCATION_ID = EFAL_UNIT_OF_WORK + Len.EFAL_UNIT_OF_WORK;
		public static final int EFAL_DB2_ERR_SQLERRMC = EFAL_FAILED_LOCATION_ID + Len.EFAL_FAILED_LOCATION_ID;
		public static final int EFAL_CICS_ERR_RESP2_SIGN = EFAL_DB2_ERR_SQLERRMC + Len.EFAL_DB2_ERR_SQLERRMC;
		public static final int EFAL_CICS_ERR_RESP2 = EFAL_CICS_ERR_RESP2_SIGN + Len.EFAL_CICS_ERR_RESP2_SIGN;
		public static final int EFAL_LOGON_USERID = EFAL_CICS_ERR_RESP2 + Len.EFAL_CICS_ERR_RESP2;
		public static final int EFAL_SEC_SYS_ID_SIGN = EFAL_LOGON_USERID + Len.EFAL_LOGON_USERID;
		public static final int EFAL_SEC_SYS_ID = EFAL_SEC_SYS_ID_SIGN + Len.EFAL_SEC_SYS_ID_SIGN;
		public static final int EFAL_OBJ_DATA_KEY = EFAL_SEC_SYS_ID + Len.EFAL_SEC_SYS_ID;
		public static final int EFAL_ACTION_BUFFER = EFAL_OBJ_DATA_KEY + Len.EFAL_OBJ_DATA_KEY;
		public static final int ETRA_ACTION_BUFFER = EFAL_ACTION_BUFFER;
		public static final int ETRA_ERR_ACTION = ETRA_ACTION_BUFFER;
		public static final int FLR1 = ETRA_ERR_ACTION + Len.ETRA_ERR_ACTION;
		public static final int EFAL_ETRA_PRIORITY_LEVEL_SIGN = EFAL_ACTION_BUFFER + Len.EFAL_ACTION_BUFFER;
		public static final int EFAL_ETRA_PRIORITY_LEVEL = EFAL_ETRA_PRIORITY_LEVEL_SIGN + Len.EFAL_ETRA_PRIORITY_LEVEL_SIGN;
		public static final int EFAL_ETRA_ERROR_REF = EFAL_ETRA_PRIORITY_LEVEL + Len.EFAL_ETRA_PRIORITY_LEVEL;
		public static final int EFAL_ETRA_ERROR_TXT = EFAL_ETRA_ERROR_REF + Len.EFAL_ETRA_ERROR_REF;
		public static final int FLR2 = EFAL_ETRA_ERROR_TXT + Len.EFAL_ETRA_ERROR_TXT;
		public static final int EDTL_MCM_DETAIL = 1;
		public static final int EMCM_MCM_RECORD_INFO = EDTL_MCM_DETAIL;
		public static final int EMCM_MCM_LVL_GUID = EMCM_MCM_RECORD_INFO;
		public static final int EMCM_MCM_LVL_ERR_TIMESTAMP = EMCM_MCM_LVL_GUID + Len.EMCM_MCM_LVL_GUID;
		public static final int EMCM_MSG_CNTL_MOD = EMCM_MCM_LVL_ERR_TIMESTAMP + Len.EMCM_MCM_LVL_ERR_TIMESTAMP;
		public static final int EMCM_PRIMARY_BUS_OBJ = EMCM_MSG_CNTL_MOD + Len.EMCM_MSG_CNTL_MOD;
		public static final int EMCM_STORE_TYPE = EMCM_PRIMARY_BUS_OBJ + Len.EMCM_PRIMARY_BUS_OBJ;
		public static final int EMCM_PASS_THRU_ACTION = EMCM_STORE_TYPE + Len.EMCM_STORE_TYPE;
		public static final int EMCM_UOW_REQ_MSG_ROWS_SIGN = EMCM_PASS_THRU_ACTION + Len.EMCM_PASS_THRU_ACTION;
		public static final int EMCM_UOW_REQ_MSG_ROWS = EMCM_UOW_REQ_MSG_ROWS_SIGN + Len.EMCM_UOW_REQ_MSG_ROWS_SIGN;
		public static final int EMCM_UOW_REQ_SWITCH_ROWS_SIGN = EMCM_UOW_REQ_MSG_ROWS + Len.EMCM_UOW_REQ_MSG_ROWS;
		public static final int EMCM_UOW_REQ_SWITCH_ROWS = EMCM_UOW_REQ_SWITCH_ROWS_SIGN + Len.EMCM_UOW_REQ_SWITCH_ROWS_SIGN;
		public static final int EMCM_UOW_RESP_HDR_ROWS_SIGN = EMCM_UOW_REQ_SWITCH_ROWS + Len.EMCM_UOW_REQ_SWITCH_ROWS;
		public static final int EMCM_UOW_RESP_HDR_ROWS = EMCM_UOW_RESP_HDR_ROWS_SIGN + Len.EMCM_UOW_RESP_HDR_ROWS_SIGN;
		public static final int EMCM_UOW_RESP_DATA_ROWS_SIGN = EMCM_UOW_RESP_HDR_ROWS + Len.EMCM_UOW_RESP_HDR_ROWS;
		public static final int EMCM_UOW_RESP_DATA_ROWS = EMCM_UOW_RESP_DATA_ROWS_SIGN + Len.EMCM_UOW_RESP_DATA_ROWS_SIGN;
		public static final int EMCM_UOW_RESP_WARN_ROWS_SIGN = EMCM_UOW_RESP_DATA_ROWS + Len.EMCM_UOW_RESP_DATA_ROWS;
		public static final int EMCM_UOW_RESP_WARN_ROWS = EMCM_UOW_RESP_WARN_ROWS_SIGN + Len.EMCM_UOW_RESP_WARN_ROWS_SIGN;
		public static final int EMCM_UOW_RESP_NLBE_ROWS_SIGN = EMCM_UOW_RESP_WARN_ROWS + Len.EMCM_UOW_RESP_WARN_ROWS;
		public static final int EMCM_UOW_RESP_NLBE_ROWS = EMCM_UOW_RESP_NLBE_ROWS_SIGN + Len.EMCM_UOW_RESP_NLBE_ROWS_SIGN;
		public static final int EMCM_UOW_PROC_HDR_ROWS_SIGN = EMCM_UOW_RESP_NLBE_ROWS + Len.EMCM_UOW_RESP_NLBE_ROWS;
		public static final int EMCM_UOW_PROC_HDR_ROWS = EMCM_UOW_PROC_HDR_ROWS_SIGN + Len.EMCM_UOW_PROC_HDR_ROWS_SIGN;
		public static final int EMCM_UOW_PROC_DATA_ROWS_SIGN = EMCM_UOW_PROC_HDR_ROWS + Len.EMCM_UOW_PROC_HDR_ROWS;
		public static final int EMCM_UOW_PROC_DATA_ROWS = EMCM_UOW_PROC_DATA_ROWS_SIGN + Len.EMCM_UOW_PROC_DATA_ROWS_SIGN;
		public static final int EMCM_UOW_PROC_WARN_ROWS_SIGN = EMCM_UOW_PROC_DATA_ROWS + Len.EMCM_UOW_PROC_DATA_ROWS;
		public static final int EMCM_UOW_PROC_WARN_ROWS = EMCM_UOW_PROC_WARN_ROWS_SIGN + Len.EMCM_UOW_PROC_WARN_ROWS_SIGN;
		public static final int EMCM_UOW_PROC_NLBE_ROWS_SIGN = EMCM_UOW_PROC_WARN_ROWS + Len.EMCM_UOW_PROC_WARN_ROWS;
		public static final int EMCM_UOW_PROC_NLBE_ROWS = EMCM_UOW_PROC_NLBE_ROWS_SIGN + Len.EMCM_UOW_PROC_NLBE_ROWS_SIGN;
		public static final int EMCM_LOCK_TYPE = EMCM_UOW_PROC_NLBE_ROWS + Len.EMCM_UOW_PROC_NLBE_ROWS;
		public static final int EMCM_UOW_LOCK_ACTION_CD = EMCM_LOCK_TYPE + Len.EMCM_LOCK_TYPE;
		public static final int EMCM_FLDR_LOKS_DEL_TIME_SIGN = EMCM_UOW_LOCK_ACTION_CD + Len.EMCM_UOW_LOCK_ACTION_CD;
		public static final int EMCM_FOLDER_LOKS_DEL_TIME = EMCM_FLDR_LOKS_DEL_TIME_SIGN + Len.EMCM_FLDR_LOKS_DEL_TIME_SIGN;
		public static final int EMCM_LOKS_UOW_CONTINUE_SW = EMCM_FOLDER_LOKS_DEL_TIME + Len.EMCM_FOLDER_LOKS_DEL_TIME;
		public static final int EMCM_SEC_AND_DATAPRIV_INFO = EMCM_LOKS_UOW_CONTINUE_SW + Len.EMCM_LOKS_UOW_CONTINUE_SW;
		public static final int EMCM_APPLY_DATA_PRIVACY_SW = EMCM_SEC_AND_DATAPRIV_INFO;
		public static final int EMCM_SECURITY_CONTEXT_NI = EMCM_APPLY_DATA_PRIVACY_SW + Len.EMCM_APPLY_DATA_PRIVACY_SW;
		public static final int EMCM_SECURITY_CONTEXT = EMCM_SECURITY_CONTEXT_NI + Len.EMCM_SECURITY_CONTEXT_NI;
		public static final int EMCM_SEC_AUT_NBR_SIGN = EMCM_SECURITY_CONTEXT + Len.EMCM_SECURITY_CONTEXT;
		public static final int EMCM_SEC_AUT_NBR = EMCM_SEC_AUT_NBR_SIGN + Len.EMCM_SEC_AUT_NBR_SIGN;
		public static final int EMCM_SEC_ASSOC_TYPE_NI = EMCM_SEC_AUT_NBR + Len.EMCM_SEC_AUT_NBR;
		public static final int EMCM_SEC_ASSOCIATION_TYPE = EMCM_SEC_ASSOC_TYPE_NI + Len.EMCM_SEC_ASSOC_TYPE_NI;
		public static final int EMCM_DATA_PRIVACY_RETCODE_NI = EMCM_SEC_ASSOCIATION_TYPE + Len.EMCM_SEC_ASSOCIATION_TYPE;
		public static final int EMCM_DATA_PRIVACY_RETCODE = EMCM_DATA_PRIVACY_RETCODE_NI + Len.EMCM_DATA_PRIVACY_RETCODE_NI;
		public static final int EMCM_AUDIT_PROCESSING_INFO = EMCM_DATA_PRIVACY_RETCODE + Len.EMCM_DATA_PRIVACY_RETCODE;
		public static final int EMCM_APPLY_AUDITS_SW = EMCM_AUDIT_PROCESSING_INFO;
		public static final int EMCM_AUDT_BUS_OBJ_NM_NI = EMCM_APPLY_AUDITS_SW + Len.EMCM_APPLY_AUDITS_SW;
		public static final int EMCM_AUDT_BUS_OBJ_NM = EMCM_AUDT_BUS_OBJ_NM_NI + Len.EMCM_AUDT_BUS_OBJ_NM_NI;
		public static final int EMCM_AUDT_EVENT_DATA_NI = EMCM_AUDT_BUS_OBJ_NM + Len.EMCM_AUDT_BUS_OBJ_NM;
		public static final int EMCM_AUDT_EVENT_DATA = EMCM_AUDT_EVENT_DATA_NI + Len.EMCM_AUDT_EVENT_DATA_NI;
		public static final int EMCM_HAL_EMCM_KEEP_CLR = EMCM_AUDT_EVENT_DATA + Len.EMCM_AUDT_EVENT_DATA;
		public static final int FLR3 = EMCM_HAL_EMCM_KEEP_CLR + Len.EMCM_HAL_EMCM_KEEP_CLR;
		public static final int EDTL_MDRV_DETAIL = 1;
		public static final int EMDR_MDRV_INFO = EDTL_MDRV_DETAIL;
		public static final int EMDR_MDRV_LVL_GUID = EMDR_MDRV_INFO;
		public static final int EMDR_MDRV_LVL_ERR_TIMESTAMP = EMDR_MDRV_LVL_GUID + Len.EMDR_MDRV_LVL_GUID;
		public static final int EMDR_TERMINAL_ID = EMDR_MDRV_LVL_ERR_TIMESTAMP + Len.EMDR_MDRV_LVL_ERR_TIMESTAMP;
		public static final int EMDR_MSG_TRANSFER_CD = EMDR_TERMINAL_ID + Len.EMDR_TERMINAL_ID;
		public static final int EMDR_CURRENT_CHUNK_SIGN = EMDR_MSG_TRANSFER_CD + Len.EMDR_MSG_TRANSFER_CD;
		public static final int EMDR_CURRENT_CHUNK = EMDR_CURRENT_CHUNK_SIGN + Len.EMDR_CURRENT_CHUNK_SIGN;
		public static final int EMDR_DEL_STORE_SW = EMDR_CURRENT_CHUNK + Len.EMDR_CURRENT_CHUNK;
		public static final int EMDR_RETURN_WARNS_SW = EMDR_DEL_STORE_SW + Len.EMDR_DEL_STORE_SW;
		public static final int EMDR_TOTAL_MSG_LENGTH_SIGN = EMDR_RETURN_WARNS_SW + Len.EMDR_RETURN_WARNS_SW;
		public static final int EMDR_TOTAL_MSG_LENGTH = EMDR_TOTAL_MSG_LENGTH_SIGN + Len.EMDR_TOTAL_MSG_LENGTH_SIGN;
		public static final int EMDR_CURRENT_BUFFER_LEN_SIGN = EMDR_TOTAL_MSG_LENGTH + Len.EMDR_TOTAL_MSG_LENGTH;
		public static final int EMDR_CURRENT_BUFFER_LENGTH = EMDR_CURRENT_BUFFER_LEN_SIGN + Len.EMDR_CURRENT_BUFFER_LEN_SIGN;
		public static final int EMDR_TOTAL_NBR_CHUNKS_SIGN = EMDR_CURRENT_BUFFER_LENGTH + Len.EMDR_CURRENT_BUFFER_LENGTH;
		public static final int EMDR_TOTAL_NBR_CHUNKS = EMDR_TOTAL_NBR_CHUNKS_SIGN + Len.EMDR_TOTAL_NBR_CHUNKS_SIGN;
		public static final int FLR4 = EMDR_TOTAL_NBR_CHUNKS + Len.EMDR_TOTAL_NBR_CHUNKS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int EFAL_FAIL_LVL_GUID = 32;
		public static final int EFAL_FAIL_LVL_ERR_TIMESTAMP = 26;
		public static final int EFAL_FAILED_APPLICATION = 10;
		public static final int EFAL_FAILED_PLATFORM = 10;
		public static final int EFAL_FAILURE_TYPE = 1;
		public static final int EFAL_FAILED_ACTION_TYPE = 8;
		public static final int EFAL_FAILED_MODULE = 32;
		public static final int EFAL_ERR_PARAGRAPH = 30;
		public static final int EFAL_ERR_OBJECT_NAME = 32;
		public static final int EFAL_DB2_ERR_SQLCODE_SIGN = 1;
		public static final int EFAL_DB2_ERR_SQLCODE = 10;
		public static final int EFAL_CICS_ERR_RESP_SIGN = 1;
		public static final int EFAL_CICS_ERR_RESP = 10;
		public static final int EFAL_ERR_COMMENT = 50;
		public static final int EFAL_UNIT_OF_WORK = 32;
		public static final int EFAL_FAILED_LOCATION_ID = 32;
		public static final int EFAL_DB2_ERR_SQLERRMC = 70;
		public static final int EFAL_CICS_ERR_RESP2_SIGN = 1;
		public static final int EFAL_CICS_ERR_RESP2 = 10;
		public static final int EFAL_LOGON_USERID = 32;
		public static final int EFAL_SEC_SYS_ID_SIGN = 1;
		public static final int EFAL_SEC_SYS_ID = 10;
		public static final int EFAL_OBJ_DATA_KEY = 200;
		public static final int ETRA_ERR_ACTION = 30;
		public static final int EFAL_ACTION_BUFFER = 250;
		public static final int EFAL_ETRA_PRIORITY_LEVEL_SIGN = 1;
		public static final int EFAL_ETRA_PRIORITY_LEVEL = 5;
		public static final int EFAL_ETRA_ERROR_REF = 10;
		public static final int EFAL_ETRA_ERROR_TXT = 100;
		public static final int EMCM_MCM_LVL_GUID = 32;
		public static final int EMCM_MCM_LVL_ERR_TIMESTAMP = 26;
		public static final int EMCM_MSG_CNTL_MOD = 32;
		public static final int EMCM_PRIMARY_BUS_OBJ = 32;
		public static final int EMCM_STORE_TYPE = 1;
		public static final int EMCM_PASS_THRU_ACTION = 20;
		public static final int EMCM_UOW_REQ_MSG_ROWS_SIGN = 1;
		public static final int EMCM_UOW_REQ_MSG_ROWS = 6;
		public static final int EMCM_UOW_REQ_SWITCH_ROWS_SIGN = 1;
		public static final int EMCM_UOW_REQ_SWITCH_ROWS = 6;
		public static final int EMCM_UOW_RESP_HDR_ROWS_SIGN = 1;
		public static final int EMCM_UOW_RESP_HDR_ROWS = 6;
		public static final int EMCM_UOW_RESP_DATA_ROWS_SIGN = 1;
		public static final int EMCM_UOW_RESP_DATA_ROWS = 6;
		public static final int EMCM_UOW_RESP_WARN_ROWS_SIGN = 1;
		public static final int EMCM_UOW_RESP_WARN_ROWS = 6;
		public static final int EMCM_UOW_RESP_NLBE_ROWS_SIGN = 1;
		public static final int EMCM_UOW_RESP_NLBE_ROWS = 6;
		public static final int EMCM_UOW_PROC_HDR_ROWS_SIGN = 1;
		public static final int EMCM_UOW_PROC_HDR_ROWS = 6;
		public static final int EMCM_UOW_PROC_DATA_ROWS_SIGN = 1;
		public static final int EMCM_UOW_PROC_DATA_ROWS = 6;
		public static final int EMCM_UOW_PROC_WARN_ROWS_SIGN = 1;
		public static final int EMCM_UOW_PROC_WARN_ROWS = 6;
		public static final int EMCM_UOW_PROC_NLBE_ROWS_SIGN = 1;
		public static final int EMCM_UOW_PROC_NLBE_ROWS = 6;
		public static final int EMCM_LOCK_TYPE = 10;
		public static final int EMCM_UOW_LOCK_ACTION_CD = 1;
		public static final int EMCM_FLDR_LOKS_DEL_TIME_SIGN = 1;
		public static final int EMCM_FOLDER_LOKS_DEL_TIME = 10;
		public static final int EMCM_LOKS_UOW_CONTINUE_SW = 1;
		public static final int EMCM_APPLY_DATA_PRIVACY_SW = 1;
		public static final int EMCM_SECURITY_CONTEXT_NI = 1;
		public static final int EMCM_SECURITY_CONTEXT = 20;
		public static final int EMCM_SEC_AUT_NBR_SIGN = 1;
		public static final int EMCM_SEC_AUT_NBR = 5;
		public static final int EMCM_SEC_ASSOC_TYPE_NI = 1;
		public static final int EMCM_SEC_ASSOCIATION_TYPE = 8;
		public static final int EMCM_DATA_PRIVACY_RETCODE_NI = 1;
		public static final int EMCM_DATA_PRIVACY_RETCODE = 10;
		public static final int EMCM_APPLY_AUDITS_SW = 1;
		public static final int EMCM_AUDT_BUS_OBJ_NM_NI = 1;
		public static final int EMCM_AUDT_BUS_OBJ_NM = 32;
		public static final int EMCM_AUDT_EVENT_DATA_NI = 1;
		public static final int EMCM_AUDT_EVENT_DATA = 100;
		public static final int EMCM_HAL_EMCM_KEEP_CLR = 1;
		public static final int EMDR_MDRV_LVL_GUID = 32;
		public static final int EMDR_MDRV_LVL_ERR_TIMESTAMP = 26;
		public static final int EMDR_TERMINAL_ID = 8;
		public static final int EMDR_MSG_TRANSFER_CD = 1;
		public static final int EMDR_CURRENT_CHUNK_SIGN = 1;
		public static final int EMDR_CURRENT_CHUNK = 5;
		public static final int EMDR_DEL_STORE_SW = 1;
		public static final int EMDR_RETURN_WARNS_SW = 1;
		public static final int EMDR_TOTAL_MSG_LENGTH_SIGN = 1;
		public static final int EMDR_TOTAL_MSG_LENGTH = 10;
		public static final int EMDR_CURRENT_BUFFER_LEN_SIGN = 1;
		public static final int EMDR_CURRENT_BUFFER_LENGTH = 10;
		public static final int EMDR_TOTAL_NBR_CHUNKS_SIGN = 1;
		public static final int EMDR_TOTAL_NBR_CHUNKS = 5;
		public static final int ESTO_DETAIL_BUFFER = 1100;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int EFAL_DB2_ERR_SQLCODE = 10;
			public static final int EFAL_CICS_ERR_RESP = 10;
			public static final int EFAL_CICS_ERR_RESP2 = 10;
			public static final int EFAL_SEC_SYS_ID = 10;
			public static final int EFAL_ETRA_PRIORITY_LEVEL = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
