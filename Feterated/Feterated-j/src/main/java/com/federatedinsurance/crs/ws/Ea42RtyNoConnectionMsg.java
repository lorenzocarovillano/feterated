/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-42-RTY-NO-CONNECTION-MSG<br>
 * Variable: EA-42-RTY-NO-CONNECTION-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea42RtyNoConnectionMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-1
	private String flr2 = "UNABLE TO";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-2
	private String flr3 = "CONNECT TO";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-3
	private String flr4 = "TARGETED CICS";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-4
	private String flr5 = "SESSIONS.";
	//Original name: FILLER-EA-42-RTY-NO-CONNECTION-MSG-5
	private String flr6 = "REGIONS TRIED:";
	//Original name: EA-42-REGIONS-ATTEMPTED
	private String ea42RegionsAttempted = DefaultValues.stringVal(Len.EA42_REGIONS_ATTEMPTED);

	//==== METHODS ====
	public String getEa42RtyNoConnectionMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa42RtyNoConnectionMsgBytes());
	}

	public byte[] getEa42RtyNoConnectionMsgBytes() {
		byte[] buffer = new byte[Len.EA42_RTY_NO_CONNECTION_MSG];
		return getEa42RtyNoConnectionMsgBytes(buffer, 1);
	}

	public byte[] getEa42RtyNoConnectionMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, ea42RegionsAttempted, Len.EA42_REGIONS_ATTEMPTED);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setEa42RegionsAttempted(String ea42RegionsAttempted) {
		this.ea42RegionsAttempted = Functions.subString(ea42RegionsAttempted, Len.EA42_REGIONS_ATTEMPTED);
	}

	public String getEa42RegionsAttempted() {
		return this.ea42RegionsAttempted;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA42_REGIONS_ATTEMPTED = 30;
		public static final int FLR1 = 11;
		public static final int FLR2 = 10;
		public static final int FLR4 = 14;
		public static final int FLR6 = 15;
		public static final int EA42_RTY_NO_CONNECTION_MSG = EA42_REGIONS_ATTEMPTED + 3 * FLR1 + FLR2 + FLR4 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
