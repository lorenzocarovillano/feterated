/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-PRINT-LINE<br>
 * Variable: L-PRINT-LINE from program TS030099<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LPrintLine extends BytesClass {

	//==== PROPERTIES ====
	public static final char NO_DIVISION_PASSED = Types.SPACE_CHAR;
	public static final char NO_SUBDIVISION_PASSED = Types.SPACE_CHAR;
	public static final String PRINT_PRTR = "  PRTR";
	public static final String NO_LINES_PER_PAGE_PASSED = "";
	public static final char NO_CARRIAGE_CONTRL_PASSED = Types.SPACE_CHAR;
	public static final char SUPPRESS_SPACE = '+';
	public static final char TOP_OF_PAGE = '0';
	public static final char SINGLE_SPACE = '1';
	public static final char DOUBLE_SPACE = '2';
	public static final char TRIPLE_SPACE = '3';
	public static final char CHANNEL1_CC = 'A';
	public static final char CHANNEL2_CC = 'B';
	public static final char CHANNEL3_CC = 'C';
	public static final char CHANNEL4_CC = 'D';
	public static final char CHANNEL5_CC = 'E';
	public static final char CHANNEL6_CC = 'F';
	public static final char CHANNEL7_CC = 'G';
	public static final char CHANNEL8_CC = 'H';
	public static final char CHANNEL9_CC = 'I';
	public static final char CHANNEL10_CC = 'J';
	public static final char CHANNEL11_CC = 'K';
	public static final char CHANNEL12_CC = 'L';
	public static final char STRUCTURE_RECORD = '!';
	public static final char NO_BEFORE_OR_AFTER_PASSED = Types.SPACE_CHAR;
	public static final char OPEN_FUNCTION = 'F';
	public static final char CLOSE_FUNCTION = 'L';
	public static final char INQUIRY_FUNCTION = 'I';
	public static final char BEFORE = 'B';
	public static final char AFTER = 'A';
	public static final char STRUCT_FIELD_RECORD = 'S';

	//==== CONSTRUCTORS ====
	public LPrintLine() {
	}

	public LPrintLine(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_PRINT_LINE;
	}

	public byte[] getlPrintLineBytes() {
		byte[] buffer = new byte[Len.L_PRINT_LINE];
		return getlPrintLineBytes(buffer, 1);
	}

	public byte[] getlPrintLineBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.L_PRINT_LINE, Pos.L_PRINT_LINE);
		return buffer;
	}

	public String getOfficeLocationFormatted() {
		return readFixedString(Pos.OFFICE_LOCATION, Len.OFFICE_LOCATION);
	}

	/**Original name: L-PL-OFFICE-LOCATION<br>*/
	public byte[] getOfficeLocationBytes() {
		byte[] buffer = new byte[Len.OFFICE_LOCATION];
		return getOfficeLocationBytes(buffer, 1);
	}

	public byte[] getOfficeLocationBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.OFFICE_LOCATION, Pos.OFFICE_LOCATION);
		return buffer;
	}

	public void setDivision(char division) {
		writeChar(Pos.DIVISION, division);
	}

	/**Original name: L-PL-DIVISION<br>*/
	public char getDivision() {
		return readChar(Pos.DIVISION);
	}

	public boolean isNoDivisionPassed() {
		return getDivision() == NO_DIVISION_PASSED;
	}

	public void setSubdivision(char subdivision) {
		writeChar(Pos.SUBDIVISION, subdivision);
	}

	/**Original name: L-PL-SUBDIVISION<br>*/
	public char getSubdivision() {
		return readChar(Pos.SUBDIVISION);
	}

	public boolean isNoSubdivisionPassed() {
		return getSubdivision() == NO_SUBDIVISION_PASSED;
	}

	/**Original name: L-PL-REPORT-NUMBER<br>*/
	public String getReportNumber() {
		return readString(Pos.REPORT_NUMBER, Len.REPORT_NUMBER);
	}

	public boolean isPrintPrtr() {
		return getReportNumber().equals(PRINT_PRTR);
	}

	public void setLinesPerPage(String linesPerPage) {
		writeString(Pos.LINES_PER_PAGE, linesPerPage, Len.LINES_PER_PAGE);
	}

	/**Original name: L-PL-LINES-PER-PAGE<br>*/
	public String getLinesPerPage() {
		return readString(Pos.LINES_PER_PAGE, Len.LINES_PER_PAGE);
	}

	public boolean isNoLinesPerPagePassed() {
		return getLinesPerPage().equals(NO_LINES_PER_PAGE_PASSED);
	}

	/**Original name: L-PL-CARRIAGE-CONTROL<br>*/
	public char getCarriageControl() {
		return readChar(Pos.CARRIAGE_CONTROL);
	}

	public boolean isStructureRecord() {
		return getCarriageControl() == STRUCTURE_RECORD;
	}

	public void setBeforeOrAfter(char beforeOrAfter) {
		writeChar(Pos.BEFORE_OR_AFTER, beforeOrAfter);
	}

	/**Original name: L-PL-BEFORE-OR-AFTER<br>*/
	public char getBeforeOrAfter() {
		return readChar(Pos.BEFORE_OR_AFTER);
	}

	public boolean isNoBeforeOrAfterPassed() {
		return getBeforeOrAfter() == NO_BEFORE_OR_AFTER_PASSED;
	}

	public boolean isOpenFunction() {
		return getBeforeOrAfter() == OPEN_FUNCTION;
	}

	public boolean isCloseFunction() {
		return getBeforeOrAfter() == CLOSE_FUNCTION;
	}

	public boolean isInquiryFunction() {
		return getBeforeOrAfter() == INQUIRY_FUNCTION;
	}

	public boolean isBefore() {
		return getBeforeOrAfter() == BEFORE;
	}

	public boolean isStructFieldRecord() {
		return getBeforeOrAfter() == STRUCT_FIELD_RECORD;
	}

	public String getData2Formatted() {
		return readFixedString(Pos.DATA2, Len.DATA2);
	}

	public void setData198BytesFormatted(String data) {
		writeString(Pos.DATA198_BYTES, data, Len.DATA198_BYTES);
	}

	public String getData198BytesFormatted() {
		return readFixedString(Pos.DATA198_BYTES, Len.DATA198_BYTES);
	}

	public void initData198BytesSpaces() {
		fill(Pos.DATA198_BYTES, Len.DATA198_BYTES, Types.SPACE_CHAR);
	}

	public void setData132BytesFormatted(String data) {
		writeString(Pos.DATA132_BYTES, data, Len.DATA132_BYTES);
	}

	public String getData132BytesFormatted() {
		return readFixedString(Pos.DATA132_BYTES, Len.DATA132_BYTES);
	}

	public void initData132BytesSpaces() {
		fill(Pos.DATA132_BYTES, Len.DATA132_BYTES, Types.SPACE_CHAR);
	}

	public void setData131Bytes(String data131Bytes) {
		writeString(Pos.DATA131_BYTES, data131Bytes, Len.DATA131_BYTES);
	}

	/**Original name: L-PL-DATA-131-BYTES<br>*/
	public String getData131Bytes() {
		return readString(Pos.DATA131_BYTES, Len.DATA131_BYTES);
	}

	public String getData131BytesFormatted() {
		return Functions.padBlanks(getData131Bytes(), Len.DATA131_BYTES);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_PRINT_LINE = 1;
		public static final int HEADING = L_PRINT_LINE;
		public static final int OFFICE_LOCATION = HEADING;
		public static final int DIVISION = OFFICE_LOCATION;
		public static final int SUBDIVISION = DIVISION + Len.DIVISION;
		public static final int REPORT_NUMBER = SUBDIVISION + Len.SUBDIVISION;
		public static final int LINES_PER_PAGE = REPORT_NUMBER + Len.REPORT_NUMBER;
		public static final int CARRIAGE_CONTROL = LINES_PER_PAGE + Len.LINES_PER_PAGE;
		public static final int BEFORE_OR_AFTER = CARRIAGE_CONTROL + Len.CARRIAGE_CONTROL;
		public static final int DATA2 = BEFORE_OR_AFTER + Len.BEFORE_OR_AFTER;
		public static final int DATA198_BYTES = DATA2;
		public static final int DATA132_BYTES = DATA198_BYTES;
		public static final int DATA131_BYTES = DATA132_BYTES;
		public static final int FLR1 = DATA131_BYTES + Len.DATA131_BYTES;
		public static final int FLR2 = FLR1 + Len.FLR1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int DIVISION = 1;
		public static final int SUBDIVISION = 1;
		public static final int REPORT_NUMBER = 6;
		public static final int LINES_PER_PAGE = 2;
		public static final int CARRIAGE_CONTROL = 1;
		public static final int BEFORE_OR_AFTER = 1;
		public static final int DATA131_BYTES = 131;
		public static final int FLR1 = 1;
		public static final int OFFICE_LOCATION = DIVISION + SUBDIVISION;
		public static final int HEADING = OFFICE_LOCATION + REPORT_NUMBER + LINES_PER_PAGE + CARRIAGE_CONTROL + BEFORE_OR_AFTER;
		public static final int DATA132_BYTES = DATA131_BYTES + FLR1;
		public static final int FLR2 = 66;
		public static final int DATA198_BYTES = DATA132_BYTES + FLR2;
		public static final int DATA2 = DATA198_BYTES;
		public static final int L_PRINT_LINE = HEADING + DATA2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
