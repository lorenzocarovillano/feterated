/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.ILegalEntityCodV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [LEGAL_ENTITY_COD_V]
 * 
 */
public class LegalEntityCodVDao extends BaseSqlDao<ILegalEntityCodV> {

	public LegalEntityCodVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ILegalEntityCodV> getToClass() {
		return ILegalEntityCodV.class;
	}

	public String selectByLegEntCd(String legEntCd, String dft) {
		return buildQuery("selectByLegEntCd").bind("legEntCd", legEntCd).scalarResultString(dft);
	}
}
