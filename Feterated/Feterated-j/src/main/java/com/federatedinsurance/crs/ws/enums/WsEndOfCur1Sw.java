/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-END-OF-CUR1-SW<br>
 * Variable: WS-END-OF-CUR1-SW from program HALOUIEH<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEndOfCur1Sw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_OF_CUR1 = 'Y';
	public static final char NOT_END_OF_CUR1 = 'N';

	//==== METHODS ====
	public void setWsEndOfCur1Sw(char wsEndOfCur1Sw) {
		this.value = wsEndOfCur1Sw;
	}

	public char getWsEndOfCur1Sw() {
		return this.value;
	}

	public boolean isEndOfCur1() {
		return value == END_OF_CUR1;
	}

	public void setEndOfCur1() {
		value = END_OF_CUR1;
	}

	public void setNotEndOfCur1() {
		value = NOT_END_OF_CUR1;
	}
}
