/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [SYSDUMMY1]
 * 
 */
public interface ISysdummy1 extends BaseSqlTo {

	/**
	 * Host Variable SA-CURRENT-DT
	 * 
	 */
	String getCurrentDt();

	void setCurrentDt(String currentDt);

	/**
	 * Host Variable SA-ONE-YEAR-AGO-DT
	 * 
	 */
	String getOneYearAgoDt();

	void setOneYearAgoDt(String oneYearAgoDt);
};
