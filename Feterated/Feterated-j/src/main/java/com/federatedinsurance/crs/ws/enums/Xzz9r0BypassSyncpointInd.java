/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: XZZ9R0-BYPASS-SYNCPOINT-IND<br>
 * Variable: XZZ9R0-BYPASS-SYNCPOINT-IND from copybook XZ0Z90R0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xzz9r0BypassSyncpointInd {

	//==== PROPERTIES ====
	private char value = 'N';
	public static final char BYPASS_SYNCPOINT = 'Y';
	public static final char DO_NOT_BYPASS_SYNCPOINT = 'N';

	//==== METHODS ====
	public void setXzz9r0BypassSyncpointInd(char xzz9r0BypassSyncpointInd) {
		this.value = xzz9r0BypassSyncpointInd;
	}

	public char getXzz9r0BypassSyncpointInd() {
		return this.value;
	}

	public boolean isBypassSyncpoint() {
		return value == BYPASS_SYNCPOINT;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZZ9R0_BYPASS_SYNCPOINT_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
