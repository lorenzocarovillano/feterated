/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.Xzc02oPolKeys;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.occurs.Xzc020NonLoggableErrors;
import com.federatedinsurance.crs.ws.occurs.Xzc020Warnings;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program XZC02090<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaXzc02090 extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int XZC020_NON_LOGGABLE_ERRORS_MAXOCCURS = 10;
	public static final int XZC020_WARNINGS_MAXOCCURS = 10;
	//Original name: XZC02I-POL-NBR
	private String iPolNbr = DefaultValues.stringVal(Len.I_POL_NBR);
	//Original name: XZC02I-POL-EFF-DT
	private String iPolEffDt = DefaultValues.stringVal(Len.I_POL_EFF_DT);
	//Original name: XZC02I-POL-EXP-DT
	private String iPolExpDt = DefaultValues.stringVal(Len.I_POL_EXP_DT);
	//Original name: XZC02I-USERID
	private String iUserid = DefaultValues.stringVal(Len.I_USERID);
	//Original name: FILLER-XZC020-SERVICE-INPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: XZC02O-POL-KEYS
	private Xzc02oPolKeys oPolKeys = new Xzc02oPolKeys();
	//Original name: XZC02O-UW-TMN-FLG
	private char oUwTmnFlg = DefaultValues.CHAR_VAL;
	//Original name: XZC02O-TMN-EMP-ID
	private String oTmnEmpId = DefaultValues.stringVal(Len.O_TMN_EMP_ID);
	//Original name: XZC02O-TMN-EMP-NM
	private String oTmnEmpNm = DefaultValues.stringVal(Len.O_TMN_EMP_NM);
	//Original name: XZC02O-TMN-PRC-DT
	private String oTmnPrcDt = DefaultValues.stringVal(Len.O_TMN_PRC_DT);
	//Original name: FILLER-XZC020-SERVICE-OUTPUTS
	private String flr2 = DefaultValues.stringVal(Len.FLR2);
	//Original name: XZC020-ERROR-RETURN-CODE
	private DsdErrorReturnCode xzc020ErrorReturnCode = new DsdErrorReturnCode();
	//Original name: XZC020-FATAL-ERROR-MESSAGE
	private String xzc020FatalErrorMessage = DefaultValues.stringVal(Len.XZC020_FATAL_ERROR_MESSAGE);
	//Original name: XZC020-NON-LOGGABLE-ERROR-CNT
	private String xzc020NonLoggableErrorCnt = DefaultValues.stringVal(Len.XZC020_NON_LOGGABLE_ERROR_CNT);
	//Original name: XZC020-NON-LOGGABLE-ERRORS
	private Xzc020NonLoggableErrors[] xzc020NonLoggableErrors = new Xzc020NonLoggableErrors[XZC020_NON_LOGGABLE_ERRORS_MAXOCCURS];
	//Original name: XZC020-WARNING-CNT
	private String xzc020WarningCnt = DefaultValues.stringVal(Len.XZC020_WARNING_CNT);
	//Original name: XZC020-WARNINGS
	private Xzc020Warnings[] xzc020Warnings = new Xzc020Warnings[XZC020_WARNINGS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public DfhcommareaXzc02090() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void init() {
		for (int xzc020NonLoggableErrorsIdx = 1; xzc020NonLoggableErrorsIdx <= XZC020_NON_LOGGABLE_ERRORS_MAXOCCURS; xzc020NonLoggableErrorsIdx++) {
			xzc020NonLoggableErrors[xzc020NonLoggableErrorsIdx - 1] = new Xzc020NonLoggableErrors();
		}
		for (int xzc020WarningsIdx = 1; xzc020WarningsIdx <= XZC020_WARNINGS_MAXOCCURS; xzc020WarningsIdx++) {
			xzc020Warnings[xzc020WarningsIdx - 1] = new Xzc020Warnings();
		}
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		setXzc020ServiceInputsBytes(buffer, position);
		position += Len.XZC020_SERVICE_INPUTS;
		setXzc020ServiceOutputsBytes(buffer, position);
		position += Len.XZC020_SERVICE_OUTPUTS;
		setXzc020ErrorHandlingParmsBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		getXzc020ServiceInputsBytes(buffer, position);
		position += Len.XZC020_SERVICE_INPUTS;
		getXzc020ServiceOutputsBytes(buffer, position);
		position += Len.XZC020_SERVICE_OUTPUTS;
		getXzc020ErrorHandlingParmsBytes(buffer, position);
		return buffer;
	}

	public void setXzc020ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		iPolNbr = MarshalByte.readString(buffer, position, Len.I_POL_NBR);
		position += Len.I_POL_NBR;
		iPolEffDt = MarshalByte.readString(buffer, position, Len.I_POL_EFF_DT);
		position += Len.I_POL_EFF_DT;
		iPolExpDt = MarshalByte.readString(buffer, position, Len.I_POL_EXP_DT);
		position += Len.I_POL_EXP_DT;
		iUserid = MarshalByte.readString(buffer, position, Len.I_USERID);
		position += Len.I_USERID;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzc020ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, iPolNbr, Len.I_POL_NBR);
		position += Len.I_POL_NBR;
		MarshalByte.writeString(buffer, position, iPolEffDt, Len.I_POL_EFF_DT);
		position += Len.I_POL_EFF_DT;
		MarshalByte.writeString(buffer, position, iPolExpDt, Len.I_POL_EXP_DT);
		position += Len.I_POL_EXP_DT;
		MarshalByte.writeString(buffer, position, iUserid, Len.I_USERID);
		position += Len.I_USERID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setiPolNbr(String iPolNbr) {
		this.iPolNbr = Functions.subString(iPolNbr, Len.I_POL_NBR);
	}

	public String getiPolNbr() {
		return this.iPolNbr;
	}

	public void setiPolEffDt(String iPolEffDt) {
		this.iPolEffDt = Functions.subString(iPolEffDt, Len.I_POL_EFF_DT);
	}

	public String getiPolEffDt() {
		return this.iPolEffDt;
	}

	public void setiPolExpDt(String iPolExpDt) {
		this.iPolExpDt = Functions.subString(iPolExpDt, Len.I_POL_EXP_DT);
	}

	public String getiPolExpDt() {
		return this.iPolExpDt;
	}

	public void setiUserid(String iUserid) {
		this.iUserid = Functions.subString(iUserid, Len.I_USERID);
	}

	public String getiUserid() {
		return this.iUserid;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setXzc020ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		oPolKeys.setoPolKeysBytes(buffer, position);
		position += Xzc02oPolKeys.Len.O_POL_KEYS;
		oUwTmnFlg = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		oTmnEmpId = MarshalByte.readString(buffer, position, Len.O_TMN_EMP_ID);
		position += Len.O_TMN_EMP_ID;
		oTmnEmpNm = MarshalByte.readString(buffer, position, Len.O_TMN_EMP_NM);
		position += Len.O_TMN_EMP_NM;
		oTmnPrcDt = MarshalByte.readString(buffer, position, Len.O_TMN_PRC_DT);
		position += Len.O_TMN_PRC_DT;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR2);
	}

	public byte[] getXzc020ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		oPolKeys.getoPolKeysBytes(buffer, position);
		position += Xzc02oPolKeys.Len.O_POL_KEYS;
		MarshalByte.writeChar(buffer, position, oUwTmnFlg);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, oTmnEmpId, Len.O_TMN_EMP_ID);
		position += Len.O_TMN_EMP_ID;
		MarshalByte.writeString(buffer, position, oTmnEmpNm, Len.O_TMN_EMP_NM);
		position += Len.O_TMN_EMP_NM;
		MarshalByte.writeString(buffer, position, oTmnPrcDt, Len.O_TMN_PRC_DT);
		position += Len.O_TMN_PRC_DT;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void setoUwTmnFlg(char oUwTmnFlg) {
		this.oUwTmnFlg = oUwTmnFlg;
	}

	public char getoUwTmnFlg() {
		return this.oUwTmnFlg;
	}

	public void setoTmnEmpId(String oTmnEmpId) {
		this.oTmnEmpId = Functions.subString(oTmnEmpId, Len.O_TMN_EMP_ID);
	}

	public String getoTmnEmpId() {
		return this.oTmnEmpId;
	}

	public void setoTmnEmpNm(String oTmnEmpNm) {
		this.oTmnEmpNm = Functions.subString(oTmnEmpNm, Len.O_TMN_EMP_NM);
	}

	public String getoTmnEmpNm() {
		return this.oTmnEmpNm;
	}

	public void setoTmnPrcDt(String oTmnPrcDt) {
		this.oTmnPrcDt = Functions.subString(oTmnPrcDt, Len.O_TMN_PRC_DT);
	}

	public String getoTmnPrcDt() {
		return this.oTmnPrcDt;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR2);
	}

	public String getFlr2() {
		return this.flr2;
	}

	/**Original name: XZC020-ERROR-HANDLING-PARMS<br>
	 * <pre>* ERROR HANDLING</pre>*/
	public byte[] getXzc020ErrorHandlingParmsBytes() {
		byte[] buffer = new byte[Len.XZC020_ERROR_HANDLING_PARMS];
		return getXzc020ErrorHandlingParmsBytes(buffer, 1);
	}

	public void setXzc020ErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc020ErrorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		xzc020FatalErrorMessage = MarshalByte.readString(buffer, position, Len.XZC020_FATAL_ERROR_MESSAGE);
		position += Len.XZC020_FATAL_ERROR_MESSAGE;
		xzc020NonLoggableErrorCnt = MarshalByte.readFixedString(buffer, position, Len.XZC020_NON_LOGGABLE_ERROR_CNT);
		position += Len.XZC020_NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= XZC020_NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				xzc020NonLoggableErrors[idx - 1].setXzc020NonLoggableErrorsBytes(buffer, position);
				position += Xzc020NonLoggableErrors.Len.XZC020_NON_LOGGABLE_ERRORS;
			} else {
				xzc020NonLoggableErrors[idx - 1].initXzc020NonLoggableErrorsSpaces();
				position += Xzc020NonLoggableErrors.Len.XZC020_NON_LOGGABLE_ERRORS;
			}
		}
		xzc020WarningCnt = MarshalByte.readFixedString(buffer, position, Len.XZC020_WARNING_CNT);
		position += Len.XZC020_WARNING_CNT;
		for (int idx = 1; idx <= XZC020_WARNINGS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				xzc020Warnings[idx - 1].setXzc020WarningsBytes(buffer, position);
				position += Xzc020Warnings.Len.XZC020_WARNINGS;
			} else {
				xzc020Warnings[idx - 1].initXzc020WarningsSpaces();
				position += Xzc020Warnings.Len.XZC020_WARNINGS;
			}
		}
	}

	public byte[] getXzc020ErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc020ErrorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, xzc020FatalErrorMessage, Len.XZC020_FATAL_ERROR_MESSAGE);
		position += Len.XZC020_FATAL_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, xzc020NonLoggableErrorCnt, Len.XZC020_NON_LOGGABLE_ERROR_CNT);
		position += Len.XZC020_NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= XZC020_NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			xzc020NonLoggableErrors[idx - 1].getXzc020NonLoggableErrorsBytes(buffer, position);
			position += Xzc020NonLoggableErrors.Len.XZC020_NON_LOGGABLE_ERRORS;
		}
		MarshalByte.writeString(buffer, position, xzc020WarningCnt, Len.XZC020_WARNING_CNT);
		position += Len.XZC020_WARNING_CNT;
		for (int idx = 1; idx <= XZC020_WARNINGS_MAXOCCURS; idx++) {
			xzc020Warnings[idx - 1].getXzc020WarningsBytes(buffer, position);
			position += Xzc020Warnings.Len.XZC020_WARNINGS;
		}
		return buffer;
	}

	public void setXzc020FatalErrorMessage(String xzc020FatalErrorMessage) {
		this.xzc020FatalErrorMessage = Functions.subString(xzc020FatalErrorMessage, Len.XZC020_FATAL_ERROR_MESSAGE);
	}

	public String getXzc020FatalErrorMessage() {
		return this.xzc020FatalErrorMessage;
	}

	public void setXzc020NonLoggableErrorCntFormatted(String xzc020NonLoggableErrorCnt) {
		this.xzc020NonLoggableErrorCnt = Trunc.toUnsignedNumeric(xzc020NonLoggableErrorCnt, Len.XZC020_NON_LOGGABLE_ERROR_CNT);
	}

	public short getXzc020NonLoggableErrorCnt() {
		return NumericDisplay.asShort(this.xzc020NonLoggableErrorCnt);
	}

	public void setXzc020WarningCntFormatted(String xzc020WarningCnt) {
		this.xzc020WarningCnt = Trunc.toUnsignedNumeric(xzc020WarningCnt, Len.XZC020_WARNING_CNT);
	}

	public short getXzc020WarningCnt() {
		return NumericDisplay.asShort(this.xzc020WarningCnt);
	}

	public DsdErrorReturnCode getXzc020ErrorReturnCode() {
		return xzc020ErrorReturnCode;
	}

	public Xzc020NonLoggableErrors getXzc020NonLoggableErrors(int idx) {
		return xzc020NonLoggableErrors[idx - 1];
	}

	public Xzc020Warnings getXzc020Warnings(int idx) {
		return xzc020Warnings[idx - 1];
	}

	public Xzc02oPolKeys getoPolKeys() {
		return oPolKeys;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int I_POL_NBR = 9;
		public static final int I_POL_EFF_DT = 10;
		public static final int I_POL_EXP_DT = 10;
		public static final int I_USERID = 8;
		public static final int FLR1 = 100;
		public static final int XZC020_SERVICE_INPUTS = I_POL_NBR + I_POL_EFF_DT + I_POL_EXP_DT + I_USERID + FLR1;
		public static final int O_UW_TMN_FLG = 1;
		public static final int O_TMN_EMP_ID = 6;
		public static final int O_TMN_EMP_NM = 67;
		public static final int O_TMN_PRC_DT = 10;
		public static final int FLR2 = 400;
		public static final int XZC020_SERVICE_OUTPUTS = Xzc02oPolKeys.Len.O_POL_KEYS + O_UW_TMN_FLG + O_TMN_EMP_ID + O_TMN_EMP_NM + O_TMN_PRC_DT
				+ FLR2;
		public static final int XZC020_FATAL_ERROR_MESSAGE = 250;
		public static final int XZC020_NON_LOGGABLE_ERROR_CNT = 4;
		public static final int XZC020_WARNING_CNT = 4;
		public static final int XZC020_ERROR_HANDLING_PARMS = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + XZC020_FATAL_ERROR_MESSAGE
				+ XZC020_NON_LOGGABLE_ERROR_CNT
				+ DfhcommareaXzc02090.XZC020_NON_LOGGABLE_ERRORS_MAXOCCURS * Xzc020NonLoggableErrors.Len.XZC020_NON_LOGGABLE_ERRORS
				+ XZC020_WARNING_CNT + DfhcommareaXzc02090.XZC020_WARNINGS_MAXOCCURS * Xzc020Warnings.Len.XZC020_WARNINGS;
		public static final int DFHCOMMAREA = XZC020_SERVICE_INPUTS + XZC020_SERVICE_OUTPUTS + XZC020_ERROR_HANDLING_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
