/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UBOC-APPLY-DATA-PRIVACY-SW<br>
 * Variable: UBOC-APPLY-DATA-PRIVACY-SW from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocApplyDataPrivacySw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char APPLY_DATA_PRIV = 'Y';
	public static final char NO_DATA_PRIVACY = 'N';

	//==== METHODS ====
	public void setApplyDataPrivacySw(char applyDataPrivacySw) {
		this.value = applyDataPrivacySw;
	}

	public char getApplyDataPrivacySw() {
		return this.value;
	}

	public boolean isUbocApplyDataPriv() {
		return value == APPLY_DATA_PRIV;
	}

	public void setUbocApplyDataPriv() {
		value = APPLY_DATA_PRIV;
	}

	public boolean isUbocNoDataPrivacy() {
		return value == NO_DATA_PRIVACY;
	}

	public void setUbocNoDataPrivacy() {
		value = NO_DATA_PRIVACY;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int APPLY_DATA_PRIVACY_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
