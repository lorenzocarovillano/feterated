/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-63-USR-PIPE-NOT-CLOSED-MSG<br>
 * Variable: EA-63-USR-PIPE-NOT-CLOSED-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea63UsrPipeNotClosedMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-63-USR-PIPE-NOT-CLOSED-MSG
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-63-USR-PIPE-NOT-CLOSED-MSG-1
	private String flr2 = "PIPE IS NOT";
	//Original name: FILLER-EA-63-USR-PIPE-NOT-CLOSED-MSG-2
	private String flr3 = "CLOSED.";

	//==== METHODS ====
	public String getEa63UsrPipeNotClosedMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa63UsrPipeNotClosedMsgBytes());
	}

	public byte[] getEa63UsrPipeNotClosedMsgBytes() {
		byte[] buffer = new byte[Len.EA63_USR_PIPE_NOT_CLOSED_MSG];
		return getEa63UsrPipeNotClosedMsgBytes(buffer, 1);
	}

	public byte[] getEa63UsrPipeNotClosedMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 12;
		public static final int FLR3 = 7;
		public static final int EA63_USR_PIPE_NOT_CLOSED_MSG = FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
