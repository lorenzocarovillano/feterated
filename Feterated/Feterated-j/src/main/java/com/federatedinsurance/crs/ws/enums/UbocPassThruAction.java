/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: UBOC-PASS-THRU-ACTION<br>
 * Variable: UBOC-PASS-THRU-ACTION from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocPassThruAction {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.UBOC_PASS_THRU_ACTION);
	public static final String NO_PASS_THRU_ACTION = "";
	public static final String FETCH_DATA_REQUEST = "FETCH";
	public static final String FETCH_ALL_DATA_REQUEST = "FETCH_ALL";
	public static final String FETCH_AND_SKIP_HIER_REQUEST = "FETCH_SKIP_HIER";
	public static final String FETCH_PENDING_DATA_REQUEST = "FETCH_PENDING";
	public static final String FETCH_WITH_EXACT_KEY_REQUEST = "FETCH_EXACT_KEY";
	public static final String FETCH_WITH_EXACT_KEY_ISS_REQ = "FETCH_EXACT_ISS";
	public static final String FETCH_ISSUED_ROWS_ONLY_REQ = "FETCH_ISSUED_ROWS";
	public static final String FETCH_WITH_EXACT_KEY_PND_REQ = "FETCH_EXT_KEY_PND";
	public static final String FETCH_PRIORITY = "FETCH_PRIORITY";
	public static final String FETCH_AS_OF_NO_ADJ_REQ = "FETCH_AS_OF_NO_ADJ";
	public static final String UPDATE_DATA_REQUEST = "UPDATE";
	public static final String UPDATE_AND_RETURN_DATA_REQUEST = "UPDATE_&_RETURN";
	public static final String INSERT_DATA_REQUEST = "INSERT";
	public static final String INSERT_AND_RETURN_DATA_REQUEST = "INSERT_&_RETURN";
	public static final String CHANGE_DATA_REQUEST = "CHANGE";
	public static final String CHANGE_AND_RETURN_DATA_REQUEST = "CHANGE_&_RETURN";
	public static final String CORRECT_DATA_REQUEST = "CORRECT";
	public static final String CORRECT_AND_RETURN_DATA_REQ = "CORRECT_&_RETURN";
	public static final String REPLACE_AND_RETURN_REQUEST = "REPLACE_&_RETURN";
	public static final String DELETE_DATA_REQUEST = "DELETE";
	public static final String CASCADING_DELETE = "CASCADING_DELETE";
	public static final String FILTER_ONLY_REQUEST = "FILTER";
	public static final String SIMPLE_EDIT_AND_PRIM_KEYS = "SIMPLE_EDIT";
	public static final String BYPASS_SIMPLE_EDIT_STEP = "BYPASS_SIMPLE_EDITS";
	public static final String IGNORE_REQUEST = "IGNORE";
	public static final String IGNORE_AND_RETURN_REQUEST = "IGNORE_&_RETURN";
	public static final String SECURITY_RETRIEVAL_REQUEST = "SEC_RETRIEVAL";
	public static final String INFRA_UNATTENDED_LOCK_PURGE = "UNATTD_LOCK_PURGE";
	public static final String ACTIVATE_PENDING_ROWS_REQUEST = "ACTIVATE_PENDING";
	public static final String FETCH_ALL_QUOTES = "FETCH_ALL_QUOTES";
	public static final String SET_DEFAULTS_REQUEST = "SET_DEFAULTS";
	public static final String ACCEPT_QUOTE_REQUEST = "ACCEPT_QUOTE";
	public static final String PURGE_PENDING_REQUEST = "PURGE_PENDING";
	public static final String COPY_DATA_REQUEST = "COPY";
	public static final String SET_POLICY_OOS = "SET_POLICY_OOS";
	public static final String POLICY_CHANGE_REQUEST = "POLICY_CHANGE";
	public static final String REN_CHG_FETCH_EXACT_KEY = "RCHG_FETCH_EXCT";
	public static final String REN_CHG_FETCH_DATA_REQUEST = "RCHG_FETCH";
	public static final String REN_CHG_UPDATE_DATA_REQUEST = "RCHG_UPDATE";
	public static final String REN_CHG_DELETE_DATA_REQUEST = "RCHG_DELETE";
	public static final String AS_OF_FETCH_DATA_REQUEST = "AS_OF_FETCH";
	public static final String RESET_OOOS_TO_PREM = "RESET_OOOS";
	public static final String WRITE_GLOBAL_VALUES = "WRITE_GLOBAL";
	public static final String PURGE_OOS_REQUEST = "PURGE_OOS_REQUEST";

	//==== METHODS ====
	public void setUbocPassThruAction(String ubocPassThruAction) {
		this.value = Functions.subString(ubocPassThruAction, Len.UBOC_PASS_THRU_ACTION);
	}

	public String getUbocPassThruAction() {
		return this.value;
	}

	public String getActionCodeFormatted() {
		return Functions.padBlanks(getUbocPassThruAction(), Len.UBOC_PASS_THRU_ACTION);
	}

	public void setNoPassThruAction() {
		value = NO_PASS_THRU_ACTION;
	}

	public boolean isFetchDataRequest() {
		return value.equals(FETCH_DATA_REQUEST);
	}

	public void setFetchDataRequest() {
		value = FETCH_DATA_REQUEST;
	}

	public boolean isFetchAllDataRequest() {
		return value.equals(FETCH_ALL_DATA_REQUEST);
	}

	public void setFetchAllDataRequest() {
		value = FETCH_ALL_DATA_REQUEST;
	}

	public boolean isFetchAndSkipHierRequest() {
		return value.equals(FETCH_AND_SKIP_HIER_REQUEST);
	}

	public void setFetchAndSkipHierRequest() {
		value = FETCH_AND_SKIP_HIER_REQUEST;
	}

	public boolean isFetchPendingDataRequest() {
		return value.equals(FETCH_PENDING_DATA_REQUEST);
	}

	public void setFetchPendingDataRequest() {
		value = FETCH_PENDING_DATA_REQUEST;
	}

	public boolean isFetchWithExactKeyRequest() {
		return value.equals(FETCH_WITH_EXACT_KEY_REQUEST);
	}

	public void setFetchWithExactKeyRequest() {
		value = FETCH_WITH_EXACT_KEY_REQUEST;
	}

	public boolean isFetchWithExactKeyIssReq() {
		return value.equals(FETCH_WITH_EXACT_KEY_ISS_REQ);
	}

	public void setFetchWithExactKeyIssReq() {
		value = FETCH_WITH_EXACT_KEY_ISS_REQ;
	}

	public boolean isFetchIssuedRowsOnlyReq() {
		return value.equals(FETCH_ISSUED_ROWS_ONLY_REQ);
	}

	public void setFetchIssuedRowsOnlyReq() {
		value = FETCH_ISSUED_ROWS_ONLY_REQ;
	}

	public boolean isFetchWithExactKeyPndReq() {
		return value.equals(FETCH_WITH_EXACT_KEY_PND_REQ);
	}

	public void setFetchWithExactKeyPndReq() {
		value = FETCH_WITH_EXACT_KEY_PND_REQ;
	}

	public boolean isUpdateDataRequest() {
		return value.equals(UPDATE_DATA_REQUEST);
	}

	public void setUpdateDataRequest() {
		value = UPDATE_DATA_REQUEST;
	}

	public boolean isUpdateAndReturnDataRequest() {
		return value.equals(UPDATE_AND_RETURN_DATA_REQUEST);
	}

	public boolean isInsertDataRequest() {
		return value.equals(INSERT_DATA_REQUEST);
	}

	public void setInsertDataRequest() {
		value = INSERT_DATA_REQUEST;
	}

	public boolean isInsertAndReturnDataRequest() {
		return value.equals(INSERT_AND_RETURN_DATA_REQUEST);
	}

	public boolean isChangeDataRequest() {
		return value.equals(CHANGE_DATA_REQUEST);
	}

	public void setChangeDataRequest() {
		value = CHANGE_DATA_REQUEST;
	}

	public boolean isChangeAndReturnDataRequest() {
		return value.equals(CHANGE_AND_RETURN_DATA_REQUEST);
	}

	public boolean isCorrectDataRequest() {
		return value.equals(CORRECT_DATA_REQUEST);
	}

	public void setCorrectDataRequest() {
		value = CORRECT_DATA_REQUEST;
	}

	public boolean isCorrectAndReturnDataReq() {
		return value.equals(CORRECT_AND_RETURN_DATA_REQ);
	}

	public boolean isDeleteDataRequest() {
		return value.equals(DELETE_DATA_REQUEST);
	}

	public void setDeleteDataRequest() {
		value = DELETE_DATA_REQUEST;
	}

	public boolean isCascadingDelete() {
		return value.equals(CASCADING_DELETE);
	}

	public void setCascadingDelete() {
		value = CASCADING_DELETE;
	}

	public boolean isFilterOnlyRequest() {
		return value.equals(FILTER_ONLY_REQUEST);
	}

	public boolean isSimpleEditAndPrimKeys() {
		return value.equals(SIMPLE_EDIT_AND_PRIM_KEYS);
	}

	public void setSimpleEditAndPrimKeys() {
		value = SIMPLE_EDIT_AND_PRIM_KEYS;
	}

	public boolean isBypassSimpleEditStep() {
		return value.equals(BYPASS_SIMPLE_EDIT_STEP);
	}

	public void setBypassSimpleEditStep() {
		value = BYPASS_SIMPLE_EDIT_STEP;
	}

	public boolean isIgnoreRequest() {
		return value.equals(IGNORE_REQUEST);
	}

	public void setIgnoreRequest() {
		value = IGNORE_REQUEST;
	}

	public boolean isIgnoreAndReturnRequest() {
		return value.equals(IGNORE_AND_RETURN_REQUEST);
	}

	public void setIgnoreAndReturnRequest() {
		value = IGNORE_AND_RETURN_REQUEST;
	}

	public void setSecurityRetrievalRequest() {
		value = SECURITY_RETRIEVAL_REQUEST;
	}

	public boolean isInfraUnattendedLockPurge() {
		return value.equals(INFRA_UNATTENDED_LOCK_PURGE);
	}

	public boolean isSetDefaultsRequest() {
		return value.equals(SET_DEFAULTS_REQUEST);
	}

	public void setSetDefaultsRequest() {
		value = SET_DEFAULTS_REQUEST;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_PASS_THRU_ACTION = 20;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
