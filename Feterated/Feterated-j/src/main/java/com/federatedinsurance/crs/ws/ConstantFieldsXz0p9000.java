/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P9000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p9000 {

	//==== PROPERTIES ====
	//Original name: CF-COMMERCIAL-PACKAGE-POLICY
	private String commercialPackagePolicy = "CPP";
	//Original name: CF-SP-GET-POL-LST-SVC
	private String spGetPolLstSvc = "XZ0X9050";
	//Original name: CF-SP-ADD-ACT-NOT-WRD-SVC
	private String spAddActNotWrdSvc = "XZ0X0010";
	//Original name: CF-SP-GET-POL-DTL-SVC
	private String spGetPolDtlSvc = "FWBX0011";
	//Original name: CF-MAX-INS-LIN-CD
	private short maxInsLinCd = ((short) 10);
	//Original name: CF-YES
	private char yes = 'Y';

	//==== METHODS ====
	public String getCommercialPackagePolicy() {
		return this.commercialPackagePolicy;
	}

	public String getSpGetPolLstSvc() {
		return this.spGetPolLstSvc;
	}

	public String getSpAddActNotWrdSvc() {
		return this.spAddActNotWrdSvc;
	}

	public String getSpGetPolDtlSvc() {
		return this.spGetPolDtlSvc;
	}

	public short getMaxInsLinCd() {
		return this.maxInsLinCd;
	}

	public char getYes() {
		return this.yes;
	}
}
