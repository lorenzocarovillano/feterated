/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9010-ROW<br>
 * Variable: WS-XZ0A9010-ROW from program XZ0B9010<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9010Row extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int POL_NBR_MAXOCCURS = 100;
	//Original name: XZA910-MAX-WRD-ROWS
	private short maxWrdRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA910-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA910-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA910-POL-NBR
	private String[] polNbr = new String[POL_NBR_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public WsXz0a9010Row() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9010_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9010RowBytes(buf);
	}

	public void init() {
		for (int polNbrIdx = 1; polNbrIdx <= POL_NBR_MAXOCCURS; polNbrIdx++) {
			setPolNbr(polNbrIdx, DefaultValues.stringVal(Len.POL_NBR));
		}
	}

	public String getWsXz0a9010RowFormatted() {
		return getActNotWrdRowFormatted();
	}

	public void setWsXz0a9010RowBytes(byte[] buffer) {
		setWsXz0a9010RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9010RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9010_ROW];
		return getWsXz0a9010RowBytes(buffer, 1);
	}

	public void setWsXz0a9010RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotWrdRowBytes(buffer, position);
	}

	public byte[] getWsXz0a9010RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotWrdRowBytes(buffer, position);
		return buffer;
	}

	public String getActNotWrdRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotWrdRowBytes());
	}

	/**Original name: XZA910-ACT-NOT-WRD-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0A9010 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_ACT_NOT_WRD_LIST                   *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  14NOV2008 E404DAP    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getActNotWrdRowBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_WRD_ROW];
		return getActNotWrdRowBytes(buffer, 1);
	}

	public void setActNotWrdRowBytes(byte[] buffer, int offset) {
		int position = offset;
		maxWrdRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		setPolListBytes(buffer, position);
	}

	public byte[] getActNotWrdRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxWrdRows);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		getPolListBytes(buffer, position);
		return buffer;
	}

	public void setMaxWrdRows(short maxWrdRows) {
		this.maxWrdRows = maxWrdRows;
	}

	public short getMaxWrdRows() {
		return this.maxWrdRows;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	/**Original name: XZA910-POL-LIST<br>*/
	public byte[] getPolListBytes() {
		byte[] buffer = new byte[Len.POL_LIST];
		return getPolListBytes(buffer, 1);
	}

	public void setPolListBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POL_NBR_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setPolNbr(idx, MarshalByte.readString(buffer, position, Len.POL_NBR));
				position += Len.POL_NBR;
			} else {
				setPolNbr(idx, "");
				position += Len.POL_NBR;
			}
		}
	}

	public byte[] getPolListBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POL_NBR_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getPolNbr(idx), Len.POL_NBR);
			position += Len.POL_NBR;
		}
		return buffer;
	}

	public void setPolNbr(int polNbrIdx, String polNbr) {
		this.polNbr[polNbrIdx - 1] = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr(int polNbrIdx) {
		return this.polNbr[polNbrIdx - 1];
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9010RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_WRD_ROWS = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int POL_NBR = 25;
		public static final int POL_LIST = WsXz0a9010Row.POL_NBR_MAXOCCURS * POL_NBR;
		public static final int ACT_NOT_WRD_ROW = MAX_WRD_ROWS + CSR_ACT_NBR + NOT_PRC_TS + POL_LIST;
		public static final int WS_XZ0A9010_ROW = ACT_NOT_WRD_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
