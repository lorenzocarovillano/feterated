/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-13-MULTIPLE-SIGN-ON-MSG<br>
 * Variable: EA-13-MULTIPLE-SIGN-ON-MSG from program TS030099<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea13MultipleSignOnMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-13-LINE-1
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-13-LINE-1-1
	private String flr2 = "TS030099 -";
	//Original name: FILLER-EA-13-LINE-1-2
	private String flr3 = "WARNING:";
	//Original name: FILLER-EA-13-LINE-1-3
	private String flr4 = "ATTEMPTED TO";
	//Original name: FILLER-EA-13-LINE-1-4
	private String flr5 = "SIGN ON TO DB2";
	//Original name: FILLER-EA-13-LINE-1-5
	private String flr6 = "MULTIPLE TIMES.";
	//Original name: FILLER-EA-13-LINE-2
	private String flr7 = "";
	//Original name: FILLER-EA-13-LINE-2-1
	private String flr8 = "-";
	//Original name: FILLER-EA-13-LINE-2-2
	private String flr9 = "ADDITIONAL";
	//Original name: FILLER-EA-13-LINE-2-3
	private String flr10 = "ATTEMPTS";
	//Original name: FILLER-EA-13-LINE-2-4
	private String flr11 = "IGNORED.";

	//==== METHODS ====
	public String getLine1Formatted() {
		return MarshalByteExt.bufferToStr(getLine1Bytes());
	}

	/**Original name: EA-13-LINE-1<br>*/
	public byte[] getLine1Bytes() {
		byte[] buffer = new byte[Len.LINE1];
		return getLine1Bytes(buffer, 1);
	}

	public byte[] getLine1Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR5);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getLine2Formatted() {
		return MarshalByteExt.bufferToStr(getLine2Bytes());
	}

	/**Original name: EA-13-LINE-2<br>*/
	public byte[] getLine2Bytes() {
		byte[] buffer = new byte[Len.LINE2];
		return getLine2Bytes(buffer, 1);
	}

	public byte[] getLine2Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		position += Len.FLR8;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr10, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr11, Len.FLR11);
		return buffer;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public String getFlr10() {
		return this.flr10;
	}

	public String getFlr11() {
		return this.flr11;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 9;
		public static final int FLR4 = 13;
		public static final int FLR5 = 15;
		public static final int LINE1 = FLR1 + FLR2 + FLR3 + FLR4 + 2 * FLR5;
		public static final int FLR7 = 10;
		public static final int FLR8 = 2;
		public static final int FLR11 = 8;
		public static final int LINE2 = FLR11 + FLR2 + FLR3 + FLR7 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
