/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ESTO-ERR-STORE-RETURN-CD<br>
 * Variable: ESTO-ERR-STORE-RETURN-CD from copybook HALLESTO<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class EstoErrStoreReturnCd {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.STORE_RETURN_CD);
	public static final String TRAN_AND_STORAGE_OK = "0";
	public static final String ERR_TRANS_FAILED = "1";
	public static final String ERR_STORAGE_FAILED = "2";

	//==== METHODS ====
	public void setStoreReturnCd(short storeReturnCd) {
		this.value = NumericDisplay.asString(storeReturnCd, Len.STORE_RETURN_CD);
	}

	public void setStoreReturnCdFormatted(String storeReturnCd) {
		this.value = Trunc.toUnsignedNumeric(storeReturnCd, Len.STORE_RETURN_CD);
	}

	public short getStoreReturnCd() {
		return NumericDisplay.asShort(this.value);
	}

	public String getStoreReturnCdFormatted() {
		return this.value;
	}

	public String getStoreReturnCdAsString() {
		return getStoreReturnCdFormatted();
	}

	public boolean isTranAndStorageOk() {
		return getStoreReturnCdFormatted().equals(TRAN_AND_STORAGE_OK);
	}

	public void setEstoErrTransFailed() {
		setStoreReturnCdFormatted(ERR_TRANS_FAILED);
	}

	public boolean isEstoErrStorageFailed() {
		return getStoreReturnCdFormatted().equals(ERR_STORAGE_FAILED);
	}

	public void setEstoErrStorageFailed() {
		setStoreReturnCdFormatted(ERR_STORAGE_FAILED);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STORE_RETURN_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
