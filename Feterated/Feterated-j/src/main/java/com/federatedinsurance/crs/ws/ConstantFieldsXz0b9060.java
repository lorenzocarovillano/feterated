/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0B9060<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0b9060 {

	//==== PROPERTIES ====
	//Original name: CF-IMP-CN-CD
	private String impCnCd = "IMP";
	//Original name: CF-ADDED-BY-BUSINESS-WORKS
	private String addedByBusinessWorks = "NA";
	//Original name: CF-DSY-PRT-GRP
	private CfDsyPrtGrp dsyPrtGrp = new CfDsyPrtGrp();
	//Original name: CF-STATUS-DELETED
	private String statusDeleted = "99";
	//Original name: CF-TIMES
	private CfTimes times = new CfTimes();

	//==== METHODS ====
	public String getImpCnCd() {
		return this.impCnCd;
	}

	public String getAddedByBusinessWorks() {
		return this.addedByBusinessWorks;
	}

	public String getStatusDeleted() {
		return this.statusDeleted;
	}

	public CfDsyPrtGrp getDsyPrtGrp() {
		return dsyPrtGrp;
	}

	public CfTimes getTimes() {
		return times;
	}
}
