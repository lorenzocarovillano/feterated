/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLNOT-CO-TYP<br>
 * Variable: DCLNOT-CO-TYP from copybook XZH00021<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclnotCoTyp {

	//==== PROPERTIES ====
	//Original name: ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: CO-CD
	private String coCd = DefaultValues.stringVal(Len.CO_CD);
	//Original name: SPE-CRT-CD
	private String speCrtCd = DefaultValues.stringVal(Len.SPE_CRT_CD);
	//Original name: DTB-CD
	private char dtbCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getActNotTypCdFormatted() {
		return Functions.padBlanks(getActNotTypCd(), Len.ACT_NOT_TYP_CD);
	}

	public void setCoCd(String coCd) {
		this.coCd = Functions.subString(coCd, Len.CO_CD);
	}

	public String getCoCd() {
		return this.coCd;
	}

	public void setSpeCrtCd(String speCrtCd) {
		this.speCrtCd = Functions.subString(speCrtCd, Len.SPE_CRT_CD);
	}

	public String getSpeCrtCd() {
		return this.speCrtCd;
	}

	public void setDtbCd(char dtbCd) {
		this.dtbCd = dtbCd;
	}

	public char getDtbCd() {
		return this.dtbCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int CO_CD = 5;
		public static final int SPE_CRT_CD = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
