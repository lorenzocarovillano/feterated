/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.ActNotDao;
import com.federatedinsurance.crs.commons.data.dao.ActNotFrmRecAtcTypDao;
import com.federatedinsurance.crs.commons.data.dao.ActNotRecDao;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.commons.data.dao.PolDtaExtDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.copy.Xz009oRetMsg;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.DfhcommareaXzc09090;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsFrameworkRequestArea;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsProxyProgramArea;
import com.federatedinsurance.crs.ws.Xz0p90h0Data;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0008;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0012;
import com.federatedinsurance.crs.ws.ptr.WsMu0t0007Row;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0P90H0<br>
 * <pre>AUTHOR.       DAWN POSSEHL.
 * DATE-WRITTEN. 03 MAR 2009.
 * *****************************************************************
 *                                                                 *
 *   PROGRAM TITLE - PREPARE INSURED POLICY                        *
 *                   XREF OBJ NM : XZ_PREPARE_INSURED_POLICY_BPO   *
 *                   UOW         : XZ_PREPARE_INSURED_POLICY       *
 *                   OPERATION   : PrepareInsuredPolicy            *
 *                                                                 *
 *   PURPOSE -  FOR EVERY POLICY IN THE LIST PASSED IN, THIS       *
 *              SERVICE WILL GET THE POLICY DETAIL AND ADD OUT     *
 *              THE POLICY INFORMATION TO ACT_NOT_POL.  THE        *
 *              INSURED RECIPIENT INFORMATION WILL ALSO BE ADDED   *
 *              TO ACT_NOT_REC, AS LONG AS IT DOESN'T ALREADY      *
 *              EXIST IN THAT TABLE.                               *
 *                                                                 *
 *   PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS   *
 *                         LINKED TO BY THE FRAMEWORK DRIVER.      *
 *                                                                 *
 *   DATA ACCESS METHODS - UMT STORAGE RECORDS                     *
 *                         DB2 DATABASE                            *
 *                                                                 *
 * *****************************************************************
 * ****************************************************************
 * * NOTE: THIS LOG FOR INFRASTRUCTURE USE ONLY FOR TEMPLATE     **
 * *       VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     **
 * *       APPLICATION CODING.                                   **
 * *     T E M P L A T E   M A I N T E N A N C E   L O G         **
 * * CASE#     DATE       PROG       DESCRIPTION                 **
 * * --------  ---------  --------   ----------------------------**
 * * TS129     06/13/2006 E404LJL    TEMPLATE CREATED            **
 * * YJ249     04/27/2007 E404NEM    STDS CHGS                   **
 * * TS130     12/28/2007 E404JSP    Changed a few bugs          **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * -------  ----------  --------   ----------------------------**
 * * TO07614  03/03/2009  E404DLP    NEW                         **
 * * TO07614  04/17/2009  E404KXS    ENSURE POLICIES ARE VALID   **
 * *                                 FOR THE NOTIFICATION TYPE   **
 * * TO07614  06/02/2009  E404KXS    ADD CO-OWNERS FOR           **
 * *                                 PERSONAL LINES ACCOUNTS     **
 * *TO0760222 03/26/2010  E404ABL    ALLOW CERTAIN CANCELLATIONS **
 * *                                 WHEN NOTHING IS FOUND FOR   **
 * *                                 3RD PARTY CHECK             **
 * * PP02570  09/20/2010  E404DLP    FLORIDA POLICIES ALSO NEED  **
 * *                                 NOTICES SENT TO ALL ANI'S.  **
 * * PP02663  11/01/2010  E404KXS    ADD SECTION KEYWORD.        **
 * * PP03294  10/12/2012  E404DNF    RECOMPILE FOR FWBT0011 CHG  **
 * * PP03824  04/02/2013  E404DNF    ADDED FRM_EXP_DT TO SQL     **
 * * PP02500  09/07/2012  E404BPO    RECOMPILE FOR XZ0T0012 CHG  **
 * * PP03903  05/14/2013  E404DNF    SHUT OFF FLORIDA NOTICE OF  **
 * *                                 CANCELLATION TO ANI'S.      **
 * * PP03933  11/18/2013  E404KXS    RECOMPILE                   **
 * * 20163    08/24/2019  E404DLP    CHANGE FROM USING POLICY    **
 * *                                 DETAIL SRV TO THE BROKER    **
 * *                                 TRS DETAIL AND POL_DTA_EXT. **
 * *                                -THE POLICY DETAIL SRV HAS A **
 * *                                 CALL TO THE CRS GET PENDING **
 * *                                 CANCEL SRV; THAT WILL CAUSE **
 * *                                 A DEADLOCK SO CRS CAN NOT   **
 * *                                 USE THE BROKER POL DET SRV. **
 * *                                -REMOVED CODE NOT USED ANY   **
 * *                                 LONGER TO PRODUCE NOTICES   **
 * *                                 FOR ANIs.                   **
 * * 20163    08/24/2019  E404DLP    WRITE THE POLICY AND INSURED**
 * *                                 DATA TO AN INTERNAL TABLE   **
 * *                                 FIRST.  WE WANT TO ADD THE  **
 * *                                 ROWS TO ACT_NOT_POL AND     **
 * *                                 ACT_NOT_REC - SO THAT THE    **
 * *                                 ADR_ID IS THE SAME FOR      **
 * *                                 ADDRESSES THAT MATCH EXCEPT **
 * *                                 FOR THE CASING.  WE NEED    **
 * *                                 TO TAKE IN CONSIDERATION    **
 * *                                 THE ADR_ID VALUE IN ACT_NOT.**
 * ****************************************************************</pre>*/
public class Xz0p90h0 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ActNotFrmRecAtcTypDao actNotFrmRecAtcTypDao = new ActNotFrmRecAtcTypDao(dbAccessStatus);
	private ActNotDao actNotDao = new ActNotDao(dbAccessStatus);
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	private PolDtaExtDao polDtaExtDao = new PolDtaExtDao(dbAccessStatus);
	private ActNotRecDao actNotRecDao = new ActNotRecDao(dbAccessStatus);
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0p90h0Data ws = new Xz0p90h0Data();
	private ExecContext execContext = null;
	/**Original name: WS-XZ0T0008-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE ACT_NOT_POL SERVICE</pre>*/
	private LServiceContractAreaXz0x0008 wsXz0t0008Row = new LServiceContractAreaXz0x0008(null);
	/**Original name: WS-XZ0T0012-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE ACT_NOT_REC SERVICE</pre>*/
	private LServiceContractAreaXz0x0012 wsXz0t0012Row = new LServiceContractAreaXz0x0012(null);
	/**Original name: WS-MU0T0007-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE GET ACCCOUNT CLIENT LIST SERVICE</pre>*/
	private WsMu0t0007Row wsMu0t0007Row = new WsMu0t0007Row(null);
	//Original name: DFHCOMMAREA
	private Dfhcommarea dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, Dfhcommarea dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0p90h0 getInstance() {
		return (Programs.getInstance(Xz0p90h0.class));
	}

	/**Original name: 1000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   MAIN PROCESSING CONTROL                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-PREPARE-INS-POL.
		rng3000PrepareInsPol();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 4000-INSERT-ROWS.
		rng4000InsertRows();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-ENDING-PROCESS
			endingProcess();
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *  INITIALIZATION CONTROL                                        *
	 *                                                                *
	 * ****************************************************************
	 * * INITIALIZE ERROR/WARNING STORAGE</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//* VALIDATE UBOC IN COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2100-READ-REQ-UMT-ROW.
		readReqUmtRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-GET-NOT-INFO.
		getNotInfo();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-READ-REQ-UMT-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR THE BPO INPUT ROW                     *
	 * *****************************************************************</pre>*/
	private void readReqUmtRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: SET HALRURQA-READ-FUNC      TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM-PREPARE-INS-POL
		//                                       TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNmPrepareInsPol());
		// COB_CODE: MOVE +1                     TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(1);
		// COB_CODE: INITIALIZE                  WS-FRAMEWORK-REQUEST-AREA.
		initWsFrameworkRequestArea();
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-FRAMEWORK-REQUEST-AREA.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsFrameworkRequestArea());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: IF HALRURQA-REC-NOT-FOUND
		//               GO TO 2100-EXIT
		//           END-IF.
		if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUEST-MSG-MISSING
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequestMsgMissing();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '2100-READ-REQ-UMT-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-READ-REQ-UMT-ROW");
			// COB_CODE: MOVE 'NO RECORD FOUND ON REQ MSG STORE'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO RECORD FOUND ON REQ MSG STORE");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HALRURQA-BUS-OBJ-NM='
			//                  HALRURQA-BUS-OBJ-NM
			//                  '; HALRURQA-REC-SEQ='
			//                  HALRURQA-REC-SEQ
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HALRURQA-BUS-OBJ-NM=").append(ws.getWsHalrurqaLinkage().getBusObjNmFormatted())
							.append("; HALRURQA-REC-SEQ=").append(ws.getWsHalrurqaLinkage().getRecSeqFormatted()).toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
	}

	/**Original name: 2200-GET-NOT-INFO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  GET INFORMATION ABOUT THE NOTIFCIATION THAT WILL BE USED LATER *
	 *  DURING PROCESSING.                                             *
	 * *****************************************************************
	 *  SETUP INPUT FOR QUERIES</pre>*/
	private void getNotInfo() {
		// COB_CODE: MOVE XZY9H0-CSR-ACT-NBR     TO CSR-ACT-NBR
		//                                       OF DCLACT-NOT.
		ws.getDclactNot().setCsrActNbr(ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbr());
		// COB_CODE: MOVE XZY9H0-NOT-PRC-TS      TO NOT-PRC-TS
		//                                       OF DCLACT-NOT.
		ws.getDclactNot().setNotPrcTs(ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTs());
		// COB_CODE: PERFORM 2210-DET-NOT-TYPE.
		detNotType();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2200-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2200-EXIT
			return;
		}
		// COB_CODE: PERFORM 2220-DET-ACT-TYPE.
		detActType();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2200-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2200-EXIT
			return;
		}
	}

	/**Original name: 2210-DET-NOT-TYPE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DETERMINE IF THIS NOTIFICATION TYPE IS ONLY SENT TO THIRD      *
	 *  PARTIES. IF SO, WE WILL NEED TO VERIFY DURING PROCESSING       *
	 *  THAT AT LEAST ONE POLICY IS ACTUALLY TIED TO THE THIRD PARTIES *
	 *  AND WILL GET PUT ON THE NOTIFICATION FORMS.  IF NONE OF THE    *
	 *  POLICIES QUALIFY, THEN THERE ARE NO FORMS TO ATTACH, AND AN    *
	 *  ERROR WILL NEED TO BE RETURNED.                                *
	 * *****************************************************************
	 *  SELECT THE ACCOUNT OWNER CLIENT ID IF THE NOTIFICATION
	 *  IS ONLY SENT TO THIRD PARTIES.</pre>*/
	private void detNotType() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//               SELECT  A.ACT_OWN_CLT_ID
		//                      ,A.ACT_NOT_TYP_CD
		//                      ,A.ACT_OWN_ADR_ID
		//                 INTO  :DCLACT-NOT.ACT-OWN-CLT-ID
		//                      ,:DCLACT-NOT.ACT-NOT-TYP-CD
		//                      ,:DCLACT-NOT.ACT-OWN-ADR-ID
		//               FROM ACT_NOT A, FRM_REC_ATC_TYP B
		//               WHERE A.ACT_NOT_TYP_CD    = B.ACT_NOT_TYP_CD
		//                 AND A.CSR_ACT_NBR       = :DCLACT-NOT.CSR-ACT-NBR
		//                 AND A.NOT_PRC_TS        = :DCLACT-NOT.NOT-PRC-TS
		//                 AND B.INSD_REC_IND      = 'N'
		//                 AND (B.ADD_INS_REC_IND  = 'Y'
		//                  OR  B.LPE_MGE_REC_IND  = 'Y')
		//                 AND B.FRM_EXP_DT        = '9999-12-31'
		//           END-EXEC.
		actNotFrmRecAtcTypDao.selectRec(ws.getDclactNot().getCsrActNbr(), ws.getDclactNot().getNotPrcTs(), ws.getDclactNot());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                                       TO WS-ACT-NOT-TYP-CD
		//               WHEN ERD-SQL-NOT-FOUND
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET SW-TTY-POL-NOT-FOUND
			//                               TO TRUE
			ws.getSwitches().setTtyPolFoundFlag(false);
			// COB_CODE: SET SW-CHECK-TTY-POLICY
			//                               TO TRUE
			ws.getSwitches().setCheckTtyPolicyFlag(true);
			// COB_CODE: MOVE ACT-NOT-TYP-CD OF DCLACT-NOT
			//                               TO WS-ACT-NOT-TYP-CD
			ws.getWorkingStorageArea().setActNotTypCd(ws.getDclactNot().getActNotTypCd());
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT'      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT");
			// COB_CODE: MOVE '2210-DET-NOT-TYPE'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2210-DET-NOT-TYPE");
			// COB_CODE: MOVE 'SELECT ACT_NOT AND FRM_REC_ATC_TYP FAILED.'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT ACT_NOT AND FRM_REC_ATC_TYP FAILED.");
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZY9H0-CSR-ACT-NBR
			//                  '; NOT-PRC-TS='
			//                  XZY9H0-NOT-PRC-TS
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CSR-ACT-NBR=",
					ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbrFormatted(), "; NOT-PRC-TS=",
					ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTsFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 2220-DET-ACT-TYPE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  GET THE ACCOUNT TYPE (PERSONAL LINES OR COMMERCIAL LINES).     *
	 *  THIS WILL BE USED LATER TO DETERMINE IF THE CO-OWNER           *
	 *  NAME SHOULD BE ADDED TO THE RECIPIENT NAME.                    *
	 * *****************************************************************</pre>*/
	private void detActType() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//               SELECT A.ACT_TYP_CD
		//                 INTO :DCLACT-NOT.ACT-TYP-CD
		//               FROM ACT_NOT A
		//               WHERE A.CSR_ACT_NBR       = :DCLACT-NOT.CSR-ACT-NBR
		//                 AND A.NOT_PRC_TS        = :DCLACT-NOT.NOT-PRC-TS
		//           END-EXEC.
		ws.getDclactNot().setActTypCd(
				actNotDao.selectRec6(ws.getDclactNot().getCsrActNbr(), ws.getDclactNot().getNotPrcTs(), ws.getDclactNot().getActTypCd()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT'      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT");
			// COB_CODE: MOVE '2220-DET-NOT-TYPE'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2220-DET-NOT-TYPE");
			// COB_CODE: MOVE 'SELECT ACT_NOT FAILED.'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT ACT_NOT FAILED.");
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZY9H0-CSR-ACT-NBR
			//                  '; NOT-PRC-TS='
			//                  XZY9H0-NOT-PRC-TS
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CSR-ACT-NBR=",
					ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbrFormatted(), "; NOT-PRC-TS=",
					ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTsFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 3000-PREPARE-INS-POL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ADD THE POLICY AND INSURED INFORMATION FOR THIS NOTICE.        *
	 * *****************************************************************
	 * * PERFORM THIS FOR EVERY POLICY IN THE REQUEST UMT</pre>*/
	private void prepareInsPol() {
		// COB_CODE: SET IX-RQ                   TO +1.
		ws.setIxRq(1);
		// COB_CODE: SET IX-TP                   TO +1.
		ws.setIxTp(1);
		// COB_CODE: MOVE HIGH-VALUES            TO TABLE-OF-POLICY-INFO.
		ws.initTableOfPolicyInfoHighValues();
	}

	/**Original name: 3000-A<br>*/
	private String a() {
		// COB_CODE: IF IX-RQ > XZY9H0-MAX-POL-ROWS
		//             OR
		//              XZY9H1-POL-NBR (IX-RQ) = SPACES
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getIxRq() > ws.getWsFrameworkRequestArea().getXz0y90h0().getMaxPolRows()
				|| Characters.EQ_SPACE.test(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolNbr())) {
			// COB_CODE: GO TO 3000-EXIT
			return "";
		}
		// COB_CODE: IF SW-FIRST-TIME-THRU
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isFirstTimeThruFlag()) {
			// COB_CODE: SET SW-NOT-FIRST-TIME-THRU
			//                                   TO TRUE
			ws.getSwitches().setFirstTimeThruFlag(false);
			// COB_CODE: PERFORM 3100-ALC-MEM-FOR-SERVICES
			alcMemForServices();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3000-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: PERFORM 9100-ENDING-PROCESS
				endingProcess();
				// COB_CODE: GO TO 3000-EXIT
				return "";
			}
		}
		//* GET THE POLICY TRANSACTION DETAIL
		// COB_CODE: PERFORM 3200-GET-POLICY-DETAIL.
		getPolicyDetail();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-ENDING-PROCESS
			endingProcess();
			// COB_CODE: GO TO 3000-EXIT
			return "";
		}
		//* GET THE POL_DTA_EXT DETAIL
		// COB_CODE: PERFORM 3300-GET-POL-DTA-EXT.
		getPolDtaExt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-ENDING-PROCESS
			endingProcess();
			// COB_CODE: GO TO 3000-EXIT
			return "";
		}
		//* LOAD THE POLICY INFO TABLE
		// COB_CODE: PERFORM 3400-LOAD-POLICY-TABLE.
		loadPolicyTable();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-ENDING-PROCESS
			endingProcess();
			// COB_CODE: GO TO 3000-EXIT
			return "";
		}
		//* CHECK IF A POLICY PERTAINS TO THIRD PARTIES.
		//* IF SO, THE POLICY'S NAMED INSURED CLIENT ID WILL
		//* MATCH THE ACCOUNT OWNER CLIENT ID.
		// COB_CODE: IF SW-CHECK-TTY-POLICY
		//             AND
		//              XZC09O-CTC-ID = ACT-OWN-CLT-ID OF DCLACT-NOT
		//               SET SW-TTY-POL-FOUND    TO TRUE
		//           END-IF.
		if (ws.getSwitches().isCheckTtyPolicyFlag() && Conditions
				.eq(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oCtcId(), ws.getDclactNot().getActOwnCltId())) {
			// COB_CODE: SET SW-TTY-POL-FOUND    TO TRUE
			ws.getSwitches().setTtyPolFoundFlag(true);
		}
		// COB_CODE: SET IX-RQ                   UP BY +1.
		ws.setIxRq(Trunc.toInt(ws.getIxRq() + 1, 9));
		// COB_CODE: GO TO 3000-A.
		return "3000-A";
	}

	/**Original name: 3100-ALC-MEM-FOR-SERVICES_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE SERVICES                           *
	 * *****************************************************************</pre>*/
	private void alcMemForServices() {
		// COB_CODE: PERFORM 3130-ALC-MEM-ACT-NOT-POL-SVC
		alcMemActNotPolSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: SET SW-ACT-NOT-POL-ALC      TO TRUE.
		ws.getSwitches().setActNotPolAlcFlag(true);
		// COB_CODE: PERFORM 3140-ALC-MEM-ACT-NOT-REC-SVC
		alcMemActNotRecSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: SET SW-ACT-NOT-REC-ALC      TO TRUE.
		ws.getSwitches().setActNotRecAlcFlag(true);
	}

	/**Original name: 3130-ALC-MEM-ACT-NOT-POL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE MAINTAIN ACT_NOT_POL SERVICE.      *
	 * *****************************************************************</pre>*/
	private void alcMemActNotPolSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T0008-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x0008.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3130-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-POL
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotPol());
			// COB_CODE: MOVE '3130-ALC-MEM-ACT-NOT-POL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3130-ALC-MEM-ACT-NOT-POL-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3130-ALC-MEM-ACT-NOT-POL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3130-ALC-MEM-ACT-NOT-POL-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3130-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T0008-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t0008Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x0008.class)));
	}

	/**Original name: 3140-ALC-MEM-ACT-NOT-REC-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE MAINTAIN ACT_NOT_REC SERVICE.      *
	 * *****************************************************************</pre>*/
	private void alcMemActNotRecSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T0012-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x0012.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3140-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotRec());
			// COB_CODE: MOVE '3140-ALC-MEM-ACT-NOT-REC-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3140-ALC-MEM-ACT-NOT-REC-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3140-ALC-MEM-ACT-NOT-REC-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3140-ALC-MEM-ACT-NOT-REC-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3140-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T0012-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t0012Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x0012.class)));
	}

	/**Original name: 3200-GET-POLICY-DETAIL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET UP THE INPUT PARMS.                                        *
	 *  CALL THE SERVICE TO GET THE POLICY TRANSACTION                 *
	 * *****************************************************************</pre>*/
	private void getPolicyDetail() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 3220-SET-POLICY-INPUT-FIELDS.
		setPolicyInputFields();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-GET-POL-TRS-DETAIL-SVC)
		//               COMMAREA(WS-XZC090C1-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90H0", execContext).commarea(ws.getWsXzc090c1Row()).length(DfhcommareaXzc09090.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getServiceProxy().getGetPolTrsDetailSvc(), new Xzc09090());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3200-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-POL-TRS-DETAIL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetPolTrsDetailSvc());
			// COB_CODE: MOVE '3200-GET-POLICY-DETAIL'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3200-GET-POLICY-DETAIL");
			// COB_CODE: MOVE 'GET POLICY DETAIL SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("GET POLICY DETAIL SERVICE CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '3200-GET-POLICY-DETAIL'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"3200-GET-POLICY-DETAIL", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3200-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-GET-POL-TRS-DETAIL-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getGetPolTrsDetailSvc());
		// COB_CODE: MOVE XZC09O-RET-CD          TO PPC-ERROR-RETURN-CODE.
		ws.getWsProxyProgramArea().getPpcErrorReturnCode()
				.setErrorReturnCode(TruncAbs.toShort(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getRetMsg().getRetCd().getRetCd(), 4));
		// COB_CODE: MOVE '3200-GET-POLICY-DETAIL'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("3200-GET-POLICY-DETAIL");
		// COB_CODE: MOVE 'GET POL TRS DETAIL SVC'
		//                                       TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("GET POL TRS DETAIL SVC");
		// COB_CODE: MOVE 'POLICY NBR'           TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("POLICY NBR");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3220-SET-POLICY-INPUT-FIELDS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET UP THE INPUT PARMS.                                        *
	 * *****************************************************************
	 * *  MOVE IN THE VALUES FOR THE POL TRS DETAIL CALL</pre>*/
	private void setPolicyInputFields() {
		// COB_CODE: INITIALIZE WS-XZC090C1-ROW.
		initWsXzc090c1Row();
		// COB_CODE: MOVE XZY9H1-POL-NBR (IX-RQ) TO XZC09I-POL-NBR.
		ws.getWsXzc090c1Row().getXzc09iServiceInputs()
				.setPolNbr(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolNbr());
		// COB_CODE: MOVE XZY9H1-POL-EXP-DT (IX-RQ)
		//                                       TO XZC09I-POL-EXP-DT.
		ws.getWsXzc090c1Row().getXzc09iServiceInputs()
				.setPolExpDt(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolExpDt());
		// COB_CODE: MOVE +0                     TO XZC09I-QTE-NBR.
		ws.getWsXzc090c1Row().getXzc09iServiceInputs().setQteNbr(0);
	}

	/**Original name: 3300-GET-POL-DTA-EXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   GET SOME OF THE POLICY DETAIL FROM POL_DTA_EXT                *
	 * *****************************************************************</pre>*/
	private void getPolDtaExt() {
		// COB_CODE: MOVE XZY9H1-POL-NBR (IX-RQ) TO POL-NBR OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolNbr(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolNbr());
		// COB_CODE: MOVE XZY9H1-POL-EFF-DT (IX-RQ)
		//                                       TO POL-EFF-DT OF DCLPOL-DTA-EXT.
		ws.getDclpolDtaExt().setPolEffDt(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolEffDt());
		// COB_CODE: EXEC SQL
		//               SELECT POL_TYP_CD
		//                     ,PRI_RSK_ST_ABB
		//                 INTO :DCLPOL-DTA-EXT.POL-TYP-CD
		//                     ,:DCLPOL-DTA-EXT.PRI-RSK-ST-ABB
		//                 FROM POL_DTA_EXT
		//                WHERE POL_NBR    = :DCLPOL-DTA-EXT.POL-NBR
		//                  AND POL_EFF_DT = :DCLPOL-DTA-EXT.POL-EFF-DT
		//           END-EXEC.
		polDtaExtDao.selectRec9(ws.getDclpolDtaExt().getPolNbr(), ws.getDclpolDtaExt().getPolEffDt(), ws.getDclpolDtaExt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'POL_DTA_EXT'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("POL_DTA_EXT");
			// COB_CODE: MOVE '3300-GET-POL-DTA-EXT'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3300-GET-POL-DTA-EXT");
			// COB_CODE: MOVE SQLCODE        TO EFAL-DB2-ERR-SQLCODE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE 'DB2 ERROR READING POL_DTA_EXT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DB2 ERROR READING POL_DTA_EXT");
			// COB_CODE: MOVE SPACES         TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'POL_NBR= '
			//                  XZY9H1-POL-NBR (IX-RQ)
			//                  '; POL_EFF_DT='
			//                  XZY9H1-POL-EFF-DT (IX-RQ)
			//              DELIMITED BY SIZE
			//              INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(new StringBuffer(256).append("POL_NBR= ")
					.append(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolNbrFormatted()).append("; POL_EFF_DT=")
					.append(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolEffDt()).toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 3400-LOAD-POLICY-TABLE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LOAD THE POLICY AND INSURED INFO TABLE                         *
	 * *****************************************************************</pre>*/
	private void loadPolicyTable() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF IX-TP > CF-MAX-TBL-ENTRIES
		//               GO TO 3400-EXIT
		//           END-IF.
		if (ws.getIxTp() > ws.getConstantFields().getMaxTblEntries()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-LIMIT-EXCEEDED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspLimitExceeded();
			// COB_CODE: MOVE '3400-LOAD-POLICY-TABLE'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3400-LOAD-POLICY-TABLE");
			// COB_CODE: MOVE 'TABLE-OF-POLICY-INFO'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("TABLE-OF-POLICY-INFO");
			// COB_CODE: MOVE CF-MAX-TBL-ENTRIES TO WS-MAX-ROWS
			ws.getWorkingStorageArea().setMaxRows(ws.getConstantFields().getMaxTblEntries());
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZY9H0-CSR-ACT-NBR
			//                  ' NOT-PRC-TS='
			//                  XZY9H0-NOT-PRC-TS
			//                  ' MAX ROWS = '
			//                  WS-MAX-ROWS
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "CSR-ACT-NBR=", ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbrFormatted(), " NOT-PRC-TS=",
							ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTsFormatted(), " MAX ROWS = ",
							ws.getWorkingStorageArea().getMaxRowsAsString(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3400-EXIT
			return;
		}
		// COB_CODE: IF XZC09O-ADR-ID = ACT-OWN-ADR-ID OF DCLACT-NOT
		//                                       TO TP-SORT-ORDER(IX-TP)
		//           ELSE
		//                                       TO TP-SORT-ORDER(IX-TP)
		//           END-IF.
		if (Conditions.eq(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oAdrId(), ws.getDclactNot().getActOwnAdrId())) {
			// COB_CODE: MOVE CF-SO-MATCH-ACT-ADR-ID
			//                                   TO TP-SORT-ORDER(IX-TP)
			ws.getTpPolicyInfo(ws.getIxTp()).setSortOrder(ws.getConstantFields().getSoMatchActAdrId());
		} else {
			// COB_CODE: MOVE CF-SO-NO-MATCH-ACT-ADR-ID
			//                                   TO TP-SORT-ORDER(IX-TP)
			ws.getTpPolicyInfo(ws.getIxTp()).setSortOrder(ws.getConstantFields().getSoNoMatchActAdrId());
		}
		// COB_CODE: MOVE XZY9H1-POL-NBR (IX-RQ) TO TP-POL-NBR(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setPolNbr(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolNbr());
		// COB_CODE: MOVE POL-TYP-CD OF DCLPOL-DTA-EXT
		//                                       TO TP-POL-TYP-CD(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setPolTypCd(ws.getDclpolDtaExt().getPolTypCd());
		// COB_CODE: MOVE PRI-RSK-ST-ABB OF DCLPOL-DTA-EXT
		//                                       TO TP-POL-RSK-ST-ABB(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setPolRskStAbb(ws.getDclpolDtaExt().getPriRskStAbb());
		// COB_CODE: IF XZY9H1-NOT-EFF-DT (IX-RQ) NOT = SPACES
		//                                       TO TP-NOT-EFF-DT(IX-TP)
		//           ELSE
		//               MOVE SPACES             TO TP-NOT-EFF-DT(IX-TP)
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getNotEffDt())) {
			// COB_CODE: MOVE XZY9H1-NOT-EFF-DT (IX-RQ)
			//                                   TO TP-NOT-EFF-DT(IX-TP)
			ws.getTpPolicyInfo(ws.getIxTp()).setNotEffDt(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getNotEffDt());
		} else {
			// COB_CODE: MOVE SPACES             TO TP-NOT-EFF-DT(IX-TP)
			ws.getTpPolicyInfo(ws.getIxTp()).setNotEffDt("");
		}
		// COB_CODE: MOVE XZY9H1-POL-EFF-DT (IX-RQ)
		//                                       TO TP-POL-EFF-DT(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setPolEffDt(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolEffDt());
		// COB_CODE: MOVE XZY9H1-POL-EXP-DT (IX-RQ)
		//                                       TO TP-POL-EXP-DT(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setPolExpDt(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolExpDt());
		// COB_CODE: IF XZY9H1-POL-DUE-AMT (IX-RQ) IS NUMERIC
		//                                       TO TP-POL-DUE-AMT(IX-TP)
		//           END-IF.
		if (Functions.isNumber(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolDueAmt())) {
			// COB_CODE: MOVE XZY9H1-POL-DUE-AMT (IX-RQ)
			//                                   TO TP-POL-DUE-AMT(IX-TP)
			ws.getTpPolicyInfo(ws.getIxTp())
					.setPolDueAmt(Trunc.toDecimal(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolDueAmt(), 10, 2));
		}
		// COB_CODE: IF XZY9H1-POL-BIL-STA-CD (IX-RQ) NOT = SPACES
		//                                       TO TP-POL-BIL-STA-CD(IX-TP)
		//           ELSE
		//               MOVE SPACES             TO TP-POL-BIL-STA-CD(IX-TP)
		//           END-IF.
		if (!Conditions.eq(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolBilStaCd(), Types.SPACE_CHAR)) {
			// COB_CODE: MOVE XZY9H1-POL-BIL-STA-CD (IX-RQ)
			//                                   TO TP-POL-BIL-STA-CD(IX-TP)
			ws.getTpPolicyInfo(ws.getIxTp())
					.setPolBilStaCd(ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(ws.getIxRq()).getXz0y90h1().getPolBilStaCd());
		} else {
			// COB_CODE: MOVE SPACES             TO TP-POL-BIL-STA-CD(IX-TP)
			ws.getTpPolicyInfo(ws.getIxTp()).setPolBilStaCd(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE XZC09O-CTC-ID          TO TP-CLT-ID(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setCltId(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oCtcId());
		// COB_CODE: MOVE XZC09O-ADR-ID          TO TP-ADR-ID(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setAdrId(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oAdrId());
		// COB_CODE: MOVE XZC09O-ADR-1           TO TP-ADR-1(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setAdr1(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oAdr1());
		// COB_CODE: MOVE XZC09O-ADR-2           TO TP-ADR-2(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setAdr2(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oAdr2());
		// COB_CODE: MOVE XZC09O-CIT-NM          TO TP-CIT-NM(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setCitNm(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oCitNm());
		// COB_CODE: MOVE XZC09O-ST-ABB          TO TP-ST-ABB(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setStAbb(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oStAbb());
		// COB_CODE: MOVE XZC09O-PST-CD          TO TP-PST-CD(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setPstCd(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oPstCd());
		// COB_CODE: MOVE XZC09O-DSY-NM          TO TP-NAME(IX-TP).
		ws.getTpPolicyInfo(ws.getIxTp()).setName(ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().getXzc09oDsyNm());
		// COB_CODE: SET IX-TP                   UP BY +1.
		ws.setIxTp(Trunc.toInt(ws.getIxTp() + 1, 9));
	}

	/**Original name: 4000-INSERT-ROWS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  USING THE INTERNAL TABLE                                       *
	 *  ADD THE POLICY AND INSURED INFORMATION FOR THIS NOTICE.        *
	 * *****************************************************************
	 * * PERFORM THIS FOR EVERY POLICY IN THE TABLE
	 * * 1ST ADD THE ROWS WITH THE SORT-ORDER OF 1
	 * * 2ND ADD THE ROWS WITH THE SORT-ORDER OF 2</pre>*/
	private void insertRows() {
		// COB_CODE: SET IX-TP                   TO +1.
		ws.setIxTp(1);
		// COB_CODE: SET SW-PROCESS-SORT-1
		//               SW-FIRST-TIME-THRU      TO TRUE.
		ws.getSwitches().setProcessSort1Flag(true);
		ws.getSwitches().setFirstTimeThruFlag(true);
		// COB_CODE: MOVE CF-SO-MATCH-ACT-ADR-ID TO WS-SORT-ORDER.
		ws.getWorkingStorageArea().setSortOrder(ws.getConstantFields().getSoMatchActAdrId());
	}

	/**Original name: 4000-A<br>*/
	private String a1() {
		// COB_CODE:      IF TP-END-OF-TABLE(IX-TP)
		//           ** NEED TO LOOP THRU THE TABLE TWICE
		//                    END-IF
		//                END-IF.
		if (ws.getTpPolicyInfo(ws.getIxTp()).isEndOfTable()) {
			//* NEED TO LOOP THRU THE TABLE TWICE
			// COB_CODE: IF SW-PROCESS-SORT-1
			//                                   TO WS-SORT-ORDER
			//           ELSE
			//               GO TO 4000-EXIT
			//           END-IF
			if (ws.getSwitches().isProcessSort1Flag()) {
				// COB_CODE: SET IX-TP           TO +1
				ws.setIxTp(1);
				// COB_CODE: SET SW-NOT-PROCESS-SORT-1
				//                               TO TRUE
				ws.getSwitches().setProcessSort1Flag(false);
				// COB_CODE: MOVE CF-SO-NO-MATCH-ACT-ADR-ID
				//                               TO WS-SORT-ORDER
				ws.getWorkingStorageArea().setSortOrder(ws.getConstantFields().getSoNoMatchActAdrId());
			} else {
				// COB_CODE: GO TO 4000-EXIT
				return "";
			}
		}
		// COB_CODE:      IF TP-SORT-ORDER(IX-TP) = WS-SORT-ORDER
		//           ** ADD THE POLICY ROW
		//                    END-IF
		//                END-IF.
		if (ws.getTpPolicyInfo(ws.getIxTp()).getSortOrder() == ws.getWorkingStorageArea().getSortOrder()) {
			//* ADD THE POLICY ROW
			// COB_CODE: PERFORM 4400-ADD-POLICY-ROW
			addPolicyRow();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 4000-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 4000-EXIT
				return "";
			}
			//* ADD THE INSURED ROW - IF ONE DOESN'T EXIST YET
			// COB_CODE: PERFORM 4500-ADD-INSURED-ROW
			addInsuredRow();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 4000-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 4000-EXIT
				return "";
			}
		}
		// COB_CODE: SET IX-TP                   UP BY +1.
		ws.setIxTp(Trunc.toInt(ws.getIxTp() + 1, 9));
		// COB_CODE: GO TO 4000-A.
		return "4000-A";
	}

	/**Original name: 4400-ADD-POLICY-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET UP THE INPUT PARMS.                                        *
	 *  CALL THE SERVICE TO ADD OUT THE POLICY ROW                     *
	 * *****************************************************************</pre>*/
	private void addPolicyRow() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 4410-SET-ADR-ID.
		setAdrId();
		// COB_CODE: PERFORM 4420-SET-ACT-NOT-POL-FIELDS.
		setActNotPolFields();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-ADD-ACT-NOT-POL)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90H0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getAddActNotPol(), new Xz0x0008());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 4400-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-POL
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotPol());
			// COB_CODE: MOVE '4400-ADD-POLICY-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4400-ADD-POLICY-ROW");
			// COB_CODE: MOVE 'ADD ACT-NOT-POL SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ADD ACT-NOT-POL SERVICE CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '4400-ADD-POLICY-ROW'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"4400-ADD-POLICY-ROW", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4400-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-POL  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getAddActNotPol());
		// COB_CODE: MOVE '4400-ADD-POLICY-ROW'  TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("4400-ADD-POLICY-ROW");
		// COB_CODE: MOVE 'ADD ACT_NOT_POL SVC'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("ADD ACT_NOT_POL SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 4410-SET-ADR-ID_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  NOTE: THE ADR-ID WILL BE DIFFERENT FOR POLICY CENTER AND S3
	 *  POLICIES. POLICY CENTER IS PROPER CASED AND S3 UPPER CASED.
	 *  WHERE THE INSURED NAME AND ADDRESS MATCHES, WE NEED TO MAKE
	 *  THESE POLICIES USE THE SAME ADDRESS ID. THEN ALL POLICIES
	 *  WITH THE SAME NAME AND ADDRESS WILL PRINT ON THE SAME NOTICE.
	 * *****************************************************************</pre>*/
	private void setAdrId() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF SW-FIRST-TIME-THRU
		//               GO TO 4410-EXIT
		//           END-IF.
		if (ws.getSwitches().isFirstTimeThruFlag()) {
			// COB_CODE: SET SW-NOT-FIRST-TIME-THRU
			//                                   TO TRUE
			ws.getSwitches().setFirstTimeThruFlag(false);
			// COB_CODE: MOVE TP-ADR-ID(IX-TP)   TO WS-ADR-ID
			ws.getWorkingStorageArea().setAdrId(ws.getTpPolicyInfo(ws.getIxTp()).getAdrId());
			// COB_CODE: GO TO 4410-EXIT
			return;
		}
		// HIT THE ACT_NOT_REC TABLE TO SEE IF THERE IS AN ADR-ID
		// THAT MATCHES THE CLIENT ID AND ADDRESS WE ARE CURRENTLY ON.
		// IF SO WE WILL USE THAT ADR-ID FOR THE ACT_NOT_POL ROW
		// WE ARE GOING TO INSERT.
		// COB_CODE: MOVE XZY9H0-CSR-ACT-NBR     TO CSR-ACT-NBR
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setCsrActNbr(ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbr());
		// COB_CODE: MOVE XZY9H0-NOT-PRC-TS      TO NOT-PRC-TS
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setNotPrcTs(ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTs());
		// COB_CODE: MOVE CF-INSURED-RECIPIENT   TO REC-TYP-CD
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setRecTypCd(ws.getConstantFields().getInsuredRecipient());
		// COB_CODE: MOVE TP-CLT-ID (IX-TP)      TO REC-CLT-ID
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setRecCltId(ws.getTpPolicyInfo(ws.getIxTp()).getCltId());
		// COB_CODE: MOVE TP-ADR-1 (IX-TP)       TO LIN-1-ADR
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setLin1Adr(ws.getTpPolicyInfo(ws.getIxTp()).getAdr1());
		// COB_CODE: MOVE TP-CIT-NM (IX-TP)      TO CIT-NM
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setCitNm(ws.getTpPolicyInfo(ws.getIxTp()).getCitNm());
		// COB_CODE: EXEC SQL
		//               SELECT A.REC_ADR_ID
		//                 INTO :DCLACT-NOT-REC.REC-ADR-ID
		//               FROM ACT_NOT_REC A
		//               WHERE A.CSR_ACT_NBR    = :DCLACT-NOT-REC.CSR-ACT-NBR
		//                 AND A.NOT_PRC_TS     = :DCLACT-NOT-REC.NOT-PRC-TS
		//                 AND A.REC_TYP_CD     = :DCLACT-NOT-REC.REC-TYP-CD
		//                 AND A.REC_CLT_ID     = :DCLACT-NOT-REC.REC-CLT-ID
		//                 AND UCASE(A.LIN_1_ADR)
		//                                  = UCASE(:DCLACT-NOT-REC.LIN-1-ADR)
		//                 AND UCASE(A.CIT_NM)
		//                                  = UCASE(:DCLACT-NOT-REC.CIT-NM)
		//           END-EXEC.
		ws.getDclactNotRec().setRecAdrId(actNotRecDao.selectRec2(ws.getDclactNotRec(), ws.getDclactNotRec().getRecAdrId()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                                       TO WS-ADR-ID
		//               WHEN ERD-SQL-NOT-FOUND
		//                                       TO WS-ADR-ID
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: MOVE REC-ADR-ID OF DCLACT-NOT-REC
			//                               TO WS-ADR-ID
			ws.getWorkingStorageArea().setAdrId(ws.getDclactNotRec().getRecAdrId());
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: MOVE TP-ADR-ID(IX-TP)
			//                               TO WS-ADR-ID
			ws.getWorkingStorageArea().setAdrId(ws.getTpPolicyInfo(ws.getIxTp()).getAdrId());
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED    OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT    OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT_REC'  TO EFAL-ERR-OBJECT-NAME
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_REC");
			// COB_CODE: MOVE '4410-SET-ADR-ID'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4410-SET-ADR-ID");
			// COB_CODE: MOVE 'SELECT ACT_NOT_REC FAILED'
			//                               TO EFAL-ERR-COMMENT
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT ACT_NOT_REC FAILED");
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZY9H0-CSR-ACT-NBR
			//                  ' NOT-PRC-TS='
			//                  XZY9H0-NOT-PRC-TS
			//                  ' REC-TYP-CD='
			//                  REC-TYP-CD  OF DCLACT-NOT-REC
			//                  ' REC-CLT-ID='
			//                  REC-CLT-ID  OF DCLACT-NOT-REC
			//                  ' LIN-1-ADR='
			//                  LIN-1-ADR OF DCLACT-NOT-REC
			//                  ' CIT-NM='
			//                  CIT-NM  OF DCLACT-NOT-REC
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//                                  OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "CSR-ACT-NBR=", ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbrFormatted(), " NOT-PRC-TS=",
							ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTsFormatted(), " REC-TYP-CD=",
							ws.getDclactNotRec().getRecTypCdFormatted(), " REC-CLT-ID=", ws.getDclactNotRec().getRecCltIdFormatted(), " LIN-1-ADR=",
							ws.getDclactNotRec().getLin1AdrFormatted(), " CIT-NM=", ws.getDclactNotRec().getCitNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 4420-SET-ACT-NOT-POL-FIELDS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET UP THE INPUT PARMS.                                        *
	 * *****************************************************************
	 * *  RE-POSITION THE MEMORY PARMS TO WHAT WAS ALC FOR THIS SVC</pre>*/
	private void setActNotPolFields() {
		// COB_CODE: MOVE LENGTH OF WS-XZ0T0008-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x0008.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: SET PPC-SERVICE-DATA-POINTER
		//                                       TO ADDRESS OF WS-XZ0T0008-ROW.
		ws.getWsProxyProgramArea().setPpcServiceDataPointer(pointerManager.addressOf(wsXz0t0008Row));
		//*  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
		//*  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-ADD-ACT-NOT-POL     TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getAddActNotPol());
		//*  MOVE IN THE VALUES FOR THE INSERT INTO ACT_NOT_POL
		// COB_CODE: INITIALIZE WS-XZ0T0008-ROW.
		initWsXz0t0008Row();
		// COB_CODE: MOVE XZY9H0-USERID          TO XZT08I-USERID.
		wsXz0t0008Row.setXzt08iUserid(ws.getWsFrameworkRequestArea().getXz0y90h0().getUserid());
		// COB_CODE: MOVE XZY9H0-CSR-ACT-NBR     TO XZT08I-CSR-ACT-NBR.
		wsXz0t0008Row.setXzt08iCsrActNbr(ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbr());
		// COB_CODE: MOVE XZY9H0-NOT-PRC-TS      TO XZT08I-TK-NOT-PRC-TS.
		wsXz0t0008Row.setXzt08iTkNotPrcTs(ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTs());
		// COB_CODE: MOVE TP-POL-NBR (IX-TP)     TO XZT08I-POL-NBR.
		wsXz0t0008Row.setXzt08iPolNbr(ws.getTpPolicyInfo(ws.getIxTp()).getPolNbr());
		// COB_CODE: MOVE TP-POL-TYP-CD (IX-TP)  TO XZT08I-POL-TYP-CD.
		wsXz0t0008Row.setXzt08iPolTypCd(ws.getTpPolicyInfo(ws.getIxTp()).getPolTypCd());
		// COB_CODE: MOVE TP-POL-RSK-ST-ABB (IX-TP)
		//                                       TO XZT08I-POL-PRI-RSK-ST-ABB.
		wsXz0t0008Row.setXzt08iPolPriRskStAbb(ws.getTpPolicyInfo(ws.getIxTp()).getPolRskStAbb());
		// COB_CODE: IF TP-NOT-EFF-DT (IX-TP) NOT = SPACES
		//                                       TO XZT08I-NOT-EFF-DT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getTpPolicyInfo(ws.getIxTp()).getNotEffDt())) {
			// COB_CODE: MOVE TP-NOT-EFF-DT (IX-TP)
			//                                   TO XZT08I-NOT-EFF-DT
			wsXz0t0008Row.setXzt08iNotEffDt(ws.getTpPolicyInfo(ws.getIxTp()).getNotEffDt());
		}
		// COB_CODE: MOVE TP-POL-EFF-DT (IX-TP)  TO XZT08I-POL-EFF-DT.
		wsXz0t0008Row.setXzt08iPolEffDt(ws.getTpPolicyInfo(ws.getIxTp()).getPolEffDt());
		// COB_CODE: MOVE TP-POL-EXP-DT (IX-TP)  TO XZT08I-POL-EXP-DT.
		wsXz0t0008Row.setXzt08iPolExpDt(ws.getTpPolicyInfo(ws.getIxTp()).getPolExpDt());
		// COB_CODE: IF TP-POL-DUE-AMT (IX-TP) IS NUMERIC
		//                                       TO XZT08I-POL-DUE-AMT
		//           END-IF.
		if (Functions.isNumber(ws.getTpPolicyInfo(ws.getIxTp()).getPolDueAmt())) {
			// COB_CODE: MOVE TP-POL-DUE-AMT (IX-TP)
			//                                   TO XZT08I-POL-DUE-AMT
			wsXz0t0008Row.setXzt08iPolDueAmt(Trunc.toDecimal(ws.getTpPolicyInfo(ws.getIxTp()).getPolDueAmt(), 10, 2));
		}
		// COB_CODE: MOVE TP-CLT-ID (IX-TP)      TO XZT08I-TK-NIN-CLT-ID.
		wsXz0t0008Row.setXzt08iTkNinCltId(ws.getTpPolicyInfo(ws.getIxTp()).getCltId());
		// COB_CODE: MOVE WS-ADR-ID              TO XZT08I-TK-NIN-ADR-ID.
		wsXz0t0008Row.setXzt08iTkNinAdrId(ws.getWorkingStorageArea().getAdrId());
		// COB_CODE: MOVE CF-NO                  TO XZT08I-TK-WF-STARTED-IND.
		wsXz0t0008Row.setXzt08iTkWfStartedInd(ws.getConstantFields().getNo());
		// COB_CODE: IF TP-POL-BIL-STA-CD (IX-TP) NOT = SPACES
		//                                       TO XZT08I-TK-POL-BIL-STA-CD
		//           END-IF.
		if (!Conditions.eq(ws.getTpPolicyInfo(ws.getIxTp()).getPolBilStaCd(), Types.SPACE_CHAR)) {
			// COB_CODE: MOVE TP-POL-BIL-STA-CD (IX-TP)
			//                                   TO XZT08I-TK-POL-BIL-STA-CD
			wsXz0t0008Row.setXzt08iTkPolBilStaCd(ws.getTpPolicyInfo(ws.getIxTp()).getPolBilStaCd());
		}
	}

	/**Original name: 4500-ADD-INSURED-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CHECK TO SEE IF THE INSURED HAS ALREADY BEEN ADDED             *
	 *  SET UP THE INPUT PARMS.                                        *
	 *  CALL THE SERVICE TO ADD OUT THE POLICY ROW                     *
	 * *****************************************************************</pre>*/
	private void addInsuredRow() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 4510-CHECK-FOR-INSURED.
		checkForInsured();
		// COB_CODE: IF SW-DO-NOT-ADD-INSURED
		//             OR
		//              UBOC-HALT-AND-RETURN
		//               GO TO 4500-EXIT
		//           END-IF.
		if (!ws.getSwitches().isAddInsuredFlag()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4500-EXIT
			return;
		}
		// COB_CODE: PERFORM 4520-CHECK-FOR-COWN.
		checkForCown();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4500-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4500-EXIT
			return;
		}
		// COB_CODE: PERFORM 4530-SET-ACT-NOT-REC-FIELDS.
		setActNotRecFields();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-ADD-ACT-NOT-REC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90H0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getAddActNotRec(), new Xz0x0012());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 4500-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotRec());
			// COB_CODE: MOVE '4500-ADD-INSURED-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4500-ADD-INSURED-ROW");
			// COB_CODE: MOVE 'ADD ACT-NOT-REC SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ADD ACT-NOT-REC SERVICE CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '4500-ADD-INSURED-ROW'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"4500-ADD-INSURED-ROW", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4500-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getAddActNotRec());
		// COB_CODE: MOVE '4500-ADD-INSURED-ROW' TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("4500-ADD-INSURED-ROW");
		// COB_CODE: MOVE 'ADD ACT_NOT_REC SVC'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("ADD ACT_NOT_REC SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 4510-CHECK-FOR-INSURED_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CHECK TO SEE IF THIS INSURED HAS ALREADY BEEN ADDED            *
	 *  ALSO CHECK ON UPPERCASE ADDRESS FIELDS MATCHING                *
	 * *****************************************************************</pre>*/
	private void checkForInsured() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE XZY9H0-CSR-ACT-NBR     TO CSR-ACT-NBR
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setCsrActNbr(ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbr());
		// COB_CODE: MOVE XZY9H0-NOT-PRC-TS      TO NOT-PRC-TS
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setNotPrcTs(ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTs());
		// COB_CODE: MOVE CF-INSURED-RECIPIENT   TO REC-TYP-CD
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setRecTypCd(ws.getConstantFields().getInsuredRecipient());
		// COB_CODE: MOVE TP-CLT-ID (IX-TP)      TO REC-CLT-ID
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setRecCltId(ws.getTpPolicyInfo(ws.getIxTp()).getCltId());
		// COB_CODE: MOVE WS-ADR-ID              TO REC-ADR-ID
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setRecAdrId(ws.getWorkingStorageArea().getAdrId());
		// COB_CODE: MOVE TP-ADR-1 (IX-TP)       TO LIN-1-ADR
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setLin1Adr(ws.getTpPolicyInfo(ws.getIxTp()).getAdr1());
		// COB_CODE: MOVE TP-CIT-NM (IX-TP)      TO CIT-NM
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setCitNm(ws.getTpPolicyInfo(ws.getIxTp()).getCitNm());
		// COB_CODE: EXEC SQL
		//               SELECT A.REC_SEQ_NBR
		//                 INTO :DCLACT-NOT-REC.REC-SEQ-NBR
		//               FROM ACT_NOT_REC A
		//               WHERE A.CSR_ACT_NBR    = :DCLACT-NOT-REC.CSR-ACT-NBR
		//                 AND A.NOT_PRC_TS     = :DCLACT-NOT-REC.NOT-PRC-TS
		//                 AND A.REC_TYP_CD     = :DCLACT-NOT-REC.REC-TYP-CD
		//                 AND A.REC_CLT_ID     = :DCLACT-NOT-REC.REC-CLT-ID
		//                 AND (A.REC_ADR_ID    = :DCLACT-NOT-REC.REC-ADR-ID
		//                  OR (UCASE(A.LIN_1_ADR)
		//                                  = UCASE(:DCLACT-NOT-REC.LIN-1-ADR)
		//                  AND UCASE(A.CIT_NM)
		//                                  = UCASE(:DCLACT-NOT-REC.CIT-NM)))
		//           END-EXEC.
		ws.getDclactNotRec().setRecSeqNbr(actNotRecDao.selectRec3(ws.getDclactNotRec(), ws.getDclactNotRec().getRecSeqNbr()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                                       TO TRUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   SET SW-ADD-INSURED  TO TRUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET SW-DO-NOT-ADD-INSURED
			//                               TO TRUE
			ws.getSwitches().setAddInsuredFlag(false);
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET SW-ADD-INSURED  TO TRUE
			ws.getSwitches().setAddInsuredFlag(true);
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED    OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT    OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'ACT_NOT_REC'  TO EFAL-ERR-OBJECT-NAME
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_REC");
			// COB_CODE: MOVE '4510-CHECK-FOR-INSURED'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4510-CHECK-FOR-INSURED");
			// COB_CODE: MOVE 'SELECT ACT_NOT_REC FAILED'
			//                               TO EFAL-ERR-COMMENT
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT ACT_NOT_REC FAILED");
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZY9H0-CSR-ACT-NBR
			//                  ' NOT-PRC-TS='
			//                  XZY9H0-NOT-PRC-TS
			//                  ' REC-TYP-CD='
			//                  REC-TYP-CD  OF DCLACT-NOT-REC
			//                  ' REC-CLT-ID='
			//                  REC-CLT-ID  OF DCLACT-NOT-REC
			//                  ' REC-ADR-ID='
			//                  REC-ADR-ID  OF DCLACT-NOT-REC
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//                                  OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "CSR-ACT-NBR=", ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbrFormatted(), " NOT-PRC-TS=",
							ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTsFormatted(), " REC-TYP-CD=",
							ws.getDclactNotRec().getRecTypCdFormatted(), " REC-CLT-ID=", ws.getDclactNotRec().getRecCltIdFormatted(), " REC-ADR-ID=",
							ws.getDclactNotRec().getRecAdrIdFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 4520-CHECK-FOR-COWN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ADD CO-OWNER INFORMATION FOR PERSONAL LINES ACCOUNTS.          *
	 * *****************************************************************</pre>*/
	private void checkForCown() {
		// COB_CODE: MOVE TP-NAME (IX-TP)        TO WS-REC-NAME.
		ws.getWorkingStorageArea().setRecName(ws.getTpPolicyInfo(ws.getIxTp()).getName());
		// COB_CODE: IF ACT-TYP-CD OF DCLACT-NOT NOT = CF-ATC-PERSONAL-LINES
		//               GO TO 4520-EXIT
		//           END-IF.
		if (!Conditions.eq(ws.getDclactNot().getActTypCd(), ws.getConstantFields().getAtcPersonalLines())) {
			// COB_CODE: GO TO 4520-EXIT
			return;
		}
		// CALL THE GET ACCOUNT CLIENT LIST SERVICE TO GET THE
		// CO-OWNER NAME
		// COB_CODE: PERFORM 4521-ALC-MEM-GET-CLT-LIST-SVC.
		alcMemGetCltListSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4520-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4520-EXIT
			return;
		}
		// COB_CODE: SET SW-GET-ACT-CLT-LIST-ALC TO TRUE.
		ws.getSwitches().setGetActCltListAlcFlag(true);
		// COB_CODE: PERFORM 4522-CALL-GET-CLT-LIST-SVC.
		callGetCltListSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4520-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4520-EXIT
			return;
		}
		// COB_CODE: PERFORM 4523-FIND-COWN-ADR.
		rng4523FindCownAdr();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4520-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4520-EXIT
			return;
		}
		// INSERT THE COWN NAME SO THAT IT WILL BE PLACED ON THE
		// SECOND LINE OF THE FORM
		// COB_CODE: MOVE WS-COWN-NM             TO WS-REC-NAME
		//                                          (CF-LINE-2-START:).
		ws.getWorkingStorageArea().setRecNameSubstring(ws.getWorkingStorageArea().getCownNmFormatted(), ws.getConstantFields().getLine2Start(), -1);
	}

	/**Original name: 4521-ALC-MEM-GET-CLT-LIST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE GET ACCOUNT CLIENT LIST SERVICE    *
	 * *****************************************************************</pre>*/
	private void alcMemGetCltListSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-MU0T0007-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(WsMu0t0007Row.Len.WS_MU0T0007_ROW);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 4521-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-ACT-CLT-LIST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetActCltListSvc());
			// COB_CODE: MOVE '4521-ALC-MEM-GET-CLT-LIST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4521-ALC-MEM-GET-CLT-LIST-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getEibrespCdAsString(), ".  EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(),
					".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4521-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-MU0T0007-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsMu0t0007Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(), WsMu0t0007Row.class)));
		// COB_CODE: INITIALIZE WS-MU0T0007-ROW.
		initWsMu0t0007Row();
	}

	/**Original name: 4522-CALL-GET-CLT-LIST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE GET ACCOUNT CLIENT LIST SERVICE                       *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void callGetCltListSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-GET-ACT-CLT-LIST    TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getGetActCltList());
		// COB_CODE: MOVE XZY9H0-USERID          TO MUT07I-USERID.
		wsMu0t0007Row.setMut07iUserid(ws.getWsFrameworkRequestArea().getXz0y90h0().getUserid());
		// COB_CODE: MOVE XZY9H0-CSR-ACT-NBR     TO MUT07I-ACT-NBR.
		wsMu0t0007Row.setMut07iActNbr(ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbr());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-GET-ACT-CLT-LIST-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90H0", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getGetActCltListSvc(), new Mu0x0007());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 4522-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-ACT-CLT-LIST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetActCltListSvc());
			// COB_CODE: MOVE '4522-CALL-GET-CLT-LIST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4522-CALL-GET-CLT-LIST-SVC");
			// COB_CODE: MOVE 'GET ACCOUNT CLIENT LIST SVC CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("GET ACCOUNT CLIENT LIST SVC CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getEibrespCdAsString(), ".  EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(),
					".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4522-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-GET-ACT-CLT-LIST-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getGetActCltListSvc());
		// COB_CODE: MOVE '4522-CALL-GET-CLT-LIST-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getErrorCheckInfo().setParagraph("4522-CALL-GET-CLT-LIST-SVC");
		// COB_CODE: MOVE 'GET ACT CLT LIST SVC' TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getErrorCheckInfo().setTableOrFile("GET ACT CLT LIST SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 4523-FIND-COWN-ADR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SEARCH THE CLIENT LIST FOR THE CO-OWNER NAME                   *
	 * *****************************************************************</pre>*/
	private void findCownAdr() {
		// COB_CODE: MOVE +1                     TO SS-CL.
		ws.getSubscripts().setCl(((short) 1));
	}

	/**Original name: 4523-A<br>*/
	private String a2() {
		// COB_CODE: IF MUT07O-TK-CLT-ID (SS-CL) = SPACES
		//               GO TO 4523-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(wsMu0t0007Row.getMut07oTkCltId(ws.getSubscripts().getCl()))) {
			// COB_CODE: GO TO 4523-EXIT
			return "";
		}
		// COB_CODE: IF MUT07O-RLT-TYP-CD (SS-CL) = CF-RLT-TYP-CD-COWN
		//               GO TO 4523-EXIT
		//           END-IF.
		if (Conditions.eq(wsMu0t0007Row.getMut07oRltTypCd(ws.getSubscripts().getCl()), ws.getConstantFields().getRltTypCdCown())) {
			// COB_CODE: MOVE MUT07O-DSP-NM (SS-CL)
			//                                   TO WS-COWN-NM
			ws.getWorkingStorageArea().setCownNm(wsMu0t0007Row.getMut07oDspNm(ws.getSubscripts().getCl()));
			// COB_CODE: GO TO 4523-EXIT
			return "";
		}
		// COB_CODE: IF SS-CL-MAX
		//               GO TO 4523-EXIT
		//           END-IF.
		if (ws.getSubscripts().isClMax()) {
			// COB_CODE: GO TO 4523-EXIT
			return "";
		}
		// COB_CODE: ADD +1                      TO SS-CL.
		ws.getSubscripts().setCl(Trunc.toShort(1 + ws.getSubscripts().getCl(), 4));
		// COB_CODE: GO TO 4523-A.
		return "4523-A";
	}

	/**Original name: 4530-SET-ACT-NOT-REC-FIELDS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET UP THE INPUT PARMS.                                        *
	 * *****************************************************************
	 * *  RE-POSITION THE MEMORY PARMS TO WHAT WAS ALC FOR THIS SVC</pre>*/
	private void setActNotRecFields() {
		// COB_CODE: MOVE LENGTH OF WS-XZ0T0012-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x0012.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: SET PPC-SERVICE-DATA-POINTER
		//                                       TO ADDRESS OF WS-XZ0T0012-ROW.
		ws.getWsProxyProgramArea().setPpcServiceDataPointer(pointerManager.addressOf(wsXz0t0012Row));
		//*  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
		//*  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-ADD-ACT-NOT-REC     TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getOperationsCalled().getAddActNotRec());
		//*  MOVE IN THE VALUES FOR THE INSERT INTO ACT_NOT_REC
		// COB_CODE: INITIALIZE WS-XZ0T0012-ROW.
		initWsXz0t0012Row();
		// COB_CODE: MOVE XZY9H0-USERID          TO XZT12I-USERID.
		wsXz0t0012Row.setXzt12iUserid(ws.getWsFrameworkRequestArea().getXz0y90h0().getUserid());
		// COB_CODE: MOVE XZY9H0-CSR-ACT-NBR     TO XZT12I-CSR-ACT-NBR.
		wsXz0t0012Row.setXzt12iCsrActNbr(ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbr());
		// COB_CODE: MOVE XZY9H0-NOT-PRC-TS      TO XZT12I-TK-NOT-PRC-TS.
		wsXz0t0012Row.setXzt12iTkNotPrcTs(ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTs());
		// COB_CODE: MOVE CF-INSURED-RECIPIENT   TO XZT12I-REC-TYP-CD.
		wsXz0t0012Row.setXzt12iRecTypCd(ws.getConstantFields().getInsuredRecipient());
		// COB_CODE: MOVE TP-CLT-ID (IX-TP)      TO XZT12I-TK-REC-CLT-ID.
		wsXz0t0012Row.setXzt12iTkRecCltId(ws.getTpPolicyInfo(ws.getIxTp()).getCltId());
		// REC-NAME DETERMINED BY 4520-CHECK-FOR-COWN
		// COB_CODE: MOVE WS-REC-NAME            TO XZT12I-REC-NAME.
		wsXz0t0012Row.setXzt12iRecName(ws.getWorkingStorageArea().getRecName());
		// COB_CODE: MOVE WS-ADR-ID              TO XZT12I-TK-REC-ADR-ID.
		wsXz0t0012Row.setXzt12iTkRecAdrId(ws.getWorkingStorageArea().getAdrId());
		// COB_CODE: MOVE TP-ADR-1 (IX-TP)       TO XZT12I-LIN-1-ADR.
		wsXz0t0012Row.setXzt12iLin1Adr(ws.getTpPolicyInfo(ws.getIxTp()).getAdr1());
		// COB_CODE: MOVE TP-ADR-2 (IX-TP)       TO XZT12I-LIN-2-ADR.
		wsXz0t0012Row.setXzt12iLin2Adr(ws.getTpPolicyInfo(ws.getIxTp()).getAdr2());
		// COB_CODE: MOVE TP-CIT-NM (IX-TP)      TO XZT12I-CITY-NAME.
		wsXz0t0012Row.setXzt12iCityName(ws.getTpPolicyInfo(ws.getIxTp()).getCitNm());
		// COB_CODE: MOVE TP-ST-ABB (IX-TP)      TO XZT12I-STATE-ABB.
		wsXz0t0012Row.setXzt12iStateAbb(ws.getTpPolicyInfo(ws.getIxTp()).getStAbb());
		// COB_CODE: MOVE TP-PST-CD (IX-TP)      TO XZT12I-POSTAL-CD.
		wsXz0t0012Row.setXzt12iPostalCd(ws.getTpPolicyInfo(ws.getIxTp()).getPstCd());
		// COB_CODE: MOVE CF-NO                  TO XZT12I-TK-MNL-IND.
		wsXz0t0012Row.setXzt12iTkMnlInd(ws.getConstantFields().getNo());
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWorkingStorageArea().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P90H0", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getCommInfo().setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getCommInfo().setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWorkingStorageArea().getApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: 9100-ENDING-PROCESS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE SERVICES  - IF ALLOCATED               *
	 *  RETURN A WARNING MESSAGE IF NO POLICIES WERE SENT IN           *
	 * *****************************************************************</pre>*/
	private void endingProcess() {
		// COB_CODE: IF SW-ACT-NOT-POL-ALC
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isActNotPolAlcFlag()) {
			// COB_CODE: PERFORM 9130-FREE-MEM-ACT-NOT-POL-SVC
			freeMemActNotPolSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9100-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9100-EXIT
				return;
			}
		}
		// COB_CODE: IF SW-ACT-NOT-REC-ALC
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isActNotRecAlcFlag()) {
			// COB_CODE: PERFORM 9140-FREE-MEM-ACT-NOT-REC-SVC
			freeMemActNotRecSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9100-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9100-EXIT
				return;
			}
		}
		// COB_CODE: IF SW-GET-ACT-CLT-LIST-ALC
		//               END-IF
		//           END-IF.
		if (ws.getSwitches().isGetActCltListAlcFlag()) {
			// COB_CODE: PERFORM 9150-FREE-MEM-GET-CLT-LIST-SVC
			freeMemGetCltListSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 9100-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 9100-EXIT
				return;
			}
		}
		//* A NON-LOGGABLE WARNING IS RETURNED IF NO POLICIES WERE SENT
		// COB_CODE: IF SW-FIRST-TIME-THRU
		//               PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
		//           END-IF.
		if (ws.getSwitches().isFirstTimeThruFlag()) {
			// COB_CODE: SET WS-NON-LOGGABLE-WARNING
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setWarning();
			// COB_CODE: MOVE WS-BUS-OBJ-NM-PREPARE-INS-POL
			//                                   TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getBusObjNmPrepareInsPol());
			// COB_CODE: MOVE 'ACT_NBR'          TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("ACT_NBR");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO UWRN-WARNING-CODE
			ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE XZY9H0-CSR-ACT-NBR TO EA-01-CSR-ACT-NBR
			ws.getErrorAndAdviceMessages().getEa01NothingAddedMsg().setCsrActNbr(ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbr());
			// COB_CODE: MOVE XZY9H0-NOT-PRC-TS  TO EA-01-NOT-PRC-TS
			ws.getErrorAndAdviceMessages().getEa01NothingAddedMsg().setNotPrcTs(ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTs());
			// COB_CODE: MOVE EA-01-NOTHING-ADDED-MSG
			//                                   TO WS-NONLOG-ERR-ALLTXT-TEXT
			ws.getWsNonlogPlaceholderValues()
					.setNonlogErrAlltxtText(ws.getErrorAndAdviceMessages().getEa01NothingAddedMsg().getEa01NothingAddedMsgFormatted());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
		}
		//* A NON-LOGGABLE ERROR OR WARNING IS RETURNED IF WE NEEDED
		//* TO CHECK FOR THIRD PARTY POLICIES BUT FOUND NONE.
		// COB_CODE:      IF SW-CHECK-TTY-POLICY
		//                  AND
		//                   SW-TTY-POL-NOT-FOUND
		//           * CERTAIN NOTIFICATION TYPES CAN BE PROCESSED EVEN WHEN THERE IS
		//           * NO PRINT TO BE PRODUCED.  WE WILL STILL PRODUCE A WARNING FOR
		//           * THESE NOTIFICATIONS.
		//                    PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
		//                END-IF.
		if (ws.getSwitches().isCheckTtyPolicyFlag() && !ws.getSwitches().isTtyPolFoundFlag()) {
			// CERTAIN NOTIFICATION TYPES CAN BE PROCESSED EVEN WHEN THERE IS
			// NO PRINT TO BE PRODUCED.  WE WILL STILL PRODUCE A WARNING FOR
			// THESE NOTIFICATIONS.
			// COB_CODE:          IF WS-ACT-NOT-TYP-CD = CF-NT-CNI
			//                      OR
			//                       WS-ACT-NOT-TYP-CD = CF-NT-NPC
			//                      OR
			//                       WS-ACT-NOT-TYP-CD = CF-NT-CRW
			//           * SINCE THIS WARNING IS BEING USED BY THE FRONT END TO DETERMINE
			//           * SCREENS THE WORDING SHOULD NOT BE CHANGED.
			//                                            TO WS-NONLOG-ERR-ALLTXT-TEXT
			//                    ELSE
			//                                            TO WS-NONLOG-ERR-ALLTXT-TEXT
			//                    END-IF
			if (Conditions.eq(ws.getWorkingStorageArea().getActNotTypCd(), ws.getConstantFields().getNoTtyFonWngNotTyp().getCni())
					|| Conditions.eq(ws.getWorkingStorageArea().getActNotTypCd(), ws.getConstantFields().getNoTtyFonWngNotTyp().getNpc())
					|| Conditions.eq(ws.getWorkingStorageArea().getActNotTypCd(), ws.getConstantFields().getNoTtyFonWngNotTyp().getCrw())) {
				// SINCE THIS WARNING IS BEING USED BY THE FRONT END TO DETERMINE
				// SCREENS THE WORDING SHOULD NOT BE CHANGED.
				// COB_CODE: SET WS-NON-LOGGABLE-WARNING
				//                               TO TRUE
				ws.getWsNonLoggableWarnOrErrSw().setWarning();
				// COB_CODE: MOVE WS-BUS-OBJ-NM-PREPARE-INS-POL
				//                               TO UWRN-FAILED-TABLE-OR-FILE
				ws.getUwrnCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getBusObjNmPrepareInsPol());
				// COB_CODE: MOVE 'ACT_NBR'      TO UWRN-FAILED-COLUMN-OR-FIELD
				ws.getUwrnCommon().setFailedColumnOrField("ACT_NBR");
				// COB_CODE: MOVE 'GEN_ALLTXT'   TO UWRN-WARNING-CODE
				ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
				// COB_CODE: MOVE SPACES         TO WS-NONLOG-PLACEHOLDER-VALUES
				ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
				// COB_CODE: MOVE EA-03-NO-PRINT-FOR-NOT-TYP-WNG
				//                               TO WS-NONLOG-ERR-ALLTXT-TEXT
				ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getErrorAndAdviceMessages().getEa03NoPrintForNotTypWngFormatted());
			} else {
				// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR
				//                               TO TRUE
				ws.getWsNonLoggableWarnOrErrSw().setBusErr();
				// COB_CODE: MOVE WS-BUS-OBJ-NM-PREPARE-INS-POL
				//                               TO NLBE-FAILED-TABLE-OR-FILE
				ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getBusObjNmPrepareInsPol());
				// COB_CODE: MOVE 'ACT_NBR'      TO NLBE-FAILED-COLUMN-OR-FIELD
				ws.getNlbeCommon().setFailedColumnOrField("ACT_NBR");
				// COB_CODE: MOVE 'GEN_ALLTXT'   TO NLBE-ERROR-CODE
				ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
				// COB_CODE: MOVE SPACES         TO WS-NONLOG-PLACEHOLDER-VALUES
				ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
				// COB_CODE: MOVE XZY9H0-CSR-ACT-NBR
				//                               TO EA-02-CSR-ACT-NBR
				ws.getErrorAndAdviceMessages().getEa02NoPolsForNotMsg().setCsrActNbr(ws.getWsFrameworkRequestArea().getXz0y90h0().getCsrActNbr());
				// COB_CODE: MOVE XZY9H0-NOT-PRC-TS
				//                               TO EA-02-NOT-PRC-TS
				ws.getErrorAndAdviceMessages().getEa02NoPolsForNotMsg().setNotPrcTs(ws.getWsFrameworkRequestArea().getXz0y90h0().getNotPrcTs());
				// COB_CODE: MOVE EA-02-NO-POLS-FOR-NOT-MSG
				//                               TO WS-NONLOG-ERR-ALLTXT-TEXT
				ws.getWsNonlogPlaceholderValues()
						.setNonlogErrAlltxtText(ws.getErrorAndAdviceMessages().getEa02NoPolsForNotMsg().getEa02NoPolsForNotMsgFormatted());
			}
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
		}
	}

	/**Original name: 9130-FREE-MEM-ACT-NOT-POL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE MAINTAIN ACT_NOT_POL SERVICE.          *
	 * *****************************************************************</pre>*/
	private void freeMemActNotPolSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T0008-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t0008Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-POL
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotPol());
			// COB_CODE: MOVE '9130-FREE-MEM-ACT-NOT-POL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9130-FREE-MEM-ACT-NOT-POL-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9130-FREE-MEM-ACT-NOT-POL-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9130-FREE-MEM-ACT-NOT-POL-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9140-FREE-MEM-ACT-NOT-REC-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE MAINTAIN ACT_NOT_REC SERVICE.          *
	 * *****************************************************************</pre>*/
	private void freeMemActNotRecSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T0012-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t0012Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-REC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotRec());
			// COB_CODE: MOVE '9140-FREE-MEM-ACT-NOT-REC-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9140-FREE-MEM-ACT-NOT-REC-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE IS '
			//                  WS-PROGRAM-NAME
			//                  '.  FAILED PARAGRAPH IS '
			//                  '9140-FREE-MEM-ACT-NOT-REC-SVC'
			//                  '.  FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "FAILED MODULE IS ", ws.getWorkingStorageArea().getProgramNameFormatted(), ".  FAILED PARAGRAPH IS ",
							"9140-FREE-MEM-ACT-NOT-REC-SVC", ".  FAILED MODULE EIBRESP CODE IS ", ws.getWorkingStorageArea().getEibrespCdAsString(),
							".  FAILED MODULE EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(), "." });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9150-FREE-MEM-GET-CLT-LIST-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE GET ACCOUNT CLIENT LIST SERVICE        *
	 * *****************************************************************</pre>*/
	private void freeMemGetCltListSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-MU0T0007-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsMu0t0007Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-ACT-CLT-LIST-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetActCltListSvc());
			// COB_CODE: MOVE '9150-FREE-MEM-GET-CLT-LIST-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9150-FREE-MEM-GET-CLT-LIST-SVC");
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getEibrespCdAsString(), ".  EIBRESP2 CODE IS ", ws.getWorkingStorageArea().getEibresp2CdAsString(),
					".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9900-CHECK-ERRORS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   CHECK FOR ERROR, NLBE, OR WARNING RETURNED FROM SERVICE      *
	 * ****************************************************************</pre>*/
	private void checkErrors() {
		// COB_CODE: IF PPC-NO-ERROR-CODE
		//               GO TO 9900-EXIT
		//           END-IF.
		if (ws.getWsProxyProgramArea().getPpcErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: GO TO 9900-EXIT
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN PPC-FATAL-ERROR-CODE
		//                   PERFORM 9910-SET-FATAL-ERROR
		//               WHEN PPC-NLBE-CODE
		//                   PERFORM 9920-SET-NLBE
		//               WHEN PPC-WARNING-CODE
		//                   PERFORM 9930-SET-WARNING
		//           END-EVALUATE.
		switch (ws.getWsProxyProgramArea().getPpcErrorReturnCode().getDsdErrorReturnCodeFormatted()) {

		case DsdErrorReturnCode.FATAL_ERROR_CODE:// COB_CODE: PERFORM 9910-SET-FATAL-ERROR
			setFatalError();
			break;

		case DsdErrorReturnCode.NLBE_CODE:// COB_CODE: PERFORM 9920-SET-NLBE
			rng9920SetNlbe();
			break;

		case DsdErrorReturnCode.WARNING_CODE:// COB_CODE: PERFORM 9930-SET-WARNING
			rng9930SetWarning();
			break;

		default:
			break;
		}
	}

	/**Original name: 9910-SET-FATAL-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET FOR FATAL ERROR                                           *
	 *   SAVE OFF ANY ERROR MESSAGE THAT WAS RECEIVED FROM THE CALLED  *
	 *   SERVICE IN ORDER TO PUT IT IN THE DISPLAY AREAS AFTER LOGGING *
	 *   THE BUSINESS PROCESS ERROR.                                   *
	 *   THIS WILL ALLOW US TO DISPLAY THE ACTUAL ERROR THAT OCCURRED  *
	 *   IN THE CALLED SERVICE TO THE USER.                            *
	 * *****************************************************************</pre>*/
	private void setFatalError() {
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET ETRA-CICS-WEB-RECEIVE   TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
		// COB_CODE: MOVE WS-EC-MODULE           TO EFAL-ERR-OBJECT-NAME.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkingStorageArea().getErrorCheckInfo().getModule());
		// COB_CODE: MOVE WS-EC-PARAGRAPH        TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph(ws.getWorkingStorageArea().getErrorCheckInfo().getParagraph());
		// COB_CODE: MOVE PPC-FATAL-ERROR-MESSAGE
		//                                       TO EFAL-ERR-COMMENT
		//                                          EFAL-OBJ-DATA-KEY
		//                                          ES-01-FATAL-ERROR-MSG.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		ws.getEs01FatalErrorMsg().setEs01FatalErrorMsgFormatted(ws.getWsProxyProgramArea().getPpcFatalErrorMessageFormatted());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
		// COB_CODE: MOVE ES-01-FAILED-MODULE    TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule(ws.getEs01FatalErrorMsg().getFailedModule());
		// COB_CODE: MOVE ES-01-FAILED-PARAGRAPH TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph(ws.getEs01FatalErrorMsg().getFailedParagraph());
		// COB_CODE: MOVE ES-01-SQLCODE-DISPLAY  TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(ws.getEs01FatalErrorMsg().getSqlcodeDisplay());
		// COB_CODE: MOVE ES-01-EIBRESP-DISPLAY  TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(ws.getEs01FatalErrorMsg().getEibrespDisplay());
		// COB_CODE: MOVE ES-01-EIBRESP2-DISPLAY TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(ws.getEs01FatalErrorMsg().getEibresp2Display());
	}

	/**Original name: 9920-SET-NLBE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR NLBE                                                 *
	 * ****************************************************************</pre>*/
	private void setNlbe() {
		// COB_CODE: MOVE +0                     TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(((short) 0));
	}

	/**Original name: 9920-A<br>*/
	private String a3() {
		// COB_CODE: ADD +1                      TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(Trunc.toShort(1 + ws.getSubscripts().getMsgIdx(), 4));
		// COB_CODE: IF PPC-NON-LOG-ERR-MSG(SS-MSG-IDX) = SPACES
		//               GO TO 9920-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg())) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// we are setting UBOC-HALT-AND-RETURN to true so that if a service
		// gets an NLBE, the service ends.
		// COB_CODE: SET UBOC-HALT-AND-RETURN    TO TRUE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setBusErr();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO NLBE-FAILED-TABLE-OR-FILE.
		ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO NLBE-FAILED-COLUMN-OR-FIELD.
		ws.getNlbeCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-NON-LOG-ERR-MSG (SS-MSG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-MSG-IDX-MAX
		//               GO TO 9920-EXIT
		//           END-IF.
		if (ws.getSubscripts().isMsgIdxMax()) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// COB_CODE: GO TO 9920-A.
		return "9920-A";
	}

	/**Original name: 9930-SET-WARNING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR WARNING                                              *
	 * ****************************************************************</pre>*/
	private void setWarning() {
		// COB_CODE: MOVE +0                     TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(((short) 0));
	}

	/**Original name: 9930-A<br>*/
	private String a4() {
		// COB_CODE: ADD +1                      TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(Trunc.toShort(1 + ws.getSubscripts().getWngIdx(), 4));
		// COB_CODE: IF PPC-WARN-MSG(SS-WNG-IDX) = SPACES
		//               GO TO 9930-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg())) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: SET WS-NON-LOGGABLE-WARNING TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setWarning();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO UWRN-FAILED-TABLE-OR-FILE.
		ws.getUwrnCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO UWRN-FAILED-COLUMN-OR-FIELD.
		ws.getUwrnCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-WARN-MSG (SS-WNG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-WNG-IDX-MAX
		//               GO TO 9930-EXIT
		//           END-IF.
		if (ws.getSubscripts().isWngIdxMax()) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: GO TO 9930-A.
		return "9930-A";
	}

	/**Original name: RNG_3000-PREPARE-INS-POL_FIRST_SENTENCES-_-3000-EXIT<br>*/
	private void rng3000PrepareInsPol() {
		String retcode = "";
		boolean goto3000A = false;
		prepareInsPol();
		do {
			goto3000A = false;
			retcode = a();
		} while (retcode.equals("3000-A"));
	}

	/**Original name: RNG_4000-INSERT-ROWS_FIRST_SENTENCES-_-4000-EXIT<br>*/
	private void rng4000InsertRows() {
		String retcode = "";
		boolean goto4000A = false;
		insertRows();
		do {
			goto4000A = false;
			retcode = a1();
		} while (retcode.equals("4000-A"));
	}

	/**Original name: RNG_4523-FIND-COWN-ADR_FIRST_SENTENCES-_-4523-EXIT<br>*/
	private void rng4523FindCownAdr() {
		String retcode = "";
		boolean goto4523A = false;
		findCownAdr();
		do {
			goto4523A = false;
			retcode = a2();
		} while (retcode.equals("4523-A"));
	}

	/**Original name: RNG_9920-SET-NLBE_FIRST_SENTENCES-_-9920-EXIT<br>*/
	private void rng9920SetNlbe() {
		String retcode = "";
		boolean goto9920A = false;
		setNlbe();
		do {
			goto9920A = false;
			retcode = a3();
		} while (retcode.equals("9920-A"));
	}

	/**Original name: RNG_9930-SET-WARNING_FIRST_SENTENCES-_-9930-EXIT<br>*/
	private void rng9930SetWarning() {
		String retcode = "";
		boolean goto9930A = false;
		setWarning();
		do {
			goto9930A = false;
			retcode = a4();
		} while (retcode.equals("9930-A"));
	}

	public void initWsFrameworkRequestArea() {
		ws.getWsFrameworkRequestArea().getXz0y90h0().setMaxPolRows(((short) 0));
		ws.getWsFrameworkRequestArea().getXz0y90h0().setCsrActNbr("");
		ws.getWsFrameworkRequestArea().getXz0y90h0().setNotPrcTs("");
		ws.getWsFrameworkRequestArea().getXz0y90h0().setUserid("");
		for (int idx0 = 1; idx0 <= WsFrameworkRequestArea.WS_XZ0Y90H1_ROW_MAXOCCURS; idx0++) {
			ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(idx0).getXz0y90h1().setPolNbr("");
			ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(idx0).getXz0y90h1().setPolEffDt("");
			ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(idx0).getXz0y90h1().setPolExpDt("");
			ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(idx0).getXz0y90h1().setNotEffDt("");
			ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(idx0).getXz0y90h1().setPolDueAmt(new AfDecimal(0, 10, 2));
			ws.getWsFrameworkRequestArea().getWsXz0y90h1Row(idx0).getXz0y90h1().setPolBilStaCd(Types.SPACE_CHAR);
		}
	}

	public void initPpcMemoryAllocationParms() {
		ws.getWsProxyProgramArea().setPpcServiceDataSize(0);
	}

	public void initWsXzc090c1Row() {
		ws.getWsXzc090c1Row().getXzc09iServiceInputs().setPolNbr("");
		ws.getWsXzc090c1Row().getXzc09iServiceInputs().setPolExpDt("");
		ws.getWsXzc090c1Row().getXzc09iServiceInputs().setQteNbr(0);
		ws.getWsXzc090c1Row().getXzc09iServiceInputs().setUri("");
		ws.getWsXzc090c1Row().getXzc09iServiceInputs().setUsrId("");
		ws.getWsXzc090c1Row().getXzc09iServiceInputs().setPwd("");
		ws.getWsXzc090c1Row().getXzc09iServiceInputs().setTarSys("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getRetMsg().getRetCd().setRetCd(((short) 0));
		for (int idx0 = 1; idx0 <= Xz009oRetMsg.ERR_MSG_MAXOCCURS; idx0++) {
			ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(idx0, "");
		}
		for (int idx0 = 1; idx0 <= Xz009oRetMsg.WNG_MSG_MAXOCCURS; idx0++) {
			ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getRetMsg().setXzc09oWngMsg(idx0, "");
		}
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().setPolNbr("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().setPtEffDt("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().setPtExpDt("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().setQteNbr(0);
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().setActNbr("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().setXzc09oCtcId("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().setXzc09oDsyNm("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().setXzc09oAdrId("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().setXzc09oAdr1("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().setXzc09oAdr2("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().setXzc09oCitNm("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().setXzc09oStAbb("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().setXzc09oPstCd("");
		ws.getWsXzc090c1Row().getXzc09oServiceOutputs().getNamedInsured().setXzc09oCtyNm("");
		ws.getWsXzc090c1Row().setXzc09oErrorIndicator("");
		ws.getWsXzc090c1Row().setXzc09oFaultCode("");
		ws.getWsXzc090c1Row().setXzc09oFaultString("");
		ws.getWsXzc090c1Row().setXzc09oFaultActor("");
		ws.getWsXzc090c1Row().setXzc09oFaultDetail("");
		ws.getWsXzc090c1Row().setXzc09oNonSoapFaultErrTxt("");
	}

	public void initWsXz0t0008Row() {
		wsXz0t0008Row.setXzt08iTkNotPrcTs("");
		wsXz0t0008Row.setXzt08iTkNinCltId("");
		wsXz0t0008Row.setXzt08iTkNinAdrId("");
		wsXz0t0008Row.setXzt08iTkWfStartedInd(Types.SPACE_CHAR);
		wsXz0t0008Row.setXzt08iTkPolBilStaCd(Types.SPACE_CHAR);
		wsXz0t0008Row.setXzt08iTkActNotPolCsumFormatted("000000000");
		wsXz0t0008Row.setXzt08iCsrActNbr("");
		wsXz0t0008Row.setXzt08iPolNbr("");
		wsXz0t0008Row.setXzt08iPolTypCd("");
		wsXz0t0008Row.setXzt08iPolPriRskStAbb("");
		wsXz0t0008Row.setXzt08iNotEffDt("");
		wsXz0t0008Row.setXzt08iPolEffDt("");
		wsXz0t0008Row.setXzt08iPolExpDt("");
		wsXz0t0008Row.setXzt08iPolDueAmt(new AfDecimal(0, 10, 2));
		wsXz0t0008Row.setXzt08iUserid("");
		wsXz0t0008Row.setXzt08oTkNotPrcTs("");
		wsXz0t0008Row.setXzt08oTkNinCltId("");
		wsXz0t0008Row.setXzt08oTkNinAdrId("");
		wsXz0t0008Row.setXzt08oTkWfStartedInd(Types.SPACE_CHAR);
		wsXz0t0008Row.setXzt08oTkPolBilStaCd(Types.SPACE_CHAR);
		wsXz0t0008Row.setXzt08oTkActNotPolCsumFormatted("000000000");
		wsXz0t0008Row.setXzt08oCsrActNbr("");
		wsXz0t0008Row.setXzt08oPolNbr("");
		wsXz0t0008Row.setXzt08oPolTypCd("");
		wsXz0t0008Row.setXzt08oPolTypDes("");
		wsXz0t0008Row.setXzt08oPolPriRskStAbb("");
		wsXz0t0008Row.setXzt08oNotEffDt("");
		wsXz0t0008Row.setXzt08oPolEffDt("");
		wsXz0t0008Row.setXzt08oPolExpDt("");
		wsXz0t0008Row.setXzt08oPolDueAmt(new AfDecimal(0, 10, 2));
	}

	public void initWsMu0t0007Row() {
		wsMu0t0007Row.setMut07iActNbr("");
		wsMu0t0007Row.setMut07iUserid("");
		for (int idx0 = 1; idx0 <= WsMu0t0007Row.MUT007_SERVICE_OUTPUTS_MAXOCCURS; idx0++) {
			wsMu0t0007Row.setMut07oTkCltId(idx0, "");
			wsMu0t0007Row.setMut07oTkAdrId(idx0, "");
			wsMu0t0007Row.setMut07oTkAdrSeqNbrFormatted(idx0, "00000");
			wsMu0t0007Row.setMut07oTkActNameIdFormatted(idx0, "00000");
			wsMu0t0007Row.setMut07oIdvNmInd(idx0, Types.SPACE_CHAR);
			wsMu0t0007Row.setMut07oDspNm(idx0, "");
			wsMu0t0007Row.setMut07oSrNm(idx0, "");
			wsMu0t0007Row.setMut07oLstNm(idx0, "");
			wsMu0t0007Row.setMut07oFstNm(idx0, "");
			wsMu0t0007Row.setMut07oMdlNm(idx0, "");
			wsMu0t0007Row.setMut07oPfx(idx0, "");
			wsMu0t0007Row.setMut07oSfx(idx0, "");
			wsMu0t0007Row.setMut07oLin1Adr(idx0, "");
			wsMu0t0007Row.setMut07oLin2Adr(idx0, "");
			wsMu0t0007Row.setMut07oCitNm(idx0, "");
			wsMu0t0007Row.setMut07oStAbb(idx0, "");
			wsMu0t0007Row.setMut07oPstCd(idx0, "");
			wsMu0t0007Row.setMut07oCtyNm(idx0, "");
			wsMu0t0007Row.setMut07oRltTypCd(idx0, "");
			wsMu0t0007Row.setMut07oRltTypDes(idx0, "");
			wsMu0t0007Row.setMut07oActTypCd(idx0, "");
			wsMu0t0007Row.setMut07oActTypDes(idx0, "");
			wsMu0t0007Row.setMut07oCltEffDt(idx0, "");
			wsMu0t0007Row.setMut07oCltExpDt(idx0, "");
		}
	}

	public void initWsXz0t0012Row() {
		wsXz0t0012Row.setXzt12iTkNotPrcTs("");
		wsXz0t0012Row.setXzt12iTkRecSeqNbr(0);
		wsXz0t0012Row.setXzt12iTkRecCltId("");
		wsXz0t0012Row.setXzt12iTkRecAdrId("");
		wsXz0t0012Row.setXzt12iTkMnlInd(Types.SPACE_CHAR);
		wsXz0t0012Row.setXzt12iTkActNotRecCsumFormatted("000000000");
		wsXz0t0012Row.setXzt12iCsrActNbr("");
		wsXz0t0012Row.setXzt12iRecTypCd("");
		wsXz0t0012Row.setXzt12iRecName("");
		wsXz0t0012Row.setXzt12iLin1Adr("");
		wsXz0t0012Row.setXzt12iLin2Adr("");
		wsXz0t0012Row.setXzt12iCityName("");
		wsXz0t0012Row.setXzt12iStateAbb("");
		wsXz0t0012Row.setXzt12iPostalCd("");
		wsXz0t0012Row.setXzt12iCertNbr("");
		wsXz0t0012Row.setXzt12iUserid("");
		wsXz0t0012Row.setXzt12oTkNotPrcTs("");
		wsXz0t0012Row.setXzt12oTkRecSeqNbr(0);
		wsXz0t0012Row.setXzt12oTkRecCltId("");
		wsXz0t0012Row.setXzt12oTkRecAdrId("");
		wsXz0t0012Row.setXzt12oTkMnlInd(Types.SPACE_CHAR);
		wsXz0t0012Row.setXzt12oTkActNotRecCsumFormatted("000000000");
		wsXz0t0012Row.setXzt12oCsrActNbr("");
		wsXz0t0012Row.setXzt12oRecTypCd("");
		wsXz0t0012Row.setXzt12oRecTypShtDes("");
		wsXz0t0012Row.setXzt12oRecTypLngDes("");
		wsXz0t0012Row.setXzt12oRecTypSrOrdNbr(0);
		wsXz0t0012Row.setXzt12oRecName("");
		wsXz0t0012Row.setXzt12oLin1Adr("");
		wsXz0t0012Row.setXzt12oLin2Adr("");
		wsXz0t0012Row.setXzt12oCityName("");
		wsXz0t0012Row.setXzt12oStateAbb("");
		wsXz0t0012Row.setXzt12oPostalCd("");
		wsXz0t0012Row.setXzt12oCertNbr("");
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
