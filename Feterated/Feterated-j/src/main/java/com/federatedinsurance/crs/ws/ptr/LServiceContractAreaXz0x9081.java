/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9081<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9081 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT981O_TTY_CERT_LIST_MAXOCCURS = 2500;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9081() {
	}

	public LServiceContractAreaXz0x9081(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt981iTkNotPrcTs(String xzt981iTkNotPrcTs) {
		writeString(Pos.XZT981I_TK_NOT_PRC_TS, xzt981iTkNotPrcTs, Len.XZT981I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT981I-TK-NOT-PRC-TS<br>*/
	public String getXzt981iTkNotPrcTs() {
		return readString(Pos.XZT981I_TK_NOT_PRC_TS, Len.XZT981I_TK_NOT_PRC_TS);
	}

	public void setXzt981iCsrActNbr(String xzt981iCsrActNbr) {
		writeString(Pos.XZT981I_CSR_ACT_NBR, xzt981iCsrActNbr, Len.XZT981I_CSR_ACT_NBR);
	}

	/**Original name: XZT981I-CSR-ACT-NBR<br>*/
	public String getXzt981iCsrActNbr() {
		return readString(Pos.XZT981I_CSR_ACT_NBR, Len.XZT981I_CSR_ACT_NBR);
	}

	public void setXzt981iUserid(String xzt981iUserid) {
		writeString(Pos.XZT981I_USERID, xzt981iUserid, Len.XZT981I_USERID);
	}

	/**Original name: XZT981I-USERID<br>*/
	public String getXzt981iUserid() {
		return readString(Pos.XZT981I_USERID, Len.XZT981I_USERID);
	}

	public String getXzt981iUseridFormatted() {
		return Functions.padBlanks(getXzt981iUserid(), Len.XZT981I_USERID);
	}

	public void setXzt981oTkNotPrcTs(String xzt981oTkNotPrcTs) {
		writeString(Pos.XZT981O_TK_NOT_PRC_TS, xzt981oTkNotPrcTs, Len.XZT981O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT981O-TK-NOT-PRC-TS<br>*/
	public String getXzt981oTkNotPrcTs() {
		return readString(Pos.XZT981O_TK_NOT_PRC_TS, Len.XZT981O_TK_NOT_PRC_TS);
	}

	public void setXzt981oCsrActNbr(String xzt981oCsrActNbr) {
		writeString(Pos.XZT981O_CSR_ACT_NBR, xzt981oCsrActNbr, Len.XZT981O_CSR_ACT_NBR);
	}

	/**Original name: XZT981O-CSR-ACT-NBR<br>*/
	public String getXzt981oCsrActNbr() {
		return readString(Pos.XZT981O_CSR_ACT_NBR, Len.XZT981O_CSR_ACT_NBR);
	}

	public void setXzt981oTkClientId(int xzt981oTkClientIdIdx, String xzt981oTkClientId) {
		int position = Pos.xzt981oTkClientId(xzt981oTkClientIdIdx - 1);
		writeString(position, xzt981oTkClientId, Len.XZT981O_TK_CLIENT_ID);
	}

	/**Original name: XZT981O-TK-CLIENT-ID<br>*/
	public String getXzt981oTkClientId(int xzt981oTkClientIdIdx) {
		int position = Pos.xzt981oTkClientId(xzt981oTkClientIdIdx - 1);
		return readString(position, Len.XZT981O_TK_CLIENT_ID);
	}

	public void setXzt981oTkAdrId(int xzt981oTkAdrIdIdx, String xzt981oTkAdrId) {
		int position = Pos.xzt981oTkAdrId(xzt981oTkAdrIdIdx - 1);
		writeString(position, xzt981oTkAdrId, Len.XZT981O_TK_ADR_ID);
	}

	/**Original name: XZT981O-TK-ADR-ID<br>*/
	public String getXzt981oTkAdrId(int xzt981oTkAdrIdIdx) {
		int position = Pos.xzt981oTkAdrId(xzt981oTkAdrIdIdx - 1);
		return readString(position, Len.XZT981O_TK_ADR_ID);
	}

	public void setXzt981oRecTypCd(int xzt981oRecTypCdIdx, String xzt981oRecTypCd) {
		int position = Pos.xzt981oRecTypCd(xzt981oRecTypCdIdx - 1);
		writeString(position, xzt981oRecTypCd, Len.XZT981O_REC_TYP_CD);
	}

	/**Original name: XZT981O-REC-TYP-CD<br>*/
	public String getXzt981oRecTypCd(int xzt981oRecTypCdIdx) {
		int position = Pos.xzt981oRecTypCd(xzt981oRecTypCdIdx - 1);
		return readString(position, Len.XZT981O_REC_TYP_CD);
	}

	public void setXzt981oRecTypDes(int xzt981oRecTypDesIdx, String xzt981oRecTypDes) {
		int position = Pos.xzt981oRecTypDes(xzt981oRecTypDesIdx - 1);
		writeString(position, xzt981oRecTypDes, Len.XZT981O_REC_TYP_DES);
	}

	/**Original name: XZT981O-REC-TYP-DES<br>*/
	public String getXzt981oRecTypDes(int xzt981oRecTypDesIdx) {
		int position = Pos.xzt981oRecTypDes(xzt981oRecTypDesIdx - 1);
		return readString(position, Len.XZT981O_REC_TYP_DES);
	}

	public void setXzt981oName(int xzt981oNameIdx, String xzt981oName) {
		int position = Pos.xzt981oName(xzt981oNameIdx - 1);
		writeString(position, xzt981oName, Len.XZT981O_NAME);
	}

	/**Original name: XZT981O-NAME<br>*/
	public String getXzt981oName(int xzt981oNameIdx) {
		int position = Pos.xzt981oName(xzt981oNameIdx - 1);
		return readString(position, Len.XZT981O_NAME);
	}

	public void setXzt981oAdrLin1(int xzt981oAdrLin1Idx, String xzt981oAdrLin1) {
		int position = Pos.xzt981oAdrLin1(xzt981oAdrLin1Idx - 1);
		writeString(position, xzt981oAdrLin1, Len.XZT981O_ADR_LIN1);
	}

	/**Original name: XZT981O-ADR-LIN1<br>*/
	public String getXzt981oAdrLin1(int xzt981oAdrLin1Idx) {
		int position = Pos.xzt981oAdrLin1(xzt981oAdrLin1Idx - 1);
		return readString(position, Len.XZT981O_ADR_LIN1);
	}

	public void setXzt981oAdrLin2(int xzt981oAdrLin2Idx, String xzt981oAdrLin2) {
		int position = Pos.xzt981oAdrLin2(xzt981oAdrLin2Idx - 1);
		writeString(position, xzt981oAdrLin2, Len.XZT981O_ADR_LIN2);
	}

	/**Original name: XZT981O-ADR-LIN2<br>*/
	public String getXzt981oAdrLin2(int xzt981oAdrLin2Idx) {
		int position = Pos.xzt981oAdrLin2(xzt981oAdrLin2Idx - 1);
		return readString(position, Len.XZT981O_ADR_LIN2);
	}

	public void setXzt981oCityNm(int xzt981oCityNmIdx, String xzt981oCityNm) {
		int position = Pos.xzt981oCityNm(xzt981oCityNmIdx - 1);
		writeString(position, xzt981oCityNm, Len.XZT981O_CITY_NM);
	}

	/**Original name: XZT981O-CITY-NM<br>*/
	public String getXzt981oCityNm(int xzt981oCityNmIdx) {
		int position = Pos.xzt981oCityNm(xzt981oCityNmIdx - 1);
		return readString(position, Len.XZT981O_CITY_NM);
	}

	public void setXzt981oStateAbb(int xzt981oStateAbbIdx, String xzt981oStateAbb) {
		int position = Pos.xzt981oStateAbb(xzt981oStateAbbIdx - 1);
		writeString(position, xzt981oStateAbb, Len.XZT981O_STATE_ABB);
	}

	/**Original name: XZT981O-STATE-ABB<br>*/
	public String getXzt981oStateAbb(int xzt981oStateAbbIdx) {
		int position = Pos.xzt981oStateAbb(xzt981oStateAbbIdx - 1);
		return readString(position, Len.XZT981O_STATE_ABB);
	}

	public void setXzt981oPstCd(int xzt981oPstCdIdx, String xzt981oPstCd) {
		int position = Pos.xzt981oPstCd(xzt981oPstCdIdx - 1);
		writeString(position, xzt981oPstCd, Len.XZT981O_PST_CD);
	}

	/**Original name: XZT981O-PST-CD<br>*/
	public String getXzt981oPstCd(int xzt981oPstCdIdx) {
		int position = Pos.xzt981oPstCd(xzt981oPstCdIdx - 1);
		return readString(position, Len.XZT981O_PST_CD);
	}

	public void setXzt981oCerNbr(int xzt981oCerNbrIdx, String xzt981oCerNbr) {
		int position = Pos.xzt981oCerNbr(xzt981oCerNbrIdx - 1);
		writeString(position, xzt981oCerNbr, Len.XZT981O_CER_NBR);
	}

	/**Original name: XZT981O-CER-NBR<br>*/
	public String getXzt981oCerNbr(int xzt981oCerNbrIdx) {
		int position = Pos.xzt981oCerNbr(xzt981oCerNbrIdx - 1);
		return readString(position, Len.XZT981O_CER_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT9081_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT981I_TECHNICAL_KEY = XZT9081_SERVICE_INPUTS;
		public static final int XZT981I_TK_NOT_PRC_TS = XZT981I_TECHNICAL_KEY;
		public static final int XZT981I_CSR_ACT_NBR = XZT981I_TK_NOT_PRC_TS + Len.XZT981I_TK_NOT_PRC_TS;
		public static final int XZT981I_USERID = XZT981I_CSR_ACT_NBR + Len.XZT981I_CSR_ACT_NBR;
		public static final int XZT9081_SERVICE_OUTPUTS = XZT981I_USERID + Len.XZT981I_USERID;
		public static final int XZT981O_TECHNICAL_KEY = XZT9081_SERVICE_OUTPUTS;
		public static final int XZT981O_TK_NOT_PRC_TS = XZT981O_TECHNICAL_KEY;
		public static final int XZT981O_CSR_ACT_NBR = XZT981O_TK_NOT_PRC_TS + Len.XZT981O_TK_NOT_PRC_TS;
		public static final int XZT981O_TTY_CERT_LIST_TBL = XZT981O_CSR_ACT_NBR + Len.XZT981O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt981oTtyCertList(int idx) {
			return XZT981O_TTY_CERT_LIST_TBL + idx * Len.XZT981O_TTY_CERT_LIST;
		}

		public static int xzt981oTtyTechnicalKey(int idx) {
			return xzt981oTtyCertList(idx);
		}

		public static int xzt981oTkClientId(int idx) {
			return xzt981oTtyTechnicalKey(idx);
		}

		public static int xzt981oTkAdrId(int idx) {
			return xzt981oTkClientId(idx) + Len.XZT981O_TK_CLIENT_ID;
		}

		public static int xzt981oRecTyp(int idx) {
			return xzt981oTkAdrId(idx) + Len.XZT981O_TK_ADR_ID;
		}

		public static int xzt981oRecTypCd(int idx) {
			return xzt981oRecTyp(idx);
		}

		public static int xzt981oRecTypDes(int idx) {
			return xzt981oRecTypCd(idx) + Len.XZT981O_REC_TYP_CD;
		}

		public static int xzt981oName(int idx) {
			return xzt981oRecTypDes(idx) + Len.XZT981O_REC_TYP_DES;
		}

		public static int xzt981oAdrLin1(int idx) {
			return xzt981oName(idx) + Len.XZT981O_NAME;
		}

		public static int xzt981oAdrLin2(int idx) {
			return xzt981oAdrLin1(idx) + Len.XZT981O_ADR_LIN1;
		}

		public static int xzt981oCityNm(int idx) {
			return xzt981oAdrLin2(idx) + Len.XZT981O_ADR_LIN2;
		}

		public static int xzt981oStateAbb(int idx) {
			return xzt981oCityNm(idx) + Len.XZT981O_CITY_NM;
		}

		public static int xzt981oPstCd(int idx) {
			return xzt981oStateAbb(idx) + Len.XZT981O_STATE_ABB;
		}

		public static int xzt981oCerNbr(int idx) {
			return xzt981oPstCd(idx) + Len.XZT981O_PST_CD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT981I_TK_NOT_PRC_TS = 26;
		public static final int XZT981I_CSR_ACT_NBR = 9;
		public static final int XZT981I_USERID = 8;
		public static final int XZT981O_TK_NOT_PRC_TS = 26;
		public static final int XZT981O_CSR_ACT_NBR = 9;
		public static final int XZT981O_TK_CLIENT_ID = 64;
		public static final int XZT981O_TK_ADR_ID = 64;
		public static final int XZT981O_TTY_TECHNICAL_KEY = XZT981O_TK_CLIENT_ID + XZT981O_TK_ADR_ID;
		public static final int XZT981O_REC_TYP_CD = 5;
		public static final int XZT981O_REC_TYP_DES = 13;
		public static final int XZT981O_REC_TYP = XZT981O_REC_TYP_CD + XZT981O_REC_TYP_DES;
		public static final int XZT981O_NAME = 120;
		public static final int XZT981O_ADR_LIN1 = 45;
		public static final int XZT981O_ADR_LIN2 = 45;
		public static final int XZT981O_CITY_NM = 30;
		public static final int XZT981O_STATE_ABB = 2;
		public static final int XZT981O_PST_CD = 13;
		public static final int XZT981O_CER_NBR = 25;
		public static final int XZT981O_TTY_CERT_LIST = XZT981O_TTY_TECHNICAL_KEY + XZT981O_REC_TYP + XZT981O_NAME + XZT981O_ADR_LIN1
				+ XZT981O_ADR_LIN2 + XZT981O_CITY_NM + XZT981O_STATE_ABB + XZT981O_PST_CD + XZT981O_CER_NBR;
		public static final int XZT981I_TECHNICAL_KEY = XZT981I_TK_NOT_PRC_TS;
		public static final int XZT9081_SERVICE_INPUTS = XZT981I_TECHNICAL_KEY + XZT981I_CSR_ACT_NBR + XZT981I_USERID;
		public static final int XZT981O_TECHNICAL_KEY = XZT981O_TK_NOT_PRC_TS;
		public static final int XZT981O_TTY_CERT_LIST_TBL = LServiceContractAreaXz0x9081.XZT981O_TTY_CERT_LIST_MAXOCCURS * XZT981O_TTY_CERT_LIST;
		public static final int XZT9081_SERVICE_OUTPUTS = XZT981O_TECHNICAL_KEY + XZT981O_CSR_ACT_NBR + XZT981O_TTY_CERT_LIST_TBL;
		public static final int L_SERVICE_CONTRACT_AREA = XZT9081_SERVICE_INPUTS + XZT9081_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
