/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-SERVICE-PROXY<br>
 * Variable: CF-SERVICE-PROXY from program XZ0P90H0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfServiceProxyXz0p90h0 {

	//==== PROPERTIES ====
	//Original name: CF-SP-ADD-ACT-NOT-POL
	private String addActNotPol = "XZ0X0008";
	//Original name: CF-SP-ADD-ACT-NOT-REC
	private String addActNotRec = "XZ0X0012";
	//Original name: CF-SP-GET-ACT-CLT-LIST-SVC
	private String getActCltListSvc = "MU0X0007";
	//Original name: CF-SP-GET-POL-TRS-DETAIL-SVC
	private String getPolTrsDetailSvc = "XZC09090";

	//==== METHODS ====
	public String getAddActNotPol() {
		return this.addActNotPol;
	}

	public String getAddActNotRec() {
		return this.addActNotRec;
	}

	public String getGetActCltListSvc() {
		return this.getActCltListSvc;
	}

	public String getGetPolTrsDetailSvc() {
		return this.getPolTrsDetailSvc;
	}
}
