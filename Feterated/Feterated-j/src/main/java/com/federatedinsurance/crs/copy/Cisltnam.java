/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.federatedinsurance.crs.ws.redefines.BusinessTable;
import com.federatedinsurance.crs.ws.redefines.PrefixTable;
import com.federatedinsurance.crs.ws.redefines.SuffixTable;
import com.federatedinsurance.crs.ws.redefines.TableName;

/**Original name: CISLTNAM<br>
 * Variable: CISLTNAM from copybook CISLTNAM<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cisltnam {

	//==== PROPERTIES ====
	/**Original name: TABLE-NAME<br>
	 * <pre>***********************************************************
	 * ** END OF WORKING STORAGE ADDED FOR FED VERSION CODE.
	 * ***********************************************************
	 *     COPY DWTNAM00.                                               00209
	 * *****************************************************************00010023
	 *               C H A N G E   L O G                               *00020023
	 * ----------------------------------------------------------------*00030023
	 *   SI/ER #    DATE    WHO           DESCRIPTION                  *00040023
	 * ----------------------------------------------------------------*00050023
	 *  SBA00019  04/07/89  JLM  ADDED PHD, 1, 2, 3, 4, 5 TO NAME TABLE*00060023
	 *                           AND INCREASED OCCURS CLAUSE TO 373    *00070023
	 *  SBA00050  04/07/89  JLM  ADDED CREDIT AND UNION TO NAME TABLE  *00080017
	 *                           AND INCREASED OCCURS CLAUSE TO 375    *00090017
	 *  SBA90046  05/02/89  DKA  ADDED TACKLE TO THE NAME TABLE        *00100017
	 *                           AND INCREASED OCCURS CLAUSE TO 376    *00110017
	 *  SBA90163  05/23/89  DKA  ADDED INCORP, INCORP., GRILL, INN,    *00120017
	 *                           PACKAGING, REPAIR, REP, CONDITIONING, *00130017
	 *                           COND, CONDO, CONDOS, CONDOMINIUM,     *00140017
	 *                           CONDOMINIUMS, FURN, FURNITURE, TRUST, *00150017
	 *                           DRUGSTORE, APPLICANCE, APPL,          *00160017
	 *                           APPLIANCES TO NAME TABLE AND TO       *00170017
	 *                           APPROPRIATE ABBREV ON BUSINESS TABLE. *00180017
	 *  RF1       01/15/91  RRF  ELIM MAJOR AND COLONEL  FOR LLF.      *00190017
	 *  RF2       05/24/91  RRF  ADD I, V THRU VIII SUFFIX TO TABLE.   *00200017
	 *  RF3       06/27/91  RRF  CORRECT SFX OCCURS NBR FROM 11 TO 14  *00210017
	 *  RF4       06/28/91  RRF  REMOVE JUNIOR AND SENIOR              *00220017
	 *  R         07/09/91  RRF  REMOVE ALL COMPANY KEY WORDS FOR LLF  *00230017
	 *  R         07/16/91  RRF  RESTORE    COMPANY KEY WORDS FOR LLF  *00240018
	 *  RF5       07/18/91  RRF  ADD SEVERAL PFX AND SFX.              *00250019
	 *  RF6       08/08/91  RRF  CORRECT OCCURS CLAUSE FROM 404 TO 414 *00260023
	 *  RF7       02/26/91  RRF  ADD M/S PREFIX                        *00261028
	 *  RF8       08/26/92  RRF  ADD COMMENTS TO EXPLAIN FUNCTION OF TBL00262029
	 *  100021    09/27/94  RRF  RENAME MODULE FROM DWTNAM00 TO CISLTNAM00263031
	 *  011092    09/17/98  ASW  MADE CONSISTENT WITH WORKSTATIONS CODE*
	 * *****************************************************************00270017
	 *    05 FILLER          PIC X(19) VALUE 'JUNIOR        0S006'.     00280015
	 *    05 FILLER          PIC X(19) VALUE 'SENIOR        0S009'.     00290015
	 *    05 FILLER          PIC X(19) VALUE 'CENTER        0C024'.     00300018
	 * *****************************************************************00301029
	 * *     ALL ENTRIES IN TABLE-NAME MUST BE IN ASCENDING SEQ BASED **00302029
	 * * ON KEY WORDS IN THE VALUE FIELD. EXAMPLE ADMI MUST PRECEED ****00303029
	 * * ADMIRAL.                                                   ****00303129
	 * *     THE 'P' IN THE VALUE 0P001 WILL POINT YOU TO THE PREFIX ***00303229
	 * * TABLE AND THE VALUE OF 001 POINTS YOU TO THE FIRST ITEM IN  ***00303329
	 * * THE PREFIX TABLE.                                           ***00303429
	 * *     EXAMPLE:     'AGTS       0C000'                         ***00303529
	 * * IF THE WORD 'AGTS' IS FOUND IN THE CLIENT NAME THE 'C'      ***00303629
	 * * INDICATES THE CLIENT NAME TYPE WILL BE A COMPANY AND THE    ***00303729
	 * * THREE ZEROS '000' INDICATES THE WORD 'AGTS' WILL TRANSLATE  ***00303829
	 * * TO ITSELF THEREFORE 'AGTS' THEREFORE THE THREE ZEROS SAYS   ***00303929
	 * * THERE IS NO NEED TO SEARCH ANOTHER TABLE.                   ***00304029
	 * *****************************************************************00305029
	 * ***********************************************************
	 * ***  TABLES BELOW NOW MATCH TABLES ON WORKSTATION,     ****
	 * ***  WITH THE POSSIBLE EXCEPTION OF ORDER OF ENTRIES   ****
	 * ***  WHICH NEED TO BE ALPHABETICALLY IN EBCDIC ON THE  ****
	 * ***  HOST INSTEAD OF IN ASCII AS ON WORKSTATION.       ****
	 * ***  ALSO, ALL PREFIX AND SUFFIX TABLE REFERENCES HAVE ****
	 * ***  BEEN SET TO 0 TO NOT REPLACE WHAT IS READ IN.     ****
	 * ***********************************************************</pre>*/
	private TableName tableName = new TableName();
	/**Original name: BUSINESS-TABLE<br>
	 * <pre>**************************************************************** 03801030
	 * * IF ITEMS ARE ADDED TO THE BUSINESS TABLE THEY MUST BE ADDED ** 03802030
	 * * AT THE END OF THE TABLE.  THEN SET POINTER   IN THE ABOVE   ** 03803030
	 * * TO NBR OF THE ENTRY IN THE BUSINESS TBL. AS AN EXAMPLE  THE ** 03803130
	 * * FOR POINTER FOR 'NATL ' IN THE BUSINESS TABLE WOULD BE '0C016'.03803230
	 * * ALSO INCREASE THE OCCURS CLAUSE AT THE END OF THE BUSINESS TBL 03803330
	 * **************************************************************** 03804030</pre>*/
	private BusinessTable businessTable = new BusinessTable();
	/**Original name: SUFFIX-TABLE<br>
	 * <pre>**************************************************************** 04611030
	 * * IF ITEMS ARE ADDED TO THE SUFFIX   TABLE THEY MUST BE ADDED ** 04612030
	 * * AT THE END OF THE TABLE.  THEN SET POINTER   IN THE ABOVE   ** 04613030
	 * * TO NBR OF THE ENTRY IN THE SUFFIX   TBL. AS AN EXAMPLE  THE ** 04614030
	 * * POINTER FOR 'PHD ' IN THE SUFFIX TBL WOULD BE '0S008'.      ** 04615030
	 * * ALSO INCREASE THE OCCURS CLAUSE AT THE END OF THE SUFFIX   TBL 04616030
	 * **************************************************************** 04617030</pre>*/
	private SuffixTable suffixTable = new SuffixTable();
	/**Original name: PREFIX-TABLE<br>
	 * <pre>**************************************************************** 04851030
	 * * IF ITEMS ARE ADDED TO THE PREFIX   TABLE THEY MUST BE ADDED ** 04852030
	 * * AT THE END OF THE TABLE.  THEN SET POINTER   IN THE ABOVE   ** 04853030
	 * * TO NBR OF THE ENTRY IN THE PREFIX   TBL. AS AN EXAMPLE  THE ** 04854030
	 * * POINTER FOR 'COL ' IN THE PREFIX TBL WOULD BE '0P003'.      ** 04855030
	 * * ALSO INCREASE THE OCCURS CLAUSE AT THE END OF THE PREFIX   TBL 04856030
	 * **************************************************************** 04857030</pre>*/
	private PrefixTable prefixTable = new PrefixTable();

	//==== METHODS ====
	public BusinessTable getBusinessTable() {
		return businessTable;
	}

	public PrefixTable getPrefixTable() {
		return prefixTable;
	}

	public SuffixTable getSuffixTable() {
		return suffixTable;
	}

	public TableName getTableName() {
		return tableName;
	}
}
