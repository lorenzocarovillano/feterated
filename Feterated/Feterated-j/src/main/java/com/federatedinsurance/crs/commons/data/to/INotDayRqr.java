/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [NOT_DAY_RQR]
 * 
 */
public interface INotDayRqr extends BaseSqlTo {

	/**
	 * Host Variable NBR-OF-NOT-DAY
	 * 
	 */
	short getNbrOfNotDay();

	void setNbrOfNotDay(short nbrOfNotDay);

	/**
	 * Host Variable ST-ABB
	 * 
	 */
	String getStAbb();

	void setStAbb(String stAbb);

	/**
	 * Host Variable ACT-NOT-TYP-CD
	 * 
	 */
	String getActNotTypCd();

	void setActNotTypCd(String actNotTypCd);

	/**
	 * Host Variable POL-TYP-CD
	 * 
	 */
	String getPolTypCd();

	void setPolTypCd(String polTypCd);

	/**
	 * Host Variable CF-ALL-POL-TYP
	 * 
	 */
	String getCfAllPolTyp();

	void setCfAllPolTyp(String cfAllPolTyp);

	/**
	 * Host Variable WS-DAYS-DIFFERENCE
	 * 
	 */
	int getWsDaysDifference();

	void setWsDaysDifference(int wsDaysDifference);
};
