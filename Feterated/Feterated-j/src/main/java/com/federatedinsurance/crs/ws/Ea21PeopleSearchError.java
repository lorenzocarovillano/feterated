/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-21-PEOPLE-SEARCH-ERROR<br>
 * Variable: EA-21-PEOPLE-SEARCH-ERROR from program XZC01090<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea21PeopleSearchError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR
	private String flr1 = "ERROR RETURNED";
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-1
	private String flr2 = "FROM PEOPLE";
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-2
	private String flr3 = "SEARCH WEB";
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-3
	private String flr4 = "SERVICE.";
	//Original name: EA-21-PROGRAM-NAME
	private String programName = DefaultValues.stringVal(Len.PROGRAM_NAME);
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-4
	private String flr5 = ", PARA =";
	//Original name: EA-21-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-5
	private String flr6 = ", CICS MODULE";
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-6
	private String flr7 = "=";
	//Original name: EA-21-CICS-MODULE
	private String cicsModule = DefaultValues.stringVal(Len.CICS_MODULE);
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-7
	private String flr8 = " FOR EMP ID =";
	//Original name: EA-21-EMP-ID
	private String empId = DefaultValues.stringVal(Len.EMP_ID);
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-8
	private String flr9 = ", POL NBR =";
	//Original name: EA-21-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-9
	private String flr10 = ", POL EFF DT =";
	//Original name: EA-21-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-10
	private String flr11 = ", POL EXP DT =";
	//Original name: EA-21-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: FILLER-EA-21-PEOPLE-SEARCH-ERROR-11
	private String flr12 = ". ERR MSG =";
	/**Original name: EA-21-ERR-MSG<br>
	 * <pre> WE ONLY HAVE ROOM FOR THE FIRST 54 BYTES OF THE ERROR MESSAGE
	 *  BEING RETURNED BY PEOPLE SEARCH.</pre>*/
	private String errMsg = DefaultValues.stringVal(Len.ERR_MSG);

	//==== METHODS ====
	public String getEa21PeopleSearchErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa21PeopleSearchErrorBytes());
	}

	public byte[] getEa21PeopleSearchErrorBytes() {
		byte[] buffer = new byte[Len.EA21_PEOPLE_SEARCH_ERROR];
		return getEa21PeopleSearchErrorBytes(buffer, 1);
	}

	public byte[] getEa21PeopleSearchErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, programName, Len.PROGRAM_NAME);
		position += Len.PROGRAM_NAME;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, cicsModule, Len.CICS_MODULE);
		position += Len.CICS_MODULE;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, empId, Len.EMP_ID);
		position += Len.EMP_ID;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, flr10, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, flr11, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeString(buffer, position, flr12, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, errMsg, Len.ERR_MSG);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setProgramName(String programName) {
		this.programName = Functions.subString(programName, Len.PROGRAM_NAME);
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setCicsModule(String cicsModule) {
		this.cicsModule = Functions.subString(cicsModule, Len.CICS_MODULE);
	}

	public String getCicsModule() {
		return this.cicsModule;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setEmpId(String empId) {
		this.empId = Functions.subString(empId, Len.EMP_ID);
	}

	public String getEmpId() {
		return this.empId;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getFlr10() {
		return this.flr10;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public String getFlr11() {
		return this.flr11;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public String getFlr12() {
		return this.flr12;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = Functions.subString(errMsg, Len.ERR_MSG);
	}

	public String getErrMsg() {
		return this.errMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int PARAGRAPH_NBR = 5;
		public static final int CICS_MODULE = 8;
		public static final int EMP_ID = 6;
		public static final int POL_NBR = 9;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int ERR_MSG = 54;
		public static final int FLR1 = 15;
		public static final int FLR2 = 12;
		public static final int FLR3 = 11;
		public static final int FLR4 = 9;
		public static final int FLR6 = 14;
		public static final int FLR7 = 2;
		public static final int EA21_PEOPLE_SEARCH_ERROR = PROGRAM_NAME + PARAGRAPH_NBR + CICS_MODULE + EMP_ID + POL_NBR + POL_EFF_DT + POL_EXP_DT
				+ ERR_MSG + 3 * FLR1 + 3 * FLR2 + FLR3 + 2 * FLR4 + 2 * FLR6 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
