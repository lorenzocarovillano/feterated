/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Fnc02oOutputMatchCodes;

/**Original name: FNC02O-MATCH-STRING-ARRAY<br>
 * Variables: FNC02O-MATCH-STRING-ARRAY from copybook FNC020C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Fnc02oMatchStringArray {

	//==== PROPERTIES ====
	//Original name: FNC02O-OUTPUT-STRING
	private String stringFld = DefaultValues.stringVal(Len.STRING_FLD);
	//Original name: FNC02O-OUTPUT-MATCH-TYPE
	private String matchType = DefaultValues.stringVal(Len.MATCH_TYPE);
	//Original name: FNC02O-OUTPUT-MATCH-CODES
	private Fnc02oOutputMatchCodes matchCodes = new Fnc02oOutputMatchCodes();

	//==== METHODS ====
	public void setoMatchStringArrayBytes(byte[] buffer, int offset) {
		int position = offset;
		stringFld = MarshalByte.readString(buffer, position, Len.STRING_FLD);
		position += Len.STRING_FLD;
		matchType = MarshalByte.readString(buffer, position, Len.MATCH_TYPE);
		position += Len.MATCH_TYPE;
		matchCodes.setMatchCodesBytes(buffer, position);
	}

	public byte[] getoMatchStringArrayBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, stringFld, Len.STRING_FLD);
		position += Len.STRING_FLD;
		MarshalByte.writeString(buffer, position, matchType, Len.MATCH_TYPE);
		position += Len.MATCH_TYPE;
		matchCodes.getMatchCodesBytes(buffer, position);
		return buffer;
	}

	public void initoMatchStringArraySpaces() {
		stringFld = "";
		matchType = "";
		matchCodes.initMatchCodesSpaces();
	}

	public void setStringFld(String stringFld) {
		this.stringFld = Functions.subString(stringFld, Len.STRING_FLD);
	}

	public String getStringFld() {
		return this.stringFld;
	}

	public void setMatchType(String matchType) {
		this.matchType = Functions.subString(matchType, Len.MATCH_TYPE);
	}

	public String getMatchType() {
		return this.matchType;
	}

	public Fnc02oOutputMatchCodes getMatchCodes() {
		return matchCodes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STRING_FLD = 256;
		public static final int MATCH_TYPE = 30;
		public static final int O_MATCH_STRING_ARRAY = STRING_FLD + MATCH_TYPE + Fnc02oOutputMatchCodes.Len.MATCH_CODES;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
