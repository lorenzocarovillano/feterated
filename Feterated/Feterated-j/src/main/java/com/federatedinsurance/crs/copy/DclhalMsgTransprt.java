/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalMsgTransprtV;

/**Original name: DCLHAL-MSG-TRANSPRT<br>
 * Variable: DCLHAL-MSG-TRANSPRT from copybook HALLGMTC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalMsgTransprt implements IHalMsgTransprtV {

	//==== PROPERTIES ====
	//Original name: HMTC-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HMTC-MSG-DEL-TM
	private int msgDelTm = DefaultValues.BIN_INT_VAL;
	//Original name: HMTC-WNG-DEL-TM
	private int wngDelTm = DefaultValues.BIN_INT_VAL;
	//Original name: HMTC-PASS-LNK-IND
	private char passLnkInd = DefaultValues.CHAR_VAL;
	//Original name: HMTC-STG-TYP-CD
	private char stgTypCd = DefaultValues.CHAR_VAL;
	//Original name: HMTC-MDRV-REQ-STG
	private String mdrvReqStg = DefaultValues.stringVal(Len.MDRV_REQ_STG);
	//Original name: HMTC-MDRV-RSP-STG
	private String mdrvRspStg = DefaultValues.stringVal(Len.MDRV_RSP_STG);
	//Original name: HMTC-UOW-REQ-STG
	private String uowReqStg = DefaultValues.stringVal(Len.UOW_REQ_STG);
	//Original name: HMTC-UOW-SWI-STG
	private String uowSwiStg = DefaultValues.stringVal(Len.UOW_SWI_STG);
	//Original name: HMTC-UOW-HDR-STG
	private String uowHdrStg = DefaultValues.stringVal(Len.UOW_HDR_STG);
	//Original name: HMTC-UOW-DTA-STG
	private String uowDtaStg = DefaultValues.stringVal(Len.UOW_DTA_STG);
	//Original name: HMTC-UOW-WNG-STG
	private String uowWngStg = DefaultValues.stringVal(Len.UOW_WNG_STG);
	//Original name: HMTC-UOW-KRP-STG
	private String uowKrpStg = DefaultValues.stringVal(Len.UOW_KRP_STG);
	//Original name: HMTC-UOW-NLBE-STG
	private String uowNlbeStg = DefaultValues.stringVal(Len.UOW_NLBE_STG);

	//==== METHODS ====
	public byte[] getDclhalMsgTransprtBytes() {
		byte[] buffer = new byte[Len.DCLHAL_MSG_TRANSPRT];
		return getDclhalMsgTransprtBytes(buffer, 1);
	}

	public byte[] getDclhalMsgTransprtBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, uowNm, Len.UOW_NM);
		position += Len.UOW_NM;
		MarshalByte.writeBinaryInt(buffer, position, msgDelTm);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, wngDelTm);
		position += Types.INT_SIZE;
		MarshalByte.writeChar(buffer, position, passLnkInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, stgTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mdrvReqStg, Len.MDRV_REQ_STG);
		position += Len.MDRV_REQ_STG;
		MarshalByte.writeString(buffer, position, mdrvRspStg, Len.MDRV_RSP_STG);
		position += Len.MDRV_RSP_STG;
		MarshalByte.writeString(buffer, position, uowReqStg, Len.UOW_REQ_STG);
		position += Len.UOW_REQ_STG;
		MarshalByte.writeString(buffer, position, uowSwiStg, Len.UOW_SWI_STG);
		position += Len.UOW_SWI_STG;
		MarshalByte.writeString(buffer, position, uowHdrStg, Len.UOW_HDR_STG);
		position += Len.UOW_HDR_STG;
		MarshalByte.writeString(buffer, position, uowDtaStg, Len.UOW_DTA_STG);
		position += Len.UOW_DTA_STG;
		MarshalByte.writeString(buffer, position, uowWngStg, Len.UOW_WNG_STG);
		position += Len.UOW_WNG_STG;
		MarshalByte.writeString(buffer, position, uowKrpStg, Len.UOW_KRP_STG);
		position += Len.UOW_KRP_STG;
		MarshalByte.writeString(buffer, position, uowNlbeStg, Len.UOW_NLBE_STG);
		return buffer;
	}

	public void initDclhalMsgTransprtSpaces() {
		uowNm = "";
		msgDelTm = Types.INVALID_BINARY_INT_VAL;
		wngDelTm = Types.INVALID_BINARY_INT_VAL;
		passLnkInd = Types.SPACE_CHAR;
		stgTypCd = Types.SPACE_CHAR;
		mdrvReqStg = "";
		mdrvRspStg = "";
		uowReqStg = "";
		uowSwiStg = "";
		uowHdrStg = "";
		uowDtaStg = "";
		uowWngStg = "";
		uowKrpStg = "";
		uowNlbeStg = "";
	}

	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public String getUowNmFormatted() {
		return Functions.padBlanks(getUowNm(), Len.UOW_NM);
	}

	@Override
	public void setMsgDelTm(int msgDelTm) {
		this.msgDelTm = msgDelTm;
	}

	@Override
	public int getMsgDelTm() {
		return this.msgDelTm;
	}

	@Override
	public void setWngDelTm(int wngDelTm) {
		this.wngDelTm = wngDelTm;
	}

	@Override
	public int getWngDelTm() {
		return this.wngDelTm;
	}

	@Override
	public void setPassLnkInd(char passLnkInd) {
		this.passLnkInd = passLnkInd;
	}

	@Override
	public char getPassLnkInd() {
		return this.passLnkInd;
	}

	@Override
	public void setStgTypCd(char stgTypCd) {
		this.stgTypCd = stgTypCd;
	}

	@Override
	public char getStgTypCd() {
		return this.stgTypCd;
	}

	@Override
	public void setMdrvReqStg(String mdrvReqStg) {
		this.mdrvReqStg = Functions.subString(mdrvReqStg, Len.MDRV_REQ_STG);
	}

	@Override
	public String getMdrvReqStg() {
		return this.mdrvReqStg;
	}

	@Override
	public void setMdrvRspStg(String mdrvRspStg) {
		this.mdrvRspStg = Functions.subString(mdrvRspStg, Len.MDRV_RSP_STG);
	}

	@Override
	public String getMdrvRspStg() {
		return this.mdrvRspStg;
	}

	@Override
	public void setUowReqStg(String uowReqStg) {
		this.uowReqStg = Functions.subString(uowReqStg, Len.UOW_REQ_STG);
	}

	@Override
	public String getUowReqStg() {
		return this.uowReqStg;
	}

	@Override
	public void setUowSwiStg(String uowSwiStg) {
		this.uowSwiStg = Functions.subString(uowSwiStg, Len.UOW_SWI_STG);
	}

	@Override
	public String getUowSwiStg() {
		return this.uowSwiStg;
	}

	@Override
	public void setUowHdrStg(String uowHdrStg) {
		this.uowHdrStg = Functions.subString(uowHdrStg, Len.UOW_HDR_STG);
	}

	@Override
	public String getUowHdrStg() {
		return this.uowHdrStg;
	}

	@Override
	public void setUowDtaStg(String uowDtaStg) {
		this.uowDtaStg = Functions.subString(uowDtaStg, Len.UOW_DTA_STG);
	}

	@Override
	public String getUowDtaStg() {
		return this.uowDtaStg;
	}

	@Override
	public void setUowWngStg(String uowWngStg) {
		this.uowWngStg = Functions.subString(uowWngStg, Len.UOW_WNG_STG);
	}

	@Override
	public String getUowWngStg() {
		return this.uowWngStg;
	}

	@Override
	public void setUowKrpStg(String uowKrpStg) {
		this.uowKrpStg = Functions.subString(uowKrpStg, Len.UOW_KRP_STG);
	}

	@Override
	public String getUowKrpStg() {
		return this.uowKrpStg;
	}

	@Override
	public void setUowNlbeStg(String uowNlbeStg) {
		this.uowNlbeStg = Functions.subString(uowNlbeStg, Len.UOW_NLBE_STG);
	}

	@Override
	public String getUowNlbeStg() {
		return this.uowNlbeStg;
	}

	@Override
	public String getHmtcUowNm() {
		return getUowNm();
	}

	@Override
	public void setHmtcUowNm(String hmtcUowNm) {
		this.setUowNm(hmtcUowNm);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MDRV_REQ_STG = 32;
		public static final int MDRV_RSP_STG = 32;
		public static final int UOW_REQ_STG = 32;
		public static final int UOW_SWI_STG = 32;
		public static final int UOW_HDR_STG = 32;
		public static final int UOW_DTA_STG = 32;
		public static final int UOW_WNG_STG = 32;
		public static final int UOW_KRP_STG = 32;
		public static final int UOW_NLBE_STG = 32;
		public static final int UOW_NM = 32;
		public static final int MSG_DEL_TM = 4;
		public static final int WNG_DEL_TM = 4;
		public static final int PASS_LNK_IND = 1;
		public static final int STG_TYP_CD = 1;
		public static final int DCLHAL_MSG_TRANSPRT = UOW_NM + MSG_DEL_TM + WNG_DEL_TM + PASS_LNK_IND + STG_TYP_CD + MDRV_REQ_STG + MDRV_RSP_STG
				+ UOW_REQ_STG + UOW_SWI_STG + UOW_HDR_STG + UOW_DTA_STG + UOW_WNG_STG + UOW_KRP_STG + UOW_NLBE_STG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
