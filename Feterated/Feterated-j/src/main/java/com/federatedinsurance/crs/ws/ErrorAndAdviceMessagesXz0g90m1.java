/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZ0G90M1<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXz0g90m1 {

	//==== PROPERTIES ====
	//Original name: EA-01-CICS-LINK-ERROR
	private Ea01CicsLinkErrorXz0g90m1 ea01CicsLinkError = new Ea01CicsLinkErrorXz0g90m1();
	//Original name: EA-02-GETMAIN-FREEMAIN-ERROR
	private Ea02GetmainFreemainErrorXz0g90m1 ea02GetmainFreemainError = new Ea02GetmainFreemainErrorXz0g90m1();
	//Original name: EA-03-FATAL-ERROR-MSG
	private Ea03FatalErrorMsg ea03FatalErrorMsg = new Ea03FatalErrorMsg();

	//==== METHODS ====
	public Ea01CicsLinkErrorXz0g90m1 getEa01CicsLinkError() {
		return ea01CicsLinkError;
	}

	public Ea02GetmainFreemainErrorXz0g90m1 getEa02GetmainFreemainError() {
		return ea02GetmainFreemainError;
	}

	public Ea03FatalErrorMsg getEa03FatalErrorMsg() {
		return ea03FatalErrorMsg;
	}
}
