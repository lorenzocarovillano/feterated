/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: CW02C-CLIENT-TAB-TAB<br>
 * Variable: CW02C-CLIENT-TAB-TAB from program CAWS002<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Cw02cClientTabTab extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int CMN_NM_TABLE_MAXOCCURS = 28;

	//==== CONSTRUCTORS ====
	public Cw02cClientTabTab() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.CW02C_CLIENT_TAB_TAB;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "CW02C-ENTIRE-ROW", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-TRANS-PROCESS-DT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CLIENT-ID", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-HISTORY-VLD-NBR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-EFF-DT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-PRI-SUB-CD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-FST-NM", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-LST-NM", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-MDL-NM", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-NM-PFX", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-NM-SFX", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-PRIMARY-PRO-DSN-CD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-LEG-ENT-CD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-SDX-CD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-OGN-INCEPT-DT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-ADD-NM-IND", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-DOB-DT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-BIR-ST-CD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-GENDER-CD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-PRI-LGG-CD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-USER-ID", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-STATUS-CD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-TERMINAL-ID", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-EXP-DT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-EFF-ACY-TS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-EXP-ACY-TS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-STATUTORY-TLE-CD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CW02C-CICL-LNG-NM", Len.FLR1);
	}

	/**Original name: WS-COLUMN-NAME<br>*/
	public String getWsColumnName(int wsColumnNameIdx) {
		int position = Pos.wsColumnName(wsColumnNameIdx - 1);
		return readString(position, Len.WS_COLUMN_NAME);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int CW02C_CLIENT_TAB_TAB = 1;
		public static final int FLR1 = CW02C_CLIENT_TAB_TAB;
		public static final int FLR2 = FLR1 + Len.FLR1;
		public static final int FLR3 = FLR2 + Len.FLR1;
		public static final int FLR4 = FLR3 + Len.FLR1;
		public static final int FLR5 = FLR4 + Len.FLR1;
		public static final int FLR6 = FLR5 + Len.FLR1;
		public static final int FLR7 = FLR6 + Len.FLR1;
		public static final int FLR8 = FLR7 + Len.FLR1;
		public static final int FLR9 = FLR8 + Len.FLR1;
		public static final int FLR10 = FLR9 + Len.FLR1;
		public static final int FLR11 = FLR10 + Len.FLR1;
		public static final int FLR12 = FLR11 + Len.FLR1;
		public static final int FLR13 = FLR12 + Len.FLR1;
		public static final int FLR14 = FLR13 + Len.FLR1;
		public static final int FLR15 = FLR14 + Len.FLR1;
		public static final int FLR16 = FLR15 + Len.FLR1;
		public static final int FLR17 = FLR16 + Len.FLR1;
		public static final int FLR18 = FLR17 + Len.FLR1;
		public static final int FLR19 = FLR18 + Len.FLR1;
		public static final int FLR20 = FLR19 + Len.FLR1;
		public static final int FLR21 = FLR20 + Len.FLR1;
		public static final int FLR22 = FLR21 + Len.FLR1;
		public static final int FLR23 = FLR22 + Len.FLR1;
		public static final int FLR24 = FLR23 + Len.FLR1;
		public static final int FLR25 = FLR24 + Len.FLR1;
		public static final int FLR26 = FLR25 + Len.FLR1;
		public static final int FLR27 = FLR26 + Len.FLR1;
		public static final int FLR28 = FLR27 + Len.FLR1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int cmnNmTable(int idx) {
			return 1 + idx * Len.CMN_NM_TABLE;
		}

		public static int wsColumnName(int idx) {
			return cmnNmTable(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 30;
		public static final int WS_COLUMN_NAME = 30;
		public static final int CMN_NM_TABLE = WS_COLUMN_NAME;
		public static final int CW02C_CLIENT_TAB_TAB = 28 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
