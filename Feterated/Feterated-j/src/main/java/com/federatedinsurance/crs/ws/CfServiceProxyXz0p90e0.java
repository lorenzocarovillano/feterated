/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-SERVICE-PROXY<br>
 * Variable: CF-SERVICE-PROXY from program XZ0P90E0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfServiceProxyXz0p90e0 {

	//==== PROPERTIES ====
	//Original name: CF-SP-ADD-ACT-NOT-FRM
	private String addActNotFrm = "XZ0X0014";
	//Original name: CF-SP-ADD-ACT-NOT-FRM-REC
	private String addActNotFrmRec = "XZ0X0018";
	//Original name: CF-SP-ADD-ACT-NOT-POL-FRM
	private String addActNotPolFrm = "XZ0X0016";
	//Original name: CF-SP-GET-POLICY-LIST-SVC
	private String getPolicyListSvc = "XZ0X9050";

	//==== METHODS ====
	public String getAddActNotFrm() {
		return this.addActNotFrm;
	}

	public String getAddActNotFrmRec() {
		return this.addActNotFrmRec;
	}

	public String getAddActNotPolFrm() {
		return this.addActNotPolFrm;
	}

	public String getGetPolicyListSvc() {
		return this.getPolicyListSvc;
	}
}
