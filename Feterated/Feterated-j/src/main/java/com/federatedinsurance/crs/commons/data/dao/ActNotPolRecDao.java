/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotPolRec;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ACT_NOT_POL_REC]
 * 
 */
public class ActNotPolRecDao extends BaseSqlDao<IActNotPolRec> {

	private Cursor actNotPolRecCsr;
	private Cursor polRecCsr;
	private final IRowMapper<IActNotPolRec> fetchPolRecCsrRm = buildNamedRowMapper(IActNotPolRec.class, "polNbr");

	public ActNotPolRecDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotPolRec> getToClass() {
		return IActNotPolRec.class;
	}

	public DbAccessStatus openActNotPolRecCsr(String csrActNbr, String notPrcTs, String polNbr, short recSeqNbr) {
		actNotPolRecCsr = buildQuery("openActNotPolRecCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr)
				.bind("recSeqNbr", recSeqNbr).open();
		return dbStatus;
	}

	public DbAccessStatus closeActNotPolRecCsr() {
		return closeCursor(actNotPolRecCsr);
	}

	public DbAccessStatus insertRec(String csrActNbr, String notPrcTs, String polNbr, short recSeqNbr) {
		return buildQuery("insertRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).bind("recSeqNbr", recSeqNbr)
				.executeInsert();
	}

	public DbAccessStatus deleteRec(String csrActNbr, String notPrcTs, String polNbr, short recSeqNbr) {
		return buildQuery("deleteRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).bind("recSeqNbr", recSeqNbr)
				.executeDelete();
	}

	public DbAccessStatus deleteRec1(String csrActNbr, String notPrcTs, short recSeqNbr) {
		return buildQuery("deleteRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("recSeqNbr", recSeqNbr).executeDelete();
	}

	public DbAccessStatus deleteRec2(String csrActNbr, String notPrcTs, String polNbr) {
		return buildQuery("deleteRec2").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).executeDelete();
	}

	public DbAccessStatus deleteRec3(String csrActNbr, String notPrcTs) {
		return buildQuery("deleteRec3").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).executeDelete();
	}

	public IActNotPolRec selectRec(String csrActNbr, String notPrcTs, String polNbr, short recSeqNbr, IActNotPolRec iActNotPolRec) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).bind("recSeqNbr", recSeqNbr)
				.singleResult(iActNotPolRec);
	}

	public DbAccessStatus openPolRecCsr(String csrActNbr, String notPrcTs, short recSeqNbr) {
		polRecCsr = buildQuery("openPolRecCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("recSeqNbr", recSeqNbr).open();
		return dbStatus;
	}

	public IActNotPolRec fetchPolRecCsr(IActNotPolRec iActNotPolRec) {
		return fetch(polRecCsr, iActNotPolRec, fetchPolRecCsrRm);
	}

	public DbAccessStatus closePolRecCsr() {
		return closeCursor(polRecCsr);
	}
}
