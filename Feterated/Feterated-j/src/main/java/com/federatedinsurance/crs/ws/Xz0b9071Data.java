/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IFrmRecOvl;
import com.federatedinsurance.crs.copy.DclfrmRecOvl;
import com.federatedinsurance.crs.copy.DclovlTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9071<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9071Data implements IFrmRecOvl {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b9010 constantFields = new ConstantFieldsXz0b9010();
	/**Original name: NI-SPE-PRC-CD<br>
	 * <pre>**  NULL-INDICATORS.</pre>*/
	private short niSpePrcCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-END-OF-CURSOR-OVL-FLAG
	private boolean swEndOfCursorOvlFlag = false;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9071 workingStorageArea = new WorkingStorageAreaXz0b9071();
	//Original name: WS-XZ0A9071-ROW
	private WsXz0a9071Row wsXz0a9071Row = new WsXz0a9071Row();
	//Original name: DCLFRM-REC-OVL
	private DclfrmRecOvl dclfrmRecOvl = new DclfrmRecOvl();
	//Original name: DCLOVL-TYP
	private DclovlTyp dclovlTyp = new DclovlTyp();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void setNiSpePrcCd(short niSpePrcCd) {
		this.niSpePrcCd = niSpePrcCd;
	}

	public short getNiSpePrcCd() {
		return this.niSpePrcCd;
	}

	public void setSwEndOfCursorOvlFlag(boolean swEndOfCursorOvlFlag) {
		this.swEndOfCursorOvlFlag = swEndOfCursorOvlFlag;
	}

	public boolean isSwEndOfCursorOvlFlag() {
		return this.swEndOfCursorOvlFlag;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public ConstantFieldsXz0b9010 getConstantFields() {
		return constantFields;
	}

	public DclfrmRecOvl getDclfrmRecOvl() {
		return dclfrmRecOvl;
	}

	public DclovlTyp getDclovlTyp() {
		return dclovlTyp;
	}

	@Override
	public String getFrmEdtDt() {
		return dclfrmRecOvl.getFrmEdtDt();
	}

	@Override
	public void setFrmEdtDt(String frmEdtDt) {
		this.dclfrmRecOvl.setFrmEdtDt(frmEdtDt);
	}

	@Override
	public String getFrmNbr() {
		return dclfrmRecOvl.getFrmNbr();
	}

	@Override
	public void setFrmNbr(String frmNbr) {
		this.dclfrmRecOvl.setFrmNbr(frmNbr);
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getOvlEdlFrmNm() {
		return dclfrmRecOvl.getOvlEdlFrmNm();
	}

	@Override
	public void setOvlEdlFrmNm(String ovlEdlFrmNm) {
		this.dclfrmRecOvl.setOvlEdlFrmNm(ovlEdlFrmNm);
	}

	@Override
	public String getOvlSrCd() {
		return dclfrmRecOvl.getOvlSrCd();
	}

	@Override
	public void setOvlSrCd(String ovlSrCd) {
		this.dclfrmRecOvl.setOvlSrCd(ovlSrCd);
	}

	@Override
	public String getRecTypCd() {
		return dclfrmRecOvl.getRecTypCd();
	}

	@Override
	public void setRecTypCd(String recTypCd) {
		this.dclfrmRecOvl.setRecTypCd(recTypCd);
	}

	@Override
	public String getSpePrcCd() {
		return dclfrmRecOvl.getSpePrcCd();
	}

	@Override
	public void setSpePrcCd(String spePrcCd) {
		this.dclfrmRecOvl.setSpePrcCd(spePrcCd);
	}

	@Override
	public String getSpePrcCdObj() {
		if (getNiSpePrcCd() >= 0) {
			return getSpePrcCd();
		} else {
			return null;
		}
	}

	@Override
	public void setSpePrcCdObj(String spePrcCdObj) {
		if (spePrcCdObj != null) {
			setSpePrcCd(spePrcCdObj);
			setNiSpePrcCd(((short) 0));
		} else {
			setNiSpePrcCd(((short) -1));
		}
	}

	@Override
	public String getStAbb() {
		return dclfrmRecOvl.getStAbb();
	}

	@Override
	public void setStAbb(String stAbb) {
		this.dclfrmRecOvl.setStAbb(stAbb);
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b9071 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9071Row getWsXz0a9071Row() {
		return wsXz0a9071Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
