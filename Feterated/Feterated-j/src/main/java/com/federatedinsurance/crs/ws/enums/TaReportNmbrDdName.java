/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TA-REPORT-NMBR-DD-NAME<br>
 * Variable: TA-REPORT-NMBR-DD-NAME from program TS030099<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TaReportNmbrDdName implements ICopyable<TaReportNmbrDdName> {

	//==== PROPERTIES ====
	public static final String END_OF_REPORT_NMBRS = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REPORT_NMBR_DD_NAME);
	//Original name: TA-REPORT-NMBR
	private String reportNmbr = DefaultValues.stringVal(Len.REPORT_NMBR);
	//Original name: TA-REPORT-OFFICE-LOCATION
	private String reportOfficeLocation = DefaultValues.stringVal(Len.REPORT_OFFICE_LOCATION);

	//==== CONSTRUCTORS ====
	public TaReportNmbrDdName() {
	}

	public TaReportNmbrDdName(TaReportNmbrDdName reportNmbrDdName) {
		this();
		this.reportNmbr = reportNmbrDdName.reportNmbr;
		this.reportOfficeLocation = reportNmbrDdName.reportOfficeLocation;
	}

	//==== METHODS ====
	public String getReportNmbrDdNameFormatted() {
		return MarshalByteExt.bufferToStr(getReportNmbrDdNameBytes());
	}

	public void setReportNmbrDdNameBytes(byte[] buffer) {
		setReportNmbrDdNameBytes(buffer, 1);
	}

	public byte[] getReportNmbrDdNameBytes() {
		byte[] buffer = new byte[Len.REPORT_NMBR_DD_NAME];
		return getReportNmbrDdNameBytes(buffer, 1);
	}

	public void setReportNmbrDdNameBytes(byte[] buffer, int offset) {
		int position = offset;
		reportNmbr = MarshalByte.readString(buffer, position, Len.REPORT_NMBR);
		position += Len.REPORT_NMBR;
		reportOfficeLocation = MarshalByte.readString(buffer, position, Len.REPORT_OFFICE_LOCATION);
	}

	public byte[] getReportNmbrDdNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, reportNmbr, Len.REPORT_NMBR);
		position += Len.REPORT_NMBR;
		MarshalByte.writeString(buffer, position, reportOfficeLocation, Len.REPORT_OFFICE_LOCATION);
		return buffer;
	}

	public void initReportNmbrDdNameHighValues() {
		reportNmbr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REPORT_NMBR);
		reportOfficeLocation = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REPORT_OFFICE_LOCATION);
	}

	public boolean isEndOfReportNmbrs() {
		return Functions.trimAfter(getReportNmbrDdNameFormatted()).equals(END_OF_REPORT_NMBRS);
	}

	public void setReportNmbr(String reportNmbr) {
		this.reportNmbr = Functions.subString(reportNmbr, Len.REPORT_NMBR);
	}

	public String getReportNmbr() {
		return this.reportNmbr;
	}

	public void setReportOfficeLocation(String reportOfficeLocation) {
		this.reportOfficeLocation = Functions.subString(reportOfficeLocation, Len.REPORT_OFFICE_LOCATION);
	}

	public String getReportOfficeLocation() {
		return this.reportOfficeLocation;
	}

	@Override
	public TaReportNmbrDdName copy() {
		return new TaReportNmbrDdName(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REPORT_NMBR = 6;
		public static final int REPORT_OFFICE_LOCATION = 2;
		public static final int REPORT_NMBR_DD_NAME = REPORT_NMBR + REPORT_OFFICE_LOCATION;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
