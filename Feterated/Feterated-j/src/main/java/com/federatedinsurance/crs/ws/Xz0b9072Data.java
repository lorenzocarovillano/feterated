/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IFrmGrpTag;
import com.federatedinsurance.crs.copy.DclfrmGrp;
import com.federatedinsurance.crs.copy.DclfrmTag;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9072<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9072Data implements IFrmGrpTag {

	//==== PROPERTIES ====
	/**Original name: NI-SPE-PRC-CD<br>
	 * <pre>**  NULL-INDICATORS.</pre>*/
	private short niSpePrcCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-END-OF-CURSOR-TAG-FLAG
	private char swEndOfCursorTagFlag = 'N';
	public static final char SW_END_OF_CURSOR_TAG = 'Y';
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9072 workingStorageArea = new WorkingStorageAreaXz0b9072();
	//Original name: WS-XZ0A9072-ROW
	private WsXz0a9072Row wsXz0a9072Row = new WsXz0a9072Row();
	//Original name: DCLFRM-GRP
	private DclfrmGrp dclfrmGrp = new DclfrmGrp();
	//Original name: DCLFRM-TAG
	private DclfrmTag dclfrmTag = new DclfrmTag();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void setNiSpePrcCd(short niSpePrcCd) {
		this.niSpePrcCd = niSpePrcCd;
	}

	public short getNiSpePrcCd() {
		return this.niSpePrcCd;
	}

	public void setSwEndOfCursorTagFlag(char swEndOfCursorTagFlag) {
		this.swEndOfCursorTagFlag = swEndOfCursorTagFlag;
	}

	public char getSwEndOfCursorTagFlag() {
		return this.swEndOfCursorTagFlag;
	}

	public boolean isSwEndOfCursorTag() {
		return swEndOfCursorTagFlag == SW_END_OF_CURSOR_TAG;
	}

	public void setSwEndOfCursorTag() {
		swEndOfCursorTagFlag = SW_END_OF_CURSOR_TAG;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public DclfrmGrp getDclfrmGrp() {
		return dclfrmGrp;
	}

	public DclfrmTag getDclfrmTag() {
		return dclfrmTag;
	}

	@Override
	public char getDelInd() {
		return dclfrmTag.getDelInd();
	}

	@Override
	public void setDelInd(char delInd) {
		this.dclfrmTag.setDelInd(delInd);
	}

	@Override
	public short getDtaGrpFldNbr() {
		return dclfrmGrp.getDtaGrpFldNbr();
	}

	@Override
	public void setDtaGrpFldNbr(short dtaGrpFldNbr) {
		this.dclfrmGrp.setDtaGrpFldNbr(dtaGrpFldNbr);
	}

	@Override
	public String getDtaGrpNm() {
		return dclfrmGrp.getDtaGrpNm();
	}

	@Override
	public void setDtaGrpNm(String dtaGrpNm) {
		this.dclfrmGrp.setDtaGrpNm(dtaGrpNm);
	}

	@Override
	public String getEdlFrmNm() {
		return dclfrmGrp.getEdlFrmNm();
	}

	@Override
	public void setEdlFrmNm(String edlFrmNm) {
		this.dclfrmGrp.setEdlFrmNm(edlFrmNm);
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	@Override
	public char getJusCd() {
		return dclfrmGrp.getJusCd();
	}

	@Override
	public void setJusCd(char jusCd) {
		this.dclfrmGrp.setJusCd(jusCd);
	}

	@Override
	public short getLenNbr() {
		return dclfrmTag.getLenNbr();
	}

	@Override
	public void setLenNbr(short lenNbr) {
		this.dclfrmTag.setLenNbr(lenNbr);
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getSpePrcCd() {
		return dclfrmGrp.getSpePrcCd();
	}

	@Override
	public void setSpePrcCd(String spePrcCd) {
		this.dclfrmGrp.setSpePrcCd(spePrcCd);
	}

	@Override
	public String getSpePrcCdObj() {
		if (getNiSpePrcCd() >= 0) {
			return getSpePrcCd();
		} else {
			return null;
		}
	}

	@Override
	public void setSpePrcCdObj(String spePrcCdObj) {
		if (spePrcCdObj != null) {
			setSpePrcCd(spePrcCdObj);
			setNiSpePrcCd(((short) 0));
		} else {
			setNiSpePrcCd(((short) -1));
		}
	}

	@Override
	public String getTagNm() {
		return dclfrmGrp.getTagNm();
	}

	@Override
	public void setTagNm(String tagNm) {
		this.dclfrmGrp.setTagNm(tagNm);
	}

	@Override
	public short getTagOccCnt() {
		return dclfrmTag.getTagOccCnt();
	}

	@Override
	public void setTagOccCnt(short tagOccCnt) {
		this.dclfrmTag.setTagOccCnt(tagOccCnt);
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b9072 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9072Row getWsXz0a9072Row() {
		return wsXz0a9072Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
