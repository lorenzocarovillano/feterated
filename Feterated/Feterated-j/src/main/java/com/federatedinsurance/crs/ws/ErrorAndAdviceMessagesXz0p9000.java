/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZ0P9000<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class ErrorAndAdviceMessagesXz0p9000 {

	//==== PROPERTIES ====
	//Original name: EA-02-INV-ACT-NOT-TYP-CD
	private Ea01InvActNotTypCd ea02InvActNotTypCd = new Ea01InvActNotTypCd();

	//==== METHODS ====
	public Ea01InvActNotTypCd getEa02InvActNotTypCd() {
		return ea02InvActNotTypCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA01_DATE = 8;
		public static final int EA01_TIME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
