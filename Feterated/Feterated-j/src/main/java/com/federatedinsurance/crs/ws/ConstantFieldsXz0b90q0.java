/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0B90Q0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0b90q0 {

	//==== PROPERTIES ====
	//Original name: CF-ATC-COMMERCIAL-LINES
	private String atcCommercialLines = "CL";
	//Original name: CF-ATC-PERSONAL-LINES
	private String atcPersonalLines = "PL";
	//Original name: CF-FORMAT-ADDRESS-PGM
	private String formatAddressPgm = "TS529099";
	//Original name: CF-PL-PRODUCER-NBR
	private String plProducerNbr = "4-000";
	//Original name: FILLER-CF-PL-PRODUCER-NM
	private String flr1 = "PERSONAL";
	//Original name: FILLER-CF-PL-PRODUCER-NM-1
	private String flr2 = "SERVICE";
	//Original name: FILLER-CF-PL-PRODUCER-NM-2
	private String flr3 = "REPRESENTATIVE";
	//Original name: FILLER-CF-PL-PRODUCER-NM-3
	private String flr4 = "";
	//Original name: CF-RLT-TYP-CD-COWN
	private String rltTypCdCown = "COWN";
	//Original name: CF-SP-GET-INSURED-DETAIL-SVC
	private String spGetInsuredDetailSvc = "MU0X0004";
	//Original name: CF-SP-GET-ACT-CLT-LIST-SVC
	private String spGetActCltListSvc = "MU0X0007";

	//==== METHODS ====
	public String getAtcCommercialLines() {
		return this.atcCommercialLines;
	}

	public String getAtcPersonalLines() {
		return this.atcPersonalLines;
	}

	public String getFormatAddressPgm() {
		return this.formatAddressPgm;
	}

	public String getFormatAddressPgmFormatted() {
		return Functions.padBlanks(getFormatAddressPgm(), Len.FORMAT_ADDRESS_PGM);
	}

	public String getPlProducerNbr() {
		return this.plProducerNbr;
	}

	public String getPlProducerNbrFormatted() {
		return Functions.padBlanks(getPlProducerNbr(), Len.PL_PRODUCER_NBR);
	}

	public String getPlProducerNmFormatted() {
		return MarshalByteExt.bufferToStr(getPlProducerNmBytes());
	}

	/**Original name: CF-PL-PRODUCER-NM<br>*/
	public byte[] getPlProducerNmBytes() {
		byte[] buffer = new byte[Len.PL_PRODUCER_NM];
		return getPlProducerNmBytes(buffer, 1);
	}

	public byte[] getPlProducerNmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getRltTypCdCown() {
		return this.rltTypCdCown;
	}

	public String getSpGetInsuredDetailSvc() {
		return this.spGetInsuredDetailSvc;
	}

	public String getSpGetActCltListSvc() {
		return this.spGetActCltListSvc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PL_PRODUCER_NBR = 5;
		public static final int FLR1 = 9;
		public static final int FLR2 = 8;
		public static final int FLR3 = 14;
		public static final int FLR4 = 89;
		public static final int PL_PRODUCER_NM = FLR1 + FLR2 + FLR3 + FLR4;
		public static final int FORMAT_ADDRESS_PGM = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
