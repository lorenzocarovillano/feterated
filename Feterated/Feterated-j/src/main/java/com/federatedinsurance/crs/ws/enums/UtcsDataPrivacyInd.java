/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UTCS-DATA-PRIVACY-IND<br>
 * Variable: UTCS-DATA-PRIVACY-IND from copybook HALLUTCS<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UtcsDataPrivacyInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char APPLY_DATA_PRIVACY = 'Y';
	public static final char NO_DATA_PRIVACY = 'N';

	//==== METHODS ====
	public void setDataPrivacyInd(char dataPrivacyInd) {
		this.value = dataPrivacyInd;
	}

	public char getDataPrivacyInd() {
		return this.value;
	}

	public boolean isApplyDataPrivacy() {
		return value == APPLY_DATA_PRIVACY;
	}
}
