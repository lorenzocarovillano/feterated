/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program XZ0B9080<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsXz0b9080 {

	//==== PROPERTIES ====
	//Original name: SS-MSG-IDX
	private short msgIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short MSG_IDX_MAX = ((short) 10);
	//Original name: SS-WNG-IDX
	private short wngIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short WNG_IDX_MAX = ((short) 10);
	//Original name: SS-TTY
	private short tty = DefaultValues.BIN_SHORT_VAL;
	public static final short TTY_MAX = ((short) 250);

	//==== METHODS ====
	public void setMsgIdx(short msgIdx) {
		this.msgIdx = msgIdx;
	}

	public short getMsgIdx() {
		return this.msgIdx;
	}

	public boolean isMsgIdxMax() {
		return msgIdx == MSG_IDX_MAX;
	}

	public void setWngIdx(short wngIdx) {
		this.wngIdx = wngIdx;
	}

	public short getWngIdx() {
		return this.wngIdx;
	}

	public boolean isWngIdxMax() {
		return wngIdx == WNG_IDX_MAX;
	}

	public void setTty(short tty) {
		this.tty = tty;
	}

	public short getTty() {
		return this.tty;
	}

	public boolean isTtyMax() {
		return tty == TTY_MAX;
	}
}
