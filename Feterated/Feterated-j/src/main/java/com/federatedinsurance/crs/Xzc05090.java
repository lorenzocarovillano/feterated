/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.lang.collection.IAfSet;
import com.bphx.ctu.af.lang.collection.creation.CollectionCreator;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpAccessStatus;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.date.DateFunctions;
import com.federatedinsurance.crs.copy.Ivoryh;
import com.federatedinsurance.crs.copy.Xz05cl1i;
import com.federatedinsurance.crs.ws.DefaultComm;
import com.federatedinsurance.crs.ws.GetCerAcySta;
import com.federatedinsurance.crs.ws.Ts571cb1;
import com.federatedinsurance.crs.ws.WorkingStorageAreaXzc05090;
import com.federatedinsurance.crs.ws.Xzc05090Data;
import com.federatedinsurance.crs.ws.occurs.Xz005oCerInf;
import com.federatedinsurance.crs.ws.occurs.Xzc05oCerInf;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZC05090<br>
 * <pre>AUTHOR.       B ONSTAD.
 * DATE-WRITTEN. 17 NOV 2015.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - GetCertificateList INTERFACE PROGRAM        **
 * *                                                             **
 * * PURPOSE -  CALLS A CALLABLE WEB SERVICE TO INTERFACE WITH   **
 * *            A CMS WEB SERVICE TO RETRIEVE THE CERTIFICATE    **
 * *            LIST.                                            **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS **
 * *                       LINKED TO BY AN APPLICATION PROGRAM.  **
 * *                                                             **
 * * DATA ACCESS METHODS - CICS LINKAGE                          **
 * *                       DB2 DATABASE                          **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #          DATE      PROG             DESCRIPTION        **
 * * --------   ---------  --------   ---------------------------**
 * * KQ000589   11/17/2015 E404BPO    NEW                        **
 * * 11824      05/12/2016 E404JAL    WSRR MIGRATION.            **
 * * 11824.24   09/08/2016 E404JAL    ADD ADDITIONAL ENVIRONMENTS**
 * *    16388   11/09/2016 E404JAL    SKIP NON-RENEWABLE INACTIVE**
 * *                                  CERTIFICATES               **
 * * 20163      11/05/2018 E404TJJ    ADD ADDITIONAL ENVIRONMENTS**
 * ****************************************************************</pre>*/
public class Xzc05090 extends Program {

	//==== PROPERTIES ====
	//Original name: DEFAULT-COMM
	private DefaultComm defaultComm;
	private ExecContext execContext = null;
	private IAfSet<String> channelSet = CollectionCreator.getStringFactory().createSet();
	//Original name: WORKING-STORAGE
	private Xzc05090Data ws = new Xzc05090Data();

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DefaultComm defaultComm) {
		this.execContext = execContext;
		this.defaultComm = defaultComm;
		mainline();
		exit();
		return 0;
	}

	public static Xzc05090 getInstance() {
		return (Programs.getInstance(Xzc05090.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>*****************************************************************
	 *   MAIN PROCESSING CONTROL
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: PERFORM 3000-GET-CER-LIS
		//              THRU 3000-EXIT.
		getCerLis();
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>****************************************************************
	 *  INITIALIZE OUTPUT AND VERIFY INPUT.
	 * ****************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: PERFORM 2100-READ-FROM-CONTAINER
		//              THRU 2100-EXIT.
		readFromContainer();
		// COB_CODE: SET XZC05O-EC-NO-ERROR      TO TRUE.
		ws.getXzc050c1().getoErrorCode().setOk();
		// COB_CODE: PERFORM 2200-VERIFY-INPUT
		//              THRU 2200-EXIT.
		verifyInput();
		// COB_CODE: PERFORM 2300-DET-CICS-ENV-INF
		//              THRU 2300-EXIT.
		detCicsEnvInf();
		// COB_CODE: PERFORM 2400-DET-TAR-SYS
		//              THRU 2400-EXIT.
		detTarSys();
	}

	/**Original name: 2100-READ-FROM-CONTAINER<br>
	 * <pre>****************************************************************
	 *  READ INPUT FROM CONTAINER.
	 * ****************************************************************</pre>*/
	private void readFromContainer() {
		TpOutputData tsOutputData = null;
		// COB_CODE: EXEC CICS GET CONTAINER(CF-CI-CICS-CONTAINER)
		//               CHANNEL (CF-CI-CICS-CHANNEL)
		//               INTO (GET-CER-LIS-INTF)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc05090Data.Len.GET_CER_LIS_INTF);
		Channel.read(execContext, ws.getConstantFields().getContainerInfo().getCicsChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getCicsContainerFormatted(), tsOutputData);
		ws.setGetCerLisIntfBytes(tsOutputData.getData());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 1000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET XZC05O-EC-SYSTEM-ERROR
			//                                   TO TRUE
			ws.getXzc050c1().getoErrorCode().setFault();
			// COB_CODE: MOVE CF-PN-2100         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2100());
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-06-RESPONSE-CODE
			ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode(ws.getWorkingStorageArea().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-06-RESPONSE-CODE2
			ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode2(ws.getWorkingStorageArea().getResponseCode2());
			// COB_CODE: MOVE EA-06-CONTAINER-ERROR
			//                                   TO EA-01-ERROR-MESSAGE
			ws.getErrorAndAdviceMessages().getEa01Error()
					.setErrorMessage(ws.getErrorAndAdviceMessages().getEa06ContainerError().getEa06ContainerErrorFormatted());
			// COB_CODE: MOVE EA-01-ERROR        TO XZC05O-ERROR-MESSAGE
			ws.getXzc050c1().setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
			// COB_CODE: PERFORM 9800-WRITE-TO-CONTAINER
			//              THRU 9800-EXIT
			writeToContainer();
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 2200-VERIFY-INPUT<br>
	 * <pre>****************************************************************
	 *  VERIFY THAT THE CONSUMER PASSED IN VALID INPUTS.
	 * ****************************************************************
	 *     VERIFY ACCOUNT NUMBER WAS PASSED IN</pre>*/
	private void verifyInput() {
		// COB_CODE: IF XZC05I-ACT-NBR = SPACES
		//             OR
		//              XZC05I-ACT-NBR = LOW-VALUES
		//                  THRU 9100-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc050c1().getiActNbr()) || Characters.EQ_LOW.test(ws.getXzc050c1().getiActNbrFormatted())) {
			// COB_CODE: MOVE CF-PN-2200         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2200());
			// COB_CODE: PERFORM 9100-INVALID-INPUT-ERROR
			//              THRU 9100-EXIT
			invalidInputError();
		}
	}

	/**Original name: 2300-DET-CICS-ENV-INF<br>
	 * <pre>****************************************************************
	 *  GET THE CURRENT CICS REGION TO DETERMINE TARGET SYSTEM.
	 * ****************************************************************</pre>*/
	private void detCicsEnvInf() {
		// COB_CODE: EXEC CICS ASSIGN
		//               APPLID(WS-CICS-APPLID)
		//           END-EXEC.
		ws.getWorkingStorageArea().getCicsApplid().setCicsApplid(execContext.getApplicationId());
		execContext.clearStatus();
	}

	/**Original name: 2400-DET-TAR-SYS<br>
	 * <pre>*****************************************************************
	 *  GET TARGET SYSTEM BASED ON CURRENT CICS REGION
	 * *****************************************************************</pre>*/
	private void detTarSys() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN WS-CA-DEV1
		//                   SET WS-TS-DEV1      TO TRUE
		//               WHEN WS-CA-DEV2
		//                   SET WS-TS-DEV2      TO TRUE
		//               WHEN WS-CA-DEV6
		//                   SET WS-TS-DEV6      TO TRUE
		//               WHEN WS-CA-BETA1
		//                   SET WS-TS-BETA1     TO TRUE
		//               WHEN WS-CA-BETA2
		//                   SET WS-TS-BETA2     TO TRUE
		//               WHEN WS-CA-BETA8
		//                   SET WS-TS-BETA8     TO TRUE
		//               WHEN WS-CA-EDUC1
		//                   SET WS-TS-EDUC1     TO TRUE
		//               WHEN WS-CA-EDUC2
		//                   SET WS-TS-EDUC2     TO TRUE
		//               WHEN WS-CA-PROD
		//                   SET WS-TS-PROD      TO TRUE
		//               WHEN OTHER
		//                      THRU 9600-EXIT
		//           END-EVALUATE.
		if (ws.getWorkingStorageArea().getCicsApplid().isDev1()) {
			// COB_CODE: SET WS-TS-DEV1      TO TRUE
			ws.getWorkingStorageArea().getTarSys().setDev1();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isDev2()) {
			// COB_CODE: SET WS-TS-DEV2      TO TRUE
			ws.getWorkingStorageArea().getTarSys().setDev2();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isDev6()) {
			// COB_CODE: SET WS-TS-DEV6      TO TRUE
			ws.getWorkingStorageArea().getTarSys().setDev6();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isBeta1()) {
			// COB_CODE: SET WS-TS-BETA1     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setBeta1();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isBeta2()) {
			// COB_CODE: SET WS-TS-BETA2     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setBeta2();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isBeta8()) {
			// COB_CODE: SET WS-TS-BETA8     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setBeta8();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isEduc1()) {
			// COB_CODE: SET WS-TS-EDUC1     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setEduc1();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isEduc2()) {
			// COB_CODE: SET WS-TS-EDUC2     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setEduc2();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isProd()) {
			// COB_CODE: SET WS-TS-PROD      TO TRUE
			ws.getWorkingStorageArea().getTarSys().setProd();
		} else {
			// COB_CODE: MOVE CF-PN-2400     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2400());
			// COB_CODE: PERFORM 9600-CICS-APPLID-ERROR
			//              THRU 9600-EXIT
			cicsApplidError();
		}
	}

	/**Original name: 3000-GET-CER-LIS<br>
	 * <pre>*****************************************************************
	 *  CALL THE GetCertificateListSvcCallable SERVICE TO RETRIEVE
	 *  THE CERTIFICATE LIST.
	 * *****************************************************************</pre>*/
	private void getCerLis() {
		// COB_CODE: PERFORM 3100-INIT-WEB-SVC-CALL
		//              THRU 3100-EXIT.
		initWebSvcCall();
		// COB_CODE: IF NOT XZC05O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!ws.getXzc050c1().getoErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3200-MOVE-WEB-SVC-INPUT
		//              THRU 3200-EXIT.
		moveWebSvcInput();
		// COB_CODE: IF NOT XZC05O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!ws.getXzc050c1().getoErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3500-CONTAINER-SETUP
		//              THRU 3500-EXIT.
		containerSetup();
		// COB_CODE: IF NOT XZC05O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!ws.getXzc050c1().getoErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3300-LINK-TO-IVORY
		//              THRU 3300-EXIT.
		linkToIvory();
		// COB_CODE: IF NOT XZC05O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!ws.getXzc050c1().getoErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3400-MOVE-WEB-SVC-OUTPUT
		//              THRU 3400-EXIT.
		moveWebSvcOutput();
		// COB_CODE: IF NOT XZC05O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!ws.getXzc050c1().getoErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3100-INIT-WEB-SVC-CALL<br>
	 * <pre>*****************************************************************
	 *  INITIAL SETUP FOR CALLING IVORY WEB SERVICE PROGRAM
	 * *****************************************************************</pre>*/
	private void initWebSvcCall() {
		// COB_CODE: MOVE LOW-VALUES             TO NON-STATIC-FIELDS
		//                                          CALLABLE-INPUTS
		//                                          CALLABLE-OUTPUTS.
		ws.getIvoryh().getNonStaticFields().initNonStaticFieldsLowValues();
		ws.initCallableInputsLowValues();
		ws.initCallableOutputsLowValues();
		// COB_CODE: MOVE CF-SOAP-SERVICE        TO SOAP-SERVICE IN IVORYH.
		ws.getIvoryh().getNonStaticFields().setService(ws.getConstantFields().getSoapService());
		// COB_CODE: MOVE CF-SOAP-OPERATION      TO SOAP-OPERATION IN IVORYH.
		ws.getIvoryh().getNonStaticFields().setOperation(ws.getConstantFields().getSoapOperation());
		// COB_CODE: MOVE CF-LV-ZLINUX-SRV       TO SOAP-LAYOUT-VERSION.
		ws.getIvoryh().setSoapLayoutVersion(ws.getConstantFields().getLvZlinuxSrv());
		// COB_CODE: SET SOAP-TRACE-STOR-CHARSET-EBCDIC
		//               SOAP-REQUEST-STOR-TYPE-CHCONT
		//               SOAP-RESPONSE-STOR-TYPE-CHCONT
		//               SOAP-TRACE-STOR-TYPE-TDQ
		//               SOAP-ERROR-STOR-TYPE-TDQ
		//                                       TO TRUE.
		ws.getIvoryh().getNonStaticFields().getTraceStorCharset().setEbcdic();
		ws.getIvoryh().getNonStaticFields().getRequestStorType().setChcont();
		ws.getIvoryh().getNonStaticFields().getResponseStorType().setChcont();
		ws.getIvoryh().getNonStaticFields().getTraceStorType().setTdq();
		ws.getIvoryh().getNonStaticFields().getErrorStorType().setTdq();
		// COB_CODE: MOVE CF-CI-INP-CONTAINER    TO SOAP-REQUEST-STORE-VALUE.
		ws.getIvoryh().getNonStaticFields().setRequestStoreValue(ws.getConstantFields().getContainerInfo().getInpContainer());
		// COB_CODE: MOVE CF-CI-OUP-CONTAINER    TO SOAP-RESPONSE-STORE-VALUE.
		ws.getIvoryh().getNonStaticFields().setResponseStoreValue(ws.getConstantFields().getContainerInfo().getOupContainer());
		// COB_CODE: MOVE LENGTH OF CALLABLE-INPUTS
		//                                       TO SOAP-REQUEST-LENGTH.
		ws.getIvoryh().getNonStaticFields().setRequestLength(Xzc05090Data.Len.CALLABLE_INPUTS);
		// COB_CODE: MOVE LENGTH OF CALLABLE-OUTPUTS
		//                                       TO SOAP-RESPONSE-LENGTH.
		ws.getIvoryh().getNonStaticFields().setResponseLength(Xzc05090Data.Len.CALLABLE_OUTPUTS);
		// COB_CODE: MOVE CF-TRACE-STOR-VALUE    TO SOAP-TRACE-STOR-VALUE
		//                                          SOAP-ERROR-STOR-VALUE.
		ws.getIvoryh().getNonStaticFields().setTraceStorValue(ws.getConstantFields().getTraceStorValue());
		ws.getIvoryh().getNonStaticFields().setErrorStorValue(ws.getConstantFields().getTraceStorValue());
	}

	/**Original name: 3200-MOVE-WEB-SVC-INPUT<br>
	 * <pre>*****************************************************************
	 *  SETUP INPUT FOR THE SERVICE CALL TO IVORY
	 * *****************************************************************</pre>*/
	private void moveWebSvcInput() {
		// COB_CODE: INITIALIZE CALLABLE-INPUTS.
		initCallableInputs();
		// COB_CODE: PERFORM 3220-RETRIEVE-SVC-URL
		//              THRU 3220-EXIT.
		retrieveSvcUrl();
		// COB_CODE: MOVE XZC05I-ACT-NBR         TO XZ005I-ACT-NBR.
		ws.getXz05cl1i().setActNbr(ws.getXzc050c1().getiActNbr());
		// COB_CODE: MOVE WS-WEB-SVC-URL         TO XZ005I-TK-URI.
		ws.getFwppcstk().setTkUri(ws.getWorkingStorageArea().getWebSvcUrl());
		// COB_CODE: PERFORM 3210-MOVE-SER-CRT
		//              THRU 3210-EXIT.
		rng3210MoveSerCrt();
		// COB_CODE: PERFORM 3230-RETRIEVE-USR-ID-PWD
		//              THRU 3230-EXIT.
		retrieveUsrIdPwd();
		// COB_CODE: MOVE WS-UP-USR-ID           TO XZ005I-TK-USERID.
		ws.getFwppcstk().setTkUserid(ws.getWorkingStorageArea().getUpUsrId());
		// COB_CODE: MOVE WS-UP-PWD              TO XZ005I-TK-PASSWORD.
		ws.getFwppcstk().setTkPassword(ws.getWorkingStorageArea().getUpPwd());
		// COB_CODE: MOVE WS-TAR-SYS             TO XZ005I-TK-FED-TAR-SYS.
		ws.getFwppcstk().setTkFedTarSys(ws.getWorkingStorageArea().getTarSys().getTarSys());
	}

	/**Original name: 3210-MOVE-SER-CRT<br>
	 * <pre>*****************************************************************
	 *  MOVE SEARCH CRITERIA.
	 * *****************************************************************</pre>*/
	private void moveSerCrt() {
		// COB_CODE: MOVE +0                     TO SS-SC.
		ws.getSubscripts().setTp(((short) 0));
	}

	/**Original name: 3210-A<br>*/
	private String a() {
		// COB_CODE: ADD +1                      TO SS-SC.
		ws.getSubscripts().setTp(Trunc.toShort(1 + ws.getSubscripts().getTp(), 4));
		// COB_CODE: IF SS-SC > CF-MAX-STA-CD
		//             OR
		//              XZC05I-STA-CD(SS-SC) = SPACES
		//               GO TO 3210-EXIT
		//           END-IF.
		if (ws.getSubscripts().getTp() > ws.getConstantFields().getMaxStaCd()
				|| Characters.EQ_SPACE.test(ws.getXzc050c1().getiStaCdLis(ws.getSubscripts().getTp()).getXzc05iStaCd())) {
			// COB_CODE: GO TO 3210-EXIT
			return "";
		}
		// COB_CODE: MOVE XZC05I-STA-CD(SS-SC)   TO XZ005I-STA-CD(SS-SC).
		ws.getXz05cl1i().getStaCdLis(ws.getSubscripts().getTp())
				.setXz005iStaCd(ws.getXzc050c1().getiStaCdLis(ws.getSubscripts().getTp()).getXzc05iStaCd());
		// COB_CODE: GO TO 3210-A.
		return "3210-A";
	}

	/**Original name: 3220-RETRIEVE-SVC-URL<br>
	 * <pre>*****************************************************************
	 *  CALL THE WEB SERVICE URI LOOKUP COMMON ROUTINE
	 * *****************************************************************</pre>*/
	private void retrieveSvcUrl() {
		// COB_CODE: INITIALIZE URI-LKU-LINKAGE.
		initTs571cb1();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO UL-SERVICE-MNEMONIC.
		ws.getTs571cb1().setServiceMnemonic(ws.getConstantFields().getWebSvcId());
		// CALL WEB SERVICE LOOKUP INTERFACE PROGRAM
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (CF-WEB-SVC-LKU-PGM)
		//               COMMAREA (URI-LKU-LINKAGE)
		//               LENGTH   (LENGTH OF URI-LKU-LINKAGE)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC05090", execContext).commarea(ws.getTs571cb1()).length(Ts571cb1.Len.TS571CB1)
				.link(ws.getConstantFields().getWebSvcLkuPgm(), new Ts571099());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// CHECK FOR ERROR FROM CICS LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3220         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3220());
			// COB_CODE: MOVE CF-WEB-SVC-LKU-PGM TO EA-03-FAILED-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getConstantFields().getWebSvcLkuPgm());
			// COB_CODE: PERFORM 9110-CICS-LINK-ERROR
			//              THRU 9110-EXIT
			cicsLinkError();
		}
		// CHECK IF RETURNING URI IS NOT VALID
		// COB_CODE: IF UL-URI = SPACES
		//             OR
		//              UL-URI = LOW-VALUES
		//                  THRU 9130-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getTs571cb1().getUri()) || Characters.EQ_LOW.test(ws.getTs571cb1().getUri(), Ts571cb1.Len.URI)) {
			// COB_CODE: MOVE CF-PN-3220         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3220());
			// COB_CODE: PERFORM 9130-WEB-URI-NOT-FOUND
			//              THRU 9130-EXIT
			webUriNotFound();
		}
		// COB_CODE: MOVE UL-URI                 TO WS-WEB-SVC-URL.
		ws.getWorkingStorageArea().setWebSvcUrl(ws.getTs571cb1().getUri());
	}

	/**Original name: 3230-RETRIEVE-USR-ID-PWD<br>
	 * <pre>*****************************************************************
	 *  READ THE QUEUE TO GET THE USERID AND PASSWORD.
	 * *****************************************************************</pre>*/
	private void retrieveUsrIdPwd() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS
		//               READQ TS
		//               ITEM (1)
		//               QNAME ('UIDPCRS1')
		//               INTO (WS-USR-ID-PWD)
		//               RESP (WS-RESPONSE-CODE)
		//               RESP2 (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(WorkingStorageAreaXzc05090.Len.USR_ID_PWD);
		TsQueueManager.read(execContext, "UIDPCRS1", ((short) 1), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.getWorkingStorageArea().setUsrIdPwdBytes(tsQueueData.getData());
		}
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9140-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3230         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3230());
			// COB_CODE: PERFORM 9140-CICS-READQ-ERROR
			//              THRU 9140-EXIT
			cicsReadqError();
		}
	}

	/**Original name: 3300-LINK-TO-IVORY<br>
	 * <pre>*****************************************************************
	 *  CALL THE IVORY PROCESSING PROGRAM
	 * *****************************************************************</pre>*/
	private void linkToIvory() {
		TpOutputData tsOutputData = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-CALLABLE-SVC-PROGRAM)
		//               CHANNEL(CF-CI-CER-LIS-CHANNEL)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC05090", execContext).channel(ws.getConstantFields().getContainerInfo().getCerLisChannelFormatted())
				.link(ws.getConstantFields().getCallableSvcProgram());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
			// COB_CODE: MOVE CF-CALLABLE-SVC-PROGRAM
			//                                   TO EA-03-FAILED-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getConstantFields().getCallableSvcProgram());
			// COB_CODE: PERFORM 9110-CICS-LINK-ERROR
			//              THRU 9110-EXIT
			cicsLinkError();
		}
		//    In order to do any further error checking, we must get the
		//    containers.
		// COB_CODE: EXEC CICS GET CONTAINER(CF-CI-IVORYH-CONTAINER)
		//               CHANNEL    (CF-CI-CER-LIS-CHANNEL)
		//               INTO       (IVORYH)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Ivoryh.Len.IVORYH);
		Channel.read(execContext, ws.getConstantFields().getContainerInfo().getCerLisChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getIvoryhContainerFormatted(), tsOutputData);
		ws.getIvoryh().setIvoryhBytes(tsOutputData.getData());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: EXEC CICS GET CONTAINER(CF-CI-OUP-CONTAINER)
		//               CHANNEL    (CF-CI-CER-LIS-CHANNEL)
		//               INTO       (CALLABLE-OUTPUTS)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc05090Data.Len.CALLABLE_OUTPUTS);
		Channel.read(execContext, ws.getConstantFields().getContainerInfo().getCerLisChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getOupContainerFormatted(), tsOutputData);
		ws.setCallableOutputsBytes(tsOutputData.getData());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: IF SOAP-RETURNCODE NOT = ZERO
		//                  THRU 9120-EXIT
		//           END-IF.
		if (ws.getIvoryh().getNonStaticFields().getReturncode() != 0) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
			// COB_CODE: PERFORM 9120-SOAP-FAULT-ERROR
			//              THRU 9120-EXIT
			soapFaultError();
		}
	}

	/**Original name: 3400-MOVE-WEB-SVC-OUTPUT<br>
	 * <pre>*****************************************************************
	 *  PROCESS RESULTS.
	 * *****************************************************************</pre>*/
	private void moveWebSvcOutput() {
		// COB_CODE: INITIALIZE XZC050-PROGRAM-OUTPUT.
		initXzc050ProgramOutput();
		// COB_CODE: PERFORM 3410-MOVE-CER-INF
		//              THRU 3410-EXIT.
		rng3410MoveCerInf();
		// COB_CODE: PERFORM 9800-WRITE-TO-CONTAINER
		//              THRU 9800-EXIT.
		writeToContainer();
	}

	/**Original name: 3410-MOVE-CER-INF<br>
	 * <pre>*****************************************************************
	 *  MOVE CERTIFICATE INFO TO THE OUTPUT AREA
	 * *****************************************************************</pre>*/
	private void moveCerInf() {
		// COB_CODE: MOVE +0                     TO SS-CI
		//                                          SS-CO.
		ws.getSubscripts().setTpOrig(((short) 0));
		ws.getSubscripts().setTpRedir(((short) 0));
	}

	/**Original name: 3410-A<br>*/
	private String a1() {
		// COB_CODE: ADD +1                      TO SS-CI.
		ws.getSubscripts().setTpOrig(Trunc.toShort(1 + ws.getSubscripts().getTpOrig(), 4));
		// COB_CODE: IF SS-CI > CF-MAX-CER-INF
		//             OR
		//              XZ005O-CER-NBR(SS-CI) = SPACES
		//               GO TO 3410-EXIT
		//           END-IF.
		if (ws.getSubscripts().getTpOrig() > ws.getConstantFields().getMaxCerInf()
				|| Characters.EQ_SPACE.test(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerNbr())) {
			// COB_CODE: GO TO 3410-EXIT
			return "";
		}
		//    IF THE CERTIFICATE IS NON-RENEWABLE CHECK IF IT IS ACTIVE
		// COB_CODE: IF XZ005O-CERT-RNL-STATUS-CD(SS-CI) = CF-NO-RENEW
		//               END-IF
		//           END-IF.
		if (ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCertRnlStatusCd() == ws.getConstantFields().getNoRenew()) {
			// COB_CODE: PERFORM 3413-SET-NOT-DT
			//              THRU 3413-EXIT
			setNotDt();
			// COB_CODE: PERFORM 3412-CHK-CER-ACY-STA
			//              THRU 3412-EXIT
			chkCerAcySta();
			// COB_CODE: IF XWC01O-CER-NOT-ACY
			//              GO TO 3410-A
			//           END-IF
			if (ws.getGetCerAcySta().getoCerAcyInd().isNotAcy()) {
				// COB_CODE: GO TO 3410-A
				return "3410-A";
			}
		}
		// COB_CODE: ADD +1                      TO SS-CO.
		ws.getSubscripts().setTpRedir(Trunc.toShort(1 + ws.getSubscripts().getTpRedir(), 4));
		// COB_CODE: MOVE XZ005O-STA-CD(SS-CI)   TO XZC05O-STA-CD(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir()).setStaCd(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getStaCd());
		// COB_CODE: MOVE XZ005O-CER-NBR(SS-CI)  TO XZC05O-CER-NBR(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir()).setCerNbr(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerNbr());
		// COB_CODE: MOVE XZ005O-ACY-TYP-CD(SS-CI)
		//                                       TO XZC05O-ACY-TYP-CD(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir()).setAcyTypCd(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getAcyTypCd());
		// COB_CODE: MOVE XZ005O-CER-HLD-ID(SS-CI)
		//                                       TO XZC05O-CER-HLD-ID(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir()).setCerHldId(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerHldId());
		// COB_CODE: MOVE XZ005O-CER-HLD-NM(SS-CI)
		//                                       TO XZC05O-CER-HLD-NM(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir()).setCerHldNm(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerHldNm());
		// COB_CODE: MOVE XZ005O-OTH-INF-TYP-CD(SS-CI)
		//                                       TO XZC05O-OTH-INF-TYP-CD(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir())
				.setOthInfTypCd(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getOthInfTypCd());
		// COB_CODE: MOVE XZ005O-OTH-INF-PFX(SS-CI)
		//                                       TO XZC05O-OTH-INF-PFX(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir()).setOthInfPfx(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getOthInfPfx());
		// COB_CODE: MOVE XZ005O-OTH-INF-TXT(SS-CI)
		//                                       TO XZC05O-OTH-INF-TXT(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir()).setOthInfTxt(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getOthInfTxt());
		// COB_CODE: MOVE XZ005O-CER-HLD-ADR-ID(SS-CI)
		//                                       TO XZC05O-CER-HLD-ADR-ID(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir())
				.setCerHldAdrId(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerHldAdrId());
		// COB_CODE: MOVE XZ005O-CER-HLD-ADR1(SS-CI)
		//                                       TO XZC05O-CER-HLD-ADR1(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir())
				.setCerHldAdr1(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerHldAdr1());
		// COB_CODE: MOVE XZ005O-CER-HLD-ADR2(SS-CI)
		//                                       TO XZC05O-CER-HLD-ADR2(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir())
				.setCerHldAdr2(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerHldAdr2());
		// COB_CODE: MOVE XZ005O-CER-HLD-CIT(SS-CI)
		//                                       TO XZC05O-CER-HLD-CIT(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir()).setCerHldCit(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerHldCit());
		// COB_CODE: MOVE XZ005O-CER-HLD-ST-ABB(SS-CI)
		//                                       TO XZC05O-CER-HLD-ST-ABB(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir())
				.setCerHldStAbb(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerHldStAbb());
		// COB_CODE: MOVE XZ005O-CER-HLD-PST-CD(SS-CI)
		//                                       TO XZC05O-CER-HLD-PST-CD(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir())
				.setCerHldPstCd(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerHldPstCd());
		// COB_CODE: MOVE XZ005O-CER-HLD-ADD-INS-IND(SS-CI)
		//                                       TO
		//                                      XZC05O-CER-HLD-ADD-INS-IND(SS-CO).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir())
				.setCerHldAddInsInd(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerHldAddInsInd());
		// COB_CODE: PERFORM 3411-MOVE-WRD-INF
		//              THRU 3411-EXIT.
		rng3411MoveWrdInf();
		// COB_CODE: GO TO 3410-A.
		return "3410-A";
	}

	/**Original name: 3411-MOVE-WRD-INF<br>
	 * <pre>*****************************************************************
	 *  MOVE WORDING INFO TO THE OUTPUT AREA
	 * *****************************************************************</pre>*/
	private void moveWrdInf() {
		// COB_CODE: MOVE +0                     TO SS-WI.
		ws.getSubscripts().setTpTemp(((short) 0));
	}

	/**Original name: 3411-A<br>*/
	private String a2() {
		// COB_CODE: ADD +1                      TO SS-WI.
		ws.getSubscripts().setTpTemp(Trunc.toShort(1 + ws.getSubscripts().getTpTemp(), 4));
		// COB_CODE: IF SS-WI > CF-MAX-WRD-INF
		//             OR
		//              XZ005O-WRD-NM(SS-CI,SS-WI) = SPACES
		//               GO TO 3411-EXIT
		//           END-IF.
		if (ws.getSubscripts().getTpTemp() > ws.getConstantFields().getMaxWrdInf() || Characters.EQ_SPACE
				.test(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getWrdInf(ws.getSubscripts().getTpTemp()).getXz005oWrdNm())) {
			// COB_CODE: GO TO 3411-EXIT
			return "";
		}
		// COB_CODE: MOVE XZ005O-WRD-NM(SS-CI,SS-WI)
		//                                       TO XZC05O-WRD-NM(SS-CO,SS-WI).
		ws.getXzc050c1().getoCerInf(ws.getSubscripts().getTpRedir()).getWrdInf(ws.getSubscripts().getTpTemp())
				.setXzc05oWrdNm(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getWrdInf(ws.getSubscripts().getTpTemp()).getXz005oWrdNm());
		// COB_CODE: GO TO 3411-A.
		return "3411-A";
	}

	/**Original name: 3412-CHK-CER-ACY-STA<br>
	 * <pre>*****************************************************************
	 *  CHECK IF CERTIFICATE IS EXPIRED
	 * *****************************************************************</pre>*/
	private void chkCerAcySta() {
		// COB_CODE: MOVE XZ005I-ACT-NBR         TO XWC01I-ACT-NBR.
		ws.getGetCerAcySta().setiActNbr(ws.getXz05cl1i().getActNbr());
		// COB_CODE: MOVE WS-NOT-DT              TO XWC01I-NOT-DT.
		ws.getGetCerAcySta().setiNotDt(ws.getWorkingStorageArea().getNotDt());
		// COB_CODE: MOVE XZ005O-CER-NBR(SS-CI)  TO XWC01I-CER-NBR.
		ws.getGetCerAcySta().setiCerNbr(ws.getXz005oCerInf(ws.getSubscripts().getTpOrig()).getCerNbr());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM     (CF-CHK-STA-PGM)
		//               COMMAREA    (GET-CER-ACY-STA)
		//               LENGTH      (LENGTH OF GET-CER-ACY-STA)
		//               RESP        (WS-RESPONSE-CODE)
		//               RESP2       (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC05090", execContext).commarea(ws.getGetCerAcySta()).length(GetCerAcySta.Len.GET_CER_ACY_STA)
				.link(ws.getConstantFields().getChkStaPgm(), new Xwc01090());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-CHK-STA-PGM     TO EA-03-FAILED-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getConstantFields().getChkStaPgm());
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-03-RESPONSE-CODE
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode(ws.getWorkingStorageArea().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-03-RESPONSE-CODE2
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode2(ws.getWorkingStorageArea().getResponseCode2());
			// COB_CODE: PERFORM 9110-CICS-LINK-ERROR
			//              THRU 9110-EXIT
			cicsLinkError();
		}
	}

	/**Original name: 3413-SET-NOT-DT<br>
	 * <pre>******************************************************
	 *  GET NOTIFICATION DATE FOR NOTIFICATION TIMESTAMP    *
	 * ******************************************************</pre>*/
	private void setNotDt() {
		// COB_CODE: IF XZC05I-NOT-PRC-TS = SPACES
		//               GO TO 3413-EXIT
		//           ELSE
		//               MOVE XZC05I-NOT-PRC-TS  TO WS-NOT-DT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getXzc050c1().getiNotPrcTs())) {
			// COB_CODE: MOVE FUNCTION CURRENT-DATE
			//                                   TO WS-CURRENT-DATE
			ws.getWorkingStorageArea().getCurrentDate().setCurrentDateFormatted(DateFunctions.getCurrentDate());
			// COB_CODE: MOVE SPACES TO WS-NOT-DT
			//skipped translation for moving SPACES to WS-NOT-DT; considered in STRING statement translation below
			// COB_CODE: STRING WS-CURRENT-DATE-YYYY
			//                  CF-DASH
			//                  WS-CURRENT-DATE-MM
			//                  CF-DASH
			//                  WS-CURRENT-DATE-DD
			//                  DELIMITED BY SIZE
			//             INTO WS-NOT-DT
			//           END-STRING
			ws.getWorkingStorageArea().setNotDt(new StringBuffer(256).append(ws.getWorkingStorageArea().getCurrentDate().getYyyyFormatted())
					.append(String.valueOf(ws.getConstantFields().getDash())).append(ws.getWorkingStorageArea().getCurrentDate().getMmFormatted())
					.append(String.valueOf(ws.getConstantFields().getDash())).append(ws.getWorkingStorageArea().getCurrentDate().getDd()).toString());
			// COB_CODE: GO TO 3413-EXIT
			return;
		} else {
			// COB_CODE: MOVE XZC05I-NOT-PRC-TS  TO WS-NOT-DT
			ws.getWorkingStorageArea().setNotDt(ws.getXzc050c1().getiNotPrcTs());
		}
	}

	/**Original name: 3500-CONTAINER-SETUP<br>
	 * <pre>*****************************************************************
	 *  SETUP THE CONTAINERS FOR CALLING GIICALS
	 * *****************************************************************</pre>*/
	private void containerSetup() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE CF-PN-3500             TO EA-01-PARAGRAPH-NBR.
		ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3500());
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-IVORYH-CONTAINER)
		//               CHANNEL    (CF-CI-CER-LIS-CHANNEL)
		//               FROM       (IVORYH)
		//               FLENGTH    (LENGTH OF IVORYH)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Ivoryh.Len.IVORYH);
		tsOutputData.setData(ws.getIvoryh().getIvoryhBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCerLisChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getIvoryhContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCerLisChannelFormatted());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-INP-CONTAINER)
		//               CHANNEL    (CF-CI-CER-LIS-CHANNEL)
		//               FROM       (CALLABLE-INPUTS)
		//               FLENGTH    (LENGTH OF CALLABLE-INPUTS)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc05090Data.Len.CALLABLE_INPUTS);
		tsOutputData.setData(ws.getCallableInputsBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCerLisChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getInpContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCerLisChannelFormatted());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: INITIALIZE CALLABLE-OUTPUTS.
		initCallableOutputs();
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-OUP-CONTAINER)
		//               CHANNEL    (CF-CI-CER-LIS-CHANNEL)
		//               FROM       (CALLABLE-OUTPUTS)
		//               FLENGTH    (LENGTH OF CALLABLE-OUTPUTS)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc05090Data.Len.CALLABLE_OUTPUTS);
		tsOutputData.setData(ws.getCallableOutputsBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCerLisChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getOupContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCerLisChannelFormatted());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
	}

	/**Original name: 9100-INVALID-INPUT-ERROR<br>
	 * <pre>*****************************************************************
	 *  INVALID INPUT ERROR
	 * *****************************************************************</pre>*/
	private void invalidInputError() {
		// COB_CODE: SET XZC05O-EC-INVALID-INPUT TO TRUE.
		ws.getXzc050c1().getoErrorCode().setInvalidInput();
		// COB_CODE: MOVE XZC05I-ACT-NBR         TO EA-02-ACCOUNT-NUMBER.
		ws.getErrorAndAdviceMessages().getEa02InvalidInput().setAccountNumber(ws.getXzc050c1().getiActNbr());
		// COB_CODE: MOVE EA-02-INVALID-INPUT    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa02InvalidInput().getEa02InvalidInputFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZC05O-ERROR-MESSAGE.
		ws.getXzc050c1().setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9800-WRITE-TO-CONTAINER
		//              THRU 9800-EXIT.
		writeToContainer();
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9110-CICS-LINK-ERROR<br>
	 * <pre>*****************************************************************
	 *  CICS LINK ERROR
	 * *****************************************************************</pre>*/
	private void cicsLinkError() {
		// COB_CODE: SET XZC05O-EC-SYSTEM-ERROR  TO TRUE.
		ws.getXzc050c1().getoErrorCode().setFault();
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EA-03-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode(ws.getWorkingStorageArea().getResponseCode());
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EA-03-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode2(ws.getWorkingStorageArea().getResponseCode2());
		// COB_CODE: MOVE EA-03-CICS-LINK-ERROR  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa03CicsLinkError().getEa03CicsLinkErrorFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZC05O-ERROR-MESSAGE.
		ws.getXzc050c1().setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9800-WRITE-TO-CONTAINER
		//              THRU 9800-EXIT.
		writeToContainer();
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9120-SOAP-FAULT-ERROR<br>
	 * <pre>*****************************************************************
	 *  SOAP FAULT ERROR
	 * *****************************************************************</pre>*/
	private void soapFaultError() {
		// COB_CODE: SET XZC05O-EC-SOAP-FAULT    TO TRUE.
		ws.getXzc050c1().getoErrorCode().setSystemError();
		// COB_CODE: MOVE SOAP-RETURNCODE        TO EA-04-SOAP-RETURN-CODE.
		ws.getErrorAndAdviceMessages().getEa04SoapFault().setEa04SoapReturnCode(((int) (ws.getIvoryh().getNonStaticFields().getReturncode())));
		// COB_CODE: MOVE EA-04-SOAP-FAULT       TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error().setErrorMessage(ws.getErrorAndAdviceMessages().getEa04SoapFault().getEa04SoapFaultFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZC05O-ERROR-MESSAGE.
		ws.getXzc050c1().setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9800-WRITE-TO-CONTAINER
		//              THRU 9800-EXIT.
		writeToContainer();
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9130-WEB-URI-NOT-FOUND<br>
	 * <pre>*****************************************************************
	 *  SOAP FAULT ERROR
	 * *****************************************************************</pre>*/
	private void webUriNotFound() {
		// COB_CODE: SET XZC05O-EC-INVALID-INPUT TO TRUE.
		ws.getXzc050c1().getoErrorCode().setInvalidInput();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO EA-05-WEB-SRV-ID.
		ws.getErrorAndAdviceMessages().getEa05WebUriFault().setEa05WebSvcId(ws.getConstantFields().getWebSvcId());
		// COB_CODE: MOVE EA-05-WEB-URI-FAULT    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa05WebUriFault().getEa05WebUriFaultFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZC05O-ERROR-MESSAGE.
		ws.getXzc050c1().setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9800-WRITE-TO-CONTAINER
		//              THRU 9800-EXIT.
		writeToContainer();
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9140-CICS-READQ-ERROR<br>
	 * <pre>*****************************************************************
	 *  CICS READQ ERROR
	 * *****************************************************************</pre>*/
	private void cicsReadqError() {
		// COB_CODE: SET XZC05O-EC-SYSTEM-ERROR  TO TRUE.
		ws.getXzc050c1().getoErrorCode().setFault();
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EA-07-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa07ReadqError().setCode(ws.getWorkingStorageArea().getResponseCode());
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EA-07-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa07ReadqError().setCode2(ws.getWorkingStorageArea().getResponseCode2());
		// COB_CODE: MOVE EA-07-READQ-ERROR      TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa07ReadqError().getEa07ReadqErrorFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZC05O-ERROR-MESSAGE.
		ws.getXzc050c1().setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9600-CICS-APPLID-ERROR<br>
	 * <pre>*****************************************************************
	 *  CALL TO DATAFLUX SERVICE HAD AN ERROR
	 * *****************************************************************</pre>*/
	private void cicsApplidError() {
		// COB_CODE: SET XZC05O-EC-SYSTEM-ERROR  TO TRUE.
		ws.getXzc050c1().getoErrorCode().setFault();
		// COB_CODE: MOVE WS-CICS-APPLID         TO EA-08-CICS-RGN.
		ws.getErrorAndAdviceMessages().getEa08TarSysNotFnd().setEa08CicsRgn(ws.getWorkingStorageArea().getCicsApplid().getCicsApplid());
		// COB_CODE: MOVE EA-08-TAR-SYS-NOT-FND  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa08TarSysNotFnd().getEa08TarSysNotFndFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZC05O-ERROR-MESSAGE.
		ws.getXzc050c1().setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9800-WRITE-TO-CONTAINER<br>
	 * <pre>******************************************************
	 *  WRITE INFORMATION OUT TO CONTAINER                  *
	 * ******************************************************</pre>*/
	private void writeToContainer() {
		TpOutputData tsOutputData = null;
		// COB_CODE: EXEC CICS PUT CONTAINER(CF-CI-CICS-CONTAINER)
		//               CHANNEL (CF-CI-CICS-CHANNEL)
		//               FROM (GET-CER-LIS-INTF)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc05090Data.Len.GET_CER_LIS_INTF);
		tsOutputData.setData(ws.getGetCerLisIntfBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCicsChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getCicsContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCicsChannelFormatted());
		//******************************************************
		// WE CAN'T REALLY DO ANYTHING ELSE HERE BUT EXIT IF   *
		// WE ENCOUNTER AN ERROR PUTTING THE ERROR MESSAGE.    *
		//******************************************************
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 1000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 9930-CONTAINER-ERRORS<br>
	 * <pre>**********************************************************
	 *   REPORT ON CONTAINER ERRORS, SET PARAGRAPH BEFOREHAND   *
	 *         MOVE CF-PN-9999         TO EA-01-PARAGRAPH-NBR   *
	 * **********************************************************</pre>*/
	private void containerErrors() {
		// COB_CODE: SET XZC05O-EC-SYSTEM-ERROR  TO TRUE.
		ws.getXzc050c1().getoErrorCode().setFault();
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EA-06-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode(ws.getWorkingStorageArea().getResponseCode());
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EA-06-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode2(ws.getWorkingStorageArea().getResponseCode2());
		// COB_CODE: MOVE EA-06-CONTAINER-ERROR  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa06ContainerError().getEa06ContainerErrorFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZC05O-ERROR-MESSAGE.
		ws.getXzc050c1().setoErrorMessage(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: PERFORM 9800-WRITE-TO-CONTAINER
		//              THRU 9800-EXIT.
		writeToContainer();
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: RNG_3210-MOVE-SER-CRT-_-3210-EXIT<br>*/
	private void rng3210MoveSerCrt() {
		String retcode = "";
		boolean goto3210A = false;
		moveSerCrt();
		do {
			goto3210A = false;
			retcode = a();
		} while (retcode.equals("3210-A"));
	}

	/**Original name: RNG_3410-MOVE-CER-INF-_-3410-EXIT<br>*/
	private void rng3410MoveCerInf() {
		String retcode = "";
		boolean goto3410A = false;
		moveCerInf();
		do {
			goto3410A = false;
			retcode = a1();
		} while (retcode.equals("3410-A"));
	}

	/**Original name: RNG_3411-MOVE-WRD-INF-_-3411-EXIT<br>*/
	private void rng3411MoveWrdInf() {
		String retcode = "";
		boolean goto3411A = false;
		moveWrdInf();
		do {
			goto3411A = false;
			retcode = a2();
		} while (retcode.equals("3411-A"));
	}

	public void initCallableInputs() {
		ws.getFwppcstk().setTkUserid("");
		ws.getFwppcstk().setTkPassword("");
		ws.getFwppcstk().setTkUri("");
		ws.getFwppcstk().setTkFedTarSys("");
		ws.getXz05cl1i().setActNbr("");
		for (int idx0 = 1; idx0 <= Xz05cl1i.STA_CD_LIS_MAXOCCURS; idx0++) {
			ws.getXz05cl1i().getStaCdLis(idx0).setXz005iStaCd("");
		}
	}

	public void initTs571cb1() {
		ws.getTs571cb1().setServiceMnemonic("");
		ws.getTs571cb1().setUri("");
		ws.getTs571cb1().setCicsSysid("");
		ws.getTs571cb1().setServicePrefix("");
	}

	public void initXzc050ProgramOutput() {
		ws.getXzc050c1().getoCerInfObj().fill(new Xzc05oCerInf().initXzc05oCerInf());
		ws.getXzc050c1().getoErrorCode().setRetCd(((short) 0));
		ws.getXzc050c1().setoErrorMessage("");
	}

	public void initCallableOutputs() {
		ws.getXz005oCerInfObj().fill(new Xz005oCerInf().initXz005oCerInf());
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
