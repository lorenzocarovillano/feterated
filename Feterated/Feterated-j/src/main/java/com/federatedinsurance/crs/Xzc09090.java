/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.lang.collection.IAfSet;
import com.bphx.ctu.af.lang.collection.creation.CollectionCreator;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpAccessStatus;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.Ivoryh;
import com.federatedinsurance.crs.copy.Xz009iServiceInputs;
import com.federatedinsurance.crs.copy.Xz009oRetMsg;
import com.federatedinsurance.crs.ws.DfhcommareaXzc09090;
import com.federatedinsurance.crs.ws.SaveAreaXzc09090;
import com.federatedinsurance.crs.ws.Ts571cb1;
import com.federatedinsurance.crs.ws.Xzc09090Data;
import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZC09090<br>
 * <pre>AUTHOR.       Tim Johnson.
 * DATE-WRITTEN. January 2019.
 * ****************************************************************
 *                                                               **
 *   PROGRAM TITLE - GetTransactionDetail INTERFACE PROGRAM      **
 *                                                               **
 *   PURPOSE -  CALLS A CALLABLE WEB SERVICE TO INTERFACE WITH   **
 *              A BROKERED WEB SERVICE TO RETRIEVE TRANSACTION   **
 *              DATA                                             **
 *                                                               **
 *                                                               **
 *   PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS **
 *                         LINKED TO BY AN APPLICATION PROGRAM.  **
 *                                                               **
 *   DATA ACCESS METHODS - CICS LINKAGE                          **
 *                         DB2 DATABASE                          **
 *                                                               **
 * ****************************************************************
 *                 M A I N T E N A N C E    L O G                **
 *                                                               **
 *   SI #          DATE      PROG             DESCRIPTION        **
 *   --------   ---------- --------   ---------------------------**
 *   20163      08/24/2019   TJJ      NEW
 * ****************************************************************</pre>*/
public class Xzc09090 extends Program {

	//==== PROPERTIES ====
	private ExecContext execContext = null;
	private IAfSet<String> channelSet = CollectionCreator.getStringFactory().createSet();
	//Original name: WORKING-STORAGE
	private Xzc09090Data ws = new Xzc09090Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaXzc09090 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaXzc09090 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		programExit();
		return 0;
	}

	public static Xzc09090 getInstance() {
		return (Programs.getInstance(Xzc09090.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>*****************************************************************
	 *   MAIN PROCESSING CONTROL
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: PERFORM 3000-GET-TRS-DTL
		//              THRU 3000-EXIT.
		getTrsDtl();
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>*****************************************************************
	 *  INITIALIZE OUTPUT AND VERIFY INPUT.
	 * *****************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: SET XZC09O-RC-OK            TO TRUE.
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setOk();
		// COB_CODE: PERFORM 2100-VERIFY-INPUT
		//              THRU 2100-EXIT.
		verifyInput();
		// COB_CODE: PERFORM 2200-DET-CICS-ENV-INF
		//              THRU 2200-EXIT.
		detCicsEnvInf();
		// COB_CODE: PERFORM 2300-DET-TAR-SYS
		//              THRU 2300-EXIT.
		detTarSys();
	}

	/**Original name: 2100-VERIFY-INPUT<br>
	 * <pre>*****************************************************************
	 *  VERIFY THAT THE CONSUMER PASSED IN VALID INPUTS.
	 * *****************************************************************</pre>*/
	private void verifyInput() {
		// COB_CODE: IF XZC09I-POL-NBR = SPACES
		//             OR
		//              XZC09I-POL-NBR = LOW-VALUES
		//             OR
		//              XZC09I-POL-EXP-DT = SPACES
		//             OR
		//              XZC09I-POL-EXP-DT = LOW-VALUES
		//                  THRU 9100-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getXzc09iServiceInputs().getPolNbr())
				|| Characters.EQ_LOW.test(dfhcommarea.getXzc09iServiceInputs().getPolNbr(), Xz009iServiceInputs.Len.POL_NBR)
				|| Characters.EQ_SPACE.test(dfhcommarea.getXzc09iServiceInputs().getPolExpDt())
				|| Characters.EQ_LOW.test(dfhcommarea.getXzc09iServiceInputs().getPolExpDtFormatted())) {
			// COB_CODE: MOVE CF-PN-2100         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2100());
			// COB_CODE: PERFORM 9100-END-WITH-FATAL-INP-ERR
			//              THRU 9100-EXIT
			endWithFatalInpErr();
		}
	}

	/**Original name: 2200-DET-CICS-ENV-INF<br>
	 * <pre>*****************************************************************
	 *  GET THE CURRENT CICS REGION TO DETERMINE TARGET SYSTEM.
	 * *****************************************************************</pre>*/
	private void detCicsEnvInf() {
		// COB_CODE: EXEC CICS ASSIGN
		//               APPLID(SA-CICS-APPLID)
		//           END-EXEC.
		ws.getSaveArea().getCicsApplid().setCicsApplid(execContext.getApplicationId());
		execContext.clearStatus();
	}

	/**Original name: 2300-DET-TAR-SYS<br>
	 * <pre>*****************************************************************
	 *  GET TARGET SYSTEM BASED ON CURRENT CICS REGION
	 * *****************************************************************</pre>*/
	private void detTarSys() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN SA-CA-PROD
		//                   SET SA-TS-PROD      TO TRUE
		//               WHEN SA-CA-DEV1
		//                   SET SA-TS-DEV1      TO TRUE
		//               WHEN SA-CA-DEV2
		//                   SET SA-TS-DEV2      TO TRUE
		//               WHEN SA-CA-DEV6
		//                   SET SA-TS-DEV6      TO TRUE
		//               WHEN SA-CA-BETA1
		//                   SET SA-TS-BETA1     TO TRUE
		//               WHEN SA-CA-BETA2
		//                   SET SA-TS-BETA2     TO TRUE
		//               WHEN SA-CA-BETA8
		//                   SET SA-TS-BETA8     TO TRUE
		//               WHEN SA-CA-EDUC1
		//                   SET SA-TS-EDUC1     TO TRUE
		//               WHEN SA-CA-EDUC2
		//                   SET SA-TS-EDUC2     TO TRUE
		//               WHEN OTHER
		//                      THRU 9600-EXIT
		//           END-EVALUATE.
		if (ws.getSaveArea().getCicsApplid().isProd()) {
			// COB_CODE: SET SA-TS-PROD      TO TRUE
			ws.getSaveArea().getTarSys().setProd();
		} else if (ws.getSaveArea().getCicsApplid().isDev1()) {
			// COB_CODE: SET SA-TS-DEV1      TO TRUE
			ws.getSaveArea().getTarSys().setDev1();
		} else if (ws.getSaveArea().getCicsApplid().isDev2()) {
			// COB_CODE: SET SA-TS-DEV2      TO TRUE
			ws.getSaveArea().getTarSys().setDev2();
		} else if (ws.getSaveArea().getCicsApplid().isDev6()) {
			// COB_CODE: SET SA-TS-DEV6      TO TRUE
			ws.getSaveArea().getTarSys().setDev6();
		} else if (ws.getSaveArea().getCicsApplid().isBeta1()) {
			// COB_CODE: SET SA-TS-BETA1     TO TRUE
			ws.getSaveArea().getTarSys().setBeta1();
		} else if (ws.getSaveArea().getCicsApplid().isBeta2()) {
			// COB_CODE: SET SA-TS-BETA2     TO TRUE
			ws.getSaveArea().getTarSys().setBeta2();
		} else if (ws.getSaveArea().getCicsApplid().isBeta8()) {
			// COB_CODE: SET SA-TS-BETA8     TO TRUE
			ws.getSaveArea().getTarSys().setBeta8();
		} else if (ws.getSaveArea().getCicsApplid().isEduc1()) {
			// COB_CODE: SET SA-TS-EDUC1     TO TRUE
			ws.getSaveArea().getTarSys().setEduc1();
		} else if (ws.getSaveArea().getCicsApplid().isEduc2()) {
			// COB_CODE: SET SA-TS-EDUC2     TO TRUE
			ws.getSaveArea().getTarSys().setEduc2();
		} else {
			// COB_CODE: MOVE CF-PN-2300     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2300());
			// COB_CODE: PERFORM 9600-CICS-APPLID-ERROR
			//              THRU 9600-EXIT
			cicsApplidError();
		}
	}

	/**Original name: 3000-GET-TRS-DTL<br>
	 * <pre>*****************************************************************
	 *  CALL THE CRSTransactionInformationCallable SERVICE TO RETRIEVE
	 *  THE TRANSACTION DETAILS.
	 * *****************************************************************</pre>*/
	private void getTrsDtl() {
		// COB_CODE: PERFORM 3100-PRP-WEB-SVC-CALL
		//              THRU 3100-EXIT.
		prpWebSvcCall();
		// COB_CODE: PERFORM 3200-MOVE-WEB-SVC-INPUT
		//              THRU 3200-EXIT.
		moveWebSvcInput();
		// COB_CODE: PERFORM 3300-SETUP-CONTAINER
		//              THRU 3300-EXIT.
		setupContainer();
		// COB_CODE: PERFORM 3400-LINK-TO-IVORY
		//              THRU 3400-EXIT.
		linkToIvory();
		// COB_CODE: PERFORM 3500-MOVE-WEB-SVC-OUTPUT
		//              THRU 3500-EXIT.
		moveWebSvcOutput();
	}

	/**Original name: 3100-PRP-WEB-SVC-CALL<br>
	 * <pre>*****************************************************************
	 *  INITIAL SETUP FOR CALLING IVORY WEB SERVICE PROGRAM
	 * *****************************************************************</pre>*/
	private void prpWebSvcCall() {
		// COB_CODE: MOVE LOW-VALUES             TO NON-STATIC-FIELDS.
		ws.getIvoryh().getNonStaticFields().initNonStaticFieldsLowValues();
		// COB_CODE: MOVE CF-SOAP-SERVICE        TO SOAP-SERVICE IN IVORYH.
		ws.getIvoryh().getNonStaticFields().setService(ws.getConstantFields().getSoapService());
		// COB_CODE: MOVE CF-SOAP-OPERATION      TO SOAP-OPERATION IN IVORYH.
		ws.getIvoryh().getNonStaticFields().setOperation(ws.getConstantFields().getSoapOperation());
		// COB_CODE: MOVE CF-LV-ZLINUX-SRV       TO SOAP-LAYOUT-VERSION.
		ws.getIvoryh().setSoapLayoutVersion(ws.getConstantFields().getLvZlinuxSrv());
		// COB_CODE: SET SOAP-TRACE-STOR-CHARSET-EBCDIC
		//               SOAP-REQUEST-STOR-TYPE-CHCONT
		//               SOAP-RESPONSE-STOR-TYPE-CHCONT
		//               SOAP-TRACE-STOR-TYPE-TDQ
		//               SOAP-ERROR-STOR-TYPE-TDQ
		//                                       TO TRUE.
		ws.getIvoryh().getNonStaticFields().getTraceStorCharset().setEbcdic();
		ws.getIvoryh().getNonStaticFields().getRequestStorType().setChcont();
		ws.getIvoryh().getNonStaticFields().getResponseStorType().setChcont();
		ws.getIvoryh().getNonStaticFields().getTraceStorType().setTdq();
		ws.getIvoryh().getNonStaticFields().getErrorStorType().setTdq();
		// COB_CODE: MOVE CF-CI-INP-CONTAINER    TO SOAP-REQUEST-STORE-VALUE.
		ws.getIvoryh().getNonStaticFields().setRequestStoreValue(ws.getConstantFields().getContainerInfo().getInpContainer());
		// COB_CODE: MOVE CF-CI-OUP-CONTAINER    TO SOAP-RESPONSE-STORE-VALUE.
		ws.getIvoryh().getNonStaticFields().setResponseStoreValue(ws.getConstantFields().getContainerInfo().getOupContainer());
		// COB_CODE: MOVE LENGTH OF CALLABLE-INPUTS
		//                                       TO SOAP-REQUEST-LENGTH.
		ws.getIvoryh().getNonStaticFields().setRequestLength(Xzc09090Data.Len.CALLABLE_INPUTS);
		// COB_CODE: MOVE LENGTH OF CALLABLE-OUTPUTS
		//                                       TO SOAP-RESPONSE-LENGTH.
		ws.getIvoryh().getNonStaticFields().setResponseLength(Xzc09090Data.Len.CALLABLE_OUTPUTS);
		// COB_CODE: MOVE CF-TRACE-STOR-VALUE    TO SOAP-TRACE-STOR-VALUE
		//                                          SOAP-ERROR-STOR-VALUE.
		ws.getIvoryh().getNonStaticFields().setTraceStorValue(ws.getConstantFields().getTraceStorValue());
		ws.getIvoryh().getNonStaticFields().setErrorStorValue(ws.getConstantFields().getTraceStorValue());
	}

	/**Original name: 3200-MOVE-WEB-SVC-INPUT<br>
	 * <pre>*****************************************************************
	 *  SETUP INPUT FOR THE SERVICE CALL TO IVORY
	 * *****************************************************************</pre>*/
	private void moveWebSvcInput() {
		// COB_CODE: INITIALIZE CALLABLE-INPUTS.
		initCallableInputs();
		// COB_CODE: PERFORM 3210-RETRIEVE-SVC-URL
		//              THRU 3210-EXIT.
		retrieveSvcUrl();
		// COB_CODE: MOVE XZC09I-POL-NBR         TO XZ009I-POL-NBR.
		ws.getXz009iServiceInputs().setPolNbr(dfhcommarea.getXzc09iServiceInputs().getPolNbr());
		// COB_CODE: MOVE XZC09I-POL-EXP-DT      TO XZ009I-POL-EXP-DT.
		ws.getXz009iServiceInputs().setPolExpDt(dfhcommarea.getXzc09iServiceInputs().getPolExpDt());
		// COB_CODE: MOVE SA-WEB-SVC-URL         TO XZ009I-URI.
		ws.getXz009iServiceInputs().setUri(ws.getSaveArea().getWebSvcUrl());
		// COB_CODE: PERFORM 3220-RETRIEVE-USR-ID-PWD
		//              THRU 3220-EXIT.
		retrieveUsrIdPwd();
		// COB_CODE: MOVE SA-UC-USR-ID           TO XZ009I-USR-ID.
		ws.getXz009iServiceInputs().setUsrId(ws.getSaveArea().getUcUsrId());
		// COB_CODE: MOVE SA-UC-PWD              TO XZ009I-PWD.
		ws.getXz009iServiceInputs().setPwd(ws.getSaveArea().getUcPwd());
		// COB_CODE: MOVE SA-TAR-SYS             TO XZ009I-TAR-SYS.
		ws.getXz009iServiceInputs().setTarSys(ws.getSaveArea().getTarSys().getTarSys());
	}

	/**Original name: 3210-RETRIEVE-SVC-URL<br>
	 * <pre>*****************************************************************
	 *  CALL THE WEB SERVICE URI LOOKUP COMMON ROUTINE
	 * *****************************************************************</pre>*/
	private void retrieveSvcUrl() {
		// COB_CODE: INITIALIZE URI-LKU-LINKAGE.
		initTs571cb1();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO UL-SERVICE-MNEMONIC.
		ws.getTs571cb1().setServiceMnemonic(ws.getConstantFields().getWebSvcId());
		// CALL WEB SERVICE LOOKUP INTERFACE PROGRAM
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (CF-WEB-SVC-LKU-PGM)
		//               COMMAREA (URI-LKU-LINKAGE)
		//               LENGTH   (LENGTH OF URI-LKU-LINKAGE)
		//               RESP     (SA-RESPONSE-CODE)
		//               RESP2    (SA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC09090", execContext).commarea(ws.getTs571cb1()).length(Ts571cb1.Len.TS571CB1)
				.link(ws.getConstantFields().getWebSvcLkuPgm(), new Ts571099());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// CHECK FOR ERROR FROM CICS LINK
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3210         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3210());
			// COB_CODE: MOVE CF-WEB-SVC-LKU-PGM TO EA-03-FAILED-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getConstantFields().getWebSvcLkuPgm());
			// COB_CODE: PERFORM 9110-CICS-LINK-ERROR
			//              THRU 9110-EXIT
			cicsLinkError();
		}
		// CHECK IF RETURNING URI IS NOT VALID
		// COB_CODE: IF UL-URI = SPACES
		//             OR
		//              UL-URI = LOW-VALUES
		//                  THRU 9130-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getTs571cb1().getUri()) || Characters.EQ_LOW.test(ws.getTs571cb1().getUri(), Ts571cb1.Len.URI)) {
			// COB_CODE: MOVE CF-PN-3210         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3210());
			// COB_CODE: PERFORM 9130-WEB-URI-NOT-FOUND
			//              THRU 9130-EXIT
			webUriNotFound();
		}
		// COB_CODE: MOVE UL-URI                 TO SA-WEB-SVC-URL.
		ws.getSaveArea().setWebSvcUrl(ws.getTs571cb1().getUri());
	}

	/**Original name: 3220-RETRIEVE-USR-ID-PWD<br>
	 * <pre>*****************************************************************
	 *  READ THE QUEUE TO GET THE USERID AND PASSWORD.
	 * *****************************************************************</pre>*/
	private void retrieveUsrIdPwd() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS READQ TS
		//               ITEM  (1)
		//               QNAME ('UIDPCRS1')
		//               INTO  (SA-USR-CREDENTIALS)
		//               RESP  (SA-RESPONSE-CODE)
		//               RESP2 (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(SaveAreaXzc09090.Len.USR_CREDENTIALS);
		TsQueueManager.read(execContext, "UIDPCRS1", ((short) 1), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.getSaveArea().setUsrCredentialsBytes(tsQueueData.getData());
		}
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9140-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3220         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3220());
			// COB_CODE: PERFORM 9140-CICS-READQ-ERROR
			//              THRU 9140-EXIT
			cicsReadqError();
		}
	}

	/**Original name: 3300-SETUP-CONTAINER<br>
	 * <pre>*****************************************************************
	 *  SETUP THE CONTAINERS FOR CALLING GIICALS
	 * *****************************************************************</pre>*/
	private void setupContainer() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE CF-PN-3300             TO EA-01-PARAGRAPH-NBR.
		ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-IVORYH-CONTAINER)
		//               CHANNEL    (CF-CI-CICS-CHANNEL)
		//               FROM       (IVORYH)
		//               FLENGTH    (LENGTH OF IVORYH)
		//               RESP       (SA-RESPONSE-CODE)
		//               RESP2      (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Ivoryh.Len.IVORYH);
		tsOutputData.setData(ws.getIvoryh().getIvoryhBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCicsChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getIvoryhContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCicsChannelFormatted());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-INP-CONTAINER)
		//               CHANNEL    (CF-CI-CICS-CHANNEL)
		//               FROM       (CALLABLE-INPUTS)
		//               FLENGTH    (LENGTH OF CALLABLE-INPUTS)
		//               RESP       (SA-RESPONSE-CODE)
		//               RESP2      (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc09090Data.Len.CALLABLE_INPUTS);
		tsOutputData.setData(ws.getCallableInputsBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCicsChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getInpContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCicsChannelFormatted());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: INITIALIZE CALLABLE-OUTPUTS.
		initCallableOutputs();
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-OUP-CONTAINER)
		//               CHANNEL    (CF-CI-CICS-CHANNEL)
		//               FROM       (CALLABLE-OUTPUTS)
		//               FLENGTH    (LENGTH OF CALLABLE-OUTPUTS)
		//               RESP       (SA-RESPONSE-CODE)
		//               RESP2      (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc09090Data.Len.CALLABLE_OUTPUTS);
		tsOutputData.setData(ws.getCallableOutputsBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCicsChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getOupContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCicsChannelFormatted());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
	}

	/**Original name: 3400-LINK-TO-IVORY<br>
	 * <pre>*****************************************************************
	 *  CALL THE IVORY PROCESSING PROGRAM
	 * *****************************************************************</pre>*/
	private void linkToIvory() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE CF-PN-3400             TO EA-01-PARAGRAPH-NBR
		ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3400());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM (CF-CALLABLE-SVC-PROGRAM)
		//               CHANNEL (CF-CI-CICS-CHANNEL)
		//               RESP    (SA-RESPONSE-CODE)
		//               RESP2   (SA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC09090", execContext).channel(ws.getConstantFields().getContainerInfo().getCicsChannelFormatted())
				.link(ws.getConstantFields().getCallableSvcProgram());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-CALLABLE-SVC-PROGRAM
			//                                   TO EA-03-FAILED-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getConstantFields().getCallableSvcProgram());
			// COB_CODE: PERFORM 9110-CICS-LINK-ERROR
			//              THRU 9110-EXIT
			cicsLinkError();
		}
		//    IN ORDER TO DO ANY FURTHER ERROR CHECKING, WE MUST GET THE
		//    CONTAINERS.
		// COB_CODE: EXEC CICS GET
		//               CONTAINER (CF-CI-IVORYH-CONTAINER)
		//               CHANNEL   (CF-CI-CICS-CHANNEL)
		//               INTO      (IVORYH)
		//               RESP      (SA-RESPONSE-CODE)
		//               RESP2     (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Ivoryh.Len.IVORYH);
		Channel.read(execContext, ws.getConstantFields().getContainerInfo().getCicsChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getIvoryhContainerFormatted(), tsOutputData);
		ws.getIvoryh().setIvoryhBytes(tsOutputData.getData());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: EXEC CICS GET
		//               CONTAINER (CF-CI-OUP-CONTAINER)
		//               CHANNEL   (CF-CI-CICS-CHANNEL)
		//               INTO      (CALLABLE-OUTPUTS)
		//               RESP      (SA-RESPONSE-CODE)
		//               RESP2     (SA-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc09090Data.Len.CALLABLE_OUTPUTS);
		Channel.read(execContext, ws.getConstantFields().getContainerInfo().getCicsChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getOupContainerFormatted(), tsOutputData);
		ws.setCallableOutputsBytes(tsOutputData.getData());
		ws.getSaveArea().setResponseCode(execContext.getResp());
		ws.getSaveArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF SA-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getSaveArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: IF SOAP-RETURNCODE NOT = 0
		//                  THRU 9120-EXIT
		//           END-IF.
		if (ws.getIvoryh().getNonStaticFields().getReturncode() != 0) {
			// COB_CODE: PERFORM 9120-SOAP-FAULT-ERROR
			//              THRU 9120-EXIT
			soapFaultError();
		}
	}

	/**Original name: 3500-MOVE-WEB-SVC-OUTPUT<br>
	 * <pre>*****************************************************************
	 *  MOVE TRANSACTION DETAILS TO THE OUTPUT AREA
	 * *****************************************************************</pre>*/
	private void moveWebSvcOutput() {
		// COB_CODE: INITIALIZE XZC09O-SERVICE-OUTPUTS.
		initXzc09oServiceOutputs();
		// COB_CODE: MOVE XZ009O-RET-CD          TO XZC09O-RET-CD.
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setRetCd(ws.getXzc090co().getTrsDtlRsp().getRetMsg().getRetCd().getRetCd());
		// COB_CODE: EVALUATE TRUE
		//               WHEN XZ009O-RC-OK
		//                   CONTINUE
		//               WHEN XZ009O-RC-FAULT
		//                   GO TO 3500-EXIT
		//               WHEN XZ009O-RC-CICS-ERROR
		//                   GO TO 3500-EXIT
		//               WHEN XZ009O-RC-WARNING
		//                      THRU 3520-EXIT
		//               WHEN OTHER
		//                   CONTINUE
		//           END-EVALUATE.
		switch (ws.getXzc090co().getTrsDtlRsp().getRetMsg().getRetCd().getRetCd()) {

		case Xz08coRetCd.OK:// COB_CODE: CONTINUE
			//continue
			break;

		case Xz08coRetCd.FAULT:// COB_CODE: PERFORM 3510-PRC-FATAL-MSGS
			//              THRU 3510-EXIT
			rng3510PrcFatalMsgs();
			// COB_CODE: GO TO 3500-EXIT
			return;

		case Xz08coRetCd.SYSTEM_ERROR:// COB_CODE: PERFORM 3510-PRC-FATAL-MSGS
			//              THRU 3510-EXIT
			rng3510PrcFatalMsgs();
			// COB_CODE: GO TO 3500-EXIT
			return;

		case Xz08coRetCd.WARNING:// COB_CODE: PERFORM 3520-PRC-WNG-MSGS
			//              THRU 3520-EXIT
			rng3520PrcWngMsgs();
			break;

		default:// COB_CODE: CONTINUE
			//continue
			break;
		}
		// COB_CODE: MOVE XZ009O-POL-NBR         TO XZC09O-POL-NBR.
		dfhcommarea.getXzc09oServiceOutputs().setPolNbr(ws.getXzc090co().getTrsDtlRsp().getPolNbr());
		// COB_CODE: MOVE XZ009O-PT-EFF-DT       TO XZC09O-PT-EFF-DT.
		dfhcommarea.getXzc09oServiceOutputs().setPtEffDt(ws.getXzc090co().getTrsDtlRsp().getPtEffDt());
		// COB_CODE: MOVE XZ009O-PT-EXP-DT       TO XZC09O-PT-EXP-DT.
		dfhcommarea.getXzc09oServiceOutputs().setPtExpDt(ws.getXzc090co().getTrsDtlRsp().getPtExpDt());
		// COB_CODE: MOVE XZ009O-QTE-NBR         TO XZC09O-QTE-NBR.
		dfhcommarea.getXzc09oServiceOutputs().setQteNbr(ws.getXzc090co().getTrsDtlRsp().getQteNbr());
		// COB_CODE: MOVE XZ009O-ACT-NBR         TO XZC09O-ACT-NBR.
		dfhcommarea.getXzc09oServiceOutputs().setActNbr(ws.getXzc090co().getTrsDtlRsp().getActNbr());
		// COB_CODE: MOVE XZ009O-CTC-ID          TO XZC09O-CTC-ID.
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oCtcId(ws.getXzc090co().getTrsDtlRsp().getNamedInsured().getXzc09oCtcId());
		// COB_CODE: MOVE XZ009O-DSY-NM          TO XZC09O-DSY-NM.
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oDsyNm(ws.getXzc090co().getTrsDtlRsp().getNamedInsured().getXzc09oDsyNm());
		// COB_CODE: MOVE XZ009O-ADR-ID          TO XZC09O-ADR-ID.
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oAdrId(ws.getXzc090co().getTrsDtlRsp().getNamedInsured().getXzc09oAdrId());
		// COB_CODE: MOVE XZ009O-ADR-1           TO XZC09O-ADR-1.
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oAdr1(ws.getXzc090co().getTrsDtlRsp().getNamedInsured().getXzc09oAdr1());
		// COB_CODE: MOVE XZ009O-ADR-2           TO XZC09O-ADR-2.
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oAdr2(ws.getXzc090co().getTrsDtlRsp().getNamedInsured().getXzc09oAdr2());
		// COB_CODE: MOVE XZ009O-CIT-NM          TO XZC09O-CIT-NM.
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oCitNm(ws.getXzc090co().getTrsDtlRsp().getNamedInsured().getXzc09oCitNm());
		// COB_CODE: MOVE XZ009O-ST-ABB          TO XZC09O-ST-ABB.
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oStAbb(ws.getXzc090co().getTrsDtlRsp().getNamedInsured().getXzc09oStAbb());
		// COB_CODE: MOVE XZ009O-PST-CD          TO XZC09O-PST-CD.
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oPstCd(ws.getXzc090co().getTrsDtlRsp().getNamedInsured().getXzc09oPstCd());
		// COB_CODE: MOVE XZ009O-CTY-NM          TO XZC09O-CTY-NM.
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oCtyNm(ws.getXzc090co().getTrsDtlRsp().getNamedInsured().getXzc09oCtyNm());
	}

	/**Original name: 3510-PRC-FATAL-MSGS<br>
	 * <pre>*****************************************************************
	 *  PROCESS FATAL ERRORS
	 * *****************************************************************</pre>*/
	private void prcFatalMsgs() {
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
	}

	/**Original name: 3510-A<br>*/
	private String a() {
		// COB_CODE: IF SS-ER > CF-MAX-ERR
		//             OR
		//              XZ009O-ERR-MSG(SS-ER) = SPACES
		//               GO TO 3510-EXIT
		//           END-IF.
		if (ws.getSsEr() > ws.getConstantFields().getMaxErr()
				|| Characters.EQ_SPACE.test(ws.getXzc090co().getTrsDtlRsp().getRetMsg().getXzc09oErrMsg(ws.getSsEr()))) {
			// COB_CODE: GO TO 3510-EXIT
			return "";
		}
		// COB_CODE: MOVE XZ009O-ERR-MSG(SS-ER)  TO XZC09O-ERR-MSG(SS-ER).
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(ws.getSsEr(),
				ws.getXzc090co().getTrsDtlRsp().getRetMsg().getXzc09oErrMsg(ws.getSsEr()));
		// COB_CODE: ADD +1                      TO SS-ER.
		ws.setSsEr(Trunc.toShort(1 + ws.getSsEr(), 4));
		// COB_CODE: GO TO 3510-A.
		return "3510-A";
	}

	/**Original name: 3520-PRC-WNG-MSGS<br>
	 * <pre>*****************************************************************
	 *  PROCESS WARNINGS
	 * *****************************************************************</pre>*/
	private void prcWngMsgs() {
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
	}

	/**Original name: 3520-A<br>*/
	private String a1() {
		// COB_CODE: IF SS-ER > CF-MAX-ERR
		//             OR
		//              XZ009O-WNG-MSG(SS-ER) = SPACES
		//               GO TO 3520-EXIT
		//           END-IF.
		if (ws.getSsEr() > ws.getConstantFields().getMaxErr()
				|| Characters.EQ_SPACE.test(ws.getXzc090co().getTrsDtlRsp().getRetMsg().getXzc09oWngMsg(ws.getSsEr()))) {
			// COB_CODE: GO TO 3520-EXIT
			return "";
		}
		// COB_CODE: MOVE XZ009O-WNG-MSG(SS-ER)  TO XZC09O-WNG-MSG(SS-ER).
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oWngMsg(ws.getSsEr(),
				ws.getXzc090co().getTrsDtlRsp().getRetMsg().getXzc09oWngMsg(ws.getSsEr()));
		// COB_CODE: ADD +1                      TO SS-ER.
		ws.setSsEr(Trunc.toShort(1 + ws.getSsEr(), 4));
		// COB_CODE: GO TO 3520-A.
		return "3520-A";
	}

	/**Original name: 9100-END-WITH-FATAL-INP-ERR<br>
	 * <pre>*****************************************************************
	 *  INVALID INPUT ERROR
	 * *****************************************************************</pre>*/
	private void endWithFatalInpErr() {
		// COB_CODE: SET XZC09O-RC-CICS-ERROR    TO TRUE.
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setSystemError();
		// COB_CODE: MOVE XZC09I-POL-NBR         TO EA-02-POL-NBR.
		ws.getErrorAndAdviceMessages().getEa02InvalidInput().setEa02PolNbr(dfhcommarea.getXzc09iServiceInputs().getPolNbr());
		// COB_CODE: MOVE EA-02-INVALID-INPUT    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa02InvalidInput().getEa02InvalidInputFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC09O-ERR-MSG(SS-ER).
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(ws.getSsEr(),
				ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9110-CICS-LINK-ERROR<br>
	 * <pre>*****************************************************************
	 *  CICS LINK ERROR
	 * *****************************************************************</pre>*/
	private void cicsLinkError() {
		// COB_CODE: SET XZC09O-RC-CICS-ERROR    TO TRUE.
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setSystemError();
		// COB_CODE: MOVE SA-RESPONSE-CODE       TO EA-03-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode(ws.getSaveArea().getResponseCode());
		// COB_CODE: MOVE SA-RESPONSE-CODE2      TO EA-03-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode2(ws.getSaveArea().getResponseCode2());
		// COB_CODE: MOVE EA-03-CICS-LINK-ERROR  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa03CicsLinkError().getEa03CicsLinkErrorFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC09O-ERR-MSG(SS-ER).
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(ws.getSsEr(),
				ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9120-SOAP-FAULT-ERROR<br>
	 * <pre>*****************************************************************
	 *  SOAP FAULT ERROR
	 * *****************************************************************</pre>*/
	private void soapFaultError() {
		// COB_CODE: SET XZC09O-RC-FAULT         TO TRUE.
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setFault();
		// COB_CODE: MOVE SOAP-RETURNCODE        TO EA-04-SOAP-RETURN-CODE.
		ws.getErrorAndAdviceMessages().getEa04SoapFault().setEa04SoapReturnCode(((int) (ws.getIvoryh().getNonStaticFields().getReturncode())));
		// COB_CODE: MOVE EA-04-SOAP-FAULT       TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error().setErrorMessage(ws.getErrorAndAdviceMessages().getEa04SoapFault().getEa04SoapFaultFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC09O-ERR-MSG(SS-ER).
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(ws.getSsEr(),
				ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9130-WEB-URI-NOT-FOUND<br>
	 * <pre>*****************************************************************
	 *  SOAP FAULT ERROR
	 * *****************************************************************</pre>*/
	private void webUriNotFound() {
		// COB_CODE: SET XZC09O-RC-FAULT         TO TRUE.
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setFault();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO EA-05-WEB-SVC-ID.
		ws.getErrorAndAdviceMessages().getEa05WebUriFault().setEa05WebSvcId(ws.getConstantFields().getWebSvcId());
		// COB_CODE: MOVE EA-05-WEB-URI-FAULT    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa05WebUriFault().getEa05WebUriFaultFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC09O-ERR-MSG(SS-ER).
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(ws.getSsEr(),
				ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9140-CICS-READQ-ERROR<br>
	 * <pre>*****************************************************************
	 *  CICS READQ ERROR
	 * *****************************************************************</pre>*/
	private void cicsReadqError() {
		// COB_CODE: SET XZC09O-RC-CICS-ERROR    TO TRUE.
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setSystemError();
		// COB_CODE: MOVE SA-RESPONSE-CODE       TO EA-07-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa07ReadqError().setCode(ws.getSaveArea().getResponseCode());
		// COB_CODE: MOVE SA-RESPONSE-CODE2      TO EA-07-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa07ReadqError().setCode2(ws.getSaveArea().getResponseCode2());
		// COB_CODE: MOVE EA-07-READQ-ERROR      TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa07ReadqError().getEa07ReadqErrorFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC09O-ERR-MSG(SS-ER).
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(ws.getSsEr(),
				ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9600-CICS-APPLID-ERROR<br>
	 * <pre>*****************************************************************
	 *  CALL TO SERVICE HAD AN ERROR
	 * *****************************************************************</pre>*/
	private void cicsApplidError() {
		// COB_CODE: SET XZC09O-RC-CICS-ERROR    TO TRUE.
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setSystemError();
		// COB_CODE: MOVE SA-CICS-APPLID         TO EA-08-CICS-RGN.
		ws.getErrorAndAdviceMessages().getEa08TarSysNotFnd().setEa08CicsRgn(ws.getSaveArea().getCicsApplid().getCicsApplid());
		// COB_CODE: MOVE EA-08-TAR-SYS-NOT-FND  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa08TarSysNotFnd().getEa08TarSysNotFndFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC09O-ERR-MSG(SS-ER).
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(ws.getSsEr(),
				ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: 9930-CONTAINER-ERRORS<br>
	 * <pre>**********************************************************
	 *   REPORT ON CONTAINER ERRORS, SET PARAGRAPH BEFOREHAND   *
	 *         MOVE CF-PN-9999         TO EA-01-PARAGRAPH-NBR   *
	 * **********************************************************</pre>*/
	private void containerErrors() {
		// COB_CODE: SET XZC09O-RC-CICS-ERROR    TO TRUE.
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setSystemError();
		// COB_CODE: MOVE SA-RESPONSE-CODE       TO EA-06-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode(ws.getSaveArea().getResponseCode());
		// COB_CODE: MOVE SA-RESPONSE-CODE2      TO EA-06-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode2(ws.getSaveArea().getResponseCode2());
		// COB_CODE: MOVE EA-06-CONTAINER-ERROR  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa06ContainerError().getEa06ContainerErrorFormatted());
		// COB_CODE: MOVE +1                     TO SS-ER.
		ws.setSsEr(((short) 1));
		// COB_CODE: MOVE EA-01-ERROR            TO XZC09O-ERR-MSG(SS-ER).
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(ws.getSsEr(),
				ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	/**Original name: RNG_3510-PRC-FATAL-MSGS-_-3510-EXIT<br>*/
	private void rng3510PrcFatalMsgs() {
		String retcode = "";
		boolean goto3510A = false;
		prcFatalMsgs();
		do {
			goto3510A = false;
			retcode = a();
		} while (retcode.equals("3510-A"));
	}

	/**Original name: RNG_3520-PRC-WNG-MSGS-_-3520-EXIT<br>*/
	private void rng3520PrcWngMsgs() {
		String retcode = "";
		boolean goto3520A = false;
		prcWngMsgs();
		do {
			goto3520A = false;
			retcode = a1();
		} while (retcode.equals("3520-A"));
	}

	public void initCallableInputs() {
		ws.getXz009iServiceInputs().setPolNbr("");
		ws.getXz009iServiceInputs().setPolExpDt("");
		ws.getXz009iServiceInputs().setQteNbr(0);
		ws.getXz009iServiceInputs().setUri("");
		ws.getXz009iServiceInputs().setUsrId("");
		ws.getXz009iServiceInputs().setPwd("");
		ws.getXz009iServiceInputs().setTarSys("");
	}

	public void initTs571cb1() {
		ws.getTs571cb1().setServiceMnemonic("");
		ws.getTs571cb1().setUri("");
		ws.getTs571cb1().setCicsSysid("");
		ws.getTs571cb1().setServicePrefix("");
	}

	public void initCallableOutputs() {
		ws.getXzc090co().getTrsDtlRsp().getRetMsg().getRetCd().setRetCd(((short) 0));
		for (int idx0 = 1; idx0 <= Xz009oRetMsg.ERR_MSG_MAXOCCURS; idx0++) {
			ws.getXzc090co().getTrsDtlRsp().getRetMsg().setXzc09oErrMsg(idx0, "");
		}
		for (int idx0 = 1; idx0 <= Xz009oRetMsg.WNG_MSG_MAXOCCURS; idx0++) {
			ws.getXzc090co().getTrsDtlRsp().getRetMsg().setXzc09oWngMsg(idx0, "");
		}
		ws.getXzc090co().getTrsDtlRsp().setPolNbr("");
		ws.getXzc090co().getTrsDtlRsp().setPtEffDt("");
		ws.getXzc090co().getTrsDtlRsp().setPtExpDt("");
		ws.getXzc090co().getTrsDtlRsp().setQteNbr(0);
		ws.getXzc090co().getTrsDtlRsp().setActNbr("");
		ws.getXzc090co().getTrsDtlRsp().getNamedInsured().setXzc09oCtcId("");
		ws.getXzc090co().getTrsDtlRsp().getNamedInsured().setXzc09oDsyNm("");
		ws.getXzc090co().getTrsDtlRsp().getNamedInsured().setXzc09oAdrId("");
		ws.getXzc090co().getTrsDtlRsp().getNamedInsured().setXzc09oAdr1("");
		ws.getXzc090co().getTrsDtlRsp().getNamedInsured().setXzc09oAdr2("");
		ws.getXzc090co().getTrsDtlRsp().getNamedInsured().setXzc09oCitNm("");
		ws.getXzc090co().getTrsDtlRsp().getNamedInsured().setXzc09oStAbb("");
		ws.getXzc090co().getTrsDtlRsp().getNamedInsured().setXzc09oPstCd("");
		ws.getXzc090co().getTrsDtlRsp().getNamedInsured().setXzc09oCtyNm("");
		ws.getXzc090co().setErrorIndicator("");
		ws.getXzc090co().setFaultCode("");
		ws.getXzc090co().setFaultString("");
		ws.getXzc090co().setFaultActor("");
		ws.getXzc090co().setFaultDetail("");
		ws.getXzc090co().setNonSoapFaultErrTxt("");
	}

	public void initXzc09oServiceOutputs() {
		dfhcommarea.getXzc09oServiceOutputs().getRetMsg().getRetCd().setRetCd(((short) 0));
		for (int idx0 = 1; idx0 <= Xz009oRetMsg.ERR_MSG_MAXOCCURS; idx0++) {
			dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oErrMsg(idx0, "");
		}
		for (int idx0 = 1; idx0 <= Xz009oRetMsg.WNG_MSG_MAXOCCURS; idx0++) {
			dfhcommarea.getXzc09oServiceOutputs().getRetMsg().setXzc09oWngMsg(idx0, "");
		}
		dfhcommarea.getXzc09oServiceOutputs().setPolNbr("");
		dfhcommarea.getXzc09oServiceOutputs().setPtEffDt("");
		dfhcommarea.getXzc09oServiceOutputs().setPtExpDt("");
		dfhcommarea.getXzc09oServiceOutputs().setQteNbr(0);
		dfhcommarea.getXzc09oServiceOutputs().setActNbr("");
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oCtcId("");
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oDsyNm("");
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oAdrId("");
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oAdr1("");
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oAdr2("");
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oCitNm("");
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oStAbb("");
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oPstCd("");
		dfhcommarea.getXzc09oServiceOutputs().getNamedInsured().setXzc09oCtyNm("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
