/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: WS-PROCESS-CHILD-SW<br>
 * Variable: WS-PROCESS-CHILD-SW from program TS020100<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsProcessChildSw {

	//==== PROPERTIES ====
	private char value = 'C';
	public static final char CALL_CHILD = 'C';
	public static final char BYPASS_CHILD = 'B';

	//==== METHODS ====
	public void setProcessChildSw(char processChildSw) {
		this.value = processChildSw;
	}

	public char getProcessChildSw() {
		return this.value;
	}

	public void setCallChild() {
		value = CALL_CHILD;
	}

	public boolean isBypassChild() {
		return value == BYPASS_CHILD;
	}
}
