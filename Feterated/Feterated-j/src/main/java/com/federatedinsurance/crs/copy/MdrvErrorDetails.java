/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.MdrvErrorLoggingLvlSw;
import com.federatedinsurance.crs.ws.enums.MdrvErrorsLoggedSw;

/**Original name: MDRV-ERROR-DETAILS<br>
 * Variable: MDRV-ERROR-DETAILS from copybook HALLMDRV<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class MdrvErrorDetails {

	//==== PROPERTIES ====
	//Original name: MDRV-FAILED-MODULE
	private String failedModule = DefaultValues.stringVal(Len.FAILED_MODULE);
	//Original name: MDRV-FAILED-PARAGRAPH
	private String failedParagraph = DefaultValues.stringVal(Len.FAILED_PARAGRAPH);
	//Original name: MDRV-SQLCODE-DISPLAY
	private String sqlcodeDisplay = DefaultValues.stringVal(Len.SQLCODE_DISPLAY);
	//Original name: MDRV-EIBRESP-DISPLAY
	private String eibrespDisplay = DefaultValues.stringVal(Len.EIBRESP_DISPLAY);
	//Original name: MDRV-EIBRESP2-DISPLAY
	private String eibresp2Display = DefaultValues.stringVal(Len.EIBRESP2_DISPLAY);
	//Original name: MDRV-ERRORS-LOGGED-SW
	private MdrvErrorsLoggedSw errorsLoggedSw = new MdrvErrorsLoggedSw();
	//Original name: MDRV-ERROR-LOGGING-LVL-SW
	private MdrvErrorLoggingLvlSw errorLoggingLvlSw = new MdrvErrorLoggingLvlSw();
	//Original name: MDRV-ERR-LOG-SQLCODE-DSPLY
	private String errLogSqlcodeDsply = DefaultValues.stringVal(Len.ERR_LOG_SQLCODE_DSPLY);
	//Original name: MDRV-ERR-LOG-EIBRESP-DSPLY
	private String errLogEibrespDsply = DefaultValues.stringVal(Len.ERR_LOG_EIBRESP_DSPLY);
	//Original name: MDRV-ERR-LOG-EIBRESP2-DSPLY
	private String errLogEibresp2Dsply = DefaultValues.stringVal(Len.ERR_LOG_EIBRESP2_DSPLY);

	//==== METHODS ====
	public void setMdrvErrorDetailsBytes(byte[] buffer, int offset) {
		int position = offset;
		setMdrvModuleErrorInfoBytes(buffer, position);
		position += Len.MODULE_ERROR_INFO;
		setErrorLoggingInfoBytes(buffer, position);
	}

	public byte[] getErrorDetailsBytes(byte[] buffer, int offset) {
		int position = offset;
		getModuleErrorInfoBytes(buffer, position);
		position += Len.MODULE_ERROR_INFO;
		getErrorLoggingInfoBytes(buffer, position);
		return buffer;
	}

	public void setMdrvModuleErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		failedModule = MarshalByte.readString(buffer, position, Len.FAILED_MODULE);
		position += Len.FAILED_MODULE;
		failedParagraph = MarshalByte.readString(buffer, position, Len.FAILED_PARAGRAPH);
		position += Len.FAILED_PARAGRAPH;
		sqlcodeDisplay = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.SQLCODE_DISPLAY), Len.SQLCODE_DISPLAY);
		position += Len.SQLCODE_DISPLAY;
		eibrespDisplay = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.EIBRESP_DISPLAY), Len.EIBRESP_DISPLAY);
		position += Len.EIBRESP_DISPLAY;
		eibresp2Display = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.EIBRESP2_DISPLAY), Len.EIBRESP2_DISPLAY);
	}

	public byte[] getModuleErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, failedModule, Len.FAILED_MODULE);
		position += Len.FAILED_MODULE;
		MarshalByte.writeString(buffer, position, failedParagraph, Len.FAILED_PARAGRAPH);
		position += Len.FAILED_PARAGRAPH;
		MarshalByte.writeString(buffer, position, sqlcodeDisplay, Len.SQLCODE_DISPLAY);
		position += Len.SQLCODE_DISPLAY;
		MarshalByte.writeString(buffer, position, eibrespDisplay, Len.EIBRESP_DISPLAY);
		position += Len.EIBRESP_DISPLAY;
		MarshalByte.writeString(buffer, position, eibresp2Display, Len.EIBRESP2_DISPLAY);
		return buffer;
	}

	public void setFailedModule(String failedModule) {
		this.failedModule = Functions.subString(failedModule, Len.FAILED_MODULE);
	}

	public String getFailedModule() {
		return this.failedModule;
	}

	public void setFailedParagraph(String failedParagraph) {
		this.failedParagraph = Functions.subString(failedParagraph, Len.FAILED_PARAGRAPH);
	}

	public String getFailedParagraph() {
		return this.failedParagraph;
	}

	public void setSqlcodeDisplay(long sqlcodeDisplay) {
		this.sqlcodeDisplay = PicFormatter.display("-Z(8)9").format(sqlcodeDisplay).toString();
	}

	public void setSqlcodeDisplayFormatted(String sqlcodeDisplay) {
		this.sqlcodeDisplay = PicFormatter.display("-Z(8)9").format(sqlcodeDisplay).toString();
	}

	public long getSqlcodeDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.sqlcodeDisplay);
	}

	public void setEibrespDisplay(long eibrespDisplay) {
		this.eibrespDisplay = PicFormatter.display("-Z(8)9").format(eibrespDisplay).toString();
	}

	public void setEibrespDisplayFormatted(String eibrespDisplay) {
		this.eibrespDisplay = PicFormatter.display("-Z(8)9").format(eibrespDisplay).toString();
	}

	public long getEibrespDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.eibrespDisplay);
	}

	public void setEibresp2Display(long eibresp2Display) {
		this.eibresp2Display = PicFormatter.display("-Z(8)9").format(eibresp2Display).toString();
	}

	public void setEibresp2DisplayFormatted(String eibresp2Display) {
		this.eibresp2Display = PicFormatter.display("-Z(8)9").format(eibresp2Display).toString();
	}

	public long getEibresp2Display() {
		return PicParser.display("-Z(8)9").parseLong(this.eibresp2Display);
	}

	public void setErrorLoggingInfoBytes(byte[] buffer) {
		setErrorLoggingInfoBytes(buffer, 1);
	}

	public void setErrorLoggingInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		errorsLoggedSw.setErrorsLoggedSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		errorLoggingLvlSw.setErrorLoggingLvlSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		errLogSqlcodeDsply = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.ERR_LOG_SQLCODE_DSPLY), Len.ERR_LOG_SQLCODE_DSPLY);
		position += Len.ERR_LOG_SQLCODE_DSPLY;
		errLogEibrespDsply = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.ERR_LOG_EIBRESP_DSPLY), Len.ERR_LOG_EIBRESP_DSPLY);
		position += Len.ERR_LOG_EIBRESP_DSPLY;
		errLogEibresp2Dsply = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.ERR_LOG_EIBRESP2_DSPLY), Len.ERR_LOG_EIBRESP2_DSPLY);
	}

	public byte[] getErrorLoggingInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, errorsLoggedSw.getErrorsLoggedSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, errorLoggingLvlSw.getErrorLoggingLvlSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, errLogSqlcodeDsply, Len.ERR_LOG_SQLCODE_DSPLY);
		position += Len.ERR_LOG_SQLCODE_DSPLY;
		MarshalByte.writeString(buffer, position, errLogEibrespDsply, Len.ERR_LOG_EIBRESP_DSPLY);
		position += Len.ERR_LOG_EIBRESP_DSPLY;
		MarshalByte.writeString(buffer, position, errLogEibresp2Dsply, Len.ERR_LOG_EIBRESP2_DSPLY);
		return buffer;
	}

	public void setErrLogSqlcodeDsply(long errLogSqlcodeDsply) {
		this.errLogSqlcodeDsply = PicFormatter.display("-Z(8)9").format(errLogSqlcodeDsply).toString();
	}

	public void setErrLogSqlcodeDsplyFormatted(String errLogSqlcodeDsply) {
		this.errLogSqlcodeDsply = PicFormatter.display("-Z(8)9").format(errLogSqlcodeDsply).toString();
	}

	public long getErrLogSqlcodeDsply() {
		return PicParser.display("-Z(8)9").parseLong(this.errLogSqlcodeDsply);
	}

	public void setErrLogEibrespDsply(long errLogEibrespDsply) {
		this.errLogEibrespDsply = PicFormatter.display("-Z(8)9").format(errLogEibrespDsply).toString();
	}

	public void setErrLogEibrespDsplyFormatted(String errLogEibrespDsply) {
		this.errLogEibrespDsply = PicFormatter.display("-Z(8)9").format(errLogEibrespDsply).toString();
	}

	public long getErrLogEibrespDsply() {
		return PicParser.display("-Z(8)9").parseLong(this.errLogEibrespDsply);
	}

	public void setErrLogEibresp2Dsply(long errLogEibresp2Dsply) {
		this.errLogEibresp2Dsply = PicFormatter.display("-Z(8)9").format(errLogEibresp2Dsply).toString();
	}

	public void setErrLogEibresp2DsplyFormatted(String errLogEibresp2Dsply) {
		this.errLogEibresp2Dsply = PicFormatter.display("-Z(8)9").format(errLogEibresp2Dsply).toString();
	}

	public long getErrLogEibresp2Dsply() {
		return PicParser.display("-Z(8)9").parseLong(this.errLogEibresp2Dsply);
	}

	public MdrvErrorLoggingLvlSw getErrorLoggingLvlSw() {
		return errorLoggingLvlSw;
	}

	public MdrvErrorsLoggedSw getErrorsLoggedSw() {
		return errorsLoggedSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FAILED_MODULE = 8;
		public static final int FAILED_PARAGRAPH = 30;
		public static final int SQLCODE_DISPLAY = 10;
		public static final int EIBRESP_DISPLAY = 10;
		public static final int EIBRESP2_DISPLAY = 10;
		public static final int ERR_LOG_SQLCODE_DSPLY = 10;
		public static final int ERR_LOG_EIBRESP_DSPLY = 10;
		public static final int ERR_LOG_EIBRESP2_DSPLY = 10;
		public static final int MODULE_ERROR_INFO = FAILED_MODULE + FAILED_PARAGRAPH + SQLCODE_DISPLAY + EIBRESP_DISPLAY + EIBRESP2_DISPLAY;
		public static final int ERROR_LOGGING_INFO = MdrvErrorsLoggedSw.Len.ERRORS_LOGGED_SW + MdrvErrorLoggingLvlSw.Len.ERROR_LOGGING_LVL_SW
				+ ERR_LOG_SQLCODE_DSPLY + ERR_LOG_EIBRESP_DSPLY + ERR_LOG_EIBRESP2_DSPLY;
		public static final int ERROR_DETAILS = MODULE_ERROR_INFO + ERROR_LOGGING_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
