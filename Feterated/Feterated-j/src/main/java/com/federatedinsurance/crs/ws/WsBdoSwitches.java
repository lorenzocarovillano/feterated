/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.WsAuditTriggerSw;
import com.federatedinsurance.crs.ws.enums.WsCursorSelectionSw;
import com.federatedinsurance.crs.ws.enums.WsDataChangedSw;
import com.federatedinsurance.crs.ws.enums.WsEndOfCursor0Sw;
import com.federatedinsurance.crs.ws.enums.WsEndOfCursor1Sw;
import com.federatedinsurance.crs.ws.enums.WsFirstFetchSw;
import com.federatedinsurance.crs.ws.enums.WsHistorizedSw;
import com.federatedinsurance.crs.ws.enums.WsIudProcessedSw;
import com.federatedinsurance.crs.ws.enums.WsMasterSwitchInd;
import com.federatedinsurance.crs.ws.enums.WsMatchingSwitchSw;
import com.federatedinsurance.crs.ws.enums.WsMsgUmtLoopSw;
import com.federatedinsurance.crs.ws.enums.WsReadForMstrSwtInd;
import com.federatedinsurance.crs.ws.enums.WsReallyIsADeleteSw;
import com.federatedinsurance.crs.ws.enums.WsReqUmtReadSw;
import com.federatedinsurance.crs.ws.enums.WsReqUmtRecSw;
import com.federatedinsurance.crs.ws.enums.WsReqUmtRowUpdatedSw;
import com.federatedinsurance.crs.ws.enums.WsRespDataRecordSw;
import com.federatedinsurance.crs.ws.enums.WsRespDataRowsWritSw;
import com.federatedinsurance.crs.ws.enums.WsSwitchesTsqSw;
import com.federatedinsurance.crs.ws.enums.WsSwitchesTypeSw;
import com.federatedinsurance.crs.ws.enums.WsUmtHdrSw;
import com.federatedinsurance.crs.ws.enums.WsValidDateSw;

/**Original name: WS-BDO-SWITCHES<br>
 * Variable: WS-BDO-SWITCHES from program CAWS002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsBdoSwitches {

	//==== PROPERTIES ====
	//Original name: WS-MASTER-SWITCH-IND
	private WsMasterSwitchInd masterSwitchInd = new WsMasterSwitchInd();
	//Original name: WS-READ-FOR-MSTR-SWT-IND
	private WsReadForMstrSwtInd readForMstrSwtInd = new WsReadForMstrSwtInd();
	//Original name: WS-MSG-UMT-LOOP-SW
	private WsMsgUmtLoopSw msgUmtLoopSw = new WsMsgUmtLoopSw();
	//Original name: WS-MATCHING-SWITCH-SW
	private WsMatchingSwitchSw matchingSwitchSw = new WsMatchingSwitchSw();
	//Original name: WS-END-OF-CURSOR0-SW
	private WsEndOfCursor0Sw endOfCursor0Sw = new WsEndOfCursor0Sw();
	//Original name: WS-END-OF-CURSOR1-SW
	private WsEndOfCursor1Sw endOfCursor1Sw = new WsEndOfCursor1Sw();
	//Original name: WS-REQ-UMT-REC-SW
	private WsReqUmtRecSw reqUmtRecSw = new WsReqUmtRecSw();
	//Original name: WS-REQ-UMT-READ-SW
	private WsReqUmtReadSw reqUmtReadSw = new WsReqUmtReadSw();
	//Original name: WS-UMT-HDR-SW
	private WsUmtHdrSw umtHdrSw = new WsUmtHdrSw();
	//Original name: WS-REALLY-IS-A-DELETE-SW
	private WsReallyIsADeleteSw reallyIsADeleteSw = new WsReallyIsADeleteSw();
	//Original name: WS-DATA-CHANGED-SW
	private WsDataChangedSw dataChangedSw = new WsDataChangedSw();
	//Original name: WS-VALID-DATE-SW
	private WsValidDateSw validDateSw = new WsValidDateSw();
	//Original name: WS-REQ-UMT-ROW-UPDATED-SW
	private WsReqUmtRowUpdatedSw reqUmtRowUpdatedSw = new WsReqUmtRowUpdatedSw();
	//Original name: WS-CURSOR-SELECTION-SW
	private WsCursorSelectionSw cursorSelectionSw = new WsCursorSelectionSw();
	//Original name: WS-AUDIT-TRIGGER-SW
	private WsAuditTriggerSw auditTriggerSw = new WsAuditTriggerSw();
	//Original name: WS-HISTORIZED-SW
	private WsHistorizedSw historizedSw = new WsHistorizedSw();
	//Original name: WS-FIRST-FETCH-SW
	private WsFirstFetchSw firstFetchSw = new WsFirstFetchSw();
	//Original name: WS-RESP-DATA-ROWS-WRIT-SW
	private WsRespDataRowsWritSw respDataRowsWritSw = new WsRespDataRowsWritSw();
	//Original name: WS-RESP-DATA-RECORD-SW
	private WsRespDataRecordSw respDataRecordSw = new WsRespDataRecordSw();
	//Original name: WS-IUD-PROCESSED-SW
	private WsIudProcessedSw iudProcessedSw = new WsIudProcessedSw();
	//Original name: WS-SWITCHES-TSQ-SW
	private WsSwitchesTsqSw switchesTsqSw = new WsSwitchesTsqSw();
	//Original name: WS-SWITCHES-TYPE-SW
	private WsSwitchesTypeSw switchesTypeSw = new WsSwitchesTypeSw();

	//==== METHODS ====
	public WsAuditTriggerSw getAuditTriggerSw() {
		return auditTriggerSw;
	}

	public WsCursorSelectionSw getCursorSelectionSw() {
		return cursorSelectionSw;
	}

	public WsDataChangedSw getDataChangedSw() {
		return dataChangedSw;
	}

	public WsEndOfCursor0Sw getEndOfCursor0Sw() {
		return endOfCursor0Sw;
	}

	public WsEndOfCursor1Sw getEndOfCursor1Sw() {
		return endOfCursor1Sw;
	}

	public WsFirstFetchSw getFirstFetchSw() {
		return firstFetchSw;
	}

	public WsHistorizedSw getHistorizedSw() {
		return historizedSw;
	}

	public WsIudProcessedSw getIudProcessedSw() {
		return iudProcessedSw;
	}

	public WsMasterSwitchInd getMasterSwitchInd() {
		return masterSwitchInd;
	}

	public WsMatchingSwitchSw getMatchingSwitchSw() {
		return matchingSwitchSw;
	}

	public WsMsgUmtLoopSw getMsgUmtLoopSw() {
		return msgUmtLoopSw;
	}

	public WsReadForMstrSwtInd getReadForMstrSwtInd() {
		return readForMstrSwtInd;
	}

	public WsReallyIsADeleteSw getReallyIsADeleteSw() {
		return reallyIsADeleteSw;
	}

	public WsReqUmtReadSw getReqUmtReadSw() {
		return reqUmtReadSw;
	}

	public WsReqUmtRecSw getReqUmtRecSw() {
		return reqUmtRecSw;
	}

	public WsReqUmtRowUpdatedSw getReqUmtRowUpdatedSw() {
		return reqUmtRowUpdatedSw;
	}

	public WsRespDataRecordSw getRespDataRecordSw() {
		return respDataRecordSw;
	}

	public WsRespDataRowsWritSw getRespDataRowsWritSw() {
		return respDataRowsWritSw;
	}

	public WsSwitchesTsqSw getSwitchesTsqSw() {
		return switchesTsqSw;
	}

	public WsSwitchesTypeSw getSwitchesTypeSw() {
		return switchesTypeSw;
	}

	public WsUmtHdrSw getUmtHdrSw() {
		return umtHdrSw;
	}

	public WsValidDateSw getValidDateSw() {
		return validDateSw;
	}
}
