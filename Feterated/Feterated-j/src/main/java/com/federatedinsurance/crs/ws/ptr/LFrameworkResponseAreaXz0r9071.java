/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9071<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9071 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9071_MAXOCCURS = 2000;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9071() {
	}

	public LFrameworkResponseAreaXz0r9071(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a9071Formatted(int lFwRespXz0a9071Idx) {
		int position = Pos.lFwRespXz0a9071(lFwRespXz0a9071Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9071);
	}

	public void setlFwRespXz0a9071Bytes(int lFwRespXz0a9071Idx, byte[] buffer) {
		setlFwRespXz0a9071Bytes(lFwRespXz0a9071Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A9071<br>*/
	public byte[] getlFwRespXz0a9071Bytes(int lFwRespXz0a9071Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_XZ0A9071];
		return getlFwRespXz0a9071Bytes(lFwRespXz0a9071Idx, buffer, 1);
	}

	public void setlFwRespXz0a9071Bytes(int lFwRespXz0a9071Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9071(lFwRespXz0a9071Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A9071, position);
	}

	public byte[] getlFwRespXz0a9071Bytes(int lFwRespXz0a9071Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9071(lFwRespXz0a9071Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A9071, position);
		return buffer;
	}

	public void setXza971rMaxOvlRows(int xza971rMaxOvlRowsIdx, short xza971rMaxOvlRows) {
		int position = Pos.xza971MaxOvlRows(xza971rMaxOvlRowsIdx - 1);
		writeBinaryShort(position, xza971rMaxOvlRows);
	}

	/**Original name: XZA971R-MAX-OVL-ROWS<br>*/
	public short getXza971rMaxOvlRows(int xza971rMaxOvlRowsIdx) {
		int position = Pos.xza971MaxOvlRows(xza971rMaxOvlRowsIdx - 1);
		return readBinaryShort(position);
	}

	/**Original name: XZA971R-ACY-OVL-INFO<br>*/
	public byte[] getXza971rAcyOvlInfoBytes(int xza971rAcyOvlInfoIdx) {
		byte[] buffer = new byte[Len.XZA971_ACY_OVL_INFO];
		return getXza971rAcyOvlInfoBytes(xza971rAcyOvlInfoIdx, buffer, 1);
	}

	public byte[] getXza971rAcyOvlInfoBytes(int xza971rAcyOvlInfoIdx, byte[] buffer, int offset) {
		int position = Pos.xza971AcyOvlInfo(xza971rAcyOvlInfoIdx - 1);
		getBytes(buffer, offset, Len.XZA971_ACY_OVL_INFO, position);
		return buffer;
	}

	public void setXza971rFrmNbr(int xza971rFrmNbrIdx, String xza971rFrmNbr) {
		int position = Pos.xza971FrmNbr(xza971rFrmNbrIdx - 1);
		writeString(position, xza971rFrmNbr, Len.XZA971_FRM_NBR);
	}

	/**Original name: XZA971R-FRM-NBR<br>*/
	public String getXza971rFrmNbr(int xza971rFrmNbrIdx) {
		int position = Pos.xza971FrmNbr(xza971rFrmNbrIdx - 1);
		return readString(position, Len.XZA971_FRM_NBR);
	}

	public void setXza971rFrmEdtDt(int xza971rFrmEdtDtIdx, String xza971rFrmEdtDt) {
		int position = Pos.xza971FrmEdtDt(xza971rFrmEdtDtIdx - 1);
		writeString(position, xza971rFrmEdtDt, Len.XZA971_FRM_EDT_DT);
	}

	/**Original name: XZA971R-FRM-EDT-DT<br>*/
	public String getXza971rFrmEdtDt(int xza971rFrmEdtDtIdx) {
		int position = Pos.xza971FrmEdtDt(xza971rFrmEdtDtIdx - 1);
		return readString(position, Len.XZA971_FRM_EDT_DT);
	}

	public void setXza971rRecTypCd(int xza971rRecTypCdIdx, String xza971rRecTypCd) {
		int position = Pos.xza971RecTypCd(xza971rRecTypCdIdx - 1);
		writeString(position, xza971rRecTypCd, Len.XZA971_REC_TYP_CD);
	}

	/**Original name: XZA971R-REC-TYP-CD<br>*/
	public String getXza971rRecTypCd(int xza971rRecTypCdIdx) {
		int position = Pos.xza971RecTypCd(xza971rRecTypCdIdx - 1);
		return readString(position, Len.XZA971_REC_TYP_CD);
	}

	public void setXza971rOvlEdlFrmNm(int xza971rOvlEdlFrmNmIdx, String xza971rOvlEdlFrmNm) {
		int position = Pos.xza971OvlEdlFrmNm(xza971rOvlEdlFrmNmIdx - 1);
		writeString(position, xza971rOvlEdlFrmNm, Len.XZA971_OVL_EDL_FRM_NM);
	}

	/**Original name: XZA971R-OVL-EDL-FRM-NM<br>*/
	public String getXza971rOvlEdlFrmNm(int xza971rOvlEdlFrmNmIdx) {
		int position = Pos.xza971OvlEdlFrmNm(xza971rOvlEdlFrmNmIdx - 1);
		return readString(position, Len.XZA971_OVL_EDL_FRM_NM);
	}

	public void setXza971rStAbb(int xza971rStAbbIdx, String xza971rStAbb) {
		int position = Pos.xza971StAbb(xza971rStAbbIdx - 1);
		writeString(position, xza971rStAbb, Len.XZA971_ST_ABB);
	}

	/**Original name: XZA971R-ST-ABB<br>*/
	public String getXza971rStAbb(int xza971rStAbbIdx) {
		int position = Pos.xza971StAbb(xza971rStAbbIdx - 1);
		return readString(position, Len.XZA971_ST_ABB);
	}

	public void setXza971rSpePrcCd(int xza971rSpePrcCdIdx, String xza971rSpePrcCd) {
		int position = Pos.xza971SpePrcCd(xza971rSpePrcCdIdx - 1);
		writeString(position, xza971rSpePrcCd, Len.XZA971_SPE_PRC_CD);
	}

	/**Original name: XZA971R-SPE-PRC-CD<br>*/
	public String getXza971rSpePrcCd(int xza971rSpePrcCdIdx) {
		int position = Pos.xza971SpePrcCd(xza971rSpePrcCdIdx - 1);
		return readString(position, Len.XZA971_SPE_PRC_CD);
	}

	public void setXza971rOvlSrCd(int xza971rOvlSrCdIdx, String xza971rOvlSrCd) {
		int position = Pos.xza971OvlSrCd(xza971rOvlSrCdIdx - 1);
		writeString(position, xza971rOvlSrCd, Len.XZA971_OVL_SR_CD);
	}

	/**Original name: XZA971R-OVL-SR-CD<br>*/
	public String getXza971rOvlSrCd(int xza971rOvlSrCdIdx) {
		int position = Pos.xza971OvlSrCd(xza971rOvlSrCdIdx - 1);
		return readString(position, Len.XZA971_OVL_SR_CD);
	}

	public void setXza971rOvlDes(int xza971rOvlDesIdx, String xza971rOvlDes) {
		int position = Pos.xza971OvlDes(xza971rOvlDesIdx - 1);
		writeString(position, xza971rOvlDes, Len.XZA971_OVL_DES);
	}

	/**Original name: XZA971R-OVL-DES<br>*/
	public String getXza971rOvlDes(int xza971rOvlDesIdx) {
		int position = Pos.xza971OvlDes(xza971rOvlDesIdx - 1);
		return readString(position, Len.XZA971_OVL_DES);
	}

	public void setXza971rXclvInd(int xza971rXclvIndIdx, char xza971rXclvInd) {
		int position = Pos.xza971XclvInd(xza971rXclvIndIdx - 1);
		writeChar(position, xza971rXclvInd);
	}

	/**Original name: XZA971R-XCLV-IND<br>*/
	public char getXza971rXclvInd(int xza971rXclvIndIdx) {
		int position = Pos.xza971XclvInd(xza971rXclvIndIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9071(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9071;
		}

		public static int xza971AcyOvlRow(int idx) {
			return lFwRespXz0a9071(idx);
		}

		public static int xza971MaxOvlRows(int idx) {
			return xza971AcyOvlRow(idx);
		}

		public static int xza971AcyOvlInfo(int idx) {
			return xza971MaxOvlRows(idx) + Len.XZA971_MAX_OVL_ROWS;
		}

		public static int xza971FrmNbr(int idx) {
			return xza971AcyOvlInfo(idx);
		}

		public static int xza971FrmEdtDt(int idx) {
			return xza971FrmNbr(idx) + Len.XZA971_FRM_NBR;
		}

		public static int xza971RecTypCd(int idx) {
			return xza971FrmEdtDt(idx) + Len.XZA971_FRM_EDT_DT;
		}

		public static int xza971OvlEdlFrmNm(int idx) {
			return xza971RecTypCd(idx) + Len.XZA971_REC_TYP_CD;
		}

		public static int xza971StAbb(int idx) {
			return xza971OvlEdlFrmNm(idx) + Len.XZA971_OVL_EDL_FRM_NM;
		}

		public static int xza971SpePrcCd(int idx) {
			return xza971StAbb(idx) + Len.XZA971_ST_ABB;
		}

		public static int xza971OvlSrCd(int idx) {
			return xza971SpePrcCd(idx) + Len.XZA971_SPE_PRC_CD;
		}

		public static int xza971OvlDes(int idx) {
			return xza971OvlSrCd(idx) + Len.XZA971_OVL_SR_CD;
		}

		public static int xza971XclvInd(int idx) {
			return xza971OvlDes(idx) + Len.XZA971_OVL_DES;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA971_MAX_OVL_ROWS = 2;
		public static final int XZA971_FRM_NBR = 30;
		public static final int XZA971_FRM_EDT_DT = 10;
		public static final int XZA971_REC_TYP_CD = 5;
		public static final int XZA971_OVL_EDL_FRM_NM = 30;
		public static final int XZA971_ST_ABB = 2;
		public static final int XZA971_SPE_PRC_CD = 8;
		public static final int XZA971_OVL_SR_CD = 8;
		public static final int XZA971_OVL_DES = 30;
		public static final int XZA971_XCLV_IND = 1;
		public static final int XZA971_ACY_OVL_INFO = XZA971_FRM_NBR + XZA971_FRM_EDT_DT + XZA971_REC_TYP_CD + XZA971_OVL_EDL_FRM_NM + XZA971_ST_ABB
				+ XZA971_SPE_PRC_CD + XZA971_OVL_SR_CD + XZA971_OVL_DES + XZA971_XCLV_IND;
		public static final int XZA971_ACY_OVL_ROW = XZA971_MAX_OVL_ROWS + XZA971_ACY_OVL_INFO;
		public static final int L_FW_RESP_XZ0A9071 = XZA971_ACY_OVL_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0r9071.L_FW_RESP_XZ0A9071_MAXOCCURS * L_FW_RESP_XZ0A9071;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
