/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;

/**Original name: L-SC-CRT1-COP<br>
 * Variable: L-SC-CRT1-COP from copybook TT008001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LScCrt1Cop {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char BGN = 'B';
	public static final char CTA = 'C';
	public static final char EQU = 'E';
	public static final char N_A = Types.SPACE_CHAR;

	//==== METHODS ====
	public void setCrt1Cop(char crt1Cop) {
		this.value = crt1Cop;
	}

	public char getCrt1Cop() {
		return this.value;
	}

	public void setEqu() {
		value = EQU;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CRT1_COP = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
