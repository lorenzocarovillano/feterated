/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-03-NO-REC-ATTACHED-ERROR<br>
 * Variable: EA-03-NO-REC-ATTACHED-ERROR from program XZ0P90E0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea03NoRecAttachedError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR
	private String flr1 = "No recipient";
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR-1
	private String flr2 = "was attached";
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR-2
	private String flr3 = "to a form that";
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR-3
	private String flr4 = "must have a";
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR-4
	private String flr5 = "recipient tied";
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR-5
	private String flr6 = "to it";
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR-6
	private String flr7 = ". Account =";
	//Original name: EA-03-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR-7
	private String flr8 = "; Notification";
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR-8
	private String flr9 = "Timestamp =";
	//Original name: EA-03-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-03-NO-REC-ATTACHED-ERROR-9
	private char flr10 = '.';

	//==== METHODS ====
	public String getEa03NoRecAttachedErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa03NoRecAttachedErrorBytes());
	}

	public byte[] getEa03NoRecAttachedErrorBytes() {
		byte[] buffer = new byte[Len.EA03_NO_REC_ATTACHED_ERROR];
		return getEa03NoRecAttachedErrorBytes(buffer, 1);
	}

	public byte[] getEa03NoRecAttachedErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr10);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr10() {
		return this.flr10;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 13;
		public static final int FLR3 = 15;
		public static final int FLR4 = 12;
		public static final int FLR6 = 5;
		public static final int FLR10 = 1;
		public static final int EA03_NO_REC_ATTACHED_ERROR = CSR_ACT_NBR + NOT_PRC_TS + FLR10 + 2 * FLR1 + 3 * FLR3 + 3 * FLR4 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
