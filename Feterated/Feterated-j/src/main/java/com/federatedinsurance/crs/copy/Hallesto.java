/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.federatedinsurance.crs.ws.enums.EstoCallEtraSw;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;

/**Original name: HALLESTO<br>
 * Variable: HALLESTO from copybook HALLESTO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Hallesto {

	//==== PROPERTIES ====
	//Original name: ESTO-INPUT-KEY
	private EstoInputKey estoInputKey = new EstoInputKey();
	/**Original name: ESTO-CALL-ETRA-SW<br>
	 * <pre>**       07 ESTO-ERR-SEQ-NUM                PIC 9(03).</pre>*/
	private EstoCallEtraSw estoCallEtraSw = new EstoCallEtraSw();
	//Original name: ESTO-DETAIL-BUFFER
	private EstoDetailBuffer estoDetailBuffer = new EstoDetailBuffer();
	//Original name: ESTO-OUTPUT
	private EstoOutput estoOutput = new EstoOutput();

	//==== METHODS ====
	public void setEstoStoreInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		estoInputKey.setEstoInputKeyBytes(buffer, position);
		position += EstoInputKey.Len.ESTO_INPUT_KEY;
		estoCallEtraSw.setEstoCallEtraSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		estoDetailBuffer.setEstoDetailBufferFromBuffer(buffer, position);
	}

	public byte[] getEstoStoreInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		estoInputKey.getEstoInputKeyBytes(buffer, position);
		position += EstoInputKey.Len.ESTO_INPUT_KEY;
		MarshalByte.writeChar(buffer, position, estoCallEtraSw.getEstoCallEtraSw());
		position += Types.CHAR_SIZE;
		estoDetailBuffer.getEstoDetailBufferAsBuffer(buffer, position);
		return buffer;
	}

	public void setEstoReturnInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		estoOutput.setEstoOutputBytes(buffer, position);
	}

	public byte[] getEstoReturnInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		estoOutput.getEstoOutputBytes(buffer, position);
		return buffer;
	}

	public EstoDetailBuffer getEstoDetailBuffer() {
		return estoDetailBuffer;
	}

	public EstoInputKey getEstoInputKey() {
		return estoInputKey;
	}

	public EstoOutput getEstoOutput() {
		return estoOutput;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ESTO_STORE_INFO = EstoInputKey.Len.ESTO_INPUT_KEY + EstoCallEtraSw.Len.ESTO_CALL_ETRA_SW
				+ EstoDetailBuffer.Len.ESTO_DETAIL_BUFFER;
		public static final int ESTO_RETURN_INFO = EstoOutput.Len.ESTO_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
