/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-03-NO-IMP-CNC-FOUND-MSG<br>
 * Variable: EA-03-NO-IMP-CNC-FOUND-MSG from program XZ0B9060<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea03NoImpCncFoundMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-03-NO-IMP-CNC-FOUND-MSG
	private String flr1 = "No impending";
	//Original name: FILLER-EA-03-NO-IMP-CNC-FOUND-MSG-1
	private String flr2 = "cancel found";
	//Original name: FILLER-EA-03-NO-IMP-CNC-FOUND-MSG-2
	private String flr3 = "for";
	//Original name: FILLER-EA-03-NO-IMP-CNC-FOUND-MSG-3
	private String flr4 = "search criteria";
	//Original name: FILLER-EA-03-NO-IMP-CNC-FOUND-MSG-4
	private String flr5 = " entered:";
	//Original name: FILLER-EA-03-NO-IMP-CNC-FOUND-MSG-5
	private String flr6 = "Account =";
	//Original name: EA-03-CSR-ACT-NBR
	private String ea03CsrActNbr = DefaultValues.stringVal(Len.EA03_CSR_ACT_NBR);
	//Original name: FILLER-EA-03-NO-IMP-CNC-FOUND-MSG-6
	private char flr7 = '.';

	//==== METHODS ====
	public String getEa03NoImpCncFoundMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa03NoImpCncFoundMsgBytes());
	}

	public byte[] getEa03NoImpCncFoundMsgBytes() {
		byte[] buffer = new byte[Len.EA03_NO_IMP_CNC_FOUND_MSG];
		return getEa03NoImpCncFoundMsgBytes(buffer, 1);
	}

	public byte[] getEa03NoImpCncFoundMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea03CsrActNbr, Len.EA03_CSR_ACT_NBR);
		position += Len.EA03_CSR_ACT_NBR;
		MarshalByte.writeChar(buffer, position, flr7);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setEa03CsrActNbr(String ea03CsrActNbr) {
		this.ea03CsrActNbr = Functions.subString(ea03CsrActNbr, Len.EA03_CSR_ACT_NBR);
	}

	public String getEa03CsrActNbr() {
		return this.ea03CsrActNbr;
	}

	public char getFlr7() {
		return this.flr7;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA03_CSR_ACT_NBR = 9;
		public static final int FLR1 = 13;
		public static final int FLR3 = 4;
		public static final int FLR4 = 15;
		public static final int FLR5 = 10;
		public static final int FLR7 = 1;
		public static final int EA03_NO_IMP_CNC_FOUND_MSG = EA03_CSR_ACT_NBR + 2 * FLR1 + FLR3 + FLR4 + 2 * FLR5 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
