/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotFrm;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRec2;
import com.federatedinsurance.crs.commons.data.to.IFrmRecAtcTyp;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: DCLACT-NOT-FRM<br>
 * Variable: DCLACT-NOT-FRM from copybook XZH00003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclactNotFrm implements IActNotFrm, IFrmRecAtcTyp, IActNotFrmRec2 {

	//==== PROPERTIES ====
	//Original name: CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FRM-SEQ-NBR
	private short frmSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: FRM-NBR
	private String frmNbr = DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: FRM-EDT-DT
	private String frmEdtDt = DefaultValues.stringVal(Len.FRM_EDT_DT);

	//==== METHODS ====
	public String getXzh006ActNotFrmRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzh006ActNotFrmRowBytes());
	}

	public byte[] getXzh006ActNotFrmRowBytes() {
		byte[] buffer = new byte[Len.XZH006_ACT_NOT_FRM_ROW];
		return getXzh006ActNotFrmRowBytes(buffer, 1);
	}

	public byte[] getXzh006ActNotFrmRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeBinaryShort(buffer, position, frmSeqNbr);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, frmNbr, Len.FRM_NBR);
		position += Len.FRM_NBR;
		MarshalByte.writeString(buffer, position, frmEdtDt, Len.FRM_EDT_DT);
		return buffer;
	}

	public void initXzh006ActNotFrmRowSpaces() {
		csrActNbr = "";
		notPrcTs = "";
		frmSeqNbr = Types.INVALID_BINARY_SHORT_VAL;
		frmNbr = "";
		frmEdtDt = "";
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public void setFrmSeqNbr(short frmSeqNbr) {
		this.frmSeqNbr = frmSeqNbr;
	}

	@Override
	public short getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	@Override
	public void setFrmNbr(String frmNbr) {
		this.frmNbr = Functions.subString(frmNbr, Len.FRM_NBR);
	}

	@Override
	public String getFrmNbr() {
		return this.frmNbr;
	}

	@Override
	public void setFrmEdtDt(String frmEdtDt) {
		this.frmEdtDt = Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	@Override
	public String getFrmEdtDt() {
		return this.frmEdtDt;
	}

	@Override
	public char getAddInsRecInd() {
		throw new FieldNotMappedException("addInsRecInd");
	}

	@Override
	public void setAddInsRecInd(char addInsRecInd) {
		throw new FieldNotMappedException("addInsRecInd");
	}

	@Override
	public char getAniRecInd() {
		throw new FieldNotMappedException("aniRecInd");
	}

	@Override
	public void setAniRecInd(char aniRecInd) {
		throw new FieldNotMappedException("aniRecInd");
	}

	@Override
	public char getCerRecInd() {
		throw new FieldNotMappedException("cerRecInd");
	}

	@Override
	public void setCerRecInd(char cerRecInd) {
		throw new FieldNotMappedException("cerRecInd");
	}

	@Override
	public String getEdtDt() {
		return getFrmEdtDt();
	}

	@Override
	public void setEdtDt(String edtDt) {
		this.setFrmEdtDt(edtDt);
	}

	@Override
	public String getFrmSpeAtcCd() {
		throw new FieldNotMappedException("frmSpeAtcCd");
	}

	@Override
	public void setFrmSpeAtcCd(String frmSpeAtcCd) {
		throw new FieldNotMappedException("frmSpeAtcCd");
	}

	@Override
	public String getFrmSpeAtcCdObj() {
		return getFrmSpeAtcCd();
	}

	@Override
	public void setFrmSpeAtcCdObj(String frmSpeAtcCdObj) {
		setFrmSpeAtcCd(frmSpeAtcCdObj);
	}

	@Override
	public char getInsdRecInd() {
		throw new FieldNotMappedException("insdRecInd");
	}

	@Override
	public void setInsdRecInd(char insdRecInd) {
		throw new FieldNotMappedException("insdRecInd");
	}

	@Override
	public char getLpeMgeRecInd() {
		throw new FieldNotMappedException("lpeMgeRecInd");
	}

	@Override
	public void setLpeMgeRecInd(char lpeMgeRecInd) {
		throw new FieldNotMappedException("lpeMgeRecInd");
	}

	@Override
	public String getNbr() {
		return getFrmNbr();
	}

	@Override
	public void setNbr(String nbr) {
		this.setFrmNbr(nbr);
	}

	@Override
	public short getSeqNbr() {
		return getFrmSeqNbr();
	}

	@Override
	public void setSeqNbr(short seqNbr) {
		this.setFrmSeqNbr(seqNbr);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FRM_NBR = 30;
		public static final int FRM_EDT_DT = 10;
		public static final int FRM_SEQ_NBR = 2;
		public static final int XZH006_ACT_NOT_FRM_ROW = CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR + FRM_NBR + FRM_EDT_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
