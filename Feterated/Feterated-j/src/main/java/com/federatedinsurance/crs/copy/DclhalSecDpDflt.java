/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.HsddCmnAtrInd;
import com.federatedinsurance.crs.ws.enums.HsddDflOfsPer;
import com.federatedinsurance.crs.ws.enums.HsddDflTypInd;
import com.federatedinsurance.crs.ws.enums.HsddReqTypCd;

/**Original name: DCLHAL-SEC-DP-DFLT<br>
 * Variable: DCLHAL-SEC-DP-DFLT from copybook HALLGSDD<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalSecDpDflt {

	//==== PROPERTIES ====
	//Original name: HSDD-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: HSDD-SEC-GRP-NM
	private String secGrpNm = DefaultValues.stringVal(Len.SEC_GRP_NM);
	//Original name: HSDD-SEC-ASC-TYP
	private String secAscTyp = DefaultValues.stringVal(Len.SEC_ASC_TYP);
	//Original name: HSDD-GEN-TXT-FLD-1
	private String genTxtFld1 = DefaultValues.stringVal(Len.GEN_TXT_FLD1);
	//Original name: HSDD-GEN-TXT-FLD-2
	private String genTxtFld2 = DefaultValues.stringVal(Len.GEN_TXT_FLD2);
	//Original name: HSDD-GEN-TXT-FLD-3
	private String genTxtFld3 = DefaultValues.stringVal(Len.GEN_TXT_FLD3);
	//Original name: HSDD-GEN-TXT-FLD-4
	private String genTxtFld4 = DefaultValues.stringVal(Len.GEN_TXT_FLD4);
	//Original name: HSDD-GEN-TXT-FLD-5
	private String genTxtFld5 = DefaultValues.stringVal(Len.GEN_TXT_FLD5);
	//Original name: HSDD-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: HSDD-REQ-TYP-CD
	private HsddReqTypCd reqTypCd = new HsddReqTypCd();
	//Original name: HSDD-EFFECTIVE-DT
	private String effectiveDt = DefaultValues.stringVal(Len.EFFECTIVE_DT);
	//Original name: HSDD-EXPIRATION-DT
	private String expirationDt = DefaultValues.stringVal(Len.EXPIRATION_DT);
	//Original name: HSDD-SEQ-NBR
	private int seqNbr = DefaultValues.BIN_INT_VAL;
	//Original name: HSDD-CMN-NM
	private String cmnNm = DefaultValues.stringVal(Len.CMN_NM);
	//Original name: HSDD-CMN-ATR-IND
	private HsddCmnAtrInd cmnAtrInd = new HsddCmnAtrInd();
	//Original name: HSDD-DFL-TYP-IND
	private HsddDflTypInd dflTypInd = new HsddDflTypInd();
	//Original name: HSDD-DFL-DTA-AMT
	private AfDecimal dflDtaAmt = new AfDecimal(DefaultValues.DEC_VAL, 14, 2);
	//Original name: HSDD-DFL-DTA-TXT
	private String dflDtaTxt = DefaultValues.stringVal(Len.DFL_DTA_TXT);
	//Original name: HSDD-DFL-OFS-NBR
	private int dflOfsNbr = DefaultValues.BIN_INT_VAL;
	//Original name: HSDD-DFL-OFS-PER
	private HsddDflOfsPer dflOfsPer = new HsddDflOfsPer();
	//Original name: HSDD-CONTEXT
	private String context = DefaultValues.stringVal(Len.CONTEXT);

	//==== METHODS ====
	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public void setSecGrpNm(String secGrpNm) {
		this.secGrpNm = Functions.subString(secGrpNm, Len.SEC_GRP_NM);
	}

	public String getSecGrpNm() {
		return this.secGrpNm;
	}

	public void setSecAscTyp(String secAscTyp) {
		this.secAscTyp = Functions.subString(secAscTyp, Len.SEC_ASC_TYP);
	}

	public String getSecAscTyp() {
		return this.secAscTyp;
	}

	public void setGenTxtFld1(String genTxtFld1) {
		this.genTxtFld1 = Functions.subString(genTxtFld1, Len.GEN_TXT_FLD1);
	}

	public String getGenTxtFld1() {
		return this.genTxtFld1;
	}

	public void setGenTxtFld2(String genTxtFld2) {
		this.genTxtFld2 = Functions.subString(genTxtFld2, Len.GEN_TXT_FLD2);
	}

	public String getGenTxtFld2() {
		return this.genTxtFld2;
	}

	public void setGenTxtFld3(String genTxtFld3) {
		this.genTxtFld3 = Functions.subString(genTxtFld3, Len.GEN_TXT_FLD3);
	}

	public String getGenTxtFld3() {
		return this.genTxtFld3;
	}

	public void setGenTxtFld4(String genTxtFld4) {
		this.genTxtFld4 = Functions.subString(genTxtFld4, Len.GEN_TXT_FLD4);
	}

	public String getGenTxtFld4() {
		return this.genTxtFld4;
	}

	public void setGenTxtFld5(String genTxtFld5) {
		this.genTxtFld5 = Functions.subString(genTxtFld5, Len.GEN_TXT_FLD5);
	}

	public String getGenTxtFld5() {
		return this.genTxtFld5;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public String getBusObjNmFormatted() {
		return Functions.padBlanks(getBusObjNm(), Len.BUS_OBJ_NM);
	}

	public void setEffectiveDt(String effectiveDt) {
		this.effectiveDt = Functions.subString(effectiveDt, Len.EFFECTIVE_DT);
	}

	public String getEffectiveDt() {
		return this.effectiveDt;
	}

	public void setExpirationDt(String expirationDt) {
		this.expirationDt = Functions.subString(expirationDt, Len.EXPIRATION_DT);
	}

	public String getExpirationDt() {
		return this.expirationDt;
	}

	public void setSeqNbr(int seqNbr) {
		this.seqNbr = seqNbr;
	}

	public int getSeqNbr() {
		return this.seqNbr;
	}

	public void setCmnNm(String cmnNm) {
		this.cmnNm = Functions.subString(cmnNm, Len.CMN_NM);
	}

	public String getCmnNm() {
		return this.cmnNm;
	}

	public String getCmnNmFormatted() {
		return Functions.padBlanks(getCmnNm(), Len.CMN_NM);
	}

	public void setDflDtaAmt(AfDecimal dflDtaAmt) {
		this.dflDtaAmt.assign(dflDtaAmt);
	}

	public AfDecimal getDflDtaAmt() {
		return this.dflDtaAmt.copy();
	}

	public void setDflDtaTxt(String dflDtaTxt) {
		this.dflDtaTxt = Functions.subString(dflDtaTxt, Len.DFL_DTA_TXT);
	}

	public String getDflDtaTxt() {
		return this.dflDtaTxt;
	}

	public String getDflDtaTxtFormatted() {
		return Functions.padBlanks(getDflDtaTxt(), Len.DFL_DTA_TXT);
	}

	public void setDflOfsNbr(int dflOfsNbr) {
		this.dflOfsNbr = dflOfsNbr;
	}

	public int getDflOfsNbr() {
		return this.dflOfsNbr;
	}

	public void setContext(String context) {
		this.context = Functions.subString(context, Len.CONTEXT);
	}

	public String getContext() {
		return this.context;
	}

	public HsddCmnAtrInd getCmnAtrInd() {
		return cmnAtrInd;
	}

	public HsddDflOfsPer getDflOfsPer() {
		return dflOfsPer;
	}

	public HsddDflTypInd getDflTypInd() {
		return dflTypInd;
	}

	public HsddReqTypCd getReqTypCd() {
		return reqTypCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NM = 32;
		public static final int SEC_GRP_NM = 8;
		public static final int SEC_ASC_TYP = 8;
		public static final int GEN_TXT_FLD1 = 20;
		public static final int GEN_TXT_FLD2 = 8;
		public static final int GEN_TXT_FLD3 = 8;
		public static final int GEN_TXT_FLD4 = 8;
		public static final int GEN_TXT_FLD5 = 8;
		public static final int BUS_OBJ_NM = 32;
		public static final int EFFECTIVE_DT = 10;
		public static final int EXPIRATION_DT = 10;
		public static final int CMN_NM = 32;
		public static final int DFL_DTA_TXT = 32;
		public static final int CONTEXT = 20;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
