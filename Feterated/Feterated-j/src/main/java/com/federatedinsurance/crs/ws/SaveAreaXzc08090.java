/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.SaCicsApplidXzc08090;
import com.federatedinsurance.crs.ws.enums.SaTarSys;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaveAreaXzc08090 {

	//==== PROPERTIES ====
	//Original name: SA-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: SA-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: SA-CHN-NM
	private String chnNm = DefaultValues.stringVal(Len.CHN_NM);
	//Original name: SA-WEB-SVC-URL
	private String webSvcUrl = "";
	//Original name: SA-UC-USR-ID
	private String ucUsrId = DefaultValues.stringVal(Len.UC_USR_ID);
	//Original name: SA-UC-PWD
	private String ucPwd = DefaultValues.stringVal(Len.UC_PWD);
	//Original name: SA-CICS-APPLID
	private SaCicsApplidXzc08090 cicsApplid = new SaCicsApplidXzc08090();
	//Original name: SA-TAR-SYS
	private SaTarSys tarSys = new SaTarSys();

	//==== METHODS ====
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public void setChnNm(String chnNm) {
		this.chnNm = Functions.subString(chnNm, Len.CHN_NM);
	}

	public String getChnNm() {
		return this.chnNm;
	}

	public String getChnNmFormatted() {
		return Functions.padBlanks(getChnNm(), Len.CHN_NM);
	}

	public void setWebSvcUrl(String webSvcUrl) {
		this.webSvcUrl = Functions.subString(webSvcUrl, Len.WEB_SVC_URL);
	}

	public String getWebSvcUrl() {
		return this.webSvcUrl;
	}

	public void setUsrCredentialsBytes(byte[] buffer) {
		setUsrCredentialsBytes(buffer, 1);
	}

	public void setUsrCredentialsBytes(byte[] buffer, int offset) {
		int position = offset;
		ucUsrId = MarshalByte.readString(buffer, position, Len.UC_USR_ID);
		position += Len.UC_USR_ID;
		ucPwd = MarshalByte.readString(buffer, position, Len.UC_PWD);
	}

	public void setUcUsrId(String ucUsrId) {
		this.ucUsrId = Functions.subString(ucUsrId, Len.UC_USR_ID);
	}

	public String getUcUsrId() {
		return this.ucUsrId;
	}

	public void setUcPwd(String ucPwd) {
		this.ucPwd = Functions.subString(ucPwd, Len.UC_PWD);
	}

	public String getUcPwd() {
		return this.ucPwd;
	}

	public SaCicsApplidXzc08090 getCicsApplid() {
		return cicsApplid;
	}

	public SaTarSys getTarSys() {
		return tarSys;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CHN_NM = 16;
		public static final int UC_USR_ID = 8;
		public static final int UC_PWD = 8;
		public static final int WEB_SVC_URL = 256;
		public static final int USR_CREDENTIALS = UC_USR_ID + UC_PWD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
