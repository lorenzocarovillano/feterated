/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Halluchs;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALOUCHS<br>
 * Generated as a class for rule WS.<br>*/
public class HalouchsData {

	//==== PROPERTIES ====
	//Original name: WS-WORK-FIELDS
	private WsWorkFieldsHalouchs wsWorkFields = new WsWorkFieldsHalouchs();
	//Original name: HALLUCHS
	private Halluchs halluchs = new Halluchs();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void setWsHalouchsLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.WS_HALOUCHS_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.WS_HALOUCHS_LINKAGE);
		setWsHalouchsLinkageBytes(buffer, 1);
	}

	public String getWsHalouchsLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getWsHalouchsLinkageBytes());
	}

	/**Original name: WS-HALOUCHS-LINKAGE<br>
	 * <pre> HALOUCHS LINKAGE</pre>*/
	public byte[] getWsHalouchsLinkageBytes() {
		byte[] buffer = new byte[Len.WS_HALOUCHS_LINKAGE];
		return getWsHalouchsLinkageBytes(buffer, 1);
	}

	public void setWsHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluchs.setLength2(MarshalByte.readBinaryShort(buffer, position));
		position += Types.SHORT_SIZE;
		halluchs.setStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		halluchs.checkSum = MarshalByte.readFixedString(buffer, position, Halluchs.Len.CHECK_SUM);
	}

	public byte[] getWsHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, halluchs.getLength2());
		position += Types.SHORT_SIZE;
		halluchs.getStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		MarshalByte.writeString(buffer, position, halluchs.checkSum, Halluchs.Len.CHECK_SUM);
		return buffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Halluchs getHalluchs() {
		return halluchs;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsWorkFieldsHalouchs getWsWorkFields() {
		return wsWorkFields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_APPLID = 8;
		public static final int WS_HALOUCHS_LINKAGE = Halluchs.Len.LENGTH2 + Halluchs.Len.STRING_FLD + Halluchs.Len.CHECK_SUM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
