/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-DATE-ADJ-FORMAT-RED<br>
 * Variable: WS-DATE-ADJ-FORMAT-RED from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsDateAdjFormatRed {

	//==== PROPERTIES ====
	public static final String DATE_ADJUST_HOURS = "HD";
	public static final String DATE_ADJUST_DAYS = "DD";
	public static final String DATE_ADJUST_WEEKS = "WD";
	public static final String DATE_ADJUST_MONTHS = "MD";
	public static final String DATE_ADJUST_YEARS = "YD";
	public static final String DATE_ADJUST_AGE = "BD";
	//Original name: WS-DATE-ADJ-FORMAT-POS1
	private char formatPos1 = DefaultValues.CHAR_VAL;
	//Original name: WS-DATE-ADJ-FORMAT-POS2
	private char formatPos2 = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setWsDateAdjFormat(String wsDateAdjFormat) {
		int position = 1;
		byte[] buffer = getWsDateAdjFormatRedBytes();
		MarshalByte.writeString(buffer, position, wsDateAdjFormat, Len.WS_DATE_ADJ_FORMAT);
		setWsDateAdjFormatRedBytes(buffer);
	}

	/**Original name: WS-DATE-ADJ-FORMAT<br>*/
	public String getWsDateAdjFormat() {
		int position = 1;
		return MarshalByte.readString(getWsDateAdjFormatRedBytes(), position, Len.WS_DATE_ADJ_FORMAT);
	}

	public boolean isDateAdjustHours() {
		return getWsDateAdjFormat().equals(DATE_ADJUST_HOURS);
	}

	public boolean isDateAdjustDays() {
		return getWsDateAdjFormat().equals(DATE_ADJUST_DAYS);
	}

	public boolean isDateAdjustWeeks() {
		return getWsDateAdjFormat().equals(DATE_ADJUST_WEEKS);
	}

	public boolean isDateAdjustMonths() {
		return getWsDateAdjFormat().equals(DATE_ADJUST_MONTHS);
	}

	public boolean isDateAdjustYears() {
		return getWsDateAdjFormat().equals(DATE_ADJUST_YEARS);
	}

	public boolean isDateAdjustAge() {
		return getWsDateAdjFormat().equals(DATE_ADJUST_AGE);
	}

	public void setWsDateAdjFormatRedBytes(byte[] buffer) {
		setWsDateAdjFormatRedBytes(buffer, 1);
	}

	/**Original name: WS-DATE-ADJ-FORMAT-RED<br>*/
	public byte[] getWsDateAdjFormatRedBytes() {
		byte[] buffer = new byte[Len.WS_DATE_ADJ_FORMAT_RED];
		return getWsDateAdjFormatRedBytes(buffer, 1);
	}

	public void setWsDateAdjFormatRedBytes(byte[] buffer, int offset) {
		int position = offset;
		formatPos1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		formatPos2 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getWsDateAdjFormatRedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, formatPos1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, formatPos2);
		return buffer;
	}

	public void setFormatPos1(char formatPos1) {
		this.formatPos1 = formatPos1;
	}

	public char getFormatPos1() {
		return this.formatPos1;
	}

	public void setFormatPos2(char formatPos2) {
		this.formatPos2 = formatPos2;
	}

	public char getFormatPos2() {
		return this.formatPos2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FORMAT_POS1 = 1;
		public static final int FORMAT_POS2 = 1;
		public static final int WS_DATE_ADJ_FORMAT_RED = FORMAT_POS1 + FORMAT_POS2;
		public static final int WS_DATE_ADJ_FORMAT = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_DATE_ADJ_FORMAT = 2;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
