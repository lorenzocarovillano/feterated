/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ESTO-ERR-FLOOD-IND<br>
 * Variable: ESTO-ERR-FLOOD-IND from copybook HALLESTO<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class EstoErrFloodInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NOT_ERROR_FLOOD = 'N';
	public static final char ERROR_FLOOD = 'Y';

	//==== METHODS ====
	public void setFloodInd(char floodInd) {
		this.value = floodInd;
	}

	public char getFloodInd() {
		return this.value;
	}

	public boolean isNotErrorFlood() {
		return value == NOT_ERROR_FLOOD;
	}

	public void setEstoNotErrorFlood() {
		value = NOT_ERROR_FLOOD;
	}

	public boolean isEstoErrorFlood() {
		return value == ERROR_FLOOD;
	}

	public void setEstoErrorFlood() {
		value = ERROR_FLOOD;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLOOD_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
