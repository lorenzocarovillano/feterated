/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0004<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0004 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0004() {
	}

	public LServiceContractAreaXz0x0004(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt004ServiceInputsBytes(byte[] buffer) {
		setXzt004ServiceInputsBytes(buffer, 1);
	}

	public void setXzt004ServiceInputsBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.XZT004_SERVICE_INPUTS, Pos.XZT004_SERVICE_INPUTS);
	}

	public void setXzt04iAccountNumber(String xzt04iAccountNumber) {
		writeString(Pos.XZT04I_ACCOUNT_NUMBER, xzt04iAccountNumber, Len.XZT04I_ACCOUNT_NUMBER);
	}

	/**Original name: XZT04I-ACCOUNT-NUMBER<br>*/
	public String getXzt04iAccountNumber() {
		return readString(Pos.XZT04I_ACCOUNT_NUMBER, Len.XZT04I_ACCOUNT_NUMBER);
	}

	public void setXzt04iOwnerState(String xzt04iOwnerState) {
		writeString(Pos.XZT04I_OWNER_STATE, xzt04iOwnerState, Len.XZT04I_OWNER_STATE);
	}

	/**Original name: XZT04I-OWNER-STATE<br>*/
	public String getXzt04iOwnerState() {
		return readString(Pos.XZT04I_OWNER_STATE, Len.XZT04I_OWNER_STATE);
	}

	public void setXzt04iPolicyNumber(String xzt04iPolicyNumber) {
		writeString(Pos.XZT04I_POLICY_NUMBER, xzt04iPolicyNumber, Len.XZT04I_POLICY_NUMBER);
	}

	/**Original name: XZT04I-POLICY-NUMBER<br>*/
	public String getXzt04iPolicyNumber() {
		return readString(Pos.XZT04I_POLICY_NUMBER, Len.XZT04I_POLICY_NUMBER);
	}

	public void setXzt04iPolicyEffDt(String xzt04iPolicyEffDt) {
		writeString(Pos.XZT04I_POLICY_EFF_DT, xzt04iPolicyEffDt, Len.XZT04I_POLICY_EFF_DT);
	}

	/**Original name: XZT04I-POLICY-EFF-DT<br>*/
	public String getXzt04iPolicyEffDt() {
		return readString(Pos.XZT04I_POLICY_EFF_DT, Len.XZT04I_POLICY_EFF_DT);
	}

	public void setXzt04iPolicyExpDt(String xzt04iPolicyExpDt) {
		writeString(Pos.XZT04I_POLICY_EXP_DT, xzt04iPolicyExpDt, Len.XZT04I_POLICY_EXP_DT);
	}

	/**Original name: XZT04I-POLICY-EXP-DT<br>*/
	public String getXzt04iPolicyExpDt() {
		return readString(Pos.XZT04I_POLICY_EXP_DT, Len.XZT04I_POLICY_EXP_DT);
	}

	public void setXzt04iPolicyType(String xzt04iPolicyType) {
		writeString(Pos.XZT04I_POLICY_TYPE, xzt04iPolicyType, Len.XZT04I_POLICY_TYPE);
	}

	/**Original name: XZT04I-POLICY-TYPE<br>*/
	public String getXzt04iPolicyType() {
		return readString(Pos.XZT04I_POLICY_TYPE, Len.XZT04I_POLICY_TYPE);
	}

	public void setXzt04iPolicyCancDt(String xzt04iPolicyCancDt) {
		writeString(Pos.XZT04I_POLICY_CANC_DT, xzt04iPolicyCancDt, Len.XZT04I_POLICY_CANC_DT);
	}

	/**Original name: XZT04I-POLICY-CANC-DT<br>*/
	public String getXzt04iPolicyCancDt() {
		return readString(Pos.XZT04I_POLICY_CANC_DT, Len.XZT04I_POLICY_CANC_DT);
	}

	public void setXzt04iUserid(String xzt04iUserid) {
		writeString(Pos.XZT04I_USERID, xzt04iUserid, Len.XZT04I_USERID);
	}

	/**Original name: XZT04I-USERID<br>*/
	public String getXzt04iUserid() {
		return readString(Pos.XZT04I_USERID, Len.XZT04I_USERID);
	}

	public String getXzt04iUseridFormatted() {
		return Functions.padBlanks(getXzt04iUserid(), Len.XZT04I_USERID);
	}

	/**Original name: XZT004-SERVICE-OUTPUTS<br>*/
	public byte[] getXzt004ServiceOutputsBytes() {
		byte[] buffer = new byte[Len.XZT004_SERVICE_OUTPUTS];
		return getXzt004ServiceOutputsBytes(buffer, 1);
	}

	public byte[] getXzt004ServiceOutputsBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.XZT004_SERVICE_OUTPUTS, Pos.XZT004_SERVICE_OUTPUTS);
		return buffer;
	}

	public void setXzt04oRescindInd(char xzt04oRescindInd) {
		writeChar(Pos.XZT04O_RESCIND_IND, xzt04oRescindInd);
	}

	/**Original name: XZT04O-RESCIND-IND<br>*/
	public char getXzt04oRescindInd() {
		return readChar(Pos.XZT04O_RESCIND_IND);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT004_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT04I_ACCOUNT_NUMBER = XZT004_SERVICE_INPUTS;
		public static final int XZT04I_OWNER_STATE = XZT04I_ACCOUNT_NUMBER + Len.XZT04I_ACCOUNT_NUMBER;
		public static final int XZT04I_POLICY_NUMBER = XZT04I_OWNER_STATE + Len.XZT04I_OWNER_STATE;
		public static final int XZT04I_POLICY_EFF_DT = XZT04I_POLICY_NUMBER + Len.XZT04I_POLICY_NUMBER;
		public static final int XZT04I_POLICY_EXP_DT = XZT04I_POLICY_EFF_DT + Len.XZT04I_POLICY_EFF_DT;
		public static final int XZT04I_POLICY_TYPE = XZT04I_POLICY_EXP_DT + Len.XZT04I_POLICY_EXP_DT;
		public static final int XZT04I_POLICY_CANC_DT = XZT04I_POLICY_TYPE + Len.XZT04I_POLICY_TYPE;
		public static final int XZT04I_USERID = XZT04I_POLICY_CANC_DT + Len.XZT04I_POLICY_CANC_DT;
		public static final int XZT004_SERVICE_OUTPUTS = XZT04I_USERID + Len.XZT04I_USERID;
		public static final int XZT04O_RESCIND_IND = XZT004_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT04I_ACCOUNT_NUMBER = 9;
		public static final int XZT04I_OWNER_STATE = 2;
		public static final int XZT04I_POLICY_NUMBER = 25;
		public static final int XZT04I_POLICY_EFF_DT = 10;
		public static final int XZT04I_POLICY_EXP_DT = 10;
		public static final int XZT04I_POLICY_TYPE = 3;
		public static final int XZT04I_POLICY_CANC_DT = 10;
		public static final int XZT04I_USERID = 8;
		public static final int XZT004_SERVICE_INPUTS = XZT04I_ACCOUNT_NUMBER + XZT04I_OWNER_STATE + XZT04I_POLICY_NUMBER + XZT04I_POLICY_EFF_DT
				+ XZT04I_POLICY_EXP_DT + XZT04I_POLICY_TYPE + XZT04I_POLICY_CANC_DT + XZT04I_USERID;
		public static final int XZT04O_RESCIND_IND = 1;
		public static final int XZT004_SERVICE_OUTPUTS = XZT04O_RESCIND_IND;
		public static final int L_SERVICE_CONTRACT_AREA = XZT004_SERVICE_INPUTS + XZT004_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
