/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.DfhxcploTs547099;
import com.federatedinsurance.crs.copy.Dfhxcrco;
import com.federatedinsurance.crs.ws.occurs.TpPipeInf;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS547099<br>
 * Generated as a class for rule WS.<br>*/
public class Ts547099Data {

	//==== PROPERTIES ====
	public static final int TP_PIPE_INF_MAXOCCURS = 25;
	//Original name: AA-OPEN-PIPES
	private short aaOpenPipes = ((short) 0);
	//Original name: AA-PIPES-AVAIL
	private short aaPipesAvail = ((short) 0);
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsTs547099 constantFields = new ConstantFieldsTs547099();
	//Original name: CICS-PIPE-MAINTENANCE-CNT
	private LCicsPipeMaintenanceCnt cicsPipeMaintenanceCnt = new LCicsPipeMaintenanceCnt();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesTs547099 errorAndAdviceMessages = new ErrorAndAdviceMessagesTs547099();
	//Original name: SAVE-AREA
	private SaveAreaTs547099 saveArea = new SaveAreaTs547099();
	//Original name: SUBSCRIPTS
	private SubscriptsTs547099 subscripts = new SubscriptsTs547099();
	//Original name: SWITCHES
	private SwitchesTs547099 switches = new SwitchesTs547099();
	//Original name: TP-PIPE-INF
	private TpPipeInf[] tpPipeInf = new TpPipeInf[TP_PIPE_INF_MAXOCCURS];
	//Original name: DFHXCPLO
	private DfhxcploTs547099 dfhxcplo = new DfhxcploTs547099();
	//Original name: DFHXCRCO
	private Dfhxcrco dfhxcrco = new Dfhxcrco();

	//==== CONSTRUCTORS ====
	public Ts547099Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int tpPipeInfIdx = 1; tpPipeInfIdx <= TP_PIPE_INF_MAXOCCURS; tpPipeInfIdx++) {
			tpPipeInf[tpPipeInfIdx - 1] = new TpPipeInf();
		}
	}

	public void setAaOpenPipes(short aaOpenPipes) {
		this.aaOpenPipes = aaOpenPipes;
	}

	public short getAaOpenPipes() {
		return this.aaOpenPipes;
	}

	public void setAaPipesAvail(short aaPipesAvail) {
		this.aaPipesAvail = aaPipesAvail;
	}

	public short getAaPipesAvail() {
		return this.aaPipesAvail;
	}

	public LCicsPipeMaintenanceCnt getCicsPipeMaintenanceCnt() {
		return cicsPipeMaintenanceCnt;
	}

	public ConstantFieldsTs547099 getConstantFields() {
		return constantFields;
	}

	public DfhxcploTs547099 getDfhxcplo() {
		return dfhxcplo;
	}

	public Dfhxcrco getDfhxcrco() {
		return dfhxcrco;
	}

	public ErrorAndAdviceMessagesTs547099 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public SaveAreaTs547099 getSaveArea() {
		return saveArea;
	}

	public SubscriptsTs547099 getSubscripts() {
		return subscripts;
	}

	public SwitchesTs547099 getSwitches() {
		return switches;
	}

	public TpPipeInf getTpPipeInf(int idx) {
		return tpPipeInf[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TABLE_OF_PIPES = Ts547099Data.TP_PIPE_INF_MAXOCCURS * TpPipeInf.Len.TP_PIPE_INF;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
