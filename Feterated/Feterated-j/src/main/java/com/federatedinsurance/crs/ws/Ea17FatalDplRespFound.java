/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.ws.enums.Ea17DplDesc;

/**Original name: EA-17-FATAL-DPL-RESP-FOUND<br>
 * Variable: EA-17-FATAL-DPL-RESP-FOUND from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea17FatalDplRespFound {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-17-FATAL-DPL-RESP-FOUND
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-17-FATAL-DPL-RESP-FOUND-1
	private String flr2 = "THE FOLLOWING";
	//Original name: FILLER-EA-17-FATAL-DPL-RESP-FOUND-2
	private String flr3 = "FATAL DPL";
	//Original name: FILLER-EA-17-FATAL-DPL-RESP-FOUND-3
	private String flr4 = "RESPONSE HAS";
	//Original name: FILLER-EA-17-FATAL-DPL-RESP-FOUND-4
	private String flr5 = "BEEN CAPTURED:";
	//Original name: EA-17-DPL-DESC
	private Ea17DplDesc ea17DplDesc = new Ea17DplDesc();

	//==== METHODS ====
	public String getEa17FatalDplRespFoundFormatted() {
		return MarshalByteExt.bufferToStr(getEa17FatalDplRespFoundBytes());
	}

	public byte[] getEa17FatalDplRespFoundBytes() {
		byte[] buffer = new byte[Len.EA17_FATAL_DPL_RESP_FOUND];
		return getEa17FatalDplRespFoundBytes(buffer, 1);
	}

	public byte[] getEa17FatalDplRespFoundBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea17DplDesc.getEa17DplDesc(), Ea17DplDesc.Len.EA17_DPL_DESC);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public Ea17DplDesc getEa17DplDesc() {
		return ea17DplDesc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 14;
		public static final int FLR3 = 10;
		public static final int FLR4 = 13;
		public static final int FLR5 = 16;
		public static final int EA17_FATAL_DPL_RESP_FOUND = Ea17DplDesc.Len.EA17_DPL_DESC + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
