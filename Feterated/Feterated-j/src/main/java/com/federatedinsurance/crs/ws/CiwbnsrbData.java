/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.Cisltnam;
import com.federatedinsurance.crs.copy.DgrstrgParmlist;
import com.federatedinsurance.crs.ws.occurs.WsParsedNameEntries;
import com.federatedinsurance.crs.ws.redefines.WsErrorMessages;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program CIWBNSRB<br>
 * Generated as a class for rule WS.<br>*/
public class CiwbnsrbData {

	//==== PROPERTIES ====
	public static final int WS_PARSED_NAME_ENTRIES_MAXOCCURS = 10;
	//Original name: WS-NAME-WORK-AREA
	private WsNameWorkArea wsNameWorkArea = new WsNameWorkArea();
	//Original name: WORK-IDX
	private int workIdx = 1;
	//Original name: WS-PARSED-NAME-ENTRIES
	private WsParsedNameEntries[] wsParsedNameEntries = new WsParsedNameEntries[WS_PARSED_NAME_ENTRIES_MAXOCCURS];
	//Original name: PARSE-IDX
	private int parseIdx = 1;
	//Original name: SWITCHES
	private SwitchesCiwbnsrb switches = new SwitchesCiwbnsrb();
	//Original name: MISCELLANEOUS
	private Miscellaneous miscellaneous = new Miscellaneous();
	//Original name: LAST-IDX
	private int lastIdx = 1;
	//Original name: WS-ERROR-MESSAGES
	private WsErrorMessages wsErrorMessages = new WsErrorMessages();
	//Original name: ERROR-INDEX
	private int errorIndex = 1;
	//Original name: WS-STRING-DATA
	private WsStringData wsStringData = new WsStringData();
	//Original name: CISLTNAM
	private Cisltnam cisltnam = new Cisltnam();
	//Original name: X
	private int x = 1;
	//Original name: CX
	private int cx = 1;
	//Original name: SX
	private int sx = 1;
	//Original name: PX
	private int px = 1;
	//Original name: DGRSTRG-PARMLIST
	private DgrstrgParmlist dgrstrgParmlist = new DgrstrgParmlist();
	//Original name: NAME-INDEX
	private int nameIndex = 1;
	//Original name: NAME-OUT-INDEX
	private int nameOutIndex = 1;
	//Original name: RETURN-IDX
	private int returnIdx = 1;

	//==== CONSTRUCTORS ====
	public CiwbnsrbData() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int wsParsedNameEntriesIdx = 1; wsParsedNameEntriesIdx <= WS_PARSED_NAME_ENTRIES_MAXOCCURS; wsParsedNameEntriesIdx++) {
			wsParsedNameEntries[wsParsedNameEntriesIdx - 1] = new WsParsedNameEntries();
		}
	}

	public void setWorkIdx(int workIdx) {
		this.workIdx = workIdx;
	}

	public int getWorkIdx() {
		return this.workIdx;
	}

	public void initWsParseNameTableSpaces() {
		for (int idx = 1; idx <= WS_PARSED_NAME_ENTRIES_MAXOCCURS; idx++) {
			wsParsedNameEntries[idx - 1].initWsParsedNameEntriesSpaces();
		}
	}

	public void setParseIdx(int parseIdx) {
		this.parseIdx = parseIdx;
	}

	public int getParseIdx() {
		return this.parseIdx;
	}

	public void setLastIdx(int lastIdx) {
		this.lastIdx = lastIdx;
	}

	public int getLastIdx() {
		return this.lastIdx;
	}

	public void setErrorIndex(int errorIndex) {
		this.errorIndex = errorIndex;
	}

	public int getErrorIndex() {
		return this.errorIndex;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getX() {
		return this.x;
	}

	public void setCx(int cx) {
		this.cx = cx;
	}

	public int getCx() {
		return this.cx;
	}

	public void setSx(int sx) {
		this.sx = sx;
	}

	public int getSx() {
		return this.sx;
	}

	public void setPx(int px) {
		this.px = px;
	}

	public int getPx() {
		return this.px;
	}

	public void setNameIndex(int nameIndex) {
		this.nameIndex = nameIndex;
	}

	public int getNameIndex() {
		return this.nameIndex;
	}

	public void setNameOutIndex(int nameOutIndex) {
		this.nameOutIndex = nameOutIndex;
	}

	public int getNameOutIndex() {
		return this.nameOutIndex;
	}

	public void setReturnIdx(int returnIdx) {
		this.returnIdx = returnIdx;
	}

	public int getReturnIdx() {
		return this.returnIdx;
	}

	public Cisltnam getCisltnam() {
		return cisltnam;
	}

	public DgrstrgParmlist getDgrstrgParmlist() {
		return dgrstrgParmlist;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public SwitchesCiwbnsrb getSwitches() {
		return switches;
	}

	public WsErrorMessages getWsErrorMessages() {
		return wsErrorMessages;
	}

	public WsNameWorkArea getWsNameWorkArea() {
		return wsNameWorkArea;
	}

	public WsParsedNameEntries getWsParsedNameEntries(int idx) {
		return wsParsedNameEntries[idx - 1];
	}

	public WsStringData getWsStringData() {
		return wsStringData;
	}
}
