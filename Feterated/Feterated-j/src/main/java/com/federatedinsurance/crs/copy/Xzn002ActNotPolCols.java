/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

/**Original name: XZN002-ACT-NOT-POL-COLS<br>
 * Variable: XZN002-ACT-NOT-POL-COLS from copybook XZ0N0002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzn002ActNotPolCols {

	//==== PROPERTIES ====
	//Original name: XZN002-TRANS-PROCESS-DT
	private String transProcessDt = "Transaction Processing Date";
	//Original name: XZN002-POL-TYP-CD
	private String polTypCd = "Pol Typ Cd";
	//Original name: XZN002-POL-PRI-RSK-ST-ABB
	private String polPriRskStAbb = "Pol Pri Rsk St Abb";
	//Original name: XZN002-NOT-EFF-DT
	private String notEffDt = "Not Eff Dt";
	//Original name: XZN002-POL-EFF-DT
	private String polEffDt = "Pol Eff Dt";
	//Original name: XZN002-POL-EXP-DT
	private String polExpDt = "Pol Exp Dt";
	//Original name: XZN002-POL-DUE-AMT
	private String polDueAmt = "Pol Due Amt";
	//Original name: XZN002-NIN-CLT-ID
	private String ninCltId = "Nin Clt Id";
	//Original name: XZN002-NIN-ADR-ID
	private String ninAdrId = "Nin Adr Id";

	//==== METHODS ====
	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public String getPolTypCd() {
		return this.polTypCd;
	}

	public String getPolPriRskStAbb() {
		return this.polPriRskStAbb;
	}

	public String getNotEffDt() {
		return this.notEffDt;
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public String getPolDueAmt() {
		return this.polDueAmt;
	}

	public String getNinCltId() {
		return this.ninCltId;
	}

	public String getNinAdrId() {
		return this.ninAdrId;
	}
}
