/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program TS571098<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaTs571098 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: UI-TSQ-NAME
	private String tsqName = DefaultValues.stringVal(Len.TSQ_NAME);
	//Original name: UI-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: UI-PASSWORD
	private String password = DefaultValues.stringVal(Len.PASSWORD);
	//Original name: UI-ERROR
	private String error = DefaultValues.stringVal(Len.ERROR);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		setRoutineInputsBytes(buffer, position);
		position += Len.ROUTINE_INPUTS;
		setRoutineOutputsBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		getRoutineInputsBytes(buffer, position);
		position += Len.ROUTINE_INPUTS;
		getRoutineOutputsBytes(buffer, position);
		return buffer;
	}

	public void setRoutineInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		tsqName = MarshalByte.readString(buffer, position, Len.TSQ_NAME);
	}

	public byte[] getRoutineInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tsqName, Len.TSQ_NAME);
		return buffer;
	}

	public void setTsqName(String tsqName) {
		this.tsqName = Functions.subString(tsqName, Len.TSQ_NAME);
	}

	public String getTsqName() {
		return this.tsqName;
	}

	public String getTsqNameFormatted() {
		return Functions.padBlanks(getTsqName(), Len.TSQ_NAME);
	}

	public void setRoutineOutputsBytes(byte[] buffer) {
		setRoutineOutputsBytes(buffer, 1);
	}

	/**Original name: UI-ROUTINE-OUTPUTS<br>*/
	public byte[] getRoutineOutputsBytes() {
		byte[] buffer = new byte[Len.ROUTINE_OUTPUTS];
		return getRoutineOutputsBytes(buffer, 1);
	}

	public void setRoutineOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		password = MarshalByte.readString(buffer, position, Len.PASSWORD);
		position += Len.PASSWORD;
		error = MarshalByte.readString(buffer, position, Len.ERROR);
	}

	public byte[] getRoutineOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeString(buffer, position, password, Len.PASSWORD);
		position += Len.PASSWORD;
		MarshalByte.writeString(buffer, position, error, Len.ERROR);
		return buffer;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setPassword(String password) {
		this.password = Functions.subString(password, Len.PASSWORD);
	}

	public String getPassword() {
		return this.password;
	}

	public void setError(String error) {
		this.error = Functions.subString(error, Len.ERROR);
	}

	public String getError() {
		return this.error;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TSQ_NAME = 16;
		public static final int ROUTINE_INPUTS = TSQ_NAME;
		public static final int USER_ID = 8;
		public static final int PASSWORD = 8;
		public static final int ERROR = 256;
		public static final int ROUTINE_OUTPUTS = USER_ID + PASSWORD + ERROR;
		public static final int DFHCOMMAREA = ROUTINE_INPUTS + ROUTINE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
