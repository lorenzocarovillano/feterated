/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZA972-ACY-TAG-INFO<br>
 * Variable: XZA972-ACY-TAG-INFO from copybook XZ0A9072<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xza972AcyTagInfo {

	//==== PROPERTIES ====
	//Original name: XZA972-EDL-FRM-NM
	private String edlFrmNm = DefaultValues.stringVal(Len.EDL_FRM_NM);
	//Original name: XZA972-DTA-GRP-NM
	private String dtaGrpNm = DefaultValues.stringVal(Len.DTA_GRP_NM);
	//Original name: XZA972-DTA-GRP-FLD-NBR
	private int dtaGrpFldNbr = DefaultValues.INT_VAL;
	//Original name: XZA972-TAG-NM
	private String tagNm = DefaultValues.stringVal(Len.TAG_NM);
	//Original name: XZA972-JUS-CD
	private char jusCd = DefaultValues.CHAR_VAL;
	//Original name: XZA972-SPE-PRC-CD
	private String spePrcCd = DefaultValues.stringVal(Len.SPE_PRC_CD);
	//Original name: XZA972-LEN-NBR
	private int lenNbr = DefaultValues.INT_VAL;
	//Original name: XZA972-TAG-OCC-CNT
	private int tagOccCnt = DefaultValues.INT_VAL;
	//Original name: XZA972-DEL-IND
	private char delInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setAcyTagInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		edlFrmNm = MarshalByte.readString(buffer, position, Len.EDL_FRM_NM);
		position += Len.EDL_FRM_NM;
		dtaGrpNm = MarshalByte.readString(buffer, position, Len.DTA_GRP_NM);
		position += Len.DTA_GRP_NM;
		dtaGrpFldNbr = MarshalByte.readInt(buffer, position, Len.DTA_GRP_FLD_NBR);
		position += Len.DTA_GRP_FLD_NBR;
		tagNm = MarshalByte.readString(buffer, position, Len.TAG_NM);
		position += Len.TAG_NM;
		jusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		spePrcCd = MarshalByte.readString(buffer, position, Len.SPE_PRC_CD);
		position += Len.SPE_PRC_CD;
		lenNbr = MarshalByte.readInt(buffer, position, Len.LEN_NBR);
		position += Len.LEN_NBR;
		tagOccCnt = MarshalByte.readInt(buffer, position, Len.TAG_OCC_CNT);
		position += Len.TAG_OCC_CNT;
		delInd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getAcyTagInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, edlFrmNm, Len.EDL_FRM_NM);
		position += Len.EDL_FRM_NM;
		MarshalByte.writeString(buffer, position, dtaGrpNm, Len.DTA_GRP_NM);
		position += Len.DTA_GRP_NM;
		MarshalByte.writeInt(buffer, position, dtaGrpFldNbr, Len.DTA_GRP_FLD_NBR);
		position += Len.DTA_GRP_FLD_NBR;
		MarshalByte.writeString(buffer, position, tagNm, Len.TAG_NM);
		position += Len.TAG_NM;
		MarshalByte.writeChar(buffer, position, jusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, spePrcCd, Len.SPE_PRC_CD);
		position += Len.SPE_PRC_CD;
		MarshalByte.writeInt(buffer, position, lenNbr, Len.LEN_NBR);
		position += Len.LEN_NBR;
		MarshalByte.writeInt(buffer, position, tagOccCnt, Len.TAG_OCC_CNT);
		position += Len.TAG_OCC_CNT;
		MarshalByte.writeChar(buffer, position, delInd);
		return buffer;
	}

	public void setEdlFrmNm(String edlFrmNm) {
		this.edlFrmNm = Functions.subString(edlFrmNm, Len.EDL_FRM_NM);
	}

	public String getEdlFrmNm() {
		return this.edlFrmNm;
	}

	public void setDtaGrpNm(String dtaGrpNm) {
		this.dtaGrpNm = Functions.subString(dtaGrpNm, Len.DTA_GRP_NM);
	}

	public String getDtaGrpNm() {
		return this.dtaGrpNm;
	}

	public void setDtaGrpFldNbr(int dtaGrpFldNbr) {
		this.dtaGrpFldNbr = dtaGrpFldNbr;
	}

	public int getDtaGrpFldNbr() {
		return this.dtaGrpFldNbr;
	}

	public void setTagNm(String tagNm) {
		this.tagNm = Functions.subString(tagNm, Len.TAG_NM);
	}

	public String getTagNm() {
		return this.tagNm;
	}

	public void setJusCd(char jusCd) {
		this.jusCd = jusCd;
	}

	public char getJusCd() {
		return this.jusCd;
	}

	public void setSpePrcCd(String spePrcCd) {
		this.spePrcCd = Functions.subString(spePrcCd, Len.SPE_PRC_CD);
	}

	public String getSpePrcCd() {
		return this.spePrcCd;
	}

	public void setLenNbr(int lenNbr) {
		this.lenNbr = lenNbr;
	}

	public int getLenNbr() {
		return this.lenNbr;
	}

	public void setTagOccCnt(int tagOccCnt) {
		this.tagOccCnt = tagOccCnt;
	}

	public int getTagOccCnt() {
		return this.tagOccCnt;
	}

	public void setDelInd(char delInd) {
		this.delInd = delInd;
	}

	public char getDelInd() {
		return this.delInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EDL_FRM_NM = 30;
		public static final int DTA_GRP_NM = 8;
		public static final int DTA_GRP_FLD_NBR = 5;
		public static final int TAG_NM = 30;
		public static final int JUS_CD = 1;
		public static final int SPE_PRC_CD = 8;
		public static final int LEN_NBR = 5;
		public static final int TAG_OCC_CNT = 5;
		public static final int DEL_IND = 1;
		public static final int ACY_TAG_INFO = EDL_FRM_NM + DTA_GRP_NM + DTA_GRP_FLD_NBR + TAG_NM + JUS_CD + SPE_PRC_CD + LEN_NBR + TAG_OCC_CNT
				+ DEL_IND;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
