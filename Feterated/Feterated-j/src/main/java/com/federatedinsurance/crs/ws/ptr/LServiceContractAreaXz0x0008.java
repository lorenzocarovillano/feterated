/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0008<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0008 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0008() {
	}

	public LServiceContractAreaXz0x0008(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt08iTkNotPrcTs(String xzt08iTkNotPrcTs) {
		writeString(Pos.XZT08I_TK_NOT_PRC_TS, xzt08iTkNotPrcTs, Len.XZT08I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT08I-TK-NOT-PRC-TS<br>*/
	public String getXzt08iTkNotPrcTs() {
		return readString(Pos.XZT08I_TK_NOT_PRC_TS, Len.XZT08I_TK_NOT_PRC_TS);
	}

	public void setXzt08iTkNinCltId(String xzt08iTkNinCltId) {
		writeString(Pos.XZT08I_TK_NIN_CLT_ID, xzt08iTkNinCltId, Len.XZT08I_TK_NIN_CLT_ID);
	}

	/**Original name: XZT08I-TK-NIN-CLT-ID<br>*/
	public String getXzt08iTkNinCltId() {
		return readString(Pos.XZT08I_TK_NIN_CLT_ID, Len.XZT08I_TK_NIN_CLT_ID);
	}

	public void setXzt08iTkNinAdrId(String xzt08iTkNinAdrId) {
		writeString(Pos.XZT08I_TK_NIN_ADR_ID, xzt08iTkNinAdrId, Len.XZT08I_TK_NIN_ADR_ID);
	}

	/**Original name: XZT08I-TK-NIN-ADR-ID<br>*/
	public String getXzt08iTkNinAdrId() {
		return readString(Pos.XZT08I_TK_NIN_ADR_ID, Len.XZT08I_TK_NIN_ADR_ID);
	}

	public void setXzt08iTkWfStartedInd(char xzt08iTkWfStartedInd) {
		writeChar(Pos.XZT08I_TK_WF_STARTED_IND, xzt08iTkWfStartedInd);
	}

	/**Original name: XZT08I-TK-WF-STARTED-IND<br>*/
	public char getXzt08iTkWfStartedInd() {
		return readChar(Pos.XZT08I_TK_WF_STARTED_IND);
	}

	public void setXzt08iTkPolBilStaCd(char xzt08iTkPolBilStaCd) {
		writeChar(Pos.XZT08I_TK_POL_BIL_STA_CD, xzt08iTkPolBilStaCd);
	}

	/**Original name: XZT08I-TK-POL-BIL-STA-CD<br>*/
	public char getXzt08iTkPolBilStaCd() {
		return readChar(Pos.XZT08I_TK_POL_BIL_STA_CD);
	}

	public void setXzt08iTkActNotPolCsumFormatted(String xzt08iTkActNotPolCsum) {
		writeString(Pos.XZT08I_TK_ACT_NOT_POL_CSUM, Trunc.toUnsignedNumeric(xzt08iTkActNotPolCsum, Len.XZT08I_TK_ACT_NOT_POL_CSUM),
				Len.XZT08I_TK_ACT_NOT_POL_CSUM);
	}

	/**Original name: XZT08I-TK-ACT-NOT-POL-CSUM<br>*/
	public int getXzt08iTkActNotPolCsum() {
		return readNumDispUnsignedInt(Pos.XZT08I_TK_ACT_NOT_POL_CSUM, Len.XZT08I_TK_ACT_NOT_POL_CSUM);
	}

	public String getXzt08iTkActNotPolCsumFormatted() {
		return readFixedString(Pos.XZT08I_TK_ACT_NOT_POL_CSUM, Len.XZT08I_TK_ACT_NOT_POL_CSUM);
	}

	public void setXzt08iCsrActNbr(String xzt08iCsrActNbr) {
		writeString(Pos.XZT08I_CSR_ACT_NBR, xzt08iCsrActNbr, Len.XZT08I_CSR_ACT_NBR);
	}

	/**Original name: XZT08I-CSR-ACT-NBR<br>*/
	public String getXzt08iCsrActNbr() {
		return readString(Pos.XZT08I_CSR_ACT_NBR, Len.XZT08I_CSR_ACT_NBR);
	}

	public void setXzt08iPolNbr(String xzt08iPolNbr) {
		writeString(Pos.XZT08I_POL_NBR, xzt08iPolNbr, Len.XZT08I_POL_NBR);
	}

	/**Original name: XZT08I-POL-NBR<br>*/
	public String getXzt08iPolNbr() {
		return readString(Pos.XZT08I_POL_NBR, Len.XZT08I_POL_NBR);
	}

	public void setXzt08iPolTypCd(String xzt08iPolTypCd) {
		writeString(Pos.XZT08I_POL_TYP_CD, xzt08iPolTypCd, Len.XZT08I_POL_TYP_CD);
	}

	/**Original name: XZT08I-POL-TYP-CD<br>*/
	public String getXzt08iPolTypCd() {
		return readString(Pos.XZT08I_POL_TYP_CD, Len.XZT08I_POL_TYP_CD);
	}

	public void setXzt08iPolPriRskStAbb(String xzt08iPolPriRskStAbb) {
		writeString(Pos.XZT08I_POL_PRI_RSK_ST_ABB, xzt08iPolPriRskStAbb, Len.XZT08I_POL_PRI_RSK_ST_ABB);
	}

	/**Original name: XZT08I-POL-PRI-RSK-ST-ABB<br>*/
	public String getXzt08iPolPriRskStAbb() {
		return readString(Pos.XZT08I_POL_PRI_RSK_ST_ABB, Len.XZT08I_POL_PRI_RSK_ST_ABB);
	}

	public void setXzt08iNotEffDt(String xzt08iNotEffDt) {
		writeString(Pos.XZT08I_NOT_EFF_DT, xzt08iNotEffDt, Len.XZT08I_NOT_EFF_DT);
	}

	/**Original name: XZT08I-NOT-EFF-DT<br>*/
	public String getXzt08iNotEffDt() {
		return readString(Pos.XZT08I_NOT_EFF_DT, Len.XZT08I_NOT_EFF_DT);
	}

	public void setXzt08iPolEffDt(String xzt08iPolEffDt) {
		writeString(Pos.XZT08I_POL_EFF_DT, xzt08iPolEffDt, Len.XZT08I_POL_EFF_DT);
	}

	/**Original name: XZT08I-POL-EFF-DT<br>*/
	public String getXzt08iPolEffDt() {
		return readString(Pos.XZT08I_POL_EFF_DT, Len.XZT08I_POL_EFF_DT);
	}

	public void setXzt08iPolExpDt(String xzt08iPolExpDt) {
		writeString(Pos.XZT08I_POL_EXP_DT, xzt08iPolExpDt, Len.XZT08I_POL_EXP_DT);
	}

	/**Original name: XZT08I-POL-EXP-DT<br>*/
	public String getXzt08iPolExpDt() {
		return readString(Pos.XZT08I_POL_EXP_DT, Len.XZT08I_POL_EXP_DT);
	}

	public void setXzt08iPolDueAmt(AfDecimal xzt08iPolDueAmt) {
		writeDecimal(Pos.XZT08I_POL_DUE_AMT, xzt08iPolDueAmt.copy());
	}

	/**Original name: XZT08I-POL-DUE-AMT<br>*/
	public AfDecimal getXzt08iPolDueAmt() {
		return readDecimal(Pos.XZT08I_POL_DUE_AMT, Len.Int.XZT08I_POL_DUE_AMT, Len.Fract.XZT08I_POL_DUE_AMT);
	}

	public void setXzt08iUserid(String xzt08iUserid) {
		writeString(Pos.XZT08I_USERID, xzt08iUserid, Len.XZT08I_USERID);
	}

	/**Original name: XZT08I-USERID<br>*/
	public String getXzt08iUserid() {
		return readString(Pos.XZT08I_USERID, Len.XZT08I_USERID);
	}

	public String getXzt08iUseridFormatted() {
		return Functions.padBlanks(getXzt08iUserid(), Len.XZT08I_USERID);
	}

	public void setXzt08oTkNotPrcTs(String xzt08oTkNotPrcTs) {
		writeString(Pos.XZT08O_TK_NOT_PRC_TS, xzt08oTkNotPrcTs, Len.XZT08O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT08O-TK-NOT-PRC-TS<br>*/
	public String getXzt08oTkNotPrcTs() {
		return readString(Pos.XZT08O_TK_NOT_PRC_TS, Len.XZT08O_TK_NOT_PRC_TS);
	}

	public void setXzt08oTkNinCltId(String xzt08oTkNinCltId) {
		writeString(Pos.XZT08O_TK_NIN_CLT_ID, xzt08oTkNinCltId, Len.XZT08O_TK_NIN_CLT_ID);
	}

	/**Original name: XZT08O-TK-NIN-CLT-ID<br>*/
	public String getXzt08oTkNinCltId() {
		return readString(Pos.XZT08O_TK_NIN_CLT_ID, Len.XZT08O_TK_NIN_CLT_ID);
	}

	public void setXzt08oTkNinAdrId(String xzt08oTkNinAdrId) {
		writeString(Pos.XZT08O_TK_NIN_ADR_ID, xzt08oTkNinAdrId, Len.XZT08O_TK_NIN_ADR_ID);
	}

	/**Original name: XZT08O-TK-NIN-ADR-ID<br>*/
	public String getXzt08oTkNinAdrId() {
		return readString(Pos.XZT08O_TK_NIN_ADR_ID, Len.XZT08O_TK_NIN_ADR_ID);
	}

	public void setXzt08oTkWfStartedInd(char xzt08oTkWfStartedInd) {
		writeChar(Pos.XZT08O_TK_WF_STARTED_IND, xzt08oTkWfStartedInd);
	}

	/**Original name: XZT08O-TK-WF-STARTED-IND<br>*/
	public char getXzt08oTkWfStartedInd() {
		return readChar(Pos.XZT08O_TK_WF_STARTED_IND);
	}

	public void setXzt08oTkPolBilStaCd(char xzt08oTkPolBilStaCd) {
		writeChar(Pos.XZT08O_TK_POL_BIL_STA_CD, xzt08oTkPolBilStaCd);
	}

	/**Original name: XZT08O-TK-POL-BIL-STA-CD<br>*/
	public char getXzt08oTkPolBilStaCd() {
		return readChar(Pos.XZT08O_TK_POL_BIL_STA_CD);
	}

	public void setXzt08oTkActNotPolCsumFormatted(String xzt08oTkActNotPolCsum) {
		writeString(Pos.XZT08O_TK_ACT_NOT_POL_CSUM, Trunc.toUnsignedNumeric(xzt08oTkActNotPolCsum, Len.XZT08O_TK_ACT_NOT_POL_CSUM),
				Len.XZT08O_TK_ACT_NOT_POL_CSUM);
	}

	/**Original name: XZT08O-TK-ACT-NOT-POL-CSUM<br>*/
	public int getXzt08oTkActNotPolCsum() {
		return readNumDispUnsignedInt(Pos.XZT08O_TK_ACT_NOT_POL_CSUM, Len.XZT08O_TK_ACT_NOT_POL_CSUM);
	}

	public void setXzt08oCsrActNbr(String xzt08oCsrActNbr) {
		writeString(Pos.XZT08O_CSR_ACT_NBR, xzt08oCsrActNbr, Len.XZT08O_CSR_ACT_NBR);
	}

	/**Original name: XZT08O-CSR-ACT-NBR<br>*/
	public String getXzt08oCsrActNbr() {
		return readString(Pos.XZT08O_CSR_ACT_NBR, Len.XZT08O_CSR_ACT_NBR);
	}

	public void setXzt08oPolNbr(String xzt08oPolNbr) {
		writeString(Pos.XZT08O_POL_NBR, xzt08oPolNbr, Len.XZT08O_POL_NBR);
	}

	/**Original name: XZT08O-POL-NBR<br>*/
	public String getXzt08oPolNbr() {
		return readString(Pos.XZT08O_POL_NBR, Len.XZT08O_POL_NBR);
	}

	public void setXzt08oPolTypCd(String xzt08oPolTypCd) {
		writeString(Pos.XZT08O_POL_TYP_CD, xzt08oPolTypCd, Len.XZT08O_POL_TYP_CD);
	}

	/**Original name: XZT08O-POL-TYP-CD<br>*/
	public String getXzt08oPolTypCd() {
		return readString(Pos.XZT08O_POL_TYP_CD, Len.XZT08O_POL_TYP_CD);
	}

	public void setXzt08oPolTypDes(String xzt08oPolTypDes) {
		writeString(Pos.XZT08O_POL_TYP_DES, xzt08oPolTypDes, Len.XZT08O_POL_TYP_DES);
	}

	/**Original name: XZT08O-POL-TYP-DES<br>*/
	public String getXzt08oPolTypDes() {
		return readString(Pos.XZT08O_POL_TYP_DES, Len.XZT08O_POL_TYP_DES);
	}

	public void setXzt08oPolPriRskStAbb(String xzt08oPolPriRskStAbb) {
		writeString(Pos.XZT08O_POL_PRI_RSK_ST_ABB, xzt08oPolPriRskStAbb, Len.XZT08O_POL_PRI_RSK_ST_ABB);
	}

	/**Original name: XZT08O-POL-PRI-RSK-ST-ABB<br>*/
	public String getXzt08oPolPriRskStAbb() {
		return readString(Pos.XZT08O_POL_PRI_RSK_ST_ABB, Len.XZT08O_POL_PRI_RSK_ST_ABB);
	}

	public void setXzt08oNotEffDt(String xzt08oNotEffDt) {
		writeString(Pos.XZT08O_NOT_EFF_DT, xzt08oNotEffDt, Len.XZT08O_NOT_EFF_DT);
	}

	/**Original name: XZT08O-NOT-EFF-DT<br>*/
	public String getXzt08oNotEffDt() {
		return readString(Pos.XZT08O_NOT_EFF_DT, Len.XZT08O_NOT_EFF_DT);
	}

	public void setXzt08oPolEffDt(String xzt08oPolEffDt) {
		writeString(Pos.XZT08O_POL_EFF_DT, xzt08oPolEffDt, Len.XZT08O_POL_EFF_DT);
	}

	/**Original name: XZT08O-POL-EFF-DT<br>*/
	public String getXzt08oPolEffDt() {
		return readString(Pos.XZT08O_POL_EFF_DT, Len.XZT08O_POL_EFF_DT);
	}

	public void setXzt08oPolExpDt(String xzt08oPolExpDt) {
		writeString(Pos.XZT08O_POL_EXP_DT, xzt08oPolExpDt, Len.XZT08O_POL_EXP_DT);
	}

	/**Original name: XZT08O-POL-EXP-DT<br>*/
	public String getXzt08oPolExpDt() {
		return readString(Pos.XZT08O_POL_EXP_DT, Len.XZT08O_POL_EXP_DT);
	}

	public void setXzt08oPolDueAmt(AfDecimal xzt08oPolDueAmt) {
		writeDecimal(Pos.XZT08O_POL_DUE_AMT, xzt08oPolDueAmt.copy());
	}

	/**Original name: XZT08O-POL-DUE-AMT<br>*/
	public AfDecimal getXzt08oPolDueAmt() {
		return readDecimal(Pos.XZT08O_POL_DUE_AMT, Len.Int.XZT08O_POL_DUE_AMT, Len.Fract.XZT08O_POL_DUE_AMT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT008_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT08I_TECHNICAL_KEY = XZT008_SERVICE_INPUTS;
		public static final int XZT08I_TK_NOT_PRC_TS = XZT08I_TECHNICAL_KEY;
		public static final int XZT08I_TK_NIN_CLT_ID = XZT08I_TK_NOT_PRC_TS + Len.XZT08I_TK_NOT_PRC_TS;
		public static final int XZT08I_TK_NIN_ADR_ID = XZT08I_TK_NIN_CLT_ID + Len.XZT08I_TK_NIN_CLT_ID;
		public static final int XZT08I_TK_WF_STARTED_IND = XZT08I_TK_NIN_ADR_ID + Len.XZT08I_TK_NIN_ADR_ID;
		public static final int XZT08I_TK_POL_BIL_STA_CD = XZT08I_TK_WF_STARTED_IND + Len.XZT08I_TK_WF_STARTED_IND;
		public static final int XZT08I_TK_ACT_NOT_POL_CSUM = XZT08I_TK_POL_BIL_STA_CD + Len.XZT08I_TK_POL_BIL_STA_CD;
		public static final int XZT08I_CSR_ACT_NBR = XZT08I_TK_ACT_NOT_POL_CSUM + Len.XZT08I_TK_ACT_NOT_POL_CSUM;
		public static final int XZT08I_POL_NBR = XZT08I_CSR_ACT_NBR + Len.XZT08I_CSR_ACT_NBR;
		public static final int XZT08I_POLICY_TYPE = XZT08I_POL_NBR + Len.XZT08I_POL_NBR;
		public static final int XZT08I_POL_TYP_CD = XZT08I_POLICY_TYPE;
		public static final int XZT08I_POL_PRI_RSK_ST_ABB = XZT08I_POL_TYP_CD + Len.XZT08I_POL_TYP_CD;
		public static final int XZT08I_NOT_EFF_DT = XZT08I_POL_PRI_RSK_ST_ABB + Len.XZT08I_POL_PRI_RSK_ST_ABB;
		public static final int XZT08I_POL_EFF_DT = XZT08I_NOT_EFF_DT + Len.XZT08I_NOT_EFF_DT;
		public static final int XZT08I_POL_EXP_DT = XZT08I_POL_EFF_DT + Len.XZT08I_POL_EFF_DT;
		public static final int XZT08I_POL_DUE_AMT = XZT08I_POL_EXP_DT + Len.XZT08I_POL_EXP_DT;
		public static final int XZT08I_USERID = XZT08I_POL_DUE_AMT + Len.XZT08I_POL_DUE_AMT;
		public static final int XZT008_SERVICE_OUTPUTS = XZT08I_USERID + Len.XZT08I_USERID;
		public static final int XZT08O_TECHNICAL_KEY = XZT008_SERVICE_OUTPUTS;
		public static final int XZT08O_TK_NOT_PRC_TS = XZT08O_TECHNICAL_KEY;
		public static final int XZT08O_TK_NIN_CLT_ID = XZT08O_TK_NOT_PRC_TS + Len.XZT08O_TK_NOT_PRC_TS;
		public static final int XZT08O_TK_NIN_ADR_ID = XZT08O_TK_NIN_CLT_ID + Len.XZT08O_TK_NIN_CLT_ID;
		public static final int XZT08O_TK_WF_STARTED_IND = XZT08O_TK_NIN_ADR_ID + Len.XZT08O_TK_NIN_ADR_ID;
		public static final int XZT08O_TK_POL_BIL_STA_CD = XZT08O_TK_WF_STARTED_IND + Len.XZT08O_TK_WF_STARTED_IND;
		public static final int XZT08O_TK_ACT_NOT_POL_CSUM = XZT08O_TK_POL_BIL_STA_CD + Len.XZT08O_TK_POL_BIL_STA_CD;
		public static final int XZT08O_CSR_ACT_NBR = XZT08O_TK_ACT_NOT_POL_CSUM + Len.XZT08O_TK_ACT_NOT_POL_CSUM;
		public static final int XZT08O_POL_NBR = XZT08O_CSR_ACT_NBR + Len.XZT08O_CSR_ACT_NBR;
		public static final int XZT08O_POLICY_TYPE = XZT08O_POL_NBR + Len.XZT08O_POL_NBR;
		public static final int XZT08O_POL_TYP_CD = XZT08O_POLICY_TYPE;
		public static final int XZT08O_POL_TYP_DES = XZT08O_POL_TYP_CD + Len.XZT08O_POL_TYP_CD;
		public static final int XZT08O_POL_PRI_RSK_ST_ABB = XZT08O_POL_TYP_DES + Len.XZT08O_POL_TYP_DES;
		public static final int XZT08O_NOT_EFF_DT = XZT08O_POL_PRI_RSK_ST_ABB + Len.XZT08O_POL_PRI_RSK_ST_ABB;
		public static final int XZT08O_POL_EFF_DT = XZT08O_NOT_EFF_DT + Len.XZT08O_NOT_EFF_DT;
		public static final int XZT08O_POL_EXP_DT = XZT08O_POL_EFF_DT + Len.XZT08O_POL_EFF_DT;
		public static final int XZT08O_POL_DUE_AMT = XZT08O_POL_EXP_DT + Len.XZT08O_POL_EXP_DT;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT08I_TK_NOT_PRC_TS = 26;
		public static final int XZT08I_TK_NIN_CLT_ID = 64;
		public static final int XZT08I_TK_NIN_ADR_ID = 64;
		public static final int XZT08I_TK_WF_STARTED_IND = 1;
		public static final int XZT08I_TK_POL_BIL_STA_CD = 1;
		public static final int XZT08I_TK_ACT_NOT_POL_CSUM = 9;
		public static final int XZT08I_CSR_ACT_NBR = 9;
		public static final int XZT08I_POL_NBR = 25;
		public static final int XZT08I_POL_TYP_CD = 3;
		public static final int XZT08I_POL_PRI_RSK_ST_ABB = 2;
		public static final int XZT08I_NOT_EFF_DT = 10;
		public static final int XZT08I_POL_EFF_DT = 10;
		public static final int XZT08I_POL_EXP_DT = 10;
		public static final int XZT08I_POL_DUE_AMT = 10;
		public static final int XZT08I_USERID = 8;
		public static final int XZT08O_TK_NOT_PRC_TS = 26;
		public static final int XZT08O_TK_NIN_CLT_ID = 64;
		public static final int XZT08O_TK_NIN_ADR_ID = 64;
		public static final int XZT08O_TK_WF_STARTED_IND = 1;
		public static final int XZT08O_TK_POL_BIL_STA_CD = 1;
		public static final int XZT08O_TK_ACT_NOT_POL_CSUM = 9;
		public static final int XZT08O_CSR_ACT_NBR = 9;
		public static final int XZT08O_POL_NBR = 25;
		public static final int XZT08O_POL_TYP_CD = 3;
		public static final int XZT08O_POL_TYP_DES = 30;
		public static final int XZT08O_POL_PRI_RSK_ST_ABB = 2;
		public static final int XZT08O_NOT_EFF_DT = 10;
		public static final int XZT08O_POL_EFF_DT = 10;
		public static final int XZT08O_POL_EXP_DT = 10;
		public static final int XZT08I_TECHNICAL_KEY = XZT08I_TK_NOT_PRC_TS + XZT08I_TK_NIN_CLT_ID + XZT08I_TK_NIN_ADR_ID + XZT08I_TK_WF_STARTED_IND
				+ XZT08I_TK_POL_BIL_STA_CD + XZT08I_TK_ACT_NOT_POL_CSUM;
		public static final int XZT08I_POLICY_TYPE = XZT08I_POL_TYP_CD;
		public static final int XZT008_SERVICE_INPUTS = XZT08I_TECHNICAL_KEY + XZT08I_CSR_ACT_NBR + XZT08I_POL_NBR + XZT08I_POLICY_TYPE
				+ XZT08I_POL_PRI_RSK_ST_ABB + XZT08I_NOT_EFF_DT + XZT08I_POL_EFF_DT + XZT08I_POL_EXP_DT + XZT08I_POL_DUE_AMT + XZT08I_USERID;
		public static final int XZT08O_TECHNICAL_KEY = XZT08O_TK_NOT_PRC_TS + XZT08O_TK_NIN_CLT_ID + XZT08O_TK_NIN_ADR_ID + XZT08O_TK_WF_STARTED_IND
				+ XZT08O_TK_POL_BIL_STA_CD + XZT08O_TK_ACT_NOT_POL_CSUM;
		public static final int XZT08O_POLICY_TYPE = XZT08O_POL_TYP_CD + XZT08O_POL_TYP_DES;
		public static final int XZT08O_POL_DUE_AMT = 10;
		public static final int XZT008_SERVICE_OUTPUTS = XZT08O_TECHNICAL_KEY + XZT08O_CSR_ACT_NBR + XZT08O_POL_NBR + XZT08O_POLICY_TYPE
				+ XZT08O_POL_PRI_RSK_ST_ABB + XZT08O_NOT_EFF_DT + XZT08O_POL_EFF_DT + XZT08O_POL_EXP_DT + XZT08O_POL_DUE_AMT;
		public static final int L_SERVICE_CONTRACT_AREA = XZT008_SERVICE_INPUTS + XZT008_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT08I_POL_DUE_AMT = 8;
			public static final int XZT08O_POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZT08I_POL_DUE_AMT = 2;
			public static final int XZT08O_POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
