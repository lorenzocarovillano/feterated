/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW<br>
 * Variable: WS-NON-LOGGABLE-WARN-OR-ERR-SW from program HALOUIDG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsNonLoggableWarnOrErrSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char WARNING = 'W';
	public static final char BUS_ERR = 'E';

	//==== METHODS ====
	public void setWsNonLoggableWarnOrErrSw(char wsNonLoggableWarnOrErrSw) {
		this.value = wsNonLoggableWarnOrErrSw;
	}

	public char getWsNonLoggableWarnOrErrSw() {
		return this.value;
	}

	public void setWarning() {
		value = WARNING;
	}

	public boolean isBusErr() {
		return value == BUS_ERR;
	}

	public void setBusErr() {
		value = BUS_ERR;
	}
}
