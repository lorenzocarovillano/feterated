/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-LASER-198-STATUS-FLAG<br>
 * Variable: SW-LASER-198-STATUS-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwLaser198StatusFlag {

	//==== PROPERTIES ====
	private char value = '0';
	public static final char CLOSED = '0';
	public static final char OPEN = '1';

	//==== METHODS ====
	public void setLaser198StatusFlag(char laser198StatusFlag) {
		this.value = laser198StatusFlag;
	}

	public char getLaser198StatusFlag() {
		return this.value;
	}

	public void setClosed() {
		value = CLOSED;
	}

	public boolean isOpen() {
		return value == OPEN;
	}

	public void setOpen() {
		value = OPEN;
	}
}
