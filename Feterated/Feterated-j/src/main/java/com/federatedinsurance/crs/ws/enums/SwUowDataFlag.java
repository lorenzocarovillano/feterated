/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-UOW-DATA-FLAG<br>
 * Variable: SW-UOW-DATA-FLAG from program TS020000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwUowDataFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char START_OF_UOW_DATA = '0';
	public static final char NO_MORE_UOW_DATA = '1';

	//==== METHODS ====
	public void setUowDataFlag(char uowDataFlag) {
		this.value = uowDataFlag;
	}

	public char getUowDataFlag() {
		return this.value;
	}

	public void setStartOfUowData() {
		value = START_OF_UOW_DATA;
	}

	public boolean isNoMoreUowData() {
		return value == NO_MORE_UOW_DATA;
	}

	public void setNoMoreUowData() {
		value = NO_MORE_UOW_DATA;
	}
}
