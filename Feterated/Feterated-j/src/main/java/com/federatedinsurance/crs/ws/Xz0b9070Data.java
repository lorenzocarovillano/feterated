/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IFrmPrcTyp;
import com.federatedinsurance.crs.copy.DclfrmPrcTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9070<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9070Data implements IFrmPrcTyp {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b9010 constantFields = new ConstantFieldsXz0b9010();
	/**Original name: NI-SPE-PRC-CD<br>
	 * <pre>**  NULL-INDICATORS.</pre>*/
	private short niSpePrcCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-END-OF-CURSOR-FRM-FLAG
	private boolean swEndOfCursorFrmFlag = false;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9070 workingStorageArea = new WorkingStorageAreaXz0b9070();
	//Original name: WS-XZ0A9070-ROW
	private WsXz0a9070Row wsXz0a9070Row = new WsXz0a9070Row();
	//Original name: DCLFRM-PRC-TYP
	private DclfrmPrcTyp dclfrmPrcTyp = new DclfrmPrcTyp();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void setNiSpePrcCd(short niSpePrcCd) {
		this.niSpePrcCd = niSpePrcCd;
	}

	public short getNiSpePrcCd() {
		return this.niSpePrcCd;
	}

	public void setSwEndOfCursorFrmFlag(boolean swEndOfCursorFrmFlag) {
		this.swEndOfCursorFrmFlag = swEndOfCursorFrmFlag;
	}

	public boolean isSwEndOfCursorFrmFlag() {
		return this.swEndOfCursorFrmFlag;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	@Override
	public String getActNotTypCd() {
		return dclfrmPrcTyp.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.dclfrmPrcTyp.setActNotTypCd(actNotTypCd);
	}

	public ConstantFieldsXz0b9010 getConstantFields() {
		return constantFields;
	}

	public DclfrmPrcTyp getDclfrmPrcTyp() {
		return dclfrmPrcTyp;
	}

	@Override
	public short getDtnCd() {
		return dclfrmPrcTyp.getDtnCd();
	}

	@Override
	public void setDtnCd(short dtnCd) {
		this.dclfrmPrcTyp.setDtnCd(dtnCd);
	}

	@Override
	public String getEdlFrmNm() {
		return dclfrmPrcTyp.getEdlFrmNm();
	}

	@Override
	public void setEdlFrmNm(String edlFrmNm) {
		this.dclfrmPrcTyp.setEdlFrmNm(edlFrmNm);
	}

	@Override
	public String getFrmDes() {
		return dclfrmPrcTyp.getFrmDes();
	}

	@Override
	public void setFrmDes(String frmDes) {
		this.dclfrmPrcTyp.setFrmDes(frmDes);
	}

	@Override
	public String getFrmEdtDt() {
		return dclfrmPrcTyp.getFrmEdtDt();
	}

	@Override
	public void setFrmEdtDt(String frmEdtDt) {
		this.dclfrmPrcTyp.setFrmEdtDt(frmEdtDt);
	}

	@Override
	public String getFrmNbr() {
		return dclfrmPrcTyp.getFrmNbr();
	}

	@Override
	public void setFrmNbr(String frmNbr) {
		this.dclfrmPrcTyp.setFrmNbr(frmNbr);
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getSpePrcCd() {
		return dclfrmPrcTyp.getSpePrcCd();
	}

	@Override
	public void setSpePrcCd(String spePrcCd) {
		this.dclfrmPrcTyp.setSpePrcCd(spePrcCd);
	}

	@Override
	public String getSpePrcCdObj() {
		if (getNiSpePrcCd() >= 0) {
			return getSpePrcCd();
		} else {
			return null;
		}
	}

	@Override
	public void setSpePrcCdObj(String spePrcCdObj) {
		if (spePrcCdObj != null) {
			setSpePrcCd(spePrcCdObj);
			setNiSpePrcCd(((short) 0));
		} else {
			setNiSpePrcCd(((short) -1));
		}
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b9070 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9070Row getWsXz0a9070Row() {
		return wsXz0a9070Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
