/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: MUH00360<br>
 * Copybook: MUH00360 from copybook MUH00360<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Muh00360 {

	//==== PROPERTIES ====
	//Original name: ST-CD
	private String stCd = DefaultValues.stringVal(Len.ST_CD);
	//Original name: ST-DES
	private String stDes = DefaultValues.stringVal(Len.ST_DES);

	//==== METHODS ====
	public void setStCd(String stCd) {
		this.stCd = Functions.subString(stCd, Len.ST_CD);
	}

	public String getStCd() {
		return this.stCd;
	}

	public String getStCdFormatted() {
		return Functions.padBlanks(getStCd(), Len.ST_CD);
	}

	public void setStDes(String stDes) {
		this.stDes = Functions.subString(stDes, Len.ST_DES);
	}

	public String getStDes() {
		return this.stDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ST_CD = 3;
		public static final int ST_DES = 40;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
