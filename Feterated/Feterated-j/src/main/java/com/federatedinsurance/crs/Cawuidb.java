/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.ClientAddressVDao;
import com.federatedinsurance.crs.commons.data.dao.ClientTabVDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.ws.CawuidbData;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.DfhcommareaHaluidb;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.enums.UidgIdType;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: CAWUIDB<br>
 * <pre>AUTHOR.       MYND.
 * DATE-WRITTEN. JAN 2002.
 * ****************************************************************
 *                                                                *
 *   PROGRAM TITLE      - CALLS THE GLOBAL ID GENERATOR.  USES    *
 *                        THE RETURNED 32 BYTE RANDOM ID TO CREATE*
 *                        IDS USED BY THE BROWSER CLIENT SYSTEM.  *
 *                                                                *
 *   PLATFORM           - HOST                                    *
 *                                                                *
 *   OPERATING SYSTEM   - MVS                                     *
 *                                                                *
 *   LANGUAGE           - COBOL II                                *
 *                                                                *
 *   PURPOSE            - TO BUILD IDS USED BY THE BROWSER CLIENT *
 *                        SYSTEM.                                 *
 *                                                                *
 *   PROGRAM INITIATION - THIS PROGRAM IS STARTED IN THE FOLLOWING*
 *                        WAYS:                                   *
 *                                                                *
 *                        STARTED BY A LINK FROM ANY BROWSER      *
 *                        CLIENT MODULE REQUIRING ID CREATION.    *
 *                                                                *
 *   DATA ACCESS METHODS - PASSED INFORMATION IN THE COMMAREA OR  *
 *                         THROUGH LINKAGE.                       *
 *                                                                *
 *                                                                *
 * ****************************************************************
 * ****************************************************************
 *                 M A I N T E N A N C E    L O G
 *   SI#       DATE      PRGMR   DESCRIPTION
 *   --------- --------  ------  ----------------------------------
 *   18687     23JAN02   18448   CREATED.
 *   16245     23AUG16   E404JAL PREVENT ID STARTING WITH OVR
 * ****************************************************************</pre>*/
public class Cawuidb extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK*
	 * **CSC *                                     *BUSINESS FRAMEWORK*
	 * **CSC *  DB2 DEFINITIONS                    *BUSINESS FRAMEWORK*
	 * ****************************************************************</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ClientAddressVDao clientAddressVDao = new ClientAddressVDao(dbAccessStatus);
	private ClientTabVDao clientTabVDao = new ClientTabVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private CawuidbData ws = new CawuidbData();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaHaluidb dfhcommarea = new DfhcommareaHaluidb();

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaHaluidb dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		registerArgListeners();
		mainLine();
		returnToCallingProgram();
		deleteArgListeners();
		return 0;
	}

	public static Cawuidb getInstance() {
		return (Programs.getInstance(Cawuidb.class));
	}

	/**Original name: 0000-MAIN-LINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *     CONTROL OF PROCESSING                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainLine() {
		// COB_CODE: PERFORM 0100-INITIALIZATION.
		initialization();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 0000-RETURN-TO-CALLING-PROGRAM
		//           END-IF.
		if (ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 0000-RETURN-TO-CALLING-PROGRAM
			returnToCallingProgram();
		}
		// COB_CODE: PERFORM 0200-BUILD-ID.
		buildId();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 0000-RETURN-TO-CALLING-PROGRAM
		//           END-IF.
		if (ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 0000-RETURN-TO-CALLING-PROGRAM
			returnToCallingProgram();
		}
	}

	/**Original name: 0000-RETURN-TO-CALLING-PROGRAM<br>*/
	private void returnToCallingProgram() {
		// COB_CODE: MOVE LENGTH OF WS-HALOUIDG-LINKAGE
		//                         TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsUbocLinkage().setAppDataBufferLength(((short) CawuidbData.Len.WS_HALOUIDG_LINKAGE));
		// COB_CODE: MOVE WS-HALOUIDG-LINKAGE TO UBOC-APP-DATA-BUFFER.
		ws.getWsUbocLinkage().setAppDataBuffer(ws.getWsHalouidgLinkageFormatted());
		// COB_CODE: MOVE SPACES TO DFHCOMMAREA.
		dfhcommarea.initDfhcommareaSpaces();
		// COB_CODE: MOVE WS-UBOC-LINKAGE TO DFHCOMMAREA.
		dfhcommarea.setDfhcommareaBytes(ws.getWsUbocLinkage().getDfhcommareaBytes());
		// COB_CODE: EXEC CICS
		//              RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 0100-INITIALIZATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *     INITIALIZATION OF WORKFIELDS, AND ANY OTHER INITIAL
	 *     PROCESSING.
	 * *****************************************************************</pre>*/
	private void initialization() {
		// COB_CODE: MOVE DFHCOMMAREA(1:(LENGTH OF WS-UBOC-LINKAGE))
		//             TO WS-UBOC-LINKAGE.
		ws.getWsUbocLinkage().setWsUbocLinkageFormatted(dfhcommarea.getDfhcommareaFormatted().substring((1) - 1, Dfhcommarea.Len.DFHCOMMAREA));
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER
		//                        TO WS-HALOUIDG-LINKAGE.
		ws.setWsHalouidgLinkageFormatted(ws.getWsUbocLinkage().getAppDataBufferFormatted());
		// COB_CODE: INITIALIZE WS-ESTO-INFO.
		initWsEstoInfo();
		// COB_CODE: MOVE SPACES TO UIDG-CA-OUTPUT.
		ws.getHalluidg().initUidgCaOutputSpaces();
		// COB_CODE: IF UBOC-MSG-ID EQUAL SPACES OR LOW-VALUES
		//               GO TO 0100-INITIALIZATION-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsUbocLinkage().getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(ws.getWsUbocLinkage().getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET COMA-MSG-ID-BLANK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE '0100-INITIALIZATION'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-INITIALIZATION");
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-INITIALIZATION-X
			return;
		}
		// COB_CODE: IF UBOC-UOW-NAME EQUAL SPACES OR LOW-VALUES
		//               GO TO 0100-INITIALIZATION-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsUbocLinkage().getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(ws.getWsUbocLinkage().getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET COMA-UOW-NAME-BLANK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE '0100-INITIALIZATION'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-INITIALIZATION");
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-INITIALIZATION-X
			return;
		}
		// COB_CODE: IF UBOC-AUTH-USERID EQUAL SPACES
		//             OR UBOC-AUTH-USERID EQUAL LOW-VALUES
		//               GO TO 0100-INITIALIZATION-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsUbocLinkage().getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(ws.getWsUbocLinkage().getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET COMA-USERID-BLANK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE '0100-INITIALIZATION'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-INITIALIZATION");
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY' TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-INITIALIZATION-X
			return;
		}
	}

	/**Original name: 0200-BUILD-ID_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *    CALL HALOUIDG TO GET A RANDOM ID.
	 *    CONSTRUCT REQUESTED ID TYPE FROM RANDOM ID.
	 *    CHECK ID TO ENSURE IT IS NOT A DUPLICATE.
	 * *****************************************************************</pre>*/
	private void buildId() {
		// COB_CODE: SET ID-BUILDING-INCOMPLETE TO TRUE.
		ws.getWsGeneralWorkfields().getBuildingStatusSw().setIncomplete();
		// COB_CODE: PERFORM 0210-CONSTRUCT-ID
		//              VARYING WS-BUILDING-ATTEMPTS FROM 1 BY 1
		//              UNTIL ID-BUILDING-COMPLETE.
		ws.getWsGeneralWorkfields().setBuildingAttempts(((short) 1));
		while (!ws.getWsGeneralWorkfields().getBuildingStatusSw().isComplete()) {
			constructId();
			ws.getWsGeneralWorkfields().setBuildingAttempts(Trunc.toShort(ws.getWsGeneralWorkfields().getBuildingAttempts() + 1, 1));
		}
	}

	/**Original name: 0210-CONSTRUCT-ID_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *    BUILD A UNIQUE OBJECT IDENTIFIER USING THE ID RETURNED FROM
	 *    HALOUIDG
	 * *****************************************************************
	 * * CALL HALOUIDG TO GET A RANDOM 32-BYTE ID.</pre>*/
	private void constructId() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM  ('HALOUIDG')
		//                COMMAREA (WS-UBOC-LINKAGE)
		//                LENGTH   (LENGTH OF WS-UBOC-LINKAGE)
		//                RESP     (WS-RESPONSE-CODE)
		//                RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("CAWUIDB", execContext).commarea(ws.getWsUbocLinkage()).length(Dfhcommarea.Len.DFHCOMMAREA).link("HALOUIDG", new Halouidg());
		ws.getWsStandardBusobjFlds().setResponseCode(execContext.getResp());
		ws.getWsStandardBusobjFlds().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 0210-CONSTRUCT-ID-X
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsStandardBusobjFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR                       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK OF WS-ESTO-INFO     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE 'HALOUIDG'
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALOUIDG");
			// COB_CODE: MOVE '0210-CONSTRUCT-ID'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0210-CONSTRUCT-ID");
			// COB_CODE: MOVE 'ERROR LINKING TO HALOUIDG FROM CAWUIDB'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR LINKING TO HALOUIDG FROM CAWUIDB");
			// COB_CODE: STRING 'ID TYPE: '
			//               UIDG-ID-TYPE
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "ID TYPE: ",
					ws.getHalluidg().getUidgIdType().getUidgIdTypeFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0210-CONSTRUCT-ID-X
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER TO WS-HALOUIDG-LINKAGE.
		ws.setWsHalouidgLinkageFormatted(ws.getWsUbocLinkage().getAppDataBufferFormatted());
		// COB_CODE: MOVE SPACES TO WS-TEMP-GEN-ID.
		ws.getWsGeneralWorkfields().setTempGenId("");
		//* PERFORM APPROPRIATE MANIPULATION DEPENDING ON THE ID TYPE.
		//* NEW TYPES MUST BE ADDED HERE AS WELL AS IN HALLUIDG VIA A
		//* REQUEST TO THE BUSINESS FRAMEWORKS TEAM.
		// COB_CODE: EVALUATE TRUE
		//               WHEN UIDG-ADDRESS-ID
		//                   PERFORM 0215-OVERLAY-USERID
		//               WHEN UIDG-CLIENT-ID
		//                   PERFORM 0215-OVERLAY-USERID
		//               WHEN OTHER
		//                   GO TO 0210-CONSTRUCT-ID-X
		//           END-EVALUATE.
		switch (ws.getHalluidg().getUidgIdType().getUidgIdType()) {

		case UidgIdType.ADDRESS_ID:// COB_CODE: MOVE 13 TO WS-GENERATED-ID-CHAR
			ws.getWsGeneralWorkfields().setGeneratedIdChar(((short) 13));
			// COB_CODE: MOVE 20 TO WS-GENERATED-ID-LEN
			ws.getWsGeneralWorkfields().setGeneratedIdLen(((short) 20));
			// COB_CODE: PERFORM 0215-OVERLAY-USERID
			overlayUserid();
			break;

		case UidgIdType.CLIENT_ID:// COB_CODE: MOVE 13 TO WS-GENERATED-ID-CHAR
			ws.getWsGeneralWorkfields().setGeneratedIdChar(((short) 13));
			// COB_CODE: MOVE 20 TO WS-GENERATED-ID-LEN
			ws.getWsGeneralWorkfields().setGeneratedIdLen(((short) 20));
			// COB_CODE: PERFORM 0215-OVERLAY-USERID
			overlayUserid();
			break;

		default:// COB_CODE: SET ID-BUILDING-ERRORED TO TRUE
			ws.getWsGeneralWorkfields().getBuildingStatusSw().setErrored();
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
			// COB_CODE: MOVE '0210-CONSTRUCT-ID'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0210-CONSTRUCT-ID");
			// COB_CODE: MOVE 'INVALID ID TYPE'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID ID TYPE");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0210-CONSTRUCT-ID-X
			return;
		}
		// COB_CODE: MOVE SPACES         TO UIDG-GENERATED-ID
		ws.getHalluidg().getUidgErrorIdOutput().setUidgGeneratedId("");
		// COB_CODE: MOVE WS-TEMP-GEN-ID TO UIDG-GENERATED-ID.
		ws.getHalluidg().getUidgErrorIdOutput().setUidgGeneratedId(ws.getWsGeneralWorkfields().getTempGenId());
		// COB_CODE: PERFORM 0220-CHECK-ID.
		checkId();
	}

	/**Original name: 0215-OVERLAY-USERID_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   PER BASE PROCESSING, TRUNCATE THE GENERATED ID THEN OVERLAY
	 *   PART OF THE ID WITH THE USERID.
	 * *****************************************************************</pre>*/
	private void overlayUserid() {
		// COB_CODE: MOVE UIDG-GENERATED-ID(1:WS-GENERATED-ID-LEN)
		//             TO WS-TEMP-GEN-ID.
		ws.getWsGeneralWorkfields().setTempGenId(ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedIdFormatted().substring((1) - 1,
				ws.getWsGeneralWorkfields().getGeneratedIdLen()));
		// COB_CODE: PERFORM VARYING WS-USERID-CHAR
		//                           FROM 1 BY 1
		//                  UNTIL WS-USERID-CHAR > 8
		//                    OR UBOC-AUTH-USERID
		//                                (WS-USERID-CHAR:1)
		//                                       = SPACES
		//                    TO WS-TEMP-GEN-ID(WS-GENERATED-ID-CHAR:1)
		//           END-PERFORM.
		ws.getWsGeneralWorkfields().setUseridChar(((short) 1));
		while (!(ws.getWsGeneralWorkfields().getUseridChar() > 8 || Conditions.eq(ws.getWsUbocLinkage().getCommInfo().getUbocAuthUseridFormatted()
				.substring((ws.getWsGeneralWorkfields().getUseridChar()) - 1, ws.getWsGeneralWorkfields().getUseridChar()), ""))) {
			// COB_CODE: ADD 1 TO WS-GENERATED-ID-CHAR
			ws.getWsGeneralWorkfields().setGeneratedIdChar(Trunc.toShort(1 + ws.getWsGeneralWorkfields().getGeneratedIdChar(), 4));
			// COB_CODE: MOVE UBOC-AUTH-USERID
			//                       (WS-USERID-CHAR:1)
			//                TO WS-TEMP-GEN-ID(WS-GENERATED-ID-CHAR:1)
			ws.getWsGeneralWorkfields()
					.setTempGenId(Functions.setSubstring(ws.getWsGeneralWorkfields().getTempGenId(),
							ws.getWsUbocLinkage().getCommInfo().getUbocAuthUseridFormatted()
									.substring((ws.getWsGeneralWorkfields().getUseridChar()) - 1, ws.getWsGeneralWorkfields().getUseridChar()),
							ws.getWsGeneralWorkfields().getGeneratedIdChar(), 1));
			ws.getWsGeneralWorkfields().setUseridChar(Trunc.toShort(ws.getWsGeneralWorkfields().getUseridChar() + 1, 4));
		}
	}

	/**Original name: 0220-CHECK-ID_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *     CHECK WHETHER THERE IS ALREADY A RECORD WITH CREATED ID.
	 *     VALIDATION FOR EACH TYPE OF ID GENERATED VIA A CALL TO
	 *     THIS MODULE MUST BE ACCOUNTED FOR IN THIS SECTION.
	 * *****************************************************************</pre>*/
	private void checkId() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF UIDG-GENERATED-ID(1:3) = CF-OVR-PREFIX
		//               GO TO 0220-CHECK-ID-X
		//           END-IF.
		if (Conditions.eq(ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedIdFormatted().substring((1) - 1, 3), ws.getCfOvrPrefix())) {
			// COB_CODE: GO TO 0220-CHECK-ID-X
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN UIDG-ADDRESS-ID
		//                   END-EXEC
		//               WHEN UIDG-CLIENT-ID
		//                   END-EXEC
		//               WHEN OTHER
		//                   GO TO 0220-CHECK-ID-X
		//           END-EVALUATE.
		switch (ws.getHalluidg().getUidgIdType().getUidgIdType()) {

		case UidgIdType.ADDRESS_ID:// COB_CODE: MOVE 'CLIENT_ADDRESS_V' TO WS-DATA-TABLE
			ws.getWsGeneralWorkfields().setDataTable("CLIENT_ADDRESS_V");
			// COB_CODE: MOVE UIDG-GENERATED-ID TO CW03H-ADR-ID
			ws.getCw03hClientAddressRow().setAdrId(ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedId());
			// COB_CODE: EXEC SQL
			//               SELECT COUNT(*)
			//                      INTO :WS-COUNT
			//               FROM CLIENT_ADDRESS_V
			//                    WHERE ADR_ID
			//                               = :CW03H-ADR-ID
			//           END-EXEC
			ws.getWsGeneralWorkfields().setCount(Trunc.toInt(
					clientAddressVDao.selectByCw03hAdrId(ws.getCw03hClientAddressRow().getAdrId(), ws.getWsGeneralWorkfields().getCount()), 9));
			break;

		case UidgIdType.CLIENT_ID:// COB_CODE: MOVE 'CLIENT_TAB_V' TO WS-DATA-TABLE
			ws.getWsGeneralWorkfields().setDataTable("CLIENT_TAB_V");
			// COB_CODE: MOVE UIDG-GENERATED-ID TO CW02H-CLIENT-ID
			ws.getCw02hClientTabRow().setClientId(ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedId());
			// COB_CODE: EXEC SQL
			//               SELECT COUNT(*)
			//                      INTO :WS-COUNT
			//               FROM CLIENT_TAB_V
			//                    WHERE CLIENT_ID
			//                               = :CW02H-CLIENT-ID
			//           END-EXEC
			ws.getWsGeneralWorkfields().setCount(Trunc
					.toInt(clientTabVDao.selectByCw02hClientId(ws.getCw02hClientTabRow().getClientId(), ws.getWsGeneralWorkfields().getCount()), 9));
			break;

		default:// COB_CODE: SET ID-BUILDING-ERRORED TO TRUE
			ws.getWsGeneralWorkfields().getBuildingStatusSw().setErrored();
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
			// COB_CODE: MOVE '0220-CHECK-ID'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0220-CHECK-ID");
			// COB_CODE: MOVE 'INVALID ID TYPE'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID ID TYPE");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0220-CHECK-ID-X
			return;
		}
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 0220-CHECK-ID-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET ID-BUILDING-ERRORED TO TRUE
			ws.getWsGeneralWorkfields().getBuildingStatusSw().setErrored();
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE WS-DATA-TABLE TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsGeneralWorkfields().getDataTable());
			// COB_CODE: MOVE '0220-CHECK-ID'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0220-CHECK-ID");
			// COB_CODE: STRING 'SELECT FROM ' WS-DATA-TABLE ' FAILED'
			//               DELIMITED BY SIZE
			//                   INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "SELECT FROM ",
					ws.getWsGeneralWorkfields().getDataTableFormatted(), " FAILED");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'UIDG-GENERATED-ID = ' UIDG-GENERATED-ID ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UIDG-GENERATED-ID = ",
					ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0220-CHECK-ID-X
			return;
		}
		// COB_CODE: IF (WS-COUNT > ZERO)
		//               GO TO 0220-CHECK-ID-X
		//           END-IF.
		if (ws.getWsGeneralWorkfields().getCount() > 0) {
			// COB_CODE: PERFORM 0225-CHECK-DUPE
			checkDupe();
			// COB_CODE: GO TO 0220-CHECK-ID-X
			return;
		}
		// COB_CODE: SET ID-BUILDING-SUCCESSFUL TO TRUE.
		ws.getWsGeneralWorkfields().getBuildingStatusSw().setSuccessful();
	}

	/**Original name: 0225-CHECK-DUPE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   IF A MATCH IS FOUND AND THIS IS THE MAX ATTEMPTS AT GETTING
	 *   AN ID, SET THE DUPE FLAG.  IF THIS IS NOT THE MAX ATTEMPT,
	 *   TRY AGAIN.
	 * *****************************************************************</pre>*/
	private void checkDupe() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF WS-BUILDING-ATTEMPTS >= WS-MAX-ATTEMPTS
		//               GO TO 0225-CHECK-DUPE-X
		//           END-IF.
		if (ws.getWsGeneralWorkfields().getBuildingAttempts() >= ws.getWsGeneralWorkfields().getMaxAttempts()) {
			// COB_CODE: SET ID-BUILDING-DUPED TO TRUE
			ws.getWsGeneralWorkfields().getBuildingStatusSw().setDuped();
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-DUPL-KEY-GENERATED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspDuplKeyGenerated();
			// COB_CODE: MOVE '0225-CHECK-DUPE'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0225-CHECK-DUPE");
			// COB_CODE: STRING 'DUPL. ID ' UIDG-GENERATED-ID
			//               ' FOR TABLE ' WS-DATA-TABLE
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "DUPL. ID ",
					ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedIdFormatted(), " FOR TABLE ",
					ws.getWsGeneralWorkfields().getDataTableFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: MOVE WS-COUNT TO WS-DATA-DISP
			ws.getWsGeneralWorkfields().setDataDisp(ws.getWsGeneralWorkfields().getCount());
			// COB_CODE: STRING 'WS-COUNT = ' WS-DATA-DISP ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "WS-COUNT = ",
					ws.getWsGeneralWorkfields().getDataDispAsString(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0225-CHECK-DUPE-X
			return;
		}
		// COB_CODE: MOVE SPACES TO UIDG-CA-OUTPUT.
		ws.getHalluidg().initUidgCaOutputSpaces();
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning()
				&& ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsStandardBusobjFlds().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsStandardBusobjFlds().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsStandardBusobjFlds().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsStandardBusobjFlds().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsStandardBusobjFlds().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(ws.getWsUbocLinkage().getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(ws.getWsUbocLinkage().getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsStandardBusobjFlds().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(ws.getWsUbocLinkage().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(ws.getWsUbocLinkage().getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("CAWUIDB", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsStandardBusobjFlds().setResponseCode(execContext.getResp());
		ws.getWsStandardBusobjFlds().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsStandardBusobjFlds().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsStandardBusobjFlds().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsStandardBusobjFlds().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			ws.getWsUbocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			ws.getWsUbocLinkage().setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			ws.getWsUbocLinkage().setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initWsEstoInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}

	public void deleteArgListeners() {
		execContext.getCommAreaLenNotifier().deleteListener(dfhcommarea.getCaLnkDataListener());
	}

	public void registerArgListeners() {
		execContext.getCommAreaLenNotifier().addListener(dfhcommarea.getCaLnkDataListener());
		execContext.getCommAreaLenNotifier().notifyListeners();
	}
}
