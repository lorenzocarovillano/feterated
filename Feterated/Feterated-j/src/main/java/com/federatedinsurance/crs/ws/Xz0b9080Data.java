/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclrecTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xzc080ci;
import com.federatedinsurance.crs.copy.Xzc080co;
import com.federatedinsurance.crs.copy.Xzh00016;
import com.federatedinsurance.crs.ws.enums.TtThirdParties;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9080<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9080Data {

	//==== PROPERTIES ====
	public static final int TT_THIRD_PARTIES_MAXOCCURS = 1000;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b9080 constantFields = new ConstantFieldsXz0b9080();
	//Original name: EA-02-NOTHING-FOUND-MSG
	private Ea02NothingFoundMsgXz0b9080 ea02NothingFoundMsg = new Ea02NothingFoundMsgXz0b9080();
	//Original name: ES-01-FATAL-ERROR-MSG
	private Es01FatalErrorMsg es01FatalErrorMsg = new Es01FatalErrorMsg();
	//Original name: SUBSCRIPTS
	private SubscriptsXz0b9080 subscripts = new SubscriptsXz0b9080();
	//Original name: SW-END-OF-CURSOR-FLAG
	private boolean swEndOfCursorFlag = false;
	//Original name: SW-TTY-FOUND-FLAG
	private boolean swTtyFoundFlag = false;
	//Original name: TT-THIRD-PARTIES
	private LazyArrayCopy<TtThirdParties> ttThirdParties = new LazyArrayCopy<>(new TtThirdParties(), 1, TT_THIRD_PARTIES_MAXOCCURS);
	//Original name: IX-TT
	private int ixTt = 1;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9080 workingStorageArea = new WorkingStorageAreaXz0b9080();
	//Original name: WS-XZ0A9080-ROW
	private WsXz0a9080Row wsXz0a9080Row = new WsXz0a9080Row();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: XZH00016
	private Xzh00016 xzh00016 = new Xzh00016();
	//Original name: DCLREC-TYP
	private DclrecTyp dclrecTyp = new DclrecTyp();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: XZC080CI
	private Xzc080ci xzc080ci = new Xzc080ci();
	//Original name: XZC080CO
	private Xzc080co xzc080co = new Xzc080co();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public void setSwEndOfCursorFlag(boolean swEndOfCursorFlag) {
		this.swEndOfCursorFlag = swEndOfCursorFlag;
	}

	public boolean isSwEndOfCursorFlag() {
		return this.swEndOfCursorFlag;
	}

	public void setSwTtyFoundFlag(boolean swTtyFoundFlag) {
		this.swTtyFoundFlag = swTtyFoundFlag;
	}

	public boolean isSwTtyFoundFlag() {
		return this.swTtyFoundFlag;
	}

	public void initTableOfThirdPartiesHighValues() {
		getTtThirdPartiesObj().fill(new TtThirdParties().initTtThirdPartiesHighValues());
	}

	public void setIxTt(int ixTt) {
		this.ixTt = ixTt;
	}

	public int getIxTt() {
		return this.ixTt;
	}

	/**Original name: SERVICE-AREA-POLICY-INFO-IN<br>
	 * <pre> CONTRACT COPYBOOK FOR POLICY INFORMATION SERVICE INPUT</pre>*/
	public byte[] getServiceAreaPolicyInfoInBytes() {
		byte[] buffer = new byte[Len.SERVICE_AREA_POLICY_INFO_IN];
		return getServiceAreaPolicyInfoInBytes(buffer, 1);
	}

	public byte[] getServiceAreaPolicyInfoInBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc080ci.getServiceInputsBytes(buffer, position);
		return buffer;
	}

	public void setServiceAreaPolicyInfoOutBytes(byte[] buffer) {
		setServiceAreaPolicyInfoOutBytes(buffer, 1);
	}

	public void setServiceAreaPolicyInfoOutBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc080co.setServiceOutputsBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public ConstantFieldsXz0b9080 getConstantFields() {
		return constantFields;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclrecTyp getDclrecTyp() {
		return dclrecTyp;
	}

	public Ea02NothingFoundMsgXz0b9080 getEa02NothingFoundMsg() {
		return ea02NothingFoundMsg;
	}

	public Es01FatalErrorMsg getEs01FatalErrorMsg() {
		return es01FatalErrorMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public SubscriptsXz0b9080 getSubscripts() {
		return subscripts;
	}

	public TtThirdParties getTtThirdParties(int idx) {
		return ttThirdParties.get(idx - 1);
	}

	public LazyArrayCopy<TtThirdParties> getTtThirdPartiesObj() {
		return ttThirdParties;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b9080 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9080Row getWsXz0a9080Row() {
		return wsXz0a9080Row;
	}

	public Xzc080ci getXzc080ci() {
		return xzc080ci;
	}

	public Xzc080co getXzc080co() {
		return xzc080co;
	}

	public Xzh00016 getXzh00016() {
		return xzh00016;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int SERVICE_AREA_POLICY_INFO_IN = Xzc080ci.Len.SERVICE_INPUTS;
		public static final int SERVICE_AREA_POLICY_INFO_OUT = Xzc080co.Len.SERVICE_OUTPUTS;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
