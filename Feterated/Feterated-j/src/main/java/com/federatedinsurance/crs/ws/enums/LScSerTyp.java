/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SC-SER-TYP<br>
 * Variable: L-SC-SER-TYP from copybook TT008001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LScSerTyp {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.SER_TYP);
	public static final String PPL = "PPL";
	public static final String DPT = "DPT";
	public static final String SPV = "SPV";
	public static final String ACT = "ACT";
	public static final String MAL = "MAL";
	public static final String EMP = "EMP";
	public static final String USR = "USR";
	public static final String TER = "TER";

	//==== METHODS ====
	public void setSerTyp(String serTyp) {
		this.value = Functions.subString(serTyp, Len.SER_TYP);
	}

	public String getSerTyp() {
		return this.value;
	}

	public void setEmp() {
		value = EMP;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SER_TYP = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
