/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-SWITCHES-TSQ-SW<br>
 * Variable: WS-SWITCHES-TSQ-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsSwitchesTsqSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char START_OF_SWITCHES_TSQ = 'S';
	public static final char END_OF_SWITCHES_TSQ = 'E';

	//==== METHODS ====
	public void setSwitchesTsqSw(char switchesTsqSw) {
		this.value = switchesTsqSw;
	}

	public char getSwitchesTsqSw() {
		return this.value;
	}

	public void setStartOfSwitchesTsq() {
		value = START_OF_SWITCHES_TSQ;
	}

	public boolean isEndOfSwitchesTsq() {
		return value == END_OF_SWITCHES_TSQ;
	}

	public void setEndOfSwitchesTsq() {
		value = END_OF_SWITCHES_TSQ;
	}
}
