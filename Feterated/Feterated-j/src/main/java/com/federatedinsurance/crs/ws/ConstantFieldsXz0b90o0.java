/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0B90O0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0b90o0 {

	//==== PROPERTIES ====
	//Original name: CF-YES
	private char yes = 'Y';
	//Original name: CF-NO
	private char no = 'N';
	//Original name: CF-TTY
	private String tty = "TTY";
	//Original name: CF-XML-REQ-STA
	private String xmlReqSta = "50";

	//==== METHODS ====
	public char getYes() {
		return this.yes;
	}

	public char getNo() {
		return this.no;
	}

	public String getTty() {
		return this.tty;
	}

	public String getXmlReqSta() {
		return this.xmlReqSta;
	}
}
