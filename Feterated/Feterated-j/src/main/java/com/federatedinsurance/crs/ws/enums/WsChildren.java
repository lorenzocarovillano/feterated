/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-CHILDREN<br>
 * Variable: WS-CHILDREN from program XZ0D0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsChildren {

	//==== PROPERTIES ====
	private String value = "";
	private static final String[] DIRECT_S3_CHILDREN = new String[] { "ACT_NOT_POL", "ACT_NOT_FRM", "ACT_NOT_REC" };
	public static final String FOREIGN_S3_CHILDREN = "DUMMY-NAME-3";

	//==== METHODS ====
	public void setChildren(String children) {
		this.value = Functions.subString(children, Len.CHILDREN);
	}

	public String getChildren() {
		return this.value;
	}

	public boolean isDirectS3Children() {
		return ArrayUtils.contains(DIRECT_S3_CHILDREN, value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CHILDREN = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
