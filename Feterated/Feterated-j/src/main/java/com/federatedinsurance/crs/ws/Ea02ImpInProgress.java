/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-IMP-IN-PROGRESS<br>
 * Variable: EA-02-IMP-IN-PROGRESS from program XZ0P90L0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02ImpInProgress {

	//==== PROPERTIES ====
	/**Original name: FILLER-EA-02-IMP-IN-PROGRESS<br>
	 * <pre>* THIS ERROR MESSAGE CAN ONLY BE USED WHEN THERE IS AN
	 * * IMPENDING CANCELLATION IN PROGRESS. DO NOT CHANGE THIS ERROR
	 * * MESSAGE WITHOUT CONTACTING THE BUSINESSWORKS TEAM - THEY ARE
	 * * KEYING OFF IT TO DETERMINE THAT THIS SPECIFIC ERROR OCCURRED.</pre>*/
	private String flr1 = "An impending";
	//Original name: FILLER-EA-02-IMP-IN-PROGRESS-1
	private String flr2 = "cancellation";
	//Original name: FILLER-EA-02-IMP-IN-PROGRESS-2
	private String flr3 = "notification is";
	//Original name: FILLER-EA-02-IMP-IN-PROGRESS-3
	private String flr4 = " in progress";
	//Original name: FILLER-EA-02-IMP-IN-PROGRESS-4
	private String flr5 = "for account";
	//Original name: EA-02-CSR-ACT-NBR
	private String ea02CsrActNbr = DefaultValues.stringVal(Len.EA02_CSR_ACT_NBR);
	//Original name: FILLER-EA-02-IMP-IN-PROGRESS-5
	private char flr6 = '.';

	//==== METHODS ====
	public String getEa02ImpInProgressFormatted() {
		return MarshalByteExt.bufferToStr(getEa02ImpInProgressBytes());
	}

	public byte[] getEa02ImpInProgressBytes() {
		byte[] buffer = new byte[Len.EA02_IMP_IN_PROGRESS];
		return getEa02ImpInProgressBytes(buffer, 1);
	}

	public byte[] getEa02ImpInProgressBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea02CsrActNbr, Len.EA02_CSR_ACT_NBR);
		position += Len.EA02_CSR_ACT_NBR;
		MarshalByte.writeChar(buffer, position, flr6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa02CsrActNbr(String ea02CsrActNbr) {
		this.ea02CsrActNbr = Functions.subString(ea02CsrActNbr, Len.EA02_CSR_ACT_NBR);
	}

	public String getEa02CsrActNbr() {
		return this.ea02CsrActNbr;
	}

	public char getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA02_CSR_ACT_NBR = 9;
		public static final int FLR1 = 13;
		public static final int FLR3 = 15;
		public static final int FLR5 = 12;
		public static final int FLR6 = 1;
		public static final int EA02_IMP_IN_PROGRESS = EA02_CSR_ACT_NBR + 3 * FLR1 + FLR3 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
