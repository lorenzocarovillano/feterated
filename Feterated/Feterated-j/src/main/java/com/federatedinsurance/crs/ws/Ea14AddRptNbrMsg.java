/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-14-ADD-RPT-NBR-MSG<br>
 * Variable: EA-14-ADD-RPT-NBR-MSG from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea14AddRptNbrMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-14-ADD-RPT-NBR-MSG
	private String flr1 = "";
	//Original name: FILLER-EA-14-ADD-RPT-NBR-MSG-1
	private String flr2 = "- PLEASE";
	//Original name: FILLER-EA-14-ADD-RPT-NBR-MSG-2
	private String flr3 = "USE THE PRINT";
	//Original name: FILLER-EA-14-ADD-RPT-NBR-MSG-3
	private String flr4 = "ROUTINE UPDATE";
	//Original name: FILLER-EA-14-ADD-RPT-NBR-MSG-4
	private String flr5 = "UTILITY TO ADD";
	//Original name: FILLER-EA-14-ADD-RPT-NBR-MSG-5
	private String flr6 = "THE REPORT";
	//Original name: FILLER-EA-14-ADD-RPT-NBR-MSG-6
	private String flr7 = "NUMBER.";

	//==== METHODS ====
	public String getEa14AddRptNbrMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa14AddRptNbrMsgBytes());
	}

	public byte[] getEa14AddRptNbrMsgBytes() {
		byte[] buffer = new byte[Len.EA14_ADD_RPT_NBR_MSG];
		return getEa14AddRptNbrMsgBytes(buffer, 1);
	}

	public byte[] getEa14AddRptNbrMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 10;
		public static final int FLR2 = 9;
		public static final int FLR3 = 14;
		public static final int FLR4 = 15;
		public static final int FLR6 = 11;
		public static final int FLR7 = 7;
		public static final int EA14_ADD_RPT_NBR_MSG = FLR1 + FLR2 + FLR3 + 2 * FLR4 + FLR6 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
