/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalUowFunSecV;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WS-TSQ2-DATA-AREA<br>
 * Variable: WS-TSQ2-DATA-AREA from program HALOUSDH<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsTsq2DataArea implements IHalUowFunSecV {

	//==== PROPERTIES ====
	//Original name: WS-TSQ2-UOW-NM
	private String uowNm = DefaultValues.stringVal(Len.UOW_NM);
	//Original name: WS-TSQ2-SEC-GRP-NM
	private String secGrpNm = DefaultValues.stringVal(Len.SEC_GRP_NM);
	//Original name: WS-TSQ2-CONTEXT
	private String context = DefaultValues.stringVal(Len.CONTEXT);
	//Original name: WS-TSQ2-SEC-ASC-TYP
	private String secAscTyp = DefaultValues.stringVal(Len.SEC_ASC_TYP);
	//Original name: WS-TSQ2-FUN-SEQ-NBR
	private short funSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-TSQ2-AUT-ACTION-CD
	private String autActionCd = DefaultValues.stringVal(Len.AUT_ACTION_CD);
	//Original name: WS-TSQ2-AUT-FUN-NM
	private String autFunNm = DefaultValues.stringVal(Len.AUT_FUN_NM);
	//Original name: WS-TSQ2-LNK-TO-MDU-NM
	private String lnkToMduNm = DefaultValues.stringVal(Len.LNK_TO_MDU_NM);
	//Original name: WS-TSQ2-APP-NM
	private String appNm = DefaultValues.stringVal(Len.APP_NM);

	//==== METHODS ====
	public void setWsTsq2DataAreaBytes(byte[] buffer) {
		setWsTsq2DataAreaBytes(buffer, 1);
	}

	public byte[] getWsTsq2DataAreaBytes() {
		byte[] buffer = new byte[Len.WS_TSQ2_DATA_AREA];
		return getWsTsq2DataAreaBytes(buffer, 1);
	}

	public void setWsTsq2DataAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uowNm = MarshalByte.readString(buffer, position, Len.UOW_NM);
		position += Len.UOW_NM;
		secGrpNm = MarshalByte.readString(buffer, position, Len.SEC_GRP_NM);
		position += Len.SEC_GRP_NM;
		context = MarshalByte.readString(buffer, position, Len.CONTEXT);
		position += Len.CONTEXT;
		secAscTyp = MarshalByte.readString(buffer, position, Len.SEC_ASC_TYP);
		position += Len.SEC_ASC_TYP;
		funSeqNbr = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		autActionCd = MarshalByte.readString(buffer, position, Len.AUT_ACTION_CD);
		position += Len.AUT_ACTION_CD;
		autFunNm = MarshalByte.readString(buffer, position, Len.AUT_FUN_NM);
		position += Len.AUT_FUN_NM;
		lnkToMduNm = MarshalByte.readString(buffer, position, Len.LNK_TO_MDU_NM);
		position += Len.LNK_TO_MDU_NM;
		appNm = MarshalByte.readString(buffer, position, Len.APP_NM);
	}

	public byte[] getWsTsq2DataAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, uowNm, Len.UOW_NM);
		position += Len.UOW_NM;
		MarshalByte.writeString(buffer, position, secGrpNm, Len.SEC_GRP_NM);
		position += Len.SEC_GRP_NM;
		MarshalByte.writeString(buffer, position, context, Len.CONTEXT);
		position += Len.CONTEXT;
		MarshalByte.writeString(buffer, position, secAscTyp, Len.SEC_ASC_TYP);
		position += Len.SEC_ASC_TYP;
		MarshalByte.writeBinaryShort(buffer, position, funSeqNbr);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, autActionCd, Len.AUT_ACTION_CD);
		position += Len.AUT_ACTION_CD;
		MarshalByte.writeString(buffer, position, autFunNm, Len.AUT_FUN_NM);
		position += Len.AUT_FUN_NM;
		MarshalByte.writeString(buffer, position, lnkToMduNm, Len.LNK_TO_MDU_NM);
		position += Len.LNK_TO_MDU_NM;
		MarshalByte.writeString(buffer, position, appNm, Len.APP_NM);
		return buffer;
	}

	public void initWsTsq2DataAreaSpaces() {
		uowNm = "";
		secGrpNm = "";
		context = "";
		secAscTyp = "";
		funSeqNbr = Types.INVALID_BINARY_SHORT_VAL;
		autActionCd = "";
		autFunNm = "";
		lnkToMduNm = "";
		appNm = "";
	}

	public void setUowNm(String uowNm) {
		this.uowNm = Functions.subString(uowNm, Len.UOW_NM);
	}

	public String getUowNm() {
		return this.uowNm;
	}

	public String getUowNmFormatted() {
		return Functions.padBlanks(getUowNm(), Len.UOW_NM);
	}

	public void setSecGrpNm(String secGrpNm) {
		this.secGrpNm = Functions.subString(secGrpNm, Len.SEC_GRP_NM);
	}

	public String getSecGrpNm() {
		return this.secGrpNm;
	}

	public String getSecGrpNmFormatted() {
		return Functions.padBlanks(getSecGrpNm(), Len.SEC_GRP_NM);
	}

	public void setContext(String context) {
		this.context = Functions.subString(context, Len.CONTEXT);
	}

	public String getContext() {
		return this.context;
	}

	public String getContextFormatted() {
		return Functions.padBlanks(getContext(), Len.CONTEXT);
	}

	public void setSecAscTyp(String secAscTyp) {
		this.secAscTyp = Functions.subString(secAscTyp, Len.SEC_ASC_TYP);
	}

	public String getSecAscTyp() {
		return this.secAscTyp;
	}

	public String getSecAscTypFormatted() {
		return Functions.padBlanks(getSecAscTyp(), Len.SEC_ASC_TYP);
	}

	@Override
	public void setFunSeqNbr(short funSeqNbr) {
		this.funSeqNbr = funSeqNbr;
	}

	@Override
	public short getFunSeqNbr() {
		return this.funSeqNbr;
	}

	@Override
	public void setAutActionCd(String autActionCd) {
		this.autActionCd = Functions.subString(autActionCd, Len.AUT_ACTION_CD);
	}

	@Override
	public String getAutActionCd() {
		return this.autActionCd;
	}

	@Override
	public void setAutFunNm(String autFunNm) {
		this.autFunNm = Functions.subString(autFunNm, Len.AUT_FUN_NM);
	}

	@Override
	public String getAutFunNm() {
		return this.autFunNm;
	}

	@Override
	public void setLnkToMduNm(String lnkToMduNm) {
		this.lnkToMduNm = Functions.subString(lnkToMduNm, Len.LNK_TO_MDU_NM);
	}

	@Override
	public String getLnkToMduNm() {
		return this.lnkToMduNm;
	}

	@Override
	public void setAppNm(String appNm) {
		this.appNm = Functions.subString(appNm, Len.APP_NM);
	}

	@Override
	public String getAppNm() {
		return this.appNm;
	}

	@Override
	public String getHufshSecAscTyp() {
		return getSecAscTyp();
	}

	@Override
	public void setHufshSecAscTyp(String hufshSecAscTyp) {
		this.setSecAscTyp(hufshSecAscTyp);
	}

	@Override
	public String getHufshSecGrpNm() {
		throw new FieldNotMappedException("hufshSecGrpNm");
	}

	@Override
	public void setHufshSecGrpNm(String hufshSecGrpNm) {
		throw new FieldNotMappedException("hufshSecGrpNm");
	}

	@Override
	public String getHufshUowNm() {
		throw new FieldNotMappedException("hufshUowNm");
	}

	@Override
	public void setHufshUowNm(String hufshUowNm) {
		throw new FieldNotMappedException("hufshUowNm");
	}

	@Override
	public String getWsAuthUseridForCursor() {
		throw new FieldNotMappedException("wsAuthUseridForCursor");
	}

	@Override
	public void setWsAuthUseridForCursor(String wsAuthUseridForCursor) {
		throw new FieldNotMappedException("wsAuthUseridForCursor");
	}

	@Override
	public String getWsContextFirst10ForCsr() {
		throw new FieldNotMappedException("wsContextFirst10ForCsr");
	}

	@Override
	public void setWsContextFirst10ForCsr(String wsContextFirst10ForCsr) {
		throw new FieldNotMappedException("wsContextFirst10ForCsr");
	}

	@Override
	public String getWsFullContext() {
		throw new FieldNotMappedException("wsFullContext");
	}

	@Override
	public void setWsFullContext(String wsFullContext) {
		throw new FieldNotMappedException("wsFullContext");
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NM = 32;
		public static final int SEC_GRP_NM = 8;
		public static final int SEC_ASC_TYP = 8;
		public static final int AUT_ACTION_CD = 32;
		public static final int AUT_FUN_NM = 32;
		public static final int LNK_TO_MDU_NM = 8;
		public static final int APP_NM = 8;
		public static final int CONTEXT = 20;
		public static final int FUN_SEQ_NBR = 2;
		public static final int WS_TSQ2_DATA_AREA = UOW_NM + SEC_GRP_NM + CONTEXT + SEC_ASC_TYP + FUN_SEQ_NBR + AUT_ACTION_CD + AUT_FUN_NM
				+ LNK_TO_MDU_NM + APP_NM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
