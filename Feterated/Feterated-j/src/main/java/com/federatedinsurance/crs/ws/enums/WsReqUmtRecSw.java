/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-REQ-UMT-REC-SW<br>
 * Variable: WS-REQ-UMT-REC-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsReqUmtRecSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FOUND = 'Y';
	public static final char NOT_FOUND = 'N';

	//==== METHODS ====
	public void setReqUmtRecSw(char reqUmtRecSw) {
		this.value = reqUmtRecSw;
	}

	public char getReqUmtRecSw() {
		return this.value;
	}

	public void setReqUmtRecFound() {
		value = FOUND;
	}

	public void setReqUmtRecNotFound() {
		value = NOT_FOUND;
	}
}
