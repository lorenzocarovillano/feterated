/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.enums.Xwc01oCerAcyInd;
import com.federatedinsurance.crs.ws.occurs.Xwc010NonLoggableErrors;
import com.federatedinsurance.crs.ws.occurs.Xwc010Warnings;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: GET-CER-ACY-STA<br>
 * Variable: GET-CER-ACY-STA from program XZC05090<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class GetCerAcySta extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int XWC010_NON_LOGGABLE_ERRORS_MAXOCCURS = 10;
	public static final int XWC010_WARNINGS_MAXOCCURS = 10;
	//Original name: XWC01I-ACT-NBR
	private String iActNbr = DefaultValues.stringVal(Len.I_ACT_NBR);
	//Original name: XWC01I-NOT-DT
	private String iNotDt = DefaultValues.stringVal(Len.I_NOT_DT);
	//Original name: XWC01I-CER-NBR
	private String iCerNbr = DefaultValues.stringVal(Len.I_CER_NBR);
	//Original name: FILLER-XWC010-SERVICE-INPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: XWC01O-CER-ACY-IND
	private Xwc01oCerAcyInd oCerAcyInd = new Xwc01oCerAcyInd();
	//Original name: FILLER-XWC010-SERVICE-OUTPUTS
	private String flr2 = DefaultValues.stringVal(Len.FLR2);
	//Original name: XWC010-ERROR-RETURN-CODE
	private DsdErrorReturnCode xwc010ErrorReturnCode = new DsdErrorReturnCode();
	//Original name: XWC010-FATAL-ERROR-MESSAGE
	private String xwc010FatalErrorMessage = DefaultValues.stringVal(Len.XWC010_FATAL_ERROR_MESSAGE);
	//Original name: XWC010-NON-LOGGABLE-ERROR-CNT
	private String xwc010NonLoggableErrorCnt = DefaultValues.stringVal(Len.XWC010_NON_LOGGABLE_ERROR_CNT);
	//Original name: XWC010-NON-LOGGABLE-ERRORS
	private Xwc010NonLoggableErrors[] xwc010NonLoggableErrors = new Xwc010NonLoggableErrors[XWC010_NON_LOGGABLE_ERRORS_MAXOCCURS];
	//Original name: XWC010-WARNING-CNT
	private String xwc010WarningCnt = DefaultValues.stringVal(Len.XWC010_WARNING_CNT);
	//Original name: XWC010-WARNINGS
	private Xwc010Warnings[] xwc010Warnings = new Xwc010Warnings[XWC010_WARNINGS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public GetCerAcySta() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.GET_CER_ACY_STA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setGetCerAcyStaBytes(buf);
	}

	public void init() {
		for (int xwc010NonLoggableErrorsIdx = 1; xwc010NonLoggableErrorsIdx <= XWC010_NON_LOGGABLE_ERRORS_MAXOCCURS; xwc010NonLoggableErrorsIdx++) {
			xwc010NonLoggableErrors[xwc010NonLoggableErrorsIdx - 1] = new Xwc010NonLoggableErrors();
		}
		for (int xwc010WarningsIdx = 1; xwc010WarningsIdx <= XWC010_WARNINGS_MAXOCCURS; xwc010WarningsIdx++) {
			xwc010Warnings[xwc010WarningsIdx - 1] = new Xwc010Warnings();
		}
	}

	public void setGetCerAcyStaBytes(byte[] buffer) {
		setGetCerAcyStaBytes(buffer, 1);
	}

	public byte[] getGetCerAcyStaBytes() {
		byte[] buffer = new byte[Len.GET_CER_ACY_STA];
		return getGetCerAcyStaBytes(buffer, 1);
	}

	public void setGetCerAcyStaBytes(byte[] buffer, int offset) {
		int position = offset;
		setXwc010ServiceInputsBytes(buffer, position);
		position += Len.XWC010_SERVICE_INPUTS;
		setXwc010ServiceOutputsBytes(buffer, position);
		position += Len.XWC010_SERVICE_OUTPUTS;
		setXwc010ErrorHandlingParmsBytes(buffer, position);
	}

	public byte[] getGetCerAcyStaBytes(byte[] buffer, int offset) {
		int position = offset;
		getXwc010ServiceInputsBytes(buffer, position);
		position += Len.XWC010_SERVICE_INPUTS;
		getXwc010ServiceOutputsBytes(buffer, position);
		position += Len.XWC010_SERVICE_OUTPUTS;
		getXwc010ErrorHandlingParmsBytes(buffer, position);
		return buffer;
	}

	public void setXwc010ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		iActNbr = MarshalByte.readString(buffer, position, Len.I_ACT_NBR);
		position += Len.I_ACT_NBR;
		iNotDt = MarshalByte.readString(buffer, position, Len.I_NOT_DT);
		position += Len.I_NOT_DT;
		iCerNbr = MarshalByte.readString(buffer, position, Len.I_CER_NBR);
		position += Len.I_CER_NBR;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXwc010ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, iActNbr, Len.I_ACT_NBR);
		position += Len.I_ACT_NBR;
		MarshalByte.writeString(buffer, position, iNotDt, Len.I_NOT_DT);
		position += Len.I_NOT_DT;
		MarshalByte.writeString(buffer, position, iCerNbr, Len.I_CER_NBR);
		position += Len.I_CER_NBR;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setiActNbr(String iActNbr) {
		this.iActNbr = Functions.subString(iActNbr, Len.I_ACT_NBR);
	}

	public String getiActNbr() {
		return this.iActNbr;
	}

	public void setiNotDt(String iNotDt) {
		this.iNotDt = Functions.subString(iNotDt, Len.I_NOT_DT);
	}

	public String getiNotDt() {
		return this.iNotDt;
	}

	public void setiCerNbr(String iCerNbr) {
		this.iCerNbr = Functions.subString(iCerNbr, Len.I_CER_NBR);
	}

	public String getiCerNbr() {
		return this.iCerNbr;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setXwc010ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		oCerAcyInd.setoCerAcyInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR2);
	}

	public byte[] getXwc010ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, oCerAcyInd.getoCerAcyInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR2);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setXwc010ErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		xwc010ErrorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		xwc010FatalErrorMessage = MarshalByte.readString(buffer, position, Len.XWC010_FATAL_ERROR_MESSAGE);
		position += Len.XWC010_FATAL_ERROR_MESSAGE;
		xwc010NonLoggableErrorCnt = MarshalByte.readFixedString(buffer, position, Len.XWC010_NON_LOGGABLE_ERROR_CNT);
		position += Len.XWC010_NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= XWC010_NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				xwc010NonLoggableErrors[idx - 1].setXwc010NonLoggableErrorsBytes(buffer, position);
				position += Xwc010NonLoggableErrors.Len.XWC010_NON_LOGGABLE_ERRORS;
			} else {
				xwc010NonLoggableErrors[idx - 1].initXwc010NonLoggableErrorsSpaces();
				position += Xwc010NonLoggableErrors.Len.XWC010_NON_LOGGABLE_ERRORS;
			}
		}
		xwc010WarningCnt = MarshalByte.readFixedString(buffer, position, Len.XWC010_WARNING_CNT);
		position += Len.XWC010_WARNING_CNT;
		for (int idx = 1; idx <= XWC010_WARNINGS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				xwc010Warnings[idx - 1].setXwc010WarningsBytes(buffer, position);
				position += Xwc010Warnings.Len.XWC010_WARNINGS;
			} else {
				xwc010Warnings[idx - 1].initXwc010WarningsSpaces();
				position += Xwc010Warnings.Len.XWC010_WARNINGS;
			}
		}
	}

	public byte[] getXwc010ErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xwc010ErrorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, xwc010FatalErrorMessage, Len.XWC010_FATAL_ERROR_MESSAGE);
		position += Len.XWC010_FATAL_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, xwc010NonLoggableErrorCnt, Len.XWC010_NON_LOGGABLE_ERROR_CNT);
		position += Len.XWC010_NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= XWC010_NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			xwc010NonLoggableErrors[idx - 1].getXwc010NonLoggableErrorsBytes(buffer, position);
			position += Xwc010NonLoggableErrors.Len.XWC010_NON_LOGGABLE_ERRORS;
		}
		MarshalByte.writeString(buffer, position, xwc010WarningCnt, Len.XWC010_WARNING_CNT);
		position += Len.XWC010_WARNING_CNT;
		for (int idx = 1; idx <= XWC010_WARNINGS_MAXOCCURS; idx++) {
			xwc010Warnings[idx - 1].getXwc010WarningsBytes(buffer, position);
			position += Xwc010Warnings.Len.XWC010_WARNINGS;
		}
		return buffer;
	}

	public void setXwc010FatalErrorMessage(String xwc010FatalErrorMessage) {
		this.xwc010FatalErrorMessage = Functions.subString(xwc010FatalErrorMessage, Len.XWC010_FATAL_ERROR_MESSAGE);
	}

	public String getXwc010FatalErrorMessage() {
		return this.xwc010FatalErrorMessage;
	}

	public Xwc01oCerAcyInd getoCerAcyInd() {
		return oCerAcyInd;
	}

	@Override
	public byte[] serialize() {
		return getGetCerAcyStaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int I_ACT_NBR = 9;
		public static final int I_NOT_DT = 10;
		public static final int I_CER_NBR = 25;
		public static final int FLR1 = 100;
		public static final int XWC010_SERVICE_INPUTS = I_ACT_NBR + I_NOT_DT + I_CER_NBR + FLR1;
		public static final int FLR2 = 400;
		public static final int XWC010_SERVICE_OUTPUTS = Xwc01oCerAcyInd.Len.O_CER_ACY_IND + FLR2;
		public static final int XWC010_FATAL_ERROR_MESSAGE = 250;
		public static final int XWC010_NON_LOGGABLE_ERROR_CNT = 4;
		public static final int XWC010_WARNING_CNT = 4;
		public static final int XWC010_ERROR_HANDLING_PARMS = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + XWC010_FATAL_ERROR_MESSAGE
				+ XWC010_NON_LOGGABLE_ERROR_CNT
				+ GetCerAcySta.XWC010_NON_LOGGABLE_ERRORS_MAXOCCURS * Xwc010NonLoggableErrors.Len.XWC010_NON_LOGGABLE_ERRORS + XWC010_WARNING_CNT
				+ GetCerAcySta.XWC010_WARNINGS_MAXOCCURS * Xwc010Warnings.Len.XWC010_WARNINGS;
		public static final int GET_CER_ACY_STA = XWC010_SERVICE_INPUTS + XWC010_SERVICE_OUTPUTS + XWC010_ERROR_HANDLING_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
