/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: CALLABLE-INPUTS<br>
 * Variable: CALLABLE-INPUTS from program FNC02090<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CallableInputs extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public CallableInputs() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.CALLABLE_INPUTS;
	}

	public void setCallableInputsBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.CALLABLE_INPUTS, Pos.CALLABLE_INPUTS);
	}

	public byte[] getCallableInputsBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.CALLABLE_INPUTS, Pos.CALLABLE_INPUTS);
		return buffer;
	}

	public void initCallableInputsLowValues() {
		fill(Pos.CALLABLE_INPUTS, Len.CALLABLE_INPUTS, Types.LOW_CHAR_VAL);
	}

	public void setMu003iTkUserid(String mu003iTkUserid) {
		writeString(Pos.MU003I_TK_USERID, mu003iTkUserid, Len.MU003I_TK_USERID);
	}

	/**Original name: MU003I-TK-USERID<br>*/
	public String getMu003iTkUserid() {
		return readString(Pos.MU003I_TK_USERID, Len.MU003I_TK_USERID);
	}

	public void setMu003iTkPassword(String mu003iTkPassword) {
		writeString(Pos.MU003I_TK_PASSWORD, mu003iTkPassword, Len.MU003I_TK_PASSWORD);
	}

	/**Original name: MU003I-TK-PASSWORD<br>*/
	public String getMu003iTkPassword() {
		return readString(Pos.MU003I_TK_PASSWORD, Len.MU003I_TK_PASSWORD);
	}

	public void setMu003iTkUri(String mu003iTkUri) {
		writeString(Pos.MU003I_TK_URI, mu003iTkUri, Len.MU003I_TK_URI);
	}

	/**Original name: MU003I-TK-URI<br>*/
	public String getMu003iTkUri() {
		return readString(Pos.MU003I_TK_URI, Len.MU003I_TK_URI);
	}

	public void setMu003iTkFedTarSys(String mu003iTkFedTarSys) {
		writeString(Pos.MU003I_TK_FED_TAR_SYS, mu003iTkFedTarSys, Len.MU003I_TK_FED_TAR_SYS);
	}

	/**Original name: MU003I-TK-FED-TAR-SYS<br>*/
	public String getMu003iTkFedTarSys() {
		return readString(Pos.MU003I_TK_FED_TAR_SYS, Len.MU003I_TK_FED_TAR_SYS);
	}

	public void setMu003iMatchType(String mu003iMatchType) {
		writeString(Pos.MU003I_MATCH_TYPE, mu003iMatchType, Len.MU003I_MATCH_TYPE);
	}

	/**Original name: MU003I-MATCH-TYPE<br>*/
	public String getMu003iMatchType() {
		return readString(Pos.MU003I_MATCH_TYPE, Len.MU003I_MATCH_TYPE);
	}

	public void setMu003iDataValue(String mu003iDataValue) {
		writeString(Pos.MU003I_DATA_VALUE, mu003iDataValue, Len.MU003I_DATA_VALUE);
	}

	/**Original name: MU003I-DATA-VALUE<br>*/
	public String getMu003iDataValue() {
		return readString(Pos.MU003I_DATA_VALUE, Len.MU003I_DATA_VALUE);
	}

	public String getCallableOutputsFormatted() {
		return readFixedString(Pos.CALLABLE_OUTPUTS, Len.CALLABLE_OUTPUTS);
	}

	public void initCallableOutputsLowValues() {
		fill(Pos.CALLABLE_OUTPUTS, Len.CALLABLE_OUTPUTS, Types.LOW_CHAR_VAL);
	}

	/**Original name: MU003O-RETURN-CODE<br>*/
	public String getMu003oReturnCode() {
		return readString(Pos.MU003O_RETURN_CODE, Len.MU003O_RETURN_CODE);
	}

	/**Original name: MU003O-MC-95<br>*/
	public String getMu003oMc95() {
		return readString(Pos.MU003O_MC95, Len.MU003O_MC95);
	}

	/**Original name: MU003O-MC-90<br>*/
	public String getMu003oMc90() {
		return readString(Pos.MU003O_MC90, Len.MU003O_MC90);
	}

	/**Original name: MU003O-MC-85<br>*/
	public String getMu003oMc85() {
		return readString(Pos.MU003O_MC85, Len.MU003O_MC85);
	}

	/**Original name: MU003O-MC-80<br>*/
	public String getMu003oMc80() {
		return readString(Pos.MU003O_MC80, Len.MU003O_MC80);
	}

	/**Original name: MU003O-MC-75<br>*/
	public String getMu003oMc75() {
		return readString(Pos.MU003O_MC75, Len.MU003O_MC75);
	}

	/**Original name: MU003O-MC-70<br>*/
	public String getMu003oMc70() {
		return readString(Pos.MU003O_MC70, Len.MU003O_MC70);
	}

	/**Original name: MU003O-MC-65<br>*/
	public String getMu003oMc65() {
		return readString(Pos.MU003O_MC65, Len.MU003O_MC65);
	}

	/**Original name: MU003O-MC-60<br>*/
	public String getMu003oMc60() {
		return readString(Pos.MU003O_MC60, Len.MU003O_MC60);
	}

	/**Original name: MU003O-MC-55<br>*/
	public String getMu003oMc55() {
		return readString(Pos.MU003O_MC55, Len.MU003O_MC55);
	}

	/**Original name: MU003O-MC-50<br>*/
	public String getMu003oMc50() {
		return readString(Pos.MU003O_MC50, Len.MU003O_MC50);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int CALLABLE_INPUTS = 1;
		public static final int MU003I_TECHNICAL_KEYS = CALLABLE_INPUTS;
		public static final int MU003I_TK_USERID = MU003I_TECHNICAL_KEYS;
		public static final int MU003I_TK_PASSWORD = MU003I_TK_USERID + Len.MU003I_TK_USERID;
		public static final int MU003I_TK_URI = MU003I_TK_PASSWORD + Len.MU003I_TK_PASSWORD;
		public static final int MU003I_TK_FED_TAR_SYS = MU003I_TK_URI + Len.MU003I_TK_URI;
		public static final int MU003I_GET_CITY_MATCH_CODE = MU003I_TK_FED_TAR_SYS + Len.MU003I_TK_FED_TAR_SYS;
		public static final int MU003I_MATCH_TYPE = MU003I_GET_CITY_MATCH_CODE;
		public static final int MU003I_DATA_VALUE = MU003I_MATCH_TYPE + Len.MU003I_MATCH_TYPE;
		public static final int CALLABLE_OUTPUTS = 1;
		public static final int MU003O_GET_CITY_MATCH_CODE = CALLABLE_OUTPUTS;
		public static final int MU003O_RETURN_CODE = MU003O_GET_CITY_MATCH_CODE;
		public static final int MU003O_RETURN_MESSAGE = MU003O_RETURN_CODE + Len.MU003O_RETURN_CODE;
		public static final int MU003O_MATCH_CODES = MU003O_RETURN_MESSAGE + Len.MU003O_RETURN_MESSAGE;
		public static final int MU003O_MC95 = MU003O_MATCH_CODES;
		public static final int MU003O_MC90 = MU003O_MC95 + Len.MU003O_MC95;
		public static final int MU003O_MC85 = MU003O_MC90 + Len.MU003O_MC90;
		public static final int MU003O_MC80 = MU003O_MC85 + Len.MU003O_MC85;
		public static final int MU003O_MC75 = MU003O_MC80 + Len.MU003O_MC80;
		public static final int MU003O_MC70 = MU003O_MC75 + Len.MU003O_MC75;
		public static final int MU003O_MC65 = MU003O_MC70 + Len.MU003O_MC70;
		public static final int MU003O_MC60 = MU003O_MC65 + Len.MU003O_MC65;
		public static final int MU003O_MC55 = MU003O_MC60 + Len.MU003O_MC60;
		public static final int MU003O_MC50 = MU003O_MC55 + Len.MU003O_MC55;
		public static final int FLR1 = MU003O_MC50 + Len.MU003O_MC50;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int MU003I_TK_USERID = 8;
		public static final int MU003I_TK_PASSWORD = 8;
		public static final int MU003I_TK_URI = 256;
		public static final int MU003I_TK_FED_TAR_SYS = 5;
		public static final int MU003I_MATCH_TYPE = 30;
		public static final int MU003O_RETURN_CODE = 30;
		public static final int MU003O_RETURN_MESSAGE = 30;
		public static final int MU003O_MC95 = 35;
		public static final int MU003O_MC90 = 35;
		public static final int MU003O_MC85 = 35;
		public static final int MU003O_MC80 = 35;
		public static final int MU003O_MC75 = 35;
		public static final int MU003O_MC70 = 35;
		public static final int MU003O_MC65 = 35;
		public static final int MU003O_MC60 = 35;
		public static final int MU003O_MC55 = 35;
		public static final int MU003O_MC50 = 35;
		public static final int MU003I_TECHNICAL_KEYS = MU003I_TK_USERID + MU003I_TK_PASSWORD + MU003I_TK_URI + MU003I_TK_FED_TAR_SYS;
		public static final int MU003I_DATA_VALUE = 256;
		public static final int MU003I_GET_CITY_MATCH_CODE = MU003I_MATCH_TYPE + MU003I_DATA_VALUE;
		public static final int CALLABLE_INPUTS = MU003I_TECHNICAL_KEYS + MU003I_GET_CITY_MATCH_CODE;
		public static final int MU003O_MATCH_CODES = MU003O_MC95 + MU003O_MC90 + MU003O_MC85 + MU003O_MC80 + MU003O_MC75 + MU003O_MC70 + MU003O_MC65
				+ MU003O_MC60 + MU003O_MC55 + MU003O_MC50;
		public static final int MU003O_GET_CITY_MATCH_CODE = MU003O_RETURN_CODE + MU003O_RETURN_MESSAGE + MU003O_MATCH_CODES;
		public static final int FLR1 = 153;
		public static final int CALLABLE_OUTPUTS = MU003O_GET_CITY_MATCH_CODE + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
