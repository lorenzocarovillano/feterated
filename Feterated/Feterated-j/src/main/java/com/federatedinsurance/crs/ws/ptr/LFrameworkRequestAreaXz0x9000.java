/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X9000<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x9000 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x9000() {
	}

	public LFrameworkRequestAreaXz0x9000(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzy900qCsrActNbr(String xzy900qCsrActNbr) {
		writeString(Pos.XZY900Q_CSR_ACT_NBR, xzy900qCsrActNbr, Len.XZY900Q_CSR_ACT_NBR);
	}

	/**Original name: XZY900Q-CSR-ACT-NBR<br>*/
	public String getXzy900qCsrActNbr() {
		return readString(Pos.XZY900Q_CSR_ACT_NBR, Len.XZY900Q_CSR_ACT_NBR);
	}

	public void setXzy900qNotPrcTs(String xzy900qNotPrcTs) {
		writeString(Pos.XZY900Q_NOT_PRC_TS, xzy900qNotPrcTs, Len.XZY900Q_NOT_PRC_TS);
	}

	/**Original name: XZY900Q-NOT-PRC-TS<br>*/
	public String getXzy900qNotPrcTs() {
		return readString(Pos.XZY900Q_NOT_PRC_TS, Len.XZY900Q_NOT_PRC_TS);
	}

	public void setXzy900qActNotTypCd(String xzy900qActNotTypCd) {
		writeString(Pos.XZY900Q_ACT_NOT_TYP_CD, xzy900qActNotTypCd, Len.XZY900Q_ACT_NOT_TYP_CD);
	}

	/**Original name: XZY900Q-ACT-NOT-TYP-CD<br>*/
	public String getXzy900qActNotTypCd() {
		return readString(Pos.XZY900Q_ACT_NOT_TYP_CD, Len.XZY900Q_ACT_NOT_TYP_CD);
	}

	public void setXzy900qUserid(String xzy900qUserid) {
		writeString(Pos.XZY900Q_USERID, xzy900qUserid, Len.XZY900Q_USERID);
	}

	/**Original name: XZY900Q-USERID<br>*/
	public String getXzy900qUserid() {
		return readString(Pos.XZY900Q_USERID, Len.XZY900Q_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_PRP_CNC_WRD = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZY900Q_PRP_CNC_WRD_ROW = L_FW_REQ_PRP_CNC_WRD;
		public static final int XZY900Q_CSR_ACT_NBR = XZY900Q_PRP_CNC_WRD_ROW;
		public static final int XZY900Q_NOT_PRC_TS = XZY900Q_CSR_ACT_NBR + Len.XZY900Q_CSR_ACT_NBR;
		public static final int XZY900Q_ACT_NOT_TYP_CD = XZY900Q_NOT_PRC_TS + Len.XZY900Q_NOT_PRC_TS;
		public static final int XZY900Q_USERID = XZY900Q_ACT_NOT_TYP_CD + Len.XZY900Q_ACT_NOT_TYP_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY900Q_CSR_ACT_NBR = 9;
		public static final int XZY900Q_NOT_PRC_TS = 26;
		public static final int XZY900Q_ACT_NOT_TYP_CD = 5;
		public static final int XZY900Q_USERID = 8;
		public static final int XZY900Q_PRP_CNC_WRD_ROW = XZY900Q_CSR_ACT_NBR + XZY900Q_NOT_PRC_TS + XZY900Q_ACT_NOT_TYP_CD + XZY900Q_USERID;
		public static final int L_FW_REQ_PRP_CNC_WRD = XZY900Q_PRP_CNC_WRD_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_PRP_CNC_WRD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
