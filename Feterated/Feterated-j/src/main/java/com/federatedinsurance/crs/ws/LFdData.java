/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-FD-DATA<br>
 * Variable: L-FD-DATA from program TS514099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LFdData extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: FILLER-L-FD-DATA
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: L-FD-DDNAME
	private String lFdDdname = DefaultValues.stringVal(Len.L_FD_DDNAME);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FD_DATA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setlFdDataBytes(buf);
	}

	public void setlFdDataBytes(byte[] buffer) {
		setlFdDataBytes(buffer, 1);
	}

	public byte[] getlFdDataBytes() {
		byte[] buffer = new byte[Len.L_FD_DATA];
		return getlFdDataBytes(buffer, 1);
	}

	public void setlFdDataBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		lFdDdname = MarshalByte.readString(buffer, position, Len.L_FD_DDNAME);
	}

	public byte[] getlFdDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, lFdDdname, Len.L_FD_DDNAME);
		return buffer;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setlFdDdname(String lFdDdname) {
		this.lFdDdname = Functions.subString(lFdDdname, Len.L_FD_DDNAME);
	}

	public String getlFdDdname() {
		return this.lFdDdname;
	}

	@Override
	public byte[] serialize() {
		return getlFdDataBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 40;
		public static final int L_FD_DDNAME = 8;
		public static final int L_FD_DATA = L_FD_DDNAME + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
