/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [CLIENT_TAB_V]
 * 
 */
public interface IClientTabV extends BaseSqlTo {

	/**
	 * Host Variable CW02H-CLIENT-ID
	 * 
	 */
	String getClientId();

	void setClientId(String clientId);

	/**
	 * Host Variable CW02H-HISTORY-VLD-NBR
	 * 
	 */
	short getHistoryVldNbr();

	void setHistoryVldNbr(short historyVldNbr);

	/**
	 * Host Variable CW02H-CICL-EFF-DT
	 * 
	 */
	String getCiclEffDt();

	void setCiclEffDt(String ciclEffDt);

	/**
	 * Host Variable CW02H-CICL-PRI-SUB-CD
	 * 
	 */
	String getCiclPriSubCd();

	void setCiclPriSubCd(String ciclPriSubCd);

	/**
	 * Host Variable CW02H-CICL-FST-NM
	 * 
	 */
	String getCiclFstNm();

	void setCiclFstNm(String ciclFstNm);

	/**
	 * Host Variable CW02H-CICL-LST-NM
	 * 
	 */
	String getCiclLstNm();

	void setCiclLstNm(String ciclLstNm);

	/**
	 * Host Variable CW02H-CICL-MDL-NM
	 * 
	 */
	String getCiclMdlNm();

	void setCiclMdlNm(String ciclMdlNm);

	/**
	 * Host Variable CW02H-NM-PFX
	 * 
	 */
	String getNmPfx();

	void setNmPfx(String nmPfx);

	/**
	 * Host Variable CW02H-NM-SFX
	 * 
	 */
	String getNmSfx();

	void setNmSfx(String nmSfx);

	/**
	 * Host Variable CW02H-PRIMARY-PRO-DSN-CD
	 * 
	 */
	String getPrimaryProDsnCd();

	void setPrimaryProDsnCd(String primaryProDsnCd);

	/**
	 * Nullable property for CW02H-PRIMARY-PRO-DSN-CD
	 * 
	 */
	String getPrimaryProDsnCdObj();

	void setPrimaryProDsnCdObj(String primaryProDsnCdObj);

	/**
	 * Host Variable CW02H-LEG-ENT-CD
	 * 
	 */
	String getLegEntCd();

	void setLegEntCd(String legEntCd);

	/**
	 * Host Variable CW02H-CICL-SDX-CD
	 * 
	 */
	String getCiclSdxCd();

	void setCiclSdxCd(String ciclSdxCd);

	/**
	 * Host Variable CW02H-CICL-OGN-INCEPT-DT
	 * 
	 */
	String getCiclOgnInceptDt();

	void setCiclOgnInceptDt(String ciclOgnInceptDt);

	/**
	 * Nullable property for CW02H-CICL-OGN-INCEPT-DT
	 * 
	 */
	String getCiclOgnInceptDtObj();

	void setCiclOgnInceptDtObj(String ciclOgnInceptDtObj);

	/**
	 * Host Variable CW02H-CICL-ADD-NM-IND
	 * 
	 */
	char getCiclAddNmInd();

	void setCiclAddNmInd(char ciclAddNmInd);

	/**
	 * Nullable property for CW02H-CICL-ADD-NM-IND
	 * 
	 */
	Character getCiclAddNmIndObj();

	void setCiclAddNmIndObj(Character ciclAddNmIndObj);

	/**
	 * Host Variable CW02H-CICL-DOB-DT
	 * 
	 */
	String getCiclDobDt();

	void setCiclDobDt(String ciclDobDt);

	/**
	 * Host Variable CW02H-CICL-BIR-ST-CD
	 * 
	 */
	String getCiclBirStCd();

	void setCiclBirStCd(String ciclBirStCd);

	/**
	 * Host Variable CW02H-GENDER-CD
	 * 
	 */
	char getGenderCd();

	void setGenderCd(char genderCd);

	/**
	 * Host Variable CW02H-PRI-LGG-CD
	 * 
	 */
	String getPriLggCd();

	void setPriLggCd(String priLggCd);

	/**
	 * Host Variable CW02H-USER-ID
	 * 
	 */
	String getUserId();

	void setUserId(String userId);

	/**
	 * Host Variable CW02H-STATUS-CD
	 * 
	 */
	char getStatusCd();

	void setStatusCd(char statusCd);

	/**
	 * Host Variable CW02H-TERMINAL-ID
	 * 
	 */
	String getTerminalId();

	void setTerminalId(String terminalId);

	/**
	 * Host Variable CW02H-CICL-EXP-DT
	 * 
	 */
	String getCiclExpDt();

	void setCiclExpDt(String ciclExpDt);

	/**
	 * Host Variable CW02H-CICL-EFF-ACY-TS
	 * 
	 */
	String getCiclEffAcyTs();

	void setCiclEffAcyTs(String ciclEffAcyTs);

	/**
	 * Host Variable CW02H-CICL-EXP-ACY-TS
	 * 
	 */
	String getCiclExpAcyTs();

	void setCiclExpAcyTs(String ciclExpAcyTs);

	/**
	 * Host Variable CW02H-STATUTORY-TLE-CD
	 * 
	 */
	String getStatutoryTleCd();

	void setStatutoryTleCd(String statutoryTleCd);

	/**
	 * Host Variable CW02H-CICL-LNG-NM
	 * 
	 */
	String getCiclLngNm();

	void setCiclLngNm(String ciclLngNm);

	/**
	 * Nullable property for CW02H-CICL-LNG-NM
	 * 
	 */
	String getCiclLngNmObj();

	void setCiclLngNmObj(String ciclLngNmObj);
};
