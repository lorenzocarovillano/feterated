/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-64-USR-PIPE-NOT-OPEN-MSG<br>
 * Variable: EA-64-USR-PIPE-NOT-OPEN-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea64UsrPipeNotOpenMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-64-USR-PIPE-NOT-OPEN-MSG
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-64-USR-PIPE-NOT-OPEN-MSG-1
	private String flr2 = "PIPE IS NOT";
	//Original name: FILLER-EA-64-USR-PIPE-NOT-OPEN-MSG-2
	private String flr3 = "OPEN.";

	//==== METHODS ====
	public String getEa64UsrPipeNotOpenMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa64UsrPipeNotOpenMsgBytes());
	}

	public byte[] getEa64UsrPipeNotOpenMsgBytes() {
		byte[] buffer = new byte[Len.EA64_USR_PIPE_NOT_OPEN_MSG];
		return getEa64UsrPipeNotOpenMsgBytes(buffer, 1);
	}

	public byte[] getEa64UsrPipeNotOpenMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 12;
		public static final int FLR3 = 5;
		public static final int EA64_USR_PIPE_NOT_OPEN_MSG = FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
