/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-NO-CER-HLD-NOT-IND-MSG<br>
 * Variable: EA-01-NO-CER-HLD-NOT-IND-MSG from program XZ0P90C0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01NoCerHldNotIndMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-NO-CER-HLD-NOT-IND-MSG
	private String flr1 = "No certholder";
	//Original name: FILLER-EA-01-NO-CER-HLD-NOT-IND-MSG-1
	private String flr2 = "notification";
	//Original name: FILLER-EA-01-NO-CER-HLD-NOT-IND-MSG-2
	private String flr3 = "indicator value";
	//Original name: FILLER-EA-01-NO-CER-HLD-NOT-IND-MSG-3
	private String flr4 = " for this";
	//Original name: FILLER-EA-01-NO-CER-HLD-NOT-IND-MSG-4
	private String flr5 = "Account =";
	//Original name: EA-01-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-01-NO-CER-HLD-NOT-IND-MSG-5
	private String flr6 = "; Notification";
	//Original name: FILLER-EA-01-NO-CER-HLD-NOT-IND-MSG-6
	private String flr7 = "TimeStamp =";
	//Original name: EA-01-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-01-NO-CER-HLD-NOT-IND-MSG-7
	private char flr8 = '.';

	//==== METHODS ====
	public String getEa01NoCerHldNotIndMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01NoCerHldNotIndMsgBytes());
	}

	public byte[] getEa01NoCerHldNotIndMsgBytes() {
		byte[] buffer = new byte[Len.EA01_NO_CER_HLD_NOT_IND_MSG];
		return getEa01NoCerHldNotIndMsgBytes(buffer, 1);
	}

	public byte[] getEa01NoCerHldNotIndMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr8);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 14;
		public static final int FLR2 = 13;
		public static final int FLR3 = 15;
		public static final int FLR4 = 10;
		public static final int FLR7 = 12;
		public static final int FLR8 = 1;
		public static final int EA01_NO_CER_HLD_NOT_IND_MSG = CSR_ACT_NBR + NOT_PRC_TS + FLR1 + FLR2 + 2 * FLR3 + 2 * FLR4 + FLR7 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
