/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.WsBillingRegion;
import com.federatedinsurance.crs.ws.enums.WsMsgStaCd;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P90E0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p90e0 {

	//==== PROPERTIES ====
	//Original name: WS-MAX-POLICIES
	private short maxPolicies = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-EIBRESP-CD
	private short eibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd = DefaultValues.SHORT_VAL;
	/**Original name: WS-BILLING-REGION<br>
	 * <pre>* !WARNING! DEV AND BETA BILLING REGIONS MAY CHANGE!
	 * * VERIFY THE FOLLOWING BEFORE TESTING!</pre>*/
	private WsBillingRegion billingRegion = new WsBillingRegion();
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-HOLD
	private char hold = DefaultValues.CHAR_VAL;
	//Original name: WS-MAX-ROWS
	private String maxRows = DefaultValues.stringVal(Len.MAX_ROWS);
	//Original name: WS-REC-SEQ-NBR
	private String recSeqNbr = DefaultValues.stringVal(Len.REC_SEQ_NBR);
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0P90E0";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-ATTACH-FORMS
	private String busObjNmAttachForms = "XZ_ATTACH_FORMS";
	//Original name: WS-OPERATIONS-CALLED
	private WsOperationsCalledXz0p90e0 operationsCalled = new WsOperationsCalledXz0p90e0();
	//Original name: WS-MSG-STA-CD
	private WsMsgStaCd msgStaCd = new WsMsgStaCd();

	//==== METHODS ====
	public void setMaxPolicies(short maxPolicies) {
		this.maxPolicies = maxPolicies;
	}

	public short getMaxPolicies() {
		return this.maxPolicies;
	}

	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd = eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public String getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public String getEibrespCdAsString() {
		return getEibrespCdFormatted();
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd = eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public String getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public String getEibresp2CdAsString() {
		return getEibresp2CdFormatted();
	}

	public void setHold(char hold) {
		this.hold = hold;
	}

	public char getHold() {
		return this.hold;
	}

	public void setMaxRows(long maxRows) {
		this.maxRows = PicFormatter.display("Z(3)9").format(maxRows).toString();
	}

	public long getMaxRows() {
		return PicParser.display("Z(3)9").parseLong(this.maxRows);
	}

	public String getMaxRowsFormatted() {
		return this.maxRows;
	}

	public String getMaxRowsAsString() {
		return getMaxRowsFormatted();
	}

	public void setRecSeqNbr(long recSeqNbr) {
		this.recSeqNbr = PicFormatter.display("Z(4)9").format(recSeqNbr).toString();
	}

	public long getRecSeqNbr() {
		return PicParser.display("Z(4)9").parseLong(this.recSeqNbr);
	}

	public String getRecSeqNbrFormatted() {
		return this.recSeqNbr;
	}

	public String getRecSeqNbrAsString() {
		return getRecSeqNbrFormatted();
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNmAttachForms() {
		return this.busObjNmAttachForms;
	}

	public WsBillingRegion getBillingRegion() {
		return billingRegion;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	public WsMsgStaCd getMsgStaCd() {
		return msgStaCd;
	}

	public WsOperationsCalledXz0p90e0 getOperationsCalled() {
		return operationsCalled;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_ROWS = 4;
		public static final int REC_SEQ_NBR = 5;
		public static final int PROGRAM_NAME = 8;
		public static final int EIBRESP_CD = 4;
		public static final int EIBRESP2_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
