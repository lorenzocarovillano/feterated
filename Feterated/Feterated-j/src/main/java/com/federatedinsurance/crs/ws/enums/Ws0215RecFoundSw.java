/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS0215-REC-FOUND-SW<br>
 * Variable: WS0215-REC-FOUND-SW from program HALRURQA<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ws0215RecFoundSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FOUND = 'Y';
	public static final char NOT_FOUND = 'N';

	//==== METHODS ====
	public void setWs0215RecFoundSw(char ws0215RecFoundSw) {
		this.value = ws0215RecFoundSw;
	}

	public char getWs0215RecFoundSw() {
		return this.value;
	}

	public void setFound() {
		value = FOUND;
	}

	public boolean isNotFound() {
		return value == NOT_FOUND;
	}

	public void setNotFound() {
		value = NOT_FOUND;
	}
}
