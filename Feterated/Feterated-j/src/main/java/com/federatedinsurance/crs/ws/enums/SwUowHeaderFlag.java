/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-UOW-HEADER-FLAG<br>
 * Variable: SW-UOW-HEADER-FLAG from program TS020000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwUowHeaderFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char START_OF_UOW_HEADERS = '0';
	public static final char NO_MORE_UOW_HEADERS = '1';

	//==== METHODS ====
	public void setUowHeaderFlag(char uowHeaderFlag) {
		this.value = uowHeaderFlag;
	}

	public char getUowHeaderFlag() {
		return this.value;
	}

	public void setStartOfUowHeaders() {
		value = START_OF_UOW_HEADERS;
	}

	public boolean isNoMoreUowHeaders() {
		return value == NO_MORE_UOW_HEADERS;
	}

	public void setNoMoreUowHeaders() {
		value = NO_MORE_UOW_HEADERS;
	}
}
