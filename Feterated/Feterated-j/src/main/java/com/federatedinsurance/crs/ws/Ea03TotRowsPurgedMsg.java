/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-03-TOT-ROWS-PURGED-MSG<br>
 * Variable: EA-03-TOT-ROWS-PURGED-MSG from program XZ400000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea03TotRowsPurgedMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-03-TOT-ROWS-PURGED-MSG
	private String flr1 = "==>TOTAL NUMBER";
	//Original name: FILLER-EA-03-TOT-ROWS-PURGED-MSG-1
	private String flr2 = " OF";
	//Original name: FILLER-EA-03-TOT-ROWS-PURGED-MSG-2
	private String flr3 = " NOTIFICATIONS";
	//Original name: FILLER-EA-03-TOT-ROWS-PURGED-MSG-3
	private String flr4 = " PURGED";
	//Original name: FILLER-EA-03-TOT-ROWS-PURGED-MSG-4
	private String flr5 = " FROM ACT_NOT";
	//Original name: FILLER-EA-03-TOT-ROWS-PURGED-MSG-5
	private String flr6 = "TABLES :";
	//Original name: EA-03-TOT-ROW-COUNT
	private String ea03TotRowCount = DefaultValues.stringVal(Len.EA03_TOT_ROW_COUNT);

	//==== METHODS ====
	public String getEa03TotRowsPurgedMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa03TotRowsPurgedMsgBytes());
	}

	public byte[] getEa03TotRowsPurgedMsgBytes() {
		byte[] buffer = new byte[Len.EA03_TOT_ROWS_PURGED_MSG];
		return getEa03TotRowsPurgedMsgBytes(buffer, 1);
	}

	public byte[] getEa03TotRowsPurgedMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, ea03TotRowCount, Len.EA03_TOT_ROW_COUNT);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setEa03TotRowCount(long ea03TotRowCount) {
		this.ea03TotRowCount = PicFormatter.display("Z(3),Z(3),Z(2)9-").format(ea03TotRowCount).toString();
	}

	public long getEa03TotRowCount() {
		return PicParser.display("Z(3),Z(3),Z(2)9-").parseLong(this.ea03TotRowCount);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA03_TOT_ROW_COUNT = 12;
		public static final int FLR1 = 15;
		public static final int FLR2 = 3;
		public static final int FLR3 = 14;
		public static final int FLR4 = 7;
		public static final int FLR6 = 9;
		public static final int EA03_TOT_ROWS_PURGED_MSG = EA03_TOT_ROW_COUNT + FLR1 + FLR2 + 2 * FLR3 + FLR4 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
