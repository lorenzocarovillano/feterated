/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplaySigned;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TR-CERT-RECIPIENTS<br>
 * Variable: TR-CERT-RECIPIENTS from program XZ0P90E0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TrCertRecipients implements ICopyable<TrCertRecipients> {

	//==== PROPERTIES ====
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TR_CERT_RECIPIENTS);
	//Original name: TR-REC-SEQ-NBR
	private int recSeqNbr = DefaultValues.INT_VAL;

	//==== CONSTRUCTORS ====
	public TrCertRecipients() {
	}

	public TrCertRecipients(TrCertRecipients trCertRecipients) {
		this();
		this.recSeqNbr = trCertRecipients.recSeqNbr;
	}

	//==== METHODS ====
	public String getTrCertRecipientsFormatted() {
		return getRecSeqNbrFormatted();
	}

	public TrCertRecipients initTrCertRecipientsHighValues() {
		recSeqNbr = Types.HIGH_INT_VAL;
		return this;
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTrCertRecipientsFormatted()).equals(END_OF_TABLE);
	}

	public void setRecSeqNbr(int recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	public int getRecSeqNbr() {
		return this.recSeqNbr;
	}

	public String getRecSeqNbrFormatted() {
		return NumericDisplaySigned.asString(getRecSeqNbr(), Len.REC_SEQ_NBR);
	}

	@Override
	public TrCertRecipients copy() {
		return new TrCertRecipients(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_SEQ_NBR = 5;
		public static final int TR_CERT_RECIPIENTS = REC_SEQ_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
