/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.federatedinsurance.crs.ws.UidgErrorIdOutput;
import com.federatedinsurance.crs.ws.enums.UidgIdType;

/**Original name: HALLUIDG<br>
 * Variable: HALLUIDG from copybook HALLUIDG<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Halluidg {

	//==== PROPERTIES ====
	/**Original name: UIDG-ID-TYPE<br>
	 * <pre>**      05  UIDG-ID-TYPE                  PIC X(01).</pre>*/
	private UidgIdType uidgIdType = new UidgIdType();
	//Original name: UIDG-UNIT-NBR
	private short uidgUnitNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: UIDG-ERROR-ID-OUTPUT
	private UidgErrorIdOutput uidgErrorIdOutput = new UidgErrorIdOutput();

	//==== METHODS ====
	public void setUidgCaIncomingBytes(byte[] buffer, int offset) {
		int position = offset;
		uidgIdType.setUidgIdType(MarshalByte.readString(buffer, position, UidgIdType.Len.UIDG_ID_TYPE));
		position += UidgIdType.Len.UIDG_ID_TYPE;
		uidgUnitNbr = MarshalByte.readBinaryShort(buffer, position);
	}

	public byte[] getUidgCaIncomingBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, uidgIdType.getUidgIdType(), UidgIdType.Len.UIDG_ID_TYPE);
		position += UidgIdType.Len.UIDG_ID_TYPE;
		MarshalByte.writeBinaryShort(buffer, position, uidgUnitNbr);
		return buffer;
	}

	public void setUidgUnitNbr(short uidgUnitNbr) {
		this.uidgUnitNbr = uidgUnitNbr;
	}

	public short getUidgUnitNbr() {
		return this.uidgUnitNbr;
	}

	public void setUidgCaOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		uidgErrorIdOutput.setUidgErrorIdOutputBytes(buffer, position);
	}

	public byte[] getUidgCaOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		uidgErrorIdOutput.getUidgErrorIdOutputBytes(buffer, position);
		return buffer;
	}

	public void initUidgCaOutputSpaces() {
		uidgErrorIdOutput.initUidgErrorIdOutputSpaces();
	}

	public UidgErrorIdOutput getUidgErrorIdOutput() {
		return uidgErrorIdOutput;
	}

	public UidgIdType getUidgIdType() {
		return uidgIdType;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UIDG_UNIT_NBR = 2;
		public static final int UIDG_CA_INCOMING = UidgIdType.Len.UIDG_ID_TYPE + UIDG_UNIT_NBR;
		public static final int UIDG_CA_OUTPUT = UidgErrorIdOutput.Len.UIDG_ERROR_ID_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
