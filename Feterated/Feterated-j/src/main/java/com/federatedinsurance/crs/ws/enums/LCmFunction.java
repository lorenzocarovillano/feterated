/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: L-CM-FUNCTION<br>
 * Variable: L-CM-FUNCTION from copybook TS54801<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LCmFunction {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char OPEN_PIPE = '1';
	public static final char CLOSE_PIPE = '2';

	//==== METHODS ====
	public void setFunction(char function) {
		this.value = function;
	}

	public char getFunction() {
		return this.value;
	}

	public void setCmFnOpenPipe() {
		value = OPEN_PIPE;
	}

	public void setCmFnClosePipe() {
		value = CLOSE_PIPE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FUNCTION = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
