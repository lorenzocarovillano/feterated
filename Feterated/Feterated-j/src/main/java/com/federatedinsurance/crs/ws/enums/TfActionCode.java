/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TF-ACTION-CODE<br>
 * Variable: TF-ACTION-CODE from copybook TS020TBL<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TfActionCode {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.TF_ACTION_CODE);
	public static final String NO_ACTION = "";
	public static final String FETCH_DATA_REQUEST = "FETCH";
	public static final String FETCH_ALL_DATA = "FETCH_ALL";
	public static final String FETCH_AND_SKIP_HIER = "FETCH_SKIP_HIER";
	public static final String FETCH_PENDING = "FETCH_PENDING";
	public static final String FETCH_WITH_EXACT_KEY = "FETCH_EXACT_KEY";
	public static final String FETCH_PRIORITY = "FETCH_PRIORITY";
	public static final String UPDATE_DATA = "UPDATE";
	public static final String UPDATE_AND_RETURN = "UPDATE_&_RETURN";
	public static final String INSERT_DATA = "INSERT";
	public static final String INSERT_AND_RETURN = "INSERT_&_RETURN";
	public static final String CHANGE_DATA = "CHANGE";
	public static final String CHANGE_AND_RETURN = "CHANGE_&_RETURN";
	public static final String DELETE_DATA = "DELETE";
	public static final String CASCADING_DELETE = "CASCADING_DELETE";
	public static final String FILTER_ONLY = "FILTER";
	public static final String SIMPLE_EDIT = "SIMPLE_EDIT";
	public static final String BYPASS_SIMPLE_EDITS = "BYPASS_SIMPLE_EDITS";
	public static final String IGNORE_REQUEST = "IGNORE";
	public static final String IGNORE_AND_RETURN = "IGNORE_&_RETURN";
	public static final String SET_DEFAULTS_REQUEST = "SET_DEFAULTS";
	private static final String[] UPDATE_ACTIONS = new String[] { "UPDATE", "UPDATE_&_RETURN", "INSERT", "INSERT_&_RETURN", "CHANGE",
			"CHANGE_&_RETURN", "DELETE", "CASCADING_DELETE" };

	//==== METHODS ====
	public void setTfActionCode(String tfActionCode) {
		this.value = Functions.subString(tfActionCode, Len.TF_ACTION_CODE);
	}

	public String getTfActionCode() {
		return this.value;
	}

	public void setTfAcFetchWithExactKey() {
		value = FETCH_WITH_EXACT_KEY;
	}

	public void setTfAcUpdateAndReturn() {
		value = UPDATE_AND_RETURN;
	}

	public void setTfAcInsertAndReturn() {
		value = INSERT_AND_RETURN;
	}

	public void setTfAcDeleteData() {
		value = DELETE_DATA;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TF_ACTION_CODE = 20;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
