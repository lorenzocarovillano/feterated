/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-READ-FOR-MSTR-SWT-IND<br>
 * Variable: WS-READ-FOR-MSTR-SWT-IND from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsReadForMstrSwtInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char YES = 'Y';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setReadForMstrSwtInd(char readForMstrSwtInd) {
		this.value = readForMstrSwtInd;
	}

	public void setReadForMstrSwtIndFormatted(String readForMstrSwtInd) {
		setReadForMstrSwtInd(Functions.charAt(readForMstrSwtInd, Types.CHAR_SIZE));
	}

	public char getReadForMstrSwtInd() {
		return this.value;
	}

	public boolean isYes() {
		return value == YES;
	}

	public void setYes() {
		value = YES;
	}

	public void setReadForMstrSwtNo() {
		value = NO;
	}
}
