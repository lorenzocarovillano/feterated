/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: UBOC-OBJECT-ERROR-INFO<br>
 * Variable: UBOC-OBJECT-ERROR-INFO from copybook HALLUBOC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class UbocObjectErrorInfo {

	//==== PROPERTIES ====
	//Original name: UBOC-ERROR-CODE
	private String errorCode = DefaultValues.stringVal(Len.ERROR_CODE);
	//Original name: UBOC-FAILED-MODULE
	private String failedModule = DefaultValues.stringVal(Len.FAILED_MODULE);
	//Original name: UBOC-FAILED-PARAGRAPH
	private String failedParagraph = DefaultValues.stringVal(Len.FAILED_PARAGRAPH);
	//Original name: UBOC-SQLCODE-DISPLAY
	private String sqlcodeDisplay = DefaultValues.stringVal(Len.SQLCODE_DISPLAY);
	//Original name: UBOC-EIBRESP-DISPLAY
	private String eibrespDisplay = DefaultValues.stringVal(Len.EIBRESP_DISPLAY);
	//Original name: UBOC-EIBRESP2-DISPLAY
	private String eibresp2Display = DefaultValues.stringVal(Len.EIBRESP2_DISPLAY);

	//==== METHODS ====
	public void setUbocObjectErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		errorCode = MarshalByte.readString(buffer, position, Len.ERROR_CODE);
		position += Len.ERROR_CODE;
		failedModule = MarshalByte.readString(buffer, position, Len.FAILED_MODULE);
		position += Len.FAILED_MODULE;
		failedParagraph = MarshalByte.readString(buffer, position, Len.FAILED_PARAGRAPH);
		position += Len.FAILED_PARAGRAPH;
		sqlcodeDisplay = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.SQLCODE_DISPLAY), Len.SQLCODE_DISPLAY);
		position += Len.SQLCODE_DISPLAY;
		eibrespDisplay = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.EIBRESP_DISPLAY), Len.EIBRESP_DISPLAY);
		position += Len.EIBRESP_DISPLAY;
		eibresp2Display = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.EIBRESP2_DISPLAY), Len.EIBRESP2_DISPLAY);
	}

	public byte[] getUbocObjectErrorInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, errorCode, Len.ERROR_CODE);
		position += Len.ERROR_CODE;
		MarshalByte.writeString(buffer, position, failedModule, Len.FAILED_MODULE);
		position += Len.FAILED_MODULE;
		MarshalByte.writeString(buffer, position, failedParagraph, Len.FAILED_PARAGRAPH);
		position += Len.FAILED_PARAGRAPH;
		MarshalByte.writeString(buffer, position, sqlcodeDisplay, Len.SQLCODE_DISPLAY);
		position += Len.SQLCODE_DISPLAY;
		MarshalByte.writeString(buffer, position, eibrespDisplay, Len.EIBRESP_DISPLAY);
		position += Len.EIBRESP_DISPLAY;
		MarshalByte.writeString(buffer, position, eibresp2Display, Len.EIBRESP2_DISPLAY);
		return buffer;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = Functions.subString(errorCode, Len.ERROR_CODE);
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	public void setFailedModule(String failedModule) {
		this.failedModule = Functions.subString(failedModule, Len.FAILED_MODULE);
	}

	public String getFailedModule() {
		return this.failedModule;
	}

	public void setFailedParagraph(String failedParagraph) {
		this.failedParagraph = Functions.subString(failedParagraph, Len.FAILED_PARAGRAPH);
	}

	public String getFailedParagraph() {
		return this.failedParagraph;
	}

	public void setSqlcodeDisplay(long sqlcodeDisplay) {
		this.sqlcodeDisplay = PicFormatter.display("-Z(8)9").format(sqlcodeDisplay).toString();
	}

	public void setUbocSqlcodeDisplayFormatted(String ubocSqlcodeDisplay) {
		this.sqlcodeDisplay = PicFormatter.display("-Z(8)9").format(ubocSqlcodeDisplay).toString();
	}

	public long getSqlcodeDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.sqlcodeDisplay);
	}

	public void setEibrespDisplay(long eibrespDisplay) {
		this.eibrespDisplay = PicFormatter.display("-Z(8)9").format(eibrespDisplay).toString();
	}

	public void setUbocEibrespDisplayFormatted(String ubocEibrespDisplay) {
		this.eibrespDisplay = PicFormatter.display("-Z(8)9").format(ubocEibrespDisplay).toString();
	}

	public long getEibrespDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.eibrespDisplay);
	}

	public void setEibresp2Display(long eibresp2Display) {
		this.eibresp2Display = PicFormatter.display("-Z(8)9").format(eibresp2Display).toString();
	}

	public void setUbocEibresp2DisplayFormatted(String ubocEibresp2Display) {
		this.eibresp2Display = PicFormatter.display("-Z(8)9").format(ubocEibresp2Display).toString();
	}

	public long getEibresp2Display() {
		return PicParser.display("-Z(8)9").parseLong(this.eibresp2Display);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_CODE = 10;
		public static final int FAILED_MODULE = 8;
		public static final int FAILED_PARAGRAPH = 30;
		public static final int SQLCODE_DISPLAY = 10;
		public static final int EIBRESP_DISPLAY = 10;
		public static final int EIBRESP2_DISPLAY = 10;
		public static final int UBOC_OBJECT_ERROR_INFO = ERROR_CODE + FAILED_MODULE + FAILED_PARAGRAPH + SQLCODE_DISPLAY + EIBRESP_DISPLAY
				+ EIBRESP2_DISPLAY;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
