/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-15-DPL-RESPONSE-GIVEN<br>
 * Variable: EA-15-DPL-RESPONSE-GIVEN from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea15DplResponseGiven {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-15-DPL-RESPONSE-GIVEN
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-15-DPL-RESPONSE-GIVEN-1
	private String flr2 = "A DPL RESPONSE";
	//Original name: FILLER-EA-15-DPL-RESPONSE-GIVEN-2
	private String flr3 = "HAS BEEN";
	//Original name: FILLER-EA-15-DPL-RESPONSE-GIVEN-3
	private String flr4 = "CAPTURED.";
	//Original name: FILLER-EA-15-DPL-RESPONSE-GIVEN-4
	private String flr5 = "PLEASE";
	//Original name: FILLER-EA-15-DPL-RESPONSE-GIVEN-5
	private String flr6 = "EVALUATE.";

	//==== METHODS ====
	public String getEa15DplResponseGivenFormatted() {
		return MarshalByteExt.bufferToStr(getEa15DplResponseGivenBytes());
	}

	public byte[] getEa15DplResponseGivenBytes() {
		byte[] buffer = new byte[Len.EA15_DPL_RESPONSE_GIVEN];
		return getEa15DplResponseGivenBytes(buffer, 1);
	}

	public byte[] getEa15DplResponseGivenBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 15;
		public static final int FLR3 = 9;
		public static final int FLR5 = 7;
		public static final int EA15_DPL_RESPONSE_GIVEN = 2 * FLR1 + FLR2 + 2 * FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
