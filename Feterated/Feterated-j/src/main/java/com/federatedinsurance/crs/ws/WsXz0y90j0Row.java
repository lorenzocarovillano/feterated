/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0Y90J0-ROW<br>
 * Variable: WS-XZ0Y90J0-ROW from program XZ0P90J0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0y90j0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZY9J0-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZY9J0-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZY9J0-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);
	//Original name: XZY9J0-FRM-ATC-IND
	private char frmAtcInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0Y90J0_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0y90j0RowBytes(buf);
	}

	public String getWsXz0y90j0RowFormatted() {
		return getPrepareNotRowFormatted();
	}

	public void setWsXz0y90j0RowBytes(byte[] buffer) {
		setWsXz0y90j0RowBytes(buffer, 1);
	}

	public byte[] getWsXz0y90j0RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0Y90J0_ROW];
		return getWsXz0y90j0RowBytes(buffer, 1);
	}

	public void setWsXz0y90j0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setPrepareNotRowBytes(buffer, position);
	}

	public byte[] getWsXz0y90j0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getPrepareNotRowBytes(buffer, position);
		return buffer;
	}

	public String getPrepareNotRowFormatted() {
		return MarshalByteExt.bufferToStr(getPrepareNotRowBytes());
	}

	/**Original name: XZY9J0-PREPARE-NOT-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0Y90J0 - SERVICE CONTRACT COPYBOOK FOR                   *
	 *             UOW : XZ_PREPARE_NOTIFICATION                   *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#        DATE      PRGRMR     DESCRIPTION                *
	 *  --------   --------- ---------- ---------------------------*
	 *  TO07614    11MAR2009 E404KXS    NEW                        *
	 *  TO0760222  31MAR2010 E404KXS    ADDED FRM-ATC-IND          *
	 * *************************************************************</pre>*/
	public byte[] getPrepareNotRowBytes() {
		byte[] buffer = new byte[Len.PREPARE_NOT_ROW];
		return getPrepareNotRowBytes(buffer, 1);
	}

	public void setPrepareNotRowBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
		position += Len.USERID;
		frmAtcInd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getPrepareNotRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position += Len.USERID;
		MarshalByte.writeChar(buffer, position, frmAtcInd);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	public void setFrmAtcInd(char frmAtcInd) {
		this.frmAtcInd = frmAtcInd;
	}

	public char getFrmAtcInd() {
		return this.frmAtcInd;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0y90j0RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int USERID = 8;
		public static final int FRM_ATC_IND = 1;
		public static final int PREPARE_NOT_ROW = CSR_ACT_NBR + NOT_PRC_TS + USERID + FRM_ATC_IND;
		public static final int WS_XZ0Y90J0_ROW = PREPARE_NOT_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
