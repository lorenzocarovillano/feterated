/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-DELETE-OBJECT<br>
 * Variable: WS-DELETE-OBJECT from program HALOUMEL<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsDeleteObject extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int WS_TABLE_NAME_MAXOCCURS = 3;

	//==== CONSTRUCTORS ====
	public WsDeleteObject() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_DELETE_OBJECT;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "HAL_ERR_LOG_MCM_V", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "HAL_ERR_LOG_MDRV_V", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "HAL_ERR_LOG_DTL_V", Len.FLR1);
	}

	/**Original name: WS-DELETE-OBJ-NAME<br>*/
	public String getDeleteObjName(int deleteObjNameIdx) {
		int position = Pos.wsDeleteObjName(deleteObjNameIdx - 1);
		return readString(position, Len.DELETE_OBJ_NAME);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_DELETE_OBJECT = 1;
		public static final int FLR1 = WS_DELETE_OBJECT;
		public static final int FLR2 = FLR1 + Len.FLR1;
		public static final int FLR3 = FLR2 + Len.FLR1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int wsTableName(int idx) {
			return 1 + idx * Len.WS_TABLE_NAME;
		}

		public static int wsDeleteObjName(int idx) {
			return wsTableName(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 18;
		public static final int DELETE_OBJ_NAME = 18;
		public static final int WS_TABLE_NAME = DELETE_OBJ_NAME;
		public static final int WS_DELETE_OBJECT = 3 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
