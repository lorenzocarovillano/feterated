/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-REPORT-ON-FILE-FLAG<br>
 * Variable: SW-REPORT-ON-FILE-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwReportOnFileFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NOT_ON_FILE = '0';
	public static final char ON_FILE = '1';

	//==== METHODS ====
	public void setReportOnFileFlag(char reportOnFileFlag) {
		this.value = reportOnFileFlag;
	}

	public char getReportOnFileFlag() {
		return this.value;
	}

	public void setNotOnFile() {
		value = NOT_ON_FILE;
	}

	public boolean isOnFile() {
		return value == ON_FILE;
	}

	public void setOnFile() {
		value = ON_FILE;
	}
}
