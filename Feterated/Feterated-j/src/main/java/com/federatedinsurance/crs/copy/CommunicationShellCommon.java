/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.MaReturnCode;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: COMMUNICATION-SHELL-COMMON<br>
 * Variable: COMMUNICATION-SHELL-COMMON from copybook TS020COM<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class CommunicationShellCommon {

	//==== PROPERTIES ====
	//Original name: MA-INPUT-DATA-SIZE
	private int maInputDataSize = DefaultValues.BIN_INT_VAL;
	//Original name: MA-INPUT-POINTER
	private int maInputPointer = DefaultValues.BIN_INT_VAL;
	//Original name: MA-OUTPUT-DATA-SIZE
	private int maOutputDataSize = DefaultValues.BIN_INT_VAL;
	//Original name: MA-OUTPUT-POINTER
	private int maOutputPointer = DefaultValues.BIN_INT_VAL;
	//Original name: MA-RETURN-CODE
	private MaReturnCode maReturnCode = new MaReturnCode();
	//Original name: MA-ERROR-MESSAGE
	private String maErrorMessage = DefaultValues.stringVal(Len.MA_ERROR_MESSAGE);
	//Original name: CSC-GENERAL-PARMS
	private CscGeneralParms cscGeneralParms = new CscGeneralParms();
	//Original name: CSC-FAILED-MODULE
	private String cscFailedModule = DefaultValues.stringVal(Len.CSC_FAILED_MODULE);
	//Original name: CSC-FAILED-PARAGRAPH
	private String cscFailedParagraph = DefaultValues.stringVal(Len.CSC_FAILED_PARAGRAPH);
	//Original name: CSC-SQLCODE-DISPLAY
	private String cscSqlcodeDisplay = DefaultValues.stringVal(Len.CSC_SQLCODE_DISPLAY);
	//Original name: CSC-EIBRESP-DISPLAY
	private String cscEibrespDisplay = DefaultValues.stringVal(Len.CSC_EIBRESP_DISPLAY);
	//Original name: CSC-EIBRESP2-DISPLAY
	private String cscEibresp2Display = DefaultValues.stringVal(Len.CSC_EIBRESP2_DISPLAY);
	//Original name: CSC-DATA-BUFFER-LENGTH
	private String cscDataBufferLength = DefaultValues.stringVal(Len.CSC_DATA_BUFFER_LENGTH);
	//Original name: CSC-DATA-BUFFER
	private String cscDataBuffer = DefaultValues.stringVal(Len.CSC_DATA_BUFFER);

	//==== METHODS ====
	public void setCommunicationShellCommonBytes(byte[] buffer) {
		setCommunicationShellCommonBytes(buffer, 1);
	}

	public byte[] getCommunicationShellCommonBytes() {
		byte[] buffer = new byte[Len.COMMUNICATION_SHELL_COMMON];
		return getCommunicationShellCommonBytes(buffer, 1);
	}

	public void setCommunicationShellCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		setMemoryAllocationParmsBytes(buffer, position);
		position += Len.MEMORY_ALLOCATION_PARMS;
		cscGeneralParms.setCscGeneralParmsBytes(buffer, position);
		position += CscGeneralParms.Len.CSC_GENERAL_PARMS;
		setCscErrorDetailsBytes(buffer, position);
		position += Len.CSC_ERROR_DETAILS;
		setCscShellDataBytes(buffer, position);
	}

	public byte[] getCommunicationShellCommonBytes(byte[] buffer, int offset) {
		int position = offset;
		getMemoryAllocationParmsBytes(buffer, position);
		position += Len.MEMORY_ALLOCATION_PARMS;
		cscGeneralParms.getCscGeneralParmsBytes(buffer, position);
		position += CscGeneralParms.Len.CSC_GENERAL_PARMS;
		getCscErrorDetailsBytes(buffer, position);
		position += Len.CSC_ERROR_DETAILS;
		getCscShellDataBytes(buffer, position);
		return buffer;
	}

	public void setMemoryAllocationParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		maInputDataSize = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		maInputPointer = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		maOutputDataSize = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		maOutputPointer = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		maReturnCode.value = MarshalByte.readFixedString(buffer, position, MaReturnCode.Len.MA_RETURN_CODE);
		position += MaReturnCode.Len.MA_RETURN_CODE;
		maErrorMessage = MarshalByte.readString(buffer, position, Len.MA_ERROR_MESSAGE);
	}

	public byte[] getMemoryAllocationParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryInt(buffer, position, maInputDataSize);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, maInputPointer);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, maOutputDataSize);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, maOutputPointer);
		position += Types.INT_SIZE;
		MarshalByte.writeString(buffer, position, maReturnCode.value, MaReturnCode.Len.MA_RETURN_CODE);
		position += MaReturnCode.Len.MA_RETURN_CODE;
		MarshalByte.writeString(buffer, position, maErrorMessage, Len.MA_ERROR_MESSAGE);
		return buffer;
	}

	public void setMaInputDataSize(int maInputDataSize) {
		this.maInputDataSize = maInputDataSize;
	}

	public int getMaInputDataSize() {
		return this.maInputDataSize;
	}

	public void setMaInputPointer(int maInputPointer) {
		this.maInputPointer = maInputPointer;
	}

	public int getMaInputPointer() {
		return this.maInputPointer;
	}

	public void setMaOutputDataSize(int maOutputDataSize) {
		this.maOutputDataSize = maOutputDataSize;
	}

	public int getMaOutputDataSize() {
		return this.maOutputDataSize;
	}

	public void setMaOutputPointer(int maOutputPointer) {
		this.maOutputPointer = maOutputPointer;
	}

	public int getMaOutputPointer() {
		return this.maOutputPointer;
	}

	public void setMaErrorMessage(String maErrorMessage) {
		this.maErrorMessage = Functions.subString(maErrorMessage, Len.MA_ERROR_MESSAGE);
	}

	public String getMaErrorMessage() {
		return this.maErrorMessage;
	}

	public String getMaErrorMessageFormatted() {
		return Functions.padBlanks(getMaErrorMessage(), Len.MA_ERROR_MESSAGE);
	}

	public void setCscErrorDetailsBytes(byte[] buffer, int offset) {
		int position = offset;
		cscFailedModule = MarshalByte.readString(buffer, position, Len.CSC_FAILED_MODULE);
		position += Len.CSC_FAILED_MODULE;
		cscFailedParagraph = MarshalByte.readString(buffer, position, Len.CSC_FAILED_PARAGRAPH);
		position += Len.CSC_FAILED_PARAGRAPH;
		cscSqlcodeDisplay = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.CSC_SQLCODE_DISPLAY), Len.CSC_SQLCODE_DISPLAY);
		position += Len.CSC_SQLCODE_DISPLAY;
		cscEibrespDisplay = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.CSC_EIBRESP_DISPLAY), Len.CSC_EIBRESP_DISPLAY);
		position += Len.CSC_EIBRESP_DISPLAY;
		cscEibresp2Display = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.CSC_EIBRESP2_DISPLAY), Len.CSC_EIBRESP2_DISPLAY);
	}

	public byte[] getCscErrorDetailsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cscFailedModule, Len.CSC_FAILED_MODULE);
		position += Len.CSC_FAILED_MODULE;
		MarshalByte.writeString(buffer, position, cscFailedParagraph, Len.CSC_FAILED_PARAGRAPH);
		position += Len.CSC_FAILED_PARAGRAPH;
		MarshalByte.writeString(buffer, position, cscSqlcodeDisplay, Len.CSC_SQLCODE_DISPLAY);
		position += Len.CSC_SQLCODE_DISPLAY;
		MarshalByte.writeString(buffer, position, cscEibrespDisplay, Len.CSC_EIBRESP_DISPLAY);
		position += Len.CSC_EIBRESP_DISPLAY;
		MarshalByte.writeString(buffer, position, cscEibresp2Display, Len.CSC_EIBRESP2_DISPLAY);
		return buffer;
	}

	public void setCscFailedModule(String cscFailedModule) {
		this.cscFailedModule = Functions.subString(cscFailedModule, Len.CSC_FAILED_MODULE);
	}

	public String getCscFailedModule() {
		return this.cscFailedModule;
	}

	public void setCscFailedParagraph(String cscFailedParagraph) {
		this.cscFailedParagraph = Functions.subString(cscFailedParagraph, Len.CSC_FAILED_PARAGRAPH);
	}

	public String getCscFailedParagraph() {
		return this.cscFailedParagraph;
	}

	public void setCscSqlcodeDisplay(long cscSqlcodeDisplay) {
		this.cscSqlcodeDisplay = PicFormatter.display("-Z(8)9").format(cscSqlcodeDisplay).toString();
	}

	public void setCscSqlcodeDisplayFormatted(String cscSqlcodeDisplay) {
		this.cscSqlcodeDisplay = PicFormatter.display("-Z(8)9").format(cscSqlcodeDisplay).toString();
	}

	public long getCscSqlcodeDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.cscSqlcodeDisplay);
	}

	public void setCscEibrespDisplay(long cscEibrespDisplay) {
		this.cscEibrespDisplay = PicFormatter.display("-Z(8)9").format(cscEibrespDisplay).toString();
	}

	public void setCscEibrespDisplayFormatted(String cscEibrespDisplay) {
		this.cscEibrespDisplay = PicFormatter.display("-Z(8)9").format(cscEibrespDisplay).toString();
	}

	public long getCscEibrespDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.cscEibrespDisplay);
	}

	public void setCscEibresp2Display(long cscEibresp2Display) {
		this.cscEibresp2Display = PicFormatter.display("-Z(8)9").format(cscEibresp2Display).toString();
	}

	public void setCscEibresp2DisplayFormatted(String cscEibresp2Display) {
		this.cscEibresp2Display = PicFormatter.display("-Z(8)9").format(cscEibresp2Display).toString();
	}

	public long getCscEibresp2Display() {
		return PicParser.display("-Z(8)9").parseLong(this.cscEibresp2Display);
	}

	public void setCscShellDataBytes(byte[] buffer, int offset) {
		int position = offset;
		cscDataBufferLength = MarshalByte.readFixedString(buffer, position, Len.CSC_DATA_BUFFER_LENGTH);
		position += Len.CSC_DATA_BUFFER_LENGTH;
		cscDataBuffer = MarshalByte.readString(buffer, position, Len.CSC_DATA_BUFFER);
	}

	public byte[] getCscShellDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cscDataBufferLength, Len.CSC_DATA_BUFFER_LENGTH);
		position += Len.CSC_DATA_BUFFER_LENGTH;
		MarshalByte.writeString(buffer, position, cscDataBuffer, Len.CSC_DATA_BUFFER);
		return buffer;
	}

	public void setCscDataBufferLength(short cscDataBufferLength) {
		this.cscDataBufferLength = NumericDisplay.asString(cscDataBufferLength, Len.CSC_DATA_BUFFER_LENGTH);
	}

	public short getCscDataBufferLength() {
		return NumericDisplay.asShort(this.cscDataBufferLength);
	}

	public void setCscDataBuffer(String cscDataBuffer) {
		this.cscDataBuffer = Functions.subString(cscDataBuffer, Len.CSC_DATA_BUFFER);
	}

	public String getCscDataBuffer() {
		return this.cscDataBuffer;
	}

	public String getCscDataBufferFormatted() {
		return Functions.padBlanks(getCscDataBuffer(), Len.CSC_DATA_BUFFER);
	}

	public CscGeneralParms getCscGeneralParms() {
		return cscGeneralParms;
	}

	public MaReturnCode getMaReturnCode() {
		return maReturnCode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MA_INPUT_DATA_SIZE = 4;
		public static final int MA_INPUT_POINTER = 4;
		public static final int MA_OUTPUT_DATA_SIZE = 4;
		public static final int MA_OUTPUT_POINTER = 4;
		public static final int MA_ERROR_MESSAGE = 256;
		public static final int MEMORY_ALLOCATION_PARMS = MA_INPUT_DATA_SIZE + MA_INPUT_POINTER + MA_OUTPUT_DATA_SIZE + MA_OUTPUT_POINTER
				+ MaReturnCode.Len.MA_RETURN_CODE + MA_ERROR_MESSAGE;
		public static final int CSC_FAILED_MODULE = 8;
		public static final int CSC_FAILED_PARAGRAPH = 30;
		public static final int CSC_SQLCODE_DISPLAY = 10;
		public static final int CSC_EIBRESP_DISPLAY = 10;
		public static final int CSC_EIBRESP2_DISPLAY = 10;
		public static final int CSC_ERROR_DETAILS = CSC_FAILED_MODULE + CSC_FAILED_PARAGRAPH + CSC_SQLCODE_DISPLAY + CSC_EIBRESP_DISPLAY
				+ CSC_EIBRESP2_DISPLAY;
		public static final int CSC_DATA_BUFFER_LENGTH = 4;
		public static final int CSC_DATA_BUFFER = 6000;
		public static final int CSC_SHELL_DATA = CSC_DATA_BUFFER_LENGTH + CSC_DATA_BUFFER;
		public static final int COMMUNICATION_SHELL_COMMON = MEMORY_ALLOCATION_PARMS + CscGeneralParms.Len.CSC_GENERAL_PARMS + CSC_ERROR_DETAILS
				+ CSC_SHELL_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
