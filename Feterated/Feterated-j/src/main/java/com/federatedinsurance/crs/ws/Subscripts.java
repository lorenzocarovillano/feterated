/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program TS020000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Subscripts {

	//==== PROPERTIES ====
	//Original name: SS-MAX-UMT-SUBSCRIPT
	private short maxUmtSubscript = ((short) 10);
	//Original name: SS-NLBE
	private short nlbe = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-UWRN
	private short uwrn = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public short getMaxUmtSubscript() {
		return this.maxUmtSubscript;
	}

	public void setNlbe(short nlbe) {
		this.nlbe = nlbe;
	}

	public short getNlbe() {
		return this.nlbe;
	}

	public void setUwrn(short uwrn) {
		this.uwrn = uwrn;
	}

	public short getUwrn() {
		return this.uwrn;
	}
}
