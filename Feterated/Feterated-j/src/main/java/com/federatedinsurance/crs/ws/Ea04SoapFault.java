/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-04-SOAP-FAULT<br>
 * Variable: EA-04-SOAP-FAULT from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea04SoapFault {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-04-SOAP-FAULT
	private String flr1 = "SOAP FAULT";
	//Original name: FILLER-EA-04-SOAP-FAULT-1
	private String flr2 = "IN CALLABLE";
	//Original name: FILLER-EA-04-SOAP-FAULT-2
	private String flr3 = "SERVICE.";
	//Original name: FILLER-EA-04-SOAP-FAULT-3
	private String flr4 = "SOAP RETURN";
	//Original name: FILLER-EA-04-SOAP-FAULT-4
	private String flr5 = "CODE:";
	//Original name: EA-04-SOAP-RETURN-CODE
	private String ea04SoapReturnCode = DefaultValues.stringVal(Len.EA04_SOAP_RETURN_CODE);

	//==== METHODS ====
	public String getEa04SoapFaultFormatted() {
		return MarshalByteExt.bufferToStr(getEa04SoapFaultBytes());
	}

	public byte[] getEa04SoapFaultBytes() {
		byte[] buffer = new byte[Len.EA04_SOAP_FAULT];
		return getEa04SoapFaultBytes(buffer, 1);
	}

	public byte[] getEa04SoapFaultBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea04SoapReturnCode, Len.EA04_SOAP_RETURN_CODE);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa04SoapReturnCode(int ea04SoapReturnCode) {
		this.ea04SoapReturnCode = NumericDisplay.asString(ea04SoapReturnCode, Len.EA04_SOAP_RETURN_CODE);
	}

	public int getEa04SoapReturnCode() {
		return NumericDisplay.asInt(this.ea04SoapReturnCode);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA04_SOAP_RETURN_CODE = 8;
		public static final int FLR1 = 11;
		public static final int FLR2 = 12;
		public static final int FLR3 = 9;
		public static final int FLR5 = 6;
		public static final int EA04_SOAP_FAULT = EA04_SOAP_RETURN_CODE + FLR1 + 2 * FLR2 + FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
