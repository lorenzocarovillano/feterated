/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-ACTION-CODE<br>
 * Variable: WS-ACTION-CODE from program MU0Q0004<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsActionCode {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.ACTION_CODE);
	public static final String FETCH_ACTION = "FETCH";
	public static final String FILTER_ACTION = "FILTER";
	public static final String FETCH_WITH_EXACT_KEY = "FETCH_EXACT_KEY";

	//==== METHODS ====
	public void setActionCode(String actionCode) {
		this.value = Functions.subString(actionCode, Len.ACTION_CODE);
	}

	public String getActionCode() {
		return this.value;
	}

	public void setFetchAction() {
		value = FETCH_ACTION;
	}

	public boolean isFilterAction() {
		return value.equals(FILTER_ACTION);
	}

	public void setFilterAction() {
		value = FILTER_ACTION;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACTION_CODE = 20;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
