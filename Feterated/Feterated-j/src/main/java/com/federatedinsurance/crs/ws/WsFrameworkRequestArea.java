/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.copy.Xz0y90h0;
import com.federatedinsurance.crs.ws.occurs.WsXz0y90h1Row;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-FRAMEWORK-REQUEST-AREA<br>
 * Variable: WS-FRAMEWORK-REQUEST-AREA from program XZ0P90H0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsFrameworkRequestArea extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int WS_XZ0Y90H1_ROW_MAXOCCURS = 50;
	//Original name: XZ0Y90H0
	private Xz0y90h0 xz0y90h0 = new Xz0y90h0();
	//Original name: WS-XZ0Y90H1-ROW
	private WsXz0y90h1Row[] wsXz0y90h1Row = new WsXz0y90h1Row[WS_XZ0Y90H1_ROW_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public WsFrameworkRequestArea() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_FRAMEWORK_REQUEST_AREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsFrameworkRequestAreaBytes(buf);
	}

	public void init() {
		for (int wsXz0y90h1RowIdx = 1; wsXz0y90h1RowIdx <= WS_XZ0Y90H1_ROW_MAXOCCURS; wsXz0y90h1RowIdx++) {
			wsXz0y90h1Row[wsXz0y90h1RowIdx - 1] = new WsXz0y90h1Row();
		}
	}

	public String getWsFrameworkRequestAreaFormatted() {
		return MarshalByteExt.bufferToStr(getWsFrameworkRequestAreaBytes());
	}

	public void setWsFrameworkRequestAreaBytes(byte[] buffer) {
		setWsFrameworkRequestAreaBytes(buffer, 1);
	}

	public byte[] getWsFrameworkRequestAreaBytes() {
		byte[] buffer = new byte[Len.WS_FRAMEWORK_REQUEST_AREA];
		return getWsFrameworkRequestAreaBytes(buffer, 1);
	}

	public void setWsFrameworkRequestAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		setWsXz0y90h0RowBytes(buffer, position);
		position += Len.WS_XZ0Y90H0_ROW;
		for (int idx = 1; idx <= WS_XZ0Y90H1_ROW_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				wsXz0y90h1Row[idx - 1].setWsXz0y90h1RowBytes(buffer, position);
				position += WsXz0y90h1Row.Len.WS_XZ0Y90H1_ROW;
			} else {
				wsXz0y90h1Row[idx - 1].initWsXz0y90h1RowSpaces();
				position += WsXz0y90h1Row.Len.WS_XZ0Y90H1_ROW;
			}
		}
	}

	public byte[] getWsFrameworkRequestAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		getWsXz0y90h0RowBytes(buffer, position);
		position += Len.WS_XZ0Y90H0_ROW;
		for (int idx = 1; idx <= WS_XZ0Y90H1_ROW_MAXOCCURS; idx++) {
			wsXz0y90h1Row[idx - 1].getWsXz0y90h1RowBytes(buffer, position);
			position += WsXz0y90h1Row.Len.WS_XZ0Y90H1_ROW;
		}
		return buffer;
	}

	public void setWsXz0y90h0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xz0y90h0.setPrepareInsPolRowBytes(buffer, position);
	}

	public byte[] getWsXz0y90h0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xz0y90h0.getPrepareInsPolRowBytes(buffer, position);
		return buffer;
	}

	public WsXz0y90h1Row getWsXz0y90h1Row(int idx) {
		return wsXz0y90h1Row[idx - 1];
	}

	public Xz0y90h0 getXz0y90h0() {
		return xz0y90h0;
	}

	@Override
	public byte[] serialize() {
		return getWsFrameworkRequestAreaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_XZ0Y90H0_ROW = Xz0y90h0.Len.PREPARE_INS_POL_ROW;
		public static final int WS_FRAMEWORK_REQUEST_AREA = WS_XZ0Y90H0_ROW
				+ WsFrameworkRequestArea.WS_XZ0Y90H1_ROW_MAXOCCURS * WsXz0y90h1Row.Len.WS_XZ0Y90H1_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
