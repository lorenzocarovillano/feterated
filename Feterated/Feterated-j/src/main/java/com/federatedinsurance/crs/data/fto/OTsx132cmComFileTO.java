/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.io.file.FileRecord;
import com.federatedinsurance.crs.ws.OTsx132cmComRecord;

/**Original name: O-TSX132CM-COM-FILE<br>
 * File: O-TSX132CM-COM-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OTsx132cmComFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: O-TSX132CM-COM-RECORD
	private OTsx132cmComRecord oTsx132cmComRecord = new OTsx132cmComRecord();

	//==== METHODS ====
	@Override
	public void getData(byte[] destination, int offset) {
		oTsx132cmComRecord.getoTsx132cmComRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		oTsx132cmComRecord.setoTsx132cmComRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return OTsx132cmComRecord.Len.O_TSX132CM_COM_RECORD;
	}

	public OTsx132cmComRecord getoTsx132cmComRecord() {
		return oTsx132cmComRecord;
	}

	@Override
	public IBuffer copy() {
		OTsx132cmComFileTO copyTO = new OTsx132cmComFileTO();
		copyTO.assign(this);
		return copyTO;
	}
}
