/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import java.util.NoSuchElementException;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.inspect.InspectUtil;
import com.bphx.ctu.af.util.tokenizer.CharTokenizer;
import com.bphx.ctu.af.util.tokenizer.IStringTokenizer;
import com.federatedinsurance.crs.copy.NameOut;
import com.federatedinsurance.crs.ws.CiwbnsrbData;
import com.federatedinsurance.crs.ws.Miscellaneous;
import com.federatedinsurance.crs.ws.NameScrubRecord;
import com.federatedinsurance.crs.ws.redefines.FormatOutLast;
import com.federatedinsurance.crs.ws.redefines.WsLastName;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: CIWBNSRB<br>
 * <pre>INSTALLATION.
 *     PMS.
 * REMARKS.                                                         00006
 * *****************************************************************00026
 *  THIS PROGRAM STANDARDIZES THE FORMAT OF CLIENT NAMES ENTERED   *00027
 *  IN THE CIS DATA BASE.  CIWBNSRB IS USED WHEN ALL CLT NMS ARE   *00028
 *  ENTERED WITH ALL CAPITAL LETTERS.                              *00028
 * *****************************************************************00007
 *   COMPILE NOTES FOR TEST REGION.                                *00009
 *     1. USE 'TESTBSUB' JCL IN THE CIS.CIS.TESTC.JCLLIB.          *00010
 *     2. COMPILE CHANGES TO BOTH CLNKLIB AND LINKLIB WITH         *00011
 *        NO SUFFIX. ( CHANGE OBJLIB )                             *00012
 *        ** IF ANY DISPLAYS OR EXHIBITS ARE IN CODE, BE SURE TO   *00013
 *           COMMENT THEM OUT BEFORE COMPILING TO THE CLNKLIB.     *00014
 *     3. TO COMPILE ONLINE PGMS THAT LINK TO NAMESCRUB, PUT THE   *00015
 *        CLNKLIB BEFORE THE LINKLIB IN THE LINKAGE CONCATINATION. *00016
 *     4. COBPARMS :  THE 'NODYNAM' AND 'NORES' PARMS WILL INSURE  *00017
 *                      THAT NO COBOL II MODULES WILL BE USED.     *00018
 *        LINKPARMS:  USE 'NCAL' SINCE IT WILL ALWAYS BE 'CALLED'. *00019
 *                                                                 *00020
 *   COMPILE NOTES FOR PROD REGION.                                *00021
 *     1. MAKE SURE ALL EXHIBITS AND DISPLAYS ARE COMMENTED OUT    *00022
 *        OR DELETED.                                              *00023
 * *****************************************************************00025
 * *****************************************************************
 * ********************  R L S E   8.0  ****************************
 * *****************************************************************
 * *  RRRRRR   LL       SSSSSS  EEEEEEE         88888      0000   **
 * *  RR   RR  LL      SS       EE             88   88    00  00  **
 * *  RR   RR  LL      SS       EE             88   88    00  00  **
 * *  RRRRRR   LL       SSSSS   EEEE            88888  -- 00  00  **
 * *  RR   RR  LL           SS  EE             88   88    00  00  **
 * *  RR   RR  LL           SS  EE             88   88    00  00  **
 * *  RR   RR  LLLLLLL SSSSSS   EEEEEEE         88888      0000   **
 * *****************************************************************
 *                       MAINTENANCE LOG                           *
 * *****************************************************************00029
 *  --DATE-- -PROGRAMMER- -CHGE ID- ----- NATURE OF CHANGE --------*00030
 *                                                                 *00031
 *  11-28-90  P. SMITH    BP0009     MODIFICATIONS TO REPLACE ALL  *00032
 *                                   COBOL 'UNSTRING' COMMANDS WITH*00033
 *                                   THE UNSTRING SUBRTN 'DGRUSTR'.*00034
 *                                                                 *00035
 *  12-10-90  D. OSBORNE  BP0010     MODIFICATIONS TO REPLACE ALL  *00036
 *                                   COBOL 'STRING' COMMANDS WITH  *00037
 *                                   THE STRING SUBRTN 'DGRSTRG'.  *00038
 *                                                                 *00039
 *  09-03-91  R. FIFER    BP0011     CORRECT 'STRING' TO HANDLE    *00040
 *                                   MULTIPLE MIDDLE NAMES.        *00041
 *                                                                 *00042
 *  10-01-91  R. FIFER    BP0012     CORRECT THE HANDLING OF 11    *00043
 *                                   WORD CLIENT NAME.             *00044
 *                                                                 *00045
 *  12/04/91   RRF        ELIM 2ND SPACE IN SCRUBBED CO. NAME (CIWS*00046
 *  07/16/91   RRF        EXPAND OUTPUT NM FIELDS FROM 14 TO 60BYTES00047
 *  01/23/92   RRF        ELIM 1ST SPACE IN LONG-NAME              *00048
 *  02/19/92  J. RICE   CHANGES REQUIRED TO CORRECT NAME SCRUBBING *00049
 *                       - DGRSTRG RETURNED ONLY THE 1ST 14 CHAR   *00050
 *                       - DGRUSTR RETURNED INVALID DATA IF PREV   *00051
 *                             NAME WAS LONGER THAN CURRENT NAME  * 00052
 *                       - SEE DWTNAM00 FOR ADDITNL CHG FOR R3 ABOV*00053
 *  07/30/92   RRF        ELIM ACCUMULATION OF MIDDLE INITIALS J J H00054
 *  09/03/92   RRF        IF TYPE-CODE SUBMITTED = P AND COMPANY   *00055
 *                        WORD FOUND ON TBL TREAT COMPANY WORD AS  *00056
 *                        JUST ANOTHER WORD IN A PERSONAL NAME.    *00057
 *  10/27/92   RRF     REPLACE COMMAS WITH 1 SPACE DON'T ELIM SPACE*00058
 *  12/01/93   REH  (BP7012):  CODE CHANGES TO CORRECT PROBLEM     *00059
 *                             SCRUBBING LAST NAME BEGINNING WITH  *00060
 *                             "ST" AND CONTAINING MORE THAN TWO   *00061
 *                             ADDITIONAL COMPONENTS               *00062
 *                             (EG. "ST MATTHEWS BAPTIST CHURCH"). *00063
 *                             NOTE THAT THIS PROBLEM IS LOGGED AS *00064
 *                             QA# 660166.                         *00065
 *  01/13/94   PMS  (770023):  CHANGE STRING COPYBOOK TO CONFORM   *00066
 *                             TO SERIES III NAMING CONVENTIONS.   *00067
 *  07/11/94   RRF             RENAME DLACIS01 TO CISLNSRB.        *00066
 *                             RENAME DWTNAM00 TO CISLTNAM.        *00066
 *  09/27/94   RRF             IF ALL 60 CHARS OF LONG-NM FULL     *00066
 *                               LAST NM NOT RETURNED.             *00066
 *  09/21/95                   CODE FIX REQUESTED BY SECURA.       *
 *  04/15/96   6751 (018078):  ADDED '~' TO GOOD-CHAR.             *00066
 *  05/14/98   6751 (042026) FIX TO PREVENT SCRUBBING OF NAMES     *
 *                           INCORRECTLY AGAINST THE PREFIX/SUFFIX/*
 *                           COMPANY ABBREVIATIONS INTERNAL TABLE; *
 *                                                                 *00055
 *  02/02/99   6751 (047468) IF TYPE-CODE SUBMITTED = C & SUFFIX   *00055
 *                           WORD FOUND ON TBL TREAT SUFFIX WORD   *00056
 *                           AS JUST ANOTHER WORD IN A COMPANY NAME*00055
 *                           (42026 ADDRESSED SAME FOR PREFIX)     *00055
 *                                                                 *00055
 *  07/08/00   6503 (005426) MODIFICATIONS TO USE SERIAL SEARCH    *00055
 *                           INSTEAD OF BINARY SEARCH DUE TO       *00056
 *                           COLLATING DIFFERENCES BETWEEN ASCII   *00055
 *                           AND EBCDIC (HOST VS WORKSTAION)       *00055
 *  01/20/2003 JAN   66398   CREATE A HYBRID VERSION THAT WILL     *
 *                            WORK FOR BOTH ONLINE AND AUTO DIRECT *
 * *****************************************************************00068
 * ****  COPY 'CIWCNSBA'.                                           01598</pre>*/
public class Ciwbnsrb extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private CiwbnsrbData ws = new CiwbnsrbData();
	//Original name: NAME-SCRUB-RECORD
	private NameScrubRecord nameScrubRecord;

	//==== METHODS ====
	/**Original name: 0010-MAINLINE<br>
	 * <pre>*****************************************************************     ***
	 *  CONFIDENTIAL INFORMATION - LIMITED DISTRIBUTION TO AUTHORIZED  *     ...
	 *  PERSONS ONLY.                                                  *07/16/91
	 *                                                                 *COPYRT
	 *  These materials and the information herein are the             *   LV001
	 *  confidential, trade secret, and proprietary product and        *00006
	 *  property of Policy Management Systems Corporation ("PMSC")     *00007
	 *  and may not be copied, reproduced, or used except as           *00008
	 *  permitted in writing by PMSC.  Under no circumstances should   *00009
	 *  any portion of these materials or the information contained    *00010
	 *  herein be disclosed or used without the written consent of     *00011
	 *  PMSC.                                                          *00012
	 *                                                                 *00013
	 *  Copyright @, 1992, 1993, 1994 POLICY MANAGEMENT SYSTEMS        *00014
	 *  CORPORATION.  UNPUBLISHED - RIGHTS RESERVED UNDER THE COPYRIGHT*00015
	 *  LAWS OF THE UNITED STATES.  All rights reserved.               *00016
	 * *************************************************************** *00017
	 * **************************************************************** 00230
	 *  THIS ROUTINE WILL SET ALL SWITCHES, COUNTERS AND WS FIELDS TO * 00231
	 *  ZEROS AND SPACES RESPECTIVELY                                 * 00232
	 * **************************************************************** 00233</pre>*/
	public long execute(NameScrubRecord nameScrubRecord) {
		this.nameScrubRecord = nameScrubRecord;
		// COB_CODE: PERFORM 0015-INITIALIZE THRU 0015-INITIALIZE-EXIT.
		initialize();
		//**************************************************************** 00237
		// THIS ROUTINE WILL REMOVE COMMAS AND PERIODS FROM WS-NAME-WORK * 00238
		// AND WILL ADJUST THE NAME BY SHIFTING THE CHARATERS TO THE LEFT* 00239
		//**************************************************************** 00240
		// COB_CODE: SET WORK-IDX TO 1.
		ws.setWorkIdx(1);
		// COB_CODE: PERFORM 0100-STRIP-COMMAS-PERIODS THRU 0100-STRIP-EXIT
		//                   VARYING NAME-INDEX FROM 1 BY 1
		//                              UNTIL NAME-INDEX > 60.
		ws.setNameIndex(1);
		while (!(ws.getNameIndex() > 60)) {
			stripCommasPeriods();
			ws.setNameIndex(Trunc.toInt(ws.getNameIndex() + 1, 9));
		}
		// COB_CODE: MOVE SPACE TO FULL-NAME-IN.
		this.nameScrubRecord.getFullNameIn().setFullNameIn("");
		// COB_CODE: MOVE WS-NAME-WORK TO FULL-NAME-IN.
		this.nameScrubRecord.getFullNameIn().setFullNameIn(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork());
		// COB_CODE: MOVE SPACE TO WS-NAME-WORK.
		ws.getWsNameWorkArea().getWsNameWork().setWsNameWork("");
		//***************************************************************  00250
		// THE NEXT SENTENCE WILL SET A ERROR IN THE LINKAGE ERROR TABLE*  00251
		// WHEN IN VALID CHARACTERS ARE FOUND IN WS-NANE-WORK           *  00252
		//***************************************************************  00253
		// COB_CODE: IF INVALID-CHAR-FOUND
		//              PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
		if (ws.getSwitches().isInvalidCharFound()) {
			// COB_CODE: MOVE 'E' TO RETURNED-ERROR-SW
			this.nameScrubRecord.getReturnedErrorSw().setReturnedErrorSwFormatted("E");
			// COB_CODE: MOVE +007 TO WS-ERROR-COUNT
			ws.getMiscellaneous().setWsErrorCount(((short) 7));
			// COB_CODE: MOVE 'INV' TO WS-ERROR-CODE
			ws.getMiscellaneous().setWsErrorCode("INV");
			// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
			buildError();
		}
		//***************************************************************  00261
		// THE NEXT SENTENCE WILL SET A WARNING IN THE LINKAGE ERROR    *  00262
		// TABLE WHEN AN HYPHEN IS INCOUNTERED IN WS-NANE-WORK          *  00263
		//***************************************************************  00264
		// COB_CODE: IF HYPHEN-FOUND
		//              PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
		if (ws.getSwitches().isHyphenFound()) {
			// COB_CODE: MOVE +008 TO WS-ERROR-COUNT
			ws.getMiscellaneous().setWsErrorCount(((short) 8));
			// COB_CODE: MOVE 'HYP' TO WS-ERROR-CODE
			ws.getMiscellaneous().setWsErrorCode("HYP");
			// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
			buildError();
		}
		//***************************************************************  00271
		// THIS ROUTINE WILL REMOVE REDUNDANT SPACES FROM WS-NAME-WORK. *  00272
		// IT WILL NOT REMOVE THE ONE SEPERATION BLANK BETWEEN NAMES.   *  00273
		//***************************************************************  00274
		// COB_CODE: MOVE  '0' TO WS-FIRST-BLANK-SW.
		ws.getSwitches().setWsFirstBlankSwFormatted("0");
		// COB_CODE: SET WORK-IDX TO 1.
		ws.setWorkIdx(1);
		// COB_CODE: PERFORM 0150-STRIP-BLANKS THRU 0150-STRIP-EXIT
		//                   VARYING NAME-INDEX FROM 1 BY 1
		//                              UNTIL NAME-INDEX > 60.
		ws.setNameIndex(1);
		while (!(ws.getNameIndex() > 60)) {
			stripBlanks();
			ws.setNameIndex(Trunc.toInt(ws.getNameIndex() + 1, 9));
		}
		// COB_CODE: MOVE WS-NAME-WORK TO FULL-NAME-IN.
		this.nameScrubRecord.getFullNameIn().setFullNameIn(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork());
		// COB_CODE: MOVE WS-NAME-WORK TO DISPLAY-OUT.
		this.nameScrubRecord.getNameOut().setCompanyOut(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork());
		//***************************************************************  00284
		// THIS ROUTINE WILL PARSE WS-FULL-NAME-IN INTO THE PARSED NAME *  00285
		// TABLE. THERE ARE 14 POSITIONS FOR THE PARSED NAME AND A ONE  *  00286
		// POSITION TYPE CODE IDENTIFIER THAT WILL BE STORED IN A 10    *  00287
		// OCCURENCE TABLE. EACH OCCURENCE WILL BE COMPARED TO A PREFIX *  00288
		// TABLE, A SUFFIX OR A COMPANY TABLE IN LATER LOGIC.           *  00289
		//***************************************************************  00290
		// COB_CODE: SET PARSE-IDX TO 1.
		ws.setParseIdx(1);
		// COB_CODE: SET WORK-IDX TO 1.
		ws.setWorkIdx(1);
		// COB_CODE: MOVE 0 TO WS-SPACE-COUNTER.
		ws.getMiscellaneous().setWsSpaceCounter(((short) 0));
		// COB_CODE: MOVE SPACES TO WS-NAME-WORK.
		ws.getWsNameWorkArea().getWsNameWork().setWsNameWork("");
		// COB_CODE: PERFORM 0200-PARSE-NAME-IN THRU 0200-PARSE-EXIT
		//                   VARYING NAME-INDEX FROM 1 BY 1
		//                          UNTIL NAME-INDEX > 60
		//                                   OR PARSE-IDX > 10.
		ws.setNameIndex(1);
		while (!(ws.getNameIndex() > 60 || ws.getParseIdx() > 10)) {
			parseNameIn();
			ws.setNameIndex(Trunc.toInt(ws.getNameIndex() + 1, 9));
		}
		//                            OR PARSE-IDX > 11.                   00299
		//***************************************************************  00301
		// THIS ROUTINE WILL CHECK FULL-NAME-IN TO SEE IF THERE IS A    *  00302
		// COMMA AFTER THE FIRST WORD. IF ONE IS FOUND WE ASSUME THAT   *  00303
		// THIS WORD PRIOR TO THE COMMA IS THE LAST NAME OF THE CLIENT. *  00304
		//***************************************************************  00305
		//*** PERFORM 0700-DETERMINE-LAST-NAME                             00307
		//***                                THRU 0700-LAST-NAME-EXIT.     00308
		// COB_CODE: PERFORM 9700-DETERMINE-LAST-NAME
		//              THRU 9700-DETERMINE-LAST-NAME-EXIT.
		determineLastName();
		//                                 THRU 9700-LAST-NAME-EXIT.       00308
		//***************************************************************  00310
		// IN THIS LOGIC, EACH PARSED OCCURENCE WITHIN THE PARSED TABLE *  00311
		// IS USED IN A TABLE SEARCH TO DETERMINE IF THAT WORD IS A     *  00312
		// COMPANY, PREFIX OR SUFFIX.                                   *  00313
		// NOTE: PARAGRAPH WILL BE PERFORMED FOR COMPANY OR IF BATCH FED*
		//***************************************************************  00314
		//** IN FED THERE IS NO IF STATEMENT HERE ALL PERFORM 0300- ****
		//** I DO NOT KNOW WHY THEY DO NOT WANT PERSONAL NAMES TO   ****
		//** NOT RUN THROUGH HERE EXCEPT FOR BATCH LOAD.            ****
		//** WE WILL NOT USE THIS INDICATOR. IF WE EVER DECIDE TO   ****
		//** USE THIS INDICATOR WE WILL NEED TO USE THE NEW VERSION ****
		//** OF CISLNSRB WHICH IS NOT THE SAME SIZE AS THE CURRENT  ****
		//** VERSION.
		//**  IF BATCH-NAME-SCRUB OR TYPE-COMPANY
		// COB_CODE: PERFORM 0300-DETERMINE-PREFIX-SUFFIX THRU
		//                   0300-PREFIX-SUFFIX-EXIT
		//                   VARYING PARSE-IDX FROM 1 BY 1
		//                          UNTIL PARSE-IDX > 10.
		ws.setParseIdx(1);
		while (!(ws.getParseIdx() > 10)) {
			determinePrefixSuffix();
			ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
		}
		//***************************************************************  00321
		// THIS PERFORM WILL DETERMINE TYPE CODE IF WHEN NAMESCRUB IS   *  00322
		// CALLED THE TYPE CODE IN LINKAGE IS SPACE.                    *  00323
		//***************************************************************  00324
		// COB_CODE: PERFORM 0350-DETERMINE-TYPE-CODE THRU 0350-TYPE-EXIT.
		determineTypeCode();
		//***************************************************************  00328
		// EACH PARSED WORD IN THE PARSED TABLE IS CHECKED TO SEE IF A  *  00329
		// CONJOINED WORD EXISTS.  IF OR, AND OR & IS FOUND  A SWITCH IS*  00330
		// SET AND THIS SENTENCE WILL SET A WARNING IN THE ERROR TABLE  *  00331
		// IN LINKAGE.                                                  *  00332
		//***************************************************************  00333
		// COB_CODE: IF AND-FOUND
		//              PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
		if (ws.getSwitches().isAndFound()) {
			// COB_CODE: MOVE +006 TO WS-ERROR-COUNT
			ws.getMiscellaneous().setWsErrorCount(((short) 6));
			// COB_CODE: MOVE 'AND' TO WS-ERROR-CODE
			ws.getMiscellaneous().setWsErrorCode("AND");
			// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
			buildError();
		}
		//***************************************************************  00340
		// IF THE TYPE CODE IS EQUAL TO C, THIS NEXT SET OF LOGIC WILL  *  00341
		// EXECUTE. IF A  CONJOINED WORD IS FOUND IN THE PARSED TABLE   *  00342
		// IT WILL BE REMOVED AND THE PARSED TABLE WILL BE RESET WITHOUT*  00343
		// THE CONJOINED WORD.                                          *  00344
		//   THE COMPANY NAME WILL BE BUILT WHEATHER A CONJOINED WORD IS*  00345
		//   FOUND OR NOT.                                              *  00346
		//***************************************************************  00347
		//*************************************************************
		//** FOLLOWING IS THE CODE THAT IS IN THE FEDERATED VERSION.
		//** THERE WAS SOME WORKING STORAGE THAT NEEDED TO BE ADDED
		//** FOR THIS CODE.
		//** THIS CODE REPLACES THROUGH THE NEXT COMMENT.
		//** WE ARE USING THIS BECAUSE THE CSC CODE CANNOT HANDLE
		//** WHEN MORE THAN ONE AND WORDS ARE FOUND IN THE NAME.
		//*************************************************************
		// COB_CODE: IF TYPE-COMPANY
		//                 THRU 9475-BUILD-CO-OUTPUT-EXIT
		//           END-IF.
		if (this.nameScrubRecord.getTypeCode().isCompany()) {
			// COB_CODE: IF AND-FOUND
			//              END-IF
			//           END-IF
			if (ws.getSwitches().isAndFound()) {
				// COB_CODE: SET PARSE-IDX         TO 1
				ws.setParseIdx(1);
				// COB_CODE: MOVE '0' TO WS-AND-SW
				ws.getSwitches().setWsAndSwFormatted("0");
				// COB_CODE: MOVE ZEROS            TO HOLD-PARSE-IDX
				//                                    HOLD-PARSE-MIN-IDX
				//                                    HOLD-PARSE-MAX-IDX
				ws.getMiscellaneous().setHoldParseIdx(((short) 0));
				ws.getWsStringData().setHoldParseMinIdx(((short) 0));
				ws.getWsStringData().setHoldParseMaxIdx(((short) 0));
				// COB_CODE: PERFORM 0400-REMOVE-AND
				//              THRU 0400-REMOVE-AND-EXIT
				//                   VARYING PARSE-IDX
				//                   FROM 1 BY 1
				//                   UNTIL PARSE-IDX > 10
				ws.setParseIdx(1);
				while (!(ws.getParseIdx() > 10)) {
					removeAnd();
					ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
				}
				// COB_CODE: IF ((HOLD-PARSE-MIN-IDX > 0)
				//            AND
				//             (HOLD-PARSE-MAX-IDX > HOLD-PARSE-MIN-IDX))
				//                      UNTIL PARSE-IDX > HOLD-PARSE-MAX-IDX
				//           END-IF
				if (ws.getWsStringData().getHoldParseMinIdx() > 0
						&& ws.getWsStringData().getHoldParseMaxIdx() > ws.getWsStringData().getHoldParseMinIdx()) {
					// COB_CODE: MOVE HOLD-PARSE-MIN-IDX
					//                              TO HOLD-PARSE-TARGET-IDX
					ws.getWsStringData().setHoldParseTargetIdx(ws.getWsStringData().getHoldParseMinIdx());
					// COB_CODE: ADD +1             TO HOLD-PARSE-MIN-IDX
					ws.getWsStringData().setHoldParseMinIdx(Trunc.toShort(1 + ws.getWsStringData().getHoldParseMinIdx(), 2));
					// COB_CODE: PERFORM 0425-MOVE-TABLE-ELEMENTS
					//              THRU 0425-MOVE-TABLE-ELEMENTS-EXIT
					//                   VARYING PARSE-IDX
					//                   FROM HOLD-PARSE-MIN-IDX BY 1
					//                   UNTIL PARSE-IDX > HOLD-PARSE-MAX-IDX
					ws.setParseIdx(ws.getWsStringData().getHoldParseMinIdx());
					while (!(ws.getParseIdx() > ws.getWsStringData().getHoldParseMaxIdx())) {
						moveTableElements();
						ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
					}
				}
			}
			// COB_CODE: PERFORM 9475-BUILD-CO-OUTPUT
			//              THRU 9475-BUILD-CO-OUTPUT-EXIT
			buildCoOutput();
		}
		//************************************************************
		//    IF TYPE-COMPANY                                              00350
		//       IF AND-FOUND                                              00351
		//          SET PARSE-IDX TO 1                                     00352
		//          MOVE '0' TO WS-AND-SW                                  00353
		//          MOVE ZEROS TO HOLD-PARSE-IDX                           00354
		//          PERFORM 0400-REMOVE-AND THRU 0400-REMOVE-AND-EXIT      00355
		//                          UNTIL AND-FOUND                        00356
		//          PERFORM 0450-RESET-PARSED-TABLE THRU 0450-RESET-EXIT   00357
		//               VARYING PARSE-IDX FROM HOLD-PARSE-IDX BY 1        00358
		//                       UNTIL PARSE-IDX > 10                      00359
		//          PERFORM 9475-BUILD-CO-OUTPUT THRU 9475-CO-EXIT         00360
		//***       PERFORM 0475-BUILD-CO-OUTPUT THRU 0475-CO-EXIT         00360
		//       ELSE                                                      00361
		//          PERFORM 9475-BUILD-CO-OUTPUT THRU 9475-CO-EXIT.        00362
		//***       PERFORM 0475-BUILD-CO-OUTPUT THRU 0475-CO-EXIT.        00362
		//***************************************************************  00364
		// IF THE TYPE CODE IS EQUAL TO P (PERSON), THIS NEXT SET OF    *  00365
		// LOGIC WILL EXECUTE.                                          *  00366
		//                                                              *  00367
		// IF AN 'AND' IS FOUND AN 'F' INDICATOR IS SET TO TELL THE     *  00368
		//    CALLING PROGRAM THAT NAMESCRUB HAS CONFIRMED A CONJOINED  *  00369
		//    NAME SITUATION.                                           *  00370
		//                                                              *  00371
		// IF THE 'AND' IS IN THE SECOND WORD OF THE PARSED TABLE, LOGIC*  00372
		//    WILL BE EXECUTED TO VERIFY IF THERE IS ARE PREFIXES OR    *  00373
		//    SUFIXES IN THE FIRST OR SECOND NAME. IF THERE ARE THEY    *  00374
		//    ARE MOVED TO THEIR RESPECTIVE FORMAT OUT AREAS IN LINKAGE.*  00375
		//    IF PREFIXES MR. & MRS. ARE FOUND, LOGIC FOR HANDELING ALL *  00376
		//    THE WORDS ASSOCIATED WITH THE NAME IS INVOKED THUS MOVING *  00377
		//    THE WORDS FROM THE PARSED TABLE TO THEIR RESPECTIVE FORMAT*  00378
		//    OUT ARES IN LINKAGE. JIM AND MARY SMITH STRUCTURE IS ALSO *  00379
		//    HANDLED USING THE SAME LOGIC EXCLUDING THE PREFIX AND     *  00380
		//    SUFFIX LOGIC.                                             *  00381
		//                                                              *  00382
		// IF THE 'AND' IS NOT IN THE SECOND OCCURENCE OF THE PARSED    *  00383
		//    TABLE, AN ALL OTHER LOGIC IS PERFORMED. HERE, EACH WORD   *  00384
		//    IS INTERIGATED AND WHEN THE 'AND' IS FOUND THE LOGIC WILL *  00385
		//    STEP BACWARD THROUGH EACH WORD IN THE PARSED TABLE PRIOR  *  00386
		//    TO THE 'AND' AND DETERMINE THE LAST NAME, FIRST NAME,     *  00387
		//    PREFIX, SUFFIX, AND ALL POSSIBLE MIDDLE NAMES THEN MOVE   *  00388
		//    THEM TO THEIR RESPECTIVE FORMAT OUT AREAS IN LINKAGE.     *  00389
		//    FOR THE NAME AFTER THE 'AND' LOGIC THAT WILL SEARCH FOR-  *  00390
		//    WARD THROUGH THE REMAINING WORDS IN THE PARSED TABLE ARE  *  00391
		//    MOVED TO THEIR RESPECTIVE FORMAT OUT2 ARES IN LINKAGE.    *  00392
		//                                                              *  00393
		// IF THERE IS NO 'AND' IN THE NAME BUT THERE WAS A COMMA AFTER *  00394
		//    THE FIRST WORD, LOGIC THAT STEPS THROUGH EACH OCCURENCE   *  00395
		//    OF THE PARSED TABLE AND MOVES ALL THE WORDS TO THEIR      *  00396
		//    RESPECTIVE FORMAT OUT AREAS IN LINKAGE WILL EXECUTE.      *  00397
		//    NOTE THAT IN THIS CASE THE PARSE-IDX IS SET TO 2 BECAUSE  *  00398
		//    THE WORD BEFORE THE COMMA IS ASSUMED TO BE THE LAST NAME  *  00399
		//    AND HAS ALREADY BEEN MOVED TO ITS RESPECTIVE FORMAT OUT   *  00400
		//    AREA IN LINKAGE PRIOR TO THIS LOGIC. LOGIC FOR HANDELING  *  00401
		//    PREFIX IS IN THIS ROUTINE ALSO.                           *  00402
		//                                                              *  00403
		// IF THERE IS NO 'AND' THERE IS NOT A COMMA AFTER THE FIRST    *  00404
		//    WORD IN THE NAME, LOGIC THAT HANDLES A ONE PERSON NAME    *  00405
		//    IS INVOKED. HERE, BEGINNING WITHE THE FIRST WORD IN THE   *  00406
		//    PARSED TABLE EACH IS MOVED TO ITS RESPECTIVE FORMAT OUT   *  00407
		//    ARES IN LINKAGE.                                          *  00408
		//***************************************************************  00409
		//                                                                 00410
		//************************************************************     00410
		// COMMENTED OUT FORMER PROCESSING FOR AN INDIVIDUAL AND     *     00410
		// ADDED NEW PROCESSING SO A SECOND NAME IS NOT CREATED.     *     00410
		//************************************************************     00410
		//                                                                 00410
		// COB_CODE: IF TYPE-PERSON
		//                 THRU 0800-PROCESS-IND-NAME-EXIT
		//           END-IF.
		if (this.nameScrubRecord.getTypeCode().isPerson()) {
			// COB_CODE: PERFORM 0800-PROCESS-IND-NAME
			//              THRU 0800-PROCESS-IND-NAME-EXIT
			rng0800ProcessIndName();
		}
		//**************************************************************
		//** THE FOLLOWING CODE IS COMMENTED OUT BECAUSE FEDERATED DOES
		//** NOT HAVE TWO NAMES CREATED OUT OF ONE NAME. WE WILL ALLOW
		//** A CONJOINED NAME. (MARY AND JIM STONE)
		//**************************************************************
		//    IF TYPE-PERSON                                               00411
		//       IF AND-FOUND                                              00412
		//          MOVE 'F' TO RETURN-CONJOINED-PERSON                    00413
		//          IF WS-PARSED-NAME (2) = 'AND           ' OR            00414
		//                                  '&             ' OR            00415
		//                                  'OR            '               00416
		//             PERFORM 0500-LOC-PREFIX-SUFFIX                      00417
		//                                      THRU 0500-LOCATE-EXIT      00418
		//                  VARYING PARSE-IDX FROM 1 BY 1                  00419
		//                        UNTIL PARSE-IDX > 10                     00420
		//             IF NOT PREFIX-NOT-FOUND                             00421
		//                SET PARSE-IDX TO 1                               00422
		//                PERFORM 0525-MR-AND-MRS-LOGIC                    00423
		//                                THRU 0525-MR-AND-MRS-EXIT        00424
		//             ELSE                                                00425
		//                MOVE WS-PARSED-NAME (1) TO FORMAT-OUT-FIRST      00426
		//                SET PARSE-IDX TO 2                               00427
		//                PERFORM 0550-AND-2ND-POS                         00428
		//                                THRU 0550-2ND-POS-EXIT           00429
		//          ELSE                                                   00430
		//             SET PARSE-IDX TO 1                                  00431
		//             MOVE '1' TO WS-FIRST-TIME-SW                        00432
		//             MOVE '0' TO WS-AND-SW                               00433
		//             PERFORM 0575-ALL-OTHER-AND THRU 0575-ALL-OTHER-EXIT 00434
		//                            UNTIL AND-FOUND                      00435
		//             SET PARSE-IDX TO HOLD-PARSE-IDX                     00436
		//             SET PARSE-IDX UP BY 1                               00437
		//             IF WS-PARSED-TYPE-CODE (PARSE-IDX) = 'P'            00438
		//                MOVE WS-PARSED-NAME (PARSE-IDX)                  00439
		//                                   TO FORMAT-OUT-PREFIX2         00440
		//                SET PARSE-IDX UP BY 2                            00441
		//                IF WS-PARSED-NAME (PARSE-IDX) = SPACES           00442
		//                   SET PARSE-IDX DOWN BY 1                       00443
		//                   MOVE WS-PARSED-NAME (PARSE-IDX)               00444
		//                                   TO FORMAT-OUT-LAST2           00445
		//                                      FORMAT-OUT-LAST            00446
		//                   MOVE 'L' TO WS-PARSED-TYPE-CODE (PARSE-IDX)   00447
		//                ELSE                                             00448
		//                   IF WS-PARSED-TYPE-CODE (PARSE-IDX) = 'S'      00449
		//                      MOVE WS-PARSED-NAME (PARSE-IDX)            00450
		//                                  TO FORMAT-OUT-SUFFIX2          00451
		//                      SET PARSE-IDX DOWN BY 1                    00452
		//                      MOVE WS-PARSED-NAME (PARSE-IDX)            00453
		//                                  TO FORMAT-OUT-LAST2            00454
		//                                      FORMAT-OUT-LAST            00455
		//                   ELSE                                          00456
		//                      SET PARSE-IDX DOWN BY 1                    00457
		//                      MOVE WS-PARSED-NAME (PARSE-IDX)            00458
		//                                      TO FORMAT-OUT-FIRST2       00459
		//                      MOVE 'F' TO WS-PARSED-TYPE-CODE (PARSE-IDX)00460
		//COBOL                        MOVE 1 TO WS-AND-MID2-POINTER
		//                      PERFORM 0600-CONT-BUILD-2ND-NAME           00462
		//                                   THRU 0600-CONT-EXIT           00463
		//                           UNTIL PARSE-IDX > 10                  00464
		//             ELSE                                                00465
		//                MOVE WS-PARSED-NAME (PARSE-IDX)                  00466
		//                                   TO FORMAT-OUT-FIRST2          00467
		//                MOVE 'F' TO WS-PARSED-TYPE-CODE (PARSE-IDX)      00468
		//COBOL                  MOVE 1 TO WS-AND-MID2-POINTER
		//                PERFORM 0600-CONT-BUILD-2ND-NAME                 00469
		//                                   THRU 0600-CONT-EXIT           00470
		//                           UNTIL PARSE-IDX > 10                  00471
		//       ELSE                                                      00472
		//          IF COMMA-FOUND                                         00473
		//COBOL               MOVE 1 TO WS-MIDDLE-NAME-POINTER
		//             PERFORM 0625-REST-OF-NAME THRU 0625-REST-EXIT       00474
		//                  VARYING PARSE-IDX FROM 2 BY 1                  00475
		//                          UNTIL PARSE-IDX > 10                   00476
		//          ELSE                                                   00477
		//             IF WS-PARSED-TYPE-CODE (1) = 'P'                    00478
		//                PERFORM 0650-MOVE-PREFIX-SUFFIX                  00479
		//                                   THRU 0650-MOVE-EXIT           00480
		//                     VARYING PARSE-IDX FROM 1 BY 1               00481
		//                             UNTIL PARSE-IDX > 10                00482
		//ASMBLR                 MOVE +1 TO DGRSTRG-STRING-POINTER         00483
		//                IF TYPE-CODE-BLANK                               00484
		//COBOL                     MOVE 1 TO WS-MIDDLE-NAME-POINTER       00483
		//                   PERFORM 0675-MOVE-MIDDLE-NAME                 00485
		//                             THRU 0675-MID-NAME-EXIT             00486
		//                      VARYING PARSE-IDX FROM 1 BY 1              00487
		//                             UNTIL PARSE-IDX > 10                00488
		//                ELSE                                             00489
		//                   NEXT SENTENCE                                 00490
		//          ELSE                                                   00491
		//             MOVE WS-PARSED-NAME (1) TO FORMAT-OUT-FIRST         00492
		//             MOVE 'F' TO WS-PARSED-TYPE-CODE (1)                 00493
		//             PERFORM 0650-MOVE-PREFIX-SUFFIX THRU 0650-MOVE-EXIT 00494
		//                     VARYING PARSE-IDX FROM 2 BY 1               00495
		//                             UNTIL PARSE-IDX > 10                00496
		//ASMBLR              MOVE +1 TO DGRSTRG-STRING-POINTER            00497
		//             IF TYPE-CODE-BLANK                                  00498
		//COBOL                  MOVE 1 TO WS-MIDDLE-NAME-POINTER          00483
		//                PERFORM 0675-MOVE-MIDDLE-NAME                    00499
		//                             THRU 0675-MID-NAME-EXIT             00500
		//                   VARYING PARSE-IDX FROM 1 BY 1                 00501
		//                          UNTIL PARSE-IDX > 10.                  00502
		//***************************************************************  00504
		// THE NEXT SENTENCE WILL EXECUTE WHEN THERE IS MORE THAN ONE   *  00505
		// NAME IN MIDDLE NAME. HERE A WARNING MESSAGE IS SET IN THE    *  00506
		// ERROR TABLE IN LINKAGE.                                      *  00507
		//***************************************************************  00508
		// COB_CODE: IF WS-MID-NAME-COUNT > 1
		//              PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
		if (ws.getMiscellaneous().getWsMidNameCount() > 1) {
			// COB_CODE: MOVE +010 TO WS-ERROR-COUNT
			ws.getMiscellaneous().setWsErrorCount(((short) 10));
			// COB_CODE: MOVE 'M1M' TO WS-ERROR-CODE
			ws.getMiscellaneous().setWsErrorCode("M1M");
			// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
			buildError();
		}
		//***************************************************************  00515
		// THE NEXT SENTENCE WILL EXECUTE WHEN THERE IS ONLY ON WORD IN *  00516
		// THE NAME. HERE A WARNING MESSAGE IS SET IN THE ERROR TABLE IN*  00517
		// LINKAGE.                                                     *  00518
		//***************************************************************  00519
		// COB_CODE: IF WS-PARSED-NAME (2) = SPACES
		//              PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
		if (Characters.EQ_SPACE.test(ws.getWsParsedNameEntries(2).getName())) {
			// COB_CODE: MOVE +005 TO WS-ERROR-COUNT
			ws.getMiscellaneous().setWsErrorCount(((short) 5));
			// COB_CODE: MOVE 'ONM' TO WS-ERROR-CODE
			ws.getMiscellaneous().setWsErrorCode("ONM");
			// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
			buildError();
		}
		// COB_CODE: MOVE FORMAT-OUT-LAST  TO WS-NAME-WORK.
		ws.getWsNameWorkArea().getWsNameWork().setWsNameWork(this.nameScrubRecord.getNameOut().getFormatOutLast().getFormatOutLast());
		// COB_CODE: MOVE '0'              TO WS-FIRST-BLANK-SW.
		ws.getSwitches().setWsFirstBlankSwFormatted("0");
		// COB_CODE: SET WORK-IDX          TO 1.
		ws.setWorkIdx(1);
		// COB_CODE: PERFORM 0160-STRIP-BLANKS THRU 0160-STRIP-EXIT
		//                   VARYING NAME-OUT-INDEX FROM 1 BY 1
		//                             UNTIL NAME-OUT-INDEX > 60.
		ws.setNameOutIndex(1);
		while (!(ws.getNameOutIndex() > 60)) {
			stripBlanks1();
			ws.setNameOutIndex(Trunc.toInt(ws.getNameOutIndex() + 1, 9));
		}
		// COB_CODE: MOVE WS-NAME-WORK     TO FORMAT-OUT-LAST.
		this.nameScrubRecord.getNameOut().getFormatOutLast().setFormatOutLast(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork());
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Ciwbnsrb getInstance() {
		return (Programs.getInstance(Ciwbnsrb.class));
	}

	/**Original name: 0015-INITIALIZE<br>*/
	private void initialize() {
		// COB_CODE: MOVE SPACES       TO FORMAT-OUT-PREFIX
		//                                FORMAT-OUT-FIRST
		//                                FORMAT-OUT-MID
		//                                FORMAT-OUT-LAST
		//                                FORMAT-OUT-SUFFIX
		//                                FORMAT-OUT-PREFIX2
		//                                FORMAT-OUT-FIRST2
		//                                FORMAT-OUT-MID2
		//                                FORMAT-OUT-LAST2
		//                                FORMAT-OUT-SUFFIX2.
		nameScrubRecord.getNameOut().setFormatOutPrefix("");
		nameScrubRecord.getNameOut().setFormatOutFirst("");
		nameScrubRecord.getNameOut().setFormatOutMid("");
		nameScrubRecord.getNameOut().getFormatOutLast().setFormatOutLast("");
		nameScrubRecord.getNameOut().setFormatOutSuffix("");
		nameScrubRecord.getNameOut().setFormatOutPrefix2("");
		nameScrubRecord.getNameOut().setFormatOutFirst2("");
		nameScrubRecord.getNameOut().setFormatOutMid2("");
		nameScrubRecord.getNameOut().setFormatOutLast2("");
		nameScrubRecord.getNameOut().setFormatOutSuffix2("");
		// COB_CODE: MOVE 'Y'          TO FIRST-BYTE.
		ws.getMiscellaneous().setFirstByteFormatted("Y");
		// COB_CODE: MOVE FULL-NAME-IN TO WS-LONG-NAME.
		ws.getMiscellaneous().setWsLongName(nameScrubRecord.getFullNameIn().getFullNameIn());
		// COB_CODE: MOVE SPACES       TO WS-STRING-DATA
		//                                STRG-FORMAT-OUT-MID.
		ws.getWsStringData().initWsStringDataSpaces();
		ws.getWsStringData().setStrgFormatOutMid("");
		// COB_CODE: MOVE +1           TO DGRSTRG-STRING-POINTER.
		ws.getDgrstrgParmlist().setStringPointer(((short) 1));
		// COB_CODE: MOVE SPACES       TO WS-NAME-WORK
		//                                WS-LAST-NAME
		//                                WS-BYTE-TEST
		//                                RETURN-CONJOINED-PERSON
		//                                WS-PARSE-NAME-TABLE.
		ws.getWsNameWorkArea().getWsNameWork().setWsNameWork("");
		ws.getMiscellaneous().getWsLastName().setWsLastName("");
		ws.getMiscellaneous().setWsByteTest(Types.SPACE_CHAR);
		nameScrubRecord.getReturnConjoinedPerson().setReturnConjoinedPerson(Types.SPACE_CHAR);
		ws.initWsParseNameTableSpaces();
		// COB_CODE: MOVE ZERO         TO WS-MID-NAME-COUNT
		//                                HOLD-PARSE-IDX.
		ws.getMiscellaneous().setWsMidNameCount(((short) 0));
		ws.getMiscellaneous().setHoldParseIdx(((short) 0));
		// COB_CODE: MOVE '0'          TO WS-HYPHEN-SW
		//                                WS-INVALID-CHAR-SW
		//                                WS-FIRST-PREFIX-SW
		//                                WS-FIRST-SUFFIX-SW
		//                                WS-COMPANY-SW
		//                                WS-COMMA-SW
		//                                WS-PREFIX-SW
		//                                WS-AND-SW
		//                                WS-NAME-SW.
		ws.getSwitches().setWsHyphenSwFormatted("0");
		ws.getSwitches().setWsInvalidCharSwFormatted("0");
		ws.getSwitches().setWsFirstPrefixSwFormatted("0");
		ws.getSwitches().setWsFirstSuffixSwFormatted("0");
		ws.getSwitches().setWsCompanySwFormatted("0");
		ws.getSwitches().setWsCommaSwFormatted("0");
		ws.getSwitches().setWsPrefixSwFormatted("0");
		ws.getSwitches().setWsAndSwFormatted("0");
		ws.getSwitches().setWsNameSwFormatted("0");
		// COB_CODE: SET RETURN-IDX    TO 1.
		ws.setReturnIdx(1);
		//***************************************************************  00573
		// THE NEXT SENTENCE WILL EXECUTE WHEN THERE IS BLANK NAME PASS-*  00574
		// ED TO NAMESCRUB. HERE A WARNING MASSAGE IS SET IN THE ERROR  *  00575
		// TABLE IN LINKAGE AND IS RETURNED TO THE CALLING MODULE.      *  00576
		//***************************************************************  00577
		// COB_CODE: IF NAME-IN EQUAL SPACES
		//              GOBACK.
		if (Characters.EQ_SPACE.test(nameScrubRecord.getNameInBytes())) {
			// COB_CODE: MOVE 'E' TO RETURNED-ERROR-SW
			nameScrubRecord.getReturnedErrorSw().setReturnedErrorSwFormatted("E");
			// COB_CODE: MOVE +001 TO WS-ERROR-COUNT
			ws.getMiscellaneous().setWsErrorCount(((short) 1));
			// COB_CODE: MOVE 'BNM' TO WS-ERROR-CODE
			ws.getMiscellaneous().setWsErrorCode("BNM");
			// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT
			buildError();
			// COB_CODE: GOBACK.
			throw new ReturnException();
		}
	}

	/**Original name: 0100-STRIP-COMMAS-PERIODS<br>
	 * <pre>**************************************************************** 00589
	 *   0100-STRIP-COMMAS-PERIODS ROUTINE WILL CHECK EACH POSITION IN* 00590
	 *   THE 60 POSITION NAME-IN FIELD FOR VALID CHARACTERS.          * 00591
	 *                                                                * 00592
	 *   IF A HYPHEN IS FOUND WS-HYPHEN-SW IS TURNED ON. THIS SWITCH  * 00593
	 *      WILL BE USED IN LATER LOGIC WHEN DECIDING TO GENERATE A   * 00594
	 *      'HYPHEN FOUND IN NAME' WARNING MESSAGE.                   * 00595
	 *                                                                * 00596
	 *   IF A INVALID CHARACTER IS FOUND IT IS NOT MOVED TO THE WORK- * 00597
	 *      ING STORAGE FIELD THAT RETAINS ALL THE VALID CHARACTERS IN* 00598
	 *      THE NAME. THE WS-INVALID-CHAR-SW IS ALSO TURNED ON AND    * 00599
	 *      IS USED IN LATER LOGIC WHEN DECIDING TO GENERATE A        * 00600
	 *      'INVALID CHARACTER FOUND IN NAME' WARNING MESSAGE.        * 00601
	 *                                                                * 00602
	 *   IF A COMMA OR A PERIOD IS FOUND IT IS BYPASSED. OTHERWISE    * 00603
	 *      THE CHARACTER IS MOVED TO ANOTHER WORKING STOTAGE FIELD   * 00604
	 *      UNTIL ALL THE VALID CHARACTERS HAVE BEEN RETAINED THERE.  * 00605
	 *      AFTER ALL 60 POSITION  AVE BEEN CHECKED, THE WORKING      * 00606
	 *      STORAGE FIELD IS MOVED TO OVERLAY WHAT IS IN NAME-IN.     * 00607
	 * **************************************************************** 00608</pre>*/
	private void stripCommasPeriods() {
		// COB_CODE: MOVE NAME-IN-BYTE (NAME-INDEX) TO WS-BYTE-TEST.
		ws.getMiscellaneous().setWsByteTest(nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()));
		// COB_CODE: IF NAME-IN-BYTE (NAME-INDEX) = '-'
		//              MOVE '1' TO WS-HYPHEN-SW.
		if (nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()) == '-') {
			// COB_CODE: MOVE '1' TO WS-HYPHEN-SW.
			ws.getSwitches().setWsHyphenSwFormatted("1");
		}
		// COB_CODE: IF NOT GOOD-CHAR
		//              MOVE '1' TO WS-INVALID-CHAR-SW.
		if (!ws.getMiscellaneous().isGoodChar()) {
			// COB_CODE: MOVE '1' TO WS-INVALID-CHAR-SW.
			ws.getSwitches().setWsInvalidCharSwFormatted("1");
		}
		//    IF NAME-IN-BYTE (NAME-INDEX) = ','
		//       MOVE ' '          TO NAME-IN-BYTE (NAME-INDEX)
		//       MOVE NAME-IN-BYTE (NAME-INDEX)
		//                      TO WS-WORK-BYTE (WORK-IDX)
		//       SET WORK-IDX UP BY 1.
		//    IF (NAME-IN-BYTE (NAME-INDEX) = ',' OR '.')
		//       GO TO 0100-STRIP-EXIT
		//    ELSE
		//       MOVE NAME-IN-BYTE (NAME-INDEX)
		//                      TO WS-WORK-BYTE (WORK-IDX)
		//       SET WORK-IDX UP BY 1.
		// COB_CODE: IF NAME-IN-BYTE (NAME-INDEX) = ',' OR '.'
		//              NEXT SENTENCE
		//           ELSE
		//                             TO WS-WORK-BYTE (WORK-IDX).
		if (nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()) == ','
				|| nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()) == '.') {
			// COB_CODE: NEXT SENTENCE
			//next sentence
		} else {
			// COB_CODE: MOVE NAME-IN-BYTE (NAME-INDEX)
			//                          TO WS-WORK-BYTE (WORK-IDX).
			ws.getWsNameWorkArea().getWsNameWork().setWorkByte(ws.getWorkIdx(), nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()));
		}
		// COB_CODE: SET WORK-IDX UP BY 1.
		ws.setWorkIdx(Trunc.toInt(ws.getWorkIdx() + 1, 9));
	}

	/**Original name: 0150-STRIP-BLANKS<br>
	 * <pre>**************************************************************** 00635
	 *  0150-STRIP-BLANKS ROUTINE INSURES THAT ONLY ONE BLANK EXISTS  * 00636
	 *      BETWEEN WORDS IN THE NAME-IN FIELD. IF ADDITIONAL BLANKS  * 00637
	 *      ARE ENCOUNTERED THEY ARE OMITTED FROM THE NAME.           * 00638
	 * **************************************************************** 00639
	 * **  / IF THE FIRST, 2ND, 3RD ETC BYTE/S ARE BLANK REMOVE IT /    00642</pre>*/
	private void stripBlanks() {
		// COB_CODE: IF (NAME-IN-BYTE (NAME-INDEX) = ' ') AND FIRST-BYTE = 'Y'
		//                 GO TO 0150-STRIP-EXIT.
		if (nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()) == ' ' && ws.getMiscellaneous().getFirstByte() == 'Y') {
			// COB_CODE: GO TO 0150-STRIP-EXIT.
			return;
		}
		// COB_CODE: MOVE 'N'  TO FIRST-BYTE.
		ws.getMiscellaneous().setFirstByteFormatted("N");
		// COB_CODE: IF (NAME-IN-BYTE (NAME-INDEX) = ' ') AND NOT FIRST-BLANK
		//              SET WORK-IDX UP BY 1
		//           ELSE
		//                 SET WORK-IDX UP BY 1.
		if (nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()) == ' ' && !ws.getSwitches().isFirstBlank()) {
			// COB_CODE: MOVE NAME-IN-BYTE (NAME-INDEX)
			//                               TO WS-WORK-BYTE (WORK-IDX)
			ws.getWsNameWorkArea().getWsNameWork().setWorkByte(ws.getWorkIdx(), nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()));
			// COB_CODE: MOVE '1' TO WS-FIRST-BLANK-SW
			ws.getSwitches().setWsFirstBlankSwFormatted("1");
			// COB_CODE: SET WORK-IDX UP BY 1
			ws.setWorkIdx(Trunc.toInt(ws.getWorkIdx() + 1, 9));
		} else if (nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()) == ' ' && ws.getSwitches().isFirstBlank()) {
			// COB_CODE: IF (NAME-IN-BYTE (NAME-INDEX) = ' ') AND FIRST-BLANK
			//              GO TO 0150-STRIP-EXIT
			//           ELSE
			//              SET WORK-IDX UP BY 1.
			// COB_CODE: GO TO 0150-STRIP-EXIT
			return;
		} else {
			// COB_CODE: MOVE NAME-IN-BYTE (NAME-INDEX)
			//                             TO WS-WORK-BYTE (WORK-IDX)
			ws.getWsNameWorkArea().getWsNameWork().setWorkByte(ws.getWorkIdx(), nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()));
			// COB_CODE: MOVE '0' TO  WS-FIRST-BLANK-SW
			ws.getSwitches().setWsFirstBlankSwFormatted("0");
			// COB_CODE: SET WORK-IDX UP BY 1.
			ws.setWorkIdx(Trunc.toInt(ws.getWorkIdx() + 1, 9));
		}
	}

	/**Original name: 0160-STRIP-BLANKS<br>
	 * <pre>                                                                 00664</pre>*/
	private void stripBlanks1() {
		// COB_CODE: IF (FORMAT-OUT-IN-BYTE (NAME-OUT-INDEX) = ' ')
		//              AND NOT FIRST-BLANK
		//              SET WORK-IDX UP BY 1
		//           ELSE
		//                 SET WORK-IDX UP BY 1.
		if (nameScrubRecord.getNameOut().getFormatOutLast().getInByte(ws.getNameOutIndex()) == ' ' && !ws.getSwitches().isFirstBlank()) {
			// COB_CODE: MOVE FORMAT-OUT-IN-BYTE (NAME-OUT-INDEX)
			//                               TO WS-WORK-BYTE (WORK-IDX)
			ws.getWsNameWorkArea().getWsNameWork().setWorkByte(ws.getWorkIdx(),
					nameScrubRecord.getNameOut().getFormatOutLast().getInByte(ws.getNameOutIndex()));
			// COB_CODE: MOVE '1' TO WS-FIRST-BLANK-SW
			ws.getSwitches().setWsFirstBlankSwFormatted("1");
			// COB_CODE: SET WORK-IDX UP BY 1
			ws.setWorkIdx(Trunc.toInt(ws.getWorkIdx() + 1, 9));
		} else if (nameScrubRecord.getNameOut().getFormatOutLast().getInByte(ws.getNameOutIndex()) == ' ' && ws.getSwitches().isFirstBlank()) {
			// COB_CODE: IF (FORMAT-OUT-IN-BYTE (NAME-OUT-INDEX) = ' ')
			//              AND FIRST-BLANK
			//              GO TO 0160-STRIP-EXIT
			//           ELSE
			//              SET WORK-IDX UP BY 1.
			// COB_CODE: GO TO 0160-STRIP-EXIT
			return;
		} else {
			// COB_CODE: MOVE FORMAT-OUT-IN-BYTE (NAME-OUT-INDEX)
			//                             TO WS-WORK-BYTE (WORK-IDX)
			ws.getWsNameWorkArea().getWsNameWork().setWorkByte(ws.getWorkIdx(),
					nameScrubRecord.getNameOut().getFormatOutLast().getInByte(ws.getNameOutIndex()));
			// COB_CODE: MOVE '0' TO  WS-FIRST-BLANK-SW
			ws.getSwitches().setWsFirstBlankSwFormatted("0");
			// COB_CODE: SET WORK-IDX UP BY 1.
			ws.setWorkIdx(Trunc.toInt(ws.getWorkIdx() + 1, 9));
		}
	}

	/**Original name: 0200-PARSE-NAME-IN<br>
	 * <pre>**************************************************************** 00686
	 *  0200-PARSE-NAME-IN IS NAMESCRUBS PARSING ROUTINE. EACH WORD   * 00687
	 *      IN NAME-IN IS MOVED TO ITS OWN OCCURENCE WITHIN THE       * 00688
	 *      PARSED NAME TABLE.                                        * 00689
	 *                                                                * 00690
	 *  IF WHILE INDEXING THROUGH NAME-IN THE WORDS THE, FOR, TO, OF, * 00691
	 *     AN OR A ARE BUILT INTO WS-NAME-WORK, NONE WILL BE MOVED    * 00692
	 *     INTO THE PARSED NAME TABLE. THERE WILL, HOWEVER, BE A      * 00693
	 *     WARNING MESSAGE BUILT INTO THE ERROR TABLE IN LINKAGE.     * 00694
	 *                                                                * 00695
	 *  IF WHILE INDEXING THROUGH NAME-IN THE WORDS MAC, DI, DE, MC   * 00696
	 *     OR ST ARE BUILT INTO WS-NAME-WORK, THEY WILL BE STORED     * 00697
	 *     UNTIL THE NEXT WORD IS BUILT IN WS-NAME-WORK. WS-NAME-WORK * 00698
	 *     AND THE STORED NAME WILL BE STRUNG TOGETHER THEN MOVED TO  * 00699
	 *     ITS RESPECTIVE OCCURENCE WITHIN THE PARSED NAME TABLE.     * 00700
	 *                                                                * 00701
	 *  ALL OTHER WORDS BUILT BY THE INDEXING PROCESS WILL BE MOVED   * 00702
	 *     TO ITS RESPECTIVE OCCURENCE IN THE PARSED NAME TABLE.      * 00703
	 * **************************************************************** 00704</pre>*/
	private void parseNameIn() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NAME-INDEX = 60
		//              SET WORK-IDX UP BY 1.
		if (ws.getNameIndex() == 60) {
			// COB_CODE: MOVE NAME-IN-BYTE(NAME-INDEX) TO WS-WORK-BYTE(WORK-IDX)
			ws.getWsNameWorkArea().getWsNameWork().setWorkByte(ws.getWorkIdx(), nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()));
			// COB_CODE: SET WORK-IDX UP BY 1.
			ws.setWorkIdx(Trunc.toInt(ws.getWorkIdx() + 1, 9));
		}
		// COB_CODE: IF NAME-IN-BYTE (NAME-INDEX) = ' '
		//                  OR NAME-INDEX = 60
		//                          MOVE SPACES TO WS-NAME-WORK
		//           ELSE
		//              SET WORK-IDX UP BY 1.
		if (nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()) == ' ' || ws.getNameIndex() == 60) {
			// COB_CODE: SET WORK-IDX TO 1
			ws.setWorkIdx(1);
			// COB_CODE: ADD 1 TO WS-SPACE-COUNTER
			ws.getMiscellaneous().setWsSpaceCounter(Trunc.toShort(1 + ws.getMiscellaneous().getWsSpaceCounter(), 1));
			// COB_CODE: IF WS-NAME-WORK = 'THE           ' OR
			//                             'FOR           ' OR
			//                             'TO            ' OR
			//                             'OF            ' OR
			//                             'AN            '
			//              MOVE 0 TO WS-SPACE-COUNTER
			//           ELSE
			//                       MOVE SPACES TO WS-NAME-WORK
			if (Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "THE           ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "FOR           ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "TO            ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "OF            ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "AN            ")) {
				// COB_CODE: MOVE +009 TO WS-ERROR-COUNT
				ws.getMiscellaneous().setWsErrorCount(((short) 9));
				// COB_CODE: MOVE 'SUP' TO WS-ERROR-CODE
				ws.getMiscellaneous().setWsErrorCode("SUP");
				// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT
				buildError();
				// COB_CODE: MOVE SPACE TO WS-NAME-WORK
				ws.getWsNameWorkArea().getWsNameWork().setWsNameWork("");
				// COB_CODE: MOVE 0 TO WS-SPACE-COUNTER
				ws.getMiscellaneous().setWsSpaceCounter(((short) 0));
			} else if (Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "MAC          ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "DI           ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "DE           ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "MC           ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "ST           ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "LA           ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "VAN          ")
					|| Conditions.eq(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork(), "VON          ")) {
				//                            'ST           '                      00726
				//** ADDED 3 PREFIXES THAT ARE IN THE FEDERATED VERSION ******
				//** OF CIWBNSRB *********************************************
				// COB_CODE:            IF WS-NAME-WORK = 'MAC          ' OR
				//                                        'DI           ' OR
				//                                        'DE           ' OR
				//                                        'MC           ' OR
				//           *                            'ST           '
				//                                        'ST           ' OR
				//           *** ADDED 3 PREFIXES THAT ARE IN THE FEDERATED VERSION ******
				//           *** OF CIWBNSRB *********************************************
				//                                        'LA           ' OR
				//                                        'VAN          ' OR
				//                                        'VON          '
				//           *************************************************************
				//           *********************************************************
				//           * ALLOW MULTIPLE OF THE ABOVE PREFIXES                  *
				//           * THIS CODE WAS ADDED FROM THE FED VERSION OF CIWBNSRB  *
				//           *********************************************************
				//           *** AT THIS POINT THE CODE DOES NOT MATCH THE WORKSTATION
				//           *** MODULE (CIWONSRB). A SEMI-COLON IS BEING STRUNG INTO
				//           *** THE LAST NAME BETWEEN THE PREFIXES LISTED ABOVE AND
				//           *** CHANGED TO SPACES SO THERE WILL BE SPACES IN THE LAST
				//           *** NAME PORTION OF THE LONG NAME.
				//           *********************************************************
				//                         MOVE 0 TO WS-SPACE-COUNTER
				//                      ELSE
				//                               MOVE SPACES TO WS-NAME-WORK
				//************************************************************
				//********************************************************
				// ALLOW MULTIPLE OF THE ABOVE PREFIXES                  *
				// THIS CODE WAS ADDED FROM THE FED VERSION OF CIWBNSRB  *
				//********************************************************
				//** AT THIS POINT THE CODE DOES NOT MATCH THE WORKSTATION
				//** MODULE (CIWONSRB). A SEMI-COLON IS BEING STRUNG INTO
				//** THE LAST NAME BETWEEN THE PREFIXES LISTED ABOVE AND
				//** CHANGED TO SPACES SO THERE WILL BE SPACES IN THE LAST
				//** NAME PORTION OF THE LONG NAME.
				//********************************************************
				// COB_CODE: IF START-NAME-FOUND
				//                                 WS-NAME-WORK
				//           ELSE
				//               MOVE '1' TO WS-START-NAME-SW
				//           END-IF
				if (ws.getSwitches().isStartNameFound()) {
					// COB_CODE: MOVE WS-NAME-WORK
					//                           TO WS-HOLD-NAME-WORK2
					ws.getMiscellaneous().setWsHoldNameWork2(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork());
					// COB_CODE: IF TYPE-COMPANY
					//                     INTO WS-HOLD-NAME-WORK3
					//           ELSE
					//                     INTO WS-HOLD-NAME-WORK3
					//           END-IF
					if (nameScrubRecord.getTypeCode().isCompany()) {
						// COB_CODE: STRING WS-HOLD-NAME-WORK1
						//                  WS-HOLD-NAME-WORK2
						//                  DELIMITED BY ' '
						//                  INTO WS-HOLD-NAME-WORK3
						concatUtil = ConcatUtil.buildString(Miscellaneous.Len.WS_HOLD_NAME_WORK3,
								Functions.substringBefore(ws.getMiscellaneous().getWsHoldNameWork1Formatted(), " "),
								Functions.substringBefore(ws.getMiscellaneous().getWsHoldNameWork2Formatted(), " "));
						ws.getMiscellaneous().setWsHoldNameWork3(concatUtil.replaceInString(ws.getMiscellaneous().getWsHoldNameWork3Formatted()));
					} else {
						// COB_CODE: STRING WS-HOLD-NAME-WORK1
						//                  CF-NAME-DELIMITER
						//                  WS-HOLD-NAME-WORK2
						//                  DELIMITED BY ' '
						//                  INTO WS-HOLD-NAME-WORK3
						concatUtil = ConcatUtil.buildString(Miscellaneous.Len.WS_HOLD_NAME_WORK3,
								Functions.substringBefore(ws.getMiscellaneous().getWsHoldNameWork1Formatted(), " "),
								Functions.substringBefore(String.valueOf(ws.getWsStringData().getCfNameDelimiter()), " "),
								Functions.substringBefore(ws.getMiscellaneous().getWsHoldNameWork2Formatted(), " "));
						ws.getMiscellaneous().setWsHoldNameWork3(concatUtil.replaceInString(ws.getMiscellaneous().getWsHoldNameWork3Formatted()));
					}
					// COB_CODE: MOVE WS-HOLD-NAME-WORK3
					//                           TO WS-HOLD-NAME-WORK1
					ws.getMiscellaneous().setWsHoldNameWork1(ws.getMiscellaneous().getWsHoldNameWork3());
					// COB_CODE: MOVE SPACES     TO WS-HOLD-NAME-WORK2
					//                              WS-HOLD-NAME-WORK3
					//                              WS-NAME-WORK
					ws.getMiscellaneous().setWsHoldNameWork2("");
					ws.getMiscellaneous().setWsHoldNameWork3("");
					ws.getWsNameWorkArea().getWsNameWork().setWsNameWork("");
				} else {
					// COB_CODE: MOVE WS-NAME-WORK TO WS-HOLD-NAME-WORK1
					ws.getMiscellaneous().setWsHoldNameWork1(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork());
					// COB_CODE: MOVE SPACE TO WS-NAME-WORK
					ws.getWsNameWorkArea().getWsNameWork().setWsNameWork("");
					// COB_CODE: MOVE '1' TO WS-START-NAME-SW
					ws.getSwitches().setWsStartNameSwFormatted("1");
				}
				// COB_CODE: MOVE 0 TO WS-SPACE-COUNTER
				ws.getMiscellaneous().setWsSpaceCounter(((short) 0));
			} else if (ws.getMiscellaneous().getWsSpaceCounter() > 1) {
				// COB_CODE: IF WS-SPACE-COUNTER > 1
				//              GO TO 0200-PARSE-EXIT
				//           ELSE
				//                 MOVE SPACES TO WS-NAME-WORK
				// COB_CODE: SET NAME-INDEX TO 60
				ws.setNameIndex(60);
				// COB_CODE: GO TO 0200-PARSE-EXIT
				return;
			} else if (ws.getSwitches().isStartNameFound()) {
				// COB_CODE: IF START-NAME-FOUND
				//              SET PARSE-IDX UP BY 1
				//           ELSE
				//              MOVE SPACES TO WS-NAME-WORK
				// COB_CODE: MOVE WS-NAME-WORK TO WS-HOLD-NAME-WORK2
				ws.getMiscellaneous().setWsHoldNameWork2(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork());
				// COB_CODE: MOVE '0200' TO DEBUG-PARAGRAPH
				ws.getMiscellaneous().setDebugParagraph("0200");
				//*****              PERFORM 0800-STRING-NAME THRU 0800-EXIT       00739
				//100021*                 PERFORM 9800-STRING-NAME THRU 9800-EXIT  00739
				//**************************************************************
				//** THE FOLLOWING CODE IS FROM THE FEDERATED VERSION OF
				//** CIWBNSRB. IT PREVENTS SPACES FROM BEING ELIMINATED FROM
				//** BETWEEN MULTIPLE NAMES IN A PERSONAL LAST NAME.
				//** VON LAST SHOULD NOT BE VONLAST
				//**************************************************************
				// COB_CODE: IF TYPE-COMPANY
				//                 THRU 9800-STRING-NAME-EXIT
				//           ELSE
				//                        TO         SPACE
				//           END-IF
				if (nameScrubRecord.getTypeCode().isCompany()) {
					// COB_CODE: PERFORM 9800-STRING-NAME
					//              THRU 9800-STRING-NAME-EXIT
					stringName();
				} else {
					// COB_CODE: STRING WS-HOLD-NAME-WORK1
					//                  CF-NAME-DELIMITER
					//                  WS-HOLD-NAME-WORK2
					//                  DELIMITED BY ' '
					//                  INTO WS-HOLD-NAME-WORK3
					concatUtil = ConcatUtil.buildString(Miscellaneous.Len.WS_HOLD_NAME_WORK3,
							Functions.substringBefore(ws.getMiscellaneous().getWsHoldNameWork1Formatted(), " "),
							Functions.substringBefore(String.valueOf(ws.getWsStringData().getCfNameDelimiter()), " "),
							Functions.substringBefore(ws.getMiscellaneous().getWsHoldNameWork2Formatted(), " "));
					ws.getMiscellaneous().setWsHoldNameWork3(concatUtil.replaceInString(ws.getMiscellaneous().getWsHoldNameWork3Formatted()));
					// COB_CODE: INSPECT WS-HOLD-NAME-WORK3
					//                     CONVERTING CF-NAME-DELIMITER
					//                     TO         SPACE
					ws.getMiscellaneous().setWsHoldNameWork3(InspectUtil.convert(ws.getMiscellaneous().getWsHoldNameWork3Formatted(),
							String.valueOf(ws.getWsStringData().getCfNameDelimiter()), Types.SPACE_STRING));
				}
				// COB_CODE: MOVE WS-HOLD-NAME-WORK3
				//                       TO WS-PARSED-NAME (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getMiscellaneous().getWsHoldNameWork3());
				// COB_CODE: MOVE SPACES TO WS-NAME-WORK
				ws.getWsNameWorkArea().getWsNameWork().setWsNameWork("");
				// COB_CODE: MOVE SPACES TO WS-HOLD-NAME-WORK1
				ws.getMiscellaneous().setWsHoldNameWork1("");
				// COB_CODE: MOVE SPACES TO WS-HOLD-NAME-WORK2
				ws.getMiscellaneous().setWsHoldNameWork2("");
				// COB_CODE: MOVE SPACES TO WS-HOLD-NAME-WORK3
				ws.getMiscellaneous().setWsHoldNameWork3("");
				// COB_CODE: MOVE '0' TO WS-START-NAME-SW
				ws.getSwitches().setWsStartNameSwFormatted("0");
				// COB_CODE: MOVE 0 TO WS-SPACE-COUNTER
				ws.getMiscellaneous().setWsSpaceCounter(((short) 0));
				// COB_CODE: SET PARSE-IDX UP BY 1
				ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
			} else {
				// COB_CODE: MOVE WS-NAME-WORK
				//                           TO WS-PARSED-NAME (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getWsNameWorkArea().getWsNameWork().getWsNameWork());
				// COB_CODE: SET PARSE-IDX UP BY 1
				ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
				// COB_CODE: MOVE 0 TO WS-SPACE-COUNTER
				ws.getMiscellaneous().setWsSpaceCounter(((short) 0));
				// COB_CODE: MOVE SPACES TO WS-NAME-WORK
				ws.getWsNameWorkArea().getWsNameWork().setWsNameWork("");
			}
		} else {
			// COB_CODE: MOVE NAME-IN-BYTE (NAME-INDEX)
			//                               TO WS-WORK-BYTE (WORK-IDX)
			ws.getWsNameWorkArea().getWsNameWork().setWorkByte(ws.getWorkIdx(), nameScrubRecord.getFullNameIn().getNameInByte(ws.getNameIndex()));
			// COB_CODE: SET WORK-IDX UP BY 1.
			ws.setWorkIdx(Trunc.toInt(ws.getWorkIdx() + 1, 9));
		}
	}

	/**Original name: 0300-DETERMINE-PREFIX-SUFFIX<br>
	 * <pre>**************************************************************** 00763
	 *  0300-DETERMINE-PREFIX-SUFFIX WHEN EXECUTED WILL CHECK EACH    * 00764
	 *      OCCURENCE IN THE PARSED NAME TABLE WITH THE COMPANY,      * 00765
	 *      PREFIX OR SUFFIX TABLES FOR STANDARDIZATION PURPOSES.     * 00766
	 *                                                                * 00767
	 *  EACH OCCURENCE FIELD WILL BE USED TO SEARCH A SET OF STANDARDS* 00768
	 *       TABLES TO INSURE CONSISTENCY FOR COMPANY NAMES, PREFIX   * 00769
	 *       NAMES AND SUFFIX NAMES. IF A MATCH IS FOUND WHILE SEARCH-* 00770
	 *       ING THE STANDARDS TABLE THE RESULT WILL REPLACE THE VALUE* 00771
	 *       WITHIN THE PARSED NAME TABLE OCCURENCE. THE PARSED TYPE  * 00772
	 *       CODE WILL ALSO BE UPDATED WITH ITS RELATED VALUE FROM THE* 00773
	 *       STANDARDS TABLE. IN LATER LOGIC WHILE INDEXING THROUGH   * 00774
	 *       THE PARSED NAME TABLE THE UPDATE TYPE CODES WILL BE USED * 00775
	 *       TO HELP DETERMINE WHICH OF THE OCCURENCES ARE PREFIXES OR* 00776
	 *       SUFIXES SO THAT THESE FIELDS CAN BE MOVED TO THE PROPER  * 00777
	 *       OUTPUT AREAS IN LINKAGE.                                 * 00778
	 *                                                                * 00779
	 *       THE PARSED NAME TYPE CODE WILL CONTAIN '&' WHEN ANY 'AND/* 00780
	 *       OR' REPRESENTATION IS FOUND IN THE TABLE SEARCH.         * 00781
	 *                                                                * 00782
	 *       THE PARSED NAME TYPE CODE WHEN UPDATED FOR ANY COMPANY   * 00783
	 *       RESENTATIONS WILL BE 'C' AND WILL OVERRIDE ANY 'P' PREFIX* 00784
	 *       OR 'S' SUFFIX VALUED OCCURENCES WHEN THE LINKAGE TYPE-   * 00785
	 *       CODE SENT BY THE CALLING MODULE IS ' '.                  * 00786
	 * **************************************************************** 00787</pre>*/
	private void determinePrefixSuffix() {
		boolean endOfTable = false;
		// COB_CODE: SET X TO 1.
		ws.setX(1);
		// COB_CODE: MOVE '0' TO WS-NAME-SW.
		ws.getSwitches().setWsNameSwFormatted("0");
		//    SEARCH ALL TESTFLD  AT END MOVE '0' TO WS-NAME-SW            00793
		// COB_CODE:      SEARCH TESTFLD  AT END MOVE '0' TO WS-NAME-SW
		//                       WHEN TEST-VAL (X) EQUAL WS-PARSED-NAME (PARSE-IDX)
		//           *           WHEN WS-PARSED-NAME (PARSE-IDX) EQUAL TEST-VAL (X)
		//                            MOVE '1' TO WS-NAME-SW.
		//                SEARCH TESTFLD  AT END MOVE '0' TO WS-NAME-SW
		//                       WHEN TEST-VAL (X) EQUAL WS-PARSED-NAME (PARSE-IDX)
		//           *           WHEN WS-PARSED-NAME (PARSE-IDX) EQUAL TEST-VAL (X)
		//                            MOVE '1' TO WS-NAME-SW.
		endOfTable = true;
		while (ws.getX() <= 411) {
			if (Conditions.eq(ws.getCisltnam().getTableName().getTestVal(ws.getX()), ws.getWsParsedNameEntries(ws.getParseIdx()).getName())) {
				endOfTable = false;
				//           WHEN WS-PARSED-NAME (PARSE-IDX) EQUAL TEST-VAL (X)    00795
				// COB_CODE: MOVE '1' TO WS-NAME-SW.
				ws.getSwitches().setWsNameSwFormatted("1");
				break;
			}
			ws.setX(Trunc.toInt(ws.getX() + 1, 9));
		}
		//AT END GROUP;
		if (endOfTable) {
			// COB_CODE: SEARCH TESTFLD  AT END MOVE '0' TO WS-NAME-SW
			ws.getSwitches().setWsNameSwFormatted("0");
		}
		// COB_CODE: IF NOT NAME-FOUND
		//              GO TO 0300-PREFIX-SUFFIX-EXIT.
		if (!ws.getSwitches().isNameFound()) {
			// COB_CODE: GO TO 0300-PREFIX-SUFFIX-EXIT.
			return;
		}
		//*****************************************************************00801
		//* IF TYPE-NAME AS SUBMITTED TO NAME SCRUB = PERSON AND THE   ****00802
		//* PERSONAL NAME CONTAINS A COMPANY KEY WORD LIKE STORE TREAT ****00803
		//* 'STORE' AS JUST ANOTHER WORD IN A PERSONAL NAME AND DON'T  ****00804
		//* GET CONFUSED BY THINKING THIS IS A COMPANY NAME.           ****00805
		//*****************************************************************00806
		// COB_CODE: IF NAME-FOUND AND
		//              TYPE-PERSON AND
		//              RET-TYPE (X) = 'C'
		//              GO TO 0300-PREFIX-SUFFIX-EXIT.
		if (ws.getSwitches().isNameFound() && nameScrubRecord.getTypeCode().isPerson()
				&& ws.getCisltnam().getTableName().getRetType(ws.getX()) == 'C') {
			// COB_CODE: GO TO 0300-PREFIX-SUFFIX-EXIT.
			return;
		}
		//*****************************************************************00801
		//* IF TYPE-NAME AS SUBMITTED TO NAME SCRUB = COMPANY AND THE  ****00802
		//* COMPANY NAME CONTAINS A PREFIX KEY WORD LIKE BROTHER,      ****00803
		//* TREAT 'BROTHER' AS JUST ANOTHER WORD IN A COMPANY NAME AND ****00804
		//* DON'T GET CONFUSED BY THINKING THIS IS A PERSONAL NAME.    ****00805
		//* IN THE SAME MANNER AS THE ABOVE, IF A COMPANY CONTAINS A   ****
		//* SUFFIX KEY WORD LIKE FIFTH, TREAT 'FIFTH' AS ANOTHER WORD  ****
		//* IN A COMPANY NAME.                                         ****
		//*****************************************************************00806
		// COB_CODE: IF NAME-FOUND AND
		//              TYPE-COMPANY
		//                    GO TO 0300-PREFIX-SUFFIX-EXIT.
		if (ws.getSwitches().isNameFound() && nameScrubRecord.getTypeCode().isCompany()) {
			// COB_CODE: IF RET-TYPE (X) = 'P'
			//              GO TO 0300-PREFIX-SUFFIX-EXIT
			//           ELSE
			//                 GO TO 0300-PREFIX-SUFFIX-EXIT.
			if (ws.getCisltnam().getTableName().getRetType(ws.getX()) == 'P') {
				// COB_CODE: GO TO 0300-PREFIX-SUFFIX-EXIT
				return;
			} else if (ws.getCisltnam().getTableName().getRetType(ws.getX()) == 'S') {
				// COB_CODE: IF RET-TYPE (X) = 'S'
				//              GO TO 0300-PREFIX-SUFFIX-EXIT.
				// COB_CODE: GO TO 0300-PREFIX-SUFFIX-EXIT.
				return;
			}
		}
		//                                                                 00810
		//042026 TYPE-COMPANY AND                                          00808
		//042026 RET-TYPE (X) = 'P'                                        00809
		//042026 GO TO 0300-PREFIX-SUFFIX-EXIT.                            00810
		// COB_CODE: IF NAME-FOUND AND RET-TYPE (X) = '&'
		//              MOVE RET-TYPE (X) TO WS-PARSED-TYPE-CODE (PARSE-IDX).
		if (ws.getSwitches().isNameFound() && ws.getCisltnam().getTableName().getRetType(ws.getX()) == '&') {
			// COB_CODE: MOVE '1' TO WS-AND-SW
			ws.getSwitches().setWsAndSwFormatted("1");
			// COB_CODE: MOVE RET-TYPE (X) TO WS-PARSED-TYPE-CODE (PARSE-IDX).
			ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
		}
		// COB_CODE: IF NAME-FOUND AND RET-TYPE (X) = 'C'
		//                 MOVE COMPANY-STD (CX) TO WS-PARSED-NAME (PARSE-IDX).
		if (ws.getSwitches().isNameFound() && ws.getCisltnam().getTableName().getRetType(ws.getX()) == 'C') {
			// COB_CODE: IF RET-INDX (X) = ZEROS
			//              MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
			//           ELSE
			//              MOVE COMPANY-STD (CX) TO WS-PARSED-NAME (PARSE-IDX).
			if (Characters.EQ_ZERO.test(ws.getCisltnam().getTableName().getRetIndxFormatted(ws.getX()))) {
				// COB_CODE: MOVE '1' TO WS-COMPANY-SW
				ws.getSwitches().setWsCompanySwFormatted("1");
				// COB_CODE: MOVE RET-TYPE (X) TO WS-PARSED-TYPE-CODE (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
				// COB_CODE: MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getTableName().getTestVal(ws.getX()));
			} else {
				// COB_CODE: MOVE '1' TO WS-COMPANY-SW
				ws.getSwitches().setWsCompanySwFormatted("1");
				// COB_CODE: MOVE RET-TYPE (X) TO WS-PARSED-TYPE-CODE (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
				// COB_CODE: SET CX TO RET-INDX (X)
				ws.setCx(ws.getCisltnam().getTableName().getRetIndx(ws.getX()));
				// COB_CODE: MOVE COMPANY-STD (CX) TO WS-PARSED-NAME (PARSE-IDX).
				ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getBusinessTable().getCompanyStd(ws.getCx()));
			}
		}
		// COB_CODE: IF NAME-FOUND AND RET-TYPE (X) = 'P'
		//                         MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX).
		if (ws.getSwitches().isNameFound() && ws.getCisltnam().getTableName().getRetType(ws.getX()) == 'P') {
			// COB_CODE: IF RET-INDX (X) = ZEROS
			//                       MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX)
			//           ELSE
			//                      MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX).
			if (Characters.EQ_ZERO.test(ws.getCisltnam().getTableName().getRetIndxFormatted(ws.getX()))) {
				// COB_CODE: IF PARSE-IDX = 1
				//              MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
				//           ELSE
				//                    MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX)
				if (ws.getParseIdx() == 1) {
					// COB_CODE: MOVE RET-TYPE (X)
					//                          TO WS-PARSED-TYPE-CODE (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
					// COB_CODE: MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getTableName().getTestVal(ws.getX()));
				} else if (ws.getSwitches().isAndFound()) {
					// COB_CODE: IF AND-FOUND
					//              MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
					//           ELSE
					//                 MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX)
					// COB_CODE: MOVE RET-TYPE (X)
					//                       TO WS-PARSED-TYPE-CODE (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
					// COB_CODE: MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getTableName().getTestVal(ws.getX()));
				} else if (ws.getParseIdx() == 2 && ws.getSwitches().isCommaFound()) {
					// COB_CODE: IF PARSE-IDX = 2 AND COMMA-FOUND
					//                          TO WS-PARSED-NAME (PARSE-IDX)
					//           ELSE
					//              MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX)
					// COB_CODE: MOVE RET-TYPE (X)
					//                    TO WS-PARSED-TYPE-CODE (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
					// COB_CODE: MOVE TEST-VAL (X)
					//                       TO WS-PARSED-NAME (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getTableName().getTestVal(ws.getX()));
				} else {
					// COB_CODE: MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(Types.SPACE_CHAR);
				}
			} else if (ws.getParseIdx() == 1) {
				// COB_CODE: IF PARSE-IDX = 1
				//              MOVE PREFIX-STD (PX) TO WS-PARSED-NAME (PARSE-IDX)
				//           ELSE
				//                   MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX).
				// COB_CODE: MOVE RET-TYPE (X)
				//                       TO WS-PARSED-TYPE-CODE (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
				// COB_CODE: SET PX TO RET-INDX (X)
				ws.setPx(ws.getCisltnam().getTableName().getRetIndx(ws.getX()));
				// COB_CODE: MOVE PREFIX-STD (PX) TO WS-PARSED-NAME (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getPrefixTable().getPrefixStd(ws.getPx()));
			} else if (ws.getSwitches().isAndFound()) {
				// COB_CODE: IF AND-FOUND
				//              MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
				//           ELSE
				//                MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX).
				// COB_CODE: MOVE RET-TYPE (X)
				//                       TO WS-PARSED-TYPE-CODE (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
				// COB_CODE: MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getTableName().getTestVal(ws.getX()));
			} else if (ws.getParseIdx() == 2 && ws.getSwitches().isCommaFound()) {
				// COB_CODE: IF PARSE-IDX = 2 AND COMMA-FOUND
				//                             TO WS-PARSED-NAME (PARSE-IDX)
				//           ELSE
				//             MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX).
				// COB_CODE: MOVE RET-TYPE (X)
				//                    TO WS-PARSED-TYPE-CODE (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
				// COB_CODE: SET PX TO RET-INDX (X)
				ws.setPx(ws.getCisltnam().getTableName().getRetIndx(ws.getX()));
				// COB_CODE: MOVE PREFIX-STD (PX)
				//                          TO WS-PARSED-NAME (PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getPrefixTable().getPrefixStd(ws.getPx()));
			} else {
				// COB_CODE: MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX).
				ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(Types.SPACE_CHAR);
			}
		}
		// COB_CODE: IF NAME-FOUND AND RET-TYPE (X) = 'S'
		//                    MOVE SUFFIX-STD (SX) TO WS-PARSED-NAME (PARSE-IDX).
		if (ws.getSwitches().isNameFound() && ws.getCisltnam().getTableName().getRetType(ws.getX()) == 'S') {
			// COB_CODE:    IF PARSE-IDX < 10
			//                       SET PARSE-IDX DOWN BY 1
			//           ELSE
			//                    MOVE SUFFIX-STD (SX) TO WS-PARSED-NAME (PARSE-IDX).
			if (ws.getParseIdx() < 10) {
				// COB_CODE: SET PARSE-IDX UP BY 1
				ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
				// COB_CODE: IF WS-PARSED-NAME (PARSE-IDX) = SPACES
				//                                   TO WS-PARSED-NAME (PARSE-IDX)
				//           ELSE
				//                 SET PARSE-IDX DOWN BY 1
				if (Characters.EQ_SPACE.test(ws.getWsParsedNameEntries(ws.getParseIdx()).getName())) {
					// COB_CODE: SET PARSE-IDX DOWN BY 1
					ws.setParseIdx(Trunc.toInt(ws.getParseIdx() - 1, 9));
					// COB_CODE: IF RET-INDX (X) = ZEROS
					//              MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
					//           ELSE
					//                                TO WS-PARSED-NAME (PARSE-IDX)
					if (Characters.EQ_ZERO.test(ws.getCisltnam().getTableName().getRetIndxFormatted(ws.getX()))) {
						// COB_CODE: MOVE RET-TYPE (X)
						//                       TO WS-PARSED-TYPE-CODE (PARSE-IDX)
						ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
						// COB_CODE: MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
						ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getTableName().getTestVal(ws.getX()));
					} else {
						// COB_CODE: MOVE RET-TYPE (X)
						//                       TO WS-PARSED-TYPE-CODE (PARSE-IDX)
						ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
						// COB_CODE: SET SX TO RET-INDX (X)
						ws.setSx(ws.getCisltnam().getTableName().getRetIndx(ws.getX()));
						// COB_CODE: MOVE SUFFIX-STD (SX)
						//                             TO WS-PARSED-NAME (PARSE-IDX)
						ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getSuffixTable().getSuffixStd(ws.getSx()));
					}
				} else if (Conditions.eq(ws.getWsParsedNameEntries(ws.getParseIdx()).getName(), "AND           ")
						|| Conditions.eq(ws.getWsParsedNameEntries(ws.getParseIdx()).getName(), "&             ")
						|| Conditions.eq(ws.getWsParsedNameEntries(ws.getParseIdx()).getName(), "OR            ")) {
					// COB_CODE: IF WS-PARSED-NAME (PARSE-IDX) = 'AND           ' OR
					//                                           '&             ' OR
					//                                           'OR            '
					//                                 TO WS-PARSED-NAME (PARSE-IDX)
					//           ELSE
					//              SET PARSE-IDX DOWN BY 1
					// COB_CODE: SET PARSE-IDX DOWN BY 1
					ws.setParseIdx(Trunc.toInt(ws.getParseIdx() - 1, 9));
					// COB_CODE: IF RET-INDX (X) = ZEROS
					//                       TO WS-PARSED-NAME (PARSE-IDX)
					//           ELSE
					//                              TO WS-PARSED-NAME (PARSE-IDX)
					if (Characters.EQ_ZERO.test(ws.getCisltnam().getTableName().getRetIndxFormatted(ws.getX()))) {
						// COB_CODE: MOVE RET-TYPE (X)
						//                    TO WS-PARSED-TYPE-CODE (PARSE-IDX)
						ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
						// COB_CODE: MOVE TEST-VAL (X)
						//                    TO WS-PARSED-NAME (PARSE-IDX)
						ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getTableName().getTestVal(ws.getX()));
					} else {
						// COB_CODE: MOVE RET-TYPE (X)
						//                    TO WS-PARSED-TYPE-CODE (PARSE-IDX)
						ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
						// COB_CODE: SET SX TO RET-INDX (X)
						ws.setSx(ws.getCisltnam().getTableName().getRetIndx(ws.getX()));
						// COB_CODE: MOVE SUFFIX-STD (SX)
						//                           TO WS-PARSED-NAME (PARSE-IDX)
						ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getSuffixTable().getSuffixStd(ws.getSx()));
					}
				} else {
					// COB_CODE: MOVE SPACE TO WS-PARSED-TYPE-CODE (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(Types.SPACE_CHAR);
					// COB_CODE: SET PARSE-IDX DOWN BY 1
					ws.setParseIdx(Trunc.toInt(ws.getParseIdx() - 1, 9));
				}
			} else if (ws.getParseIdx() == 10) {
				// COB_CODE: IF PARSE-IDX = 10
				//                 MOVE SUFFIX-STD (SX) TO WS-PARSED-NAME (PARSE-IDX).
				// COB_CODE: IF RET-INDX (X) = ZEROS
				//              MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
				//           ELSE
				//              MOVE SUFFIX-STD (SX) TO WS-PARSED-NAME (PARSE-IDX).
				if (Characters.EQ_ZERO.test(ws.getCisltnam().getTableName().getRetIndxFormatted(ws.getX()))) {
					// COB_CODE: MOVE RET-TYPE (X)
					//                          TO WS-PARSED-TYPE-CODE (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
					// COB_CODE: MOVE TEST-VAL (X) TO WS-PARSED-NAME (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getTableName().getTestVal(ws.getX()));
				} else {
					// COB_CODE: MOVE RET-TYPE (X)
					//                          TO WS-PARSED-TYPE-CODE (PARSE-IDX)
					ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(ws.getCisltnam().getTableName().getRetType(ws.getX()));
					// COB_CODE: SET SX TO RET-INDX (X)
					ws.setSx(ws.getCisltnam().getTableName().getRetIndx(ws.getX()));
					// COB_CODE: MOVE SUFFIX-STD (SX) TO WS-PARSED-NAME (PARSE-IDX).
					ws.getWsParsedNameEntries(ws.getParseIdx()).setName(ws.getCisltnam().getSuffixTable().getSuffixStd(ws.getSx()));
				}
			}
		}
		//*****************************************************************00913
		// THE NEXT SENTENCE WILL EXECUTE WHEN TYPE CODE PASSED FROM THE  *00914
		// CALLING MODULE IS EQUAL TO 'P' BUT A COMPANY NAME HAS BEEN     *00915
		// IDENTIFIED IN THE PARSED TABLE SEARCH. AN WARNING MESSAGE WILL *00916
		// BE PLACED IN THE ERROR TABLE IN LINKAGE AREA.                  *00917
		//*****************************************************************00918
		// COB_CODE: IF TYPE-CODE = 'P' AND RET-TYPE (X) = 'C'
		//              PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
		if (nameScrubRecord.getTypeCode().getTypeCode() == 'P' && ws.getCisltnam().getTableName().getRetType(ws.getX()) == 'C') {
			// COB_CODE: MOVE +004 TO WS-ERROR-COUNT
			ws.getMiscellaneous().setWsErrorCount(((short) 4));
			// COB_CODE: MOVE 'CCF' TO WS-ERROR-CODE
			ws.getMiscellaneous().setWsErrorCode("CCF");
			// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT.
			buildError();
		}
		// COB_CODE: MOVE '0' TO WS-NAME-SW.
		ws.getSwitches().setWsNameSwFormatted("0");
	}

	/**Original name: 0350-DETERMINE-TYPE-CODE<br>
	 * <pre>***************************************************************  00930
	 *  IF THE TYPE CODE IS SET TO SPACES PRIOR TO THE CALL TO THIS  *  00931
	 *  MODULE, THE TYPE CODE WILL BE SET DEPENDING ON THE OUT COME  *  00932
	 *  OF THE TABLE SEARCEHD PERFORMED IN THE PRIOR PARAGRAPH.      *  00933
	 *     IF A COMPANY NAME WAS FOUND IN THE PARSED WORD THEN A     *  00934
	 *        WARNING MESSAGE WILL BE MOVED TO THE ERROR TABLE IN    *  00935
	 *        LINKAGE THAT SAYS 'COMPANY NAME FOUND' AND THE TYPE    *  00936
	 *        CODE WILL BE SET TO 'C'.                               *  00937
	 *     IF A PREFIX OR SUFFIX WAS FOUND IN THE PARSED WORD, THEN  *  00938
	 *        A WARNING MESSAGE WILL BE MOVED TO THE ERROR TABLE IN  *  00939
	 *        LINKAGE THAT SAYS 'PERSONAL NAME FOUND' AND THE TYPE   *  00940
	 *        CODE WILL BE SET TO 'P'.                               *  00941
	 * ***************************************************************  00942</pre>*/
	private void determineTypeCode() {
		// COB_CODE: IF WS-PARSED-NAME (2) = SPACES
		//              GO TO 0350-TYPE-EXIT.
		if (Characters.EQ_SPACE.test(ws.getWsParsedNameEntries(2).getName())) {
			// COB_CODE: MOVE 'C' TO TYPE-CODE
			nameScrubRecord.getTypeCode().setTypeCodeFormatted("C");
			// COB_CODE: GO TO 0350-TYPE-EXIT.
			return;
		}
		// COB_CODE: IF TYPE-SPACE
		//                 MOVE 'P' TO TYPE-CODE.
		if (nameScrubRecord.getTypeCode().isSpace()) {
			// COB_CODE: IF COMPANY-FOUND
			//              MOVE 'C' TO TYPE-CODE
			//           ELSE
			//              MOVE 'P' TO TYPE-CODE.
			if (ws.getSwitches().isCompanyFound()) {
				// COB_CODE: MOVE +003 TO WS-ERROR-COUNT
				ws.getMiscellaneous().setWsErrorCount(((short) 3));
				// COB_CODE: MOVE 'COF' TO WS-ERROR-CODE
				ws.getMiscellaneous().setWsErrorCode("COF");
				// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT
				buildError();
				// COB_CODE: MOVE 'C' TO TYPE-CODE
				nameScrubRecord.getTypeCode().setTypeCodeFormatted("C");
			} else {
				// COB_CODE: MOVE +002 TO WS-ERROR-COUNT
				ws.getMiscellaneous().setWsErrorCount(((short) 2));
				// COB_CODE: MOVE 'POF' TO WS-ERROR-CODE
				ws.getMiscellaneous().setWsErrorCode("POF");
				// COB_CODE: PERFORM 0999-BUILD-ERROR THRU 0999-ERROR-EXIT
				buildError();
				// COB_CODE: MOVE 'P' TO TYPE-CODE.
				nameScrubRecord.getTypeCode().setTypeCodeFormatted("P");
			}
		}
	}

	/**Original name: 0400-REMOVE-AND<br>
	 * <pre>*****************************************************************00965
	 *  WHEN AN CONJOINED WORD IS FOUND IN A COMPANY NAME IT IS REMOVED*00966
	 *  AND THE NEXT WORD IN THE PARSED TABLE IS MOVED TO ITS TABLE    *00967
	 *  POSITION.                                                      *00968
	 * *****************************************************************00969
	 * ** THE FOLLOWING IS COMMENTED OUT AND REPLACED BY THE CODE THAT
	 * ** WAS IN THE FED VERSION OF CIWBNSRB SINCE IT WILL HANDLE WHEN
	 * ** A NAME HAS MORE THAN ONE AND WORD.
	 * ****************************************************************
	 * 0400-REMOVE-AND.                                                 00971
	 *     IF WS-PARSED-TYPE-CODE (PARSE-IDX) = '&'                     00973
	 *        MOVE '1' TO WS-AND-SW                                     00974
	 *        MOVE SPACES TO WS-PARSED-NAME (PARSE-IDX)                 00975
	 *        MOVE SPACES TO WS-PARSED-TYPE-CODE (PARSE-IDX)            00976
	 *        SET PARSE-IDX UP BY 1                                     00977
	 *        MOVE WS-PARSED-NAME (PARSE-IDX) TO HOLD-PARSED-NAME       00978
	 *        MOVE WS-PARSED-TYPE-CODE (PARSE-IDX)                      00979
	 *                                    TO HOLD-PARSED-TYPE-CODE      00980
	 *        MOVE SPACES TO WS-PARSED-NAME (PARSE-IDX)                 00981
	 *        MOVE SPACES TO WS-PARSED-TYPE-CODE (PARSE-IDX)            00982
	 *        SET PARSE-IDX DOWN BY 1                                   00983
	 *        SET HOLD-PARSE-IDX TO PARSE-IDX                           00984
	 *        MOVE HOLD-PARSE-IDX TO WS-DISPLAY                         00985
	 *        MOVE HOLD-PARSED-NAME TO WS-PARSED-NAME (PARSE-IDX)       00986
	 *        MOVE HOLD-PARSED-TYPE-CODE                                00987
	 *                         TO WS-PARSED-TYPE-CODE (PARSE-IDX).      00988
	 *     SET PARSE-IDX UP BY 1.                                       00989
	 * 0400-REMOVE-AND-EXIT. EXIT.                                      00991
	 * **************************************************************
	 * ** THE FOLLOWING CODE REPLACES 0400-REMOVE-AND. THIS CODE IS
	 * ** FROM THE FED VERSION OF CIWBNSRB.
	 * **************************************************************</pre>*/
	private void removeAnd() {
		// COB_CODE: IF WS-PARSED-TYPE-CODE(PARSE-IDX) = '&'
		//                                          WS-PARSED-TYPE-CODE(PARSE-IDX)
		//           END-IF.
		if (ws.getWsParsedNameEntries(ws.getParseIdx()).getTypeCode() == '&') {
			// COB_CODE: IF NOT AND-FOUND
			//                                    TO PARSE-IDX
			//           END-IF
			if (!ws.getSwitches().isAndFound()) {
				// COB_CODE: MOVE '1'              TO WS-AND-SW
				ws.getSwitches().setWsAndSwFormatted("1");
				// COB_CODE: SET HOLD-PARSE-MIN-IDX
				//                                 TO PARSE-IDX
				ws.getWsStringData().setHoldParseMinIdx(Trunc.toShort(ws.getParseIdx(), 2));
			}
			// COB_CODE: MOVE SPACES              TO WS-PARSED-NAME(PARSE-IDX)
			//                                       WS-PARSED-TYPE-CODE(PARSE-IDX)
			ws.getWsParsedNameEntries(ws.getParseIdx()).setName("");
			ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(Types.SPACE_CHAR);
		}
		// COB_CODE: IF WS-PARSED-NAME(PARSE-IDX) NOT = SPACES
		//              SET HOLD-PARSE-MAX-IDX   TO PARSE-IDX
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsParsedNameEntries(ws.getParseIdx()).getName())) {
			// COB_CODE: SET HOLD-PARSE-MAX-IDX   TO PARSE-IDX
			ws.getWsStringData().setHoldParseMaxIdx(Trunc.toShort(ws.getParseIdx(), 2));
		}
	}

	/**Original name: 0425-MOVE-TABLE-ELEMENTS<br>
	 * <pre>*****************************************************************00994
	 *  0425-MOVE-TABLE-ELEMENTS WILL BUMP ALL ELEMENTS DOWN WHEN A    *00995
	 *  TABLE ELEMENT HAS BEEN REMOVED (CONJUNCTION, PREFIX, OR SUFFIX)*00996
	 * *****************************************************************01003</pre>*/
	private void moveTableElements() {
		// COB_CODE: IF WS-PARSED-NAME(PARSE-IDX) NOT = SPACES
		//              ADD +1                   TO HOLD-PARSE-TARGET-IDX
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsParsedNameEntries(ws.getParseIdx()).getName())) {
			// COB_CODE: MOVE WS-PARSED-NAME(PARSE-IDX)
			//                                    TO WS-PARSED-NAME
			//                                            (HOLD-PARSE-TARGET-IDX)
			ws.getWsParsedNameEntries(ws.getWsStringData().getHoldParseTargetIdx()).setName(ws.getWsParsedNameEntries(ws.getParseIdx()).getName());
			// COB_CODE: MOVE WS-PARSED-TYPE-CODE(PARSE-IDX)
			//                                    TO WS-PARSED-TYPE-CODE
			//                                            (HOLD-PARSE-TARGET-IDX)
			ws.getWsParsedNameEntries(ws.getWsStringData().getHoldParseTargetIdx())
					.setTypeCode(ws.getWsParsedNameEntries(ws.getParseIdx()).getTypeCode());
			// COB_CODE: MOVE SPACES              TO WS-PARSED-NAME(PARSE-IDX)
			//                                       WS-PARSED-TYPE-CODE(PARSE-IDX)
			ws.getWsParsedNameEntries(ws.getParseIdx()).setName("");
			ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCode(Types.SPACE_CHAR);
			// COB_CODE: ADD +1                   TO HOLD-PARSE-TARGET-IDX
			ws.getWsStringData().setHoldParseTargetIdx(Trunc.toShort(1 + ws.getWsStringData().getHoldParseTargetIdx(), 2));
		}
	}

	/**Original name: 0800-PROCESS-IND-NAME<br>
	 * <pre>                                                                 01531
	 * ***************************************************************
	 *   ALL &, /, OR, AND WILL BE LEFT IN NAME                      *
	 *   EXAMPLES OF NAME SCRUB:                                     *
	 *                                                               *
	 *   NAME                      PRE  FIRST  MIDDLE  LAST    SUFFIX*
	 *   ---------------           ---  -----  ------  ------  ------*
	 *   JOE SMITH                      JOE            SMITH         *
	 *   MR JOE SMITH              MR   JOE            SMITH         *
	 *   MR JOE SMITH SR           MR   JOE            SMITH   SR    *
	 *   MR JOE BOB SMITH          MR   JOE    BOB     SMITH         *
	 *   MR JOE BOB SMITH SR       MR   JOE    BOB     SMITH   SR    *
	 *   MR JOE BOB AL SMITH SR    MR   JOE    BOB AL  SMITH   SR    *
	 *   MR JOE                    MR                  JOE           *
	 *   JOE SR                                        JOE     SR    *
	 *   MR SR                     MR                  SR            *
	 *   MR JOE SR                 MR                  JOE     SR    *
	 *   JOE MC DA                      JOE            MC DA         *
	 *   JOE MC DA SR                   JOE            MC DA   SR    *
	 *   MR JOE MC DA SR           MR   JOE            MC DA   SR    *
	 *   MR JOE BOB MC DA SR       MR   JOE    BOB     MC DA   SR    *
	 *                                                               *
	 *   SOME GENERAL RULES:                                         *
	 *    -THE PREFIX MUST ALWAYS BE IN THE FIRST POSITION.          *
	 *    -THE SUFFIX MUST ALWAYS BE IN THE LAST POSITION.           *
	 *    -THE FIRST WORD ENCOUNTERED THAT IS NOT A PREFIX IS        *
	 *     CONSIDERED TO BE THE FIRST NAME.                          *
	 *    -THE LAST WORD ENCOUNTERED THAT IS NOT A SUFFIX IS         *
	 *     CONSIDERED TO BE THE LAST NAME.                           *
	 *    -ALL OTHER WORDS IN BETWEEN ARE CONSIDERED PART OF THE     *
	 *     MIDDLE NAME.                                              *
	 *    -MC, MAC, DI, DE, OR ST START THE LAST NAME, THEY WILL     *
	 *     BE INCLUDED IN THE LAST NAME.                             *
	 *                                                               *
	 * ***************************************************************</pre>*/
	private void processIndName() {
		// COB_CODE: SET NAME-PARSE-NOT-DONE     TO TRUE.
		ws.getWsStringData().getWsNameParsedSw().setNotDone();
		// COB_CODE: SET FIRST-NAME-NOT-FOUND    TO TRUE.
		ws.getWsStringData().getWsFirstNameFoundSw().setNotFound();
		// COB_CODE: SET PARSE-IDX               TO 1.
		ws.setParseIdx(1);
		// COB_CODE: MOVE 1                      TO WS-AND-MID-POINTER.
		ws.getMiscellaneous().setWsAndMidPointer(((short) 1));
	}

	/**Original name: 0800-A<br>*/
	private String a() {
		// COB_CODE: PERFORM 0810-CHECK-IF-LAST-NAME
		//              THRU 0810-CHECK-IF-LAST-NAME-EXIT.
		checkIfLastName();
		// COB_CODE: IF NAME-PARSE-DONE
		//              GO TO 0800-PROCESS-IND-NAME-EXIT
		//           END-IF.
		if (ws.getWsStringData().getWsNameParsedSw().isDone()) {
			// COB_CODE: GO TO 0800-PROCESS-IND-NAME-EXIT
			return "";
		}
		// COB_CODE: IF FIRST-NAME-NOT-FOUND
		//              GO TO 0800-A
		//           END-IF.
		if (ws.getWsStringData().getWsFirstNameFoundSw().isNotFound()) {
			// COB_CODE: IF (WS-PARSED-TYPE-CODE(1) = 'P')
			//              SET PARSE-IDX         UP BY 1
			//           END-IF
			if (ws.getWsParsedNameEntries(1).getTypeCode() == 'P') {
				// COB_CODE: MOVE WS-PARSED-NAME(1)
				//                                 TO FORMAT-OUT-PREFIX
				nameScrubRecord.getNameOut().setFormatOutPrefix(ws.getWsParsedNameEntries(1).getName());
				// COB_CODE: SET PARSE-IDX         UP BY 1
				ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
			}
			// COB_CODE: PERFORM 0810-CHECK-IF-LAST-NAME
			//              THRU 0810-CHECK-IF-LAST-NAME-EXIT
			checkIfLastName();
			// COB_CODE: IF NAME-PARSE-DONE
			//              GO TO 0800-PROCESS-IND-NAME-EXIT
			//           END-IF
			if (ws.getWsStringData().getWsNameParsedSw().isDone()) {
				// COB_CODE: GO TO 0800-PROCESS-IND-NAME-EXIT
				return "";
			}
			// COB_CODE: MOVE WS-PARSED-NAME(PARSE-IDX)
			//                                    TO FORMAT-OUT-FIRST
			nameScrubRecord.getNameOut().setFormatOutFirst(ws.getWsParsedNameEntries(ws.getParseIdx()).getName());
			// COB_CODE: MOVE 'F'                 TO WS-PARSED-TYPE-CODE(PARSE-IDX)
			ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCodeFormatted("F");
			// COB_CODE: SET FIRST-NAME-FOUND     TO TRUE
			ws.getWsStringData().getWsFirstNameFoundSw().setFound();
			// COB_CODE: SET PARSE-IDX            UP BY 1
			ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
			// COB_CODE: GO TO 0800-A
			return "0800-A";
		}
		// COB_CODE: MOVE WS-AND-MID-POINTER     TO WS-STRING-POINTER.
		ws.getMiscellaneous().setWsStringPointer(ws.getMiscellaneous().getWsAndMidPointer());
		// COB_CODE: PERFORM 9598-STRING-PNAME-MID
		//              THRU 9598-STRING-PNAME-MID-EXIT.
		stringPnameMid();
		// COB_CODE: MOVE WS-STRING-POINTER      TO WS-AND-MID-POINTER.
		ws.getMiscellaneous().setWsAndMidPointer(ws.getMiscellaneous().getWsStringPointer());
		// COB_CODE: ADD 1                       TO WS-MID-NAME-COUNT.
		ws.getMiscellaneous().setWsMidNameCount(Trunc.toShort(1 + ws.getMiscellaneous().getWsMidNameCount(), 1));
		// COB_CODE: SET PARSE-IDX               UP BY 1.
		ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
		// COB_CODE: GO TO 0800-A.
		return "0800-A";
	}

	/**Original name: 0810-CHECK-IF-LAST-NAME<br>*/
	private void checkIfLastName() {
		// COB_CODE: IF ((PARSE-IDX = 10)
		//            OR
		//             (WS-PARSED-NAME(PARSE-IDX + 1) = SPACES))
		//              GO TO 0810-CHECK-IF-LAST-NAME-EXIT
		//           END-IF.
		if (ws.getParseIdx() == 10 || Characters.EQ_SPACE.test(ws.getWsParsedNameEntries(ws.getParseIdx() + 1).getName())) {
			// COB_CODE: MOVE WS-PARSED-NAME(PARSE-IDX)
			//                                    TO FORMAT-OUT-LAST
			nameScrubRecord.getNameOut().getFormatOutLast().setFormatOutLast(ws.getWsParsedNameEntries(ws.getParseIdx()).getName());
			// COB_CODE: MOVE 'L'                 TO WS-PARSED-TYPE-CODE(PARSE-IDX)
			ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCodeFormatted("L");
			// COB_CODE: SET PARSE-IDX            TO 10
			ws.setParseIdx(10);
			// COB_CODE: SET NAME-PARSE-DONE      TO TRUE
			ws.getWsStringData().getWsNameParsedSw().setDone();
			// COB_CODE: GO TO 0810-CHECK-IF-LAST-NAME-EXIT
			return;
		}
		// COB_CODE: IF (((PARSE-IDX = 9)
		//            AND
		//             (WS-PARSED-TYPE-CODE(PARSE-IDX + 1) = 'S'))
		//            OR
		//             ((PARSE-IDX < 9)
		//            AND
		//             (WS-PARSED-TYPE-CODE(PARSE-IDX + 1) = 'S')
		//            AND
		//             (WS-PARSED-NAME(PARSE-IDX + 2) = SPACES)))
		//              END-IF
		//           END-IF.
		if (ws.getParseIdx() == 9 && ws.getWsParsedNameEntries(ws.getParseIdx() + 1).getTypeCode() == 'S'
				|| ws.getParseIdx() < 9 && ws.getWsParsedNameEntries(ws.getParseIdx() + 1).getTypeCode() == 'S'
						&& Characters.EQ_SPACE.test(ws.getWsParsedNameEntries(ws.getParseIdx() + 2).getName())) {
			// COB_CODE: IF ((PARSE-IDX = 1)
			//            AND
			//             (WS-PARSED-TYPE-CODE(PARSE-IDX) = 'P'))
			//              GO TO 0810-CHECK-IF-LAST-NAME-EXIT
			//           ELSE
			//              GO TO 0810-CHECK-IF-LAST-NAME-EXIT
			//           END-IF
			if (ws.getParseIdx() == 1 && ws.getWsParsedNameEntries(ws.getParseIdx()).getTypeCode() == 'P') {
				// COB_CODE: MOVE WS-PARSED-NAME(PARSE-IDX)
				//                                 TO FORMAT-OUT-PREFIX
				nameScrubRecord.getNameOut().setFormatOutPrefix(ws.getWsParsedNameEntries(ws.getParseIdx()).getName());
				// COB_CODE: SET PARSE-IDX         UP BY 1
				ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
				// COB_CODE: MOVE WS-PARSED-NAME(PARSE-IDX)
				//                                 TO FORMAT-OUT-LAST
				nameScrubRecord.getNameOut().getFormatOutLast().setFormatOutLast(ws.getWsParsedNameEntries(ws.getParseIdx()).getName());
				// COB_CODE: MOVE 'L'              TO WS-PARSED-TYPE-CODE(PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCodeFormatted("L");
				// COB_CODE: SET NAME-PARSE-DONE   TO TRUE
				ws.getWsStringData().getWsNameParsedSw().setDone();
				// COB_CODE: GO TO 0810-CHECK-IF-LAST-NAME-EXIT
				return;
			} else {
				// COB_CODE: MOVE WS-PARSED-NAME(PARSE-IDX)
				//                                 TO FORMAT-OUT-LAST
				nameScrubRecord.getNameOut().getFormatOutLast().setFormatOutLast(ws.getWsParsedNameEntries(ws.getParseIdx()).getName());
				// COB_CODE: MOVE 'L'              TO WS-PARSED-TYPE-CODE(PARSE-IDX)
				ws.getWsParsedNameEntries(ws.getParseIdx()).setTypeCodeFormatted("L");
				// COB_CODE: SET PARSE-IDX         UP BY 1
				ws.setParseIdx(Trunc.toInt(ws.getParseIdx() + 1, 9));
				// COB_CODE: MOVE WS-PARSED-NAME(PARSE-IDX)
				//                                 TO FORMAT-OUT-SUFFIX
				nameScrubRecord.getNameOut().setFormatOutSuffix(ws.getWsParsedNameEntries(ws.getParseIdx()).getName());
				// COB_CODE: SET NAME-PARSE-DONE   TO TRUE
				ws.getWsStringData().getWsNameParsedSw().setDone();
				// COB_CODE: GO TO 0810-CHECK-IF-LAST-NAME-EXIT
				return;
			}
		}
	}

	/**Original name: 0999-BUILD-ERROR<br>*/
	private void buildError() {
		// COB_CODE: IF RETURN-IDX > 5
		//              GO TO 0999-ERROR-EXIT.
		if (ws.getReturnIdx() > 5) {
			// COB_CODE: GO TO 0999-ERROR-EXIT.
			return;
		}
		// COB_CODE: SET ERROR-INDEX TO WS-ERROR-COUNT.
		ws.setErrorIndex(ws.getMiscellaneous().getWsErrorCount());
		// COB_CODE: MOVE WS-ERROR-CODE TO RETURN-ERROR-CODE (RETURN-IDX).
		nameScrubRecord.getReturnedData(ws.getReturnIdx()).setReturnErrorCode(ws.getMiscellaneous().getWsErrorCode());
		// COB_CODE: MOVE 'CISBNSRB ' TO RETURN-PROGRAM-ID (RETURN-IDX).
		nameScrubRecord.getReturnedData(ws.getReturnIdx()).setReturnProgramId("CISBNSRB ");
		// COB_CODE: MOVE ERROR-STATUS (ERROR-INDEX)
		//                            TO RETURN-ERROR-STATUS (RETURN-IDX).
		nameScrubRecord.getReturnedData(ws.getReturnIdx()).getReturnErrorStatus()
				.setReturnErrorStatus(ws.getWsErrorMessages().getErrorStatus(ws.getErrorIndex()));
		// COB_CODE: MOVE ERROR-MESSAGE (ERROR-INDEX)
		//                            TO MESSAGE-RETURNED (RETURN-IDX).
		nameScrubRecord.getReturnedData(ws.getReturnIdx()).setMessageReturned(ws.getWsErrorMessages().getErrorMessage(ws.getErrorIndex()));
		// COB_CODE: SET RETURN-IDX UP BY 1.
		ws.setReturnIdx(Trunc.toInt(ws.getReturnIdx() + 1, 9));
	}

	/**Original name: 9475-BUILD-CO-OUTPUT<br>
	 * <pre>/ COPYBOOK "CIWCNSBC" CONTAINS THE SOURCE CODE USING THE COBOL / 01598
	 * / VERB 'STRING' AND 'UNSTRING'.                                / 01598
	 * / COPYBOOK "CIWCNSBA CONTAINS THE SOURCE CODE THAT CALLS PGMS  / 01598
	 * / "CIWBSTRG" AND "CIWBUSTG" WHICH ARE THE ASSEMBLER VERSION OF / 01598
	 * / THE COBOL VERB 'STRING' AND 'UNSTRING'.                      / 01598
	 * *****************************************************************
	 *    CIWCNSBC  -  COPYBOOK FOR CIWBNSRB - COBOL VERSION           *
	 * *****************************************************************
	 *              M A I N T E N A N C E   L O G                      *
	 *                                                                 *
	 *    SI #    DATE    EMP ID              DESCRIPTION              *
	 *   ------ --------  ------    ---------------------------------- *
	 *          11/17/94  7594      SOURCE CODE CREATED.               *
	 * FED11092 10/01/98   ASW      SET CLOSER TO FED STANDARDS AND    *
	 *                              MOVED STRIP COMMAS PARAGRAPH FROM  *
	 *                              MAIN PROGRAM TO COPYBOOK.          *
	 *                                                                 *
	 * *****************************************************************
	 * *****************************************************************01028
	 *  AFTER THE CONJOINED WORD IS REMOVED FROM THE PARSED TABLE AND  *01029
	 *  THE TABLE RESET, EACH PARSED PIECE OCCURENCE IS STRUNG TOGETHER*01030
	 *  INTO FORMAT OUT LAST IN LINKAGE.                               *01031
	 * *****************************************************************01032</pre>*/
	private void buildCoOutput() {
		ConcatUtil concatUtil = null;
		// COB_CODE: STRING  WS-PARSED-NAME (1)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//                   WS-PARSED-NAME (2)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//                   WS-PARSED-NAME (3)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//                   WS-PARSED-NAME (4)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//                   WS-PARSED-NAME (5)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//                   WS-PARSED-NAME (6)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//                   WS-PARSED-NAME (7)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//                   WS-PARSED-NAME (8)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//                   WS-PARSED-NAME (9)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//                   WS-PARSED-NAME (10)
		//                        DELIMITED BY SPACE
		//                INTO  FORMAT-OUT-LAST.
		concatUtil = ConcatUtil.buildString(FormatOutLast.Len.FORMAT_OUT_LAST,
				new String[] { Functions.substringBefore(ws.getWsParsedNameEntries(1).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING,
						Functions.substringBefore(ws.getWsParsedNameEntries(2).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING,
						Functions.substringBefore(ws.getWsParsedNameEntries(3).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING,
						Functions.substringBefore(ws.getWsParsedNameEntries(4).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING,
						Functions.substringBefore(ws.getWsParsedNameEntries(5).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING,
						Functions.substringBefore(ws.getWsParsedNameEntries(6).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING,
						Functions.substringBefore(ws.getWsParsedNameEntries(7).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING,
						Functions.substringBefore(ws.getWsParsedNameEntries(8).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING,
						Functions.substringBefore(ws.getWsParsedNameEntries(9).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING,
						Functions.substringBefore(ws.getWsParsedNameEntries(10).getNameFormatted(), Types.SPACE_STRING) });
		nameScrubRecord.getNameOut().getFormatOutLast()
				.setFormatOutLast(concatUtil.replaceInString(nameScrubRecord.getNameOut().getFormatOutLast().getFormatOutLastFormatted()));
	}

	/**Original name: 9598-STRING-PNAME-MID<br>*/
	private void stringPnameMid() {
		ConcatUtil concatUtil = null;
		// COB_CODE: STRING  WS-PARSED-NAME (PARSE-IDX)
		//                        DELIMITED BY SPACE
		//                        SPACE
		//                        DELIMITED BY SIZE
		//              INTO FORMAT-OUT-MID
		//           POINTER WS-STRING-POINTER.
		concatUtil = ConcatUtil.buildString(ws.getMiscellaneous().getWsStringPointer(), NameOut.Len.FORMAT_OUT_MID,
				Functions.substringBefore(ws.getWsParsedNameEntries(ws.getParseIdx()).getNameFormatted(), Types.SPACE_STRING), Types.SPACE_STRING);
		nameScrubRecord.getNameOut().setFormatOutMid(
				concatUtil.replaceInString(nameScrubRecord.getNameOut().getFormatOutMidFormatted(), ws.getMiscellaneous().getWsStringPointer()));
		ws.getMiscellaneous().setWsStringPointer(((short) (concatUtil.getPointer())));
	}

	/**Original name: 9700-DETERMINE-LAST-NAME<br>*/
	private void determineLastName() {
		IStringTokenizer tokenizer = null;
		// COB_CODE: MOVE '0'                    TO WS-COMMA-SW.
		ws.getSwitches().setWsCommaSwFormatted("0");
		// COB_CODE: MOVE SPACES                 TO WS-LAST-NAME.
		ws.getMiscellaneous().getWsLastName().setWsLastName("");
		// COB_CODE: UNSTRING WS-LONG-NAME
		//               DELIMITED BY ' '
		//               INTO WS-LAST-NAME.
		tokenizer = new CharTokenizer(' ');
		tokenizer.tokenize(ws.getMiscellaneous().getWsLongNameFormatted());
		try {
			ws.getMiscellaneous().getWsLastName().setWsLastName(Functions.trimAfter(tokenizer.nextString(), WsLastName.Len.WS_LAST_NAME));
		} catch (NoSuchElementException e) {
		}
		// COB_CODE: PERFORM 9710-LOOK-FOR-COMMAS
		//              THRU 9710-LOOK-FOR-COMMAS-EXIT
		//                   VARYING LAST-IDX
		//                   FROM 1 BY 1
		//                   UNTIL LAST-IDX > 60.
		ws.setLastIdx(1);
		while (!(ws.getLastIdx() > 60)) {
			lookForCommas();
			ws.setLastIdx(Trunc.toInt(ws.getLastIdx() + 1, 9));
		}
	}

	/**Original name: 9710-LOOK-FOR-COMMAS<br>*/
	private void lookForCommas() {
		// COB_CODE: IF WS-LAST-NAME-BYTE(LAST-IDX) = ','
		//              SET LAST-IDX             TO 60
		//           END-IF.
		if (ws.getMiscellaneous().getWsLastName().getNameByte(ws.getLastIdx()) == ',') {
			// COB_CODE: MOVE '1'                 TO WS-COMMA-SW
			ws.getSwitches().setWsCommaSwFormatted("1");
			// COB_CODE: MOVE WS-PARSED-NAME(1)   TO FORMAT-OUT-LAST
			nameScrubRecord.getNameOut().getFormatOutLast().setFormatOutLast(ws.getWsParsedNameEntries(1).getName());
			// COB_CODE: SET LAST-IDX             TO 60
			ws.setLastIdx(60);
		}
	}

	/**Original name: 9800-STRING-NAME<br>*/
	private void stringName() {
		ConcatUtil concatUtil = null;
		// COB_CODE: STRING  WS-HOLD-NAME-WORK1
		//                   WS-HOLD-NAME-WORK2
		//                       DELIMITED BY ' '
		//             INTO  WS-HOLD-NAME-WORK3.
		concatUtil = ConcatUtil.buildString(Miscellaneous.Len.WS_HOLD_NAME_WORK3,
				Functions.substringBefore(ws.getMiscellaneous().getWsHoldNameWork1Formatted(), " "),
				Functions.substringBefore(ws.getMiscellaneous().getWsHoldNameWork2Formatted(), " "));
		ws.getMiscellaneous().setWsHoldNameWork3(concatUtil.replaceInString(ws.getMiscellaneous().getWsHoldNameWork3Formatted()));
	}

	/**Original name: RNG_0800-PROCESS-IND-NAME-_-0800-PROCESS-IND-NAME-EXIT<br>*/
	private void rng0800ProcessIndName() {
		String retcode = "";
		boolean goto0800A = false;
		processIndName();
		do {
			goto0800A = false;
			retcode = a();
		} while (retcode.equals("0800-A"));
	}
}
