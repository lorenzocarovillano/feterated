/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.redefines.FormatOutLast;

/**Original name: NAME-OUT<br>
 * Variable: NAME-OUT from copybook CISLNSRB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class NameOut {

	//==== PROPERTIES ====
	//Original name: FORMAT-OUT-PREFIX
	private String formatOutPrefix = "";
	//Original name: FORMAT-OUT-FIRST
	private String formatOutFirst = "";
	//Original name: FORMAT-OUT-MID
	private String formatOutMid = "";
	//Original name: FORMAT-OUT-LAST
	private FormatOutLast formatOutLast = new FormatOutLast();
	//Original name: FORMAT-OUT-SUFFIX
	private String formatOutSuffix = "";
	//Original name: FORMAT-OUT-PREFIX2
	private String formatOutPrefix2 = "";
	//Original name: FORMAT-OUT-FIRST2
	private String formatOutFirst2 = "";
	//Original name: FORMAT-OUT-MID2
	private String formatOutMid2 = "";
	//Original name: FORMAT-OUT-LAST2
	private String formatOutLast2 = "";
	//Original name: FORMAT-OUT-SUFFIX2
	private String formatOutSuffix2 = "";
	//Original name: COMPANY-OUT
	private String companyOut = "";

	//==== METHODS ====
	public void setNameOutBytes(byte[] buffer, int offset) {
		int position = offset;
		setFormattedNameOutBytes(buffer, position);
		position += Len.FORMATTED_NAME_OUT;
		setFormattedNameOut2Bytes(buffer, position);
		position += Len.FORMATTED_NAME_OUT2;
		setCompanyNameOutBytes(buffer, position);
	}

	public byte[] getNameOutBytes(byte[] buffer, int offset) {
		int position = offset;
		getFormattedNameOutBytes(buffer, position);
		position += Len.FORMATTED_NAME_OUT;
		getFormattedNameOut2Bytes(buffer, position);
		position += Len.FORMATTED_NAME_OUT2;
		getCompanyNameOutBytes(buffer, position);
		return buffer;
	}

	public void setFormattedNameOutBytes(byte[] buffer, int offset) {
		int position = offset;
		formatOutPrefix = MarshalByte.readString(buffer, position, Len.FORMAT_OUT_PREFIX);
		position += Len.FORMAT_OUT_PREFIX;
		formatOutFirst = MarshalByte.readString(buffer, position, Len.FORMAT_OUT_FIRST);
		position += Len.FORMAT_OUT_FIRST;
		formatOutMid = MarshalByte.readString(buffer, position, Len.FORMAT_OUT_MID);
		position += Len.FORMAT_OUT_MID;
		formatOutLast.setFormatOutLastFromBuffer(buffer, position);
		position += FormatOutLast.Len.FORMAT_OUT_LAST;
		formatOutSuffix = MarshalByte.readString(buffer, position, Len.FORMAT_OUT_SUFFIX);
	}

	public byte[] getFormattedNameOutBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, formatOutPrefix, Len.FORMAT_OUT_PREFIX);
		position += Len.FORMAT_OUT_PREFIX;
		MarshalByte.writeString(buffer, position, formatOutFirst, Len.FORMAT_OUT_FIRST);
		position += Len.FORMAT_OUT_FIRST;
		MarshalByte.writeString(buffer, position, formatOutMid, Len.FORMAT_OUT_MID);
		position += Len.FORMAT_OUT_MID;
		formatOutLast.getFormatOutLastAsBuffer(buffer, position);
		position += FormatOutLast.Len.FORMAT_OUT_LAST;
		MarshalByte.writeString(buffer, position, formatOutSuffix, Len.FORMAT_OUT_SUFFIX);
		return buffer;
	}

	public void setFormatOutPrefix(String formatOutPrefix) {
		this.formatOutPrefix = Functions.subString(formatOutPrefix, Len.FORMAT_OUT_PREFIX);
	}

	public String getFormatOutPrefix() {
		return this.formatOutPrefix;
	}

	public void setFormatOutFirst(String formatOutFirst) {
		this.formatOutFirst = Functions.subString(formatOutFirst, Len.FORMAT_OUT_FIRST);
	}

	public String getFormatOutFirst() {
		return this.formatOutFirst;
	}

	public void setFormatOutMid(String formatOutMid) {
		this.formatOutMid = Functions.subString(formatOutMid, Len.FORMAT_OUT_MID);
	}

	public String getFormatOutMid() {
		return this.formatOutMid;
	}

	public String getFormatOutMidFormatted() {
		return Functions.padBlanks(getFormatOutMid(), Len.FORMAT_OUT_MID);
	}

	public void setFormatOutSuffix(String formatOutSuffix) {
		this.formatOutSuffix = Functions.subString(formatOutSuffix, Len.FORMAT_OUT_SUFFIX);
	}

	public String getFormatOutSuffix() {
		return this.formatOutSuffix;
	}

	public void setFormattedNameOut2Bytes(byte[] buffer, int offset) {
		int position = offset;
		formatOutPrefix2 = MarshalByte.readString(buffer, position, Len.FORMAT_OUT_PREFIX2);
		position += Len.FORMAT_OUT_PREFIX2;
		formatOutFirst2 = MarshalByte.readString(buffer, position, Len.FORMAT_OUT_FIRST2);
		position += Len.FORMAT_OUT_FIRST2;
		formatOutMid2 = MarshalByte.readString(buffer, position, Len.FORMAT_OUT_MID2);
		position += Len.FORMAT_OUT_MID2;
		formatOutLast2 = MarshalByte.readString(buffer, position, Len.FORMAT_OUT_LAST2);
		position += Len.FORMAT_OUT_LAST2;
		formatOutSuffix2 = MarshalByte.readString(buffer, position, Len.FORMAT_OUT_SUFFIX2);
	}

	public byte[] getFormattedNameOut2Bytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, formatOutPrefix2, Len.FORMAT_OUT_PREFIX2);
		position += Len.FORMAT_OUT_PREFIX2;
		MarshalByte.writeString(buffer, position, formatOutFirst2, Len.FORMAT_OUT_FIRST2);
		position += Len.FORMAT_OUT_FIRST2;
		MarshalByte.writeString(buffer, position, formatOutMid2, Len.FORMAT_OUT_MID2);
		position += Len.FORMAT_OUT_MID2;
		MarshalByte.writeString(buffer, position, formatOutLast2, Len.FORMAT_OUT_LAST2);
		position += Len.FORMAT_OUT_LAST2;
		MarshalByte.writeString(buffer, position, formatOutSuffix2, Len.FORMAT_OUT_SUFFIX2);
		return buffer;
	}

	public void setFormatOutPrefix2(String formatOutPrefix2) {
		this.formatOutPrefix2 = Functions.subString(formatOutPrefix2, Len.FORMAT_OUT_PREFIX2);
	}

	public String getFormatOutPrefix2() {
		return this.formatOutPrefix2;
	}

	public void setFormatOutFirst2(String formatOutFirst2) {
		this.formatOutFirst2 = Functions.subString(formatOutFirst2, Len.FORMAT_OUT_FIRST2);
	}

	public String getFormatOutFirst2() {
		return this.formatOutFirst2;
	}

	public void setFormatOutMid2(String formatOutMid2) {
		this.formatOutMid2 = Functions.subString(formatOutMid2, Len.FORMAT_OUT_MID2);
	}

	public String getFormatOutMid2() {
		return this.formatOutMid2;
	}

	public void setFormatOutLast2(String formatOutLast2) {
		this.formatOutLast2 = Functions.subString(formatOutLast2, Len.FORMAT_OUT_LAST2);
	}

	public String getFormatOutLast2() {
		return this.formatOutLast2;
	}

	public void setFormatOutSuffix2(String formatOutSuffix2) {
		this.formatOutSuffix2 = Functions.subString(formatOutSuffix2, Len.FORMAT_OUT_SUFFIX2);
	}

	public String getFormatOutSuffix2() {
		return this.formatOutSuffix2;
	}

	public void setCompanyNameOutBytes(byte[] buffer, int offset) {
		int position = offset;
		companyOut = MarshalByte.readString(buffer, position, Len.COMPANY_OUT);
	}

	public byte[] getCompanyNameOutBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, companyOut, Len.COMPANY_OUT);
		return buffer;
	}

	public void setCompanyOut(String companyOut) {
		this.companyOut = Functions.subString(companyOut, Len.COMPANY_OUT);
	}

	public String getCompanyOut() {
		return this.companyOut;
	}

	public FormatOutLast getFormatOutLast() {
		return formatOutLast;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FORMAT_OUT_PREFIX = 8;
		public static final int FORMAT_OUT_FIRST = 45;
		public static final int FORMAT_OUT_MID = 45;
		public static final int FORMAT_OUT_SUFFIX = 4;
		public static final int FORMATTED_NAME_OUT = FORMAT_OUT_PREFIX + FORMAT_OUT_FIRST + FORMAT_OUT_MID + FormatOutLast.Len.FORMAT_OUT_LAST
				+ FORMAT_OUT_SUFFIX;
		public static final int FORMAT_OUT_PREFIX2 = 8;
		public static final int FORMAT_OUT_FIRST2 = 45;
		public static final int FORMAT_OUT_MID2 = 45;
		public static final int FORMAT_OUT_LAST2 = 60;
		public static final int FORMAT_OUT_SUFFIX2 = 4;
		public static final int FORMATTED_NAME_OUT2 = FORMAT_OUT_PREFIX2 + FORMAT_OUT_FIRST2 + FORMAT_OUT_MID2 + FORMAT_OUT_LAST2
				+ FORMAT_OUT_SUFFIX2;
		public static final int COMPANY_OUT = 60;
		public static final int COMPANY_NAME_OUT = COMPANY_OUT;
		public static final int NAME_OUT = FORMATTED_NAME_OUT + FORMATTED_NAME_OUT2 + COMPANY_NAME_OUT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
