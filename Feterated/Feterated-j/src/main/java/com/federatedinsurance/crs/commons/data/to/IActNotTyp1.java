/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ACT_NOT, ACT_NOT_TYP]
 * 
 */
public interface IActNotTyp1 extends BaseSqlTo {

	/**
	 * Host Variable CSR-ACT-NBR
	 * 
	 */
	String getCsrActNbr();

	void setCsrActNbr(String csrActNbr);

	/**
	 * Host Variable NOT-PRC-TS
	 * 
	 */
	String getNotPrcTs();

	void setNotPrcTs(String notPrcTs);

	/**
	 * Host Variable ACT-NOT-TYP-CD
	 * 
	 */
	String getActNotTypCd();

	void setActNotTypCd(String actNotTypCd);

	/**
	 * Host Variable NOT-DT
	 * 
	 */
	String getNotDt();

	void setNotDt(String notDt);

	/**
	 * Host Variable EMP-ID
	 * 
	 */
	String getEmpId();

	void setEmpId(String empId);

	/**
	 * Nullable property for EMP-ID
	 * 
	 */
	String getEmpIdObj();

	void setEmpIdObj(String empIdObj);

	/**
	 * Host Variable ACT-TYP-CD
	 * 
	 */
	String getActTypCd();

	void setActTypCd(String actTypCd);

	/**
	 * Nullable property for ACT-TYP-CD
	 * 
	 */
	String getActTypCdObj();

	void setActTypCdObj(String actTypCdObj);

	/**
	 * Host Variable DOC-DES
	 * 
	 */
	String getDocDes();

	void setDocDes(String docDes);
};
