/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IPolDtaExt;

/**Original name: DCLPOL-DTA-EXT<br>
 * Variable: DCLPOL-DTA-EXT from copybook FWH00001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclpolDtaExt implements IPolDtaExt {

	//==== PROPERTIES ====
	//Original name: POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: PLN-EXP-DT
	private String plnExpDt = DefaultValues.stringVal(Len.PLN_EXP_DT);
	//Original name: ACT-NBR
	private String actNbr = DefaultValues.stringVal(Len.ACT_NBR);
	//Original name: POL-ID
	private String polId = DefaultValues.stringVal(Len.POL_ID);
	//Original name: CMP-CD
	private String cmpCd = DefaultValues.stringVal(Len.CMP_CD);
	//Original name: POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: STS-POL-TYP-CD
	private String stsPolTypCd = DefaultValues.stringVal(Len.STS_POL_TYP_CD);
	//Original name: BND-IND
	private char bndInd = DefaultValues.CHAR_VAL;
	//Original name: MOL-CR-POL-IND
	private char molCrPolInd = DefaultValues.CHAR_VAL;
	//Original name: AUD-IND
	private char audInd = DefaultValues.CHAR_VAL;
	//Original name: LGE-PRD-IND
	private char lgePrdInd = DefaultValues.CHAR_VAL;
	//Original name: SPL-BIL-POL-IND
	private char splBilPolInd = DefaultValues.CHAR_VAL;
	//Original name: SIR-AMT
	private long sirAmt = DefaultValues.LONG_VAL;
	//Original name: PRI-RSK-ST-ABB
	private String priRskStAbb = DefaultValues.stringVal(Len.PRI_RSK_ST_ABB);
	//Original name: PRI-RSK-ST-CD
	private String priRskStCd = DefaultValues.stringVal(Len.PRI_RSK_ST_CD);
	//Original name: PRI-RSK-CTY-CD
	private String priRskCtyCd = DefaultValues.stringVal(Len.PRI_RSK_CTY_CD);
	//Original name: PRI-RSK-TWN-CD
	private String priRskTwnCd = DefaultValues.stringVal(Len.PRI_RSK_TWN_CD);
	//Original name: REW-POL-NBR
	private String rewPolNbr = DefaultValues.stringVal(Len.REW_POL_NBR);
	//Original name: RNL-QTE-IND
	private char rnlQteInd = DefaultValues.CHAR_VAL;
	//Original name: WHY-CNC-CD
	private char whyCncCd = DefaultValues.CHAR_VAL;
	//Original name: TMN-EXP-CNC-DT
	private String tmnExpCncDt = DefaultValues.stringVal(Len.TMN_EXP_CNC_DT);
	//Original name: POL-PER-EN-TRS-CD
	private String polPerEnTrsCd = DefaultValues.stringVal(Len.POL_PER_EN_TRS_CD);
	//Original name: ACY-POL-IND
	private char acyPolInd = DefaultValues.CHAR_VAL;
	//Original name: WRT-PRM-AMT
	private long wrtPrmAmt = DefaultValues.LONG_VAL;
	//Original name: FST-TRM-IND
	private char fstTrmInd = DefaultValues.CHAR_VAL;
	//Original name: SRC-SYS-CD
	private String srcSysCd = DefaultValues.stringVal(Len.SRC_SYS_CD);

	//==== METHODS ====
	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	@Override
	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPlnExpDt(String plnExpDt) {
		this.plnExpDt = Functions.subString(plnExpDt, Len.PLN_EXP_DT);
	}

	public String getPlnExpDt() {
		return this.plnExpDt;
	}

	@Override
	public void setActNbr(String actNbr) {
		this.actNbr = Functions.subString(actNbr, Len.ACT_NBR);
	}

	@Override
	public String getActNbr() {
		return this.actNbr;
	}

	@Override
	public void setPolId(String polId) {
		this.polId = Functions.subString(polId, Len.POL_ID);
	}

	@Override
	public String getPolId() {
		return this.polId;
	}

	public void setCmpCd(String cmpCd) {
		this.cmpCd = Functions.subString(cmpCd, Len.CMP_CD);
	}

	public String getCmpCd() {
		return this.cmpCd;
	}

	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	public String getPolTypCd() {
		return this.polTypCd;
	}

	public void setStsPolTypCd(String stsPolTypCd) {
		this.stsPolTypCd = Functions.subString(stsPolTypCd, Len.STS_POL_TYP_CD);
	}

	public String getStsPolTypCd() {
		return this.stsPolTypCd;
	}

	public void setBndInd(char bndInd) {
		this.bndInd = bndInd;
	}

	public char getBndInd() {
		return this.bndInd;
	}

	public void setMolCrPolInd(char molCrPolInd) {
		this.molCrPolInd = molCrPolInd;
	}

	public char getMolCrPolInd() {
		return this.molCrPolInd;
	}

	public void setAudInd(char audInd) {
		this.audInd = audInd;
	}

	public char getAudInd() {
		return this.audInd;
	}

	public void setLgePrdInd(char lgePrdInd) {
		this.lgePrdInd = lgePrdInd;
	}

	public char getLgePrdInd() {
		return this.lgePrdInd;
	}

	public void setSplBilPolInd(char splBilPolInd) {
		this.splBilPolInd = splBilPolInd;
	}

	public char getSplBilPolInd() {
		return this.splBilPolInd;
	}

	public void setSirAmt(long sirAmt) {
		this.sirAmt = sirAmt;
	}

	public long getSirAmt() {
		return this.sirAmt;
	}

	public void setPriRskStAbb(String priRskStAbb) {
		this.priRskStAbb = Functions.subString(priRskStAbb, Len.PRI_RSK_ST_ABB);
	}

	public String getPriRskStAbb() {
		return this.priRskStAbb;
	}

	public void setPriRskStCd(String priRskStCd) {
		this.priRskStCd = Functions.subString(priRskStCd, Len.PRI_RSK_ST_CD);
	}

	public String getPriRskStCd() {
		return this.priRskStCd;
	}

	public void setPriRskCtyCd(String priRskCtyCd) {
		this.priRskCtyCd = Functions.subString(priRskCtyCd, Len.PRI_RSK_CTY_CD);
	}

	public String getPriRskCtyCd() {
		return this.priRskCtyCd;
	}

	public void setPriRskTwnCd(String priRskTwnCd) {
		this.priRskTwnCd = Functions.subString(priRskTwnCd, Len.PRI_RSK_TWN_CD);
	}

	public String getPriRskTwnCd() {
		return this.priRskTwnCd;
	}

	public void setRewPolNbr(String rewPolNbr) {
		this.rewPolNbr = Functions.subString(rewPolNbr, Len.REW_POL_NBR);
	}

	public String getRewPolNbr() {
		return this.rewPolNbr;
	}

	public void setRnlQteInd(char rnlQteInd) {
		this.rnlQteInd = rnlQteInd;
	}

	public char getRnlQteInd() {
		return this.rnlQteInd;
	}

	public void setWhyCncCd(char whyCncCd) {
		this.whyCncCd = whyCncCd;
	}

	public char getWhyCncCd() {
		return this.whyCncCd;
	}

	public void setTmnExpCncDt(String tmnExpCncDt) {
		this.tmnExpCncDt = Functions.subString(tmnExpCncDt, Len.TMN_EXP_CNC_DT);
	}

	public String getTmnExpCncDt() {
		return this.tmnExpCncDt;
	}

	public void setPolPerEnTrsCd(String polPerEnTrsCd) {
		this.polPerEnTrsCd = Functions.subString(polPerEnTrsCd, Len.POL_PER_EN_TRS_CD);
	}

	public String getPolPerEnTrsCd() {
		return this.polPerEnTrsCd;
	}

	public void setAcyPolInd(char acyPolInd) {
		this.acyPolInd = acyPolInd;
	}

	public char getAcyPolInd() {
		return this.acyPolInd;
	}

	public void setWrtPrmAmt(long wrtPrmAmt) {
		this.wrtPrmAmt = wrtPrmAmt;
	}

	public long getWrtPrmAmt() {
		return this.wrtPrmAmt;
	}

	public void setFstTrmInd(char fstTrmInd) {
		this.fstTrmInd = fstTrmInd;
	}

	public char getFstTrmInd() {
		return this.fstTrmInd;
	}

	public void setSrcSysCd(String srcSysCd) {
		this.srcSysCd = Functions.subString(srcSysCd, Len.SRC_SYS_CD);
	}

	public String getSrcSysCd() {
		return this.srcSysCd;
	}

	@Override
	public String getLnExpDt() {
		return getPlnExpDt();
	}

	@Override
	public void setLnExpDt(String lnExpDt) {
		this.setPlnExpDt(lnExpDt);
	}

	@Override
	public String getOlTypCd() {
		return getPolTypCd();
	}

	@Override
	public void setOlTypCd(String olTypCd) {
		this.setPolTypCd(olTypCd);
	}

	@Override
	public String getRiRskStAbb() {
		return getPriRskStAbb();
	}

	@Override
	public void setRiRskStAbb(String riRskStAbb) {
		this.setPriRskStAbb(riRskStAbb);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_EFF_DT = 10;
		public static final int ACT_NBR = 9;
		public static final int POL_ID = 16;
		public static final int POL_TYP_CD = 3;
		public static final int PRI_RSK_ST_ABB = 2;
		public static final int POL_NBR = 25;
		public static final int PLN_EXP_DT = 10;
		public static final int CMP_CD = 2;
		public static final int STS_POL_TYP_CD = 4;
		public static final int PRI_RSK_ST_CD = 3;
		public static final int PRI_RSK_CTY_CD = 3;
		public static final int PRI_RSK_TWN_CD = 4;
		public static final int REW_POL_NBR = 25;
		public static final int TMN_EXP_CNC_DT = 10;
		public static final int POL_PER_EN_TRS_CD = 2;
		public static final int SRC_SYS_CD = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
