/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9010<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9010 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT91I_POL_NBR_MAXOCCURS = 100;
	public static final int XZT91O_WRD_LIST_MAXOCCURS = 20;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9010() {
	}

	public LServiceContractAreaXz0x9010(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt91iCsrActNbr(String xzt91iCsrActNbr) {
		writeString(Pos.XZT91I_CSR_ACT_NBR, xzt91iCsrActNbr, Len.XZT91I_CSR_ACT_NBR);
	}

	/**Original name: XZT91I-CSR-ACT-NBR<br>*/
	public String getXzt91iCsrActNbr() {
		return readString(Pos.XZT91I_CSR_ACT_NBR, Len.XZT91I_CSR_ACT_NBR);
	}

	public void setXzt91iNotPrcTs(String xzt91iNotPrcTs) {
		writeString(Pos.XZT91I_NOT_PRC_TS, xzt91iNotPrcTs, Len.XZT91I_NOT_PRC_TS);
	}

	/**Original name: XZT91I-NOT-PRC-TS<br>*/
	public String getXzt91iNotPrcTs() {
		return readString(Pos.XZT91I_NOT_PRC_TS, Len.XZT91I_NOT_PRC_TS);
	}

	/**Original name: XZT91I-POL-LIST<br>*/
	public byte[] getXzt91iPolListBytes() {
		byte[] buffer = new byte[Len.XZT91I_POL_LIST];
		return getXzt91iPolListBytes(buffer, 1);
	}

	public byte[] getXzt91iPolListBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.XZT91I_POL_LIST, Pos.XZT91I_POL_LIST);
		return buffer;
	}

	public void setXzt91iPolNbr(int xzt91iPolNbrIdx, String xzt91iPolNbr) {
		int position = Pos.xzt91iPolNbr(xzt91iPolNbrIdx - 1);
		writeString(position, xzt91iPolNbr, Len.XZT91I_POL_NBR);
	}

	/**Original name: XZT91I-POL-NBR<br>*/
	public String getXzt91iPolNbr(int xzt91iPolNbrIdx) {
		int position = Pos.xzt91iPolNbr(xzt91iPolNbrIdx - 1);
		return readString(position, Len.XZT91I_POL_NBR);
	}

	public void setXzt91iUserid(String xzt91iUserid) {
		writeString(Pos.XZT91I_USERID, xzt91iUserid, Len.XZT91I_USERID);
	}

	/**Original name: XZT91I-USERID<br>*/
	public String getXzt91iUserid() {
		return readString(Pos.XZT91I_USERID, Len.XZT91I_USERID);
	}

	public String getXzt91iUseridFormatted() {
		return Functions.padBlanks(getXzt91iUserid(), Len.XZT91I_USERID);
	}

	public void setXzt91oCsrActNbr(String xzt91oCsrActNbr) {
		writeString(Pos.XZT91O_CSR_ACT_NBR, xzt91oCsrActNbr, Len.XZT91O_CSR_ACT_NBR);
	}

	/**Original name: XZT91O-CSR-ACT-NBR<br>*/
	public String getXzt91oCsrActNbr() {
		return readString(Pos.XZT91O_CSR_ACT_NBR, Len.XZT91O_CSR_ACT_NBR);
	}

	public void setXzt91oNotPrcTs(String xzt91oNotPrcTs) {
		writeString(Pos.XZT91O_NOT_PRC_TS, xzt91oNotPrcTs, Len.XZT91O_NOT_PRC_TS);
	}

	/**Original name: XZT91O-NOT-PRC-TS<br>*/
	public String getXzt91oNotPrcTs() {
		return readString(Pos.XZT91O_NOT_PRC_TS, Len.XZT91O_NOT_PRC_TS);
	}

	public void setXzt91oWrdSeqCd(int xzt91oWrdSeqCdIdx, String xzt91oWrdSeqCd) {
		int position = Pos.xzt91oWrdSeqCd(xzt91oWrdSeqCdIdx - 1);
		writeString(position, xzt91oWrdSeqCd, Len.XZT91O_WRD_SEQ_CD);
	}

	/**Original name: XZT91O-WRD-SEQ-CD<br>*/
	public String getXzt91oWrdSeqCd(int xzt91oWrdSeqCdIdx) {
		int position = Pos.xzt91oWrdSeqCd(xzt91oWrdSeqCdIdx - 1);
		return readString(position, Len.XZT91O_WRD_SEQ_CD);
	}

	public void setXzt91oWrdFont(int xzt91oWrdFontIdx, String xzt91oWrdFont) {
		int position = Pos.xzt91oWrdFont(xzt91oWrdFontIdx - 1);
		writeString(position, xzt91oWrdFont, Len.XZT91O_WRD_FONT);
	}

	/**Original name: XZT91O-WRD-FONT<br>*/
	public String getXzt91oWrdFont(int xzt91oWrdFontIdx) {
		int position = Pos.xzt91oWrdFont(xzt91oWrdFontIdx - 1);
		return readString(position, Len.XZT91O_WRD_FONT);
	}

	public void setXzt91oWrdTxt(int xzt91oWrdTxtIdx, String xzt91oWrdTxt) {
		int position = Pos.xzt91oWrdTxt(xzt91oWrdTxtIdx - 1);
		writeString(position, xzt91oWrdTxt, Len.XZT91O_WRD_TXT);
	}

	/**Original name: XZT91O-WRD-TXT<br>*/
	public String getXzt91oWrdTxt(int xzt91oWrdTxtIdx) {
		int position = Pos.xzt91oWrdTxt(xzt91oWrdTxtIdx - 1);
		return readString(position, Len.XZT91O_WRD_TXT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT901_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT91I_CSR_ACT_NBR = XZT901_SERVICE_INPUTS;
		public static final int XZT91I_NOT_PRC_TS = XZT91I_CSR_ACT_NBR + Len.XZT91I_CSR_ACT_NBR;
		public static final int XZT91I_POL_LIST = XZT91I_NOT_PRC_TS + Len.XZT91I_NOT_PRC_TS;
		public static final int XZT91I_USERID = xzt91iPolNbr(XZT91I_POL_NBR_MAXOCCURS - 1) + Len.XZT91I_POL_NBR;
		public static final int XZT901_SERVICE_OUTPUTS = XZT91I_USERID + Len.XZT91I_USERID;
		public static final int XZT91O_CSR_ACT_NBR = XZT901_SERVICE_OUTPUTS;
		public static final int XZT91O_NOT_PRC_TS = XZT91O_CSR_ACT_NBR + Len.XZT91O_CSR_ACT_NBR;
		public static final int XZT91O_TABLE_OF_WRD_LISTS = XZT91O_NOT_PRC_TS + Len.XZT91O_NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt91iPolNbr(int idx) {
			return XZT91I_POL_LIST + idx * Len.XZT91I_POL_NBR;
		}

		public static int xzt91oWrdList(int idx) {
			return XZT91O_TABLE_OF_WRD_LISTS + idx * Len.XZT91O_WRD_LIST;
		}

		public static int xzt91oWrdSeqCd(int idx) {
			return xzt91oWrdList(idx);
		}

		public static int xzt91oWrdFont(int idx) {
			return xzt91oWrdSeqCd(idx) + Len.XZT91O_WRD_SEQ_CD;
		}

		public static int xzt91oWrdTxt(int idx) {
			return xzt91oWrdFont(idx) + Len.XZT91O_WRD_FONT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT91I_CSR_ACT_NBR = 9;
		public static final int XZT91I_NOT_PRC_TS = 26;
		public static final int XZT91I_POL_NBR = 25;
		public static final int XZT91I_USERID = 8;
		public static final int XZT91O_CSR_ACT_NBR = 9;
		public static final int XZT91O_NOT_PRC_TS = 26;
		public static final int XZT91O_WRD_SEQ_CD = 5;
		public static final int XZT91O_WRD_FONT = 6;
		public static final int XZT91O_WRD_TXT = 4500;
		public static final int XZT91O_WRD_LIST = XZT91O_WRD_SEQ_CD + XZT91O_WRD_FONT + XZT91O_WRD_TXT;
		public static final int XZT91I_POL_LIST = LServiceContractAreaXz0x9010.XZT91I_POL_NBR_MAXOCCURS * XZT91I_POL_NBR;
		public static final int XZT901_SERVICE_INPUTS = XZT91I_CSR_ACT_NBR + XZT91I_NOT_PRC_TS + XZT91I_POL_LIST + XZT91I_USERID;
		public static final int XZT91O_TABLE_OF_WRD_LISTS = LServiceContractAreaXz0x9010.XZT91O_WRD_LIST_MAXOCCURS * XZT91O_WRD_LIST;
		public static final int XZT901_SERVICE_OUTPUTS = XZT91O_CSR_ACT_NBR + XZT91O_NOT_PRC_TS + XZT91O_TABLE_OF_WRD_LISTS;
		public static final int L_SERVICE_CONTRACT_AREA = XZT901_SERVICE_INPUTS + XZT901_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
