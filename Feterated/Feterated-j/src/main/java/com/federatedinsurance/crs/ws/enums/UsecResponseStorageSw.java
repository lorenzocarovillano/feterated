/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: USEC-RESPONSE-STORAGE-SW<br>
 * Variable: USEC-RESPONSE-STORAGE-SW from copybook HALLUSEC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UsecResponseStorageSw {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.RESPONSE_STORAGE_SW);
	public static final String UMT = "0";
	public static final String LINKAGE = "1";

	//==== METHODS ====
	public void setResponseStorageSw(short responseStorageSw) {
		this.value = NumericDisplay.asString(responseStorageSw, Len.RESPONSE_STORAGE_SW);
	}

	public void setResponseStorageSwFormatted(String responseStorageSw) {
		this.value = Trunc.toUnsignedNumeric(responseStorageSw, Len.RESPONSE_STORAGE_SW);
	}

	public short getResponseStorageSw() {
		return NumericDisplay.asShort(this.value);
	}

	public void setLinkage() {
		setResponseStorageSwFormatted(LINKAGE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESPONSE_STORAGE_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
