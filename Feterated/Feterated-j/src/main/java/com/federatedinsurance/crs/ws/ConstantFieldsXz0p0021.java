/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P0021<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p0021 {

	//==== PROPERTIES ====
	//Original name: CF-MAIL-ALLOWANCE
	private short mailAllowance = ((short) 8);
	//Original name: CF-PROCESSING-ALLOWANCE
	private short processingAllowance = ((short) 2);
	//Original name: CF-MAX-DATE
	private String maxDate = "9999-12-31";
	//Original name: CF-DT-SEPARATOR
	private char dtSeparator = '-';
	//Original name: CF-MAX-POL
	private short maxPol = ((short) 150);
	//Original name: CF-BUS-OBJ-XZ-DET-NOT-DT-BPO
	private String busObjXzDetNotDtBpo = "XZ_DET_NOT_DT_BPO";

	//==== METHODS ====
	public short getMailAllowance() {
		return this.mailAllowance;
	}

	public short getProcessingAllowance() {
		return this.processingAllowance;
	}

	public String getMaxDate() {
		return this.maxDate;
	}

	public char getDtSeparator() {
		return this.dtSeparator;
	}

	public short getMaxPol() {
		return this.maxPol;
	}

	public String getBusObjXzDetNotDtBpo() {
		return this.busObjXzDetNotDtBpo;
	}
}
