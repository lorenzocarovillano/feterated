/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLCLT-PHONE-TYPE-V<br>
 * Variable: DCLCLT-PHONE-TYPE-V from copybook MUH00290<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclcltPhoneTypeV {

	//==== PROPERTIES ====
	//Original name: PHN-TYP-CD
	private String phnTypCd = DefaultValues.stringVal(Len.PHN_TYP_CD);
	//Original name: CTR-NBR-CD
	private short ctrNbrCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: CIPT-PHN-TYP-DES
	private String ciptPhnTypDes = DefaultValues.stringVal(Len.CIPT_PHN_TYP_DES);

	//==== METHODS ====
	public void setPhnTypCd(String phnTypCd) {
		this.phnTypCd = Functions.subString(phnTypCd, Len.PHN_TYP_CD);
	}

	public String getPhnTypCd() {
		return this.phnTypCd;
	}

	public String getPhnTypCdFormatted() {
		return Functions.padBlanks(getPhnTypCd(), Len.PHN_TYP_CD);
	}

	public void setCtrNbrCd(short ctrNbrCd) {
		this.ctrNbrCd = ctrNbrCd;
	}

	public short getCtrNbrCd() {
		return this.ctrNbrCd;
	}

	public void setCiptPhnTypDes(String ciptPhnTypDes) {
		this.ciptPhnTypDes = Functions.subString(ciptPhnTypDes, Len.CIPT_PHN_TYP_DES);
	}

	public String getCiptPhnTypDes() {
		return this.ciptPhnTypDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PHN_TYP_CD = 10;
		public static final int CIPT_PHN_TYP_DES = 40;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
