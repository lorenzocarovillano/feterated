/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.BcmlactAbendText;
import com.federatedinsurance.crs.ws.enums.BcmlactErrorSw;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-BCMLACT-LINKAGE<br>
 * Variable: WS-BCMLACT-LINKAGE from program XZ0P90D0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsBcmlactLinkage extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: BCMLACT-API-TYPE
	private String apiType = DefaultValues.stringVal(Len.API_TYPE);
	//Original name: BCMLACT-POLICY-ID
	private String policyId = DefaultValues.stringVal(Len.POLICY_ID);
	//Original name: BCMLACT-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: BCMLACT-POL-SYMBOL-CD
	private String polSymbolCd = DefaultValues.stringVal(Len.POL_SYMBOL_CD);
	//Original name: BCMLACT-CANCEL-DT
	private String cancelDt = DefaultValues.stringVal(Len.CANCEL_DT);
	//Original name: BCMLACT-EFFECTIVE-DT
	private String effectiveDt = DefaultValues.stringVal(Len.EFFECTIVE_DT);
	//Original name: BCMLACT-ERROR-SW
	private BcmlactErrorSw errorSw = new BcmlactErrorSw();
	//Original name: BCMLACT-ABEND-TEXT
	private BcmlactAbendText abendText = new BcmlactAbendText();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_BCMLACT_LINKAGE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsBcmlactLinkageBytes(buf);
	}

	public void setWsBcmlactLinkageBytes(byte[] buffer) {
		setWsBcmlactLinkageBytes(buffer, 1);
	}

	public byte[] getWsBcmlactLinkageBytes() {
		byte[] buffer = new byte[Len.WS_BCMLACT_LINKAGE];
		return getWsBcmlactLinkageBytes(buffer, 1);
	}

	public void setWsBcmlactLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		setLinkageBcmlactBytes(buffer, position);
	}

	public byte[] getWsBcmlactLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		getLinkageBcmlactBytes(buffer, position);
		return buffer;
	}

	public void setLinkageBcmlactBytes(byte[] buffer, int offset) {
		int position = offset;
		apiType = MarshalByte.readString(buffer, position, Len.API_TYPE);
		position += Len.API_TYPE;
		policyId = MarshalByte.readString(buffer, position, Len.POLICY_ID);
		position += Len.POLICY_ID;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		polSymbolCd = MarshalByte.readString(buffer, position, Len.POL_SYMBOL_CD);
		position += Len.POL_SYMBOL_CD;
		cancelDt = MarshalByte.readString(buffer, position, Len.CANCEL_DT);
		position += Len.CANCEL_DT;
		effectiveDt = MarshalByte.readString(buffer, position, Len.EFFECTIVE_DT);
		position += Len.EFFECTIVE_DT;
		errorSw.setErrorSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		abendText.setAbendTextBytes(buffer, position);
	}

	public byte[] getLinkageBcmlactBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, apiType, Len.API_TYPE);
		position += Len.API_TYPE;
		MarshalByte.writeString(buffer, position, policyId, Len.POLICY_ID);
		position += Len.POLICY_ID;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polSymbolCd, Len.POL_SYMBOL_CD);
		position += Len.POL_SYMBOL_CD;
		MarshalByte.writeString(buffer, position, cancelDt, Len.CANCEL_DT);
		position += Len.CANCEL_DT;
		MarshalByte.writeString(buffer, position, effectiveDt, Len.EFFECTIVE_DT);
		position += Len.EFFECTIVE_DT;
		MarshalByte.writeChar(buffer, position, errorSw.getErrorSw());
		position += Types.CHAR_SIZE;
		abendText.getAbendTextBytes(buffer, position);
		return buffer;
	}

	public void setApiType(String apiType) {
		this.apiType = Functions.subString(apiType, Len.API_TYPE);
	}

	public String getApiType() {
		return this.apiType;
	}

	public void setPolicyId(String policyId) {
		this.policyId = Functions.subString(policyId, Len.POLICY_ID);
	}

	public String getPolicyId() {
		return this.policyId;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolSymbolCd(String polSymbolCd) {
		this.polSymbolCd = Functions.subString(polSymbolCd, Len.POL_SYMBOL_CD);
	}

	public String getPolSymbolCd() {
		return this.polSymbolCd;
	}

	public void setCancelDt(String cancelDt) {
		this.cancelDt = Functions.subString(cancelDt, Len.CANCEL_DT);
	}

	public String getCancelDt() {
		return this.cancelDt;
	}

	public void setEffectiveDt(String effectiveDt) {
		this.effectiveDt = Functions.subString(effectiveDt, Len.EFFECTIVE_DT);
	}

	public String getEffectiveDt() {
		return this.effectiveDt;
	}

	public BcmlactAbendText getAbendText() {
		return abendText;
	}

	public BcmlactErrorSw getErrorSw() {
		return errorSw;
	}

	@Override
	public byte[] serialize() {
		return getWsBcmlactLinkageBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int API_TYPE = 3;
		public static final int POLICY_ID = 16;
		public static final int POL_NBR = 25;
		public static final int POL_SYMBOL_CD = 3;
		public static final int CANCEL_DT = 10;
		public static final int EFFECTIVE_DT = 10;
		public static final int LINKAGE_BCMLACT = API_TYPE + POLICY_ID + POL_NBR + POL_SYMBOL_CD + CANCEL_DT + EFFECTIVE_DT
				+ BcmlactErrorSw.Len.ERROR_SW + BcmlactAbendText.Len.ABEND_TEXT;
		public static final int WS_BCMLACT_LINKAGE = LINKAGE_BCMLACT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
