/*
 * OCCURS class(es) for program CIWBNSRB
 * OCCURS class(es) for program CIWOSDX
 * OCCURS class(es) for program FNC02090
 * OCCURS class(es) for program TS020000
 * OCCURS class(es) for program TS030099
 * OCCURS class(es) for program TS547099
 * OCCURS class(es) for program TS548099
 * OCCURS class(es) for program XZ001000
 * OCCURS class(es) for program XZ0B90P0
 * OCCURS class(es) for program XZ0P0021
 * OCCURS class(es) for program XZ0P0022
 * OCCURS class(es) for program XZ0P9000
 * OCCURS class(es) for program XZ0P90C0
 * OCCURS class(es) for program XZ0P90H0
 * OCCURS class(es) for program XZ0P90K0
 * OCCURS class(es) for program XZ0U8000
 * OCCURS class(es) for program XZC01090
 * OCCURS class(es) for program XZC02090
 * OCCURS class(es) for program XZC03090
 * OCCURS class(es) for program XZC05090
 * OCCURS class(es) for program XZC06090
 * OCCURS class(es) for program XZC08090
 */
package com.federatedinsurance.crs.ws.occurs;
