/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: CW06F-CLT-CLT-RELATION-KEY<br>
 * Variable: CW06F-CLT-CLT-RELATION-KEY from copybook CAWLF006<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw06fCltCltRelationKey {

	//==== PROPERTIES ====
	//Original name: CW06F-CLIENT-ID-CI
	private char clientIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: CW06F-CLT-TYP-CD-CI
	private char cltTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-CLT-TYP-CD
	private String cltTypCd = DefaultValues.stringVal(Len.CLT_TYP_CD);
	//Original name: CW06F-HISTORY-VLD-NBR-CI
	private char historyVldNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-HISTORY-VLD-NBR-SIGNED
	private String historyVldNbrSigned = DefaultValues.stringVal(Len.HISTORY_VLD_NBR_SIGNED);
	//Original name: CW06F-CICR-XRF-ID-CI
	private char cicrXrfIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-CICR-XRF-ID
	private String cicrXrfId = DefaultValues.stringVal(Len.CICR_XRF_ID);
	//Original name: CW06F-XRF-TYP-CD-CI
	private char xrfTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-XRF-TYP-CD
	private String xrfTypCd = DefaultValues.stringVal(Len.XRF_TYP_CD);
	//Original name: CW06F-CICR-EFF-DT-CI
	private char cicrEffDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW06F-CICR-EFF-DT
	private String cicrEffDt = DefaultValues.stringVal(Len.CICR_EFF_DT);

	//==== METHODS ====
	public void setCltCltRelationKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		clientIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		clientId = MarshalByte.readString(buffer, position, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		cltTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cltTypCd = MarshalByte.readString(buffer, position, Len.CLT_TYP_CD);
		position += Len.CLT_TYP_CD;
		historyVldNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbrSigned = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.HISTORY_VLD_NBR_SIGNED), Len.HISTORY_VLD_NBR_SIGNED);
		position += Len.HISTORY_VLD_NBR_SIGNED;
		cicrXrfIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicrXrfId = MarshalByte.readString(buffer, position, Len.CICR_XRF_ID);
		position += Len.CICR_XRF_ID;
		xrfTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		xrfTypCd = MarshalByte.readString(buffer, position, Len.XRF_TYP_CD);
		position += Len.XRF_TYP_CD;
		cicrEffDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicrEffDt = MarshalByte.readString(buffer, position, Len.CICR_EFF_DT);
	}

	public byte[] getCltCltRelationKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, clientIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeChar(buffer, position, cltTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cltTypCd, Len.CLT_TYP_CD);
		position += Len.CLT_TYP_CD;
		MarshalByte.writeChar(buffer, position, historyVldNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, historyVldNbrSigned, Len.HISTORY_VLD_NBR_SIGNED);
		position += Len.HISTORY_VLD_NBR_SIGNED;
		MarshalByte.writeChar(buffer, position, cicrXrfIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicrXrfId, Len.CICR_XRF_ID);
		position += Len.CICR_XRF_ID;
		MarshalByte.writeChar(buffer, position, xrfTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, xrfTypCd, Len.XRF_TYP_CD);
		position += Len.XRF_TYP_CD;
		MarshalByte.writeChar(buffer, position, cicrEffDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicrEffDt, Len.CICR_EFF_DT);
		return buffer;
	}

	public void setClientIdCi(char clientIdCi) {
		this.clientIdCi = clientIdCi;
	}

	public char getClientIdCi() {
		return this.clientIdCi;
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setCltTypCdCi(char cltTypCdCi) {
		this.cltTypCdCi = cltTypCdCi;
	}

	public char getCltTypCdCi() {
		return this.cltTypCdCi;
	}

	public void setCltTypCd(String cltTypCd) {
		this.cltTypCd = Functions.subString(cltTypCd, Len.CLT_TYP_CD);
	}

	public String getCltTypCd() {
		return this.cltTypCd;
	}

	public void setHistoryVldNbrCi(char historyVldNbrCi) {
		this.historyVldNbrCi = historyVldNbrCi;
	}

	public char getHistoryVldNbrCi() {
		return this.historyVldNbrCi;
	}

	public void setCw06fHistoryVldNbrSignedFormatted(String cw06fHistoryVldNbrSigned) {
		this.historyVldNbrSigned = PicFormatter.display("-9(5)").format(cw06fHistoryVldNbrSigned).toString();
	}

	public long getCw06fHistoryVldNbrSigned() {
		return PicParser.display("-9(5)").parseLong(this.historyVldNbrSigned);
	}

	public void setCicrXrfIdCi(char cicrXrfIdCi) {
		this.cicrXrfIdCi = cicrXrfIdCi;
	}

	public char getCicrXrfIdCi() {
		return this.cicrXrfIdCi;
	}

	public void setCicrXrfId(String cicrXrfId) {
		this.cicrXrfId = Functions.subString(cicrXrfId, Len.CICR_XRF_ID);
	}

	public String getCicrXrfId() {
		return this.cicrXrfId;
	}

	public void setXrfTypCdCi(char xrfTypCdCi) {
		this.xrfTypCdCi = xrfTypCdCi;
	}

	public char getXrfTypCdCi() {
		return this.xrfTypCdCi;
	}

	public void setXrfTypCd(String xrfTypCd) {
		this.xrfTypCd = Functions.subString(xrfTypCd, Len.XRF_TYP_CD);
	}

	public String getXrfTypCd() {
		return this.xrfTypCd;
	}

	public void setCicrEffDtCi(char cicrEffDtCi) {
		this.cicrEffDtCi = cicrEffDtCi;
	}

	public char getCicrEffDtCi() {
		return this.cicrEffDtCi;
	}

	public void setCicrEffDt(String cicrEffDt) {
		this.cicrEffDt = Functions.subString(cicrEffDt, Len.CICR_EFF_DT);
	}

	public String getCicrEffDt() {
		return this.cicrEffDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ID = 20;
		public static final int CLT_TYP_CD = 4;
		public static final int HISTORY_VLD_NBR_SIGNED = 6;
		public static final int CICR_XRF_ID = 20;
		public static final int XRF_TYP_CD = 4;
		public static final int CICR_EFF_DT = 10;
		public static final int CLIENT_ID_CI = 1;
		public static final int CLT_TYP_CD_CI = 1;
		public static final int HISTORY_VLD_NBR_CI = 1;
		public static final int CICR_XRF_ID_CI = 1;
		public static final int XRF_TYP_CD_CI = 1;
		public static final int CICR_EFF_DT_CI = 1;
		public static final int CLT_CLT_RELATION_KEY = CLIENT_ID_CI + CLIENT_ID + CLT_TYP_CD_CI + CLT_TYP_CD + HISTORY_VLD_NBR_CI
				+ HISTORY_VLD_NBR_SIGNED + CICR_XRF_ID_CI + CICR_XRF_ID + XRF_TYP_CD_CI + XRF_TYP_CD + CICR_EFF_DT_CI + CICR_EFF_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
