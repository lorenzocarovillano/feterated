/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotPolFrm1;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ACT_NOT_POL_FRM]
 * 
 */
public class ActNotPolFrm1Dao extends BaseSqlDao<IActNotPolFrm1> {

	private Cursor actNotPolFrmCsr;
	private final IRowMapper<IActNotPolFrm1> fetchActNotPolFrmCsrRm = buildNamedRowMapper(IActNotPolFrm1.class, "csrActNbr", "notPrcTs", "frmSeqNbr",
			"polNbr");

	public ActNotPolFrm1Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotPolFrm1> getToClass() {
		return IActNotPolFrm1.class;
	}

	public DbAccessStatus openActNotPolFrmCsr(String csrActNbr, String notPrcTs, short frmSeqNbr, String polNbr) {
		actNotPolFrmCsr = buildQuery("openActNotPolFrmCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr)
				.bind("polNbr", polNbr).open();
		return dbStatus;
	}

	public DbAccessStatus closeActNotPolFrmCsr() {
		return closeCursor(actNotPolFrmCsr);
	}

	public IActNotPolFrm1 fetchActNotPolFrmCsr(IActNotPolFrm1 iActNotPolFrm1) {
		return fetch(actNotPolFrmCsr, iActNotPolFrm1, fetchActNotPolFrmCsrRm);
	}

	public DbAccessStatus insertRec(String csrActNbr, String notPrcTs, short frmSeqNbr, String polNbr) {
		return buildQuery("insertRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr).bind("polNbr", polNbr)
				.executeInsert();
	}

	public DbAccessStatus deleteRec(String csrActNbr, String notPrcTs, short frmSeqNbr, String polNbr) {
		return buildQuery("deleteRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr).bind("polNbr", polNbr)
				.executeDelete();
	}

	public DbAccessStatus deleteRec1(String csrActNbr, String notPrcTs, short frmSeqNbr) {
		return buildQuery("deleteRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr).executeDelete();
	}

	public DbAccessStatus deleteRec2(String csrActNbr, String notPrcTs, String polNbr) {
		return buildQuery("deleteRec2").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).executeDelete();
	}

	public DbAccessStatus deleteRec3(String csrActNbr, String notPrcTs) {
		return buildQuery("deleteRec3").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).executeDelete();
	}

	public IActNotPolFrm1 selectRec(String csrActNbr, String notPrcTs, short frmSeqNbr, String polNbr, IActNotPolFrm1 iActNotPolFrm1) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr).bind("polNbr", polNbr)
				.singleResult(iActNotPolFrm1);
	}
}
