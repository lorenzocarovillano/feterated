/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-END-OF-CURSOR-WORD-FLAG<br>
 * Variable: SW-END-OF-CURSOR-WORD-FLAG from program XZ0B9010<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwEndOfCursorWordFlag {

	//==== PROPERTIES ====
	private char value = 'N';
	public static final char END_OF_CURSOR_WORD = 'Y';
	public static final char NOT_END_OF_CURSOR_WORD = 'N';

	//==== METHODS ====
	public void setSwEndOfCursorWordFlag(char swEndOfCursorWordFlag) {
		this.value = swEndOfCursorWordFlag;
	}

	public char getSwEndOfCursorWordFlag() {
		return this.value;
	}

	public boolean isEndOfCursorWord() {
		return value == END_OF_CURSOR_WORD;
	}
}
