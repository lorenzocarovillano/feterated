/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZY800-WARNINGS<br>
 * Variables: XZY800-WARNINGS from copybook XZ0Y8000<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzy800Warnings {

	//==== PROPERTIES ====
	//Original name: XZY800-WARN-MSG
	private String xzy800WarnMsg = DefaultValues.stringVal(Len.XZY800_WARN_MSG);

	//==== METHODS ====
	public void setWarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzy800WarnMsg = MarshalByte.readString(buffer, position, Len.XZY800_WARN_MSG);
	}

	public byte[] getWarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzy800WarnMsg, Len.XZY800_WARN_MSG);
		return buffer;
	}

	public void initWarningsSpaces() {
		xzy800WarnMsg = "";
	}

	public void setXzy800WarnMsg(String xzy800WarnMsg) {
		this.xzy800WarnMsg = Functions.subString(xzy800WarnMsg, Len.XZY800_WARN_MSG);
	}

	public String getXzy800WarnMsg() {
		return this.xzy800WarnMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY800_WARN_MSG = 400;
		public static final int WARNINGS = XZY800_WARN_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
