/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: USEC-ACTION<br>
 * Variable: USEC-ACTION from copybook HALLUSEC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UsecAction {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.ACTION);
	public static final String USER_CLIENT_ID = "001";
	public static final String CLIENT_USER_ID = "002";
	public static final String USER_UOW_AUTH_ASSOC = "003";
	public static final String NON_FRMWK_AUTH = "004";

	//==== METHODS ====
	public void setAction(short action) {
		this.value = NumericDisplay.asString(action, Len.ACTION);
	}

	public void setActionFormatted(String action) {
		this.value = Trunc.toUnsignedNumeric(action, Len.ACTION);
	}

	public short getAction() {
		return NumericDisplay.asShort(this.value);
	}

	public void setUserClientId() {
		setActionFormatted(USER_CLIENT_ID);
	}

	public void setUserUowAuthAssoc() {
		setActionFormatted(USER_UOW_AUTH_ASSOC);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACTION = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
