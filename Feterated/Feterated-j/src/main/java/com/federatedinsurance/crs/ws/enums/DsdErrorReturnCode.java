/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DSD-ERROR-RETURN-CODE<br>
 * Variable: DSD-ERROR-RETURN-CODE from copybook TS020DRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class DsdErrorReturnCode {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.ERROR_RETURN_CODE);
	public static final String FATAL_ERROR_CODE = "0300";
	public static final String NLBE_CODE = "0200";
	public static final String WARNING_CODE = "0100";
	public static final String NO_ERROR_CODE = "0000";

	//==== METHODS ====
	public void setErrorReturnCode(short errorReturnCode) {
		this.value = NumericDisplay.asString(errorReturnCode, Len.ERROR_RETURN_CODE);
	}

	public void setErrorReturnCodeFormatted(String errorReturnCode) {
		this.value = Trunc.toUnsignedNumeric(errorReturnCode, Len.ERROR_RETURN_CODE);
	}

	public short getErrorReturnCode() {
		return NumericDisplay.asShort(this.value);
	}

	public String getDsdErrorReturnCodeFormatted() {
		return this.value;
	}

	public boolean isFatalErrorCode() {
		return getDsdErrorReturnCodeFormatted().equals(FATAL_ERROR_CODE);
	}

	public void setFatalErrorCode() {
		setErrorReturnCodeFormatted(FATAL_ERROR_CODE);
	}

	public boolean isNlbeCode() {
		return getDsdErrorReturnCodeFormatted().equals(NLBE_CODE);
	}

	public void setNlbeCode() {
		setErrorReturnCodeFormatted(NLBE_CODE);
	}

	public boolean isWarningCode() {
		return getDsdErrorReturnCodeFormatted().equals(WARNING_CODE);
	}

	public void setWarningCode() {
		setErrorReturnCodeFormatted(WARNING_CODE);
	}

	public boolean isDsdNoErrorCode() {
		return getDsdErrorReturnCodeFormatted().equals(NO_ERROR_CODE);
	}

	public void setNoErrorCode() {
		setErrorReturnCodeFormatted(NO_ERROR_CODE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_RETURN_CODE = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
