/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ0C0003<br>
 * Variable: XZ0C0003 from copybook XZ0C0003<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz0c0003 {

	//==== PROPERTIES ====
	//Original name: XZC003-ACT-NOT-REC-FIXED
	private Xzc002ActNotPolFixed actNotRecFixed = new Xzc002ActNotPolFixed();
	//Original name: XZC003-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC003-ACT-NOT-REC-KEY
	private Xzc003ActNotRecKey actNotRecKey = new Xzc003ActNotRecKey();
	//Original name: XZC003-ACT-NOT-REC-KEY-CI
	private Xzc003ActNotRecKeyCi actNotRecKeyCi = new Xzc003ActNotRecKeyCi();
	//Original name: XZC003-ACT-NOT-REC-DATA
	private Xzc003ActNotRecData actNotRecData = new Xzc003ActNotRecData();
	//Original name: XZC003-EXTENSION-FIELDS
	private Xzc003ExtensionFields extensionFields = new Xzc003ExtensionFields();

	//==== METHODS ====
	public void setActNotRecRowFormatted(String data) {
		byte[] buffer = new byte[Len.ACT_NOT_REC_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.ACT_NOT_REC_ROW);
		setActNotRecRowBytes(buffer, 1);
	}

	public String getActNotRecRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotRecRowBytes());
	}

	/**Original name: XZC003-ACT-NOT-REC-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0C0003 - ACT_NOT_REC TABLE                                   *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 24 Dec 2008 E404GRK   GENERATED                        *
	 *  TO07614 02 Feb 2009 E404DLP   Added in the extension fields    *
	 *  TO07614 09 Mar 2009 E404DLP   Changed ADR-SEQ-NBR to ADR-ID    *
	 *  PP02500 24 Jul 2012 E404BPO   Changed length of CER-NBR        *
	 *  20163.20 13 Jul 2018 E404DMW  UPDATED ADDRESS AND CLIENT IDS   *
	 *                                FROM 20 TO 64                    *
	 * *****************************************************************</pre>*/
	public byte[] getActNotRecRowBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_REC_ROW];
		return getActNotRecRowBytes(buffer, 1);
	}

	public void setActNotRecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotRecFixed.setActNotPolFixedBytes(buffer, position);
		position += Xzc002ActNotPolFixed.Len.ACT_NOT_POL_FIXED;
		setActNotRecDatesBytes(buffer, position);
		position += Len.ACT_NOT_REC_DATES;
		actNotRecKey.setActNotRecKeyBytes(buffer, position);
		position += Xzc003ActNotRecKey.Len.ACT_NOT_REC_KEY;
		actNotRecKeyCi.setActNotRecKeyCiBytes(buffer, position);
		position += Xzc003ActNotRecKeyCi.Len.ACT_NOT_REC_KEY_CI;
		actNotRecData.setActNotRecDataBytes(buffer, position);
		position += Xzc003ActNotRecData.Len.ACT_NOT_REC_DATA;
		extensionFields.setExtensionFieldsBytes(buffer, position);
	}

	public byte[] getActNotRecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotRecFixed.getActNotPolFixedBytes(buffer, position);
		position += Xzc002ActNotPolFixed.Len.ACT_NOT_POL_FIXED;
		getActNotRecDatesBytes(buffer, position);
		position += Len.ACT_NOT_REC_DATES;
		actNotRecKey.getActNotRecKeyBytes(buffer, position);
		position += Xzc003ActNotRecKey.Len.ACT_NOT_REC_KEY;
		actNotRecKeyCi.getActNotRecKeyCiBytes(buffer, position);
		position += Xzc003ActNotRecKeyCi.Len.ACT_NOT_REC_KEY_CI;
		actNotRecData.getActNotRecDataBytes(buffer, position);
		position += Xzc003ActNotRecData.Len.ACT_NOT_REC_DATA;
		extensionFields.getExtensionFieldsBytes(buffer, position);
		return buffer;
	}

	public void setActNotRecDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getActNotRecDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public String getTransProcessDtFormatted() {
		return Functions.padBlanks(getTransProcessDt(), Len.TRANS_PROCESS_DT);
	}

	public Xzc003ActNotRecData getActNotRecData() {
		return actNotRecData;
	}

	public Xzc002ActNotPolFixed getActNotRecFixed() {
		return actNotRecFixed;
	}

	public Xzc003ActNotRecKey getActNotRecKey() {
		return actNotRecKey;
	}

	public Xzc003ActNotRecKeyCi getActNotRecKeyCi() {
		return actNotRecKeyCi;
	}

	public Xzc003ExtensionFields getExtensionFields() {
		return extensionFields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRANS_PROCESS_DT = 10;
		public static final int ACT_NOT_REC_DATES = TRANS_PROCESS_DT;
		public static final int ACT_NOT_REC_ROW = Xzc002ActNotPolFixed.Len.ACT_NOT_POL_FIXED + ACT_NOT_REC_DATES
				+ Xzc003ActNotRecKey.Len.ACT_NOT_REC_KEY + Xzc003ActNotRecKeyCi.Len.ACT_NOT_REC_KEY_CI + Xzc003ActNotRecData.Len.ACT_NOT_REC_DATA
				+ Xzc003ExtensionFields.Len.EXTENSION_FIELDS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
