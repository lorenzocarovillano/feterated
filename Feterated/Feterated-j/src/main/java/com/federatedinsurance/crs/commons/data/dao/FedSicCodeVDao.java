/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IFedSicCodeV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [FED_SIC_CODE_V]
 * 
 */
public class FedSicCodeVDao extends BaseSqlDao<IFedSicCodeV> {

	public FedSicCodeVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IFedSicCodeV> getToClass() {
		return IFedSicCodeV.class;
	}

	public String selectByTobCd(String tobCd, String dft) {
		return buildQuery("selectByTobCd").bind("tobCd", tobCd).scalarResultString(dft);
	}
}
