/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-VPS-REPORT-FILE-STATUS-FLAG<br>
 * Variable: SW-VPS-REPORT-FILE-STATUS-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwVpsReportFileStatusFlag {

	//==== PROPERTIES ====
	private char value = '0';
	public static final char CLOSED = '0';
	public static final char OPEN = '1';

	//==== METHODS ====
	public void setVpsReportFileStatusFlag(char vpsReportFileStatusFlag) {
		this.value = vpsReportFileStatusFlag;
	}

	public char getVpsReportFileStatusFlag() {
		return this.value;
	}

	public void setClosed() {
		value = CLOSED;
	}

	public boolean isOpen() {
		return value == OPEN;
	}

	public void setOpen() {
		value = OPEN;
	}
}
