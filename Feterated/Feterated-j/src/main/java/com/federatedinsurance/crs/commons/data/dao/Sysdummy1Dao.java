/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.ISysdummy1;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [SYSDUMMY1]
 * 
 */
public class Sysdummy1Dao extends BaseSqlDao<ISysdummy1> {

	private final IRowMapper<ISysdummy1> selectRec1Rm = buildNamedRowMapper(ISysdummy1.class, "currentDt", "oneYearAgoDt");

	public Sysdummy1Dao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ISysdummy1> getToClass() {
		return ISysdummy1.class;
	}

	public short selectRec(short dft) {
		return buildQuery("selectRec").scalarResultShort(dft);
	}

	public String selectBySdStartTimestamp(String sdStartTimestamp, String dft) {
		return buildQuery("selectBySdStartTimestamp").bind("sdStartTimestamp", sdStartTimestamp).scalarResultString(dft);
	}

	public ISysdummy1 selectRec1(ISysdummy1 iSysdummy1) {
		return buildQuery("selectRec1").rowMapper(selectRec1Rm).singleResult(iSysdummy1);
	}

	public String selectByWsNbrOfNotDay(short wsNbrOfNotDay, String dft) {
		return buildQuery("selectByWsNbrOfNotDay").bind("wsNbrOfNotDay", wsNbrOfNotDay).scalarResultString(dft);
	}

	public int selectByWsMinPolEffDt(String wsMinPolEffDt, int dft) {
		return buildQuery("selectByWsMinPolEffDt").bind("wsMinPolEffDt", wsMinPolEffDt).scalarResultInt(dft);
	}

	public String selectRec2(String dft) {
		return buildQuery("selectRec2").scalarResultString(dft);
	}
}
