/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-END-OF-CURSOR3-SW<br>
 * Variable: WS-END-OF-CURSOR3-SW from program HALRLOMG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEndOfCursor3Sw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_OF_CURSOR3 = 'Y';
	public static final char NOT_END_OF_CURSOR3 = 'N';

	//==== METHODS ====
	public void setEndOfCursor3Sw(char endOfCursor3Sw) {
		this.value = endOfCursor3Sw;
	}

	public char getEndOfCursor3Sw() {
		return this.value;
	}

	public boolean isEndOfCursor3() {
		return value == END_OF_CURSOR3;
	}

	public void setEndOfCursor3() {
		value = END_OF_CURSOR3;
	}

	public void setNotEndOfCursor3() {
		value = NOT_END_OF_CURSOR3;
	}
}
