/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: TC-CERT-POLICIES<br>
 * Variable: TC-CERT-POLICIES from program XZ0P90E0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TcCertPolicies {

	//==== PROPERTIES ====
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TC_CERT_POLICIES);
	//Original name: TC-POLICY-NUMBER
	private String policyNumber = DefaultValues.stringVal(Len.POLICY_NUMBER);

	//==== METHODS ====
	public String getTcCertPoliciesFormatted() {
		return getPolicyNumberFormatted();
	}

	public void initTcCertPoliciesHighValues() {
		policyNumber = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POLICY_NUMBER);
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTcCertPoliciesFormatted()).equals(END_OF_TABLE);
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = Functions.subString(policyNumber, Len.POLICY_NUMBER);
	}

	public String getPolicyNumber() {
		return this.policyNumber;
	}

	public String getPolicyNumberFormatted() {
		return Functions.padBlanks(getPolicyNumber(), Len.POLICY_NUMBER);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POLICY_NUMBER = 25;
		public static final int TC_CERT_POLICIES = POLICY_NUMBER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
