/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-UHDR-BROWSE-SW<br>
 * Variable: WS-UHDR-BROWSE-SW from program HALRRESP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsUhdrBrowseSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char OK = 'Y';
	public static final char NOT_OK = 'N';

	//==== METHODS ====
	public void setsUhdrBrowseSw(char sUhdrBrowseSw) {
		this.value = sUhdrBrowseSw;
	}

	public char getsUhdrBrowseSw() {
		return this.value;
	}

	public boolean isOk() {
		return value == OK;
	}

	public void setOk() {
		value = OK;
	}

	public void setNotOk() {
		value = NOT_OK;
	}
}
