/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-PRTR-FILE-OPEN-FLAG<br>
 * Variable: SW-PRTR-FILE-OPEN-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwPrtrFileOpenFlag {

	//==== PROPERTIES ====
	private char value = '0';
	public static final char NOT_OPENED = '0';
	public static final char OPENED = '1';

	//==== METHODS ====
	public void setPrtrFileOpenFlag(char prtrFileOpenFlag) {
		this.value = prtrFileOpenFlag;
	}

	public char getPrtrFileOpenFlag() {
		return this.value;
	}

	public boolean isNotOpened() {
		return value == NOT_OPENED;
	}

	public void setOpened() {
		value = OPENED;
	}
}
