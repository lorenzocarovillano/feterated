/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IActNot;
import com.federatedinsurance.crs.commons.data.to.IActNotTyp;
import com.federatedinsurance.crs.commons.data.to.IRfrSeg;
import com.federatedinsurance.crs.commons.data.to.IRfrStPvnTer;
import com.federatedinsurance.crs.copy.DclhalBoAudTgrV;
import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclhalUowObjHierV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Halluchs;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Halluidg;
import com.federatedinsurance.crs.copy.Hallukrp;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xz0c0002;
import com.federatedinsurance.crs.copy.Xz0c0003;
import com.federatedinsurance.crs.copy.Xz0n0001;
import com.federatedinsurance.crs.copy.Xzc001ActNotRow;
import com.federatedinsurance.crs.copy.Xzc006ActNotFrmRow;
import com.federatedinsurance.crs.copy.Xzh001ActNotRow;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.redefines.CidpTableInfo;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0D0001<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0d0001Data implements IActNot, IActNotTyp, IRfrSeg, IRfrStPvnTer {

	//==== PROPERTIES ====
	//Original name: AA-LENGTH
	private short aaLength = DefaultValues.BIN_SHORT_VAL;
	//Original name: AA-SPACES
	private short aaSpaces = ((short) 0);
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0d0001 constantFields = new ConstantFieldsXz0d0001();
	//Original name: EA-01-INV-PDC-NBR
	private Ea01InvPdcNbr ea01InvPdcNbr = new Ea01InvPdcNbr();
	//Original name: EA-02-INV-ACT-TYP-CD
	private Ea02InvActTypCd ea02InvActTypCd = new Ea02InvActTypCd();
	//Original name: WS-SPECIFIC-MISC
	private WsSpecificMiscXz0d0001 wsSpecificMisc = new WsSpecificMiscXz0d0001();
	//Original name: XZC001-ACT-NOT-ROW
	private Xzc001ActNotRow xzc001ActNotRow = new Xzc001ActNotRow();
	//Original name: XZ0C0002
	private Xz0c0002 xz0c0002 = new Xz0c0002();
	//Original name: XZ0C0003
	private Xz0c0003 xz0c0003 = new Xz0c0003();
	//Original name: XZC006-ACT-NOT-FRM-ROW
	private Xzc006ActNotFrmRow xzc006ActNotFrmRow = new Xzc006ActNotFrmRow();
	//Original name: XZ0N0001
	private Xz0n0001 xz0n0001 = new Xz0n0001();
	//Original name: XZH001-ACT-NOT-ROW
	private Xzh001ActNotRow xzh001ActNotRow = new Xzh001ActNotRow();
	//Original name: WS-BDO-WORK-FIELDS
	private WsBdoWorkFields wsBdoWorkFields = new WsBdoWorkFields();
	//Original name: WS-BDO-SWITCHES
	private WsBdoSwitches wsBdoSwitches = new WsBdoSwitches();
	//Original name: DCLHAL-BO-AUD-TGR-V
	private DclhalBoAudTgrV dclhalBoAudTgrV = new DclhalBoAudTgrV();
	//Original name: HALLUCHS
	private Halluchs halluchs = new Halluchs();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();
	//Original name: HALLUKRP
	private Hallukrp hallukrp = new Hallukrp();
	//Original name: HALRLODR-LOCK-DRIVER-STORAGE
	private WsHalrlodrLinkage halrlodrLockDriverStorage = new WsHalrlodrLinkage();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: HALLUSW
	private Hallusw hallusw = new Hallusw();
	//Original name: HALLUDAT
	private Halludat halludat = new Halludat();
	//Original name: HALLUHDR
	private Halluhdr halluhdr = new Halluhdr();
	//Original name: CIDP-TABLE-INFO
	private CidpTableInfo cidpTableInfo = new CidpTableInfo();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: HALRMON-PERF-MONITOR-STORAGE
	private HalrmonPerfMonitorStorage halrmonPerfMonitorStorage = new HalrmonPerfMonitorStorage();
	//Original name: DATE-STRUCTURE
	private DateStructureCawpcorc dateStructure = new DateStructureCawpcorc();
	//Original name: WS-SE3-CUR-ISO-DATE
	private String wsSe3CurIsoDate = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_DATE);
	//Original name: WS-SE3-CUR-ISO-TIME
	private String wsSe3CurIsoTime = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_TIME);
	//Original name: DCLHAL-UOW-OBJ-HIER-V
	private DclhalUowObjHierV dclhalUowObjHierV = new DclhalUowObjHierV();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public void setAaLength(short aaLength) {
		this.aaLength = aaLength;
	}

	public short getAaLength() {
		return this.aaLength;
	}

	public void setAaSpaces(short aaSpaces) {
		this.aaSpaces = aaSpaces;
	}

	public short getAaSpaces() {
		return this.aaSpaces;
	}

	public String getWsActNotPolCommCopybookFormatted() {
		return xz0c0002.getActNotPolRowFormatted();
	}

	public String getWsActNotRecCommCopybookFormatted() {
		return xz0c0003.getActNotRecRowFormatted();
	}

	public String getWsActNotFrmCommCopybookFormatted() {
		return xzc006ActNotFrmRow.getXzc006ActNotFrmRowFormatted();
	}

	public void setHalouchsLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.HALOUCHS_LINKAGE);
		setHalouchsLinkageBytes(buffer, 1);
	}

	public String getHalouchsLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHalouchsLinkageBytes());
	}

	/**Original name: HALOUCHS-LINKAGE<br>*/
	public byte[] getHalouchsLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		return getHalouchsLinkageBytes(buffer, 1);
	}

	public void setHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluchs.setLength2(MarshalByte.readBinaryShort(buffer, position));
		position += Types.SHORT_SIZE;
		halluchs.setStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		halluchs.checkSum = MarshalByte.readFixedString(buffer, position, Halluchs.Len.CHECK_SUM);
	}

	public byte[] getHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, halluchs.getLength2());
		position += Types.SHORT_SIZE;
		halluchs.getStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		MarshalByte.writeString(buffer, position, halluchs.checkSum, Halluchs.Len.CHECK_SUM);
		return buffer;
	}

	public String getHaloukrpLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHaloukrpLinkageBytes());
	}

	/**Original name: HALOUKRP-LINKAGE<br>
	 * <pre>*         07 FILLER                       PIC  X(4).</pre>*/
	public byte[] getHaloukrpLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUKRP_LINKAGE];
		return getHaloukrpLinkageBytes(buffer, 1);
	}

	public byte[] getHaloukrpLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallukrp.getInputFieldsBytes(buffer, position);
		position += Hallukrp.Len.INPUT_FIELDS;
		hallukrp.getInputOutputFieldsBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-DATA-UMT-AREA<br>
	 * <pre>* DATA RESPONSE UMT AREA</pre>*/
	public byte[] getWsDataUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_DATA_UMT_AREA];
		return getWsDataUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsDataUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		halludat.getCommonBytes(buffer, position);
		return buffer;
	}

	public void initWsDataUmtAreaSpaces() {
		halludat.initHalludatSpaces();
	}

	public void initWsHdrUmtAreaSpaces() {
		halluhdr.initHalluhdrSpaces();
	}

	public void setWsDataPrivacyInfoFormatted(String data) {
		byte[] buffer = new byte[Len.WS_DATA_PRIVACY_INFO];
		MarshalByte.writeString(buffer, 1, data, Len.WS_DATA_PRIVACY_INFO);
		setWsDataPrivacyInfoBytes(buffer, 1);
	}

	public String getWsDataPrivacyInfoFormatted() {
		return getCidpPassedInfoFormatted();
	}

	public void setWsDataPrivacyInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setCidpPassedInfoBytes(buffer, position);
	}

	public String getCidpPassedInfoFormatted() {
		return MarshalByteExt.bufferToStr(getCidpPassedInfoBytes());
	}

	/**Original name: CIDP-PASSED-INFO<br>
	 * <pre>****************************************************************
	 *  HALLCIDP                                                      *
	 *  COMMON INTERFACE TO DATA PRIVACY                              *
	 *  USED BY BUSINESS DATA OBJECTS TO PASS DATA ROWS FOR           *
	 *  DATA PRIVACY CHECKING.                                        *
	 * ****************************************************************
	 *  SI#       PRGRMR  DATE       DESCRIPTION                      *
	 *  --------- ------- ---------- ---------------------------------*
	 *  SAVANNAH  PM      31MAY2000  INITIAL CODING                   *
	 *  14969     18448   25JUN2001  MADE CHANGES REQUIRED FOR        *
	 *                               ENHANCED DATA PRIVACY AND SET    *
	 *                               DEFAULTS PROCESSING.             *
	 *                               (ARCHITECTURE 2.3)               *
	 * ****************************************************************</pre>*/
	public byte[] getCidpPassedInfoBytes() {
		byte[] buffer = new byte[Len.CIDP_PASSED_INFO];
		return getCidpPassedInfoBytes(buffer, 1);
	}

	public void setCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.setCidpTableInfoBytes(buffer, position);
	}

	public byte[] getCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.getCidpTableInfoBytes(buffer, position);
		return buffer;
	}

	public void setWsSe3CurIsoDateTimeFormatted(String data) {
		byte[] buffer = new byte[Len.WS_SE3_CUR_ISO_DATE_TIME];
		MarshalByte.writeString(buffer, 1, data, Len.WS_SE3_CUR_ISO_DATE_TIME);
		setWsSe3CurIsoDateTimeBytes(buffer, 1);
	}

	public void setWsSe3CurIsoDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		wsSe3CurIsoDate = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_DATE);
		position += Len.WS_SE3_CUR_ISO_DATE;
		wsSe3CurIsoTime = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_TIME);
	}

	public void setWsSe3CurIsoDate(String wsSe3CurIsoDate) {
		this.wsSe3CurIsoDate = Functions.subString(wsSe3CurIsoDate, Len.WS_SE3_CUR_ISO_DATE);
	}

	public String getWsSe3CurIsoDate() {
		return this.wsSe3CurIsoDate;
	}

	public void setWsSe3CurIsoTime(String wsSe3CurIsoTime) {
		this.wsSe3CurIsoTime = Functions.subString(wsSe3CurIsoTime, Len.WS_SE3_CUR_ISO_TIME);
	}

	public String getWsSe3CurIsoTime() {
		return this.wsSe3CurIsoTime;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getActNotDes() {
		throw new FieldNotMappedException("actNotDes");
	}

	@Override
	public void setActNotDes(String actNotDes) {
		throw new FieldNotMappedException("actNotDes");
	}

	@Override
	public String getActNotPthCd() {
		throw new FieldNotMappedException("actNotPthCd");
	}

	@Override
	public void setActNotPthCd(String actNotPthCd) {
		throw new FieldNotMappedException("actNotPthCd");
	}

	@Override
	public String getActNotPthDes() {
		throw new FieldNotMappedException("actNotPthDes");
	}

	@Override
	public void setActNotPthDes(String actNotPthDes) {
		throw new FieldNotMappedException("actNotPthDes");
	}

	@Override
	public String getActNotStaCd() {
		return xzh001ActNotRow.getActNotStaCd();
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		this.xzh001ActNotRow.setActNotStaCd(actNotStaCd);
	}

	@Override
	public String getActNotTypCd() {
		return xzh001ActNotRow.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.xzh001ActNotRow.setActNotTypCd(actNotTypCd);
	}

	@Override
	public String getActOwnAdrId() {
		return xzh001ActNotRow.getActOwnAdrId();
	}

	@Override
	public void setActOwnAdrId(String actOwnAdrId) {
		this.xzh001ActNotRow.setActOwnAdrId(actOwnAdrId);
	}

	@Override
	public String getActOwnCltId() {
		return xzh001ActNotRow.getActOwnCltId();
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		this.xzh001ActNotRow.setActOwnCltId(actOwnCltId);
	}

	@Override
	public String getActTypCd() {
		return xzh001ActNotRow.getActTypCd();
	}

	@Override
	public void setActTypCd(String actTypCd) {
		this.xzh001ActNotRow.setActTypCd(actTypCd);
	}

	@Override
	public String getActTypCdObj() {
		if (xzh001ActNotRow.getActTypCdNi() >= 0) {
			return getActTypCd();
		} else {
			return null;
		}
	}

	@Override
	public void setActTypCdObj(String actTypCdObj) {
		if (actTypCdObj != null) {
			setActTypCd(actTypCdObj);
			xzh001ActNotRow.setActTypCdNi(((short) 0));
		} else {
			xzh001ActNotRow.setActTypCdNi(((short) -1));
		}
	}

	@Override
	public short getAddCncDay() {
		return xzh001ActNotRow.getAddCncDay();
	}

	@Override
	public void setAddCncDay(short addCncDay) {
		this.xzh001ActNotRow.setAddCncDay(addCncDay);
	}

	@Override
	public Short getAddCncDayObj() {
		if (xzh001ActNotRow.getAddCncDayNi() >= 0) {
			return (getAddCncDay());
		} else {
			return null;
		}
	}

	@Override
	public void setAddCncDayObj(Short addCncDayObj) {
		if (addCncDayObj != null) {
			setAddCncDay((addCncDayObj));
			xzh001ActNotRow.setAddCncDayNi(((short) 0));
		} else {
			xzh001ActNotRow.setAddCncDayNi(((short) -1));
		}
	}

	@Override
	public char getCerHldNotInd() {
		return xzh001ActNotRow.getCerHldNotInd();
	}

	@Override
	public void setCerHldNotInd(char cerHldNotInd) {
		this.xzh001ActNotRow.setCerHldNotInd(cerHldNotInd);
	}

	@Override
	public Character getCerHldNotIndObj() {
		if (xzh001ActNotRow.getCerHldNotIndNi() >= 0) {
			return (getCerHldNotInd());
		} else {
			return null;
		}
	}

	@Override
	public void setCerHldNotIndObj(Character cerHldNotIndObj) {
		if (cerHldNotIndObj != null) {
			setCerHldNotInd((cerHldNotIndObj));
			xzh001ActNotRow.setCerHldNotIndNi(((short) 0));
		} else {
			xzh001ActNotRow.setCerHldNotIndNi(((short) -1));
		}
	}

	public CidpTableInfo getCidpTableInfo() {
		return cidpTableInfo;
	}

	public ConstantFieldsXz0d0001 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		return xzh001ActNotRow.getCsrActNbr();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.xzh001ActNotRow.setCsrActNbr(csrActNbr);
	}

	public DateStructureCawpcorc getDateStructure() {
		return dateStructure;
	}

	public DclhalBoAudTgrV getDclhalBoAudTgrV() {
		return dclhalBoAudTgrV;
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclhalUowObjHierV getDclhalUowObjHierV() {
		return dclhalUowObjHierV;
	}

	@Override
	public String getDocDes() {
		throw new FieldNotMappedException("docDes");
	}

	@Override
	public void setDocDes(String docDes) {
		throw new FieldNotMappedException("docDes");
	}

	@Override
	public String getDocDesObj() {
		return getDocDes();
	}

	@Override
	public void setDocDesObj(String docDesObj) {
		setDocDes(docDesObj);
	}

	@Override
	public short getDsyOrdNbr() {
		throw new FieldNotMappedException("dsyOrdNbr");
	}

	@Override
	public void setDsyOrdNbr(short dsyOrdNbr) {
		throw new FieldNotMappedException("dsyOrdNbr");
	}

	@Override
	public Short getDsyOrdNbrObj() {
		return (getDsyOrdNbr());
	}

	@Override
	public void setDsyOrdNbrObj(Short dsyOrdNbrObj) {
		setDsyOrdNbr((dsyOrdNbrObj));
	}

	public Ea01InvPdcNbr getEa01InvPdcNbr() {
		return ea01InvPdcNbr;
	}

	public Ea02InvActTypCd getEa02InvActTypCd() {
		return ea02InvActTypCd;
	}

	@Override
	public String getEmpId() {
		return xzh001ActNotRow.getEmpId();
	}

	@Override
	public void setEmpId(String empId) {
		this.xzh001ActNotRow.setEmpId(empId);
	}

	@Override
	public String getEmpIdObj() {
		if (xzh001ActNotRow.getEmpIdNi() >= 0) {
			return getEmpId();
		} else {
			return null;
		}
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		if (empIdObj != null) {
			setEmpId(empIdObj);
			xzh001ActNotRow.setEmpIdNi(((short) 0));
		} else {
			xzh001ActNotRow.setEmpIdNi(((short) -1));
		}
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Halluchs getHalluchs() {
		return halluchs;
	}

	public Halludat getHalludat() {
		return halludat;
	}

	public Halluhdr getHalluhdr() {
		return halluhdr;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public Hallukrp getHallukrp() {
		return hallukrp;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	public WsHalrlodrLinkage getHalrlodrLockDriverStorage() {
		return halrlodrLockDriverStorage;
	}

	public HalrmonPerfMonitorStorage getHalrmonPerfMonitorStorage() {
		return halrmonPerfMonitorStorage;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotDt() {
		return xzh001ActNotRow.getNotDt();
	}

	@Override
	public void setNotDt(String notDt) {
		this.xzh001ActNotRow.setNotDt(notDt);
	}

	@Override
	public String getNotPrcTs() {
		return xzh001ActNotRow.getNotPrcTs();
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.xzh001ActNotRow.setNotPrcTs(notPrcTs);
	}

	@Override
	public String getPdcNbr() {
		return xzh001ActNotRow.getPdcNbr();
	}

	@Override
	public void setPdcNbr(String pdcNbr) {
		this.xzh001ActNotRow.setPdcNbr(pdcNbr);
	}

	@Override
	public String getPdcNbrObj() {
		if (xzh001ActNotRow.getPdcNbrNi() >= 0) {
			return getPdcNbr();
		} else {
			return null;
		}
	}

	@Override
	public void setPdcNbrObj(String pdcNbrObj) {
		if (pdcNbrObj != null) {
			setPdcNbr(pdcNbrObj);
			xzh001ActNotRow.setPdcNbrNi(((short) 0));
		} else {
			xzh001ActNotRow.setPdcNbrNi(((short) -1));
		}
	}

	@Override
	public String getPdcNm() {
		return FixedStrings.get(xzh001ActNotRow.getPdcNmText(), xzh001ActNotRow.getPdcNmLen());
	}

	@Override
	public void setPdcNm(String pdcNm) {
		this.xzh001ActNotRow.setPdcNmText(pdcNm);
		this.xzh001ActNotRow.setPdcNmLen((((short) strLen(pdcNm))));
	}

	@Override
	public String getReaDes() {
		return FixedStrings.get(xzh001ActNotRow.getReaDesText(), xzh001ActNotRow.getReaDesLen());
	}

	@Override
	public void setReaDes(String reaDes) {
		this.xzh001ActNotRow.setReaDesText(reaDes);
		this.xzh001ActNotRow.setReaDesLen((((short) strLen(reaDes))));
	}

	@Override
	public String getSaNotPrcTsTime() {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	@Override
	public void setSaNotPrcTsTime(String saNotPrcTsTime) {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	@Override
	public String getSegCd() {
		return xzh001ActNotRow.getSegCd();
	}

	@Override
	public void setSegCd(String segCd) {
		this.xzh001ActNotRow.setSegCd(segCd);
	}

	@Override
	public String getSegCdObj() {
		if (xzh001ActNotRow.getSegCdNi() >= 0) {
			return getSegCd();
		} else {
			return null;
		}
	}

	@Override
	public void setSegCdObj(String segCdObj) {
		if (segCdObj != null) {
			setSegCd(segCdObj);
			xzh001ActNotRow.setSegCdNi(((short) 0));
		} else {
			xzh001ActNotRow.setSegCdNi(((short) -1));
		}
	}

	@Override
	public String getStAbb() {
		return xzh001ActNotRow.getStAbb();
	}

	@Override
	public void setStAbb(String stAbb) {
		this.xzh001ActNotRow.setStAbb(stAbb);
	}

	@Override
	public String getStAbbObj() {
		if (xzh001ActNotRow.getStAbbNi() >= 0) {
			return getStAbb();
		} else {
			return null;
		}
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		if (stAbbObj != null) {
			setStAbb(stAbbObj);
			xzh001ActNotRow.setStAbbNi(((short) 0));
		} else {
			xzh001ActNotRow.setStAbbNi(((short) -1));
		}
	}

	@Override
	public String getStaMdfTs() {
		return xzh001ActNotRow.getStaMdfTs();
	}

	@Override
	public void setStaMdfTs(String staMdfTs) {
		this.xzh001ActNotRow.setStaMdfTs(staMdfTs);
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		return xzh001ActNotRow.getTotFeeAmt();
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		this.xzh001ActNotRow.setTotFeeAmt(totFeeAmt.copy());
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		if (xzh001ActNotRow.getTotFeeAmtNi() >= 0) {
			return getTotFeeAmt().toString();
		} else {
			return null;
		}
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		if (totFeeAmtObj != null) {
			setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
			xzh001ActNotRow.setTotFeeAmtNi(((short) 0));
		} else {
			xzh001ActNotRow.setTotFeeAmtNi(((short) -1));
		}
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WsBdoSwitches getWsBdoSwitches() {
		return wsBdoSwitches;
	}

	public WsBdoWorkFields getWsBdoWorkFields() {
		return wsBdoWorkFields;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSpecificMiscXz0d0001 getWsSpecificMisc() {
		return wsSpecificMisc;
	}

	public Xz0c0002 getXz0c0002() {
		return xz0c0002;
	}

	public Xz0c0003 getXz0c0003() {
		return xz0c0003;
	}

	public Xz0n0001 getXz0n0001() {
		return xz0n0001;
	}

	public Xzc001ActNotRow getXzc001ActNotRow() {
		return xzc001ActNotRow;
	}

	public Xzc006ActNotFrmRow getXzc006ActNotFrmRow() {
		return xzc006ActNotFrmRow;
	}

	public Xzh001ActNotRow getXzh001ActNotRow() {
		return xzh001ActNotRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_SE3_CUR_ISO_DATE_TIME = WS_SE3_CUR_ISO_DATE + WS_SE3_CUR_ISO_TIME;
		public static final int WS_DATA_UMT_AREA = Halludat.Len.COMMON;
		public static final int CIDP_PASSED_INFO = CidpTableInfo.Len.CIDP_TABLE_INFO;
		public static final int WS_DATA_PRIVACY_INFO = CIDP_PASSED_INFO;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;
		public static final int HALOUKRP_LINKAGE = Hallukrp.Len.INPUT_FIELDS + Hallukrp.Len.INPUT_OUTPUT_FIELDS;
		public static final int HALOUCHS_LINKAGE = Halluchs.Len.LENGTH2 + Halluchs.Len.STRING_FLD + Halluchs.Len.CHECK_SUM;
		public static final int WS_ACT_NOT_POL_COMM_COPYBOOK = Xz0c0002.Len.ACT_NOT_POL_ROW;
		public static final int WS_ACT_NOT_FRM_COMM_COPYBOOK = Xzc006ActNotFrmRow.Len.XZC006_ACT_NOT_FRM_ROW;
		public static final int WS_ACT_NOT_REC_COMM_COPYBOOK = Xz0c0003.Len.ACT_NOT_REC_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
