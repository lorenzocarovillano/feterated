/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-18-PIPE-IS-UNUSABLE<br>
 * Variable: EA-18-PIPE-IS-UNUSABLE from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea18PipeIsUnusable {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-18-PIPE-IS-UNUSABLE
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-18-PIPE-IS-UNUSABLE-1
	private String flr2 = "THE PIPE";
	//Original name: FILLER-EA-18-PIPE-IS-UNUSABLE-2
	private String flr3 = "ALLOCATED FOR";
	//Original name: EA-18-CICS-APPL-ID
	private String ea18CicsApplId = DefaultValues.stringVal(Len.EA18_CICS_APPL_ID);
	//Original name: FILLER-EA-18-PIPE-IS-UNUSABLE-3
	private String flr4 = "IS IN AN";
	//Original name: FILLER-EA-18-PIPE-IS-UNUSABLE-4
	private String flr5 = "UNUSABLE STATE.";

	//==== METHODS ====
	public String getEa18PipeIsUnusableFormatted() {
		return MarshalByteExt.bufferToStr(getEa18PipeIsUnusableBytes());
	}

	public byte[] getEa18PipeIsUnusableBytes() {
		byte[] buffer = new byte[Len.EA18_PIPE_IS_UNUSABLE];
		return getEa18PipeIsUnusableBytes(buffer, 1);
	}

	public byte[] getEa18PipeIsUnusableBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea18CicsApplId, Len.EA18_CICS_APPL_ID);
		position += Len.EA18_CICS_APPL_ID;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEa18CicsApplId(String ea18CicsApplId) {
		this.ea18CicsApplId = Functions.subString(ea18CicsApplId, Len.EA18_CICS_APPL_ID);
	}

	public String getEa18CicsApplId() {
		return this.ea18CicsApplId;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA18_CICS_APPL_ID = 8;
		public static final int FLR1 = 11;
		public static final int FLR2 = 9;
		public static final int FLR3 = 14;
		public static final int FLR5 = 15;
		public static final int EA18_PIPE_IS_UNUSABLE = EA18_CICS_APPL_ID + FLR1 + 2 * FLR2 + FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
