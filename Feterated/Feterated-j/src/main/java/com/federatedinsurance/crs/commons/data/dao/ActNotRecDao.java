/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotRec;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ACT_NOT_REC]
 * 
 */
public class ActNotRecDao extends BaseSqlDao<IActNotRec> {

	private Cursor cerRecCsr;
	private Cursor actNotRecCsr;
	private Cursor recipientCrsr;
	private Cursor recipientCsr;
	private final IRowMapper<IActNotRec> fetchCerRecCsrRm = buildNamedRowMapper(IActNotRec.class, "recSeqNbr", "recTypCd", "recNmObj", "lin1AdrObj",
			"lin2AdrObj", "citNmObj", "stAbbObj", "pstCdObj", "cerNbrObj");
	private final IRowMapper<IActNotRec> fetchActNotRecCsrRm = buildNamedRowMapper(IActNotRec.class, "csrActNbr", "notPrcTs", "recSeqNbr", "recTypCd",
			"recCltIdObj", "recAdrIdObj", "recNmObj", "lin1AdrObj", "lin2AdrObj", "citNmObj", "stAbbObj", "pstCdObj", "mnlInd", "cerNbrObj");
	private final IRowMapper<IActNotRec> fetchRecipientCrsrRm = buildNamedRowMapper(IActNotRec.class, "recCltIdObj", "recAdrIdObj", "recNmObj",
			"lin1AdrObj", "citNmObj", "stAbbObj");
	private final IRowMapper<IActNotRec> fetchRecipientCsrRm = buildNamedRowMapper(IActNotRec.class, "recSeqNbr", "recTypCd", "cltIdObj", "adrIdObj");

	public ActNotRecDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotRec> getToClass() {
		return IActNotRec.class;
	}

	public DbAccessStatus openCerRecCsr(String csrActNbr, String notPrcTs, String cfTypeCdCert, String cerNbr) {
		cerRecCsr = buildQuery("openCerRecCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("cfTypeCdCert", cfTypeCdCert)
				.bind("cerNbr", cerNbr).open();
		return dbStatus;
	}

	public IActNotRec fetchCerRecCsr(IActNotRec iActNotRec) {
		return fetch(cerRecCsr, iActNotRec, fetchCerRecCsrRm);
	}

	public DbAccessStatus closeCerRecCsr() {
		return closeCursor(cerRecCsr);
	}

	public IActNotRec selectRec(String csrActNbr, String notPrcTs, IActNotRec iActNotRec) {
	}

	public DbAccessStatus openActNotRecCsr(String csrActNbr, String notPrcTs, short recSeqNbr) {
		actNotRecCsr = buildQuery("openActNotRecCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("recSeqNbr", recSeqNbr).open();
		return dbStatus;
	}

	public DbAccessStatus closeActNotRecCsr() {
		return closeCursor(actNotRecCsr);
	}

	public IActNotRec fetchActNotRecCsr(IActNotRec iActNotRec) {
		return fetch(actNotRecCsr, iActNotRec, fetchActNotRecCsrRm);
	}

	public DbAccessStatus insertRec(IActNotRec iActNotRec) {
		return buildQuery("insertRec").bind(iActNotRec).executeInsert();
	}

	public DbAccessStatus deleteRec(String csrActNbr, String notPrcTs, short recSeqNbr) {
		return buildQuery("deleteRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("recSeqNbr", recSeqNbr).executeDelete();
	}

	public DbAccessStatus deleteRec1(String csrActNbr, String notPrcTs) {
		return buildQuery("deleteRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).executeDelete();
	}

	public IActNotRec selectRec1(String csrActNbr, String notPrcTs, short recSeqNbr, IActNotRec iActNotRec) {
		return buildQuery("selectRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("recSeqNbr", recSeqNbr).singleResult(iActNotRec);
	}

	public DbAccessStatus openRecipientCrsr(String csrActNbr, String notPrcTs) {
		recipientCrsr = buildQuery("openRecipientCrsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public IActNotRec fetchRecipientCrsr(IActNotRec iActNotRec) {
		return fetch(recipientCrsr, iActNotRec, fetchRecipientCrsrRm);
	}

	public DbAccessStatus closeRecipientCrsr() {
		return closeCursor(recipientCrsr);
	}

	public DbAccessStatus openRecipientCsr(String csrActNbr, String notPrcTs) {
		recipientCsr = buildQuery("openRecipientCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public IActNotRec fetchRecipientCsr(IActNotRec iActNotRec) {
		return fetch(recipientCsr, iActNotRec, fetchRecipientCsrRm);
	}

	public DbAccessStatus closeRecipientCsr() {
		return closeCursor(recipientCsr);
	}

	public String selectRec2(IActNotRec iActNotRec, String dft) {
		return buildQuery("selectRec2").bind(iActNotRec).scalarResultString(dft);
	}

	public short selectRec3(IActNotRec iActNotRec, short dft) {
		return buildQuery("selectRec3").bind(iActNotRec).scalarResultShort(dft);
	}

	public DbAccessStatus updateRec(IActNotRec iActNotRec) {
		return buildQuery("updateRec").bind(iActNotRec).executeUpdate();
	}

	public String selectRec4(String xzh003CsrActNbr, String notPrcTs, short recSeqNbr, String dft) {
		return buildQuery("selectRec4").bind("xzh003CsrActNbr", xzh003CsrActNbr).bind("notPrcTs", notPrcTs).bind("recSeqNbr", recSeqNbr)
				.scalarResultString(dft);
	}
}
