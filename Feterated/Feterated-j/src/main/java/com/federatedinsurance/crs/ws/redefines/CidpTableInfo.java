/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: CIDP-TABLE-INFO<br>
 * Variable: CIDP-TABLE-INFO from program CAWS002<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CidpTableInfo extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public CidpTableInfo() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.CIDP_TABLE_INFO;
	}

	public void setCidpTableInfoBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.CIDP_TABLE_INFO, Pos.CIDP_TABLE_INFO);
	}

	public byte[] getCidpTableInfoBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.CIDP_TABLE_INFO, Pos.CIDP_TABLE_INFO);
		return buffer;
	}

	public void setTableName(String tableName) {
		writeString(Pos.TABLE_NAME, tableName, Len.TABLE_NAME);
	}

	/**Original name: CIDP-TABLE-NAME<br>*/
	public String getTableName() {
		return readString(Pos.TABLE_NAME, Len.TABLE_NAME);
	}

	public void setTableRowLength(short tableRowLength) {
		writeBinaryShort(Pos.TABLE_ROW_LENGTH, tableRowLength);
	}

	/**Original name: CIDP-TABLE-ROW-LENGTH<br>*/
	public short getTableRowLength() {
		return readBinaryShort(Pos.TABLE_ROW_LENGTH);
	}

	public void setTableRow(String tableRow) {
		writeString(Pos.TABLE_ROW, tableRow, Len.TABLE_ROW);
	}

	/**Original name: CIDP-TABLE-ROW<br>*/
	public String getTableRow() {
		return readString(Pos.TABLE_ROW, Len.TABLE_ROW);
	}

	public String getTableRowFormatted() {
		return Functions.padBlanks(getTableRow(), Len.TABLE_ROW);
	}

	public void setObjectName(String objectName) {
		writeString(Pos.OBJECT_NAME, objectName, Len.OBJECT_NAME);
	}

	/**Original name: CIDP-OBJECT-NAME<br>*/
	public String getObjectName() {
		return readString(Pos.OBJECT_NAME, Len.OBJECT_NAME);
	}

	public void setObjectRowLength(short objectRowLength) {
		writeBinaryShort(Pos.OBJECT_ROW_LENGTH, objectRowLength);
	}

	/**Original name: CIDP-OBJECT-ROW-LENGTH<br>*/
	public short getObjectRowLength() {
		return readBinaryShort(Pos.OBJECT_ROW_LENGTH);
	}

	public void setObjectRow(String objectRow) {
		writeString(Pos.OBJECT_ROW, objectRow, Len.OBJECT_ROW);
	}

	/**Original name: CIDP-OBJECT-ROW<br>*/
	public String getObjectRow() {
		return readString(Pos.OBJECT_ROW, Len.OBJECT_ROW);
	}

	public String getObjectRowFormatted() {
		return Functions.padBlanks(getObjectRow(), Len.OBJECT_ROW);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int CIDP_TABLE_INFO = 1;
		public static final int TABLE_NAME = CIDP_TABLE_INFO;
		public static final int TABLE_ROW_LENGTH = TABLE_NAME + Len.TABLE_NAME;
		public static final int TABLE_ROW = TABLE_ROW_LENGTH + Len.TABLE_ROW_LENGTH;
		public static final int CIDP_OBJECT_INFO = 1;
		public static final int OBJECT_NAME = CIDP_OBJECT_INFO;
		public static final int OBJECT_ROW_LENGTH = OBJECT_NAME + Len.OBJECT_NAME;
		public static final int OBJECT_ROW = OBJECT_ROW_LENGTH + Len.OBJECT_ROW_LENGTH;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int TABLE_NAME = 32;
		public static final int TABLE_ROW_LENGTH = 2;
		public static final int OBJECT_NAME = 32;
		public static final int OBJECT_ROW_LENGTH = 2;
		public static final int TABLE_ROW = 4963;
		public static final int CIDP_TABLE_INFO = TABLE_NAME + TABLE_ROW_LENGTH + TABLE_ROW;
		public static final int OBJECT_ROW = 4963;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
