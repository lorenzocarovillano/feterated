/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BXT03O-FEE-INF<br>
 * Variables: BXT03O-FEE-INF from copybook BX0T0003<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Bxt03oFeeInf {

	//==== PROPERTIES ====
	//Original name: BXT03O-FEE-TYPE
	private String type = DefaultValues.stringVal(Len.TYPE);
	//Original name: BXT03O-FEE-AMT
	private AfDecimal amt = new AfDecimal(DefaultValues.DEC_VAL, 17, 2);

	//==== METHODS ====
	public void setFeeInfBytes(byte[] buffer, int offset) {
		int position = offset;
		type = MarshalByte.readString(buffer, position, Len.TYPE);
		position += Len.TYPE;
		amt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.AMT, Len.Fract.AMT));
	}

	public byte[] getFeeInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, type, Len.TYPE);
		position += Len.TYPE;
		MarshalByte.writeDecimal(buffer, position, amt.copy());
		return buffer;
	}

	public void initFeeInfSpaces() {
		type = "";
		amt.setNaN();
	}

	public void setType(String type) {
		this.type = Functions.subString(type, Len.TYPE);
	}

	public String getType() {
		return this.type;
	}

	public void setAmt(AfDecimal amt) {
		this.amt.assign(amt);
	}

	public AfDecimal getAmt() {
		return this.amt.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TYPE = 3;
		public static final int AMT = 17;
		public static final int FEE_INF = TYPE + AMT;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int AMT = 15;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
