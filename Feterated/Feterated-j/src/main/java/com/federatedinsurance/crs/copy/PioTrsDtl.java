/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: PIO-TRS-DTL<br>
 * Variable: PIO-TRS-DTL from copybook XZC0690O<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class PioTrsDtl {

	//==== PROPERTIES ====
	//Original name: PIO-TRS-TYP-CD
	private char trsTypCd = DefaultValues.CHAR_VAL;
	//Original name: PIO-TRS-TYP-DES
	private String trsTypDes = DefaultValues.stringVal(Len.TRS_TYP_DES);
	//Original name: PIO-PND-FLG
	private char pndFlg = DefaultValues.CHAR_VAL;
	//Original name: PIO-TRS-ORT-CD
	private char trsOrtCd = DefaultValues.CHAR_VAL;
	//Original name: PIO-TRS-ORT-DES
	private String trsOrtDes = DefaultValues.stringVal(Len.TRS_ORT_DES);

	//==== METHODS ====
	public void setTrsDtlBytes(byte[] buffer, int offset) {
		int position = offset;
		setTrsTypBytes(buffer, position);
		position += Len.TRS_TYP;
		pndFlg = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		setTrsOrtSysBytes(buffer, position);
	}

	public byte[] getTrsDtlBytes(byte[] buffer, int offset) {
		int position = offset;
		getTrsTypBytes(buffer, position);
		position += Len.TRS_TYP;
		MarshalByte.writeChar(buffer, position, pndFlg);
		position += Types.CHAR_SIZE;
		getTrsOrtSysBytes(buffer, position);
		return buffer;
	}

	public void initTrsDtlSpaces() {
		initTrsTypSpaces();
		pndFlg = Types.SPACE_CHAR;
		initTrsOrtSysSpaces();
	}

	public void setTrsTypBytes(byte[] buffer, int offset) {
		int position = offset;
		trsTypCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		trsTypDes = MarshalByte.readString(buffer, position, Len.TRS_TYP_DES);
	}

	public byte[] getTrsTypBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, trsTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, trsTypDes, Len.TRS_TYP_DES);
		return buffer;
	}

	public void initTrsTypSpaces() {
		trsTypCd = Types.SPACE_CHAR;
		trsTypDes = "";
	}

	public void setTrsTypCd(char trsTypCd) {
		this.trsTypCd = trsTypCd;
	}

	public char getTrsTypCd() {
		return this.trsTypCd;
	}

	public void setTrsTypDes(String trsTypDes) {
		this.trsTypDes = Functions.subString(trsTypDes, Len.TRS_TYP_DES);
	}

	public String getTrsTypDes() {
		return this.trsTypDes;
	}

	public void setPndFlg(char pndFlg) {
		this.pndFlg = pndFlg;
	}

	public char getPndFlg() {
		return this.pndFlg;
	}

	public void setTrsOrtSysBytes(byte[] buffer, int offset) {
		int position = offset;
		trsOrtCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		trsOrtDes = MarshalByte.readString(buffer, position, Len.TRS_ORT_DES);
	}

	public byte[] getTrsOrtSysBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, trsOrtCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, trsOrtDes, Len.TRS_ORT_DES);
		return buffer;
	}

	public void initTrsOrtSysSpaces() {
		trsOrtCd = Types.SPACE_CHAR;
		trsOrtDes = "";
	}

	public void setTrsOrtCd(char trsOrtCd) {
		this.trsOrtCd = trsOrtCd;
	}

	public char getTrsOrtCd() {
		return this.trsOrtCd;
	}

	public void setTrsOrtDes(String trsOrtDes) {
		this.trsOrtDes = Functions.subString(trsOrtDes, Len.TRS_ORT_DES);
	}

	public String getTrsOrtDes() {
		return this.trsOrtDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRS_TYP_DES = 35;
		public static final int TRS_ORT_DES = 20;
		public static final int TRS_TYP_CD = 1;
		public static final int TRS_TYP = TRS_TYP_CD + TRS_TYP_DES;
		public static final int PND_FLG = 1;
		public static final int TRS_ORT_CD = 1;
		public static final int TRS_ORT_SYS = TRS_ORT_CD + TRS_ORT_DES;
		public static final int TRS_DTL = TRS_TYP + PND_FLG + TRS_ORT_SYS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
