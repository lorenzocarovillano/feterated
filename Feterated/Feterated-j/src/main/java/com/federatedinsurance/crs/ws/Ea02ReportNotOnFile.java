/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-REPORT-NOT-ON-FILE<br>
 * Variable: EA-02-REPORT-NOT-ON-FILE from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea02ReportNotOnFile {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-REPORT-NOT-ON-FILE
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-02-REPORT-NOT-ON-FILE-1
	private String flr2 = "TS030099 -";
	//Original name: FILLER-EA-02-REPORT-NOT-ON-FILE-2
	private String flr3 = "REPORT NUMBER";
	//Original name: FILLER-EA-02-REPORT-NOT-ON-FILE-3
	private char flr4 = Types.QUOTE_CHAR;
	//Original name: EA-02-REPORT-NUMBER
	private String ea02ReportNumber = DefaultValues.stringVal(Len.EA02_REPORT_NUMBER);
	//Original name: FILLER-EA-02-REPORT-NOT-ON-FILE-4
	private char flr5 = Types.QUOTE_CHAR;
	//Original name: FILLER-EA-02-REPORT-NOT-ON-FILE-5
	private String flr6 = " NOT ON FILE.";

	//==== METHODS ====
	public String getEa02ReportNotOnFileFormatted() {
		return MarshalByteExt.bufferToStr(getEa02ReportNotOnFileBytes());
	}

	public byte[] getEa02ReportNotOnFileBytes() {
		byte[] buffer = new byte[Len.EA02_REPORT_NOT_ON_FILE];
		return getEa02ReportNotOnFileBytes(buffer, 1);
	}

	public byte[] getEa02ReportNotOnFileBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeChar(buffer, position, flr4);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ea02ReportNumber, Len.EA02_REPORT_NUMBER);
		position += Len.EA02_REPORT_NUMBER;
		MarshalByte.writeChar(buffer, position, flr5);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public char getFlr4() {
		return this.flr4;
	}

	public void setEa02ReportNumber(String ea02ReportNumber) {
		this.ea02ReportNumber = Functions.subString(ea02ReportNumber, Len.EA02_REPORT_NUMBER);
	}

	public String getEa02ReportNumber() {
		return this.ea02ReportNumber;
	}

	public char getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA02_REPORT_NUMBER = 6;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 14;
		public static final int FLR6 = 13;
		public static final int EA02_REPORT_NOT_ON_FILE = EA02_REPORT_NUMBER + 3 * FLR1 + FLR2 + FLR3 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
