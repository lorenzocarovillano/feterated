/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZC03090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXzc03090 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-POL-COV-INF
	private short maxPolCovInf = ((short) 50);
	//Original name: CF-MAX-COV-INF
	private short maxCovInf = ((short) 10);
	//Original name: CF-MAX-MSG-INF
	private short maxMsgInf = ((short) 50);
	//Original name: CF-SOAP-SERVICE
	private String soapService = "CRSXWCMSInquiryCallable";
	//Original name: CF-SOAP-OPERATION
	private String soapOperation = "GetCertPolicyListByAccount";
	//Original name: CF-CALLABLE-SVC-PROGRAM
	private String callableSvcProgram = "GIICALS";
	//Original name: CF-TRACE-STOR-VALUE
	private String traceStorValue = "GWAO";
	//Original name: CF-PARAGRAPH-NAMES
	private CfParagraphNamesXzc03090 paragraphNames = new CfParagraphNamesXzc03090();
	//Original name: CF-WEB-SVC-ID
	private String webSvcId = "XWCMSINQSVC";
	//Original name: CF-WEB-SVC-LKU-PGM
	private String webSvcLkuPgm = "TS571099";
	//Original name: CF-LV-ZLINUX-SRV
	private String lvZlinuxSrv = "03";
	//Original name: CF-CONTAINER-INFO
	private CfContainerInfoXzc03090 containerInfo = new CfContainerInfoXzc03090();

	//==== METHODS ====
	public short getMaxPolCovInf() {
		return this.maxPolCovInf;
	}

	public short getMaxCovInf() {
		return this.maxCovInf;
	}

	public short getMaxMsgInf() {
		return this.maxMsgInf;
	}

	public String getSoapService() {
		return this.soapService;
	}

	public String getSoapOperation() {
		return this.soapOperation;
	}

	public String getCallableSvcProgram() {
		return this.callableSvcProgram;
	}

	public String getTraceStorValue() {
		return this.traceStorValue;
	}

	public String getWebSvcId() {
		return this.webSvcId;
	}

	public String getWebSvcLkuPgm() {
		return this.webSvcLkuPgm;
	}

	public String getLvZlinuxSrv() {
		return this.lvZlinuxSrv;
	}

	public CfContainerInfoXzc03090 getContainerInfo() {
		return containerInfo;
	}

	public CfParagraphNamesXzc03090 getParagraphNames() {
		return paragraphNames;
	}
}
