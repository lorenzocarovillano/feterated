/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WA-CICS-REGION-TYPE<br>
 * Variable: WA-CICS-REGION-TYPE from program TS571099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WaCicsRegionType {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char DEVELOPMENT = 'D';
	public static final char BETA = 'B';
	public static final char EDUCATION = 'E';
	public static final char MODEL_OFFICE = 'M';
	public static final char TECH_SERVICES = 'T';
	public static final char PRODUCTION = 'C';

	//==== METHODS ====
	public void setWaCicsRegionType(char waCicsRegionType) {
		this.value = waCicsRegionType;
	}

	public char getWaCicsRegionType() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WA_CICS_REGION_TYPE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
