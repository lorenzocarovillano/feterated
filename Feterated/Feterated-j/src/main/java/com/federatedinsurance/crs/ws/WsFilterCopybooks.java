/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.Cawlf006;
import com.federatedinsurance.crs.copy.Cawlf008;
import com.federatedinsurance.crs.copy.CwcacfClientAddrComposite;

/**Original name: WS-FILTER-COPYBOOKS<br>
 * Variable: WS-FILTER-COPYBOOKS from program MU0Q0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsFilterCopybooks {

	//==== PROPERTIES ====
	//Original name: CWCACF-CLIENT-ADDR-COMPOSITE
	private CwcacfClientAddrComposite cwcacfClientAddrComposite = new CwcacfClientAddrComposite();
	//Original name: CAWLF006
	private Cawlf006 cawlf006 = new Cawlf006();
	//Original name: CAWLF008
	private Cawlf008 cawlf008 = new Cawlf008();

	//==== METHODS ====
	public Cawlf006 getCawlf006() {
		return cawlf006;
	}

	public Cawlf008 getCawlf008() {
		return cawlf008;
	}

	public CwcacfClientAddrComposite getCwcacfClientAddrComposite() {
		return cwcacfClientAddrComposite;
	}
}
