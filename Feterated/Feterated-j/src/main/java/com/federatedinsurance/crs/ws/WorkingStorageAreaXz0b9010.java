/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0B9010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0b9010 {

	//==== PROPERTIES ====
	//Original name: WS-ROW-COUNT
	private short rowCount = ((short) 0);
	public static final short NOTHING_FOUND = ((short) 0);
	//Original name: WS-MAX-ROWS
	private String maxRows = DefaultValues.stringVal(Len.MAX_ROWS);
	//Original name: WS-POL-LIST
	private WsPolList polList = new WsPolList();
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0B9010";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-WORD-KEY
	private String busObjNmWordKey = "XZ_GET_ACT_NOT_WORD_KEY";
	//Original name: WS-BUS-OBJ-NM-WORD-TXT
	private String busObjNmWordTxt = "XZ_GET_ACT_NOT_WORD_TXT";

	//==== METHODS ====
	public void setRowCount(short rowCount) {
		this.rowCount = rowCount;
	}

	public short getRowCount() {
		return this.rowCount;
	}

	public boolean isNothingFound() {
		return rowCount == NOTHING_FOUND;
	}

	public void setNothingFound() {
		rowCount = NOTHING_FOUND;
	}

	public void setMaxRows(long maxRows) {
		this.maxRows = PicFormatter.display("Z(3)9").format(maxRows).toString();
	}

	public long getMaxRows() {
		return PicParser.display("Z(3)9").parseLong(this.maxRows);
	}

	public String getMaxRowsFormatted() {
		return this.maxRows;
	}

	public String getMaxRowsAsString() {
		return getMaxRowsFormatted();
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNmWordKey() {
		return this.busObjNmWordKey;
	}

	public String getBusObjNmWordTxt() {
		return this.busObjNmWordTxt;
	}

	public WsPolList getPolList() {
		return polList;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_ROWS = 4;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
