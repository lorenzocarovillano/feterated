/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsOperationName;

/**Original name: WS-MISC-WORK-FLDS<br>
 * Variable: WS-MISC-WORK-FLDS from program MU0R0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsMiscWorkFldsMu0r0004 {

	//==== PROPERTIES ====
	//Original name: WS-REC-SEQ
	private int recSeq = DefaultValues.INT_VAL;
	//Original name: WS-PROGRAM-NAME
	private String programName = "MU0R0004";
	//Original name: WS-OPERATION-NAME
	private WsOperationName operationName = new WsOperationName();
	//Original name: WS-PROGRAM-LINK-FAILED
	private WsProgramLinkFailed programLinkFailed = new WsProgramLinkFailed();
	//Original name: WS-INVALID-OPERATION-MSG
	private WsInvalidOperationMsgMu0r0004 invalidOperationMsg = new WsInvalidOperationMsgMu0r0004();

	//==== METHODS ====
	public void setRecSeq(int recSeq) {
		this.recSeq = recSeq;
	}

	public int getRecSeq() {
		return this.recSeq;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public WsInvalidOperationMsgMu0r0004 getInvalidOperationMsg() {
		return invalidOperationMsg;
	}

	public WsOperationName getOperationName() {
		return operationName;
	}

	public WsProgramLinkFailed getProgramLinkFailed() {
		return programLinkFailed;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
