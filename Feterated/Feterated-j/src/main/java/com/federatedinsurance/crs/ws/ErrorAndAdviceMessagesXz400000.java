/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZ400000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXz400000 {

	//==== PROPERTIES ====
	//Original name: EA-01-PROGRAM-MSG
	private Ea01ProgramMsgXz400000 ea01ProgramMsg = new Ea01ProgramMsgXz400000();
	//Original name: EA-02-PROGRAM-START
	private Ea02ProgramStartXz400000 ea02ProgramStart = new Ea02ProgramStartXz400000();
	//Original name: EA-03-TOT-ROWS-PURGED-MSG
	private Ea03TotRowsPurgedMsg ea03TotRowsPurgedMsg = new Ea03TotRowsPurgedMsg();
	//Original name: EA-04-SUCCESSFUL-END
	private Ea04SuccessfulEnd ea04SuccessfulEnd = new Ea04SuccessfulEnd();
	//Original name: EA-05-PARM-ERR-MSG
	private Ea05ParmErrMsgXz400000 ea05ParmErrMsg = new Ea05ParmErrMsgXz400000();
	//Original name: EA-06-CICS-CALL-ERR
	private Ea06CicsCallErr ea06CicsCallErr = new Ea06CicsCallErr();
	//Original name: EA-07-CICS-PARM-MSG
	private Ea07CicsParmMsg ea07CicsParmMsg = new Ea07CicsParmMsg();
	//Original name: FILLER-EA-08-OPENING-PIPE-FAILED
	private String flr1 = "OPENING PIPE";
	//Original name: FILLER-EA-08-OPENING-PIPE-FAILED-1
	private String flr2 = " FAILED!";
	//Original name: FILLER-EA-09-OPENING-PIPE-WARNING
	private String flr3 = "OPENING PIPE";
	//Original name: FILLER-EA-09-OPENING-PIPE-WARNING-1
	private String flr4 = "WARNING!";
	//Original name: FILLER-EA-10-CALLING-PIPE-FAILED
	private String flr5 = "CALLING PIPE";
	//Original name: FILLER-EA-10-CALLING-PIPE-FAILED-1
	private String flr6 = " FAILED!";
	//Original name: FILLER-EA-11-CALLING-PIPE-WARNING
	private String flr7 = "CALLING PIPE";
	//Original name: FILLER-EA-11-CALLING-PIPE-WARNING-1
	private String flr8 = "WARNING!";
	//Original name: FILLER-EA-12-CLOSING-PIPE-FAILED
	private String flr9 = "CLOSING PIPE";
	//Original name: FILLER-EA-12-CLOSING-PIPE-FAILED-1
	private String flr10 = " FAILED!";
	//Original name: EA-13-GET-ACT-NOT-INPUT-PARMS
	private Ea16GetActNotInputParms ea13GetActNotInputParms = new Ea16GetActNotInputParms();
	//Original name: EA-14-DLT-ACT-NOT-INPUT-PARMS
	private Ea17DltActNotInputParms ea14DltActNotInputParms = new Ea17DltActNotInputParms();
	//Original name: EA-15-SERVICE-ERROR-FIELDS
	private Ea18ServiceErrorFields ea15ServiceErrorFields = new Ea18ServiceErrorFields();
	//Original name: EA-16-OPEN-CURSOR-ERROR
	private Ea16OpenCursorError ea16OpenCursorError = new Ea16OpenCursorError();
	//Original name: EA-17-FETCH-CURSOR-ERROR
	private Ea17FetchCursorError ea17FetchCursorError = new Ea17FetchCursorError();
	//Original name: EA-18-CLOSE-CURSOR-ERROR
	private Ea18CloseCursorError ea18CloseCursorError = new Ea18CloseCursorError();
	//Original name: EA-19-DB2-ERROR-ON-SELECT
	private Ea19Db2ErrorOnSelect ea19Db2ErrorOnSelect = new Ea19Db2ErrorOnSelect();
	//Original name: EA-20-PURGE-DT-MSG
	private Ea20PurgeDtMsg ea20PurgeDtMsg = new Ea20PurgeDtMsg();

	//==== METHODS ====
	public String getEa08OpeningPipeFailedFormatted() {
		return MarshalByteExt.bufferToStr(getEa08OpeningPipeFailedBytes());
	}

	/**Original name: EA-08-OPENING-PIPE-FAILED<br>*/
	public byte[] getEa08OpeningPipeFailedBytes() {
		byte[] buffer = new byte[Len.EA08_OPENING_PIPE_FAILED];
		return getEa08OpeningPipeFailedBytes(buffer, 1);
	}

	public byte[] getEa08OpeningPipeFailedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getEa09OpeningPipeWarningFormatted() {
		return MarshalByteExt.bufferToStr(getEa09OpeningPipeWarningBytes());
	}

	/**Original name: EA-09-OPENING-PIPE-WARNING<br>*/
	public byte[] getEa09OpeningPipeWarningBytes() {
		byte[] buffer = new byte[Len.EA09_OPENING_PIPE_WARNING];
		return getEa09OpeningPipeWarningBytes(buffer, 1);
	}

	public byte[] getEa09OpeningPipeWarningBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getEa10CallingPipeFailedFormatted() {
		return MarshalByteExt.bufferToStr(getEa10CallingPipeFailedBytes());
	}

	/**Original name: EA-10-CALLING-PIPE-FAILED<br>*/
	public byte[] getEa10CallingPipeFailedBytes() {
		byte[] buffer = new byte[Len.EA10_CALLING_PIPE_FAILED];
		return getEa10CallingPipeFailedBytes(buffer, 1);
	}

	public byte[] getEa10CallingPipeFailedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR4);
		return buffer;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getEa11CallingPipeWarningFormatted() {
		return MarshalByteExt.bufferToStr(getEa11CallingPipeWarningBytes());
	}

	/**Original name: EA-11-CALLING-PIPE-WARNING<br>*/
	public byte[] getEa11CallingPipeWarningBytes() {
		byte[] buffer = new byte[Len.EA11_CALLING_PIPE_WARNING];
		return getEa11CallingPipeWarningBytes(buffer, 1);
	}

	public byte[] getEa11CallingPipeWarningBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR4);
		return buffer;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public String getEa12ClosingPipeFailedFormatted() {
		return MarshalByteExt.bufferToStr(getEa12ClosingPipeFailedBytes());
	}

	/**Original name: EA-12-CLOSING-PIPE-FAILED<br>*/
	public byte[] getEa12ClosingPipeFailedBytes() {
		byte[] buffer = new byte[Len.EA12_CLOSING_PIPE_FAILED];
		return getEa12ClosingPipeFailedBytes(buffer, 1);
	}

	public byte[] getEa12ClosingPipeFailedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr10, Len.FLR4);
		return buffer;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public String getFlr10() {
		return this.flr10;
	}

	public Ea01ProgramMsgXz400000 getEa01ProgramMsg() {
		return ea01ProgramMsg;
	}

	public Ea02ProgramStartXz400000 getEa02ProgramStart() {
		return ea02ProgramStart;
	}

	public Ea03TotRowsPurgedMsg getEa03TotRowsPurgedMsg() {
		return ea03TotRowsPurgedMsg;
	}

	public Ea04SuccessfulEnd getEa04SuccessfulEnd() {
		return ea04SuccessfulEnd;
	}

	public Ea05ParmErrMsgXz400000 getEa05ParmErrMsg() {
		return ea05ParmErrMsg;
	}

	public Ea06CicsCallErr getEa06CicsCallErr() {
		return ea06CicsCallErr;
	}

	public Ea07CicsParmMsg getEa07CicsParmMsg() {
		return ea07CicsParmMsg;
	}

	public Ea16GetActNotInputParms getEa13GetActNotInputParms() {
		return ea13GetActNotInputParms;
	}

	public Ea17DltActNotInputParms getEa14DltActNotInputParms() {
		return ea14DltActNotInputParms;
	}

	public Ea18ServiceErrorFields getEa15ServiceErrorFields() {
		return ea15ServiceErrorFields;
	}

	public Ea16OpenCursorError getEa16OpenCursorError() {
		return ea16OpenCursorError;
	}

	public Ea17FetchCursorError getEa17FetchCursorError() {
		return ea17FetchCursorError;
	}

	public Ea18CloseCursorError getEa18CloseCursorError() {
		return ea18CloseCursorError;
	}

	public Ea19Db2ErrorOnSelect getEa19Db2ErrorOnSelect() {
		return ea19Db2ErrorOnSelect;
	}

	public Ea20PurgeDtMsg getEa20PurgeDtMsg() {
		return ea20PurgeDtMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR3 = 13;
		public static final int FLR4 = 8;
		public static final int EA09_OPENING_PIPE_WARNING = FLR3 + FLR4;
		public static final int EA08_OPENING_PIPE_FAILED = FLR3 + FLR4;
		public static final int EA11_CALLING_PIPE_WARNING = FLR3 + FLR4;
		public static final int EA10_CALLING_PIPE_FAILED = FLR3 + FLR4;
		public static final int EA12_CLOSING_PIPE_FAILED = FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
