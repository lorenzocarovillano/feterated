/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: W-ID-BUILDING-ERR<br>
 * Variable: W-ID-BUILDING-ERR from program HALOUIDG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WIdBuildingErr {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char OKAY = Types.SPACE_CHAR;
	public static final char FAILED = 'F';

	//==== METHODS ====
	public void setIdBuildingErr(char idBuildingErr) {
		this.value = idBuildingErr;
	}

	public void setIdBuildingErrFormatted(String idBuildingErr) {
		setIdBuildingErr(Functions.charAt(idBuildingErr, Types.CHAR_SIZE));
	}

	public char getIdBuildingErr() {
		return this.value;
	}

	public void setOkay() {
		setIdBuildingErrFormatted(String.valueOf(OKAY));
	}

	public boolean isFailed() {
		return value == FAILED;
	}

	public void setFailed() {
		value = FAILED;
	}
}
