/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-REGION-INF-FND-FLAG<br>
 * Variable: SW-REGION-INF-FND-FLAG from program TS547099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwRegionInfFndFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NOT_FND = '0';
	public static final char FND = '1';

	//==== METHODS ====
	public void setRegionInfFndFlag(char regionInfFndFlag) {
		this.value = regionInfFndFlag;
	}

	public char getRegionInfFndFlag() {
		return this.value;
	}

	public boolean isNotFnd() {
		return value == NOT_FND;
	}

	public void setNotFnd() {
		value = NOT_FND;
	}

	public boolean isFnd() {
		return value == FND;
	}

	public void setFnd() {
		value = FND;
	}
}
