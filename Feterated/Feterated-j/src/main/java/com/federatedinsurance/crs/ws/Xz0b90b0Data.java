/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xzy800GetNotDayRqrRow;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B90B0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b90b0Data {

	//==== PROPERTIES ====
	//Original name: CF-SP-GET-POL-LST-SVC
	private String cfSpGetPolLstSvc = "XZ0X9050";
	//Original name: CF-SP-GET-NOT-DAY-RQR-UTY-PGM
	private String cfSpGetNotDayRqrUtyPgm = "XZ0U8000";
	//Original name: EA-01-INV-ACT-NOT-TYP-CD
	private Ea01InvActNotTypCd ea01InvActNotTypCd = new Ea01InvActNotTypCd();
	//Original name: ES-01-FATAL-ERROR-MSG
	private Es01FatalErrorMsg es01FatalErrorMsg = new Es01FatalErrorMsg();
	//Original name: SUBSCRIPTS
	private SubscriptsXz0b90b0 subscripts = new SubscriptsXz0b90b0();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b90b0 workingStorageArea = new WorkingStorageAreaXz0b90b0();
	//Original name: WS-XZ0A90B0-ROW
	private WsXz0a90b0Row wsXz0a90b0Row = new WsXz0a90b0Row();
	//Original name: WS-XZ0A90B1-ROW
	private WsXz0a90b1Row wsXz0a90b1Row = new WsXz0a90b1Row();
	//Original name: XZY800-GET-NOT-DAY-RQR-ROW
	private Xzy800GetNotDayRqrRow xzy800GetNotDayRqrRow = new Xzy800GetNotDayRqrRow();
	//Original name: WS-PROXY-PROGRAM-AREA
	private WsProxyProgramArea wsProxyProgramArea = new WsProxyProgramArea();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public String getCfSpGetPolLstSvc() {
		return this.cfSpGetPolLstSvc;
	}

	public String getCfSpGetNotDayRqrUtyPgm() {
		return this.cfSpGetNotDayRqrUtyPgm;
	}

	public void setWsXz0y8000RowFormatted(String data) {
		byte[] buffer = new byte[Len.WS_XZ0Y8000_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0Y8000_ROW);
		setWsXz0y8000RowBytes(buffer, 1);
	}

	public String getWsXz0y8000RowFormatted() {
		return xzy800GetNotDayRqrRow.getXzy800GetNotDayRqrRowFormatted();
	}

	public void setWsXz0y8000RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xzy800GetNotDayRqrRow.setXzy800GetNotDayRqrRowBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public Ea01InvActNotTypCd getEa01InvActNotTypCd() {
		return ea01InvActNotTypCd;
	}

	public Es01FatalErrorMsg getEs01FatalErrorMsg() {
		return es01FatalErrorMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public SubscriptsXz0b90b0 getSubscripts() {
		return subscripts;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b90b0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsProxyProgramArea getWsProxyProgramArea() {
		return wsProxyProgramArea;
	}

	public WsXz0a90b0Row getWsXz0a90b0Row() {
		return wsXz0a90b0Row;
	}

	public WsXz0a90b1Row getWsXz0a90b1Row() {
		return wsXz0a90b1Row;
	}

	public Xzy800GetNotDayRqrRow getXzy800GetNotDayRqrRow() {
		return xzy800GetNotDayRqrRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_POL_TYP_CD = 3;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_XZ0Y8000_ROW = Xzy800GetNotDayRqrRow.Len.XZY800_GET_NOT_DAY_RQR_ROW;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
