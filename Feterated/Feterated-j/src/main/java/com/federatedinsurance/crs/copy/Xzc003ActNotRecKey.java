/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XZC003-ACT-NOT-REC-KEY<br>
 * Variable: XZC003-ACT-NOT-REC-KEY from copybook XZ0C0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzc003ActNotRecKey {

	//==== PROPERTIES ====
	//Original name: XZC003-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC003-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC003-REC-SEQ-NBR-SIGN
	private char recSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: XZC003-REC-SEQ-NBR
	private String recSeqNbr = DefaultValues.stringVal(Len.REC_SEQ_NBR);

	//==== METHODS ====
	public String getActNotRecKeyFormatted() {
		return MarshalByteExt.bufferToStr(getActNotRecKeyBytes());
	}

	public byte[] getActNotRecKeyBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_REC_KEY];
		return getActNotRecKeyBytes(buffer, 1);
	}

	public void setActNotRecKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		recSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recSeqNbr = MarshalByte.readFixedString(buffer, position, Len.REC_SEQ_NBR);
	}

	public byte[] getActNotRecKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, recSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, recSeqNbr, Len.REC_SEQ_NBR);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getXzc003CsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getXzc003NotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setRecSeqNbrSign(char recSeqNbrSign) {
		this.recSeqNbrSign = recSeqNbrSign;
	}

	public void setXzc003RecSeqNbrSignFormatted(String xzc003RecSeqNbrSign) {
		setRecSeqNbrSign(Functions.charAt(xzc003RecSeqNbrSign, Types.CHAR_SIZE));
	}

	public char getRecSeqNbrSign() {
		return this.recSeqNbrSign;
	}

	public void setXzc003RecSeqNbr(int xzc003RecSeqNbr) {
		this.recSeqNbr = NumericDisplay.asString(xzc003RecSeqNbr, Len.REC_SEQ_NBR);
	}

	public void setRecSeqNbrFormatted(String recSeqNbr) {
		this.recSeqNbr = Trunc.toUnsignedNumeric(recSeqNbr, Len.REC_SEQ_NBR);
	}

	public int getRecSeqNbr() {
		return NumericDisplay.asInt(this.recSeqNbr);
	}

	public String getXzc003RecSeqNbrFormatted() {
		return this.recSeqNbr;
	}

	public String getXzc003RecSeqNbrAsString() {
		return getXzc003RecSeqNbrFormatted();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int REC_SEQ_NBR_SIGN = 1;
		public static final int REC_SEQ_NBR = 5;
		public static final int ACT_NOT_REC_KEY = CSR_ACT_NBR + NOT_PRC_TS + REC_SEQ_NBR_SIGN + REC_SEQ_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
