/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ0P90L0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz0p90l0 {

	//==== PROPERTIES ====
	//Original name: SW-ACT-NOT-SVC-ALC-FLAG
	private boolean actNotSvcAlcFlag = false;
	//Original name: SW-ADD-NOTIFICATION-FLAG
	private boolean addNotificationFlag = true;
	//Original name: SW-ADD-POLICY-FLAG
	private boolean addPolicyFlag = false;
	//Original name: SW-FIRST-SELECT-FLAG
	private boolean firstSelectFlag = true;
	//Original name: SW-INSURED-DETAIL-ALC-FLAG
	private boolean insuredDetailAlcFlag = false;
	//Original name: SW-PERSONAL-LINES-ACT-FLAG
	private boolean personalLinesActFlag = false;
	//Original name: SW-PREPARE-INS-POL-FLAG
	private boolean prepareInsPolFlag = false;
	//Original name: SW-PREPARE-NOT-FLAG
	private boolean prepareNotFlag = false;
	//Original name: SW-PREPARE-TTY-FLAG
	private boolean prepareTtyFlag = false;
	//Original name: SW-UPD-NOT-STA-FLAG
	private boolean updNotStaFlag = false;
	//Original name: SW-IMP-IN-PROGRESS-FLAG
	private boolean impInProgressFlag = false;

	//==== METHODS ====
	public void setActNotSvcAlcFlag(boolean actNotSvcAlcFlag) {
		this.actNotSvcAlcFlag = actNotSvcAlcFlag;
	}

	public boolean isActNotSvcAlcFlag() {
		return this.actNotSvcAlcFlag;
	}

	public void setAddNotificationFlag(boolean addNotificationFlag) {
		this.addNotificationFlag = addNotificationFlag;
	}

	public boolean isAddNotificationFlag() {
		return this.addNotificationFlag;
	}

	public void setAddPolicyFlag(boolean addPolicyFlag) {
		this.addPolicyFlag = addPolicyFlag;
	}

	public boolean isAddPolicyFlag() {
		return this.addPolicyFlag;
	}

	public void setFirstSelectFlag(boolean firstSelectFlag) {
		this.firstSelectFlag = firstSelectFlag;
	}

	public boolean isFirstSelectFlag() {
		return this.firstSelectFlag;
	}

	public void setInsuredDetailAlcFlag(boolean insuredDetailAlcFlag) {
		this.insuredDetailAlcFlag = insuredDetailAlcFlag;
	}

	public boolean isInsuredDetailAlcFlag() {
		return this.insuredDetailAlcFlag;
	}

	public void setPersonalLinesActFlag(boolean personalLinesActFlag) {
		this.personalLinesActFlag = personalLinesActFlag;
	}

	public boolean isPersonalLinesActFlag() {
		return this.personalLinesActFlag;
	}

	public void setPrepareInsPolFlag(boolean prepareInsPolFlag) {
		this.prepareInsPolFlag = prepareInsPolFlag;
	}

	public boolean isPrepareInsPolFlag() {
		return this.prepareInsPolFlag;
	}

	public void setPrepareNotFlag(boolean prepareNotFlag) {
		this.prepareNotFlag = prepareNotFlag;
	}

	public boolean isPrepareNotFlag() {
		return this.prepareNotFlag;
	}

	public void setPrepareTtyFlag(boolean prepareTtyFlag) {
		this.prepareTtyFlag = prepareTtyFlag;
	}

	public boolean isPrepareTtyFlag() {
		return this.prepareTtyFlag;
	}

	public void setUpdNotStaFlag(boolean updNotStaFlag) {
		this.updNotStaFlag = updNotStaFlag;
	}

	public boolean isUpdNotStaFlag() {
		return this.updNotStaFlag;
	}

	public void setImpInProgressFlag(boolean impInProgressFlag) {
		this.impInProgressFlag = impInProgressFlag;
	}

	public boolean isImpInProgressFlag() {
		return this.impInProgressFlag;
	}
}
