/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.ActNotFrmRecTypDao;
import com.federatedinsurance.crs.commons.data.dao.ActNotRecTypDao;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsXz0a9020Row;
import com.federatedinsurance.crs.ws.WsXz0a9021Row;
import com.federatedinsurance.crs.ws.Xz0b9020Data;
import com.federatedinsurance.crs.ws.enums.Xza920Operation;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0B9020<br>
 * <pre>AUTHOR.       DAWN POSSEHL.
 * DATE-WRITTEN. 12 JAN 2009.
 * *****************************************************************
 *                                                                 *
 *   PROGRAM TITLE - RETRIEVE RECIPIENT LIST BY FORM SEQ NBR       *
 *                   XREF OBJ NM : XZ_GET_REC_LIST_BPO             *
 *                   UOW         : XZ_GET_REC_LIST                 *
 *                   OPERATION   : GetRecipientListByFormSeqNbr    *
 *                                 GetRecipientListByNotification  *
 *                                                                 *
 *   PURPOSE -  PERFORM ONE OF TWO RECIPIENT LISTS:                *
 *           1) READ ACT_NOT_FRM_REC JOINED WITH ACT_NOT_REC TO    *
 *              LIST ALL THE RECIPIENTS (ALONG WITH THE RECIPIENT  *
 *              INFORMATION) FOR A SPECIFIC FORM SEQUENCE NUMBER.  *
 *           2) READ ACT_NOT_REC TO LIST ALL THE RECIPIENTS        *
 *              (ALONG WITH THE RECIPIENT INFORMATION) FOR A       *
 *              A SPECIFIC FORM SEQUENCE NUMBER.                   *
 *           -  IF THERE ARE MORE RECIPIENTS, THAN WE CAN RETURN   *
 *              IN OUR CALL, WE WILL FILL IN THE RECALL RECIPIENT  *
 *              SEQUENCE NUMBER WITH THE NEXT RECIPIENT WE NEED TO *
 *              START WITH.                                        *
 *                                                                 *
 *   PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS   *
 *                         LINKED TO BY THE FRAMEWORK DRIVER.      *
 *                                                                 *
 *   DATA ACCESS METHODS - UMT STORAGE RECORDS                     *
 *                         DB2 DATABASE                            *
 *                                                                 *
 * *****************************************************************
 * ****************************************************************
 * * NOTE: THIS LOG FOR INFRASTRUCTURE USE ONLY FOR TEMPLATE     **
 * *       VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     **
 * *       APPLICATION CODING.                                   **
 * *     T E M P L A T E   M A I N T E N A N C E   L O G         **
 * * CASE#     DATE       PROG       DESCRIPTION                 **
 * * --------  ---------  --------   ----------------------------**
 * * TS129     06/13/2006 E404LJL    TEMPLATE CREATED            **
 * * YJ249     04/27/2007 E404NEM    STDS CHGS                   **
 * * TS130     12/28/2007 E404JSP    Changed a few bugs          **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * -------  ----------  -------- ------------------------------**
 * * TO07614  01/12/2009  E404DLP  NEW                           **
 * * TO07614  03/04/2009  E404GCL  Add new operation -           **
 * *                               GetRecipientListByNotification**
 * * TO07614  06/03/2009  E404KXS  Update to use name/address    **
 * *                               utlity function               **
 * * PP02500  10/10/2012  E404BPO  Remove call to cert info list **
 * *                               service.                      **
 * ****************************************************************</pre>*/
public class Xz0b9020 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ActNotFrmRecTypDao actNotFrmRecTypDao = new ActNotFrmRecTypDao(dbAccessStatus);
	private ActNotRecTypDao actNotRecTypDao = new ActNotRecTypDao(dbAccessStatus);
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0b9020Data ws = new Xz0b9020Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private Dfhcommarea dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, Dfhcommarea dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0b9020 getInstance() {
		return (Programs.getInstance(Xz0b9020.class));
	}

	/**Original name: 1000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   MAIN PROCESSING CONTROL                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINING-HOUSEKEEPING.
		beginingHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN XZA920-GET-REC-LIST-BY-FRM
		//                   END-IF
		//               WHEN XZA920-GET-REC-LIST-BY-NOT
		//                   END-IF
		//           END-EVALUATE.
		switch (ws.getWsXz0a9020Row().getOperation().getOperation()) {

		case Xza920Operation.FRM:// COB_CODE: PERFORM 3000-GET-REC-BY-FRM-INFO
			getRecByFrmInfo();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 1000-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 1000-EXIT
				exit();
			}
			break;

		case Xza920Operation.NOT_FLD:// COB_CODE: PERFORM 4000-GET-REC-BY-NOT-INFO
			getRecByNotInfo();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 1000-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 1000-EXIT
				exit();
			}
			break;

		default:
			break;
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *  INITIALIZATION CONTROL                                        *
	 *                                                                *
	 * ****************************************************************
	 * * INITIALIZE ERROR/WARNING STORAGE</pre>*/
	private void beginingHousekeeping() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//* VALIDATE UBOC IN COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2100-READ-REQ-UMT-ROW.
		readReqUmtRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-READ-REQ-UMT-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR THE BPO INPUT ROW                     *
	 * *****************************************************************</pre>*/
	private void readReqUmtRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: SET HALRURQA-READ-FUNC      TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM-LIST-KEY TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNmListKey());
		// COB_CODE: MOVE +1                     TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(1);
		// COB_CODE: INITIALIZE                  WS-XZ0A9020-ROW.
		initWsXz0a9020Row();
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-XZ0A9020-ROW.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsXz0a9020Row());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: IF HALRURQA-REC-NOT-FOUND
		//               GO TO 2100-EXIT
		//           END-IF.
		if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUEST-MSG-MISSING
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequestMsgMissing();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '2100-READ-REQ-UMT-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-READ-REQ-UMT-ROW");
			// COB_CODE: MOVE 'NO RECORD FOUND ON REQ MSG STORE'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO RECORD FOUND ON REQ MSG STORE");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HALRURQA-BUS-OBJ-NM='
			//                  HALRURQA-BUS-OBJ-NM
			//                  '; HALRURQA-REC-SEQ='
			//                  HALRURQA-REC-SEQ
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HALRURQA-BUS-OBJ-NM=").append(ws.getWsHalrurqaLinkage().getBusObjNmFormatted())
							.append("; HALRURQA-REC-SEQ=").append(ws.getWsHalrurqaLinkage().getRecSeqFormatted()).toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
	}

	/**Original name: 3000-GET-REC-BY-FRM-INFO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE PASSED INFO AND BUILD THE SQL QUERY FOR HIT ON THE    *
	 *  TABLES TO LOCATE THE RECIPIENTS FOR THE FORM SEQ NBR PASSED IN.*
	 * *****************************************************************
	 * * OPEN REQUIRED CURSOR</pre>*/
	private void getRecByFrmInfo() {
		// COB_CODE: PERFORM 3100-OPEN-REC-BY-FRM-CURSOR.
		openRecByFrmCursor();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* FETCH ALL DATA FROM REQUIRED CURSOR
		// COB_CODE: PERFORM 3200-PROCESS-REC-BY-FRM-ROWS.
		rng3200ProcessRecByFrmRows();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* CLOSE CURSOR
		// COB_CODE: PERFORM 3300-CLOSE-REC-BY-FRM-CURSOR.
		closeRecByFrmCursor();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* NO ERROR CREATED IF MAX ROWS IS HIT.  INSTEAD WE WILL FILL
		//* IN A RECALL RECIPIENT VALUE ON THE RECIPIENT KEY ROW.
		//* A NON-LOGGABLE WARNING IS RETURNED IF SEARCH RETURNS NOTHING
		// COB_CODE: IF WS-NOTHING-FOUND
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getWorkingStorageArea().isNothingFound()) {
			// COB_CODE: SET WS-NON-LOGGABLE-WARNING
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setWarning();
			// COB_CODE: MOVE WS-BUS-OBJ-NM-LIST-DETAIL
			//                                   TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getBusObjNmListDetail());
			// COB_CODE: MOVE 'ACT_NBR'          TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("ACT_NBR");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO UWRN-WARNING-CODE
			ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE XZA920-CSR-ACT-NBR TO EA-02-CSR-ACT-NBR
			ws.getEa02NothingFoundMsg().setCsrActNbr(ws.getWsXz0a9020Row().getCsrActNbr());
			// COB_CODE: MOVE XZA920-NOT-PRC-TS  TO EA-02-NOT-PRC-TS
			ws.getEa02NothingFoundMsg().setNotPrcTs(ws.getWsXz0a9020Row().getNotPrcTs());
			// COB_CODE: MOVE XZA920-FRM-SEQ-NBR TO EA-02-FRM-SEQ-NBR
			ws.getEa02NothingFoundMsg().setFrmSeqNbr(ws.getWsXz0a9020Row().getFrmSeqNbr());
			// COB_CODE: MOVE EA-02-NOTHING-FOUND-MSG
			//                                   TO WS-NONLOG-ERR-ALLTXT-TEXT
			ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getEa02NothingFoundMsg().getEa02NothingFoundMsgFormatted());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//* NOTE THIS NEEDS TO BE DONE LAST TO UPDATE THE RECALL VALUE
		// COB_CODE: PERFORM 3400-WRITE-REC-BY-FRM-KEY-ROW.
		writeRecByFrmKeyRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3100-OPEN-REC-BY-FRM-CURSOR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  OPEN RECIPIENT CURSOR                                          *
	 * *****************************************************************</pre>*/
	private void openRecByFrmCursor() {
		// COB_CODE: MOVE XZA920-CSR-ACT-NBR     TO CSR-ACT-NBR
		//                                       OF DCLACT-NOT-FRM-REC.
		ws.getDclactNotFrmRec().setCsrActNbr(ws.getWsXz0a9020Row().getCsrActNbr());
		// COB_CODE: MOVE XZA920-NOT-PRC-TS      TO NOT-PRC-TS
		//                                       OF DCLACT-NOT-FRM-REC.
		ws.getDclactNotFrmRec().setNotPrcTs(ws.getWsXz0a9020Row().getNotPrcTs());
		// COB_CODE: MOVE XZA920-FRM-SEQ-NBR     TO FRM-SEQ-NBR
		//                                       OF DCLACT-NOT-FRM-REC.
		ws.getDclactNotFrmRec().setFrmSeqNbr(Trunc.toShort(ws.getWsXz0a9020Row().getFrmSeqNbr(), 4));
		// COB_CODE: MOVE XZA920-RECALL-REC-SEQ-NBR
		//                                       TO REC-SEQ-NBR
		//                                       OF DCLACT-NOT-FRM-REC.
		ws.getDclactNotFrmRec().setRecSeqNbr(Trunc.toShort(ws.getWsXz0a9020Row().getRecallRecSeqNbr(), 4));
		// COB_CODE: EXEC SQL
		//               OPEN RECIPIENT_BY_FRM_CSR
		//           END-EXEC.
		actNotFrmRecTypDao.openRecipientByFrmCsr(ws.getDclactNotFrmRec().getCsrActNbr(), ws.getDclactNotFrmRec().getNotPrcTs(),
				ws.getDclactNotFrmRec().getFrmSeqNbr(), ws.getDclactNotFrmRec().getRecSeqNbr());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'ACT_NOT_FRM_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_FRM_REC");
			// COB_CODE: MOVE '3100-OPEN-REC-BY-FRM-CURSOR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3100-OPEN-REC-BY-FRM-CURSOR");
			// COB_CODE: MOVE 'OPEN RECIPIENT_BY_FRM_CSR FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN RECIPIENT_BY_FRM_CSR FAILED");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZA920-CSR-ACT-NBR
			//                  ' NOT-PRC-TS='
			//                  XZA920-NOT-PRC-TS
			//                  ' FRM-SEQ-NBR='
			//                  XZA920-FRM-SEQ-NBR
			//                  ' RECALL-SEQ-NBR='
			//                  XZA920-RECALL-REC-SEQ-NBR
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("CSR-ACT-NBR=").append(ws.getWsXz0a9020Row().getCsrActNbrFormatted())
							.append(" NOT-PRC-TS=").append(ws.getWsXz0a9020Row().getNotPrcTsFormatted()).append(" FRM-SEQ-NBR=")
							.append(ws.getWsXz0a9020Row().getFrmSeqNbrFormatted()).append(" RECALL-SEQ-NBR=")
							.append(ws.getWsXz0a9020Row().getRecallRecSeqNbrFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 3200-PROCESS-REC-BY-FRM-ROWS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CONTROL FETCH DATA FROM RECIPIENT CURSOR                       *
	 * *****************************************************************
	 * * MAX ROWS IS SET FROM A CONSTANT FIELD DEFINED IN THE PROXY
	 * * AND PASSED IN ON THE BPO COPYBOOK.</pre>*/
	private void processRecByFrmRows() {
		// COB_CODE: SET WS-NOTHING-FOUND        TO TRUE.
		ws.getWorkingStorageArea().setNothingFound();
	}

	/**Original name: 3200-A<br>*/
	private String a() {
		// COB_CODE: PERFORM 3210-FETCH-REC-BY-FRM-CURSOR.
		fetchRecByFrmCursor();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//             OR
		//              SW-END-OF-CURSOR
		//               GO TO 3200-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn() || ws.isSwEndOfCursorFlag()) {
			// COB_CODE: MOVE +0                 TO XZA920-RECALL-REC-SEQ-NBR
			ws.getWsXz0a9020Row().setRecallRecSeqNbr(0);
			// move zero to the recall field
			// COB_CODE: GO TO 3200-EXIT
			return "";
		}
		// COB_CODE: ADD +1                      TO WS-ROW-COUNT.
		ws.getWorkingStorageArea().setRowCount(Trunc.toShort(1 + ws.getWorkingStorageArea().getRowCount(), 4));
		// COB_CODE:      IF WS-ROW-COUNT > XZA920-MAX-REC-ROWS
		//           * move currently fetched recipient seq nbr to the recall field
		//                    GO TO 3200-EXIT
		//                END-IF.
		if (ws.getWorkingStorageArea().getRowCount() > ws.getWsXz0a9020Row().getMaxRecRows()) {
			// move currently fetched recipient seq nbr to the recall field
			// COB_CODE: MOVE REC-SEQ-NBR     OF DCLACT-NOT-REC
			//                                   TO XZA920-RECALL-REC-SEQ-NBR
			ws.getWsXz0a9020Row().setRecallRecSeqNbr(ws.getDclactNotRec().getRecSeqNbr());
			// COB_CODE: GO TO 3200-EXIT
			return "";
		}
		// COB_CODE: PERFORM 9100-WRITE-REC-DTL-ROW.
		rng9100WriteRecDtlRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3200-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3200-EXIT
			return "";
		}
		// COB_CODE: GO TO 3200-A.
		return "3200-A";
	}

	/**Original name: 3210-FETCH-REC-BY-FRM-CURSOR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FETCH ROW FROM THE RECIPIENT CURSOR                            *
	 * *****************************************************************</pre>*/
	private void fetchRecByFrmCursor() {
		// COB_CODE: INITIALIZE DCLACT-NOT-REC
		//                      DCLREC-TYP.
		initDclactNotRec();
		initDclrecTyp();
		// COB_CODE: EXEC SQL
		//               FETCH RECIPIENT_BY_FRM_CSR
		//                   INTO :DCLACT-NOT-REC.REC-SEQ-NBR
		//                      , :DCLACT-NOT-REC.REC-TYP-CD
		//                      , :DCLACT-NOT-REC.REC-NM
		//                             :NI-REC-NM
		//                      , :DCLACT-NOT-REC.LIN-1-ADR
		//                             :NI-LIN-1-ADR
		//                      , :DCLACT-NOT-REC.LIN-2-ADR
		//                             :NI-LIN-2-ADR
		//                      , :DCLACT-NOT-REC.CIT-NM
		//                             :NI-CIT-NM
		//                      , :DCLACT-NOT-REC.ST-ABB
		//                             :NI-ST-ABB
		//                      , :DCLACT-NOT-REC.PST-CD
		//                             :NI-PST-CD
		//                      , :DCLACT-NOT-REC.CER-NBR
		//                             :NI-CER-NBR
		//                      , :DCLREC-TYP.REC-LNG-DES
		//                      , :DCLREC-TYP.REC-SR-ORD-NBR
		//           END-EXEC.
		actNotFrmRecTypDao.fetchRecipientByFrmCsr(ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 3210-EXIT
		//               WHEN OTHER
		//                   GO TO 3210-EXIT
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET SW-END-OF-CURSOR
			//                               TO TRUE
			ws.setSwEndOfCursorFlag(true);
			// COB_CODE: GO TO 3210-EXIT
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'ACT_NOT_FRM_REC'
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_FRM_REC");
			// COB_CODE: MOVE '3210-FETCH-REC-BY-FRM-CURSOR'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3210-FETCH-REC-BY-FRM-CURSOR");
			// COB_CODE: MOVE 'FETCH RECIPIENT_BY_FRM_CSR FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH RECIPIENT_BY_FRM_CSR FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3210-EXIT
			return;
		}
	}

	/**Original name: 3300-CLOSE-REC-BY-FRM-CURSOR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CLOSE RECIPIENT CURSOR                                         *
	 * *****************************************************************</pre>*/
	private void closeRecByFrmCursor() {
		// COB_CODE: EXEC SQL
		//               CLOSE RECIPIENT_BY_FRM_CSR
		//           END-EXEC.
		actNotFrmRecTypDao.closeRecipientByFrmCsr();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'ACT_NOT_FRM_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_FRM_REC");
			// COB_CODE: MOVE '3300-CLOSE-REC-BY-FRM-CURSOR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3300-CLOSE-REC-BY-FRM-CURSOR");
			// COB_CODE: MOVE 'CLOSE RECIPIENT_BY_FRM_CSR FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE RECIPIENT_BY_FRM_CSR FAILED");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZA920-CSR-ACT-NBR
			//                  ' NOT-PRC-TS='
			//                  XZA920-NOT-PRC-TS
			//                  ' FRM-SEQ-NBR='
			//                  XZA920-FRM-SEQ-NBR
			//                  ' RECALL-SEQ-NBR='
			//                  XZA920-RECALL-REC-SEQ-NBR
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("CSR-ACT-NBR=").append(ws.getWsXz0a9020Row().getCsrActNbrFormatted())
							.append(" NOT-PRC-TS=").append(ws.getWsXz0a9020Row().getNotPrcTsFormatted()).append(" FRM-SEQ-NBR=")
							.append(ws.getWsXz0a9020Row().getFrmSeqNbrFormatted()).append(" RECALL-SEQ-NBR=")
							.append(ws.getWsXz0a9020Row().getRecallRecSeqNbrFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 3400-WRITE-REC-BY-FRM-KEY-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  WRITE RECIPIENT KEY ROW TO RESPONSE UMT.                       *
	 * *****************************************************************
	 * * THE RECALL RECIPIENT SEQUENCE NUMBER IS UPDATED TO THE
	 * * NEXT RECIPIENT TO RECALL THE SERVICE WITH OR ZERO IF ALL
	 * * RECIPIENTS HAVE BEEN RETURNED.
	 * * THE VALUE IS UPDATED IN THE 3200 PARAGRAPH</pre>*/
	private void writeRecByFrmKeyRow() {
		Halrresp halrresp = null;
		// COB_CODE: SET HALRRESP-WRITE-FUNC     TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM-LIST-KEY TO HALRRESP-BUS-OBJ-NM.
		ws.getWsHalrrespLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNmListKey());
		// COB_CODE: MOVE LENGTH OF WS-XZ0A9020-ROW
		//                                       TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(((short) WsXz0a9020Row.Len.WS_XZ0A9020_ROW));
		// COB_CODE: CALL HALRRESP-HALRRESP-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRRESP-LINKAGE
		//                WS-XZ0A9020-ROW.
		halrresp = Halrresp.getInstance();
		halrresp.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrrespLinkage(), ws.getWsXz0a9020Row());
	}

	/**Original name: 4000-GET-REC-BY-NOT-INFO_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE PASSED INFO AND BUILD THE SQL QUERY FOR HIT ON THE    *
	 *  TABLES TO LOCATE THE RECIPIENTS FOR THE ACCOUNT NUMBER AND     *
	 *  TIMESTAMP FILLED PASSED IN.                                    *
	 * *****************************************************************
	 * * OPEN REQUIRED CURSOR</pre>*/
	private void getRecByNotInfo() {
		// COB_CODE: PERFORM 4100-OPEN-REC-BY-NOT-CURSOR.
		openRecByNotCursor();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		//* FETCH ALL DATA FROM REQUIRED CURSOR
		// COB_CODE: PERFORM 4200-PROCESS-REC-BY-NOT-ROWS.
		rng4200ProcessRecByNotRows();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		//* CLOSE CURSOR
		// COB_CODE: PERFORM 4300-CLOSE-REC-BY-NOT-CURSOR.
		closeRecByNotCursor();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		//* NO ERROR CREATED IF MAX ROWS IS HIT.  INSTEAD WE WILL FILL
		//* IN A RECALL RECIPIENT VALUE ON THE RECIPIENT KEY ROW.
		//* A NON-LOGGABLE WARNING IS RETURNED IF SEARCH RETURNS NOTHING
		// COB_CODE: IF WS-NOTHING-FOUND
		//               GO TO 4000-EXIT
		//           END-IF.
		if (ws.getWorkingStorageArea().isNothingFound()) {
			// COB_CODE: SET WS-NON-LOGGABLE-WARNING
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setWarning();
			// COB_CODE: MOVE WS-BUS-OBJ-NM-LIST-DETAIL
			//                                   TO NLBE-FAILED-TABLE-OR-FILE
			ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getBusObjNmListDetail());
			// COB_CODE: MOVE 'ACT_NBR'          TO NLBE-FAILED-COLUMN-OR-FIELD
			ws.getNlbeCommon().setFailedColumnOrField("ACT_NBR");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO UWRN-WARNING-CODE
			ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE XZA920-CSR-ACT-NBR TO EA-03-CSR-ACT-NBR
			ws.getEa03NothingFoundByNotMsg().setCsrActNbr(ws.getWsXz0a9020Row().getCsrActNbr());
			// COB_CODE: MOVE XZA920-NOT-PRC-TS  TO EA-03-NOT-PRC-TS
			ws.getEa03NothingFoundByNotMsg().setNotPrcTs(ws.getWsXz0a9020Row().getNotPrcTs());
			// COB_CODE: MOVE EA-03-NOTHING-FOUND-BY-NOT-MSG
			//                                   TO WS-NONLOG-ERR-ALLTXT-TEXT
			ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(ws.getEa03NothingFoundByNotMsg().getEa03NothingFoundByNotMsgFormatted());
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		//* NOTE THIS NEEDS TO BE DONE LAST TO UPDATE THE RECALL VALUE
		// COB_CODE: PERFORM 4400-WRITE-REC-BY-NOT-KEY-ROW.
		writeRecByNotKeyRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
	}

	/**Original name: 4100-OPEN-REC-BY-NOT-CURSOR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  OPEN RECIPIENT CURSOR                                          *
	 * *****************************************************************</pre>*/
	private void openRecByNotCursor() {
		// COB_CODE: MOVE XZA920-CSR-ACT-NBR     TO CSR-ACT-NBR
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setCsrActNbr(ws.getWsXz0a9020Row().getCsrActNbr());
		// COB_CODE: MOVE XZA920-NOT-PRC-TS      TO NOT-PRC-TS
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setNotPrcTs(ws.getWsXz0a9020Row().getNotPrcTs());
		// COB_CODE: MOVE XZA920-RECALL-REC-SEQ-NBR
		//                                       TO REC-SEQ-NBR
		//                                       OF DCLACT-NOT-REC.
		ws.getDclactNotRec().setRecSeqNbr(Trunc.toShort(ws.getWsXz0a9020Row().getRecallRecSeqNbr(), 4));
		// COB_CODE: EXEC SQL
		//               OPEN RECIPIENT_BY_NOT_CSR
		//           END-EXEC.
		actNotRecTypDao.openRecipientByNotCsr(ws.getDclactNotRec().getCsrActNbr(), ws.getDclactNotRec().getNotPrcTs(),
				ws.getDclactNotRec().getRecSeqNbr());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'ACT_NOT_REC'      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_REC");
			// COB_CODE: MOVE '4100-OPEN-REC-BY-NOT-CURSOR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4100-OPEN-REC-BY-NOT-CURSOR");
			// COB_CODE: MOVE 'OPEN RECIPIENT_BY_NOT_CSR FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN RECIPIENT_BY_NOT_CSR FAILED");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZA920-CSR-ACT-NBR
			//                  ' NOT-PRC-TS='
			//                  XZA920-NOT-PRC-TS
			//                  ' RECALL-SEQ-NBR='
			//                  XZA920-RECALL-REC-SEQ-NBR
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("CSR-ACT-NBR=").append(ws.getWsXz0a9020Row().getCsrActNbrFormatted())
							.append(" NOT-PRC-TS=").append(ws.getWsXz0a9020Row().getNotPrcTsFormatted()).append(" RECALL-SEQ-NBR=")
							.append(ws.getWsXz0a9020Row().getRecallRecSeqNbrFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 4200-PROCESS-REC-BY-NOT-ROWS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CONTROL FETCH DATA FROM RECIPIENT CURSOR                       *
	 * *****************************************************************
	 * * MAX ROWS IS SET FROM A CONSTANT FIELD DEFINED IN THE PROXY
	 * * AND PASSED IN ON THE BPO COPYBOOK.</pre>*/
	private void processRecByNotRows() {
		// COB_CODE: SET WS-NOTHING-FOUND        TO TRUE.
		ws.getWorkingStorageArea().setNothingFound();
	}

	/**Original name: 4200-A<br>*/
	private String a1() {
		// COB_CODE: PERFORM 4210-FETCH-REC-BY-NOT-CURSOR.
		fetchRecByNotCursor();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//             OR
		//              SW-END-OF-CURSOR
		//               GO TO 4200-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn() || ws.isSwEndOfCursorFlag()) {
			// COB_CODE: MOVE +0                 TO XZA920-RECALL-REC-SEQ-NBR
			ws.getWsXz0a9020Row().setRecallRecSeqNbr(0);
			// move zero to the recall field
			// COB_CODE: GO TO 4200-EXIT
			return "";
		}
		// COB_CODE: ADD +1                      TO WS-ROW-COUNT.
		ws.getWorkingStorageArea().setRowCount(Trunc.toShort(1 + ws.getWorkingStorageArea().getRowCount(), 4));
		// COB_CODE:      IF WS-ROW-COUNT > XZA920-MAX-REC-ROWS
		//           * move currently fetched recipient seq nbr to the recall field
		//                    GO TO 4200-EXIT
		//                END-IF.
		if (ws.getWorkingStorageArea().getRowCount() > ws.getWsXz0a9020Row().getMaxRecRows()) {
			// move currently fetched recipient seq nbr to the recall field
			// COB_CODE: MOVE REC-SEQ-NBR     OF DCLACT-NOT-REC
			//                                   TO XZA920-RECALL-REC-SEQ-NBR
			ws.getWsXz0a9020Row().setRecallRecSeqNbr(ws.getDclactNotRec().getRecSeqNbr());
			// COB_CODE: GO TO 4200-EXIT
			return "";
		}
		// COB_CODE: PERFORM 9100-WRITE-REC-DTL-ROW.
		rng9100WriteRecDtlRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4200-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4200-EXIT
			return "";
		}
		// COB_CODE: GO TO 4200-A.
		return "4200-A";
	}

	/**Original name: 4210-FETCH-REC-BY-NOT-CURSOR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FETCH ROW FROM THE RECIPIENT CURSOR                            *
	 * *****************************************************************</pre>*/
	private void fetchRecByNotCursor() {
		// COB_CODE: INITIALIZE DCLACT-NOT-REC
		//                      DCLREC-TYP.
		initDclactNotRec();
		initDclrecTyp();
		// COB_CODE: EXEC SQL
		//               FETCH RECIPIENT_BY_NOT_CSR
		//                   INTO :DCLACT-NOT-REC.REC-SEQ-NBR
		//                      , :DCLACT-NOT-REC.REC-TYP-CD
		//                      , :DCLACT-NOT-REC.REC-NM
		//                             :NI-REC-NM
		//                      , :DCLACT-NOT-REC.LIN-1-ADR
		//                             :NI-LIN-1-ADR
		//                      , :DCLACT-NOT-REC.LIN-2-ADR
		//                             :NI-LIN-2-ADR
		//                      , :DCLACT-NOT-REC.CIT-NM
		//                             :NI-CIT-NM
		//                      , :DCLACT-NOT-REC.ST-ABB
		//                             :NI-ST-ABB
		//                      , :DCLACT-NOT-REC.PST-CD
		//                             :NI-PST-CD
		//                      , :DCLACT-NOT-REC.CER-NBR
		//                             :NI-CER-NBR
		//                      , :DCLREC-TYP.REC-LNG-DES
		//                      , :DCLREC-TYP.REC-SR-ORD-NBR
		//           END-EXEC.
		actNotRecTypDao.fetchRecipientByNotCsr(ws.getActNotRecTypXz0b9020());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 4210-EXIT
		//               WHEN OTHER
		//                   GO TO 4210-EXIT
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET SW-END-OF-CURSOR
			//                               TO TRUE
			ws.setSwEndOfCursorFlag(true);
			// COB_CODE: GO TO 4210-EXIT
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'ACT_NOT_REC'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_REC");
			// COB_CODE: MOVE '4210-FETCH-REC-BY-NOT-CURSOR'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4210-FETCH-REC-BY-NOT-CURSOR");
			// COB_CODE: MOVE 'FETCH RECIPIENT_BY_NOT_CSR FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH RECIPIENT_BY_NOT_CSR FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4210-EXIT
			return;
		}
	}

	/**Original name: 4300-CLOSE-REC-BY-NOT-CURSOR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CLOSE RECIPIENT CURSOR                                         *
	 * *****************************************************************</pre>*/
	private void closeRecByNotCursor() {
		// COB_CODE: EXEC SQL
		//               CLOSE RECIPIENT_BY_NOT_CSR
		//           END-EXEC.
		actNotRecTypDao.closeRecipientByNotCsr();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'ACT_NOT_REC'      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("ACT_NOT_REC");
			// COB_CODE: MOVE '4300-CLOSE-REC-BY-NOT-CURSOR'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4300-CLOSE-REC-BY-NOT-CURSOR");
			// COB_CODE: MOVE 'CLOSE RECIPIENT_BY_NOT_CSR FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE RECIPIENT_BY_NOT_CSR FAILED");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'CSR-ACT-NBR='
			//                  XZA920-CSR-ACT-NBR
			//                  ' NOT-PRC-TS='
			//                  XZA920-NOT-PRC-TS
			//                  ' RECALL-SEQ-NBR='
			//                  XZA920-RECALL-REC-SEQ-NBR
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("CSR-ACT-NBR=").append(ws.getWsXz0a9020Row().getCsrActNbrFormatted())
							.append(" NOT-PRC-TS=").append(ws.getWsXz0a9020Row().getNotPrcTsFormatted()).append(" RECALL-SEQ-NBR=")
							.append(ws.getWsXz0a9020Row().getRecallRecSeqNbrFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 4400-WRITE-REC-BY-NOT-KEY-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  WRITE RECIPIENT KEY ROW TO RESPONSE UMT.                       *
	 * *****************************************************************
	 * * THE RECALL RECIPIENT SEQUENCE NUMBER IS UPDATED TO THE
	 * * NEXT RECIPIENT TO RECALL THE SERVICE WITH OR ZERO IF ALL
	 * * RECIPIENTS HAVE BEEN RETURNED.
	 * * THE VALUE IS UPDATED IN THE 4200 PARAGRAPH</pre>*/
	private void writeRecByNotKeyRow() {
		Halrresp halrresp = null;
		// COB_CODE: SET HALRRESP-WRITE-FUNC     TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM-LIST-KEY TO HALRRESP-BUS-OBJ-NM.
		ws.getWsHalrrespLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNmListKey());
		// COB_CODE: MOVE LENGTH OF WS-XZ0A9020-ROW
		//                                       TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(((short) WsXz0a9020Row.Len.WS_XZ0A9020_ROW));
		// COB_CODE: CALL HALRRESP-HALRRESP-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRRESP-LINKAGE
		//                WS-XZ0A9020-ROW.
		halrresp = Halrresp.getInstance();
		halrresp.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrrespLinkage(), ws.getWsXz0a9020Row());
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWorkingStorageArea().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0B9020", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getCommInfo().setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getCommInfo().setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWorkingStorageArea().getApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: 9100-WRITE-REC-DTL-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  WRITE ROW TO RESPONSE UMT                                      *
	 * *****************************************************************</pre>*/
	private String writeRecDtlRow() {
		String retcode = "";
		Halrresp halrresp = null;
		// COB_CODE: INITIALIZE WS-XZ0A9021-ROW.
		initWsXz0a9021Row();
		// COB_CODE: MOVE REC-SR-ORD-NBR   OF DCLREC-TYP
		//                                       TO XZA921-REC-SORT-ORD-NBR.
		ws.getWsXz0a9021Row().setRecSortOrdNbr(ws.getDclrecTyp().getRecSrOrdNbr());
		// COB_CODE: MOVE REC-SEQ-NBR      OF DCLACT-NOT-REC
		//                                       TO XZA921-REC-SEQ-NBR.
		ws.getWsXz0a9021Row().setRecSeqNbr(ws.getDclactNotRec().getRecSeqNbr());
		// COB_CODE: MOVE REC-TYP-CD       OF DCLACT-NOT-REC
		//                                       TO XZA921-REC-TYP-CD.
		ws.getWsXz0a9021Row().setRecTypCd(ws.getDclactNotRec().getRecTypCd());
		// COB_CODE: MOVE REC-LNG-DES      OF DCLREC-TYP
		//                                       TO XZA921-REC-TYP-DES.
		ws.getWsXz0a9021Row().setRecTypDes(ws.getDclrecTyp().getRecLngDes());
		// COB_CODE: IF NI-CER-NBR  = -1
		//               MOVE SPACES             TO XZA921-CER-NBR
		//           ELSE
		//                                       TO XZA921-CER-NBR
		//           END-IF.
		if (ws.getNiCerNbr() == -1) {
			// COB_CODE: MOVE SPACES             TO XZA921-CER-NBR
			ws.getWsXz0a9021Row().setCerNbr("");
		} else {
			// COB_CODE: MOVE CER-NBR      OF DCLACT-NOT-REC
			//                                   TO XZA921-CER-NBR
			ws.getWsXz0a9021Row().setCerNbr(ws.getDclactNotRec().getCerNbr());
		}
		// COB_CODE: PERFORM 9110-FORMAT-REC-NM-ADR.
		retcode = rng9110FormatRecNmAdr();
		if (!retcode.equals("")) {
			return retcode;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9100-EXIT
			return "";
		}
		// COB_CODE: SET HALRRESP-WRITE-FUNC     TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM-LIST-DETAIL
		//                                       TO HALRRESP-BUS-OBJ-NM.
		ws.getWsHalrrespLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjNmListDetail());
		// COB_CODE: MOVE LENGTH OF WS-XZ0A9021-ROW
		//                                       TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(((short) WsXz0a9021Row.Len.WS_XZ0A9021_ROW));
		// COB_CODE: CALL HALRRESP-HALRRESP-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRRESP-LINKAGE
		//                WS-XZ0A9021-ROW.
		halrresp = Halrresp.getInstance();
		halrresp.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrrespLinkage(), ws.getWsXz0a9021Row());
		return "";
	}

	/**Original name: 9110-FORMAT-REC-NM-ADR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  USE THE ADDRESS FORMATTER UTILITY PROGRAM TO FORMAT THE        *
	 *  RECIPIENT NAME AND ADDRESS FIELDS                              *
	 * *****************************************************************
	 *  SET UP INPUT TO UTILTY PROGRAM</pre>*/
	private String formatRecNmAdr() {
		String retcode = "";
		// COB_CODE: PERFORM 9111-SET-NM-ADR-INPUT.
		setNmAdrInput();
		// CALL UTILITY PROGRAM
		// COB_CODE: PERFORM 9113-CALL-ADR-FORMATTER-PGM.
		retcode = rng9113CallAdrFormatterPgm();
		if (!retcode.equals("")) {
			return retcode;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9110-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9110-EXIT
			return "";
		}
		// STORE OFF THE FORMATTED NAME AND ADDRESS
		// COB_CODE: PERFORM 9115-STORE-REC-NM-ADR.
		storeRecNmAdr();
		return "";
	}

	/**Original name: 9111-SET-NM-ADR-INPUT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET UP INPUT TO THE ADDRESS FORMATTER UTILITY PROGRAM          *
	 * *****************************************************************</pre>*/
	private void setNmAdrInput() {
		// COB_CODE: INITIALIZE FA-ADDRESS-INPUTS.
		initAddressInputs();
		// COB_CODE: IF NI-REC-NM   = -1
		//               MOVE SPACES             TO FA-AI-DISPLAY-NAME
		//           ELSE
		//                                       TO FA-AI-DISPLAY-NAME
		//           END-IF.
		if (ws.getNiRecNm() == -1) {
			// COB_CODE: MOVE SPACES             TO FA-AI-DISPLAY-NAME
			ws.getTs52901().getAddressInputs().setDisplayName("");
		} else {
			// COB_CODE: MOVE REC-NM-TEXT  OF DCLACT-NOT-REC
			//                                   TO FA-AI-DISPLAY-NAME
			ws.getTs52901().getAddressInputs().setDisplayName(ws.getDclactNotRec().getRecNmText());
		}
		// COB_CODE: IF NI-LIN-1-ADR   = -1
		//               MOVE SPACES             TO FA-AI-ADR-LINE-1
		//           ELSE
		//                                       TO FA-AI-ADR-LINE-1
		//           END-IF.
		if (ws.getNiLin1Adr() == -1) {
			// COB_CODE: MOVE SPACES             TO FA-AI-ADR-LINE-1
			ws.getTs52901().getAddressInputs().setAdrLine1("");
		} else {
			// COB_CODE: MOVE LIN-1-ADR    OF DCLACT-NOT-REC
			//                                   TO FA-AI-ADR-LINE-1
			ws.getTs52901().getAddressInputs().setAdrLine1(ws.getDclactNotRec().getLin1Adr());
		}
		// COB_CODE: IF NI-LIN-2-ADR   = -1
		//               MOVE SPACES             TO FA-AI-ADR-LINE-2
		//           ELSE
		//                                       TO FA-AI-ADR-LINE-2
		//           END-IF.
		if (ws.getNiLin2Adr() == -1) {
			// COB_CODE: MOVE SPACES             TO FA-AI-ADR-LINE-2
			ws.getTs52901().getAddressInputs().setAdrLine2("");
		} else {
			// COB_CODE: MOVE LIN-2-ADR    OF DCLACT-NOT-REC
			//                                   TO FA-AI-ADR-LINE-2
			ws.getTs52901().getAddressInputs().setAdrLine2(ws.getDclactNotRec().getLin2Adr());
		}
		// COB_CODE: IF NI-CIT-NM      = -1
		//               MOVE SPACES             TO FA-AI-CITY
		//           ELSE
		//                                       TO FA-AI-CITY
		//           END-IF.
		if (ws.getNiCitNm() == -1) {
			// COB_CODE: MOVE SPACES             TO FA-AI-CITY
			ws.getTs52901().getAddressInputs().setCity("");
		} else {
			// COB_CODE: MOVE CIT-NM       OF DCLACT-NOT-REC
			//                                   TO FA-AI-CITY
			ws.getTs52901().getAddressInputs().setCity(ws.getDclactNotRec().getCitNm());
		}
		// COB_CODE: IF NI-ST-ABB      = -1
		//               MOVE SPACES             TO FA-AI-STATE-ABB
		//           ELSE
		//                                       TO FA-AI-STATE-ABB
		//           END-IF.
		if (ws.getNiStAbb() == -1) {
			// COB_CODE: MOVE SPACES             TO FA-AI-STATE-ABB
			ws.getTs52901().getAddressInputs().setStateAbb("");
		} else {
			// COB_CODE: MOVE ST-ABB       OF DCLACT-NOT-REC
			//                                   TO FA-AI-STATE-ABB
			ws.getTs52901().getAddressInputs().setStateAbb(ws.getDclactNotRec().getStAbb());
		}
		// COB_CODE: IF NI-PST-CD      = -1
		//               MOVE SPACES             TO FA-AI-POSTAL-CODE
		//           ELSE
		//                                       TO FA-AI-POSTAL-CODE
		//           END-IF.
		if (ws.getNiPstCd() == -1) {
			// COB_CODE: MOVE SPACES             TO FA-AI-POSTAL-CODE
			ws.getTs52901().getAddressInputs().setPostalCode("");
		} else {
			// COB_CODE: MOVE PST-CD       OF DCLACT-NOT-REC
			//                                   TO FA-AI-POSTAL-CODE
			ws.getTs52901().getAddressInputs().setPostalCode(ws.getDclactNotRec().getPstCd());
		}
	}

	/**Original name: 9113-CALL-ADR-FORMATTER-PGM_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE ADDRESS FORMATTER UTILITY PROGRAM TO FORMAT THE       *
	 *  THE NAME AND ADDRESS FIELDS                                    *
	 * *****************************************************************</pre>*/
	private String callAdrFormatterPgm() {
		Ts529099 ts529099 = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: CALL CF-FORMAT-ADDRESS-PGM USING FORMAT-ADDRESS-PARMS.
		ts529099 = Ts529099.getInstance();
		ts529099.run(ws.getTs52901());
		// COB_CODE: IF NOT FA-RC-GOOD-RUN
		//               GO TO 9100-EXIT
		//           END-IF.
		if (!ws.getTs52901().getReturnCode().isFaRcGoodRun()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-TRANSFER-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalTransferFailed();
			// COB_CODE: SET ETRA-COBOL-CALL     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCobolCall();
			// COB_CODE: MOVE CF-FORMAT-ADDRESS-PGM
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getCfFormatAddressPgm());
			// COB_CODE: MOVE '9113-CALL-ADR-FORMATTER-PGM'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9113-CALL-ADR-FORMATTER-PGM");
			// COB_CODE: STRING 'ERR RETURNED FROM PROGRAM: '
			//                  CF-FORMAT-ADDRESS-PGM
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ERR-COMMENT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "ERR RETURNED FROM PROGRAM: ",
					ws.getCfFormatAddressPgmFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: STRING 'RETURN CODE: '
			//                  FA-RETURN-CODE
			//                  '; CSR-ACT-NBR='
			//                  XZA920-CSR-ACT-NBR
			//                  '; NOT-PRC-TS='
			//                  XZA920-NOT-PRC-TS
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "RETURN CODE: ", ws.getTs52901().getReturnCode().getReturnCodeAsString(), "; CSR-ACT-NBR=",
							ws.getWsXz0a9020Row().getCsrActNbrFormatted(), "; NOT-PRC-TS=", ws.getWsXz0a9020Row().getNotPrcTsFormatted() });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9100-EXIT
			return "9100-EXIT";
		}
		return "";
	}

	/**Original name: 9115-STORE-REC-NM-ADR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  STORE OFF THE FORMATTED RECIPIENT NAME AND ADDRESS
	 * *****************************************************************</pre>*/
	private void storeRecNmAdr() {
		// COB_CODE: MOVE FA-FA-LINE-1           TO XZA921-NM-ADR-LIN-1.
		ws.getWsXz0a9021Row().setNmAdrLin1(ws.getTs52901().getFormattedAddressLines().getLine1());
		// COB_CODE: MOVE FA-FA-LINE-2           TO XZA921-NM-ADR-LIN-2.
		ws.getWsXz0a9021Row().setNmAdrLin2(ws.getTs52901().getFormattedAddressLines().getLine2());
		// COB_CODE: MOVE FA-FA-LINE-3           TO XZA921-NM-ADR-LIN-3.
		ws.getWsXz0a9021Row().setNmAdrLin3(ws.getTs52901().getFormattedAddressLines().getLine3());
		// COB_CODE: MOVE FA-FA-LINE-4           TO XZA921-NM-ADR-LIN-4.
		ws.getWsXz0a9021Row().setNmAdrLin4(ws.getTs52901().getFormattedAddressLines().getLine4());
		// COB_CODE: MOVE FA-FA-LINE-5           TO XZA921-NM-ADR-LIN-5.
		ws.getWsXz0a9021Row().setNmAdrLin5(ws.getTs52901().getFormattedAddressLines().getLine5());
		// COB_CODE: MOVE FA-FA-LINE-6           TO XZA921-NM-ADR-LIN-6.
		ws.getWsXz0a9021Row().setNmAdrLin6(ws.getTs52901().getFormattedAddressLines().getLine6());
	}

	/**Original name: RNG_3200-PROCESS-REC-BY-FRM-ROWS_FIRST_SENTENCES-_-3200-EXIT<br>*/
	private void rng3200ProcessRecByFrmRows() {
		String retcode = "";
		boolean goto3200A = false;
		processRecByFrmRows();
		do {
			goto3200A = false;
			retcode = a();
		} while (retcode.equals("3200-A"));
	}

	/**Original name: RNG_4200-PROCESS-REC-BY-NOT-ROWS_FIRST_SENTENCES-_-4200-EXIT<br>*/
	private void rng4200ProcessRecByNotRows() {
		String retcode = "";
		boolean goto4200A = false;
		processRecByNotRows();
		do {
			goto4200A = false;
			retcode = a1();
		} while (retcode.equals("4200-A"));
	}

	/**Original name: RNG_9100-WRITE-REC-DTL-ROW_FIRST_SENTENCES-_-9100-EXIT<br>*/
	private void rng9100WriteRecDtlRow() {
		String retcode = "";
		boolean goto9100Exit = false;
		retcode = writeRecDtlRow();
		goto9100Exit = false;
	}

	/**Original name: RNG_9110-FORMAT-REC-NM-ADR_FIRST_SENTENCES-_-9110-EXIT_M<br>*/
	private String rng9110FormatRecNmAdr() {
		String retcode = "";
		retcode = formatRecNmAdr();
		if (!retcode.equals("")) {
			return retcode;
		}
		return "";
	}

	/**Original name: RNG_9113-CALL-ADR-FORMATTER-PGM_FIRST_SENTENCES-_-9113-EXIT_M<br>*/
	private String rng9113CallAdrFormatterPgm() {
		String retcode = "";
		retcode = callAdrFormatterPgm();
		if (!retcode.equals("")) {
			return retcode;
		}
		return "";
	}

	public void initWsXz0a9020Row() {
		ws.getWsXz0a9020Row().setMaxRecRows(((short) 0));
		ws.getWsXz0a9020Row().getOperation().setOperation("");
		ws.getWsXz0a9020Row().setCsrActNbr("");
		ws.getWsXz0a9020Row().setNotPrcTs("");
		ws.getWsXz0a9020Row().setFrmSeqNbr(0);
		ws.getWsXz0a9020Row().setRecallRecSeqNbr(0);
		ws.getWsXz0a9020Row().setUserid("");
	}

	public void initDclactNotRec() {
		ws.getDclactNotRec().setCsrActNbr("");
		ws.getDclactNotRec().setNotPrcTs("");
		ws.getDclactNotRec().setRecSeqNbr(((short) 0));
		ws.getDclactNotRec().setRecTypCd("");
		ws.getDclactNotRec().setRecCltId("");
		ws.getDclactNotRec().setRecNmLen(((short) 0));
		ws.getDclactNotRec().setRecNmText("");
		ws.getDclactNotRec().setRecAdrId("");
		ws.getDclactNotRec().setLin1Adr("");
		ws.getDclactNotRec().setLin2Adr("");
		ws.getDclactNotRec().setCitNm("");
		ws.getDclactNotRec().setStAbb("");
		ws.getDclactNotRec().setPstCd("");
		ws.getDclactNotRec().setMnlInd(Types.SPACE_CHAR);
		ws.getDclactNotRec().setCerNbr("");
	}

	public void initDclrecTyp() {
		ws.getDclrecTyp().setRecTypCd("");
		ws.getDclrecTyp().setRecShtDes("");
		ws.getDclrecTyp().setRecLngDes("");
		ws.getDclrecTyp().setRecSrOrdNbr(((short) 0));
		ws.getDclrecTyp().setAcyInd(Types.SPACE_CHAR);
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public void initWsXz0a9021Row() {
		ws.getWsXz0a9021Row().setRecSortOrdNbr(0);
		ws.getWsXz0a9021Row().setRecSeqNbr(0);
		ws.getWsXz0a9021Row().setRecTypCd("");
		ws.getWsXz0a9021Row().setRecTypDes("");
		ws.getWsXz0a9021Row().setCerNbr("");
		ws.getWsXz0a9021Row().setNmAdrLin1("");
		ws.getWsXz0a9021Row().setNmAdrLin2("");
		ws.getWsXz0a9021Row().setNmAdrLin3("");
		ws.getWsXz0a9021Row().setNmAdrLin4("");
		ws.getWsXz0a9021Row().setNmAdrLin5("");
		ws.getWsXz0a9021Row().setNmAdrLin6("");
	}

	public void initAddressInputs() {
		ws.getTs52901().getAddressInputs().setDisplayName("");
		ws.getTs52901().getAddressInputs().setNameLine1("");
		ws.getTs52901().getAddressInputs().setNameLine2("");
		ws.getTs52901().getAddressInputs().setAdrLine1("");
		ws.getTs52901().getAddressInputs().setAdrLine2("");
		ws.getTs52901().getAddressInputs().setCity("");
		ws.getTs52901().getAddressInputs().setStateAbb("");
		ws.getTs52901().getAddressInputs().setPostalCode("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
