/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CAWLF004<br>
 * Variable: CAWLF004 from copybook CAWLF004<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cawlf004 {

	//==== PROPERTIES ====
	//Original name: CW04F-CLIENT-PHONE-CHK-SUM
	private String clientPhoneChkSum = DefaultValues.stringVal(Len.CLIENT_PHONE_CHK_SUM);
	//Original name: CW04F-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CW04F-CIPH-PHN-SEQ-NBR-KCRE
	private String ciphPhnSeqNbrKcre = DefaultValues.stringVal(Len.CIPH_PHN_SEQ_NBR_KCRE);
	//Original name: CW04F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW04F-CLIENT-PHONE-KEY
	private Cw01fBusinessClientKey clientPhoneKey = new Cw01fBusinessClientKey();
	//Original name: CW04F-CLIENT-PHONE-DATA
	private Cw04fClientPhoneData clientPhoneData = new Cw04fClientPhoneData();

	//==== METHODS ====
	public void setClientPhoneRowFormatted(String data) {
		byte[] buffer = new byte[Len.CLIENT_PHONE_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CLIENT_PHONE_ROW);
		setClientPhoneRowBytes(buffer, 1);
	}

	public String getClientPhoneRowFormatted() {
		return MarshalByteExt.bufferToStr(getClientPhoneRowBytes());
	}

	/**Original name: CW04F-CLIENT-PHONE-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CAWLF004 - CLIENT_PHONE TABLE                                  *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  PP00015  03/07/2007 E404ASW   NEW COPYBOOK                     *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getClientPhoneRowBytes() {
		byte[] buffer = new byte[Len.CLIENT_PHONE_ROW];
		return getClientPhoneRowBytes(buffer, 1);
	}

	public void setClientPhoneRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setClientPhoneFixedBytes(buffer, position);
		position += Len.CLIENT_PHONE_FIXED;
		setClientPhoneDatesBytes(buffer, position);
		position += Len.CLIENT_PHONE_DATES;
		clientPhoneKey.setBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		clientPhoneData.setClientPhoneDataBytes(buffer, position);
	}

	public byte[] getClientPhoneRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getClientPhoneFixedBytes(buffer, position);
		position += Len.CLIENT_PHONE_FIXED;
		getClientPhoneDatesBytes(buffer, position);
		position += Len.CLIENT_PHONE_DATES;
		clientPhoneKey.getBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		clientPhoneData.getClientPhoneDataBytes(buffer, position);
		return buffer;
	}

	public void setClientPhoneFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		clientPhoneChkSum = MarshalByte.readFixedString(buffer, position, Len.CLIENT_PHONE_CHK_SUM);
		position += Len.CLIENT_PHONE_CHK_SUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		ciphPhnSeqNbrKcre = MarshalByte.readString(buffer, position, Len.CIPH_PHN_SEQ_NBR_KCRE);
	}

	public byte[] getClientPhoneFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientPhoneChkSum, Len.CLIENT_PHONE_CHK_SUM);
		position += Len.CLIENT_PHONE_CHK_SUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		MarshalByte.writeString(buffer, position, ciphPhnSeqNbrKcre, Len.CIPH_PHN_SEQ_NBR_KCRE);
		return buffer;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setCiphPhnSeqNbrKcre(String ciphPhnSeqNbrKcre) {
		this.ciphPhnSeqNbrKcre = Functions.subString(ciphPhnSeqNbrKcre, Len.CIPH_PHN_SEQ_NBR_KCRE);
	}

	public String getCiphPhnSeqNbrKcre() {
		return this.ciphPhnSeqNbrKcre;
	}

	public void setClientPhoneDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getClientPhoneDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public Cw04fClientPhoneData getClientPhoneData() {
		return clientPhoneData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_PHONE_CHK_SUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int CIPH_PHN_SEQ_NBR_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLIENT_PHONE_FIXED = CLIENT_PHONE_CHK_SUM + CLIENT_ID_KCRE + CIPH_PHN_SEQ_NBR_KCRE;
		public static final int CLIENT_PHONE_DATES = TRANS_PROCESS_DT;
		public static final int CLIENT_PHONE_ROW = CLIENT_PHONE_FIXED + CLIENT_PHONE_DATES + Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY
				+ Cw04fClientPhoneData.Len.CLIENT_PHONE_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
