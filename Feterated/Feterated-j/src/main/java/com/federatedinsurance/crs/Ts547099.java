/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.ws.Ea64UsrCicsServerError;
import com.federatedinsurance.crs.ws.Ts547099Data;
import com.federatedinsurance.crs.ws.enums.LCmEiErrorSeverity;
import com.federatedinsurance.crs.ws.occurs.TpPipeInf;
import com.federatedinsurance.crs.ws.ptr.LCicsPipeDataToPass;
import com.federatedinsurance.crs.ws.ptr.LCicsPipeInterfaceContract;
import com.federatedinsurance.crs.ws.ptr.LNullPtr;
import com.federatedinsurance.crs.ws.ptr.LServerMessage;
import com.federatedinsurance.crs.ws.redefines.LCicsPipeContractAddress;
import com.federatedinsurance.crs.ws.redefines.LCicsPipeDataAddress;
import com.modernsystems.ctu.core.ProgramInfo;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: TS547099<br>
 * <pre>AUTHOR.        DARREN M. ALBERS
 * DATE-WRITTEN.  19 NOV 2008.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - COBOL CICS PIPE HANDLING PROGRAM            **
 * *                                                             **
 * * PURPOSE -  THIS PROGRAM WILL HANDLE THE FUNCTIONALITY       **
 * *            OF THE CICS PIPE COMMANDS.  THE COMMANDS INCLUDE:**
 * *            * OPEN CICS PIPE (THROUGH SUBPROGRAM)            **
 * *            * CALL CICS PROGRAM USING CICS PIPE              **
 * *            * CLOSE CICS PIPE (THROUGH SUBPROGRAM)           **
 * *            * GET TOKEN INFORMATION                          **
 * *                                                             **
 * * INPUT  -LINKAGE SECTION                                     **
 * *          : FUNCTION                                         **
 * *          : CICS REGION (APPL ID)                            **
 * *          : CICS PROGRAM NAME                                **
 * *          : TARGET TRAN ID                                   **
 * *          : LENGTH OF DATA                                   **
 * *          : DATA (COMMAREA) (OPTIONAL)                       **
 * *                                                             **
 * * OUTPUT -LINKAGE SECTION                                     **
 * *          : CICS REGION CONNECTED (APPL ID)                  **
 * *          : ERROR INFORMATION                                **
 * *          : PIPE IDENTIFICATION TOKENS                       **
 * *          : MEMORY PASSED BACK (COMMAREA)                    **
 * *                                                             **
 * * ASSUMPTIONS/NOTES                                           **
 * *        - ERROR HANDLING HAS BEEN SIMPLIFIED FOR THE         **
 * *           AS BEST AS POSSIBLE, BUT NOT EVERY ERROR CAN BE   **
 * *           PREDICTED BY THIS PROGRAM. THUS, ERROR MESSAGES   **
 * *           WILL VARY BASED ON TYPES OF ERRORS ENCOUNTERED.   **
 * *                                                             **
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * -------  ----------  --------   ----------------------------**
 * * TL000059  11/19/2008 E404DMA    NEW                         **
 * * TL000093  03/26/2009 E404DMA    EXPANDED EXCEPTION HANDLING **
 * *                                 TO CHECK THE DPL RESP VALUES**
 * *                                 DURING A CICS CALL PROCESS  **
 * * CH00700-2 07/30/2009 E404DMA    REMOVED EDIT OF TRANS ID TO **
 * *                                 ENSURE THEY WERE SET UP FOR **
 * *                                 EXCI CALLS                  **
 * * TL000163  12/16/2009 E404DMA    REMOVED UPDATING OF APPL ID **
 * *                                 FIELD IN LINKAGE SECTION    **
 * *                                 WHEN REDIRECTION LOGIC TAKES**
 * *                                 PLACE.  UPDATED TO RETURN   **
 * *                                 CURRENT APPL ID WHEN TOKENS **
 * *                                 ARE REQUESTED BY CONSUMER.  **
 * * TL000217  07/22/2010 E404DMA    ADD CUSTOM ERROR MESSAGE FOR**
 * *                                 WHEN USER FORGETS JCL LIB.  **
 * * TL000271  02/18/2011 E404DMA    PARTIAL REWRITE OF LOGIC TO **
 * *                                 HANDLE PIPE REDIRECTION.    **
 * *                                 NEEDED TO ELIMINATE VARIOUS **
 * *                                 GAPS IN THE LOGIC.          **
 * ****************************************************************</pre>*/
@ProgramInfo(name = "TS547099", isByValue = true)
public class Ts547099 extends Program {

	//==== PROPERTIES ====
	@Inject
	private IPointerManager pointerManager;
	//Original name: WORKING-STORAGE
	private Ts547099Data ws = new Ts547099Data();
	//Original name: L-CICS-PIPE-CONTRACT-ADDRESS
	private LCicsPipeContractAddress lCicsPipeContractAddress;
	//Original name: L-CICS-PIPE-DATA-ADDRESS
	private LCicsPipeDataAddress lCicsPipeDataAddress;
	/**Original name: L-CICS-PIPE-INTERFACE-CONTRACT<br>
	 * <pre>    BRING IN A COPY OF THE CICS PIPE INTERFACE CONTRACT
	 *  COPYBOOK USED FOR CICS PIPE INTERFACE COMMON ROUTINE</pre>*/
	private LCicsPipeInterfaceContract lCicsPipeInterfaceContract = new LCicsPipeInterfaceContract(null);
	/**Original name: L-CICS-PIPE-DATA-TO-PASS<br>
	 * <pre>ANY CHANGE IN THE MAX AMOUNT OF DATA TO PASS NEEDS TO BE
	 *     REFLECTED IN THE SIZE OF THIS FIELD AND IN THE CONSTANT FIELD</pre>*/
	private LCicsPipeDataToPass lCicsPipeDataToPass = new LCicsPipeDataToPass(null);
	/**Original name: L-NULL-PTR<br>
	 * <pre>THIS IS NEEDED FOR THE CICS CALLS</pre>*/
	private LNullPtr lNullPtr = new LNullPtr(null);
	/**Original name: L-SERVER-MESSAGE<br>
	 * <pre>    THIS IS NEEDED TO CAPTURE THE CICS SERVER MESSAGE WHEN AN
	 *       ERROR IS FOUND.  THE MESSAGE LENGTH IS VARIABLE, SO A
	 *       LENGTH OF ONE IS JUST AS ACCURATE AS ANY OTHER LENGTH.</pre>*/
	private LServerMessage lServerMessage = new LServerMessage(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LCicsPipeContractAddress lCicsPipeContractAddress, LCicsPipeDataAddress lCicsPipeDataAddress) {
		this.lCicsPipeContractAddress = lCicsPipeContractAddress;
		this.lCicsPipeDataAddress = lCicsPipeDataAddress;
		processPipeRequest();
		programExit();
		return 0;
	}

	public static Ts547099 getInstance() {
		return (Programs.getInstance(Ts547099.class));
	}

	/**Original name: 1000-PROCESS-PIPE-REQUEST<br>*/
	private void processPipeRequest() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 1000-PROGRAM-EXIT
		//           END-IF.
		if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN L-CI-FN-OPEN-PIPE
		//                      THRU 3000-EXIT
		//               WHEN L-CI-FN-CALL-CICS-PGM-NO-SYNC
		//               WHEN L-CI-FN-CALL-CICS-PGM-SYNC
		//                      THRU 4000-EXIT
		//               WHEN L-CI-FN-CLOSE-PIPE
		//                      THRU 5000-EXIT
		//               WHEN L-CI-FN-GET-TOKEN-INF
		//                      THRU 6000-EXIT
		//               WHEN OTHER
		//                                       TO L-CI-EI-MESSAGE
		//           END-EVALUATE.
		switch (lCicsPipeInterfaceContract.getFunction()) {

		case LCicsPipeInterfaceContract.FN_OPEN_PIPE:// COB_CODE: PERFORM 3000-OPEN-PIPE
			//              THRU 3000-EXIT
			openPipe();
			break;

		case LCicsPipeInterfaceContract.FN_CALL_CICS_PGM_NO_SYNC:
		case LCicsPipeInterfaceContract.FN_CALL_CICS_PGM_SYNC:// COB_CODE: PERFORM 4000-CALL-CICS-USING-PIPE
			//              THRU 4000-EXIT
			rng4000CallCicsUsingPipe();
			break;

		case LCicsPipeInterfaceContract.FN_CLOSE_PIPE:// COB_CODE: PERFORM 5000-CLOSE-PIPE
			//              THRU 5000-EXIT
			closePipe();
			break;

		case LCicsPipeInterfaceContract.FN_GET_TOKEN_INF:// COB_CODE: PERFORM 6000-RETURN-TOKEN-INF
			//              THRU 6000-EXIT
			returnTokenInf();
			break;

		default:// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND  TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE L-CI-FUNCTION  TO EA-02-FUNCTION-VAL
			ws.getErrorAndAdviceMessages().getEa02FunctionInvalid().setEa02FunctionVal(lCicsPipeInterfaceContract.getFunction());
			// COB_CODE: MOVE EA-02-FUNCTION-INVALID
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa02FunctionInvalid().getEa02FunctionInvalidFormatted());
			break;
		}
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>*/
	private void beginningHousekeeping() {
		// COB_CODE: PERFORM 2100-CAPTURE-PASSED-INPUTS
		//              THRU 2100-EXIT.
		capturePassedInputs();
		//    WHEN CALLED FOR THE FIRST TIME, SET THE NULL POINTER TO NULLS
		//      AND CALCULATE THE MAX NUMBER OF OPEN PIPES ALLOWED.
		// COB_CODE: IF SW-IS-FIRST-TIME-CALLED
		//                                         / LENGTH OF TP-PIPE-INF
		//           END-IF.
		if (ws.getSwitches().isFirstTimeCalledFlag()) {
			// COB_CODE: SET SW-NOT-FIRST-TIME-CALLED
			//                                   TO TRUE
			ws.getSwitches().setFirstTimeCalledFlag(false);
			// COB_CODE: SET ADDRESS OF L-NULL-PTR
			//                                   TO NULLS
			lNullPtr = ((pointerManager.resolve(pointerManager.getNullPointer(), LNullPtr.class)));
			// COB_CODE: INITIALIZE TABLE-OF-PIPES
			initTableOfPipes();
			// COB_CODE: COMPUTE SA-MAX-PIPE-COUNT = LENGTH OF TABLE-OF-PIPES
			//                                     / LENGTH OF TP-PIPE-INF
			ws.getSaveArea()
					.setMaxPipeCount((new AfDecimal(((((double) Ts547099Data.Len.TABLE_OF_PIPES)) / TpPipeInf.Len.TP_PIPE_INF), 9, 0)).toInt());
		}
		//    INITIALIZE THE ERROR INFORMATION AND CAPTURE THE CICS APPL ID
		// COB_CODE: SET SW-NO-ERROR-FOUND       TO TRUE.
		ws.getSwitches().getErrorFoundFlag().setNoErrorFound();
		// COB_CODE: INITIALIZE L-CI-ERROR-INFORMATION.
		initErrorInformation();
		//    ENSURE A CICS APPL ID WAS PROVIDED
		// COB_CODE: MOVE L-CI-CICS-APPL-ID      TO SA-CICS-APPL-ID.
		ws.getSaveArea().setCicsApplId(lCicsPipeInterfaceContract.getCicsApplId());
		//    INITIALIZE THE ERROR INFORMATION AND CAPTURE THE CICS APPL ID
		// COB_CODE: IF SA-CICS-APPL-ID-IS-BLANK
		//               GO TO 2000-EXIT
		//           END-IF.
		if (ws.getSaveArea().isCicsApplIdIsBlank()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND
			//               EA-01-CICS-REGION-PARM
			//                                   TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			ws.getErrorAndAdviceMessages().getEa01ParmNotFound().getFlr5().setCicsRegionParm();
			// COB_CODE: MOVE EA-01-PARM-NOT-FOUND
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa01ParmNotFound().getEa01ParmNotFoundFormatted());
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//    CHECK TO SEE IF THE REGION REQUESTED BY THE CALLER IS ON RCD
		// COB_CODE: MOVE L-CI-CICS-APPL-ID      TO SA-CICS-APPL-ID.
		ws.getSaveArea().setCicsApplId(lCicsPipeInterfaceContract.getCicsApplId());
		// COB_CODE: PERFORM 9200-GET-PIPE-INF
		//              THRU 9200-EXIT.
		getPipeInf();
	}

	/**Original name: 2100-CAPTURE-PASSED-INPUTS<br>
	 * <pre>***************************************************************
	 *     THIS PARAGRAPH DETERMINES IF ONE OR TWO PARAMETERS WERE
	 *     PASSED TO THE SUBPROGRAM.  THE LAST PARAMETER RECEIVED IS
	 *     MARKED WITH A BINARY 1 IN THE FIRST BIT WHICH MAKES IT
	 *     NEGATIVE.  IF THE FIRST PARM ADDRESS IS NEGATIVE, THAT MEANS
	 *     WE ONLY HAVE ONE PARAMETER - NO DATA WAS PASSED FOR A CALL.
	 *     IF THE ADDRESS OF THE FIRST PARM IS NOT NEGATIVE, THERE ARE
	 *     TWO PARAMETERS - CAPTURE THE DATA TO PASS TO THE CICS PGM.
	 * ***************************************************************</pre>*/
	private void capturePassedInputs() {
		// COB_CODE: SET ADDRESS OF L-CICS-PIPE-INTERFACE-CONTRACT
		//                                       TO L-CICS-PIPE-CONTRACT-POINTER.
		lCicsPipeInterfaceContract = ((pointerManager.resolve(lCicsPipeContractAddress.getlCicsPipeContractPointer(),
				LCicsPipeInterfaceContract.class)));
		// COB_CODE: IF L-CICS-PIPE-CONTRACT-ADDRESS > +0
		//               SET SW-DATA-PARM-PASSED TO TRUE
		//           ELSE
		//                                       TO TRUE
		//           END-IF.
		if (lCicsPipeContractAddress.getlCicsPipeContractAddress() > 0) {
			// COB_CODE: SET ADDRESS OF L-CICS-PIPE-DATA-TO-PASS
			//                                   TO L-CICS-PIPE-DATA-POINTER
			lCicsPipeDataToPass = ((pointerManager.resolve(lCicsPipeDataAddress.getlCicsPipeDataPointer(),
					LCicsPipeDataToPass.class)));
			// COB_CODE: SET SW-DATA-PARM-PASSED TO TRUE
			ws.getSwitches().getDataParmPassedFlag().setPassed();
		} else {
			// COB_CODE: SET ADDRESS OF L-CICS-PIPE-DATA-TO-PASS
			//                                   TO NULLS
			lCicsPipeDataToPass = ((pointerManager.resolve(pointerManager.getNullPointer(), LCicsPipeDataToPass.class)));
			// COB_CODE: SET SW-DATA-PARM-NOT-PASSED
			//                                   TO TRUE
			ws.getSwitches().getDataParmPassedFlag().setNotPassed();
		}
	}

	/**Original name: 3000-OPEN-PIPE<br>
	 * <pre>***************************************************************
	 *   ENSURES A CICS PIPE IS OPEN TO THE REQUESTED REGION.  CONSIDER
	 *   THE REGION REQUESTED MAY ALREADY HAVE AN OPEN PIPE.
	 * ***************************************************************
	 *     THE REQUESTED PIPE IS ALREADY OPEN.  JUST UPDATE THE
	 *       OPEN/CLOSE TALLY AND EXIT.  IF THE PIPE REQUESTED BY THE
	 *       USER IS BEING REDIRECTED, UPDATE THE REDIRECTION TALLY TOO.</pre>*/
	private void openPipe() {
		// COB_CODE: IF SW-REGION-INF-FND
		//             AND
		//              NOT TP-PI-PIPE-IS-CLOSED (SS-TP)
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getSwitches().getRegionInfFndFlag().isFnd() && !ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally().isPipeIsClosed()) {
			// COB_CODE: ADD +1                  TO TP-PI-OPEN-CLOSE-TALLY (SS-TP)
			ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally()
					.setOpenCloseTally(Trunc.toShort(1 + ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally().getOpenCloseTally(), 4));
			// COB_CODE: IF TP-PI-RI-PIPE-IS-REDIRECTED (SS-TP-ORIG)
			//                                                        (SS-TP-ORIG)
			//           END-IF
			if (ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().isIsRedirected()) {
				// COB_CODE: ADD +1              TO TP-PI-RI-NBR-REDIRECTS
				//                                                    (SS-TP-ORIG)
				ws.getTpPipeInf(ws.getSubscripts().getTpOrig())
						.setRiNbrRedirects(Trunc.toShort(1 + ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects(), 4));
			}
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//    THE REQUESTED REGION ISN'T FOUND.  CREATE NEW PIPE INFO AND
		//      SET IT UP AS THE REGION ORIGINALLY REQUESTED.
		// COB_CODE: IF SW-REGION-INF-NOT-FND
		//               MOVE SS-TP              TO SS-TP-ORIG
		//           END-IF.
		if (ws.getSwitches().getRegionInfFndFlag().isNotFnd()) {
			// COB_CODE: PERFORM 9220-FIND-AVAIL-POS-IN-TBL
			//              THRU 9220-EXIT
			findAvailPosInTbl();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 3000-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 3000-EXIT
				return;
			}
			// COB_CODE: MOVE L-CI-CICS-APPL-ID  TO TP-PI-CICS-APPL-ID (SS-TP)
			ws.getTpPipeInf(ws.getSubscripts().getTp()).setCicsApplId(lCicsPipeInterfaceContract.getCicsApplId());
			// COB_CODE: MOVE SS-TP              TO SS-TP-ORIG
			ws.getSubscripts().setTpOrig(ws.getSubscripts().getTp());
		}
		//    CALL THE CICS PIPE MAINTENANCE ROUTINE TO HANDLE THE
		//      OPENING OF THE ORIGINALLY REQUESTED PIPE.
		// COB_CODE: PERFORM 9310-OPEN-PIPE-TO-ORIG-REGION
		//              THRU 9310-EXIT.
		openPipeToOrigRegion();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//    IF SUCCESSFUL, RECORD THE NEWLY CREATED PIPE INFORMATION IN
		//      THE TABLE OF OPEN PIPES.
		// COB_CODE: IF CM-EI-ET-REGION-SWAPPED
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getCicsPipeMaintenanceCnt().getErrorInformation().getErrorType().isCmEiEtRegionSwapped()) {
			// COB_CODE: PERFORM 9400-HANDLE-PIPE-REDIRECTION
			//              THRU 9400-EXIT
			handlePipeRedirection();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: ADD +1                      TO AA-OPEN-PIPES.
		ws.setAaOpenPipes(Trunc.toShort(1 + ws.getAaOpenPipes(), 4));
		// COB_CODE: MOVE CM-PI-PIPE-TOKEN       TO TP-PI-PIPE-TOKEN (SS-TP).
		ws.getTpPipeInf(ws.getSubscripts().getTp()).setPipeToken(ws.getCicsPipeMaintenanceCnt().getPiPipeToken());
		// COB_CODE: MOVE CM-PI-USER-TOKEN       TO TP-PI-USER-TOKEN (SS-TP).
		ws.getTpPipeInf(ws.getSubscripts().getTp()).setUserToken(ws.getCicsPipeMaintenanceCnt().getPiUserToken());
		// COB_CODE: MOVE +1                     TO TP-PI-OPEN-CLOSE-TALLY
		//                                                                (SS-TP).
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally().setOpenCloseTally(((short) 1));
		// COB_CODE: SET TP-PI-PIPE-STA-OK (SS-TP)
		//                                       TO TRUE.
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().setOk();
		// COB_CODE: INITIALIZE TP-PI-REDIRECTION-INF (SS-TP).
		initRedirectionInf();
	}

	/**Original name: 4000-CALL-CICS-USING-PIPE<br>
	 * <pre>***************************************************************
	 *   PERFORM THE LOGIC OF CALLING A CICS PROGRAM USING THE AVAILABLE
	 *   CICS PIPE.  MAKE SURE PIPE IS OPEN AND INPUT IS PROVIDED/VALID.
	 * ***************************************************************</pre>*/
	private String callCicsUsingPipe() {
		// COB_CODE: IF SW-REGION-INF-NOT-FND
		//               GO TO 4000-EXIT
		//           END-IF.
		if (ws.getSwitches().getRegionInfFndFlag().isNotFnd()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE L-CI-CICS-APPL-ID  TO EA-11-CICS-APPL-ID
			ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().setEa11CicsApplId(lCicsPipeInterfaceContract.getCicsApplId());
			// COB_CODE: MOVE EA-11-REGION-NOT-OPEN
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().getEa11RegionNotOpenFormatted());
			// COB_CODE: GO TO 4000-EXIT
			return "4000-EXIT";
		}
		// COB_CODE: IF TP-PI-PIPE-IS-CLOSED (SS-TP)
		//               GO TO 4000-EXIT
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally().isPipeIsClosed()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP)
			//                                   TO EA-11-CICS-APPL-ID
			ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().setEa11CicsApplId(ws.getTpPipeInf(ws.getSubscripts().getTp()).getCicsApplId());
			// COB_CODE: MOVE EA-11-REGION-NOT-OPEN
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().getEa11RegionNotOpenFormatted());
			// COB_CODE: GO TO 4000-EXIT
			return "4000-EXIT";
		}
		// COB_CODE: PERFORM 4100-EDIT-CICS-CALLING-PARMS
		//              THRU 4100-EXIT
		editCicsCallingParms();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 4000-EXIT
		//           END-IF.
		if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 4000-EXIT
			return "4000-EXIT";
		}
		// COB_CODE: MOVE L-CI-CP-DATA-LEN       TO SA-COMM-LENGTH
		//                                          SA-DATA-LENGTH.
		ws.getSaveArea().setCommLength(lCicsPipeInterfaceContract.getCpDataLen());
		ws.getSaveArea().setDataLength(lCicsPipeInterfaceContract.getCpDataLen());
		return "";
	}

	/**Original name: 4000-A<br>*/
	private String a() {
		// COB_CODE: SET SW-DO-NOT-RETRY-CICS-CALL
		//                                       TO TRUE.
		ws.getSwitches().getRetryCicsCallFlag().setDoNotRetryCicsCall();
		// COB_CODE: PERFORM 4200-PERFORM-CICS-CALL
		//              THRU 4200-EXIT.
		performCicsCall();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 4000-EXIT
		//           END-IF.
		if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 4000-EXIT
			return "";
		}
		//    IF NEEDED, LOOP BACK AND RETRY THE PIPE CONNECTION
		// COB_CODE: IF SW-RETRY-CICS-CALL
		//               GO TO 4000-A
		//           END-IF.
		if (ws.getSwitches().getRetryCicsCallFlag().isRetryCicsCall()) {
			// COB_CODE: GO TO 4000-A
			return "4000-A";
		}
		//    SINCE VALUES CAN BE FOUND HERE EVEN WHEN NO ERRORS ARE FOUND,
		//      ALWAYS GIVE BACK THE CODES WE RECEIVED.
		// COB_CODE: MOVE EXCI-RESPONSE          TO L-CI-EI-EXCI-RESP.
		lCicsPipeInterfaceContract.setEiExciResp(((int) (ws.getDfhxcplo().getExciReturnCode().getResponse())));
		// COB_CODE: MOVE EXCI-REASON            TO L-CI-EI-EXCI-RESP2.
		lCicsPipeInterfaceContract.setEiExciResp2(((int) (ws.getDfhxcplo().getExciReturnCode().getReason())));
		// COB_CODE: MOVE EXCI-SUB-REASON1       TO L-CI-EI-EXCI-RESP3.
		lCicsPipeInterfaceContract.setEiExciResp3(((int) (ws.getDfhxcplo().getExciReturnCode().getSubReason1())));
		// COB_CODE: MOVE EXCI-DPL-RESP          TO L-CI-EI-DPL-RESP.
		lCicsPipeInterfaceContract.setEiDplResp(((int) (ws.getDfhxcplo().getExciDplRetarea().getResp())));
		// COB_CODE: MOVE EXCI-DPL-RESP2         TO L-CI-EI-DPL-RESP2.
		lCicsPipeInterfaceContract.setEiDplResp2(((int) (ws.getDfhxcplo().getExciDplRetarea().getResp2())));
		return "";
	}

	/**Original name: 4100-EDIT-CICS-CALLING-PARMS<br>
	 * <pre>***************************************************************
	 *   VALIDATE USER PROVIDED INFORMATION FOR CALLING THE CICS PROGRAM
	 * ***************************************************************</pre>*/
	private void editCicsCallingParms() {
		// COB_CODE: MOVE L-CI-CP-CICS-PGM-NM    TO SA-CICS-PGM-TO-CALL.
		ws.getSaveArea().setCicsPgmToCall(lCicsPipeInterfaceContract.getCpCicsPgmNm());
		// COB_CODE: MOVE L-CI-CP-TARGET-TRAN-ID TO SA-TARGET-TRAN-ID.
		ws.getSaveArea().setTargetTranId(lCicsPipeInterfaceContract.getCpTargetTranId());
		// COB_CODE: MOVE L-CI-CP-DATA-LEN       TO SA-INPUT-DATA-LEN.
		ws.getSaveArea().setInputDataLen(lCicsPipeInterfaceContract.getCpDataLen());
		// COB_CODE: IF SA-CICS-PGM-NM-IS-BLANK
		//               GO TO 4100-EXIT
		//           END-IF.
		if (ws.getSaveArea().isCicsPgmNmIsBlank()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND
			//               EA-01-CICS-PROGRAM-NM-PARM
			//                                   TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			ws.getErrorAndAdviceMessages().getEa01ParmNotFound().getFlr5().setCicsProgramNmParm();
			// COB_CODE: MOVE EA-01-PARM-NOT-FOUND
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa01ParmNotFound().getEa01ParmNotFoundFormatted());
			// COB_CODE: GO TO 4100-EXIT
			return;
		}
		// COB_CODE: IF SA-TARGET-TRAN-ID-IS-BLANK
		//               GO TO 4100-EXIT
		//           END-IF.
		if (ws.getSaveArea().isTargetTranIdIsBlank()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND
			//               EA-01-TRAN-ID-PARM  TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			ws.getErrorAndAdviceMessages().getEa01ParmNotFound().getFlr5().setTranIdParm();
			// COB_CODE: MOVE EA-01-PARM-NOT-FOUND
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa01ParmNotFound().getEa01ParmNotFoundFormatted());
			// COB_CODE: GO TO 4100-EXIT
			return;
		}
		//    ENSURE THE LENGTH OF THE DATA TO BE PASSED IS BETWEEN THE SET
		//      MINIMUM AND MAXIMUM VALUES.  NOTE:  SINCE THE INPUT FIELD
		//      IS BINARY, THERE IS NO NEED TO DETERMINE IF IT IS NUMERIC.
		// COB_CODE: IF SA-INPUT-DATA-LEN < CF-MIN-DATA-LEN
		//             OR
		//              SA-INPUT-DATA-LEN > CF-MAX-DATA-LEN
		//               GO TO 4100-EXIT
		//           END-IF.
		if (ws.getSaveArea().getInputDataLen() < ws.getConstantFields().getMinDataLen()
				|| ws.getSaveArea().getInputDataLen() > ws.getConstantFields().getMaxDataLen()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE L-CI-CP-DATA-LEN   TO EA-04-BAD-DATA-LEN
			ws.getErrorAndAdviceMessages().getEa04DataLengthInvalid().setBadDataLen(lCicsPipeInterfaceContract.getCpDataLen());
			// COB_CODE: MOVE CF-MIN-DATA-LEN    TO EA-04-MIN-DATA-LEN
			ws.getErrorAndAdviceMessages().getEa04DataLengthInvalid().setMinDataLen(ws.getConstantFields().getMinDataLen());
			// COB_CODE: MOVE CF-MAX-DATA-LEN    TO EA-04-MAX-DATA-LEN
			ws.getErrorAndAdviceMessages().getEa04DataLengthInvalid().setMaxDataLen(ws.getConstantFields().getMaxDataLen());
			// COB_CODE: MOVE EA-04-DATA-LENGTH-INVALID
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa04DataLengthInvalid().getEa04DataLengthInvalidFormatted());
			// COB_CODE: GO TO 4100-EXIT
			return;
		}
		//    ENSURE THE CALLER PROVIDED DATA TO PASS TO THE CICS PROGRAM
		// COB_CODE: IF SW-DATA-PARM-NOT-PASSED
		//               GO TO 4100-EXIT
		//           END-IF.
		if (ws.getSwitches().getDataParmPassedFlag().isNotPassed()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE EA-05-NO-DATA-TO-PASS
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa05NoDataToPass().getEa05NoDataToPassFormatted());
			// COB_CODE: GO TO 4100-EXIT
			return;
		}
	}

	/**Original name: 4200-PERFORM-CICS-CALL<br>
	 * <pre>***************************************************************
	 *  MAKE THE CALL TO THE CICS REGION AND HANDLE ANY PIPE MANAGEMENT
	 *  AS NEEDED.
	 * ***************************************************************
	 *     ITS POSSIBLE THE PIPE IS STILL OPEN, BUT IN AN UNUSABLE
	 *       STATE.  IF SO, DO NOT TRY TO MAKE A CALL USING THE PIPE
	 *       AND INSTEAD FALL INTO THE REDIRECTION LOGIC.</pre>*/
	private void performCicsCall() {
		// COB_CODE: IF TP-PI-PIPE-STA-OK (SS-TP)
		//                  THRU 4210-EXIT
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().isOk()) {
			// COB_CODE: PERFORM 4210-MAKE-DPL-REQUEST
			//              THRU 4210-EXIT
			makeDplRequest();
		}
		//    SPECIAL ERROR HANDLING LOGIC IS NEEDED FOR RETRY ATTEMPTS.
		// COB_CODE: EVALUATE TRUE
		//               WHEN TP-PI-PIPE-STA-UNUSABLE (SS-TP)
		//               WHEN EXCI-RESPONSE = EXCI-RETRYABLE
		//               WHEN EXCI-DPL-RESP = EXEC-SYSIDERR
		//                   END-IF
		//               WHEN EXCI-RESPONSE = EXCI-NORMAL
		//                   AND
		//                    EXCI-DPL-RESP = EXEC-NORMAL
		//                   CONTINUE
		//               WHEN OTHER
		//                   END-IF
		//           END-EVALUATE.
		if (ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().isUnusable()
				|| (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciRetryable())
				|| (ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecSysiderr())) {
			// COB_CODE: PERFORM 4220-CALL-PIPE-ERR-HANDLING
			//              THRU 4220-EXIT
			callPipeErrHandling();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 4200-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 4200-EXIT
				return;
			}
		} else if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciNormal()
				&& ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecNormal()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: PERFORM 9000-CICS-ERROR
			//              THRU 9000-EXIT
			cicsError();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 4200-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 4200-EXIT
				return;
			}
		}
	}

	/**Original name: 4210-MAKE-DPL-REQUEST<br>
	 * <pre>***************************************************************
	 *   PERFORM THE APPROPRIATE DPL REQUEST.  ERROR CHECKING WILL BE
	 *   DONE IN ANOTHER PARAGRAPH.  BELOW IS AN EXPLANATION OF THE
	 *   CALL TO THE CICS PROGRAMS (DPL REQUEST)
	 *     CALL 'DFHXCIS'
	 *         USING
	 *             VERSION-1 - CONSTANT IN COPYBOOK DFHXCPLO
	 *             EXCI-RETURN-CODE - DEFINED IN COPYBOOK DFHXCPLO
	 *             USER-TOKEN - CALL FILLS THIS IN
	 *             DPL-REQUEST - CONSTANT IN COPYBOOK DFHXCPLO
	 *             PIPE-TOKEN - CALL FILLS THIS IN
	 *             TARGET-PROGRAM - CICS PROGRAM TO CALL (V*)
	 *             COMMAREA - THE COPYBOOK FOR THE PGM BEING CALLED
	 *             COMM-LENGTH - LENGTH OF THE COPYBOOK FOR PGM CALLED
	 *             DATA-LENGTH - LENGTH OF THE COPYBOOK FOR PGM CALLED
	 *             TARGET-TRANSID - CONSTANT FIELD
	 *             NULL-PTR - NEEDED FOR CALL
	 *             NULL-PTR - NEEDED FOR CALL
	 *             EXCI-DPL-RETAREA - DEFINED IN COPYBOOK DFHXCPLO
	 *             SYNCONRETURN. - !WE WILL NOT ALWAYS WANT TO SYNC!
	 * ***************************************************************
	 *     USE THE IBM-PROVIDED PARAMETER TO FILL IN THE SYNC-ON-RETURN
	 *       INDICATOR FIELD.  THEN, USE THE FIELD IN PART OF THE CALL
	 *       TO ISSUE A DPL REQUEST.</pre>*/
	private void makeDplRequest() {
		GenericParam version1 = null;
		GenericParam tpPiUserToken = null;
		GenericParam dplRequest = null;
		GenericParam tpPiPipeToken = null;
		GenericParam saCommLength = null;
		GenericParam saDataLength = null;
		GenericParam saSynconreturnInd = null;
		// COB_CODE: IF L-CI-FN-CALL-CICS-PGM-SYNC
		//               MOVE SYNCONRETURN       TO SA-SYNCONRETURN-IND
		//           ELSE
		//               MOVE NOSYNCONRETURN     TO SA-SYNCONRETURN-IND
		//           END-IF.
		if (lCicsPipeInterfaceContract.isFnCallCicsPgmSync()) {
			// COB_CODE: MOVE SYNCONRETURN       TO SA-SYNCONRETURN-IND
			ws.getSaveArea().setSynconreturnInd(ws.getDfhxcplo().getSynconreturn());
		} else {
			// COB_CODE: MOVE NOSYNCONRETURN     TO SA-SYNCONRETURN-IND
			ws.getSaveArea().setSynconreturnInd(ws.getDfhxcplo().getNosynconreturn());
		}
		// COB_CODE: CALL 'DFHXCIS' USING VERSION-1
		//                                EXCI-RETURN-CODE
		//                                TP-PI-USER-TOKEN (SS-TP)
		//                                DPL-REQUEST
		//                                TP-PI-PIPE-TOKEN (SS-TP)
		//                                L-CI-CP-CICS-PGM-NM
		//                                L-CICS-PIPE-DATA-TO-PASS
		//                                SA-COMM-LENGTH
		//                                SA-DATA-LENGTH
		//                                L-CI-CP-TARGET-TRAN-ID
		//                                L-NULL-PTR
		//                                L-NULL-PTR
		//                                EXCI-DPL-RETAREA
		//                                SA-SYNCONRETURN-IND.
		version1 = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getVersion1()));
		tpPiUserToken = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getTpPipeInf(ws.getSubscripts().getTp()).getUserToken()));
		dplRequest = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getDplRequest()));
		tpPiPipeToken = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeToken()));
		saCommLength = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getSaveArea().getCommLength()));
		saDataLength = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getSaveArea().getDataLength()));
		saSynconreturnInd = new GenericParam(MarshalByteExt.chToBuffer(ws.getSaveArea().getSynconreturnInd()));
		DynamicCall.invoke("DFHXCIS",
				new Object[] { version1, ws.getDfhxcplo().getExciReturnCode(), tpPiUserToken, dplRequest, tpPiPipeToken,
						new BasicBytesClass(lCicsPipeInterfaceContract.getArray(), LCicsPipeInterfaceContract.Pos.CP_CICS_PGM_NM - 1),
						lCicsPipeDataToPass, saCommLength, saDataLength,
						new BasicBytesClass(lCicsPipeInterfaceContract.getArray(), LCicsPipeInterfaceContract.Pos.CP_TARGET_TRAN_ID - 1), lNullPtr,
						lNullPtr, ws.getDfhxcplo().getExciDplRetarea(), saSynconreturnInd });
		ws.getDfhxcplo().setVersion1FromBuffer(version1.getByteData());
		ws.getTpPipeInf(ws.getSubscripts().getTp()).setUserTokenFromBuffer(tpPiUserToken.getByteData());
		ws.getDfhxcplo().setDplRequestFromBuffer(dplRequest.getByteData());
		ws.getTpPipeInf(ws.getSubscripts().getTp()).setPipeTokenFromBuffer(tpPiPipeToken.getByteData());
		ws.getSaveArea().setCommLengthFromBuffer(saCommLength.getByteData());
		ws.getSaveArea().setDataLengthFromBuffer(saDataLength.getByteData());
		ws.getSaveArea().setSynconreturnIndFromBuffer(saSynconreturnInd.getByteData());
	}

	/**Original name: 4220-CALL-PIPE-ERR-HANDLING<br>
	 * <pre>******************************************************
	 *   PERFORM SPECIAL ERROR HANDLING LOGIC FOR PIPE CONNECTION
	 * ******************************************************
	 *     MARK THE CURRENT PIPE AS 'UNUSABLE'</pre>*/
	private void callPipeErrHandling() {
		// COB_CODE: SET TP-PI-PIPE-STA-UNUSABLE (SS-TP)
		//                                       TO TRUE
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().setUnusable();
		//    CALL THE CICS PIPE MAINTENANCE ROUTINE TO RETRY
		//        OPENING THE PIPE.
		// COB_CODE: PERFORM 9310-OPEN-PIPE-TO-ORIG-REGION
		//              THRU 9310-EXIT.
		openPipeToOrigRegion();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 4220-EXIT
		//           END-IF.
		if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 4220-EXIT
			return;
		}
		//    WHEN THE NEWLY ESTABLISHED PIPE IS TO THE SAME REGION WE
		//      ARE CURRENTLY TARGETING, THEN SIMPLY UPDATE THE TOKEN
		//      FIELDS AND RETRY THE CALL USING THE UPDATED PIPE.  THIS
		//      APPLIES REGARDLESS IF WE WERE REDIRECTING OR NOT.
		// COB_CODE: IF CM-CICS-APPL-ID = TP-PI-CICS-APPL-ID (SS-TP)
		//               GO TO 4220-EXIT
		//           END-IF.
		if (Conditions.eq(ws.getCicsPipeMaintenanceCnt().getCicsApplId().getCicsApplId(),
				ws.getTpPipeInf(ws.getSubscripts().getTp()).getCicsApplId())) {
			// COB_CODE: PERFORM 4221-HANDLE-UPDATE-CUR-PIPE
			//              THRU 4221-EXIT
			handleUpdateCurPipe();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 4220-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 4220-EXIT
				return;
			}
			// COB_CODE: SET SW-RETRY-CICS-CALL  TO TRUE
			ws.getSwitches().getRetryCicsCallFlag().setRetryCicsCall();
			// COB_CODE: GO TO 4220-EXIT
			return;
		}
		//    WHEN WE WERE REDIRECTING BUT HAVE REESTABLISHED A PIPE TO THE
		//      ORIGINAL REGION, CLOSE THE CURRENT PIPE AND NOW REDIRECT
		//      CALLS BACK TO THE ORIGINAL REGION.
		// COB_CODE: IF TP-PI-RI-PIPE-IS-REDIRECTED (SS-TP-ORIG)
		//             AND
		//              CM-CICS-APPL-ID = TP-PI-CICS-APPL-ID (SS-TP-ORIG)
		//               GO TO 4220-EXIT
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().isIsRedirected() && Conditions.eq(
				ws.getCicsPipeMaintenanceCnt().getCicsApplId().getCicsApplId(), ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getCicsApplId())) {
			// COB_CODE: PERFORM 4222-CAPTURE-ORIG-REGION-CALL
			//              THRU 4222-EXIT
			captureOrigRegionCall();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 4220-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 4220-EXIT
				return;
			}
			// COB_CODE: SET SW-RETRY-CICS-CALL  TO TRUE
			ws.getSwitches().getRetryCicsCallFlag().setRetryCicsCall();
			// COB_CODE: GO TO 4220-EXIT
			return;
		}
		//    WE ARE NOW BEING REDIRECTED FROM ONE PIPE TO ANOTHER.
		//      HANDLE THE NECESSARY REDIRECTION LOGIC.  MAKE SURE TO
		//      CONSIDER IF WE WERE REDIRECTED ALREADY.
		// COB_CODE: IF TP-PI-RI-PIPE-IS-REDIRECTED (SS-TP-ORIG)
		//               END-IF
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().isIsRedirected()) {
			// COB_CODE: PERFORM 4223-HANDLE-REDIR-CHANGE
			//              THRU 4223-EXIT
			handleRedirChange();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 4220-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 4220-EXIT
				return;
			}
		}
		// COB_CODE: PERFORM 9400-HANDLE-PIPE-REDIRECTION
		//              THRU 9400-EXIT.
		handlePipeRedirection();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 4220-EXIT
		//           END-IF.
		if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 4220-EXIT
			return;
		}
		// COB_CODE: SET SW-RETRY-CICS-CALL      TO TRUE.
		ws.getSwitches().getRetryCicsCallFlag().setRetryCicsCall();
	}

	/**Original name: 4221-HANDLE-UPDATE-CUR-PIPE<br>
	 * <pre>***************************************************************
	 *     WE HAVE RE-ESTABLISHED A CONNECTION THE CICS REGION WE ARE
	 *     CURRENTLY WORKING WITH.  MAKE SURE THE CORRECT PIPE
	 *     INFORMATION IS BEING USED.
	 * ***************************************************************
	 *     REGARDLESS OF WHETHER OR NOT THE TOKENS RETURNED ARE NEW OR
	 *       THE SAME, SET THIS PIPE AS USABLE AGAIN.</pre>*/
	private void handleUpdateCurPipe() {
		// COB_CODE: SET TP-PI-PIPE-STA-OK (SS-TP)
		//                                       TO TRUE.
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().setOk();
		//    WHEN A CICS REGION IS SHUTTING DOWN, IT WILL NO LONGER ALLOW
		//      DPL REQUESTS TO BE PROCESSED.  HOWEVER, IT WILL STILL ALLOW
		//      FOR NEW PIPE CONNECTIONS TO BE MADE.  BECAUSE OF THIS, WE
		//      WILL NOT CLOSE THE EXISTING PIPE WHEN A NEW PIPE HAS BEEN
		//      CAPTURED.  THE RESULT WILL BE THE CICS SHUTTING DOWN WILL
		//      EVENTUALLY RUN OUT OF FREE SESSIONS (EXCI2 = +202).  WHEN
		//      THIS HAPPENS, OPENING A PIPE TO THE REGION WILL FAIL AND WE
		//      WILL INSTEAD GO AFTER A NEW REGION (IF AVAILABLE).  IF WE
		//      WERE TO CLOSE THE PREVIOUS PIPE, THE BATCH JOB WOULD SIMPLY
		//      SIT AND GO BACK AND FORTH BETWEEN TWO PIPES UNTIL THE
		//      REGION FINALLY WENT DOWN COMPLETELY.  THIS UNWANTED RESULT
		//      WOULD CAUSE HIGH CPU CONSUMPTION AND A DELAY OF THE JOB FOR
		//      SEVERAL MINUTES IN PRODUCTION.
		// COB_CODE: MOVE CM-PI-PIPE-TOKEN       TO TP-PI-PIPE-TOKEN (SS-TP).
		ws.getTpPipeInf(ws.getSubscripts().getTp()).setPipeToken(ws.getCicsPipeMaintenanceCnt().getPiPipeToken());
		// COB_CODE: MOVE CM-PI-USER-TOKEN       TO TP-PI-USER-TOKEN (SS-TP).
		ws.getTpPipeInf(ws.getSubscripts().getTp()).setUserToken(ws.getCicsPipeMaintenanceCnt().getPiUserToken());
	}

	/**Original name: 4222-CAPTURE-ORIG-REGION-CALL<br>
	 * <pre>***************************************************************
	 *   THE REGION HAS BEEN SWITCHED BACK TO THE REGION ORIGINALLY
	 *   REQUESTED.  UPDATE THE CALLER AND OUR TABLE INFORMATION TO
	 *   REFLECT THE NEW CHANGES.
	 * ***************************************************************
	 *     THE ORIGINAL PIPE INFORMATION CURRENTLY INDICATES THE PIPE IS
	 *       CLOSED AND BEING REDIRECTED.  UPDATE THE ORIGINAL PIPE.</pre>*/
	private void captureOrigRegionCall() {
		// COB_CODE: MOVE CM-PI-PIPE-TOKEN       TO TP-PI-PIPE-TOKEN (SS-TP-ORIG).
		ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).setPipeToken(ws.getCicsPipeMaintenanceCnt().getPiPipeToken());
		// COB_CODE: MOVE CM-PI-USER-TOKEN       TO TP-PI-USER-TOKEN (SS-TP-ORIG).
		ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).setUserToken(ws.getCicsPipeMaintenanceCnt().getPiUserToken());
		// COB_CODE: MOVE TP-PI-RI-NBR-REDIRECTS (SS-TP-ORIG)
		//                                       TO TP-PI-OPEN-CLOSE-TALLY
		//                                                           (SS-TP-ORIG).
		ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getOpenCloseTally()
				.setOpenCloseTally(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects());
		// COB_CODE: ADD +1                      TO AA-OPEN-PIPES.
		ws.setAaOpenPipes(Trunc.toShort(1 + ws.getAaOpenPipes(), 4));
		//    BEFORE CLEARING OUT THE REDIRECTION INFORMATION, UPDATE THE
		//      PIPE WE WERE REDIRECTING TO.  IF NECESSARY, CLOSE IT.
		// COB_CODE: SUBTRACT TP-PI-RI-NBR-REDIRECTS (SS-TP-ORIG)
		//                                       FROM TP-PI-OPEN-CLOSE-TALLY
		//                                                          (SS-TP-REDIR).
		ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally()
				.setOpenCloseTally(Trunc.toShort(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().getOpenCloseTally()
						- ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects(), 4));
		// COB_CODE: IF TP-PI-KEEP-PIPE-OPEN (SS-TP-REDIR)
		//               CONTINUE
		//           ELSE
		//               SUBTRACT +1             FROM AA-OPEN-PIPES
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().isKeepPipeOpen()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET CM-FN-CLOSE-PIPE    TO TRUE
			ws.getCicsPipeMaintenanceCnt().getFunction().setCmFnClosePipe();
			// COB_CODE: MOVE TP-PI-PIPE-TOKEN (SS-TP-REDIR)
			//                                   TO CM-PI-PIPE-TOKEN
			ws.getCicsPipeMaintenanceCnt().setPiPipeToken(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getPipeToken());
			// COB_CODE: MOVE TP-PI-USER-TOKEN (SS-TP-REDIR)
			//                                   TO CM-PI-USER-TOKEN
			ws.getCicsPipeMaintenanceCnt().setPiUserToken(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getUserToken());
			// COB_CODE: PERFORM 9300-CALL-PIPE-MTN-COMMON-RTN
			//              THRU 9300-EXIT
			callPipeMtnCommonRtn();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 4222-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 4222-EXIT
				return;
			}
			// COB_CODE: MOVE +0                 TO TP-PI-USER-TOKEN (SS-TP-REDIR)
			//                                      TP-PI-PIPE-TOKEN (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).setUserToken(0);
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).setPipeToken(0);
			// COB_CODE: SET TP-PI-PIPE-IS-CLOSED (SS-TP-REDIR)
			//               TP-PI-PIPE-STA-OK (SS-TP-REDIR)
			//                                   TO TRUE
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().setPipeIsClosed();
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getPipeStaCd().setOk();
			//        THIS PIPE CANNOT BE SET UP TO REDIRECT TO THE NEXT PIPE
			//          FOR MANY REASONS.  IF AN OPEN CALL IS MADE TO THIS
			//          REGION, WE WILL THEN SET IT UP FOR REDIRECTION
			// COB_CODE: INITIALIZE TP-PI-REDIRECTION-INF (SS-TP-REDIR)
			initRedirectionInf();
			// COB_CODE: SUBTRACT +1             FROM AA-OPEN-PIPES
			ws.setAaOpenPipes(Trunc.toShort(ws.getAaOpenPipes() - 1, 4));
		}
		//    CLEAR OUT ANY REDIRECTION INFORMATION FOR THE ORIGINAL PIPE.
		// COB_CODE: INITIALIZE TP-PI-REDIRECTION-INF (SS-TP-ORIG).
		initRedirectionInf();
		//    GET A WARNING BACK TO THE CALLER FOR THE REGION SWAP.
		// COB_CODE: SET L-CI-EI-ES-WARNING
		//               L-CI-EI-ET-REGION-SWAPPED
		//                                       TO TRUE.
		lCicsPipeInterfaceContract.setEiEsWarning();
		lCicsPipeInterfaceContract.setEiEtRegionSwapped();
		// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP-REDIR)
		//                                       TO EA-14-ORIG-REGION.
		ws.getErrorAndAdviceMessages().getEa14RegionSwapped().setOrigRegion(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getCicsApplId());
		// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP-ORIG)
		//                                       TO EA-14-REDIR-REGION.
		ws.getErrorAndAdviceMessages().getEa14RegionSwapped().setRedirRegion(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getCicsApplId());
		// COB_CODE: MOVE EA-14-REGION-SWAPPED   TO L-CI-EI-MESSAGE.
		lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa14RegionSwapped().getEa14RegionSwappedFormatted());
		//    SET THE ORIGINAL REGION TO BE THE REGION TO BE USED.
		// COB_CODE: MOVE SS-TP-ORIG             TO SS-TP.
		ws.getSubscripts().setTp(ws.getSubscripts().getTpOrig());
	}

	/**Original name: 4223-HANDLE-REDIR-CHANGE<br>
	 * <pre>***************************************************************
	 *   WE ARE GOING TO BE CHANGING WHICH REGIONS WE ARE REDIRECTING
	 *   TO.  ENSURE THE CURRENT REDIRECTION REGION IS PROPERLY HANDLED
	 *   BEFORE SWITCHING OVER.
	 * ***************************************************************</pre>*/
	private void handleRedirChange() {
		// COB_CODE: SUBTRACT TP-PI-RI-NBR-REDIRECTS (SS-TP-ORIG)
		//                                       FROM TP-PI-OPEN-CLOSE-TALLY
		//                                                          (SS-TP-REDIR).
		ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally()
				.setOpenCloseTally(Trunc.toShort(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().getOpenCloseTally()
						- ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects(), 4));
		// COB_CODE:      IF TP-PI-KEEP-PIPE-OPEN (SS-TP-REDIR)
		//                    CONTINUE
		//                ELSE
		//           *        WE WILL NOW NEED TO CLOSE THE OLD PIPE.  HOWEVER, WE
		//           *          STILL HAVE NOT YET SAVED THE INFORMATION OF THE NEW
		//           *          PIPE WHICH WAS RETURNED FROM THE LAST CALL TO THE CICS
		//           *          PIPE MAINTAINENCE PROGRAM.  SAVE THE INFORMATION AND
		//           *          RESTORE IT AFTER CLOSING THE UNNECESSARY PIPE.
		//                                            TO CICS-PIPE-MAINTENANCE-CNT
		//                END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().isKeepPipeOpen()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			//        WE WILL NOW NEED TO CLOSE THE OLD PIPE.  HOWEVER, WE
			//          STILL HAVE NOT YET SAVED THE INFORMATION OF THE NEW
			//          PIPE WHICH WAS RETURNED FROM THE LAST CALL TO THE CICS
			//          PIPE MAINTAINENCE PROGRAM.  SAVE THE INFORMATION AND
			//          RESTORE IT AFTER CLOSING THE UNNECESSARY PIPE.
			// COB_CODE: MOVE CICS-PIPE-MAINTENANCE-CNT
			//                                   TO SA-CICS-PIPE-MAINTENANCE-CNT
			ws.getSaveArea().setCicsPipeMaintenanceCnt(ws.getCicsPipeMaintenanceCnt().getCicsPipeMaintenanceCntFormatted());
			// COB_CODE: SET CM-FN-CLOSE-PIPE    TO TRUE
			ws.getCicsPipeMaintenanceCnt().getFunction().setCmFnClosePipe();
			// COB_CODE: MOVE TP-PI-PIPE-TOKEN (SS-TP-REDIR)
			//                                   TO CM-PI-PIPE-TOKEN
			ws.getCicsPipeMaintenanceCnt().setPiPipeToken(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getPipeToken());
			// COB_CODE: MOVE TP-PI-USER-TOKEN (SS-TP-REDIR)
			//                                   TO CM-PI-USER-TOKEN
			ws.getCicsPipeMaintenanceCnt().setPiUserToken(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getUserToken());
			// COB_CODE: PERFORM 9300-CALL-PIPE-MTN-COMMON-RTN
			//              THRU 9300-EXIT
			callPipeMtnCommonRtn();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 4223-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 4223-EXIT
				return;
			}
			// COB_CODE: MOVE +0                 TO TP-PI-USER-TOKEN (SS-TP-REDIR)
			//                                      TP-PI-PIPE-TOKEN (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).setUserToken(0);
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).setPipeToken(0);
			// COB_CODE: SET TP-PI-PIPE-IS-CLOSED (SS-TP-REDIR)
			//               TP-PI-PIPE-STA-OK (SS-TP-REDIR)
			//                                   TO TRUE
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().setPipeIsClosed();
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getPipeStaCd().setOk();
			//        THIS PIPE CANNOT BE SET UP TO REDIRECT TO THE NEXT PIPE
			//          FOR MANY REASONS.  IF AN OPEN CALL IS MADE TO THIS
			//          REGION, WE WILL THEN SET IT UP FOR REDIRECTION
			// COB_CODE: INITIALIZE TP-PI-REDIRECTION-INF (SS-TP-REDIR)
			initRedirectionInf();
			// COB_CODE: SUBTRACT +1             FROM AA-OPEN-PIPES
			ws.setAaOpenPipes(Trunc.toShort(ws.getAaOpenPipes() - 1, 4));
			//        RESTORE THE CICS PIPE MAINTAINENCE CONTRACT SO THAT THE
			//          NEW PIPE INFORMATION MAY BE SAVED TO THE TABLE.
			// COB_CODE: MOVE SA-CICS-PIPE-MAINTENANCE-CNT
			//                                   TO CICS-PIPE-MAINTENANCE-CNT
			ws.getCicsPipeMaintenanceCnt().setCicsPipeMaintenanceCntFormatted(ws.getSaveArea().getCicsPipeMaintenanceCntFormatted());
		}
		//    SINCE AT THIS MOMENT WE WILL NO LONGER BE REDIRECTED TO
		//      THIS PIPE, SET THE ORIGINAL PIPE AS NOT BEING REDIRECTED.
		//      HOWEVER, DO NOT CLEAR THE NUMBER OF REDIRECTIONS MADE.
		//      THIS INFORMATION WILL BE NECESSARY FOR PROPERLY REDIRECTING
		//      TO THE NEXT PIPE.
		// COB_CODE: SET TP-PI-RI-PIPE-NOT-REDIRECTED (SS-TP-ORIG)
		//                                       TO TRUE.
		ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().setNotRedirected();
	}

	/**Original name: 5000-CLOSE-PIPE<br>
	 * <pre>***************************************************************
	 *   ENSURES A CICS PIPE IS CLOSED WHEN THE TIME IS APPROPRIATE.
	 *   WE WILL WAIT UNTIL THE NUMBER OF REQUESTED CLOSES ARE EQUAL
	 *   TO THE NUMBER OF REQUESTED OPENS.
	 * ***************************************************************</pre>*/
	private void closePipe() {
		// COB_CODE: IF SW-REGION-INF-NOT-FND
		//               GO TO 5000-EXIT
		//           END-IF.
		if (ws.getSwitches().getRegionInfFndFlag().isNotFnd()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE L-CI-CICS-APPL-ID  TO EA-11-CICS-APPL-ID
			ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().setEa11CicsApplId(lCicsPipeInterfaceContract.getCicsApplId());
			// COB_CODE: MOVE EA-11-REGION-NOT-OPEN
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().getEa11RegionNotOpenFormatted());
			// COB_CODE: GO TO 5000-EXIT
			return;
		}
		//    IF THE PIPE IS ALREADY CLOSED, EXIT WITH A WARNING.
		// COB_CODE: IF TP-PI-PIPE-IS-CLOSED (SS-TP)
		//               GO TO 5000-EXIT
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally().isPipeIsClosed()) {
			// COB_CODE: SET L-CI-EI-ES-WARNING  TO TRUE
			lCicsPipeInterfaceContract.setEiEsWarning();
			// COB_CODE: MOVE EA-13-PIPE-ALREADY-CLOSED
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa13PipeAlreadyClosed().getEa13PipeAlreadyClosedFormatted());
			// COB_CODE: GO TO 5000-EXIT
			return;
		}
		//    DECREMENT THE OPEN/CLOSE TALLY FOR THIS PIPE.  IF WE ARE
		//      REDIRECTING TO THIS PIPE, UPDATE THE NUMBER OF REDIRECTS
		//      BEING DONE AND, WHEN NECESSARY, REMOVE THE REDIRECTION.
		// COB_CODE: SUBTRACT +1                 FROM TP-PI-OPEN-CLOSE-TALLY
		//                                                                (SS-TP).
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally()
				.setOpenCloseTally(Trunc.toShort(ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally().getOpenCloseTally() - 1, 4));
		// COB_CODE: IF TP-PI-RI-PIPE-IS-REDIRECTED (SS-TP-ORIG)
		//               END-IF
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().isIsRedirected()) {
			// COB_CODE: SUBTRACT +1             FROM TP-PI-RI-NBR-REDIRECTS
			//                                                       (SS-TP-ORIG)
			ws.getTpPipeInf(ws.getSubscripts().getTpOrig())
					.setRiNbrRedirects(Trunc.toShort(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects() - 1, 4));
			// COB_CODE: IF TP-PI-RI-NBR-REDIRECTS (SS-TP-ORIG) = +0
			//               INITIALIZE TP-PI-REDIRECTION-INF (SS-TP-ORIG)
			//           END-IF
			if (ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects() == 0) {
				// COB_CODE: INITIALIZE TP-PI-REDIRECTION-INF (SS-TP-ORIG)
				initRedirectionInf();
			}
		}
		// COB_CODE: IF TP-PI-KEEP-PIPE-OPEN (SS-TP)
		//               GO TO 5000-EXIT
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally().isKeepPipeOpen()) {
			// COB_CODE: GO TO 5000-EXIT
			return;
		}
		//    CALL THE CICS PIPE MAINTENANCE ROUTINE TO HANDLE THE CLOSING
		//      OF THE REQUESTED PIPE.
		// COB_CODE: INITIALIZE CICS-PIPE-MAINTENANCE-CNT.
		initCicsPipeMaintenanceCnt();
		// COB_CODE: SET CM-FN-CLOSE-PIPE        TO TRUE.
		ws.getCicsPipeMaintenanceCnt().getFunction().setCmFnClosePipe();
		// COB_CODE: MOVE TP-PI-PIPE-TOKEN (SS-TP)
		//                                       TO CM-PI-PIPE-TOKEN.
		ws.getCicsPipeMaintenanceCnt().setPiPipeToken(ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeToken());
		// COB_CODE: MOVE TP-PI-USER-TOKEN (SS-TP)
		//                                       TO CM-PI-USER-TOKEN.
		ws.getCicsPipeMaintenanceCnt().setPiUserToken(ws.getTpPipeInf(ws.getSubscripts().getTp()).getUserToken());
		// COB_CODE: PERFORM 9300-CALL-PIPE-MTN-COMMON-RTN
		//              THRU 9300-EXIT.
		callPipeMtnCommonRtn();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 5000-EXIT
		//           END-IF.
		if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 5000-EXIT
			return;
		}
		//    IF SUCCESSFUL, UPDATE PIPE INFORMATION IN TABLE TO 'CLOSED'
		// COB_CODE: MOVE +0                     TO TP-PI-USER-TOKEN (SS-TP)
		//                                          TP-PI-PIPE-TOKEN (SS-TP).
		ws.getTpPipeInf(ws.getSubscripts().getTp()).setUserToken(0);
		ws.getTpPipeInf(ws.getSubscripts().getTp()).setPipeToken(0);
		// COB_CODE: SET TP-PI-PIPE-IS-CLOSED (SS-TP)
		//               TP-PI-PIPE-STA-OK (SS-TP)
		//                                       TO TRUE.
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally().setPipeIsClosed();
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().setOk();
		// COB_CODE: SUBTRACT +1                 FROM AA-OPEN-PIPES.
		ws.setAaOpenPipes(Trunc.toShort(ws.getAaOpenPipes() - 1, 4));
	}

	/**Original name: 6000-RETURN-TOKEN-INF<br>
	 * <pre>***************************************************************
	 *   ADVANCED USERS MAY REQUEST THE PIPE IDENTIFICATION TOKENS TO
	 *   BE USED FOR THEIR OWN PURPOSES.  ENSURE PIPE IS OPEN/AVAILABLE
	 * ***************************************************************</pre>*/
	private void returnTokenInf() {
		// COB_CODE: IF SW-REGION-INF-NOT-FND
		//               GO TO 6000-EXIT
		//           END-IF.
		if (ws.getSwitches().getRegionInfFndFlag().isNotFnd()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE L-CI-CICS-APPL-ID  TO EA-11-CICS-APPL-ID
			ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().setEa11CicsApplId(lCicsPipeInterfaceContract.getCicsApplId());
			// COB_CODE: MOVE EA-11-REGION-NOT-OPEN
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().getEa11RegionNotOpenFormatted());
			// COB_CODE: GO TO 6000-EXIT
			return;
		}
		// COB_CODE: IF TP-PI-PIPE-IS-CLOSED (SS-TP)
		//               GO TO 6000-EXIT
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTp()).getOpenCloseTally().isPipeIsClosed()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP)
			//                                   TO EA-11-CICS-APPL-ID
			ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().setEa11CicsApplId(ws.getTpPipeInf(ws.getSubscripts().getTp()).getCicsApplId());
			// COB_CODE: MOVE EA-11-REGION-NOT-OPEN
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa11RegionNotOpen().getEa11RegionNotOpenFormatted());
			// COB_CODE: GO TO 6000-EXIT
			return;
		}
		// COB_CODE: IF TP-PI-PIPE-STA-UNUSABLE (SS-TP)
		//               GO TO 6000-EXIT
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().isUnusable()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP)
			//                                   TO EA-18-CICS-APPL-ID
			ws.getErrorAndAdviceMessages().getEa18PipeIsUnusable().setEa18CicsApplId(ws.getTpPipeInf(ws.getSubscripts().getTp()).getCicsApplId());
			// COB_CODE: MOVE EA-18-PIPE-IS-UNUSABLE
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa18PipeIsUnusable().getEa18PipeIsUnusableFormatted());
			// COB_CODE: GO TO 6000-EXIT
			return;
		}
		// COB_CODE: MOVE TP-PI-PIPE-TOKEN (SS-TP)
		//                                       TO L-CI-GT-PIPE-TOKEN.
		lCicsPipeInterfaceContract.setGtPipeToken(ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeToken());
		// COB_CODE: MOVE TP-PI-USER-TOKEN (SS-TP)
		//                                       TO L-CI-GT-USER-TOKEN.
		lCicsPipeInterfaceContract.setGtUserToken(ws.getTpPipeInf(ws.getSubscripts().getTp()).getUserToken());
		//    IN CASE THIS AREA IS REDIRECTED, PROVIDE THE APPLID
		//      ASSOCIATED TO THESE TOKENS.
		// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP)
		//                                       TO L-CI-GT-APPL-ID.
		lCicsPipeInterfaceContract.setGtApplId(ws.getTpPipeInf(ws.getSubscripts().getTp()).getCicsApplId());
	}

	/**Original name: 9000-CICS-ERROR<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING
	 * ******************************************************</pre>*/
	private void cicsError() {
		// COB_CODE: MOVE EXCI-RESPONSE          TO L-CI-EI-EXCI-RESP.
		lCicsPipeInterfaceContract.setEiExciResp(((int) (ws.getDfhxcplo().getExciReturnCode().getResponse())));
		// COB_CODE: MOVE EXCI-REASON            TO L-CI-EI-EXCI-RESP2.
		lCicsPipeInterfaceContract.setEiExciResp2(((int) (ws.getDfhxcplo().getExciReturnCode().getReason())));
		// COB_CODE: MOVE EXCI-SUB-REASON1       TO L-CI-EI-EXCI-RESP3.
		lCicsPipeInterfaceContract.setEiExciResp3(((int) (ws.getDfhxcplo().getExciReturnCode().getSubReason1())));
		// COB_CODE: MOVE EXCI-DPL-RESP          TO L-CI-EI-DPL-RESP.
		lCicsPipeInterfaceContract.setEiDplResp(((int) (ws.getDfhxcplo().getExciDplRetarea().getResp())));
		// COB_CODE: MOVE EXCI-DPL-RESP2         TO L-CI-EI-DPL-RESP2.
		lCicsPipeInterfaceContract.setEiDplResp2(((int) (ws.getDfhxcplo().getExciDplRetarea().getResp2())));
		// COB_CODE: PERFORM 9100-GENERATE-ERR-MSG
		//              THRU 9100-EXIT.
		generateErrMsg();
	}

	/**Original name: 9100-GENERATE-ERR-MSG<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING - MESSAGE GENERATION
	 * ******************************************************</pre>*/
	private void generateErrMsg() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-RESPONSE = EXCI-WARNING
		//                      THRU 9110-EXIT
		//               WHEN EXCI-RESPONSE = EXCI-RETRYABLE
		//                      THRU 9120-EXIT
		//               WHEN EXCI-RESPONSE = EXCI-USER-ERROR
		//                      THRU 9130-EXIT
		//               WHEN EXCI-RESPONSE = EXCI-SYSTEM-ERROR
		//                      THRU 9140-EXIT
		//               WHEN EXCI-DPL-RESP NOT = EXEC-NORMAL
		//                      THRU 9150-EXIT
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciWarning()) {
			// COB_CODE: PERFORM 9110-WARNING-MSG
			//              THRU 9110-EXIT
			warningMsg();
		} else if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciRetryable()) {
			// COB_CODE: PERFORM 9120-RETRYABLE-MSG
			//              THRU 9120-EXIT
			retryableMsg();
		} else if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciUserError()) {
			// COB_CODE: PERFORM 9130-USER-MSG
			//              THRU 9130-EXIT
			userMsg();
		} else if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciSystemError()) {
			// COB_CODE: PERFORM 9140-SYSTEM-MSG
			//              THRU 9140-EXIT
			systemMsg();
		} else if (ws.getDfhxcplo().getExciDplRetarea().getResp() != ws.getDfhxcrco().getExecNormal()) {
			// COB_CODE: PERFORM 9150-CHECK-DPL-RESP-VALUES
			//              THRU 9150-EXIT
			checkDplRespValues();
		}
	}

	/**Original name: 9110-WARNING-MSG<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING - WARNING MESSAGE GENERATION
	 * ******************************************************
	 *     CURRENTLY, ALL WARNING CODES WILL PRODUCE A GENERIC MESSAGE</pre>*/
	private void warningMsg() {
		// COB_CODE: MOVE EA-20-GEN-WARNING-MSG  TO L-CI-EI-MESSAGE.
		lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa20GenWarningMsg().getEa20GenWarningMsgFormatted());
		// COB_CODE: SET L-CI-EI-ES-WARNING
		//               L-CI-EI-ET-SYSTEM-WARNING
		//                                       TO TRUE.
		lCicsPipeInterfaceContract.setEiEsWarning();
		lCicsPipeInterfaceContract.setEiEtSystemWarning();
	}

	/**Original name: 9120-RETRYABLE-MSG<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING - RETRYABLE MESSAGE GENERATION
	 * ******************************************************
	 *     MARK THE CURRENT PIPE AS 'UNUSABLE'</pre>*/
	private void retryableMsg() {
		// COB_CODE: SET TP-PI-PIPE-STA-UNUSABLE (SS-TP)
		//                                       TO TRUE
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().setUnusable();
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-REASON = NO-CICS
		//                                       TO L-CI-EI-MESSAGE
		//               WHEN OTHER
		//                                       TO L-CI-EI-MESSAGE
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getNoCics()) {
			// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP)
			//                               TO EA-41-REGION-ATTEMPTED
			ws.getErrorAndAdviceMessages().getEa41RtyNoConnectionMsg()
					.setEa41RegionAttempted(ws.getTpPipeInf(ws.getSubscripts().getTp()).getCicsApplId());
			// COB_CODE: MOVE EA-41-RTY-NO-CONNECTION-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa41RtyNoConnectionMsg().getEa41RtyNoConnectionMsgFormatted());
		} else {
			// COB_CODE: MOVE EA-40-GEN-RETRYABLE-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa40GenRetryableMsg().getEa40GenRetryableMsgFormatted());
		}
		// COB_CODE: SET L-CI-EI-ES-SYSTEM-ERROR
		//               SW-ERROR-FOUND          TO TRUE.
		lCicsPipeInterfaceContract.setEiEsSystemError();
		ws.getSwitches().getErrorFoundFlag().setErrorFound();
	}

	/**Original name: 9130-USER-MSG<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING - USER ERROR MESSAGE GENERATION
	 * ******************************************************
	 *     MARK THE CURRENT PIPE AS 'UNUSABLE'</pre>*/
	private void userMsg() {
		// COB_CODE: SET TP-PI-PIPE-STA-UNUSABLE (SS-TP)
		//                                       TO TRUE
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().setUnusable();
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN EXCI-REASON = INVALID-USER-TOKEN
		//                                            TO L-CI-EI-MESSAGE
		//                    WHEN EXCI-REASON = PIPE-NOT-OPEN
		//                                            TO L-CI-EI-MESSAGE
		//                    WHEN EXCI-REASON = INVALID-TRANSID
		//                                            TO L-CI-EI-MESSAGE
		//                    WHEN EXCI-REASON = IRP-ABORT-RECEIVED
		//           *          CAPTURE THE SERVER ERROR MESSAGE AND GIVE TO CONSUMER
		//                                            TO L-CI-EI-MESSAGE
		//                    WHEN EXCI-REASON = INVALID-PIPE-TOKEN
		//                                            TO L-CI-EI-MESSAGE
		//                    WHEN EXCI-REASON = SERVER-ABENDED
		//                                            TO L-CI-EI-MESSAGE
		//                    WHEN EXCI-REASON = DFHXCOPT-LOAD-FAILED
		//                                            TO L-CI-EI-MESSAGE
		//                    WHEN OTHER
		//                                            TO L-CI-EI-MESSAGE
		//                END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getInvalidUserToken()) {
			// COB_CODE: MOVE TP-PI-USER-TOKEN (SS-TP)
			//                               TO EA-61-USER-TOKEN
			ws.getErrorAndAdviceMessages().getEa61UsrInvalidUsrTknMsg()
					.setEa61UserToken(TruncAbs.toLong(ws.getTpPipeInf(ws.getSubscripts().getTp()).getUserToken(), 10));
			// COB_CODE: MOVE EA-61-USR-INVALID-USR-TKN-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract
					.setEiMessage(ws.getErrorAndAdviceMessages().getEa61UsrInvalidUsrTknMsg().getEa61UsrInvalidUsrTknMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getPipeNotOpen()) {
			// COB_CODE: MOVE EA-62-USR-PIPE-NOT-OPEN-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa62UsrPipeNotOpenMsg().getEa62UsrPipeNotOpenMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getInvalidTransid()) {
			// COB_CODE: MOVE L-CI-CP-TARGET-TRAN-ID
			//                               TO EA-63-TRAN-ID
			ws.getErrorAndAdviceMessages().getEa63UsrInvalidTranIdMsg().setEa63TranId(lCicsPipeInterfaceContract.getCpTargetTranId());
			// COB_CODE: MOVE EA-63-USR-INVALID-TRAN-ID-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract
					.setEiMessage(ws.getErrorAndAdviceMessages().getEa63UsrInvalidTranIdMsg().getEa63UsrInvalidTranIdMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getIrpAbortReceived()) {
			//          CAPTURE THE SERVER ERROR MESSAGE AND GIVE TO CONSUMER
			// COB_CODE: SET ADDRESS OF L-SERVER-MESSAGE
			//                               TO EXCI-MSG-PTR
			lServerMessage = ((pointerManager.resolve(ws.getDfhxcplo().getExciReturnCode().getMsgPtr(), LServerMessage.class)));
			// COB_CODE: MOVE L-SM-MESSAGE (1: L-SM-LENGTH)
			//                               TO EA-64-SERVER-MESSAGE
			ws.getErrorAndAdviceMessages().getEa64UsrCicsServerError().setMessageFormatted(String.valueOf(lServerMessage.getMessage()));
			// COB_CODE: IF L-SM-LENGTH > LENGTH OF EA-64-SERVER-MESSAGE
			//                               TO TRUE
			//           END-IF
			if (lServerMessage.getLength2() > Ea64UsrCicsServerError.Len.MESSAGE) {
				// COB_CODE: SET EA-64-SERVER-MSG-CUT-OFF
				//                           TO TRUE
				ws.getErrorAndAdviceMessages().getEa64UsrCicsServerError().setMsgCutOff();
			}
			// COB_CODE: MOVE EA-64-USR-CICS-SERVER-ERROR
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa64UsrCicsServerError().getEa64UsrCicsServerErrorFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getInvalidPipeToken()) {
			// COB_CODE: MOVE TP-PI-PIPE-TOKEN (SS-TP)
			//                               TO EA-65-PIPE-TOKEN
			ws.getErrorAndAdviceMessages().getEa65UsrInvalidPipTknMsg()
					.setEa65PipeToken(TruncAbs.toLong(ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeToken(), 10));
			// COB_CODE: MOVE EA-65-USR-INVALID-PIP-TKN-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract
					.setEiMessage(ws.getErrorAndAdviceMessages().getEa65UsrInvalidPipTknMsg().getEa65UsrInvalidPipTknMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getServerAbended()) {
			// COB_CODE: MOVE EXCI-DPL-ABCODE
			//                               TO EA-66-ABEND-CODE
			ws.getErrorAndAdviceMessages().getEa66UsrCicsPgmAbendedMsg().setEa66AbendCode(ws.getDfhxcplo().getExciDplRetarea().getAbcode());
			// COB_CODE: MOVE EA-66-USR-CICS-PGM-ABENDED-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract
					.setEiMessage(ws.getErrorAndAdviceMessages().getEa66UsrCicsPgmAbendedMsg().getEa66UsrCicsPgmAbendedMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getDfhxcoptLoadFailed()) {
			// COB_CODE: MOVE EA-67-USR-DFHXCOPT-LOAD-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa67UsrDfhxcoptLoadMsg().getEa67UsrDfhxcoptLoadMsgFormatted());
		} else {
			// COB_CODE: MOVE EA-60-GEN-USER-ERROR-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa60GenUserErrorMsg().getEa60GenUserErrorMsgFormatted());
		}
		// COB_CODE: SET L-CI-EI-ES-SYSTEM-ERROR
		//               SW-ERROR-FOUND          TO TRUE.
		lCicsPipeInterfaceContract.setEiEsSystemError();
		ws.getSwitches().getErrorFoundFlag().setErrorFound();
	}

	/**Original name: 9140-SYSTEM-MSG<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING - SYSTEM ERROR MESSAGE GENERATION
	 * ******************************************************
	 *     MARK THE CURRENT PIPE AS 'UNUSABLE'</pre>*/
	private void systemMsg() {
		// COB_CODE: SET TP-PI-PIPE-STA-UNUSABLE (SS-TP)
		//                                       TO TRUE
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getPipeStaCd().setUnusable();
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-REASON = SERVER-TIMEDOUT
		//                                       TO L-CI-EI-MESSAGE
		//               WHEN OTHER
		//                                       TO L-CI-EI-MESSAGE
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getServerTimedout()) {
			// COB_CODE: MOVE SA-CICS-PGM-TO-CALL
			//                               TO EA-81-TARGET-CICS-PGM
			ws.getErrorAndAdviceMessages().getEa81CicsProgramTimeoutMsg().setEa81TargetCicsPgm(ws.getSaveArea().getCicsPgmToCall());
			// COB_CODE: MOVE EA-81-CICS-PROGRAM-TIMEOUT-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract
					.setEiMessage(ws.getErrorAndAdviceMessages().getEa81CicsProgramTimeoutMsg().getEa81CicsProgramTimeoutMsgFormatted());
		} else {
			// COB_CODE: MOVE EA-80-GEN-SYSTEM-ERR-MSG
			//                               TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa80GenSystemErrMsg().getEa80GenSystemErrMsgFormatted());
		}
		// COB_CODE: SET L-CI-EI-ES-SYSTEM-ERROR
		//               SW-ERROR-FOUND          TO TRUE.
		lCicsPipeInterfaceContract.setEiEsSystemError();
		ws.getSwitches().getErrorFoundFlag().setErrorFound();
	}

	/**Original name: 9150-CHECK-DPL-RESP-VALUES<br>
	 * <pre>***************************************************************
	 *   A DPL VALUE HAS BEEN FOUND WITHOUT A CORRESPONDING EXCI VALUE.
	 *   INTERROGATE AND MAKE SURE THE CODE DOES NOT COME BACK AS A
	 *   FATAL VALUE.
	 * ***************************************************************
	 *     INDICATE TO THE CALLING PGM A DPL RESPONSE VALUE WAS FOUND</pre>*/
	private void checkDplRespValues() {
		// COB_CODE: SET L-CI-EI-ET-DPL-RESP-CAPTURED
		//                                       TO TRUE.
		lCicsPipeInterfaceContract.setEiEtDplRespCaptured();
		//    CHECK THE VALUES THAT ARE POSSIBLE WHEN A DPL_REQUEST CALL
		//      IS MADE.  IF FOUND TO BE TRUE, SET A FATAL ERROR SINCE THE
		//      CICS PROGRAM WAS NOT EXECUTED.
		// COB_CODE: IF EXCI-DPL-RESP = EXEC-LENGERR
		//             OR
		//              EXCI-DPL-RESP = EXEC-PGMIDERR
		//             OR
		//              EXCI-DPL-RESP = EXEC-SYSIDERR
		//             OR
		//              EXCI-DPL-RESP = EXEC-NOTAUTH
		//             OR
		//              EXCI-DPL-RESP = EXEC-TERMERR
		//             OR
		//              EXCI-DPL-RESP = EXEC-ROLLEDBACK
		//               GO TO 9150-EXIT
		//           END-IF.
		if (ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecLengerr()
				|| ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecPgmiderr()
				|| ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecSysiderr()
				|| ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecNotauth()
				|| ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecTermerr()
				|| ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecRolledback()) {
			// COB_CODE: PERFORM 9151-SET-DPL-FATAL-ERR
			//              THRU 9151-EXIT
			setDplFatalErr();
			// COB_CODE: GO TO 9150-EXIT
			return;
		}
		//    A DPL VALUE IS FOUND, BUT NOT ONE NORMALLY PRODUCED IN A
		//      DPL_REQUEST CALL.  THIS MUST HAVE BEEN GENERATED FROM THE
		//      VARIOUS OPERATIONS HANDLED BY THE ONLINE SERVICE BEING
		//      CALLED AND THUS HANDLED BY THE SERVICE.  SET A WARNING ONLY
		// COB_CODE: SET L-CI-EI-ES-WARNING      TO TRUE.
		lCicsPipeInterfaceContract.setEiEsWarning();
		// COB_CODE: MOVE EA-15-DPL-RESPONSE-GIVEN
		//                                       TO L-CI-EI-MESSAGE.
		lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa15DplResponseGiven().getEa15DplResponseGivenFormatted());
	}

	/**Original name: 9151-SET-DPL-FATAL-ERR<br>
	 * <pre>***************************************************************
	 *   SET THE DPL FATAL ERROR MESSAGE WITH THE APPROPRIATE
	 *   DESCRIPTION.
	 * ***************************************************************</pre>*/
	private void setDplFatalErr() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-DPL-RESP = EXEC-LENGERR
		//                                       TO TRUE
		//               WHEN EXCI-DPL-RESP = EXEC-PGMIDERR
		//                                       TO TRUE
		//               WHEN EXCI-DPL-RESP = EXEC-SYSIDERR
		//                                       TO TRUE
		//               WHEN EXCI-DPL-RESP = EXEC-NOTAUTH
		//                                       TO TRUE
		//               WHEN EXCI-DPL-RESP = EXEC-TERMERR
		//                                       TO TRUE
		//               WHEN EXCI-DPL-RESP = EXEC-ROLLEDBACK
		//                                       TO TRUE
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecLengerr()) {
			// COB_CODE: SET EA-17-DD-LEN-ERR
			//                               TO TRUE
			ws.getErrorAndAdviceMessages().getEa17FatalDplRespFound().getEa17DplDesc().setLenErr();
		} else if (ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecPgmiderr()) {
			// COB_CODE: SET EA-17-DD-PGM-ID-ERR
			//                               TO TRUE
			ws.getErrorAndAdviceMessages().getEa17FatalDplRespFound().getEa17DplDesc().setPgmIdErr();
		} else if (ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecSysiderr()) {
			// COB_CODE: SET EA-17-DD-SYSID-ERR
			//                               TO TRUE
			ws.getErrorAndAdviceMessages().getEa17FatalDplRespFound().getEa17DplDesc().setSysidErr();
		} else if (ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecNotauth()) {
			// COB_CODE: SET EA-17-DD-NOT-AUTH
			//                               TO TRUE
			ws.getErrorAndAdviceMessages().getEa17FatalDplRespFound().getEa17DplDesc().setNotAuth();
		} else if (ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecTermerr()) {
			// COB_CODE: SET EA-17-DD-TERM-ERR
			//                               TO TRUE
			ws.getErrorAndAdviceMessages().getEa17FatalDplRespFound().getEa17DplDesc().setTermErr();
		} else if (ws.getDfhxcplo().getExciDplRetarea().getResp() == ws.getDfhxcrco().getExecRolledback()) {
			// COB_CODE: SET EA-17-DD-ROLLED-BACK
			//                               TO TRUE
			ws.getErrorAndAdviceMessages().getEa17FatalDplRespFound().getEa17DplDesc().setRolledBack();
		}
		// COB_CODE: MOVE EA-17-FATAL-DPL-RESP-FOUND
		//                                       TO L-CI-EI-MESSAGE.
		lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa17FatalDplRespFound().getEa17FatalDplRespFoundFormatted());
		// COB_CODE: SET L-CI-EI-ES-SYSTEM-ERROR
		//               SW-ERROR-FOUND          TO TRUE.
		lCicsPipeInterfaceContract.setEiEsSystemError();
		ws.getSwitches().getErrorFoundFlag().setErrorFound();
	}

	/**Original name: 9200-GET-PIPE-INF<br>
	 * <pre>******************************************************
	 *   SETS THE SUBSCRIPTS TO POINT AT ANY INFORMATION WE HAVE ON
	 *   THE REGIONS IN QUESTION.
	 * ******************************************************</pre>*/
	private void getPipeInf() {
		// COB_CODE: PERFORM 9210-FIND-PIPE-IN-TBL
		//              THRU 9210-EXIT.
		rng9210FindPipeInTbl();
		//    IF THE PIPE INFORMATION COULD NOT BE FOUND, EXIT.
		// COB_CODE: IF SW-REGION-INF-NOT-FND
		//               GO TO 9200-EXIT
		//           END-IF.
		if (ws.getSwitches().getRegionInfFndFlag().isNotFnd()) {
			// COB_CODE: GO TO 9200-EXIT
			return;
		}
		//    THE ORIGINALLY REQUESTED PIPE WAS FOUND.  SAVE IT.
		//    SS-TP-TEMP REPRESENTS THE PIPE FOUND BY THE SEARCH.
		// COB_CODE: MOVE SS-TP-TEMP             TO SS-TP
		//                                          SS-TP-ORIG.
		ws.getSubscripts().setTp(ws.getSubscripts().getTpTemp());
		ws.getSubscripts().setTpOrig(ws.getSubscripts().getTpTemp());
		//    THERE IS NO REDIRECTION FOR THE CURRENT PIPE.  EXIT.
		// COB_CODE: IF TP-PI-RI-PIPE-NOT-REDIRECTED (SS-TP-ORIG)
		//               GO TO 9200-EXIT
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().isNotRedirected()) {
			// COB_CODE: GO TO 9200-EXIT
			return;
		}
		//    CAPTURE THE REDIRECTED PIPE INFORMATION.  SET IT TO BE USED
		//      FOR FROM NOW ON.
		// COB_CODE: MOVE TP-PI-RI-REDIR-PIPE (SS-TP-ORIG)
		//                                       TO SS-TP
		//                                          SS-TP-REDIR.
		ws.getSubscripts().setTp(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().getRiRedirPipe());
		ws.getSubscripts().setTpRedir(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().getRiRedirPipe());
	}

	/**Original name: 9210-FIND-PIPE-IN-TBL<br>
	 * <pre>******************************************************
	 *   FIND THE INFORMATION LOCATION ON THE REQUESTED CICS REGION.
	 *   INFO MAY NOT EXIST AND THAT IS NOT AN ERROR YET.
	 * ******************************************************
	 *     SS-TP-TEMP REPRESENTS THE PIPE WE ARE CURRENTLY LOOKING AT
	 *       IN OUR SEARCH FOR A DESIRED PIPE.</pre>*/
	private void findPipeInTbl() {
		// COB_CODE: SET SW-REGION-INF-NOT-FND   TO TRUE.
		ws.getSwitches().getRegionInfFndFlag().setNotFnd();
		// COB_CODE: MOVE +0                     TO SS-TP-TEMP.
		ws.getSubscripts().setTpTemp(((short) 0));
	}

	/**Original name: 9210-A<br>*/
	private String a1() {
		// COB_CODE: ADD +1                      TO SS-TP-TEMP.
		ws.getSubscripts().setTpTemp(Trunc.toShort(1 + ws.getSubscripts().getTpTemp(), 4));
		// COB_CODE: IF SS-TP-TEMP > SA-MAX-PIPE-COUNT
		//             OR
		//              SS-TP-TEMP > AA-PIPES-AVAIL
		//               GO TO 9210-EXIT
		//           END-IF.
		if (ws.getSubscripts().getTpTemp() > ws.getSaveArea().getMaxPipeCount() || ws.getSubscripts().getTpTemp() > ws.getAaPipesAvail()) {
			// COB_CODE: GO TO 9210-EXIT
			return "";
		}
		// COB_CODE: IF TP-PI-CICS-APPL-ID (SS-TP-TEMP) = SA-CICS-APPL-ID
		//               GO TO 9210-EXIT
		//           END-IF.
		if (Conditions.eq(ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).getCicsApplId(), ws.getSaveArea().getCicsApplId())) {
			// COB_CODE: SET SW-REGION-INF-FND   TO TRUE
			ws.getSwitches().getRegionInfFndFlag().setFnd();
			// COB_CODE: GO TO 9210-EXIT
			return "";
		}
		// COB_CODE: GO TO 9210-A.
		return "9210-A";
	}

	/**Original name: 9220-FIND-AVAIL-POS-IN-TBL<br>
	 * <pre>***************************************************************
	 *   LOCATES THE FIRST POSITION IN THE TABLE WHERE EITHER NO PIPE
	 *   INFORMATION IS FOUND OR THE PIPE FOUND IS ALREADY CLOSED.
	 * ***************************************************************</pre>*/
	private void findAvailPosInTbl() {
		// COB_CODE: IF AA-PIPES-AVAIL >= SA-MAX-PIPE-COUNT
		//               GO TO 9220-EXIT
		//           END-IF.
		if (ws.getAaPipesAvail() >= ws.getSaveArea().getMaxPipeCount()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE SA-MAX-PIPE-COUNT  TO EA-12-MAX-PIPES
			ws.getErrorAndAdviceMessages().getEa12TooManyPipes().setEa12MaxPipes(ws.getSaveArea().getMaxPipeCount());
			// COB_CODE: MOVE EA-12-TOO-MANY-PIPES
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa12TooManyPipes().getEa12TooManyPipesFormatted());
			// COB_CODE: GO TO 9220-EXIT
			return;
		}
		// COB_CODE: IF AA-OPEN-PIPES >= CF-MAX-OPEN-PIPES
		//               GO TO 9220-EXIT
		//           END-IF.
		if (ws.getAaOpenPipes() >= ws.getConstantFields().getMaxOpenPipes()) {
			// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND      TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE CF-MAX-OPEN-PIPES  TO EA-10-MAX-PIPES
			ws.getErrorAndAdviceMessages().getEa10TooManyPipesOpen().setEa10MaxPipes(ws.getConstantFields().getMaxOpenPipes());
			// COB_CODE: MOVE EA-10-TOO-MANY-PIPES-OPEN
			//                                   TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa10TooManyPipesOpen().getEa10TooManyPipesOpenFormatted());
			// COB_CODE: GO TO 9220-EXIT
			return;
		}
		// COB_CODE: ADD +1                      TO AA-PIPES-AVAIL.
		ws.setAaPipesAvail(Trunc.toShort(1 + ws.getAaPipesAvail(), 4));
		// COB_CODE: MOVE AA-PIPES-AVAIL         TO SS-TP.
		ws.getSubscripts().setTp(ws.getAaPipesAvail());
	}

	/**Original name: 9300-CALL-PIPE-MTN-COMMON-RTN<br>
	 * <pre>******************************************************
	 *   CALL THE CICS PIPE MAINTENANCE ROUTINE.   IF ERRORS ARE FOUND
	 *   PASS THEM ALONG TO THE CALLER.
	 * ******************************************************</pre>*/
	private void callPipeMtnCommonRtn() {
		Ts548099 ts548099 = null;
		// COB_CODE: CALL CF-PIPE-MAINTENANCE-RTN USING
		//                                        CICS-PIPE-MAINTENANCE-CNT.
		ts548099 = Ts548099.getInstance();
		ts548099.run(ws.getCicsPipeMaintenanceCnt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN CM-EI-ES-WARNING
		//                   END-IF
		//               WHEN CM-EI-ES-LOGIC-ERROR
		//                       SW-ERROR-FOUND  TO TRUE
		//                       SW-ERROR-FOUND  TO TRUE
		//               WHEN CM-EI-ES-SYSTEM-ERROR
		//                       SW-ERROR-FOUND  TO TRUE
		//                       SW-ERROR-FOUND  TO TRUE
		//           END-EVALUATE.
		switch (ws.getCicsPipeMaintenanceCnt().getErrorInformation().getErrorSeverity().getErrorSeverity()) {

		case LCmEiErrorSeverity.WARNING:// COB_CODE: SET L-CI-EI-ES-WARNING
			//                               TO TRUE
			lCicsPipeInterfaceContract.setEiEsWarning();
			// COB_CODE: IF CM-EI-ET-REGION-SWAPPED
			//                               TO TRUE
			//           END-IF
			if (ws.getCicsPipeMaintenanceCnt().getErrorInformation().getErrorType().isCmEiEtRegionSwapped()) {
				// COB_CODE: SET L-CI-EI-ET-REGION-SWAPPED
				//                           TO TRUE
				lCicsPipeInterfaceContract.setEiEtRegionSwapped();
			}
			// COB_CODE: IF CM-EI-ET-SYSTEM-WARNING
			//                               TO TRUE
			//           END-IF
			if (ws.getCicsPipeMaintenanceCnt().getErrorInformation().getErrorType().isCmEiEtSystemWarning()) {
				// COB_CODE: SET L-CI-EI-ET-SYSTEM-WARNING
				//                           TO TRUE
				lCicsPipeInterfaceContract.setEiEtSystemWarning();
			}
			break;

		case LCmEiErrorSeverity.LOGIC_ERROR:// COB_CODE: SET L-CI-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND  TO TRUE
			lCicsPipeInterfaceContract.setEiEsLogicError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			break;

		case LCmEiErrorSeverity.SYSTEM_ERROR:// COB_CODE: SET L-CI-EI-ES-SYSTEM-ERROR
			//               SW-ERROR-FOUND  TO TRUE
			lCicsPipeInterfaceContract.setEiEsSystemError();
			ws.getSwitches().getErrorFoundFlag().setErrorFound();
			break;

		default:
			break;
		}
		//    MAKE SURE TO OVERWRITE THE ERROR MESSAGE GOING BACK TO THE
		//      CALLER ONLY WHEN A MESSAGE HAS BEEN PRODUCED BY THE PIPE
		//      MAINTENANCE PROGRAM.
		// COB_CODE: IF CM-EI-MESSAGE NOT = SPACES
		//               MOVE CM-EI-MESSAGE      TO L-CI-EI-MESSAGE
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getCicsPipeMaintenanceCnt().getErrorInformation().getMessage())) {
			// COB_CODE: MOVE CM-EI-MESSAGE      TO L-CI-EI-MESSAGE
			lCicsPipeInterfaceContract.setEiMessage(ws.getCicsPipeMaintenanceCnt().getErrorInformation().getMessage());
		}
		// COB_CODE: MOVE CM-EI-EXCI-RESP        TO L-CI-EI-EXCI-RESP.
		lCicsPipeInterfaceContract.setEiExciResp(ws.getCicsPipeMaintenanceCnt().getErrorInformation().getExciResp());
		// COB_CODE: MOVE CM-EI-EXCI-RESP2       TO L-CI-EI-EXCI-RESP2.
		lCicsPipeInterfaceContract.setEiExciResp2(ws.getCicsPipeMaintenanceCnt().getErrorInformation().getExciResp2());
		// COB_CODE: MOVE CM-EI-EXCI-RESP3       TO L-CI-EI-EXCI-RESP3.
		lCicsPipeInterfaceContract.setEiExciResp3(ws.getCicsPipeMaintenanceCnt().getErrorInformation().getExciResp3());
		// COB_CODE: MOVE CM-EI-DPL-RESP         TO L-CI-EI-DPL-RESP.
		lCicsPipeInterfaceContract.setEiDplResp(ws.getCicsPipeMaintenanceCnt().getErrorInformation().getDplResp());
		// COB_CODE: MOVE CM-EI-DPL-RESP2        TO L-CI-EI-DPL-RESP2.
		lCicsPipeInterfaceContract.setEiDplResp2(ws.getCicsPipeMaintenanceCnt().getErrorInformation().getDplResp2());
	}

	/**Original name: 9310-OPEN-PIPE-TO-ORIG-REGION<br>
	 * <pre>******************************************************
	 *   CALL THE CICS PIPE MAINTENANCE ROUTINE REQUESTING A PIPE TO BE
	 *   OPENED TO THE ORIGINALLY REQUESTED REGION.
	 * ******************************************************</pre>*/
	private void openPipeToOrigRegion() {
		// COB_CODE: INITIALIZE CICS-PIPE-MAINTENANCE-CNT.
		initCicsPipeMaintenanceCnt();
		// COB_CODE: SET CM-FN-OPEN-PIPE         TO TRUE.
		ws.getCicsPipeMaintenanceCnt().getFunction().setCmFnOpenPipe();
		// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP-ORIG)
		//                                       TO CM-CICS-APPL-ID.
		ws.getCicsPipeMaintenanceCnt().getCicsApplId().setCicsApplId(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getCicsApplId());
		// COB_CODE: PERFORM 9300-CALL-PIPE-MTN-COMMON-RTN
		//              THRU 9300-EXIT.
		callPipeMtnCommonRtn();
	}

	/**Original name: 9400-HANDLE-PIPE-REDIRECTION<br>
	 * <pre>***************************************************************
	 *   PERFORM THE LOGIC OF HANDLING WHEN A CICS REGION HAS BEEN
	 *   REDIRECTED FROM THE INITIAL REQUEST
	 * ***************************************************************
	 *     FIND IF THE PIPE WE ARE REDIRECTED TO ALREADY HAS INFO SET UP</pre>*/
	private void handlePipeRedirection() {
		// COB_CODE: MOVE CM-CICS-APPL-ID        TO SA-CICS-APPL-ID.
		ws.getSaveArea().setCicsApplId(ws.getCicsPipeMaintenanceCnt().getCicsApplId().getCicsApplId());
		// COB_CODE: PERFORM 9210-FIND-PIPE-IN-TBL
		//              THRU 9210-EXIT.
		rng9210FindPipeInTbl();
		//    HANDLE LOGIC OF REDIRECTION BASED ON IF THE PIPE INFO ALREADY
		//      EXISTS
		// COB_CODE: IF SW-REGION-INF-NOT-FND
		//                  THRU 9410-EXIT
		//           ELSE
		//                  THRU 9420-EXIT
		//           END-IF.
		if (ws.getSwitches().getRegionInfFndFlag().isNotFnd()) {
			// COB_CODE: PERFORM 9410-REDIR-TO-NEW-PIPE
			//              THRU 9410-EXIT
			redirToNewPipe();
		} else {
			// COB_CODE: PERFORM 9420-REDIR-TO-EXISTING
			//              THRU 9420-EXIT
			redirToExisting();
		}
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 9400-EXIT
		//           END-IF.
		if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 9400-EXIT
			return;
		}
		//    IF NECESSARY, CLOSE THE PIPE WHICH WAS ORIGINALLY REQUESTED.
		// COB_CODE: IF TP-PI-PIPE-IS-CLOSED (SS-TP-ORIG)
		//               CONTINUE
		//           ELSE
		//               SUBTRACT +1             FROM AA-OPEN-PIPES
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getOpenCloseTally().isPipeIsClosed()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET CM-FN-CLOSE-PIPE    TO TRUE
			ws.getCicsPipeMaintenanceCnt().getFunction().setCmFnClosePipe();
			// COB_CODE: MOVE TP-PI-PIPE-TOKEN (SS-TP-ORIG)
			//                                   TO CM-PI-PIPE-TOKEN
			ws.getCicsPipeMaintenanceCnt().setPiPipeToken(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getPipeToken());
			// COB_CODE: MOVE TP-PI-USER-TOKEN (SS-TP-ORIG)
			//                                   TO CM-PI-USER-TOKEN
			ws.getCicsPipeMaintenanceCnt().setPiUserToken(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getUserToken());
			// COB_CODE: PERFORM 9300-CALL-PIPE-MTN-COMMON-RTN
			//              THRU 9300-EXIT
			callPipeMtnCommonRtn();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 9400-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 9400-EXIT
				return;
			}
			// COB_CODE: MOVE +0                 TO TP-PI-USER-TOKEN (SS-TP-ORIG)
			//                                      TP-PI-PIPE-TOKEN (SS-TP-ORIG)
			ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).setUserToken(0);
			ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).setPipeToken(0);
			// COB_CODE: SET TP-PI-PIPE-IS-CLOSED (SS-TP-ORIG)
			//               TP-PI-PIPE-STA-OK (SS-TP-ORIG)
			//                                   TO TRUE
			ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getOpenCloseTally().setPipeIsClosed();
			ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getPipeStaCd().setOk();
			// COB_CODE: SUBTRACT +1             FROM AA-OPEN-PIPES
			ws.setAaOpenPipes(Trunc.toShort(ws.getAaOpenPipes() - 1, 4));
		}
		//    INFORM USER OF THE PIPE SWAP
		// COB_CODE: SET L-CI-EI-ES-WARNING
		//               L-CI-EI-ET-REGION-SWAPPED
		//                                       TO TRUE.
		lCicsPipeInterfaceContract.setEiEsWarning();
		lCicsPipeInterfaceContract.setEiEtRegionSwapped();
		// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP-ORIG)
		//                                       TO EA-14-ORIG-REGION.
		ws.getErrorAndAdviceMessages().getEa14RegionSwapped().setOrigRegion(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getCicsApplId());
		// COB_CODE: MOVE TP-PI-CICS-APPL-ID (SS-TP-REDIR)
		//                                       TO EA-14-REDIR-REGION.
		ws.getErrorAndAdviceMessages().getEa14RegionSwapped().setRedirRegion(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getCicsApplId());
		// COB_CODE: MOVE EA-14-REGION-SWAPPED   TO L-CI-EI-MESSAGE.
		lCicsPipeInterfaceContract.setEiMessage(ws.getErrorAndAdviceMessages().getEa14RegionSwapped().getEa14RegionSwappedFormatted());
		//    SET THE REDIRECTED REGION AS THE CURRENT REGION TO USE
		// COB_CODE: MOVE SS-TP-REDIR            TO SS-TP.
		ws.getSubscripts().setTp(ws.getSubscripts().getTpRedir());
	}

	/**Original name: 9410-REDIR-TO-NEW-PIPE<br>
	 * <pre>***************************************************************
	 *   HANDLE A PIPE REDIRECTION WHERE A PIPE WITH NO CURRENT
	 *   INFORMATION EXISTS.
	 * ***************************************************************
	 *     FIND THE FIRST AVAILABLE SPOT IN THE TABLE.  NOTE THAT
	 *       SS-TP WILL BE UPDATED IN THIS CALL.</pre>*/
	private void redirToNewPipe() {
		// COB_CODE: PERFORM 9220-FIND-AVAIL-POS-IN-TBL
		//              THRU 9220-EXIT.
		findAvailPosInTbl();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 9410-EXIT
		//           END-IF.
		if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 9410-EXIT
			return;
		}
		//    SET THE REQUESTED PIPE AS BEING REDIRECTED TO THE NEW PIPE
		// COB_CODE: MOVE SS-TP                  TO SS-TP-REDIR.
		ws.getSubscripts().setTpRedir(ws.getSubscripts().getTp());
		// COB_CODE: MOVE SS-TP-REDIR            TO TP-PI-RI-REDIR-PIPE
		//                                                           (SS-TP-ORIG).
		ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().setRiRedirPipe(ws.getSubscripts().getTpRedir());
		//    CREATE THE NEW PIPE INFORMATION FOR THE REDIRECTION PIPE
		// COB_CODE: MOVE CM-CICS-APPL-ID        TO TP-PI-CICS-APPL-ID
		//                                                          (SS-TP-REDIR).
		ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).setCicsApplId(ws.getCicsPipeMaintenanceCnt().getCicsApplId().getCicsApplId());
		// COB_CODE: MOVE CM-PI-PIPE-TOKEN       TO TP-PI-PIPE-TOKEN
		//                                                          (SS-TP-REDIR).
		ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).setPipeToken(ws.getCicsPipeMaintenanceCnt().getPiPipeToken());
		// COB_CODE: MOVE CM-PI-USER-TOKEN       TO TP-PI-USER-TOKEN
		//                                                          (SS-TP-REDIR).
		ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).setUserToken(ws.getCicsPipeMaintenanceCnt().getPiUserToken());
		// COB_CODE: ADD +1                      TO AA-OPEN-PIPES.
		ws.setAaOpenPipes(Trunc.toShort(1 + ws.getAaOpenPipes(), 4));
		//    IF THIS IS A REDIRECTION FROM OPENING A PIPE, SET THIS AS THE
		//      FIRST REDIRECTION FROM THE ORIGINAL PIPE.  OTHERWISE,
		//      UPDATE THE OPEN/CLOSE TALLY WITH THE INFORMATION FROM THE
		//      ORIGINAL PIPE.
		// COB_CODE:      IF L-CI-FN-OPEN-PIPE
		//                                                               (SS-TP-REDIR)
		//                ELSE
		//           *        DON'T UPDATE THE NUMBER OF REDIRECTS IF A VALUE ALREADY
		//           *          EXISTS.  THIS CAN HAPPEN IF THE ORIGINAL PIPE WAS
		//           *          WAS BEING REDIRECTED AND IS NOW BEING REDIRECTED
		//           *          AGAIN (SEE PARAGRAPH 4223).
		//                    END-IF
		//                END-IF.
		if (lCicsPipeInterfaceContract.isFnOpenPipe()) {
			// COB_CODE: MOVE +1                 TO TP-PI-RI-NBR-REDIRECTS
			//                                                       (SS-TP-ORIG)
			//                                      TP-PI-OPEN-CLOSE-TALLY
			//                                                      (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).setRiNbrRedirects(((short) 1));
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().setOpenCloseTally(((short) 1));
		} else if (ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects() == 0) {
			//        DON'T UPDATE THE NUMBER OF REDIRECTS IF A VALUE ALREADY
			//          EXISTS.  THIS CAN HAPPEN IF THE ORIGINAL PIPE WAS
			//          WAS BEING REDIRECTED AND IS NOW BEING REDIRECTED
			//          AGAIN (SEE PARAGRAPH 4223).
			// COB_CODE: IF TP-PI-RI-NBR-REDIRECTS (SS-TP-ORIG) = +0
			//                                                      (SS-TP-REDIR)
			//           ELSE
			//                                                      (SS-TP-REDIR)
			//           END-IF
			// COB_CODE: MOVE TP-PI-OPEN-CLOSE-TALLY (SS-TP-ORIG)
			//                               TO TP-PI-RI-NBR-REDIRECTS
			//                                                   (SS-TP-ORIG)
			//                                  TP-PI-OPEN-CLOSE-TALLY
			//                                                  (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpOrig())
					.setRiNbrRedirects(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getOpenCloseTally().getOpenCloseTally());
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally()
					.setOpenCloseTally(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getOpenCloseTally().getOpenCloseTally());
		} else {
			// COB_CODE: ADD TP-PI-RI-NBR-REDIRECTS (SS-TP-ORIG)
			//                               TO TP-PI-OPEN-CLOSE-TALLY
			//                                                  (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally()
					.setOpenCloseTally(Trunc.toShort(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects()
							+ ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().getOpenCloseTally(), 4));
		}
	}

	/**Original name: 9420-REDIR-TO-EXISTING<br>
	 * <pre>***************************************************************
	 *   HANDLE A PIPE REDIRECTION WHERE THE REDIRECTED PIPE ALREADY
	 *   EXISTS.
	 * ***************************************************************
	 *     SINCE WE ALREADY HAVE AN EXISTING PIPE, CLOSE THE DUPLICATE
	 *       IF THE EXISTING PIPE IS ALREADY OPEN.  IF CLOSED, USE
	 *       THE CURRENT TOKEN INFORMATION CAPTURED.  INCREMENT THE
	 *       NUMBER OF OPEN PIPES WHEN APPROPRIATE.
	 *     SS-TP-TEMP REPRESENTS THE LOCATION OF THE EXISTING PIPE. THIS
	 *       VALUE WAS FOUND IN PARAGRAPH 9210.</pre>*/
	private void redirToExisting() {
		// COB_CODE: MOVE SS-TP-TEMP             TO SS-TP-REDIR.
		ws.getSubscripts().setTpRedir(ws.getSubscripts().getTpTemp());
		// COB_CODE: IF TP-PI-PIPE-IS-CLOSED (SS-TP-REDIR)
		//               ADD +1                  TO AA-OPEN-PIPES
		//           ELSE
		//               END-IF
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().isPipeIsClosed()) {
			// COB_CODE: MOVE CM-PI-PIPE-TOKEN   TO TP-PI-PIPE-TOKEN (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).setPipeToken(ws.getCicsPipeMaintenanceCnt().getPiPipeToken());
			// COB_CODE: MOVE CM-PI-USER-TOKEN   TO TP-PI-USER-TOKEN (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).setUserToken(ws.getCicsPipeMaintenanceCnt().getPiUserToken());
			// COB_CODE: ADD +1                  TO AA-OPEN-PIPES
			ws.setAaOpenPipes(Trunc.toShort(1 + ws.getAaOpenPipes(), 4));
		} else {
			// COB_CODE: SET CM-FN-CLOSE-PIPE    TO TRUE
			ws.getCicsPipeMaintenanceCnt().getFunction().setCmFnClosePipe();
			// COB_CODE: PERFORM 9300-CALL-PIPE-MTN-COMMON-RTN
			//              THRU 9300-EXIT
			callPipeMtnCommonRtn();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 9420-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 9420-EXIT
				return;
			}
		}
		//    SET THE REQUESTED PIPE AS BEING REDIRECTED TO THE NEW PIPE
		// COB_CODE: MOVE SS-TP-REDIR            TO TP-PI-RI-REDIR-PIPE
		//                                                           (SS-TP-ORIG).
		ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiRedirPipe().setRiRedirPipe(ws.getSubscripts().getTpRedir());
		//    IF THIS IS A REDIRECTION FROM OPENING A PIPE, INCREMENT THE
		//      NUMBER OF OPENS IN THE OPEN/CLOSE TALLY.  OTHERWISE,
		//      UPDATE THE OPEN/CLOSE TALLY WITH THE INFORMATION FROM THE
		//      ORIGINAL PIPE.
		// COB_CODE:      IF L-CI-FN-OPEN-PIPE
		//                                                               (SS-TP-REDIR)
		//                ELSE
		//           *        DON'T UPDATE THE NUMBER OF REDIRECTS IF A VALUE ALREADY
		//           *          EXISTS.  THIS CAN HAPPEN IF THE ORIGINAL PIPE WAS
		//           *          WAS BEING REDIRECTED AND IS NOW BEING REDIRECTED
		//           *          AGAIN (SEE PARAGRAPH 4223).
		//                    END-IF
		//                END-IF.
		if (lCicsPipeInterfaceContract.isFnOpenPipe()) {
			// COB_CODE: MOVE +1                 TO TP-PI-RI-NBR-REDIRECTS
			//                                                       (SS-TP-ORIG)
			ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).setRiNbrRedirects(((short) 1));
			// COB_CODE: ADD +1                  TO TP-PI-OPEN-CLOSE-TALLY
			//                                                      (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().setOpenCloseTally(
					Trunc.toShort(1 + ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().getOpenCloseTally(), 4));
		} else if (ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects() == 0) {
			//        DON'T UPDATE THE NUMBER OF REDIRECTS IF A VALUE ALREADY
			//          EXISTS.  THIS CAN HAPPEN IF THE ORIGINAL PIPE WAS
			//          WAS BEING REDIRECTED AND IS NOW BEING REDIRECTED
			//          AGAIN (SEE PARAGRAPH 4223).
			// COB_CODE: IF TP-PI-RI-NBR-REDIRECTS (SS-TP-ORIG) = +0
			//                                                      (SS-TP-REDIR)
			//           ELSE
			//                                                      (SS-TP-REDIR)
			//           END-IF
			// COB_CODE: MOVE TP-PI-OPEN-CLOSE-TALLY (SS-TP-ORIG)
			//                               TO TP-PI-RI-NBR-REDIRECTS
			//                                                   (SS-TP-ORIG)
			ws.getTpPipeInf(ws.getSubscripts().getTpOrig())
					.setRiNbrRedirects(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getOpenCloseTally().getOpenCloseTally());
			// COB_CODE: ADD TP-PI-OPEN-CLOSE-TALLY (SS-TP-ORIG)
			//                               TO TP-PI-OPEN-CLOSE-TALLY
			//                                                  (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally()
					.setOpenCloseTally(Trunc.toShort(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getOpenCloseTally().getOpenCloseTally()
							+ ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().getOpenCloseTally(), 4));
		} else {
			// COB_CODE: ADD TP-PI-RI-NBR-REDIRECTS (SS-TP-ORIG)
			//                               TO TP-PI-OPEN-CLOSE-TALLY
			//                                                  (SS-TP-REDIR)
			ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally()
					.setOpenCloseTally(Trunc.toShort(ws.getTpPipeInf(ws.getSubscripts().getTpOrig()).getRiNbrRedirects()
							+ ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().getOpenCloseTally(), 4));
		}
		// COB_CODE: IF TP-PI-RI-PIPE-IS-REDIRECTED (SS-TP-REDIR)
		//               END-IF
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getRiRedirPipe().isIsRedirected()) {
			// COB_CODE: PERFORM 9421-CLOSE-REDIR-PIPE-REDIR
			//              THRU 9421-EXIT
			closeRedirPipeRedir();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 9420-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 9420-EXIT
				return;
			}
		}
	}

	/**Original name: 9421-CLOSE-REDIR-PIPE-REDIR<br>
	 * <pre>***************************************************************
	 *   THE PIPE WE ARE REDIRECTING TO IS SET UP TO BE REDIRECTED.
	 *   CLOSE THAT PIPE AND ENSURE THE REDIRECTING PIPE IS NOT ALSO
	 *   REDIRECTED.
	 * ***************************************************************
	 *     SINCE THE PIPE IS NOW OPEN, WE WILL NOT WANT IT TO BE
	 *       REDIRECTED AS WELL.
	 *     SS-TP-TEMP REPRESENTS THE REDIRECTED PIPE FROM OUR REDIRECTED
	 *       PIPE.  WE WISH TO LONGER HAVE A REDIRECTION ON OUR
	 *       REDIRECTED PIPE.</pre>*/
	private void closeRedirPipeRedir() {
		// COB_CODE: MOVE TP-PI-RI-REDIR-PIPE (SS-TP-REDIR)
		//                                       TO SS-TP-TEMP.
		ws.getSubscripts().setTpTemp(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getRiRedirPipe().getRiRedirPipe());
		// COB_CODE: SUBTRACT TP-PI-RI-NBR-REDIRECTS (SS-TP-REDIR)
		//                                       FROM TP-PI-OPEN-CLOSE-TALLY
		//                                                           (SS-TP-TEMP).
		ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).getOpenCloseTally()
				.setOpenCloseTally(Trunc.toShort(ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).getOpenCloseTally().getOpenCloseTally()
						- ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getRiNbrRedirects(), 4));
		// COB_CODE: IF TP-PI-KEEP-PIPE-OPEN (SS-TP-TEMP)
		//               CONTINUE
		//           ELSE
		//               SUBTRACT +1             FROM AA-OPEN-PIPES
		//           END-IF.
		if (ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).getOpenCloseTally().isKeepPipeOpen()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET CM-FN-CLOSE-PIPE    TO TRUE
			ws.getCicsPipeMaintenanceCnt().getFunction().setCmFnClosePipe();
			// COB_CODE: MOVE TP-PI-PIPE-TOKEN (SS-TP-TEMP)
			//                                   TO CM-PI-PIPE-TOKEN
			ws.getCicsPipeMaintenanceCnt().setPiPipeToken(ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).getPipeToken());
			// COB_CODE: MOVE TP-PI-USER-TOKEN (SS-TP-TEMP)
			//                                   TO CM-PI-USER-TOKEN
			ws.getCicsPipeMaintenanceCnt().setPiUserToken(ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).getUserToken());
			// COB_CODE: PERFORM 9300-CALL-PIPE-MTN-COMMON-RTN
			//              THRU 9300-EXIT
			callPipeMtnCommonRtn();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 9421-EXIT
			//           END-IF
			if (ws.getSwitches().getErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 9421-EXIT
				return;
			}
			// COB_CODE: MOVE +0                 TO TP-PI-USER-TOKEN (SS-TP-TEMP)
			//                                      TP-PI-PIPE-TOKEN (SS-TP-TEMP)
			ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).setUserToken(0);
			ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).setPipeToken(0);
			// COB_CODE: SET TP-PI-PIPE-IS-CLOSED (SS-TP-TEMP)
			//               TP-PI-PIPE-STA-OK (SS-TP-TEMP)
			//                                   TO TRUE
			ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).getOpenCloseTally().setPipeIsClosed();
			ws.getTpPipeInf(ws.getSubscripts().getTpTemp()).getPipeStaCd().setOk();
			// COB_CODE: INITIALIZE TP-PI-REDIRECTION-INF (SS-TP-TEMP)
			initRedirectionInf();
			// COB_CODE: SUBTRACT +1             FROM AA-OPEN-PIPES
			ws.setAaOpenPipes(Trunc.toShort(ws.getAaOpenPipes() - 1, 4));
		}
		//    CLEAR THE REDIRECTION INFO FROM THE PIPE WE WILL BE NOW USING
		// COB_CODE: ADD TP-PI-RI-NBR-REDIRECTS (SS-TP-REDIR)
		//                                       TO TP-PI-OPEN-CLOSE-TALLY
		//                                                          (SS-TP-REDIR).
		ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally()
				.setOpenCloseTally(Trunc.toShort(ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getRiNbrRedirects()
						+ ws.getTpPipeInf(ws.getSubscripts().getTpRedir()).getOpenCloseTally().getOpenCloseTally(), 4));
		// COB_CODE: INITIALIZE TP-PI-REDIRECTION-INF (SS-TP-REDIR).
		initRedirectionInf();
	}

	/**Original name: RNG_4000-CALL-CICS-USING-PIPE-_-4000-EXIT<br>*/
	private void rng4000CallCicsUsingPipe() {
		String retcode = "";
		boolean goto4000A = false;
		boolean goto4000Exit = false;
		retcode = callCicsUsingPipe();
		if (!retcode.equals("4000-EXIT")) {
			do {
				goto4000A = false;
				retcode = a();
			} while (retcode.equals("4000-A"));
		}
		goto4000Exit = false;
	}

	/**Original name: RNG_9210-FIND-PIPE-IN-TBL-_-9210-EXIT<br>*/
	private void rng9210FindPipeInTbl() {
		String retcode = "";
		boolean goto9210A = false;
		findPipeInTbl();
		do {
			goto9210A = false;
			retcode = a1();
		} while (retcode.equals("9210-A"));
	}

	public void initTableOfPipes() {
		for (int idx0 = 1; idx0 <= Ts547099Data.TP_PIPE_INF_MAXOCCURS; idx0++) {
			ws.getTpPipeInf(idx0).setCicsApplId("");
			ws.getTpPipeInf(idx0).setUserToken(0);
			ws.getTpPipeInf(idx0).setPipeToken(0);
			ws.getTpPipeInf(idx0).getOpenCloseTally().setOpenCloseTally(((short) 0));
			ws.getTpPipeInf(idx0).getPipeStaCd().setPipeStaCdFormatted("0");
			ws.getTpPipeInf(idx0).getRiRedirPipe().setRiRedirPipe(((short) 0));
			ws.getTpPipeInf(idx0).setRiNbrRedirects(((short) 0));
		}
	}

	public void initErrorInformation() {
		lCicsPipeInterfaceContract.setEiErrorSeverity(Types.SPACE_CHAR);
		lCicsPipeInterfaceContract.setEiErrorType(Types.SPACE_CHAR);
		lCicsPipeInterfaceContract.setEiMessage("");
		lCicsPipeInterfaceContract.setEiExciResp(0);
		lCicsPipeInterfaceContract.setEiExciResp2(0);
		lCicsPipeInterfaceContract.setEiExciResp3(0);
		lCicsPipeInterfaceContract.setEiDplResp(0);
		lCicsPipeInterfaceContract.setEiDplResp2(0);
	}

	public void initRedirectionInf() {
		ws.getTpPipeInf(ws.getSubscripts().getTp()).getRiRedirPipe().setRiRedirPipe(((short) 0));
		ws.getTpPipeInf(ws.getSubscripts().getTp()).setRiNbrRedirects(((short) 0));
	}

	public void initCicsPipeMaintenanceCnt() {
		ws.getCicsPipeMaintenanceCnt().getFunction().setFunction(Types.SPACE_CHAR);
		ws.getCicsPipeMaintenanceCnt().getCicsApplId().setCicsApplId("");
		ws.getCicsPipeMaintenanceCnt().setPiUserToken(0);
		ws.getCicsPipeMaintenanceCnt().setPiPipeToken(0);
		ws.getCicsPipeMaintenanceCnt().getErrorInformation().getErrorSeverity().setErrorSeverity(Types.SPACE_CHAR);
		ws.getCicsPipeMaintenanceCnt().getErrorInformation().getErrorType().setErrorType(Types.SPACE_CHAR);
		ws.getCicsPipeMaintenanceCnt().getErrorInformation().setMessage("");
		ws.getCicsPipeMaintenanceCnt().getErrorInformation().setExciResp(0);
		ws.getCicsPipeMaintenanceCnt().getErrorInformation().setExciResp2(0);
		ws.getCicsPipeMaintenanceCnt().getErrorInformation().setExciResp3(0);
		ws.getCicsPipeMaintenanceCnt().getErrorInformation().setDplResp(0);
		ws.getCicsPipeMaintenanceCnt().getErrorInformation().setDplResp2(0);
	}
}
