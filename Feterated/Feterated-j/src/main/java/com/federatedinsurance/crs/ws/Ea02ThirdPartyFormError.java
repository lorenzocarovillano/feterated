/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-THIRD-PARTY-FORM-ERROR<br>
 * Variable: EA-02-THIRD-PARTY-FORM-ERROR from program XZ0P90E0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02ThirdPartyFormError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-THIRD-PARTY-FORM-ERROR
	private String flr1 = "Third parties";
	//Original name: FILLER-EA-02-THIRD-PARTY-FORM-ERROR-1
	private String flr2 = "exist on the";
	//Original name: FILLER-EA-02-THIRD-PARTY-FORM-ERROR-2
	private String flr3 = "notice that did";
	//Original name: FILLER-EA-02-THIRD-PARTY-FORM-ERROR-3
	private String flr4 = " not get any";
	//Original name: FILLER-EA-02-THIRD-PARTY-FORM-ERROR-4
	private String flr5 = " forms attached";
	//Original name: FILLER-EA-02-THIRD-PARTY-FORM-ERROR-5
	private String flr6 = ". Account =";
	//Original name: EA-02-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-02-THIRD-PARTY-FORM-ERROR-6
	private String flr7 = "; Notification";
	//Original name: FILLER-EA-02-THIRD-PARTY-FORM-ERROR-7
	private String flr8 = "Timestamp =";
	//Original name: EA-02-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-02-THIRD-PARTY-FORM-ERROR-8
	private char flr9 = '.';

	//==== METHODS ====
	public String getEa02ThirdPartyFormErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa02ThirdPartyFormErrorBytes());
	}

	public byte[] getEa02ThirdPartyFormErrorBytes() {
		byte[] buffer = new byte[Len.EA02_THIRD_PARTY_FORM_ERROR];
		return getEa02ThirdPartyFormErrorBytes(buffer, 1);
	}

	public byte[] getEa02ThirdPartyFormErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr9);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr9() {
		return this.flr9;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 14;
		public static final int FLR2 = 13;
		public static final int FLR3 = 15;
		public static final int FLR4 = 12;
		public static final int FLR9 = 1;
		public static final int EA02_THIRD_PARTY_FORM_ERROR = CSR_ACT_NBR + NOT_PRC_TS + FLR1 + FLR2 + 3 * FLR3 + 3 * FLR4 + FLR9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
