/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P90C0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p90c0 {

	//==== PROPERTIES ====
	//Original name: WS-SPACE-COUNT
	private short spaceCount = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-CER-NBR-LEN
	private short cerNbrLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-EIBRESP-CD
	private short eibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-CER-HLD-NM
	private String cerHldNm = DefaultValues.stringVal(Len.CER_HLD_NM);
	//Original name: WS-CER-HLD-ADR1
	private String cerHldAdr1 = DefaultValues.stringVal(Len.CER_HLD_ADR1);
	//Original name: WS-CER-HLD-ADR2
	private String cerHldAdr2 = DefaultValues.stringVal(Len.CER_HLD_ADR2);
	//Original name: WS-CER-HLD-CIT
	private String cerHldCit = DefaultValues.stringVal(Len.CER_HLD_CIT);
	//Original name: WS-CER-HLD-ST-ABB
	private String cerHldStAbb = DefaultValues.stringVal(Len.CER_HLD_ST_ABB);
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-SORT-FIELDS
	private WsSortFields sortFields = new WsSortFields();
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0P90C0";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-CERT-LIST
	private String busObjNmCertList = "XZ_PREPARE_CERT_HOLDER_LIST";
	//Original name: WS-ADD-ACT-NOT-REC
	private String addActNotRec = "AddAccountNotificationRecipient";

	//==== METHODS ====
	public void setSpaceCount(short spaceCount) {
		this.spaceCount = spaceCount;
	}

	public short getSpaceCount() {
		return this.spaceCount;
	}

	public void setCerNbrLen(short cerNbrLen) {
		this.cerNbrLen = cerNbrLen;
	}

	public short getCerNbrLen() {
		return this.cerNbrLen;
	}

	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd = eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public String getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public String getEibrespCdAsString() {
		return getEibrespCdFormatted();
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd = eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public String getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public String getEibresp2CdAsString() {
		return getEibresp2CdFormatted();
	}

	public void setCerHldNm(String cerHldNm) {
		this.cerHldNm = Functions.subString(cerHldNm, Len.CER_HLD_NM);
	}

	public void setCerHldNmSubstring(String replacement, int start, int length) {
		cerHldNm = Functions.setSubstring(cerHldNm, replacement, start, length);
	}

	public String getCerHldNm() {
		return this.cerHldNm;
	}

	public String getCerHldNmFormatted() {
		return Functions.padBlanks(getCerHldNm(), Len.CER_HLD_NM);
	}

	public void setCerHldAdr1(String cerHldAdr1) {
		this.cerHldAdr1 = Functions.subString(cerHldAdr1, Len.CER_HLD_ADR1);
	}

	public String getCerHldAdr1() {
		return this.cerHldAdr1;
	}

	public void setCerHldAdr2(String cerHldAdr2) {
		this.cerHldAdr2 = Functions.subString(cerHldAdr2, Len.CER_HLD_ADR2);
	}

	public String getCerHldAdr2() {
		return this.cerHldAdr2;
	}

	public void setCerHldCit(String cerHldCit) {
		this.cerHldCit = Functions.subString(cerHldCit, Len.CER_HLD_CIT);
	}

	public String getCerHldCit() {
		return this.cerHldCit;
	}

	public void setCerHldStAbb(String cerHldStAbb) {
		this.cerHldStAbb = Functions.subString(cerHldStAbb, Len.CER_HLD_ST_ABB);
	}

	public String getCerHldStAbb() {
		return this.cerHldStAbb;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNmCertList() {
		return this.busObjNmCertList;
	}

	public String getAddActNotRec() {
		return this.addActNotRec;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	public WsSortFields getSortFields() {
		return sortFields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CER_HLD_NM = 120;
		public static final int CER_HLD_ADR1 = 45;
		public static final int CER_HLD_ADR2 = 45;
		public static final int CER_HLD_CIT = 30;
		public static final int CER_HLD_ST_ABB = 2;
		public static final int PROGRAM_NAME = 8;
		public static final int EIBRESP_CD = 4;
		public static final int EIBRESP2_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
