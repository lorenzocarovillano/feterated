/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.federatedinsurance.crs.ws.occurs.TaReportData;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS030099<br>
 * Generated as a class for rule WS.<br>*/
public class Ts030099Data {

	//==== PROPERTIES ====
	public static final int TA_REPORT_DATA_MAXOCCURS = 500;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsTs030099 constantFields = new ConstantFieldsTs030099();
	//Original name: CALLING-PGM-IS-TS030097-IND
	private CallingPgmIsTs030097Ind callingPgmIsTs030097Ind;
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesTs030099 errorAndAdviceMessages = new ErrorAndAdviceMessagesTs030099();
	//Original name: REPORT-DB-PARAMETERS
	private LReportDbParameters reportDbParameters = new LReportDbParameters();
	//Original name: REPORT-INFORMATION
	private ReportInformation reportInformation = new ReportInformation();
	//Original name: SAVE-AREA
	private SaveAreaTs030099 saveArea = new SaveAreaTs030099();
	//Original name: SS-RI
	private short ssRi = DefaultValues.BIN_SHORT_VAL;
	public static final short SS_RI_END_OF_TABLE = ((short) 16);
	//Original name: SS-TA
	private short ssTa = DefaultValues.BIN_SHORT_VAL;
	public static final short SS_TA_END_OF_TABLE = ((short) 501);
	//Original name: SWITCHES
	private SwitchesTs030099 switches = new SwitchesTs030099();
	//Original name: TA-REPORT-DATA
	private LazyArrayCopy<TaReportData> taReportData = new LazyArrayCopy<>(new TaReportData(), 1, TA_REPORT_DATA_MAXOCCURS);

	//==== CONSTRUCTORS ====
	public Ts030099Data() {
		callingPgmIsTs030097Ind = Session.current().getShared(CallingPgmIsTs030097Ind.class);
	}

	//==== METHODS ====
	public void setSsRi(short ssRi) {
		this.ssRi = ssRi;
	}

	public short getSsRi() {
		return this.ssRi;
	}

	public boolean isSsRiEndOfTable() {
		return ssRi == SS_RI_END_OF_TABLE;
	}

	public void setSsTa(short ssTa) {
		this.ssTa = ssTa;
	}

	public short getSsTa() {
		return this.ssTa;
	}

	public boolean isSsTaEndOfTable() {
		return ssTa == SS_TA_END_OF_TABLE;
	}

	public CallingPgmIsTs030097Ind getCallingPgmIsTs030097Ind() {
		return callingPgmIsTs030097Ind;
	}

	public ConstantFieldsTs030099 getConstantFields() {
		return constantFields;
	}

	public ErrorAndAdviceMessagesTs030099 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public LReportDbParameters getReportDbParameters() {
		return reportDbParameters;
	}

	public ReportInformation getReportInformation() {
		return reportInformation;
	}

	public SaveAreaTs030099 getSaveArea() {
		return saveArea;
	}

	public SwitchesTs030099 getSwitches() {
		return switches;
	}

	public TaReportData getTaReportData(int idx) {
		return taReportData.get(idx - 1);
	}
}
