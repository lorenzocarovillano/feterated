/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHolidayPrc;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [HOLIDAY_PRC]
 * 
 */
public class HolidayPrcDao extends BaseSqlDao<IHolidayPrc> {

	public HolidayPrcDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHolidayPrc> getToClass() {
		return IHolidayPrc.class;
	}

	public IHolidayPrc selectByHolidayDt(String holidayDt, IHolidayPrc iHolidayPrc) {
		return buildQuery("selectByHolidayDt").bind("holidayDt", holidayDt).singleResult(iHolidayPrc);
	}

	public IHolidayPrc selectByHolidayDt1(String holidayDt, IHolidayPrc iHolidayPrc) {
		return buildQuery("selectByHolidayDt1").bind("holidayDt", holidayDt).singleResult(iHolidayPrc);
	}
}
