/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-02-FETCH-ERROR-MSG<br>
 * Variable: EA-02-FETCH-ERROR-MSG from program TS030299<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea02FetchErrorMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-FETCH-ERROR-MSG
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-02-FETCH-ERROR-MSG-1
	private String flr2 = "TS030299 -";
	//Original name: FILLER-EA-02-FETCH-ERROR-MSG-2
	private String flr3 = "FETCH ERROR;";
	//Original name: FILLER-EA-02-FETCH-ERROR-MSG-3
	private String flr4 = "SQLCODE =";
	//Original name: EA-02-SQLCODE
	private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);
	//Original name: FILLER-EA-02-FETCH-ERROR-MSG-4
	private String flr5 = ", ERROR MSG =";
	//Original name: EA-02-ERROR-MSG
	private String errorMsg = DefaultValues.stringVal(Len.ERROR_MSG);

	//==== METHODS ====
	public String getEa02FetchErrorMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02FetchErrorMsgBytes());
	}

	public byte[] getEa02FetchErrorMsgBytes() {
		byte[] buffer = new byte[Len.EA02_FETCH_ERROR_MSG];
		return getEa02FetchErrorMsgBytes(buffer, 1);
	}

	public byte[] getEa02FetchErrorMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
		position += Len.SQLCODE;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, errorMsg, Len.ERROR_MSG);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setSqlcode(long sqlcode) {
		this.sqlcode = PicFormatter.display("-9(5)").format(sqlcode).toString();
	}

	public long getSqlcode() {
		return PicParser.display("-9(5)").parseLong(this.sqlcode);
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = Functions.subString(errorMsg, Len.ERROR_MSG);
	}

	public String getErrorMsg() {
		return this.errorMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SQLCODE = 6;
		public static final int ERROR_MSG = 70;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 13;
		public static final int FLR4 = 10;
		public static final int FLR5 = 14;
		public static final int EA02_FETCH_ERROR_MSG = SQLCODE + ERROR_MSG + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
