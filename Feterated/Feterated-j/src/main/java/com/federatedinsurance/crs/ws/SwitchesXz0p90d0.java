/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ0P90D0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz0p90d0 {

	//==== PROPERTIES ====
	//Original name: SW-POL-FOUND-FLAG
	private char polFoundFlag = 'Y';
	public static final char NO_POL_FOUND = 'N';
	//Original name: SW-GET-POL-LST-ALC-FLAG
	private char getPolLstAlcFlag = 'N';
	public static final char GET_POL_LST_ALC = 'Y';
	//Original name: SW-UPD-NOT-STA-ALC-FLAG
	private char updNotStaAlcFlag = 'N';
	public static final char UPD_NOT_STA_ALC = 'Y';

	//==== METHODS ====
	public void setPolFoundFlag(char polFoundFlag) {
		this.polFoundFlag = polFoundFlag;
	}

	public char getPolFoundFlag() {
		return this.polFoundFlag;
	}

	public boolean isNoPolFound() {
		return polFoundFlag == NO_POL_FOUND;
	}

	public void setNoPolFound() {
		polFoundFlag = NO_POL_FOUND;
	}

	public void setGetPolLstAlcFlag(char getPolLstAlcFlag) {
		this.getPolLstAlcFlag = getPolLstAlcFlag;
	}

	public char getGetPolLstAlcFlag() {
		return this.getPolLstAlcFlag;
	}

	public boolean isGetPolLstAlc() {
		return getPolLstAlcFlag == GET_POL_LST_ALC;
	}

	public void setGetPolLstAlc() {
		getPolLstAlcFlag = GET_POL_LST_ALC;
	}

	public void setUpdNotStaAlcFlag(char updNotStaAlcFlag) {
		this.updNotStaAlcFlag = updNotStaAlcFlag;
	}

	public char getUpdNotStaAlcFlag() {
		return this.updNotStaAlcFlag;
	}

	public boolean isUpdNotStaAlc() {
		return updNotStaAlcFlag == UPD_NOT_STA_ALC;
	}

	public void setUpdNotStaAlc() {
		updNotStaAlcFlag = UPD_NOT_STA_ALC;
	}
}
