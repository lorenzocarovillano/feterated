/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TT-THIRD-PARTIES<br>
 * Variable: TT-THIRD-PARTIES from program XZ0B9081<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TtThirdPartiesXz0b9081 implements ICopyable<TtThirdPartiesXz0b9081> {

	//==== PROPERTIES ====
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TT_THIRD_PARTIES);
	//Original name: TT-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: TT-ADR-ID
	private String adrId = DefaultValues.stringVal(Len.ADR_ID);
	//Original name: TT-NAME
	private String name = DefaultValues.stringVal(Len.NAME);
	//Original name: TT-ADR-LIN1
	private String adrLin1 = DefaultValues.stringVal(Len.ADR_LIN1);
	//Original name: TT-ADR-LIN2
	private String adrLin2 = DefaultValues.stringVal(Len.ADR_LIN2);
	//Original name: TT-CITY-NM
	private String cityNm = DefaultValues.stringVal(Len.CITY_NM);
	//Original name: TT-STATE-ABB
	private String stateAbb = DefaultValues.stringVal(Len.STATE_ABB);
	//Original name: TT-PST-CD
	private String pstCd = DefaultValues.stringVal(Len.PST_CD);
	//Original name: TT-CER-NBR
	private String cerNbr = DefaultValues.stringVal(Len.CER_NBR);

	//==== CONSTRUCTORS ====
	public TtThirdPartiesXz0b9081() {
	}

	public TtThirdPartiesXz0b9081(TtThirdPartiesXz0b9081 ttThirdParties) {
		this();
		this.clientId = ttThirdParties.clientId;
		this.adrId = ttThirdParties.adrId;
		this.name = ttThirdParties.name;
		this.adrLin1 = ttThirdParties.adrLin1;
		this.adrLin2 = ttThirdParties.adrLin2;
		this.cityNm = ttThirdParties.cityNm;
		this.stateAbb = ttThirdParties.stateAbb;
		this.pstCd = ttThirdParties.pstCd;
		this.cerNbr = ttThirdParties.cerNbr;
	}

	//==== METHODS ====
	public String getTtThirdPartiesFormatted() {
		return MarshalByteExt.bufferToStr(getTtThirdPartiesBytes());
	}

	public byte[] getTtThirdPartiesBytes() {
		byte[] buffer = new byte[Len.TT_THIRD_PARTIES];
		return getTtThirdPartiesBytes(buffer, 1);
	}

	public byte[] getTtThirdPartiesBytes(byte[] buffer, int offset) {
		int position = offset;
		getKeyFieldsBytes(buffer, position);
		position += Len.KEY_FIELDS;
		MarshalByte.writeString(buffer, position, name, Len.NAME);
		position += Len.NAME;
		MarshalByte.writeString(buffer, position, adrLin1, Len.ADR_LIN1);
		position += Len.ADR_LIN1;
		MarshalByte.writeString(buffer, position, adrLin2, Len.ADR_LIN2);
		position += Len.ADR_LIN2;
		MarshalByte.writeString(buffer, position, cityNm, Len.CITY_NM);
		position += Len.CITY_NM;
		MarshalByte.writeString(buffer, position, stateAbb, Len.STATE_ABB);
		position += Len.STATE_ABB;
		MarshalByte.writeString(buffer, position, pstCd, Len.PST_CD);
		position += Len.PST_CD;
		MarshalByte.writeString(buffer, position, cerNbr, Len.CER_NBR);
		return buffer;
	}

	public TtThirdPartiesXz0b9081 initTtThirdPartiesHighValues() {
		initKeyFieldsHighValues();
		name = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NAME);
		adrLin1 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ADR_LIN1);
		adrLin2 = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ADR_LIN2);
		cityNm = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CITY_NM);
		stateAbb = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.STATE_ABB);
		pstCd = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.PST_CD);
		cerNbr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CER_NBR);
		return this;
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTtThirdPartiesFormatted()).equals(END_OF_TABLE);
	}

	public byte[] getKeyFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeString(buffer, position, adrId, Len.ADR_ID);
		return buffer;
	}

	public void initKeyFieldsHighValues() {
		clientId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CLIENT_ID);
		adrId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ADR_ID);
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setAdrId(String adrId) {
		this.adrId = Functions.subString(adrId, Len.ADR_ID);
	}

	public String getAdrId() {
		return this.adrId;
	}

	public void setName(String name) {
		this.name = Functions.subString(name, Len.NAME);
	}

	public String getName() {
		return this.name;
	}

	public void setAdrLin1(String adrLin1) {
		this.adrLin1 = Functions.subString(adrLin1, Len.ADR_LIN1);
	}

	public String getAdrLin1() {
		return this.adrLin1;
	}

	public void setAdrLin2(String adrLin2) {
		this.adrLin2 = Functions.subString(adrLin2, Len.ADR_LIN2);
	}

	public String getAdrLin2() {
		return this.adrLin2;
	}

	public void setCityNm(String cityNm) {
		this.cityNm = Functions.subString(cityNm, Len.CITY_NM);
	}

	public String getCityNm() {
		return this.cityNm;
	}

	public void setStateAbb(String stateAbb) {
		this.stateAbb = Functions.subString(stateAbb, Len.STATE_ABB);
	}

	public String getStateAbb() {
		return this.stateAbb;
	}

	public void setPstCd(String pstCd) {
		this.pstCd = Functions.subString(pstCd, Len.PST_CD);
	}

	public String getPstCd() {
		return this.pstCd;
	}

	public void setCerNbr(String cerNbr) {
		this.cerNbr = Functions.subString(cerNbr, Len.CER_NBR);
	}

	public String getCerNbr() {
		return this.cerNbr;
	}

	@Override
	public TtThirdPartiesXz0b9081 copy() {
		return new TtThirdPartiesXz0b9081(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ID = 20;
		public static final int ADR_ID = 20;
		public static final int KEY_FIELDS = CLIENT_ID + ADR_ID;
		public static final int NAME = 120;
		public static final int ADR_LIN1 = 45;
		public static final int ADR_LIN2 = 45;
		public static final int CITY_NM = 30;
		public static final int STATE_ABB = 2;
		public static final int PST_CD = 13;
		public static final int CER_NBR = 25;
		public static final int TT_THIRD_PARTIES = KEY_FIELDS + NAME + ADR_LIN1 + ADR_LIN2 + CITY_NM + STATE_ABB + PST_CD + CER_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
