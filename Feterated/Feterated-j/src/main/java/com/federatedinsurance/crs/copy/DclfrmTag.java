/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLFRM-TAG<br>
 * Variable: DCLFRM-TAG from copybook XZH00027<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclfrmTag {

	//==== PROPERTIES ====
	//Original name: EDL-FRM-NM
	private String edlFrmNm = DefaultValues.stringVal(Len.EDL_FRM_NM);
	//Original name: TAG-NM
	private String tagNm = DefaultValues.stringVal(Len.TAG_NM);
	//Original name: LEN-NBR
	private short lenNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: TAG-OCC-CNT
	private short tagOccCnt = DefaultValues.BIN_SHORT_VAL;
	//Original name: DEL-IND
	private char delInd = DefaultValues.CHAR_VAL;
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setEdlFrmNm(String edlFrmNm) {
		this.edlFrmNm = Functions.subString(edlFrmNm, Len.EDL_FRM_NM);
	}

	public String getEdlFrmNm() {
		return this.edlFrmNm;
	}

	public void setTagNm(String tagNm) {
		this.tagNm = Functions.subString(tagNm, Len.TAG_NM);
	}

	public String getTagNm() {
		return this.tagNm;
	}

	public void setLenNbr(short lenNbr) {
		this.lenNbr = lenNbr;
	}

	public short getLenNbr() {
		return this.lenNbr;
	}

	public void setTagOccCnt(short tagOccCnt) {
		this.tagOccCnt = tagOccCnt;
	}

	public short getTagOccCnt() {
		return this.tagOccCnt;
	}

	public void setDelInd(char delInd) {
		this.delInd = delInd;
	}

	public char getDelInd() {
		return this.delInd;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EDL_FRM_NM = 30;
		public static final int TAG_NM = 30;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
