/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: TABLE-OF-POL-TYP-CD-TO-SEARCH<br>
 * Variable: TABLE-OF-POL-TYP-CD-TO-SEARCH from program XZ0B9050<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TableOfPolTypCdToSearch extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int TP_POL_TYP_CD_MAXOCCURS = 9;

	//==== CONSTRUCTORS ====
	public TableOfPolTypCdToSearch() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.TABLE_OF_POL_TYP_CD_TO_SEARCH;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "CPP", Len.TP_POL_TYP_CD1);
		position += Len.TP_POL_TYP_CD1;
		writeString(position, "WC", Len.TP_POL_TYP_CD2);
		position += Len.TP_POL_TYP_CD2;
		writeString(position, "BOP", Len.TP_POL_TYP_CD3);
		position += Len.TP_POL_TYP_CD3;
		writeString(position, "UMB", Len.TP_POL_TYP_CD4);
		position += Len.TP_POL_TYP_CD4;
		writeString(position, "EPL", Len.TP_POL_TYP_CD5);
		position += Len.TP_POL_TYP_CD5;
		writeString(position, "PAP", Len.TP_POL_TYP_CD6);
		position += Len.TP_POL_TYP_CD6;
		writeString(position, "HOP", Len.TP_POL_TYP_CD7);
		position += Len.TP_POL_TYP_CD7;
		writeString(position, "PU", Len.TP_POL_TYP_CD8);
		position += Len.TP_POL_TYP_CD8;
		writeString(position, "BAP", Len.TP_POL_TYP_CD9);
	}

	/**Original name: TP-POL-TYP-CD<br>*/
	public String getTpPolTypCd(int tpPolTypCdIdx) {
		int position = Pos.tpPolTypCd(tpPolTypCdIdx - 1);
		return readString(position, Len.TP_POL_TYP_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int TABLE_OF_POL_TYP_CD_TO_SEARCH = 1;
		public static final int TP_POL_TYP_CD1 = TABLE_OF_POL_TYP_CD_TO_SEARCH;
		public static final int TP_POL_TYP_CD2 = TP_POL_TYP_CD1 + Len.TP_POL_TYP_CD1;
		public static final int TP_POL_TYP_CD3 = TP_POL_TYP_CD2 + Len.TP_POL_TYP_CD2;
		public static final int TP_POL_TYP_CD4 = TP_POL_TYP_CD3 + Len.TP_POL_TYP_CD3;
		public static final int TP_POL_TYP_CD5 = TP_POL_TYP_CD4 + Len.TP_POL_TYP_CD4;
		public static final int TP_POL_TYP_CD6 = TP_POL_TYP_CD5 + Len.TP_POL_TYP_CD5;
		public static final int TP_POL_TYP_CD7 = TP_POL_TYP_CD6 + Len.TP_POL_TYP_CD6;
		public static final int TP_POL_TYP_CD8 = TP_POL_TYP_CD7 + Len.TP_POL_TYP_CD7;
		public static final int TP_POL_TYP_CD9 = TP_POL_TYP_CD8 + Len.TP_POL_TYP_CD8;
		public static final int FILLER_WS = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int tpPolTypCd(int idx) {
			return FILLER_WS + idx * Len.TP_POL_TYP_CD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int TP_POL_TYP_CD1 = 3;
		public static final int TP_POL_TYP_CD2 = 3;
		public static final int TP_POL_TYP_CD3 = 3;
		public static final int TP_POL_TYP_CD4 = 3;
		public static final int TP_POL_TYP_CD5 = 3;
		public static final int TP_POL_TYP_CD6 = 3;
		public static final int TP_POL_TYP_CD7 = 3;
		public static final int TP_POL_TYP_CD8 = 3;
		public static final int TP_POL_TYP_CD = 3;
		public static final int TP_POL_TYP_CD9 = 3;
		public static final int TABLE_OF_POL_TYP_CD_TO_SEARCH = TP_POL_TYP_CD1 + TP_POL_TYP_CD2 + TP_POL_TYP_CD3 + TP_POL_TYP_CD4 + TP_POL_TYP_CD5
				+ TP_POL_TYP_CD6 + TP_POL_TYP_CD7 + TP_POL_TYP_CD8 + TP_POL_TYP_CD9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
