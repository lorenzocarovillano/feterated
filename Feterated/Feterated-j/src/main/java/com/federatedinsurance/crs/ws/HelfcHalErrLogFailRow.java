/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.redefines.HelfcHalErrLogFailData;

/**Original name: HELFC-HAL-ERR-LOG-FAIL-ROW<br>
 * Variable: HELFC-HAL-ERR-LOG-FAIL-ROW from copybook HALLCELF<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class HelfcHalErrLogFailRow {

	//==== PROPERTIES ====
	//Original name: HELFC-HAL-ERR-LOG-FAIL-CHK-SUM
	private String helfcHalErrLogFailChkSum = DefaultValues.stringVal(Len.HELFC_HAL_ERR_LOG_FAIL_CHK_SUM);
	//Original name: HELFC-TRANS-PROCESS-DT
	private String helfcTransProcessDt = DefaultValues.stringVal(Len.HELFC_TRANS_PROCESS_DT);
	//Original name: HELFC-UOW-ID-CI
	private char helfcUowIdCi = DefaultValues.CHAR_VAL;
	//Original name: HELFC-UOW-ID
	private String helfcUowId = DefaultValues.stringVal(Len.HELFC_UOW_ID);
	//Original name: HELFC-FAIL-TS-CI
	private char helfcFailTsCi = DefaultValues.CHAR_VAL;
	//Original name: HELFC-FAIL-TS
	private String helfcFailTs = DefaultValues.stringVal(Len.HELFC_FAIL_TS);
	//Original name: HELFC-HAL-ERR-LOG-FAIL-DATA
	private HelfcHalErrLogFailData helfcHalErrLogFailData = new HelfcHalErrLogFailData();

	//==== METHODS ====
	public String getHelfcHalErrLogFailRowFormatted() {
		return MarshalByteExt.bufferToStr(getHelfcHalErrLogFailRowBytes());
	}

	public byte[] getHelfcHalErrLogFailRowBytes() {
		byte[] buffer = new byte[Len.HELFC_HAL_ERR_LOG_FAIL_ROW];
		return getHelfcHalErrLogFailRowBytes(buffer, 1);
	}

	public void setHelfcHalErrLogFailRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setHelfcHalErrLogFailFixedBytes(buffer, position);
		position += Len.HELFC_HAL_ERR_LOG_FAIL_FIXED;
		setHelfcHalErrLogFailDatesBytes(buffer, position);
		position += Len.HELFC_HAL_ERR_LOG_FAIL_DATES;
		setHelfcHalErrLogFailKeyBytes(buffer, position);
		position += Len.HELFC_HAL_ERR_LOG_FAIL_KEY;
		helfcHalErrLogFailData.setHelfcHalErrLogFailDataBytes(buffer, position);
	}

	public byte[] getHelfcHalErrLogFailRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getHelfcHalErrLogFailFixedBytes(buffer, position);
		position += Len.HELFC_HAL_ERR_LOG_FAIL_FIXED;
		getHelfcHalErrLogFailDatesBytes(buffer, position);
		position += Len.HELFC_HAL_ERR_LOG_FAIL_DATES;
		getHelfcHalErrLogFailKeyBytes(buffer, position);
		position += Len.HELFC_HAL_ERR_LOG_FAIL_KEY;
		helfcHalErrLogFailData.getHelfcHalErrLogFailDataBytes(buffer, position);
		return buffer;
	}

	public void initHelfcHalErrLogFailRowSpaces() {
		initHelfcHalErrLogFailFixedSpaces();
		initHelfcHalErrLogFailDatesSpaces();
		initHelfcHalErrLogFailKeySpaces();
		helfcHalErrLogFailData.initHelfcHalErrLogFailDataSpaces();
	}

	public void setHelfcHalErrLogFailFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		helfcHalErrLogFailChkSum = MarshalByte.readFixedString(buffer, position, Len.HELFC_HAL_ERR_LOG_FAIL_CHK_SUM);
	}

	public byte[] getHelfcHalErrLogFailFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, helfcHalErrLogFailChkSum, Len.HELFC_HAL_ERR_LOG_FAIL_CHK_SUM);
		return buffer;
	}

	public void initHelfcHalErrLogFailFixedSpaces() {
		helfcHalErrLogFailChkSum = "";
	}

	public void setHelfcHalErrLogFailDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		helfcTransProcessDt = MarshalByte.readString(buffer, position, Len.HELFC_TRANS_PROCESS_DT);
	}

	public byte[] getHelfcHalErrLogFailDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, helfcTransProcessDt, Len.HELFC_TRANS_PROCESS_DT);
		return buffer;
	}

	public void initHelfcHalErrLogFailDatesSpaces() {
		helfcTransProcessDt = "";
	}

	public void setHelfcTransProcessDt(String helfcTransProcessDt) {
		this.helfcTransProcessDt = Functions.subString(helfcTransProcessDt, Len.HELFC_TRANS_PROCESS_DT);
	}

	public String getHelfcTransProcessDt() {
		return this.helfcTransProcessDt;
	}

	public void setHelfcHalErrLogFailKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		helfcUowIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		helfcUowId = MarshalByte.readString(buffer, position, Len.HELFC_UOW_ID);
		position += Len.HELFC_UOW_ID;
		helfcFailTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		helfcFailTs = MarshalByte.readString(buffer, position, Len.HELFC_FAIL_TS);
	}

	public byte[] getHelfcHalErrLogFailKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, helfcUowIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, helfcUowId, Len.HELFC_UOW_ID);
		position += Len.HELFC_UOW_ID;
		MarshalByte.writeChar(buffer, position, helfcFailTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, helfcFailTs, Len.HELFC_FAIL_TS);
		return buffer;
	}

	public void initHelfcHalErrLogFailKeySpaces() {
		helfcUowIdCi = Types.SPACE_CHAR;
		helfcUowId = "";
		helfcFailTsCi = Types.SPACE_CHAR;
		helfcFailTs = "";
	}

	public void setHelfcUowIdCi(char helfcUowIdCi) {
		this.helfcUowIdCi = helfcUowIdCi;
	}

	public char getHelfcUowIdCi() {
		return this.helfcUowIdCi;
	}

	public void setHelfcUowId(String helfcUowId) {
		this.helfcUowId = Functions.subString(helfcUowId, Len.HELFC_UOW_ID);
	}

	public String getHelfcUowId() {
		return this.helfcUowId;
	}

	public void setHelfcFailTsCi(char helfcFailTsCi) {
		this.helfcFailTsCi = helfcFailTsCi;
	}

	public char getHelfcFailTsCi() {
		return this.helfcFailTsCi;
	}

	public void setHelfcFailTs(String helfcFailTs) {
		this.helfcFailTs = Functions.subString(helfcFailTs, Len.HELFC_FAIL_TS);
	}

	public String getHelfcFailTs() {
		return this.helfcFailTs;
	}

	public HelfcHalErrLogFailData getHelfcHalErrLogFailData() {
		return helfcHalErrLogFailData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HELFC_HAL_ERR_LOG_FAIL_CHK_SUM = 9;
		public static final int HELFC_TRANS_PROCESS_DT = 10;
		public static final int HELFC_UOW_ID = 32;
		public static final int HELFC_FAIL_TS = 26;
		public static final int HELFC_HAL_ERR_LOG_FAIL_FIXED = HELFC_HAL_ERR_LOG_FAIL_CHK_SUM;
		public static final int HELFC_HAL_ERR_LOG_FAIL_DATES = HELFC_TRANS_PROCESS_DT;
		public static final int HELFC_UOW_ID_CI = 1;
		public static final int HELFC_FAIL_TS_CI = 1;
		public static final int HELFC_HAL_ERR_LOG_FAIL_KEY = HELFC_UOW_ID_CI + HELFC_UOW_ID + HELFC_FAIL_TS_CI + HELFC_FAIL_TS;
		public static final int HELFC_HAL_ERR_LOG_FAIL_ROW = HELFC_HAL_ERR_LOG_FAIL_FIXED + HELFC_HAL_ERR_LOG_FAIL_DATES + HELFC_HAL_ERR_LOG_FAIL_KEY
				+ HelfcHalErrLogFailData.Len.HELFC_HAL_ERR_LOG_FAIL_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
