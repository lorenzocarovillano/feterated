/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: WS-WORK-FIELDS<br>
 * Variable: WS-WORK-FIELDS from program HALOUKRP<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkFieldsHaloukrp {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "HALOUKRP";
	//Original name: WS-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-DATA-KEY-DISP
	private String dataKeyDisp = DefaultValues.stringVal(Len.DATA_KEY_DISP);

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public void setDataKeyDisp(long dataKeyDisp) {
		this.dataKeyDisp = PicFormatter.display("-Z(8)9").format(dataKeyDisp).toString();
	}

	public long getDataKeyDisp() {
		return PicParser.display("-Z(8)9").parseLong(this.dataKeyDisp);
	}

	public String getDataKeyDispFormatted() {
		return this.dataKeyDisp;
	}

	public String getDataKeyDispAsString() {
		return getDataKeyDispFormatted();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EIBRESP_DISPLAY = 10;
		public static final int EIBRESP2_DISPLAY = 10;
		public static final int DATA_KEY_DISP = 10;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
