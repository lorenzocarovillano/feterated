/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.Halrvdt1Data;
import com.federatedinsurance.crs.ws.LInputDate;
import com.federatedinsurance.crs.ws.enums.LOutputValidDateInd;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;

/**Original name: HALRVDT1<br>
 * <pre>AUTHOR.
 *                   J. A. FULCHER FOR PMSC.
 * DATE-WRITTEN.
 *                   MAY 2000.
 * DATE-COMPILED.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  DETERMINE IF INPUT 10 CHAR STRING          **
 * *                  CONTAINS A DATE IN FORMAT YYYY-MM-DD       **
 * *                                                             **
 * * PLATFORM - IBM MAINFRAME                                    **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -        DETERMINE IF INPUT 10 CHAR STRING          **
 * *                  CONTAINS A DATE IN FORMAT YYYY-MM-DD       **
 * *                  - ZERO IN DAY, MONTH OR YEAR NOT ALLOWED   **
 * *                  - LEAP YEARS CATERED FOR                   **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED AS FOLLOWS:   **
 * *                       1) STATICALLY CALLED BY A DATA OBJECT **
 * *                                                             **
 * * DATA ACCESS METHODS - DATA PASSED IN LINKAGE                **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #      DATE      PRGMR   DESCRIPTION                     **
 * * --------- --------- ------  --------------------------------**
 * * SAVANNAH  06MAY00   JAF     NEW ROUTINE.                    **
 * ****************************************************************</pre>*/
public class Halrvdt1 extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private Halrvdt1Data ws = new Halrvdt1Data();
	//Original name: L-INPUT-DATE
	private LInputDate lInputDate;
	//Original name: L-OUTPUT-VALID-DATE-IND
	private LOutputValidDateInd lOutputValidDateInd;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LInputDate lInputDate, LOutputValidDateInd lOutputValidDateInd) {
		this.lInputDate = lInputDate;
		this.lOutputValidDateInd = lOutputValidDateInd;
		s0000Main();
		mainX();
		return 0;
	}

	public static Halrvdt1 getInstance() {
		return (Programs.getInstance(Halrvdt1.class));
	}

	/**Original name: S0000-MAIN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONTROLS PROCESSING                                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void s0000Main() {
		// COB_CODE: PERFORM 0100-INITIALIZATION.
		initialization();
		// COB_CODE: PERFORM 0200-CHECK-BASIC-FORMAT.
		checkBasicFormat();
		// COB_CODE: IF NOT-VALID-DATE
		//               GO TO 0000-MAIN-X
		//           END-IF.
		if (lOutputValidDateInd.isNotValidDate()) {
			// COB_CODE: GO TO 0000-MAIN-X
			mainX();
		}
		// COB_CODE: IF L-INPUT-DD = 29 AND L-INPUT-MM = 02
		//               PERFORM 0300-CHECK-29-FEB
		//           ELSE
		//               PERFORM 0400-CHECK-NON-29-FEB
		//           END-IF.
		if (lInputDate.getlInputDd() == 29 && lInputDate.getlInputMm() == 2) {
			// COB_CODE: PERFORM 0300-CHECK-29-FEB
			check29Feb();
		} else {
			// COB_CODE: PERFORM 0400-CHECK-NON-29-FEB
			checkNon29Feb();
		}
	}

	/**Original name: 0000-MAIN-X<br>*/
	private void mainX() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 0100-INITIALIZATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PERFORM INITIAL PROCESSING.                                    *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void initialization() {
		// COB_CODE: SET VALID-DATE TO TRUE.
		lOutputValidDateInd.setValidDate();
	}

	/**Original name: 0200-CHECK-BASIC-FORMAT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CHECK DAY, MONTH, YEAR HAVE CORRECT BASIC FORMAT (E.G. NUMERICS)
	 *                                                                 *
	 * *****************************************************************
	 *  BASIC FORMAT CHECKS</pre>*/
	private void checkBasicFormat() {
		// COB_CODE: IF L-INPUT-DD     NOT NUMERIC OR
		//              NOT VALID-L-INPUT-DELIM1   OR
		//              L-INPUT-MM     NOT NUMERIC OR
		//              NOT VALID-L-INPUT-DELIM2   OR
		//              L-INPUT-YYYY   NOT NUMERIC
		//               GO TO 0200-CHECK-BASIC-FORMAT-X
		//           END-IF.
		if (!Functions.isNumber(lInputDate.getlInputDdFormatted()) || !lInputDate.isValidLInputDelim1()
				|| !Functions.isNumber(lInputDate.getlInputMm()) || !lInputDate.isValidLInputDelim2()
				|| !Functions.isNumber(lInputDate.getlInputYyyyFormatted())) {
			// COB_CODE: SET NOT-VALID-DATE TO TRUE
			lOutputValidDateInd.setNotValidDate();
			// COB_CODE: GO TO 0200-CHECK-BASIC-FORMAT-X
			return;
		}
		// BASIC RANGE CHECKS
		// COB_CODE: IF L-INPUT-DD    < 01          OR
		//             (L-INPUT-MM    < 01 OR > 12) OR
		//              L-INPUT-YYYY  < 01
		//               GO TO 0200-CHECK-BASIC-FORMAT-X
		//           END-IF.
		if (lInputDate.getlInputDd() < 1 || lInputDate.getlInputMm() < 1 || lInputDate.getlInputMm() > 12 || lInputDate.getlInputYyyy() < 1) {
			// COB_CODE: SET NOT-VALID-DATE TO TRUE
			lOutputValidDateInd.setNotValidDate();
			// COB_CODE: GO TO 0200-CHECK-BASIC-FORMAT-X
			return;
		}
	}

	/**Original name: 0300-CHECK-29-FEB_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CHECK SUPPLIED DATE OF 29 FEB CORRESPONDS TO A LEAP YEAR       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void check29Feb() {
		// COB_CODE: PERFORM 0350-CHECK-IF-LEAP-YEAR.
		checkIfLeapYear();
		// COB_CODE: IF NOT-LEAP-YEAR
		//               SET NOT-VALID-DATE TO TRUE
		//           END-IF.
		if (ws.getWsLeapYearInd().isNotLeapYear()) {
			// COB_CODE: SET NOT-VALID-DATE TO TRUE
			lOutputValidDateInd.setNotValidDate();
		}
	}

	/**Original name: 0350-CHECK-IF-LEAP-YEAR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CHECK IF YEAR IS A LEAP YEAR                                   *
	 *                                                                 *
	 * *****************************************************************
	 *  NOT DIVISIBLE BY 4 => NOT LEAP</pre>*/
	private void checkIfLeapYear() {
		// COB_CODE: DIVIDE L-INPUT-YYYY BY 4 GIVING WS-DUMMY
		//                  REMAINDER WS-REMAINDER.
		ws.setWsDummy(lInputDate.getlInputYyyy() / 4);
		ws.setWsRemainder(((short) (lInputDate.getlInputYyyy() % 4)));
		// COB_CODE: IF WS-REMAINDER NOT = 0
		//               GO TO 0350-CHECK-IF-LEAP-YEAR-X
		//           END-IF.
		if (ws.getWsRemainder() != 0) {
			// COB_CODE: SET NOT-LEAP-YEAR TO TRUE
			ws.getWsLeapYearInd().setNotLeapYear();
			// COB_CODE: GO TO 0350-CHECK-IF-LEAP-YEAR-X
			return;
		}
		// NOT DIVISIBLE BY 100 AND DIVISIBLE BY 4 => LEAP
		// COB_CODE: DIVIDE L-INPUT-YYYY BY 100 GIVING WS-DUMMY
		//                  REMAINDER WS-REMAINDER.
		ws.setWsDummy(lInputDate.getlInputYyyy() / 100);
		ws.setWsRemainder(((short) (lInputDate.getlInputYyyy() % 100)));
		// COB_CODE: IF WS-REMAINDER NOT = 0
		//               GO TO 0350-CHECK-IF-LEAP-YEAR-X
		//           END-IF.
		if (ws.getWsRemainder() != 0) {
			// COB_CODE: SET LEAP-YEAR TO TRUE
			ws.getWsLeapYearInd().setLeapYear();
			// COB_CODE: GO TO 0350-CHECK-IF-LEAP-YEAR-X
			return;
		}
		// DIVISIBLE BY 400 => LEAP
		// COB_CODE: DIVIDE L-INPUT-YYYY BY 400 GIVING WS-DUMMY
		//                  REMAINDER WS-REMAINDER.
		ws.setWsDummy(lInputDate.getlInputYyyy() / 400);
		ws.setWsRemainder(((short) (lInputDate.getlInputYyyy() % 400)));
		// COB_CODE: IF WS-REMAINDER = 0
		//               GO TO 0350-CHECK-IF-LEAP-YEAR-X
		//           END-IF.
		if (ws.getWsRemainder() == 0) {
			// COB_CODE: SET LEAP-YEAR TO TRUE
			ws.getWsLeapYearInd().setLeapYear();
			// COB_CODE: GO TO 0350-CHECK-IF-LEAP-YEAR-X
			return;
		}
		// DIVISIBLE BY 100 BUT NOT 400 => NOT LEAP
		// COB_CODE: SET NOT-LEAP-YEAR TO TRUE.
		ws.getWsLeapYearInd().setNotLeapYear();
	}

	/**Original name: 0400-CHECK-NON-29-FEB_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CHECK DAY AND MONTH (WHICH IS NOT 29 FEB) IS VALID             *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void checkNon29Feb() {
		// COB_CODE: IF L-INPUT-DD > WS-DAYS-IN-MONTH(L-INPUT-MM)
		//               SET NOT-VALID-DATE TO TRUE
		//           END-IF.
		if (lInputDate.getlInputDd() > ws.getWsDaysInMonthTab().getMonth(lInputDate.getlInputMm())) {
			// COB_CODE: SET NOT-VALID-DATE TO TRUE
			lOutputValidDateInd.setNotValidDate();
		}
	}
}
