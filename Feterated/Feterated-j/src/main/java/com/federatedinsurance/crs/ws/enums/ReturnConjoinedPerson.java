/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: RETURN-CONJOINED-PERSON<br>
 * Variable: RETURN-CONJOINED-PERSON from copybook CISLNSRB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ReturnConjoinedPerson {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char CONJOINED_PERSON = 'F';

	//==== METHODS ====
	public void setReturnConjoinedPerson(char returnConjoinedPerson) {
		this.value = returnConjoinedPerson;
	}

	public char getReturnConjoinedPerson() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURN_CONJOINED_PERSON = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
