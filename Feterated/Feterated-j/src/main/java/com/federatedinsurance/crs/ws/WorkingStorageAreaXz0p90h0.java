/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P90H0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p90h0 {

	//==== PROPERTIES ====
	//Original name: WS-EIBRESP-CD
	private short eibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-COWN-NM
	private String cownNm = "";
	//Original name: WS-REC-NAME
	private String recName = "";
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0P90H0";
	//Original name: WS-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: WS-ADR-ID
	private String adrId = DefaultValues.stringVal(Len.ADR_ID);
	//Original name: WS-MAX-ROWS
	private String maxRows = DefaultValues.stringVal(Len.MAX_ROWS);
	//Original name: WS-SORT-ORDER
	private char sortOrder = DefaultValues.CHAR_VAL;
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-PREPARE-INS-POL
	private String busObjNmPrepareInsPol = "XZ_PREPARE_INSURED_POLICY";
	//Original name: WS-OPERATIONS-CALLED
	private WsOperationsCalledXz0p90h0 operationsCalled = new WsOperationsCalledXz0p90h0();

	//==== METHODS ====
	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd = eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public String getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public String getEibrespCdAsString() {
		return getEibrespCdFormatted();
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd = eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public String getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public String getEibresp2CdAsString() {
		return getEibresp2CdFormatted();
	}

	public void setCownNm(String cownNm) {
		this.cownNm = Functions.subString(cownNm, Len.COWN_NM);
	}

	public String getCownNm() {
		return this.cownNm;
	}

	public String getCownNmFormatted() {
		return Functions.padBlanks(getCownNm(), Len.COWN_NM);
	}

	public void setRecName(String recName) {
		this.recName = Functions.subString(recName, Len.REC_NAME);
	}

	public void setRecNameSubstring(String replacement, int start, int length) {
		recName = Functions.setSubstring(recName, replacement, start, length);
	}

	public String getRecName() {
		return this.recName;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setAdrId(String adrId) {
		this.adrId = Functions.subString(adrId, Len.ADR_ID);
	}

	public String getAdrId() {
		return this.adrId;
	}

	public void setMaxRows(long maxRows) {
		this.maxRows = PicFormatter.display("Z(3)9").format(maxRows).toString();
	}

	public long getMaxRows() {
		return PicParser.display("Z(3)9").parseLong(this.maxRows);
	}

	public String getMaxRowsFormatted() {
		return this.maxRows;
	}

	public String getMaxRowsAsString() {
		return getMaxRowsFormatted();
	}

	public void setSortOrder(char sortOrder) {
		this.sortOrder = sortOrder;
	}

	public char getSortOrder() {
		return this.sortOrder;
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNmPrepareInsPol() {
		return this.busObjNmPrepareInsPol;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	public WsOperationsCalledXz0p90h0 getOperationsCalled() {
		return operationsCalled;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int ADR_ID = 64;
		public static final int MAX_ROWS = 4;
		public static final int PROGRAM_NAME = 8;
		public static final int EIBRESP_CD = 4;
		public static final int EIBRESP2_CD = 4;
		public static final int REC_NAME = 120;
		public static final int COWN_NM = 45;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
