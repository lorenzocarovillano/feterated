/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XZC002-ACT-NOT-POL-FIXED<br>
 * Variable: XZC002-ACT-NOT-POL-FIXED from copybook XZ0C0002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzc002ActNotPolFixed {

	//==== PROPERTIES ====
	//Original name: XZC002-ACT-NOT-POL-CSUM
	private String actNotPolCsum = DefaultValues.stringVal(Len.ACT_NOT_POL_CSUM);
	//Original name: XZC002-CSR-ACT-NBR-KCRE
	private String csrActNbrKcre = DefaultValues.stringVal(Len.CSR_ACT_NBR_KCRE);
	//Original name: XZC002-NOT-PRC-TS-KCRE
	private String notPrcTsKcre = DefaultValues.stringVal(Len.NOT_PRC_TS_KCRE);
	//Original name: XZC002-POL-NBR-KCRE
	private String polNbrKcre = DefaultValues.stringVal(Len.POL_NBR_KCRE);

	//==== METHODS ====
	public void setActNotPolFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotPolCsum = MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_POL_CSUM);
		position += Len.ACT_NOT_POL_CSUM;
		csrActNbrKcre = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		notPrcTsKcre = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		polNbrKcre = MarshalByte.readString(buffer, position, Len.POL_NBR_KCRE);
	}

	public byte[] getActNotPolFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotPolCsum, Len.ACT_NOT_POL_CSUM);
		position += Len.ACT_NOT_POL_CSUM;
		MarshalByte.writeString(buffer, position, csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		MarshalByte.writeString(buffer, position, notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		MarshalByte.writeString(buffer, position, polNbrKcre, Len.POL_NBR_KCRE);
		return buffer;
	}

	public void initActNotPolFixedSpaces() {
		actNotPolCsum = "";
		csrActNbrKcre = "";
		notPrcTsKcre = "";
		polNbrKcre = "";
	}

	public void setActNotPolCsumFormatted(String actNotPolCsum) {
		this.actNotPolCsum = Trunc.toUnsignedNumeric(actNotPolCsum, Len.ACT_NOT_POL_CSUM);
	}

	public int getActNotPolCsum() {
		return NumericDisplay.asInt(this.actNotPolCsum);
	}

	public String getXzc002ActNotPolCsumFormatted() {
		return this.actNotPolCsum;
	}

	public String getXzc002ActNotPolCsumAsString() {
		return getXzc002ActNotPolCsumFormatted();
	}

	public void setCsrActNbrKcre(String csrActNbrKcre) {
		this.csrActNbrKcre = Functions.subString(csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
	}

	public String getCsrActNbrKcre() {
		return this.csrActNbrKcre;
	}

	public String getXzc002CsrActNbrKcreFormatted() {
		return Functions.padBlanks(getCsrActNbrKcre(), Len.CSR_ACT_NBR_KCRE);
	}

	public void setNotPrcTsKcre(String notPrcTsKcre) {
		this.notPrcTsKcre = Functions.subString(notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
	}

	public String getNotPrcTsKcre() {
		return this.notPrcTsKcre;
	}

	public String getXzc002NotPrcTsKcreFormatted() {
		return Functions.padBlanks(getNotPrcTsKcre(), Len.NOT_PRC_TS_KCRE);
	}

	public void setPolNbrKcre(String polNbrKcre) {
		this.polNbrKcre = Functions.subString(polNbrKcre, Len.POL_NBR_KCRE);
	}

	public String getPolNbrKcre() {
		return this.polNbrKcre;
	}

	public String getXzc002PolNbrKcreFormatted() {
		return Functions.padBlanks(getPolNbrKcre(), Len.POL_NBR_KCRE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_POL_CSUM = 9;
		public static final int CSR_ACT_NBR_KCRE = 32;
		public static final int NOT_PRC_TS_KCRE = 32;
		public static final int POL_NBR_KCRE = 32;
		public static final int ACT_NOT_POL_FIXED = ACT_NOT_POL_CSUM + CSR_ACT_NBR_KCRE + NOT_PRC_TS_KCRE + POL_NBR_KCRE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
