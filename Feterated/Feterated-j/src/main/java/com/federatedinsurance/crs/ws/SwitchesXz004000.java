/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesXz004000 {

	//==== PROPERTIES ====
	//Original name: SW-FIRST-FOUND-FLAG
	private boolean firstFoundFlag = true;
	//Original name: SW-ACT-FOUND-FLAG
	private boolean actFoundFlag = true;
	//Original name: SW-ACT-NOT-STS-FLAG
	private boolean actNotStsFlag = true;
	//Original name: SW-RES-IMP-STS-FLAG
	private boolean resImpStsFlag = true;

	//==== METHODS ====
	public void setFirstFoundFlag(boolean firstFoundFlag) {
		this.firstFoundFlag = firstFoundFlag;
	}

	public boolean isFirstFoundFlag() {
		return this.firstFoundFlag;
	}

	public void setActFoundFlag(boolean actFoundFlag) {
		this.actFoundFlag = actFoundFlag;
	}

	public boolean isActFoundFlag() {
		return this.actFoundFlag;
	}

	public void setActNotStsFlag(boolean actNotStsFlag) {
		this.actNotStsFlag = actNotStsFlag;
	}

	public boolean isActNotStsFlag() {
		return this.actNotStsFlag;
	}

	public void setResImpStsFlag(boolean resImpStsFlag) {
		this.resImpStsFlag = resImpStsFlag;
	}

	public boolean isResImpStsFlag() {
		return this.resImpStsFlag;
	}
}
