/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.copy.LCmErrorInformation;
import com.federatedinsurance.crs.ws.enums.LCmCicsApplId;
import com.federatedinsurance.crs.ws.enums.LCmFunction;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-CICS-PIPE-MAINTENANCE-CNT<br>
 * Variable: L-CICS-PIPE-MAINTENANCE-CNT from copybook TS54801<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LCicsPipeMaintenanceCnt extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: L-CM-FUNCTION
	private LCmFunction function = new LCmFunction();
	/**Original name: L-CM-CICS-APPL-ID<br>
	 * <pre> BELOW IS A LIST OF COMMONLY USED CICS REGIONS.
	 *   NOTE:  THERE IS NOT ANY LIMIT IN CONNECTING TO THESE REGIONS
	 *            ONLY.</pre>*/
	private LCmCicsApplId cicsApplId = new LCmCicsApplId();
	//Original name: L-CM-PI-USER-TOKEN
	private int piUserToken = DefaultValues.BIN_INT_VAL;
	//Original name: L-CM-PI-PIPE-TOKEN
	private int piPipeToken = DefaultValues.BIN_INT_VAL;
	//Original name: L-CM-ERROR-INFORMATION
	private LCmErrorInformation errorInformation = new LCmErrorInformation();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_CICS_PIPE_MAINTENANCE_CNT;
	}

	@Override
	public void deserialize(byte[] buf) {
		setlCicsPipeMaintenanceCntBytes(buf);
	}

	public void setCicsPipeMaintenanceCntFormatted(String data) {
		byte[] buffer = new byte[Len.L_CICS_PIPE_MAINTENANCE_CNT];
		MarshalByte.writeString(buffer, 1, data, Len.L_CICS_PIPE_MAINTENANCE_CNT);
		setlCicsPipeMaintenanceCntBytes(buffer, 1);
	}

	public String getCicsPipeMaintenanceCntFormatted() {
		return MarshalByteExt.bufferToStr(getlCicsPipeMaintenanceCntBytes());
	}

	public void setlCicsPipeMaintenanceCntBytes(byte[] buffer) {
		setlCicsPipeMaintenanceCntBytes(buffer, 1);
	}

	public byte[] getlCicsPipeMaintenanceCntBytes() {
		byte[] buffer = new byte[Len.L_CICS_PIPE_MAINTENANCE_CNT];
		return getlCicsPipeMaintenanceCntBytes(buffer, 1);
	}

	public void setlCicsPipeMaintenanceCntBytes(byte[] buffer, int offset) {
		int position = offset;
		function.setFunction(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		cicsApplId.setCicsApplId(MarshalByte.readString(buffer, position, LCmCicsApplId.Len.CICS_APPL_ID));
		position += LCmCicsApplId.Len.CICS_APPL_ID;
		setPipeIdTokensBytes(buffer, position);
		position += Len.PIPE_ID_TOKENS;
		errorInformation.setErrorInformationBytes(buffer, position);
	}

	public byte[] getlCicsPipeMaintenanceCntBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, function.getFunction());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicsApplId.getCicsApplId(), LCmCicsApplId.Len.CICS_APPL_ID);
		position += LCmCicsApplId.Len.CICS_APPL_ID;
		getPipeIdTokensBytes(buffer, position);
		position += Len.PIPE_ID_TOKENS;
		errorInformation.getErrorInformationBytes(buffer, position);
		return buffer;
	}

	public void setPipeIdTokensBytes(byte[] buffer, int offset) {
		int position = offset;
		piUserToken = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		piPipeToken = MarshalByte.readBinaryInt(buffer, position);
	}

	public byte[] getPipeIdTokensBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryInt(buffer, position, piUserToken);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, piPipeToken);
		return buffer;
	}

	public void setPiUserToken(int piUserToken) {
		this.piUserToken = piUserToken;
	}

	public int getPiUserToken() {
		return this.piUserToken;
	}

	public void setPiPipeToken(int piPipeToken) {
		this.piPipeToken = piPipeToken;
	}

	public int getPiPipeToken() {
		return this.piPipeToken;
	}

	public LCmCicsApplId getCicsApplId() {
		return cicsApplId;
	}

	public LCmErrorInformation getErrorInformation() {
		return errorInformation;
	}

	public LCmFunction getFunction() {
		return function;
	}

	@Override
	public byte[] serialize() {
		return getlCicsPipeMaintenanceCntBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PI_USER_TOKEN = 4;
		public static final int PI_PIPE_TOKEN = 4;
		public static final int PIPE_ID_TOKENS = PI_USER_TOKEN + PI_PIPE_TOKEN;
		public static final int L_CICS_PIPE_MAINTENANCE_CNT = LCmFunction.Len.FUNCTION + LCmCicsApplId.Len.CICS_APPL_ID + PIPE_ID_TOKENS
				+ LCmErrorInformation.Len.ERROR_INFORMATION;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
