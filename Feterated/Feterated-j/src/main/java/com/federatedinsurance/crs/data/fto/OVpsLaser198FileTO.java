/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: O-VPS-LASER-198-FILE<br>
 * File: O-VPS-LASER-198-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OVpsLaser198FileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: VLO-CARRIAGE-CONTROL
	private String vloCarriageControl = DefaultValues.stringVal(Len.VLO_CARRIAGE_CONTROL);
	//Original name: VLO-DATA
	private String vloData = DefaultValues.stringVal(Len.VLO_DATA);

	//==== METHODS ====
	public void setoVpsLaser198RecordBytes(byte[] buffer, int offset) {
		int position = offset;
		vloCarriageControl = MarshalByte.readFixedString(buffer, position, Len.VLO_CARRIAGE_CONTROL);
		position += Len.VLO_CARRIAGE_CONTROL;
		vloData = MarshalByte.readString(buffer, position, Len.VLO_DATA);
	}

	public byte[] getoVpsLaser198RecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, vloCarriageControl, Len.VLO_CARRIAGE_CONTROL);
		position += Len.VLO_CARRIAGE_CONTROL;
		MarshalByte.writeString(buffer, position, vloData, Len.VLO_DATA);
		return buffer;
	}

	public void setVloCarriageControlFormatted(String vloCarriageControl) {
		this.vloCarriageControl = Trunc.toUnsignedNumeric(vloCarriageControl, Len.VLO_CARRIAGE_CONTROL);
	}

	public short getVloCarriageControl() {
		return NumericDisplay.asShort(this.vloCarriageControl);
	}

	public void setVloData(String vloData) {
		this.vloData = Functions.subString(vloData, Len.VLO_DATA);
	}

	public String getVloData() {
		return this.vloData;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoVpsLaser198RecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoVpsLaser198RecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_VPS_LASER198_RECORD;
	}

	@Override
	public IBuffer copy() {
		OVpsLaser198FileTO copyTO = new OVpsLaser198FileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VLO_CARRIAGE_CONTROL = 1;
		public static final int VLO_DATA = 198;
		public static final int O_VPS_LASER198_RECORD = VLO_CARRIAGE_CONTROL + VLO_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
