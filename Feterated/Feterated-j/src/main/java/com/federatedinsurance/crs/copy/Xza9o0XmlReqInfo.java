/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZA9O0-XML-REQ-INFO<br>
 * Variable: XZA9O0-XML-REQ-INFO from copybook XZ0A90O0<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xza9o0XmlReqInfo {

	//==== PROPERTIES ====
	//Original name: XZA9O0-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA9O0-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA9O0-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZA9O0-NOT-DT
	private String notDt = DefaultValues.stringVal(Len.NOT_DT);
	//Original name: XZA9O0-EMP-ID
	private String empId = DefaultValues.stringVal(Len.EMP_ID);
	//Original name: XZA9O0-ACT-TYP-CD
	private String actTypCd = DefaultValues.stringVal(Len.ACT_TYP_CD);
	//Original name: XZA9O0-DOC-DES
	private String docDes = DefaultValues.stringVal(Len.DOC_DES);
	//Original name: XZA9O0-TTY-CO-IND
	private char ttyCoInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setXmlReqInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		actNotTypCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		notDt = MarshalByte.readString(buffer, position, Len.NOT_DT);
		position += Len.NOT_DT;
		empId = MarshalByte.readString(buffer, position, Len.EMP_ID);
		position += Len.EMP_ID;
		actTypCd = MarshalByte.readString(buffer, position, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		docDes = MarshalByte.readString(buffer, position, Len.DOC_DES);
		position += Len.DOC_DES;
		ttyCoInd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getXmlReqInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, notDt, Len.NOT_DT);
		position += Len.NOT_DT;
		MarshalByte.writeString(buffer, position, empId, Len.EMP_ID);
		position += Len.EMP_ID;
		MarshalByte.writeString(buffer, position, actTypCd, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		MarshalByte.writeString(buffer, position, docDes, Len.DOC_DES);
		position += Len.DOC_DES;
		MarshalByte.writeChar(buffer, position, ttyCoInd);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setNotDt(String notDt) {
		this.notDt = Functions.subString(notDt, Len.NOT_DT);
	}

	public String getNotDt() {
		return this.notDt;
	}

	public void setEmpId(String empId) {
		this.empId = Functions.subString(empId, Len.EMP_ID);
	}

	public String getEmpId() {
		return this.empId;
	}

	public void setActTypCd(String actTypCd) {
		this.actTypCd = Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	public String getActTypCd() {
		return this.actTypCd;
	}

	public void setDocDes(String docDes) {
		this.docDes = Functions.subString(docDes, Len.DOC_DES);
	}

	public String getDocDes() {
		return this.docDes;
	}

	public void setTtyCoInd(char ttyCoInd) {
		this.ttyCoInd = ttyCoInd;
	}

	public char getTtyCoInd() {
		return this.ttyCoInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int NOT_DT = 10;
		public static final int EMP_ID = 6;
		public static final int ACT_TYP_CD = 2;
		public static final int DOC_DES = 240;
		public static final int TTY_CO_IND = 1;
		public static final int XML_REQ_INFO = CSR_ACT_NBR + NOT_PRC_TS + ACT_NOT_TYP_CD + NOT_DT + EMP_ID + ACT_TYP_CD + DOC_DES + TTY_CO_IND;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
