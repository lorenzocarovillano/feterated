/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90B0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90b0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int BO_GET_NOT_DAYS_RQR_ROW_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90b0() {
	}

	public LServiceContractAreaXz0x90b0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setBiCsrActNbr(String biCsrActNbr) {
		writeString(Pos.BI_CSR_ACT_NBR, biCsrActNbr, Len.BI_CSR_ACT_NBR);
	}

	/**Original name: XZT9BI-CSR-ACT-NBR<br>*/
	public String getBiCsrActNbr() {
		return readString(Pos.BI_CSR_ACT_NBR, Len.BI_CSR_ACT_NBR);
	}

	public void setBiNotPrcTs(String biNotPrcTs) {
		writeString(Pos.BI_NOT_PRC_TS, biNotPrcTs, Len.BI_NOT_PRC_TS);
	}

	/**Original name: XZT9BI-NOT-PRC-TS<br>*/
	public String getBiNotPrcTs() {
		return readString(Pos.BI_NOT_PRC_TS, Len.BI_NOT_PRC_TS);
	}

	public void setBiActNotTypCd(String biActNotTypCd) {
		writeString(Pos.BI_ACT_NOT_TYP_CD, biActNotTypCd, Len.BI_ACT_NOT_TYP_CD);
	}

	/**Original name: XZT9BI-ACT-NOT-TYP-CD<br>*/
	public String getBiActNotTypCd() {
		return readString(Pos.BI_ACT_NOT_TYP_CD, Len.BI_ACT_NOT_TYP_CD);
	}

	public void setBiUserid(String biUserid) {
		writeString(Pos.BI_USERID, biUserid, Len.BI_USERID);
	}

	/**Original name: XZT9BI-USERID<br>*/
	public String getBiUserid() {
		return readString(Pos.BI_USERID, Len.BI_USERID);
	}

	public String getBiUseridFormatted() {
		return Functions.padBlanks(getBiUserid(), Len.BI_USERID);
	}

	public void setBoPolNbr(int boPolNbrIdx, String boPolNbr) {
		int position = Pos.xzt9boPolNbr(boPolNbrIdx - 1);
		writeString(position, boPolNbr, Len.BO_POL_NBR);
	}

	/**Original name: XZT9BO-POL-NBR<br>*/
	public String getBoPolNbr(int boPolNbrIdx) {
		int position = Pos.xzt9boPolNbr(boPolNbrIdx - 1);
		return readString(position, Len.BO_POL_NBR);
	}

	public void setBoNbrOfNotDay(int boNbrOfNotDayIdx, int boNbrOfNotDay) {
		int position = Pos.xzt9boNbrOfNotDay(boNbrOfNotDayIdx - 1);
		writeInt(position, boNbrOfNotDay, Len.Int.BO_NBR_OF_NOT_DAY);
	}

	/**Original name: XZT9BO-NBR-OF-NOT-DAY<br>*/
	public int getBoNbrOfNotDay(int boNbrOfNotDayIdx) {
		int position = Pos.xzt9boNbrOfNotDay(boNbrOfNotDayIdx - 1);
		return readNumDispInt(position, Len.BO_NBR_OF_NOT_DAY);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT90B_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int BI_CSR_ACT_NBR = XZT90B_SERVICE_INPUTS;
		public static final int BI_NOT_PRC_TS = BI_CSR_ACT_NBR + Len.BI_CSR_ACT_NBR;
		public static final int BI_ACT_NOT_TYP_CD = BI_NOT_PRC_TS + Len.BI_NOT_PRC_TS;
		public static final int BI_USERID = BI_ACT_NOT_TYP_CD + Len.BI_ACT_NOT_TYP_CD;
		public static final int XZT90B_SERVICE_OUTPUTS = BI_USERID + Len.BI_USERID;
		public static final int BO_GET_NOT_DAYS_RQR_ROW_TB = XZT90B_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt9boGetNotDaysRqrRow(int idx) {
			return BO_GET_NOT_DAYS_RQR_ROW_TB + idx * Len.BO_GET_NOT_DAYS_RQR_ROW;
		}

		public static int xzt9boPolNbr(int idx) {
			return xzt9boGetNotDaysRqrRow(idx);
		}

		public static int xzt9boNbrOfNotDay(int idx) {
			return xzt9boPolNbr(idx) + Len.BO_POL_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int BI_CSR_ACT_NBR = 9;
		public static final int BI_NOT_PRC_TS = 26;
		public static final int BI_ACT_NOT_TYP_CD = 5;
		public static final int BI_USERID = 8;
		public static final int BO_POL_NBR = 25;
		public static final int BO_NBR_OF_NOT_DAY = 5;
		public static final int BO_GET_NOT_DAYS_RQR_ROW = BO_POL_NBR + BO_NBR_OF_NOT_DAY;
		public static final int XZT90B_SERVICE_INPUTS = BI_CSR_ACT_NBR + BI_NOT_PRC_TS + BI_ACT_NOT_TYP_CD + BI_USERID;
		public static final int BO_GET_NOT_DAYS_RQR_ROW_TB = LServiceContractAreaXz0x90b0.BO_GET_NOT_DAYS_RQR_ROW_MAXOCCURS * BO_GET_NOT_DAYS_RQR_ROW;
		public static final int XZT90B_SERVICE_OUTPUTS = BO_GET_NOT_DAYS_RQR_ROW_TB;
		public static final int L_SERVICE_CONTRACT_AREA = XZT90B_SERVICE_INPUTS + XZT90B_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int BO_NBR_OF_NOT_DAY = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
