/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.WsCharacterTypeSw;
import com.federatedinsurance.crs.ws.enums.WsEndOfObjCursor1Sw;

/**Original name: WS-SPECIFIC-SWITCHES<br>
 * Variable: WS-SPECIFIC-SWITCHES from program CAWPCORC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSpecificSwitches {

	//==== PROPERTIES ====
	//Original name: WS-FATAL-ERROR
	private boolean wsFatalError = false;
	//Original name: WS-END-OF-OBJ-CURSOR1-SW
	private WsEndOfObjCursor1Sw wsEndOfObjCursor1Sw = new WsEndOfObjCursor1Sw();
	//Original name: WS-CHARACTER-TYPE-SW
	private WsCharacterTypeSw wsCharacterTypeSw = new WsCharacterTypeSw();

	//==== METHODS ====
	public void setWsFatalError(boolean wsFatalError) {
		this.wsFatalError = wsFatalError;
	}

	public boolean isWsFatalError() {
		return this.wsFatalError;
	}

	public WsCharacterTypeSw getWsCharacterTypeSw() {
		return wsCharacterTypeSw;
	}

	public WsEndOfObjCursor1Sw getWsEndOfObjCursor1Sw() {
		return wsEndOfObjCursor1Sw;
	}
}
