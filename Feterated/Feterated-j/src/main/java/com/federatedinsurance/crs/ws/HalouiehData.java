/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalUowPrcSeq;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallpst;
import com.federatedinsurance.crs.ws.enums.WsBoActionSw;
import com.federatedinsurance.crs.ws.enums.WsEndOfCur1Sw;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALOUIEH<br>
 * Generated as a class for rule WS.<br>*/
public class HalouiehData {

	//==== PROPERTIES ====
	//Original name: WS-END-OF-CUR1-SW
	private WsEndOfCur1Sw wsEndOfCur1Sw = new WsEndOfCur1Sw();
	/**Original name: WS-BO-ACTION-SW<br>
	 * <pre>* SWITCH USED TO DETERMINE WHICH ACTION TO SET FOR BO PROCESSING.</pre>*/
	private WsBoActionSw wsBoActionSw = new WsBoActionSw();
	//Original name: WS-WORK-FIELDS
	private WsWorkFieldsHalouieh wsWorkFields = new WsWorkFieldsHalouieh();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: HALLPST
	private Hallpst hallpst = new Hallpst();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-GENERIC-REC
	private WsGenericRec wsGenericRec = new WsGenericRec();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();
	//Original name: DCLHAL-UOW-PRC-SEQ
	private DclhalUowPrcSeq dclhalUowPrcSeq = new DclhalUowPrcSeq();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalUowPrcSeq getDclhalUowPrcSeq() {
		return dclhalUowPrcSeq;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallpst getHallpst() {
		return hallpst;
	}

	public WsBoActionSw getWsBoActionSw() {
		return wsBoActionSw;
	}

	public WsEndOfCur1Sw getWsEndOfCur1Sw() {
		return wsEndOfCur1Sw;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsGenericRec getWsGenericRec() {
		return wsGenericRec;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsWorkFieldsHalouieh getWsWorkFields() {
		return wsWorkFields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_APPLID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
