/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import java.util.NoSuchElementException;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.bphx.ctu.af.util.tokenizer.CharTokenizer;
import com.bphx.ctu.af.util.tokenizer.IStringTokenizer;
import com.federatedinsurance.crs.commons.data.dao.ClientTabVDao;
import com.federatedinsurance.crs.commons.data.dao.CltObjRelationVDao;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.copy.Cawlc008;
import com.federatedinsurance.crs.copy.Cawlcorc;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.ws.CawpcorcData;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.DfhcommareaCawpcorc;
import com.federatedinsurance.crs.ws.WsClientProcessing;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsSpecificWorkAreasCawpcorc;
import com.federatedinsurance.crs.ws.enums.WsObjRelCltFilterType;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: CAWPCORC<br>
 * <pre>AUTHOR.  A. WITIKKO
 * DATE-WRITTEN. JAN 2004.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - CLT OBJECT RELATED CLIENT BPO               **
 * *                                                             **
 * * AVAILABLE FUNCTIONS:                                        **
 * *                                                             **
 * * (C) = COMPULSORY                                            **
 * * (O) = OPTIONAL                                              **
 * *                                                             **
 * * PLATFORM - I-BASE                                           **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -  BUSINESS PROCESS OBJECT TO GET THE DATA FROM     **
 * *            CLIENT_TAB_V CLIENTS RELATED TO OBJECTS.         **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED IN THE FOLLOW-**
 * *                       ING WAYS:                             **
 * *                       CAWPCORC IS STARTED AS A ROOT OBJECT  **
 * *                                                             **
 * * DATA ACCESS METHODS - UMT STORAGE RECORDS.                  **
 * *                       DB2 TABLES.                           **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #      DATE      PROG     DESCRIPTION                    **
 * * --------  --------  -------  ------------------------------ **
 * * 66970     01/27/04  E404ASW  INITIAL CODING                 **
 * *TO07602-18 02/22/10  E404EJD  ADDED CALL FOR COOWNER DATA    **
 * ****************************************************************</pre>*/
public class Cawpcorc extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private CltObjRelationVDao cltObjRelationVDao = new CltObjRelationVDao(dbAccessStatus);
	private ClientTabVDao clientTabVDao = new ClientTabVDao(dbAccessStatus);
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private CawpcorcData ws = new CawpcorcData();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaCawpcorc dfhcommarea = new DfhcommareaCawpcorc();

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaCawpcorc dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		registerArgListeners();
		mainline();
		return1();
		deleteArgListeners();
		return 0;
	}

	public static Cawpcorc getInstance() {
		return (Programs.getInstance(Cawpcorc.class));
	}

	/**Original name: 0000-MAINLINE_FIRST_SENTENCES<br>*/
	private void mainline() {
		// COB_CODE: MOVE DFHCOMMAREA(1:LENGTH OF WS-HALLUBOC-LINKAGE)
		//                                       TO WS-HALLUBOC-LINKAGE.
		ws.getWsHallubocLinkage().setWsUbocLinkageFormatted(dfhcommarea.getDfhcommareaFormatted().substring((1) - 1, Dfhcommarea.Len.DFHCOMMAREA));
		// COB_CODE: PERFORM 1000-INITIALIZE.
		initialize();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//             OR FATAL-ERROR
		//               GO TO 0000-RETURN
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsSpecificSwitches().isWsFatalError()) {
			// COB_CODE: GO TO 0000-RETURN
			return1();
		}
		// COB_CODE: PERFORM 4000-READ-REQ-MSG-UMT
		//             UNTIL UBOC-HALT-AND-RETURN
		//               OR HALRURQA-REC-NOT-FOUND.
		while (!(ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound())) {
			readReqMsgUmt();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//             OR
		//              FATAL-ERROR
		//               GO TO 0000-RETURN
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsSpecificSwitches().isWsFatalError()) {
			// COB_CODE: GO TO 0000-RETURN
			return1();
		}
	}

	/**Original name: 0000-RETURN<br>
	 * <pre>****************************************************************
	 * **0000-RETURN  IS THE EXIT PARAGRAPH OF CAWPCORC. THE CONTROL***
	 * **OF CAWPCORC COMES TO THIS PARAGRAPH IF ANY ERROR OCCURES   ***
	 * **DURING EXECUTION OF THE PROGRAM OR THE PROGRAM IS          ***
	 * **SUCCESSFULLY EXECUTED.                                     ***
	 * ****************************************************************</pre>*/
	private void return1() {
		// COB_CODE: PERFORM 8000-TERMINATE.
		terminate();
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 1000-INITIALIZE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 * **1000-INITIALIZE SECTION                                     ***
	 * **PERFORM ANY REQUIRED INITIALIZATION PROCESSING.             ***
	 * **                                                            ***
	 * **1. MOVE THE DFHCOMMAREA TO WS-HALLUBOC-LINKAGE.             ***
	 * **2. CLEAR ERROR/WARNING MESSAGE ARRAY AND ERROR              ***
	 * **   HANDLING FIELDS.                                         ***
	 * **3. VERIFY THAT THE PASSED IN COMMAREA CONTAINS              ***
	 * **   ALL REQUIRED FIELDS.                                     ***
	 * **4. GET THE CURRENT S3+ TIMESTAMP.                           ***
	 * *****************************************************************</pre>*/
	private void initialize() {
		// COB_CODE: SET NOT-FATAL-ERROR         TO TRUE.
		ws.getWsSpecificSwitches().setWsFatalError(false);
		// COB_CODE: SET UBOC-UOW-OK             TO TRUE.
		ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setUbocUowOk();
		// COB_CODE: SET HALRURQA-REC-FOUND      TO TRUE.
		ws.getWsHalrurqaLinkage().getRecFoundSw().setFound();
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-INITIALIZE-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-INITIALIZE-X
			return;
		}
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-INITIALIZE-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-INITIALIZE-X
			return;
		}
		// COB_CODE: PERFORM 1100-GET-CUR-TIMESTAMP.
		getCurTimestamp();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-INITIALIZE-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-INITIALIZE-X
			return;
		}
	}

	/**Original name: 1100-GET-CUR-TIMESTAMP_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 * **GET CURRENT TIMESTAMP.                                     ***
	 * ****************************************************************</pre>*/
	private void getCurTimestamp() {
		Xpiodat xpiodat = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE SPACES                 TO DATE-STRUCTURE.
		ws.getDateStructure().initDateStructureSpaces();
		// COB_CODE: MOVE 'C'                    TO DATE-FUNCTION.
		ws.getDateStructure().setDateFunctionFormatted("C");
		// COB_CODE: MOVE SPACES                 TO DATE-LANGUAGE.
		ws.getDateStructure().setDateLanguage("");
		// COB_CODE: MOVE 'S3'                   TO DATE-INP-FORMAT.
		ws.getDateStructure().setDateInpFormat("S3");
		// COB_CODE: MOVE 'B'                    TO DATE-OUT-FORMAT.
		ws.getDateStructure().setDateOutFormat("B");
		// COB_CODE: MOVE SPACES                 TO DATE-INPUT.
		ws.getDateStructure().setDateInput("");
		//*************************************************************         ***
		//                                                            *         ...
		//    XPXCDAT - COPYBOOK USED TO LINK TO XPIODAT, THE ON-LINE *    04/11/01
		//              COBOL DATE ROUTINE.                           *    XPXCDAT
		//                                                            *       LV004
		// IR REF    DATE   RELEASE            DESCRIPTION            *    00006
		// XXXXXX  DDMMMYY    X.X    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX *    00007
		// XXXXXX  16JAN03  DNF     CHANGED TO R70 STRUTURE. FEDERATED*    00008
		// 002607  06JUL00  IND9176 ADDED CODE IN ORDER TO MAKE IT A  *    00008
		//                          DYNAMIC CALL TO XPIODAT.          *    00009
		//*************************************************************    00010
		//    MOVE LENGTH OF DATE-STRUCTURE TO DATE-STRUCTURE-LENGTH.      00012
		//**  EXEC CICS LINK                                               00014
		//**       PROGRAM  ('XPIODAT')                                    00015
		//**       COMMAREA (DATE-STRUCTURE)                               00016
		//**       LENGTH   (DATE-STRUCTURE-LENGTH)                        00017
		//**       NOHANDLE                                                00018
		//**       END-EXEC.                                               00019
		//**                                                               00020
		//    MOVE 'XPIODAT ' TO WS-XPIODAT-MODULE.                        00021
		//    CALL WS-XPIODAT-MODULE USING                                 00022
		//         DFHEIBLK                                                00023
		//         DFHCOMMAREA                                             00024
		//         DATE-STRUCTURE.                                         00025
		// COB_CODE: CALL 'XPIODAT' USING DATE-STRUCTURE.
		xpiodat = Xpiodat.getInstance();
		xpiodat.run(ws.getDateStructure());
		// COB_CODE: IF DATE-RETURN-CODE NOT EQUAL 'OK'
		//             OR DATE-OUTPUT EQUAL SPACES
		//               GO TO 1100-GET-CUR-TIMESTAMP-X
		//           END-IF.
		if (!Conditions.eq(ws.getDateStructure().getDateReturnCode(), "OK") || Characters.EQ_SPACE.test(ws.getDateStructure().getDateOutput())) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-IAP-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalIapFailed();
			// COB_CODE: MOVE '1100-GET-CUR-TIMESTAMP'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1100-GET-CUR-TIMESTAMP");
			// COB_CODE: MOVE 'IAP DATE CALL FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("IAP DATE CALL FAILED");
			// COB_CODE: STRING 'DATE-INPUT=' DATE-INPUT ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "DATE-INPUT=", ws.getDateStructure().getDateInputFormatted(),
					";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1100-GET-CUR-TIMESTAMP-X
			return;
		}
		// COB_CODE: MOVE DATE-OUTPUT            TO WS-SE3-CUR-ISO-DATE-TIME.
		ws.setWsSe3CurIsoDateTimeFormatted(ws.getDateStructure().getDateOutputFormatted());
	}

	/**Original name: 4000-READ-REQ-MSG-UMT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 * ** READ THE REQUEST UMT TO SEE IF THIS PROGRAM SHOULD         ***
	 * ** ACTUALLY DO ANYTHING.                                      ***
	 * *****************************************************************</pre>*/
	private void readReqMsgUmt() {
		// COB_CODE: ADD 1                       TO WS-URQM-SEQ-NUM.
		ws.getWsWorkArea().setUrqmSeqNum(Trunc.toShort(1 + ws.getWsWorkArea().getUrqmSeqNum(), 4));
		// COB_CODE: SET HALRURQA-READ-FUNC      TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM          TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWsSpecificWorkAreas().getBusObjNm());
		// COB_CODE: MOVE WS-URQM-SEQ-NUM        TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(TruncAbs.toInt(ws.getWsWorkArea().getUrqmSeqNum(), 5));
		// COB_CODE: MOVE LENGTH OF CWORC-CLT-OBJ-RELATION-ROW
		//                                       TO HALRURQA-BUS-OBJ-DATA-LENGTH.
		ws.getWsHalrurqaLinkage().setBusObjDataLength(((short) Cawlcorc.Len.CLT_OBJ_RELATION_ROW));
		// COB_CODE: INITIALIZE CWORC-CLT-OBJ-RELATION-ROW.
		initCltObjRelationRow();
		// COB_CODE: INITIALIZE WS-BUS-OBJ-DATA-ROW.
		ws.getWsSpecificWorkAreas().setBusObjDataRow("");
		// COB_CODE: MOVE CWORC-CLT-OBJ-RELATION-ROW
		//                                       TO WS-BUS-OBJ-DATA-ROW.
		ws.getWsSpecificWorkAreas().setBusObjDataRow(ws.getCawlcorc().getCltObjRelationRowFormatted());
		// COB_CODE: PERFORM 7000-CALL-HALRURQA.
		callHalrurqa();
		// COB_CODE: MOVE WS-BUS-OBJ-DATA-ROW    TO CWORC-CLT-OBJ-RELATION-ROW.
		ws.getCawlcorc().setCltObjRelationRowFormatted(ws.getWsSpecificWorkAreas().getBusObjDataRowFormatted());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-READ-REQ-MSG-UMT-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-READ-REQ-MSG-UMT-X
			return;
		}
		//* IF NO ROW WAS PASSED IN THE REQUEST UMT, LOOK AT LINKAGE
		// COB_CODE: IF SW-NO-INPUT-FROM-UMT
		//               PERFORM 4100-READ-FROM-LINKAGE
		//           END-IF.
		if (!ws.getSwitches().isInputFromUmtFlag()) {
			// COB_CODE: PERFORM 4100-READ-FROM-LINKAGE
			readFromLinkage();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-READ-REQ-MSG-UMT-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-READ-REQ-MSG-UMT-X
			return;
		}
		//*IF THERE WAS NO DATA IN THE REQUEST UMT OR LINKAGE, EXIT
		// COB_CODE: IF SW-NO-INPUT-FROM-UMT
		//             AND
		//              SW-NO-INPUT-FROM-LINKAGE
		//               GO TO 4000-READ-REQ-MSG-UMT-X
		//           END-IF.
		if (!ws.getSwitches().isInputFromUmtFlag() && !ws.getSwitches().isInputFromLinkageFlag()) {
			// COB_CODE: GO TO 4000-READ-REQ-MSG-UMT-X
			return;
		}
		// COB_CODE: SET HALRRESP-REC-FOUND      TO TRUE.
		ws.getWsHalrrespLinkage().getRecFoundSw().setFound();
		// COB_CODE: IF HALRURQA-ACTION-CODE = 'FETCH'
		//             OR
		//              HALRURQA-ACTION-CODE = 'FETCH_PRIORITY'
		//             OR
		//              SW-INPUT-FROM-LINKAGE
		//                   OR HALRRESP-REC-NOT-FOUND
		//           END-IF.
		if (Conditions.eq(ws.getWsHalrurqaLinkage().getActionCode(), "FETCH")
				|| Conditions.eq(ws.getWsHalrurqaLinkage().getActionCode(), "FETCH_PRIORITY") || ws.getSwitches().isInputFromLinkageFlag()) {
			// COB_CODE: MOVE ZERO               TO WS-UDAT-SEQ-NUM
			ws.getWsWorkArea().setUdatSeqNum(((short) 0));
			// COB_CODE: PERFORM 4500-READ-RESP-MSG-UMT
			//             UNTIL UBOC-HALT-AND-RETURN
			//               OR HALRRESP-REC-NOT-FOUND
			while (!(ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
					|| ws.getWsHalrrespLinkage().getRecFoundSw().isHalrrespRecNotFound())) {
				readRespMsgUmt();
			}
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-READ-REQ-MSG-UMT-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-READ-REQ-MSG-UMT-X
			return;
		}
	}

	/**Original name: 4100-READ-FROM-LINKAGE_FIRST_SENTENCES<br>*/
	private void readFromLinkage() {
		// COB_CODE: IF UBOC-APP-DATA-BUFFER EQUAL SPACES OR LOW-VALUES
		//               GO TO 4100-READ-FROM-LINKAGE-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getAppDataBuffer())
				|| Characters.EQ_LOW.test(ws.getWsHallubocLinkage().getAppDataBuffer(), Dfhcommarea.Len.APP_DATA_BUFFER)) {
			// COB_CODE: GO TO 4100-READ-FROM-LINKAGE-X
			return;
		}
		// COB_CODE: SET SW-INPUT-FROM-LINKAGE   TO TRUE.
		ws.getSwitches().setInputFromLinkageFlag(true);
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER
		//                (1:LENGTH OF CWORC-CLT-OBJ-RELATION-ROW)
		//                                       TO CWORC-CLT-OBJ-RELATION-ROW.
		ws.getCawlcorc().setCltObjRelationRowFormatted(
				ws.getWsHallubocLinkage().getAppDataBufferFormatted().substring((1) - 1, Cawlcorc.Len.CLT_OBJ_RELATION_ROW));
	}

	/**Original name: 4500-READ-RESP-MSG-UMT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 * * READ THE RESPONSE MESSAGE UMT FOR CLT_OBJ_RELATION_V. IF A   **
	 * * ROW EXISTS THEN USE THE INFORMATION TO SEE IF WE NEED TO GET **
	 * * CLIENT INFORMATION FOR A CLIENT RELATED TO THE OBJECT.       **
	 * *****************************************************************</pre>*/
	private void readRespMsgUmt() {
		// COB_CODE: MOVE CWORC-FILTER-TYPE      TO WS-OBJ-REL-CLT-FILTER-TYPE.
		ws.getWsWorkArea().getObjRelCltFilterType().setObjRelCltFilterType(ws.getCawlcorc().getFilterType());
		// COB_CODE: ADD 1                       TO WS-UDAT-SEQ-NUM.
		ws.getWsWorkArea().setUdatSeqNum(Trunc.toShort(1 + ws.getWsWorkArea().getUdatSeqNum(), 4));
		// COB_CODE: SET HALRRESP-READ-FUNC      TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: IF WS-ORC-RETURN-COWN
		//               MOVE WS-OWN-OBJ-NM      TO HALRRESP-BUS-OBJ-NM
		//           ELSE
		//               MOVE WS-008-BUS-OBJ-NM  TO HALRRESP-BUS-OBJ-NM
		//           END-IF.
		if (ws.getWsWorkArea().getObjRelCltFilterType().isCown()) {
			// COB_CODE: MOVE WS-OWN-OBJ-NM      TO HALRRESP-BUS-OBJ-NM
			ws.getWsHalrrespLinkage().setBusObjNm(ws.getWsSpecificWorkAreas().getOwnObjNm());
		} else {
			// COB_CODE: MOVE WS-008-BUS-OBJ-NM  TO HALRRESP-BUS-OBJ-NM
			ws.getWsHalrrespLinkage().setBusObjNm(ws.getWsSpecificWorkAreas().getWs008BusObjNm());
		}
		// COB_CODE: MOVE WS-UDAT-SEQ-NUM        TO HALRRESP-REC-SEQ.
		ws.getWsHalrrespLinkage().setRecSeq(TruncAbs.toInt(ws.getWsWorkArea().getUdatSeqNum(), 5));
		// COB_CODE: MOVE LENGTH OF CW08C-CLT-OBJ-RELATION-ROW
		//                                       TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(((short) Cawlc008.Len.CLT_OBJ_RELATION_ROW));
		// COB_CODE: INITIALIZE CW08C-CLT-OBJ-RELATION-ROW.
		initCltObjRelationRow1();
		// COB_CODE: INITIALIZE WS-BUS-OBJ-DATA-ROW.
		ws.getWsSpecificWorkAreas().setBusObjDataRow("");
		// COB_CODE: MOVE CW08C-CLT-OBJ-RELATION-ROW
		//                                       TO WS-BUS-OBJ-DATA-ROW.
		ws.getWsSpecificWorkAreas().setBusObjDataRow(ws.getCawlc008().getCltObjRelationRowFormatted());
		// COB_CODE: PERFORM 6000-CALL-HALRRESP.
		callHalrresp();
		// COB_CODE: MOVE WS-BUS-OBJ-DATA-ROW    TO CW08C-CLT-OBJ-RELATION-ROW.
		ws.getCawlc008().setCltObjRelationRowFormatted(ws.getWsSpecificWorkAreas().getBusObjDataRowFormatted());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//             OR
		//              HALRRESP-REC-NOT-FOUND
		//               GO TO 4500-READ-RESP-MSG-UMT-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsHalrrespLinkage().getRecFoundSw().isHalrrespRecNotFound()) {
			// COB_CODE: GO TO 4500-READ-RESP-MSG-UMT-X
			return;
		}
		// COB_CODE: PERFORM 4510-FIND-RELATED-CLIENTS.
		findRelatedClients();
	}

	/**Original name: 4510-FIND-RELATED-CLIENTS_FIRST_SENTENCES<br>*/
	private void findRelatedClients() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE SPACES                 TO WS-PRIOR-CLIENT-ID.
		ws.getWsClientProcessing().setPriorClientId("");
		// COB_CODE: MOVE CW08C-CIOR-SHW-OBJ-KEY TO CW08H-CIOR-SHW-OBJ-KEY.
		ws.getCw08hCltObjRelationRow().setCiorShwObjKey(ws.getCawlc008().getCltObjRelationData().getCiorShwObjKey());
		// COB_CODE: EXEC SQL
		//              OPEN WS-OBJ-CURSOR1
		//           END-EXEC.
		cltObjRelationVDao.openWsObjCursor1(ws.getCw08hCltObjRelationRow().getCiorShwObjKey());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 4510-FIND-RELATED-CLIENTS-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'CLT_OBJ_RELATION_V'
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("CLT_OBJ_RELATION_V");
			// COB_CODE: MOVE '4510-FIND-RELATED-CLIENTS'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4510-FIND-RELATED-CLIENTS");
			// COB_CODE: MOVE 'OPEN WS-OBJ-CURSOR1 FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN WS-OBJ-CURSOR1 FAILED");
			// COB_CODE: STRING 'CW08H-CIOR-SHW-OBJ-KEY='
			//                  CW08H-CIOR-SHW-OBJ-KEY ';'
			//                 DELIMITED BY SIZE
			//                 INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CW08H-CIOR-SHW-OBJ-KEY=",
					ws.getCw08hCltObjRelationRow().getCiorShwObjKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4510-FIND-RELATED-CLIENTS-X
			return;
		}
		// COB_CODE: SET WS-NOT-END-OF-OBJ-CURSOR1
		//                                       TO TRUE.
		ws.getWsSpecificSwitches().getWsEndOfObjCursor1Sw().setNotEndOfObjCursor1();
		// COB_CODE: PERFORM 4511-FETCH-OBJ-CURSOR1
		//             UNTIL UBOC-HALT-AND-RETURN
		//                OR WS-END-OF-OBJ-CURSOR1.
		while (!(ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsSpecificSwitches().getWsEndOfObjCursor1Sw().isEndOfObjCursor1())) {
			fetchObjCursor1();
		}
		// COB_CODE: EXEC SQL
		//              CLOSE WS-OBJ-CURSOR1
		//           END-EXEC.
		cltObjRelationVDao.closeWsObjCursor1();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 4510-FIND-RELATED-CLIENTS-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING      TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'CLT_OBJ_RELATION_V'
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("CLT_OBJ_RELATION_V");
			// COB_CODE: MOVE '4510-FIND-RELATED-CLIENTS'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4510-FIND-RELATED-CLIENTS");
			// COB_CODE: MOVE 'CLOSE WS-OBJ-CURSOR1 FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE WS-OBJ-CURSOR1 FAILED");
			// COB_CODE: STRING 'CW08H-CIOR-SHW-OBJ-KEY='
			//                  CW08H-CIOR-SHW-OBJ-KEY ';'
			//                 DELIMITED BY SIZE
			//                 INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CW08H-CIOR-SHW-OBJ-KEY=",
					ws.getCw08hCltObjRelationRow().getCiorShwObjKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4510-FIND-RELATED-CLIENTS-X
			return;
		}
	}

	/**Original name: 4511-FETCH-OBJ-CURSOR1_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 * **FETCH ROW DATA USING WS-OBJ-CURSOR1                        ***
	 * ****************************************************************</pre>*/
	private void fetchObjCursor1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//             FETCH WS-OBJ-CURSOR1
		//              INTO :CW08H-TCH-OBJECT-KEY,
		//                   :CW08H-HISTORY-VLD-NBR,
		//                   :CW08H-CIOR-EFF-DT,
		//                   :CW08H-OBJ-SYS-ID,
		//                   :CW08H-CIOR-OBJ-SEQ-NBR,
		//                   :CW08H-CLIENT-ID,
		//                   :CW08H-RLT-TYP-CD,
		//                   :CW08H-OBJ-CD,
		//                   :CW08H-CIOR-SHW-OBJ-KEY,
		//                   :CW08H-ADR-SEQ-NBR,
		//                   :CW08H-USER-ID,
		//                   :CW08H-STATUS-CD,
		//                   :CW08H-TERMINAL-ID,
		//                   :CW08H-CIOR-EXP-DT,
		//                   :CW08H-CIOR-EFF-ACY-TS,
		//                   :CW08H-CIOR-EXP-ACY-TS
		//           END-EXEC.
		cltObjRelationVDao.fetchWsObjCursor1(ws.getCw08hCltObjRelationRow());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   END-IF
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 4511-FETCH-OBJ-CURSOR1-X
		//               WHEN OTHER
		//                   GO TO 4511-FETCH-OBJ-CURSOR1-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: PERFORM 4511A-CHECK-TO-RETURN
			aCheckToReturn();
			// COB_CODE: IF SW-RETURN-CLIENT
			//               PERFORM 4900-GET-RELATED-CLIENT-DATA
			//           END-IF
			if (ws.getSwitches().isReturnClientFlag()) {
				// COB_CODE: PERFORM 4511B-MOVE-OBJ-DATA
				bMoveObjData();
				// COB_CODE: MOVE CW08H-CLIENT-ID
				//                           TO WS-PRIOR-CLIENT-ID
				//                              CW02H-CLIENT-ID
				ws.getWsClientProcessing().setPriorClientId(ws.getCw08hCltObjRelationRow().getClientId());
				ws.getCw02hClientTabRow().setClientId(ws.getCw08hCltObjRelationRow().getClientId());
				// COB_CODE: PERFORM 4900-GET-RELATED-CLIENT-DATA
				getRelatedClientData();
			}
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-OBJ-CURSOR1
			//                               TO TRUE
			ws.getWsSpecificSwitches().getWsEndOfObjCursor1Sw().setEndOfObjCursor1();
			// COB_CODE: GO TO 4511-FETCH-OBJ-CURSOR1-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'CLT_OBJ_RELATION_V'
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("CLT_OBJ_RELATION_V");
			// COB_CODE: MOVE '4511-FETCH-OBJ-CURSOR1'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4511-FETCH-OBJ-CURSOR1");
			// COB_CODE: MOVE 'FETCH FROM WS-OBJ-CURSOR1 FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH FROM WS-OBJ-CURSOR1 FAILED");
			// COB_CODE: STRING 'CW08H-CIOR-SHW-OBJ-KEY='
			//                  CW08H-CIOR-SHW-OBJ-KEY ';'
			//                 DELIMITED BY SIZE
			//                 INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CW08H-CIOR-SHW-OBJ-KEY=",
					ws.getCw08hCltObjRelationRow().getCiorShwObjKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4511-FETCH-OBJ-CURSOR1-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 4511-FETCH-OBJ-CURSOR1-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4511-FETCH-OBJ-CURSOR1-X
			return;
		}
	}

	/**Original name: 4511A-CHECK-TO-RETURN_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 * * THIS PARAGRAPH FILTERS OUT THE DATA TO BE WRITTEN TO THE    **
	 * * RESPONSE QUEUE BASED ON THE INPUT REQUEST AND THE VALUES OF **
	 * * THE DATA FIELDS.                                            **
	 * ****************************************************************</pre>*/
	private void aCheckToReturn() {
		// COB_CODE: SET SW-DONT-RETURN-CLIENT   TO TRUE.
		ws.getSwitches().setReturnClientFlag(false);
		// COB_CODE: EVALUATE TRUE
		//               WHEN WS-ORC-RETURN-ALL
		//                   END-IF
		//               WHEN WS-ORC-RETURN-OWN
		//                   END-IF
		//               WHEN WS-ORC-RETURN-FNI
		//                   END-IF
		//               WHEN WS-ORC-RETURN-OWN-AND-FNI
		//                   END-IF
		//               WHEN WS-ORC-RETURN-COWN
		//                   END-IF
		//               WHEN OTHER
		//                   CONTINUE
		//           END-EVALUATE.
		switch (ws.getWsWorkArea().getObjRelCltFilterType().getObjRelCltFilterType()) {

		case WsObjRelCltFilterType.ALL:// COB_CODE: IF CW08H-CLIENT-ID NOT EQUAL WS-PRIOR-CLIENT-ID
			//                               TO TRUE
			//           END-IF
			if (!Conditions.eq(ws.getCw08hCltObjRelationRow().getClientId(), ws.getWsClientProcessing().getPriorClientId())) {
				// COB_CODE: SET SW-RETURN-CLIENT
				//                           TO TRUE
				ws.getSwitches().setReturnClientFlag(true);
			}
			break;

		case WsObjRelCltFilterType.OWN:// COB_CODE: IF CW08H-RLT-TYP-CD = CF-OWNER
			//             AND
			//              CW08H-CLIENT-ID NOT EQUAL WS-PRIOR-CLIENT-ID
			//                               TO TRUE
			//           END-IF
			if (Conditions.eq(ws.getCw08hCltObjRelationRow().getRltTypCd(), ws.getConstantFields().getOwner())
					&& !Conditions.eq(ws.getCw08hCltObjRelationRow().getClientId(), ws.getWsClientProcessing().getPriorClientId())) {
				// COB_CODE: SET SW-RETURN-CLIENT
				//                           TO TRUE
				ws.getSwitches().setReturnClientFlag(true);
			}
			break;

		case WsObjRelCltFilterType.FNI:// COB_CODE: IF CW08H-RLT-TYP-CD = CF-FIRST-NAMED-INSURED
			//             AND
			//              CW08H-CLIENT-ID NOT EQUAL WS-PRIOR-CLIENT-ID
			//                               TO TRUE
			//           END-IF
			if (Conditions.eq(ws.getCw08hCltObjRelationRow().getRltTypCd(), ws.getConstantFields().getFirstNamedInsured())
					&& !Conditions.eq(ws.getCw08hCltObjRelationRow().getClientId(), ws.getWsClientProcessing().getPriorClientId())) {
				// COB_CODE: SET SW-RETURN-CLIENT
				//                           TO TRUE
				ws.getSwitches().setReturnClientFlag(true);
			}
			break;

		case WsObjRelCltFilterType.OWN_AND_FNI:// COB_CODE: IF (CW08H-RLT-TYP-CD = CF-OWNER
			//             OR
			//              CW08H-RLT-TYP-CD = CF-FIRST-NAMED-INSURED)
			//             AND
			//              CW08H-CLIENT-ID NOT EQUAL WS-PRIOR-CLIENT-ID
			//                               TO TRUE
			//           END-IF
			if ((Conditions.eq(ws.getCw08hCltObjRelationRow().getRltTypCd(), ws.getConstantFields().getOwner())
					|| Conditions.eq(ws.getCw08hCltObjRelationRow().getRltTypCd(), ws.getConstantFields().getFirstNamedInsured()))
					&& !Conditions.eq(ws.getCw08hCltObjRelationRow().getClientId(), ws.getWsClientProcessing().getPriorClientId())) {
				// COB_CODE: SET SW-RETURN-CLIENT
				//                           TO TRUE
				ws.getSwitches().setReturnClientFlag(true);
			}
			break;

		case WsObjRelCltFilterType.COWN:// COB_CODE: IF CW08H-RLT-TYP-CD = CF-COOWNER
			//             AND
			//              CW08H-CLIENT-ID NOT EQUAL WS-PRIOR-CLIENT-ID
			//                               TO TRUE
			//           END-IF
			if (Conditions.eq(ws.getCw08hCltObjRelationRow().getRltTypCd(), ws.getConstantFields().getCoowner())
					&& !Conditions.eq(ws.getCw08hCltObjRelationRow().getClientId(), ws.getWsClientProcessing().getPriorClientId())) {
				// COB_CODE: SET SW-RETURN-CLIENT
				//                           TO TRUE
				ws.getSwitches().setReturnClientFlag(true);
			}
			break;

		default:// COB_CODE: CONTINUE
			//continue
			break;
		}
	}

	/**Original name: 4511B-MOVE-OBJ-DATA_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 * * THIS PARAGRAPH MOVES THE NEEDED DATA FROM THE DCLGEN TO THE **
	 * * COPYBOOK                                                    **
	 * ****************************************************************</pre>*/
	private void bMoveObjData() {
		// COB_CODE: MOVE CW08H-TCH-OBJECT-KEY   TO CW08C-TCH-OBJECT-KEY.
		ws.getCawlc008().getCltObjRelationKey().setTchObjectKey(ws.getCw08hCltObjRelationRow().getTchObjectKey());
		// COB_CODE: MOVE CW08H-HISTORY-VLD-NBR  TO CW08C-HISTORY-VLD-NBR.
		ws.getCawlc008().getCltObjRelationKey().setHistoryVldNbr(TruncAbs.toInt(ws.getCw08hCltObjRelationRow().getHistoryVldNbr(), 5));
		// COB_CODE: MOVE CW08H-CIOR-EFF-DT      TO CW08C-CIOR-EFF-DT.
		ws.getCawlc008().getCltObjRelationKey().setCiorEffDt(ws.getCw08hCltObjRelationRow().getCiorEffDt());
		// COB_CODE: MOVE CW08H-OBJ-SYS-ID       TO CW08C-OBJ-SYS-ID.
		ws.getCawlc008().getCltObjRelationKey().setObjSysId(ws.getCw08hCltObjRelationRow().getObjSysId());
		// COB_CODE: MOVE CW08H-CIOR-OBJ-SEQ-NBR TO CW08C-CIOR-OBJ-SEQ-NBR.
		ws.getCawlc008().getCltObjRelationKey().setCiorObjSeqNbr(TruncAbs.toInt(ws.getCw08hCltObjRelationRow().getCiorObjSeqNbr(), 5));
		// COB_CODE: MOVE CW08H-CLIENT-ID        TO CW08C-CLIENT-ID.
		ws.getCawlc008().getCltObjRelationData().setClientId(ws.getCw08hCltObjRelationRow().getClientId());
		// COB_CODE: MOVE CW08H-RLT-TYP-CD       TO CW08C-RLT-TYP-CD.
		ws.getCawlc008().getCltObjRelationData().setRltTypCd(ws.getCw08hCltObjRelationRow().getRltTypCd());
		// COB_CODE: MOVE CW08H-OBJ-CD           TO CW08C-OBJ-CD.
		ws.getCawlc008().getCltObjRelationData().setObjCd(ws.getCw08hCltObjRelationRow().getObjCd());
		// COB_CODE: MOVE CW08H-CIOR-SHW-OBJ-KEY TO CW08C-CIOR-SHW-OBJ-KEY.
		ws.getCawlc008().getCltObjRelationData().setCiorShwObjKey(ws.getCw08hCltObjRelationRow().getCiorShwObjKey());
		// COB_CODE: MOVE CW08H-ADR-SEQ-NBR      TO CW08C-ADR-SEQ-NBR.
		ws.getCawlc008().getCltObjRelationData().setAdrSeqNbr(TruncAbs.toInt(ws.getCw08hCltObjRelationRow().getAdrSeqNbr(), 5));
		// COB_CODE: MOVE CW08H-USER-ID          TO CW08C-USER-ID.
		ws.getCawlc008().getCltObjRelationData().setUserId(ws.getCw08hCltObjRelationRow().getUserId());
		// COB_CODE: MOVE CW08H-STATUS-CD        TO CW08C-STATUS-CD.
		ws.getCawlc008().getCltObjRelationData().setStatusCd(ws.getCw08hCltObjRelationRow().getStatusCd());
		// COB_CODE: MOVE CW08H-TERMINAL-ID      TO CW08C-TERMINAL-ID.
		ws.getCawlc008().getCltObjRelationData().setTerminalId(ws.getCw08hCltObjRelationRow().getTerminalId());
		// COB_CODE: MOVE CW08H-CIOR-EXP-DT      TO CW08C-CIOR-EXP-DT.
		ws.getCawlc008().getCltObjRelationData().setCiorExpDt(ws.getCw08hCltObjRelationRow().getCiorExpDt());
		// COB_CODE: MOVE CW08H-CIOR-EFF-ACY-TS  TO CW08C-CIOR-EFF-ACY-TS.
		ws.getCawlc008().getCltObjRelationData().setCiorEffAcyTs(ws.getCw08hCltObjRelationRow().getCiorEffAcyTs());
		// COB_CODE: MOVE CW08H-CIOR-EXP-ACY-TS  TO CW08C-CIOR-EXP-ACY-TS.
		ws.getCawlc008().getCltObjRelationData().setCiorExpAcyTs(ws.getCw08hCltObjRelationRow().getCiorExpAcyTs());
	}

	/**Original name: 4900-GET-RELATED-CLIENT-DATA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 * * GET THE CLIENT DATA FOR THE RELATED CLIENT.                  **
	 * *****************************************************************</pre>*/
	private void getRelatedClientData() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//              OPEN WS-CLT-CURSOR1
		//           END-EXEC.
		clientTabVDao.openWsCltCursor1(ws.getCw02hClientTabRow().getClientId());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 4900-GET-RELATED-CLIENT-DATA-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'CLIENT_TAB_V'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("CLIENT_TAB_V");
			// COB_CODE: MOVE '4900-GET-RELATED-CLIENT-DATA'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4900-GET-RELATED-CLIENT-DATA");
			// COB_CODE: MOVE 'OPEN WS-CLT-CURSOR1 FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN WS-CLT-CURSOR1 FAILED");
			// COB_CODE: STRING 'CW02H-CLIENT-ID='
			//                  CW02H-CLIENT-ID ';'
			//                 DELIMITED BY SIZE
			//                 INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CW02H-CLIENT-ID=",
					ws.getCw02hClientTabRow().getClientIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4900-GET-RELATED-CLIENT-DATA-X
			return;
		}
		// COB_CODE: PERFORM 4910-FETCH-CLT-CURSOR1.
		fetchCltCursor1();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4900-GET-RELATED-CLIENT-DATA-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4900-GET-RELATED-CLIENT-DATA-X
			return;
		}
		// COB_CODE: EXEC SQL
		//              CLOSE WS-CLT-CURSOR1
		//           END-EXEC.
		clientTabVDao.closeWsCltCursor1();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 4900-GET-RELATED-CLIENT-DATA-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING      TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'CLIENT_TAB_V'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("CLIENT_TAB_V");
			// COB_CODE: MOVE '4900-GET-RELATED-CLIENT-DATA'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4900-GET-RELATED-CLIENT-DATA");
			// COB_CODE: MOVE 'CLOSE CURSOR1 FAILED'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CURSOR1 FAILED");
			// COB_CODE: STRING 'CW02H-CLIENT-ID='
			//                  CW02H-CLIENT-ID ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CW02H-CLIENT-ID=",
					ws.getCw02hClientTabRow().getClientIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4900-GET-RELATED-CLIENT-DATA-X
			return;
		}
		// COB_CODE: PERFORM 4920-WRITE-RESP-MSG-UMT-CRC.
		writeRespMsgUmtCrc();
	}

	/**Original name: 4910-FETCH-CLT-CURSOR1_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 * **FETCH ROW DATA USING CURSOR1.                              ***
	 * ****************************************************************</pre>*/
	private void fetchCltCursor1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//            FETCH WS-CLT-CURSOR1
		//            INTO :CW02H-CLIENT-ID,
		//                 :CW02H-HISTORY-VLD-NBR,
		//                 :CW02H-CICL-EFF-DT,
		//                 :CW02H-CICL-PRI-SUB-CD,
		//                 :CW02H-CICL-FST-NM,
		//                 :CW02H-CICL-LST-NM,
		//                 :CW02H-CICL-MDL-NM,
		//                 :CW02H-NM-PFX,
		//                 :CW02H-NM-SFX,
		//                 :CW02H-PRIMARY-PRO-DSN-CD
		//                   :CW02H-PRIMARY-PRO-DSN-CD-NI,
		//                 :CW02H-LEG-ENT-CD,
		//                 :CW02H-CICL-SDX-CD,
		//                 :CW02H-CICL-OGN-INCEPT-DT
		//                   :CW02H-CICL-OGN-INCEPT-DT-NI,
		//                 :CW02H-CICL-ADD-NM-IND
		//                   :CW02H-CICL-ADD-NM-IND-NI,
		//                 :CW02H-CICL-DOB-DT,
		//                 :CW02H-CICL-BIR-ST-CD,
		//                 :CW02H-GENDER-CD,
		//                 :CW02H-PRI-LGG-CD,
		//                 :CW02H-USER-ID,
		//                 :CW02H-STATUS-CD,
		//                 :CW02H-TERMINAL-ID,
		//                 :CW02H-CICL-EXP-DT,
		//                 :CW02H-CICL-EFF-ACY-TS,
		//                 :CW02H-CICL-EXP-ACY-TS,
		//                 :CW02H-STATUTORY-TLE-CD,
		//                 :CW02H-CICL-LNG-NM
		//                   :CW02H-CICL-LNG-NM-NI
		//           END-EXEC.
		clientTabVDao.fetchWsCltCursor1(ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
		//               WHEN OTHER
		//                   GO TO 4910-FETCH-CLT-CURSOR1-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-NON-LOGGABLE-WARNING
			//                               TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setWarning();
			// COB_CODE: MOVE WS-BUS-OBJ-NM  TO UWRN-FAILED-TABLE-OR-FILE
			ws.getUwrnCommon().setFailedTableOrFile(ws.getWsSpecificWorkAreas().getBusObjNm());
			// COB_CODE: MOVE 'GEN_ALLNFD'   TO UWRN-WARNING-CODE
			ws.getUwrnCommon().setWarningCode("GEN_ALLNFD");
			// COB_CODE: MOVE 'FETCH WITH WS-CLT-CURSOR1 FAILED'
			//                               TO UWRN-FAILED-COLUMN-OR-FIELD
			ws.getUwrnCommon().setFailedColumnOrField("FETCH WITH WS-CLT-CURSOR1 FAILED");
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'CLIENT_TAB_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("CLIENT_TAB_V");
			// COB_CODE: MOVE '4910-FETCH-CLT-CURSOR1'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4910-FETCH-CLT-CURSOR1");
			// COB_CODE: MOVE 'FETCH FROM CURSOR1 FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH FROM CURSOR1 FAILED");
			// COB_CODE: STRING 'CW02H-CLIENT-ID='
			//                  CW02H-CLIENT-ID ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "CW02H-CLIENT-ID=",
					ws.getCw02hClientTabRow().getClientIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4910-FETCH-CLT-CURSOR1-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 4910-FETCH-CLT-CURSOR1-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4910-FETCH-CLT-CURSOR1-X
			return;
		}
	}

	/**Original name: 4920-WRITE-RESP-MSG-UMT-CRC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 * * 4920-WRITE-RESP-MSG-UMT-CRC FORMATS THE OUTOUT AND WRITES IT **
	 * * IN THE RESPONSE UMT.                                        ***
	 * *****************************************************************</pre>*/
	private void writeRespMsgUmtCrc() {
		// COB_CODE: PERFORM 5000-REFORMAT-DATA.
		reformatData();
		// COB_CODE: SET  HALRRESP-WRITE-FUNC    TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE WS-BUS-OBJ-NM          TO HALRRESP-BUS-OBJ-NM.
		ws.getWsHalrrespLinkage().setBusObjNm(ws.getWsSpecificWorkAreas().getBusObjNm());
		// COB_CODE: MOVE LENGTH OF CWORC-CLT-OBJ-RELATION-ROW
		//                                       TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(((short) Cawlcorc.Len.CLT_OBJ_RELATION_ROW));
		// COB_CODE: INITIALIZE WS-BUS-OBJ-DATA-ROW.
		ws.getWsSpecificWorkAreas().setBusObjDataRow("");
		// COB_CODE: MOVE CWORC-CLT-OBJ-RELATION-ROW
		//                                        TO WS-BUS-OBJ-DATA-ROW
		ws.getWsSpecificWorkAreas().setBusObjDataRow(ws.getCawlcorc().getCltObjRelationRowFormatted());
		// COB_CODE: PERFORM 6000-CALL-HALRRESP.
		callHalrresp();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4920-WRITE-RESP-MSG-UMT-CRC-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4920-WRITE-RESP-MSG-UMT-CRC-X
			return;
		}
	}

	/**Original name: 5000-REFORMAT-DATA_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  FORMAT THE DATA BEFORE WRITING TO RESPONSE UMT.
	 * ****************************************************************</pre>*/
	private void reformatData() {
		// COB_CODE: MOVE SPACES                 TO CWORC-CLT-OBJ-RELATION-ROW.
		ws.getCawlcorc().initCltObjRelationRowSpaces();
		// COB_CODE: MOVE CW08C-CLT-OBJ-RELATION-CSUM
		//                                       TO CWORC-CLT-OBJ-RELATION-CSUM.
		ws.getCawlcorc().getCltObjRelationFixed()
				.setCltObjRelationCsumFormatted(ws.getCawlc008().getCltObjRelationFixed().getCltObjRelationCsumFormatted());
		// COB_CODE: MOVE WS-SE3-CUR-ISO-DATE    TO CWORC-TRANS-PROCESS-DT.
		ws.getCawlcorc().setTransProcessDt(ws.getWsSe3CurIsoDate());
		// COB_CODE: MOVE CW08C-TCH-OBJECT-KEY   TO CWORC-TCH-OBJECT-KEY.
		ws.getCawlcorc().getCltObjRelationKey().setTchObjectKey(ws.getCawlc008().getCltObjRelationKey().getTchObjectKey());
		// COB_CODE: IF CW08C-HISTORY-VLD-NBR < 0
		//               MOVE '-'                TO CWORC-HISTORY-VLD-NBR-SIGN
		//           ELSE
		//               MOVE '+'                TO CWORC-HISTORY-VLD-NBR-SIGN
		//           END-IF.
		if (ws.getCawlc008().getCltObjRelationKey().getHistoryVldNbr() < 0) {
			// COB_CODE: MOVE '-'                TO CWORC-HISTORY-VLD-NBR-SIGN
			ws.getCawlcorc().getCltObjRelationKey().setHistoryVldNbrSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+'                TO CWORC-HISTORY-VLD-NBR-SIGN
			ws.getCawlcorc().getCltObjRelationKey().setHistoryVldNbrSignFormatted("+");
		}
		// COB_CODE: MOVE CW08C-HISTORY-VLD-NBR  TO CWORC-HISTORY-VLD-NBR.
		ws.getCawlcorc().getCltObjRelationKey().setHistoryVldNbrFormatted(ws.getCawlc008().getCltObjRelationKey().getHistoryVldNbrFormatted());
		// COB_CODE: MOVE CW08C-CIOR-EFF-DT      TO CWORC-CIOR-EFF-DT.
		ws.getCawlcorc().getCltObjRelationKey().setCiorEffDt(ws.getCawlc008().getCltObjRelationKey().getCiorEffDt());
		// COB_CODE: MOVE CW08C-OBJ-SYS-ID       TO CWORC-OBJ-SYS-ID.
		ws.getCawlcorc().getCltObjRelationKey().setObjSysId(ws.getCawlc008().getCltObjRelationKey().getObjSysId());
		// COB_CODE: IF CW08C-CIOR-OBJ-SEQ-NBR < 0
		//               MOVE '-'                TO CWORC-CIOR-OBJ-SEQ-NBR-SIGN
		//           ELSE
		//               MOVE '+'                TO CWORC-CIOR-OBJ-SEQ-NBR-SIGN
		//           END-IF.
		if (ws.getCawlc008().getCltObjRelationKey().getCiorObjSeqNbr() < 0) {
			// COB_CODE: MOVE '-'                TO CWORC-CIOR-OBJ-SEQ-NBR-SIGN
			ws.getCawlcorc().getCltObjRelationKey().setCiorObjSeqNbrSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+'                TO CWORC-CIOR-OBJ-SEQ-NBR-SIGN
			ws.getCawlcorc().getCltObjRelationKey().setCiorObjSeqNbrSignFormatted("+");
		}
		// COB_CODE: MOVE CW08C-CIOR-OBJ-SEQ-NBR TO CWORC-CIOR-OBJ-SEQ-NBR.
		ws.getCawlcorc().getCltObjRelationKey().setCiorObjSeqNbrFormatted(ws.getCawlc008().getCltObjRelationKey().getCiorObjSeqNbrFormatted());
		// COB_CODE: MOVE CW08C-CLIENT-ID        TO CWORC-CLIENT-ID.
		ws.getCawlcorc().getCltObjRelationData().setClientId(ws.getCawlc008().getCltObjRelationData().getClientId());
		// COB_CODE: MOVE CW08C-RLT-TYP-CD       TO CWORC-RLT-TYP-CD.
		ws.getCawlcorc().getCltObjRelationData().setRltTypCd(ws.getCawlc008().getCltObjRelationData().getRltTypCd());
		// COB_CODE: MOVE CW08C-OBJ-CD           TO CWORC-OBJ-CD.
		ws.getCawlcorc().getCltObjRelationData().setObjCd(ws.getCawlc008().getCltObjRelationData().getObjCd());
		// COB_CODE: MOVE CW08C-CIOR-SHW-OBJ-KEY TO CWORC-CIOR-SHW-OBJ-KEY.
		ws.getCawlcorc().getCltObjRelationData().setCiorShwObjKey(ws.getCawlc008().getCltObjRelationData().getCiorShwObjKey());
		// COB_CODE: IF CW08C-ADR-SEQ-NBR < 0
		//               MOVE '-'                TO CWORC-ADR-SEQ-NBR-SIGN
		//           ELSE
		//               MOVE '+'                TO CWORC-ADR-SEQ-NBR-SIGN
		//           END-IF.
		if (ws.getCawlc008().getCltObjRelationData().getAdrSeqNbr() < 0) {
			// COB_CODE: MOVE '-'                TO CWORC-ADR-SEQ-NBR-SIGN
			ws.getCawlcorc().getCltObjRelationData().setAdrSeqNbrSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+'                TO CWORC-ADR-SEQ-NBR-SIGN
			ws.getCawlcorc().getCltObjRelationData().setAdrSeqNbrSignFormatted("+");
		}
		// COB_CODE: MOVE CW08C-ADR-SEQ-NBR      TO CWORC-ADR-SEQ-NBR.
		ws.getCawlcorc().getCltObjRelationData().setAdrSeqNbrFormatted(ws.getCawlc008().getCltObjRelationData().getAdrSeqNbrFormatted());
		// COB_CODE: MOVE CW08C-USER-ID          TO CWORC-USER-ID.
		ws.getCawlcorc().getCltObjRelationData().setUserId(ws.getCawlc008().getCltObjRelationData().getUserId());
		// COB_CODE: MOVE CW08C-STATUS-CD        TO CWORC-STATUS-CD.
		ws.getCawlcorc().getCltObjRelationData().setStatusCd(ws.getCawlc008().getCltObjRelationData().getStatusCd());
		// COB_CODE: MOVE CW08C-TERMINAL-ID      TO CWORC-TERMINAL-ID.
		ws.getCawlcorc().getCltObjRelationData().setTerminalId(ws.getCawlc008().getCltObjRelationData().getTerminalId());
		// COB_CODE: MOVE CW08C-CIOR-EXP-DT      TO CWORC-CIOR-EXP-DT.
		ws.getCawlcorc().getCltObjRelationData().setCiorExpDt(ws.getCawlc008().getCltObjRelationData().getCiorExpDt());
		// COB_CODE: MOVE CW08C-CIOR-EFF-ACY-TS  TO CWORC-CIOR-EFF-ACY-TS.
		ws.getCawlcorc().getCltObjRelationData().setCiorEffAcyTs(ws.getCawlc008().getCltObjRelationData().getCiorEffAcyTs());
		// COB_CODE: MOVE CW08C-CIOR-EXP-ACY-TS  TO CWORC-CIOR-EXP-ACY-TS.
		ws.getCawlcorc().getCltObjRelationData().setCiorExpAcyTs(ws.getCawlc008().getCltObjRelationData().getCiorExpAcyTs());
		// COB_CODE: MOVE CW02H-LEG-ENT-CD       TO CWORC-CIOR-LEG-ENT-CD.
		ws.getCawlcorc().getCltObjRelationData().setCiorLegEntCd(ws.getCw02hClientTabRow().getLegEntCd());
		// COB_CODE: SET COM-IS-VIEWONLY-COL-IND TO TRUE.
		ws.getHallcom().getOutgoingColIndVal().setComIsViewonlyColInd();
		// COB_CODE: MOVE COM-OUTGOING-COL-IND-VAL
		//                                       TO CWORC-CIOR-LNG-NM-CI.
		ws.getCawlcorc().getCltObjRelationData().setCiorLngNmCi(ws.getHallcom().getOutgoingColIndVal().getOutgoingColIndVal());
		// COB_CODE: IF CW02H-CICL-LNG-NM-NI = -1
		//               MOVE SPACES             TO CWORC-CIOR-LNG-NM
		//           ELSE
		//                 BY ' '
		//           END-IF.
		if (ws.getCw02hClientTabRow().getCiclLngNmNi() == -1) {
			// COB_CODE: MOVE COM-COL-IS-NULL    TO CWORC-CIOR-LNG-NM-NI
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmNi(ws.getHallcom().getColIsNull());
			// COB_CODE: MOVE SPACES             TO CWORC-CIOR-LNG-NM
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNm("");
		} else {
			// COB_CODE: MOVE COM-COL-IS-NOT-NULL
			//                                   TO CWORC-CIOR-LNG-NM-NI
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmNi(ws.getHallcom().getColIsNotNull());
			// COB_CODE: MOVE CW02H-CICL-LNG-NM-TEXT
			//                (1:CW02H-CICL-LNG-NM-LEN)
			//                                   TO CWORC-CIOR-LNG-NM
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNm(
					ws.getCw02hClientTabRow().getCiclLngNmTextFormatted().substring((1) - 1, ws.getCw02hClientTabRow().getCiclLngNmLen()));
			// COB_CODE: INSPECT CWORC-CIOR-LNG-NM
			//             REPLACING ALL WS-LONGNAME-DELIM
			//             BY ' '
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNm(ws.getCawlcorc().getCltObjRelationData().getCiorLngNmFormatted()
					.replace(String.valueOf(ws.getWsClientProcessing().getLongnameDelim()), " "));
		}
		// COB_CODE: IF CWORC-CIOR-LNG-NM-NI = COM-COL-IS-NULL
		//             OR
		//              CWORC-CIOR-LNG-NM = SPACES
		//               MOVE CW02H-NM-SFX       TO CWORC-CIOR-NM-SFX
		//           ELSE
		//               PERFORM 5600-BUILD-DISPLAY-NAMES
		//           END-IF.
		if (ws.getCawlcorc().getCltObjRelationData().getCiorLngNmNi() == ws.getHallcom().getColIsNull()
				|| Characters.EQ_SPACE.test(ws.getCawlcorc().getCltObjRelationData().getCiorLngNm())) {
			// COB_CODE: MOVE CW02H-CICL-FST-NM  TO CWORC-CIOR-FST-NM
			ws.getCawlcorc().getCltObjRelationData().setCiorFstNm(ws.getCw02hClientTabRow().getCiclFstNm());
			// COB_CODE: MOVE CW02H-CICL-MDL-NM  TO CWORC-CIOR-MDL-NM
			ws.getCawlcorc().getCltObjRelationData().setCiorMdlNm(ws.getCw02hClientTabRow().getCiclMdlNm());
			// COB_CODE: MOVE CW02H-CICL-LST-NM  TO CWORC-CIOR-LST-NM
			ws.getCawlcorc().getCltObjRelationData().setCiorLstNm(ws.getCw02hClientTabRow().getCiclLstNm());
			// COB_CODE: MOVE CW02H-NM-PFX       TO CWORC-CIOR-NM-PFX
			ws.getCawlcorc().getCltObjRelationData().setCiorNmPfx(ws.getCw02hClientTabRow().getNmPfx());
			// COB_CODE: MOVE CW02H-NM-SFX       TO CWORC-CIOR-NM-SFX
			ws.getCawlcorc().getCltObjRelationData().setCiorNmSfx(ws.getCw02hClientTabRow().getNmSfx());
		} else {
			// COB_CODE: PERFORM 5500-DERIVE-LONGNAME-PARTS
			deriveLongnameParts();
			// COB_CODE: PERFORM 5600-BUILD-DISPLAY-NAMES
			buildDisplayNames();
		}
	}

	/**Original name: 5500-DERIVE-LONGNAME-PARTS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  RETRIEVE THE ORIGINAL LONGNAME.                                *
	 * *****************************************************************</pre>*/
	private void deriveLongnameParts() {
		IStringTokenizer tokenizer = null;
		// COB_CODE: UNSTRING CW02H-CICL-LNG-NM-TEXT
		//             DELIMITED BY WS-LONGNAME-DELIM
		//             INTO WS-HOLD-UNSTR-DUMMY
		//                  WS-HOLD-PREFIX-VERBIAGE
		//                  WS-HOLD-FIRST-NAME
		//                  WS-HOLD-MIDDLE-NAME
		//                  WS-HOLD-LAST-NAME
		//                  WS-HOLD-SUFFIX-VERBIAGE
		//           END-UNSTRING.
		tokenizer = new CharTokenizer(ws.getWsClientProcessing().getLongnameDelim());
		tokenizer.tokenize(ws.getCw02hClientTabRow().getCiclLngNmTextFormatted());
		try {
			ws.getWsClientProcessing().setHoldUnstrDummy(tokenizer.nextChar());
			ws.getWsClientProcessing()
					.setHoldPrefixVerbiage(Functions.trimAfter(tokenizer.nextString(), WsClientProcessing.Len.HOLD_PREFIX_VERBIAGE));
			ws.getWsClientProcessing().setHoldFirstName(Functions.trimAfter(tokenizer.nextString(), WsClientProcessing.Len.HOLD_FIRST_NAME));
			ws.getWsClientProcessing().setHoldMiddleName(Functions.trimAfter(tokenizer.nextString(), WsClientProcessing.Len.HOLD_MIDDLE_NAME));
			ws.getWsClientProcessing().setHoldLastName(Functions.trimAfter(tokenizer.nextString(), WsClientProcessing.Len.HOLD_LAST_NAME));
			ws.getWsClientProcessing()
					.setHoldSuffixVerbiage(Functions.trimAfter(tokenizer.nextString(), WsClientProcessing.Len.HOLD_SUFFIX_VERBIAGE));
		} catch (NoSuchElementException e) {
		}
	}

	/**Original name: 5600-BUILD-DISPLAY-NAMES_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  IF THE LONGNAME CONTAINED ACTUAL NAME DATA, RETRIEVE THE       *
	 *  FIRST, MIDDLE, AND LASTNAME NODES SO THAT THESE CAN BE         *
	 *  RETURNED TO THE FRONT-END AS ORIGINALLY ENTERED (NOT IN        *
	 *  COMPRESSED AND CAPITALIZED FORMAT).  ADDITIONALLY, BUILD THE   *
	 *  DISPLAYABLE LONGNAME - WITHOUT SUPERFLUOUS SPACES.             *
	 * *****************************************************************</pre>*/
	private void buildDisplayNames() {
		// COB_CODE: INSPECT WS-HOLD-FIRST-NAME  REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldFirstName(ws.getWsClientProcessing().getHoldFirstNameFormatted().replace("~", "+"));
		// COB_CODE: INSPECT WS-HOLD-MIDDLE-NAME REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldMiddleName(ws.getWsClientProcessing().getHoldMiddleNameFormatted().replace("~", "+"));
		// COB_CODE: INSPECT WS-HOLD-LAST-NAME   REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldLastName(ws.getWsClientProcessing().getHoldLastNameFormatted().replace("~", "+"));
		// COB_CODE: INSPECT WS-HOLD-PREFIX-VERBIAGE
		//                                       REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldPrefixVerbiage(ws.getWsClientProcessing().getHoldPrefixVerbiageFormatted().replace("~", "+"));
		// COB_CODE: INSPECT WS-HOLD-SUFFIX-VERBIAGE
		//                                       REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldSuffixVerbiage(ws.getWsClientProcessing().getHoldSuffixVerbiageFormatted().replace("~", "+"));
		// COB_CODE: MOVE WS-HOLD-FIRST-NAME     TO CWORC-CIOR-FST-NM.
		ws.getCawlcorc().getCltObjRelationData().setCiorFstNm(ws.getWsClientProcessing().getHoldFirstName());
		// COB_CODE: MOVE WS-HOLD-MIDDLE-NAME    TO CWORC-CIOR-MDL-NM.
		ws.getCawlcorc().getCltObjRelationData().setCiorMdlNm(ws.getWsClientProcessing().getHoldMiddleName());
		// COB_CODE: MOVE WS-HOLD-LAST-NAME      TO CWORC-CIOR-LST-NM.
		ws.getCawlcorc().getCltObjRelationData().setCiorLstNm(ws.getWsClientProcessing().getHoldLastName());
		// COB_CODE: MOVE WS-HOLD-PREFIX-VERBIAGE
		//                                       TO CWORC-CIOR-NM-PFX.
		ws.getCawlcorc().getCltObjRelationData().setCiorNmPfx(ws.getWsClientProcessing().getHoldPrefixVerbiage());
		// COB_CODE: MOVE WS-HOLD-SUFFIX-VERBIAGE
		//                                       TO CWORC-CIOR-NM-SFX.
		ws.getCawlcorc().getCltObjRelationData().setCiorNmSfx(ws.getWsClientProcessing().getHoldSuffixVerbiage());
		// COB_CODE: INSPECT WS-HOLD-FIRST-NAME  REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldFirstName(ws.getWsClientProcessing().getHoldFirstNameFormatted().replace("~", "+"));
		// COB_CODE: INSPECT WS-HOLD-MIDDLE-NAME REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldMiddleName(ws.getWsClientProcessing().getHoldMiddleNameFormatted().replace("~", "+"));
		// COB_CODE: INSPECT WS-HOLD-LAST-NAME   REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldLastName(ws.getWsClientProcessing().getHoldLastNameFormatted().replace("~", "+"));
		// COB_CODE: INSPECT WS-HOLD-PREFIX-VERBIAGE
		//                                       REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldPrefixVerbiage(ws.getWsClientProcessing().getHoldPrefixVerbiageFormatted().replace("~", "+"));
		// COB_CODE: INSPECT WS-HOLD-SUFFIX-VERBIAGE
		//                                       REPLACING ALL '~' BY '+'.
		ws.getWsClientProcessing().setHoldSuffixVerbiage(ws.getWsClientProcessing().getHoldSuffixVerbiageFormatted().replace("~", "+"));
		// COB_CODE: MOVE WS-HOLD-FIRST-NAME     TO CWORC-CIOR-FST-NM.
		ws.getCawlcorc().getCltObjRelationData().setCiorFstNm(ws.getWsClientProcessing().getHoldFirstName());
		// COB_CODE: MOVE WS-HOLD-MIDDLE-NAME    TO CWORC-CIOR-MDL-NM.
		ws.getCawlcorc().getCltObjRelationData().setCiorMdlNm(ws.getWsClientProcessing().getHoldMiddleName());
		// COB_CODE: MOVE WS-HOLD-LAST-NAME      TO CWORC-CIOR-LST-NM.
		ws.getCawlcorc().getCltObjRelationData().setCiorLstNm(ws.getWsClientProcessing().getHoldLastName());
		// COB_CODE: MOVE WS-HOLD-PREFIX-VERBIAGE
		//                                       TO CWORC-CIOR-NM-PFX.
		ws.getCawlcorc().getCltObjRelationData().setCiorNmPfx(ws.getWsClientProcessing().getHoldPrefixVerbiage());
		// COB_CODE: MOVE WS-HOLD-SUFFIX-VERBIAGE
		//                                       TO CWORC-CIOR-NM-SFX.
		ws.getCawlcorc().getCltObjRelationData().setCiorNmSfx(ws.getWsClientProcessing().getHoldSuffixVerbiage());
		// COB_CODE: MOVE SPACES                 TO CWORC-CIOR-LNG-NM.
		ws.getCawlcorc().getCltObjRelationData().setCiorLngNm("");
		// COB_CODE: MOVE 1                      TO WS-CUR-LONGNAME-LEN.
		ws.getWsClientProcessing().setCurLongnameLen(((short) 1));
		// COB_CODE: IF WS-HOLD-PREFIX-VERBIAGE NOT EQUAL SPACES
		//               ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsClientProcessing().getHoldPrefixVerbiage())) {
			// COB_CODE: MOVE WS-HOLD-PREFIX-VERBIAGE
			//                                   TO WS-TRUNC-STRING
			ws.getWsClientProcessing().setTruncString(ws.getWsClientProcessing().getHoldPrefixVerbiage());
			// COB_CODE: PERFORM 5700-TRUNCATE-SPACES
			truncateSpaces();
			// COB_CODE: MOVE WS-TRUNC-STRING(1:WS-TRUNC-LEN)
			//                                   TO CWORC-CIOR-LNG-NM
			//                                 (WS-CUR-LONGNAME-LEN:WS-TRUNC-LEN)
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmSubstring(
					ws.getWsClientProcessing().getTruncStringFormatted().substring((1) - 1, ws.getWsClientProcessing().getTruncLen()),
					ws.getWsClientProcessing().getCurLongnameLen(), ws.getWsClientProcessing().getTruncLen());
			// COB_CODE: ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
			ws.getWsClientProcessing()
					.setCurLongnameLen(Trunc.toShort(ws.getWsClientProcessing().getTruncLen() + ws.getWsClientProcessing().getCurLongnameLen(), 4));
		}
		// COB_CODE: IF WS-HOLD-FIRST-NAME NOT EQUAL SPACES
		//               ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsClientProcessing().getHoldFirstName())) {
			// COB_CODE: MOVE SPACE              TO CWORC-CIOR-LNG-NM
			//                                        (WS-CUR-LONGNAME-LEN:1)
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmSubstring(LiteralGenerator.create(Types.SPACE_CHAR, 1),
					ws.getWsClientProcessing().getCurLongnameLen(), 1);
			// COB_CODE: ADD 1                   TO WS-CUR-LONGNAME-LEN
			ws.getWsClientProcessing().setCurLongnameLen(Trunc.toShort(1 + ws.getWsClientProcessing().getCurLongnameLen(), 4));
			// COB_CODE: MOVE WS-HOLD-FIRST-NAME TO WS-TRUNC-STRING
			ws.getWsClientProcessing().setTruncString(ws.getWsClientProcessing().getHoldFirstName());
			// COB_CODE: PERFORM 5700-TRUNCATE-SPACES
			truncateSpaces();
			// COB_CODE: MOVE WS-TRUNC-STRING(1:WS-TRUNC-LEN)
			//                                   TO CWORC-CIOR-LNG-NM
			//                                 (WS-CUR-LONGNAME-LEN:WS-TRUNC-LEN)
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmSubstring(
					ws.getWsClientProcessing().getTruncStringFormatted().substring((1) - 1, ws.getWsClientProcessing().getTruncLen()),
					ws.getWsClientProcessing().getCurLongnameLen(), ws.getWsClientProcessing().getTruncLen());
			// COB_CODE: ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
			ws.getWsClientProcessing()
					.setCurLongnameLen(Trunc.toShort(ws.getWsClientProcessing().getTruncLen() + ws.getWsClientProcessing().getCurLongnameLen(), 4));
		}
		// COB_CODE: IF WS-HOLD-MIDDLE-NAME NOT EQUAL SPACES
		//               ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsClientProcessing().getHoldMiddleName())) {
			// COB_CODE: MOVE SPACE              TO CWORC-CIOR-LNG-NM
			//                                        (WS-CUR-LONGNAME-LEN:1)
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmSubstring(LiteralGenerator.create(Types.SPACE_CHAR, 1),
					ws.getWsClientProcessing().getCurLongnameLen(), 1);
			// COB_CODE: ADD 1                   TO WS-CUR-LONGNAME-LEN
			ws.getWsClientProcessing().setCurLongnameLen(Trunc.toShort(1 + ws.getWsClientProcessing().getCurLongnameLen(), 4));
			// COB_CODE: MOVE WS-HOLD-MIDDLE-NAME
			//                                   TO WS-TRUNC-STRING
			ws.getWsClientProcessing().setTruncString(ws.getWsClientProcessing().getHoldMiddleName());
			// COB_CODE: PERFORM 5700-TRUNCATE-SPACES
			truncateSpaces();
			// COB_CODE: MOVE WS-TRUNC-STRING(1:WS-TRUNC-LEN)
			//                                   TO CWORC-CIOR-LNG-NM
			//                                 (WS-CUR-LONGNAME-LEN:WS-TRUNC-LEN)
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmSubstring(
					ws.getWsClientProcessing().getTruncStringFormatted().substring((1) - 1, ws.getWsClientProcessing().getTruncLen()),
					ws.getWsClientProcessing().getCurLongnameLen(), ws.getWsClientProcessing().getTruncLen());
			// COB_CODE: ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
			ws.getWsClientProcessing()
					.setCurLongnameLen(Trunc.toShort(ws.getWsClientProcessing().getTruncLen() + ws.getWsClientProcessing().getCurLongnameLen(), 4));
		}
		// COB_CODE: IF WS-HOLD-LAST-NAME NOT EQUAL SPACES
		//               ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsClientProcessing().getHoldLastName())) {
			// COB_CODE: MOVE SPACE              TO CWORC-CIOR-LNG-NM
			//                                        (WS-CUR-LONGNAME-LEN:1)
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmSubstring(LiteralGenerator.create(Types.SPACE_CHAR, 1),
					ws.getWsClientProcessing().getCurLongnameLen(), 1);
			// COB_CODE: ADD 1                   TO WS-CUR-LONGNAME-LEN
			ws.getWsClientProcessing().setCurLongnameLen(Trunc.toShort(1 + ws.getWsClientProcessing().getCurLongnameLen(), 4));
			// COB_CODE: MOVE WS-HOLD-LAST-NAME  TO WS-TRUNC-STRING
			ws.getWsClientProcessing().setTruncString(ws.getWsClientProcessing().getHoldLastName());
			// COB_CODE: PERFORM 5700-TRUNCATE-SPACES
			truncateSpaces();
			// COB_CODE: MOVE WS-TRUNC-STRING(1:WS-TRUNC-LEN)
			//                                   TO CWORC-CIOR-LNG-NM
			//                                 (WS-CUR-LONGNAME-LEN:WS-TRUNC-LEN)
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmSubstring(
					ws.getWsClientProcessing().getTruncStringFormatted().substring((1) - 1, ws.getWsClientProcessing().getTruncLen()),
					ws.getWsClientProcessing().getCurLongnameLen(), ws.getWsClientProcessing().getTruncLen());
			// COB_CODE: ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
			ws.getWsClientProcessing()
					.setCurLongnameLen(Trunc.toShort(ws.getWsClientProcessing().getTruncLen() + ws.getWsClientProcessing().getCurLongnameLen(), 4));
		}
		// COB_CODE: IF WS-HOLD-SUFFIX-VERBIAGE NOT EQUAL SPACES
		//               ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsClientProcessing().getHoldSuffixVerbiage())) {
			// COB_CODE: MOVE SPACE              TO CWORC-CIOR-LNG-NM
			//                                        (WS-CUR-LONGNAME-LEN:1)
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmSubstring(LiteralGenerator.create(Types.SPACE_CHAR, 1),
					ws.getWsClientProcessing().getCurLongnameLen(), 1);
			// COB_CODE: ADD 1                   TO WS-CUR-LONGNAME-LEN
			ws.getWsClientProcessing().setCurLongnameLen(Trunc.toShort(1 + ws.getWsClientProcessing().getCurLongnameLen(), 4));
			// COB_CODE: MOVE WS-HOLD-SUFFIX-VERBIAGE
			//                                   TO WS-TRUNC-STRING
			ws.getWsClientProcessing().setTruncString(ws.getWsClientProcessing().getHoldSuffixVerbiage());
			// COB_CODE: PERFORM 5700-TRUNCATE-SPACES
			truncateSpaces();
			// COB_CODE: MOVE WS-TRUNC-STRING(1:WS-TRUNC-LEN)
			//                                   TO CWORC-CIOR-LNG-NM
			//                                 (WS-CUR-LONGNAME-LEN:WS-TRUNC-LEN)
			ws.getCawlcorc().getCltObjRelationData().setCiorLngNmSubstring(
					ws.getWsClientProcessing().getTruncStringFormatted().substring((1) - 1, ws.getWsClientProcessing().getTruncLen()),
					ws.getWsClientProcessing().getCurLongnameLen(), ws.getWsClientProcessing().getTruncLen());
			// COB_CODE: ADD WS-TRUNC-LEN        TO WS-CUR-LONGNAME-LEN
			ws.getWsClientProcessing()
					.setCurLongnameLen(Trunc.toShort(ws.getWsClientProcessing().getTruncLen() + ws.getWsClientProcessing().getCurLongnameLen(), 4));
		}
		// COB_CODE: SUBTRACT 1                  FROM WS-CUR-LONGNAME-LEN.
		ws.getWsClientProcessing().setCurLongnameLen(Trunc.toShort(ws.getWsClientProcessing().getCurLongnameLen() - 1, 4));
	}

	/**Original name: 5700-TRUNCATE-SPACES_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  DERIVE THE LENGTH OF A STRINGS VALID CHARACTERS NOT INCLUDING  *
	 *  ANY TRAILING SPACES.                                           *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void truncateSpaces() {
		// COB_CODE: SET SPACE-FOUND             TO TRUE.
		ws.getWsSpecificSwitches().getWsCharacterTypeSw().setSpaceFound();
		// COB_CODE: PERFORM 5800-GET-CHAR-COUNT
		//               VARYING WS-TRUNC-LEN FROM WS-MAX-TRUNC-LEN BY -1
		//               UNTIL VALID-CHAR-FOUND
		//               OR WS-TRUNC-LEN = 1.
		ws.getWsClientProcessing().setTruncLen(ws.getWsClientProcessing().getMaxTruncLen());
		while (!(ws.getWsSpecificSwitches().getWsCharacterTypeSw().isValidCharFound() || ws.getWsClientProcessing().getTruncLen() == 1)) {
			getCharCount();
			ws.getWsClientProcessing().setTruncLen(Trunc.toShort(ws.getWsClientProcessing().getTruncLen() + (-1), 4));
		}
		// COB_CODE: ADD 1                       TO WS-TRUNC-LEN.
		ws.getWsClientProcessing().setTruncLen(Trunc.toShort(1 + ws.getWsClientProcessing().getTruncLen(), 4));
	}

	/**Original name: 5800-GET-CHAR-COUNT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DERIVE THE LENGTH OF A STRINGS VALID CHARACTERS NOT INCLUDING  *
	 *  ANY TRAILING SPACES.                                           *
	 * *****************************************************************</pre>*/
	private void getCharCount() {
		// COB_CODE: IF WS-TRUNC-STRING(WS-TRUNC-LEN:1) NOT EQUAL SPACE
		//               SET VALID-CHAR-FOUND    TO TRUE
		//           END-IF.
		if (!Conditions.eq(ws.getWsClientProcessing().getTruncStringFormatted().substring((ws.getWsClientProcessing().getTruncLen()) - 1,
				ws.getWsClientProcessing().getTruncLen()), "")) {
			// COB_CODE: SET VALID-CHAR-FOUND    TO TRUE
			ws.getWsSpecificSwitches().getWsCharacterTypeSw().setValidCharFound();
		}
	}

	/**Original name: 6000-CALL-HALRRESP_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DYNAMIC CALL TO THE UMT ACCESS ROUTINE FOR HALFUHDR AND
	 *  HALFUDAT.
	 * *****************************************************************</pre>*/
	private void callHalrresp() {
		Halrresp halrresp = null;
		StringParam wsBusObjDataRow = null;
		// COB_CODE: CALL HALRRESP-HALRRESP-LIT USING
		//                  DFHEIBLK
		//                  DFHCOMMAREA
		//                  UBOC-RECORD
		//                  WS-HALRRESP-LINKAGE
		//                  WS-BUS-OBJ-DATA-ROW.
		halrresp = Halrresp.getInstance();
		wsBusObjDataRow = new StringParam(ws.getWsSpecificWorkAreas().getBusObjDataRow(), WsSpecificWorkAreasCawpcorc.Len.BUS_OBJ_DATA_ROW);
		halrresp.run(execContext, dfhcommarea, ws.getWsHallubocLinkage(), ws.getWsHalrrespLinkage(), wsBusObjDataRow);
		ws.getWsSpecificWorkAreas().setBusObjDataRow(wsBusObjDataRow.getString());
	}

	/**Original name: 7000-CALL-HALRURQA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DYNAMIC CALL TO THE UMT ACCESS ROUTINE FOR HALFURQM.
	 * *****************************************************************</pre>*/
	private void callHalrurqa() {
		Halrurqa halrurqa = null;
		StringParam wsBusObjDataRow = null;
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                   DFHEIBLK
		//                   DFHCOMMAREA
		//                   UBOC-RECORD
		//                   WS-HALRURQA-LINKAGE
		//                   WS-BUS-OBJ-DATA-ROW.
		halrurqa = Halrurqa.getInstance();
		wsBusObjDataRow = new StringParam(ws.getWsSpecificWorkAreas().getBusObjDataRow(), WsSpecificWorkAreasCawpcorc.Len.BUS_OBJ_DATA_ROW);
		halrurqa.run(execContext, dfhcommarea, ws.getWsHallubocLinkage(), ws.getWsHalrurqaLinkage(), wsBusObjDataRow);
		ws.getWsSpecificWorkAreas().setBusObjDataRow(wsBusObjDataRow.getString());
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-HALT-AND-RETURN
		//                   GO TO 7000-CALL-HALRURQA-X
		//               WHEN HALRURQA-REC-NOT-FOUND
		//                   GO TO 7000-CALL-HALRURQA-X
		//               WHEN HALRURQA-REC-FOUND
		//                   GO TO 7000-CALL-HALRURQA-X
		//           END-EVALUATE.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 7000-CALL-HALRURQA-X
			return;
		} else if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound()) {
			// COB_CODE: SET SW-NO-INPUT-FROM-UMT
			//                               TO TRUE
			ws.getSwitches().setInputFromUmtFlag(false);
			// COB_CODE: GO TO 7000-CALL-HALRURQA-X
			return;
		} else if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecFound()) {
			// COB_CODE: SET SW-INPUT-FROM-UMT
			//                               TO TRUE
			ws.getSwitches().setInputFromUmtFlag(true);
			// COB_CODE: GO TO 7000-CALL-HALRURQA-X
			return;
		}
	}

	/**Original name: 8000-TERMINATE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PROCESSING AT END OF PROGRAM                                   *
	 * *****************************************************************</pre>*/
	private void terminate() {
		// COB_CODE: MOVE WS-HALLUBOC-LINKAGE    TO DFHCOMMAREA.
		dfhcommarea.setDfhcommareaBytes(ws.getWsHallubocLinkage().getDfhcommareaBytes());
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(ws.getWsHallubocLinkage().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(ws.getWsHallubocLinkage().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(ws.getWsHallubocLinkage().getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(ws.getWsHallubocLinkage().getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(ws.getWsHallubocLinkage().getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(ws.getWsHallubocLinkage().getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowReqSwitchesStore()) || Characters.EQ_LOW
				.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowRespHeaderStore()) || Characters.EQ_LOW
				.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowRespDataStore()) || Characters.EQ_LOW
				.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowRespWarningsStore()) || Characters.EQ_LOW
				.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowKeyReplaceStore()) || Characters.EQ_LOW
				.test(ws.getWsHallubocLinkage().getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning()
				&& ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsSpecificWorkAreas().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(ws.getWsHallubocLinkage().getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(ws.getWsHallubocLinkage().getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsSpecificWorkAreas().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(ws.getWsHallubocLinkage().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(ws.getWsHallubocLinkage().getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("CAWPCORC", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			ws.getWsHallubocLinkage().setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			ws.getWsHallubocLinkage().setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(ws.getWsHallubocLinkage().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(ws.getWsHallubocLinkage().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWsSpecificWorkAreas().getProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getWsHallubocLinkage().getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		ws.getWsHallubocLinkage().getCommInfo()
				.setUbocNbrNonlogBlErrs(Trunc.toInt(1 + ws.getWsHallubocLinkage().getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(ws.getWsHallubocLinkage().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(ws.getWsHallubocLinkage().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWsSpecificWorkAreas().getProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (ws.getWsHallubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getWsHallubocLinkage().getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		ws.getWsHallubocLinkage().getCommInfo().setUbocNbrWarnings(Trunc.toInt(1 + ws.getWsHallubocLinkage().getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWsSpecificWorkAreas().getApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	public void initCltObjRelationRow() {
		ws.getCawlcorc().getCltObjRelationFixed().setCltObjRelationCsumFormatted("000000000");
		ws.getCawlcorc().getCltObjRelationFixed().setClientIdKcre("");
		ws.getCawlcorc().getCltObjRelationFixed().setCltTypCdKcre("");
		ws.getCawlcorc().getCltObjRelationFixed().setHistoryVldNbrKcre("");
		ws.getCawlcorc().getCltObjRelationFixed().setCicrXrfIdKcre("");
		ws.getCawlcorc().getCltObjRelationFixed().setXrfTypCdKcre("");
		ws.getCawlcorc().getCltObjRelationFixed().setCicrEffDtKcre("");
		ws.getCawlcorc().setTransProcessDt("");
		ws.getCawlcorc().getCltObjRelationKey().setTchObjectKeyCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationKey().setTchObjectKey("");
		ws.getCawlcorc().getCltObjRelationKey().setHistoryVldNbrCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationKey().setHistoryVldNbrSign(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationKey().setHistoryVldNbrFormatted("00000");
		ws.getCawlcorc().getCltObjRelationKey().setCiorEffDtCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationKey().setCiorEffDt("");
		ws.getCawlcorc().getCltObjRelationKey().setObjSysIdCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationKey().setObjSysId("");
		ws.getCawlcorc().getCltObjRelationKey().setCiorObjSeqNbrCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationKey().setCiorObjSeqNbrSign(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationKey().setCiorObjSeqNbrFormatted("00000");
		ws.getCawlcorc().getCltObjRelationData().setClientIdCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setClientId("");
		ws.getCawlcorc().getCltObjRelationData().setRltTypCdCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setRltTypCd("");
		ws.getCawlcorc().getCltObjRelationData().setObjCdCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setObjCd("");
		ws.getCawlcorc().getCltObjRelationData().setCiorShwObjKeyCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorShwObjKey("");
		ws.getCawlcorc().getCltObjRelationData().setAdrSeqNbrCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setAdrSeqNbrSign(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setAdrSeqNbrFormatted("00000");
		ws.getCawlcorc().getCltObjRelationData().setUserIdCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setUserId("");
		ws.getCawlcorc().getCltObjRelationData().setStatusCdCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setStatusCd(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setTerminalIdCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setTerminalId("");
		ws.getCawlcorc().getCltObjRelationData().setCiorExpDtCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorExpDt("");
		ws.getCawlcorc().getCltObjRelationData().setCiorEffAcyTsCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorEffAcyTs("");
		ws.getCawlcorc().getCltObjRelationData().setCiorExpAcyTsCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorExpAcyTs("");
		ws.getCawlcorc().getCltObjRelationData().setCiorLegEntCdCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorLegEntCd("");
		ws.getCawlcorc().getCltObjRelationData().setCiorFstNmCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorFstNm("");
		ws.getCawlcorc().getCltObjRelationData().setCiorLstNmCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorLstNm("");
		ws.getCawlcorc().getCltObjRelationData().setCiorMdlNmCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorMdlNm("");
		ws.getCawlcorc().getCltObjRelationData().setCiorNmPfxCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorNmPfx("");
		ws.getCawlcorc().getCltObjRelationData().setCiorNmSfxCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorNmSfx("");
		ws.getCawlcorc().getCltObjRelationData().setCiorLngNmCi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorLngNmNi(Types.SPACE_CHAR);
		ws.getCawlcorc().getCltObjRelationData().setCiorLngNm("");
		ws.getCawlcorc().setFilterType("");
	}

	public void initCltObjRelationRow1() {
		ws.getCawlc008().getCltObjRelationFixed().setCltObjRelationCsumFormatted("000000000");
		ws.getCawlc008().getCltObjRelationFixed().setClientIdKcre("");
		ws.getCawlc008().getCltObjRelationFixed().setCltTypCdKcre("");
		ws.getCawlc008().getCltObjRelationFixed().setHistoryVldNbrKcre("");
		ws.getCawlc008().getCltObjRelationFixed().setCicrXrfIdKcre("");
		ws.getCawlc008().getCltObjRelationFixed().setXrfTypCdKcre("");
		ws.getCawlc008().getCltObjRelationFixed().setCicrEffDtKcre("");
		ws.getCawlc008().setTransProcessDt("");
		ws.getCawlc008().getCltObjRelationKey().setTchObjectKeyCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationKey().setTchObjectKey("");
		ws.getCawlc008().getCltObjRelationKey().setHistoryVldNbrCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationKey().setHistoryVldNbrSign(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationKey().setHistoryVldNbrFormatted("00000");
		ws.getCawlc008().getCltObjRelationKey().setCiorEffDtCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationKey().setCiorEffDt("");
		ws.getCawlc008().getCltObjRelationKey().setObjSysIdCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationKey().setObjSysId("");
		ws.getCawlc008().getCltObjRelationKey().setCiorObjSeqNbrCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationKey().setCiorObjSeqNbrSign(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationKey().setCiorObjSeqNbrFormatted("00000");
		ws.getCawlc008().getCltObjRelationData().setClientIdCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setClientId("");
		ws.getCawlc008().getCltObjRelationData().setRltTypCdCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setRltTypCd("");
		ws.getCawlc008().getCltObjRelationData().setObjCdCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setObjCd("");
		ws.getCawlc008().getCltObjRelationData().setCiorShwObjKeyCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setCiorShwObjKey("");
		ws.getCawlc008().getCltObjRelationData().setAdrSeqNbrCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setAdrSeqNbrSign(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setAdrSeqNbrFormatted("00000");
		ws.getCawlc008().getCltObjRelationData().setUserIdCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setUserId("");
		ws.getCawlc008().getCltObjRelationData().setStatusCdCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setStatusCd(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setTerminalIdCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setTerminalId("");
		ws.getCawlc008().getCltObjRelationData().setCiorExpDtCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setCiorExpDt("");
		ws.getCawlc008().getCltObjRelationData().setCiorEffAcyTsCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setCiorEffAcyTs("");
		ws.getCawlc008().getCltObjRelationData().setCiorExpAcyTsCi(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setCiorExpAcyTs("");
		ws.getCawlc008().getCltObjRelationData().setAppType(Types.SPACE_CHAR);
		ws.getCawlc008().getCltObjRelationData().setBusObjNm("");
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}

	public void deleteArgListeners() {
		execContext.getCommAreaLenNotifier().deleteListener(dfhcommarea.getFillerDfhcommareaListener());
	}

	public void registerArgListeners() {
		execContext.getCommAreaLenNotifier().addListener(dfhcommarea.getFillerDfhcommareaListener());
		execContext.getCommAreaLenNotifier().notifyListeners();
	}
}
