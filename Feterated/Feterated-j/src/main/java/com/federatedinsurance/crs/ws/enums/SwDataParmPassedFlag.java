/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-DATA-PARM-PASSED-FLAG<br>
 * Variable: SW-DATA-PARM-PASSED-FLAG from program TS547099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwDataParmPassedFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NOT_PASSED = '0';
	public static final char PASSED = '1';

	//==== METHODS ====
	public void setDataParmPassedFlag(char dataParmPassedFlag) {
		this.value = dataParmPassedFlag;
	}

	public char getDataParmPassedFlag() {
		return this.value;
	}

	public boolean isNotPassed() {
		return value == NOT_PASSED;
	}

	public void setNotPassed() {
		value = NOT_PASSED;
	}

	public void setPassed() {
		value = PASSED;
	}
}
