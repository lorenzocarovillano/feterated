/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-01-FATAL-ERROR-MSG<br>
 * Variable: EA-01-FATAL-ERROR-MSG from program TS020000<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01FatalErrorMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG
	private String flr1 = "A system error";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-1
	private String flr2 = "has occurred.";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-2
	private String flr3 = "Please call";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-3
	private String flr4 = "your Helpdesk.";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-4
	private String flr5 = " Failed module";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-5
	private String flr6 = " is";
	//Original name: EA-01-FAILED-MODULE
	private String failedModule = DefaultValues.stringVal(Len.FAILED_MODULE);
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-6
	private String flr7 = ".  Failed";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-7
	private String flr8 = "paragraph is";
	//Original name: EA-01-FAILED-PARAGRAPH
	private String failedParagraph = DefaultValues.stringVal(Len.FAILED_PARAGRAPH);
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-8
	private String flr9 = ".  SqlCode is";
	//Original name: EA-01-SQLCODE-DISPLAY
	private String sqlcodeDisplay = DefaultValues.stringVal(Len.SQLCODE_DISPLAY);
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-9
	private String flr10 = ".  EIBRESP code";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-10
	private String flr11 = " is";
	//Original name: EA-01-EIBRESP-DISPLAY
	private String eibrespDisplay = DefaultValues.stringVal(Len.EIBRESP_DISPLAY);
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-11
	private String flr12 = ".  EIBRESP2";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-12
	private String flr13 = "code is";
	//Original name: EA-01-EIBRESP2-DISPLAY
	private String eibresp2Display = DefaultValues.stringVal(Len.EIBRESP2_DISPLAY);
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-13
	private char flr14 = '.';

	//==== METHODS ====
	public String getEa01FatalErrorMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01FatalErrorMsgBytes());
	}

	public byte[] getEa01FatalErrorMsgBytes() {
		byte[] buffer = new byte[Len.EA01_FATAL_ERROR_MSG];
		return getEa01FatalErrorMsgBytes(buffer, 1);
	}

	public byte[] getEa01FatalErrorMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, failedModule, Len.FAILED_MODULE);
		position += Len.FAILED_MODULE;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		position += Len.FLR8;
		MarshalByte.writeString(buffer, position, failedParagraph, Len.FAILED_PARAGRAPH);
		position += Len.FAILED_PARAGRAPH;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, sqlcodeDisplay, Len.SQLCODE_DISPLAY);
		position += Len.SQLCODE_DISPLAY;
		MarshalByte.writeString(buffer, position, flr10, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr11, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, eibrespDisplay, Len.EIBRESP_DISPLAY);
		position += Len.EIBRESP_DISPLAY;
		MarshalByte.writeString(buffer, position, flr12, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr13, Len.FLR13);
		position += Len.FLR13;
		MarshalByte.writeString(buffer, position, eibresp2Display, Len.EIBRESP2_DISPLAY);
		position += Len.EIBRESP2_DISPLAY;
		MarshalByte.writeChar(buffer, position, flr14);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setFailedModule(String failedModule) {
		this.failedModule = Functions.subString(failedModule, Len.FAILED_MODULE);
	}

	public String getFailedModule() {
		return this.failedModule;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setFailedParagraph(String failedParagraph) {
		this.failedParagraph = Functions.subString(failedParagraph, Len.FAILED_PARAGRAPH);
	}

	public String getFailedParagraph() {
		return this.failedParagraph;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public void setSqlcodeDisplay(long sqlcodeDisplay) {
		this.sqlcodeDisplay = PicFormatter.display("-Z(8)9").format(sqlcodeDisplay).toString();
	}

	public long getSqlcodeDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.sqlcodeDisplay);
	}

	public String getFlr10() {
		return this.flr10;
	}

	public String getFlr11() {
		return this.flr11;
	}

	public void setEibrespDisplay(long eibrespDisplay) {
		this.eibrespDisplay = PicFormatter.display("-Z(8)9").format(eibrespDisplay).toString();
	}

	public long getEibrespDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.eibrespDisplay);
	}

	public String getFlr12() {
		return this.flr12;
	}

	public String getFlr13() {
		return this.flr13;
	}

	public void setEibresp2Display(long eibresp2Display) {
		this.eibresp2Display = PicFormatter.display("-Z(8)9").format(eibresp2Display).toString();
	}

	public long getEibresp2Display() {
		return PicParser.display("-Z(8)9").parseLong(this.eibresp2Display);
	}

	public char getFlr14() {
		return this.flr14;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FAILED_MODULE = 8;
		public static final int FAILED_PARAGRAPH = 30;
		public static final int SQLCODE_DISPLAY = 10;
		public static final int EIBRESP_DISPLAY = 10;
		public static final int EIBRESP2_DISPLAY = 10;
		public static final int FLR1 = 15;
		public static final int FLR3 = 12;
		public static final int FLR5 = 14;
		public static final int FLR6 = 4;
		public static final int FLR7 = 10;
		public static final int FLR8 = 13;
		public static final int FLR13 = 8;
		public static final int FLR14 = 1;
		public static final int EA01_FATAL_ERROR_MSG = FAILED_MODULE + FAILED_PARAGRAPH + SQLCODE_DISPLAY + EIBRESP_DISPLAY + EIBRESP2_DISPLAY + FLR13
				+ FLR14 + 4 * FLR1 + 2 * FLR3 + 2 * FLR5 + 2 * FLR6 + FLR7 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
