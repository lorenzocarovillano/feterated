/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-SYSTEM-TIME-RED<br>
 * Variable: WS-SYSTEM-TIME-RED from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsSystemTimeRed {

	//==== PROPERTIES ====
	//Original name: WS-SYSTEM-HOURS
	private String hours = DefaultValues.stringVal(Len.HOURS);
	//Original name: FILLER-WS-SYSTEM-TIME-RED
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: WS-SYSTEM-MINUTES
	private String minutes = DefaultValues.stringVal(Len.MINUTES);
	//Original name: FILLER-WS-SYSTEM-TIME-RED-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: WS-SYSTEM-SECONDS
	private String seconds = DefaultValues.stringVal(Len.SECONDS);

	//==== METHODS ====
	public void setWsSystemTime(String wsSystemTime) {
		int position = 1;
		byte[] buffer = getWsSystemTimeRedBytes();
		MarshalByte.writeString(buffer, position, wsSystemTime, Len.WS_SYSTEM_TIME);
		setWsSystemTimeRedBytes(buffer);
	}

	/**Original name: WS-SYSTEM-TIME<br>*/
	public String getWsSystemTime() {
		int position = 1;
		return MarshalByte.readString(getWsSystemTimeRedBytes(), position, Len.WS_SYSTEM_TIME);
	}

	public void setWsSystemTimeRedBytes(byte[] buffer) {
		setWsSystemTimeRedBytes(buffer, 1);
	}

	/**Original name: WS-SYSTEM-TIME-RED<br>*/
	public byte[] getWsSystemTimeRedBytes() {
		byte[] buffer = new byte[Len.WS_SYSTEM_TIME_RED];
		return getWsSystemTimeRedBytes(buffer, 1);
	}

	public void setWsSystemTimeRedBytes(byte[] buffer, int offset) {
		int position = offset;
		hours = MarshalByte.readFixedString(buffer, position, Len.HOURS);
		position += Len.HOURS;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		minutes = MarshalByte.readFixedString(buffer, position, Len.MINUTES);
		position += Len.MINUTES;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		seconds = MarshalByte.readFixedString(buffer, position, Len.SECONDS);
	}

	public byte[] getWsSystemTimeRedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, hours, Len.HOURS);
		position += Len.HOURS;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, minutes, Len.MINUTES);
		position += Len.MINUTES;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, seconds, Len.SECONDS);
		return buffer;
	}

	public String getHoursFormatted() {
		return this.hours;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getMinutesFormatted() {
		return this.minutes;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public String getSecondsFormatted() {
		return this.seconds;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HOURS = 2;
		public static final int MINUTES = 2;
		public static final int SECONDS = 2;
		public static final int FLR1 = 1;
		public static final int WS_SYSTEM_TIME_RED = HOURS + MINUTES + SECONDS + 2 * FLR1;
		public static final int WS_SYSTEM_TIME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_SYSTEM_TIME = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
