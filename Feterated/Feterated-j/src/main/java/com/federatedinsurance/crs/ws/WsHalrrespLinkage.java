/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.HalrrespRecFoundSw;
import com.federatedinsurance.crs.ws.enums.HalrurqaFunction;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-HALRRESP-LINKAGE<br>
 * Variable: WS-HALRRESP-LINKAGE from program XZ0F0001<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsHalrrespLinkage extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: HALRRESP-HALRRESP-LIT
	private String halrrespLit = "HALRRESP";
	//Original name: HALRRESP-FUNCTION
	private HalrurqaFunction function = new HalrurqaFunction();
	//Original name: HALRRESP-REC-FOUND-SW
	private HalrrespRecFoundSw recFoundSw = new HalrrespRecFoundSw();
	//Original name: HALRRESP-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: HALRRESP-REC-SEQ
	private String recSeq = DefaultValues.stringVal(Len.REC_SEQ);
	/**Original name: HALRRESP-BUS-OBJ-DATA-LEN<br>
	 * <pre>**     05 HALRRESP-REC-SEQ           PIC 9(03).</pre>*/
	private String busObjDataLen = DefaultValues.stringVal(Len.BUS_OBJ_DATA_LEN);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_HALRRESP_LINKAGE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsHalrrespLinkageBytes(buf);
	}

	public String getWsHalrrespLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getWsHalrrespLinkageBytes());
	}

	public void setWsHalrrespLinkageBytes(byte[] buffer) {
		setWsHalrrespLinkageBytes(buffer, 1);
	}

	public byte[] getWsHalrrespLinkageBytes() {
		byte[] buffer = new byte[Len.WS_HALRRESP_LINKAGE];
		return getWsHalrrespLinkageBytes(buffer, 1);
	}

	public void setWsHalrrespLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		setConstantsBytes(buffer, position);
		position += Len.CONSTANTS;
		setInputLinkageBytes(buffer, position);
		position += Len.INPUT_LINKAGE;
		setOutputLinkageBytes(buffer, position);
		position += Len.OUTPUT_LINKAGE;
		setInputOutputLinkageBytes(buffer, position);
	}

	public byte[] getWsHalrrespLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		getConstantsBytes(buffer, position);
		position += Len.CONSTANTS;
		getInputLinkageBytes(buffer, position);
		position += Len.INPUT_LINKAGE;
		getOutputLinkageBytes(buffer, position);
		position += Len.OUTPUT_LINKAGE;
		getInputOutputLinkageBytes(buffer, position);
		return buffer;
	}

	public void setConstantsBytes(byte[] buffer, int offset) {
		int position = offset;
		halrrespLit = MarshalByte.readString(buffer, position, Len.HALRRESP_LIT);
	}

	public byte[] getConstantsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, halrrespLit, Len.HALRRESP_LIT);
		return buffer;
	}

	public void setHalrrespLit(String halrrespLit) {
		this.halrrespLit = Functions.subString(halrrespLit, Len.HALRRESP_LIT);
	}

	public String getHalrrespLit() {
		return this.halrrespLit;
	}

	public void setInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		function.setFunction(MarshalByte.readChar(buffer, position));
	}

	public byte[] getInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, function.getFunction());
		return buffer;
	}

	public void setOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		recFoundSw.setRecFoundSw(MarshalByte.readChar(buffer, position));
	}

	public byte[] getOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, recFoundSw.getRecFoundSw());
		return buffer;
	}

	public void setInputOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		recSeq = MarshalByte.readFixedString(buffer, position, Len.REC_SEQ);
		position += Len.REC_SEQ;
		busObjDataLen = MarshalByte.readFixedString(buffer, position, Len.BUS_OBJ_DATA_LEN);
	}

	public byte[] getInputOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, recSeq, Len.REC_SEQ);
		position += Len.REC_SEQ;
		MarshalByte.writeString(buffer, position, busObjDataLen, Len.BUS_OBJ_DATA_LEN);
		return buffer;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setRecSeq(int recSeq) {
		this.recSeq = NumericDisplay.asString(recSeq, Len.REC_SEQ);
	}

	public void setRecSeqFormatted(String recSeq) {
		this.recSeq = Trunc.toUnsignedNumeric(recSeq, Len.REC_SEQ);
	}

	public int getRecSeq() {
		return NumericDisplay.asInt(this.recSeq);
	}

	public void setBusObjDataLen(short busObjDataLen) {
		this.busObjDataLen = NumericDisplay.asString(busObjDataLen, Len.BUS_OBJ_DATA_LEN);
	}

	public short getBusObjDataLen() {
		return NumericDisplay.asShort(this.busObjDataLen);
	}

	public HalrurqaFunction getFunction() {
		return function;
	}

	public HalrrespRecFoundSw getRecFoundSw() {
		return recFoundSw;
	}

	@Override
	public byte[] serialize() {
		return getWsHalrrespLinkageBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HALRRESP_LIT = 8;
		public static final int CONSTANTS = HALRRESP_LIT;
		public static final int INPUT_LINKAGE = HalrurqaFunction.Len.FUNCTION;
		public static final int OUTPUT_LINKAGE = HalrrespRecFoundSw.Len.REC_FOUND_SW;
		public static final int BUS_OBJ_NM = 32;
		public static final int REC_SEQ = 5;
		public static final int BUS_OBJ_DATA_LEN = 4;
		public static final int INPUT_OUTPUT_LINKAGE = BUS_OBJ_NM + REC_SEQ + BUS_OBJ_DATA_LEN;
		public static final int WS_HALRRESP_LINKAGE = CONSTANTS + INPUT_LINKAGE + OUTPUT_LINKAGE + INPUT_OUTPUT_LINKAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
