/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DP-DATE-PICTURE<br>
 * Variable: DP-DATE-PICTURE from program XZ0P0021<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DpDatePicture extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: DP-DP-LEN
	private short len = ((short) 10);
	//Original name: DP-DP-PICTURE
	private String picture = "YYYY-MM-DD";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DP_DATE_PICTURE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDpDatePictureBytes(buf);
	}

	public String getDpDatePictureFormatted() {
		byte[] buffer = new byte[Len.DP_DATE_PICTURE];
		return MarshalByteExt.bufferToStr(getDpDatePictureBytes(buffer, 1));
	}

	public void setDpDatePictureBytes(byte[] buffer) {
		setDpDatePictureBytes(buffer, 1);
	}

	public byte[] getDpDatePictureBytes() {
		byte[] buffer = new byte[Len.DP_DATE_PICTURE];
		return getDpDatePictureBytes(buffer, 1);
	}

	public void setDpDatePictureBytes(byte[] buffer, int offset) {
		int position = offset;
		len = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		picture = MarshalByte.readString(buffer, position, Len.PICTURE);
	}

	public byte[] getDpDatePictureBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, len);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, picture, Len.PICTURE);
		return buffer;
	}

	public void setLen(short len) {
		this.len = len;
	}

	public short getLen() {
		return this.len;
	}

	public void setPicture(String picture) {
		this.picture = Functions.subString(picture, Len.PICTURE);
	}

	public String getPicture() {
		return this.picture;
	}

	@Override
	public byte[] serialize() {
		return getDpDatePictureBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LEN = 2;
		public static final int PICTURE = 10;
		public static final int DP_DATE_PICTURE = LEN + PICTURE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
