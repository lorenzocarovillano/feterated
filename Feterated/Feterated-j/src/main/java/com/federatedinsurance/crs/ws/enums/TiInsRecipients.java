/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TI-INS-RECIPIENTS<br>
 * Variable: TI-INS-RECIPIENTS from program XZ0P90E0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TiInsRecipients implements ICopyable<TiInsRecipients> {

	//==== PROPERTIES ====
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TI_INS_RECIPIENTS);
	//Original name: TI-REC-SEQ-NBR
	private int recSeqNbr = DefaultValues.INT_VAL;
	//Original name: TI-REC-CLIENT-ID
	private String recClientId = DefaultValues.stringVal(Len.REC_CLIENT_ID);
	//Original name: TI-REC-ADR-ID
	private String recAdrId = DefaultValues.stringVal(Len.REC_ADR_ID);

	//==== CONSTRUCTORS ====
	public TiInsRecipients() {
	}

	public TiInsRecipients(TiInsRecipients tiInsRecipients) {
		this();
		this.recSeqNbr = tiInsRecipients.recSeqNbr;
		this.recClientId = tiInsRecipients.recClientId;
		this.recAdrId = tiInsRecipients.recAdrId;
	}

	//==== METHODS ====
	public String getTiInsRecipientsFormatted() {
		return MarshalByteExt.bufferToStr(getTiInsRecipientsBytes());
	}

	public byte[] getTiInsRecipientsBytes() {
		byte[] buffer = new byte[Len.TI_INS_RECIPIENTS];
		return getTiInsRecipientsBytes(buffer, 1);
	}

	public byte[] getTiInsRecipientsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeInt(buffer, position, recSeqNbr, Len.REC_SEQ_NBR);
		position += Len.REC_SEQ_NBR;
		MarshalByte.writeString(buffer, position, recClientId, Len.REC_CLIENT_ID);
		position += Len.REC_CLIENT_ID;
		MarshalByte.writeString(buffer, position, recAdrId, Len.REC_ADR_ID);
		return buffer;
	}

	public TiInsRecipients initTiInsRecipientsHighValues() {
		recSeqNbr = Types.HIGH_INT_VAL;
		recClientId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_CLIENT_ID);
		recAdrId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_ADR_ID);
		return this;
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTiInsRecipientsFormatted()).equals(END_OF_TABLE);
	}

	public void setRecSeqNbr(int recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	public int getRecSeqNbr() {
		return this.recSeqNbr;
	}

	public void setRecClientId(String recClientId) {
		this.recClientId = Functions.subString(recClientId, Len.REC_CLIENT_ID);
	}

	public String getRecClientId() {
		return this.recClientId;
	}

	public void setRecAdrId(String recAdrId) {
		this.recAdrId = Functions.subString(recAdrId, Len.REC_ADR_ID);
	}

	public String getRecAdrId() {
		return this.recAdrId;
	}

	@Override
	public TiInsRecipients copy() {
		return new TiInsRecipients(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_SEQ_NBR = 5;
		public static final int REC_CLIENT_ID = 64;
		public static final int REC_ADR_ID = 64;
		public static final int TI_INS_RECIPIENTS = REC_SEQ_NBR + REC_CLIENT_ID + REC_ADR_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
