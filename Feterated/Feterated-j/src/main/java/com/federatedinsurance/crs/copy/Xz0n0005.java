/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

/**Original name: XZ0N0005<br>
 * Variable: XZ0N0005 from copybook XZ0N0005<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz0n0005 {

	//==== PROPERTIES ====
	//Original name: XZN005-TRANS-PROCESS-DT
	private String transProcessDt = "Transaction Processing Date";
	//Original name: XZN005-PROCESSING-CONTEXT-TEXT
	private Xzn005ProcessingContextText processingContextText = new Xzn005ProcessingContextText();

	//==== METHODS ====
	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public Xzn005ProcessingContextText getProcessingContextText() {
		return processingContextText;
	}
}
