/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-LEAP-YEAR-IND<br>
 * Variable: WS-LEAP-YEAR-IND from program HALRVDT1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsLeapYearInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char LEAP_YEAR = 'Y';
	public static final char NOT_LEAP_YEAR = 'N';

	//==== METHODS ====
	public void setWsLeapYearInd(char wsLeapYearInd) {
		this.value = wsLeapYearInd;
	}

	public char getWsLeapYearInd() {
		return this.value;
	}

	public void setLeapYear() {
		value = LEAP_YEAR;
	}

	public boolean isNotLeapYear() {
		return value == NOT_LEAP_YEAR;
	}

	public void setNotLeapYear() {
		value = NOT_LEAP_YEAR;
	}
}
