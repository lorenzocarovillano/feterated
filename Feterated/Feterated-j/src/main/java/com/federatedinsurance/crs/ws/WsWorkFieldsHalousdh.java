/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-WORK-FIELDS<br>
 * Variable: WS-WORK-FIELDS from program HALOUSDH<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkFieldsHalousdh {

	//==== PROPERTIES ====
	//Original name: WS-DATA-PRIV-MDU
	private String wsDataPrivMdu = "";
	//Original name: WS-DATA-PRIV-CTX-MDU
	private String wsDataPrivCtxMdu = "";
	//Original name: WS-HOLD-PASS-THRU-ACTION
	private String wsHoldPassThruAction = "";
	//Original name: WS-AUTH-USERID-FOR-CURSOR
	private String wsAuthUseridForCursor = "";
	//Original name: WS-CUR1-BOBJ-NM
	private String wsCur1BobjNm = DefaultValues.stringVal(Len.WS_CUR1_BOBJ_NM);
	//Original name: WS-CONTEXT-REDEF
	private WsContextRedef wsContextRedef = new WsContextRedef();
	/**Original name: WS-CONTEXT-FIRST-10-FOR-CSR<br>
	 * <pre> ADDED NEXT WS FIELD FOR CURSOR.  FIELDS THAT ARE NOT THE SAME
	 *  AS WHAT IS BEING FETCHED CAUSE A TABLESPACE SCAN.</pre>*/
	private String wsContextFirst10ForCsr = DefaultValues.stringVal(Len.WS_CONTEXT_FIRST10_FOR_CSR);

	//==== METHODS ====
	public void setWsDataPrivMdu(String wsDataPrivMdu) {
		this.wsDataPrivMdu = Functions.subString(wsDataPrivMdu, Len.WS_DATA_PRIV_MDU);
	}

	public String getWsDataPrivMdu() {
		return this.wsDataPrivMdu;
	}

	public void setWsDataPrivCtxMdu(String wsDataPrivCtxMdu) {
		this.wsDataPrivCtxMdu = Functions.subString(wsDataPrivCtxMdu, Len.WS_DATA_PRIV_CTX_MDU);
	}

	public String getWsDataPrivCtxMdu() {
		return this.wsDataPrivCtxMdu;
	}

	public void setWsHoldPassThruAction(String wsHoldPassThruAction) {
		this.wsHoldPassThruAction = Functions.subString(wsHoldPassThruAction, Len.WS_HOLD_PASS_THRU_ACTION);
	}

	public String getWsHoldPassThruAction() {
		return this.wsHoldPassThruAction;
	}

	public void setWsAuthUseridForCursor(String wsAuthUseridForCursor) {
		this.wsAuthUseridForCursor = Functions.subString(wsAuthUseridForCursor, Len.WS_AUTH_USERID_FOR_CURSOR);
	}

	public String getWsAuthUseridForCursor() {
		return this.wsAuthUseridForCursor;
	}

	public void setWsCur1BobjNm(String wsCur1BobjNm) {
		this.wsCur1BobjNm = Functions.subString(wsCur1BobjNm, Len.WS_CUR1_BOBJ_NM);
	}

	public String getWsCur1BobjNm() {
		return this.wsCur1BobjNm;
	}

	public String getWsCur1BobjNmFormatted() {
		return Functions.padBlanks(getWsCur1BobjNm(), Len.WS_CUR1_BOBJ_NM);
	}

	public void setWsContextFirst10ForCsr(String wsContextFirst10ForCsr) {
		this.wsContextFirst10ForCsr = Functions.subString(wsContextFirst10ForCsr, Len.WS_CONTEXT_FIRST10_FOR_CSR);
	}

	public String getWsContextFirst10ForCsr() {
		return this.wsContextFirst10ForCsr;
	}

	public WsContextRedef getWsContextRedef() {
		return wsContextRedef;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_AUTH_USERID_FOR_CURSOR = 8;
		public static final int WS_CONTEXT_FIRST10_FOR_CSR = 20;
		public static final int WS_CUR1_BOBJ_NM = 32;
		public static final int WS_HOLD_PASS_THRU_ACTION = 20;
		public static final int WS_DATA_PRIV_MDU = 32;
		public static final int WS_DATA_PRIV_CTX_MDU = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
