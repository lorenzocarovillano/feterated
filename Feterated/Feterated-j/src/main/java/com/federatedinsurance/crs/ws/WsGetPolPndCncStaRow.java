/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-GET-POL-PND-CNC-STA-ROW<br>
 * Variable: WS-GET-POL-PND-CNC-STA-ROW from program XZ0P0023<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsGetPolPndCncStaRow extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZY023-OPERATION
	private String operation = DefaultValues.stringVal(Len.OPERATION);
	public static final String GET_POL_PND_CNC_STA = "GetPolPndCncStatus";
	//Original name: XZY023-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZY023-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZY023-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZY023-PND-CNC-IND
	private char pndCncInd = DefaultValues.CHAR_VAL;
	//Original name: XZY023-SCH-CNC-DT
	private String schCncDt = DefaultValues.stringVal(Len.SCH_CNC_DT);
	//Original name: XZY023-NOT-TYP-CD
	private String notTypCd = DefaultValues.stringVal(Len.NOT_TYP_CD);
	//Original name: XZY023-NOT-TYP-DES
	private String notTypDes = DefaultValues.stringVal(Len.NOT_TYP_DES);
	//Original name: XZY023-NOT-EMP-ID
	private String notEmpId = DefaultValues.stringVal(Len.NOT_EMP_ID);
	//Original name: XZY023-NOT-EMP-NM
	private String notEmpNm = DefaultValues.stringVal(Len.NOT_EMP_NM);
	//Original name: XZY023-NOT-PRC-DT
	private String notPrcDt = DefaultValues.stringVal(Len.NOT_PRC_DT);
	//Original name: FILLER-XZY023-GET-POL-PND-CNC-STA-ROW
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_GET_POL_PND_CNC_STA_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsGetPolPndCncStaRowBytes(buf);
	}

	public String getWsGetPolPndCncStaRowFormatted() {
		return getGetPolPndCncStaRowFormatted();
	}

	public void setWsGetPolPndCncStaRowBytes(byte[] buffer) {
		setWsGetPolPndCncStaRowBytes(buffer, 1);
	}

	public byte[] getWsGetPolPndCncStaRowBytes() {
		byte[] buffer = new byte[Len.WS_GET_POL_PND_CNC_STA_ROW];
		return getWsGetPolPndCncStaRowBytes(buffer, 1);
	}

	public void setWsGetPolPndCncStaRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetPolPndCncStaRowBytes(buffer, position);
	}

	public byte[] getWsGetPolPndCncStaRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetPolPndCncStaRowBytes(buffer, position);
		return buffer;
	}

	public String getGetPolPndCncStaRowFormatted() {
		return MarshalByteExt.bufferToStr(getGetPolPndCncStaRowBytes());
	}

	/**Original name: XZY023-GET-POL-PND-CNC-STA-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0Y0023 - XZ_GET_POL_PND_CNC_STA                              *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  WR#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 * 20163.41 09/25/2018  E404CDT   NEW                              *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getGetPolPndCncStaRowBytes() {
		byte[] buffer = new byte[Len.GET_POL_PND_CNC_STA_ROW];
		return getGetPolPndCncStaRowBytes(buffer, 1);
	}

	public void setGetPolPndCncStaRowBytes(byte[] buffer, int offset) {
		int position = offset;
		operation = MarshalByte.readString(buffer, position, Len.OPERATION);
		position += Len.OPERATION;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		polExpDt = MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		pndCncInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		schCncDt = MarshalByte.readString(buffer, position, Len.SCH_CNC_DT);
		position += Len.SCH_CNC_DT;
		notTypCd = MarshalByte.readString(buffer, position, Len.NOT_TYP_CD);
		position += Len.NOT_TYP_CD;
		notTypDes = MarshalByte.readString(buffer, position, Len.NOT_TYP_DES);
		position += Len.NOT_TYP_DES;
		notEmpId = MarshalByte.readString(buffer, position, Len.NOT_EMP_ID);
		position += Len.NOT_EMP_ID;
		notEmpNm = MarshalByte.readString(buffer, position, Len.NOT_EMP_NM);
		position += Len.NOT_EMP_NM;
		notPrcDt = MarshalByte.readString(buffer, position, Len.NOT_PRC_DT);
		position += Len.NOT_PRC_DT;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getGetPolPndCncStaRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, operation, Len.OPERATION);
		position += Len.OPERATION;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeChar(buffer, position, pndCncInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, schCncDt, Len.SCH_CNC_DT);
		position += Len.SCH_CNC_DT;
		MarshalByte.writeString(buffer, position, notTypCd, Len.NOT_TYP_CD);
		position += Len.NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, notTypDes, Len.NOT_TYP_DES);
		position += Len.NOT_TYP_DES;
		MarshalByte.writeString(buffer, position, notEmpId, Len.NOT_EMP_ID);
		position += Len.NOT_EMP_ID;
		MarshalByte.writeString(buffer, position, notEmpNm, Len.NOT_EMP_NM);
		position += Len.NOT_EMP_NM;
		MarshalByte.writeString(buffer, position, notPrcDt, Len.NOT_PRC_DT);
		position += Len.NOT_PRC_DT;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setOperation(String operation) {
		this.operation = Functions.subString(operation, Len.OPERATION);
	}

	public String getOperation() {
		return this.operation;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setPndCncInd(char pndCncInd) {
		this.pndCncInd = pndCncInd;
	}

	public char getPndCncInd() {
		return this.pndCncInd;
	}

	public void setSchCncDt(String schCncDt) {
		this.schCncDt = Functions.subString(schCncDt, Len.SCH_CNC_DT);
	}

	public String getSchCncDt() {
		return this.schCncDt;
	}

	public void setNotTypCd(String notTypCd) {
		this.notTypCd = Functions.subString(notTypCd, Len.NOT_TYP_CD);
	}

	public String getNotTypCd() {
		return this.notTypCd;
	}

	public void setNotTypDes(String notTypDes) {
		this.notTypDes = Functions.subString(notTypDes, Len.NOT_TYP_DES);
	}

	public String getNotTypDes() {
		return this.notTypDes;
	}

	public void setNotEmpId(String notEmpId) {
		this.notEmpId = Functions.subString(notEmpId, Len.NOT_EMP_ID);
	}

	public String getNotEmpId() {
		return this.notEmpId;
	}

	public void setNotEmpNm(String notEmpNm) {
		this.notEmpNm = Functions.subString(notEmpNm, Len.NOT_EMP_NM);
	}

	public String getNotEmpNm() {
		return this.notEmpNm;
	}

	public void setNotPrcDt(String notPrcDt) {
		this.notPrcDt = Functions.subString(notPrcDt, Len.NOT_PRC_DT);
	}

	public String getNotPrcDt() {
		return this.notPrcDt;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	@Override
	public byte[] serialize() {
		return getWsGetPolPndCncStaRowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPERATION = 32;
		public static final int POL_NBR = 25;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int PND_CNC_IND = 1;
		public static final int SCH_CNC_DT = 10;
		public static final int NOT_TYP_CD = 5;
		public static final int NOT_TYP_DES = 35;
		public static final int NOT_EMP_ID = 6;
		public static final int NOT_EMP_NM = 67;
		public static final int NOT_PRC_DT = 10;
		public static final int FLR1 = 100;
		public static final int GET_POL_PND_CNC_STA_ROW = OPERATION + POL_NBR + POL_EFF_DT + POL_EXP_DT + PND_CNC_IND + SCH_CNC_DT + NOT_TYP_CD
				+ NOT_TYP_DES + NOT_EMP_ID + NOT_EMP_NM + NOT_PRC_DT + FLR1;
		public static final int WS_GET_POL_PND_CNC_STA_ROW = GET_POL_PND_CNC_STA_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
