/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.CommunicationShellCommon;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-HUB-DATA<br>
 * Variable: WS-HUB-DATA from program TS020000<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsHubData extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: UBOC-RECORD
	private UbocRecord ubocRecord = new UbocRecord();
	//Original name: COMMUNICATION-SHELL-COMMON
	private CommunicationShellCommon communicationShellCommon = new CommunicationShellCommon();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_HUB_DATA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsHubDataBytes(buf);
	}

	public void setWsHubDataBytes(byte[] buffer) {
		setWsHubDataBytes(buffer, 1);
	}

	public byte[] getWsHubDataBytes() {
		byte[] buffer = new byte[Len.WS_HUB_DATA];
		return getWsHubDataBytes(buffer, 1);
	}

	public void setWsHubDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ubocRecord.setUbocRecordBytes(buffer, position);
		position += UbocRecord.Len.UBOC_RECORD;
		communicationShellCommon.setCommunicationShellCommonBytes(buffer, position);
	}

	public byte[] getWsHubDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ubocRecord.getUbocRecordBytes(buffer, position);
		position += UbocRecord.Len.UBOC_RECORD;
		communicationShellCommon.getCommunicationShellCommonBytes(buffer, position);
		return buffer;
	}

	public CommunicationShellCommon getCommunicationShellCommon() {
		return communicationShellCommon;
	}

	public UbocRecord getUbocRecord() {
		return ubocRecord;
	}

	@Override
	public byte[] serialize() {
		return getWsHubDataBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_HUB_DATA = UbocRecord.Len.UBOC_RECORD + CommunicationShellCommon.Len.COMMUNICATION_SHELL_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
