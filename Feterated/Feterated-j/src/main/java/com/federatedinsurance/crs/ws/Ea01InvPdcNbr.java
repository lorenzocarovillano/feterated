/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-INV-PDC-NBR<br>
 * Variable: EA-01-INV-PDC-NBR from program XZ0D0001<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01InvPdcNbr {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-INV-PDC-NBR
	private String flr1 = "Producer number";
	//Original name: FILLER-EA-01-INV-PDC-NBR-1
	private String flr2 = " of";
	//Original name: EA-01-PDC-NBR
	private String pdcNbr = DefaultValues.stringVal(Len.PDC_NBR);
	//Original name: FILLER-EA-01-INV-PDC-NBR-2
	private String flr3 = " is not in";
	//Original name: FILLER-EA-01-INV-PDC-NBR-3
	private String flr4 = "format of #-###";
	//Original name: FILLER-EA-01-INV-PDC-NBR-4
	private String flr5 = " for account";
	//Original name: EA-01-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-01-INV-PDC-NBR-5
	private String flr6 = ", process time";
	//Original name: FILLER-EA-01-INV-PDC-NBR-6
	private String flr7 = "stamp date";
	//Original name: EA-01-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-01-INV-PDC-NBR-7
	private char flr8 = '.';

	//==== METHODS ====
	public String getEa01InvPdcNbrFormatted() {
		return MarshalByteExt.bufferToStr(getEa01InvPdcNbrBytes());
	}

	public byte[] getEa01InvPdcNbrBytes() {
		byte[] buffer = new byte[Len.EA01_INV_PDC_NBR];
		return getEa01InvPdcNbrBytes(buffer, 1);
	}

	public byte[] getEa01InvPdcNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, pdcNbr, Len.PDC_NBR);
		position += Len.PDC_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr8);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setPdcNbr(String pdcNbr) {
		this.pdcNbr = Functions.subString(pdcNbr, Len.PDC_NBR);
	}

	public String getPdcNbr() {
		return this.pdcNbr;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PDC_NBR = 5;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 15;
		public static final int FLR2 = 4;
		public static final int FLR3 = 11;
		public static final int FLR5 = 13;
		public static final int FLR6 = 14;
		public static final int FLR8 = 1;
		public static final int EA01_INV_PDC_NBR = PDC_NBR + CSR_ACT_NBR + NOT_PRC_TS + 2 * FLR1 + FLR2 + 2 * FLR3 + FLR5 + FLR6 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
