/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-SPECIFIC-MISC<br>
 * Variable: WS-SPECIFIC-MISC from program XZ0D0002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSpecificMiscXz0d0002 {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0D0002";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-APPLICATION-LOK-TAB
	private String applicationLokTab = "XXX";
	//Original name: WS-APPLICATION-LOK-NM
	private String applicationLokNm = "CRS";
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "ACT_NOT_POL";
	/**Original name: WS-CHILDREN<br>
	 * <pre>* LIST ALL DIRECT AND FOREIGN CHILDREN OF THIS BDO HERE.</pre>*/
	private String children = "";
	private static final String[] DIRECT_S3_CHILDREN = new String[] { "ACT_NOT_WRD", "ACT_NOT_POL_FRM", "ACT_NOT_POL_REC" };

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getApplicationLokTab() {
		return this.applicationLokTab;
	}

	public String getApplicationLokNm() {
		return this.applicationLokNm;
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setChildren(String children) {
		this.children = Functions.subString(children, Len.CHILDREN);
	}

	public String getChildren() {
		return this.children;
	}

	public boolean isDirectS3Children() {
		return ArrayUtils.contains(DIRECT_S3_CHILDREN, children);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUS_OBJ_NM = 32;
		public static final int CHILDREN = 32;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
