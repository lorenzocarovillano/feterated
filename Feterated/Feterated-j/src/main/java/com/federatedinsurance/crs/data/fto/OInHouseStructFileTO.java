/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;

/**Original name: O-IN-HOUSE-STRUCT-FILE<br>
 * File: O-IN-HOUSE-STRUCT-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OInHouseStructFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: IHO-STRUCT-CARRIAGE-CTRL
	private String ihoStructCarriageCtrl = DefaultValues.stringVal(Len.IHO_STRUCT_CARRIAGE_CTRL);
	//Original name: IHO-STRUCT-DATA
	private String ihoStructData = DefaultValues.stringVal(Len.IHO_STRUCT_DATA);

	//==== METHODS ====
	public void setoInHouseStructRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		setIhoStructCarriageCtrlXBytes(buffer, position);
		position += Len.IHO_STRUCT_CARRIAGE_CTRL_X;
		ihoStructData = MarshalByte.readString(buffer, position, Len.IHO_STRUCT_DATA);
	}

	public byte[] getoInHouseStructRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		getIhoStructCarriageCtrlXBytes(buffer, position);
		position += Len.IHO_STRUCT_CARRIAGE_CTRL_X;
		MarshalByte.writeString(buffer, position, ihoStructData, Len.IHO_STRUCT_DATA);
		return buffer;
	}

	public void setIhoStructCarriageCtrlXFormatted(String data) {
		byte[] buffer = new byte[Len.IHO_STRUCT_CARRIAGE_CTRL_X];
		MarshalByte.writeString(buffer, 1, data, Len.IHO_STRUCT_CARRIAGE_CTRL_X);
		setIhoStructCarriageCtrlXBytes(buffer, 1);
	}

	public void setIhoStructCarriageCtrlXBytes(byte[] buffer, int offset) {
		int position = offset;
		ihoStructCarriageCtrl = MarshalByte.readFixedString(buffer, position, Len.IHO_STRUCT_CARRIAGE_CTRL);
	}

	public byte[] getIhoStructCarriageCtrlXBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ihoStructCarriageCtrl, Len.IHO_STRUCT_CARRIAGE_CTRL);
		return buffer;
	}

	public void setIhoStructData(String ihoStructData) {
		this.ihoStructData = Functions.subString(ihoStructData, Len.IHO_STRUCT_DATA);
	}

	public String getIhoStructData() {
		return this.ihoStructData;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoInHouseStructRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoInHouseStructRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_IN_HOUSE_STRUCT_RECORD;
	}

	@Override
	public IBuffer copy() {
		OInHouseStructFileTO copyTO = new OInHouseStructFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int IHO_STRUCT_CARRIAGE_CTRL = 1;
		public static final int IHO_STRUCT_CARRIAGE_CTRL_X = IHO_STRUCT_CARRIAGE_CTRL;
		public static final int IHO_STRUCT_DATA = 132;
		public static final int O_IN_HOUSE_STRUCT_RECORD = IHO_STRUCT_CARRIAGE_CTRL_X + IHO_STRUCT_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
