/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CAWLF002<br>
 * Variable: CAWLF002 from copybook CAWLF002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cawlf002 {

	//==== PROPERTIES ====
	//Original name: CW02F-CLIENT-TAB-CHK-SUM
	private String clientTabChkSum = DefaultValues.stringVal(Len.CLIENT_TAB_CHK_SUM);
	//Original name: CW02F-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CW02F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW02F-CLIENT-TAB-KEY
	private Cw02fClientTabKey clientTabKey = new Cw02fClientTabKey();
	//Original name: CW02F-CLIENT-TAB-DATA
	private Cw02fClientTabData clientTabData = new Cw02fClientTabData();

	//==== METHODS ====
	public void setClientTabRowFormatted(String data) {
		byte[] buffer = new byte[Len.CLIENT_TAB_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CLIENT_TAB_ROW);
		setClientTabRowBytes(buffer, 1);
	}

	public String getClientTabRowFormatted() {
		return MarshalByteExt.bufferToStr(getClientTabRowBytes());
	}

	/**Original name: CW02F-CLIENT-TAB-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CAWLF002 - CLIENT_TAB TABLE                                    *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  PP00015  03/07/2007 E404ASW   NEW COPYBOOK                     *
	 *  SM1320-3 02/01/2012 E404DAP   ADDED ACQUISITION SOURCE FIELDS  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getClientTabRowBytes() {
		byte[] buffer = new byte[Len.CLIENT_TAB_ROW];
		return getClientTabRowBytes(buffer, 1);
	}

	public void setClientTabRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setClientTabFixedBytes(buffer, position);
		position += Len.CLIENT_TAB_FIXED;
		setClientTabDatesBytes(buffer, position);
		position += Len.CLIENT_TAB_DATES;
		clientTabKey.setClientTabKeyBytes(buffer, position);
		position += Cw02fClientTabKey.Len.CLIENT_TAB_KEY;
		clientTabData.setClientTabDataBytes(buffer, position);
	}

	public byte[] getClientTabRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getClientTabFixedBytes(buffer, position);
		position += Len.CLIENT_TAB_FIXED;
		getClientTabDatesBytes(buffer, position);
		position += Len.CLIENT_TAB_DATES;
		clientTabKey.getClientTabKeyBytes(buffer, position);
		position += Cw02fClientTabKey.Len.CLIENT_TAB_KEY;
		clientTabData.getClientTabDataBytes(buffer, position);
		return buffer;
	}

	public void setClientTabFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		clientTabChkSum = MarshalByte.readFixedString(buffer, position, Len.CLIENT_TAB_CHK_SUM);
		position += Len.CLIENT_TAB_CHK_SUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
	}

	public byte[] getClientTabFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientTabChkSum, Len.CLIENT_TAB_CHK_SUM);
		position += Len.CLIENT_TAB_CHK_SUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		return buffer;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setClientTabDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getClientTabDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public Cw02fClientTabData getClientTabData() {
		return clientTabData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_TAB_CHK_SUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLIENT_TAB_FIXED = CLIENT_TAB_CHK_SUM + CLIENT_ID_KCRE;
		public static final int CLIENT_TAB_DATES = TRANS_PROCESS_DT;
		public static final int CLIENT_TAB_ROW = CLIENT_TAB_FIXED + CLIENT_TAB_DATES + Cw02fClientTabKey.Len.CLIENT_TAB_KEY
				+ Cw02fClientTabData.Len.CLIENT_TAB_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
