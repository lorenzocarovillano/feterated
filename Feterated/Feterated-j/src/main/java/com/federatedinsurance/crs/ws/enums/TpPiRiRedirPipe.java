/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TP-PI-RI-REDIR-PIPE<br>
 * Variable: TP-PI-RI-REDIR-PIPE from program TS547099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TpPiRiRedirPipe {

	//==== PROPERTIES ====
	private short value = DefaultValues.BIN_SHORT_VAL;
	public static final short IS_REDIRECTED_MIN = ((short) 1);
	public static final short IS_REDIRECTED_MAX = ((short) 32767);
	public static final short NOT_REDIRECTED = ((short) 0);

	//==== METHODS ====
	public void setRiRedirPipe(short riRedirPipe) {
		this.value = riRedirPipe;
	}

	public short getRiRedirPipe() {
		return this.value;
	}

	public boolean isIsRedirected() {
		short fld = value;
		return fld >= IS_REDIRECTED_MIN && fld <= IS_REDIRECTED_MAX;
	}

	public boolean isNotRedirected() {
		return value == NOT_REDIRECTED;
	}

	public void setNotRedirected() {
		value = NOT_REDIRECTED;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RI_REDIR_PIPE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
