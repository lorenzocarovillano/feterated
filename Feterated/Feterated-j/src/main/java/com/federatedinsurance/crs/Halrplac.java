/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.HalrplacData;
import com.federatedinsurance.crs.ws.LiErrAlltxtText;
import com.federatedinsurance.crs.ws.LiErrColName1;
import com.federatedinsurance.crs.ws.LiErrColValue1;
import com.federatedinsurance.crs.ws.LiErrContextText;
import com.federatedinsurance.crs.ws.LioMess;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;

/**Original name: HALRPLAC<br>
 * <pre>AUTHOR.
 *                   J. A. FULCHER.
 * DATE-WRITTEN.
 *                   SEP 2000.
 * DATE-COMPILED.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  SUBSTITUTE PLACEHOLDER VALUES INTO A       **
 * *                  NON LOGGABLE ERROR/WARNING MESSAGE.        **
 * *                                                             **
 * * PLATFORM - IBM MAINFRAME                                    **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -        SUBSTITUTE PLACEHOLDER VALUES INTO A       **
 * *                  NON LOGGABLE ERROR/WARNING MESSAGE.        **
 * *                  PLACEHOLDERS ARE IDENTIFIED BY             **
 * *                  <N> WHERE N = A DIGIT  E.G. <1>            **
 * *                                                             **
 * *                  VALID PLACEHOLDERS ARE:                    **
 * *                                                             **
 * *                  <1>- ERRORED COLUMN NAME #1 (ENGLISH STYLE)**
 * *                  <2>- ERRORED COLUMN VALUE #1               **
 * *                  <3>- ERRORED COLUMN NAME #2 (ENGLISH STYLE)**
 * *                  <4>- ERRORED COLUMN VALUE #2               **
 * *                  <5>- ERROR CONTEXT TEXT (E.G. CLIENT)      **
 * *                       (SOME WAY OF SPECIFYING THE OBJECT    **
 * *                       ON WHICH THE ERROR OCCURED)           **
 * *                  <6>- ERROR CONTEXT KEY                     **
 * *                       E.G. JULIAN FULCHER                   **
 * *                       (SOME WAY OF SPECIFYING THE KEY OF    **
 * *                       THE OBJECT ON WHICH THE ERROR OCCURED)**
 * *                  <7>- ENTIRE 500 BYTE NON-LOGGABLE ERROR/   **
 * *                       WARNING MESSAGE TEXT.                 **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED AS FOLLOWS:   **
 * *                       1) STATICALLY CALLED BY A MODULE      **
 * *                                                             **
 * * DATA ACCESS METHODS - DATA PASSED IN LINKAGE                **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #     DATE      PRGMR    DESCRIPTION                     **
 * * -------- --------- -------  --------------------------------**
 * * SAVANNAH 23SEP00   JAF      NEW ROUTINE.                    **
 * * C14849   22JUN01   ARSI600  CHANGES TO INCREASE LENGTH OF   **
 * *                             NON-LOGGABLE MESSAGES.          **
 * * 24179A   25JUN02   SE3H929  ADDED A SINGLE 500 BYTE         **
 * *                             NON-LOGGABLE ERROR/WARNING      **
 * *                             MESSAGE TEXT FIELD.             **
 * ****************************************************************</pre>*/
public class Halrplac extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private HalrplacData ws = new HalrplacData();
	//Original name: LI-ERR-COL-NAME1
	private LiErrColName1 liErrColName1;
	//Original name: LI-ERR-COL-VALUE1
	private LiErrColValue1 liErrColValue1;
	//Original name: LI-ERR-COL-NAME2
	private LiErrColName1 liErrColName2;
	//Original name: LI-ERR-COL-VALUE2
	private LiErrColValue1 liErrColValue2;
	//Original name: LI-ERR-CONTEXT-TEXT
	private LiErrContextText liErrContextText;
	//Original name: LI-ERR-CONTEXT-VALUE
	private LiErrContextText liErrContextValue;
	//Original name: LI-ERR-ALLTXT-TEXT
	private LiErrAlltxtText liErrAlltxtText;
	//Original name: LIO-MESS
	private LioMess lioMess;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LiErrColName1 liErrColName1, LiErrColValue1 liErrColValue1, LiErrColName1 liErrColName2, LiErrColValue1 liErrColValue2,
			LiErrContextText liErrContextText, LiErrContextText liErrContextValue, LiErrAlltxtText liErrAlltxtText, LioMess lioMess) {
		this.liErrColName1 = liErrColName1;
		this.liErrColValue1 = liErrColValue1;
		this.liErrColName2 = liErrColName2;
		this.liErrColValue2 = liErrColValue2;
		this.liErrContextText = liErrContextText;
		this.liErrContextValue = liErrContextValue;
		this.liErrAlltxtText = liErrAlltxtText;
		this.lioMess = lioMess;
		main1();
		mainX();
		return 0;
	}

	public static Halrplac getInstance() {
		return (Programs.getInstance(Halrplac.class));
	}

	/**Original name: 0000-MAIN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONTROLS PROCESSING                                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void main1() {
		// COB_CODE: PERFORM 0100-INITIALIZATION.
		initialization();
		// COB_CODE: PERFORM 0200-SUBSTITUTE-PLACEHOLDERS.
		substitutePlaceholders();
		// COB_CODE: PERFORM 0900-FINALIZATION.
		finalization();
	}

	/**Original name: 0000-MAIN-X<br>*/
	private void mainX() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 0100-INITIALIZATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PERFORM INITIAL PROCESSING.                                    *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void initialization() {
		// COB_CODE: MOVE SPACES TO WS-ASSEM-STRING.
		ws.getWsWorkFields().initAssemStringSpaces();
	}

	/**Original name: 0200-SUBSTITUTE-PLACEHOLDERS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   CONTROLS THE SUBSTITUTION OF PLACEHOLDERS.                    *
	 *                                                                 *
	 * *****************************************************************
	 * * INIT</pre>*/
	private void substitutePlaceholders() {
		// COB_CODE: SET NOT-END-OF-MESS  TO TRUE.
		ws.getWsWorkFields().getEndOfMessSw().setNotEndOfMess();
		// COB_CODE: SET MESS-IND TO 1.
		ws.setMessInd(1);
		// COB_CODE: SET NOT-END-OF-ASSEM TO TRUE.
		ws.getWsWorkFields().getEndOfAssemSw().setNotEndOfAssem();
		// COB_CODE: SET ASSEM-IND TO 1.
		ws.setAssemInd(1);
		// COB_CODE: SET ASSEM-IND DOWN BY 1.
		ws.setAssemInd(Trunc.toInt(ws.getAssemInd() - 1, 9));
		//* LOOP AROUND REPLACING PLACEHOLDERS.
		// COB_CODE: PERFORM 0300-SUBSTITUTE-PLACEHOLDER
		//             UNTIL END-OF-MESS
		//                OR END-OF-ASSEM.
		while (!(ws.getWsWorkFields().getEndOfMessSw().isEndOfMess() || ws.getWsWorkFields().getEndOfAssemSw().isEndOfAssem())) {
			substitutePlaceholder();
		}
	}

	/**Original name: 0300-SUBSTITUTE-PLACEHOLDER_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   COPY MESSAGE ACROSS FROM INPUT MESSAGE BUFFER TO ASSEMBLY     *
	 *   BUFFER UNTIL PLACEHOLDER FOUND OR END OF INPUT MESSAGE.       *
	 *   IF PLACEHOLDER FOUND, ATTEMPT TO SUBSTITUTE IT.               *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void substitutePlaceholder() {
		// COB_CODE: SET PLACEHOLDER-NOT-FOUND TO TRUE.
		ws.getWsWorkFields().getPlaceholderFoundSw().setNotFound();
		//* ATTEMPT TO FIND NEXT PLACEHOLDER
		// COB_CODE: PERFORM 0400-FIND-PLACEHOLDER
		//               UNTIL END-OF-MESS
		//                  OR END-OF-ASSEM
		//                  OR PLACEHOLDER-FOUND.
		while (!(ws.getWsWorkFields().getEndOfMessSw().isEndOfMess() || ws.getWsWorkFields().getEndOfAssemSw().isEndOfAssem()
				|| ws.getWsWorkFields().getPlaceholderFoundSw().isFound())) {
			findPlaceholder();
		}
		//* SUBSTITUTE PLACE HOLDER
		// COB_CODE: IF PLACEHOLDER-FOUND
		//               PERFORM 0500-SUBSTITUTE-PLACEHOLDER
		//           END-IF.
		if (ws.getWsWorkFields().getPlaceholderFoundSw().isFound()) {
			// COB_CODE: PERFORM 0500-SUBSTITUTE-PLACEHOLDER
			substitutePlaceholder1();
		}
	}

	/**Original name: 0400-FIND-PLACEHOLDER_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   COPY MESSAGE ACROSS FROM INPUT MESSAGE BUFFER TO ASSEMBLY     *
	 *   BUFFER UNTIL PLACEHOLDER FOUND OR END OF INPUT MESSAGE.       *
	 *                                                                 *
	 * *****************************************************************
	 * * CHECK FOR END OF STRING
	 *     IF MESS-IND > 100</pre>*/
	private void findPlaceholder() {
		// COB_CODE: IF MESS-IND > 500
		//               GO TO 0400-FIND-PLACEHOLDER-X
		//           END-IF.
		if (ws.getMessInd() > 500) {
			// COB_CODE: SET END-OF-MESS TO TRUE
			ws.getWsWorkFields().getEndOfMessSw().setEndOfMess();
			// COB_CODE: GO TO 0400-FIND-PLACEHOLDER-X
			return;
		}
		//* CHECK FOR PLACEHOLDER
		//    IF    MESS-IND < 99
		//          '1' OR '2' OR '3' OR '4' OR '5' OR '6')
		// COB_CODE:      IF    MESS-IND < 499
		//                  AND LIO-MESS-STRING-X(MESS-IND)      = '<'
		//                  AND (LIO-MESS-STRING-X(MESS-IND + 1) =
		//           *          '1' OR '2' OR '3' OR '4' OR '5' OR '6')
		//                      '1' OR '2' OR '3' OR '4' OR '5' OR '6' OR '7')
		//                  AND LIO-MESS-STRING-X(MESS-IND + 2)  = '>'
		//                    GO TO 0400-FIND-PLACEHOLDER-X
		//                END-IF.
		if (ws.getMessInd() < 499 && lioMess.getLioMessStringX(ws.getMessInd()) == '<'
				&& (lioMess.getLioMessStringX(ws.getMessInd() + 1) == '1' || lioMess.getLioMessStringX(ws.getMessInd() + 1) == '2'
						|| lioMess.getLioMessStringX(ws.getMessInd() + 1) == '3' || lioMess.getLioMessStringX(ws.getMessInd() + 1) == '4'
						|| lioMess.getLioMessStringX(ws.getMessInd() + 1) == '5' || lioMess.getLioMessStringX(ws.getMessInd() + 1) == '6'
						|| lioMess.getLioMessStringX(ws.getMessInd() + 1) == '7')
				&& lioMess.getLioMessStringX(ws.getMessInd() + 2) == '>') {
			// COB_CODE: SET PLACEHOLDER-FOUND TO TRUE
			ws.getWsWorkFields().getPlaceholderFoundSw().setFound();
			// COB_CODE: GO TO 0400-FIND-PLACEHOLDER-X
			return;
		}
		//* ATTEMPT TO COPY CHARACTER ACROSS
		// COB_CODE: SET ASSEM-IND UP BY 1.
		ws.setAssemInd(Trunc.toInt(ws.getAssemInd() + 1, 9));
		//    IF ASSEM-IND > 100
		// COB_CODE: IF ASSEM-IND > 500
		//               GO TO 0400-FIND-PLACEHOLDER-X
		//           END-IF.
		if (ws.getAssemInd() > 500) {
			// COB_CODE: SET END-OF-ASSEM TO TRUE
			ws.getWsWorkFields().getEndOfAssemSw().setEndOfAssem();
			// COB_CODE: GO TO 0400-FIND-PLACEHOLDER-X
			return;
		}
		// COB_CODE: MOVE LIO-MESS-STRING-X(MESS-IND)
		//             TO WS-ASSEM-STRING-X(ASSEM-IND).
		ws.getWsWorkFields().setAssemStringX(ws.getAssemInd(), lioMess.getLioMessStringX(ws.getMessInd()));
		//* INCREMENT STRING IND
		// COB_CODE: SET MESS-IND UP BY 1.
		ws.setMessInd(Trunc.toInt(ws.getMessInd() + 1, 9));
	}

	/**Original name: 0500-SUBSTITUTE-PLACEHOLDER_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   ATTEMPT TO SUBSTITUTE PLACEHOLDER                             *
	 *                                                                 *
	 * *****************************************************************
	 * * DETERMINE LENGTH OF STRING IN PLACEHOLDER</pre>*/
	private void substitutePlaceholder1() {
		Halrlen1 halrlen1 = null;
		GenericParam wsHalrlen1InLen = null;
		GenericParam wsHalrlen1OutLen = null;
		// COB_CODE:      EVALUATE LIO-MESS-STRING-X(MESS-IND + 1)
		//                   WHEN '1'
		//                         TO WS-HALRLEN1-IN-LEN
		//                   WHEN '2'
		//                         TO WS-HALRLEN1-IN-LEN
		//                   WHEN '3'
		//                         TO WS-HALRLEN1-IN-LEN
		//                   WHEN '4'
		//                         TO WS-HALRLEN1-IN-LEN
		//                   WHEN '5'
		//                         TO WS-HALRLEN1-IN-LEN
		//                   WHEN '6'
		//           *           IF LI-ERR-CONTEXT-VALUE = SPACES
		//           *               MOVE 'spaces'             TO WS-PLACEHOLDER
		//           *               MOVE 6
		//           *                 TO WS-HALRLEN1-IN-LEN
		//           *           ELSE
		//                             TO WS-HALRLEN1-IN-LEN
		//           *           END-IF
		//                   WHEN '7'
		//                             TO WS-HALRLEN1-IN-LEN
		//                END-EVALUATE.
		switch (lioMess.getLioMessStringX(ws.getMessInd() + 1)) {

		case '1':// COB_CODE: MOVE LI-ERR-COL-NAME1     TO WS-PLACEHOLDER
			ws.getWsWorkFields().getPlaceholder().setPlaceholderFormatted(liErrColName1.getLiErrColName1Formatted());
			// COB_CODE: MOVE LENGTH OF LI-ERR-COL-NAME1
			//             TO WS-HALRLEN1-IN-LEN
			ws.getWsWorkFields().setHalrlen1InLen(((short) LiErrColName1.Len.LI_ERR_COL_NAME1));
			break;

		case '2':// COB_CODE: MOVE LI-ERR-COL-VALUE1    TO WS-PLACEHOLDER
			ws.getWsWorkFields().getPlaceholder().setPlaceholderFormatted(liErrColValue1.getLiErrColValue1Formatted());
			// COB_CODE: MOVE LENGTH OF LI-ERR-COL-VALUE1
			//             TO WS-HALRLEN1-IN-LEN
			ws.getWsWorkFields().setHalrlen1InLen(((short) LiErrColValue1.Len.LI_ERR_COL_VALUE1));
			break;

		case '3':// COB_CODE: MOVE LI-ERR-COL-NAME2     TO WS-PLACEHOLDER
			ws.getWsWorkFields().getPlaceholder().setPlaceholderFormatted(liErrColName2.getLiErrColName1Formatted());
			// COB_CODE: MOVE LENGTH OF LI-ERR-COL-NAME2
			//             TO WS-HALRLEN1-IN-LEN
			ws.getWsWorkFields().setHalrlen1InLen(((short) LiErrColName1.Len.LI_ERR_COL_NAME1));
			break;

		case '4':// COB_CODE: MOVE LI-ERR-COL-VALUE2    TO WS-PLACEHOLDER
			ws.getWsWorkFields().getPlaceholder().setPlaceholderFormatted(liErrColValue2.getLiErrColValue1Formatted());
			// COB_CODE: MOVE LENGTH OF LI-ERR-COL-VALUE2
			//             TO WS-HALRLEN1-IN-LEN
			ws.getWsWorkFields().setHalrlen1InLen(((short) LiErrColValue1.Len.LI_ERR_COL_VALUE1));
			break;

		case '5':// COB_CODE: MOVE LI-ERR-CONTEXT-TEXT  TO WS-PLACEHOLDER
			ws.getWsWorkFields().getPlaceholder().setPlaceholderFormatted(liErrContextText.getLiErrContextTextFormatted());
			// COB_CODE: MOVE LENGTH OF LI-ERR-CONTEXT-TEXT
			//             TO WS-HALRLEN1-IN-LEN
			ws.getWsWorkFields().setHalrlen1InLen(((short) LiErrContextText.Len.LI_ERR_CONTEXT_TEXT));
			break;

		case '6'://           IF LI-ERR-CONTEXT-VALUE = SPACES
			//               MOVE 'spaces'             TO WS-PLACEHOLDER
			//               MOVE 6
			//                 TO WS-HALRLEN1-IN-LEN
			//           ELSE
			// COB_CODE: MOVE LI-ERR-CONTEXT-VALUE TO WS-PLACEHOLDER
			ws.getWsWorkFields().getPlaceholder().setPlaceholderFormatted(liErrContextValue.getLiErrContextTextFormatted());
			// COB_CODE: MOVE LENGTH OF LI-ERR-CONTEXT-VALUE
			//             TO WS-HALRLEN1-IN-LEN
			ws.getWsWorkFields().setHalrlen1InLen(((short) LiErrContextText.Len.LI_ERR_CONTEXT_TEXT));
			//           END-IF
			break;

		case '7':// COB_CODE: MOVE LI-ERR-ALLTXT-TEXT TO WS-PLACEHOLDER
			ws.getWsWorkFields().getPlaceholder().setPlaceholderFormatted(liErrAlltxtText.getLiErrAlltxtTextFormatted());
			// COB_CODE: MOVE LENGTH OF LI-ERR-ALLTXT-TEXT
			//             TO WS-HALRLEN1-IN-LEN
			ws.getWsWorkFields().setHalrlen1InLen(((short) LiErrAlltxtText.Len.LI_ERR_ALLTXT_TEXT));
			break;

		default:
			break;
		}
		// COB_CODE: CALL 'HALRLEN1' USING WS-PLACEHOLDER
		//                                 WS-HALRLEN1-IN-LEN
		//                                 WS-HALRLEN1-OUT-LEN.
		halrlen1 = Halrlen1.getInstance();
		wsHalrlen1InLen = new GenericParam(MarshalByteExt.binShortToBuffer(ws.getWsWorkFields().getHalrlen1InLen()));
		wsHalrlen1OutLen = new GenericParam(MarshalByteExt.binShortToBuffer(ws.getWsWorkFields().getHalrlen1OutLen()));
		halrlen1.run(ws.getWsWorkFields().getPlaceholder(), wsHalrlen1InLen, wsHalrlen1OutLen);
		ws.getWsWorkFields().setHalrlen1InLenFromBuffer(wsHalrlen1InLen.getByteData());
		ws.getWsWorkFields().setHalrlen1OutLenFromBuffer(wsHalrlen1OutLen.getByteData());
		//* SUBSTITUTE PLACEHOLDER IN
		// COB_CODE: SET PLAC-IND TO 1.
		ws.setPlacInd(1);
		// COB_CODE: SET NOT-END-OF-PLACEHOLDER TO TRUE.
		ws.getWsWorkFields().getEndOfPlaceholderSw().setNotEndOfPlaceholder();
		// COB_CODE: PERFORM 0600-SUBSTITUTE-PLACE-CHAR
		//             UNTIL END-OF-ASSEM
		//                OR END-OF-PLACEHOLDER.
		while (!(ws.getWsWorkFields().getEndOfAssemSw().isEndOfAssem() || ws.getWsWorkFields().getEndOfPlaceholderSw().isEndOfPlaceholder())) {
			substitutePlaceChar();
		}
		//* JUMP OVER PLACEHOLDER IN INPUT STRING
		// COB_CODE: SET MESS-IND UP BY 3.
		ws.setMessInd(Trunc.toInt(ws.getMessInd() + 3, 9));
	}

	/**Original name: 0600-SUBSTITUTE-PLACE-CHAR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   SUBSTITUTE A PLACEHOLDER CHARACTER                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void substitutePlaceChar() {
		// COB_CODE: IF PLAC-IND > WS-HALRLEN1-OUT-LEN
		//               GO TO 0600-SUBSTITUTE-PLACE-CHAR-X
		//           END-IF.
		if (ws.getPlacInd() > ws.getWsWorkFields().getHalrlen1OutLen()) {
			// COB_CODE: SET END-OF-PLACEHOLDER TO TRUE
			ws.getWsWorkFields().getEndOfPlaceholderSw().setEndOfPlaceholder();
			// COB_CODE: GO TO 0600-SUBSTITUTE-PLACE-CHAR-X
			return;
		}
		// COB_CODE: SET ASSEM-IND UP BY 1.
		ws.setAssemInd(Trunc.toInt(ws.getAssemInd() + 1, 9));
		//    IF ASSEM-IND > 100
		// COB_CODE: IF ASSEM-IND > 500
		//               GO TO 0600-SUBSTITUTE-PLACE-CHAR-X
		//           END-IF.
		if (ws.getAssemInd() > 500) {
			// COB_CODE: SET END-OF-ASSEM TO TRUE
			ws.getWsWorkFields().getEndOfAssemSw().setEndOfAssem();
			// COB_CODE: GO TO 0600-SUBSTITUTE-PLACE-CHAR-X
			return;
		}
		// COB_CODE: MOVE WS-PLACEHOLDER-X(PLAC-IND)
		//             TO WS-ASSEM-STRING-X(ASSEM-IND).
		ws.getWsWorkFields().setAssemStringX(ws.getAssemInd(), ws.getWsWorkFields().getPlaceholder().getWsPlaceholderX(ws.getPlacInd()));
		// COB_CODE: SET PLAC-IND UP BY 1.
		ws.setPlacInd(Trunc.toInt(ws.getPlacInd() + 1, 9));
	}

	/**Original name: 0900-FINALIZATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  PERFORM FINAL   PROCESSING.                                    *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void finalization() {
		// COB_CODE: MOVE WS-ASSEM-STRING TO LIO-MESS.
		lioMess.setLioMessBytes(ws.getWsWorkFields().getAssemStringBytes());
	}
}
