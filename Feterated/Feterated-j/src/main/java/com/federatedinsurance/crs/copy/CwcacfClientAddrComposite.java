/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CWCACF-CLIENT-ADDR-COMPOSITE<br>
 * Variable: CWCACF-CLIENT-ADDR-COMPOSITE from copybook CAWLFCAC<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class CwcacfClientAddrComposite {

	//==== PROPERTIES ====
	//Original name: CWCACF-CLIENT-ADDR-COMP-CHKSUM
	private String clientAddrCompChksum = DefaultValues.stringVal(Len.CLIENT_ADDR_COMP_CHKSUM);
	//Original name: CWCACF-ADR-ID-KCRE
	private String adrIdKcre = DefaultValues.stringVal(Len.ADR_ID_KCRE);
	//Original name: CWCACF-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CWCACF-ADR-SEQ-NBR-KCRE
	private String adrSeqNbrKcre = DefaultValues.stringVal(Len.ADR_SEQ_NBR_KCRE);
	//Original name: CWCACF-TCH-OBJECT-KEY-KCRE
	private String tchObjectKeyKcre = DefaultValues.stringVal(Len.TCH_OBJECT_KEY_KCRE);
	//Original name: CWCACF-ADR-ID-CI
	private char adrIdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-ADR-ID
	private String adrId = DefaultValues.stringVal(Len.ADR_ID);
	//Original name: CWCACF-CLT-ADR-RELATION-KEY
	private Cw01fBusinessClientKey cltAdrRelationKey = new Cw01fBusinessClientKey();
	//Original name: CWCACF-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CWCACF-CLIENT-ADDR-COMP-DATA
	private CwcacfClientAddrCompData clientAddrCompData = new CwcacfClientAddrCompData();

	//==== METHODS ====
	public void setCwcacfClientAddrCompositeFormatted(String data) {
		byte[] buffer = new byte[Len.CWCACF_CLIENT_ADDR_COMPOSITE];
		MarshalByte.writeString(buffer, 1, data, Len.CWCACF_CLIENT_ADDR_COMPOSITE);
		setCwcacfClientAddrCompositeBytes(buffer, 1);
	}

	public String getCwcacfClientAddrCompositeFormatted() {
		return MarshalByteExt.bufferToStr(getCwcacfClientAddrCompositeBytes());
	}

	public byte[] getCwcacfClientAddrCompositeBytes() {
		byte[] buffer = new byte[Len.CWCACF_CLIENT_ADDR_COMPOSITE];
		return getCwcacfClientAddrCompositeBytes(buffer, 1);
	}

	public void setCwcacfClientAddrCompositeBytes(byte[] buffer, int offset) {
		int position = offset;
		setClientAddrCompFixedBytes(buffer, position);
		position += Len.CLIENT_ADDR_COMP_FIXED;
		setClientAddressKeyBytes(buffer, position);
		position += Len.CLIENT_ADDRESS_KEY;
		cltAdrRelationKey.setBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		setClientAddrCompDatesBytes(buffer, position);
		position += Len.CLIENT_ADDR_COMP_DATES;
		clientAddrCompData.setClientAddrCompDataBytes(buffer, position);
	}

	public byte[] getCwcacfClientAddrCompositeBytes(byte[] buffer, int offset) {
		int position = offset;
		getClientAddrCompFixedBytes(buffer, position);
		position += Len.CLIENT_ADDR_COMP_FIXED;
		getClientAddressKeyBytes(buffer, position);
		position += Len.CLIENT_ADDRESS_KEY;
		cltAdrRelationKey.getBusinessClientKeyBytes(buffer, position);
		position += Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY;
		getClientAddrCompDatesBytes(buffer, position);
		position += Len.CLIENT_ADDR_COMP_DATES;
		clientAddrCompData.getClientAddrCompDataBytes(buffer, position);
		return buffer;
	}

	public void setClientAddrCompFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		clientAddrCompChksum = MarshalByte.readFixedString(buffer, position, Len.CLIENT_ADDR_COMP_CHKSUM);
		position += Len.CLIENT_ADDR_COMP_CHKSUM;
		adrIdKcre = MarshalByte.readString(buffer, position, Len.ADR_ID_KCRE);
		position += Len.ADR_ID_KCRE;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		adrSeqNbrKcre = MarshalByte.readString(buffer, position, Len.ADR_SEQ_NBR_KCRE);
		position += Len.ADR_SEQ_NBR_KCRE;
		tchObjectKeyKcre = MarshalByte.readString(buffer, position, Len.TCH_OBJECT_KEY_KCRE);
	}

	public byte[] getClientAddrCompFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientAddrCompChksum, Len.CLIENT_ADDR_COMP_CHKSUM);
		position += Len.CLIENT_ADDR_COMP_CHKSUM;
		MarshalByte.writeString(buffer, position, adrIdKcre, Len.ADR_ID_KCRE);
		position += Len.ADR_ID_KCRE;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		MarshalByte.writeString(buffer, position, adrSeqNbrKcre, Len.ADR_SEQ_NBR_KCRE);
		position += Len.ADR_SEQ_NBR_KCRE;
		MarshalByte.writeString(buffer, position, tchObjectKeyKcre, Len.TCH_OBJECT_KEY_KCRE);
		return buffer;
	}

	public void setClientAddrCompChksumFormatted(String clientAddrCompChksum) {
		this.clientAddrCompChksum = Trunc.toUnsignedNumeric(clientAddrCompChksum, Len.CLIENT_ADDR_COMP_CHKSUM);
	}

	public int getClientAddrCompChksum() {
		return NumericDisplay.asInt(this.clientAddrCompChksum);
	}

	public void setAdrIdKcre(String adrIdKcre) {
		this.adrIdKcre = Functions.subString(adrIdKcre, Len.ADR_ID_KCRE);
	}

	public String getAdrIdKcre() {
		return this.adrIdKcre;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setAdrSeqNbrKcre(String adrSeqNbrKcre) {
		this.adrSeqNbrKcre = Functions.subString(adrSeqNbrKcre, Len.ADR_SEQ_NBR_KCRE);
	}

	public String getAdrSeqNbrKcre() {
		return this.adrSeqNbrKcre;
	}

	public void setTchObjectKeyKcre(String tchObjectKeyKcre) {
		this.tchObjectKeyKcre = Functions.subString(tchObjectKeyKcre, Len.TCH_OBJECT_KEY_KCRE);
	}

	public String getTchObjectKeyKcre() {
		return this.tchObjectKeyKcre;
	}

	public void setClientAddressKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		adrIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		adrId = MarshalByte.readString(buffer, position, Len.ADR_ID);
	}

	public byte[] getClientAddressKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, adrIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, adrId, Len.ADR_ID);
		return buffer;
	}

	public void setAdrIdCi(char adrIdCi) {
		this.adrIdCi = adrIdCi;
	}

	public char getAdrIdCi() {
		return this.adrIdCi;
	}

	public void setAdrId(String adrId) {
		this.adrId = Functions.subString(adrId, Len.ADR_ID);
	}

	public String getAdrId() {
		return this.adrId;
	}

	public void setClientAddrCompDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getClientAddrCompDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public CwcacfClientAddrCompData getClientAddrCompData() {
		return clientAddrCompData;
	}

	public Cw01fBusinessClientKey getCltAdrRelationKey() {
		return cltAdrRelationKey;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ADDR_COMP_CHKSUM = 9;
		public static final int ADR_ID_KCRE = 32;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int ADR_SEQ_NBR_KCRE = 32;
		public static final int TCH_OBJECT_KEY_KCRE = 32;
		public static final int ADR_ID = 20;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLIENT_ADDR_COMP_FIXED = CLIENT_ADDR_COMP_CHKSUM + ADR_ID_KCRE + CLIENT_ID_KCRE + ADR_SEQ_NBR_KCRE
				+ TCH_OBJECT_KEY_KCRE;
		public static final int ADR_ID_CI = 1;
		public static final int CLIENT_ADDRESS_KEY = ADR_ID_CI + ADR_ID;
		public static final int CLIENT_ADDR_COMP_DATES = TRANS_PROCESS_DT;
		public static final int CWCACF_CLIENT_ADDR_COMPOSITE = CLIENT_ADDR_COMP_FIXED + CLIENT_ADDRESS_KEY
				+ Cw01fBusinessClientKey.Len.BUSINESS_CLIENT_KEY + CLIENT_ADDR_COMP_DATES + CwcacfClientAddrCompData.Len.CLIENT_ADDR_COMP_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
