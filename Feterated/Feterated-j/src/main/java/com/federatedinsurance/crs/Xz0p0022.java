/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.commons.data.dao.Sysdummy1Dao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.WsProxyProgramArea;
import com.federatedinsurance.crs.ws.WsXz0y0022Row;
import com.federatedinsurance.crs.ws.Xz0p0022Data;
import com.federatedinsurance.crs.ws.ptr.LServiceContractArea;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0006;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x90h0;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x90m0;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x90s0;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0P0022<br>
 * <pre>AUTHOR.       JERAMY LAWSON.
 * DATE-WRITTEN. 24 JUL 2017.
 * *****************************************************************
 *                                                                 *
 *   PROGRAM TITLE - SET TERMINATION FLAG                          *
 *                   XREF OBJ NM : XZ_SET_TMN_FLG_BPO              *
 *                   UOW         : XZ_SET_TMN_FLG                  *
 *                   OPERATIONS  : setTerminationFlag              *
 *                                                                 *
 *   PURPOSE -  THIS BPO WILL CALL THE SERVICES TO SET THE         *
 *              TERMINATION FLAG FOR THE POLICIES PASSED IN BY     *
 *              THE CALLING USER/APPLICATION.                      *
 *                                                                 *
 *   PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS   *
 *                         LINKED TO BY THE FRAMEWORK DRIVER.      *
 *                                                                 *
 *   DATA ACCESS METHODS - UMT STORAGE RECORDS                     *
 *                         DB2 DATABASE                            *
 *                                                                 *
 * *****************************************************************
 * ****************************************************************
 * * NOTE: THIS LOG FOR INFRASTRUCTURE USE ONLY FOR TEMPLATE     **
 * *       VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     **
 * *       APPLICATION CODING.                                   **
 * *     T E M P L A T E   M A I N T E N A N C E   L O G         **
 * * CASE#     DATE       PROG       DESCRIPTION                 **
 * * --------  ---------  --------   ----------------------------**
 * * TS129     06/13/2006 E404LJL    TEMPLATE CREATED            **
 * * YJ249     04/27/2007 E404NEM    STDS CHGS                   **
 * * TS130     12/28/2007 E404JSP    Changed a few bugs          **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * -------  ----------  --------   ----------------------------**
 * *   15839  07/24/2017  E404JAL    NEW                         **
 * ****************************************************************</pre>*/
public class Xz0p0022 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>*****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private Sysdummy1Dao sysdummy1Dao = new Sysdummy1Dao(dbAccessStatus);
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0p0022Data ws = new Xz0p0022Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private Dfhcommarea dfhcommarea;
	/**Original name: WS-XZ0T0006-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE ACCOUNT NOTIFICATION SERVICE</pre>*/
	private LServiceContractAreaXz0x0006 wsXz0t0006Row = new LServiceContractAreaXz0x0006(null);
	/**Original name: WS-XZ0T90S0-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE ACY/PND/CNC/TMN POLICY SERVICE</pre>*/
	private LServiceContractAreaXz0x90s0 wsXz0t90s0Row = new LServiceContractAreaXz0x90s0(null);
	/**Original name: WS-XZ0T90H0-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE PREPARE INSURED POLICY SERVICE</pre>*/
	private LServiceContractAreaXz0x90h0 wsXz0t90h0Row = new LServiceContractAreaXz0x90h0(null);
	/**Original name: WS-XZ0T90M0-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE UPDATE NOTIFICATION STATUS SERVICE</pre>*/
	private LServiceContractAreaXz0x90m0 wsXz0t90m0Row = new LServiceContractAreaXz0x90m0(null);
	/**Original name: WS-MU0T0004-ROW<br>
	 * <pre> CONTRACT COPYBOOK FOR THE INSURED DETAIL SERVICE</pre>*/
	private LServiceContractArea wsMu0t0004Row = new LServiceContractArea(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, Dfhcommarea dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0p0022 getInstance() {
		return (Programs.getInstance(Xz0p0022.class));
	}

	/**Original name: 1000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   MAIN PROCESSING CONTROL                                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-SET-TMN-FLG.
		setTmnFlg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *  INITIALIZATION CONTROL                                        *
	 *                                                                *
	 * ****************************************************************
	 * * INITIALIZE ERROR/WARNING STORAGE</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		//* VALIDATE UBOC IN COMMAREA
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2100-READ-REQ-UMT-ROW.
		readReqUmtRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-GET-CURRENT-DATE-TIME.
		getCurrentDateTime();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-READ-REQ-UMT-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR THE BPO INPUT ROW                     *
	 * *****************************************************************</pre>*/
	private void readReqUmtRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: SET HALRURQA-READ-FUNC      TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: MOVE CF-BUS-OBJ-XZ-SET-TMN-FLG-BPO
		//                                       TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getConstantFields().getBusObjXzSetTmnFlgBpo());
		// COB_CODE: MOVE +1                     TO HALRURQA-REC-SEQ.
		ws.getWsHalrurqaLinkage().setRecSeq(1);
		// COB_CODE: INITIALIZE                  WS-XZ0Y0022-ROW.
		initWsXz0y0022Row();
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-XZ0Y0022-ROW.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsXz0y0022Row());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: IF HALRURQA-REC-NOT-FOUND
		//               GO TO 2100-EXIT
		//           END-IF.
		if (ws.getWsHalrurqaLinkage().getRecFoundSw().isHalrurqaRecNotFound()) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUEST-MSG-MISSING
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequestMsgMissing();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '2100-READ-REQ-UMT-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-READ-REQ-UMT-ROW");
			// COB_CODE: MOVE 'NO RECORD FOUND ON REQ MSG STORE'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NO RECORD FOUND ON REQ MSG STORE");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HALRURQA-BUS-OBJ-NM='
			//                  HALRURQA-BUS-OBJ-NM
			//                  '; HALRURQA-REC-SEQ='
			//                  HALRURQA-REC-SEQ
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HALRURQA-BUS-OBJ-NM=").append(ws.getWsHalrurqaLinkage().getBusObjNmFormatted())
							.append("; HALRURQA-REC-SEQ=").append(ws.getWsHalrurqaLinkage().getRecSeqFormatted()).toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
	}

	/**Original name: 2200-GET-CURRENT-DATE-TIME_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  GET THE CURRENT DATE TIME                                      *
	 * *****************************************************************</pre>*/
	private void getCurrentDateTime() {
		// COB_CODE: EXEC SQL
		//               SELECT CURRENT TIMESTAMP
		//                 INTO :WS-CURRENT-TIMESTAMP
		//                 FROM SYSIBM.SYSDUMMY1
		//           END-EXEC.
		ws.getWorkingStorageArea().getWsCurrentTimestampX()
				.setWsCurrentTimestamp(sysdummy1Dao.selectRec2(ws.getWorkingStorageArea().getWsCurrentTimestampX().getWsCurrentTimestamp()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   GO TO 2200-EXIT
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: GO TO 2200-EXIT
			return;

		default:// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'SYSIBM DATE'  TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("SYSIBM DATE");
			// COB_CODE: MOVE '2200-GET-CURRENT-DATE-TIME'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2200-GET-CURRENT-DATE-TIME");
			// COB_CODE: MOVE SQLCODE        TO EFAL-DB2-ERR-SQLCODE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: IF SQLCODE < 0
			//                MOVE '-'       TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           ELSE
			//                MOVE '+'       TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           END-IF
			if (sqlca.getSqlcode() < 0) {
				// COB_CODE: MOVE '-'       TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'       TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			}
			// COB_CODE: MOVE 'DB2 ERROR READING SYSIBM TABLE'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DB2 ERROR READING SYSIBM TABLE");
			// COB_CODE: MOVE SQLERRMC       TO EFAL-DB2-ERR-SQLERRMC
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE SPACES         TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;
		}
	}

	/**Original name: 3000-SET-TMN-FLG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SET THE TERMINATION FLAG FOR THE ACCOUNT AND POLICIES RECEIVED.*
	 * *****************************************************************
	 * *   CREATE THE ACT_NOT ROW FOR THE ACCOUNT.</pre>*/
	private void setTmnFlg() {
		// COB_CODE: PERFORM 3100-START-NOTIFICATION
		startNotification();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//*   CREATE THE ACT_NOT_POL ROWS FOR THE POLICIES PASSED.
		// COB_CODE: PERFORM 3200-CRN-POL-NOT.
		rng3200CrnPolNot();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		//*   UPDATE STATUS TO COMPLETE THE SETTING OF THE TERMINATION FLAG
		// COB_CODE: PERFORM 3300-CPL-TMN-FLG-SET.
		cplTmnFlgSet();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3100-START-NOTIFICATION_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  START THE NOTIFICATION GETTING THE CLT DETAIL AND ADD ACT_NOT  *
	 * *****************************************************************</pre>*/
	private void startNotification() {
		// COB_CODE: PERFORM 3110-GET-INSURED-DETAIL.
		getInsuredDetail();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: PERFORM 3120-CRN-ACT-NOT-ROW.
		crnActNotRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
	}

	/**Original name: 3110-GET-INSURED-DETAIL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CALL THE SERVICE TO GET THE INSURED DETAIL                     *
	 * *****************************************************************</pre>*/
	private void getInsuredDetail() {
		// COB_CODE: PERFORM 3111-ALC-MEM-INSURED-DTL-SVC.
		alcMemInsuredDtlSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3110-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3110-EXIT
			return;
		}
		// COB_CODE: PERFORM 3112-CALL-INSURED-DTL-SVC.
		callInsuredDtlSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3110-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3110-EXIT
			return;
		}
	}

	/**Original name: 3111-ALC-MEM-INSURED-DTL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE GET INSURED DETAIL SERVICE         *
	 * *****************************************************************</pre>*/
	private void alcMemInsuredDtlSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-MU0T0004-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractArea.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3111-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-INSURED-DETAIL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc());
			// COB_CODE: MOVE '3111-ALC-MEM-INSURED-DTL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3111-ALC-MEM-INSURED-DTL-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "FAILED MODULE EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getWsEibrespCdAsString(), ".  FAILED MODULE EIBRESP2 CODE IS ",
					ws.getWorkingStorageArea().getWsEibresp2CdAsString(), ".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3111-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-MU0T0004-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsMu0t0004Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractArea.class)));
		// COB_CODE: INITIALIZE WS-MU0T0004-ROW.
		initWsMu0t0004Row();
	}

	/**Original name: 3112-CALL-INSURED-DTL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS AND CALL THE INSURED DETAIL SERVICE.            *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void callInsuredDtlSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-GET-INSURED-DETAIL   TO TRUE.
		ws.getWorkingStorageArea().getWsOperationsCalled().setGetInsuredDetail();
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-OPERATIONS-CALLED   TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getWsOperationsCalled());
		// COB_CODE: MOVE XZY022-USERID          TO MUT04I-USR-ID.
		wsMu0t0004Row.setMut04iUsrId(ws.getWsXz0y0022Row().getUserid());
		// COB_CODE: MOVE XZY022-CSR-ACT-NBR     TO MUT04I-ACT-NBR.
		wsMu0t0004Row.setMut04iActNbr(ws.getWsXz0y0022Row().getCsrActNbr());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-GET-INSURED-DETAIL-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P0022", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc(), new Mu0x0004());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3112-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-INSURED-DETAIL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc());
			// COB_CODE: MOVE '3112-CALL-INSURED-DTL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3112-CALL-INSURED-DTL-SVC");
			// COB_CODE: MOVE 'INSURED DETAIL SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INSURED DETAIL SERVICE CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "FAILED MODULE EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getWsEibrespCdAsString(), ".  FAILED MODULE EIBRESP2 CODE IS ",
					ws.getWorkingStorageArea().getWsEibresp2CdAsString(), ".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3112-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-GET-INSURED-DETAIL-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc());
		// COB_CODE: MOVE '3112-GET-INSURED-DTL-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3112-GET-INSURED-DTL-SVC");
		// COB_CODE: MOVE 'GET INSURED DTL SVC'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("GET INSURED DTL SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3120-CRN-ACT-NOT-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ADD THE ROW TO ACT_NOT TABLE                                   *
	 * *****************************************************************</pre>*/
	private void crnActNotRow() {
		// COB_CODE: PERFORM 3121-ALC-MEM-ACT-NOT-SVC.
		alcMemActNotSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3120-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3120-EXIT
			return;
		}
		// COB_CODE: PERFORM 3122-SET-INPUT-ACT-NOT.
		setInputActNot();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3120-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3120-EXIT
			return;
		}
		// COB_CODE: PERFORM 3123-CALL-ADD-ACT-NOT-SVC.
		callAddActNotSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3120-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3120-EXIT
			return;
		}
	}

	/**Original name: 3121-ALC-MEM-ACT-NOT-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE ADD ACT_NOT SERVICE                *
	 * *****************************************************************</pre>*/
	private void alcMemActNotSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T0006-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x0006.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3121-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotSvc());
			// COB_CODE: MOVE '3121-ALC-MEM-ACT-NOT-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3121-ALC-MEM-ACT-NOT-SVC");
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "FAILED MODULE EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getWsEibrespCdAsString(), ".  FAILED MODULE EIBRESP2 CODE IS ",
					ws.getWorkingStorageArea().getWsEibresp2CdAsString(), ".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3121-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T0006-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t0006Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x0006.class)));
		// COB_CODE: INITIALIZE WS-XZ0T0006-ROW.
		initWsXz0t0006Row();
	}

	/**Original name: 3122-SET-INPUT-ACT-NOT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS NEEDED FOR THE ADD ACT_NOT SERVICE              *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setInputActNot() {
		// COB_CODE: SET WS-ADD-ACT-NOT          TO TRUE.
		ws.getWorkingStorageArea().getWsOperationsCalled().setAddActNot();
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-OPERATIONS-CALLED   TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getWsOperationsCalled());
		// COB_CODE: MOVE XZY022-USERID          TO XZT06I-USERID.
		wsXz0t0006Row.setXzt06iUserid(ws.getWsXz0y0022Row().getUserid());
		// COB_CODE: MOVE XZY022-CSR-ACT-NBR     TO XZT06I-CSR-ACT-NBR.
		wsXz0t0006Row.setXzt06iCsrActNbr(ws.getWsXz0y0022Row().getCsrActNbr());
		// COB_CODE: MOVE CF-TMN-FLG             TO XZT06I-ACT-NOT-TYP-CD.
		wsXz0t0006Row.setXzt06iActNotTypCd(ws.getConstantFields().getTmnFlg());
		// COB_CODE: MOVE WS-CURRENT-DATE        TO XZT06I-NOT-DT.
		wsXz0t0006Row.setXzt06iNotDt(ws.getWorkingStorageArea().getWsCurrentTimestampX().getDateFld());
		// COB_CODE: MOVE MUT04O-TK-CLT-ID       TO XZT06I-TK-ACT-OWN-CLT-ID.
		wsXz0t0006Row.setXzt06iTkActOwnCltId(wsMu0t0004Row.getMut04oTkCltId());
		// COB_CODE: MOVE MUT04O-BSM-ADR-ID      TO XZT06I-TK-ACT-OWN-ADR-ID.
		wsXz0t0006Row.setXzt06iTkActOwnAdrId(wsMu0t0004Row.getMut04oBsmAdrId());
		// COB_CODE: MOVE WS-CURRENT-TIMESTAMP   TO XZT06I-TK-STA-MDF-TS.
		wsXz0t0006Row.setXzt06iTkStaMdfTs(ws.getWorkingStorageArea().getWsCurrentTimestampX().getWsCurrentTimestamp());
		// COB_CODE: MOVE CF-ACT-NOT-STATUS-STARTED
		//                                       TO XZT06I-TK-ACT-NOT-STA-CD.
		wsXz0t0006Row.setXzt06iTkActNotStaCd(ws.getConstantFields().getActNotStatusStarted());
		// COB_CODE: MOVE MUT04O-SEG-CD          TO XZT06I-TK-SEG-CD.
		wsXz0t0006Row.setXzt06iTkSegCd(wsMu0t0004Row.getMut04oSegCd());
		// COB_CODE: MOVE CF-NO-FEE              TO XZT06I-TOT-FEE-AMT.
		wsXz0t0006Row.setXzt06iTotFeeAmt(Trunc.toDecimal(ws.getConstantFields().getNoFee(), 10, 2));
		// COB_CODE: MOVE MUT04O-BSM-ST-ABB      TO XZT06I-ST-ABB.
		wsXz0t0006Row.setXzt06iStAbb(wsMu0t0004Row.getMut04oBsmStAbb());
		// COB_CODE:      IF MUT04O-PRS-LIN-ACT-NBR NOT = SPACES
		//                                            TO XZT06I-TK-ACT-TYP-CD
		//                ELSE
		//           **  THE TERR NBR NEEDS TO BE FORMATTED LIKE THIS X-XXX
		//                                            TO XZT06I-TK-ACT-TYP-CD
		//                END-IF.
		if (!Characters.EQ_SPACE.test(wsMu0t0004Row.getMut04oPrsLinActNbr())) {
			// COB_CODE: SET SW-PERSONAL-LINES-ACT
			//                                   TO TRUE
			ws.setSwPersonalLinesActFlag(true);
			// COB_CODE: MOVE CF-PL-PRODUCER-NBR TO XZT06I-PDC-NBR
			wsXz0t0006Row.setXzt06iPdcNbr(ws.getConstantFields().getPlProducerNbr());
			// COB_CODE: MOVE CF-PL-PRODUCER-NM  TO XZT06I-PDC-NM
			wsXz0t0006Row.setXzt06iPdcNm(ws.getConstantFields().getPlProducerNmFormatted());
			// COB_CODE: MOVE CF-ATC-PERSONAL-LINES
			//                                   TO XZT06I-TK-ACT-TYP-CD
			wsXz0t0006Row.setXzt06iTkActTypCd(ws.getConstantFields().getAtcPersonalLines());
		} else {
			//*  THE TERR NBR NEEDS TO BE FORMATTED LIKE THIS X-XXX
			// COB_CODE: MOVE MUT04O-MKT-TER-NBR(1:1)
			//                                   TO WS-PRODUCER-NBR-START
			ws.getWorkingStorageArea().getWsProducerNbr().setStart2Formatted(wsMu0t0004Row.getMut04oMktTerNbrFormatted().substring((1) - 1, 1));
			// COB_CODE: MOVE MUT04O-MKT-TER-NBR(2:3)
			//                                   TO WS-PRODUCER-NBR-END
			ws.getWorkingStorageArea().getWsProducerNbr().setEnd(wsMu0t0004Row.getMut04oMktTerNbrFormatted().substring((2) - 1, 4));
			// COB_CODE: MOVE WS-PRODUCER-NBR    TO XZT06I-PDC-NBR
			wsXz0t0006Row.setXzt06iPdcNbr(ws.getWorkingStorageArea().getWsProducerNbr().getWsProducerNbrFormatted());
			// COB_CODE: MOVE MUT04O-MKT-SVC-DSY-NM
			//                                   TO XZT06I-PDC-NM
			wsXz0t0006Row.setXzt06iPdcNm(wsMu0t0004Row.getMut04oMktSvcDsyNm());
			// COB_CODE: MOVE CF-ATC-COMMERCIAL-LINES
			//                                   TO XZT06I-TK-ACT-TYP-CD
			wsXz0t0006Row.setXzt06iTkActTypCd(ws.getConstantFields().getAtcCommercialLines());
		}
	}

	/**Original name: 3123-CALL-ADD-ACT-NOT-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   CALL THE ADD ACT_NOT SERVICE                                  *
	 * *****************************************************************</pre>*/
	private void callAddActNotSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-ADD-ACT-NOT-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P0022", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getAddActNotSvc(), new Xz0x0006());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3123-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotSvc());
			// COB_CODE: MOVE '3123-CALL-ADD-ACT-NOT-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3123-CALL-ADD-ACT-NOT-SVC");
			// COB_CODE: MOVE 'ADD ACT-NOT SERVICE CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ADD ACT-NOT SERVICE CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "FAILED MODULE EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getWsEibrespCdAsString(), ".  FAILED MODULE EIBRESP2 CODE IS ",
					ws.getWorkingStorageArea().getWsEibresp2CdAsString(), ".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3123-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-SVC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getAddActNotSvc());
		// COB_CODE: MOVE '3123-CALL-ADD-ACT-NOT-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3123-CALL-ADD-ACT-NOT-SVC");
		// COB_CODE: MOVE 'ADD ACT-NOT SERVICE'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("ADD ACT-NOT SERVICE");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3200-CRN-POL-NOT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ADD THE ROWS TO ACT_NOT_POL BASED ON POLICIES PROVIDED         *
	 * *****************************************************************
	 * *   VERIFY THE POLICIES ARE SET TO PENDING CANCEL NO, AND THE
	 * *   TERMINATION INDICATOR IS NO.</pre>*/
	private String crnPolNot() {
		String retcode = "";
		// COB_CODE: PERFORM 3210-VRF-POL-STA.
		retcode = rng3210VrfPolSta();
		if (!retcode.equals("")) {
			return retcode;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3200-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3200-EXIT
			return "";
		}
		//*   CREATE THE ROWS IN ACT_NOT_POL.
		// COB_CODE: PERFORM 3220-PRP-INSD-POL.
		rng3220PrpInsdPol();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3200-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3200-EXIT
			return "";
		}
		return "";
	}

	/**Original name: 3210-VRF-POL-STA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  VERIFY POLICIES TO BE TERMINATED                               *
	 * *****************************************************************
	 * * ALLOCATE THE MEMORY FOR THE XZAcyPndCncTmnPolList SERVICE.</pre>*/
	private String vrfPolSta() {
		// COB_CODE: PERFORM 3211-ALC-MEM-VRF-POL-STA.
		alcMemVrfPolSta();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3300-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3300-EXIT
			return "3300-EXIT";
		}
		//*   CALL THE XZAcyPndCncTmnPolList SERVICE TO GET LIST OF ACTIVE
		//*   POLICIES AND THEIR STATUS FOR THE ACCOUNT.
		// COB_CODE: PERFORM 3212-CALL-VRF-POL-STA.
		callVrfPolSta();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3120-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3120-EXIT
			return "3120-EXIT";
		}
		return "";
	}

	/**Original name: 3211-ALC-MEM-VRF-POL-STA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR GetAcyPndCncTmnPolListByAct            *
	 * *****************************************************************</pre>*/
	private void alcMemVrfPolSta() {
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T90S0-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x90s0.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3211-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-PND-CNC-STA-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetPndCncStaSvc());
			// COB_CODE: MOVE '3211-ALC-MEM-VRF-POL-STA'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3211-ALC-MEM-VRF-POL-STA");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("EIBRESP CODE IS ").append(ws.getWorkingStorageArea().getWsEibrespCdFormatted())
							.append(".  EIBRESP2 CODE IS ").append(ws.getWorkingStorageArea().getWsEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3211-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T90S0-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t90s0Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x90s0.class)));
		// COB_CODE: INITIALIZE WS-XZ0T90S0-ROW.
		initWsXz0t90s0Row();
	}

	/**Original name: 3212-CALL-VRF-POL-STA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   CALL THE GetAcyPndCncTmnPolListByAct OPERATION                *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void callVrfPolSta() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET WS-GET-POL-STATUS-LIST  TO TRUE.
		ws.getWorkingStorageArea().getWsOperationsCalled().setGetPolStatusList();
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-OPERATIONS-CALLED   TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getWsOperationsCalled());
		// COB_CODE: MOVE XZY022-USERID          TO XZT9SI-USERID.
		wsXz0t90s0Row.setiUserid(ws.getWsXz0y0022Row().getUserid());
		// COB_CODE: MOVE XZY022-CSR-ACT-NBR     TO XZT9SI-ACT-NBR.
		wsXz0t90s0Row.setiActNbr(ws.getWsXz0y0022Row().getCsrActNbr());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-GET-PND-CNC-STA-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P0022", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getGetPndCncStaSvc(), new Xz0x90s0());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3212-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-PND-CNC-STA-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetPndCncStaSvc());
			// COB_CODE: MOVE '3212-CALL-VRF-POL-STA'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3212-CALL-VRF-POL-STA");
			// COB_CODE: MOVE 'GET PND CNC STA PGM CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("GET PND CNC STA PGM CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "FAILED MODULE EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getWsEibrespCdAsString(), ".  FAILED MODULE EIBRESP2 CODE IS ",
					ws.getWorkingStorageArea().getWsEibresp2CdAsString(), ".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3212-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-SVC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getAddActNotSvc());
		// COB_CODE: MOVE '3212-CALL-VRF-POL-STA'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3212-CALL-VRF-POL-STA");
		// COB_CODE: MOVE 'GET PND CNC STA PGM'  TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("GET PND CNC STA PGM");
		// COB_CODE: MOVE 'CANCEL DATA'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("CANCEL DATA");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 3220-PRP-INSD-POL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CREATE ACT_NOT_POL ROWS FOR TERMINATION.                       *
	 * *****************************************************************</pre>*/
	private String prpInsdPol() {
		String retcode = "";
		// COB_CODE: PERFORM 3221-ALC-MEM-PRP-INSD-POL-SVC.
		alcMemPrpInsdPolSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3220-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3220-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3222-SET-INPUT-PRP-INSD-POL.
		retcode = rng3222SetInputPrpInsdPol();
		if (!retcode.equals("")) {
			return retcode;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3220-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3220-EXIT
			return "";
		}
		return "";
	}

	/**Original name: 3221-ALC-MEM-PRP-INSD-POL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE PREPARE INSURED POLICY SERVICE.    *
	 * *****************************************************************</pre>*/
	private void alcMemPrpInsdPolSvc() {
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T90H0-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x90h0.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3221-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
			// COB_CODE: MOVE '3221-ALC-MEM-PRP-INSD-POL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3221-ALC-MEM-PRP-INSD-POL-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("EIBRESP CODE IS ").append(ws.getWorkingStorageArea().getWsEibrespCdFormatted())
							.append(".  EIBRESP2 CODE IS ").append(ws.getWorkingStorageArea().getWsEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3221-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T90H0-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t90h0Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x90h0.class)));
		// COB_CODE: INITIALIZE WS-XZ0T90H0-ROW.
		initWsXz0t90h0Row();
	}

	/**Original name: 3222-SET-INPUT-PRP-INSD-POL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SETUP INPUT FOR PREPARE INSURED POLICY OPERATION.              *
	 * *****************************************************************</pre>*/
	private void setInputPrpInsdPol() {
		// COB_CODE: MOVE +0                     TO SS-IP
		//                                          SS-TP.
		ws.getSubscripts().setIp(((short) 0));
		ws.getSubscripts().setTp(((short) 0));
		// COB_CODE: SET WS-PREPARE-INS-POL      TO TRUE.
		ws.getWorkingStorageArea().getWsOperationsCalled().setPrepareInsPol();
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-OPERATIONS-CALLED   TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getWsOperationsCalled());
		// COB_CODE: MOVE XZT06O-TK-NOT-PRC-TS   TO XZT9HI-TK-NOT-PRC-TS.
		wsXz0t90h0Row.setiTkNotPrcTs(wsXz0t0006Row.getXzt06oTkNotPrcTs());
		// COB_CODE: MOVE XZY022-CSR-ACT-NBR     TO XZT9HI-CSR-ACT-NBR.
		wsXz0t90h0Row.setiCsrActNbr(ws.getWsXz0y0022Row().getCsrActNbr());
		// COB_CODE: MOVE XZY022-USERID          TO XZT9HI-USERID.
		wsXz0t90h0Row.setiUserid(ws.getWsXz0y0022Row().getUserid());
	}

	/**Original name: 3222-A<br>*/
	private void a() {
		// COB_CODE: ADD +1                      TO SS-IP.
		ws.getSubscripts().setIp(Trunc.toShort(1 + ws.getSubscripts().getIp(), 4));
		// COB_CODE: MOVE +1                     TO SS-AP.
		ws.getSubscripts().setAp(((short) 1));
	}

	/**Original name: 3222-B<br>*/
	private String b() {
		ConcatUtil concatUtil = null;
		// COB_CODE:      IF SS-IP > CF-MAX-ACT-POL
		//                  OR
		//                   XZY022-CP-POL-NBR(SS-IP) = SPACES
		//           *        WE HAVE REACHED THE END OF THE POLICY LIST
		//                    GO TO 3222-EXIT
		//                END-IF.
		if (ws.getSubscripts().getIp() > ws.getConstantFields().getMaxActPol()
				|| Characters.EQ_SPACE.test(ws.getWsXz0y0022Row().getCncPolList(ws.getSubscripts().getIp()).getXzy022CpPolNbr())) {
			//        WE HAVE REACHED THE END OF THE POLICY LIST
			// COB_CODE: IF SS-TP > +0
			//               END-IF
			//           END-IF
			if (ws.getSubscripts().getTp() > 0) {
				// COB_CODE: PERFORM 3222A-CALL-PRP-INSD-POL-SVC
				aCallPrpInsdPolSvc();
				// COB_CODE: IF UBOC-HALT-AND-RETURN
				//                GO TO 3220-EXIT
				//           END-IF
				if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
					// COB_CODE: GO TO 3220-EXIT
					return "3220-EXIT";
				}
			}
			// COB_CODE: GO TO 3222-EXIT
			return "";
		}
		//    WRITE OUT MAX OF 50 ACT_NOT_POL ROWS AT A TIME DUE TO LIMITS
		//    ON THE PREPARE INSURED POLICY SERVICE
		// COB_CODE: IF SS-TP >= CF-MAX-POL-NOT
		//               MOVE +0                 TO SS-TP
		//           END-IF.
		if (ws.getSubscripts().getTp() >= ws.getConstantFields().getMaxPolNot()) {
			// COB_CODE: PERFORM 3222A-CALL-PRP-INSD-POL-SVC
			aCallPrpInsdPolSvc();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 3222-EXIT
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3222-EXIT
				return "";
			}
			// COB_CODE: MOVE +0                 TO SS-TP
			ws.getSubscripts().setTp(((short) 0));
		}
		// COB_CODE:      IF SS-AP > CF-MAX-ACT-POL
		//                  OR
		//                   XZT9SO-POL-NBR(SS-AP) = SPACES
		//           *        LOG WARNING THAT POLICY WAS NOT FOUND ON ACCOUNT
		//                    GO TO 3222-A
		//                END-IF.
		if (ws.getSubscripts().getAp() > ws.getConstantFields().getMaxActPol()
				|| Characters.EQ_SPACE.test(wsXz0t90s0Row.getoPolNbr(ws.getSubscripts().getAp()))) {
			//        LOG WARNING THAT POLICY WAS NOT FOUND ON ACCOUNT
			// COB_CODE: SET WS-SET-TMN-FLG      TO TRUE
			ws.getWorkingStorageArea().getWsOperationsCalled().setSetTmnFlg();
			// COB_CODE: SET WS-NON-LOGGABLE-WARNING
			//                                   TO TRUE
			ws.getWsNonLoggableWarnOrErrSw().setWarning();
			// COB_CODE: MOVE WS-OPERATIONS-CALLED
			//                                   TO UWRN-FAILED-MODULE
			ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getWsOperationsCalled().getWsOperationsCalled());
			// COB_CODE: MOVE 'ACT_NOT_POL'      TO UWRN-FAILED-TABLE-OR-FILE
			ws.getUwrnCommon().setFailedTableOrFile("ACT_NOT_POL");
			// COB_CODE: MOVE 'GEN_ALLTXT'       TO UWRN-WARNING-CODE
			ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
			// COB_CODE: MOVE SPACES             TO WS-NONLOG-PLACEHOLDER-VALUES
			ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
			// COB_CODE: MOVE 'POL_NBR'          TO WS-NONLOG-ERR-COL1-NAME
			ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Name("POL_NBR");
			// COB_CODE: MOVE XZY022-CP-POL-NBR(SS-IP)
			//                                   TO XZY022-CP-POL-NBR(SS-IP)
			ws.getWsXz0y0022Row().getCncPolList(ws.getSubscripts().getIp())
					.setXzy022CpPolNbr(ws.getWsXz0y0022Row().getCncPolList(ws.getSubscripts().getIp()).getXzy022CpPolNbr());
			// COB_CODE: STRING 'POLICY '
			//                  XZY022-CP-POL-NBR(SS-IP)
			//                  ' WAS NOT FOUND ON ACCOUNT.'
			//                   DELIMITED BY SIZE
			//                  INTO WS-NONLOG-ERR-ALLTXT-TEXT
			//           END-STRING
			concatUtil = ConcatUtil.buildString(WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT, "POLICY ",
					ws.getWsXz0y0022Row().getCncPolList(ws.getSubscripts().getIp()).getXzy022CpPolNbrFormatted(), " WAS NOT FOUND ON ACCOUNT.");
			ws.getWsNonlogPlaceholderValues()
					.setNonlogErrAlltxtText(concatUtil.replaceInString(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtTextFormatted()));
			// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
			procNonLogWrnOrErr();
			// COB_CODE: GO TO 3222-A
			return "3222-A";
		}
		// COB_CODE:      IF XZT9SO-POL-NBR(SS-AP) = XZY022-CP-POL-NBR(SS-IP)
		//           *        FOR TERMINATION PENDING CANCEL AND TERMINATION INDICATORS
		//           *        MUST BE SET TO NO TO QUALIFY
		//                    END-IF
		//                ELSE
		//                    GO TO 3222-B
		//                END-IF.
		if (Conditions.eq(wsXz0t90s0Row.getoPolNbr(ws.getSubscripts().getAp()),
				ws.getWsXz0y0022Row().getCncPolList(ws.getSubscripts().getIp()).getXzy022CpPolNbr())) {
			//        FOR TERMINATION PENDING CANCEL AND TERMINATION INDICATORS
			//        MUST BE SET TO NO TO QUALIFY
			// COB_CODE:          IF XZT9SO-PND-CNC-IND(SS-AP) = CF-NO
			//                     AND
			//                       XZT9SO-TMN-IND(SS-AP) = CF-NO
			//                        GO TO 3222-A
			//                    ELSE
			//           *        LOG WARNING THAT POLICY WAS NOT SUITABLE FOR TERMINATION
			//                        GO TO 3222-A
			//                    END-IF
			if (wsXz0t90s0Row.getoPndCncInd(ws.getSubscripts().getAp()) == ws.getConstantFields().getNo()
					&& wsXz0t90s0Row.getoTmnInd(ws.getSubscripts().getAp()) == ws.getConstantFields().getNo()) {
				// COB_CODE: ADD +1              TO SS-TP
				ws.getSubscripts().setTp(Trunc.toShort(1 + ws.getSubscripts().getTp(), 4));
				// COB_CODE: MOVE XZT9SO-POL-NBR(SS-AP)
				//                               TO XZT9HI-POL-NBR(SS-TP)
				wsXz0t90h0Row.setiPolNbr(ws.getSubscripts().getTp(), wsXz0t90s0Row.getoPolNbr(ws.getSubscripts().getAp()));
				// COB_CODE: MOVE XZT9SO-POL-EFF-DT(SS-AP)
				//                               TO XZT9HI-POL-EFF-DT(SS-TP)
				wsXz0t90h0Row.setiPolEffDt(ws.getSubscripts().getTp(), wsXz0t90s0Row.getoPolEffDt(ws.getSubscripts().getAp()));
				// COB_CODE: MOVE XZT9SO-POL-EXP-DT(SS-AP)
				//                               TO XZT9HI-POL-EXP-DT(SS-TP)
				wsXz0t90h0Row.setiPolExpDt(ws.getSubscripts().getTp(), wsXz0t90s0Row.getoPolExpDt(ws.getSubscripts().getAp()));
				// COB_CODE: MOVE WS-CURRENT-DATE
				//                               TO XZT9HI-NOT-EFF-DT(SS-TP)
				wsXz0t90h0Row.setiNotEffDt(ws.getSubscripts().getTp(), ws.getWorkingStorageArea().getWsCurrentTimestampX().getDateFld());
				// COB_CODE: GO TO 3222-A
				return "3222-A";
			} else {
				//        LOG WARNING THAT POLICY WAS NOT SUITABLE FOR TERMINATION
				// COB_CODE: SET WS-SET-TMN-FLG  TO TRUE
				ws.getWorkingStorageArea().getWsOperationsCalled().setSetTmnFlg();
				// COB_CODE: SET WS-NON-LOGGABLE-WARNING
				//                               TO TRUE
				ws.getWsNonLoggableWarnOrErrSw().setWarning();
				// COB_CODE: MOVE WS-OPERATIONS-CALLED
				//                               TO UWRN-FAILED-MODULE
				ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getWsOperationsCalled().getWsOperationsCalled());
				// COB_CODE: MOVE 'ACT_NOT_POL'  TO UWRN-FAILED-TABLE-OR-FILE
				ws.getUwrnCommon().setFailedTableOrFile("ACT_NOT_POL");
				// COB_CODE: MOVE 'GEN_ALLTXT'   TO UWRN-WARNING-CODE
				ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
				// COB_CODE: MOVE SPACES         TO WS-NONLOG-PLACEHOLDER-VALUES
				ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
				// COB_CODE: MOVE 'POL_NBR'      TO WS-NONLOG-ERR-COL1-NAME
				ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Name("POL_NBR");
				// COB_CODE: MOVE XZY022-CP-POL-NBR(SS-IP)
				//                               TO WS-NONLOG-ERR-COL1-VALUE
				ws.getWsNonlogPlaceholderValues()
						.setNonlogErrCol1Value(ws.getWsXz0y0022Row().getCncPolList(ws.getSubscripts().getIp()).getXzy022CpPolNbr());
				// COB_CODE: STRING 'POLICY '
				//                  XZY022-CP-POL-NBR(SS-IP)
				//                  ' DOES NOT QUALIFY FOR TERMINATION.'
				//                   DELIMITED BY SIZE
				//                  INTO WS-NONLOG-ERR-ALLTXT-TEXT
				//           END-STRING
				concatUtil = ConcatUtil.buildString(WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT, "POLICY ",
						ws.getWsXz0y0022Row().getCncPolList(ws.getSubscripts().getIp()).getXzy022CpPolNbrFormatted(),
						" DOES NOT QUALIFY FOR TERMINATION.");
				ws.getWsNonlogPlaceholderValues()
						.setNonlogErrAlltxtText(concatUtil.replaceInString(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtTextFormatted()));
				// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR
				procNonLogWrnOrErr();
				// COB_CODE: GO TO 3222-A
				return "3222-A";
			}
		} else {
			// COB_CODE: ADD +1                  TO SS-AP
			ws.getSubscripts().setAp(Trunc.toShort(1 + ws.getSubscripts().getAp(), 4));
			// COB_CODE: GO TO 3222-B
			return "3222-B";
		}
	}

	/**Original name: 3222A-CALL-PRP-INSD-POL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   CALL THE PREPARE INSURCED POLICY SERVICE.                     *
	 * *****************************************************************</pre>*/
	private void aCallPrpInsdPolSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-PREPARE-INS-POL-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P0022", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc(), new Xz0x90h0());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3222A-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
			// COB_CODE: MOVE '3222A-CALL-PRP-INSD-POL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3222A-CALL-PRP-INSD-POL-SVC");
			// COB_CODE: MOVE 'PREPARE INSURED POLICY CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("PREPARE INSURED POLICY CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "FAILED MODULE EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getWsEibrespCdAsString(), ".  FAILED MODULE EIBRESP2 CODE IS ",
					ws.getWorkingStorageArea().getWsEibresp2CdAsString(), ".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3222A-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
		//                                       TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
		// COB_CODE: MOVE '3222A-CALL-PRP-INSD-POL-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3222A-CALL-PRP-INSD-POL-SVC");
		// COB_CODE: MOVE 'PREPARE INSURED POLICY'
		//                                       TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("PREPARE INSURED POLICY");
		// COB_CODE: MOVE 'CANCEL DATA'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("CANCEL DATA");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
		// COB_CODE: INITIALIZE XZT9HI-POLICY-LIST-TBL.
		initIPolicyListTbl();
	}

	/**Original name: 3300-CPL-TMN-FLG-SET_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  UPDATE ACT_NOT STATUS TO COMPLETE THE TERMINATION.             *
	 * *****************************************************************
	 * * ALLOCATE MEMORY FOR THE UPDATE ACCOUNT NOTICE STATUS SERVICE.</pre>*/
	private void cplTmnFlgSet() {
		// COB_CODE: PERFORM 3310-ALC-MEM-UPD-NOT-STA-SVC.
		alcMemUpdNotStaSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3300-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3300-EXIT
			return;
		}
		// COB_CODE: PERFORM 3320-SET-INPUT-UPD-STATUS.
		setInputUpdStatus();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3300-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3300-EXIT
			return;
		}
		// COB_CODE: PERFORM 3330-CALL-UPD-STATUS-SVC.
		callUpdStatusSvc();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3300-EXIT
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3300-EXIT
			return;
		}
	}

	/**Original name: 3310-ALC-MEM-UPD-NOT-STA-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ALLOCATE THE MEMORY FOR THE UPDATE NOTIFICATION STATUS SERVICE *
	 * *****************************************************************</pre>*/
	private void alcMemUpdNotStaSvc() {
		// COB_CODE: INITIALIZE PPC-MEMORY-ALLOCATION-PARMS.
		initPpcMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF WS-XZ0T90M0-ROW
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getWsProxyProgramArea().setPpcServiceDataSize(LServiceContractAreaXz0x90m0.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(PPC-SERVICE-DATA-POINTER)
		//               FLENGTH(PPC-SERVICE-DATA-SIZE)
		//               INITIMG(LOW-VALUES)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsProxyProgramArea()
				.setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext, ws.getWsProxyProgramArea().getPpcServiceDataSize()));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3310-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
			// COB_CODE: MOVE '3310-ALC-MEM-UPD-NOT-STA-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3310-ALC-MEM-UPD-NOT-STA-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO ALLOCATE MEMORY FOR SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("EIBRESP CODE IS ").append(ws.getWorkingStorageArea().getWsEibrespCdFormatted())
							.append(".  EIBRESP2 CODE IS ").append(ws.getWorkingStorageArea().getWsEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3310-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF WS-XZ0T90M0-ROW
		//                                       TO PPC-SERVICE-DATA-POINTER.
		wsXz0t90m0Row = ((pointerManager.resolve(ws.getWsProxyProgramArea().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x90m0.class)));
		// COB_CODE: INITIALIZE WS-XZ0T90M0-ROW.
		initWsXz0t90m0Row();
	}

	/**Original name: 3320-SET-INPUT-UPD-STATUS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET THE PARMS FOR THE UPDATE NOTIFICATION STATUS SERVICE      *
	 * *****************************************************************
	 * *  DO NOT ALLOW SERVICES TO COMMIT ANY POSSIBLE CHANGES IN CASE
	 * *  AN ERROR OCCURS LATER.  OVERALL SERVICE WILL HANDLE COMMITS.</pre>*/
	private void setInputUpdStatus() {
		// COB_CODE: SET WS-UPD-NOT-STA          TO TRUE.
		ws.getWorkingStorageArea().getWsOperationsCalled().setUpdNotSta();
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE.
		ws.getWsProxyProgramArea().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE WS-OPERATIONS-CALLED   TO PPC-OPERATION.
		ws.getWsProxyProgramArea().setPpcOperation(ws.getWorkingStorageArea().getWsOperationsCalled().getWsOperationsCalled());
		// COB_CODE: MOVE XZY022-USERID          TO XZT9MI-USERID.
		wsXz0t90m0Row.setiUserid(ws.getWsXz0y0022Row().getUserid());
		// COB_CODE: MOVE XZY022-CSR-ACT-NBR     TO XZT9MI-CSR-ACT-NBR.
		wsXz0t90m0Row.setiCsrActNbr(ws.getWsXz0y0022Row().getCsrActNbr());
		// COB_CODE: MOVE XZT06O-TK-NOT-PRC-TS   TO XZT9MI-TK-NOT-PRC-TS.
		wsXz0t90m0Row.setiTkNotPrcTs(wsXz0t0006Row.getXzt06oTkNotPrcTs());
		// COB_CODE: MOVE CF-TMN-CPL-STA-CD      TO XZT9MI-TK-ACT-NOT-STA-CD.
		wsXz0t90m0Row.setiTkActNotStaCd(ws.getConstantFields().getTmnCplStaCd());
	}

	/**Original name: 3330-CALL-UPD-STATUS-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   CALL THE UPDATE NOTIFICATION STATUS SERVICE                   *
	 * *****************************************************************</pre>*/
	private void callUpdStatusSvc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-SP-UPD-NOT-STA-SVC)
		//               COMMAREA(WS-PROXY-PROGRAM-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P0022", execContext).commarea(ws.getWsProxyProgramArea()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxy().getUpdNotStaSvc(), new Xz0x90m0());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3330-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-UPD-NOT-STA-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getUpdNotStaSvc());
			// COB_CODE: MOVE '3330-CALL-UPD-STATUS-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3330-CALL-UPD-STATUS-SVC");
			// COB_CODE: MOVE 'UPDATE NOTIFICATION STATUS SVC CALL FAILED.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE NOTIFICATION STATUS SVC CALL FAILED.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: STRING 'FAILED MODULE EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  FAILED MODULE EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "FAILED MODULE EIBRESP CODE IS ",
					ws.getWorkingStorageArea().getWsEibrespCdAsString(), ".  FAILED MODULE EIBRESP2 CODE IS ",
					ws.getWorkingStorageArea().getWsEibresp2CdAsString(), ".");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3330-EXIT
			return;
		}
		//*  CHECK FOR ERRORS/WARNINGS FROM THE SERVICE.
		// COB_CODE: MOVE CF-SP-UPD-NOT-STA-SVC  TO WS-EC-MODULE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setModule(ws.getConstantFields().getServiceProxy().getUpdNotStaSvc());
		// COB_CODE: MOVE '3330-CALL-UPD-STATUS-SVC'
		//                                       TO WS-EC-PARAGRAPH.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setParagraph("3330-CALL-UPD-STATUS-SVC");
		// COB_CODE: MOVE 'UPDATE NOT STA SVC'   TO WS-EC-TABLE-OR-FILE.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setTableOrFile("UPDATE NOT STA SVC");
		// COB_CODE: MOVE 'ACCT NUMBER'          TO WS-EC-COLUMN-OR-FIELD.
		ws.getWorkingStorageArea().getWsErrorCheckInfo().setColumnOrField("ACCT NUMBER");
		// COB_CODE: PERFORM 9900-CHECK-ERRORS.
		checkErrors();
	}

	/**Original name: 8000-ENDING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  ENDING HOUSEKEEPING.                                           *
	 * *****************************************************************</pre>*/
	private void endingHousekeeping() {
		// COB_CODE: PERFORM 8100-FREE-MEM-GET-INSD-DTL-SVC.
		freeMemGetInsdDtlSvc();
		// COB_CODE: PERFORM 8200-FREE-MEM-ACT-NOT-SVC.
		freeMemActNotSvc();
		// COB_CODE: PERFORM 8300-FREE-MEM-ACY-TMN-POL-SVC.
		freeMemAcyTmnPolSvc();
		// COB_CODE: PERFORM 8400-FREE-MEM-PRP-INS-POL-SVC.
		freeMemPrpInsPolSvc();
		// COB_CODE: PERFORM 8500-FREE-UPD-NOT-STA-SVC.
		freeUpdNotStaSvc();
	}

	/**Original name: 8100-FREE-MEM-GET-INSD-DTL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE ACCOUNT NOTIFICATION SERVICE.          *
	 * *****************************************************************</pre>*/
	private void freeMemGetInsdDtlSvc() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-MU0T0004-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsMu0t0004Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-GET-INSURED-DETAIL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getGetInsuredDetailSvc());
			// COB_CODE: MOVE '8100-FREE-MEM-GET-INSD-DTL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("8100-FREE-MEM-GET-INSD-DTL-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY AFTER SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY AFTER SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("EIBRESP CODE IS ").append(ws.getWorkingStorageArea().getWsEibrespCdFormatted())
							.append(".  EIBRESP2 CODE IS ").append(ws.getWorkingStorageArea().getWsEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 8200-FREE-MEM-ACT-NOT-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE ACCOUNT NOTIFICATION SERVICE.          *
	 * *****************************************************************</pre>*/
	private void freeMemActNotSvc() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T0006-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t0006Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-ADD-ACT-NOT-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getAddActNotSvc());
			// COB_CODE: MOVE '8200-FREE-MEM-ACT-NOT-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("8200-FREE-MEM-ACT-NOT-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY AFTER SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY AFTER SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("EIBRESP CODE IS ").append(ws.getWorkingStorageArea().getWsEibrespCdFormatted())
							.append(".  EIBRESP2 CODE IS ").append(ws.getWorkingStorageArea().getWsEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 8300-FREE-MEM-ACY-TMN-POL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE ACY/PND/CNC/TMP POLICY SERVICE.        *
	 * *****************************************************************</pre>*/
	private void freeMemAcyTmnPolSvc() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T90S0-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t90s0Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
			// COB_CODE: MOVE '8300-FREE-MEM-ACY-TMN-POL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("8300-FREE-MEM-ACY-TMN-POL-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY AFTER SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY AFTER SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("EIBRESP CODE IS ").append(ws.getWorkingStorageArea().getWsEibrespCdFormatted())
							.append(".  EIBRESP2 CODE IS ").append(ws.getWorkingStorageArea().getWsEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 8400-FREE-MEM-PRP-INS-POL-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE PREPARE INSURED POLICY SERVICE.        *
	 * *****************************************************************</pre>*/
	private void freeMemPrpInsPolSvc() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T90H0-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t90h0Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-PREPARE-INS-POL-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getPrepareInsPolSvc());
			// COB_CODE: MOVE '8400-FREE-MEM-PRP-INS-POL-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("8400-FREE-MEM-PRP-INS-POL-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY AFTER SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY AFTER SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("EIBRESP CODE IS ").append(ws.getWorkingStorageArea().getWsEibrespCdFormatted())
							.append(".  EIBRESP2 CODE IS ").append(ws.getWorkingStorageArea().getWsEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 8500-FREE-UPD-NOT-STA-SVC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  FREE THE MEMORY FOR THE UPDATE NOTIFICATION STATUS SERVICE.    *
	 * *****************************************************************</pre>*/
	private void freeUpdNotStaSvc() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(WS-XZ0T90M0-ROW)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(wsXz0t90m0Row));
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WEB-RECEIVE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
			// COB_CODE: MOVE CF-SP-UPD-NOT-STA-SVC
			//                                   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getConstantFields().getServiceProxy().getUpdNotStaSvc());
			// COB_CODE: MOVE '8500-FREE-UPM-NOT-STA-SVC'
			//                                   TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("8500-FREE-UPM-NOT-STA-SVC");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-CD
			ws.getWorkingStorageArea().setWsEibrespCd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode(), 4));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-CD
			ws.getWorkingStorageArea().setWsEibresp2Cd(Trunc.toShort(ws.getWsNotSpecificMisc().getResponseCode2(), 4));
			// COB_CODE: MOVE 'FAILED TO FREE MEMORY AFTER SERVICE CALL.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FAILED TO FREE MEMORY AFTER SERVICE CALL.");
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'EIBRESP CODE IS '
			//                  WS-EIBRESP-CD
			//                  '.  EIBRESP2 CODE IS '
			//                  WS-EIBRESP2-CD
			//                  '.'  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("EIBRESP CODE IS ").append(ws.getWorkingStorageArea().getWsEibrespCdFormatted())
							.append(".  EIBRESP2 CODE IS ").append(ws.getWorkingStorageArea().getWsEibresp2CdFormatted()).append(".").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowName(), UbocCommInfo.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocMsgId(), UbocCommInfo.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocSessionId(), UbocCommInfo.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserid(), UbocCommInfo.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore(), UbocCommInfo.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfo.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore(), UbocCommInfo.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfo.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfo.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWorkingStorageArea().getWsProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkingStorageArea().getWsProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0P0022", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWorkingStorageArea().getWsProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getCommInfo().setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWorkingStorageArea().getWsProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getCommInfo().setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWorkingStorageArea().getWsApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: 9900-CHECK-ERRORS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   CHECK FOR ERROR, NLBE, OR WARNING RETURNED FROM SERVICE      *
	 * ****************************************************************</pre>*/
	private void checkErrors() {
		// COB_CODE: IF PPC-NO-ERROR-CODE
		//               GO TO 9900-EXIT
		//           END-IF.
		if (ws.getWsProxyProgramArea().getPpcErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: GO TO 9900-EXIT
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN PPC-FATAL-ERROR-CODE
		//                   PERFORM 9910-SET-FATAL-ERROR
		//               WHEN PPC-NLBE-CODE
		//                   PERFORM 9920-SET-NLBE
		//               WHEN PPC-WARNING-CODE
		//                      AND
		//                    WS-EC-MODULE = WS-PROGRAM-NAME
		//                   PERFORM 9930-SET-WARNING
		//               WHEN OTHER
		//                   CONTINUE
		//           END-EVALUATE.
		if (ws.getWsProxyProgramArea().getPpcErrorReturnCode().isFatalErrorCode()) {
			// COB_CODE: PERFORM 9910-SET-FATAL-ERROR
			setFatalError();
		} else if (ws.getWsProxyProgramArea().getPpcErrorReturnCode().isNlbeCode()) {
			// COB_CODE: PERFORM 9920-SET-NLBE
			rng9920SetNlbe();
		} else if (ws.getWsProxyProgramArea().getPpcErrorReturnCode().isWarningCode()
				&& Conditions.eq(ws.getWorkingStorageArea().getWsErrorCheckInfo().getModule(), ws.getWorkingStorageArea().getWsProgramName())) {
			// COB_CODE: PERFORM 9930-SET-WARNING
			rng9930SetWarning();
		} else {
			// COB_CODE: CONTINUE
			//continue
		}
	}

	/**Original name: 9910-SET-FATAL-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *   SET FOR FATAL ERROR                                           *
	 *   SAVE OFF ANY ERROR MESSAGE THAT WAS RECEIVED FROM THE CALLED  *
	 *   SERVICE IN ORDER TO PUT IT IN THE DISPLAY AREAS AFTER LOGGING *
	 *   THE BUSINESS PROCESS ERROR.                                   *
	 *   THIS WILL ALLOW US TO DISPLAY THE ACTUAL ERROR THAT OCCURRED  *
	 *   IN THE CALLED SERVICE TO THE USER.                            *
	 * *****************************************************************</pre>*/
	private void setFatalError() {
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: SET ETRA-CICS-WEB-RECEIVE   TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWebReceive();
		// COB_CODE: MOVE WS-EC-MODULE           TO EFAL-ERR-OBJECT-NAME.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkingStorageArea().getWsErrorCheckInfo().getModule());
		// COB_CODE: MOVE WS-EC-PARAGRAPH        TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph(ws.getWorkingStorageArea().getWsErrorCheckInfo().getParagraph());
		// COB_CODE: MOVE PPC-FATAL-ERROR-MESSAGE
		//                                       TO EFAL-ERR-COMMENT
		//                                          EFAL-OBJ-DATA-KEY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getWsProxyProgramArea().getPpcFatalErrorMessage());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 9920-SET-NLBE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR NLBE                                                 *
	 * ****************************************************************</pre>*/
	private void setNlbe() {
		// COB_CODE: MOVE +0                     TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(((short) 0));
	}

	/**Original name: 9920-A<br>*/
	private String a1() {
		// COB_CODE: ADD +1                      TO SS-MSG-IDX.
		ws.getSubscripts().setMsgIdx(Trunc.toShort(1 + ws.getSubscripts().getMsgIdx(), 4));
		// COB_CODE: IF PPC-NON-LOG-ERR-MSG(SS-MSG-IDX) = SPACES
		//               GO TO 9920-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg())) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// we are setting UBOC-HALT-AND-RETURN to true so that if a service
		// gets an NLBE, the service ends.
		// COB_CODE: SET UBOC-HALT-AND-RETURN    TO TRUE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setBusErr();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO NLBE-FAILED-TABLE-OR-FILE.
		ws.getNlbeCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getWsErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO NLBE-FAILED-COLUMN-OR-FIELD.
		ws.getNlbeCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getWsErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-NON-LOG-ERR-MSG (SS-MSG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcNonLoggableErrors(ws.getSubscripts().getMsgIdx()).getPpcNonLogErrMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-MSG-IDX-MAX
		//               GO TO 9920-EXIT
		//           END-IF.
		if (ws.getSubscripts().isMsgIdxMax()) {
			// COB_CODE: GO TO 9920-EXIT
			return "";
		}
		// COB_CODE: GO TO 9920-A.
		return "9920-A";
	}

	/**Original name: 9930-SET-WARNING_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   SET FOR WARNING                                              *
	 * ****************************************************************</pre>*/
	private void setWarning() {
		// COB_CODE: MOVE +0                     TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(((short) 0));
	}

	/**Original name: 9930-A<br>*/
	private String a2() {
		// COB_CODE: ADD +1                      TO SS-WNG-IDX.
		ws.getSubscripts().setWngIdx(Trunc.toShort(1 + ws.getSubscripts().getWngIdx(), 4));
		// COB_CODE: IF PPC-WARN-MSG(SS-WNG-IDX) = SPACES
		//               GO TO 9930-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg())) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: SET WS-NON-LOGGABLE-WARNING TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setWarning();
		// COB_CODE: MOVE WS-EC-TABLE-OR-FILE    TO UWRN-FAILED-TABLE-OR-FILE.
		ws.getUwrnCommon().setFailedTableOrFile(ws.getWorkingStorageArea().getWsErrorCheckInfo().getTableOrFile());
		// COB_CODE: MOVE WS-EC-COLUMN-OR-FIELD  TO UWRN-FAILED-COLUMN-OR-FIELD.
		ws.getUwrnCommon().setFailedColumnOrField(ws.getWorkingStorageArea().getWsErrorCheckInfo().getColumnOrField());
		// COB_CODE: MOVE 'GEN_ALLTXT'           TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode("GEN_ALLTXT");
		// COB_CODE: MOVE SPACES                 TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE PPC-WARN-MSG (SS-WNG-IDX)
		//                                       TO WS-NONLOG-ERR-ALLTXT-TEXT.
		ws.getWsNonlogPlaceholderValues()
				.setNonlogErrAlltxtText(ws.getWsProxyProgramArea().getPpcWarnings(ws.getSubscripts().getWngIdx()).getPpcWarnMsg());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
		// COB_CODE: IF SS-WNG-IDX-MAX
		//               GO TO 9930-EXIT
		//           END-IF.
		if (ws.getSubscripts().isWngIdxMax()) {
			// COB_CODE: GO TO 9930-EXIT
			return "";
		}
		// COB_CODE: GO TO 9930-A.
		return "9930-A";
	}

	/**Original name: RNG_3200-CRN-POL-NOT_FIRST_SENTENCES-_-3200-EXIT<br>*/
	private void rng3200CrnPolNot() {
		String retcode = "";
		boolean goto3200Exit = false;
		retcode = crnPolNot();
		goto3200Exit = false;
	}

	/**Original name: RNG_3210-VRF-POL-STA_FIRST_SENTENCES-_-3210-EXIT_M<br>*/
	private String rng3210VrfPolSta() {
		String retcode = "";
		boolean goto3120Exit = false;
		boolean goto3210VrfPolStaFirstSentences = false;
		boolean goto3300Exit = false;
		boolean goto9920A = false;
		boolean goto9930A = false;
		boolean gotoExitlabel = false;
		do {
			if (goto3120Exit) {
				goto3120Exit = false;
				alcMemActNotSvc();
				setInputActNot();
				callAddActNotSvc();
				retcode = crnPolNot();
				return "3200-EXIT";
			}
			goto3210VrfPolStaFirstSentences = false;
			retcode = vrfPolSta();
			goto3120Exit = retcode.equals("3120-EXIT");
		} while (goto3120Exit);
		if (!retcode.equals("3300-EXIT")) {
			gotoExitlabel = true;
		}
		if (!gotoExitlabel) {
			goto3300Exit = false;
			alcMemUpdNotStaSvc();
			setInputUpdStatus();
			callUpdStatusSvc();
			endingHousekeeping();
			freeMemGetInsdDtlSvc();
			freeMemActNotSvc();
			freeMemAcyTmnPolSvc();
			freeMemPrpInsPolSvc();
			freeUpdNotStaSvc();
			iwaeInitializeWarnMsg();
			vcomValidateCommarea();
			logWarningOrError();
			procNonLogWrnOrErr();
			writeNlbeUmtRec();
			writeWarnUmtRec();
			convertErrorToText();
			checkErrors();
			setFatalError();
			setNlbe();
			do {
				goto9920A = false;
				retcode = a1();
			} while (retcode.equals("9920-A"));
			setWarning();
			do {
				goto9930A = false;
				retcode = a2();
			} while (retcode.equals("9930-A"));
			throw new ReturnException();
		}
		return "";
	}

	/**Original name: RNG_3220-PRP-INSD-POL_FIRST_SENTENCES-_-3220-EXIT<br>*/
	private void rng3220PrpInsdPol() {
		String retcode = "";
		boolean goto3220Exit = false;
		retcode = prpInsdPol();
		goto3220Exit = false;
	}

	/**Original name: RNG_3222-SET-INPUT-PRP-INSD-POL_FIRST_SENTENCES-_-3222-EXIT_M<br>*/
	private String rng3222SetInputPrpInsdPol() {
		String retcode = "";
		boolean goto3222A = false;
		boolean goto3222B = false;
		setInputPrpInsdPol();
		do {
			do {
				if (!goto3222B) {
					goto3222A = false;
					a();
				}
				goto3222B = false;
				retcode = b();
				if (retcode.equals("3220-EXIT")) {
					return retcode;
				}
			} while (retcode.equals("3222-A"));
			goto3222B = retcode.equals("3222-B");
		} while (goto3222B);
		return "";
	}

	/**Original name: RNG_9920-SET-NLBE_FIRST_SENTENCES-_-9920-EXIT<br>*/
	private void rng9920SetNlbe() {
		String retcode = "";
		boolean goto9920A = false;
		setNlbe();
		do {
			goto9920A = false;
			retcode = a1();
		} while (retcode.equals("9920-A"));
	}

	/**Original name: RNG_9930-SET-WARNING_FIRST_SENTENCES-_-9930-EXIT<br>*/
	private void rng9930SetWarning() {
		String retcode = "";
		boolean goto9930A = false;
		setWarning();
		do {
			goto9930A = false;
			retcode = a2();
		} while (retcode.equals("9930-A"));
	}

	public void initWsXz0y0022Row() {
		ws.getWsXz0y0022Row().setCsrActNbr("");
		for (int idx0 = 1; idx0 <= WsXz0y0022Row.CNC_POL_LIST_MAXOCCURS; idx0++) {
			ws.getWsXz0y0022Row().getCncPolList(idx0).setXzy022CpPolNbr("");
		}
		ws.getWsXz0y0022Row().setUserid("");
		ws.getWsXz0y0022Row().getNlbeInd().setNlbeInd(Types.SPACE_CHAR);
		ws.getWsXz0y0022Row().getWarningInd().setWarningInd(Types.SPACE_CHAR);
		ws.getWsXz0y0022Row().setNonLogErrMsg("");
		for (int idx0 = 1; idx0 <= WsXz0y0022Row.WARNINGS_MAXOCCURS; idx0++) {
			ws.getWsXz0y0022Row().getWarnings(idx0).setXzy022WarnMsg("");
		}
	}

	public void initPpcMemoryAllocationParms() {
		ws.getWsProxyProgramArea().setPpcServiceDataSize(0);
	}

	public void initWsMu0t0004Row() {
		wsMu0t0004Row.setMut04iTkCltId("");
		wsMu0t0004Row.setMut04iTkCiorShwObjKey("");
		wsMu0t0004Row.setMut04iTkAdrSeqNbr(0);
		wsMu0t0004Row.setMut04iUsrId("");
		wsMu0t0004Row.setMut04iActNbr("");
		wsMu0t0004Row.setMut04iTerNbr("");
		wsMu0t0004Row.setMut04iAsOfDt("");
		wsMu0t0004Row.setMut04iFein("");
		wsMu0t0004Row.setMut04iSsn("");
		wsMu0t0004Row.setMut04iPhnAcd("");
		wsMu0t0004Row.setMut04iPhnPfxNbr("");
		wsMu0t0004Row.setMut04iPhnLinNbr("");
		wsMu0t0004Row.setMut04oTkCltId("");
		wsMu0t0004Row.setMut04oCnIdvNmInd(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oCnDsyNm("");
		wsMu0t0004Row.setMut04oCnSrNm("");
		wsMu0t0004Row.setMut04oCnFstNm("");
		wsMu0t0004Row.setMut04oCnMdlNm("");
		wsMu0t0004Row.setMut04oCnLstNm("");
		wsMu0t0004Row.setMut04oCnSfx("");
		wsMu0t0004Row.setMut04oCnPfx("");
		wsMu0t0004Row.setMut04oCiClientId("");
		wsMu0t0004Row.setMut04oCiIdvNmInd(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oCiDsyNm("");
		wsMu0t0004Row.setMut04oCiSrNm("");
		wsMu0t0004Row.setMut04oCiFstNm("");
		wsMu0t0004Row.setMut04oCiMdlNm("");
		wsMu0t0004Row.setMut04oCiLstNm("");
		wsMu0t0004Row.setMut04oCiSfx("");
		wsMu0t0004Row.setMut04oCiPfx("");
		wsMu0t0004Row.setMut04oBsmAdrSeqNbr(0);
		wsMu0t0004Row.setMut04oBsmAdrId("");
		wsMu0t0004Row.setMut04oBsmLin1Adr("");
		wsMu0t0004Row.setMut04oBsmLin2Adr("");
		wsMu0t0004Row.setMut04oBsmCit("");
		wsMu0t0004Row.setMut04oBsmStAbb("");
		wsMu0t0004Row.setMut04oBsmPstCd("");
		wsMu0t0004Row.setMut04oBsmCty("");
		wsMu0t0004Row.setMut04oBsmCtrCd("");
		wsMu0t0004Row.setMut04oBslAdrSeqNbr(0);
		wsMu0t0004Row.setMut04oBslAdrId("");
		wsMu0t0004Row.setMut04oBslLin1Adr("");
		wsMu0t0004Row.setMut04oBslLin2Adr("");
		wsMu0t0004Row.setMut04oBslCit("");
		wsMu0t0004Row.setMut04oBslStAbb("");
		wsMu0t0004Row.setMut04oBslPstCd("");
		wsMu0t0004Row.setMut04oBslCty("");
		wsMu0t0004Row.setMut04oBslCtrCd("");
		wsMu0t0004Row.setMut04oBusPhnAcd("");
		wsMu0t0004Row.setMut04oBusPhnPfxNbr("");
		wsMu0t0004Row.setMut04oBusPhnLinNbr("");
		wsMu0t0004Row.setMut04oBusPhnExtNbr("");
		wsMu0t0004Row.setMut04oBusPhnNbrFmt("");
		wsMu0t0004Row.setMut04oFaxPhnAcd("");
		wsMu0t0004Row.setMut04oFaxPhnPfxNbr("");
		wsMu0t0004Row.setMut04oFaxPhnLinNbr("");
		wsMu0t0004Row.setMut04oFaxPhnExtNbr("");
		wsMu0t0004Row.setMut04oFaxPhnNbrFmt("");
		wsMu0t0004Row.setMut04oCtcPhnAcd("");
		wsMu0t0004Row.setMut04oCtcPhnPfxNbr("");
		wsMu0t0004Row.setMut04oCtcPhnLinNbr("");
		wsMu0t0004Row.setMut04oCtcPhnExtNbr("");
		wsMu0t0004Row.setMut04oCtcPhnNbrFmt("");
		wsMu0t0004Row.setMut04oFein("");
		wsMu0t0004Row.setMut04oSsn("");
		wsMu0t0004Row.setMut04oFeinFmt("");
		wsMu0t0004Row.setMut04oSsnFmt("");
		wsMu0t0004Row.setMut04oLegEtyCd("");
		wsMu0t0004Row.setMut04oLegEtyDes("");
		wsMu0t0004Row.setMut04oTobCd("");
		wsMu0t0004Row.setMut04oTobDes("");
		wsMu0t0004Row.setMut04oSicCd("");
		wsMu0t0004Row.setMut04oSicDes("");
		wsMu0t0004Row.setMut04oSegCd("");
		wsMu0t0004Row.setMut04oSegDes("");
		wsMu0t0004Row.setMut04oPcPsptActInd(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oPcActNbr("");
		wsMu0t0004Row.setMut04oPcActNbr2("");
		wsMu0t0004Row.setMut04oPcActNbrFmt("");
		wsMu0t0004Row.setMut04oPcActNbr2Fmt("");
		wsMu0t0004Row.setMut04oPsptActNbr("");
		wsMu0t0004Row.setMut04oPsptActNbrFmt("");
		wsMu0t0004Row.setMut04oActNmId(0);
		wsMu0t0004Row.setMut04oAct2NmId(0);
		wsMu0t0004Row.setMut04oObjCd("");
		wsMu0t0004Row.setMut04oObjCd2("");
		wsMu0t0004Row.setMut04oObjDes("");
		wsMu0t0004Row.setMut04oObjDes2("");
		wsMu0t0004Row.setMut04oSinWcActNbr("");
		wsMu0t0004Row.setMut04oSinWcActNbrFmt("");
		wsMu0t0004Row.setMut04oSinWcActNmId(0);
		wsMu0t0004Row.setMut04oSinWcObjCd("");
		wsMu0t0004Row.setMut04oSinWcObjDes("");
		wsMu0t0004Row.setMut04oSplBillActNbr("");
		wsMu0t0004Row.setMut04oSplBillActNbrFmt("");
		wsMu0t0004Row.setMut04oSplBillActNmId(0);
		wsMu0t0004Row.setMut04oSplBillObjCd("");
		wsMu0t0004Row.setMut04oSplBillObjDes("");
		wsMu0t0004Row.setMut04oPrsLinActNbr("");
		wsMu0t0004Row.setMut04oPrsLinActNbrFmt("");
		wsMu0t0004Row.setMut04oPrsLinActNmId(0);
		wsMu0t0004Row.setMut04oPrsLinObjCd("");
		wsMu0t0004Row.setMut04oPrsLinObjDes("");
		wsMu0t0004Row.setMut04oCstNbr("");
		wsMu0t0004Row.setMut04oMktCltId("");
		wsMu0t0004Row.setMut04oMktTerNbr("");
		wsMu0t0004Row.setMut04oMktSvcDsyNm("");
		wsMu0t0004Row.setMut04oMktTypeCd(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oMktTypeDes("");
		wsMu0t0004Row.setMut04oMtMktCltId("");
		wsMu0t0004Row.setMut04oMtMktTerNbr("");
		wsMu0t0004Row.setMut04oMtMktDsyNm("");
		wsMu0t0004Row.setMut04oMtMktFstNm("");
		wsMu0t0004Row.setMut04oMtMktMdlNm("");
		wsMu0t0004Row.setMut04oMtMktLstNm("");
		wsMu0t0004Row.setMut04oMtMktSfx("");
		wsMu0t0004Row.setMut04oMtMktPfx("");
		wsMu0t0004Row.setMut04oMtCsrCltId("");
		wsMu0t0004Row.setMut04oMtCsrTerNbr("");
		wsMu0t0004Row.setMut04oMtCsrDsyNm("");
		wsMu0t0004Row.setMut04oMtCsrFstNm("");
		wsMu0t0004Row.setMut04oMtCsrMdlNm("");
		wsMu0t0004Row.setMut04oMtCsrLstNm("");
		wsMu0t0004Row.setMut04oMtCsrSfx("");
		wsMu0t0004Row.setMut04oMtCsrPfx("");
		wsMu0t0004Row.setMut04oMtSmrCltId("");
		wsMu0t0004Row.setMut04oMtSmrTerNbr("");
		wsMu0t0004Row.setMut04oMtSmrDsyNm("");
		wsMu0t0004Row.setMut04oMtSmrFstNm("");
		wsMu0t0004Row.setMut04oMtSmrMdlNm("");
		wsMu0t0004Row.setMut04oMtSmrLstNm("");
		wsMu0t0004Row.setMut04oMtSmrSfx("");
		wsMu0t0004Row.setMut04oMtSmrPfx("");
		wsMu0t0004Row.setMut04oMtDmmCltId("");
		wsMu0t0004Row.setMut04oMtDmmTerNbr("");
		wsMu0t0004Row.setMut04oMtDmmDsyNm("");
		wsMu0t0004Row.setMut04oMtDmmFstNm("");
		wsMu0t0004Row.setMut04oMtDmmMdlNm("");
		wsMu0t0004Row.setMut04oMtDmmLstNm("");
		wsMu0t0004Row.setMut04oMtDmmSfx("");
		wsMu0t0004Row.setMut04oMtDmmPfx("");
		wsMu0t0004Row.setMut04oMtRmmCltId("");
		wsMu0t0004Row.setMut04oMtRmmTerNbr("");
		wsMu0t0004Row.setMut04oMtRmmDsyNm("");
		wsMu0t0004Row.setMut04oMtRmmFstNm("");
		wsMu0t0004Row.setMut04oMtRmmMdlNm("");
		wsMu0t0004Row.setMut04oMtRmmLstNm("");
		wsMu0t0004Row.setMut04oMtRmmSfx("");
		wsMu0t0004Row.setMut04oMtRmmPfx("");
		wsMu0t0004Row.setMut04oMtDfoCltId("");
		wsMu0t0004Row.setMut04oMtDfoTerNbr("");
		wsMu0t0004Row.setMut04oMtDfoDsyNm("");
		wsMu0t0004Row.setMut04oMtDfoFstNm("");
		wsMu0t0004Row.setMut04oMtDfoMdlNm("");
		wsMu0t0004Row.setMut04oMtDfoLstNm("");
		wsMu0t0004Row.setMut04oMtDfoSfx("");
		wsMu0t0004Row.setMut04oMtDfoPfx("");
		wsMu0t0004Row.setMut04oAtBrnCltId("");
		wsMu0t0004Row.setMut04oAtBrnTerNbr("");
		wsMu0t0004Row.setMut04oAtBrnNm("");
		wsMu0t0004Row.setMut04oAtAgcCltId("");
		wsMu0t0004Row.setMut04oAtAgcTerNbr("");
		wsMu0t0004Row.setMut04oAtAgcNm("");
		wsMu0t0004Row.setMut04oAtSmrCltId("");
		wsMu0t0004Row.setMut04oAtSmrTerNbr("");
		wsMu0t0004Row.setMut04oAtSmrNm("");
		wsMu0t0004Row.setMut04oAtAmmCltId("");
		wsMu0t0004Row.setMut04oAtAmmTerNbr("");
		wsMu0t0004Row.setMut04oAtAmmDsyNm("");
		wsMu0t0004Row.setMut04oAtAmmFstNm("");
		wsMu0t0004Row.setMut04oAtAmmMdlNm("");
		wsMu0t0004Row.setMut04oAtAmmLstNm("");
		wsMu0t0004Row.setMut04oAtAmmSfx("");
		wsMu0t0004Row.setMut04oAtAmmPfx("");
		wsMu0t0004Row.setMut04oAtAfmCltId("");
		wsMu0t0004Row.setMut04oAtAfmTerNbr("");
		wsMu0t0004Row.setMut04oAtAfmDsyNm("");
		wsMu0t0004Row.setMut04oAtAfmFstNm("");
		wsMu0t0004Row.setMut04oAtAfmMdlNm("");
		wsMu0t0004Row.setMut04oAtAfmLstNm("");
		wsMu0t0004Row.setMut04oAtAfmSfx("");
		wsMu0t0004Row.setMut04oAtAfmPfx("");
		wsMu0t0004Row.setMut04oAtAvpCltId("");
		wsMu0t0004Row.setMut04oAtAvpTerNbr("");
		wsMu0t0004Row.setMut04oAtAvpDsyNm("");
		wsMu0t0004Row.setMut04oAtAvpFstNm("");
		wsMu0t0004Row.setMut04oAtAvpMdlNm("");
		wsMu0t0004Row.setMut04oAtAvpLstNm("");
		wsMu0t0004Row.setMut04oAtAvpSfx("");
		wsMu0t0004Row.setMut04oAtAvpPfx("");
		wsMu0t0004Row.setMut04oSvcUwDsyNm("");
		wsMu0t0004Row.setMut04oSvcUwTypeCd("");
		wsMu0t0004Row.setMut04oSvcUwTypeDes("");
		wsMu0t0004Row.setMut04oUwCltId("");
		wsMu0t0004Row.setMut04oUwDsyNm("");
		wsMu0t0004Row.setMut04oUwFstNm("");
		wsMu0t0004Row.setMut04oUwMdlNm("");
		wsMu0t0004Row.setMut04oUwLstNm("");
		wsMu0t0004Row.setMut04oUwSfx("");
		wsMu0t0004Row.setMut04oUwPfx("");
		wsMu0t0004Row.setMut04oRskAlsCltId("");
		wsMu0t0004Row.setMut04oRskAlsDsyNm("");
		wsMu0t0004Row.setMut04oRskAlsFstNm("");
		wsMu0t0004Row.setMut04oRskAlsMdlNm("");
		wsMu0t0004Row.setMut04oRskAlsLstNm("");
		wsMu0t0004Row.setMut04oRskAlsSfx("");
		wsMu0t0004Row.setMut04oRskAlsPfx("");
		wsMu0t0004Row.setMut04oDumCltId("");
		wsMu0t0004Row.setMut04oDumDsyNm("");
		wsMu0t0004Row.setMut04oDumTer("");
		wsMu0t0004Row.setMut04oDumFstNm("");
		wsMu0t0004Row.setMut04oDumMdlNm("");
		wsMu0t0004Row.setMut04oDumLstNm("");
		wsMu0t0004Row.setMut04oDumSfx("");
		wsMu0t0004Row.setMut04oDumPfx("");
		wsMu0t0004Row.setMut04oRumCltId("");
		wsMu0t0004Row.setMut04oRumDsyNm("");
		wsMu0t0004Row.setMut04oRumTer("");
		wsMu0t0004Row.setMut04oRumFstNm("");
		wsMu0t0004Row.setMut04oRumMdlNm("");
		wsMu0t0004Row.setMut04oRumLstNm("");
		wsMu0t0004Row.setMut04oRumSfx("");
		wsMu0t0004Row.setMut04oRumPfx("");
		wsMu0t0004Row.setMut04oFpuCltId("");
		wsMu0t0004Row.setMut04oFpuDsyNm("");
		wsMu0t0004Row.setMut04oFpuFstNm("");
		wsMu0t0004Row.setMut04oFpuMdlNm("");
		wsMu0t0004Row.setMut04oFpuLstNm("");
		wsMu0t0004Row.setMut04oFpuSfx("");
		wsMu0t0004Row.setMut04oFpuPfx("");
		wsMu0t0004Row.setMut04oFpuDumCltId("");
		wsMu0t0004Row.setMut04oFpuDumDsyNm("");
		wsMu0t0004Row.setMut04oFpuDumTer("");
		wsMu0t0004Row.setMut04oFpuDumFstNm("");
		wsMu0t0004Row.setMut04oFpuDumMdlNm("");
		wsMu0t0004Row.setMut04oFpuDumLstNm("");
		wsMu0t0004Row.setMut04oFpuDumSfx("");
		wsMu0t0004Row.setMut04oFpuDumPfx("");
		wsMu0t0004Row.setMut04oFpuRumCltId("");
		wsMu0t0004Row.setMut04oFpuRumDsyNm("");
		wsMu0t0004Row.setMut04oFpuRumTer("");
		wsMu0t0004Row.setMut04oFpuRumFstNm("");
		wsMu0t0004Row.setMut04oFpuRumMdlNm("");
		wsMu0t0004Row.setMut04oFpuRumLstNm("");
		wsMu0t0004Row.setMut04oFpuRumSfx("");
		wsMu0t0004Row.setMut04oFpuRumPfx("");
		wsMu0t0004Row.setMut04oLpRskAlsCltId("");
		wsMu0t0004Row.setMut04oLpRskAlsDsyNm("");
		wsMu0t0004Row.setMut04oLpRskAlsFstNm("");
		wsMu0t0004Row.setMut04oLpRskAlsMdlNm("");
		wsMu0t0004Row.setMut04oLpRskAlsLstNm("");
		wsMu0t0004Row.setMut04oLpRskAlsSfx("");
		wsMu0t0004Row.setMut04oLpRskAlsPfx("");
		wsMu0t0004Row.setMut04oLpraDumCltId("");
		wsMu0t0004Row.setMut04oLpraDumDsyNm("");
		wsMu0t0004Row.setMut04oLpraDumTer("");
		wsMu0t0004Row.setMut04oLpraDumFstNm("");
		wsMu0t0004Row.setMut04oLpraDumMdlNm("");
		wsMu0t0004Row.setMut04oLpraDumLstNm("");
		wsMu0t0004Row.setMut04oLpraDumSfx("");
		wsMu0t0004Row.setMut04oLpraDumPfx("");
		wsMu0t0004Row.setMut04oLpraRumCltId("");
		wsMu0t0004Row.setMut04oLpraRumDsyNm("");
		wsMu0t0004Row.setMut04oLpraRumTer("");
		wsMu0t0004Row.setMut04oLpraRumFstNm("");
		wsMu0t0004Row.setMut04oLpraRumMdlNm("");
		wsMu0t0004Row.setMut04oLpraRumLstNm("");
		wsMu0t0004Row.setMut04oLpraRumSfx("");
		wsMu0t0004Row.setMut04oLpraRumPfx("");
		wsMu0t0004Row.setMut04oAlStateCd("");
		wsMu0t0004Row.setMut04oAlCountyCd("");
		wsMu0t0004Row.setMut04oAlTownCd("");
		wsMu0t0004Row.setMut04oNbrEmployees(0);
		for (int idx0 = 1; idx0 <= LServiceContractArea.MUT04O_SE_SIC_MAXOCCURS; idx0++) {
			wsMu0t0004Row.setMut04oSeSicCd(idx0, "");
			wsMu0t0004Row.setMut04oSeSicDes(idx0, "");
		}
		wsMu0t0004Row.setMut04oOlDivision(Types.SPACE_CHAR);
		wsMu0t0004Row.setMut04oOlSubDivision(Types.SPACE_CHAR);
	}

	public void initWsXz0t0006Row() {
		wsXz0t0006Row.setXzt06iTkNotPrcTs("");
		wsXz0t0006Row.setXzt06iTkActOwnCltId("");
		wsXz0t0006Row.setXzt06iTkActOwnAdrId("");
		wsXz0t0006Row.setXzt06iTkEmpId("");
		wsXz0t0006Row.setXzt06iTkStaMdfTs("");
		wsXz0t0006Row.setXzt06iTkActNotStaCd("");
		wsXz0t0006Row.setXzt06iTkSegCd("");
		wsXz0t0006Row.setXzt06iTkActTypCd("");
		wsXz0t0006Row.setXzt06iTkActNotCsumFormatted("000000000");
		wsXz0t0006Row.setXzt06iCsrActNbr("");
		wsXz0t0006Row.setXzt06iActNotTypCd("");
		wsXz0t0006Row.setXzt06iNotDt("");
		wsXz0t0006Row.setXzt06iPdcNbr("");
		wsXz0t0006Row.setXzt06iPdcNm("");
		wsXz0t0006Row.setXzt06iTotFeeAmt(new AfDecimal(0, 10, 2));
		wsXz0t0006Row.setXzt06iStAbb("");
		wsXz0t0006Row.setXzt06iCerHldNotInd(Types.SPACE_CHAR);
		wsXz0t0006Row.setXzt06iAddCncDay(0);
		wsXz0t0006Row.setXzt06iReaDes("");
		wsXz0t0006Row.setXzt06iUserid("");
		wsXz0t0006Row.setXzt06oTkNotPrcTs("");
		wsXz0t0006Row.setXzt06oTkActOwnCltId("");
		wsXz0t0006Row.setXzt06oTkActOwnAdrId("");
		wsXz0t0006Row.setXzt06oTkEmpId("");
		wsXz0t0006Row.setXzt06oTkStaMdfTs("");
		wsXz0t0006Row.setXzt06oTkActNotStaCd("");
		wsXz0t0006Row.setXzt06oTkSegCd("");
		wsXz0t0006Row.setXzt06oTkActTypCd("");
		wsXz0t0006Row.setXzt06oTkActNotCsumFormatted("000000000");
		wsXz0t0006Row.setXzt06oCsrActNbr("");
		wsXz0t0006Row.setXzt06oActNotTypCd("");
		wsXz0t0006Row.setXzt06oActNotTypDesc("");
		wsXz0t0006Row.setXzt06oNotDt("");
		wsXz0t0006Row.setXzt06oPdcNbr("");
		wsXz0t0006Row.setXzt06oPdcNm("");
		wsXz0t0006Row.setXzt06oTotFeeAmt(new AfDecimal(0, 10, 2));
		wsXz0t0006Row.setXzt06oStAbb("");
		wsXz0t0006Row.setXzt06oCerHldNotInd(Types.SPACE_CHAR);
		wsXz0t0006Row.setXzt06oAddCncDay(0);
		wsXz0t0006Row.setXzt06oReaDes("");
	}

	public void initWsXz0t90s0Row() {
		wsXz0t90s0Row.setiActNbr("");
		wsXz0t90s0Row.setiUserid("");
		for (int idx0 = 1; idx0 <= LServiceContractAreaXz0x90s0.O_POLICY_LIST_MAXOCCURS; idx0++) {
			wsXz0t90s0Row.setoTkPolId(idx0, "");
			wsXz0t90s0Row.setoTkRltTypCd(idx0, "");
			wsXz0t90s0Row.setoPolNbr(idx0, "");
			wsXz0t90s0Row.setoPolEffDt(idx0, "");
			wsXz0t90s0Row.setoPolExpDt(idx0, "");
			wsXz0t90s0Row.setoQteNbr(idx0, 0);
			wsXz0t90s0Row.setoActNbr(idx0, "");
			wsXz0t90s0Row.setoActNbrFmt(idx0, "");
			wsXz0t90s0Row.setoPriRskStAbb(idx0, "");
			wsXz0t90s0Row.setoPriRskStDes(idx0, "");
			wsXz0t90s0Row.setoLobCd(idx0, "");
			wsXz0t90s0Row.setoLobDes(idx0, "");
			wsXz0t90s0Row.setoTobCd(idx0, "");
			wsXz0t90s0Row.setoTobDes(idx0, "");
			wsXz0t90s0Row.setoSegCd(idx0, "");
			wsXz0t90s0Row.setoSegDes(idx0, "");
			wsXz0t90s0Row.setoLgeActInd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoMnlPolInd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoCurStaCd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoCurStaDes(idx0, "");
			wsXz0t90s0Row.setoCurTrsTypCd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoCurTrsTypDes(idx0, "");
			wsXz0t90s0Row.setoCurPndTrsInd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoCurOgnCd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoCurOgnDes(idx0, "");
			wsXz0t90s0Row.setoCoverageParts(idx0, "");
			wsXz0t90s0Row.setoBondInd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoPbBranchNbr(idx0, "");
			wsXz0t90s0Row.setoPbBranchDesc(idx0, "");
			wsXz0t90s0Row.setoWrtPremAmt(idx0, 0);
			wsXz0t90s0Row.setoPolActInd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoPndCncInd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoSchCncDt(idx0, "");
			wsXz0t90s0Row.setoNotTypCd(idx0, "");
			wsXz0t90s0Row.setoNotTypDes(idx0, "");
			wsXz0t90s0Row.setoNotEmpId(idx0, "");
			wsXz0t90s0Row.setoNotEmpNm(idx0, "");
			wsXz0t90s0Row.setoNotPrcDt(idx0, "");
			wsXz0t90s0Row.setoTmnInd(idx0, Types.SPACE_CHAR);
			wsXz0t90s0Row.setoTmnEmpId(idx0, "");
			wsXz0t90s0Row.setoTmnEmpNm(idx0, "");
			wsXz0t90s0Row.setoTmnPrcDt(idx0, "");
		}
	}

	public void initWsXz0t90h0Row() {
		wsXz0t90h0Row.setiTkNotPrcTs("");
		wsXz0t90h0Row.setiCsrActNbr("");
		wsXz0t90h0Row.setiUserid("");
		for (int idx0 = 1; idx0 <= LServiceContractAreaXz0x90h0.I_POLICY_LIST_MAXOCCURS; idx0++) {
			wsXz0t90h0Row.setiPolNbr(idx0, "");
			wsXz0t90h0Row.setiPolEffDt(idx0, "");
			wsXz0t90h0Row.setiPolExpDt(idx0, "");
			wsXz0t90h0Row.setiNotEffDt(idx0, "");
			wsXz0t90h0Row.setiPolDueAmt(idx0, new AfDecimal(0, 10, 2));
			wsXz0t90h0Row.setiPolBilStaCd(idx0, Types.SPACE_CHAR);
		}
	}

	public void initIPolicyListTbl() {
		for (int idx0 = 1; idx0 <= LServiceContractAreaXz0x90h0.I_POLICY_LIST_MAXOCCURS; idx0++) {
			wsXz0t90h0Row.setiPolNbr(idx0, "");
			wsXz0t90h0Row.setiPolEffDt(idx0, "");
			wsXz0t90h0Row.setiPolExpDt(idx0, "");
			wsXz0t90h0Row.setiNotEffDt(idx0, "");
			wsXz0t90h0Row.setiPolDueAmt(idx0, new AfDecimal(0, 10, 2));
			wsXz0t90h0Row.setiPolBilStaCd(idx0, Types.SPACE_CHAR);
		}
	}

	public void initWsXz0t90m0Row() {
		wsXz0t90m0Row.setiTkNotPrcTs("");
		wsXz0t90m0Row.setiTkActNotStaCd("");
		wsXz0t90m0Row.setiCsrActNbr("");
		wsXz0t90m0Row.setiUserid("");
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
