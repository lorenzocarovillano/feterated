/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90Q0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90q0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90q0() {
	}

	public LServiceContractAreaXz0x90q0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiCsrActNbr(String iCsrActNbr) {
		writeString(Pos.I_CSR_ACT_NBR, iCsrActNbr, Len.I_CSR_ACT_NBR);
	}

	/**Original name: XZT9QI-CSR-ACT-NBR<br>*/
	public String getiCsrActNbr() {
		return readString(Pos.I_CSR_ACT_NBR, Len.I_CSR_ACT_NBR);
	}

	public void setiUserid(String iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT9QI-USERID<br>*/
	public String getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	public String getiUseridFormatted() {
		return Functions.padBlanks(getiUserid(), Len.I_USERID);
	}

	public void setoTkActOwnCltId(String oTkActOwnCltId) {
		writeString(Pos.O_TK_ACT_OWN_CLT_ID, oTkActOwnCltId, Len.O_TK_ACT_OWN_CLT_ID);
	}

	/**Original name: XZT9QO-TK-ACT-OWN-CLT-ID<br>*/
	public String getoTkActOwnCltId() {
		return readString(Pos.O_TK_ACT_OWN_CLT_ID, Len.O_TK_ACT_OWN_CLT_ID);
	}

	public void setoTkActOwnAdrId(String oTkActOwnAdrId) {
		writeString(Pos.O_TK_ACT_OWN_ADR_ID, oTkActOwnAdrId, Len.O_TK_ACT_OWN_ADR_ID);
	}

	/**Original name: XZT9QO-TK-ACT-OWN-ADR-ID<br>*/
	public String getoTkActOwnAdrId() {
		return readString(Pos.O_TK_ACT_OWN_ADR_ID, Len.O_TK_ACT_OWN_ADR_ID);
	}

	public void setoTkSegCd(String oTkSegCd) {
		writeString(Pos.O_TK_SEG_CD, oTkSegCd, Len.O_TK_SEG_CD);
	}

	/**Original name: XZT9QO-TK-SEG-CD<br>*/
	public String getoTkSegCd() {
		return readString(Pos.O_TK_SEG_CD, Len.O_TK_SEG_CD);
	}

	public void setoTkActTypCd(String oTkActTypCd) {
		writeString(Pos.O_TK_ACT_TYP_CD, oTkActTypCd, Len.O_TK_ACT_TYP_CD);
	}

	/**Original name: XZT9QO-TK-ACT-TYP-CD<br>*/
	public String getoTkActTypCd() {
		return readString(Pos.O_TK_ACT_TYP_CD, Len.O_TK_ACT_TYP_CD);
	}

	public void setoCsrActNbr(String oCsrActNbr) {
		writeString(Pos.O_CSR_ACT_NBR, oCsrActNbr, Len.O_CSR_ACT_NBR);
	}

	/**Original name: XZT9QO-CSR-ACT-NBR<br>*/
	public String getoCsrActNbr() {
		return readString(Pos.O_CSR_ACT_NBR, Len.O_CSR_ACT_NBR);
	}

	public void setoPdcNbr(String oPdcNbr) {
		writeString(Pos.O_PDC_NBR, oPdcNbr, Len.O_PDC_NBR);
	}

	/**Original name: XZT9QO-PDC-NBR<br>*/
	public String getoPdcNbr() {
		return readString(Pos.O_PDC_NBR, Len.O_PDC_NBR);
	}

	public void setoPdcNm(String oPdcNm) {
		writeString(Pos.O_PDC_NM, oPdcNm, Len.O_PDC_NM);
	}

	/**Original name: XZT9QO-PDC-NM<br>*/
	public String getoPdcNm() {
		return readString(Pos.O_PDC_NM, Len.O_PDC_NM);
	}

	public void setoStAbb(String oStAbb) {
		writeString(Pos.O_ST_ABB, oStAbb, Len.O_ST_ABB);
	}

	/**Original name: XZT9QO-ST-ABB<br>*/
	public String getoStAbb() {
		return readString(Pos.O_ST_ABB, Len.O_ST_ABB);
	}

	public void setoLastName(String oLastName) {
		writeString(Pos.O_LAST_NAME, oLastName, Len.O_LAST_NAME);
	}

	/**Original name: XZT9QO-LAST-NAME<br>*/
	public String getoLastName() {
		return readString(Pos.O_LAST_NAME, Len.O_LAST_NAME);
	}

	public void setoNmAdrLin1(String oNmAdrLin1) {
		writeString(Pos.O_NM_ADR_LIN1, oNmAdrLin1, Len.O_NM_ADR_LIN1);
	}

	/**Original name: XZT9QO-NM-ADR-LIN-1<br>*/
	public String getoNmAdrLin1() {
		return readString(Pos.O_NM_ADR_LIN1, Len.O_NM_ADR_LIN1);
	}

	public void setoNmAdrLin2(String oNmAdrLin2) {
		writeString(Pos.O_NM_ADR_LIN2, oNmAdrLin2, Len.O_NM_ADR_LIN2);
	}

	/**Original name: XZT9QO-NM-ADR-LIN-2<br>*/
	public String getoNmAdrLin2() {
		return readString(Pos.O_NM_ADR_LIN2, Len.O_NM_ADR_LIN2);
	}

	public void setoNmAdrLin3(String oNmAdrLin3) {
		writeString(Pos.O_NM_ADR_LIN3, oNmAdrLin3, Len.O_NM_ADR_LIN3);
	}

	/**Original name: XZT9QO-NM-ADR-LIN-3<br>*/
	public String getoNmAdrLin3() {
		return readString(Pos.O_NM_ADR_LIN3, Len.O_NM_ADR_LIN3);
	}

	public void setoNmAdrLin4(String oNmAdrLin4) {
		writeString(Pos.O_NM_ADR_LIN4, oNmAdrLin4, Len.O_NM_ADR_LIN4);
	}

	/**Original name: XZT9QO-NM-ADR-LIN-4<br>*/
	public String getoNmAdrLin4() {
		return readString(Pos.O_NM_ADR_LIN4, Len.O_NM_ADR_LIN4);
	}

	public void setoNmAdrLin5(String oNmAdrLin5) {
		writeString(Pos.O_NM_ADR_LIN5, oNmAdrLin5, Len.O_NM_ADR_LIN5);
	}

	/**Original name: XZT9QO-NM-ADR-LIN-5<br>*/
	public String getoNmAdrLin5() {
		return readString(Pos.O_NM_ADR_LIN5, Len.O_NM_ADR_LIN5);
	}

	public void setoNmAdrLin6(String oNmAdrLin6) {
		writeString(Pos.O_NM_ADR_LIN6, oNmAdrLin6, Len.O_NM_ADR_LIN6);
	}

	/**Original name: XZT9QO-NM-ADR-LIN-6<br>*/
	public String getoNmAdrLin6() {
		return readString(Pos.O_NM_ADR_LIN6, Len.O_NM_ADR_LIN6);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT9Q0_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_CSR_ACT_NBR = XZT9Q0_SERVICE_INPUTS;
		public static final int I_USERID = I_CSR_ACT_NBR + Len.I_CSR_ACT_NBR;
		public static final int XZT9Q0_SERVICE_OUTPUTS = I_USERID + Len.I_USERID;
		public static final int O_TECHNICAL_KEY = XZT9Q0_SERVICE_OUTPUTS;
		public static final int O_TK_ACT_OWN_CLT_ID = O_TECHNICAL_KEY;
		public static final int O_TK_ACT_OWN_ADR_ID = O_TK_ACT_OWN_CLT_ID + Len.O_TK_ACT_OWN_CLT_ID;
		public static final int O_TK_SEG_CD = O_TK_ACT_OWN_ADR_ID + Len.O_TK_ACT_OWN_ADR_ID;
		public static final int O_TK_ACT_TYP_CD = O_TK_SEG_CD + Len.O_TK_SEG_CD;
		public static final int O_CSR_ACT_NBR = O_TK_ACT_TYP_CD + Len.O_TK_ACT_TYP_CD;
		public static final int O_PDC = O_CSR_ACT_NBR + Len.O_CSR_ACT_NBR;
		public static final int O_PDC_NBR = O_PDC;
		public static final int O_PDC_NM = O_PDC_NBR + Len.O_PDC_NBR;
		public static final int O_ST_ABB = O_PDC_NM + Len.O_PDC_NM;
		public static final int O_LAST_NAME = O_ST_ABB + Len.O_ST_ABB;
		public static final int O_NM_ADR_LIN1 = O_LAST_NAME + Len.O_LAST_NAME;
		public static final int O_NM_ADR_LIN2 = O_NM_ADR_LIN1 + Len.O_NM_ADR_LIN1;
		public static final int O_NM_ADR_LIN3 = O_NM_ADR_LIN2 + Len.O_NM_ADR_LIN2;
		public static final int O_NM_ADR_LIN4 = O_NM_ADR_LIN3 + Len.O_NM_ADR_LIN3;
		public static final int O_NM_ADR_LIN5 = O_NM_ADR_LIN4 + Len.O_NM_ADR_LIN4;
		public static final int O_NM_ADR_LIN6 = O_NM_ADR_LIN5 + Len.O_NM_ADR_LIN5;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_CSR_ACT_NBR = 9;
		public static final int I_USERID = 8;
		public static final int O_TK_ACT_OWN_CLT_ID = 64;
		public static final int O_TK_ACT_OWN_ADR_ID = 64;
		public static final int O_TK_SEG_CD = 3;
		public static final int O_TK_ACT_TYP_CD = 2;
		public static final int O_CSR_ACT_NBR = 9;
		public static final int O_PDC_NBR = 5;
		public static final int O_PDC_NM = 120;
		public static final int O_ST_ABB = 2;
		public static final int O_LAST_NAME = 60;
		public static final int O_NM_ADR_LIN1 = 45;
		public static final int O_NM_ADR_LIN2 = 45;
		public static final int O_NM_ADR_LIN3 = 45;
		public static final int O_NM_ADR_LIN4 = 45;
		public static final int O_NM_ADR_LIN5 = 45;
		public static final int XZT9Q0_SERVICE_INPUTS = I_CSR_ACT_NBR + I_USERID;
		public static final int O_TECHNICAL_KEY = O_TK_ACT_OWN_CLT_ID + O_TK_ACT_OWN_ADR_ID + O_TK_SEG_CD + O_TK_ACT_TYP_CD;
		public static final int O_PDC = O_PDC_NBR + O_PDC_NM;
		public static final int O_NM_ADR_LIN6 = 45;
		public static final int XZT9Q0_SERVICE_OUTPUTS = O_TECHNICAL_KEY + O_CSR_ACT_NBR + O_PDC + O_ST_ABB + O_LAST_NAME + O_NM_ADR_LIN1
				+ O_NM_ADR_LIN2 + O_NM_ADR_LIN3 + O_NM_ADR_LIN4 + O_NM_ADR_LIN5 + O_NM_ADR_LIN6;
		public static final int L_SERVICE_CONTRACT_AREA = XZT9Q0_SERVICE_INPUTS + XZT9Q0_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
