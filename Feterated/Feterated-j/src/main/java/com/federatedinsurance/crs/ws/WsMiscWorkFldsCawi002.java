/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-MISC-WORK-FLDS<br>
 * Variable: WS-MISC-WORK-FLDS from program CAWI002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsMiscWorkFldsCawi002 {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "CAWI002";
	//Original name: WS-BUS-OBJ-NAME
	private String busObjName = "CLIENT_TAB_V";
	//Original name: FILLER-WS-PROGRAM-LINK-FAILED
	private String flr1 = "CALL TO";
	//Original name: WS-FAILED-LINK-PGM-NAME
	private String failedLinkPgmName = DefaultValues.stringVal(Len.FAILED_LINK_PGM_NAME);
	//Original name: FILLER-WS-PROGRAM-LINK-FAILED-1
	private String flr2 = " FAILED.";

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getBusObjName() {
		return this.busObjName;
	}

	public String getProgramLinkFailedFormatted() {
		return MarshalByteExt.bufferToStr(getProgramLinkFailedBytes());
	}

	/**Original name: WS-PROGRAM-LINK-FAILED<br>*/
	public byte[] getProgramLinkFailedBytes() {
		byte[] buffer = new byte[Len.PROGRAM_LINK_FAILED];
		return getProgramLinkFailedBytes(buffer, 1);
	}

	public byte[] getProgramLinkFailedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, failedLinkPgmName, Len.FAILED_LINK_PGM_NAME);
		position += Len.FAILED_LINK_PGM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setFailedLinkPgmName(String failedLinkPgmName) {
		this.failedLinkPgmName = Functions.subString(failedLinkPgmName, Len.FAILED_LINK_PGM_NAME);
	}

	public String getFailedLinkPgmName() {
		return this.failedLinkPgmName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FAILED_LINK_PGM_NAME = 8;
		public static final int FLR1 = 8;
		public static final int PROGRAM_LINK_FAILED = FAILED_LINK_PGM_NAME + 2 * FLR1;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
