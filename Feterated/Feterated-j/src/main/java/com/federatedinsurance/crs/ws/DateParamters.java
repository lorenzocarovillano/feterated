/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.federatedinsurance.crs.ws.enums.DpDayOfWeek;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DATE-PARAMTERS<br>
 * Variable: DATE-PARAMTERS from program XZ0P0021<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DateParamters extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: DP-DC-LEN
	private short dpDcLen = ((short) 10);
	//Original name: DP-DC-DATE
	private String dpDcDate = DefaultValues.stringVal(Len.DP_DC_DATE);
	//Original name: DP-DATE-PICTURE
	private DpDatePicture dpDatePicture = new DpDatePicture();
	//Original name: DP-LILIAN-DATE
	private int dpLilianDate = DefaultValues.BIN_INT_VAL;
	//Original name: DP-DAY-OF-WEEK
	private DpDayOfWeek dpDayOfWeek = new DpDayOfWeek();
	//Original name: DP-FEEDBACK-CODE
	private String dpFeedbackCode = DefaultValues.stringVal(Len.DP_FEEDBACK_CODE);
	public static final String DB_VALID_DATE = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.DP_FEEDBACK_CODE);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DATE_PARAMTERS;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDateParamtersBytes(buf);
	}

	public String getDateParamtersFormatted() {
		return MarshalByteExt.bufferToStr(getDateParamtersBytes());
	}

	public void setDateParamtersBytes(byte[] buffer) {
		setDateParamtersBytes(buffer, 1);
	}

	public byte[] getDateParamtersBytes() {
		byte[] buffer = new byte[Len.DATE_PARAMTERS];
		return getDateParamtersBytes(buffer, 1);
	}

	public void setDateParamtersBytes(byte[] buffer, int offset) {
		int position = offset;
		setDpDateToConvertBytes(buffer, position);
		position += Len.DP_DATE_TO_CONVERT;
		dpDatePicture.setDpDatePictureBytes(buffer, position);
		position += DpDatePicture.Len.DP_DATE_PICTURE;
		dpLilianDate = MarshalByte.readBinaryInt(buffer, position);
		position += Types.INT_SIZE;
		dpDayOfWeek.setDpDayOfWeek(MarshalByte.readBinaryInt(buffer, position));
		position += Types.INT_SIZE;
		dpFeedbackCode = MarshalByte.readString(buffer, position, Len.DP_FEEDBACK_CODE);
	}

	public byte[] getDateParamtersBytes(byte[] buffer, int offset) {
		int position = offset;
		getDpDateToConvertBytes(buffer, position);
		position += Len.DP_DATE_TO_CONVERT;
		dpDatePicture.getDpDatePictureBytes(buffer, position);
		position += DpDatePicture.Len.DP_DATE_PICTURE;
		MarshalByte.writeBinaryInt(buffer, position, dpLilianDate);
		position += Types.INT_SIZE;
		MarshalByte.writeBinaryInt(buffer, position, dpDayOfWeek.getDpDayOfWeek());
		position += Types.INT_SIZE;
		MarshalByte.writeString(buffer, position, dpFeedbackCode, Len.DP_FEEDBACK_CODE);
		return buffer;
	}

	public String getDpDateToConvertFormatted() {
		byte[] buffer = new byte[Len.DP_DATE_TO_CONVERT];
		return MarshalByteExt.bufferToStr(getDpDateToConvertBytes(buffer, 1));
	}

	public void setDpDateToConvertBytes(byte[] buffer, int offset) {
		int position = offset;
		dpDcLen = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		dpDcDate = MarshalByte.readString(buffer, position, Len.DP_DC_DATE);
	}

	public byte[] getDpDateToConvertBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, dpDcLen);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, dpDcDate, Len.DP_DC_DATE);
		return buffer;
	}

	public void setDpDcLen(short dpDcLen) {
		this.dpDcLen = dpDcLen;
	}

	public short getDpDcLen() {
		return this.dpDcLen;
	}

	public void setDpDcDate(String dpDcDate) {
		this.dpDcDate = Functions.subString(dpDcDate, Len.DP_DC_DATE);
	}

	public String getDpDcDate() {
		return this.dpDcDate;
	}

	public void setDpLilianDate(int dpLilianDate) {
		this.dpLilianDate = dpLilianDate;
	}

	public void setDpLilianDateFromBuffer(byte[] buffer) {
		dpLilianDate = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getDpLilianDate() {
		return this.dpLilianDate;
	}

	public String getDpLilianDateFormatted() {
		return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.BINARY)).format(getDpLilianDate()).toString();
	}

	public void setDpFeedbackCode(String dpFeedbackCode) {
		this.dpFeedbackCode = Functions.subString(dpFeedbackCode, Len.DP_FEEDBACK_CODE);
	}

	public String getDpFeedbackCode() {
		return this.dpFeedbackCode;
	}

	public String getDpFeedbackCodeFormatted() {
		return Functions.padBlanks(getDpFeedbackCode(), Len.DP_FEEDBACK_CODE);
	}

	public boolean isDbValidDate() {
		return dpFeedbackCode.equals(DB_VALID_DATE);
	}

	public DpDatePicture getDpDatePicture() {
		return dpDatePicture;
	}

	public DpDayOfWeek getDpDayOfWeek() {
		return dpDayOfWeek;
	}

	@Override
	public byte[] serialize() {
		return getDateParamtersBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DP_DC_LEN = 2;
		public static final int DP_DC_DATE = 10;
		public static final int DP_DATE_TO_CONVERT = DP_DC_LEN + DP_DC_DATE;
		public static final int DP_LILIAN_DATE = 4;
		public static final int DP_FEEDBACK_CODE = 12;
		public static final int DATE_PARAMTERS = DP_DATE_TO_CONVERT + DpDatePicture.Len.DP_DATE_PICTURE + DP_LILIAN_DATE
				+ DpDayOfWeek.Len.DP_DAY_OF_WEEK + DP_FEEDBACK_CODE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
