/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: SA-SEG-CD<br>
 * Variable: SA-SEG-CD from program XZ0P90E0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaSegCd {

	//==== PROPERTIES ====
	private String value = "";
	public static final String SPACES = "";
	public static final String SELECT = "010";
	public static final String SELECT_EXPRESS = "020";
	public static final String LARGE_ACCOUNT = "080";

	//==== METHODS ====
	public void setSegCd(String segCd) {
		this.value = Functions.subString(segCd, Len.SEG_CD);
	}

	public String getSegCd() {
		return this.value;
	}

	public boolean isSelect() {
		return value.equals(SELECT);
	}

	public boolean isSelectExpress() {
		return value.equals(SELECT_EXPRESS);
	}

	public boolean isLargeAccount() {
		return value.equals(LARGE_ACCOUNT);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SEG_CD = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
