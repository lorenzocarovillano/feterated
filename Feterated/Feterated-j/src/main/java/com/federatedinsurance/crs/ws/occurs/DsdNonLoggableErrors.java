/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: DSD-NON-LOGGABLE-ERRORS<br>
 * Variables: DSD-NON-LOGGABLE-ERRORS from copybook TS020DRV<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class DsdNonLoggableErrors {

	//==== PROPERTIES ====
	//Original name: DSD-NON-LOG-ERR-MSG
	private String dsdNonLogErrMsg = DefaultValues.stringVal(Len.DSD_NON_LOG_ERR_MSG);

	//==== METHODS ====
	public void setNonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		dsdNonLogErrMsg = MarshalByte.readString(buffer, position, Len.DSD_NON_LOG_ERR_MSG);
	}

	public byte[] getNonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, dsdNonLogErrMsg, Len.DSD_NON_LOG_ERR_MSG);
		return buffer;
	}

	public void initNonLoggableErrorsSpaces() {
		dsdNonLogErrMsg = "";
	}

	public void setDsdNonLogErrMsg(String dsdNonLogErrMsg) {
		this.dsdNonLogErrMsg = Functions.subString(dsdNonLogErrMsg, Len.DSD_NON_LOG_ERR_MSG);
	}

	public String getDsdNonLogErrMsg() {
		return this.dsdNonLogErrMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DSD_NON_LOG_ERR_MSG = 500;
		public static final int NON_LOGGABLE_ERRORS = DSD_NON_LOG_ERR_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
