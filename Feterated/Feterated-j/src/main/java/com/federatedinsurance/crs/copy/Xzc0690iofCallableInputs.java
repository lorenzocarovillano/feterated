/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC0690I<br>
 * Variable: XZC0690I from copybook XZC0690I<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc0690iofCallableInputs {

	//==== PROPERTIES ====
	//Original name: PII-URI
	private String uri = DefaultValues.stringVal(Len.URI);
	//Original name: PII-CALLABLE-USERID
	private String callableUserid = DefaultValues.stringVal(Len.CALLABLE_USERID);
	//Original name: PII-CALLABLE-PASSWORD
	private String callablePassword = DefaultValues.stringVal(Len.CALLABLE_PASSWORD);
	//Original name: PII-FED-TAR-SYS
	private String fedTarSys = DefaultValues.stringVal(Len.FED_TAR_SYS);
	//Original name: PII-ACT-NBR
	private String actNbr = DefaultValues.stringVal(Len.ACT_NBR);
	//Original name: PII-AS-OF-DT
	private String asOfDt = DefaultValues.stringVal(Len.AS_OF_DT);

	//==== METHODS ====
	public void setGetPolTrmLisByActBytes(byte[] buffer, int offset) {
		int position = offset;
		uri = MarshalByte.readString(buffer, position, Len.URI);
		position += Len.URI;
		callableUserid = MarshalByte.readString(buffer, position, Len.CALLABLE_USERID);
		position += Len.CALLABLE_USERID;
		callablePassword = MarshalByte.readString(buffer, position, Len.CALLABLE_PASSWORD);
		position += Len.CALLABLE_PASSWORD;
		fedTarSys = MarshalByte.readString(buffer, position, Len.FED_TAR_SYS);
		position += Len.FED_TAR_SYS;
		actNbr = MarshalByte.readString(buffer, position, Len.ACT_NBR);
		position += Len.ACT_NBR;
		asOfDt = MarshalByte.readString(buffer, position, Len.AS_OF_DT);
	}

	public byte[] getGetPolTrmLisByActBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, uri, Len.URI);
		position += Len.URI;
		MarshalByte.writeString(buffer, position, callableUserid, Len.CALLABLE_USERID);
		position += Len.CALLABLE_USERID;
		MarshalByte.writeString(buffer, position, callablePassword, Len.CALLABLE_PASSWORD);
		position += Len.CALLABLE_PASSWORD;
		MarshalByte.writeString(buffer, position, fedTarSys, Len.FED_TAR_SYS);
		position += Len.FED_TAR_SYS;
		MarshalByte.writeString(buffer, position, actNbr, Len.ACT_NBR);
		position += Len.ACT_NBR;
		MarshalByte.writeString(buffer, position, asOfDt, Len.AS_OF_DT);
		return buffer;
	}

	public void setUri(String uri) {
		this.uri = Functions.subString(uri, Len.URI);
	}

	public String getUri() {
		return this.uri;
	}

	public void setCallableUserid(String callableUserid) {
		this.callableUserid = Functions.subString(callableUserid, Len.CALLABLE_USERID);
	}

	public String getCallableUserid() {
		return this.callableUserid;
	}

	public void setCallablePassword(String callablePassword) {
		this.callablePassword = Functions.subString(callablePassword, Len.CALLABLE_PASSWORD);
	}

	public String getCallablePassword() {
		return this.callablePassword;
	}

	public void setFedTarSys(String fedTarSys) {
		this.fedTarSys = Functions.subString(fedTarSys, Len.FED_TAR_SYS);
	}

	public String getFedTarSys() {
		return this.fedTarSys;
	}

	public void setActNbr(String actNbr) {
		this.actNbr = Functions.subString(actNbr, Len.ACT_NBR);
	}

	public String getActNbr() {
		return this.actNbr;
	}

	public String getActNbrFormatted() {
		return Functions.padBlanks(getActNbr(), Len.ACT_NBR);
	}

	public void setAsOfDt(String asOfDt) {
		this.asOfDt = Functions.subString(asOfDt, Len.AS_OF_DT);
	}

	public String getAsOfDt() {
		return this.asOfDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int URI = 256;
		public static final int CALLABLE_USERID = 8;
		public static final int CALLABLE_PASSWORD = 8;
		public static final int FED_TAR_SYS = 5;
		public static final int ACT_NBR = 9;
		public static final int AS_OF_DT = 10;
		public static final int GET_POL_TRM_LIS_BY_ACT = URI + CALLABLE_USERID + CALLABLE_PASSWORD + FED_TAR_SYS + ACT_NBR + AS_OF_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
