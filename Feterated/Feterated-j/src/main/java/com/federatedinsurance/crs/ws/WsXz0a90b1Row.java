/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90B1-ROW<br>
 * Variable: WS-XZ0A90B1-ROW from program XZ0B90B0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90b1Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9B1-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZA9B1-NBR-OF-NOT-DAY
	private int nbrOfNotDay = DefaultValues.INT_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90B1_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90b1RowBytes(buf);
	}

	public String getWsXz0a90b1RowFormatted() {
		return getGetNotDaysRqrDtlFormatted();
	}

	public void setWsXz0a90b1RowBytes(byte[] buffer) {
		setWsXz0a90b1RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90b1RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90B1_ROW];
		return getWsXz0a90b1RowBytes(buffer, 1);
	}

	public void setWsXz0a90b1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetNotDaysRqrDtlBytes(buffer, position);
	}

	public byte[] getWsXz0a90b1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetNotDaysRqrDtlBytes(buffer, position);
		return buffer;
	}

	public String getGetNotDaysRqrDtlFormatted() {
		return MarshalByteExt.bufferToStr(getGetNotDaysRqrDtlBytes());
	}

	/**Original name: XZA9B1-GET-NOT-DAYS-RQR-DTL<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0A90B1 - DETAIL BPO COPYBOOK FOR                             *
	 *             UOW : XZ_GET_NOT_DAYS_RQR_LIST                      *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 02/06/2009  E404GCL   NEW                              *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getGetNotDaysRqrDtlBytes() {
		byte[] buffer = new byte[Len.GET_NOT_DAYS_RQR_DTL];
		return getGetNotDaysRqrDtlBytes(buffer, 1);
	}

	public void setGetNotDaysRqrDtlBytes(byte[] buffer, int offset) {
		int position = offset;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		nbrOfNotDay = MarshalByte.readInt(buffer, position, Len.NBR_OF_NOT_DAY);
	}

	public byte[] getGetNotDaysRqrDtlBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeInt(buffer, position, nbrOfNotDay, Len.NBR_OF_NOT_DAY);
		return buffer;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setNbrOfNotDay(int nbrOfNotDay) {
		this.nbrOfNotDay = nbrOfNotDay;
	}

	public int getNbrOfNotDay() {
		return this.nbrOfNotDay;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90b1RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int NBR_OF_NOT_DAY = 5;
		public static final int GET_NOT_DAYS_RQR_DTL = POL_NBR + NBR_OF_NOT_DAY;
		public static final int WS_XZ0A90B1_ROW = GET_NOT_DAYS_RQR_DTL;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
