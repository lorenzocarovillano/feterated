/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-CONTEXT-REDEF<br>
 * Variable: WS-CONTEXT-REDEF from program CAWS002<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsContextRedef {

	//==== PROPERTIES ====
	//Original name: WS-CONTEXT-FIRST-10
	private String contextFirst10 = DefaultValues.stringVal(Len.CONTEXT_FIRST10);
	//Original name: WS-MODE-SECOND-10
	private String modeSecond10 = DefaultValues.stringVal(Len.MODE_SECOND10);

	//==== METHODS ====
	public void setWsFullContext(String wsFullContext) {
		int position = 1;
		byte[] buffer = getWsContextRedefBytes();
		MarshalByte.writeString(buffer, position, wsFullContext, Len.WS_FULL_CONTEXT);
		setWsContextRedefBytes(buffer);
	}

	/**Original name: WS-FULL-CONTEXT<br>*/
	public String getWsFullContext() {
		int position = 1;
		return MarshalByte.readString(getWsContextRedefBytes(), position, Len.WS_FULL_CONTEXT);
	}

	public void setWsContextRedefBytes(byte[] buffer) {
		setWsContextRedefBytes(buffer, 1);
	}

	/**Original name: WS-CONTEXT-REDEF<br>*/
	public byte[] getWsContextRedefBytes() {
		byte[] buffer = new byte[Len.WS_CONTEXT_REDEF];
		return getWsContextRedefBytes(buffer, 1);
	}

	public void setWsContextRedefBytes(byte[] buffer, int offset) {
		int position = offset;
		contextFirst10 = MarshalByte.readString(buffer, position, Len.CONTEXT_FIRST10);
		position += Len.CONTEXT_FIRST10;
		modeSecond10 = MarshalByte.readString(buffer, position, Len.MODE_SECOND10);
	}

	public byte[] getWsContextRedefBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, contextFirst10, Len.CONTEXT_FIRST10);
		position += Len.CONTEXT_FIRST10;
		MarshalByte.writeString(buffer, position, modeSecond10, Len.MODE_SECOND10);
		return buffer;
	}

	public void setContextFirst10(String contextFirst10) {
		this.contextFirst10 = Functions.subString(contextFirst10, Len.CONTEXT_FIRST10);
	}

	public String getContextFirst10() {
		return this.contextFirst10;
	}

	public void setModeSecond10(String modeSecond10) {
		this.modeSecond10 = Functions.subString(modeSecond10, Len.MODE_SECOND10);
	}

	public String getModeSecond10() {
		return this.modeSecond10;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CONTEXT_FIRST10 = 10;
		public static final int MODE_SECOND10 = 10;
		public static final int WS_CONTEXT_REDEF = CONTEXT_FIRST10 + MODE_SECOND10;
		public static final int WS_FULL_CONTEXT = 20;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_FULL_CONTEXT = 20;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
