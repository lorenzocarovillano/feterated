/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-04-SUCCESSFUL-END<br>
 * Variable: EA-04-SUCCESSFUL-END from program XZ400000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea04SuccessfulEnd {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-04-SUCCESSFUL-END
	private String flr1 = "XZ400000 -";
	//Original name: FILLER-EA-04-SUCCESSFUL-END-1
	private String flr2 = "SUCCESSFUL END";
	//Original name: FILLER-EA-04-SUCCESSFUL-END-2
	private String flr3 = "OF PROGRAM";

	//==== METHODS ====
	public String getEa04SuccessfulEndFormatted() {
		return MarshalByteExt.bufferToStr(getEa04SuccessfulEndBytes());
	}

	public byte[] getEa04SuccessfulEndBytes() {
		byte[] buffer = new byte[Len.EA04_SUCCESSFUL_END];
		return getEa04SuccessfulEndBytes(buffer, 1);
	}

	public byte[] getEa04SuccessfulEndBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 15;
		public static final int FLR3 = 10;
		public static final int EA04_SUCCESSFUL_END = FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
