/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: SA-DB2-ACCESS-PROGRAM<br>
 * Variable: SA-DB2-ACCESS-PROGRAM from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaDb2AccessProgram {

	//==== PROPERTIES ====
	private String value = "TS030199";
	public static final String TS030199 = "TS030199";
	public static final String TS030299 = "TS030299";

	//==== METHODS ====
	public void setDb2AccessProgram(String db2AccessProgram) {
		this.value = Functions.subString(db2AccessProgram, Len.DB2_ACCESS_PROGRAM);
	}

	public String getDb2AccessProgram() {
		return this.value;
	}

	public void setTs030199() {
		value = TS030199;
	}

	public void setTs030299() {
		value = TS030299;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DB2_ACCESS_PROGRAM = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
