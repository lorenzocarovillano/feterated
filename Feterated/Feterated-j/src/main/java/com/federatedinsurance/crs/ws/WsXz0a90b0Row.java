/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90B0-ROW<br>
 * Variable: WS-XZ0A90B0-ROW from program XZ0B90B0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90b0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9B0-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA9B0-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA9B0-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZA9B0-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90B0_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90b0RowBytes(buf);
	}

	public String getWsXz0a90b0RowFormatted() {
		return getGetNotDaysRqrHdrFormatted();
	}

	public void setWsXz0a90b0RowBytes(byte[] buffer) {
		setWsXz0a90b0RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90b0RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90B0_ROW];
		return getWsXz0a90b0RowBytes(buffer, 1);
	}

	public void setWsXz0a90b0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetNotDaysRqrHdrBytes(buffer, position);
	}

	public byte[] getWsXz0a90b0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetNotDaysRqrHdrBytes(buffer, position);
		return buffer;
	}

	public String getGetNotDaysRqrHdrFormatted() {
		return MarshalByteExt.bufferToStr(getGetNotDaysRqrHdrBytes());
	}

	/**Original name: XZA9B0-GET-NOT-DAYS-RQR-HDR<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0A90B0 - HEADER BPO COPYBOOK FOR                             *
	 *             UOW : XZ_GET_NOT_DAYS_RQR_LIST                      *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 02/06/2009  E404GCL   NEW                              *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getGetNotDaysRqrHdrBytes() {
		byte[] buffer = new byte[Len.GET_NOT_DAYS_RQR_HDR];
		return getGetNotDaysRqrHdrBytes(buffer, 1);
	}

	public void setGetNotDaysRqrHdrBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		actNotTypCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
	}

	public byte[] getGetNotDaysRqrHdrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getActNotTypCdFormatted() {
		return Functions.padBlanks(getActNotTypCd(), Len.ACT_NOT_TYP_CD);
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90b0RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int USERID = 8;
		public static final int GET_NOT_DAYS_RQR_HDR = CSR_ACT_NBR + NOT_PRC_TS + ACT_NOT_TYP_CD + USERID;
		public static final int WS_XZ0A90B0_ROW = GET_NOT_DAYS_RQR_HDR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
