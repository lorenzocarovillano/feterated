/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-06-CICS-PARM-MSG<br>
 * Variable: EA-06-CICS-PARM-MSG from program XZ003000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea06CicsParmMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-06-CICS-PARM-MSG
	private String flr1 = "XZ003000";
	//Original name: FILLER-EA-06-CICS-PARM-MSG-1
	private String flr2 = "CICS TO USE";
	//Original name: FILLER-EA-06-CICS-PARM-MSG-2
	private String flr3 = "PARAMETER:";
	//Original name: EA-06-CICS-PARM
	private String ea06CicsParm = DefaultValues.stringVal(Len.EA06_CICS_PARM);

	//==== METHODS ====
	public String getEa06CicsParmMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa06CicsParmMsgBytes());
	}

	public byte[] getEa06CicsParmMsgBytes() {
		byte[] buffer = new byte[Len.EA06_CICS_PARM_MSG];
		return getEa06CicsParmMsgBytes(buffer, 1);
	}

	public byte[] getEa06CicsParmMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea06CicsParm, Len.EA06_CICS_PARM);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setEa06CicsParm(String ea06CicsParm) {
		this.ea06CicsParm = Functions.subString(ea06CicsParm, Len.EA06_CICS_PARM);
	}

	public String getEa06CicsParm() {
		return this.ea06CicsParm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA06_CICS_PARM = 8;
		public static final int FLR1 = 9;
		public static final int FLR2 = 12;
		public static final int FLR3 = 11;
		public static final int EA06_CICS_PARM_MSG = EA06_CICS_PARM + FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
