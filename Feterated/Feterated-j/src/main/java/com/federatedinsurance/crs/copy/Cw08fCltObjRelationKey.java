/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CW08F-CLT-OBJ-RELATION-KEY<br>
 * Variable: CW08F-CLT-OBJ-RELATION-KEY from copybook CAWLF008<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw08fCltObjRelationKey {

	//==== PROPERTIES ====
	//Original name: CW08F-TCH-OBJECT-KEY-CI
	private char tchObjectKeyCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-TCH-OBJECT-KEY
	private String tchObjectKey = DefaultValues.stringVal(Len.TCH_OBJECT_KEY);
	//Original name: CW08F-HISTORY-VLD-NBR-CI
	private char historyVldNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-HISTORY-VLD-NBR-SIGN
	private char historyVldNbrSign = DefaultValues.CHAR_VAL;
	//Original name: CW08F-HISTORY-VLD-NBR
	private String historyVldNbr = DefaultValues.stringVal(Len.HISTORY_VLD_NBR);
	//Original name: CW08F-CIOR-EFF-DT-CI
	private char ciorEffDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-CIOR-EFF-DT
	private String ciorEffDt = DefaultValues.stringVal(Len.CIOR_EFF_DT);
	//Original name: CW08F-OBJ-SYS-ID-CI
	private char objSysIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-OBJ-SYS-ID
	private String objSysId = DefaultValues.stringVal(Len.OBJ_SYS_ID);
	//Original name: CW08F-CIOR-OBJ-SEQ-NBR-CI
	private char ciorObjSeqNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW08F-CIOR-OBJ-SEQ-NBR-SIGN
	private char ciorObjSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: CW08F-CIOR-OBJ-SEQ-NBR
	private String ciorObjSeqNbr = DefaultValues.stringVal(Len.CIOR_OBJ_SEQ_NBR);

	//==== METHODS ====
	public void setCltObjRelationKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		tchObjectKeyCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tchObjectKey = MarshalByte.readString(buffer, position, Len.TCH_OBJECT_KEY);
		position += Len.TCH_OBJECT_KEY;
		historyVldNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbr = MarshalByte.readFixedString(buffer, position, Len.HISTORY_VLD_NBR);
		position += Len.HISTORY_VLD_NBR;
		ciorEffDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorEffDt = MarshalByte.readString(buffer, position, Len.CIOR_EFF_DT);
		position += Len.CIOR_EFF_DT;
		objSysIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		objSysId = MarshalByte.readString(buffer, position, Len.OBJ_SYS_ID);
		position += Len.OBJ_SYS_ID;
		ciorObjSeqNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorObjSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciorObjSeqNbr = MarshalByte.readFixedString(buffer, position, Len.CIOR_OBJ_SEQ_NBR);
	}

	public byte[] getCltObjRelationKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, tchObjectKeyCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tchObjectKey, Len.TCH_OBJECT_KEY);
		position += Len.TCH_OBJECT_KEY;
		MarshalByte.writeChar(buffer, position, historyVldNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, historyVldNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, historyVldNbr, Len.HISTORY_VLD_NBR);
		position += Len.HISTORY_VLD_NBR;
		MarshalByte.writeChar(buffer, position, ciorEffDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorEffDt, Len.CIOR_EFF_DT);
		position += Len.CIOR_EFF_DT;
		MarshalByte.writeChar(buffer, position, objSysIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, objSysId, Len.OBJ_SYS_ID);
		position += Len.OBJ_SYS_ID;
		MarshalByte.writeChar(buffer, position, ciorObjSeqNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ciorObjSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciorObjSeqNbr, Len.CIOR_OBJ_SEQ_NBR);
		return buffer;
	}

	public void initCltObjRelationKeySpaces() {
		tchObjectKeyCi = Types.SPACE_CHAR;
		tchObjectKey = "";
		historyVldNbrCi = Types.SPACE_CHAR;
		historyVldNbrSign = Types.SPACE_CHAR;
		historyVldNbr = "";
		ciorEffDtCi = Types.SPACE_CHAR;
		ciorEffDt = "";
		objSysIdCi = Types.SPACE_CHAR;
		objSysId = "";
		ciorObjSeqNbrCi = Types.SPACE_CHAR;
		ciorObjSeqNbrSign = Types.SPACE_CHAR;
		ciorObjSeqNbr = "";
	}

	public void setTchObjectKeyCi(char tchObjectKeyCi) {
		this.tchObjectKeyCi = tchObjectKeyCi;
	}

	public char getTchObjectKeyCi() {
		return this.tchObjectKeyCi;
	}

	public void setTchObjectKey(String tchObjectKey) {
		this.tchObjectKey = Functions.subString(tchObjectKey, Len.TCH_OBJECT_KEY);
	}

	public String getTchObjectKey() {
		return this.tchObjectKey;
	}

	public void setHistoryVldNbrCi(char historyVldNbrCi) {
		this.historyVldNbrCi = historyVldNbrCi;
	}

	public char getHistoryVldNbrCi() {
		return this.historyVldNbrCi;
	}

	public void setHistoryVldNbrSign(char historyVldNbrSign) {
		this.historyVldNbrSign = historyVldNbrSign;
	}

	public void setHistoryVldNbrSignFormatted(String historyVldNbrSign) {
		setHistoryVldNbrSign(Functions.charAt(historyVldNbrSign, Types.CHAR_SIZE));
	}

	public char getHistoryVldNbrSign() {
		return this.historyVldNbrSign;
	}

	public void setHistoryVldNbr(int historyVldNbr) {
		this.historyVldNbr = NumericDisplay.asString(historyVldNbr, Len.HISTORY_VLD_NBR);
	}

	public void setHistoryVldNbrFormatted(String historyVldNbr) {
		this.historyVldNbr = Trunc.toUnsignedNumeric(historyVldNbr, Len.HISTORY_VLD_NBR);
	}

	public int getHistoryVldNbr() {
		return NumericDisplay.asInt(this.historyVldNbr);
	}

	public String getHistoryVldNbrFormatted() {
		return this.historyVldNbr;
	}

	public void setCiorEffDtCi(char ciorEffDtCi) {
		this.ciorEffDtCi = ciorEffDtCi;
	}

	public char getCiorEffDtCi() {
		return this.ciorEffDtCi;
	}

	public void setCiorEffDt(String ciorEffDt) {
		this.ciorEffDt = Functions.subString(ciorEffDt, Len.CIOR_EFF_DT);
	}

	public String getCiorEffDt() {
		return this.ciorEffDt;
	}

	public void setObjSysIdCi(char objSysIdCi) {
		this.objSysIdCi = objSysIdCi;
	}

	public char getObjSysIdCi() {
		return this.objSysIdCi;
	}

	public void setObjSysId(String objSysId) {
		this.objSysId = Functions.subString(objSysId, Len.OBJ_SYS_ID);
	}

	public String getObjSysId() {
		return this.objSysId;
	}

	public void setCiorObjSeqNbrCi(char ciorObjSeqNbrCi) {
		this.ciorObjSeqNbrCi = ciorObjSeqNbrCi;
	}

	public char getCiorObjSeqNbrCi() {
		return this.ciorObjSeqNbrCi;
	}

	public void setCiorObjSeqNbrSign(char ciorObjSeqNbrSign) {
		this.ciorObjSeqNbrSign = ciorObjSeqNbrSign;
	}

	public void setCiorObjSeqNbrSignFormatted(String ciorObjSeqNbrSign) {
		setCiorObjSeqNbrSign(Functions.charAt(ciorObjSeqNbrSign, Types.CHAR_SIZE));
	}

	public char getCiorObjSeqNbrSign() {
		return this.ciorObjSeqNbrSign;
	}

	public void setCiorObjSeqNbr(int ciorObjSeqNbr) {
		this.ciorObjSeqNbr = NumericDisplay.asString(ciorObjSeqNbr, Len.CIOR_OBJ_SEQ_NBR);
	}

	public void setCiorObjSeqNbrFormatted(String ciorObjSeqNbr) {
		this.ciorObjSeqNbr = Trunc.toUnsignedNumeric(ciorObjSeqNbr, Len.CIOR_OBJ_SEQ_NBR);
	}

	public int getCiorObjSeqNbr() {
		return NumericDisplay.asInt(this.ciorObjSeqNbr);
	}

	public String getCiorObjSeqNbrFormatted() {
		return this.ciorObjSeqNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TCH_OBJECT_KEY = 20;
		public static final int HISTORY_VLD_NBR = 5;
		public static final int CIOR_EFF_DT = 10;
		public static final int OBJ_SYS_ID = 4;
		public static final int CIOR_OBJ_SEQ_NBR = 5;
		public static final int TCH_OBJECT_KEY_CI = 1;
		public static final int HISTORY_VLD_NBR_CI = 1;
		public static final int HISTORY_VLD_NBR_SIGN = 1;
		public static final int CIOR_EFF_DT_CI = 1;
		public static final int OBJ_SYS_ID_CI = 1;
		public static final int CIOR_OBJ_SEQ_NBR_CI = 1;
		public static final int CIOR_OBJ_SEQ_NBR_SIGN = 1;
		public static final int CLT_OBJ_RELATION_KEY = TCH_OBJECT_KEY_CI + TCH_OBJECT_KEY + HISTORY_VLD_NBR_CI + HISTORY_VLD_NBR_SIGN
				+ HISTORY_VLD_NBR + CIOR_EFF_DT_CI + CIOR_EFF_DT + OBJ_SYS_ID_CI + OBJ_SYS_ID + CIOR_OBJ_SEQ_NBR_CI + CIOR_OBJ_SEQ_NBR_SIGN
				+ CIOR_OBJ_SEQ_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
