/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpAccessStatus;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.federatedinsurance.crs.ws.Ts571099Data;
import com.federatedinsurance.crs.ws.Ts571cb1;
import com.federatedinsurance.crs.ws.enums.WaCicsRegionType;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: TS571099<br>
 * <pre>AUTHOR.        LORNE LUND.
 * DATE-WRITTEN.  MARCH 2005.
 *        TITLE - WEB SERVICE URI LOOKUP COMMON ROUTINE
 *      PROCESS - THIS PROGRAM CONTAINS THE LOGIC NEEDED TO FIND
 *                THE URI ASSOCIATED WITH THE WEB SERVICE MNEMONIC
 *                PASSED IN.
 *        INPUT - SERVICE MNEMONIC PASSED IN VIA LINKAGE.
 *                LAYOUT COPYBOOK IS TS571CB1
 *       OUTPUT - URI AND CICS ENVIRONMENT INFORMATION VIA LINKAGE.
 *                LAYOUT COPYBOOK IS TS571CB1
 *     MAINTENANCE HISTORY
 *     *******************
 *     INFO    CHANGE
 *     NMBR     DATE    III  DESCRIPTION
 *     *****  ********  ***  **************************************
 *     TS087  03/10/05  LJL  CREATED NEW PROGRAM.
 *  IM000335  03/21/07  JSP  UPDATE TO USE URI MAPPING</pre>*/
public class Ts571099 extends Program {

	//==== PROPERTIES ====
	private ExecContext execContext = null;
	//Original name: WORKING-STORAGE
	private Ts571099Data ws = new Ts571099Data();
	//Original name: DFHCOMMAREA
	private Ts571cb1 ts571cb1;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, Ts571cb1 ts571cb1) {
		this.execContext = execContext;
		this.ts571cb1 = ts571cb1;
		progStart();
		programExit();
		return 0;
	}

	public static Ts571099 getInstance() {
		return (Programs.getInstance(Ts571099.class));
	}

	/**Original name: 1000-PROG-START<br>*/
	private void progStart() {
		TpOutputData tsQueueData = null;
		// COB_CODE: INITIALIZE WA-URI.
		ws.setWaUri("");
		// COB_CODE: EXEC CICS
		//               ASSIGN
		//                   SYSID(UL-CICS-SYSID)
		//           END-EXEC.
		ts571cb1.setCicsSysid(execContext.getSysId());
		execContext.clearStatus();
		// COB_CODE: MOVE UL-CICS-SYSID          TO WA-CICS-SYSID.
		ws.setWaCicsSysidFormatted(ts571cb1.getCicsSysidFormatted());
		// COB_CODE: EVALUATE TRUE
		//               WHEN WA-CICS-DEVELOPMENT
		//                                       TO WA-QN-PREFIX
		//               WHEN WA-CICS-BETA
		//                   MOVE CF-SP-BETA     TO WA-QN-PREFIX
		//               WHEN WA-CICS-EDUCATION
		//                                       TO WA-QN-PREFIX
		//               WHEN WA-CICS-MODEL-OFFICE
		//                                       TO WA-QN-PREFIX
		//               WHEN WA-CICS-TECH-SERVICES
		//                                       TO WA-QN-PREFIX
		//               WHEN WA-CICS-PRODUCTION
		//                                       TO WA-QN-PREFIX
		//               WHEN OTHER
		//                                       TO WA-QN-PREFIX
		//           END-EVALUATE.
		switch (ws.getWaCicsRegionType().getWaCicsRegionType()) {

		case WaCicsRegionType.DEVELOPMENT:// COB_CODE: MOVE CF-SP-DEVELOPMENT
			//                               TO WA-QN-PREFIX
			ws.setWaQnPrefix(ws.getCfSpDevelopment());
			break;

		case WaCicsRegionType.BETA:// COB_CODE: MOVE CF-SP-BETA     TO WA-QN-PREFIX
			ws.setWaQnPrefix(ws.getCfSpBeta());
			break;

		case WaCicsRegionType.EDUCATION:// COB_CODE: MOVE CF-SP-EDUCATION
			//                               TO WA-QN-PREFIX
			ws.setWaQnPrefix(ws.getCfSpEducation());
			break;

		case WaCicsRegionType.MODEL_OFFICE:// COB_CODE: MOVE CF-SP-MODEL-OFFICE
			//                               TO WA-QN-PREFIX
			ws.setWaQnPrefix(ws.getCfSpModelOffice());
			break;

		case WaCicsRegionType.TECH_SERVICES:// COB_CODE: MOVE CF-SP-TECH-SERVICES
			//                               TO WA-QN-PREFIX
			ws.setWaQnPrefix(ws.getCfSpTechServices());
			break;

		case WaCicsRegionType.PRODUCTION:// COB_CODE: MOVE CF-SP-PRODUCTION
			//                               TO WA-QN-PREFIX
			ws.setWaQnPrefix(ws.getCfSpProduction());
			break;

		default:// COB_CODE: MOVE CF-SP-DEVELOPMENT
			//                               TO WA-QN-PREFIX
			ws.setWaQnPrefix(ws.getCfSpDevelopment());
			break;
		}
		// COB_CODE: MOVE UL-SERVICE-MNEMONIC    TO WA-QN-SERVICE-MNEMONIC.
		ws.setWaQnServiceMnemonic(ts571cb1.getServiceMnemonic());
		// COB_CODE: EXEC CICS
		//               READQ TS
		//               ITEM (1)
		//               QNAME (WA-QUEUE-NAME)
		//               INTO (WA-URI)
		//               NOHANDLE
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(Ts571099Data.Len.WA_URI);
		TsQueueManager.read(execContext, ws.getWaQueueNameFormatted(), ((short) 1), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.setWaUriFromBuffer(tsQueueData.getData());
		}
		// COB_CODE: MOVE WA-URI                 TO UL-URI.
		ts571cb1.setUri(ws.getWaUri());
		// COB_CODE: MOVE WA-QN-PREFIX           TO UL-SERVICE-PREFIX.
		ts571cb1.setServicePrefix(ws.getWaQnPrefix());
		// COB_CODE: IF WA-URI NOT = SPACES
		//               GO TO 1000-PROGRAM-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWaUri())) {
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
	}

	/**Original name: 1000-PROGRAM-EXIT<br>
	 * <pre>    IF TSQ RETURNS SPACES USE THE URI MAPPING LOGIC
	 * LCAROVILLANO     EXEC CICS INQUIRE
	 *          URIMAP(UL-SERVICE-MNEMONIC)
	 *          LOCATION(UL-URI)
	 *          NOHANDLE
	 * LCAROVILLANO     END-EXEC.</pre>*/
	private void programExit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
		// COB_CODE: EXIT.
		//exit
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
