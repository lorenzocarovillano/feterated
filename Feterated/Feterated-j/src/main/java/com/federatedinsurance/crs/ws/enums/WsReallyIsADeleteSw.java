/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-REALLY-IS-A-DELETE-SW<br>
 * Variable: WS-REALLY-IS-A-DELETE-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsReallyIsADeleteSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char REALLY_IS_A_DELETE = 'Y';
	public static final char NOT_REALLY_IS_A_DELETE = 'N';

	//==== METHODS ====
	public void setReallyIsADeleteSw(char reallyIsADeleteSw) {
		this.value = reallyIsADeleteSw;
	}

	public char getReallyIsADeleteSw() {
		return this.value;
	}

	public boolean isWsReallyIsADelete() {
		return value == REALLY_IS_A_DELETE;
	}

	public boolean isWsNotReallyIsADelete() {
		return value == NOT_REALLY_IS_A_DELETE;
	}

	public void setWsNotReallyIsADelete() {
		value = NOT_REALLY_IS_A_DELETE;
	}
}
