/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsTs547099 {

	//==== PROPERTIES ====
	/**Original name: CF-MAX-OPEN-PIPES<br>
	 * <pre>    NOTE:  ANY CHANGE TO THIS CONSTANT FIELD MUST BE REFLECTED
	 *            IN THE TABLE OCCURS CLAUSE</pre>*/
	private short maxOpenPipes = ((short) 5);
	/**Original name: CF-MAX-DATA-LEN<br>
	 * <pre>    NOTE:  ANY CHANGE TO THIS CONSTANT FIELD MUST BE REFLECTED
	 *            IN THE SIZE OF THE FIELD USED TO HOLD DATA TO PASS</pre>*/
	private int maxDataLen = 32767;
	//Original name: CF-MIN-DATA-LEN
	private int minDataLen = 1;

	//==== METHODS ====
	public short getMaxOpenPipes() {
		return this.maxOpenPipes;
	}

	public int getMaxDataLen() {
		return this.maxDataLen;
	}

	public int getMinDataLen() {
		return this.minDataLen;
	}
}
