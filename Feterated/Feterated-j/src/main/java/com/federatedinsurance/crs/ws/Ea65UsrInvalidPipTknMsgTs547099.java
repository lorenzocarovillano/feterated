/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EA-65-USR-INVALID-PIP-TKN-MSG<br>
 * Variable: EA-65-USR-INVALID-PIP-TKN-MSG from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea65UsrInvalidPipTknMsgTs547099 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-65-USR-INVALID-PIP-TKN-MSG
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-65-USR-INVALID-PIP-TKN-MSG-1
	private String flr2 = "INVALID PIPE";
	//Original name: FILLER-EA-65-USR-INVALID-PIP-TKN-MSG-2
	private String flr3 = "TOKEN PROVIDED.";
	//Original name: FILLER-EA-65-USR-INVALID-PIP-TKN-MSG-3
	private String flr4 = "  (";
	//Original name: EA-65-PIPE-TOKEN
	private String ea65PipeToken = DefaultValues.stringVal(Len.EA65_PIPE_TOKEN);
	//Original name: FILLER-EA-65-USR-INVALID-PIP-TKN-MSG-4
	private char flr5 = ')';

	//==== METHODS ====
	public String getEa65UsrInvalidPipTknMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa65UsrInvalidPipTknMsgBytes());
	}

	public byte[] getEa65UsrInvalidPipTknMsgBytes() {
		byte[] buffer = new byte[Len.EA65_USR_INVALID_PIP_TKN_MSG];
		return getEa65UsrInvalidPipTknMsgBytes(buffer, 1);
	}

	public byte[] getEa65UsrInvalidPipTknMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, ea65PipeToken, Len.EA65_PIPE_TOKEN);
		position += Len.EA65_PIPE_TOKEN;
		MarshalByte.writeChar(buffer, position, flr5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setEa65PipeToken(long ea65PipeToken) {
		this.ea65PipeToken = NumericDisplay.asString(ea65PipeToken, Len.EA65_PIPE_TOKEN);
	}

	public long getEa65PipeToken() {
		return NumericDisplay.asLong(this.ea65PipeToken);
	}

	public char getFlr5() {
		return this.flr5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA65_PIPE_TOKEN = 10;
		public static final int FLR1 = 11;
		public static final int FLR2 = 13;
		public static final int FLR3 = 15;
		public static final int FLR4 = 3;
		public static final int FLR5 = 1;
		public static final int EA65_USR_INVALID_PIP_TKN_MSG = EA65_PIPE_TOKEN + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
