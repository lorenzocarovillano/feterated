/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-END-ST-CNC-WRD-RQR-CSR-FLAG<br>
 * Variable: SW-END-ST-CNC-WRD-RQR-CSR-FLAG from program XZ0P9000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwEndStCncWrdRqrCsrFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_ST_CNC_WRD_RQR_CSR = 'Y';
	public static final char NOT_END_ST_CNC_WRD_RQR_CSR = 'N';

	//==== METHODS ====
	public void setEndStCncWrdRqrCsrFlag(char endStCncWrdRqrCsrFlag) {
		this.value = endStCncWrdRqrCsrFlag;
	}

	public char getEndStCncWrdRqrCsrFlag() {
		return this.value;
	}

	public boolean isEndStCncWrdRqrCsr() {
		return value == END_ST_CNC_WRD_RQR_CSR;
	}

	public void setEndStCncWrdRqrCsr() {
		value = END_ST_CNC_WRD_RQR_CSR;
	}

	public void setNotEndStCncWrdRqrCsr() {
		value = NOT_END_ST_CNC_WRD_RQR_CSR;
	}
}
