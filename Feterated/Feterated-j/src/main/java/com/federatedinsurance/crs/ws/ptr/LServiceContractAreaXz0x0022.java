/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0022<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0022 extends BytesClass {

	//==== PROPERTIES ====
	public static final int I_POL_NBR_LIST_MAXOCCURS = 150;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0022() {
	}

	public LServiceContractAreaXz0x0022(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiCsrActNbr(String iCsrActNbr) {
		writeString(Pos.I_CSR_ACT_NBR, iCsrActNbr, Len.I_CSR_ACT_NBR);
	}

	/**Original name: XZT022I-CSR-ACT-NBR<br>*/
	public String getiCsrActNbr() {
		return readString(Pos.I_CSR_ACT_NBR, Len.I_CSR_ACT_NBR);
	}

	/**Original name: XZT022I-POL-NBR-LIST<br>*/
	public byte[] getiPolNbrListBytes(int iPolNbrListIdx) {
		byte[] buffer = new byte[Len.I_POL_NBR_LIST];
		return getiPolNbrListBytes(iPolNbrListIdx, buffer, 1);
	}

	public byte[] getiPolNbrListBytes(int iPolNbrListIdx, byte[] buffer, int offset) {
		int position = Pos.xzt022iPolNbrList(iPolNbrListIdx - 1);
		getBytes(buffer, offset, Len.I_POL_NBR_LIST, position);
		return buffer;
	}

	public void setiPolNbr(int iPolNbrIdx, String iPolNbr) {
		int position = Pos.xzt022iPolNbr(iPolNbrIdx - 1);
		writeString(position, iPolNbr, Len.I_POL_NBR);
	}

	/**Original name: XZT022I-POL-NBR<br>*/
	public String getiPolNbr(int iPolNbrIdx) {
		int position = Pos.xzt022iPolNbr(iPolNbrIdx - 1);
		return readString(position, Len.I_POL_NBR);
	}

	public void setiUserid(String iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT022I-USERID<br>*/
	public String getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_CSR_ACT_NBR = SERVICE_INPUTS;
		public static final int I_USERID = xzt022iPolNbr(I_POL_NBR_LIST_MAXOCCURS - 1) + Len.I_POL_NBR;
		public static final int SERVICE_OUTPUTS = I_USERID + Len.I_USERID;
		public static final int FLR1 = SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt022iPolNbrList(int idx) {
			return I_CSR_ACT_NBR + Len.I_CSR_ACT_NBR + idx * Len.I_POL_NBR_LIST;
		}

		public static int xzt022iPolNbr(int idx) {
			return xzt022iPolNbrList(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_CSR_ACT_NBR = 9;
		public static final int I_POL_NBR = 25;
		public static final int I_POL_NBR_LIST = I_POL_NBR;
		public static final int I_USERID = 8;
		public static final int SERVICE_INPUTS = I_CSR_ACT_NBR + LServiceContractAreaXz0x0022.I_POL_NBR_LIST_MAXOCCURS * I_POL_NBR_LIST + I_USERID;
		public static final int FLR1 = 1;
		public static final int SERVICE_OUTPUTS = FLR1;
		public static final int L_SERVICE_CONTRACT_AREA = SERVICE_INPUTS + SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
