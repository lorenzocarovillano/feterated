/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW48F-FED-BUSINESS-TYP-ROW<br>
 * Variable: CW48F-FED-BUSINESS-TYP-ROW from copybook CAWLF048<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw48fFedBusinessTypRow {

	//==== PROPERTIES ====
	//Original name: CW48F-FED-BUSINESS-TYP-CSUM
	private String fedBusinessTypCsum = DefaultValues.stringVal(Len.FED_BUSINESS_TYP_CSUM);
	//Original name: CW48F-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CW48F-TOB-CD-KCRE
	private String tobCdKcre = DefaultValues.stringVal(Len.TOB_CD_KCRE);
	//Original name: CW48F-HISTORY-VLD-NBR-KCRE
	private String historyVldNbrKcre = DefaultValues.stringVal(Len.HISTORY_VLD_NBR_KCRE);
	//Original name: CW48F-EFFECTIVE-DT-KCRE
	private String effectiveDtKcre = DefaultValues.stringVal(Len.EFFECTIVE_DT_KCRE);
	//Original name: CW48F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW48F-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: CW48F-TOB-CD
	private String tobCd = DefaultValues.stringVal(Len.TOB_CD);
	//Original name: CW48F-HISTORY-VLD-NBR-SIGN
	private char historyVldNbrSign = DefaultValues.CHAR_VAL;
	//Original name: CW48F-HISTORY-VLD-NBR
	private String historyVldNbr = DefaultValues.stringVal(Len.HISTORY_VLD_NBR);
	//Original name: CW48F-EFFECTIVE-DT
	private String effectiveDt = DefaultValues.stringVal(Len.EFFECTIVE_DT);
	//Original name: CW48F-CLIENT-ID-CI
	private char clientIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-TOB-CD-CI
	private char tobCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-HISTORY-VLD-NBR-CI
	private char historyVldNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-EFFECTIVE-DT-CI
	private char effectiveDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-FED-BUSINESS-TYP-DATA
	private Cw48fFedBusinessTypData fedBusinessTypData = new Cw48fFedBusinessTypData();

	//==== METHODS ====
	public void setCw48fFedBusinessTypRowFormatted(String data) {
		byte[] buffer = new byte[Len.CW48F_FED_BUSINESS_TYP_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CW48F_FED_BUSINESS_TYP_ROW);
		setCw48fFedBusinessTypRowBytes(buffer, 1);
	}

	public String getCw48fFedBusinessTypRowFormatted() {
		return MarshalByteExt.bufferToStr(getCw48fFedBusinessTypRowBytes());
	}

	public byte[] getCw48fFedBusinessTypRowBytes() {
		byte[] buffer = new byte[Len.CW48F_FED_BUSINESS_TYP_ROW];
		return getCw48fFedBusinessTypRowBytes(buffer, 1);
	}

	public void setCw48fFedBusinessTypRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setFedBusinessTypFixedBytes(buffer, position);
		position += Len.FED_BUSINESS_TYP_FIXED;
		setFedBusinessTypDatesBytes(buffer, position);
		position += Len.FED_BUSINESS_TYP_DATES;
		setFedBusinessTypKeyBytes(buffer, position);
		position += Len.FED_BUSINESS_TYP_KEY;
		setFedBusinessTypKeyCiBytes(buffer, position);
		position += Len.FED_BUSINESS_TYP_KEY_CI;
		fedBusinessTypData.setFedBusinessTypDataBytes(buffer, position);
	}

	public byte[] getCw48fFedBusinessTypRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getFedBusinessTypFixedBytes(buffer, position);
		position += Len.FED_BUSINESS_TYP_FIXED;
		getFedBusinessTypDatesBytes(buffer, position);
		position += Len.FED_BUSINESS_TYP_DATES;
		getFedBusinessTypKeyBytes(buffer, position);
		position += Len.FED_BUSINESS_TYP_KEY;
		getFedBusinessTypKeyCiBytes(buffer, position);
		position += Len.FED_BUSINESS_TYP_KEY_CI;
		fedBusinessTypData.getFedBusinessTypDataBytes(buffer, position);
		return buffer;
	}

	public void setFedBusinessTypFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		fedBusinessTypCsum = MarshalByte.readFixedString(buffer, position, Len.FED_BUSINESS_TYP_CSUM);
		position += Len.FED_BUSINESS_TYP_CSUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		tobCdKcre = MarshalByte.readString(buffer, position, Len.TOB_CD_KCRE);
		position += Len.TOB_CD_KCRE;
		historyVldNbrKcre = MarshalByte.readString(buffer, position, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		effectiveDtKcre = MarshalByte.readString(buffer, position, Len.EFFECTIVE_DT_KCRE);
	}

	public byte[] getFedBusinessTypFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, fedBusinessTypCsum, Len.FED_BUSINESS_TYP_CSUM);
		position += Len.FED_BUSINESS_TYP_CSUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		position += Len.CLIENT_ID_KCRE;
		MarshalByte.writeString(buffer, position, tobCdKcre, Len.TOB_CD_KCRE);
		position += Len.TOB_CD_KCRE;
		MarshalByte.writeString(buffer, position, historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
		position += Len.HISTORY_VLD_NBR_KCRE;
		MarshalByte.writeString(buffer, position, effectiveDtKcre, Len.EFFECTIVE_DT_KCRE);
		return buffer;
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setTobCdKcre(String tobCdKcre) {
		this.tobCdKcre = Functions.subString(tobCdKcre, Len.TOB_CD_KCRE);
	}

	public String getTobCdKcre() {
		return this.tobCdKcre;
	}

	public void setHistoryVldNbrKcre(String historyVldNbrKcre) {
		this.historyVldNbrKcre = Functions.subString(historyVldNbrKcre, Len.HISTORY_VLD_NBR_KCRE);
	}

	public String getHistoryVldNbrKcre() {
		return this.historyVldNbrKcre;
	}

	public void setEffectiveDtKcre(String effectiveDtKcre) {
		this.effectiveDtKcre = Functions.subString(effectiveDtKcre, Len.EFFECTIVE_DT_KCRE);
	}

	public String getEffectiveDtKcre() {
		return this.effectiveDtKcre;
	}

	public void setFedBusinessTypDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getFedBusinessTypDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setFedBusinessTypKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		clientId = MarshalByte.readString(buffer, position, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		tobCd = MarshalByte.readString(buffer, position, Len.TOB_CD);
		position += Len.TOB_CD;
		historyVldNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbr = MarshalByte.readFixedString(buffer, position, Len.HISTORY_VLD_NBR);
		position += Len.HISTORY_VLD_NBR;
		effectiveDt = MarshalByte.readString(buffer, position, Len.EFFECTIVE_DT);
	}

	public byte[] getFedBusinessTypKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeString(buffer, position, tobCd, Len.TOB_CD);
		position += Len.TOB_CD;
		MarshalByte.writeChar(buffer, position, historyVldNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, historyVldNbr, Len.HISTORY_VLD_NBR);
		position += Len.HISTORY_VLD_NBR;
		MarshalByte.writeString(buffer, position, effectiveDt, Len.EFFECTIVE_DT);
		return buffer;
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setTobCd(String tobCd) {
		this.tobCd = Functions.subString(tobCd, Len.TOB_CD);
	}

	public String getTobCd() {
		return this.tobCd;
	}

	public void setHistoryVldNbrSign(char historyVldNbrSign) {
		this.historyVldNbrSign = historyVldNbrSign;
	}

	public char getHistoryVldNbrSign() {
		return this.historyVldNbrSign;
	}

	public void setEffectiveDt(String effectiveDt) {
		this.effectiveDt = Functions.subString(effectiveDt, Len.EFFECTIVE_DT);
	}

	public String getEffectiveDt() {
		return this.effectiveDt;
	}

	public void setFedBusinessTypKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		clientIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tobCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		effectiveDtCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getFedBusinessTypKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, clientIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, tobCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, historyVldNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, effectiveDtCi);
		return buffer;
	}

	public void setClientIdCi(char clientIdCi) {
		this.clientIdCi = clientIdCi;
	}

	public char getClientIdCi() {
		return this.clientIdCi;
	}

	public void setTobCdCi(char tobCdCi) {
		this.tobCdCi = tobCdCi;
	}

	public char getTobCdCi() {
		return this.tobCdCi;
	}

	public void setHistoryVldNbrCi(char historyVldNbrCi) {
		this.historyVldNbrCi = historyVldNbrCi;
	}

	public char getHistoryVldNbrCi() {
		return this.historyVldNbrCi;
	}

	public void setEffectiveDtCi(char effectiveDtCi) {
		this.effectiveDtCi = effectiveDtCi;
	}

	public char getEffectiveDtCi() {
		return this.effectiveDtCi;
	}

	public Cw48fFedBusinessTypData getFedBusinessTypData() {
		return fedBusinessTypData;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FED_BUSINESS_TYP_CSUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int TOB_CD_KCRE = 32;
		public static final int HISTORY_VLD_NBR_KCRE = 32;
		public static final int EFFECTIVE_DT_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLIENT_ID = 20;
		public static final int TOB_CD = 4;
		public static final int HISTORY_VLD_NBR = 5;
		public static final int EFFECTIVE_DT = 10;
		public static final int FED_BUSINESS_TYP_FIXED = FED_BUSINESS_TYP_CSUM + CLIENT_ID_KCRE + TOB_CD_KCRE + HISTORY_VLD_NBR_KCRE
				+ EFFECTIVE_DT_KCRE;
		public static final int FED_BUSINESS_TYP_DATES = TRANS_PROCESS_DT;
		public static final int HISTORY_VLD_NBR_SIGN = 1;
		public static final int FED_BUSINESS_TYP_KEY = CLIENT_ID + TOB_CD + HISTORY_VLD_NBR_SIGN + HISTORY_VLD_NBR + EFFECTIVE_DT;
		public static final int CLIENT_ID_CI = 1;
		public static final int TOB_CD_CI = 1;
		public static final int HISTORY_VLD_NBR_CI = 1;
		public static final int EFFECTIVE_DT_CI = 1;
		public static final int FED_BUSINESS_TYP_KEY_CI = CLIENT_ID_CI + TOB_CD_CI + HISTORY_VLD_NBR_CI + EFFECTIVE_DT_CI;
		public static final int CW48F_FED_BUSINESS_TYP_ROW = FED_BUSINESS_TYP_FIXED + FED_BUSINESS_TYP_DATES + FED_BUSINESS_TYP_KEY
				+ FED_BUSINESS_TYP_KEY_CI + Cw48fFedBusinessTypData.Len.FED_BUSINESS_TYP_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
