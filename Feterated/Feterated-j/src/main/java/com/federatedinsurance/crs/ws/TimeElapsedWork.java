/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: TIME-ELAPSED-WORK<br>
 * Variable: TIME-ELAPSED-WORK from program TS020000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TimeElapsedWork {

	//==== PROPERTIES ====
	//Original name: TE-TRAN-HOURS-START
	private String hoursStart = DefaultValues.stringVal(Len.HOURS_START);
	//Original name: TE-TRAN-MINS-START
	private String minsStart = DefaultValues.stringVal(Len.MINS_START);
	//Original name: TE-TRAN-SECS-START
	private String secsStart = DefaultValues.stringVal(Len.SECS_START);
	//Original name: TE-TRAN-HTHS-START
	private String hthsStart = DefaultValues.stringVal(Len.HTHS_START);
	//Original name: TE-TRAN-HOURS-END
	private String hoursEnd = DefaultValues.stringVal(Len.HOURS_END);
	//Original name: TE-TRAN-MINS-END
	private String minsEnd = DefaultValues.stringVal(Len.MINS_END);
	//Original name: TE-TRAN-SECS-END
	private String secsEnd = DefaultValues.stringVal(Len.SECS_END);
	//Original name: TE-TRAN-HTHS-END
	private String hthsEnd = DefaultValues.stringVal(Len.HTHS_END);
	//Original name: TE-TRAN-START-TIME-HTH
	private int startTimeHth = DefaultValues.INT_VAL;
	//Original name: TE-TRAN-END-TIME-HTH
	private int endTimeHth = DefaultValues.INT_VAL;
	//Original name: TE-TRANS-ELAPSED-TIME-AMT
	private int sElapsedTimeAmt = DefaultValues.INT_VAL;

	//==== METHODS ====
	public void setStartTimeFormatted(String data) {
		byte[] buffer = new byte[Len.START_TIME];
		MarshalByte.writeString(buffer, 1, data, Len.START_TIME);
		setStartTimeBytes(buffer, 1);
	}

	/**Original name: TE-TRAN-START-TIME<br>
	 * <pre>* USED FOR CALCULATING THE ELAPSED TIME FOR THE TRANSACTION.</pre>*/
	public byte[] getStartTimeBytes() {
		byte[] buffer = new byte[Len.START_TIME];
		return getStartTimeBytes(buffer, 1);
	}

	public void setStartTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		hoursStart = MarshalByte.readFixedString(buffer, position, Len.HOURS_START);
		position += Len.HOURS_START;
		minsStart = MarshalByte.readFixedString(buffer, position, Len.MINS_START);
		position += Len.MINS_START;
		secsStart = MarshalByte.readFixedString(buffer, position, Len.SECS_START);
		position += Len.SECS_START;
		hthsStart = MarshalByte.readFixedString(buffer, position, Len.HTHS_START);
	}

	public byte[] getStartTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, hoursStart, Len.HOURS_START);
		position += Len.HOURS_START;
		MarshalByte.writeString(buffer, position, minsStart, Len.MINS_START);
		position += Len.MINS_START;
		MarshalByte.writeString(buffer, position, secsStart, Len.SECS_START);
		position += Len.SECS_START;
		MarshalByte.writeString(buffer, position, hthsStart, Len.HTHS_START);
		return buffer;
	}

	public void initStartTimeZeroes() {
		hoursStart = "00";
		minsStart = "00";
		secsStart = "00";
		hthsStart = "00";
	}

	public short getHoursStart() {
		return NumericDisplay.asShort(this.hoursStart);
	}

	public short getMinsStart() {
		return NumericDisplay.asShort(this.minsStart);
	}

	public short getSecsStart() {
		return NumericDisplay.asShort(this.secsStart);
	}

	public short getHthsStart() {
		return NumericDisplay.asShort(this.hthsStart);
	}

	public void setEndTimeFormatted(String data) {
		byte[] buffer = new byte[Len.END_TIME];
		MarshalByte.writeString(buffer, 1, data, Len.END_TIME);
		setEndTimeBytes(buffer, 1);
	}

	/**Original name: TE-TRAN-END-TIME<br>*/
	public byte[] getEndTimeBytes() {
		byte[] buffer = new byte[Len.END_TIME];
		return getEndTimeBytes(buffer, 1);
	}

	public void setEndTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		hoursEnd = MarshalByte.readFixedString(buffer, position, Len.HOURS_END);
		position += Len.HOURS_END;
		minsEnd = MarshalByte.readFixedString(buffer, position, Len.MINS_END);
		position += Len.MINS_END;
		secsEnd = MarshalByte.readFixedString(buffer, position, Len.SECS_END);
		position += Len.SECS_END;
		hthsEnd = MarshalByte.readFixedString(buffer, position, Len.HTHS_END);
	}

	public byte[] getEndTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, hoursEnd, Len.HOURS_END);
		position += Len.HOURS_END;
		MarshalByte.writeString(buffer, position, minsEnd, Len.MINS_END);
		position += Len.MINS_END;
		MarshalByte.writeString(buffer, position, secsEnd, Len.SECS_END);
		position += Len.SECS_END;
		MarshalByte.writeString(buffer, position, hthsEnd, Len.HTHS_END);
		return buffer;
	}

	public void initEndTimeZeroes() {
		hoursEnd = "00";
		minsEnd = "00";
		secsEnd = "00";
		hthsEnd = "00";
	}

	public short getHoursEnd() {
		return NumericDisplay.asShort(this.hoursEnd);
	}

	public short getMinsEnd() {
		return NumericDisplay.asShort(this.minsEnd);
	}

	public short getSecsEnd() {
		return NumericDisplay.asShort(this.secsEnd);
	}

	public short getHthsEnd() {
		return NumericDisplay.asShort(this.hthsEnd);
	}

	public void setStartTimeHth(int startTimeHth) {
		this.startTimeHth = startTimeHth;
	}

	public int getStartTimeHth() {
		return this.startTimeHth;
	}

	public void setEndTimeHth(int endTimeHth) {
		this.endTimeHth = endTimeHth;
	}

	public int getEndTimeHth() {
		return this.endTimeHth;
	}

	public void setsElapsedTimeAmt(int sElapsedTimeAmt) {
		this.sElapsedTimeAmt = sElapsedTimeAmt;
	}

	public int getsElapsedTimeAmt() {
		return this.sElapsedTimeAmt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HOURS_START = 2;
		public static final int MINS_START = 2;
		public static final int SECS_START = 2;
		public static final int HTHS_START = 2;
		public static final int HOURS_END = 2;
		public static final int MINS_END = 2;
		public static final int SECS_END = 2;
		public static final int HTHS_END = 2;
		public static final int START_TIME = HOURS_START + MINS_START + SECS_START + HTHS_START;
		public static final int END_TIME = HOURS_END + MINS_END + SECS_END + HTHS_END;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
