/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-07-IMP-NOT-NOT-FOUND<br>
 * Variable: EA-07-IMP-NOT-NOT-FOUND from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea07ImpNotNotFound {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-07-IMP-NOT-NOT-FOUND
	private String flr1 = " PGM = XZ004000";
	//Original name: FILLER-EA-07-IMP-NOT-NOT-FOUND-1
	private String flr2 = " -";
	//Original name: FILLER-EA-07-IMP-NOT-NOT-FOUND-2
	private String flr3 = "A PRIOR";
	//Original name: FILLER-EA-07-IMP-NOT-NOT-FOUND-3
	private String flr4 = "IMPENDING";
	//Original name: FILLER-EA-07-IMP-NOT-NOT-FOUND-4
	private String flr5 = "NOTICE WAS NOT";
	//Original name: FILLER-EA-07-IMP-NOT-NOT-FOUND-5
	private String flr6 = "FOUND FOR";
	//Original name: FILLER-EA-07-IMP-NOT-NOT-FOUND-6
	private String flr7 = "POL-NBR=";
	//Original name: EA-07-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: FILLER-EA-07-IMP-NOT-NOT-FOUND-7
	private String flr8 = " POL-EFF-DT=";
	//Original name: EA-07-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: FILLER-EA-07-IMP-NOT-NOT-FOUND-8
	private String flr9 = " STA-MDF-TS=";
	//Original name: EA-07-STA-MDF-TS
	private String staMdfTs = DefaultValues.stringVal(Len.STA_MDF_TS);

	//==== METHODS ====
	public String getEa07ImpNotNotFoundFormatted() {
		return MarshalByteExt.bufferToStr(getEa07ImpNotNotFoundBytes());
	}

	public byte[] getEa07ImpNotNotFoundBytes() {
		byte[] buffer = new byte[Len.EA07_IMP_NOT_NOT_FOUND];
		return getEa07ImpNotNotFoundBytes(buffer, 1);
	}

	public byte[] getEa07ImpNotNotFoundBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		position += Len.FLR8;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR8);
		position += Len.FLR8;
		MarshalByte.writeString(buffer, position, staMdfTs, Len.STA_MDF_TS);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public void setStaMdfTs(String staMdfTs) {
		this.staMdfTs = Functions.subString(staMdfTs, Len.STA_MDF_TS);
	}

	public String getStaMdfTs() {
		return this.staMdfTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 16;
		public static final int POL_EFF_DT = 10;
		public static final int STA_MDF_TS = 26;
		public static final int FLR1 = 15;
		public static final int FLR2 = 3;
		public static final int FLR3 = 8;
		public static final int FLR4 = 10;
		public static final int FLR7 = 9;
		public static final int FLR8 = 13;
		public static final int EA07_IMP_NOT_NOT_FOUND = POL_NBR + POL_EFF_DT + STA_MDF_TS + 2 * FLR1 + FLR2 + FLR3 + 2 * FLR4 + FLR7 + 2 * FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
