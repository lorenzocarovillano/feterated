/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-MSG-UMT-LOOP-SW<br>
 * Variable: WS-MSG-UMT-LOOP-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMsgUmtLoopSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char STAY_IN_MSG_UMT_LOOP = 'Y';
	public static final char EXIT_MSG_UMT_LOOP = 'N';

	//==== METHODS ====
	public void setMsgUmtLoopSw(char msgUmtLoopSw) {
		this.value = msgUmtLoopSw;
	}

	public char getMsgUmtLoopSw() {
		return this.value;
	}

	public void setStayInMsgUmtLoop() {
		value = STAY_IN_MSG_UMT_LOOP;
	}

	public boolean isExitMsgUmtLoop() {
		return value == EXIT_MSG_UMT_LOOP;
	}

	public void setExitMsgUmtLoop() {
		value = EXIT_MSG_UMT_LOOP;
	}
}
