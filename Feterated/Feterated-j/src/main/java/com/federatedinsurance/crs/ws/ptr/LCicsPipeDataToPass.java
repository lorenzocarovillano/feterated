/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-CICS-PIPE-DATA-TO-PASS<br>
 * Variable: L-CICS-PIPE-DATA-TO-PASS from program TS547099<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LCicsPipeDataToPass extends BytesClass {

	//==== CONSTRUCTORS ====
	public LCicsPipeDataToPass() {
	}

	public LCicsPipeDataToPass(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_CICS_PIPE_DATA_TO_PASS;
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_CICS_PIPE_DATA_TO_PASS = 1;
		public static final int FLR1 = L_CICS_PIPE_DATA_TO_PASS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 32767;
		public static final int L_CICS_PIPE_DATA_TO_PASS = FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
