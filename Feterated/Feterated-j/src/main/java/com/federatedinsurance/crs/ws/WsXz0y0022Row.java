/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xzy022NlbeInd;
import com.federatedinsurance.crs.ws.enums.Xzy022WarningInd;
import com.federatedinsurance.crs.ws.occurs.Xzy022CncPolList;
import com.federatedinsurance.crs.ws.occurs.Xzy022Warnings;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0Y0022-ROW<br>
 * Variable: WS-XZ0Y0022-ROW from program XZ0P0022<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0y0022Row extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int CNC_POL_LIST_MAXOCCURS = 150;
	public static final int WARNINGS_MAXOCCURS = 10;
	//Original name: XZY022-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZY022-CNC-POL-LIST
	private Xzy022CncPolList[] cncPolList = new Xzy022CncPolList[CNC_POL_LIST_MAXOCCURS];
	//Original name: XZY022-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);
	//Original name: XZY022-NLBE-IND
	private Xzy022NlbeInd nlbeInd = new Xzy022NlbeInd();
	//Original name: XZY022-WARNING-IND
	private Xzy022WarningInd warningInd = new Xzy022WarningInd();
	//Original name: XZY022-NON-LOG-ERR-MSG
	private String nonLogErrMsg = DefaultValues.stringVal(Len.NON_LOG_ERR_MSG);
	//Original name: XZY022-WARNINGS
	private Xzy022Warnings[] warnings = new Xzy022Warnings[WARNINGS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public WsXz0y0022Row() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0Y0022_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0y0022RowBytes(buf);
	}

	public void init() {
		for (int cncPolListIdx = 1; cncPolListIdx <= CNC_POL_LIST_MAXOCCURS; cncPolListIdx++) {
			cncPolList[cncPolListIdx - 1] = new Xzy022CncPolList();
		}
		for (int warningsIdx = 1; warningsIdx <= WARNINGS_MAXOCCURS; warningsIdx++) {
			warnings[warningsIdx - 1] = new Xzy022Warnings();
		}
	}

	public String getWsXz0y0022RowFormatted() {
		return getSetTmnFlgRowFormatted();
	}

	public void setWsXz0y0022RowBytes(byte[] buffer) {
		setWsXz0y0022RowBytes(buffer, 1);
	}

	public byte[] getWsXz0y0022RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0Y0022_ROW];
		return getWsXz0y0022RowBytes(buffer, 1);
	}

	public void setWsXz0y0022RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setSetTmnFlgRowBytes(buffer, position);
	}

	public byte[] getWsXz0y0022RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getSetTmnFlgRowBytes(buffer, position);
		return buffer;
	}

	public String getSetTmnFlgRowFormatted() {
		return MarshalByteExt.bufferToStr(getSetTmnFlgRowBytes());
	}

	/**Original name: XZY022-SET-TMN-FLG-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0Y0022 - SET TERMINATION FLAG                                *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *    15389 07/24/2017  E404JAL   NEW                              *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getSetTmnFlgRowBytes() {
		byte[] buffer = new byte[Len.SET_TMN_FLG_ROW];
		return getSetTmnFlgRowBytes(buffer, 1);
	}

	public void setSetTmnFlgRowBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		for (int idx = 1; idx <= CNC_POL_LIST_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				cncPolList[idx - 1].setCncPolListBytes(buffer, position);
				position += Xzy022CncPolList.Len.CNC_POL_LIST;
			} else {
				cncPolList[idx - 1].initCncPolListSpaces();
				position += Xzy022CncPolList.Len.CNC_POL_LIST;
			}
		}
		userid = MarshalByte.readString(buffer, position, Len.USERID);
		position += Len.USERID;
		setErrorFlagsBytes(buffer, position);
		position += Len.ERROR_FLAGS;
		nonLogErrMsg = MarshalByte.readString(buffer, position, Len.NON_LOG_ERR_MSG);
		position += Len.NON_LOG_ERR_MSG;
		for (int idx = 1; idx <= WARNINGS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				warnings[idx - 1].setWarningsBytes(buffer, position);
				position += Xzy022Warnings.Len.WARNINGS;
			} else {
				warnings[idx - 1].initWarningsSpaces();
				position += Xzy022Warnings.Len.WARNINGS;
			}
		}
	}

	public byte[] getSetTmnFlgRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		for (int idx = 1; idx <= CNC_POL_LIST_MAXOCCURS; idx++) {
			cncPolList[idx - 1].getCncPolListBytes(buffer, position);
			position += Xzy022CncPolList.Len.CNC_POL_LIST;
		}
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position += Len.USERID;
		getErrorFlagsBytes(buffer, position);
		position += Len.ERROR_FLAGS;
		MarshalByte.writeString(buffer, position, nonLogErrMsg, Len.NON_LOG_ERR_MSG);
		position += Len.NON_LOG_ERR_MSG;
		for (int idx = 1; idx <= WARNINGS_MAXOCCURS; idx++) {
			warnings[idx - 1].getWarningsBytes(buffer, position);
			position += Xzy022Warnings.Len.WARNINGS;
		}
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	public void setErrorFlagsBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeInd.setNlbeInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		warningInd.setWarningInd(MarshalByte.readChar(buffer, position));
	}

	public byte[] getErrorFlagsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, nlbeInd.getNlbeInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, warningInd.getWarningInd());
		return buffer;
	}

	public void setNonLogErrMsg(String nonLogErrMsg) {
		this.nonLogErrMsg = Functions.subString(nonLogErrMsg, Len.NON_LOG_ERR_MSG);
	}

	public String getNonLogErrMsg() {
		return this.nonLogErrMsg;
	}

	public Xzy022CncPolList getCncPolList(int idx) {
		return cncPolList[idx - 1];
	}

	public Xzy022NlbeInd getNlbeInd() {
		return nlbeInd;
	}

	public Xzy022WarningInd getWarningInd() {
		return warningInd;
	}

	public Xzy022Warnings getWarnings(int idx) {
		return warnings[idx - 1];
	}

	@Override
	public byte[] serialize() {
		return getWsXz0y0022RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int USERID = 8;
		public static final int ERROR_FLAGS = Xzy022NlbeInd.Len.NLBE_IND + Xzy022WarningInd.Len.WARNING_IND;
		public static final int NON_LOG_ERR_MSG = 500;
		public static final int SET_TMN_FLG_ROW = CSR_ACT_NBR + WsXz0y0022Row.CNC_POL_LIST_MAXOCCURS * Xzy022CncPolList.Len.CNC_POL_LIST + USERID
				+ ERROR_FLAGS + NON_LOG_ERR_MSG + WsXz0y0022Row.WARNINGS_MAXOCCURS * Xzy022Warnings.Len.WARNINGS;
		public static final int WS_XZ0Y0022_ROW = SET_TMN_FLG_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
