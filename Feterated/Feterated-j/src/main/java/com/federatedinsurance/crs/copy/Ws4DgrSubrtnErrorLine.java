/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Ws4DgrSubrtnName;

/**Original name: WS4-DGR-SUBRTN-ERROR-LINE<br>
 * Variable: WS4-DGR-SUBRTN-ERROR-LINE from copybook CISLNSRB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ws4DgrSubrtnErrorLine {

	//==== PROPERTIES ====
	//Original name: FILLER-WS4-DGR-SUBRTN-ERROR-LINE
	private String flr1 = "MAIN CALLING PROGRAM:";
	//Original name: WS4-DGR-MAIN-CALLING-PGM
	private String mainCallingPgm = "";
	//Original name: FILLER-WS4-DGR-SUBRTN-ERROR-LINE-1
	private String flr2 = ".  DGR00UC: CRITICAL ERROR OCCURED IN SUBROUTINE";
	//Original name: WS4-DGR-SUBRTN-NAME
	private Ws4DgrSubrtnName subrtnName = new Ws4DgrSubrtnName();
	//Original name: FILLER-WS4-DGR-SUBRTN-ERROR-LINE-2
	private String flr3 = ".  RETURN CODE:";
	//Original name: WS4-DGR-SUBRTN-RETURN-CODE
	private char subrtnReturnCode = Types.SPACE_CHAR;
	//Original name: FILLER-WS4-DGR-SUBRTN-ERROR-LINE-3
	private String flr4 = ". LAST ROUTINE =";
	//Original name: WS4-DGR-SUBRTN-LAST-RTN
	private String subrtnLastRtn = "";

	//==== METHODS ====
	public void setWs4DgrSubrtnErrorLineBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		mainCallingPgm = MarshalByte.readString(buffer, position, Len.MAIN_CALLING_PGM);
		position += Len.MAIN_CALLING_PGM;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR2);
		position += Len.FLR2;
		subrtnName.setSubrtnName(MarshalByte.readString(buffer, position, Ws4DgrSubrtnName.Len.SUBRTN_NAME));
		position += Ws4DgrSubrtnName.Len.SUBRTN_NAME;
		flr3 = MarshalByte.readString(buffer, position, Len.FLR3);
		position += Len.FLR3;
		subrtnReturnCode = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		flr4 = MarshalByte.readString(buffer, position, Len.FLR4);
		position += Len.FLR4;
		subrtnLastRtn = MarshalByte.readString(buffer, position, Len.SUBRTN_LAST_RTN);
	}

	public byte[] getWs4DgrSubrtnErrorLineBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, mainCallingPgm, Len.MAIN_CALLING_PGM);
		position += Len.MAIN_CALLING_PGM;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, subrtnName.getSubrtnName(), Ws4DgrSubrtnName.Len.SUBRTN_NAME);
		position += Ws4DgrSubrtnName.Len.SUBRTN_NAME;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeChar(buffer, position, subrtnReturnCode);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, subrtnLastRtn, Len.SUBRTN_LAST_RTN);
		return buffer;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setMainCallingPgm(String mainCallingPgm) {
		this.mainCallingPgm = Functions.subString(mainCallingPgm, Len.MAIN_CALLING_PGM);
	}

	public String getMainCallingPgm() {
		return this.mainCallingPgm;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR2);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setFlr3(String flr3) {
		this.flr3 = Functions.subString(flr3, Len.FLR3);
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setSubrtnReturnCode(char subrtnReturnCode) {
		this.subrtnReturnCode = subrtnReturnCode;
	}

	public char getSubrtnReturnCode() {
		return this.subrtnReturnCode;
	}

	public void setFlr4(String flr4) {
		this.flr4 = Functions.subString(flr4, Len.FLR4);
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setSubrtnLastRtn(String subrtnLastRtn) {
		this.subrtnLastRtn = Functions.subString(subrtnLastRtn, Len.SUBRTN_LAST_RTN);
	}

	public String getSubrtnLastRtn() {
		return this.subrtnLastRtn;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 22;
		public static final int MAIN_CALLING_PGM = 8;
		public static final int FLR2 = 49;
		public static final int FLR3 = 16;
		public static final int SUBRTN_RETURN_CODE = 1;
		public static final int FLR4 = 17;
		public static final int SUBRTN_LAST_RTN = 4;
		public static final int WS4_DGR_SUBRTN_ERROR_LINE = MAIN_CALLING_PGM + Ws4DgrSubrtnName.Len.SUBRTN_NAME + SUBRTN_RETURN_CODE + SUBRTN_LAST_RTN
				+ FLR1 + FLR2 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
