/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-ERROR-FOUND-FLAG<br>
 * Variable: SW-ERROR-FOUND-FLAG from program TS548099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwErrorFoundFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NO_ERROR_FOUND = '0';
	public static final char ERROR_FOUND = '1';

	//==== METHODS ====
	public void setSwErrorFoundFlag(char swErrorFoundFlag) {
		this.value = swErrorFoundFlag;
	}

	public char getSwErrorFoundFlag() {
		return this.value;
	}

	public boolean isNoErrorFound() {
		return value == NO_ERROR_FOUND;
	}

	public void setNoErrorFound() {
		value = NO_ERROR_FOUND;
	}

	public boolean isErrorFound() {
		return value == ERROR_FOUND;
	}

	public void setErrorFound() {
		value = ERROR_FOUND;
	}
}
