/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZC06090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXzc06090 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-LIST-ROWS
	private short maxListRows = ((short) 351);
	//Original name: CF-MAX-ERR
	private short maxErr = ((short) 10);
	//Original name: CF-MAX-LOCV-CDS
	private short maxLocvCds = ((short) 10);
	//Original name: CF-SOAP-SERVICE
	private String soapService = "CRSPolicyInformationCallable";
	//Original name: CF-SOAP-OPERATION
	private String soapOperation = "getPolicyTermListByAccount";
	//Original name: CF-CALLABLE-SVC-PROGRAM
	private String callableSvcProgram = "GIICALS";
	//Original name: CF-TRACE-STOR-VALUE
	private String traceStorValue = "GWAO";
	//Original name: CF-PARAGRAPH-NAMES
	private CfParagraphNames paragraphNames = new CfParagraphNames();
	//Original name: CF-WEB-SVC-ID
	private String webSvcId = "XZBKRPOLINF";
	//Original name: CF-WEB-SVC-LKU-PGM
	private String webSvcLkuPgm = "TS571099";
	//Original name: CF-LV-ZLINUX-SRV
	private String lvZlinuxSrv = "03";
	//Original name: CF-CONTAINER-INFO
	private CfContainerInfoXzc06090 containerInfo = new CfContainerInfoXzc06090();

	//==== METHODS ====
	public short getMaxListRows() {
		return this.maxListRows;
	}

	public short getMaxErr() {
		return this.maxErr;
	}

	public short getMaxLocvCds() {
		return this.maxLocvCds;
	}

	public String getSoapService() {
		return this.soapService;
	}

	public String getSoapOperation() {
		return this.soapOperation;
	}

	public String getCallableSvcProgram() {
		return this.callableSvcProgram;
	}

	public String getTraceStorValue() {
		return this.traceStorValue;
	}

	public String getWebSvcId() {
		return this.webSvcId;
	}

	public String getWebSvcLkuPgm() {
		return this.webSvcLkuPgm;
	}

	public String getLvZlinuxSrv() {
		return this.lvZlinuxSrv;
	}

	public CfContainerInfoXzc06090 getContainerInfo() {
		return containerInfo;
	}

	public CfParagraphNames getParagraphNames() {
		return paragraphNames;
	}
}
