/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WaCicsRegionType;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS571099<br>
 * Generated as a class for rule WS.<br>*/
public class Ts571099Data {

	//==== PROPERTIES ====
	//Original name: CF-SP-DEVELOPMENT
	private String cfSpDevelopment = "DEVL";
	//Original name: CF-SP-BETA
	private String cfSpBeta = "BETA";
	//Original name: CF-SP-EDUCATION
	private String cfSpEducation = "EDUC";
	//Original name: CF-SP-MODEL-OFFICE
	private String cfSpModelOffice = "MODL";
	//Original name: CF-SP-TECH-SERVICES
	private String cfSpTechServices = "TECH";
	//Original name: CF-SP-PRODUCTION
	private String cfSpProduction = "CICS";
	//Original name: WA-CICS-REGION-TYPE
	private WaCicsRegionType waCicsRegionType = new WaCicsRegionType();
	//Original name: FILLER-WA-CICS-SYSID
	private String flr5 = DefaultValues.stringVal(Len.FLR5);
	//Original name: WA-QN-PREFIX
	private String waQnPrefix = DefaultValues.stringVal(Len.WA_QN_PREFIX);
	//Original name: WA-QN-SERVICE-MNEMONIC
	private String waQnServiceMnemonic = DefaultValues.stringVal(Len.WA_QN_SERVICE_MNEMONIC);
	//Original name: WA-URI
	private String waUri = DefaultValues.stringVal(Len.WA_URI);
	public static final String WA_URI_NOT_FOUND = "";

	//==== METHODS ====
	public String getCfSpDevelopment() {
		return this.cfSpDevelopment;
	}

	public String getCfSpBeta() {
		return this.cfSpBeta;
	}

	public String getCfSpEducation() {
		return this.cfSpEducation;
	}

	public String getCfSpModelOffice() {
		return this.cfSpModelOffice;
	}

	public String getCfSpTechServices() {
		return this.cfSpTechServices;
	}

	public String getCfSpProduction() {
		return this.cfSpProduction;
	}

	public void setWaCicsSysidFormatted(String data) {
		byte[] buffer = new byte[Len.WA_CICS_SYSID];
		MarshalByte.writeString(buffer, 1, data, Len.WA_CICS_SYSID);
		setWaCicsSysidBytes(buffer, 1);
	}

	public void setWaCicsSysidBytes(byte[] buffer, int offset) {
		int position = offset;
		waCicsRegionType.setWaCicsRegionType(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		flr5 = MarshalByte.readString(buffer, position, Len.FLR5);
	}

	public void setFlr5(String flr5) {
		this.flr5 = Functions.subString(flr5, Len.FLR5);
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getWaQueueNameFormatted() {
		return MarshalByteExt.bufferToStr(getWaQueueNameBytes());
	}

	/**Original name: WA-QUEUE-NAME<br>*/
	public byte[] getWaQueueNameBytes() {
		byte[] buffer = new byte[Len.WA_QUEUE_NAME];
		return getWaQueueNameBytes(buffer, 1);
	}

	public byte[] getWaQueueNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, waQnPrefix, Len.WA_QN_PREFIX);
		position += Len.WA_QN_PREFIX;
		MarshalByte.writeString(buffer, position, waQnServiceMnemonic, Len.WA_QN_SERVICE_MNEMONIC);
		return buffer;
	}

	public void setWaQnPrefix(String waQnPrefix) {
		this.waQnPrefix = Functions.subString(waQnPrefix, Len.WA_QN_PREFIX);
	}

	public String getWaQnPrefix() {
		return this.waQnPrefix;
	}

	public void setWaQnServiceMnemonic(String waQnServiceMnemonic) {
		this.waQnServiceMnemonic = Functions.subString(waQnServiceMnemonic, Len.WA_QN_SERVICE_MNEMONIC);
	}

	public String getWaQnServiceMnemonic() {
		return this.waQnServiceMnemonic;
	}

	public void setWaUri(String waUri) {
		this.waUri = Functions.subString(waUri, Len.WA_URI);
	}

	public void setWaUriFromBuffer(byte[] buffer) {
		waUri = MarshalByte.readString(buffer, 1, Len.WA_URI);
	}

	public String getWaUri() {
		return this.waUri;
	}

	public WaCicsRegionType getWaCicsRegionType() {
		return waCicsRegionType;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA01_SERVICE_MNEMONIC = 12;
		public static final int FLR5 = 3;
		public static final int WA_QN_PREFIX = 4;
		public static final int WA_QN_SERVICE_MNEMONIC = 12;
		public static final int WA_URI = 256;
		public static final int WA_CICS_SYSID = WaCicsRegionType.Len.WA_CICS_REGION_TYPE + FLR5;
		public static final int WA_QUEUE_NAME = WA_QN_PREFIX + WA_QN_SERVICE_MNEMONIC;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
