/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLACT-NOT-TYP<br>
 * Variable: DCLACT-NOT-TYP from copybook XZH00007<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclactNotTyp {

	//==== PROPERTIES ====
	//Original name: ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: ACT-NOT-DES
	private String actNotDes = DefaultValues.stringVal(Len.ACT_NOT_DES);
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;
	//Original name: DSY-ORD-NBR
	private short dsyOrdNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: DOC-DES-LEN
	private short docDesLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: DOC-DES-TEXT
	private String docDesText = DefaultValues.stringVal(Len.DOC_DES_TEXT);
	//Original name: ACT-NOT-PTH-CD
	private String actNotPthCd = DefaultValues.stringVal(Len.ACT_NOT_PTH_CD);
	//Original name: ACT-NOT-PTH-DES
	private String actNotPthDes = DefaultValues.stringVal(Len.ACT_NOT_PTH_DES);

	//==== METHODS ====
	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getActNotTypCdFormatted() {
		return Functions.padBlanks(getActNotTypCd(), Len.ACT_NOT_TYP_CD);
	}

	public void setActNotDes(String actNotDes) {
		this.actNotDes = Functions.subString(actNotDes, Len.ACT_NOT_DES);
	}

	public String getActNotDes() {
		return this.actNotDes;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	public void setDsyOrdNbr(short dsyOrdNbr) {
		this.dsyOrdNbr = dsyOrdNbr;
	}

	public short getDsyOrdNbr() {
		return this.dsyOrdNbr;
	}

	public void setDocDesLen(short docDesLen) {
		this.docDesLen = docDesLen;
	}

	public short getDocDesLen() {
		return this.docDesLen;
	}

	public void setDocDesText(String docDesText) {
		this.docDesText = Functions.subString(docDesText, Len.DOC_DES_TEXT);
	}

	public String getDocDesText() {
		return this.docDesText;
	}

	public void setActNotPthCd(String actNotPthCd) {
		this.actNotPthCd = Functions.subString(actNotPthCd, Len.ACT_NOT_PTH_CD);
	}

	public String getActNotPthCd() {
		return this.actNotPthCd;
	}

	public void setActNotPthDes(String actNotPthDes) {
		this.actNotPthDes = Functions.subString(actNotPthDes, Len.ACT_NOT_PTH_DES);
	}

	public String getActNotPthDes() {
		return this.actNotPthDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_DES = 35;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int DOC_DES_TEXT = 240;
		public static final int ACT_NOT_PTH_CD = 5;
		public static final int ACT_NOT_PTH_DES = 35;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
