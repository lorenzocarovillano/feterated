/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-PARAGRAPH-NAMES<br>
 * Variable: CF-PARAGRAPH-NAMES from program XZC03090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfParagraphNamesXzc03090 {

	//==== PROPERTIES ====
	//Original name: CF-PN-2100
	private String pn2100 = "2100";
	//Original name: CF-PN-2300
	private String pn2300 = "2300";
	//Original name: CF-PN-3220
	private String pn3220 = "3220";
	//Original name: CF-PN-3230
	private String pn3230 = "3230";
	//Original name: CF-PN-3300
	private String pn3300 = "3300";
	//Original name: CF-PN-3500
	private String pn3500 = "3500";

	//==== METHODS ====
	public String getPn2100() {
		return this.pn2100;
	}

	public String getPn2300() {
		return this.pn2300;
	}

	public String getPn3220() {
		return this.pn3220;
	}

	public String getPn3230() {
		return this.pn3230;
	}

	public String getPn3300() {
		return this.pn3300;
	}

	public String getPn3500() {
		return this.pn3500;
	}
}
