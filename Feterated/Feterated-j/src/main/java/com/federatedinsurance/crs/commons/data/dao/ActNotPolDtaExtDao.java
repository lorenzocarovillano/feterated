/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotPolDtaExt;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [ACT_NOT, ACT_NOT_POL, POL_DTA_EXT]
 * 
 */
public class ActNotPolDtaExtDao extends BaseSqlDao<IActNotPolDtaExt> {

	private Cursor actNotCsr;
	private final IRowMapper<IActNotPolDtaExt> fetchActNotCsrRm = buildNamedRowMapper(IActNotPolDtaExt.class, "polNbr", "csrActNbr", "notPrcTs",
			"notEffDt", "polEffDt");

	public ActNotPolDtaExtDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotPolDtaExt> getToClass() {
		return IActNotPolDtaExt.class;
	}

	public DbAccessStatus openActNotCsr(IActNotPolDtaExt iActNotPolDtaExt) {
		actNotCsr = buildQuery("openActNotCsr").bind(iActNotPolDtaExt).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public DbAccessStatus closeActNotCsr() {
		return closeCursor(actNotCsr);
	}

	public IActNotPolDtaExt fetchActNotCsr(IActNotPolDtaExt iActNotPolDtaExt) {
		return fetch(actNotCsr, iActNotPolDtaExt, fetchActNotCsrRm);
	}
}
