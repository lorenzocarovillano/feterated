/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ACT_NOT]
 * 
 */
public interface IActNot extends BaseSqlTo {

	/**
	 * Host Variable CSR-ACT-NBR
	 * 
	 */
	String getCsrActNbr();

	void setCsrActNbr(String csrActNbr);

	/**
	 * Host Variable NOT-PRC-TS
	 * 
	 */
	String getNotPrcTs();

	void setNotPrcTs(String notPrcTs);

	/**
	 * Host Variable ACT-NOT-STA-CD
	 * 
	 */
	String getActNotStaCd();

	void setActNotStaCd(String actNotStaCd);

	/**
	 * Host Variable SA-NOT-PRC-TS-TIME
	 * 
	 */
	String getSaNotPrcTsTime();

	void setSaNotPrcTsTime(String saNotPrcTsTime);

	/**
	 * Host Variable NOT-DT
	 * 
	 */
	String getNotDt();

	void setNotDt(String notDt);

	/**
	 * Host Variable TOT-FEE-AMT
	 * 
	 */
	AfDecimal getTotFeeAmt();

	void setTotFeeAmt(AfDecimal totFeeAmt);

	/**
	 * Nullable property for TOT-FEE-AMT
	 * 
	 */
	AfDecimal getTotFeeAmtObj();

	void setTotFeeAmtObj(AfDecimal totFeeAmtObj);

	/**
	 * Host Variable ST-ABB
	 * 
	 */
	String getStAbb();

	void setStAbb(String stAbb);

	/**
	 * Nullable property for ST-ABB
	 * 
	 */
	String getStAbbObj();

	void setStAbbObj(String stAbbObj);

	/**
	 * Host Variable XZH001-ACT-NOT-TYP-CD
	 * 
	 */
	String getActNotTypCd();

	void setActNotTypCd(String actNotTypCd);

	/**
	 * Host Variable XZH001-ACT-OWN-CLT-ID
	 * 
	 */
	String getActOwnCltId();

	void setActOwnCltId(String actOwnCltId);

	/**
	 * Host Variable XZH001-ACT-OWN-ADR-ID
	 * 
	 */
	String getActOwnAdrId();

	void setActOwnAdrId(String actOwnAdrId);

	/**
	 * Host Variable XZH001-EMP-ID
	 * 
	 */
	String getEmpId();

	void setEmpId(String empId);

	/**
	 * Nullable property for XZH001-EMP-ID
	 * 
	 */
	String getEmpIdObj();

	void setEmpIdObj(String empIdObj);

	/**
	 * Host Variable XZH001-STA-MDF-TS
	 * 
	 */
	String getStaMdfTs();

	void setStaMdfTs(String staMdfTs);

	/**
	 * Host Variable XZH001-PDC-NBR
	 * 
	 */
	String getPdcNbr();

	void setPdcNbr(String pdcNbr);

	/**
	 * Nullable property for XZH001-PDC-NBR
	 * 
	 */
	String getPdcNbrObj();

	void setPdcNbrObj(String pdcNbrObj);

	/**
	 * Host Variable XZH001-PDC-NM
	 * 
	 */
	String getPdcNm();

	void setPdcNm(String pdcNm);

	/**
	 * Nullable property for XZH001-PDC-NM
	 * 
	 */
	String getPdcNmObj();

	void setPdcNmObj(String pdcNmObj);

	/**
	 * Host Variable XZH001-SEG-CD
	 * 
	 */
	String getSegCd();

	void setSegCd(String segCd);

	/**
	 * Nullable property for XZH001-SEG-CD
	 * 
	 */
	String getSegCdObj();

	void setSegCdObj(String segCdObj);

	/**
	 * Host Variable XZH001-ACT-TYP-CD
	 * 
	 */
	String getActTypCd();

	void setActTypCd(String actTypCd);

	/**
	 * Nullable property for XZH001-ACT-TYP-CD
	 * 
	 */
	String getActTypCdObj();

	void setActTypCdObj(String actTypCdObj);

	/**
	 * Host Variable XZH001-CER-HLD-NOT-IND
	 * 
	 */
	char getCerHldNotInd();

	void setCerHldNotInd(char cerHldNotInd);

	/**
	 * Nullable property for XZH001-CER-HLD-NOT-IND
	 * 
	 */
	Character getCerHldNotIndObj();

	void setCerHldNotIndObj(Character cerHldNotIndObj);

	/**
	 * Host Variable XZH001-ADD-CNC-DAY
	 * 
	 */
	short getAddCncDay();

	void setAddCncDay(short addCncDay);

	/**
	 * Nullable property for XZH001-ADD-CNC-DAY
	 * 
	 */
	Short getAddCncDayObj();

	void setAddCncDayObj(Short addCncDayObj);

	/**
	 * Host Variable XZH001-REA-DES
	 * 
	 */
	String getReaDes();

	void setReaDes(String reaDes);

	/**
	 * Nullable property for XZH001-REA-DES
	 * 
	 */
	String getReaDesObj();

	void setReaDesObj(String reaDesObj);
};
