/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9030-ROW<br>
 * Variable: WS-XZ0A9030-ROW from program XZ0B9030<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9030Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA930-MAX-ACT-NOT-TYP-ROWS
	private short maxActNotTypRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA930-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZA930-ACT-NOT-DES
	private String actNotDes = DefaultValues.stringVal(Len.ACT_NOT_DES);
	//Original name: XZA930-DSY-ORD-NBR
	private int dsyOrdNbr = DefaultValues.INT_VAL;
	//Original name: XZA930-DOC-DES
	private String docDes = DefaultValues.stringVal(Len.DOC_DES);
	//Original name: XZA930-ACT-NOT-PTH-CD
	private String actNotPthCd = DefaultValues.stringVal(Len.ACT_NOT_PTH_CD);
	//Original name: XZA930-ACT-NOT-PTH-DES
	private String actNotPthDes = DefaultValues.stringVal(Len.ACT_NOT_PTH_DES);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9030_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9030RowBytes(buf);
	}

	public String getWsXz0a9030RowFormatted() {
		return getActNotTypInfoFormatted();
	}

	public void setWsXz0a9030RowBytes(byte[] buffer) {
		setWsXz0a9030RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9030RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9030_ROW];
		return getWsXz0a9030RowBytes(buffer, 1);
	}

	public void setWsXz0a9030RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotTypInfoBytes(buffer, position);
	}

	public byte[] getWsXz0a9030RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotTypInfoBytes(buffer, position);
		return buffer;
	}

	public String getActNotTypInfoFormatted() {
		return MarshalByteExt.bufferToStr(getActNotTypInfoBytes());
	}

	/**Original name: XZA930-ACT-NOT-TYP-INFO<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0A9030 - BPO COPYBOOK FOR                                    *
	 *             UOW : XZ_GET_ACT_NOT_TYP_LIST                       *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#       DATE        PROG#     DESCRIPTION                    *
	 *  -------   ----------- --------- -----------------------------  *
	 *  TO07614   01/16/2009  E404GCL   NEW                            *
	 *  TO07614   04/07/2009  E404DAP   ADDED DOCUMENT DESCRIPTION     *
	 *  TO0760222 03/23/2010  E404KXS   ADDED PATH FIELDS              *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getActNotTypInfoBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_TYP_INFO];
		return getActNotTypInfoBytes(buffer, 1);
	}

	public void setActNotTypInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		maxActNotTypRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		setActNotTypRowBytes(buffer, position);
	}

	public byte[] getActNotTypInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxActNotTypRows);
		position += Types.SHORT_SIZE;
		getActNotTypRowBytes(buffer, position);
		return buffer;
	}

	public void setMaxActNotTypRows(short maxActNotTypRows) {
		this.maxActNotTypRows = maxActNotTypRows;
	}

	public short getMaxActNotTypRows() {
		return this.maxActNotTypRows;
	}

	public void setActNotTypRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotTypCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		actNotDes = MarshalByte.readString(buffer, position, Len.ACT_NOT_DES);
		position += Len.ACT_NOT_DES;
		dsyOrdNbr = MarshalByte.readInt(buffer, position, Len.DSY_ORD_NBR);
		position += Len.DSY_ORD_NBR;
		docDes = MarshalByte.readString(buffer, position, Len.DOC_DES);
		position += Len.DOC_DES;
		actNotPthCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_PTH_CD);
		position += Len.ACT_NOT_PTH_CD;
		actNotPthDes = MarshalByte.readString(buffer, position, Len.ACT_NOT_PTH_DES);
	}

	public byte[] getActNotTypRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, actNotDes, Len.ACT_NOT_DES);
		position += Len.ACT_NOT_DES;
		MarshalByte.writeInt(buffer, position, dsyOrdNbr, Len.DSY_ORD_NBR);
		position += Len.DSY_ORD_NBR;
		MarshalByte.writeString(buffer, position, docDes, Len.DOC_DES);
		position += Len.DOC_DES;
		MarshalByte.writeString(buffer, position, actNotPthCd, Len.ACT_NOT_PTH_CD);
		position += Len.ACT_NOT_PTH_CD;
		MarshalByte.writeString(buffer, position, actNotPthDes, Len.ACT_NOT_PTH_DES);
		return buffer;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setActNotDes(String actNotDes) {
		this.actNotDes = Functions.subString(actNotDes, Len.ACT_NOT_DES);
	}

	public String getActNotDes() {
		return this.actNotDes;
	}

	public void setDsyOrdNbr(int dsyOrdNbr) {
		this.dsyOrdNbr = dsyOrdNbr;
	}

	public int getDsyOrdNbr() {
		return this.dsyOrdNbr;
	}

	public void setDocDes(String docDes) {
		this.docDes = Functions.subString(docDes, Len.DOC_DES);
	}

	public String getDocDes() {
		return this.docDes;
	}

	public void setActNotPthCd(String actNotPthCd) {
		this.actNotPthCd = Functions.subString(actNotPthCd, Len.ACT_NOT_PTH_CD);
	}

	public String getActNotPthCd() {
		return this.actNotPthCd;
	}

	public void setActNotPthDes(String actNotPthDes) {
		this.actNotPthDes = Functions.subString(actNotPthDes, Len.ACT_NOT_PTH_DES);
	}

	public String getActNotPthDes() {
		return this.actNotPthDes;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9030RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_ACT_NOT_TYP_ROWS = 2;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int ACT_NOT_DES = 35;
		public static final int DSY_ORD_NBR = 5;
		public static final int DOC_DES = 240;
		public static final int ACT_NOT_PTH_CD = 5;
		public static final int ACT_NOT_PTH_DES = 35;
		public static final int ACT_NOT_TYP_ROW = ACT_NOT_TYP_CD + ACT_NOT_DES + DSY_ORD_NBR + DOC_DES + ACT_NOT_PTH_CD + ACT_NOT_PTH_DES;
		public static final int ACT_NOT_TYP_INFO = MAX_ACT_NOT_TYP_ROWS + ACT_NOT_TYP_ROW;
		public static final int WS_XZ0A9030_ROW = ACT_NOT_TYP_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
