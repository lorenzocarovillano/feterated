/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpAccessStatus;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalLokObjDetVDao;
import com.federatedinsurance.crs.commons.data.dao.HalLokObjHdrVDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.ws.DefaultComm;
import com.federatedinsurance.crs.ws.HalrlomgData;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsHalrlomgLinkage1;
import com.federatedinsurance.crs.ws.WsHalrlomgLinkage2;
import com.federatedinsurance.crs.ws.enums.HalrlomgFunction;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;

/**Original name: HALRLOMG<br>
 * <pre>****************************************************************
 * * PROGRAM TITLE - BROWSER INTERFACE - LOCK MANAGER MODULE     **
 * *                                                             **
 * * WRITTEN BY:  CSC                                            **
 * *                                                             **
 * * DATE WRITTEN: DEC 2000                                      **
 * *                                                             **
 * * PLATFORM - HOST                                             **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - MVS COBOL                                        **
 * *                                                             **
 * * PURPOSE - THIS MODULE IS THE MAIN LOCK MANAGER MODULE WHICH **
 * *           HANDLES ALL I/O TO THE LOCKING TABLES WITHIN      **
 * *           SAVANNAH                                          **
 * *                                                             **
 * * PROGRAM INITIATION - CICS LINK FROM HALRLODR OR HALRUPLI    **
 * *                                                             **
 * * DATA ACCESS METHODS - PASSED INFORMATION                    **
 * *                                                             **
 * --------------------------------------------------------------**
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #       DATE     PROG                DESCRIPTION         **
 * * -------- --------   ------  --------------------------------**
 * * SAVANNAH 14 DEC 00  90927   BUILT.                          **
 * * C14128   23 MAY 01  18600   CHANGE THE COMPARISON LITERAL   **
 * *                             FOR THE HLOD-ZAPPED-IND FROM    **
 * *                             'Z' TO 'Y'. ADD THE VARIABLE    **
 * *                             HLOD_ZAPPED_BY TO THE SELECT    **
 * *                             STATEMENT IN SECTION            **
 * *                             0300-CHECK-LOCK-FOR-FETCH.      **
 * *                                                             **
 * * C15321   27 JUL 01  99361   THIS IS TO FACILITATE THE       **
 * *                             REFRESHING OF THE SCREEN WHEN   **
 * *                             THE LOCK GETS ZAPPED BY OTHER   **
 * *                             USER ACCESSING THE SAME DATA.   **
 * *                             THIS IS DONE BY :
 * *                             RESETTING THE HLOD_ZAPPED_IND   **
 * *                             TO 'N', ALSO MOVING SPACES TO   **
 * *                             HLOD_ZAPPED_BY, WS-DEFAULT-TS   **
 * *                             TO HLOD_ZAPPED_TS, IF THE LOCK  **
 * *                             IS FOUND ZAPPED.THIS MAKES THE  **
 * *                             LOCK REFRESHED WITHOUT EXITING  **
 * *                             UOW. THIS IS DONE IN SECTION    **
 * *                             0300-CHECK-LOCK-FOR-FETCH.      **
 * * 16323    01 OCT 01  18448   CALLING MODULES MAY NEED TO     **
 * *                             INTERPRET 'NO SESSION LOCKS FND'**
 * *                             DIFFERENTLY THAN AN ALL OUT     **
 * *                             FAILURE.                        **
 * * 17238    29 OCT 01  18448   INFRA 2.3 LOCKING ENHANCEMENTS. **
 * * 17241    13 NOV 01  03539   REPLACE REFERENCES TO IAP WITH  **
 * *                             COMPARABLE INFRASTRUCTURE CODE. **
 * * 24149    21 JUN 02  18448   IF LOCK ZAPPING IS CALLED AS A  **
 * *                             RESULT OF NON-BROWSER PROCESSING**
 * *                             THEN ENSURE THAT THE HEADER IS  **
 * *                             LOCKED BEFORE ZAPPING THE DETAIL**
 * *                             ROWS.  OTHERWISE, TIMEOUTS MAY  **
 * *                             OCCUR AGAINST THE DETAILS.      **
 * *                             (XPIOCHK->XPIOHAL->HALRUPLI->   **
 * *                              HALRLOMG FOR ZAPPING)          **
 * * 24864   25 JUN 02   18448   DELETE ORPHANED DETAILS DURING  **
 * *                             UNATTENDED LOCK PURGE.          **
 * * 24597   01 JUL 02   18448   CHANGED PROCESSING TO USE UMT   **
 * *  GA                         DURING AN INSTANCE OF A UOW     **
 * *                             EXECUTING IN ORDER TO REDUCE DB2**
 * *                             ACCESS DURING LARGE UOWS.       **
 * * 26294   19 SEP 02   18448   IF HEADER NOT FOUND WHEN CHECKING*
 * *                             LOCK FOR UPDATE, RETURN 'NOT FND'*
 * *                             INSTEAD OF A LOGGABLE ERROR.     *
 *   32098     02MAY03   18448   USE UOW LOCKING TSQ TO AVOID
 *                               EXCESSIVE TCB SWITCHING.
 *   32701     23JUN03   18448   ADD GROUP LOCKING CAPABILITIES.
 *   34504     28AUG03   18448   NEW PROCESSING TO VERIFY LOCKS FOR
 *                               AN INSERT REQUEST.
 *   36295     01DEC03   18448   GROUP LOCKING - PHASE 2
 * ****************************************************************</pre>*/
public class Halrlomg extends BatchProgram {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>---*
	 *  SET UP SQL
	 * ---*</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	//Original name: DEFAULT-COMM
	private DefaultComm defaultComm;
	private HalLokObjHdrVDao halLokObjHdrVDao = new HalLokObjHdrVDao(dbAccessStatus);
	private HalLokObjDetVDao halLokObjDetVDao = new HalLokObjDetVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private HalrlomgData ws = new HalrlomgData();
	private ExecContext execContext = null;
	//Original name: WS-HALRLOMG-LINKAGE-1
	private WsHalrlomgLinkage1 wsHalrlomgLinkage1;
	//Original name: WS-HALRLOMG-LINKAGE-2
	private WsHalrlomgLinkage2 wsHalrlomgLinkage2;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DefaultComm defaultComm, WsHalrlomgLinkage1 wsHalrlomgLinkage1,
			WsHalrlomgLinkage2 wsHalrlomgLinkage2) {
		this.execContext = execContext;
		this.defaultComm = defaultComm;
		this.wsHalrlomgLinkage1 = wsHalrlomgLinkage1;
		this.wsHalrlomgLinkage2 = wsHalrlomgLinkage2;
		mainline();
		mainlineX();
		return 0;
	}

	public static Halrlomg getInstance() {
		return (Programs.getInstance(Halrlomg.class));
	}

	/**Original name: 0000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A) INITIALISE ARRAYS                                         *
	 *  B) RUN MAIN PROCESS TO MATCH ACTION BUFFER ENTRIES TO REQUEST*
	 *  B) RETURN.                                                   *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 0010-INITIALIZE.
		initialize();
		//---*
		//   PERFORM FUNCTION
		//---*
		// COB_CODE: EVALUATE TRUE
		//               WHEN HALRLOMG-ADD-LOCK-ENTRY
		//                    PERFORM 0100-ADD-LOCK-ENTRY
		//               WHEN HALRLOMG-UPDATE-LOCK-ENTRY
		//                    PERFORM 0200-UPDATE-LOCK-ENTRY
		//               WHEN HALRLOMG-CHECK-FETCH-LOCK
		//               WHEN HALRLOMG-CHECK-INSRET-LOCK
		//               WHEN HALRLOMG-VERIFY-INSERT-ACCESS
		//                    PERFORM 0300-CHECK-LOCK-FOR-FETCH
		//               WHEN HALRLOMG-CHECK-UPDATE-LOCK
		//                    PERFORM 0400-CHECK-LOCK-FOR-UPDATE
		//               WHEN HALRLOMG-LIST-SESSION-LOCKS
		//                    PERFORM 0500-LIST-LOCKS-FOR-SESSION
		//               WHEN HALRLOMG-DELETE-LOCK-ENTRY
		//               WHEN HALRLOMG-PURGE-LOCK-ENTRY
		//                    PERFORM 0600-DELETE-LOCK-ENTRY
		//               WHEN HALRLOMG-ZAP-ALL-LOCK-ENTRIES
		//                    PERFORM 0700-ZAP-ALL-LOCK-ENTRIES
		//               WHEN HALRLOMG-UPGRADE-LOCK-ENTRY
		//                    PERFORM 0800-UPGRADE-LOCK-ENTRY
		//           END-EVALUATE.
		switch (wsHalrlomgLinkage2.getFunction().getFunction()) {

		case HalrlomgFunction.ADD_LOCK_ENTRY:// COB_CODE: PERFORM 0100-ADD-LOCK-ENTRY
			addLockEntry();
			break;

		case HalrlomgFunction.UPDATE_LOCK_ENTRY:// COB_CODE: PERFORM 0200-UPDATE-LOCK-ENTRY
			updateLockEntry();
			break;

		case HalrlomgFunction.CHECK_FETCH_LOCK:
		case HalrlomgFunction.CHECK_INSRET_LOCK:
		case HalrlomgFunction.VERIFY_INSERT_ACCESS:// COB_CODE: PERFORM 0300-CHECK-LOCK-FOR-FETCH
			checkLockForFetch();
			break;

		case HalrlomgFunction.CHECK_UPDATE_LOCK:// COB_CODE: PERFORM 0400-CHECK-LOCK-FOR-UPDATE
			checkLockForUpdate();
			break;

		case HalrlomgFunction.LIST_SESSION_LOCKS:// COB_CODE: PERFORM 0500-LIST-LOCKS-FOR-SESSION
			listLocksForSession();
			break;

		case HalrlomgFunction.DELETE_LOCK_ENTRY:
		case HalrlomgFunction.PURGE_LOCK_ENTRY:// COB_CODE: PERFORM 0600-DELETE-LOCK-ENTRY
			deleteLockEntry();
			break;

		case HalrlomgFunction.ZAP_ALL_LOCK_ENTRIES:// COB_CODE: PERFORM 0700-ZAP-ALL-LOCK-ENTRIES
			zapAllLockEntries();
			break;

		case HalrlomgFunction.UPGRADE_LOCK_ENTRY:// COB_CODE: PERFORM 0800-UPGRADE-LOCK-ENTRY
			upgradeLockEntry();
			break;

		default:
			break;
		}
	}

	/**Original name: 0000-MAINLINE-X<br>*/
	private void mainlineX() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 0010-INITIALIZE_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A>  INITIALIZE                                               *
	 * ***************************************************************</pre>*/
	private void initialize() {
		// COB_CODE: INITIALIZE WS-WORKFIELDS.
		initWsWorkfields();
		// COB_CODE: MOVE UBOC-UOW-LOCK-TIMEOUT-INTERVAL TO WS-TMO-VALUE.
		ws.getWsWorkfields().setTmoValue(Trunc.toShort(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocUowLockTimeoutInterval(), 4));
	}

	/**Original name: 0100-ADD-LOCK-ENTRY_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FUNCTION FOR 'ADD_LOCK_ENTRY'                     *
	 *  1) CHECK IF LOCK ALREADY EXISTS:                             *
	 *     - ATTEMPT TO READ DETAILS ROW BY SESSION, TECH AND APPL.  *
	 *     - IF A RECORD IS FOUND ZAPPED - EXIT MODULE               *
	 *  2) UPDATE (TO LOCK) HEADER ROW BY SETTING TIMESTAMP.         *
	 *     - IF NO RECORD FOUND - CREATE ONE.                        *
	 *  3) CREATE DETAILS ROW                                        *
	 *     - IF EXISTS BUT ZAPPED, RESET ZAPPED FIELDS AND TS.       *
	 *     - IF A DETAILS RECORD DOES NOT EXIST, CREATE ONE.         *
	 * ***************************************************************
	 * NO LONGER CHECK IF LOCK ALREADY EXISTS, FOLLOWING SPEC UPDATES*
	 * ***************************************************************</pre>*/
	private void addLockEntry() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE HALRLOMG-TCH-KEY    TO HLOH-TCH-KEY.
		ws.getDclhalLokObjHdr().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID     TO HLOH-APP-ID.
		ws.getDclhalLokObjHdr().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: PERFORM 2000-UPDATE-HDR-TS.
		updateHdrTs();
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                    CONTINUE
		//               WHEN OTHER
		//                   GO TO 0100-ADD-LOCK-ENTRY-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_HDR_V");
			// COB_CODE: MOVE '0100-ADD-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-ADD-LOCK-ENTRY");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_HDR CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_HDR CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-HDR
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjHdr().getDclhalLokObjHdrFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-ADD-LOCK-ENTRY-X
			return;
		}
		// COB_CODE: IF SQLERRD (3) = ZERO
		//               END-EVALUATE
		//           END-IF.
		if (sqlca.getSqlerrd(3) == 0) {
			// COB_CODE: EXEC SQL
			//              INSERT INTO HAL_LOK_OBJ_HDR_V
			//                     ( TCH_KEY,
			//                       APP_ID,
			//                       LAST_ACY_TS )
			//              VALUES ( :HLOH-TCH-KEY,
			//                       :HLOH-APP-ID,
			//                       CURRENT TIMESTAMP )
			//           END-EXEC
			halLokObjHdrVDao.insertRec(ws.getDclhalLokObjHdr().getTchKey(), ws.getDclhalLokObjHdr().getAppId());
			// COB_CODE: EVALUATE TRUE
			//               WHEN ERD-SQL-GOOD
			//                   CONTINUE
			//               WHEN OTHER
			//                   GO TO 0100-ADD-LOCK-ENTRY-X
			//           END-EVALUATE
			switch (sqlca.getErdSqlRed()) {

			case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
				//continue
				break;

			default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
				// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
				// COB_CODE: SET ETRA-DB2-INSERT      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Insert();
				// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR_V' TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_HDR_V");
				// COB_CODE: MOVE '0100-ADD-LOCK-ENTRY'
				//                TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-ADD-LOCK-ENTRY");
				// COB_CODE: MOVE 'INSERT OF HAL_LOK_OBJ_HDR FAILED'
				//                TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INSERT OF HAL_LOK_OBJ_HDR FAILED");
				// COB_CODE: STRING 'LOCK HEADER ROW='
				//                  DCLHAL-LOK-OBJ-HDR
				//                  DELIMITED BY SIZE
				//                  INTO EFAL-OBJ-DATA-KEY
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK HEADER ROW=",
						ws.getDclhalLokObjHdr().getDclhalLokObjHdrFormatted());
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 0100-ADD-LOCK-ENTRY-X
				return;
			}
		}
		// COB_CODE: MOVE UBOC-SESSION-ID      TO HLOD-SESSION-ID.
		ws.getDclhalLokObjDet().setSessionId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocSessionId());
		// COB_CODE: MOVE HALRLOMG-TCH-KEY     TO HLOD-TCH-KEY.
		ws.getDclhalLokObjDet().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID      TO HLOD-APP-ID.
		ws.getDclhalLokObjDet().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: MOVE HALRLOMG-TABLE-NM    TO HLOD-TABLE-NM.
		ws.getDclhalLokObjDet().setTableNm(wsHalrlomgLinkage2.getTableNm());
		// COB_CODE: IF HALRLOMG-LOCK-GROUP-PESSI
		//               MOVE UBOC-LOCK-GROUP  TO HLOD-USERID
		//           ELSE
		//               MOVE UBOC-AUTH-USERID TO HLOD-USERID
		//           END-IF.
		if (wsHalrlomgLinkage2.getLokTypeCd().isLockGroupPessi()) {
			// COB_CODE: MOVE UBOC-LOCK-GROUP  TO HLOD-USERID
			ws.getDclhalLokObjDet().setUserid(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocLockGroup());
		} else {
			// COB_CODE: MOVE UBOC-AUTH-USERID TO HLOD-USERID
			ws.getDclhalLokObjDet().setUserid(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocAuthUserid());
		}
		// COB_CODE: MOVE HALRLOMG-LOK-TYPE-CD TO HLOD-LOK-TYPE-CD.
		ws.getDclhalLokObjDet().setLokTypeCd(wsHalrlomgLinkage2.getLokTypeCd().getLokTypeCd());
		// COB_CODE: MOVE UBOC-MSG-ID          TO HLOD-UPD-BY-MSG-ID.
		ws.getDclhalLokObjDet().setUpdByMsgId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE SPACES               TO HLOD-ZAPPED-BY.
		ws.getDclhalLokObjDet().setZappedBy("");
		// COB_CODE: MOVE WS-DEFAULT-TS        TO HLOD-ZAPPED-TS.
		ws.getDclhalLokObjDet().setZappedTs(ws.getWsWorkAreas().getDefaultTs());
		// COB_CODE: MOVE 'N'                  TO HLOD-ZAPPED-IND.
		ws.getDclhalLokObjDet().setZappedIndFormatted("N");
		//*****************************************************************
		//* THE FOLLOWING CODE IN THIS SECTION WAS TAKEN FROM THE ABOVE  **
		//* EVALUATE FOR ADD RECORD.                                     **
		//*****************************************************************
		// COB_CODE: EXEC SQL
		//              INSERT INTO HAL_LOK_OBJ_DET_V
		//                     ( HLOD_SESSION_ID,
		//                       TCH_KEY,
		//                       APP_ID,
		//                       TABLE_NM,
		//                       USERID,
		//                       LOK_TYPE_CD,
		//                       LAST_ACY_TS,
		//                       HLOD_LOK_TMO_TS,
		//                       HLOD_UPD_BY_MSG_ID,
		//                       HLOD_ZAPPED_BY,
		//                       HLOD_ZAPPED_TS,
		//                       HLOD_ZAPPED_IND )
		//              VALUES ( :HLOD-SESSION-ID,
		//                       :HLOD-TCH-KEY,
		//                       :HLOD-APP-ID,
		//                       :HLOD-TABLE-NM,
		//                       :HLOD-USERID,
		//                       :HLOD-LOK-TYPE-CD,
		//                       CURRENT TIMESTAMP,
		//                       CURRENT TIMESTAMP,
		//                       :HLOD-UPD-BY-MSG-ID,
		//                       :HLOD-ZAPPED-BY,
		//                       :HLOD-ZAPPED-TS,
		//                       :HLOD-ZAPPED-IND   )
		//           END-EXEC.
		halLokObjDetVDao.insertRec(ws.getDclhalLokObjDet());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0100-ADD-LOCK-ENTRY-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-INSERT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Insert();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0100-ADD-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-ADD-LOCK-ENTRY");
			// COB_CODE: MOVE 'INSERT OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INSERT OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-ADD-LOCK-ENTRY-X
			return;
		}
		//*****************************************************************
		//* NOW UPDATE THE TIMESTAMP MY THE EXPIRATION MINUTES.          **
		//*****************************************************************
		// COB_CODE: EXEC SQL UPDATE HAL_LOK_OBJ_DET_V
		//                SET HLOD_LOK_TMO_TS =
		//                       CURRENT TIMESTAMP +
		//                       :WS-TMO-VALUE MINUTES
		//                WHERE HLOD_SESSION_ID = :HLOD-SESSION-ID
		//                AND   TCH_KEY    = :HLOD-TCH-KEY
		//                AND   APP_ID     = :HLOD-APP-ID
		//           END-EXEC.
		halLokObjDetVDao.updateRec(ws.getWsWorkfields().getTmoValue(), ws.getDclhalLokObjDet().getSessionId(), ws.getDclhalLokObjDet().getTchKey(),
				ws.getDclhalLokObjDet().getAppId());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0100-ADD-LOCK-ENTRY-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-INSERT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Insert();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0100-ADD-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0100-ADD-LOCK-ENTRY");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0100-ADD-LOCK-ENTRY-X
			return;
		}
		//** WRITE UMT ROW FOR UOW MSG ID, TECH KEY, AND APP ID.
		//**  PERFORM 1000-WRITE-LOCK-KEY-UMT-ROW.
		// COB_CODE: PERFORM 1000-WRITE-LOCK-KEY-TSQ-ROW.
		writeLockKeyTsqRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0100-ADD-LOCK-ENTRY-X
		//           END-IF.
		if (wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0100-ADD-LOCK-ENTRY-X
			return;
		}
	}

	/**Original name: 0200-UPDATE-LOCK-ENTRY_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FUNCTION FOR 'UPDATE_LOCK_ENTRY'                  *
	 *  NOTE: HEADER ALREADY LOCKED/CREATED BY CHECK_LOCK_FOR_UPDATE *
	 *  1) UPDATE DETAILS FOR CORR. SESSION/TECH/APPL BY SETTING TS. *
	 *     - IF ERROR, SET SUCCESS-UND TO 'N'.                       *
	 *  2) UPDATE ALL OTHER LOCKS ON DETAILS THAT FOR OTHER SESSIONS *
	 *     - SET ZAPPED FIELDS.                                      *
	 * ***************************************************************</pre>*/
	private void updateLockEntry() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-SESSION-ID      TO HLOD-SESSION-ID
		ws.getDclhalLokObjDet().setSessionId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocSessionId());
		// COB_CODE: MOVE HALRLOMG-TCH-KEY     TO HLOD-TCH-KEY
		ws.getDclhalLokObjDet().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID      TO HLOD-APP-ID
		ws.getDclhalLokObjDet().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: MOVE UBOC-MSG-ID          TO HLOD-UPD-BY-MSG-ID.
		ws.getDclhalLokObjDet().setUpdByMsgId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: EXEC SQL UPDATE HAL_LOK_OBJ_DET_V
		//                SET LAST_ACY_TS =
		//                    CURRENT TIMESTAMP,
		//                    HLOD_LOK_TMO_TS =
		//                    CURRENT TIMESTAMP +
		//                    :WS-TMO-VALUE MINUTES,
		//                    HLOD_UPD_BY_MSG_ID =
		//                    :HLOD-UPD-BY-MSG-ID
		//                WHERE HLOD_SESSION_ID = :HLOD-SESSION-ID
		//                AND   TCH_KEY    = :HLOD-TCH-KEY
		//                AND   APP_ID     = :HLOD-APP-ID
		//           END-EXEC.
		halLokObjDetVDao.updateRec1(ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   SET HALRLOMG-FOUND TO TRUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0200-UPDATE-LOCK-ENTRY-X
		//               WHEN OTHER
		//                   GO TO 0200-UPDATE-LOCK-ENTRY-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET HALRLOMG-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setFound();
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET HALRLOMG-NOT-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setNotFound();
			// COB_CODE: GO TO 0200-UPDATE-LOCK-ENTRY-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0200-UPDATE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0200-UPDATE-LOCK-ENTRY");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0200-UPDATE-LOCK-ENTRY-X
			return;
		}
		// COB_CODE: EXEC SQL
		//             DECLARE WS-CURSOR1 CURSOR FOR
		//                 SELECT HLOD_SESSION_ID,
		//                        TCH_KEY,
		//                        APP_ID
		//                 FROM   HAL_LOK_OBJ_DET_V
		//                 WHERE  TCH_KEY = :HLOD-TCH-KEY
		//                 FOR READ ONLY
		//           END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
		// COB_CODE: EXEC SQL
		//               OPEN WS-CURSOR1
		//           END-EXEC.
		halLokObjDetVDao.openWsCursor12(ws.getDclhalLokObjDet().getTchKey());
		// COB_CODE: EVALUATE TRUE
		//               WHEN NOT ERD-SQL-GOOD
		//                   GO TO 0200-UPDATE-LOCK-ENTRY-X
		//           END-EVALUATE.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0200-UPDATE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0200-UPDATE-LOCK-ENTRY");
			// COB_CODE: MOVE 'OPEN OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0200-UPDATE-LOCK-ENTRY-X
			return;
		}
		// COB_CODE: SET WS-NOT-END-OF-CURSOR1 TO TRUE.
		ws.getWsWorkfields().getEndOfCursor1Sw().setNotEndOfCursor1();
		// COB_CODE: PERFORM 0210-FETCH-CURSOR1
		//             UNTIL NOT ERD-SQL-GOOD
		//                OR WS-END-OF-CURSOR1.
		while (!(!sqlca.isErdSqlGood() || ws.getWsWorkfields().getEndOfCursor1Sw().isEndOfCursor1())) {
			fetchCursor1();
		}
		// COB_CODE: EXEC SQL
		//               CLOSE WS-CURSOR1
		//           END-EXEC.
		halLokObjDetVDao.closeWsCursor12();
		// COB_CODE: EVALUATE TRUE
		//               WHEN NOT ERD-SQL-GOOD
		//                   GO TO 0200-UPDATE-LOCK-ENTRY-X
		//           END-EVALUATE.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0200-UPDATE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0200-UPDATE-LOCK-ENTRY");
			// COB_CODE: MOVE 'CLOSE OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0200-UPDATE-LOCK-ENTRY-X
			return;
		}
	}

	/**Original name: 0210-FETCH-CURSOR1_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FOR CURSOR 1                                      *
	 * ***************************************************************</pre>*/
	private void fetchCursor1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//            FETCH WS-CURSOR1
		//            INTO :HLOD-SESSION-ID,
		//                 :HLOD-TCH-KEY,
		//                 :HLOD-APP-ID
		//           END-EXEC.
		halLokObjDetVDao.fetchWsCursor12(ws.getDclhalLokObjDet());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0210-FETCH-CURSOR1-X
		//               WHEN OTHER
		//                   GO TO 0210-FETCH-CURSOR1-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CURSOR1 TO TRUE
			ws.getWsWorkfields().getEndOfCursor1Sw().setEndOfCursor1();
			// COB_CODE: GO TO 0210-FETCH-CURSOR1-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0210-FETCH-CURSOR1'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0210-FETCH-CURSOR1");
			// COB_CODE: MOVE 'FETCH OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0210-FETCH-CURSOR1-X
			return;
		}
		// COB_CODE: IF HLOD-SESSION-ID   = UBOC-SESSION-ID
		//               GO TO 0210-FETCH-CURSOR1-X
		//           END-IF.
		if (Conditions.eq(ws.getDclhalLokObjDet().getSessionId(), wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocSessionId())) {
			// COB_CODE: GO TO 0210-FETCH-CURSOR1-X
			return;
		}
		// COB_CODE: MOVE UBOC-AUTH-USERID    TO HLOD-ZAPPED-BY.
		ws.getDclhalLokObjDet().setZappedBy(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: MOVE 'Y'                 TO HLOD-ZAPPED-IND.
		ws.getDclhalLokObjDet().setZappedIndFormatted("Y");
		// COB_CODE: EXEC SQL UPDATE HAL_LOK_OBJ_DET_V
		//                SET HLOD_ZAPPED_TS =
		//                    CURRENT TIMESTAMP,
		//                    HLOD_ZAPPED_BY   =
		//                    :HLOD-ZAPPED-BY,
		//                    HLOD_ZAPPED_IND =
		//                    :HLOD-ZAPPED-IND
		//                WHERE HLOD_SESSION_ID = :HLOD-SESSION-ID
		//                AND   TCH_KEY    = :HLOD-TCH-KEY
		//                AND   APP_ID     = :HLOD-APP-ID
		//           END-EXEC.
		halLokObjDetVDao.updateRec2(ws.getDclhalLokObjDet());
		// COB_CODE: EVALUATE TRUE
		//               WHEN NOT ERD-SQL-GOOD
		//                   GO TO 0210-FETCH-CURSOR1-X
		//           END-EVALUATE.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0210-FETCH-CURSOR-1'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0210-FETCH-CURSOR-1");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: SET WS-END-OF-CURSOR1 TO TRUE
			ws.getWsWorkfields().getEndOfCursor1Sw().setEndOfCursor1();
			// COB_CODE: GO TO 0210-FETCH-CURSOR1-X
			return;
		}
	}

	/**Original name: 0300-CHECK-LOCK-FOR-FETCH_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FUNCTION FOR 'CHECK_LOCK_FOR_FETCH'               *
	 *  1) CHECK DETAILS RECORD EXISTS FOR SESSION/TECH/APPL AND NOT *
	 *     ZAPPED (='N').                                            *
	 *     - IF EXISTS SET SUCCESS-IND TO 'FOUND'.                   *
	 *     - OTHERWISE SET SUCCESS-IND TO 'NOTFOUND'.                *
	 * ***************************************************************
	 * *CHECK FOR ROW IN UMT FOR UOW MSG ID, TECH KEY, AND APP ID.
	 * *IF FOUND, SET HALRLOMG-FOUND TO TRUE AND RETURN.
	 * *IF NOT FOUND, PROCEED AS NORMAL.
	 * *   PERFORM 1010-READ-LOCK-KEY-UMT-ROW.</pre>*/
	private void checkLockForFetch() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 1010-READ-LOCK-KEY-TSQ-ROW.
		readLockKeyTsqRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0300-CHECK-LOCK-FOR-FETCH-X
		//           END-IF.
		if (wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0300-CHECK-LOCK-FOR-FETCH-X
			return;
		}
		// COB_CODE: IF HALOUKRP-OKAY
		//               GO TO 0300-CHECK-LOCK-FOR-FETCH-X
		//           END-IF.
		if (ws.getHallukrp().getReturnCode().isHaloukrpOkay()) {
			// COB_CODE: GO TO 0300-CHECK-LOCK-FOR-FETCH-X
			return;
		}
		// COB_CODE: MOVE UBOC-SESSION-ID    TO HLOD-SESSION-ID.
		ws.getDclhalLokObjDet().setSessionId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocSessionId());
		// COB_CODE: MOVE HALRLOMG-TCH-KEY   TO HLOD-TCH-KEY.
		ws.getDclhalLokObjDet().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID    TO HLOD-APP-ID.
		ws.getDclhalLokObjDet().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: EXEC SQL
		//               SELECT USERID,
		//                      HLOD_ZAPPED_IND,
		//                      LOK_TYPE_CD,
		//                      HLOD_UPD_BY_MSG_ID,
		//                      HLOD_ZAPPED_BY,
		//                      HLOD_ZAPPED_TS
		//               INTO  :HLOD-USERID,
		//                     :HLOD-ZAPPED-IND,
		//                     :HLOD-LOK-TYPE-CD,
		//                     :HLOD-UPD-BY-MSG-ID,
		//                     :HLOD-ZAPPED-BY,
		//                     :HLOD-ZAPPED-TS
		//               FROM   HAL_LOK_OBJ_DET_V
		//               WHERE  HLOD_SESSION_ID = :HLOD-SESSION-ID
		//               AND    TCH_KEY    = :HLOD-TCH-KEY
		//               AND    APP_ID     = :HLOD-APP-ID
		//           END-EXEC.
		halLokObjDetVDao.selectRec(ws.getDclhalLokObjDet().getSessionId(), ws.getDclhalLokObjDet().getTchKey(), ws.getDclhalLokObjDet().getAppId(),
				ws.getDclhalLokObjDet());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   END-IF
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0300-CHECK-LOCK-FOR-FETCH-X
		//               WHEN OTHER
		//                   GO TO 0300-CHECK-LOCK-FOR-FETCH-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET HALRLOMG-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setFound();
			//            IF HLOD-ZAPPED-IND   = 'Z'
			// COB_CODE: IF HLOD-ZAPPED-IND   = 'Y'
			//               CONTINUE
			//           ELSE
			//               END-IF
			//           END-IF
			if (ws.getDclhalLokObjDet().getZappedInd() == 'Y') {
				// COB_CODE: SET HALRLOMG-ZAPPED TO TRUE
				wsHalrlomgLinkage2.getSuccessInd().setZapped();
				// COB_CODE: IF HALRLOMG-CHECK-INSRET-LOCK
				//             OR HALRLOMG-VERIFY-INSERT-ACCESS
				//               GO TO 0300-CHECK-LOCK-FOR-FETCH-X
				//           END-IF
				if (wsHalrlomgLinkage2.getFunction().isCheckInsretLock() || wsHalrlomgLinkage2.getFunction().isVerifyInsertAccess()) {
					// COB_CODE: MOVE HLOD-ZAPPED-BY
					//             TO HALRLOMG-ZAPPED-BY-USERID
					wsHalrlomgLinkage2.setZappedByUserid(ws.getDclhalLokObjDet().getZappedBy());
					// COB_CODE: GO TO 0300-CHECK-LOCK-FOR-FETCH-X
					return;
				}
				// COB_CODE: CONTINUE
				//continue
			} else {
				// COB_CODE: PERFORM 0305-CHECK-LOCK-TYPE
				checkLockType();
				//*               IF  HLOD-ZAPPED-IND   = 'N'
				//*               AND UBOC-MSG-ID = HLOD-UPD-BY-MSG-ID
				// COB_CODE:                  IF (HALRLOMG-FOUND
				//                                AND UBOC-MSG-ID = HLOD-UPD-BY-MSG-ID)
				//                              OR HALRLOMG-TYPE-MISMATCH
				//           *** WRITE UMT ROW FOR UOW MSG ID, TECH KEY, AND APP ID.
				//           ***                  PERFORM 1000-WRITE-LOCK-KEY-UMT-ROW
				//                                GO TO 0300-CHECK-LOCK-FOR-FETCH-X
				//                            END-IF
				if (wsHalrlomgLinkage2.getSuccessInd().isFound()
						&& Conditions.eq(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocMsgId(), ws.getDclhalLokObjDet().getUpdByMsgId())
						|| wsHalrlomgLinkage2.getSuccessInd().isTypeMismatch()) {
					//** WRITE UMT ROW FOR UOW MSG ID, TECH KEY, AND APP ID.
					//**                  PERFORM 1000-WRITE-LOCK-KEY-UMT-ROW
					// COB_CODE: PERFORM 1000-WRITE-LOCK-KEY-TSQ-ROW
					writeLockKeyTsqRow();
					// COB_CODE: GO TO 0300-CHECK-LOCK-FOR-FETCH-X
					return;
				}
			}
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET HALRLOMG-NOT-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setNotFound();
			// COB_CODE: GO TO 0300-CHECK-LOCK-FOR-FETCH-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0300-CHECK-LOCK-FOR-FETCH'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0300-CHECK-LOCK-FOR-FETCH");
			// COB_CODE: MOVE 'SELECT OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0300-CHECK-LOCK-FOR-FETCH-X
			return;
		}
		// COB_CODE: MOVE HALRLOMG-TCH-KEY   TO HLOH-TCH-KEY.
		ws.getDclhalLokObjHdr().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID    TO HLOH-APP-ID.
		ws.getDclhalLokObjHdr().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: PERFORM 2000-UPDATE-HDR-TS.
		updateHdrTs();
		// COB_CODE: EVALUATE TRUE
		//               WHEN NOT ERD-SQL-GOOD
		//                   GO TO 0300-CHECK-LOCK-FOR-FETCH-X
		//           END-EVALUATE.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_HDR_V");
			// COB_CODE: MOVE '0300-CHECK-LOCK-FOR-FETCH'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0300-CHECK-LOCK-FOR-FETCH");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_HDR CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_HDR CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-HDR
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjHdr().getDclhalLokObjHdrFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0300-CHECK-LOCK-FOR-FETCH-X
			return;
		}
		// COB_CODE: MOVE UBOC-MSG-ID TO HLOD-UPD-BY-MSG-ID.
		ws.getDclhalLokObjDet().setUpdByMsgId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocMsgId());
		//    EXEC SQL UPDATE HAL_LOK_OBJ_DET_V
		//         SET LAST_ACY_TS =
		//             CURRENT TIMESTAMP,
		//             HLOD_LOK_TMO_TS =
		//             CURRENT TIMESTAMP +
		//             :WS-TMO-VALUE MINUTES,
		//             HLOD_UPD_BY_MSG_ID =
		//             :HLOD-UPD-BY-MSG-ID
		//         WHERE HLOD_SESSION_ID = :HLOD-SESSION-ID
		//         AND   TCH_KEY    = :HLOD-TCH-KEY
		//         AND   APP_ID     = :HLOD-APP-ID
		//    END-EXEC.
		//* 'UNZAP' THE LOCK TO ALLOW A RE-FETCHING OF THE REQUESTED DATA.
		// COB_CODE: IF HALRLOMG-ZAPPED
		//              MOVE WS-DEFAULT-TS   TO  HLOD-ZAPPED-TS
		//           END-IF.
		if (wsHalrlomgLinkage2.getSuccessInd().isZapped()) {
			// COB_CODE: MOVE 'N'             TO  HLOD-ZAPPED-IND
			ws.getDclhalLokObjDet().setZappedIndFormatted("N");
			// COB_CODE: MOVE SPACES          TO  HLOD-ZAPPED-BY
			ws.getDclhalLokObjDet().setZappedBy("");
			// COB_CODE: MOVE WS-DEFAULT-TS   TO  HLOD-ZAPPED-TS
			ws.getDclhalLokObjDet().setZappedTs(ws.getWsWorkAreas().getDefaultTs());
		}
		// COB_CODE: PERFORM 0310-UPDT-LOK-OBJ-DET
		updtLokObjDet();
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        END-IF
		//                    WHEN OTHER
		//           *        WHEN NOT ERD-SQL-GOOD
		//                        GO TO 0300-CHECK-LOCK-FOR-FETCH-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE:              IF HALRLOMG-ZAPPED
			//           ***C15321                SET  HALRLOMG-FOUND   TO TRUE
			//                            PERFORM 0305-CHECK-LOCK-TYPE
			//                        END-IF
			if (wsHalrlomgLinkage2.getSuccessInd().isZapped()) {
				//**C15321                SET  HALRLOMG-FOUND   TO TRUE
				// COB_CODE: PERFORM 0305-CHECK-LOCK-TYPE
				checkLockType();
			}
			break;

		default://        WHEN NOT ERD-SQL-GOOD
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0300-CHECK-LOCK-FOR-FETCH'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0300-CHECK-LOCK-FOR-FETCH");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0300-CHECK-LOCK-FOR-FETCH-X
			return;
		}
		//** WRITE UMT ROW FOR UOW MSG ID, TECH KEY, AND APP ID.
		//**  PERFORM 1000-WRITE-LOCK-KEY-UMT-ROW.
		// COB_CODE: PERFORM 1000-WRITE-LOCK-KEY-TSQ-ROW.
		writeLockKeyTsqRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0300-CHECK-LOCK-FOR-FETCH-X
		//           END-IF.
		if (wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0300-CHECK-LOCK-FOR-FETCH-X
			return;
		}
	}

	/**Original name: 0305-CHECK-LOCK-TYPE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  SEE IF THE REQUESTED LOCK TYPE IS THE SAME AS THE EXISTING LOCK
	 *  TYPE.
	 * *****************************************************************</pre>*/
	private void checkLockType() {
		// COB_CODE: IF HALRLOMG-LOK-TYPE-CD EQUAL HLOD-LOK-TYPE-CD
		//               GO TO 0305-CHECK-LOCK-TYPE-X
		//           END-IF.
		if (wsHalrlomgLinkage2.getLokTypeCd().getLokTypeCd() == ws.getDclhalLokObjDet().getLokTypeCd()) {
			// COB_CODE: SET HALRLOMG-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setFound();
			// COB_CODE: GO TO 0305-CHECK-LOCK-TYPE-X
			return;
		}
		// COB_CODE: MOVE HALRLOMG-LOK-TYPE-CD TO WS-REQUESTED-LOCK-TYPE.
		ws.getWsWorkfields().setRequestedLockType(wsHalrlomgLinkage2.getLokTypeCd().getLokTypeCd());
		// COB_CODE: MOVE HLOD-LOK-TYPE-CD TO WS-EXISTING-LOCK-TYPE.
		ws.getWsWorkfields().setExistingLockType(ws.getDclhalLokObjDet().getLokTypeCd());
		// COB_CODE: PERFORM 1100-COMPARE-LOCK-TYPE.
		compareLockType();
	}

	/**Original name: 0310-UPDT-LOK-OBJ-DET_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  THIS UPDATES THE HAL_LOK_OBJ_DET_V TABLE.                    *
	 * ***************************************************************</pre>*/
	private void updtLokObjDet() {
		// COB_CODE: EXEC SQL UPDATE HAL_LOK_OBJ_DET_V
		//                SET LAST_ACY_TS =
		//                    CURRENT TIMESTAMP,
		//                    HLOD_LOK_TMO_TS =
		//                    CURRENT TIMESTAMP +
		//                    :WS-TMO-VALUE MINUTES,
		//                    HLOD_UPD_BY_MSG_ID =
		//                    :HLOD-UPD-BY-MSG-ID,
		//                    HLOD_ZAPPED_IND =
		//                    :HLOD-ZAPPED-IND,
		//                    HLOD_ZAPPED_BY =
		//                    :HLOD-ZAPPED-BY,
		//                    HLOD_ZAPPED_TS =
		//                    :HLOD-ZAPPED-TS
		//                WHERE HLOD_SESSION_ID = :HLOD-SESSION-ID
		//                AND   TCH_KEY         = :HLOD-TCH-KEY
		//                AND   APP_ID          = :HLOD-APP-ID
		//           END-EXEC.
		halLokObjDetVDao.updateRec3(ws);
	}

	/**Original name: 0400-CHECK-LOCK-FOR-UPDATE_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FUNCTION FOR 'CHECK_LOCK_FOR_UPDATE'              *
	 *  1) ATTEMPT TO LOCK HEADER                                    *
	 *     - IF UNSUCCESSFUL SET SUCCESS-IND TO 'N' AND EXIT.        *
	 *  2) READ DETAILS CHECKING IF THERE IS A 'PESSIMISTIC' OR      *
	 *     'OPTIMISTIC' LOCK BY SESSION/TECH/APPL.                   *
	 *     - IF NO RECORD FOUND SET SUCCESS-IND TO 'NOTFOUND'        *
	 *     - IF RECORD FOUND BUT ZAPPED SET SUCCESS-IND TO 'ZAPPED'  *
	 *     - IF FOUND AND NOT ZAPPED SET SUCCESS-IND TO 'FOUND'      *
	 * ***************************************************************
	 * *CHECK FOR ROW IN UMT FOR SESSION ID, TECH KEY, AND APP ID.
	 * *IF FOUND, SET HALRLOMG-FOUND TO TRUE AND RETURN.
	 * *IF NOT FOUND, PROCEED AS NORMAL.
	 * *   PERFORM 1010-READ-LOCK-KEY-UMT-ROW.</pre>*/
	private void checkLockForUpdate() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 1010-READ-LOCK-KEY-TSQ-ROW.
		readLockKeyTsqRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
		//           END-IF.
		if (wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
			return;
		}
		// COB_CODE:      IF HALOUKRP-OKAY
		//           ******   SET HALRLOMG-FOUND TO TRUE
		//                    GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
		//                END-IF.
		if (ws.getHallukrp().getReturnCode().isHaloukrpOkay()) {
			//*****   SET HALRLOMG-FOUND TO TRUE
			// COB_CODE: GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
			return;
		}
		// COB_CODE: MOVE HALRLOMG-TCH-KEY   TO HLOH-TCH-KEY
		ws.getDclhalLokObjHdr().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID    TO HLOH-APP-ID
		ws.getDclhalLokObjHdr().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: PERFORM 2000-UPDATE-HDR-TS.
		updateHdrTs();
		//*       WHEN NOT ERD-SQL-GOOD
		// COB_CODE:      EVALUATE TRUE
		//           **       WHEN NOT ERD-SQL-GOOD
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//                        GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
		//                    WHEN OTHER
		//                        GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET HALRLOMG-NOT-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setNotFound();
			// COB_CODE: GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_HDR_V");
			// COB_CODE: MOVE '0400-CHECK-LOCK-FOR-UPDATE'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0400-CHECK-LOCK-FOR-UPDATE");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_HDR CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_HDR CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-HDR
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjHdr().getDclhalLokObjHdrFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
			return;
		}
		// COB_CODE: IF SQLERRD (3) = ZERO
		//               GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
		//           END-IF.
		if (sqlca.getSqlerrd(3) == 0) {
			// COB_CODE: SET HALRLOMG-NOT-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setNotFound();
			// COB_CODE: GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
			return;
		}
		// COB_CODE: MOVE UBOC-SESSION-ID    TO HLOD-SESSION-ID.
		ws.getDclhalLokObjDet().setSessionId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocSessionId());
		// COB_CODE: MOVE HALRLOMG-TCH-KEY   TO HLOD-TCH-KEY.
		ws.getDclhalLokObjDet().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID    TO HLOD-APP-ID.
		ws.getDclhalLokObjDet().setAppId(wsHalrlomgLinkage2.getAppId());
		//**      AND    LOK_TYPE_CD IN ('P', 'O')
		// COB_CODE:      EXEC SQL
		//                    SELECT USERID,
		//                           LOK_TYPE_CD,
		//                           HLOD_ZAPPED_IND,
		//                           HLOD_ZAPPED_BY
		//                    INTO  :HLOD-USERID,
		//                          :HLOD-LOK-TYPE-CD,
		//                          :HLOD-ZAPPED-IND,
		//                          :HLOD-ZAPPED-BY
		//                    FROM   HAL_LOK_OBJ_DET_V
		//                    WHERE  HLOD_SESSION_ID = :HLOD-SESSION-ID
		//                    AND    TCH_KEY    = :HLOD-TCH-KEY
		//                    AND    APP_ID     = :HLOD-APP-ID
		//                    AND    LOK_TYPE_CD IN ('P', 'O', 'G')
		//           ***      AND    LOK_TYPE_CD IN ('P', 'O')
		//                END-EXEC.
		halLokObjDetVDao.selectRec1(ws.getDclhalLokObjDet().getSessionId(), ws.getDclhalLokObjDet().getTchKey(), ws.getDclhalLokObjDet().getAppId(),
				ws.getDclhalLokObjDet());
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//           **           MOVE HLOD-LOK-TYPE-CD   TO HALRLOMG-LOK-TYPE-CD
		//                        GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
		//                    WHEN ERD-SQL-NOT-FOUND
		//                        GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
		//                    WHEN OTHER
		//                        GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD://*           MOVE HLOD-LOK-TYPE-CD   TO HALRLOMG-LOK-TYPE-CD
			// COB_CODE:              IF HLOD-ZAPPED-IND   = 'Y'
			//                              TO HALRLOMG-ZAPPED-BY-USERID
			//                        ELSE
			//           **               SET HALRLOMG-FOUND TO TRUE
			//                            PERFORM 1000-WRITE-LOCK-KEY-TSQ-ROW
			//                        END-IF
			if (ws.getDclhalLokObjDet().getZappedInd() == 'Y') {
				// COB_CODE: SET HALRLOMG-ZAPPED TO TRUE
				wsHalrlomgLinkage2.getSuccessInd().setZapped();
				// COB_CODE: MOVE HLOD-ZAPPED-BY
				//             TO HALRLOMG-ZAPPED-BY-USERID
				wsHalrlomgLinkage2.setZappedByUserid(ws.getDclhalLokObjDet().getZappedBy());
			} else {
				//*               SET HALRLOMG-FOUND TO TRUE
				// COB_CODE: PERFORM 0305-CHECK-LOCK-TYPE
				checkLockType();
				//* WRITE UMT ROW FOR SESSION ID, TECH KEY, AND APP ID
				//*               PERFORM 1000-WRITE-LOCK-KEY-UMT-ROW
				// COB_CODE: PERFORM 1000-WRITE-LOCK-KEY-TSQ-ROW
				writeLockKeyTsqRow();
			}
			// COB_CODE: GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
			return;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET HALRLOMG-NOT-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setNotFound();
			// COB_CODE: GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0400-CHECK-LOCK-FOR-UPDATE'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0400-CHECK-LOCK-FOR-UPDATE");
			// COB_CODE: MOVE 'SELECT OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0400-CHECK-LOCK-FOR-UPDATE-X
			return;
		}
	}

	/**Original name: 0500-LIST-LOCKS-FOR-SESSION_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FUNCTION FOR 'LIST_LOCKS_FOR_SESSION'             *
	 *  1) READ ALL DETAIL RECORDS FOR SESSION-ID.                   *
	 *  2) FOR EACH RECORD FOUND, WRITE DETAILS TO TSQ.              *
	 * ***************************************************************</pre>*/
	private void listLocksForSession() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-SESSION-ID TO HLOD-SESSION-ID.
		ws.getDclhalLokObjDet().setSessionId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocSessionId());
		// COB_CODE: EXEC SQL
		//             DECLARE WS-CURSOR2 CURSOR FOR
		//                 SELECT HLOD_SESSION_ID,
		//                        TCH_KEY,
		//                        APP_ID,
		//                        TABLE_NM,
		//                        USERID,
		//                        LOK_TYPE_CD
		//                 FROM   HAL_LOK_OBJ_DET_V
		//                 WHERE  HLOD_SESSION_ID = :HLOD-SESSION-ID
		//                 FOR READ ONLY
		//           END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
		// COB_CODE: EXEC SQL
		//               OPEN WS-CURSOR2
		//           END-EXEC.
		halLokObjDetVDao.openWsCursor2(ws.getDclhalLokObjDet().getSessionId());
		// COB_CODE: EVALUATE TRUE
		//               WHEN NOT ERD-SQL-GOOD
		//                   GO TO 0500-LIST-LOCKS-FOR-SESSION-X
		//           END-EVALUATE.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0500-LIST-LOCKS-FOR-SESSION'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0500-LIST-LOCKS-FOR-SESSION");
			// COB_CODE: MOVE 'OPEN OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0500-LIST-LOCKS-FOR-SESSION-X
			return;
		}
		// COB_CODE: SET HALRLOMG-NOT-FOUND TO TRUE.
		wsHalrlomgLinkage2.getSuccessInd().setNotFound();
		// COB_CODE: SET WS-NOT-END-OF-CURSOR2 TO TRUE.
		ws.getWsWorkfields().getEndOfCursor2Sw().setNotEndOfCursor2();
		// COB_CODE: PERFORM 0510-FETCH-CURSOR2
		//             UNTIL NOT ERD-SQL-GOOD
		//                OR WS-END-OF-CURSOR2.
		while (!(!sqlca.isErdSqlGood() || ws.getWsWorkfields().getEndOfCursor2Sw().isEndOfCursor2())) {
			fetchCursor2();
		}
		// COB_CODE: EXEC SQL
		//               CLOSE WS-CURSOR2
		//           END-EXEC.
		halLokObjDetVDao.closeWsCursor2();
		// COB_CODE: EVALUATE TRUE
		//               WHEN NOT ERD-SQL-GOOD
		//                   GO TO 0500-LIST-LOCKS-FOR-SESSION-X
		//           END-EVALUATE.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0500-LIST-LOCKS-FOR-SESSION'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0500-LIST-LOCKS-FOR-SESSION");
			// COB_CODE: MOVE 'CLOSE OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0500-LIST-LOCKS-FOR-SESSION-X
			return;
		}
	}

	/**Original name: 0510-FETCH-CURSOR2_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FOR CURSOR 2                                      *
	 * ***************************************************************</pre>*/
	private void fetchCursor2() {
		ConcatUtil concatUtil = null;
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC SQL
		//            FETCH WS-CURSOR2
		//            INTO :WS-SESSION-ID,
		//                 :WS-TCH-KEY,
		//                 :WS-APP-ID,
		//                 :WS-TABLE-NM,
		//                 :WS-USER-ID,
		//                 :WS-LOK-TYPE-CD
		//           END-EXEC.
		halLokObjDetVDao.fetchWsCursor2(ws.getWsTsqFields());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0510-FETCH-CURSOR2-X
		//               WHEN OTHER
		//                   GO TO 0510-FETCH-CURSOR2-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET HALRLOMG-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setFound();
			// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CURSOR2 TO TRUE
			ws.getWsWorkfields().getEndOfCursor2Sw().setEndOfCursor2();
			// COB_CODE: GO TO 0510-FETCH-CURSOR2-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0510-FETCH-CURSOR2'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0510-FETCH-CURSOR2");
			// COB_CODE: MOVE 'FETCH OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0510-FETCH-CURSOR2-X
			return;
		}
		// COB_CODE: EXEC CICS
		//              WRITEQ TS
		//              QUEUE(HALRLOMG-TSQ-NAME)
		//              FROM(WS-LOKDET-ROW)
		//              LENGTH(WS-LOKDET-ROW-LGTH)
		//              NOHANDLE
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(ws.getWsTsqFields().getLokdetRowLgth());
		tsQueueData.setData(ws.getWsTsqFields().getLokdetRowBytes());
		TsQueueManager.insert(execContext, wsHalrlomgLinkage2.getTsqNameFormatted(), tsQueueData);
		// COB_CODE:      IF EIBRESP NOT EQUAL ZERO
		//           ***      SET HALRLOMG-NOT-FOUND TO TRUE
		//                    GO TO 0510-FETCH-CURSOR2-X
		//                END-IF.
		if (execContext.getResp() != 0) {
			//**      SET HALRLOMG-NOT-FOUND TO TRUE
			// COB_CODE: SET WS-END-OF-CURSOR2  TO TRUE
			ws.getWsWorkfields().getEndOfCursor2Sw().setEndOfCursor2();
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-CICS-WRITE-TSQ  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteTsq();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0510-FETCH-CURSOR2'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0510-FETCH-CURSOR2");
			// COB_CODE: MOVE 'WRITE TO TSQ FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO TSQ FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  WS-LOKDET-ROW
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getWsTsqFields().getLokdetRowFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0510-FETCH-CURSOR2-X
			return;
		}
	}

	/**Original name: 0600-DELETE-LOCK-ENTRY_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FUNCTION FOR 'DELETE_LOCK_ENTRY'                  *
	 *  1) DELETE DETAILS FOR CORR. SESSION/TECH/APPL                *
	 *  2) IF ERROR, NO ACTION TO BE TAKEN!                          *
	 * ***************************************************************</pre>*/
	private void deleteLockEntry() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE HALRLOMG-TCH-KEY    TO HLOH-TCH-KEY.
		ws.getDclhalLokObjHdr().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID     TO HLOH-APP-ID.
		ws.getDclhalLokObjHdr().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: PERFORM 2000-UPDATE-HDR-TS.
		updateHdrTs();
		//**      WHEN NOT ERD-SQL-GOOD
		// COB_CODE:      EVALUATE TRUE
		//           ***      WHEN NOT ERD-SQL-GOOD
		//                    WHEN ERD-SQL-GOOD
		//                         SET WS-HEADER-FND  TO TRUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//                        PERFORM 9000-LOG-WARNING-OR-ERROR
		//                    WHEN OTHER
		//                        GO TO 0600-DELETE-LOCK-ENTRY-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET WS-HEADER-FND  TO TRUE
			ws.getWsWorkfields().getHeaderFoundSw().setFnd();
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: IF HALRLOMG-DELETE-LOCK-ENTRY
			//               GO TO 0600-DELETE-LOCK-ENTRY-X
			//           END-IF
			if (wsHalrlomgLinkage2.getFunction().isDeleteLockEntry()) {
				// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
				return;
			}
			// COB_CODE: SET WS-HEADER-NOT-FND    TO TRUE
			ws.getWsWorkfields().getHeaderFoundSw().setNotFnd();
			// COB_CODE: SET WS-LOG-WARNING       TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_HDR_V");
			// COB_CODE: MOVE '0600-DELETE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-DELETE-LOCK-ENTRY");
			// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR NOT FOUND FOR DELETE'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("HAL_LOK_OBJ_HDR NOT FOUND FOR DELETE");
			// COB_CODE: STRING 'LOCK HEADER ROW='
			//                  DCLHAL-LOK-OBJ-HDR
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK HEADER ROW=",
					ws.getDclhalLokObjHdr().getDclhalLokObjHdrFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_HDR_V");
			// COB_CODE: MOVE '0600-DELETE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-DELETE-LOCK-ENTRY");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_HDR CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_HDR CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-HDR
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjHdr().getDclhalLokObjHdrFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
			return;
		}
		// COB_CODE: IF SQLERRD (3) = ZERO
		//             AND HALRLOMG-DELETE-LOCK-ENTRY
		//               GO TO 0600-DELETE-LOCK-ENTRY-X
		//           END-IF.
		if (sqlca.getSqlerrd(3) == 0 && wsHalrlomgLinkage2.getFunction().isDeleteLockEntry()) {
			// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
			return;
		}
		// COB_CODE: MOVE UBOC-SESSION-ID    TO HLOD-SESSION-ID
		ws.getDclhalLokObjDet().setSessionId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocSessionId());
		// COB_CODE: MOVE HALRLOMG-TCH-KEY   TO HLOD-TCH-KEY
		ws.getDclhalLokObjDet().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID    TO HLOD-APP-ID
		ws.getDclhalLokObjDet().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: MOVE HALRLOMG-TABLE-NM  TO HLOD-TABLE-NM
		ws.getDclhalLokObjDet().setTableNm(wsHalrlomgLinkage2.getTableNm());
		// COB_CODE: EXEC SQL
		//               DELETE FROM HAL_LOK_OBJ_DET_V
		//               WHERE  HLOD_SESSION_ID = :HLOD-SESSION-ID
		//               AND    TCH_KEY    = :HLOD-TCH-KEY
		//               AND    APP_ID     = :HLOD-APP-ID
		//               AND    TABLE_NM   = :HLOD-TABLE-NM
		//           END-EXEC.
		halLokObjDetVDao.deleteRec(ws.getDclhalLokObjDet().getSessionId(), ws.getDclhalLokObjDet().getTchKey(), ws.getDclhalLokObjDet().getAppId(),
				ws.getDclhalLokObjDet().getTableNm());
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//           **24597               PERFORM 1020-DELETE-LOCK-KEY-UMT-ROW
		//                         CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//                        CONTINUE
		//                    WHEN NOT ERD-SQL-GOOD
		//                        GO TO 0600-DELETE-LOCK-ENTRY-X
		//                END-EVALUATE.
		if (sqlca.isErdSqlGood()) {
			//*24597               PERFORM 1020-DELETE-LOCK-KEY-UMT-ROW
			// COB_CODE: IF NOT INFRA-UNATTENDED-LOCK-PURGE
			//               END-IF
			//           END-IF
			if (!wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocPassThruAction().isInfraUnattendedLockPurge()) {
				// COB_CODE: PERFORM 1020-DELETE-LOCK-KEY-TSQ-ROW
				deleteLockKeyTsqRow();
				// COB_CODE: IF UBOC-HALT-AND-RETURN
				//              GO TO 0600-DELETE-LOCK-ENTRY-X
				//           END-IF
				if (wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
					// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
					return;
				}
			}
			// COB_CODE: CONTINUE
			//continue
		} else if (sqlca.isErdSqlNotFound()) {
			// COB_CODE: CONTINUE
			//continue
		} else if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-DELETE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Delete();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0600-DELETE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-DELETE-LOCK-ENTRY");
			// COB_CODE: MOVE 'DELETE OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DELETE OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
			return;
		}
		//* SEE IF THERE ARE ANY OTHER LOCKS FOR THIS OBJECT.
		// COB_CODE: EXEC SQL
		//             SELECT COUNT (*)
		//               INTO :WS-UCT-ROW-COUNTER
		//               FROM HAL_LOK_OBJ_DET_V
		//               WHERE  TCH_KEY    = :HLOD-TCH-KEY
		//               AND    APP_ID     = :HLOD-APP-ID
		//               AND    TABLE_NM   = :HLOD-TABLE-NM
		//           END-EXEC.
		ws.getWsWorkfields().setUctRowCounter(Trunc.toShort(halLokObjDetVDao.selectRec2(ws.getDclhalLokObjDet().getTchKey(),
				ws.getDclhalLokObjDet().getAppId(), ws.getDclhalLokObjDet().getTableNm(), ws.getWsWorkfields().getUctRowCounter()), 4));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NULL-VALUE
		//                   MOVE 0 TO WS-UCT-ROW-COUNTER
		//               WHEN OTHER
		//                   GO TO 0600-DELETE-LOCK-ENTRY-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NULL_VALUE:// COB_CODE: MOVE 0 TO WS-UCT-ROW-COUNTER
			ws.getWsWorkfields().setUctRowCounter(((short) 0));
			break;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0600-DELETE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-DELETE-LOCK-ENTRY");
			// COB_CODE: MOVE 'COUNT OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("COUNT OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
			return;
		}
		//* IF THERE ARE, THEN THE LOCK HEADER CANNOT BE DELETED.
		//* IF THERE AREN'T, THEN DELETE THE LOCK HEADER ALSO.
		// COB_CODE: IF WS-UCT-ROW-COUNTER = ZERO
		//             AND WS-HEADER-FND
		//               GO TO 0600-DELETE-LOCK-ENTRY-X
		//           END-IF.
		if (ws.getWsWorkfields().getUctRowCounter() == 0 && ws.getWsWorkfields().getHeaderFoundSw().isFnd()) {
			// COB_CODE: EXEC SQL
			//               DELETE FROM HAL_LOK_OBJ_HDR_V
			//               WHERE  TCH_KEY    = :HLOH-TCH-KEY
			//               AND    APP_ID     = :HLOH-APP-ID
			//           END-EXEC
			halLokObjHdrVDao.deleteRec(ws.getDclhalLokObjHdr().getTchKey(), ws.getDclhalLokObjHdr().getAppId());
			// COB_CODE: EVALUATE TRUE
			//               WHEN NOT ERD-SQL-GOOD
			//                   GO TO 0600-DELETE-LOCK-ENTRY-X
			//           END-EVALUATE
			if (!sqlca.isErdSqlGood()) {
				// COB_CODE: SET WS-LOG-ERROR         TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
				// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
				// COB_CODE: SET ETRA-DB2-DELETE      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Delete();
				// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR_V' TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_HDR_V");
				// COB_CODE: MOVE '0600-DELETE-LOCK-ENTRY'
				//                TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-DELETE-LOCK-ENTRY");
				// COB_CODE: MOVE 'DELETE OF HAL_LOK_OBJ_HDR CURSOR FAILED'
				//                TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DELETE OF HAL_LOK_OBJ_HDR CURSOR FAILED");
				// COB_CODE: STRING 'LOCK HEADER ROW='
				//              DCLHAL-LOK-OBJ-HDR
				//              DELIMITED BY SIZE
				//              INTO EFAL-OBJ-DATA-KEY
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK HEADER ROW=",
						ws.getDclhalLokObjHdr().getDclhalLokObjHdrFormatted());
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
				return;
			}
			// COB_CODE: SET LOMG-DEL-GRP-LEGACY-LOK TO TRUE
			wsHalrlomgLinkage2.getGrpLokClrSw().setDelGrpLegacyLok();
			// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
			return;
		}
		//*** BEGIN NEW CODE
		//*** IF THIS IS NOT A GROUP LEVEL LOCK, EXIT THE SECTION.
		// COB_CODE: IF HALRLOMG-LOCK-GROUP-PESSI
		//             OR (HALRLOMG-LOCK-PES-READUPDT AND UBOC-GROUP-LOCK-USER)
		//               CONTINUE
		//           ELSE
		//               GO TO 0600-DELETE-LOCK-ENTRY-X
		//           END-IF.
		if (wsHalrlomgLinkage2.getLokTypeCd().isLockGroupPessi() || wsHalrlomgLinkage2.getLokTypeCd().isLockPesReadupdt()
				&& wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocUserInLockGrpSw().isUbocGroupLockUser()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
			return;
		}
		//*** SEE IF THERE ARE ANY ACTIVE LOCKS FOR THE OBJECT AND GROUP.
		// COB_CODE: MOVE UBOC-LOCK-GROUP TO HLOD-USERID.
		ws.getDclhalLokObjDet().setUserid(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocLockGroup());
		// COB_CODE: MOVE 'N'             TO HLOD-ZAPPED-IND.
		ws.getDclhalLokObjDet().setZappedIndFormatted("N");
		// COB_CODE: EXEC SQL
		//             SELECT HLOD_SESSION_ID
		//               INTO :HLOD-SESSION-ID
		//               FROM HAL_LOK_OBJ_DET_V
		//               WHERE  TCH_KEY    = :HLOD-TCH-KEY
		//               AND    APP_ID     = :HLOD-APP-ID
		//               AND    TABLE_NM   = :HLOD-TABLE-NM
		//               AND    USERID     = :HLOD-USERID
		//               AND    HLOD_ZAPPED_IND = :HLOD-ZAPPED-IND
		//               FETCH FIRST ROW ONLY
		//           END-EXEC.
		ws.getDclhalLokObjDet().setSessionId(halLokObjDetVDao.selectRec3(ws.getDclhalLokObjDet(), ws.getDclhalLokObjDet().getSessionId()));
		//** IF THERE ARE ACTIVE LOCKS FOR THE OBJECT AND GROUP, THEN
		//** KEEP THE LOCK ON THE LEGACY SYSTEM.
		//** IF ALL THE LOCKS FOR THE OBJECT AND GROUP HAVE BEEN ZAPPED,
		//** THEN DELETE THE LEGACY LOCK, ALSO.
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   GO TO 0600-DELETE-LOCK-ENTRY-X
		//               WHEN ERD-SQL-NOT-FOUND
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0600-DELETE-LOCK-ENTRY-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: IF HALRLOMG-LOCK-GROUP-PESSI
			//               SET LOMG-KEEP-GRP-LEGACY-LOK TO TRUE
			//           END-IF
			if (wsHalrlomgLinkage2.getLokTypeCd().isLockGroupPessi()) {
				// COB_CODE: SET LOMG-KEEP-GRP-LEGACY-LOK TO TRUE
				wsHalrlomgLinkage2.getGrpLokClrSw().setKeepGrpLegacyLok();
			}
			// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
			return;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0600-DELETE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0600-DELETE-LOCK-ENTRY");
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET COUNT FOR GROUP FAILED.'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("HAL_LOK_OBJ_DET COUNT FOR GROUP FAILED.");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0600-DELETE-LOCK-ENTRY-X
			return;
		}
		// COB_CODE: IF HALRLOMG-LOCK-GROUP-PESSI
		//               SET LOMG-DEL-GRP-LEGACY-LOK TO TRUE
		//           END-IF.
		if (wsHalrlomgLinkage2.getLokTypeCd().isLockGroupPessi()) {
			// COB_CODE: SET LOMG-DEL-GRP-LEGACY-LOK TO TRUE
			wsHalrlomgLinkage2.getGrpLokClrSw().setDelGrpLegacyLok();
		}
		// COB_CODE: EXEC SQL
		//             DELETE FROM HAL_LOK_OBJ_DET_V
		//               WHERE  TCH_KEY    = :HLOD-TCH-KEY
		//               AND    APP_ID     = :HLOD-APP-ID
		//               AND    TABLE_NM   = :HLOD-TABLE-NM
		//               AND    USERID     = :HLOD-USERID
		//           END-EXEC.
		halLokObjDetVDao.deleteRec1(ws.getDclhalLokObjDet().getTchKey(), ws.getDclhalLokObjDet().getAppId(), ws.getDclhalLokObjDet().getTableNm(),
				ws.getDclhalLokObjDet().getUserid());
	}

	/**Original name: 0700-ZAP-ALL-LOCK-ENTRIES_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FUNCTION FOR 'ZAP_ALL_LOCK_ENTRIES'               *
	 *  1) UPDATE DETAILS FOR CORR. TECH/APPL                        *
	 *     - SET ALL ZAPPED FIELDS AND TS.                           *
	 *  2) IF ERROR, SET 'HALT_AND_RETURN' AND FORMAT ERROR FIELDS   *
	 *     IN UBOC (DO NOT PROCESS STANDARD SAVANNAH ERRORS).        *
	 * ***************************************************************
	 * * THIS SECTION CAN BE INVOKED FROM XPIOCHK/BCMOCHK (VIA HAL) FROM
	 * * S3 NON-BROWSER PROCESSES.  THEREFORE, ENSURE THAT THE HEADER
	 * * IS LOCKED BEFORE UPDATING THE DETAILS IN CASE THIS SECTION HAS
	 * * BEEN INVOKED AS A RESULT OF NON-BROWSER LEGACY PROCESSING.</pre>*/
	private void zapAllLockEntries() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE HALRLOMG-TCH-KEY    TO HLOH-TCH-KEY.
		ws.getDclhalLokObjHdr().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID     TO HLOH-APP-ID.
		ws.getDclhalLokObjHdr().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: PERFORM 2000-UPDATE-HDR-TS.
		updateHdrTs();
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
		//               WHEN OTHER
		//                   GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_HDR_V");
			// COB_CODE: MOVE '0700-ZAP-ALL-LOCK-ENTRIES'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0700-ZAP-ALL-LOCK-ENTRIES");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_HDR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_HDR FAILED");
			// COB_CODE: STRING 'LOCK KEY= '
			//                  HLOH-TCH-KEY ' ' HLOH-APP-ID
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK KEY= ", ws.getDclhalLokObjHdr().getTchKeyFormatted(),
					" ", ws.getDclhalLokObjHdr().getAppIdFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
			return;
		}
		// COB_CODE: IF SQLERRD (3) = ZERO
		//               GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
		//           END-IF.
		if (sqlca.getSqlerrd(3) == 0) {
			// COB_CODE: GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
			return;
		}
		// COB_CODE: MOVE HALRLOMG-TCH-KEY TO HLOD-TCH-KEY
		ws.getDclhalLokObjDet().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID  TO HLOD-APP-ID
		ws.getDclhalLokObjDet().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: EXEC SQL
		//             DECLARE WS-CURSOR3 CURSOR FOR
		//                 SELECT HLOD_SESSION_ID,
		//                        TCH_KEY,
		//                        APP_ID
		//                 FROM   HAL_LOK_OBJ_DET_V
		//                 WHERE  TCH_KEY  = :HLOD-TCH-KEY
		//                 AND    APP_ID   = :HLOD-APP-ID
		//                 FOR READ ONLY
		//           END-EXEC.
		// DECLARE CURSOR doesn't need a translation;
		// COB_CODE: EXEC SQL
		//               OPEN WS-CURSOR3
		//           END-EXEC.
		halLokObjDetVDao.openWsCursor3(ws.getDclhalLokObjDet().getTchKey(), ws.getDclhalLokObjDet().getAppId());
		// COB_CODE: EVALUATE TRUE
		//               WHEN NOT ERD-SQL-GOOD
		//                   GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
		//           END-EVALUATE.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0700-ZAP-ALL-LOCK-ENTRIES'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0700-ZAP-ALL-LOCK-ENTRIES");
			// COB_CODE: MOVE 'OPEN CURSOR OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CURSOR OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
			return;
		}
		// COB_CODE: SET WS-NOT-END-OF-CURSOR3 TO TRUE.
		ws.getWsWorkfields().getEndOfCursor3Sw().setNotEndOfCursor3();
		// COB_CODE: PERFORM 0710-FETCH-CURSOR3
		//             UNTIL NOT ERD-SQL-GOOD
		//                OR WS-END-OF-CURSOR3.
		while (!(!sqlca.isErdSqlGood() || ws.getWsWorkfields().getEndOfCursor3Sw().isEndOfCursor3())) {
			fetchCursor3();
		}
		// COB_CODE: EXEC SQL
		//               CLOSE WS-CURSOR3
		//           END-EXEC.
		halLokObjDetVDao.closeWsCursor3();
		// COB_CODE: EVALUATE TRUE
		//               WHEN NOT ERD-SQL-GOOD
		//                   GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
		//           END-EVALUATE.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0700-ZAP-ALL-LOCK-ENTRIES'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0700-ZAP-ALL-LOCK-ENTRIES");
			// COB_CODE: MOVE 'CLOSE CURSOR OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CURSOR OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: GO TO 0700-ZAP-ALL-LOCK-ENTRIES-X
			return;
		}
	}

	/**Original name: 0710-FETCH-CURSOR3_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FOR CURSOR 3                                      *
	 * ***************************************************************</pre>*/
	private void fetchCursor3() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//            FETCH WS-CURSOR3
		//            INTO :HLOD-SESSION-ID,
		//                 :HLOD-TCH-KEY,
		//                 :HLOD-APP-ID
		//           END-EXEC.
		halLokObjDetVDao.fetchWsCursor3(ws.getDclhalLokObjDet());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0710-FETCH-CURSOR3-X
		//               WHEN OTHER
		//                   GO TO 0710-FETCH-CURSOR3-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CURSOR3 TO TRUE
			ws.getWsWorkfields().getEndOfCursor3Sw().setEndOfCursor3();
			// COB_CODE: GO TO 0710-FETCH-CURSOR3-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0710-FETCH-CURSOR3'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0710-FETCH-CURSOR3");
			// COB_CODE: MOVE 'FETCH OF HAL_LOK_OBJ_DET CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH OF HAL_LOK_OBJ_DET CURSOR FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0710-FETCH-CURSOR3-X
			return;
		}
		// COB_CODE: MOVE UBOC-AUTH-USERID    TO HLOD-ZAPPED-BY.
		ws.getDclhalLokObjDet().setZappedBy(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: MOVE 'Y'                 TO HLOD-ZAPPED-IND.
		ws.getDclhalLokObjDet().setZappedIndFormatted("Y");
		// COB_CODE: EXEC SQL UPDATE HAL_LOK_OBJ_DET_V
		//                SET HLOD_LOK_TMO_TS =
		//                    CURRENT TIMESTAMP +
		//                    :WS-TMO-VALUE MINUTES,
		//                    HLOD_ZAPPED_TS =
		//                    CURRENT TIMESTAMP,
		//                    HLOD_ZAPPED_BY   =
		//                    :HLOD-ZAPPED-BY,
		//                    HLOD_ZAPPED_IND =
		//                    :HLOD-ZAPPED-IND
		//                WHERE HLOD_SESSION_ID = :HLOD-SESSION-ID
		//                AND   TCH_KEY    = :HLOD-TCH-KEY
		//                AND   APP_ID     = :HLOD-APP-ID
		//           END-EXEC.
		halLokObjDetVDao.updateRec4(ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0710-FETCH-CURSOR3-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-END-OF-CURSOR3    TO TRUE
			ws.getWsWorkfields().getEndOfCursor3Sw().setEndOfCursor3();
			// COB_CODE: SET UBOC-HALT-AND-RETURN TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
			// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0710-FETCH-CURSOR3'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0710-FETCH-CURSOR3");
			// COB_CODE: MOVE 'DELETE OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DELETE OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: GO TO 0710-FETCH-CURSOR3-X
			return;
		}
	}

	/**Original name: 0800-UPGRADE-LOCK-ENTRY_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PROCESSING FUNCTION FOR 'UPGRADE_LOCK_ENTRY'                 *
	 *  1) UPDATE DETAILS FOR CORR. TECH/APPL                        *
	 *     - CHANGE THE LOCK TYPE ON THE TABLE AND UMT TO THE        *
	 *     - LOCK TYPE REQUESTED BY THIS UOW.                        *
	 *  2) IF ERROR, SET 'HALT_AND_RETURN' AND FORMAT ERROR FIELDS   *
	 *     IN UBOC (DO NOT PROCESS STANDARD SAVANNAH ERRORS).        *
	 * ***************************************************************
	 * * ENSURE THAT THE HEADER IS LOCKED BEFORE UPDATING THE DETAILS.</pre>*/
	private void upgradeLockEntry() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE HALRLOMG-TCH-KEY    TO HLOH-TCH-KEY.
		ws.getDclhalLokObjHdr().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID     TO HLOH-APP-ID.
		ws.getDclhalLokObjHdr().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: PERFORM 2000-UPDATE-HDR-TS.
		updateHdrTs();
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0800-UPGRADE-LOCK-ENTRY-X
		//               WHEN OTHER
		//                   GO TO 0800-UPGRADE-LOCK-ENTRY-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET HALRLOMG-NOT-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setNotFound();
			// COB_CODE: GO TO 0800-UPGRADE-LOCK-ENTRY-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-UPDATE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Update();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_HDR_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_HDR_V");
			// COB_CODE: MOVE '0800-UPGRADE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0800-UPGRADE-LOCK-ENTRY");
			// COB_CODE: MOVE 'UPDATE OF HAL_LOK_OBJ_HDR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPDATE OF HAL_LOK_OBJ_HDR FAILED");
			// COB_CODE: STRING 'LOCK KEY= '
			//                  HLOH-TCH-KEY ' ' HLOH-APP-ID
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK KEY= ", ws.getDclhalLokObjHdr().getTchKeyFormatted(),
					" ", ws.getDclhalLokObjHdr().getAppIdFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0800-UPGRADE-LOCK-ENTRY-X
			return;
		}
		// COB_CODE: IF SQLERRD (3) = ZERO
		//               GO TO 0800-UPGRADE-LOCK-ENTRY-X
		//           END-IF.
		if (sqlca.getSqlerrd(3) == 0) {
			// COB_CODE: SET HALRLOMG-NOT-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setNotFound();
			// COB_CODE: GO TO 0800-UPGRADE-LOCK-ENTRY-X
			return;
		}
		// COB_CODE: MOVE UBOC-SESSION-ID      TO HLOD-SESSION-ID
		ws.getDclhalLokObjDet().setSessionId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocSessionId());
		// COB_CODE: MOVE HALRLOMG-TCH-KEY     TO HLOD-TCH-KEY.
		ws.getDclhalLokObjDet().setTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID      TO HLOD-APP-ID.
		ws.getDclhalLokObjDet().setAppId(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: MOVE HALRLOMG-LOK-TYPE-CD TO HLOD-LOK-TYPE-CD.
		ws.getDclhalLokObjDet().setLokTypeCd(wsHalrlomgLinkage2.getLokTypeCd().getLokTypeCd());
		// COB_CODE: MOVE UBOC-MSG-ID          TO HLOD-UPD-BY-MSG-ID.
		ws.getDclhalLokObjDet().setUpdByMsgId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: IF HALRLOMG-LOCK-GROUP-PESSI
		//               MOVE UBOC-LOCK-GROUP  TO HLOD-USERID
		//           ELSE
		//               MOVE UBOC-AUTH-USERID TO HLOD-USERID
		//           END-IF.
		if (wsHalrlomgLinkage2.getLokTypeCd().isLockGroupPessi()) {
			// COB_CODE: MOVE UBOC-LOCK-GROUP  TO HLOD-USERID
			ws.getDclhalLokObjDet().setUserid(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocLockGroup());
		} else {
			// COB_CODE: MOVE UBOC-AUTH-USERID TO HLOD-USERID
			ws.getDclhalLokObjDet().setUserid(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocAuthUserid());
		}
		// COB_CODE: EXEC SQL UPDATE HAL_LOK_OBJ_DET_V
		//                SET LAST_ACY_TS =
		//                    CURRENT TIMESTAMP,
		//                    HLOD_LOK_TMO_TS =
		//                    CURRENT TIMESTAMP +
		//                    :WS-TMO-VALUE MINUTES,
		//                    LOK_TYPE_CD =
		//                    :HLOD-LOK-TYPE-CD,
		//                    HLOD_UPD_BY_MSG_ID =
		//                    :HLOD-UPD-BY-MSG-ID,
		//                    USERID =
		//                    :HLOD-USERID
		//                WHERE HLOD_SESSION_ID = :HLOD-SESSION-ID
		//                AND   TCH_KEY         = :HLOD-TCH-KEY
		//                AND   APP_ID          = :HLOD-APP-ID
		//           END-EXEC.
		halLokObjDetVDao.updateRec5(ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   SET HALRLOMG-FOUND TO TRUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0800-UPGRADE-LOCK-ENTRY-X
		//               WHEN OTHER
		//                   GO TO 0800-UPGRADE-LOCK-ENTRY-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET HALRLOMG-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setFound();
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET HALRLOMG-NOT-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setNotFound();
			// COB_CODE: GO TO 0800-UPGRADE-LOCK-ENTRY-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR         TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_LOK_OBJ_DET_V' TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_LOK_OBJ_DET_V");
			// COB_CODE: MOVE '0800-UPGRADE-LOCK-ENTRY'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0800-UPGRADE-LOCK-ENTRY");
			// COB_CODE: MOVE 'UPGRADE OF HAL_LOK_OBJ_DET FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UPGRADE OF HAL_LOK_OBJ_DET FAILED");
			// COB_CODE: STRING 'LOCK DETAILS ROW='
			//                  DCLHAL-LOK-OBJ-DET
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "LOCK DETAILS ROW=",
					ws.getDclhalLokObjDet().getDclhalLokObjDetFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0800-UPGRADE-LOCK-ENTRY-X
			return;
		}
		//    PERFORM 1015-UPDATE-LOCK-KEY-UMT-ROW.
		// COB_CODE: PERFORM 1015-UPDATE-LOCK-KEY-TSQ-ROW.
		updateLockKeyTsqRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0800-UPGRADE-LOCK-ENTRY-X
		//           END-IF.
		if (wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0800-UPGRADE-LOCK-ENTRY-X
			return;
		}
	}

	/**Original name: 1000-WRITE-LOCK-KEY-TSQ-ROW_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  WRITE A ROW TO THE UOW LOCK TSQ
	 * ****************************************************************
	 * * SINCE THESE ROWS ONLY PERSIST FOR THE LIFETIME OF THE UOW,
	 * * SESSION ID IS ACTUALLY IRRELEVANT.  ONLY THE TECHNICAL KEY
	 * * AND APPID ARE NEEDED FOR LOCK PROCESSING.</pre>*/
	private void writeLockKeyTsqRow() {
		TpOutputData tsQueueData = null;
		// COB_CODE: MOVE SPACES           TO WS-UOW-LOCK-TSQ-LAYOUT.
		ws.initWsUowLockTsqLayoutSpaces();
		// COB_CODE: MOVE HALRLOMG-TCH-KEY TO WS-LOCK-TCH-KEY.
		ws.setWsLockTchKey(wsHalrlomgLinkage2.getTchKey());
		// COB_CODE: MOVE HALRLOMG-APP-ID  TO WS-LOCK-APPID.
		ws.setWsLockAppid(wsHalrlomgLinkage2.getAppId());
		// COB_CODE: MOVE HALRLOMG-LOK-TYPE-CD TO WS-LOCK-TYPE-CODE.
		ws.setWsLockTypeCode(wsHalrlomgLinkage2.getLokTypeCd().getLokTypeCd());
		// COB_CODE: EXEC CICS
		//                WRITEQ TS MAIN
		//                QNAME (UBOC-UOW-LOCK-PROC-TSQ)
		//                FROM  (WS-UOW-LOCK-ROW)
		//                RESP  (WS-RESPONSE-CODE)
		//                RESP2 (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(HalrlomgData.Len.WS_UOW_LOCK_ROW);
		tsQueueData.setData(ws.getWsUowLockRowBytes());
		TsQueueManager.insert(execContext, wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocUowLockProcTsqFormatted(), tsQueueData);
		ws.getWsWorkfields().setResponseCode(execContext.getResp());
		ws.getWsWorkfields().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 1000-WRITE-LOCK-KEY-TSQ-ROW-X
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsWorkfields().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteTsq();
			// COB_CODE: MOVE UBOC-UOW-LOCK-PROC-TSQ
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocUowLockProcTsq());
			// COB_CODE: MOVE '1000-WRITE-LOCK-KEY-TSQ-ROW'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1000-WRITE-LOCK-KEY-TSQ-ROW");
			// COB_CODE: MOVE 'WRITE OF UOW LOCK TO TSQ FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE OF UOW LOCK TO TSQ FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1000-WRITE-LOCK-KEY-TSQ-ROW-X
			return;
		}
	}

	/**Original name: 1010-READ-LOCK-KEY-TSQ-ROW_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ ROWS FROM THE UOW LOCK TSQ
	 * ****************************************************************</pre>*/
	private void readLockKeyTsqRow() {
		// COB_CODE: SET WS-START-OF-UOW-LOCK-TSQ TO TRUE.
		ws.getWsWorkfields().getUowLockTsqSw().setStartOfUowLockTsq();
		//* FOR COMPATIBILITY WITH ORIGINAL UMT CODE.
		// COB_CODE: SET HALOUKRP-OKAY TO TRUE.
		ws.getHallukrp().getReturnCode().setOkay();
		// COB_CODE: PERFORM 1012-READ-ALL-LOCK-TSQ-ROWS
		//              VARYING WS-TS-LOCK-ROW-CNT
		//              FROM 1 BY 1
		//              UNTIL UBOC-HALT-AND-RETURN
		//                 OR WS-END-OF-UOW-LOCK-TSQ.
		ws.getWsWorkfields().setTsLockRowCnt(((short) 1));
		while (!(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsWorkfields().getUowLockTsqSw().isEndOfUowLockTsq())) {
			readAllLockTsqRows();
			ws.getWsWorkfields().setTsLockRowCnt(Trunc.toShort(ws.getWsWorkfields().getTsLockRowCnt() + 1, 4));
		}
	}

	/**Original name: 1012-READ-ALL-LOCK-TSQ-ROWS_FIRST_SENTENCES<br>*/
	private void readAllLockTsqRows() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS
		//                READQ TS QNAME(UBOC-UOW-LOCK-PROC-TSQ)
		//                INTO          (WS-UOW-LOCK-ROW)
		//                ITEM          (WS-TS-LOCK-ROW-CNT)
		//                RESP          (WS-RESPONSE-CODE)
		//                RESP2         (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(HalrlomgData.Len.WS_UOW_LOCK_ROW);
		TsQueueManager.read(execContext, wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocUowLockProcTsqFormatted(),
				ws.getWsWorkfields().getTsLockRowCnt(), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.setWsUowLockRowBytes(tsQueueData.getData());
		}
		ws.getWsWorkfields().setResponseCode(execContext.getResp());
		ws.getWsWorkfields().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//             WHEN DFHRESP(NORMAL)
		//               CONTINUE
		//             WHEN DFHRESP(QIDERR)
		//             WHEN DFHRESP(ITEMERR)
		//               GO TO 1012-READ-ALL-LOCK-TSQ-ROWS-X
		//             WHEN OTHER
		//               GO TO 1012-READ-ALL-LOCK-TSQ-ROWS-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsWorkfields().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if ((TpConditionType.valueOf(ws.getWsWorkfields().getResponseCode()) == TpConditionType.QIDERR)
				|| (TpConditionType.valueOf(ws.getWsWorkfields().getResponseCode()) == TpConditionType.ITEMERR)) {
			// COB_CODE: SET WS-END-OF-UOW-LOCK-TSQ TO TRUE
			ws.getWsWorkfields().getUowLockTsqSw().setEndOfUowLockTsq();
			// COB_CODE: SET HALRLOMG-NOT-FOUND     TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setNotFound();
			//* FOR COMPATIBILITY WITH ORIGINAL UMT CODE
			// COB_CODE: SET HALOUKRP-NOTFND        TO TRUE
			ws.getHallukrp().getReturnCode().setNotfnd();
			// COB_CODE: GO TO 1012-READ-ALL-LOCK-TSQ-ROWS-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadTsq();
			// COB_CODE: MOVE UBOC-UOW-LOCK-PROC-TSQ
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocUowLockProcTsq());
			// COB_CODE: MOVE '1012-READ-ALL-LOCK-TSQ-ROWS'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1012-READ-ALL-LOCK-TSQ-ROWS");
			// COB_CODE: MOVE 'READ OF UOW LOCK TO TSQ FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ OF UOW LOCK TO TSQ FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1012-READ-ALL-LOCK-TSQ-ROWS-X
			return;
		}
		// COB_CODE:      IF (HALRLOMG-TCH-KEY EQUAL WS-LOCK-TCH-KEY)
		//                  AND (HALRLOMG-APP-ID EQUAL WS-LOCK-APPID)
		//                    PERFORM 1100-COMPARE-LOCK-TYPE
		//           *        IF HALRLOMG-LOK-TYPE-CD NOT = WS-LOCK-TYPE-CODE
		//           *            SET HALRLOMG-TYPE-MISMATCH TO TRUE
		//           *        END-IF
		//                END-IF.
		if (Conditions.eq(wsHalrlomgLinkage2.getTchKey(), ws.getWsLockTchKey())
				&& Conditions.eq(wsHalrlomgLinkage2.getAppId(), ws.getWsLockAppid())) {
			// COB_CODE: SET HALRLOMG-FOUND TO TRUE
			wsHalrlomgLinkage2.getSuccessInd().setFound();
			// COB_CODE: SET WS-END-OF-UOW-LOCK-TSQ TO TRUE
			ws.getWsWorkfields().getUowLockTsqSw().setEndOfUowLockTsq();
			// COB_CODE: IF HALRLOMG-LOK-TYPE-CD = WS-LOCK-TYPE-CODE
			//               GO TO 1012-READ-ALL-LOCK-TSQ-ROWS-X
			//           END-IF
			if (wsHalrlomgLinkage2.getLokTypeCd().getLokTypeCd() == ws.getWsLockTypeCode()) {
				// COB_CODE: GO TO 1012-READ-ALL-LOCK-TSQ-ROWS-X
				return;
			}
			// COB_CODE: MOVE HALRLOMG-LOK-TYPE-CD TO WS-REQUESTED-LOCK-TYPE
			ws.getWsWorkfields().setRequestedLockType(wsHalrlomgLinkage2.getLokTypeCd().getLokTypeCd());
			// COB_CODE: MOVE WS-LOCK-TYPE-CODE TO WS-EXISTING-LOCK-TYPE
			ws.getWsWorkfields().setExistingLockType(ws.getWsLockTypeCode());
			// COB_CODE: PERFORM 1100-COMPARE-LOCK-TYPE
			compareLockType();
			//        IF HALRLOMG-LOK-TYPE-CD NOT = WS-LOCK-TYPE-CODE
			//            SET HALRLOMG-TYPE-MISMATCH TO TRUE
			//        END-IF
		}
	}

	/**Original name: 1015-UPDATE-LOCK-KEY-TSQ-ROW_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  UPDATE THE LOCK TYPE FOR THE ROW ON THE UOW LOCK TSQ
	 * ****************************************************************</pre>*/
	private void updateLockKeyTsqRow() {
		// COB_CODE: PERFORM 1010-READ-LOCK-KEY-TSQ-ROW.
		readLockKeyTsqRow();
		// COB_CODE: IF HALRLOMG-NOT-FOUND OR UBOC-HALT-AND-RETURN
		//               GO TO 1015-UPDATE-LOCK-KEY-TSQ-ROW-X
		//           END-IF.
		if (wsHalrlomgLinkage2.getSuccessInd().isNotFound()
				|| wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1015-UPDATE-LOCK-KEY-TSQ-ROW-X
			return;
		}
		// COB_CODE: MOVE HALRLOMG-LOK-TYPE-CD TO WS-LOCK-TYPE-CODE.
		ws.setWsLockTypeCode(wsHalrlomgLinkage2.getLokTypeCd().getLokTypeCd());
		// COB_CODE: PERFORM 1025-REWRITE-LOCK-KEY-TSQ-ROW.
		rewriteLockKeyTsqRow();
	}

	/**Original name: 1020-DELETE-LOCK-KEY-TSQ-ROW_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  LOGICALLY DELETE A TSQ ROW BY INVALIDATING THE ROW CONTENTS
	 * ****************************************************************</pre>*/
	private void deleteLockKeyTsqRow() {
		// COB_CODE: PERFORM 1010-READ-LOCK-KEY-TSQ-ROW.
		readLockKeyTsqRow();
		// COB_CODE: IF HALRLOMG-NOT-FOUND OR UBOC-HALT-AND-RETURN
		//               GO TO 1020-DELETE-LOCK-KEY-TSQ-ROW-X
		//           END-IF.
		if (wsHalrlomgLinkage2.getSuccessInd().isNotFound()
				|| wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1020-DELETE-LOCK-KEY-TSQ-ROW-X
			return;
		}
		// COB_CODE: MOVE 'X' TO WS-LOCK-TCH-KEY.
		ws.setWsLockTchKey("X");
		// COB_CODE: MOVE 'X' TO WS-LOCK-APPID.
		ws.setWsLockAppid("X");
		// COB_CODE: MOVE 'X' TO WS-LOCK-TYPE-CODE.
		ws.setWsLockTypeCodeFormatted("X");
		// COB_CODE: PERFORM 1025-REWRITE-LOCK-KEY-TSQ-ROW.
		rewriteLockKeyTsqRow();
	}

	/**Original name: 1025-REWRITE-LOCK-KEY-TSQ-ROW_FIRST_SENTENCES<br>*/
	private void rewriteLockKeyTsqRow() {
		TpOutputData tsQueueData = null;
		// COB_CODE: COMPUTE WS-TS-LOCK-ROW-CNT = WS-TS-LOCK-ROW-CNT - 1.
		ws.getWsWorkfields().setTsLockRowCnt(Trunc.toShort(ws.getWsWorkfields().getTsLockRowCnt() - 1, 4));
		// COB_CODE: EXEC CICS
		//                WRITEQ TS
		//                QNAME   (UBOC-UOW-LOCK-PROC-TSQ)
		//                FROM    (WS-UOW-LOCK-ROW)
		//                ITEM    (WS-TS-LOCK-ROW-CNT)
		//                REWRITE
		//                RESP    (WS-RESPONSE-CODE)
		//                RESP2   (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(HalrlomgData.Len.WS_UOW_LOCK_ROW);
		tsQueueData.setData(ws.getWsUowLockRowBytes());
		TsQueueManager.update(execContext, wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocUowLockProcTsqFormatted(),
				ws.getWsWorkfields().getTsLockRowCnt(), tsQueueData);
		ws.getWsWorkfields().setResponseCode(execContext.getResp());
		ws.getWsWorkfields().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 1025-REWRITE-LOCK-KEY-TSQ-ROWX
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsWorkfields().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-UPDATE-TSQ     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsUpdateTsq();
			// COB_CODE: MOVE UBOC-UOW-LOCK-PROC-TSQ
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocUowLockProcTsq());
			// COB_CODE: MOVE '1025-REWRITE-LOCK-KEY-TSQ-ROW'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1025-REWRITE-LOCK-KEY-TSQ-ROW");
			// COB_CODE: MOVE 'REWRITE OF UOW LOCK TO TSQ FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("REWRITE OF UOW LOCK TO TSQ FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1025-REWRITE-LOCK-KEY-TSQ-ROWX
			return;
		}
	}

	/**Original name: 1100-COMPARE-LOCK-TYPE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  COMPARE THE EXISTING LOCK TYPE FOR THE DATA IN THE SESSION TO
	 *  THE REQUESTED LOCK TYPE.  IF THE EXISTING LOCK TYPE IS WEAKER,
	 *  UPGRADE TO THE REQUESTED LOCK TYPE, IF POSSIBLE. IF THE EXISTING
	 *  LOCK TYPE IS STRONGER, RETAIN IT.
	 * *****************************************************************</pre>*/
	private void compareLockType() {
		// COB_CODE: SET HALRLOMG-FOUND TO TRUE.
		wsHalrlomgLinkage2.getSuccessInd().setFound();
		// COB_CODE: IF WS-EXISTING-LOCK-TYPE = 'O'
		//               GO TO 1100-COMPARE-LOCK-TYPE-X
		//           END-IF.
		if (ws.getWsWorkfields().getExistingLockType() == 'O') {
			// COB_CODE: IF WS-REQUESTED-LOCK-TYPE = 'P'
			//             OR WS-REQUESTED-LOCK-TYPE = 'G'
			//               SET HALRLOMG-TYPE-MISMATCH TO TRUE
			//           END-IF
			if (ws.getWsWorkfields().getRequestedLockType() == 'P' || ws.getWsWorkfields().getRequestedLockType() == 'G') {
				// COB_CODE: SET HALRLOMG-TYPE-MISMATCH TO TRUE
				wsHalrlomgLinkage2.getSuccessInd().setTypeMismatch();
			}
			// COB_CODE: GO TO 1100-COMPARE-LOCK-TYPE-X
			return;
		}
		// COB_CODE: IF WS-EXISTING-LOCK-TYPE = 'P'
		//               GO TO 1100-COMPARE-LOCK-TYPE-X
		//           END-IF.
		if (ws.getWsWorkfields().getExistingLockType() == 'P') {
			// COB_CODE: IF WS-REQUESTED-LOCK-TYPE = 'G'
			//               SET HALRLOMG-TYPE-MISMATCH TO TRUE
			//           END-IF
			if (ws.getWsWorkfields().getRequestedLockType() == 'G') {
				// COB_CODE: SET HALRLOMG-TYPE-MISMATCH TO TRUE
				wsHalrlomgLinkage2.getSuccessInd().setTypeMismatch();
			}
			// COB_CODE: GO TO 1100-COMPARE-LOCK-TYPE-X
			return;
		}
		// COB_CODE: IF WS-EXISTING-LOCK-TYPE = 'G'
		//               GO TO 1100-COMPARE-LOCK-TYPE-X
		//           END-IF.
		if (ws.getWsWorkfields().getExistingLockType() == 'G') {
			// COB_CODE: IF WS-REQUESTED-LOCK-TYPE = 'P'
			//               SET HALRLOMG-TYPE-MISMATCH TO TRUE
			//           END-IF
			if (ws.getWsWorkfields().getRequestedLockType() == 'P') {
				// COB_CODE: SET HALRLOMG-TYPE-MISMATCH TO TRUE
				wsHalrlomgLinkage2.getSuccessInd().setTypeMismatch();
			}
			// COB_CODE: GO TO 1100-COMPARE-LOCK-TYPE-X
			return;
		}
	}

	/**Original name: 2000-UPDATE-HDR-TS_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  COMMON CODE TO SQL UPDATE TIMESTAMP ON HEADER.                 *
	 * *****************************************************************</pre>*/
	private void updateHdrTs() {
		// COB_CODE: EXEC SQL UPDATE HAL_LOK_OBJ_HDR_V
		//                SET LAST_ACY_TS =
		//                    CURRENT TIMESTAMP
		//                WHERE TCH_KEY    = :HLOH-TCH-KEY
		//                AND   APP_ID     = :HLOH-APP-ID
		//           END-EXEC.
		halLokObjHdrVDao.updateRec(ws.getDclhalLokObjHdr().getTchKey(), ws.getDclhalLokObjHdr().getAppId());
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning()
				&& wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError() && !wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails()
						.getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsWorkAreas().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsWorkfields().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsWorkfields().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsWorkfields().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsWorkfields().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsWorkAreas().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALRLOMG", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsWorkfields().setResponseCode(execContext.getResp());
		ws.getWsWorkfields().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsWorkfields().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsWorkfields().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsWorkfields().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw()
					.setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			wsHalrlomgLinkage1.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			wsHalrlomgLinkage1.getUbocRecord().setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			wsHalrlomgLinkage1.getUbocRecord().setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initWsWorkfields() {
		ws.getWsWorkfields().setResponseCode(0);
		ws.getWsWorkfields().setResponseCode2(0);
		ws.getWsWorkfields().setUctRowCounter(((short) 0));
		ws.getWsWorkfields().setTmoValue(((short) 0));
		ws.getWsWorkfields().setTsLockRowCnt(((short) 0));
		ws.getWsWorkfields().setExistingLockType(Types.SPACE_CHAR);
		ws.getWsWorkfields().setRequestedLockType(Types.SPACE_CHAR);
		ws.getWsWorkfields().getEndOfCursor1Sw().setEndOfCursor1Sw(Types.SPACE_CHAR);
		ws.getWsWorkfields().getEndOfCursor2Sw().setEndOfCursor2Sw(Types.SPACE_CHAR);
		ws.getWsWorkfields().getEndOfCursor3Sw().setEndOfCursor3Sw(Types.SPACE_CHAR);
		ws.getWsWorkfields().getAddUpdateSw().setAddUpdateSw(Types.SPACE_CHAR);
		ws.getWsWorkfields().getHeaderFoundSw().setHeaderFoundSw(Types.SPACE_CHAR);
		ws.getWsWorkfields().getUowLockTsqSw().setUowLockTsqSw(Types.SPACE_CHAR);
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
