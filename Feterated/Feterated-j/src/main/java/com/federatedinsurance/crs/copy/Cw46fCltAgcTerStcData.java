/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW46F-CLT-AGC-TER-STC-DATA<br>
 * Variable: CW46F-CLT-AGC-TER-STC-DATA from copybook CAWLF046<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw46fCltAgcTerStcData {

	//==== PROPERTIES ====
	/**Original name: CW46F-TER-NBR-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char terNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-TER-NBR
	private String terNbr = DefaultValues.stringVal(Len.TER_NBR);
	//Original name: CW46F-BRN-TER-NBR-CI
	private char brnTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-BRN-TER-NBR
	private String brnTerNbr = DefaultValues.stringVal(Len.BRN_TER_NBR);
	//Original name: CW46F-BRN-TER-CLT-ID-CI
	private char brnTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-BRN-TER-CLT-ID
	private String brnTerCltId = DefaultValues.stringVal(Len.BRN_TER_CLT_ID);
	//Original name: CW46F-BRN-NM-CI
	private char brnNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-BRN-NM
	private String brnNm = DefaultValues.stringVal(Len.BRN_NM);
	//Original name: CW46F-BRN-CLT-ID-CI
	private char brnCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-BRN-CLT-ID
	private String brnCltId = DefaultValues.stringVal(Len.BRN_CLT_ID);
	//Original name: CW46F-AGC-TER-NBR-CI
	private char agcTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AGC-TER-NBR
	private String agcTerNbr = DefaultValues.stringVal(Len.AGC_TER_NBR);
	//Original name: CW46F-AGC-TER-CLT-ID-CI
	private char agcTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AGC-TER-CLT-ID
	private String agcTerCltId = DefaultValues.stringVal(Len.AGC_TER_CLT_ID);
	//Original name: CW46F-AGC-NM-CI
	private char agcNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AGC-NM
	private String agcNm = DefaultValues.stringVal(Len.AGC_NM);
	//Original name: CW46F-AGC-CLT-ID-CI
	private char agcCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AGC-CLT-ID
	private String agcCltId = DefaultValues.stringVal(Len.AGC_CLT_ID);
	//Original name: CW46F-SMR-TER-NBR-CI
	private char smrTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-SMR-TER-NBR
	private String smrTerNbr = DefaultValues.stringVal(Len.SMR_TER_NBR);
	//Original name: CW46F-SMR-TER-CLT-ID-CI
	private char smrTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-SMR-TER-CLT-ID
	private String smrTerCltId = DefaultValues.stringVal(Len.SMR_TER_CLT_ID);
	//Original name: CW46F-SMR-NM-CI
	private char smrNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-SMR-NM
	private String smrNm = DefaultValues.stringVal(Len.SMR_NM);
	//Original name: CW46F-SMR-CLT-ID-CI
	private char smrCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-SMR-CLT-ID
	private String smrCltId = DefaultValues.stringVal(Len.SMR_CLT_ID);
	//Original name: CW46F-AMM-TER-NBR-CI
	private char ammTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AMM-TER-NBR
	private String ammTerNbr = DefaultValues.stringVal(Len.AMM_TER_NBR);
	//Original name: CW46F-AMM-TER-CLT-ID-CI
	private char ammTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AMM-TER-CLT-ID
	private String ammTerCltId = DefaultValues.stringVal(Len.AMM_TER_CLT_ID);
	//Original name: CW46F-AMM-NM-PFX-CI
	private char ammNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AMM-NM-PFX
	private String ammNmPfx = DefaultValues.stringVal(Len.AMM_NM_PFX);
	//Original name: CW46F-AMM-FST-NM-CI
	private char ammFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AMM-FST-NM
	private String ammFstNm = DefaultValues.stringVal(Len.AMM_FST_NM);
	//Original name: CW46F-AMM-MDL-NM-CI
	private char ammMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AMM-MDL-NM
	private String ammMdlNm = DefaultValues.stringVal(Len.AMM_MDL_NM);
	//Original name: CW46F-AMM-LST-NM-CI
	private char ammLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AMM-LST-NM
	private String ammLstNm = DefaultValues.stringVal(Len.AMM_LST_NM);
	//Original name: CW46F-AMM-NM-CI
	private char ammNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AMM-NM
	private String ammNm = DefaultValues.stringVal(Len.AMM_NM);
	//Original name: CW46F-AMM-NM-SFX-CI
	private char ammNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AMM-NM-SFX
	private String ammNmSfx = DefaultValues.stringVal(Len.AMM_NM_SFX);
	//Original name: CW46F-AMM-CLT-ID-CI
	private char ammCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AMM-CLT-ID
	private String ammCltId = DefaultValues.stringVal(Len.AMM_CLT_ID);
	//Original name: CW46F-AFM-TER-NBR-CI
	private char afmTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AFM-TER-NBR
	private String afmTerNbr = DefaultValues.stringVal(Len.AFM_TER_NBR);
	//Original name: CW46F-AFM-TER-CLT-ID-CI
	private char afmTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AFM-TER-CLT-ID
	private String afmTerCltId = DefaultValues.stringVal(Len.AFM_TER_CLT_ID);
	//Original name: CW46F-AFM-NM-PFX-CI
	private char afmNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AFM-NM-PFX
	private String afmNmPfx = DefaultValues.stringVal(Len.AFM_NM_PFX);
	//Original name: CW46F-AFM-FST-NM-CI
	private char afmFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AFM-FST-NM
	private String afmFstNm = DefaultValues.stringVal(Len.AFM_FST_NM);
	//Original name: CW46F-AFM-MDL-NM-CI
	private char afmMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AFM-MDL-NM
	private String afmMdlNm = DefaultValues.stringVal(Len.AFM_MDL_NM);
	//Original name: CW46F-AFM-LST-NM-CI
	private char afmLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AFM-LST-NM
	private String afmLstNm = DefaultValues.stringVal(Len.AFM_LST_NM);
	//Original name: CW46F-AFM-NM-CI
	private char afmNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AFM-NM
	private String afmNm = DefaultValues.stringVal(Len.AFM_NM);
	//Original name: CW46F-AFM-NM-SFX-CI
	private char afmNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AFM-NM-SFX
	private String afmNmSfx = DefaultValues.stringVal(Len.AFM_NM_SFX);
	//Original name: CW46F-AFM-CLT-ID-CI
	private char afmCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AFM-CLT-ID
	private String afmCltId = DefaultValues.stringVal(Len.AFM_CLT_ID);
	//Original name: CW46F-AVP-TER-NBR-CI
	private char avpTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AVP-TER-NBR
	private String avpTerNbr = DefaultValues.stringVal(Len.AVP_TER_NBR);
	//Original name: CW46F-AVP-TER-CLT-ID-CI
	private char avpTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AVP-TER-CLT-ID
	private String avpTerCltId = DefaultValues.stringVal(Len.AVP_TER_CLT_ID);
	//Original name: CW46F-AVP-NM-PFX-CI
	private char avpNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AVP-NM-PFX
	private String avpNmPfx = DefaultValues.stringVal(Len.AVP_NM_PFX);
	//Original name: CW46F-AVP-FST-NM-CI
	private char avpFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AVP-FST-NM
	private String avpFstNm = DefaultValues.stringVal(Len.AVP_FST_NM);
	//Original name: CW46F-AVP-MDL-NM-CI
	private char avpMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AVP-MDL-NM
	private String avpMdlNm = DefaultValues.stringVal(Len.AVP_MDL_NM);
	//Original name: CW46F-AVP-LST-NM-CI
	private char avpLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AVP-LST-NM
	private String avpLstNm = DefaultValues.stringVal(Len.AVP_LST_NM);
	//Original name: CW46F-AVP-NM-CI
	private char avpNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AVP-NM
	private String avpNm = DefaultValues.stringVal(Len.AVP_NM);
	//Original name: CW46F-AVP-NM-SFX-CI
	private char avpNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AVP-NM-SFX
	private String avpNmSfx = DefaultValues.stringVal(Len.AVP_NM_SFX);
	//Original name: CW46F-AVP-CLT-ID-CI
	private char avpCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-AVP-CLT-ID
	private String avpCltId = DefaultValues.stringVal(Len.AVP_CLT_ID);

	//==== METHODS ====
	public void setCltAgcTerStcDataBytes(byte[] buffer, int offset) {
		int position = offset;
		terNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terNbr = MarshalByte.readString(buffer, position, Len.TER_NBR);
		position += Len.TER_NBR;
		brnTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		brnTerNbr = MarshalByte.readString(buffer, position, Len.BRN_TER_NBR);
		position += Len.BRN_TER_NBR;
		brnTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		brnTerCltId = MarshalByte.readString(buffer, position, Len.BRN_TER_CLT_ID);
		position += Len.BRN_TER_CLT_ID;
		brnNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		brnNm = MarshalByte.readString(buffer, position, Len.BRN_NM);
		position += Len.BRN_NM;
		brnCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		brnCltId = MarshalByte.readString(buffer, position, Len.BRN_CLT_ID);
		position += Len.BRN_CLT_ID;
		agcTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		agcTerNbr = MarshalByte.readString(buffer, position, Len.AGC_TER_NBR);
		position += Len.AGC_TER_NBR;
		agcTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		agcTerCltId = MarshalByte.readString(buffer, position, Len.AGC_TER_CLT_ID);
		position += Len.AGC_TER_CLT_ID;
		agcNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		agcNm = MarshalByte.readString(buffer, position, Len.AGC_NM);
		position += Len.AGC_NM;
		agcCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		agcCltId = MarshalByte.readString(buffer, position, Len.AGC_CLT_ID);
		position += Len.AGC_CLT_ID;
		smrTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrTerNbr = MarshalByte.readString(buffer, position, Len.SMR_TER_NBR);
		position += Len.SMR_TER_NBR;
		smrTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrTerCltId = MarshalByte.readString(buffer, position, Len.SMR_TER_CLT_ID);
		position += Len.SMR_TER_CLT_ID;
		smrNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrNm = MarshalByte.readString(buffer, position, Len.SMR_NM);
		position += Len.SMR_NM;
		smrCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrCltId = MarshalByte.readString(buffer, position, Len.SMR_CLT_ID);
		position += Len.SMR_CLT_ID;
		ammTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ammTerNbr = MarshalByte.readString(buffer, position, Len.AMM_TER_NBR);
		position += Len.AMM_TER_NBR;
		ammTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ammTerCltId = MarshalByte.readString(buffer, position, Len.AMM_TER_CLT_ID);
		position += Len.AMM_TER_CLT_ID;
		ammNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ammNmPfx = MarshalByte.readString(buffer, position, Len.AMM_NM_PFX);
		position += Len.AMM_NM_PFX;
		ammFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ammFstNm = MarshalByte.readString(buffer, position, Len.AMM_FST_NM);
		position += Len.AMM_FST_NM;
		ammMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ammMdlNm = MarshalByte.readString(buffer, position, Len.AMM_MDL_NM);
		position += Len.AMM_MDL_NM;
		ammLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ammLstNm = MarshalByte.readString(buffer, position, Len.AMM_LST_NM);
		position += Len.AMM_LST_NM;
		ammNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ammNm = MarshalByte.readString(buffer, position, Len.AMM_NM);
		position += Len.AMM_NM;
		ammNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ammNmSfx = MarshalByte.readString(buffer, position, Len.AMM_NM_SFX);
		position += Len.AMM_NM_SFX;
		ammCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ammCltId = MarshalByte.readString(buffer, position, Len.AMM_CLT_ID);
		position += Len.AMM_CLT_ID;
		afmTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		afmTerNbr = MarshalByte.readString(buffer, position, Len.AFM_TER_NBR);
		position += Len.AFM_TER_NBR;
		afmTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		afmTerCltId = MarshalByte.readString(buffer, position, Len.AFM_TER_CLT_ID);
		position += Len.AFM_TER_CLT_ID;
		afmNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		afmNmPfx = MarshalByte.readString(buffer, position, Len.AFM_NM_PFX);
		position += Len.AFM_NM_PFX;
		afmFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		afmFstNm = MarshalByte.readString(buffer, position, Len.AFM_FST_NM);
		position += Len.AFM_FST_NM;
		afmMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		afmMdlNm = MarshalByte.readString(buffer, position, Len.AFM_MDL_NM);
		position += Len.AFM_MDL_NM;
		afmLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		afmLstNm = MarshalByte.readString(buffer, position, Len.AFM_LST_NM);
		position += Len.AFM_LST_NM;
		afmNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		afmNm = MarshalByte.readString(buffer, position, Len.AFM_NM);
		position += Len.AFM_NM;
		afmNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		afmNmSfx = MarshalByte.readString(buffer, position, Len.AFM_NM_SFX);
		position += Len.AFM_NM_SFX;
		afmCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		afmCltId = MarshalByte.readString(buffer, position, Len.AFM_CLT_ID);
		position += Len.AFM_CLT_ID;
		avpTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		avpTerNbr = MarshalByte.readString(buffer, position, Len.AVP_TER_NBR);
		position += Len.AVP_TER_NBR;
		avpTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		avpTerCltId = MarshalByte.readString(buffer, position, Len.AVP_TER_CLT_ID);
		position += Len.AVP_TER_CLT_ID;
		avpNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		avpNmPfx = MarshalByte.readString(buffer, position, Len.AVP_NM_PFX);
		position += Len.AVP_NM_PFX;
		avpFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		avpFstNm = MarshalByte.readString(buffer, position, Len.AVP_FST_NM);
		position += Len.AVP_FST_NM;
		avpMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		avpMdlNm = MarshalByte.readString(buffer, position, Len.AVP_MDL_NM);
		position += Len.AVP_MDL_NM;
		avpLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		avpLstNm = MarshalByte.readString(buffer, position, Len.AVP_LST_NM);
		position += Len.AVP_LST_NM;
		avpNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		avpNm = MarshalByte.readString(buffer, position, Len.AVP_NM);
		position += Len.AVP_NM;
		avpNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		avpNmSfx = MarshalByte.readString(buffer, position, Len.AVP_NM_SFX);
		position += Len.AVP_NM_SFX;
		avpCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		avpCltId = MarshalByte.readString(buffer, position, Len.AVP_CLT_ID);
	}

	public byte[] getCltAgcTerStcDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, terNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terNbr, Len.TER_NBR);
		position += Len.TER_NBR;
		MarshalByte.writeChar(buffer, position, brnTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, brnTerNbr, Len.BRN_TER_NBR);
		position += Len.BRN_TER_NBR;
		MarshalByte.writeChar(buffer, position, brnTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, brnTerCltId, Len.BRN_TER_CLT_ID);
		position += Len.BRN_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, brnNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, brnNm, Len.BRN_NM);
		position += Len.BRN_NM;
		MarshalByte.writeChar(buffer, position, brnCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, brnCltId, Len.BRN_CLT_ID);
		position += Len.BRN_CLT_ID;
		MarshalByte.writeChar(buffer, position, agcTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, agcTerNbr, Len.AGC_TER_NBR);
		position += Len.AGC_TER_NBR;
		MarshalByte.writeChar(buffer, position, agcTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, agcTerCltId, Len.AGC_TER_CLT_ID);
		position += Len.AGC_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, agcNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, agcNm, Len.AGC_NM);
		position += Len.AGC_NM;
		MarshalByte.writeChar(buffer, position, agcCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, agcCltId, Len.AGC_CLT_ID);
		position += Len.AGC_CLT_ID;
		MarshalByte.writeChar(buffer, position, smrTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrTerNbr, Len.SMR_TER_NBR);
		position += Len.SMR_TER_NBR;
		MarshalByte.writeChar(buffer, position, smrTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrTerCltId, Len.SMR_TER_CLT_ID);
		position += Len.SMR_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, smrNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrNm, Len.SMR_NM);
		position += Len.SMR_NM;
		MarshalByte.writeChar(buffer, position, smrCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrCltId, Len.SMR_CLT_ID);
		position += Len.SMR_CLT_ID;
		MarshalByte.writeChar(buffer, position, ammTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ammTerNbr, Len.AMM_TER_NBR);
		position += Len.AMM_TER_NBR;
		MarshalByte.writeChar(buffer, position, ammTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ammTerCltId, Len.AMM_TER_CLT_ID);
		position += Len.AMM_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, ammNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ammNmPfx, Len.AMM_NM_PFX);
		position += Len.AMM_NM_PFX;
		MarshalByte.writeChar(buffer, position, ammFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ammFstNm, Len.AMM_FST_NM);
		position += Len.AMM_FST_NM;
		MarshalByte.writeChar(buffer, position, ammMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ammMdlNm, Len.AMM_MDL_NM);
		position += Len.AMM_MDL_NM;
		MarshalByte.writeChar(buffer, position, ammLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ammLstNm, Len.AMM_LST_NM);
		position += Len.AMM_LST_NM;
		MarshalByte.writeChar(buffer, position, ammNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ammNm, Len.AMM_NM);
		position += Len.AMM_NM;
		MarshalByte.writeChar(buffer, position, ammNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ammNmSfx, Len.AMM_NM_SFX);
		position += Len.AMM_NM_SFX;
		MarshalByte.writeChar(buffer, position, ammCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ammCltId, Len.AMM_CLT_ID);
		position += Len.AMM_CLT_ID;
		MarshalByte.writeChar(buffer, position, afmTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, afmTerNbr, Len.AFM_TER_NBR);
		position += Len.AFM_TER_NBR;
		MarshalByte.writeChar(buffer, position, afmTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, afmTerCltId, Len.AFM_TER_CLT_ID);
		position += Len.AFM_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, afmNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, afmNmPfx, Len.AFM_NM_PFX);
		position += Len.AFM_NM_PFX;
		MarshalByte.writeChar(buffer, position, afmFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, afmFstNm, Len.AFM_FST_NM);
		position += Len.AFM_FST_NM;
		MarshalByte.writeChar(buffer, position, afmMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, afmMdlNm, Len.AFM_MDL_NM);
		position += Len.AFM_MDL_NM;
		MarshalByte.writeChar(buffer, position, afmLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, afmLstNm, Len.AFM_LST_NM);
		position += Len.AFM_LST_NM;
		MarshalByte.writeChar(buffer, position, afmNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, afmNm, Len.AFM_NM);
		position += Len.AFM_NM;
		MarshalByte.writeChar(buffer, position, afmNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, afmNmSfx, Len.AFM_NM_SFX);
		position += Len.AFM_NM_SFX;
		MarshalByte.writeChar(buffer, position, afmCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, afmCltId, Len.AFM_CLT_ID);
		position += Len.AFM_CLT_ID;
		MarshalByte.writeChar(buffer, position, avpTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, avpTerNbr, Len.AVP_TER_NBR);
		position += Len.AVP_TER_NBR;
		MarshalByte.writeChar(buffer, position, avpTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, avpTerCltId, Len.AVP_TER_CLT_ID);
		position += Len.AVP_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, avpNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, avpNmPfx, Len.AVP_NM_PFX);
		position += Len.AVP_NM_PFX;
		MarshalByte.writeChar(buffer, position, avpFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, avpFstNm, Len.AVP_FST_NM);
		position += Len.AVP_FST_NM;
		MarshalByte.writeChar(buffer, position, avpMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, avpMdlNm, Len.AVP_MDL_NM);
		position += Len.AVP_MDL_NM;
		MarshalByte.writeChar(buffer, position, avpLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, avpLstNm, Len.AVP_LST_NM);
		position += Len.AVP_LST_NM;
		MarshalByte.writeChar(buffer, position, avpNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, avpNm, Len.AVP_NM);
		position += Len.AVP_NM;
		MarshalByte.writeChar(buffer, position, avpNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, avpNmSfx, Len.AVP_NM_SFX);
		position += Len.AVP_NM_SFX;
		MarshalByte.writeChar(buffer, position, avpCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, avpCltId, Len.AVP_CLT_ID);
		return buffer;
	}

	public void setTerNbrCi(char terNbrCi) {
		this.terNbrCi = terNbrCi;
	}

	public char getTerNbrCi() {
		return this.terNbrCi;
	}

	public void setTerNbr(String terNbr) {
		this.terNbr = Functions.subString(terNbr, Len.TER_NBR);
	}

	public String getTerNbr() {
		return this.terNbr;
	}

	public void setBrnTerNbrCi(char brnTerNbrCi) {
		this.brnTerNbrCi = brnTerNbrCi;
	}

	public char getBrnTerNbrCi() {
		return this.brnTerNbrCi;
	}

	public void setBrnTerNbr(String brnTerNbr) {
		this.brnTerNbr = Functions.subString(brnTerNbr, Len.BRN_TER_NBR);
	}

	public String getBrnTerNbr() {
		return this.brnTerNbr;
	}

	public void setBrnTerCltIdCi(char brnTerCltIdCi) {
		this.brnTerCltIdCi = brnTerCltIdCi;
	}

	public char getBrnTerCltIdCi() {
		return this.brnTerCltIdCi;
	}

	public void setBrnTerCltId(String brnTerCltId) {
		this.brnTerCltId = Functions.subString(brnTerCltId, Len.BRN_TER_CLT_ID);
	}

	public String getBrnTerCltId() {
		return this.brnTerCltId;
	}

	public void setBrnNmCi(char brnNmCi) {
		this.brnNmCi = brnNmCi;
	}

	public char getBrnNmCi() {
		return this.brnNmCi;
	}

	public void setBrnNm(String brnNm) {
		this.brnNm = Functions.subString(brnNm, Len.BRN_NM);
	}

	public String getBrnNm() {
		return this.brnNm;
	}

	public void setBrnCltIdCi(char brnCltIdCi) {
		this.brnCltIdCi = brnCltIdCi;
	}

	public char getBrnCltIdCi() {
		return this.brnCltIdCi;
	}

	public void setBrnCltId(String brnCltId) {
		this.brnCltId = Functions.subString(brnCltId, Len.BRN_CLT_ID);
	}

	public String getBrnCltId() {
		return this.brnCltId;
	}

	public void setAgcTerNbrCi(char agcTerNbrCi) {
		this.agcTerNbrCi = agcTerNbrCi;
	}

	public char getAgcTerNbrCi() {
		return this.agcTerNbrCi;
	}

	public void setAgcTerNbr(String agcTerNbr) {
		this.agcTerNbr = Functions.subString(agcTerNbr, Len.AGC_TER_NBR);
	}

	public String getAgcTerNbr() {
		return this.agcTerNbr;
	}

	public void setAgcTerCltIdCi(char agcTerCltIdCi) {
		this.agcTerCltIdCi = agcTerCltIdCi;
	}

	public char getAgcTerCltIdCi() {
		return this.agcTerCltIdCi;
	}

	public void setAgcTerCltId(String agcTerCltId) {
		this.agcTerCltId = Functions.subString(agcTerCltId, Len.AGC_TER_CLT_ID);
	}

	public String getAgcTerCltId() {
		return this.agcTerCltId;
	}

	public void setAgcNmCi(char agcNmCi) {
		this.agcNmCi = agcNmCi;
	}

	public char getAgcNmCi() {
		return this.agcNmCi;
	}

	public void setAgcNm(String agcNm) {
		this.agcNm = Functions.subString(agcNm, Len.AGC_NM);
	}

	public String getAgcNm() {
		return this.agcNm;
	}

	public void setAgcCltIdCi(char agcCltIdCi) {
		this.agcCltIdCi = agcCltIdCi;
	}

	public char getAgcCltIdCi() {
		return this.agcCltIdCi;
	}

	public void setAgcCltId(String agcCltId) {
		this.agcCltId = Functions.subString(agcCltId, Len.AGC_CLT_ID);
	}

	public String getAgcCltId() {
		return this.agcCltId;
	}

	public void setSmrTerNbrCi(char smrTerNbrCi) {
		this.smrTerNbrCi = smrTerNbrCi;
	}

	public char getSmrTerNbrCi() {
		return this.smrTerNbrCi;
	}

	public void setSmrTerNbr(String smrTerNbr) {
		this.smrTerNbr = Functions.subString(smrTerNbr, Len.SMR_TER_NBR);
	}

	public String getSmrTerNbr() {
		return this.smrTerNbr;
	}

	public void setSmrTerCltIdCi(char smrTerCltIdCi) {
		this.smrTerCltIdCi = smrTerCltIdCi;
	}

	public char getSmrTerCltIdCi() {
		return this.smrTerCltIdCi;
	}

	public void setSmrTerCltId(String smrTerCltId) {
		this.smrTerCltId = Functions.subString(smrTerCltId, Len.SMR_TER_CLT_ID);
	}

	public String getSmrTerCltId() {
		return this.smrTerCltId;
	}

	public void setSmrNmCi(char smrNmCi) {
		this.smrNmCi = smrNmCi;
	}

	public char getSmrNmCi() {
		return this.smrNmCi;
	}

	public void setSmrNm(String smrNm) {
		this.smrNm = Functions.subString(smrNm, Len.SMR_NM);
	}

	public String getSmrNm() {
		return this.smrNm;
	}

	public void setSmrCltIdCi(char smrCltIdCi) {
		this.smrCltIdCi = smrCltIdCi;
	}

	public char getSmrCltIdCi() {
		return this.smrCltIdCi;
	}

	public void setSmrCltId(String smrCltId) {
		this.smrCltId = Functions.subString(smrCltId, Len.SMR_CLT_ID);
	}

	public String getSmrCltId() {
		return this.smrCltId;
	}

	public void setAmmTerNbrCi(char ammTerNbrCi) {
		this.ammTerNbrCi = ammTerNbrCi;
	}

	public char getAmmTerNbrCi() {
		return this.ammTerNbrCi;
	}

	public void setAmmTerNbr(String ammTerNbr) {
		this.ammTerNbr = Functions.subString(ammTerNbr, Len.AMM_TER_NBR);
	}

	public String getAmmTerNbr() {
		return this.ammTerNbr;
	}

	public void setAmmTerCltIdCi(char ammTerCltIdCi) {
		this.ammTerCltIdCi = ammTerCltIdCi;
	}

	public char getAmmTerCltIdCi() {
		return this.ammTerCltIdCi;
	}

	public void setAmmTerCltId(String ammTerCltId) {
		this.ammTerCltId = Functions.subString(ammTerCltId, Len.AMM_TER_CLT_ID);
	}

	public String getAmmTerCltId() {
		return this.ammTerCltId;
	}

	public void setAmmNmPfxCi(char ammNmPfxCi) {
		this.ammNmPfxCi = ammNmPfxCi;
	}

	public char getAmmNmPfxCi() {
		return this.ammNmPfxCi;
	}

	public void setAmmNmPfx(String ammNmPfx) {
		this.ammNmPfx = Functions.subString(ammNmPfx, Len.AMM_NM_PFX);
	}

	public String getAmmNmPfx() {
		return this.ammNmPfx;
	}

	public void setAmmFstNmCi(char ammFstNmCi) {
		this.ammFstNmCi = ammFstNmCi;
	}

	public char getAmmFstNmCi() {
		return this.ammFstNmCi;
	}

	public void setAmmFstNm(String ammFstNm) {
		this.ammFstNm = Functions.subString(ammFstNm, Len.AMM_FST_NM);
	}

	public String getAmmFstNm() {
		return this.ammFstNm;
	}

	public void setAmmMdlNmCi(char ammMdlNmCi) {
		this.ammMdlNmCi = ammMdlNmCi;
	}

	public char getAmmMdlNmCi() {
		return this.ammMdlNmCi;
	}

	public void setAmmMdlNm(String ammMdlNm) {
		this.ammMdlNm = Functions.subString(ammMdlNm, Len.AMM_MDL_NM);
	}

	public String getAmmMdlNm() {
		return this.ammMdlNm;
	}

	public void setAmmLstNmCi(char ammLstNmCi) {
		this.ammLstNmCi = ammLstNmCi;
	}

	public char getAmmLstNmCi() {
		return this.ammLstNmCi;
	}

	public void setAmmLstNm(String ammLstNm) {
		this.ammLstNm = Functions.subString(ammLstNm, Len.AMM_LST_NM);
	}

	public String getAmmLstNm() {
		return this.ammLstNm;
	}

	public void setAmmNmCi(char ammNmCi) {
		this.ammNmCi = ammNmCi;
	}

	public char getAmmNmCi() {
		return this.ammNmCi;
	}

	public void setAmmNm(String ammNm) {
		this.ammNm = Functions.subString(ammNm, Len.AMM_NM);
	}

	public String getAmmNm() {
		return this.ammNm;
	}

	public void setAmmNmSfxCi(char ammNmSfxCi) {
		this.ammNmSfxCi = ammNmSfxCi;
	}

	public char getAmmNmSfxCi() {
		return this.ammNmSfxCi;
	}

	public void setAmmNmSfx(String ammNmSfx) {
		this.ammNmSfx = Functions.subString(ammNmSfx, Len.AMM_NM_SFX);
	}

	public String getAmmNmSfx() {
		return this.ammNmSfx;
	}

	public void setAmmCltIdCi(char ammCltIdCi) {
		this.ammCltIdCi = ammCltIdCi;
	}

	public char getAmmCltIdCi() {
		return this.ammCltIdCi;
	}

	public void setAmmCltId(String ammCltId) {
		this.ammCltId = Functions.subString(ammCltId, Len.AMM_CLT_ID);
	}

	public String getAmmCltId() {
		return this.ammCltId;
	}

	public void setAfmTerNbrCi(char afmTerNbrCi) {
		this.afmTerNbrCi = afmTerNbrCi;
	}

	public char getAfmTerNbrCi() {
		return this.afmTerNbrCi;
	}

	public void setAfmTerNbr(String afmTerNbr) {
		this.afmTerNbr = Functions.subString(afmTerNbr, Len.AFM_TER_NBR);
	}

	public String getAfmTerNbr() {
		return this.afmTerNbr;
	}

	public void setAfmTerCltIdCi(char afmTerCltIdCi) {
		this.afmTerCltIdCi = afmTerCltIdCi;
	}

	public char getAfmTerCltIdCi() {
		return this.afmTerCltIdCi;
	}

	public void setAfmTerCltId(String afmTerCltId) {
		this.afmTerCltId = Functions.subString(afmTerCltId, Len.AFM_TER_CLT_ID);
	}

	public String getAfmTerCltId() {
		return this.afmTerCltId;
	}

	public void setAfmNmPfxCi(char afmNmPfxCi) {
		this.afmNmPfxCi = afmNmPfxCi;
	}

	public char getAfmNmPfxCi() {
		return this.afmNmPfxCi;
	}

	public void setAfmNmPfx(String afmNmPfx) {
		this.afmNmPfx = Functions.subString(afmNmPfx, Len.AFM_NM_PFX);
	}

	public String getAfmNmPfx() {
		return this.afmNmPfx;
	}

	public void setAfmFstNmCi(char afmFstNmCi) {
		this.afmFstNmCi = afmFstNmCi;
	}

	public char getAfmFstNmCi() {
		return this.afmFstNmCi;
	}

	public void setAfmFstNm(String afmFstNm) {
		this.afmFstNm = Functions.subString(afmFstNm, Len.AFM_FST_NM);
	}

	public String getAfmFstNm() {
		return this.afmFstNm;
	}

	public void setAfmMdlNmCi(char afmMdlNmCi) {
		this.afmMdlNmCi = afmMdlNmCi;
	}

	public char getAfmMdlNmCi() {
		return this.afmMdlNmCi;
	}

	public void setAfmMdlNm(String afmMdlNm) {
		this.afmMdlNm = Functions.subString(afmMdlNm, Len.AFM_MDL_NM);
	}

	public String getAfmMdlNm() {
		return this.afmMdlNm;
	}

	public void setAfmLstNmCi(char afmLstNmCi) {
		this.afmLstNmCi = afmLstNmCi;
	}

	public char getAfmLstNmCi() {
		return this.afmLstNmCi;
	}

	public void setAfmLstNm(String afmLstNm) {
		this.afmLstNm = Functions.subString(afmLstNm, Len.AFM_LST_NM);
	}

	public String getAfmLstNm() {
		return this.afmLstNm;
	}

	public void setAfmNmCi(char afmNmCi) {
		this.afmNmCi = afmNmCi;
	}

	public char getAfmNmCi() {
		return this.afmNmCi;
	}

	public void setAfmNm(String afmNm) {
		this.afmNm = Functions.subString(afmNm, Len.AFM_NM);
	}

	public String getAfmNm() {
		return this.afmNm;
	}

	public void setAfmNmSfxCi(char afmNmSfxCi) {
		this.afmNmSfxCi = afmNmSfxCi;
	}

	public char getAfmNmSfxCi() {
		return this.afmNmSfxCi;
	}

	public void setAfmNmSfx(String afmNmSfx) {
		this.afmNmSfx = Functions.subString(afmNmSfx, Len.AFM_NM_SFX);
	}

	public String getAfmNmSfx() {
		return this.afmNmSfx;
	}

	public void setAfmCltIdCi(char afmCltIdCi) {
		this.afmCltIdCi = afmCltIdCi;
	}

	public char getAfmCltIdCi() {
		return this.afmCltIdCi;
	}

	public void setAfmCltId(String afmCltId) {
		this.afmCltId = Functions.subString(afmCltId, Len.AFM_CLT_ID);
	}

	public String getAfmCltId() {
		return this.afmCltId;
	}

	public void setAvpTerNbrCi(char avpTerNbrCi) {
		this.avpTerNbrCi = avpTerNbrCi;
	}

	public char getAvpTerNbrCi() {
		return this.avpTerNbrCi;
	}

	public void setAvpTerNbr(String avpTerNbr) {
		this.avpTerNbr = Functions.subString(avpTerNbr, Len.AVP_TER_NBR);
	}

	public String getAvpTerNbr() {
		return this.avpTerNbr;
	}

	public void setAvpTerCltIdCi(char avpTerCltIdCi) {
		this.avpTerCltIdCi = avpTerCltIdCi;
	}

	public char getAvpTerCltIdCi() {
		return this.avpTerCltIdCi;
	}

	public void setAvpTerCltId(String avpTerCltId) {
		this.avpTerCltId = Functions.subString(avpTerCltId, Len.AVP_TER_CLT_ID);
	}

	public String getAvpTerCltId() {
		return this.avpTerCltId;
	}

	public void setAvpNmPfxCi(char avpNmPfxCi) {
		this.avpNmPfxCi = avpNmPfxCi;
	}

	public char getAvpNmPfxCi() {
		return this.avpNmPfxCi;
	}

	public void setAvpNmPfx(String avpNmPfx) {
		this.avpNmPfx = Functions.subString(avpNmPfx, Len.AVP_NM_PFX);
	}

	public String getAvpNmPfx() {
		return this.avpNmPfx;
	}

	public void setAvpFstNmCi(char avpFstNmCi) {
		this.avpFstNmCi = avpFstNmCi;
	}

	public char getAvpFstNmCi() {
		return this.avpFstNmCi;
	}

	public void setAvpFstNm(String avpFstNm) {
		this.avpFstNm = Functions.subString(avpFstNm, Len.AVP_FST_NM);
	}

	public String getAvpFstNm() {
		return this.avpFstNm;
	}

	public void setAvpMdlNmCi(char avpMdlNmCi) {
		this.avpMdlNmCi = avpMdlNmCi;
	}

	public char getAvpMdlNmCi() {
		return this.avpMdlNmCi;
	}

	public void setAvpMdlNm(String avpMdlNm) {
		this.avpMdlNm = Functions.subString(avpMdlNm, Len.AVP_MDL_NM);
	}

	public String getAvpMdlNm() {
		return this.avpMdlNm;
	}

	public void setAvpLstNmCi(char avpLstNmCi) {
		this.avpLstNmCi = avpLstNmCi;
	}

	public char getAvpLstNmCi() {
		return this.avpLstNmCi;
	}

	public void setAvpLstNm(String avpLstNm) {
		this.avpLstNm = Functions.subString(avpLstNm, Len.AVP_LST_NM);
	}

	public String getAvpLstNm() {
		return this.avpLstNm;
	}

	public void setAvpNmCi(char avpNmCi) {
		this.avpNmCi = avpNmCi;
	}

	public char getAvpNmCi() {
		return this.avpNmCi;
	}

	public void setAvpNm(String avpNm) {
		this.avpNm = Functions.subString(avpNm, Len.AVP_NM);
	}

	public String getAvpNm() {
		return this.avpNm;
	}

	public void setAvpNmSfxCi(char avpNmSfxCi) {
		this.avpNmSfxCi = avpNmSfxCi;
	}

	public char getAvpNmSfxCi() {
		return this.avpNmSfxCi;
	}

	public void setAvpNmSfx(String avpNmSfx) {
		this.avpNmSfx = Functions.subString(avpNmSfx, Len.AVP_NM_SFX);
	}

	public String getAvpNmSfx() {
		return this.avpNmSfx;
	}

	public void setAvpCltIdCi(char avpCltIdCi) {
		this.avpCltIdCi = avpCltIdCi;
	}

	public char getAvpCltIdCi() {
		return this.avpCltIdCi;
	}

	public void setAvpCltId(String avpCltId) {
		this.avpCltId = Functions.subString(avpCltId, Len.AVP_CLT_ID);
	}

	public String getAvpCltId() {
		return this.avpCltId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TER_NBR = 4;
		public static final int BRN_TER_NBR = 4;
		public static final int BRN_TER_CLT_ID = 20;
		public static final int BRN_NM = 60;
		public static final int BRN_CLT_ID = 20;
		public static final int AGC_TER_NBR = 4;
		public static final int AGC_TER_CLT_ID = 20;
		public static final int AGC_NM = 60;
		public static final int AGC_CLT_ID = 20;
		public static final int SMR_TER_NBR = 4;
		public static final int SMR_TER_CLT_ID = 20;
		public static final int SMR_NM = 60;
		public static final int SMR_CLT_ID = 20;
		public static final int AMM_TER_NBR = 4;
		public static final int AMM_TER_CLT_ID = 20;
		public static final int AMM_NM_PFX = 4;
		public static final int AMM_FST_NM = 30;
		public static final int AMM_MDL_NM = 30;
		public static final int AMM_LST_NM = 60;
		public static final int AMM_NM = 120;
		public static final int AMM_NM_SFX = 4;
		public static final int AMM_CLT_ID = 20;
		public static final int AFM_TER_NBR = 4;
		public static final int AFM_TER_CLT_ID = 20;
		public static final int AFM_NM_PFX = 4;
		public static final int AFM_FST_NM = 30;
		public static final int AFM_MDL_NM = 30;
		public static final int AFM_LST_NM = 60;
		public static final int AFM_NM = 120;
		public static final int AFM_NM_SFX = 4;
		public static final int AFM_CLT_ID = 20;
		public static final int AVP_TER_NBR = 4;
		public static final int AVP_TER_CLT_ID = 20;
		public static final int AVP_NM_PFX = 4;
		public static final int AVP_FST_NM = 30;
		public static final int AVP_MDL_NM = 30;
		public static final int AVP_LST_NM = 60;
		public static final int AVP_NM = 120;
		public static final int AVP_NM_SFX = 4;
		public static final int AVP_CLT_ID = 20;
		public static final int TER_NBR_CI = 1;
		public static final int BRN_TER_NBR_CI = 1;
		public static final int BRN_TER_CLT_ID_CI = 1;
		public static final int BRN_NM_CI = 1;
		public static final int BRN_CLT_ID_CI = 1;
		public static final int AGC_TER_NBR_CI = 1;
		public static final int AGC_TER_CLT_ID_CI = 1;
		public static final int AGC_NM_CI = 1;
		public static final int AGC_CLT_ID_CI = 1;
		public static final int SMR_TER_NBR_CI = 1;
		public static final int SMR_TER_CLT_ID_CI = 1;
		public static final int SMR_NM_CI = 1;
		public static final int SMR_CLT_ID_CI = 1;
		public static final int AMM_TER_NBR_CI = 1;
		public static final int AMM_TER_CLT_ID_CI = 1;
		public static final int AMM_NM_PFX_CI = 1;
		public static final int AMM_FST_NM_CI = 1;
		public static final int AMM_MDL_NM_CI = 1;
		public static final int AMM_LST_NM_CI = 1;
		public static final int AMM_NM_CI = 1;
		public static final int AMM_NM_SFX_CI = 1;
		public static final int AMM_CLT_ID_CI = 1;
		public static final int AFM_TER_NBR_CI = 1;
		public static final int AFM_TER_CLT_ID_CI = 1;
		public static final int AFM_NM_PFX_CI = 1;
		public static final int AFM_FST_NM_CI = 1;
		public static final int AFM_MDL_NM_CI = 1;
		public static final int AFM_LST_NM_CI = 1;
		public static final int AFM_NM_CI = 1;
		public static final int AFM_NM_SFX_CI = 1;
		public static final int AFM_CLT_ID_CI = 1;
		public static final int AVP_TER_NBR_CI = 1;
		public static final int AVP_TER_CLT_ID_CI = 1;
		public static final int AVP_NM_PFX_CI = 1;
		public static final int AVP_FST_NM_CI = 1;
		public static final int AVP_MDL_NM_CI = 1;
		public static final int AVP_LST_NM_CI = 1;
		public static final int AVP_NM_CI = 1;
		public static final int AVP_NM_SFX_CI = 1;
		public static final int AVP_CLT_ID_CI = 1;
		public static final int CLT_AGC_TER_STC_DATA = TER_NBR_CI + TER_NBR + BRN_TER_NBR_CI + BRN_TER_NBR + BRN_TER_CLT_ID_CI + BRN_TER_CLT_ID
				+ BRN_NM_CI + BRN_NM + BRN_CLT_ID_CI + BRN_CLT_ID + AGC_TER_NBR_CI + AGC_TER_NBR + AGC_TER_CLT_ID_CI + AGC_TER_CLT_ID + AGC_NM_CI
				+ AGC_NM + AGC_CLT_ID_CI + AGC_CLT_ID + SMR_TER_NBR_CI + SMR_TER_NBR + SMR_TER_CLT_ID_CI + SMR_TER_CLT_ID + SMR_NM_CI + SMR_NM
				+ SMR_CLT_ID_CI + SMR_CLT_ID + AMM_TER_NBR_CI + AMM_TER_NBR + AMM_TER_CLT_ID_CI + AMM_TER_CLT_ID + AMM_NM_PFX_CI + AMM_NM_PFX
				+ AMM_FST_NM_CI + AMM_FST_NM + AMM_MDL_NM_CI + AMM_MDL_NM + AMM_LST_NM_CI + AMM_LST_NM + AMM_NM_CI + AMM_NM + AMM_NM_SFX_CI
				+ AMM_NM_SFX + AMM_CLT_ID_CI + AMM_CLT_ID + AFM_TER_NBR_CI + AFM_TER_NBR + AFM_TER_CLT_ID_CI + AFM_TER_CLT_ID + AFM_NM_PFX_CI
				+ AFM_NM_PFX + AFM_FST_NM_CI + AFM_FST_NM + AFM_MDL_NM_CI + AFM_MDL_NM + AFM_LST_NM_CI + AFM_LST_NM + AFM_NM_CI + AFM_NM
				+ AFM_NM_SFX_CI + AFM_NM_SFX + AFM_CLT_ID_CI + AFM_CLT_ID + AVP_TER_NBR_CI + AVP_TER_NBR + AVP_TER_CLT_ID_CI + AVP_TER_CLT_ID
				+ AVP_NM_PFX_CI + AVP_NM_PFX + AVP_FST_NM_CI + AVP_FST_NM + AVP_MDL_NM_CI + AVP_MDL_NM + AVP_LST_NM_CI + AVP_LST_NM + AVP_NM_CI
				+ AVP_NM + AVP_NM_SFX_CI + AVP_NM_SFX + AVP_CLT_ID_CI + AVP_CLT_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
