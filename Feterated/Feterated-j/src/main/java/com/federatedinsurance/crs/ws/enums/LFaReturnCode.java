/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: L-FA-RETURN-CODE<br>
 * Variable: L-FA-RETURN-CODE from copybook TS52901<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LFaReturnCode {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.RETURN_CODE);
	public static final String GOOD_RUN = "00";
	public static final String INPUT_ERR = "20";
	public static final String TRUNCATION_OCCURRED = "30";
	public static final String FORMATTED_NM_TOO_LONG = "35";

	//==== METHODS ====
	public void setReturnCode(short returnCode) {
		this.value = NumericDisplay.asString(returnCode, Len.RETURN_CODE);
	}

	public void setReturnCodeFormatted(String returnCode) {
		this.value = Trunc.toUnsignedNumeric(returnCode, Len.RETURN_CODE);
	}

	public short getReturnCode() {
		return NumericDisplay.asShort(this.value);
	}

	public String getReturnCodeFormatted() {
		return this.value;
	}

	public String getReturnCodeAsString() {
		return getReturnCodeFormatted();
	}

	public boolean isFaRcGoodRun() {
		return getReturnCodeFormatted().equals(GOOD_RUN);
	}

	public void setGoodRun() {
		setReturnCodeFormatted(GOOD_RUN);
	}

	public void setInputErr() {
		setReturnCodeFormatted(INPUT_ERR);
	}

	public void setTruncationOccurred() {
		setReturnCodeFormatted(TRUNCATION_OCCURRED);
	}

	public void setFormattedNmTooLong() {
		setReturnCodeFormatted(FORMATTED_NM_TOO_LONG);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURN_CODE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
