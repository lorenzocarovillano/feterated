/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P90E0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p90e0 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-CERT-POLICIES
	private short maxCertPolicies = ((short) 50);
	//Original name: CF-MAX-EXT-MSG
	private short maxExtMsg = ((short) 10);
	//Original name: CF-MAX-TBL-ENTRIES-POLICIES
	private short maxTblEntriesPolicies = ((short) 100);
	//Original name: CF-MAX-TBL-ENTRIES-RECIPIENTS
	private short maxTblEntriesRecipients = ((short) 4500);
	//Original name: CF-NULL
	private short nullFld = ((short) -1);
	//Original name: CF-BILLING-USERID
	private String billingUserid = "BILLSRV";
	//Original name: CF-GET-ACT-BILL-DTL-PGM
	private String getActBillDtlPgm = "BX0G0001";
	//Original name: CF-PCN-COUNT-PGM
	private String pcnCountPgm = "XZ0G0001";
	//Original name: CF-CERT-POL-LIS-PGM
	private String certPolLisPgm = "XZC03090";
	//Original name: CF-SERVICE-PROXY
	private CfServiceProxyXz0p90e0 serviceProxy = new CfServiceProxyXz0p90e0();
	//Original name: CF-YES
	private char yes = 'Y';
	//Original name: CF-NO-FRM-ATC-WNG-NOT-TYP
	private CfNoFrmAtcWngNotTyp noFrmAtcWngNotTyp = new CfNoFrmAtcWngNotTyp();

	//==== METHODS ====
	public short getMaxCertPolicies() {
		return this.maxCertPolicies;
	}

	public short getMaxExtMsg() {
		return this.maxExtMsg;
	}

	public short getMaxTblEntriesPolicies() {
		return this.maxTblEntriesPolicies;
	}

	public short getMaxTblEntriesRecipients() {
		return this.maxTblEntriesRecipients;
	}

	public short getNullFld() {
		return this.nullFld;
	}

	public String getBillingUserid() {
		return this.billingUserid;
	}

	public String getGetActBillDtlPgm() {
		return this.getActBillDtlPgm;
	}

	public String getPcnCountPgm() {
		return this.pcnCountPgm;
	}

	public String getCertPolLisPgm() {
		return this.certPolLisPgm;
	}

	public char getYes() {
		return this.yes;
	}

	public CfNoFrmAtcWngNotTyp getNoFrmAtcWngNotTyp() {
		return noFrmAtcWngNotTyp;
	}

	public CfServiceProxyXz0p90e0 getServiceProxy() {
		return serviceProxy;
	}
}
