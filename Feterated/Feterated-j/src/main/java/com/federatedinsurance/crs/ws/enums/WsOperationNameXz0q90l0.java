/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-OPERATION-NAME<br>
 * Variable: WS-OPERATION-NAME from program XZ0Q90L0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsOperationNameXz0q90l0 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.OPERATION_NAME);
	public static final String PREPARE_RESC_NOT = "PrepareRescindNotification";
	public static final String VALID_OPERATION = "PrepareRescindNotification";

	//==== METHODS ====
	public void setOperationName(String operationName) {
		this.value = Functions.subString(operationName, Len.OPERATION_NAME);
	}

	public String getOperationName() {
		return this.value;
	}

	public boolean isValidOperation() {
		return value.equals(VALID_OPERATION);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPERATION_NAME = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
