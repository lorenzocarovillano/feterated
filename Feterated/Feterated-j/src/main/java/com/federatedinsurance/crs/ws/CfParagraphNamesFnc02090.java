/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-PARAGRAPH-NAMES<br>
 * Variable: CF-PARAGRAPH-NAMES from program FNC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfParagraphNamesFnc02090 {

	//==== PROPERTIES ====
	//Original name: CF-PN-2100
	private String pn2100 = "2100";
	//Original name: CF-PN-2200
	private String pn2200 = "2200";
	//Original name: CF-PN-3210
	private String pn3210 = "3210";
	//Original name: CF-PN-3300
	private String pn3300 = "3300";

	//==== METHODS ====
	public String getPn2100() {
		return this.pn2100;
	}

	public String getPn2200() {
		return this.pn2200;
	}

	public String getPn3210() {
		return this.pn3210;
	}

	public String getPn3300() {
		return this.pn3300;
	}
}
