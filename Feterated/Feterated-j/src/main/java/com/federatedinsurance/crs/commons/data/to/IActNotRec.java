/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ACT_NOT_REC]
 * 
 */
public interface IActNotRec extends BaseSqlTo {

	/**
	 * Host Variable REC-SEQ-NBR
	 * 
	 */
	short getRecSeqNbr();

	void setRecSeqNbr(short recSeqNbr);

	/**
	 * Host Variable REC-TYP-CD
	 * 
	 */
	String getRecTypCd();

	void setRecTypCd(String recTypCd);

	/**
	 * Host Variable REC-NM
	 * 
	 */
	String getRecNm();

	void setRecNm(String recNm);

	/**
	 * Nullable property for REC-NM
	 * 
	 */
	String getRecNmObj();

	void setRecNmObj(String recNmObj);

	/**
	 * Host Variable LIN-1-ADR
	 * 
	 */
	String getLin1Adr();

	void setLin1Adr(String lin1Adr);

	/**
	 * Nullable property for LIN-1-ADR
	 * 
	 */
	String getLin1AdrObj();

	void setLin1AdrObj(String lin1AdrObj);

	/**
	 * Host Variable LIN-2-ADR
	 * 
	 */
	String getLin2Adr();

	void setLin2Adr(String lin2Adr);

	/**
	 * Nullable property for LIN-2-ADR
	 * 
	 */
	String getLin2AdrObj();

	void setLin2AdrObj(String lin2AdrObj);

	/**
	 * Host Variable CIT-NM
	 * 
	 */
	String getCitNm();

	void setCitNm(String citNm);

	/**
	 * Nullable property for CIT-NM
	 * 
	 */
	String getCitNmObj();

	void setCitNmObj(String citNmObj);

	/**
	 * Host Variable ST-ABB
	 * 
	 */
	String getStAbb();

	void setStAbb(String stAbb);

	/**
	 * Nullable property for ST-ABB
	 * 
	 */
	String getStAbbObj();

	void setStAbbObj(String stAbbObj);

	/**
	 * Host Variable PST-CD
	 * 
	 */
	String getPstCd();

	void setPstCd(String pstCd);

	/**
	 * Nullable property for PST-CD
	 * 
	 */
	String getPstCdObj();

	void setPstCdObj(String pstCdObj);

	/**
	 * Host Variable CER-NBR
	 * 
	 */
	String getCerNbr();

	void setCerNbr(String cerNbr);

	/**
	 * Nullable property for CER-NBR
	 * 
	 */
	String getCerNbrObj();

	void setCerNbrObj(String cerNbrObj);

	/**
	 * Host Variable XZH003-CSR-ACT-NBR
	 * 
	 */
	String getCsrActNbr();

	void setCsrActNbr(String csrActNbr);

	/**
	 * Host Variable XZH003-NOT-PRC-TS
	 * 
	 */
	String getNotPrcTs();

	void setNotPrcTs(String notPrcTs);

	/**
	 * Host Variable XZH003-REC-CLT-ID
	 * 
	 */
	String getRecCltId();

	void setRecCltId(String recCltId);

	/**
	 * Nullable property for XZH003-REC-CLT-ID
	 * 
	 */
	String getRecCltIdObj();

	void setRecCltIdObj(String recCltIdObj);

	/**
	 * Host Variable XZH003-REC-ADR-ID
	 * 
	 */
	String getRecAdrId();

	void setRecAdrId(String recAdrId);

	/**
	 * Nullable property for XZH003-REC-ADR-ID
	 * 
	 */
	String getRecAdrIdObj();

	void setRecAdrIdObj(String recAdrIdObj);

	/**
	 * Host Variable XZH003-MNL-IND
	 * 
	 */
	char getMnlInd();

	void setMnlInd(char mnlInd);

	/**
	 * Host Variable REC-CLT-ID
	 * 
	 */
	String getCltId();

	void setCltId(String cltId);

	/**
	 * Nullable property for REC-CLT-ID
	 * 
	 */
	String getCltIdObj();

	void setCltIdObj(String cltIdObj);

	/**
	 * Host Variable REC-ADR-ID
	 * 
	 */
	String getAdrId();

	void setAdrId(String adrId);

	/**
	 * Nullable property for REC-ADR-ID
	 * 
	 */
	String getAdrIdObj();

	void setAdrIdObj(String adrIdObj);
};
