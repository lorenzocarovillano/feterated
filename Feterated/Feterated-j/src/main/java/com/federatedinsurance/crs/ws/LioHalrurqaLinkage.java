/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.notifier.StringChangeNotifier;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.HalrurqaFunction;
import com.federatedinsurance.crs.ws.enums.HalrurqaRecFoundSw;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LIO-HALRURQA-LINKAGE<br>
 * Variable: LIO-HALRURQA-LINKAGE from program HALRURQA<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LioHalrurqaLinkage extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: HALRURQA-HALRURQA-LIT
	private String halrurqaLit = "HALRURQA";
	//Original name: HALRURQA-FUNCTION
	private HalrurqaFunction function = new HalrurqaFunction();
	//Original name: HALRURQA-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: HALRURQA-REC-FOUND-SW
	private HalrurqaRecFoundSw recFoundSw = new HalrurqaRecFoundSw();
	//Original name: HALRURQA-REC-SEQ
	private String recSeq = DefaultValues.stringVal(Len.REC_SEQ);
	/**Original name: HALRURQA-ACTION-CODE<br>
	 * <pre>**     05 HALRURQA-REC-SEQ           PIC 9(03).</pre>*/
	private String actionCode = DefaultValues.stringVal(Len.ACTION_CODE);
	//Original name: HALRURQA-BUS-OBJ-DATA-LENGTH
	private StringChangeNotifier busObjDataLength = new StringChangeNotifier(Len.BUS_OBJ_DATA_LENGTH);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.LIO_HALRURQA_LINKAGE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setLioHalrurqaLinkageBytes(buf);
	}

	public void setLioHalrurqaLinkageBytes(byte[] buffer) {
		setLioHalrurqaLinkageBytes(buffer, 1);
	}

	public byte[] getLioHalrurqaLinkageBytes() {
		byte[] buffer = new byte[Len.LIO_HALRURQA_LINKAGE];
		return getLioHalrurqaLinkageBytes(buffer, 1);
	}

	public void setLioHalrurqaLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		setConstantsBytes(buffer, position);
		position += Len.CONSTANTS;
		setInputLinkageBytes(buffer, position);
		position += Len.INPUT_LINKAGE;
		setOutputLinkageBytes(buffer, position);
		position += Len.OUTPUT_LINKAGE;
		setInputOutputLinkageBytes(buffer, position);
	}

	public byte[] getLioHalrurqaLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		getConstantsBytes(buffer, position);
		position += Len.CONSTANTS;
		getInputLinkageBytes(buffer, position);
		position += Len.INPUT_LINKAGE;
		getOutputLinkageBytes(buffer, position);
		position += Len.OUTPUT_LINKAGE;
		getInputOutputLinkageBytes(buffer, position);
		return buffer;
	}

	public void setConstantsBytes(byte[] buffer, int offset) {
		int position = offset;
		halrurqaLit = MarshalByte.readString(buffer, position, Len.HALRURQA_LIT);
	}

	public byte[] getConstantsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, halrurqaLit, Len.HALRURQA_LIT);
		return buffer;
	}

	public void setHalrurqaLit(String halrurqaLit) {
		this.halrurqaLit = Functions.subString(halrurqaLit, Len.HALRURQA_LIT);
	}

	public String getHalrurqaLit() {
		return this.halrurqaLit;
	}

	public void setInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		function.setFunction(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
	}

	public byte[] getInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, function.getFunction());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		return buffer;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		recFoundSw.setRecFoundSw(MarshalByte.readChar(buffer, position));
	}

	public byte[] getOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, recFoundSw.getRecFoundSw());
		return buffer;
	}

	public void setInputOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		recSeq = MarshalByte.readFixedString(buffer, position, Len.REC_SEQ);
		position += Len.REC_SEQ;
		actionCode = MarshalByte.readString(buffer, position, Len.ACTION_CODE);
		position += Len.ACTION_CODE;
		setBusObjDataLength(MarshalByte.readShort(buffer, position, Len.BUS_OBJ_DATA_LENGTH, SignType.NO_SIGN));
	}

	public byte[] getInputOutputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, recSeq, Len.REC_SEQ);
		position += Len.REC_SEQ;
		MarshalByte.writeString(buffer, position, actionCode, Len.ACTION_CODE);
		position += Len.ACTION_CODE;
		MarshalByte.writeShort(buffer, position, getBusObjDataLength0(), Len.BUS_OBJ_DATA_LENGTH, SignType.NO_SIGN);
		return buffer;
	}

	public void setRecSeqFormatted(String recSeq) {
		this.recSeq = Trunc.toUnsignedNumeric(recSeq, Len.REC_SEQ);
	}

	public int getRecSeq() {
		return NumericDisplay.asInt(this.recSeq);
	}

	public String getRecSeqFormatted() {
		return this.recSeq;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = Functions.subString(actionCode, Len.ACTION_CODE);
	}

	public String getActionCode() {
		return this.actionCode;
	}

	public void setBusObjDataLength(short busObjDataLength) {
		this.busObjDataLength.setValue(busObjDataLength);
	}

	public short getBusObjDataLength0() {
		return ((short) busObjDataLength.getValue());
	}

	public StringChangeNotifier getBusObjDataLength() {
		return busObjDataLength;
	}

	public HalrurqaFunction getFunction() {
		return function;
	}

	public HalrurqaRecFoundSw getRecFoundSw() {
		return recFoundSw;
	}

	@Override
	public byte[] serialize() {
		return getLioHalrurqaLinkageBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HALRURQA_LIT = 8;
		public static final int CONSTANTS = HALRURQA_LIT;
		public static final int BUS_OBJ_NM = 32;
		public static final int INPUT_LINKAGE = HalrurqaFunction.Len.FUNCTION + BUS_OBJ_NM;
		public static final int OUTPUT_LINKAGE = HalrurqaRecFoundSw.Len.REC_FOUND_SW;
		public static final int REC_SEQ = 5;
		public static final int ACTION_CODE = 20;
		public static final int BUS_OBJ_DATA_LENGTH = 4;
		public static final int INPUT_OUTPUT_LINKAGE = REC_SEQ + ACTION_CODE + BUS_OBJ_DATA_LENGTH;
		public static final int LIO_HALRURQA_LINKAGE = CONSTANTS + INPUT_LINKAGE + OUTPUT_LINKAGE + INPUT_OUTPUT_LINKAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
