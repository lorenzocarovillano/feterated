/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-RUN-DT<br>
 * Variable: WS-RUN-DT from program XZ0P0021<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsRunDt {

	//==== PROPERTIES ====
	//Original name: WS-RD-YYYY
	private String yyyy = DefaultValues.stringVal(Len.YYYY);
	//Original name: WS-RD-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: WS-RD-DD
	private String dd = DefaultValues.stringVal(Len.DD);

	//==== METHODS ====
	public void setRunDtFormatted(String data) {
		byte[] buffer = new byte[Len.RUN_DT];
		MarshalByte.writeString(buffer, 1, data, Len.RUN_DT);
		setRunDtBytes(buffer, 1);
	}

	public void setRunDtBytes(byte[] buffer, int offset) {
		int position = offset;
		yyyy = MarshalByte.readString(buffer, position, Len.YYYY);
		position += Len.YYYY;
		mm = MarshalByte.readString(buffer, position, Len.MM);
		position += Len.MM;
		dd = MarshalByte.readString(buffer, position, Len.DD);
	}

	public void setYyyy(String yyyy) {
		this.yyyy = Functions.subString(yyyy, Len.YYYY);
	}

	public String getYyyy() {
		return this.yyyy;
	}

	public String getYyyyFormatted() {
		return Functions.padBlanks(getYyyy(), Len.YYYY);
	}

	public void setMm(String mm) {
		this.mm = Functions.subString(mm, Len.MM);
	}

	public String getMm() {
		return this.mm;
	}

	public String getMmFormatted() {
		return Functions.padBlanks(getMm(), Len.MM);
	}

	public void setDd(String dd) {
		this.dd = Functions.subString(dd, Len.DD);
	}

	public String getDd() {
		return this.dd;
	}

	public String getDdFormatted() {
		return Functions.padBlanks(getDd(), Len.DD);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YYYY = 4;
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int RUN_DT = YYYY + MM + DD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
