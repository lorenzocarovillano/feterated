/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-NO-RESCINDS<br>
 * Variable: EA-01-NO-RESCINDS from program XZ0P90L0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01NoRescinds {

	//==== PROPERTIES ====
	/**Original name: FILLER-EA-01-NO-RESCINDS<br>
	 * <pre>* THIS ERROR MESSAGE CAN ONLY BE USED WHEN NO POLICIES QUALIFY
	 * * TO BE RESCINDED. DO NOT CHANGE THIS ERROR MESSAGE WITHOUT
	 * * CONTACTING THE BUSINESSWORKS TEAM - THEY ARE KEYING OFF IT
	 * * TO DETERMINE THAT THIS SPECIFIC ERROR OCCURRED.</pre>*/
	private String flr1 = "No prior";
	//Original name: FILLER-EA-01-NO-RESCINDS-1
	private String flr2 = "impending";
	//Original name: FILLER-EA-01-NO-RESCINDS-2
	private String flr3 = "notifications";
	//Original name: FILLER-EA-01-NO-RESCINDS-3
	private String flr4 = "for rescinding";
	//Original name: FILLER-EA-01-NO-RESCINDS-4
	private String flr5 = "policies on";
	//Original name: FILLER-EA-01-NO-RESCINDS-5
	private String flr6 = "account =";
	//Original name: EA-01-CSR-ACT-NBR
	private String ea01CsrActNbr = DefaultValues.stringVal(Len.EA01_CSR_ACT_NBR);
	//Original name: FILLER-EA-01-NO-RESCINDS-6
	private String flr7 = " were found.";

	//==== METHODS ====
	public String getEa01NoRescindsFormatted() {
		return MarshalByteExt.bufferToStr(getEa01NoRescindsBytes());
	}

	public byte[] getEa01NoRescindsBytes() {
		byte[] buffer = new byte[Len.EA01_NO_RESCINDS];
		return getEa01NoRescindsBytes(buffer, 1);
	}

	public byte[] getEa01NoRescindsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, ea01CsrActNbr, Len.EA01_CSR_ACT_NBR);
		position += Len.EA01_CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setEa01CsrActNbr(String ea01CsrActNbr) {
		this.ea01CsrActNbr = Functions.subString(ea01CsrActNbr, Len.EA01_CSR_ACT_NBR);
	}

	public String getEa01CsrActNbr() {
		return this.ea01CsrActNbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA01_CSR_ACT_NBR = 9;
		public static final int FLR1 = 9;
		public static final int FLR2 = 10;
		public static final int FLR3 = 14;
		public static final int FLR4 = 15;
		public static final int FLR5 = 12;
		public static final int EA01_NO_RESCINDS = EA01_CSR_ACT_NBR + FLR1 + 2 * FLR2 + FLR3 + FLR4 + 2 * FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
