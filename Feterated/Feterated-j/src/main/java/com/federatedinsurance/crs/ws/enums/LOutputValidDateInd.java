/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-OUTPUT-VALID-DATE-IND<br>
 * Variable: L-OUTPUT-VALID-DATE-IND from program HALRVDT1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LOutputValidDateInd extends SerializableParameter {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char VALID_DATE = 'Y';
	public static final char NOT_VALID_DATE = 'N';

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_OUTPUT_VALID_DATE_IND;
	}

	@Override
	public void deserialize(byte[] buf) {
		setlOutputValidDateIndFromBuffer(buf);
	}

	public void setlOutputValidDateInd(char lOutputValidDateInd) {
		this.value = lOutputValidDateInd;
	}

	public void setlOutputValidDateIndFromBuffer(byte[] buffer) {
		value = MarshalByte.readChar(buffer, 1);
	}

	public char getlOutputValidDateInd() {
		return this.value;
	}

	public void setValidDate() {
		value = VALID_DATE;
	}

	public boolean isNotValidDate() {
		return value == NOT_VALID_DATE;
	}

	public void setNotValidDate() {
		value = NOT_VALID_DATE;
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.chToBuffer(getlOutputValidDateInd());
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int L_OUTPUT_VALID_DATE_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int L_OUTPUT_VALID_DATE_IND = 1;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int L_OUTPUT_VALID_DATE_IND = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
