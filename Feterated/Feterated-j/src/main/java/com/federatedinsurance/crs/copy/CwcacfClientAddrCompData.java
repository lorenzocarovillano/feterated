/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.CwcacfMoreRowsSw;

/**Original name: CWCACF-CLIENT-ADDR-COMP-DATA<br>
 * Variable: CWCACF-CLIENT-ADDR-COMP-DATA from copybook CAWLFCAC<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class CwcacfClientAddrCompData {

	//==== PROPERTIES ====
	//Original name: CWCACF-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CWCACF-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CWCACF-CICA-ADR-1-CI
	private char cicaAdr1Ci = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-ADR-1
	private String cicaAdr1 = DefaultValues.stringVal(Len.CICA_ADR1);
	//Original name: CWCACF-CICA-ADR-2-CI
	private char cicaAdr2Ci = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-ADR-2-NI
	private char cicaAdr2Ni = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-ADR-2
	private String cicaAdr2 = DefaultValues.stringVal(Len.CICA_ADR2);
	//Original name: CWCACF-CICA-CIT-NM-CI
	private char cicaCitNmCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-CIT-NM
	private String cicaCitNm = DefaultValues.stringVal(Len.CICA_CIT_NM);
	//Original name: CWCACF-CICA-CTY-CI
	private char cicaCtyCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-CTY-NI
	private char cicaCtyNi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-CTY
	private String cicaCty = DefaultValues.stringVal(Len.CICA_CTY);
	//Original name: CWCACF-ST-CD-CI
	private char stCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-ST-CD
	private String stCd = DefaultValues.stringVal(Len.ST_CD);
	//Original name: CWCACF-CICA-PST-CD-CI
	private char cicaPstCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-PST-CD
	private String cicaPstCd = DefaultValues.stringVal(Len.CICA_PST_CD);
	//Original name: CWCACF-CTR-CD-CI
	private char ctrCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CTR-CD
	private String ctrCd = DefaultValues.stringVal(Len.CTR_CD);
	//Original name: CWCACF-CICA-ADD-ADR-IND-CI
	private char cicaAddAdrIndCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-ADD-ADR-IND-NI
	private char cicaAddAdrIndNi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-ADD-ADR-IND
	private char cicaAddAdrInd = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-ADDR-STATUS-CD-CI
	private char addrStatusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-ADDR-STATUS-CD
	private char addrStatusCd = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-ADD-NM-IND-CI
	private char addNmIndCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-ADD-NM-IND
	private char addNmInd = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-ADR-TYP-CD-CI
	private char adrTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-ADR-TYP-CD
	private String adrTypCd = DefaultValues.stringVal(Len.ADR_TYP_CD);
	//Original name: CWCACF-TCH-OBJECT-KEY-CI
	private char tchObjectKeyCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-TCH-OBJECT-KEY
	private String tchObjectKey = DefaultValues.stringVal(Len.TCH_OBJECT_KEY);
	//Original name: CWCACF-CIAR-EXP-DT-CI
	private String ciarExpDtCi = DefaultValues.stringVal(Len.CIAR_EXP_DT_CI);
	//Original name: CWCACF-CIAR-EXP-DT
	private String ciarExpDt = DefaultValues.stringVal(Len.CIAR_EXP_DT);
	//Original name: CWCACF-CIAR-SER-ADR-1-TXT-CI
	private char ciarSerAdr1TxtCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CIAR-SER-ADR-1-TXT
	private String ciarSerAdr1Txt = DefaultValues.stringVal(Len.CIAR_SER_ADR1_TXT);
	//Original name: CWCACF-CIAR-SER-CIT-NM-CI
	private char ciarSerCitNmCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CIAR-SER-CIT-NM
	private String ciarSerCitNm = DefaultValues.stringVal(Len.CIAR_SER_CIT_NM);
	//Original name: CWCACF-CIAR-SER-ST-CD-CI
	private char ciarSerStCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CIAR-SER-ST-CD
	private String ciarSerStCd = DefaultValues.stringVal(Len.CIAR_SER_ST_CD);
	//Original name: CWCACF-CIAR-SER-PST-CD-CI
	private char ciarSerPstCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CIAR-SER-PST-CD
	private String ciarSerPstCd = DefaultValues.stringVal(Len.CIAR_SER_PST_CD);
	//Original name: CWCACF-REL-STATUS-CD-CI
	private char relStatusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-REL-STATUS-CD
	private char relStatusCd = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-EFF-ACY-TS-CI
	private char effAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-EFF-ACY-TS
	private String effAcyTs = DefaultValues.stringVal(Len.EFF_ACY_TS);
	//Original name: CWCACF-CIAR-EXP-ACY-TS-CI
	private char ciarExpAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CIAR-EXP-ACY-TS
	private String ciarExpAcyTs = DefaultValues.stringVal(Len.CIAR_EXP_ACY_TS);
	/**Original name: CWCACF-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	private CwcacfMoreRowsSw moreRowsSw = new CwcacfMoreRowsSw();
	/**Original name: CWCACF-CICA-SHW-OBJ-KEY-CI<br>
	 * <pre>* VARIABLE FOR SHW-OBJ-KEY(SHOWABLE OBJECT KEY)</pre>*/
	private char cicaShwObjKeyCi = DefaultValues.CHAR_VAL;
	//Original name: CWCACF-CICA-SHW-OBJ-KEY
	private String cicaShwObjKey = DefaultValues.stringVal(Len.CICA_SHW_OBJ_KEY);
	//Original name: CWCACF-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: CWCACF-ST-DESC
	private String stDesc = DefaultValues.stringVal(Len.ST_DESC);
	//Original name: CWCACF-ADR-TYP-DESC
	private String adrTypDesc = DefaultValues.stringVal(Len.ADR_TYP_DESC);

	//==== METHODS ====
	public void setClientAddrCompDataBytes(byte[] buffer, int offset) {
		int position = offset;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		cicaAdr1Ci = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaAdr1 = MarshalByte.readString(buffer, position, Len.CICA_ADR1);
		position += Len.CICA_ADR1;
		cicaAdr2Ci = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaAdr2Ni = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaAdr2 = MarshalByte.readString(buffer, position, Len.CICA_ADR2);
		position += Len.CICA_ADR2;
		cicaCitNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaCitNm = MarshalByte.readString(buffer, position, Len.CICA_CIT_NM);
		position += Len.CICA_CIT_NM;
		cicaCtyCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaCtyNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaCty = MarshalByte.readString(buffer, position, Len.CICA_CTY);
		position += Len.CICA_CTY;
		stCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		stCd = MarshalByte.readString(buffer, position, Len.ST_CD);
		position += Len.ST_CD;
		cicaPstCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaPstCd = MarshalByte.readString(buffer, position, Len.CICA_PST_CD);
		position += Len.CICA_PST_CD;
		ctrCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ctrCd = MarshalByte.readString(buffer, position, Len.CTR_CD);
		position += Len.CTR_CD;
		cicaAddAdrIndCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaAddAdrIndNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaAddAdrInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addrStatusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addrStatusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addNmIndCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addNmInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		adrTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		adrTypCd = MarshalByte.readString(buffer, position, Len.ADR_TYP_CD);
		position += Len.ADR_TYP_CD;
		tchObjectKeyCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tchObjectKey = MarshalByte.readString(buffer, position, Len.TCH_OBJECT_KEY);
		position += Len.TCH_OBJECT_KEY;
		ciarExpDtCi = MarshalByte.readString(buffer, position, Len.CIAR_EXP_DT_CI);
		position += Len.CIAR_EXP_DT_CI;
		ciarExpDt = MarshalByte.readString(buffer, position, Len.CIAR_EXP_DT);
		position += Len.CIAR_EXP_DT;
		ciarSerAdr1TxtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciarSerAdr1Txt = MarshalByte.readString(buffer, position, Len.CIAR_SER_ADR1_TXT);
		position += Len.CIAR_SER_ADR1_TXT;
		ciarSerCitNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciarSerCitNm = MarshalByte.readString(buffer, position, Len.CIAR_SER_CIT_NM);
		position += Len.CIAR_SER_CIT_NM;
		ciarSerStCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciarSerStCd = MarshalByte.readString(buffer, position, Len.CIAR_SER_ST_CD);
		position += Len.CIAR_SER_ST_CD;
		ciarSerPstCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciarSerPstCd = MarshalByte.readString(buffer, position, Len.CIAR_SER_PST_CD);
		position += Len.CIAR_SER_PST_CD;
		relStatusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		relStatusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		effAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		effAcyTs = MarshalByte.readString(buffer, position, Len.EFF_ACY_TS);
		position += Len.EFF_ACY_TS;
		ciarExpAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciarExpAcyTs = MarshalByte.readString(buffer, position, Len.CIAR_EXP_ACY_TS);
		position += Len.CIAR_EXP_ACY_TS;
		moreRowsSw.setMoreRowsSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		cicaShwObjKeyCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cicaShwObjKey = MarshalByte.readString(buffer, position, Len.CICA_SHW_OBJ_KEY);
		position += Len.CICA_SHW_OBJ_KEY;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		stDesc = MarshalByte.readString(buffer, position, Len.ST_DESC);
		position += Len.ST_DESC;
		adrTypDesc = MarshalByte.readString(buffer, position, Len.ADR_TYP_DESC);
	}

	public byte[] getClientAddrCompDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, cicaAdr1Ci);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicaAdr1, Len.CICA_ADR1);
		position += Len.CICA_ADR1;
		MarshalByte.writeChar(buffer, position, cicaAdr2Ci);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cicaAdr2Ni);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicaAdr2, Len.CICA_ADR2);
		position += Len.CICA_ADR2;
		MarshalByte.writeChar(buffer, position, cicaCitNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicaCitNm, Len.CICA_CIT_NM);
		position += Len.CICA_CIT_NM;
		MarshalByte.writeChar(buffer, position, cicaCtyCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cicaCtyNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicaCty, Len.CICA_CTY);
		position += Len.CICA_CTY;
		MarshalByte.writeChar(buffer, position, stCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, stCd, Len.ST_CD);
		position += Len.ST_CD;
		MarshalByte.writeChar(buffer, position, cicaPstCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicaPstCd, Len.CICA_PST_CD);
		position += Len.CICA_PST_CD;
		MarshalByte.writeChar(buffer, position, ctrCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ctrCd, Len.CTR_CD);
		position += Len.CTR_CD;
		MarshalByte.writeChar(buffer, position, cicaAddAdrIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cicaAddAdrIndNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cicaAddAdrInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, addrStatusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, addrStatusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, addNmIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, addNmInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, adrTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, adrTypCd, Len.ADR_TYP_CD);
		position += Len.ADR_TYP_CD;
		MarshalByte.writeChar(buffer, position, tchObjectKeyCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tchObjectKey, Len.TCH_OBJECT_KEY);
		position += Len.TCH_OBJECT_KEY;
		MarshalByte.writeString(buffer, position, ciarExpDtCi, Len.CIAR_EXP_DT_CI);
		position += Len.CIAR_EXP_DT_CI;
		MarshalByte.writeString(buffer, position, ciarExpDt, Len.CIAR_EXP_DT);
		position += Len.CIAR_EXP_DT;
		MarshalByte.writeChar(buffer, position, ciarSerAdr1TxtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciarSerAdr1Txt, Len.CIAR_SER_ADR1_TXT);
		position += Len.CIAR_SER_ADR1_TXT;
		MarshalByte.writeChar(buffer, position, ciarSerCitNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciarSerCitNm, Len.CIAR_SER_CIT_NM);
		position += Len.CIAR_SER_CIT_NM;
		MarshalByte.writeChar(buffer, position, ciarSerStCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciarSerStCd, Len.CIAR_SER_ST_CD);
		position += Len.CIAR_SER_ST_CD;
		MarshalByte.writeChar(buffer, position, ciarSerPstCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciarSerPstCd, Len.CIAR_SER_PST_CD);
		position += Len.CIAR_SER_PST_CD;
		MarshalByte.writeChar(buffer, position, relStatusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, relStatusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, effAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, effAcyTs, Len.EFF_ACY_TS);
		position += Len.EFF_ACY_TS;
		MarshalByte.writeChar(buffer, position, ciarExpAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciarExpAcyTs, Len.CIAR_EXP_ACY_TS);
		position += Len.CIAR_EXP_ACY_TS;
		MarshalByte.writeChar(buffer, position, moreRowsSw.getMoreRowsSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cicaShwObjKeyCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cicaShwObjKey, Len.CICA_SHW_OBJ_KEY);
		position += Len.CICA_SHW_OBJ_KEY;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, stDesc, Len.ST_DESC);
		position += Len.ST_DESC;
		MarshalByte.writeString(buffer, position, adrTypDesc, Len.ADR_TYP_DESC);
		return buffer;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setCicaAdr1Ci(char cicaAdr1Ci) {
		this.cicaAdr1Ci = cicaAdr1Ci;
	}

	public char getCicaAdr1Ci() {
		return this.cicaAdr1Ci;
	}

	public void setCicaAdr1(String cicaAdr1) {
		this.cicaAdr1 = Functions.subString(cicaAdr1, Len.CICA_ADR1);
	}

	public String getCicaAdr1() {
		return this.cicaAdr1;
	}

	public void setCicaAdr2Ci(char cicaAdr2Ci) {
		this.cicaAdr2Ci = cicaAdr2Ci;
	}

	public char getCicaAdr2Ci() {
		return this.cicaAdr2Ci;
	}

	public void setCicaAdr2Ni(char cicaAdr2Ni) {
		this.cicaAdr2Ni = cicaAdr2Ni;
	}

	public char getCicaAdr2Ni() {
		return this.cicaAdr2Ni;
	}

	public void setCicaAdr2(String cicaAdr2) {
		this.cicaAdr2 = Functions.subString(cicaAdr2, Len.CICA_ADR2);
	}

	public String getCicaAdr2() {
		return this.cicaAdr2;
	}

	public void setCicaCitNmCi(char cicaCitNmCi) {
		this.cicaCitNmCi = cicaCitNmCi;
	}

	public char getCicaCitNmCi() {
		return this.cicaCitNmCi;
	}

	public void setCicaCitNm(String cicaCitNm) {
		this.cicaCitNm = Functions.subString(cicaCitNm, Len.CICA_CIT_NM);
	}

	public String getCicaCitNm() {
		return this.cicaCitNm;
	}

	public void setCicaCtyCi(char cicaCtyCi) {
		this.cicaCtyCi = cicaCtyCi;
	}

	public char getCicaCtyCi() {
		return this.cicaCtyCi;
	}

	public void setCicaCtyNi(char cicaCtyNi) {
		this.cicaCtyNi = cicaCtyNi;
	}

	public char getCicaCtyNi() {
		return this.cicaCtyNi;
	}

	public void setCicaCty(String cicaCty) {
		this.cicaCty = Functions.subString(cicaCty, Len.CICA_CTY);
	}

	public String getCicaCty() {
		return this.cicaCty;
	}

	public void setStCdCi(char stCdCi) {
		this.stCdCi = stCdCi;
	}

	public char getStCdCi() {
		return this.stCdCi;
	}

	public void setStCd(String stCd) {
		this.stCd = Functions.subString(stCd, Len.ST_CD);
	}

	public String getStCd() {
		return this.stCd;
	}

	public void setCicaPstCdCi(char cicaPstCdCi) {
		this.cicaPstCdCi = cicaPstCdCi;
	}

	public char getCicaPstCdCi() {
		return this.cicaPstCdCi;
	}

	public void setCicaPstCd(String cicaPstCd) {
		this.cicaPstCd = Functions.subString(cicaPstCd, Len.CICA_PST_CD);
	}

	public String getCicaPstCd() {
		return this.cicaPstCd;
	}

	public void setCtrCdCi(char ctrCdCi) {
		this.ctrCdCi = ctrCdCi;
	}

	public char getCtrCdCi() {
		return this.ctrCdCi;
	}

	public void setCtrCd(String ctrCd) {
		this.ctrCd = Functions.subString(ctrCd, Len.CTR_CD);
	}

	public String getCtrCd() {
		return this.ctrCd;
	}

	public void setCicaAddAdrIndCi(char cicaAddAdrIndCi) {
		this.cicaAddAdrIndCi = cicaAddAdrIndCi;
	}

	public char getCicaAddAdrIndCi() {
		return this.cicaAddAdrIndCi;
	}

	public void setCicaAddAdrIndNi(char cicaAddAdrIndNi) {
		this.cicaAddAdrIndNi = cicaAddAdrIndNi;
	}

	public char getCicaAddAdrIndNi() {
		return this.cicaAddAdrIndNi;
	}

	public void setCicaAddAdrInd(char cicaAddAdrInd) {
		this.cicaAddAdrInd = cicaAddAdrInd;
	}

	public char getCicaAddAdrInd() {
		return this.cicaAddAdrInd;
	}

	public void setAddrStatusCdCi(char addrStatusCdCi) {
		this.addrStatusCdCi = addrStatusCdCi;
	}

	public char getAddrStatusCdCi() {
		return this.addrStatusCdCi;
	}

	public void setAddrStatusCd(char addrStatusCd) {
		this.addrStatusCd = addrStatusCd;
	}

	public char getAddrStatusCd() {
		return this.addrStatusCd;
	}

	public void setAddNmIndCi(char addNmIndCi) {
		this.addNmIndCi = addNmIndCi;
	}

	public char getAddNmIndCi() {
		return this.addNmIndCi;
	}

	public void setAddNmInd(char addNmInd) {
		this.addNmInd = addNmInd;
	}

	public char getAddNmInd() {
		return this.addNmInd;
	}

	public void setAdrTypCdCi(char adrTypCdCi) {
		this.adrTypCdCi = adrTypCdCi;
	}

	public char getAdrTypCdCi() {
		return this.adrTypCdCi;
	}

	public void setAdrTypCd(String adrTypCd) {
		this.adrTypCd = Functions.subString(adrTypCd, Len.ADR_TYP_CD);
	}

	public String getAdrTypCd() {
		return this.adrTypCd;
	}

	public void setTchObjectKeyCi(char tchObjectKeyCi) {
		this.tchObjectKeyCi = tchObjectKeyCi;
	}

	public char getTchObjectKeyCi() {
		return this.tchObjectKeyCi;
	}

	public void setTchObjectKey(String tchObjectKey) {
		this.tchObjectKey = Functions.subString(tchObjectKey, Len.TCH_OBJECT_KEY);
	}

	public String getTchObjectKey() {
		return this.tchObjectKey;
	}

	public void setCiarExpDtCi(String ciarExpDtCi) {
		this.ciarExpDtCi = Functions.subString(ciarExpDtCi, Len.CIAR_EXP_DT_CI);
	}

	public String getCiarExpDtCi() {
		return this.ciarExpDtCi;
	}

	public void setCiarExpDt(String ciarExpDt) {
		this.ciarExpDt = Functions.subString(ciarExpDt, Len.CIAR_EXP_DT);
	}

	public String getCiarExpDt() {
		return this.ciarExpDt;
	}

	public void setCiarSerAdr1TxtCi(char ciarSerAdr1TxtCi) {
		this.ciarSerAdr1TxtCi = ciarSerAdr1TxtCi;
	}

	public char getCiarSerAdr1TxtCi() {
		return this.ciarSerAdr1TxtCi;
	}

	public void setCiarSerAdr1Txt(String ciarSerAdr1Txt) {
		this.ciarSerAdr1Txt = Functions.subString(ciarSerAdr1Txt, Len.CIAR_SER_ADR1_TXT);
	}

	public String getCiarSerAdr1Txt() {
		return this.ciarSerAdr1Txt;
	}

	public void setCiarSerCitNmCi(char ciarSerCitNmCi) {
		this.ciarSerCitNmCi = ciarSerCitNmCi;
	}

	public char getCiarSerCitNmCi() {
		return this.ciarSerCitNmCi;
	}

	public void setCiarSerCitNm(String ciarSerCitNm) {
		this.ciarSerCitNm = Functions.subString(ciarSerCitNm, Len.CIAR_SER_CIT_NM);
	}

	public String getCiarSerCitNm() {
		return this.ciarSerCitNm;
	}

	public void setCiarSerStCdCi(char ciarSerStCdCi) {
		this.ciarSerStCdCi = ciarSerStCdCi;
	}

	public char getCiarSerStCdCi() {
		return this.ciarSerStCdCi;
	}

	public void setCiarSerStCd(String ciarSerStCd) {
		this.ciarSerStCd = Functions.subString(ciarSerStCd, Len.CIAR_SER_ST_CD);
	}

	public String getCiarSerStCd() {
		return this.ciarSerStCd;
	}

	public void setCiarSerPstCdCi(char ciarSerPstCdCi) {
		this.ciarSerPstCdCi = ciarSerPstCdCi;
	}

	public char getCiarSerPstCdCi() {
		return this.ciarSerPstCdCi;
	}

	public void setCiarSerPstCd(String ciarSerPstCd) {
		this.ciarSerPstCd = Functions.subString(ciarSerPstCd, Len.CIAR_SER_PST_CD);
	}

	public String getCiarSerPstCd() {
		return this.ciarSerPstCd;
	}

	public void setRelStatusCdCi(char relStatusCdCi) {
		this.relStatusCdCi = relStatusCdCi;
	}

	public char getRelStatusCdCi() {
		return this.relStatusCdCi;
	}

	public void setRelStatusCd(char relStatusCd) {
		this.relStatusCd = relStatusCd;
	}

	public char getRelStatusCd() {
		return this.relStatusCd;
	}

	public void setEffAcyTsCi(char effAcyTsCi) {
		this.effAcyTsCi = effAcyTsCi;
	}

	public char getEffAcyTsCi() {
		return this.effAcyTsCi;
	}

	public void setEffAcyTs(String effAcyTs) {
		this.effAcyTs = Functions.subString(effAcyTs, Len.EFF_ACY_TS);
	}

	public String getEffAcyTs() {
		return this.effAcyTs;
	}

	public void setCiarExpAcyTsCi(char ciarExpAcyTsCi) {
		this.ciarExpAcyTsCi = ciarExpAcyTsCi;
	}

	public char getCiarExpAcyTsCi() {
		return this.ciarExpAcyTsCi;
	}

	public void setCiarExpAcyTs(String ciarExpAcyTs) {
		this.ciarExpAcyTs = Functions.subString(ciarExpAcyTs, Len.CIAR_EXP_ACY_TS);
	}

	public String getCiarExpAcyTs() {
		return this.ciarExpAcyTs;
	}

	public void setCicaShwObjKeyCi(char cicaShwObjKeyCi) {
		this.cicaShwObjKeyCi = cicaShwObjKeyCi;
	}

	public char getCicaShwObjKeyCi() {
		return this.cicaShwObjKeyCi;
	}

	public void setCicaShwObjKey(String cicaShwObjKey) {
		this.cicaShwObjKey = Functions.subString(cicaShwObjKey, Len.CICA_SHW_OBJ_KEY);
	}

	public String getCicaShwObjKey() {
		return this.cicaShwObjKey;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setStDesc(String stDesc) {
		this.stDesc = Functions.subString(stDesc, Len.ST_DESC);
	}

	public String getStDesc() {
		return this.stDesc;
	}

	public void setAdrTypDesc(String adrTypDesc) {
		this.adrTypDesc = Functions.subString(adrTypDesc, Len.ADR_TYP_DESC);
	}

	public String getAdrTypDesc() {
		return this.adrTypDesc;
	}

	public CwcacfMoreRowsSw getMoreRowsSw() {
		return moreRowsSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CICA_ADR1 = 45;
		public static final int CICA_ADR2 = 45;
		public static final int CICA_CIT_NM = 30;
		public static final int CICA_CTY = 30;
		public static final int ST_CD = 3;
		public static final int CICA_PST_CD = 13;
		public static final int CTR_CD = 4;
		public static final int ADR_TYP_CD = 4;
		public static final int TCH_OBJECT_KEY = 20;
		public static final int CIAR_EXP_DT_CI = 10;
		public static final int CIAR_EXP_DT = 10;
		public static final int CIAR_SER_ADR1_TXT = 8;
		public static final int CIAR_SER_CIT_NM = 5;
		public static final int CIAR_SER_ST_CD = 3;
		public static final int CIAR_SER_PST_CD = 6;
		public static final int EFF_ACY_TS = 26;
		public static final int CIAR_EXP_ACY_TS = 26;
		public static final int CICA_SHW_OBJ_KEY = 30;
		public static final int BUS_OBJ_NM = 32;
		public static final int ST_DESC = 40;
		public static final int ADR_TYP_DESC = 40;
		public static final int USER_ID_CI = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int CICA_ADR1_CI = 1;
		public static final int CICA_ADR2_CI = 1;
		public static final int CICA_ADR2_NI = 1;
		public static final int CICA_CIT_NM_CI = 1;
		public static final int CICA_CTY_CI = 1;
		public static final int CICA_CTY_NI = 1;
		public static final int ST_CD_CI = 1;
		public static final int CICA_PST_CD_CI = 1;
		public static final int CTR_CD_CI = 1;
		public static final int CICA_ADD_ADR_IND_CI = 1;
		public static final int CICA_ADD_ADR_IND_NI = 1;
		public static final int CICA_ADD_ADR_IND = 1;
		public static final int ADDR_STATUS_CD_CI = 1;
		public static final int ADDR_STATUS_CD = 1;
		public static final int ADD_NM_IND_CI = 1;
		public static final int ADD_NM_IND = 1;
		public static final int ADR_TYP_CD_CI = 1;
		public static final int TCH_OBJECT_KEY_CI = 1;
		public static final int CIAR_SER_ADR1_TXT_CI = 1;
		public static final int CIAR_SER_CIT_NM_CI = 1;
		public static final int CIAR_SER_ST_CD_CI = 1;
		public static final int CIAR_SER_PST_CD_CI = 1;
		public static final int REL_STATUS_CD_CI = 1;
		public static final int REL_STATUS_CD = 1;
		public static final int EFF_ACY_TS_CI = 1;
		public static final int CIAR_EXP_ACY_TS_CI = 1;
		public static final int CICA_SHW_OBJ_KEY_CI = 1;
		public static final int CLIENT_ADDR_COMP_DATA = USER_ID_CI + USER_ID + TERMINAL_ID_CI + TERMINAL_ID + CICA_ADR1_CI + CICA_ADR1 + CICA_ADR2_CI
				+ CICA_ADR2_NI + CICA_ADR2 + CICA_CIT_NM_CI + CICA_CIT_NM + CICA_CTY_CI + CICA_CTY_NI + CICA_CTY + ST_CD_CI + ST_CD + CICA_PST_CD_CI
				+ CICA_PST_CD + CTR_CD_CI + CTR_CD + CICA_ADD_ADR_IND_CI + CICA_ADD_ADR_IND_NI + CICA_ADD_ADR_IND + ADDR_STATUS_CD_CI + ADDR_STATUS_CD
				+ ADD_NM_IND_CI + ADD_NM_IND + ADR_TYP_CD_CI + ADR_TYP_CD + TCH_OBJECT_KEY_CI + TCH_OBJECT_KEY + CIAR_EXP_DT_CI + CIAR_EXP_DT
				+ CIAR_SER_ADR1_TXT_CI + CIAR_SER_ADR1_TXT + CIAR_SER_CIT_NM_CI + CIAR_SER_CIT_NM + CIAR_SER_ST_CD_CI + CIAR_SER_ST_CD
				+ CIAR_SER_PST_CD_CI + CIAR_SER_PST_CD + REL_STATUS_CD_CI + REL_STATUS_CD + EFF_ACY_TS_CI + EFF_ACY_TS + CIAR_EXP_ACY_TS_CI
				+ CIAR_EXP_ACY_TS + CwcacfMoreRowsSw.Len.MORE_ROWS_SW + CICA_SHW_OBJ_KEY_CI + CICA_SHW_OBJ_KEY + BUS_OBJ_NM + ST_DESC + ADR_TYP_DESC;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
