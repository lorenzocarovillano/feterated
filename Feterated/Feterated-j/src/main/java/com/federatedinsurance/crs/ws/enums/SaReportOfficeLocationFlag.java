/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SA-REPORT-OFFICE-LOCATION-FLAG<br>
 * Variable: SA-REPORT-OFFICE-LOCATION-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaReportOfficeLocationFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	private static final char[] IN_HOUSE_ONLY = new char[] { '1', '2' };
	public static final char NO_OUTPUT_AT_ALL = '3';
	private static final char[] COM_ONLY = new char[] { '4', '6' };
	private static final char[] COM_AND_IN_HOUSE = new char[] { '5', '7' };
	public static final char VPS_ONLY = '8';
	public static final char VPS_AND_IN_HOUSE = '9';
	public static final char COM_AND_VPS = 'A';
	public static final char COM_VPS_AND_IN_HOUSE = 'B';
	private static final char[] IS_INVLD_OFFICE_DESTINATION = new char[] { '0', '2', '6', '7' };
	private static final char[] VPS_REPORTS = new char[] { '8', '9', 'A', 'B' };
	private static final char[] COM_REPORTS = new char[] { '4', '6', '5', '7', 'A', 'B' };

	//==== METHODS ====
	public void setReportOfficeLocationFlag(char reportOfficeLocationFlag) {
		this.value = reportOfficeLocationFlag;
	}

	public char getReportOfficeLocationFlag() {
		return this.value;
	}

	public boolean isInHouseOnly() {
		return ArrayUtils.contains(IN_HOUSE_ONLY, value);
	}

	public boolean isNoOutputAtAll() {
		return value == NO_OUTPUT_AT_ALL;
	}

	public boolean isComOnly() {
		return ArrayUtils.contains(COM_ONLY, value);
	}

	public boolean isComAndInHouse() {
		return ArrayUtils.contains(COM_AND_IN_HOUSE, value);
	}

	public boolean isVpsOnly() {
		return value == VPS_ONLY;
	}

	public boolean isVpsAndInHouse() {
		return value == VPS_AND_IN_HOUSE;
	}

	public boolean isComAndVps() {
		return value == COM_AND_VPS;
	}

	public boolean isComVpsAndInHouse() {
		return value == COM_VPS_AND_IN_HOUSE;
	}

	public boolean isIsInvldOfficeDestination() {
		return ArrayUtils.contains(IS_INVLD_OFFICE_DESTINATION, value);
	}

	public boolean isComReports() {
		return ArrayUtils.contains(COM_REPORTS, value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REPORT_OFFICE_LOCATION_FLAG = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
