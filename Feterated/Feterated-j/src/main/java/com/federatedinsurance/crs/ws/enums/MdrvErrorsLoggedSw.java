/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: MDRV-ERRORS-LOGGED-SW<br>
 * Variable: MDRV-ERRORS-LOGGED-SW from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvErrorsLoggedSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char OK = 'Y';
	public static final char FAILED = 'N';

	//==== METHODS ====
	public void setErrorsLoggedSw(char errorsLoggedSw) {
		this.value = errorsLoggedSw;
	}

	public char getErrorsLoggedSw() {
		return this.value;
	}

	public void setFailed() {
		value = FAILED;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERRORS_LOGGED_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
