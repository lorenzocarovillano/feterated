/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalErrTranSupVDao;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.ws.Dfhcommarea;
import com.federatedinsurance.crs.ws.DfhcommareaHaloetra;
import com.federatedinsurance.crs.ws.HaloetraData;
import com.federatedinsurance.crs.ws.enums.UidgErridErrorCode;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: HALOETRA<br>
 * <pre>--------------------------------------------------------------*
 *   PROGRAM TITLE - SERIES III INTERFACE - ERROR TRANSLATION    *
 *                                          MODULE               *
 *   WRITTEN BY:  PMSC                                           *
 *                                                               *
 *   DATE WRITTEN: APR 2000                                      *
 *                                                               *
 *   PLATFORM - HOST                                             *
 *                                                               *
 *   OPERATING SYSTEM - MVS                                      *
 *                                                               *
 *   LANGUAGE - MVS COBOL                                        *
 *                                                               *
 *   PURPOSE - THIS MODULE IS THE MAIN AQ2 ERROR TRANSLATION     *
 *             MODULE                                            *
 *                                                               *
 *   PROGRAM INITIATION - CICS LINK FROM HALOESTO WHICH IS THE   *
 *                        MAIN ERROR HANDLING MODULE             *
 *                                                               *
 *   DATA ACCESS METHODS - PASSED INFORMATION                    *
 *                                                               *
 * --------------------------------------------------------------*
 * --------------------------------------------------------------*
 *                 M A I N T E N A N C E    L O G
 *  SI#      DATE      PRGMR     DESCRIPTION
 *  -------- --------- --------- ---------------------------------
 *  SAVANNAH 12APR00   LCP       CREATED.
 *  17238    01NOV01   AICI448   ACCOUNT FOR LOCKING ERRORS.
 *  17241    13NOV01   03539     REPLACE REFERENCES TO IAP WITH
 *                               COMPARABLE INFRASTRUCTURE CODE.
 *  17241    20DEC01   18448     REMOVE IAP DATE ROUTINE COPYBOOK.
 *  17330    15JAN02   18448     NEW ID GENERATION ALGORITHM
 *   (GA)                        REQUIRES UBOC-MSG-ID. ALSO,
 *                               UBOC-UOW-NAME NEEDED FOR PROPER
 *                               FUNCTIONING OF HALCWAER IN CALL
 *                               TO ID GENERATOR.
 *                               (RETRO'D FROM BF 2.2 GA)
 *  18687    16JAN02   18448     NOW CALLING BF ID BUILDER TO
 *                               CREATE ERROR REF NUMBER.
 *  20070B   29MAR02   18448     CONVERT ABENDS TO LOGGABLE ERRORS
 *                               BEFORE RETURNING TO MIDDLEWARE.
 *  28729    07JAN03   18448     ADD PROCESSING FOR NEW 88S ADDED
 *                               FOR MIP USAGE IMPROVEMENTS.
 * --------------------------------------------------------------*</pre>*/
public class Haloetra extends BatchProgram {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>---*
	 *  SET UP SQL
	 * ---*</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private HalErrTranSupVDao halErrTranSupVDao = new HalErrTranSupVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private HaloetraData ws = new HaloetraData();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaHaloetra dfhcommarea = new DfhcommareaHaloetra();

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaHaloetra dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		registerArgListeners();
		mainline();
		return1();
		deleteArgListeners();
		return 0;
	}

	public static Haloetra getInstance() {
		return (Programs.getInstance(Haloetra.class));
	}

	/**Original name: 0000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A) INITIALISE ARRAYS                                         *
	 *  B) VALIDATE ESTO-ACTION-BUFFER                               *
	 *  C) RUN MAIN PROCESS TO MATCH ACTION BUFFER ENTRIES TO ERRORS *
	 *  D) RETURN.                                                   *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: MOVE DFHCOMMAREA(1:LENGTH OF WS-HALOETRA-LINKAGE)
		//                TO WS-HALOETRA-LINKAGE.
		ws.setWsHaloetraLinkageFormatted(dfhcommarea.getDfhcommareaFormatted().substring((1) - 1, HaloetraData.Len.WS_HALOETRA_LINKAGE));
		// COB_CODE: PERFORM 0100-INITIALIZE.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=HALOETRA.CBL:line=175, because the code is unreachable.
		// COB_CODE: PERFORM 0120-VALIDATE-INPUT.
		validateInput();
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK
		//               GO TO 0000-RETURN
		//           END-IF.
		if (!ws.getHallesto().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: GO TO 0000-RETURN
			return1();
		}
		//---*
		//   PERFORM MATCHING
		//---*
		// COB_CODE: PERFORM 0500-PERFORM-MATCHING.
		matching();
	}

	/**Original name: 0000-RETURN<br>*/
	private void return1() {
		// COB_CODE: MOVE WS-HALOETRA-LINKAGE TO DFHCOMMAREA.
		dfhcommarea.setDfhcommareaBytes(ws.getWsHaloetraLinkageBytes());
		// COB_CODE: EXEC CICS RETURN END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 0120-VALIDATE-INPUT_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  THIS SECTION VALIDATES ESTO-ACTION-BUFFER AGAINST            *
	 *  EFAL-FAILED-ACTION-TYPE PRIOR TO MATCHING.                   *
	 *                                                               *
	 *  A>  VALIDATE ESTO LINKAGE                                    *
	 *  B>  VALIDATE DB2 FAILS                                       *
	 *  C>  VALIDATE CICS FAILS                                      *
	 *  D>  VALIDATE IAP FAILS                --                     *
	 *  E>  VALIDATE COMMAREA FAILS           --                     *
	 *  F>  VALIDATE SECURITY FAILS           --  NEED TO BE DONE    *
	 *  G>  VALIDATE DATA PRIVACY FAILS       --                     *
	 *  H>  VALIDATE BUSINESS PROCESS FAILS   --                     *
	 *  H>  VALIDATE ABEND FAILS                                     *
	 *  H>  VALIDATE TRANSFER FAILS                                  *
	 * ***************************************************************</pre>*/
	private void validateInput() {
		// COB_CODE: IF ETRA-ERR-ACTION = (SPACES OR LOW-VALUES)
		//               GO TO 0120-VALIDATE-INPUT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallesto().getEstoDetailBuffer().getEtraErrAction())
				|| Characters.EQ_LOW.test(ws.getHallesto().getEstoDetailBuffer().getEtraErrAction(), EstoDetailBuffer.Len.ETRA_ERR_ACTION)) {
			// COB_CODE: SET ESTO-ERR-TRANS-FAILED         TO TRUE
			ws.getHallesto().getEstoOutput().getStoreReturnCd().setEstoErrTransFailed();
			// COB_CODE: SET ESTO-ACTION-NOT-FILLED        TO TRUE
			ws.getHallesto().getEstoOutput().getStoreDetailCd().setEstoActionNotFilled();
			// COB_CODE: GO TO 0120-VALIDATE-INPUT-X
			return;
		}
		// COB_CODE: IF EFAL-FAILED-ACTION-TYPE = (SPACES OR LOW-VALUES)
		//               GO TO 0120-VALIDATE-INPUT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallesto().getEstoDetailBuffer().getEfalFailedActionType())
				|| Characters.EQ_LOW.test(ws.getHallesto().getEstoDetailBuffer().getEfalFailedActionTypeFormatted())) {
			// COB_CODE: SET ESTO-ERR-TRANS-FAILED         TO TRUE
			ws.getHallesto().getEstoOutput().getStoreReturnCd().setEstoErrTransFailed();
			// COB_CODE: SET ESTO-FAILURE-ACTION-NOT-FILLED
			//                                             TO TRUE
			ws.getHallesto().getEstoOutput().getStoreDetailCd().setEstoFailureActionNotFilled();
			// COB_CODE: GO TO 0120-VALIDATE-INPUT-X
			return;
		}
		// COB_CODE: EVALUATE EFAL-FAILED-ACTION-TYPE
		//               WHEN 'DB2'
		//                   END-IF
		//               WHEN 'CICS'
		//                   END-IF
		//               WHEN 'IAP'
		//               WHEN 'COMMAREA'
		//               WHEN 'SECURITY'
		//               WHEN 'DATAPRIV'
		//               WHEN 'BUSPROC'
		//               WHEN 'AUDIT'
		//               WHEN 'LOCKING'
		//               WHEN 'ABEND'
		//               WHEN 'TRANSFER'
		//                   CONTINUE
		//               WHEN OTHER
		//                   SET ESTO-FAILURE-ACTION-INVALID   TO TRUE
		//           END-EVALUATE.
		switch (ws.getHallesto().getEstoDetailBuffer().getEfalFailedActionType()) {

		case "DB2":// COB_CODE: IF EFAL-DB2-ERR-SQLCODE = ZEROS
			//               SET ESTO-DB2-SQLCODE-NOT-FILLED   TO TRUE
			//           END-IF
			if (Characters.EQ_ZERO.test(ws.getHallesto().getEstoDetailBuffer().getEfalDb2ErrSqlcodeFormatted())) {
				// COB_CODE: SET ESTO-ERR-TRANS-FAILED         TO TRUE
				ws.getHallesto().getEstoOutput().getStoreReturnCd().setEstoErrTransFailed();
				// COB_CODE: SET ESTO-DB2-SQLCODE-NOT-FILLED   TO TRUE
				ws.getHallesto().getEstoOutput().getStoreDetailCd().setEstoDb2SqlcodeNotFilled();
			}
			break;

		case "CICS":// COB_CODE: IF EFAL-CICS-ERR-RESP = ZEROS
			//               SET ESTO-CICS-ERR-RESP-NOT-FILLED TO TRUE
			//           END-IF
			if (Characters.EQ_ZERO.test(ws.getHallesto().getEstoDetailBuffer().getEfalCicsErrRespFormatted())) {
				// COB_CODE: SET ESTO-ERR-TRANS-FAILED         TO TRUE
				ws.getHallesto().getEstoOutput().getStoreReturnCd().setEstoErrTransFailed();
				// COB_CODE: SET ESTO-CICS-ERR-RESP-NOT-FILLED TO TRUE
				ws.getHallesto().getEstoOutput().getStoreDetailCd().setEstoCicsErrRespNotFilled();
			}
			break;

		case "IAP":
		case "COMMAREA":
		case "SECURITY":
		case "DATAPRIV":
		case "BUSPROC":
		case "AUDIT":
		case "LOCKING":
		case "ABEND":
		case "TRANSFER":// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET ESTO-ERR-TRANS-FAILED         TO TRUE
			ws.getHallesto().getEstoOutput().getStoreReturnCd().setEstoErrTransFailed();
			// COB_CODE: SET ESTO-FAILURE-ACTION-INVALID   TO TRUE
			ws.getHallesto().getEstoOutput().getStoreDetailCd().setEstoFailureActionInvalid();
			break;
		}
	}

	/**Original name: 0500-MATCHING<br>
	 * <pre>***************************************************************
	 *  A)PERFORM MATCHING OF ERRORS TO ERROR CODES AND PRORITY LEVEL*
	 *  B)UPDATE HALLESTO LINKAGE WITH RESULT                        *
	 * ***************************************************************
	 * ---*
	 *    DETERMINE MATCHING SECTION
	 * ---*</pre>*/
	private void matching() {
		// COB_CODE: EVALUATE EFAL-FAILED-ACTION-TYPE
		//               WHEN 'DB2'
		//                   PERFORM 0510-MATCH-DB2
		//               WHEN 'CICS'
		//                   PERFORM 0520-MATCH-CICS
		//               WHEN 'IAP'
		//                   PERFORM 0530-MATCH-IAP
		//               WHEN 'COMMAREA'
		//                   PERFORM 0540-MATCH-COMA
		//               WHEN 'SECURITY'
		//                   PERFORM 0550-MATCH-SECU
		//               WHEN 'DATAPRIV'
		//                   PERFORM 0560-MATCH-DATP
		//               WHEN 'BUSPROC'
		//                   PERFORM 0570-MATCH-BUSP
		//               WHEN 'AUDIT'
		//                   PERFORM 0580-MATCH-AUDT
		//               WHEN 'LOCKING'
		//                   PERFORM 0590-MATCH-LOCK
		//               WHEN 'ABEND'
		//                   PERFORM 0600-MATCH-ABEND
		//               WHEN 'TRANSFER'
		//                   PERFORM 0610-MATCH-TRANSFER
		//               WHEN OTHER
		//                   MOVE 10            TO EFAL-ETRA-PRIORITY-LEVEL
		//           END-EVALUATE.
		switch (ws.getHallesto().getEstoDetailBuffer().getEfalFailedActionType()) {

		case "DB2":// COB_CODE: PERFORM 0510-MATCH-DB2
			matchDb2();
			break;

		case "CICS":// COB_CODE: PERFORM 0520-MATCH-CICS
			matchCics();
			break;

		case "IAP":// COB_CODE: PERFORM 0530-MATCH-IAP
			matchIap();
			break;

		case "COMMAREA":// COB_CODE: PERFORM 0540-MATCH-COMA
			matchComa();
			break;

		case "SECURITY":// COB_CODE: PERFORM 0550-MATCH-SECU
			matchSecu();
			break;

		case "DATAPRIV":// COB_CODE: PERFORM 0560-MATCH-DATP
			matchDatp();
			break;

		case "BUSPROC":// COB_CODE: PERFORM 0570-MATCH-BUSP
			matchBusp();
			break;

		case "AUDIT":// COB_CODE: PERFORM 0580-MATCH-AUDT
			matchAudt();
			break;

		case "LOCKING":// COB_CODE: PERFORM 0590-MATCH-LOCK
			matchLock();
			break;

		case "ABEND":// COB_CODE: PERFORM 0600-MATCH-ABEND
			matchAbend();
			break;

		case "TRANSFER":// COB_CODE: PERFORM 0610-MATCH-TRANSFER
			matchTransfer();
			break;

		default:// COB_CODE: MOVE '1111/111111' TO EFAL-ETRA-ERROR-REF
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorRef("1111/111111");
			// COB_CODE: MOVE 'UNKNOWN'     TO EFAL-ETRA-ERROR-TXT
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorTxt("UNKNOWN");
			// COB_CODE: MOVE 10            TO EFAL-ETRA-PRIORITY-LEVEL
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraPriorityLevel(10);
			break;
		}
	}

	/**Original name: 0510-MATCH-DB2_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH DB2 ERRORS                                           *
	 * ***************************************************************</pre>*/
	private void matchDb2() {
		// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr(ws.getHallesto().getEstoDetailBuffer().getEfalDb2ErrSqlcodeFormatted());
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0520-MATCH-CICS_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH CICS ERRORS                                          *
	 * ***************************************************************</pre>*/
	private void matchCics() {
		// COB_CODE: MOVE EFAL-CICS-ERR-RESP TO WS-CICS-RESP-CODE.
		ws.getWsWorkAreas().setCicsRespCodeFormatted(ws.getHallesto().getEstoDetailBuffer().getEfalCicsErrRespFormatted());
		// COB_CODE: MOVE WS-CICS-RESP       TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr(ws.getWsWorkAreas().getCicsRespFormatted());
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0530-MATCH-IAP_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH CICS ERRORS                                          *
	 * ***************************************************************</pre>*/
	private void matchIap() {
		// COB_CODE: MOVE SPACES          TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr("");
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0540-MATCH-COMA_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH COMMAREA ERRORS                                      *
	 * ***************************************************************</pre>*/
	private void matchComa() {
		// COB_CODE: MOVE SPACES          TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr("");
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0550-MATCH-SECU_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH SECURITY ERRORS                                      *
	 * ***************************************************************</pre>*/
	private void matchSecu() {
		// COB_CODE: MOVE SPACES          TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr("");
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0560-MATCH-DATP_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH DATA PRIVACY ERRORS                                  *
	 * ***************************************************************</pre>*/
	private void matchDatp() {
		// COB_CODE: MOVE SPACES          TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr("");
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0570-MATCH-BUSP_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH BUSINESS PROCESS ERRORS                              *
	 * ***************************************************************</pre>*/
	private void matchBusp() {
		// COB_CODE: MOVE SPACES          TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr("");
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0580-MATCH-AUDT_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH AUDIT ERRORS                                         *
	 * ***************************************************************</pre>*/
	private void matchAudt() {
		// COB_CODE: MOVE SPACES          TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr("");
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0590-MATCH-LOCK_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH LOCKING ERRORS                                       *
	 * ***************************************************************</pre>*/
	private void matchLock() {
		// COB_CODE: MOVE SPACES          TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr("");
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0600-MATCH-ABEND_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A) CREATE AN ERROR REFERENCE OUTSIDE HALOUIDG BECAUSE DB2    *
	 *     WILL BE CALLED WHICH MAY CAUSE AEY9 ABEND UNDER CERTAIN   *
	 *     CIRCUMSTANCES.                                            *
	 * ***************************************************************</pre>*/
	private void matchAbend() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS ASKTIME
		//                ABSTIME   (WS-ABEND-ABSTIME)
		//           END-EXEC.
		ws.getWsWorkAreas().setAbendAbstime((execContext.updateTime()).toLong());
		// COB_CODE: MOVE WS-ABEND-ABSTIME   TO WS-ABEND-9-ABSTIME.
		ws.getWsWorkAreas().setAbend9Abstime(TruncAbs.toLong(ws.getWsWorkAreas().getAbendAbstime(), 15));
		// COB_CODE: STRING WS-ABEND-9-ABSTIME(7:4)
		//                  '/'
		//                  WS-ABEND-9-ABSTIME(11:5)
		//                        DELIMITED BY SIZE
		//                                 INTO EFAL-ETRA-ERROR-REF
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_REF,
				ws.getWsWorkAreas().getAbend9AbstimeFormatted().substring((7) - 1, 10), "/",
				ws.getWsWorkAreas().getAbend9AbstimeFormatted().substring((11) - 1, 15));
		ws.getHallesto().getEstoDetailBuffer()
				.setEfalEtraErrorRef(concatUtil.replaceInString(ws.getHallesto().getEstoDetailBuffer().getEfalEtraErrorRefFormatted()));
		// COB_CODE: MOVE 'DEFAULT ABEND ERROR CODE'
		//                                   TO EFAL-ETRA-ERROR-TXT.
		ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorTxt("DEFAULT ABEND ERROR CODE");
		// COB_CODE: MOVE 90                 TO EFAL-ETRA-PRIORITY-LEVEL.
		ws.getHallesto().getEstoDetailBuffer().setEfalEtraPriorityLevel(90);
	}

	/**Original name: 0610-MATCH-TRANSFER_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)MATCH TRANSFER ERRORS                                      *
	 * ***************************************************************</pre>*/
	private void matchTransfer() {
		// COB_CODE: MOVE SPACES          TO HETS-TYP-ERR.
		ws.getDclhalertErrTranSupV().setHetsTypErr("");
		// COB_CODE: PERFORM 0700-MATCH-ON-SUP-TABLE.
		matchOnSupTable();
	}

	/**Original name: 0700-MATCH-ON-SUP-TABLE_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)USING MATCHING KEYS FROM EACH CALLING SECTION SELECT       *
	 *    UNIQUE ERROR ON SUP TABLE                                  *
	 *  B)USING KEY DATA FROM BEFORE TRY TO OBTAIN DEFAULT SETTINGS  *
	 *    FOR THE ERROR                                              *
	 * ***************************************************************</pre>*/
	private void matchOnSupTable() {
		// COB_CODE: MOVE ETRA-ERR-ACTION         TO HETS-ERR-ATN-CD.
		ws.getDclhalertErrTranSupV().setHetsErrAtnCd(ws.getHallesto().getEstoDetailBuffer().getEtraErrAction());
		// COB_CODE: MOVE EFAL-FAILED-APPLICATION TO APP-NM.
		ws.getDclhalertErrTranSupV().setAppNm(ws.getHallesto().getEstoDetailBuffer().getEfalFailedApplication());
		// COB_CODE: MOVE EFAL-FAILED-ACTION-TYPE TO HETS-FAIL-ACY-CD.
		ws.getDclhalertErrTranSupV().setHetsFailAcyCd(ws.getHallesto().getEstoDetailBuffer().getEfalFailedActionType());
		// COB_CODE: EXEC SQL
		//               SELECT HETS_ERR_PTY_NBR,
		//                      HETS_ERR_TXT
		//               INTO   :HETS-ERR-PTY-NBR,
		//                      :HETS-ERR-TXT
		//               FROM HAL_ERR_TRAN_SUP_V
		//               WHERE APP_NM             = :APP-NM
		//               AND   FAIL_ACY_CD        = :HETS-FAIL-ACY-CD
		//               AND   HETS_ERR_ATN_CD    = :HETS-ERR-ATN-CD
		//               AND   HETS_TYP_ERR       = :HETS-TYP-ERR
		//           END-EXEC.
		halErrTranSupVDao.selectRec(ws.getDclhalertErrTranSupV().getAppNm(), ws.getDclhalertErrTranSupV().getHetsFailAcyCd(),
				ws.getDclhalertErrTranSupV().getHetsErrAtnCd(), ws.getDclhalertErrTranSupV().getHetsTypErr(), ws.getDclhalertErrTranSupV());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   GO TO 0700-MATCH-ON-SUP-TABLE-X
		//               WHEN ERD-SQL-NOT-FOUND
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0700-MATCH-ON-SUP-TABLE-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: PERFORM 0710-GENERATE-ERR-REF
			generateErrRef();
			// COB_CODE: MOVE HETS-ERR-PTY-NBR      TO
			//                                     EFAL-ETRA-PRIORITY-LEVEL
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraPriorityLevel(TruncAbs.toInt(ws.getDclhalertErrTranSupV().getHetsErrPtyNbr(), 5));
			// COB_CODE: MOVE HETS-ERR-TXT          TO EFAL-ETRA-ERROR-TXT
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorTxt(ws.getDclhalertErrTranSupV().getHetsErrTxt());
			// COB_CODE: GO TO 0700-MATCH-ON-SUP-TABLE-X
			return;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET ESTO-ERR-TRANS-FAILED       TO TRUE
			ws.getHallesto().getEstoOutput().getStoreReturnCd().setEstoErrTransFailed();
			// COB_CODE: SET ESTO-SUP-TAB-UNIQUE-DB2-ERR TO TRUE
			ws.getHallesto().getEstoOutput().getStoreDetailCd().setEstoSupTabUniqueDb2Err();
			// COB_CODE: MOVE SQLCODE                    TO ESTO-ERR-SQLCODE
			ws.getHallesto().getEstoOutput().setSqlcode(sqlca.getSqlcode());
			// COB_CODE: MOVE SQLERRMC                   TO ESTO-ERR-SQLERRMC
			ws.getHallesto().getEstoOutput().setSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: GO TO 0700-MATCH-ON-SUP-TABLE-X
			return;
		}
		// COB_CODE: EXEC SQL
		//               SELECT HETS_ERR_PTY_NBR,
		//                      HETS_ERR_TXT
		//               INTO   :HETS-ERR-PTY-NBR,
		//                      :HETS-ERR-TXT
		//               FROM HAL_ERR_TRAN_SUP_V
		//               WHERE APP_NM             = :APP-NM
		//               AND   FAIL_ACY_CD        = :HETS-FAIL-ACY-CD
		//               AND   HETS_ERR_ATN_CD    = 'DEFAULT'
		//           END-EXEC.
		halErrTranSupVDao.selectRec1(ws.getDclhalertErrTranSupV().getAppNm(), ws.getDclhalertErrTranSupV().getHetsFailAcyCd(),
				ws.getDclhalertErrTranSupV());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   GO TO 0700-MATCH-ON-SUP-TABLE-X
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 0700-MATCH-ON-SUP-TABLE-X
		//               WHEN OTHER
		//                   GO TO 0700-MATCH-ON-SUP-TABLE-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: PERFORM 0710-GENERATE-ERR-REF
			generateErrRef();
			// COB_CODE: MOVE HETS-ERR-PTY-NBR      TO
			//                                     EFAL-ETRA-PRIORITY-LEVEL
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraPriorityLevel(TruncAbs.toInt(ws.getDclhalertErrTranSupV().getHetsErrPtyNbr(), 5));
			// COB_CODE: MOVE HETS-ERR-TXT          TO EFAL-ETRA-ERROR-TXT
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorTxt(ws.getDclhalertErrTranSupV().getHetsErrTxt());
			// COB_CODE: GO TO 0700-MATCH-ON-SUP-TABLE-X
			return;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: PERFORM 0710-GENERATE-ERR-REF
			generateErrRef();
			// COB_CODE: MOVE 10                    TO
			//                                     EFAL-ETRA-PRIORITY-LEVEL
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraPriorityLevel(10);
			// COB_CODE: MOVE 'DEFAULT TEXT NOT FOUND'
			//                                      TO EFAL-ETRA-ERROR-TXT
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorTxt("DEFAULT TEXT NOT FOUND");
			// COB_CODE: GO TO 0700-MATCH-ON-SUP-TABLE-X
			return;

		default:// COB_CODE: SET ESTO-ERR-TRANS-FAILED       TO TRUE
			ws.getHallesto().getEstoOutput().getStoreReturnCd().setEstoErrTransFailed();
			// COB_CODE: SET ESTO-SUP-TAB-NON-UQUE-DB2-ERR
			//                                           TO TRUE
			ws.getHallesto().getEstoOutput().getStoreDetailCd().setEstoSupTabNonUqueDb2Err();
			// COB_CODE: MOVE SQLCODE                    TO ESTO-ERR-SQLCODE
			ws.getHallesto().getEstoOutput().setSqlcode(sqlca.getSqlcode());
			// COB_CODE: MOVE SQLERRMC                   TO ESTO-ERR-SQLERRMC
			ws.getHallesto().getEstoOutput().setSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: GO TO 0700-MATCH-ON-SUP-TABLE-X
			return;
		}
	}

	/**Original name: 0710-GENERATE-ERR-REF_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)GENERATE THE ERROR REFERENCE TO SEND BACK IN THE ERROR CODE*
	 *  FIELD                                                        *
	 * ***************************************************************</pre>*/
	private void generateErrRef() {
		// COB_CODE: INITIALIZE WS-HALOUBOC-LINKAGE.
		initWsHaloubocLinkage();
		// COB_CODE: MOVE ZERO              TO UIDG-UNIT-NBR.
		ws.getHalluidg().setUidgUnitNbr(((short) 0));
		// COB_CODE: MOVE 1                 TO W-COUNT.
		ws.getwWorkfields().setCount(((short) 1));
		// COB_CODE: MOVE EFAL-LOGON-USERID TO UBOC-AUTH-USERID.
		ws.getWsHaloubocLinkage().getCommInfo().setUbocAuthUserid(ws.getHallesto().getEstoDetailBuffer().getEfalLogonUserid());
		// COB_CODE: MOVE EFAL-UNIT-OF-WORK TO UBOC-UOW-NAME.
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowName(ws.getHallesto().getEstoDetailBuffer().getEfalUnitOfWork());
		// COB_CODE: MOVE ESTO-STORE-ID     TO UBOC-MSG-ID.
		ws.getWsHaloubocLinkage().getCommInfo().setUbocMsgId(ws.getHallesto().getEstoInputKey().getStoreId());
		// COB_CODE: SET DETERMINE-ERR-REF TO TRUE.
		ws.getwWorkfields().getHalouidgSearchInd().setDetermineErrRef();
		// COB_CODE: PERFORM 0720-GET-ERR-REF
		//               UNTIL W-COUNT > 5
		//               OR ERROR-CODE-OK
		//               OR ERROR-FOUND-NO-RETRY.
		while (!(ws.getwWorkfields().getCount() > 5 || ws.getwWorkfields().getHalouidgSearchInd().isErrorCodeOk()
				|| ws.getwWorkfields().getHalouidgSearchInd().isErrorFoundNoRetry())) {
			getErrRef();
		}
	}

	/**Original name: 0720-GET-ERR-REF_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A)GET THE ERROR REFERENCE FROM HALUIDB/HALOUIDG
	 * ***************************************************************</pre>*/
	private void getErrRef() {
		// COB_CODE: SET UIDG-ERROR-ID TO TRUE.
		ws.getHalluidg().getUidgIdType().setUidgErrorId();
		// COB_CODE: ADD 1 TO UIDG-UNIT-NBR.
		ws.getHalluidg().setUidgUnitNbr(Trunc.toShort(1 + ws.getHalluidg().getUidgUnitNbr(), 4));
		// COB_CODE: MOVE LENGTH OF WS-HALOUIDG-LINKAGE
		//                TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsHaloubocLinkage().setAppDataBufferLength(((short) HaloetraData.Len.WS_HALOUIDG_LINKAGE));
		// COB_CODE: MOVE WS-HALOUIDG-LINKAGE TO UBOC-APP-DATA-BUFFER.
		ws.getWsHaloubocLinkage().setAppDataBuffer(ws.getWsHalouidgLinkageFormatted());
		//*        PROGRAM  ('HALOUIDG')
		// COB_CODE:      EXEC CICS LINK
		//           **        PROGRAM  ('HALOUIDG')
		//                     PROGRAM  ('HALUIDB')
		//                     COMMAREA (WS-HALOUBOC-LINKAGE)
		//                     LENGTH   (LENGTH OF WS-HALOUBOC-LINKAGE)
		//                     RESP     (WS-RESPONSE-CODE)
		//                     RESP2    (WS-RESPONSE-CODE2)
		//                END-EXEC.
		TpRunner.context("HALOETRA", execContext).commarea(ws.getWsHaloubocLinkage()).length(Dfhcommarea.Len.DFHCOMMAREA).link("HALUIDB",
				new Haluidb());
		ws.getWsWorkAreas().setResponseCode(execContext.getResp());
		ws.getWsWorkAreas().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 0720-GET-ERR-REF-X
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsWorkAreas().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE '9999/99999' TO EFAL-ETRA-ERROR-REF
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorRef("9999/99999");
			// COB_CODE: SET ERROR-FOUND-NO-RETRY TO TRUE
			ws.getwWorkfields().getHalouidgSearchInd().setErrorFoundNoRetry();
			// COB_CODE: GO TO 0720-GET-ERR-REF-X
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER TO WS-HALOUIDG-LINKAGE.
		ws.setWsHalouidgLinkageFormatted(ws.getWsHaloubocLinkage().getAppDataBufferFormatted());
		// COB_CODE: IF UIDG-GENERATED-ERROR-REF = SPACES OR LOW-VALUES
		//               GO TO 0720-GET-ERR-REF-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHalluidg().getUidgErrorIdOutput().getGeneratedErrorRef())
				|| Characters.EQ_LOW.test(ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedErrorRefFormatted())) {
			// COB_CODE: MOVE '8888/88888' TO EFAL-ETRA-ERROR-REF
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorRef("8888/88888");
			// COB_CODE: SET ERROR-FOUND-NO-RETRY TO TRUE
			ws.getwWorkfields().getHalouidgSearchInd().setErrorFoundNoRetry();
			// COB_CODE: GO TO 0720-GET-ERR-REF-X
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//             WHEN UIDG-ERRID-SUCCESSFUL
		//               SET ERROR-CODE-OK     TO TRUE
		//             WHEN UIDG-ERRID-DUPERR
		//                                     TO EFAL-ETRA-ERROR-REF
		//             WHEN UIDG-ERRID-SQLERR
		//               GO TO 0720-GET-ERR-REF-X
		//           END-EVALUATE.
		switch (ws.getHalluidg().getUidgErrorIdOutput().getErridErrorCode().getErridErrorCode()) {

		case UidgErridErrorCode.SUCCESSFUL:// COB_CODE: MOVE UIDG-GENERATED-ERROR-REF
			//                                 TO EFAL-ETRA-ERROR-REF
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorRef(ws.getHalluidg().getUidgErrorIdOutput().getGeneratedErrorRef());
			// COB_CODE: SET ERROR-CODE-OK     TO TRUE
			ws.getwWorkfields().getHalouidgSearchInd().setErrorCodeOk();
			break;

		case UidgErridErrorCode.DUPERR:// COB_CODE: MOVE UIDG-GENERATED-ERROR-REF
			//                                 TO EFAL-ETRA-ERROR-REF
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorRef(ws.getHalluidg().getUidgErrorIdOutput().getGeneratedErrorRef());
			break;

		case UidgErridErrorCode.SQLERR:// COB_CODE: MOVE '7777/77777'     TO EFAL-ETRA-ERROR-REF
			ws.getHallesto().getEstoDetailBuffer().setEfalEtraErrorRef("7777/77777");
			// COB_CODE: SET ERROR-FOUND-NO-RETRY
			//                                 TO TRUE
			ws.getwWorkfields().getHalouidgSearchInd().setErrorFoundNoRetry();
			// COB_CODE: GO TO 0720-GET-ERR-REF-X
			return;

		default:
			break;
		}
		// COB_CODE: ADD 1 TO W-COUNT.
		ws.getwWorkfields().setCount(Trunc.toShort(1 + ws.getwWorkfields().getCount(), 1));
	}

	public void initWsHaloubocLinkage() {
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowName("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocMsgId("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocAuthUserid("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocAuthUserClientid("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocTerminalId("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocPrimaryBusObj("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocStoreTypeCd().setUbocStoreTypeCd(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().setUbocMdrReqStore("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocMdrRspStore("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowReqMsgStore("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowReqSwitchesStore("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowRespHeaderStore("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowRespDataStore("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowRespWarningsStore("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowKeyReplaceStore("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowRespNlBlErrsStore("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocSessionId("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowLockProcTsq("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowReqSwitchesTsq("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocPassThruAction().setUbocPassThruAction("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocNbrHdrRowsFormatted("000000000");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocNbrDataRowsFormatted("000000000");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocNbrWarningsFormatted("000000000");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocNbrNonlogBlErrsFormatted("000000000");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocApplicationTsSw().setUbocApplicationTsSw(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().setUbocApplicationTs("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocUowLockStrategyCd().setUbocUowLockStrategyCd(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().setUbocUowLockTimeoutInterval(0);
		ws.getWsHaloubocLinkage().getCommInfo().getUbocUserInLockGrpSw().setUbocUserInLockGrpSw(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().setUbocLockGroup("");
		ws.getWsHaloubocLinkage().getCommInfo().setFiller3("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().setApplyDataPrivacySw(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().getUbocSecAndDataPrivInfo().setSecurityContext("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocSecAndDataPrivInfo().getSecAutNbr().setUbocSecAutNbrFormatted("00000");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocSecAndDataPrivInfo().setSecAssociationType("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocDataPrivRetCode().setUbocDataPrivRetCode("");
		ws.getWsHaloubocLinkage().getCommInfo().setUbocSecGroupName("");
		ws.getWsHaloubocLinkage().getCommInfo().setFiller4("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocAuditProcessingInfo().getApplyAuditsSw().setApplyAuditsSw(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().getUbocAuditProcessingInfo().setAudtBusObjNm("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocAuditProcessingInfo().setAudtEventData("");
		ws.getWsHaloubocLinkage().getCommInfo().setFiller5("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocKeepClearUowStorageSw().setUbocKeepClearUowStorageSw(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setUbocObjectLoggableProblems(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setUbocProcessingStatusSw(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().setFiller6("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setErrorCode("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setUbocSqlcodeDisplayFormatted("0000000000");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setUbocEibrespDisplayFormatted("0000000000");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setUbocEibresp2DisplayFormatted("0000000000");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setUbocLoggableErrLogOnlySw(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().setFiller7("");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw()
				.setErrorsLoggedSw(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw()
				.setErrorLoggingLvlSw(Types.SPACE_CHAR);
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setUbocErrLogSqlcodeDsplyFormatted("0000000000");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setUbocErrLogEibrespDsplyFormatted("0000000000");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setUbocErrLogEibresp2DsplyFormatted("0000000000");
		ws.getWsHaloubocLinkage().getCommInfo().getUbocErrorDetails().setFiller8("");
		ws.getWsHaloubocLinkage().setAppDataBufferLengthFormatted("0000");
		ws.getWsHaloubocLinkage().setAppDataBuffer("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}

	public void deleteArgListeners() {
		execContext.getCommAreaLenNotifier().deleteListener(dfhcommarea.getLnkCaDataListener());
	}

	public void registerArgListeners() {
		execContext.getCommAreaLenNotifier().addListener(dfhcommarea.getLnkCaDataListener());
		execContext.getCommAreaLenNotifier().notifyListeners();
	}
}
