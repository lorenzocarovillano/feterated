/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0011<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0011 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0011() {
	}

	public LServiceContractAreaXz0x0011(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt11iTkNotPrcTs(String xzt11iTkNotPrcTs) {
		writeString(Pos.XZT11I_TK_NOT_PRC_TS, xzt11iTkNotPrcTs, Len.XZT11I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT11I-TK-NOT-PRC-TS<br>*/
	public String getXzt11iTkNotPrcTs() {
		return readString(Pos.XZT11I_TK_NOT_PRC_TS, Len.XZT11I_TK_NOT_PRC_TS);
	}

	public void setXzt11iTkRecSeqNbr(int xzt11iTkRecSeqNbr) {
		writeInt(Pos.XZT11I_TK_REC_SEQ_NBR, xzt11iTkRecSeqNbr, Len.Int.XZT11I_TK_REC_SEQ_NBR);
	}

	/**Original name: XZT11I-TK-REC-SEQ-NBR<br>*/
	public int getXzt11iTkRecSeqNbr() {
		return readNumDispInt(Pos.XZT11I_TK_REC_SEQ_NBR, Len.XZT11I_TK_REC_SEQ_NBR);
	}

	public String getXzt11iTkRecSeqNbrFormatted() {
		return readFixedString(Pos.XZT11I_TK_REC_SEQ_NBR, Len.XZT11I_TK_REC_SEQ_NBR);
	}

	public void setXzt11iCsrActNbr(String xzt11iCsrActNbr) {
		writeString(Pos.XZT11I_CSR_ACT_NBR, xzt11iCsrActNbr, Len.XZT11I_CSR_ACT_NBR);
	}

	/**Original name: XZT11I-CSR-ACT-NBR<br>*/
	public String getXzt11iCsrActNbr() {
		return readString(Pos.XZT11I_CSR_ACT_NBR, Len.XZT11I_CSR_ACT_NBR);
	}

	public void setXzt11iUserid(String xzt11iUserid) {
		writeString(Pos.XZT11I_USERID, xzt11iUserid, Len.XZT11I_USERID);
	}

	/**Original name: XZT11I-USERID<br>*/
	public String getXzt11iUserid() {
		return readString(Pos.XZT11I_USERID, Len.XZT11I_USERID);
	}

	public String getXzt11iUseridFormatted() {
		return Functions.padBlanks(getXzt11iUserid(), Len.XZT11I_USERID);
	}

	public void setXzt11oTkNotPrcTs(String xzt11oTkNotPrcTs) {
		writeString(Pos.XZT11O_TK_NOT_PRC_TS, xzt11oTkNotPrcTs, Len.XZT11O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT11O-TK-NOT-PRC-TS<br>*/
	public String getXzt11oTkNotPrcTs() {
		return readString(Pos.XZT11O_TK_NOT_PRC_TS, Len.XZT11O_TK_NOT_PRC_TS);
	}

	public void setXzt11oTkRecSeqNbr(int xzt11oTkRecSeqNbr) {
		writeInt(Pos.XZT11O_TK_REC_SEQ_NBR, xzt11oTkRecSeqNbr, Len.Int.XZT11O_TK_REC_SEQ_NBR);
	}

	public void setXzt11oTkRecSeqNbrFormatted(String xzt11oTkRecSeqNbr) {
		writeString(Pos.XZT11O_TK_REC_SEQ_NBR, Trunc.toUnsignedNumeric(xzt11oTkRecSeqNbr, Len.XZT11O_TK_REC_SEQ_NBR), Len.XZT11O_TK_REC_SEQ_NBR);
	}

	/**Original name: XZT11O-TK-REC-SEQ-NBR<br>*/
	public int getXzt11oTkRecSeqNbr() {
		return readNumDispInt(Pos.XZT11O_TK_REC_SEQ_NBR, Len.XZT11O_TK_REC_SEQ_NBR);
	}

	public void setXzt11oTkRecCltId(String xzt11oTkRecCltId) {
		writeString(Pos.XZT11O_TK_REC_CLT_ID, xzt11oTkRecCltId, Len.XZT11O_TK_REC_CLT_ID);
	}

	/**Original name: XZT11O-TK-REC-CLT-ID<br>*/
	public String getXzt11oTkRecCltId() {
		return readString(Pos.XZT11O_TK_REC_CLT_ID, Len.XZT11O_TK_REC_CLT_ID);
	}

	public void setXzt11oTkRecAdrId(String xzt11oTkRecAdrId) {
		writeString(Pos.XZT11O_TK_REC_ADR_ID, xzt11oTkRecAdrId, Len.XZT11O_TK_REC_ADR_ID);
	}

	/**Original name: XZT11O-TK-REC-ADR-ID<br>*/
	public String getXzt11oTkRecAdrId() {
		return readString(Pos.XZT11O_TK_REC_ADR_ID, Len.XZT11O_TK_REC_ADR_ID);
	}

	public void setXzt11oTkMnlInd(char xzt11oTkMnlInd) {
		writeChar(Pos.XZT11O_TK_MNL_IND, xzt11oTkMnlInd);
	}

	/**Original name: XZT11O-TK-MNL-IND<br>*/
	public char getXzt11oTkMnlInd() {
		return readChar(Pos.XZT11O_TK_MNL_IND);
	}

	public void setXzt11oTkActNotRecCsumFormatted(String xzt11oTkActNotRecCsum) {
		writeString(Pos.XZT11O_TK_ACT_NOT_REC_CSUM, Trunc.toUnsignedNumeric(xzt11oTkActNotRecCsum, Len.XZT11O_TK_ACT_NOT_REC_CSUM),
				Len.XZT11O_TK_ACT_NOT_REC_CSUM);
	}

	/**Original name: XZT11O-TK-ACT-NOT-REC-CSUM<br>*/
	public int getXzt11oTkActNotRecCsum() {
		return readNumDispUnsignedInt(Pos.XZT11O_TK_ACT_NOT_REC_CSUM, Len.XZT11O_TK_ACT_NOT_REC_CSUM);
	}

	public void setXzt11oCsrActNbr(String xzt11oCsrActNbr) {
		writeString(Pos.XZT11O_CSR_ACT_NBR, xzt11oCsrActNbr, Len.XZT11O_CSR_ACT_NBR);
	}

	/**Original name: XZT11O-CSR-ACT-NBR<br>*/
	public String getXzt11oCsrActNbr() {
		return readString(Pos.XZT11O_CSR_ACT_NBR, Len.XZT11O_CSR_ACT_NBR);
	}

	public void setXzt11oRecTypCd(String xzt11oRecTypCd) {
		writeString(Pos.XZT11O_REC_TYP_CD, xzt11oRecTypCd, Len.XZT11O_REC_TYP_CD);
	}

	/**Original name: XZT11O-REC-TYP-CD<br>*/
	public String getXzt11oRecTypCd() {
		return readString(Pos.XZT11O_REC_TYP_CD, Len.XZT11O_REC_TYP_CD);
	}

	public void setXzt11oRecTypShtDes(String xzt11oRecTypShtDes) {
		writeString(Pos.XZT11O_REC_TYP_SHT_DES, xzt11oRecTypShtDes, Len.XZT11O_REC_TYP_SHT_DES);
	}

	/**Original name: XZT11O-REC-TYP-SHT-DES<br>*/
	public String getXzt11oRecTypShtDes() {
		return readString(Pos.XZT11O_REC_TYP_SHT_DES, Len.XZT11O_REC_TYP_SHT_DES);
	}

	public void setXzt11oRecTypLngDes(String xzt11oRecTypLngDes) {
		writeString(Pos.XZT11O_REC_TYP_LNG_DES, xzt11oRecTypLngDes, Len.XZT11O_REC_TYP_LNG_DES);
	}

	/**Original name: XZT11O-REC-TYP-LNG-DES<br>*/
	public String getXzt11oRecTypLngDes() {
		return readString(Pos.XZT11O_REC_TYP_LNG_DES, Len.XZT11O_REC_TYP_LNG_DES);
	}

	public void setXzt11oRecTypSrOrdNbr(int xzt11oRecTypSrOrdNbr) {
		writeInt(Pos.XZT11O_REC_TYP_SR_ORD_NBR, xzt11oRecTypSrOrdNbr, Len.Int.XZT11O_REC_TYP_SR_ORD_NBR);
	}

	public void setXzt11oRecTypSrOrdNbrFormatted(String xzt11oRecTypSrOrdNbr) {
		writeString(Pos.XZT11O_REC_TYP_SR_ORD_NBR, Trunc.toUnsignedNumeric(xzt11oRecTypSrOrdNbr, Len.XZT11O_REC_TYP_SR_ORD_NBR),
				Len.XZT11O_REC_TYP_SR_ORD_NBR);
	}

	/**Original name: XZT11O-REC-TYP-SR-ORD-NBR<br>*/
	public int getXzt11oRecTypSrOrdNbr() {
		return readNumDispInt(Pos.XZT11O_REC_TYP_SR_ORD_NBR, Len.XZT11O_REC_TYP_SR_ORD_NBR);
	}

	public void setXzt11oRecName(String xzt11oRecName) {
		writeString(Pos.XZT11O_REC_NAME, xzt11oRecName, Len.XZT11O_REC_NAME);
	}

	/**Original name: XZT11O-REC-NAME<br>*/
	public String getXzt11oRecName() {
		return readString(Pos.XZT11O_REC_NAME, Len.XZT11O_REC_NAME);
	}

	public void setXzt11oLin1Adr(String xzt11oLin1Adr) {
		writeString(Pos.XZT11O_LIN1_ADR, xzt11oLin1Adr, Len.XZT11O_LIN1_ADR);
	}

	/**Original name: XZT11O-LIN-1-ADR<br>*/
	public String getXzt11oLin1Adr() {
		return readString(Pos.XZT11O_LIN1_ADR, Len.XZT11O_LIN1_ADR);
	}

	public void setXzt11oLin2Adr(String xzt11oLin2Adr) {
		writeString(Pos.XZT11O_LIN2_ADR, xzt11oLin2Adr, Len.XZT11O_LIN2_ADR);
	}

	/**Original name: XZT11O-LIN-2-ADR<br>*/
	public String getXzt11oLin2Adr() {
		return readString(Pos.XZT11O_LIN2_ADR, Len.XZT11O_LIN2_ADR);
	}

	public void setXzt11oCityName(String xzt11oCityName) {
		writeString(Pos.XZT11O_CITY_NAME, xzt11oCityName, Len.XZT11O_CITY_NAME);
	}

	/**Original name: XZT11O-CITY-NAME<br>*/
	public String getXzt11oCityName() {
		return readString(Pos.XZT11O_CITY_NAME, Len.XZT11O_CITY_NAME);
	}

	public void setXzt11oStateAbb(String xzt11oStateAbb) {
		writeString(Pos.XZT11O_STATE_ABB, xzt11oStateAbb, Len.XZT11O_STATE_ABB);
	}

	/**Original name: XZT11O-STATE-ABB<br>*/
	public String getXzt11oStateAbb() {
		return readString(Pos.XZT11O_STATE_ABB, Len.XZT11O_STATE_ABB);
	}

	public void setXzt11oPostalCd(String xzt11oPostalCd) {
		writeString(Pos.XZT11O_POSTAL_CD, xzt11oPostalCd, Len.XZT11O_POSTAL_CD);
	}

	/**Original name: XZT11O-POSTAL-CD<br>*/
	public String getXzt11oPostalCd() {
		return readString(Pos.XZT11O_POSTAL_CD, Len.XZT11O_POSTAL_CD);
	}

	public void setXzt11oCertNbr(String xzt11oCertNbr) {
		writeString(Pos.XZT11O_CERT_NBR, xzt11oCertNbr, Len.XZT11O_CERT_NBR);
	}

	/**Original name: XZT11O-CERT-NBR<br>*/
	public String getXzt11oCertNbr() {
		return readString(Pos.XZT11O_CERT_NBR, Len.XZT11O_CERT_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT011_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT11I_TECHNICAL_KEY = XZT011_SERVICE_INPUTS;
		public static final int XZT11I_TK_NOT_PRC_TS = XZT11I_TECHNICAL_KEY;
		public static final int XZT11I_TK_REC_SEQ_NBR = XZT11I_TK_NOT_PRC_TS + Len.XZT11I_TK_NOT_PRC_TS;
		public static final int XZT11I_CSR_ACT_NBR = XZT11I_TK_REC_SEQ_NBR + Len.XZT11I_TK_REC_SEQ_NBR;
		public static final int XZT11I_USERID = XZT11I_CSR_ACT_NBR + Len.XZT11I_CSR_ACT_NBR;
		public static final int XZT011_SERVICE_OUTPUTS = XZT11I_USERID + Len.XZT11I_USERID;
		public static final int XZT11O_TECHNICAL_KEY = XZT011_SERVICE_OUTPUTS;
		public static final int XZT11O_TK_NOT_PRC_TS = XZT11O_TECHNICAL_KEY;
		public static final int XZT11O_TK_REC_SEQ_NBR = XZT11O_TK_NOT_PRC_TS + Len.XZT11O_TK_NOT_PRC_TS;
		public static final int XZT11O_TK_REC_CLT_ID = XZT11O_TK_REC_SEQ_NBR + Len.XZT11O_TK_REC_SEQ_NBR;
		public static final int XZT11O_TK_REC_ADR_ID = XZT11O_TK_REC_CLT_ID + Len.XZT11O_TK_REC_CLT_ID;
		public static final int XZT11O_TK_MNL_IND = XZT11O_TK_REC_ADR_ID + Len.XZT11O_TK_REC_ADR_ID;
		public static final int XZT11O_TK_ACT_NOT_REC_CSUM = XZT11O_TK_MNL_IND + Len.XZT11O_TK_MNL_IND;
		public static final int XZT11O_CSR_ACT_NBR = XZT11O_TK_ACT_NOT_REC_CSUM + Len.XZT11O_TK_ACT_NOT_REC_CSUM;
		public static final int XZT11O_REC_TYP = XZT11O_CSR_ACT_NBR + Len.XZT11O_CSR_ACT_NBR;
		public static final int XZT11O_REC_TYP_CD = XZT11O_REC_TYP;
		public static final int XZT11O_REC_TYP_SHT_DES = XZT11O_REC_TYP_CD + Len.XZT11O_REC_TYP_CD;
		public static final int XZT11O_REC_TYP_LNG_DES = XZT11O_REC_TYP_SHT_DES + Len.XZT11O_REC_TYP_SHT_DES;
		public static final int XZT11O_REC_TYP_SR_ORD_NBR = XZT11O_REC_TYP_LNG_DES + Len.XZT11O_REC_TYP_LNG_DES;
		public static final int XZT11O_REC_NAME = XZT11O_REC_TYP_SR_ORD_NBR + Len.XZT11O_REC_TYP_SR_ORD_NBR;
		public static final int XZT11O_LIN1_ADR = XZT11O_REC_NAME + Len.XZT11O_REC_NAME;
		public static final int XZT11O_LIN2_ADR = XZT11O_LIN1_ADR + Len.XZT11O_LIN1_ADR;
		public static final int XZT11O_CITY_NAME = XZT11O_LIN2_ADR + Len.XZT11O_LIN2_ADR;
		public static final int XZT11O_STATE_ABB = XZT11O_CITY_NAME + Len.XZT11O_CITY_NAME;
		public static final int XZT11O_POSTAL_CD = XZT11O_STATE_ABB + Len.XZT11O_STATE_ABB;
		public static final int XZT11O_CERT_NBR = XZT11O_POSTAL_CD + Len.XZT11O_POSTAL_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT11I_TK_NOT_PRC_TS = 26;
		public static final int XZT11I_TK_REC_SEQ_NBR = 5;
		public static final int XZT11I_CSR_ACT_NBR = 9;
		public static final int XZT11I_USERID = 8;
		public static final int XZT11O_TK_NOT_PRC_TS = 26;
		public static final int XZT11O_TK_REC_SEQ_NBR = 5;
		public static final int XZT11O_TK_REC_CLT_ID = 64;
		public static final int XZT11O_TK_REC_ADR_ID = 64;
		public static final int XZT11O_TK_MNL_IND = 1;
		public static final int XZT11O_TK_ACT_NOT_REC_CSUM = 9;
		public static final int XZT11O_CSR_ACT_NBR = 9;
		public static final int XZT11O_REC_TYP_CD = 5;
		public static final int XZT11O_REC_TYP_SHT_DES = 13;
		public static final int XZT11O_REC_TYP_LNG_DES = 30;
		public static final int XZT11O_REC_TYP_SR_ORD_NBR = 5;
		public static final int XZT11O_REC_NAME = 120;
		public static final int XZT11O_LIN1_ADR = 45;
		public static final int XZT11O_LIN2_ADR = 45;
		public static final int XZT11O_CITY_NAME = 30;
		public static final int XZT11O_STATE_ABB = 2;
		public static final int XZT11O_POSTAL_CD = 13;
		public static final int XZT11I_TECHNICAL_KEY = XZT11I_TK_NOT_PRC_TS + XZT11I_TK_REC_SEQ_NBR;
		public static final int XZT011_SERVICE_INPUTS = XZT11I_TECHNICAL_KEY + XZT11I_CSR_ACT_NBR + XZT11I_USERID;
		public static final int XZT11O_TECHNICAL_KEY = XZT11O_TK_NOT_PRC_TS + XZT11O_TK_REC_SEQ_NBR + XZT11O_TK_REC_CLT_ID + XZT11O_TK_REC_ADR_ID
				+ XZT11O_TK_MNL_IND + XZT11O_TK_ACT_NOT_REC_CSUM;
		public static final int XZT11O_REC_TYP = XZT11O_REC_TYP_CD + XZT11O_REC_TYP_SHT_DES + XZT11O_REC_TYP_LNG_DES + XZT11O_REC_TYP_SR_ORD_NBR;
		public static final int XZT11O_CERT_NBR = 25;
		public static final int XZT011_SERVICE_OUTPUTS = XZT11O_TECHNICAL_KEY + XZT11O_CSR_ACT_NBR + XZT11O_REC_TYP + XZT11O_REC_NAME
				+ XZT11O_LIN1_ADR + XZT11O_LIN2_ADR + XZT11O_CITY_NAME + XZT11O_STATE_ABB + XZT11O_POSTAL_CD + XZT11O_CERT_NBR;
		public static final int L_SERVICE_CONTRACT_AREA = XZT011_SERVICE_INPUTS + XZT011_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT11I_TK_REC_SEQ_NBR = 5;
			public static final int XZT11O_TK_REC_SEQ_NBR = 5;
			public static final int XZT11O_REC_TYP_SR_ORD_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
