/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: FILLER-WORK-AREA<br>
 * Variable: FILLER-WORK-AREA from program TS020000<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class FillerWorkArea {

	//==== PROPERTIES ====
	//Original name: FILLER-WORK-AREA-1
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: WA-TS-ONES-MINUTE
	private char tsOnesMinute = DefaultValues.CHAR_VAL;
	//Original name: FILLER-WORK-AREA-2
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: WA-TS-SEC
	private String tsSec = DefaultValues.stringVal(Len.TS_SEC);
	//Original name: FILLER-WORK-AREA-3
	private char flr3 = DefaultValues.CHAR_VAL;
	//Original name: WA-TS-MILLISEC
	private String tsMillisec = DefaultValues.stringVal(Len.TS_MILLISEC);

	//==== METHODS ====
	public void setWaTimestamp(String waTimestamp) {
		int position = 1;
		byte[] buffer = getFillerWorkAreaBytes();
		MarshalByte.writeString(buffer, position, waTimestamp, Len.WA_TIMESTAMP);
		setFillerWorkAreaBytes(buffer);
	}

	/**Original name: WA-TIMESTAMP<br>*/
	public String getWaTimestamp() {
		int position = 1;
		return MarshalByte.readString(getFillerWorkAreaBytes(), position, Len.WA_TIMESTAMP);
	}

	public void setFillerWorkAreaBytes(byte[] buffer) {
		setFillerWorkAreaBytes(buffer, 1);
	}

	/**Original name: FILLER-WORK-AREA<br>*/
	public byte[] getFillerWorkAreaBytes() {
		byte[] buffer = new byte[Len.FILLER_WORK_AREA];
		return getFillerWorkAreaBytes(buffer, 1);
	}

	public void setFillerWorkAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		tsOnesMinute = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tsSec = MarshalByte.readString(buffer, position, Len.TS_SEC);
		position += Len.TS_SEC;
		flr3 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tsMillisec = MarshalByte.readString(buffer, position, Len.TS_MILLISEC);
	}

	public byte[] getFillerWorkAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeChar(buffer, position, tsOnesMinute);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tsSec, Len.TS_SEC);
		position += Len.TS_SEC;
		MarshalByte.writeChar(buffer, position, flr3);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tsMillisec, Len.TS_MILLISEC);
		return buffer;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setTsOnesMinute(char tsOnesMinute) {
		this.tsOnesMinute = tsOnesMinute;
	}

	public char getTsOnesMinute() {
		return this.tsOnesMinute;
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setTsSec(String tsSec) {
		this.tsSec = Functions.subString(tsSec, Len.TS_SEC);
	}

	public String getTsSec() {
		return this.tsSec;
	}

	public String getTsSecFormatted() {
		return Functions.padBlanks(getTsSec(), Len.TS_SEC);
	}

	public void setFlr3(char flr3) {
		this.flr3 = flr3;
	}

	public char getFlr3() {
		return this.flr3;
	}

	public void setTsMillisec(String tsMillisec) {
		this.tsMillisec = Functions.subString(tsMillisec, Len.TS_MILLISEC);
	}

	public String getTsMillisec() {
		return this.tsMillisec;
	}

	public String getTsMillisecFormatted() {
		return Functions.padBlanks(getTsMillisec(), Len.TS_MILLISEC);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 15;
		public static final int TS_SEC = 2;
		public static final int TS_MILLISEC = 6;
		public static final int TS_ONES_MINUTE = 1;
		public static final int FLR2 = 1;
		public static final int FILLER_WORK_AREA = TS_ONES_MINUTE + TS_SEC + TS_MILLISEC + FLR1 + 2 * FLR2;
		public static final int WA_TIMESTAMP = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WA_TIMESTAMP = 26;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
