/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLRFR-CMP<br>
 * Variable: DCLRFR-CMP from copybook MUH00626<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclrfrCmp {

	//==== PROPERTIES ====
	//Original name: CMP-CD
	private String cmpCd = DefaultValues.stringVal(Len.CMP_CD);
	//Original name: CMP-DES
	private String cmpDes = DefaultValues.stringVal(Len.CMP_DES);
	//Original name: ACY-IND
	private char acyInd = DefaultValues.CHAR_VAL;
	//Original name: NAIC-CD
	private String naicCd = DefaultValues.stringVal(Len.NAIC_CD);
	//Original name: NAIC-GRP-CD
	private String naicGrpCd = DefaultValues.stringVal(Len.NAIC_GRP_CD);
	//Original name: AM-BST-CMP-CD
	private String amBstCmpCd = DefaultValues.stringVal(Len.AM_BST_CMP_CD);
	//Original name: FED-TIN
	private String fedTin = DefaultValues.stringVal(Len.FED_TIN);
	//Original name: NCCI-CRR-CD
	private String ncciCrrCd = DefaultValues.stringVal(Len.NCCI_CRR_CD);
	//Original name: NCCI-GRP-CD
	private String ncciGrpCd = DefaultValues.stringVal(Len.NCCI_GRP_CD);
	//Original name: PC-CMP-IND
	private char pcCmpInd = DefaultValues.CHAR_VAL;
	//Original name: ISO-CMP-CD
	private String isoCmpCd = DefaultValues.stringVal(Len.ISO_CMP_CD);
	//Original name: GL-BUS-UNT-CD
	private String glBusUntCd = DefaultValues.stringVal(Len.GL_BUS_UNT_CD);
	//Original name: SHT-CMP-DES-LEN
	private short shtCmpDesLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: SHT-CMP-DES-TEXT
	private String shtCmpDesText = DefaultValues.stringVal(Len.SHT_CMP_DES_TEXT);

	//==== METHODS ====
	public void setCmpCd(String cmpCd) {
		this.cmpCd = Functions.subString(cmpCd, Len.CMP_CD);
	}

	public String getCmpCd() {
		return this.cmpCd;
	}

	public void setCmpDes(String cmpDes) {
		this.cmpDes = Functions.subString(cmpDes, Len.CMP_DES);
	}

	public String getCmpDes() {
		return this.cmpDes;
	}

	public void setAcyInd(char acyInd) {
		this.acyInd = acyInd;
	}

	public char getAcyInd() {
		return this.acyInd;
	}

	public void setNaicCd(String naicCd) {
		this.naicCd = Functions.subString(naicCd, Len.NAIC_CD);
	}

	public String getNaicCd() {
		return this.naicCd;
	}

	public void setNaicGrpCd(String naicGrpCd) {
		this.naicGrpCd = Functions.subString(naicGrpCd, Len.NAIC_GRP_CD);
	}

	public String getNaicGrpCd() {
		return this.naicGrpCd;
	}

	public void setAmBstCmpCd(String amBstCmpCd) {
		this.amBstCmpCd = Functions.subString(amBstCmpCd, Len.AM_BST_CMP_CD);
	}

	public String getAmBstCmpCd() {
		return this.amBstCmpCd;
	}

	public void setFedTin(String fedTin) {
		this.fedTin = Functions.subString(fedTin, Len.FED_TIN);
	}

	public String getFedTin() {
		return this.fedTin;
	}

	public void setNcciCrrCd(String ncciCrrCd) {
		this.ncciCrrCd = Functions.subString(ncciCrrCd, Len.NCCI_CRR_CD);
	}

	public String getNcciCrrCd() {
		return this.ncciCrrCd;
	}

	public void setNcciGrpCd(String ncciGrpCd) {
		this.ncciGrpCd = Functions.subString(ncciGrpCd, Len.NCCI_GRP_CD);
	}

	public String getNcciGrpCd() {
		return this.ncciGrpCd;
	}

	public void setPcCmpInd(char pcCmpInd) {
		this.pcCmpInd = pcCmpInd;
	}

	public char getPcCmpInd() {
		return this.pcCmpInd;
	}

	public void setIsoCmpCd(String isoCmpCd) {
		this.isoCmpCd = Functions.subString(isoCmpCd, Len.ISO_CMP_CD);
	}

	public String getIsoCmpCd() {
		return this.isoCmpCd;
	}

	public void setGlBusUntCd(String glBusUntCd) {
		this.glBusUntCd = Functions.subString(glBusUntCd, Len.GL_BUS_UNT_CD);
	}

	public String getGlBusUntCd() {
		return this.glBusUntCd;
	}

	public void setShtCmpDesLen(short shtCmpDesLen) {
		this.shtCmpDesLen = shtCmpDesLen;
	}

	public short getShtCmpDesLen() {
		return this.shtCmpDesLen;
	}

	public void setShtCmpDesText(String shtCmpDesText) {
		this.shtCmpDesText = Functions.subString(shtCmpDesText, Len.SHT_CMP_DES_TEXT);
	}

	public String getShtCmpDesText() {
		return this.shtCmpDesText;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CMP_DES = 40;
		public static final int CMP_CD = 2;
		public static final int NAIC_CD = 5;
		public static final int NAIC_GRP_CD = 4;
		public static final int AM_BST_CMP_CD = 5;
		public static final int FED_TIN = 9;
		public static final int NCCI_CRR_CD = 5;
		public static final int NCCI_GRP_CD = 5;
		public static final int ISO_CMP_CD = 4;
		public static final int GL_BUS_UNT_CD = 5;
		public static final int SHT_CMP_DES_TEXT = 20;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
