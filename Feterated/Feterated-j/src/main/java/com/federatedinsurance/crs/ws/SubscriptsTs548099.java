/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsTs548099 {

	//==== PROPERTIES ====
	//Original name: SS-RA
	private short ra = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-RA-LIST
	private short raList = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-TEXT-START
	private short textStart = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setRa(short ra) {
		this.ra = ra;
	}

	public short getRa() {
		return this.ra;
	}

	public void setRaList(short raList) {
		this.raList = raList;
	}

	public short getRaList() {
		return this.raList;
	}

	public void setTextStart(short textStart) {
		this.textStart = textStart;
	}

	public short getTextStart() {
		return this.textStart;
	}
}
