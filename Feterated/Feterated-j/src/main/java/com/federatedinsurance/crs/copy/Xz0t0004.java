/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: XZ0T0004<br>
 * Copybook: XZ0T0004 from copybook XZ0T0004<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Xz0t0004 {

	//==== PROPERTIES ====
	//Original name: XZT004-SERVICE-INPUTS
	private Xzt004ServiceInputs xzt004ServiceInputs = new Xzt004ServiceInputs();
	//Original name: XZT04O-RESCIND-IND
	private char xzt04oRescindInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setXzt004ServiceOutputsBytes(byte[] buffer) {
		setXzt004ServiceOutputsBytes(buffer, 1);
	}

	public void setXzt004ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzt04oRescindInd = MarshalByte.readChar(buffer, position);
	}

	public byte[] getXzt004ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, xzt04oRescindInd);
		return buffer;
	}

	public void setXzt04oRescindInd(char xzt04oRescindInd) {
		this.xzt04oRescindInd = xzt04oRescindInd;
	}

	public char getXzt04oRescindInd() {
		return this.xzt04oRescindInd;
	}

	public Xzt004ServiceInputs getXzt004ServiceInputs() {
		return xzt004ServiceInputs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT04O_RESCIND_IND = 1;
		public static final int XZT004_SERVICE_OUTPUTS = XZT04O_RESCIND_IND;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
