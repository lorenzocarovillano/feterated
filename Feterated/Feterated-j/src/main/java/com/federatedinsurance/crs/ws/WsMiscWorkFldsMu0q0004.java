/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsActionCode;
import com.federatedinsurance.crs.ws.enums.WsBusinessObjectSwitch;
import com.federatedinsurance.crs.ws.enums.WsOperationName;

/**Original name: WS-MISC-WORK-FLDS<br>
 * Variable: WS-MISC-WORK-FLDS from program MU0Q0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsMiscWorkFldsMu0q0004 {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String programName = "MU0Q0004";
	//Original name: WS-ACTION-CODE
	private WsActionCode actionCode = new WsActionCode();
	//Original name: WS-OPERATION-NAME
	private WsOperationName operationName = new WsOperationName();
	//Original name: WS-PROGRAM-LINK-FAILED
	private WsProgramLinkFailed programLinkFailed = new WsProgramLinkFailed();
	//Original name: WS-INVALID-OPERATION-MSG
	private WsInvalidOperationMsg invalidOperationMsg = new WsInvalidOperationMsg();
	//Original name: WS-BUSINESS-OBJECT-SWITCH
	private WsBusinessObjectSwitch businessObjectSwitch = new WsBusinessObjectSwitch();

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public WsActionCode getActionCode() {
		return actionCode;
	}

	public WsBusinessObjectSwitch getBusinessObjectSwitch() {
		return businessObjectSwitch;
	}

	public WsInvalidOperationMsg getInvalidOperationMsg() {
		return invalidOperationMsg;
	}

	public WsOperationName getOperationName() {
		return operationName;
	}

	public WsProgramLinkFailed getProgramLinkFailed() {
		return programLinkFailed;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
