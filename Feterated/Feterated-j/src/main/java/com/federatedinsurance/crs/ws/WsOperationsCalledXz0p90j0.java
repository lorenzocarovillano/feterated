/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WS-OPERATIONS-CALLED<br>
 * Variable: WS-OPERATIONS-CALLED from program XZ0P90J0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsOperationsCalledXz0p90j0 {

	//==== PROPERTIES ====
	//Original name: WS-PRP-CAN-WRD
	private String prpCanWrd = "PrepareCancellationWording";
	//Original name: WS-PRP-CERT-LIST
	private String prpCertList = "PrepareCertHolderList";
	//Original name: WS-ATTACH-FORMS
	private String attachForms = "AttachForms";

	//==== METHODS ====
	public String getPrpCanWrd() {
		return this.prpCanWrd;
	}

	public String getPrpCertList() {
		return this.prpCertList;
	}

	public String getAttachForms() {
		return this.attachForms;
	}
}
