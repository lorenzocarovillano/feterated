/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IPmPlfV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [PM_PLF_V]
 * 
 */
public class PmPlfVDao extends BaseSqlDao<IPmPlfV> {

	public PmPlfVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IPmPlfV> getToClass() {
		return IPmPlfV.class;
	}

	public short selectRec(short dft) {
		return buildQuery("selectRec").scalarResultShort(dft);
	}
}
