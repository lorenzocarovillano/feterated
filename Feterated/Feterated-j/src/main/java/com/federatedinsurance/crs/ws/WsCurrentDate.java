/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-CURRENT-DATE<br>
 * Variable: WS-CURRENT-DATE from program XZC05090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsCurrentDate {

	//==== PROPERTIES ====
	//Original name: WS-CURRENT-DATE-YYYY
	private String yyyy = DefaultValues.stringVal(Len.YYYY);
	//Original name: WS-CURRENT-DATE-MM
	private String mm = DefaultValues.stringVal(Len.MM);
	//Original name: WS-CURRENT-DATE-DD
	private String dd = DefaultValues.stringVal(Len.DD);
	//Original name: FILLER-WS-CURRENT-DATE
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setCurrentDateFormatted(String data) {
		byte[] buffer = new byte[Len.CURRENT_DATE];
		MarshalByte.writeString(buffer, 1, data, Len.CURRENT_DATE);
		setCurrentDateBytes(buffer, 1);
	}

	public void setCurrentDateBytes(byte[] buffer, int offset) {
		int position = offset;
		yyyy = MarshalByte.readString(buffer, position, Len.YYYY);
		position += Len.YYYY;
		mm = MarshalByte.readString(buffer, position, Len.MM);
		position += Len.MM;
		dd = MarshalByte.readString(buffer, position, Len.DD);
		position += Len.DD;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public void setYyyy(String yyyy) {
		this.yyyy = Functions.subString(yyyy, Len.YYYY);
	}

	public String getYyyy() {
		return this.yyyy;
	}

	public String getYyyyFormatted() {
		return Functions.padBlanks(getYyyy(), Len.YYYY);
	}

	public void setMm(String mm) {
		this.mm = Functions.subString(mm, Len.MM);
	}

	public String getMm() {
		return this.mm;
	}

	public String getMmFormatted() {
		return Functions.padBlanks(getMm(), Len.MM);
	}

	public void setDd(String dd) {
		this.dd = Functions.subString(dd, Len.DD);
	}

	public String getDd() {
		return this.dd;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YYYY = 4;
		public static final int MM = 2;
		public static final int DD = 2;
		public static final int FLR1 = 13;
		public static final int CURRENT_DATE = YYYY + MM + DD + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
