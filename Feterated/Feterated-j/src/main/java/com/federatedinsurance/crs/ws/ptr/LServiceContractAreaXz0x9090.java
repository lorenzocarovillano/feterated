/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9090<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9090 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT99O_NOT_CO_ROW_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9090() {
	}

	public LServiceContractAreaXz0x9090(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt99iUserid(String xzt99iUserid) {
		writeString(Pos.XZT99I_USERID, xzt99iUserid, Len.XZT99I_USERID);
	}

	/**Original name: XZT99I-USERID<br>*/
	public String getXzt99iUserid() {
		return readString(Pos.XZT99I_USERID, Len.XZT99I_USERID);
	}

	public String getXzt99iUseridFormatted() {
		return Functions.padBlanks(getXzt99iUserid(), Len.XZT99I_USERID);
	}

	public void setXzt99oActNotTypCd(int xzt99oActNotTypCdIdx, String xzt99oActNotTypCd) {
		int position = Pos.xzt99oActNotTypCd(xzt99oActNotTypCdIdx - 1);
		writeString(position, xzt99oActNotTypCd, Len.XZT99O_ACT_NOT_TYP_CD);
	}

	/**Original name: XZT99O-ACT-NOT-TYP-CD<br>*/
	public String getXzt99oActNotTypCd(int xzt99oActNotTypCdIdx) {
		int position = Pos.xzt99oActNotTypCd(xzt99oActNotTypCdIdx - 1);
		return readString(position, Len.XZT99O_ACT_NOT_TYP_CD);
	}

	public void setXzt99oCoCd(int xzt99oCoCdIdx, String xzt99oCoCd) {
		int position = Pos.xzt99oCoCd(xzt99oCoCdIdx - 1);
		writeString(position, xzt99oCoCd, Len.XZT99O_CO_CD);
	}

	/**Original name: XZT99O-CO-CD<br>*/
	public String getXzt99oCoCd(int xzt99oCoCdIdx) {
		int position = Pos.xzt99oCoCd(xzt99oCoCdIdx - 1);
		return readString(position, Len.XZT99O_CO_CD);
	}

	public void setXzt99oCoDes(int xzt99oCoDesIdx, String xzt99oCoDes) {
		int position = Pos.xzt99oCoDes(xzt99oCoDesIdx - 1);
		writeString(position, xzt99oCoDes, Len.XZT99O_CO_DES);
	}

	/**Original name: XZT99O-CO-DES<br>*/
	public String getXzt99oCoDes(int xzt99oCoDesIdx) {
		int position = Pos.xzt99oCoDes(xzt99oCoDesIdx - 1);
		return readString(position, Len.XZT99O_CO_DES);
	}

	public void setXzt99oSpeCrtCd(int xzt99oSpeCrtCdIdx, String xzt99oSpeCrtCd) {
		int position = Pos.xzt99oSpeCrtCd(xzt99oSpeCrtCdIdx - 1);
		writeString(position, xzt99oSpeCrtCd, Len.XZT99O_SPE_CRT_CD);
	}

	/**Original name: XZT99O-SPE-CRT-CD<br>*/
	public String getXzt99oSpeCrtCd(int xzt99oSpeCrtCdIdx) {
		int position = Pos.xzt99oSpeCrtCd(xzt99oSpeCrtCdIdx - 1);
		return readString(position, Len.XZT99O_SPE_CRT_CD);
	}

	public void setXzt99oCoOfsNbr(int xzt99oCoOfsNbrIdx, int xzt99oCoOfsNbr) {
		int position = Pos.xzt99oCoOfsNbr(xzt99oCoOfsNbrIdx - 1);
		writeInt(position, xzt99oCoOfsNbr, Len.Int.XZT99O_CO_OFS_NBR);
	}

	/**Original name: XZT99O-CO-OFS-NBR<br>*/
	public int getXzt99oCoOfsNbr(int xzt99oCoOfsNbrIdx) {
		int position = Pos.xzt99oCoOfsNbr(xzt99oCoOfsNbrIdx - 1);
		return readNumDispInt(position, Len.XZT99O_CO_OFS_NBR);
	}

	public void setXzt99oDtbCd(int xzt99oDtbCdIdx, char xzt99oDtbCd) {
		int position = Pos.xzt99oDtbCd(xzt99oDtbCdIdx - 1);
		writeChar(position, xzt99oDtbCd);
	}

	/**Original name: XZT99O-DTB-CD<br>*/
	public char getXzt99oDtbCd(int xzt99oDtbCdIdx) {
		int position = Pos.xzt99oDtbCd(xzt99oDtbCdIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT909_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT99I_USERID = XZT909_SERVICE_INPUTS;
		public static final int XZT909_SERVICE_OUTPUTS = XZT99I_USERID + Len.XZT99I_USERID;
		public static final int XZT99O_NOT_CO_ROW_TBL = XZT909_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt99oNotCoRow(int idx) {
			return XZT99O_NOT_CO_ROW_TBL + idx * Len.XZT99O_NOT_CO_ROW;
		}

		public static int xzt99oActNotTypCd(int idx) {
			return xzt99oNotCoRow(idx);
		}

		public static int xzt99oCoCd(int idx) {
			return xzt99oActNotTypCd(idx) + Len.XZT99O_ACT_NOT_TYP_CD;
		}

		public static int xzt99oCoDes(int idx) {
			return xzt99oCoCd(idx) + Len.XZT99O_CO_CD;
		}

		public static int xzt99oSpeCrtCd(int idx) {
			return xzt99oCoDes(idx) + Len.XZT99O_CO_DES;
		}

		public static int xzt99oCoOfsNbr(int idx) {
			return xzt99oSpeCrtCd(idx) + Len.XZT99O_SPE_CRT_CD;
		}

		public static int xzt99oDtbCd(int idx) {
			return xzt99oCoOfsNbr(idx) + Len.XZT99O_CO_OFS_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT99I_USERID = 8;
		public static final int XZT99O_ACT_NOT_TYP_CD = 5;
		public static final int XZT99O_CO_CD = 5;
		public static final int XZT99O_CO_DES = 30;
		public static final int XZT99O_SPE_CRT_CD = 5;
		public static final int XZT99O_CO_OFS_NBR = 5;
		public static final int XZT99O_DTB_CD = 1;
		public static final int XZT99O_NOT_CO_ROW = XZT99O_ACT_NOT_TYP_CD + XZT99O_CO_CD + XZT99O_CO_DES + XZT99O_SPE_CRT_CD + XZT99O_CO_OFS_NBR
				+ XZT99O_DTB_CD;
		public static final int XZT909_SERVICE_INPUTS = XZT99I_USERID;
		public static final int XZT99O_NOT_CO_ROW_TBL = LServiceContractAreaXz0x9090.XZT99O_NOT_CO_ROW_MAXOCCURS * XZT99O_NOT_CO_ROW;
		public static final int XZT909_SERVICE_OUTPUTS = XZT99O_NOT_CO_ROW_TBL;
		public static final int L_SERVICE_CONTRACT_AREA = XZT909_SERVICE_INPUTS + XZT909_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT99O_CO_OFS_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
