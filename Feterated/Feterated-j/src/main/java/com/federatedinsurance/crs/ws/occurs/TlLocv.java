/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: TL-LOCV<br>
 * Variables: TL-LOCV from program XZC06090<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TlLocv {

	//==== PROPERTIES ====
	//Original name: TL-LOCV-CD
	private String tlLocvCd = DefaultValues.stringVal(Len.TL_LOCV_CD);
	//Original name: FILLER-TL-LOCV
	private char flr1 = Types.SPACE_CHAR;

	//==== METHODS ====
	public byte[] getTlLocvBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tlLocvCd, Len.TL_LOCV_CD);
		position += Len.TL_LOCV_CD;
		MarshalByte.writeChar(buffer, position, flr1);
		return buffer;
	}

	public void initTlLocvSpaces() {
		tlLocvCd = "";
		flr1 = Types.SPACE_CHAR;
	}

	public void setTlLocvCd(String tlLocvCd) {
		this.tlLocvCd = Functions.subString(tlLocvCd, Len.TL_LOCV_CD);
	}

	public String getTlLocvCd() {
		return this.tlLocvCd;
	}

	public char getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TL_LOCV_CD = 3;
		public static final int FLR1 = 1;
		public static final int TL_LOCV = TL_LOCV_CD + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
