/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-06-OPEN-MESSAGE<br>
 * Variable: EA-06-OPEN-MESSAGE from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea06OpenMessage {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-06-OPEN-MESSAGE
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-06-OPEN-MESSAGE-1
	private String flr2 = "TS030099 -";
	//Original name: FILLER-EA-06-OPEN-MESSAGE-2
	private String flr3 = "REPORT NUMBER";
	//Original name: FILLER-EA-06-OPEN-MESSAGE-3
	private char flr4 = Types.QUOTE_CHAR;
	//Original name: EA-06-REPORT-NMBR-DD-NAME
	private String ea06ReportNmbrDdName = DefaultValues.stringVal(Len.EA06_REPORT_NMBR_DD_NAME);
	//Original name: FILLER-EA-06-OPEN-MESSAGE-4
	private char flr5 = Types.QUOTE_CHAR;
	//Original name: FILLER-EA-06-OPEN-MESSAGE-5
	private String flr6 = " IS PERFORMING";
	//Original name: FILLER-EA-06-OPEN-MESSAGE-6
	private String flr7 = "EXCESSIVE OPENS";
	//Original name: FILLER-EA-06-OPEN-MESSAGE-7
	private String flr8 = " FOR A DD.";

	//==== METHODS ====
	public String getEa06OpenMessageFormatted() {
		return MarshalByteExt.bufferToStr(getEa06OpenMessageBytes());
	}

	public byte[] getEa06OpenMessageBytes() {
		byte[] buffer = new byte[Len.EA06_OPEN_MESSAGE];
		return getEa06OpenMessageBytes(buffer, 1);
	}

	public byte[] getEa06OpenMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeChar(buffer, position, flr4);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ea06ReportNmbrDdName, Len.EA06_REPORT_NMBR_DD_NAME);
		position += Len.EA06_REPORT_NMBR_DD_NAME;
		MarshalByte.writeChar(buffer, position, flr5);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public char getFlr4() {
		return this.flr4;
	}

	public void setEa06ReportNmbrDdName(String ea06ReportNmbrDdName) {
		this.ea06ReportNmbrDdName = Functions.subString(ea06ReportNmbrDdName, Len.EA06_REPORT_NMBR_DD_NAME);
	}

	public String getEa06ReportNmbrDdName() {
		return this.ea06ReportNmbrDdName;
	}

	public char getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA06_REPORT_NMBR_DD_NAME = 8;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 14;
		public static final int FLR6 = 15;
		public static final int FLR8 = 10;
		public static final int EA06_OPEN_MESSAGE = EA06_REPORT_NMBR_DD_NAME + 3 * FLR1 + FLR2 + FLR3 + 2 * FLR6 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
