/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9020<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9020 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9021_MAXOCCURS = 750;
	public static final String XZA920_GET_REC_LIST_BY_FRM = "GetRecipientListByFormSeqNbr";
	public static final String XZA920_GET_REC_LIST_BY_NOT = "GetRecipientListByNotification";

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9020() {
	}

	public LFrameworkResponseAreaXz0r9020(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza920MaxRecRows(short xza920MaxRecRows) {
		writeBinaryShort(Pos.XZA920_MAX_REC_ROWS, xza920MaxRecRows);
	}

	/**Original name: XZA920-MAX-REC-ROWS<br>*/
	public short getXza920MaxRecRows() {
		return readBinaryShort(Pos.XZA920_MAX_REC_ROWS);
	}

	public void setXza920Operation(String xza920Operation) {
		writeString(Pos.XZA920_OPERATION, xza920Operation, Len.XZA920_OPERATION);
	}

	/**Original name: XZA920-OPERATION<br>*/
	public String getXza920Operation() {
		return readString(Pos.XZA920_OPERATION, Len.XZA920_OPERATION);
	}

	public void setXza920CsrActNbr(String xza920CsrActNbr) {
		writeString(Pos.XZA920_CSR_ACT_NBR, xza920CsrActNbr, Len.XZA920_CSR_ACT_NBR);
	}

	/**Original name: XZA920-CSR-ACT-NBR<br>*/
	public String getXza920CsrActNbr() {
		return readString(Pos.XZA920_CSR_ACT_NBR, Len.XZA920_CSR_ACT_NBR);
	}

	public void setXza920NotPrcTs(String xza920NotPrcTs) {
		writeString(Pos.XZA920_NOT_PRC_TS, xza920NotPrcTs, Len.XZA920_NOT_PRC_TS);
	}

	/**Original name: XZA920-NOT-PRC-TS<br>*/
	public String getXza920NotPrcTs() {
		return readString(Pos.XZA920_NOT_PRC_TS, Len.XZA920_NOT_PRC_TS);
	}

	public void setXza920FrmSeqNbr(int xza920FrmSeqNbr) {
		writeInt(Pos.XZA920_FRM_SEQ_NBR, xza920FrmSeqNbr, Len.Int.XZA920_FRM_SEQ_NBR);
	}

	/**Original name: XZA920-FRM-SEQ-NBR<br>*/
	public int getXza920FrmSeqNbr() {
		return readNumDispInt(Pos.XZA920_FRM_SEQ_NBR, Len.XZA920_FRM_SEQ_NBR);
	}

	public void setXza920RecallRecSeqNbr(int xza920RecallRecSeqNbr) {
		writeInt(Pos.XZA920_RECALL_REC_SEQ_NBR, xza920RecallRecSeqNbr, Len.Int.XZA920_RECALL_REC_SEQ_NBR);
	}

	/**Original name: XZA920-RECALL-REC-SEQ-NBR<br>*/
	public int getXza920RecallRecSeqNbr() {
		return readNumDispInt(Pos.XZA920_RECALL_REC_SEQ_NBR, Len.XZA920_RECALL_REC_SEQ_NBR);
	}

	public void setXza920Userid(String xza920Userid) {
		writeString(Pos.XZA920_USERID, xza920Userid, Len.XZA920_USERID);
	}

	/**Original name: XZA920-USERID<br>*/
	public String getXza920Userid() {
		return readString(Pos.XZA920_USERID, Len.XZA920_USERID);
	}

	public String getlFwRespXz0a9021Formatted(int lFwRespXz0a9021Idx) {
		int position = Pos.lFwRespXz0a9021(lFwRespXz0a9021Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9021);
	}

	public void setXza921rRecSortOrdNbr(int xza921rRecSortOrdNbrIdx, int xza921rRecSortOrdNbr) {
		int position = Pos.xza921RecSortOrdNbr(xza921rRecSortOrdNbrIdx - 1);
		writeInt(position, xza921rRecSortOrdNbr, Len.Int.XZA921R_REC_SORT_ORD_NBR);
	}

	/**Original name: XZA921R-REC-SORT-ORD-NBR<br>*/
	public int getXza921rRecSortOrdNbr(int xza921rRecSortOrdNbrIdx) {
		int position = Pos.xza921RecSortOrdNbr(xza921rRecSortOrdNbrIdx - 1);
		return readNumDispInt(position, Len.XZA921_REC_SORT_ORD_NBR);
	}

	public void setXza921rRecSeqNbr(int xza921rRecSeqNbrIdx, int xza921rRecSeqNbr) {
		int position = Pos.xza921RecSeqNbr(xza921rRecSeqNbrIdx - 1);
		writeInt(position, xza921rRecSeqNbr, Len.Int.XZA921R_REC_SEQ_NBR);
	}

	/**Original name: XZA921R-REC-SEQ-NBR<br>*/
	public int getXza921rRecSeqNbr(int xza921rRecSeqNbrIdx) {
		int position = Pos.xza921RecSeqNbr(xza921rRecSeqNbrIdx - 1);
		return readNumDispInt(position, Len.XZA921_REC_SEQ_NBR);
	}

	public void setXza921rRecTypCd(int xza921rRecTypCdIdx, String xza921rRecTypCd) {
		int position = Pos.xza921RecTypCd(xza921rRecTypCdIdx - 1);
		writeString(position, xza921rRecTypCd, Len.XZA921_REC_TYP_CD);
	}

	/**Original name: XZA921R-REC-TYP-CD<br>*/
	public String getXza921rRecTypCd(int xza921rRecTypCdIdx) {
		int position = Pos.xza921RecTypCd(xza921rRecTypCdIdx - 1);
		return readString(position, Len.XZA921_REC_TYP_CD);
	}

	public void setXza921rRecTypDes(int xza921rRecTypDesIdx, String xza921rRecTypDes) {
		int position = Pos.xza921RecTypDes(xza921rRecTypDesIdx - 1);
		writeString(position, xza921rRecTypDes, Len.XZA921_REC_TYP_DES);
	}

	/**Original name: XZA921R-REC-TYP-DES<br>*/
	public String getXza921rRecTypDes(int xza921rRecTypDesIdx) {
		int position = Pos.xza921RecTypDes(xza921rRecTypDesIdx - 1);
		return readString(position, Len.XZA921_REC_TYP_DES);
	}

	public void setXza921rCerNbr(int xza921rCerNbrIdx, String xza921rCerNbr) {
		int position = Pos.xza921CerNbr(xza921rCerNbrIdx - 1);
		writeString(position, xza921rCerNbr, Len.XZA921_CER_NBR);
	}

	/**Original name: XZA921R-CER-NBR<br>*/
	public String getXza921rCerNbr(int xza921rCerNbrIdx) {
		int position = Pos.xza921CerNbr(xza921rCerNbrIdx - 1);
		return readString(position, Len.XZA921_CER_NBR);
	}

	public void setXza921rNmAdrLin1(int xza921rNmAdrLin1Idx, String xza921rNmAdrLin1) {
		int position = Pos.xza921NmAdrLin1(xza921rNmAdrLin1Idx - 1);
		writeString(position, xza921rNmAdrLin1, Len.XZA921_NM_ADR_LIN1);
	}

	/**Original name: XZA921R-NM-ADR-LIN-1<br>*/
	public String getXza921rNmAdrLin1(int xza921rNmAdrLin1Idx) {
		int position = Pos.xza921NmAdrLin1(xza921rNmAdrLin1Idx - 1);
		return readString(position, Len.XZA921_NM_ADR_LIN1);
	}

	public void setXza921rNmAdrLin2(int xza921rNmAdrLin2Idx, String xza921rNmAdrLin2) {
		int position = Pos.xza921NmAdrLin2(xza921rNmAdrLin2Idx - 1);
		writeString(position, xza921rNmAdrLin2, Len.XZA921_NM_ADR_LIN2);
	}

	/**Original name: XZA921R-NM-ADR-LIN-2<br>*/
	public String getXza921rNmAdrLin2(int xza921rNmAdrLin2Idx) {
		int position = Pos.xza921NmAdrLin2(xza921rNmAdrLin2Idx - 1);
		return readString(position, Len.XZA921_NM_ADR_LIN2);
	}

	public void setXza921rNmAdrLin3(int xza921rNmAdrLin3Idx, String xza921rNmAdrLin3) {
		int position = Pos.xza921NmAdrLin3(xza921rNmAdrLin3Idx - 1);
		writeString(position, xza921rNmAdrLin3, Len.XZA921_NM_ADR_LIN3);
	}

	/**Original name: XZA921R-NM-ADR-LIN-3<br>*/
	public String getXza921rNmAdrLin3(int xza921rNmAdrLin3Idx) {
		int position = Pos.xza921NmAdrLin3(xza921rNmAdrLin3Idx - 1);
		return readString(position, Len.XZA921_NM_ADR_LIN3);
	}

	public void setXza921rNmAdrLin4(int xza921rNmAdrLin4Idx, String xza921rNmAdrLin4) {
		int position = Pos.xza921NmAdrLin4(xza921rNmAdrLin4Idx - 1);
		writeString(position, xza921rNmAdrLin4, Len.XZA921_NM_ADR_LIN4);
	}

	/**Original name: XZA921R-NM-ADR-LIN-4<br>*/
	public String getXza921rNmAdrLin4(int xza921rNmAdrLin4Idx) {
		int position = Pos.xza921NmAdrLin4(xza921rNmAdrLin4Idx - 1);
		return readString(position, Len.XZA921_NM_ADR_LIN4);
	}

	public void setXza921rNmAdrLin5(int xza921rNmAdrLin5Idx, String xza921rNmAdrLin5) {
		int position = Pos.xza921NmAdrLin5(xza921rNmAdrLin5Idx - 1);
		writeString(position, xza921rNmAdrLin5, Len.XZA921_NM_ADR_LIN5);
	}

	/**Original name: XZA921R-NM-ADR-LIN-5<br>*/
	public String getXza921rNmAdrLin5(int xza921rNmAdrLin5Idx) {
		int position = Pos.xza921NmAdrLin5(xza921rNmAdrLin5Idx - 1);
		return readString(position, Len.XZA921_NM_ADR_LIN5);
	}

	public void setXza921rNmAdrLin6(int xza921rNmAdrLin6Idx, String xza921rNmAdrLin6) {
		int position = Pos.xza921NmAdrLin6(xza921rNmAdrLin6Idx - 1);
		writeString(position, xza921rNmAdrLin6, Len.XZA921_NM_ADR_LIN6);
	}

	/**Original name: XZA921R-NM-ADR-LIN-6<br>*/
	public String getXza921rNmAdrLin6(int xza921rNmAdrLin6Idx) {
		int position = Pos.xza921NmAdrLin6(xza921rNmAdrLin6Idx - 1);
		return readString(position, Len.XZA921_NM_ADR_LIN6);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_XZ0A9020 = L_FRAMEWORK_RESPONSE_AREA;
		public static final int XZA920_GET_REC_LIST_KEY = L_FW_RESP_XZ0A9020;
		public static final int XZA920_MAX_REC_ROWS = XZA920_GET_REC_LIST_KEY;
		public static final int XZA920_OPERATION = XZA920_MAX_REC_ROWS + Len.XZA920_MAX_REC_ROWS;
		public static final int XZA920_CSR_ACT_NBR = XZA920_OPERATION + Len.XZA920_OPERATION;
		public static final int XZA920_NOT_PRC_TS = XZA920_CSR_ACT_NBR + Len.XZA920_CSR_ACT_NBR;
		public static final int XZA920_FRM_SEQ_NBR = XZA920_NOT_PRC_TS + Len.XZA920_NOT_PRC_TS;
		public static final int XZA920_RECALL_REC_SEQ_NBR = XZA920_FRM_SEQ_NBR + Len.XZA920_FRM_SEQ_NBR;
		public static final int XZA920_USERID = XZA920_RECALL_REC_SEQ_NBR + Len.XZA920_RECALL_REC_SEQ_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9021(int idx) {
			return XZA920_USERID + Len.XZA920_USERID + idx * Len.L_FW_RESP_XZ0A9021;
		}

		public static int xza921GetRecListDetail(int idx) {
			return lFwRespXz0a9021(idx);
		}

		public static int xza921RecSortOrdNbr(int idx) {
			return xza921GetRecListDetail(idx);
		}

		public static int xza921RecSeqNbr(int idx) {
			return xza921RecSortOrdNbr(idx) + Len.XZA921_REC_SORT_ORD_NBR;
		}

		public static int xza921RecTypCd(int idx) {
			return xza921RecSeqNbr(idx) + Len.XZA921_REC_SEQ_NBR;
		}

		public static int xza921RecTypDes(int idx) {
			return xza921RecTypCd(idx) + Len.XZA921_REC_TYP_CD;
		}

		public static int xza921CerNbr(int idx) {
			return xza921RecTypDes(idx) + Len.XZA921_REC_TYP_DES;
		}

		public static int xza921NmAdrLin1(int idx) {
			return xza921CerNbr(idx) + Len.XZA921_CER_NBR;
		}

		public static int xza921NmAdrLin2(int idx) {
			return xza921NmAdrLin1(idx) + Len.XZA921_NM_ADR_LIN1;
		}

		public static int xza921NmAdrLin3(int idx) {
			return xza921NmAdrLin2(idx) + Len.XZA921_NM_ADR_LIN2;
		}

		public static int xza921NmAdrLin4(int idx) {
			return xza921NmAdrLin3(idx) + Len.XZA921_NM_ADR_LIN3;
		}

		public static int xza921NmAdrLin5(int idx) {
			return xza921NmAdrLin4(idx) + Len.XZA921_NM_ADR_LIN4;
		}

		public static int xza921NmAdrLin6(int idx) {
			return xza921NmAdrLin5(idx) + Len.XZA921_NM_ADR_LIN5;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA920_MAX_REC_ROWS = 2;
		public static final int XZA920_OPERATION = 32;
		public static final int XZA920_CSR_ACT_NBR = 9;
		public static final int XZA920_NOT_PRC_TS = 26;
		public static final int XZA920_FRM_SEQ_NBR = 5;
		public static final int XZA920_RECALL_REC_SEQ_NBR = 5;
		public static final int XZA920_USERID = 8;
		public static final int XZA921_REC_SORT_ORD_NBR = 5;
		public static final int XZA921_REC_SEQ_NBR = 5;
		public static final int XZA921_REC_TYP_CD = 5;
		public static final int XZA921_REC_TYP_DES = 30;
		public static final int XZA921_CER_NBR = 25;
		public static final int XZA921_NM_ADR_LIN1 = 45;
		public static final int XZA921_NM_ADR_LIN2 = 45;
		public static final int XZA921_NM_ADR_LIN3 = 45;
		public static final int XZA921_NM_ADR_LIN4 = 45;
		public static final int XZA921_NM_ADR_LIN5 = 45;
		public static final int XZA921_NM_ADR_LIN6 = 45;
		public static final int XZA921_GET_REC_LIST_DETAIL = XZA921_REC_SORT_ORD_NBR + XZA921_REC_SEQ_NBR + XZA921_REC_TYP_CD + XZA921_REC_TYP_DES
				+ XZA921_CER_NBR + XZA921_NM_ADR_LIN1 + XZA921_NM_ADR_LIN2 + XZA921_NM_ADR_LIN3 + XZA921_NM_ADR_LIN4 + XZA921_NM_ADR_LIN5
				+ XZA921_NM_ADR_LIN6;
		public static final int L_FW_RESP_XZ0A9021 = XZA921_GET_REC_LIST_DETAIL;
		public static final int XZA920_GET_REC_LIST_KEY = XZA920_MAX_REC_ROWS + XZA920_OPERATION + XZA920_CSR_ACT_NBR + XZA920_NOT_PRC_TS
				+ XZA920_FRM_SEQ_NBR + XZA920_RECALL_REC_SEQ_NBR + XZA920_USERID;
		public static final int L_FW_RESP_XZ0A9020 = XZA920_GET_REC_LIST_KEY;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_XZ0A9020
				+ LFrameworkResponseAreaXz0r9020.L_FW_RESP_XZ0A9021_MAXOCCURS * L_FW_RESP_XZ0A9021;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA920_FRM_SEQ_NBR = 5;
			public static final int XZA920_RECALL_REC_SEQ_NBR = 5;
			public static final int XZA921R_REC_SORT_ORD_NBR = 5;
			public static final int XZA921R_REC_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
