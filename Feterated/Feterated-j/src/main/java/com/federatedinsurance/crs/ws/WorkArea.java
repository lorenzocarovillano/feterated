/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WORK-AREA<br>
 * Variable: WORK-AREA from program TS020000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkArea {

	//==== PROPERTIES ====
	//Original name: WA-RESPONSE-CODE
	private int waResponseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WA-RESPONSE-CODE2
	private int waResponseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WA-LAST-UOW-SEQ-NBR
	private short waLastUowSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: WA-COMM-LENGTH
	private short waCommLength = DefaultValues.BIN_SHORT_VAL;
	/**Original name: WA-NBR-RESP-HDR-ROWS-READ<br>
	 * <pre>* UMT RESPONSE PROCESSING VARIABLES.</pre>*/
	private int waNbrRespHdrRowsRead = 0;
	//Original name: WA-NBR-RESP-DATA-ROWS-READ
	private int waNbrRespDataRowsRead = 0;
	//Original name: WA-NBR-RESP-WRN-ROWS-READ
	private int waNbrRespWrnRowsRead = 0;
	//Original name: WA-NBR-RESP-NBE-ROWS-READ
	private int waNbrRespNbeRowsRead = 0;
	/**Original name: WA-PROGRAM-NAME<br>
	 * <pre>* GENERAL PROGRAM VARIABLES.</pre>*/
	private String waProgramName = "TS020000";
	//Original name: WA-APPLID
	private String waApplid = DefaultValues.stringVal(Len.WA_APPLID);
	//Original name: WA-MESSAGE-ID
	private String waMessageId = DefaultValues.stringVal(Len.WA_MESSAGE_ID);
	/**Original name: WA-UOW-LEVEL-TSQNAME<br>
	 * <pre>* UOW-LEVEL APP-SPECIFIC TSQ DELETION</pre>*/
	private String waUowLevelTsqname = DefaultValues.stringVal(Len.WA_UOW_LEVEL_TSQNAME);
	/**Original name: WA-BAD-HDR<br>
	 * <pre>* LINKAGE CORRUPTION CHECK</pre>*/
	private String waBadHdr = "";
	//Original name: WA-BAD-DATA
	private String waBadData = "";
	//Original name: WA-BAD-NLBE
	private String waBadNlbe = "";
	//Original name: WA-BAD-WRNS
	private String waBadWrns = "";
	//Original name: WA-BAD-LOGPROB
	private String waBadLogprob = "";
	//Original name: WA-BUS-OBJ-NM
	private String waBusObjNm = "";
	//Original name: WA-BUS-OBJ-MDU
	private String waBusObjMdu = "";
	//Original name: WA-FORMATTED-DATE
	private WaFormattedDate waFormattedDate = new WaFormattedDate();
	//Original name: WA-TRANS-DATA-QUE-INFO
	private WaTransDataQueInfo waTransDataQueInfo = new WaTransDataQueInfo();
	//Original name: WA-RANDOM-NUMBER1
	private AfDecimal waRandomNumber1 = new AfDecimal(DefaultValues.DEC_VAL, 8, 8);
	//Original name: WA-RANDOM-NUMBER2
	private AfDecimal waRandomNumber2 = new AfDecimal(DefaultValues.DEC_VAL, 8, 8);
	//Original name: WA-RANDOM-NUMBER3
	private AfDecimal waRandomNumber3 = new AfDecimal(DefaultValues.DEC_VAL, 8, 8);
	//Original name: WA-RANDOM-NUMBER4
	private String waRandomNumber4 = DefaultValues.stringVal(Len.WA_RANDOM_NUMBER4);
	//Original name: WA-CURRENT-DATE
	private String waCurrentDate = DefaultValues.stringVal(Len.WA_CURRENT_DATE);
	//Original name: WA-SEED
	private String waSeed = DefaultValues.stringVal(Len.WA_SEED);
	//Original name: FILLER-WORK-AREA
	private FillerWorkArea fillerWorkArea = new FillerWorkArea();

	//==== METHODS ====
	public void setWaResponseCode(int waResponseCode) {
		this.waResponseCode = waResponseCode;
	}

	public int getWaResponseCode() {
		return this.waResponseCode;
	}

	public void setWaResponseCode2(int waResponseCode2) {
		this.waResponseCode2 = waResponseCode2;
	}

	public int getWaResponseCode2() {
		return this.waResponseCode2;
	}

	public void setWaLastUowSeqNbr(short waLastUowSeqNbr) {
		this.waLastUowSeqNbr = waLastUowSeqNbr;
	}

	public short getWaLastUowSeqNbr() {
		return this.waLastUowSeqNbr;
	}

	public void setWaCommLength(short waCommLength) {
		this.waCommLength = waCommLength;
	}

	public short getWaCommLength() {
		return this.waCommLength;
	}

	public void setWaNbrRespHdrRowsRead(int waNbrRespHdrRowsRead) {
		this.waNbrRespHdrRowsRead = waNbrRespHdrRowsRead;
	}

	public int getWaNbrRespHdrRowsRead() {
		return this.waNbrRespHdrRowsRead;
	}

	public void setWaNbrRespDataRowsRead(int waNbrRespDataRowsRead) {
		this.waNbrRespDataRowsRead = waNbrRespDataRowsRead;
	}

	public int getWaNbrRespDataRowsRead() {
		return this.waNbrRespDataRowsRead;
	}

	public void setWaNbrRespWrnRowsRead(int waNbrRespWrnRowsRead) {
		this.waNbrRespWrnRowsRead = waNbrRespWrnRowsRead;
	}

	public int getWaNbrRespWrnRowsRead() {
		return this.waNbrRespWrnRowsRead;
	}

	public void setWaNbrRespNbeRowsRead(int waNbrRespNbeRowsRead) {
		this.waNbrRespNbeRowsRead = waNbrRespNbeRowsRead;
	}

	public int getWaNbrRespNbeRowsRead() {
		return this.waNbrRespNbeRowsRead;
	}

	public String getWaProgramName() {
		return this.waProgramName;
	}

	public void setWaApplid(String waApplid) {
		this.waApplid = Functions.subString(waApplid, Len.WA_APPLID);
	}

	public String getWaApplid() {
		return this.waApplid;
	}

	public void setWaMessageId(String waMessageId) {
		this.waMessageId = Functions.subString(waMessageId, Len.WA_MESSAGE_ID);
	}

	public String getWaMessageId() {
		return this.waMessageId;
	}

	public String getWaMessageIdFormatted() {
		return Functions.padBlanks(getWaMessageId(), Len.WA_MESSAGE_ID);
	}

	public void setWaUowLevelTsqname(String waUowLevelTsqname) {
		this.waUowLevelTsqname = Functions.subString(waUowLevelTsqname, Len.WA_UOW_LEVEL_TSQNAME);
	}

	public String getWaUowLevelTsqname() {
		return this.waUowLevelTsqname;
	}

	public String getWaUowLevelTsqnameFormatted() {
		return Functions.padBlanks(getWaUowLevelTsqname(), Len.WA_UOW_LEVEL_TSQNAME);
	}

	public void setWaBadHdr(String waBadHdr) {
		this.waBadHdr = Functions.subString(waBadHdr, Len.WA_BAD_HDR);
	}

	public String getWaBadHdr() {
		return this.waBadHdr;
	}

	public String getWaBadHdrFormatted() {
		return Functions.padBlanks(getWaBadHdr(), Len.WA_BAD_HDR);
	}

	public void setWaBadData(String waBadData) {
		this.waBadData = Functions.subString(waBadData, Len.WA_BAD_DATA);
	}

	public String getWaBadData() {
		return this.waBadData;
	}

	public String getWaBadDataFormatted() {
		return Functions.padBlanks(getWaBadData(), Len.WA_BAD_DATA);
	}

	public void setWaBadNlbe(String waBadNlbe) {
		this.waBadNlbe = Functions.subString(waBadNlbe, Len.WA_BAD_NLBE);
	}

	public String getWaBadNlbe() {
		return this.waBadNlbe;
	}

	public String getWaBadNlbeFormatted() {
		return Functions.padBlanks(getWaBadNlbe(), Len.WA_BAD_NLBE);
	}

	public void setWaBadWrns(String waBadWrns) {
		this.waBadWrns = Functions.subString(waBadWrns, Len.WA_BAD_WRNS);
	}

	public String getWaBadWrns() {
		return this.waBadWrns;
	}

	public String getWaBadWrnsFormatted() {
		return Functions.padBlanks(getWaBadWrns(), Len.WA_BAD_WRNS);
	}

	public void setWaBadLogprob(String waBadLogprob) {
		this.waBadLogprob = Functions.subString(waBadLogprob, Len.WA_BAD_LOGPROB);
	}

	public String getWaBadLogprob() {
		return this.waBadLogprob;
	}

	public String getWaBadLogprobFormatted() {
		return Functions.padBlanks(getWaBadLogprob(), Len.WA_BAD_LOGPROB);
	}

	public void setWaBusObjNm(String waBusObjNm) {
		this.waBusObjNm = Functions.subString(waBusObjNm, Len.WA_BUS_OBJ_NM);
	}

	public String getWaBusObjNm() {
		return this.waBusObjNm;
	}

	public void setWaBusObjMdu(String waBusObjMdu) {
		this.waBusObjMdu = Functions.subString(waBusObjMdu, Len.WA_BUS_OBJ_MDU);
	}

	public String getWaBusObjMdu() {
		return this.waBusObjMdu;
	}

	public String getWaRandomNumber1XFormatted() {
		return getWaRandomNumber1Formatted();
	}

	public void setWaRandomNumber1(AfDecimal waRandomNumber1) {
		this.waRandomNumber1.assign(waRandomNumber1);
	}

	public AfDecimal getWaRandomNumber1() {
		return this.waRandomNumber1.copy();
	}

	public String getWaRandomNumber1Formatted() {
		return PicFormatter.display("V9(8)").format(getWaRandomNumber1()).toString();
	}

	public String getWaRandomNumber2XFormatted() {
		return getWaRandomNumber2Formatted();
	}

	public void setWaRandomNumber2(AfDecimal waRandomNumber2) {
		this.waRandomNumber2.assign(waRandomNumber2);
	}

	public AfDecimal getWaRandomNumber2() {
		return this.waRandomNumber2.copy();
	}

	public String getWaRandomNumber2Formatted() {
		return PicFormatter.display("V9(8)").format(getWaRandomNumber2()).toString();
	}

	public String getWaRandomNumber3XFormatted() {
		return getWaRandomNumber3Formatted();
	}

	public void setWaRandomNumber3(AfDecimal waRandomNumber3) {
		this.waRandomNumber3.assign(waRandomNumber3);
	}

	public AfDecimal getWaRandomNumber3() {
		return this.waRandomNumber3.copy();
	}

	public String getWaRandomNumber3Formatted() {
		return PicFormatter.display("V9(8)").format(getWaRandomNumber3()).toString();
	}

	public String getWaRandomNumber4XFormatted() {
		return getWaRandomNumber4Formatted();
	}

	public void setWaRandomNumber4(int waRandomNumber4) {
		this.waRandomNumber4 = NumericDisplay.asString(waRandomNumber4, Len.WA_RANDOM_NUMBER4);
	}

	public int getWaRandomNumber4() {
		return NumericDisplay.asInt(this.waRandomNumber4);
	}

	public String getWaRandomNumber4Formatted() {
		return this.waRandomNumber4;
	}

	public void setWaCurrentDate(String waCurrentDate) {
		this.waCurrentDate = Functions.subString(waCurrentDate, Len.WA_CURRENT_DATE);
	}

	public String getWaCurrentDate() {
		return this.waCurrentDate;
	}

	public String getWaCurrentDateFormatted() {
		return Functions.padBlanks(getWaCurrentDate(), Len.WA_CURRENT_DATE);
	}

	public void setWaSeedXFormatted(String data) {
		byte[] buffer = new byte[Len.WA_SEED_X];
		MarshalByte.writeString(buffer, 1, data, Len.WA_SEED_X);
		setWaSeedXBytes(buffer, 1);
	}

	public String getWaSeedXFormatted() {
		return getWaSeedFormatted();
	}

	public void setWaSeedXBytes(byte[] buffer, int offset) {
		int position = offset;
		waSeed = MarshalByte.readFixedString(buffer, position, Len.WA_SEED);
	}

	public void setWaSeed(int waSeed) {
		this.waSeed = NumericDisplay.asString(waSeed, Len.WA_SEED);
	}

	public int getWaSeed() {
		return NumericDisplay.asInt(this.waSeed);
	}

	public String getWaSeedFormatted() {
		return this.waSeed;
	}

	public FillerWorkArea getFillerWorkArea() {
		return fillerWorkArea;
	}

	public WaFormattedDate getWaFormattedDate() {
		return waFormattedDate;
	}

	public WaTransDataQueInfo getWaTransDataQueInfo() {
		return waTransDataQueInfo;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WA_APPLID = 8;
		public static final int WA_MESSAGE_ID = 32;
		public static final int WA_UOW_LEVEL_TSQNAME = 16;
		public static final int WA_ABEND_ABCODE = 4;
		public static final int WA_ABEND_PROGRAM = 8;
		public static final int WA_RANDOM_NUMBER4 = 8;
		public static final int WA_CURRENT_DATE = 21;
		public static final int WA_SEED = 9;
		public static final int WA_SEED_X = WA_SEED;
		public static final int WA_BUS_OBJ_NM = 32;
		public static final int WA_BUS_OBJ_MDU = 32;
		public static final int WA_BAD_LOGPROB = 16;
		public static final int WA_BAD_HDR = 7;
		public static final int WA_BAD_DATA = 8;
		public static final int WA_BAD_NLBE = 8;
		public static final int WA_BAD_WRNS = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
