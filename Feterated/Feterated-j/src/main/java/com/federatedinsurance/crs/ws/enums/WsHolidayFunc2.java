/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-HOLIDAY-FUNC-2<br>
 * Variable: WS-HOLIDAY-FUNC-2 from program XPIODAT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsHolidayFunc2 {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char HOLIDAY_TEST = 'T';
	public static final char NON_WKN_BEFORE = '1';
	public static final char NON_WKN_AFTER = '2';
	public static final char NON_WKN_BEFORE_SAT = '3';
	public static final char NON_WKN_AFTER_SUN = '4';
	public static final char NON_WKN_SAT_SUN_SPLIT = '5';
	public static final char NON_HOL_BEFORE = '6';
	public static final char NON_HOL_AFTER = '7';
	public static final char NON_HOL_WKN_BEFORE = '8';
	public static final char NON_HOL_WKN_AFTER = '9';

	//==== METHODS ====
	public void setWsHolidayFunc2(char wsHolidayFunc2) {
		this.value = wsHolidayFunc2;
	}

	public char getWsHolidayFunc2() {
		return this.value;
	}

	public boolean isHolidayTest() {
		return value == HOLIDAY_TEST;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_HOLIDAY_FUNC2 = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
