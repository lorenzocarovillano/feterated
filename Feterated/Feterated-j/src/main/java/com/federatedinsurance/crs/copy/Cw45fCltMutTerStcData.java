/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW45F-CLT-MUT-TER-STC-DATA<br>
 * Variable: CW45F-CLT-MUT-TER-STC-DATA from copybook CAWLF045<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw45fCltMutTerStcData {

	//==== PROPERTIES ====
	/**Original name: CW45F-MR-TER-NBR-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char mrTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-MR-TER-NBR
	private String mrTerNbr = DefaultValues.stringVal(Len.MR_TER_NBR);
	//Original name: CW45F-MR-TER-CLT-ID-CI
	private char mrTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-MR-TER-CLT-ID
	private String mrTerCltId = DefaultValues.stringVal(Len.MR_TER_CLT_ID);
	//Original name: CW45F-MR-NM-PFX-CI
	private char mrNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-MR-NM-PFX
	private String mrNmPfx = DefaultValues.stringVal(Len.MR_NM_PFX);
	//Original name: CW45F-MR-FST-NM-CI
	private char mrFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-MR-FST-NM
	private String mrFstNm = DefaultValues.stringVal(Len.MR_FST_NM);
	//Original name: CW45F-MR-MDL-NM-CI
	private char mrMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-MR-MDL-NM
	private String mrMdlNm = DefaultValues.stringVal(Len.MR_MDL_NM);
	//Original name: CW45F-MR-LST-NM-CI
	private char mrLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-MR-LST-NM
	private String mrLstNm = DefaultValues.stringVal(Len.MR_LST_NM);
	//Original name: CW45F-MR-NM-CI
	private char mrNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-MR-NM
	private String mrNm = DefaultValues.stringVal(Len.MR_NM);
	//Original name: CW45F-MR-NM-SFX-CI
	private char mrNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-MR-NM-SFX
	private String mrNmSfx = DefaultValues.stringVal(Len.MR_NM_SFX);
	//Original name: CW45F-MR-CLT-ID-CI
	private char mrCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-MR-CLT-ID
	private String mrCltId = DefaultValues.stringVal(Len.MR_CLT_ID);
	//Original name: CW45F-CSR-TER-NBR-CI
	private char csrTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CSR-TER-NBR
	private String csrTerNbr = DefaultValues.stringVal(Len.CSR_TER_NBR);
	//Original name: CW45F-CSR-TER-CLT-ID-CI
	private char csrTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CSR-TER-CLT-ID
	private String csrTerCltId = DefaultValues.stringVal(Len.CSR_TER_CLT_ID);
	//Original name: CW45F-CSR-NM-PFX-CI
	private char csrNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CSR-NM-PFX
	private String csrNmPfx = DefaultValues.stringVal(Len.CSR_NM_PFX);
	//Original name: CW45F-CSR-FST-NM-CI
	private char csrFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CSR-FST-NM
	private String csrFstNm = DefaultValues.stringVal(Len.CSR_FST_NM);
	//Original name: CW45F-CSR-MDL-NM-CI
	private char csrMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CSR-MDL-NM
	private String csrMdlNm = DefaultValues.stringVal(Len.CSR_MDL_NM);
	//Original name: CW45F-CSR-LST-NM-CI
	private char csrLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CSR-LST-NM
	private String csrLstNm = DefaultValues.stringVal(Len.CSR_LST_NM);
	//Original name: CW45F-CSR-NM-CI
	private char csrNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CSR-NM
	private String csrNm = DefaultValues.stringVal(Len.CSR_NM);
	//Original name: CW45F-CSR-NM-SFX-CI
	private char csrNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CSR-NM-SFX
	private String csrNmSfx = DefaultValues.stringVal(Len.CSR_NM_SFX);
	//Original name: CW45F-CSR-CLT-ID-CI
	private char csrCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CSR-CLT-ID
	private String csrCltId = DefaultValues.stringVal(Len.CSR_CLT_ID);
	//Original name: CW45F-SMR-TER-NBR-CI
	private char smrTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-SMR-TER-NBR
	private String smrTerNbr = DefaultValues.stringVal(Len.SMR_TER_NBR);
	//Original name: CW45F-SMR-TER-CLT-ID-CI
	private char smrTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-SMR-TER-CLT-ID
	private String smrTerCltId = DefaultValues.stringVal(Len.SMR_TER_CLT_ID);
	//Original name: CW45F-SMR-NM-PFX-CI
	private char smrNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-SMR-NM-PFX
	private String smrNmPfx = DefaultValues.stringVal(Len.SMR_NM_PFX);
	//Original name: CW45F-SMR-FST-NM-CI
	private char smrFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-SMR-FST-NM
	private String smrFstNm = DefaultValues.stringVal(Len.SMR_FST_NM);
	//Original name: CW45F-SMR-MDL-NM-CI
	private char smrMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-SMR-MDL-NM
	private String smrMdlNm = DefaultValues.stringVal(Len.SMR_MDL_NM);
	//Original name: CW45F-SMR-LST-NM-CI
	private char smrLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-SMR-LST-NM
	private String smrLstNm = DefaultValues.stringVal(Len.SMR_LST_NM);
	//Original name: CW45F-SMR-NM-CI
	private char smrNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-SMR-NM
	private String smrNm = DefaultValues.stringVal(Len.SMR_NM);
	//Original name: CW45F-SMR-NM-SFX-CI
	private char smrNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-SMR-NM-SFX
	private String smrNmSfx = DefaultValues.stringVal(Len.SMR_NM_SFX);
	//Original name: CW45F-SMR-CLT-ID-CI
	private char smrCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-SMR-CLT-ID
	private String smrCltId = DefaultValues.stringVal(Len.SMR_CLT_ID);
	//Original name: CW45F-DMM-TER-NBR-CI
	private char dmmTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DMM-TER-NBR
	private String dmmTerNbr = DefaultValues.stringVal(Len.DMM_TER_NBR);
	//Original name: CW45F-DMM-TER-CLT-ID-CI
	private char dmmTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DMM-TER-CLT-ID
	private String dmmTerCltId = DefaultValues.stringVal(Len.DMM_TER_CLT_ID);
	//Original name: CW45F-DMM-NM-PFX-CI
	private char dmmNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DMM-NM-PFX
	private String dmmNmPfx = DefaultValues.stringVal(Len.DMM_NM_PFX);
	//Original name: CW45F-DMM-FST-NM-CI
	private char dmmFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DMM-FST-NM
	private String dmmFstNm = DefaultValues.stringVal(Len.DMM_FST_NM);
	//Original name: CW45F-DMM-MDL-NM-CI
	private char dmmMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DMM-MDL-NM
	private String dmmMdlNm = DefaultValues.stringVal(Len.DMM_MDL_NM);
	//Original name: CW45F-DMM-LST-NM-CI
	private char dmmLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DMM-LST-NM
	private String dmmLstNm = DefaultValues.stringVal(Len.DMM_LST_NM);
	//Original name: CW45F-DMM-NM-CI
	private char dmmNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DMM-NM
	private String dmmNm = DefaultValues.stringVal(Len.DMM_NM);
	//Original name: CW45F-DMM-NM-SFX-CI
	private char dmmNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DMM-NM-SFX
	private String dmmNmSfx = DefaultValues.stringVal(Len.DMM_NM_SFX);
	//Original name: CW45F-DMM-CLT-ID-CI
	private char dmmCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DMM-CLT-ID
	private String dmmCltId = DefaultValues.stringVal(Len.DMM_CLT_ID);
	//Original name: CW45F-RMM-TER-NBR-CI
	private char rmmTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-RMM-TER-NBR
	private String rmmTerNbr = DefaultValues.stringVal(Len.RMM_TER_NBR);
	//Original name: CW45F-RMM-TER-CLT-ID-CI
	private char rmmTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-RMM-TER-CLT-ID
	private String rmmTerCltId = DefaultValues.stringVal(Len.RMM_TER_CLT_ID);
	//Original name: CW45F-RMM-NM-PFX-CI
	private char rmmNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-RMM-NM-PFX
	private String rmmNmPfx = DefaultValues.stringVal(Len.RMM_NM_PFX);
	//Original name: CW45F-RMM-FST-NM-CI
	private char rmmFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-RMM-FST-NM
	private String rmmFstNm = DefaultValues.stringVal(Len.RMM_FST_NM);
	//Original name: CW45F-RMM-MDL-NM-CI
	private char rmmMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-RMM-MDL-NM
	private String rmmMdlNm = DefaultValues.stringVal(Len.RMM_MDL_NM);
	//Original name: CW45F-RMM-LST-NM-CI
	private char rmmLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-RMM-LST-NM
	private String rmmLstNm = DefaultValues.stringVal(Len.RMM_LST_NM);
	//Original name: CW45F-RMM-NM-CI
	private char rmmNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-RMM-NM
	private String rmmNm = DefaultValues.stringVal(Len.RMM_NM);
	//Original name: CW45F-RMM-NM-SFX-CI
	private char rmmNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-RMM-NM-SFX
	private String rmmNmSfx = DefaultValues.stringVal(Len.RMM_NM_SFX);
	//Original name: CW45F-RMM-CLT-ID-CI
	private char rmmCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-RMM-CLT-ID
	private String rmmCltId = DefaultValues.stringVal(Len.RMM_CLT_ID);
	//Original name: CW45F-DFO-TER-NBR-CI
	private char dfoTerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DFO-TER-NBR
	private String dfoTerNbr = DefaultValues.stringVal(Len.DFO_TER_NBR);
	//Original name: CW45F-DFO-TER-CLT-ID-CI
	private char dfoTerCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DFO-TER-CLT-ID
	private String dfoTerCltId = DefaultValues.stringVal(Len.DFO_TER_CLT_ID);
	//Original name: CW45F-DFO-NM-PFX-CI
	private char dfoNmPfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DFO-NM-PFX
	private String dfoNmPfx = DefaultValues.stringVal(Len.DFO_NM_PFX);
	//Original name: CW45F-DFO-FST-NM-CI
	private char dfoFstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DFO-FST-NM
	private String dfoFstNm = DefaultValues.stringVal(Len.DFO_FST_NM);
	//Original name: CW45F-DFO-MDL-NM-CI
	private char dfoMdlNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DFO-MDL-NM
	private String dfoMdlNm = DefaultValues.stringVal(Len.DFO_MDL_NM);
	//Original name: CW45F-DFO-LST-NM-CI
	private char dfoLstNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DFO-LST-NM
	private String dfoLstNm = DefaultValues.stringVal(Len.DFO_LST_NM);
	//Original name: CW45F-DFO-NM-CI
	private char dfoNmCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DFO-NM
	private String dfoNm = DefaultValues.stringVal(Len.DFO_NM);
	//Original name: CW45F-DFO-NM-SFX-CI
	private char dfoNmSfxCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DFO-NM-SFX
	private String dfoNmSfx = DefaultValues.stringVal(Len.DFO_NM_SFX);
	//Original name: CW45F-DFO-CLT-ID-CI
	private char dfoCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-DFO-CLT-ID
	private String dfoCltId = DefaultValues.stringVal(Len.DFO_CLT_ID);

	//==== METHODS ====
	public void setCltMutTerStcDataBytes(byte[] buffer, int offset) {
		int position = offset;
		mrTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mrTerNbr = MarshalByte.readString(buffer, position, Len.MR_TER_NBR);
		position += Len.MR_TER_NBR;
		mrTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mrTerCltId = MarshalByte.readString(buffer, position, Len.MR_TER_CLT_ID);
		position += Len.MR_TER_CLT_ID;
		mrNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mrNmPfx = MarshalByte.readString(buffer, position, Len.MR_NM_PFX);
		position += Len.MR_NM_PFX;
		mrFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mrFstNm = MarshalByte.readString(buffer, position, Len.MR_FST_NM);
		position += Len.MR_FST_NM;
		mrMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mrMdlNm = MarshalByte.readString(buffer, position, Len.MR_MDL_NM);
		position += Len.MR_MDL_NM;
		mrLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mrLstNm = MarshalByte.readString(buffer, position, Len.MR_LST_NM);
		position += Len.MR_LST_NM;
		mrNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mrNm = MarshalByte.readString(buffer, position, Len.MR_NM);
		position += Len.MR_NM;
		mrNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mrNmSfx = MarshalByte.readString(buffer, position, Len.MR_NM_SFX);
		position += Len.MR_NM_SFX;
		mrCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mrCltId = MarshalByte.readString(buffer, position, Len.MR_CLT_ID);
		position += Len.MR_CLT_ID;
		csrTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		csrTerNbr = MarshalByte.readString(buffer, position, Len.CSR_TER_NBR);
		position += Len.CSR_TER_NBR;
		csrTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		csrTerCltId = MarshalByte.readString(buffer, position, Len.CSR_TER_CLT_ID);
		position += Len.CSR_TER_CLT_ID;
		csrNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		csrNmPfx = MarshalByte.readString(buffer, position, Len.CSR_NM_PFX);
		position += Len.CSR_NM_PFX;
		csrFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		csrFstNm = MarshalByte.readString(buffer, position, Len.CSR_FST_NM);
		position += Len.CSR_FST_NM;
		csrMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		csrMdlNm = MarshalByte.readString(buffer, position, Len.CSR_MDL_NM);
		position += Len.CSR_MDL_NM;
		csrLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		csrLstNm = MarshalByte.readString(buffer, position, Len.CSR_LST_NM);
		position += Len.CSR_LST_NM;
		csrNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		csrNm = MarshalByte.readString(buffer, position, Len.CSR_NM);
		position += Len.CSR_NM;
		csrNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		csrNmSfx = MarshalByte.readString(buffer, position, Len.CSR_NM_SFX);
		position += Len.CSR_NM_SFX;
		csrCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		csrCltId = MarshalByte.readString(buffer, position, Len.CSR_CLT_ID);
		position += Len.CSR_CLT_ID;
		smrTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrTerNbr = MarshalByte.readString(buffer, position, Len.SMR_TER_NBR);
		position += Len.SMR_TER_NBR;
		smrTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrTerCltId = MarshalByte.readString(buffer, position, Len.SMR_TER_CLT_ID);
		position += Len.SMR_TER_CLT_ID;
		smrNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrNmPfx = MarshalByte.readString(buffer, position, Len.SMR_NM_PFX);
		position += Len.SMR_NM_PFX;
		smrFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrFstNm = MarshalByte.readString(buffer, position, Len.SMR_FST_NM);
		position += Len.SMR_FST_NM;
		smrMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrMdlNm = MarshalByte.readString(buffer, position, Len.SMR_MDL_NM);
		position += Len.SMR_MDL_NM;
		smrLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrLstNm = MarshalByte.readString(buffer, position, Len.SMR_LST_NM);
		position += Len.SMR_LST_NM;
		smrNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrNm = MarshalByte.readString(buffer, position, Len.SMR_NM);
		position += Len.SMR_NM;
		smrNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrNmSfx = MarshalByte.readString(buffer, position, Len.SMR_NM_SFX);
		position += Len.SMR_NM_SFX;
		smrCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		smrCltId = MarshalByte.readString(buffer, position, Len.SMR_CLT_ID);
		position += Len.SMR_CLT_ID;
		dmmTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dmmTerNbr = MarshalByte.readString(buffer, position, Len.DMM_TER_NBR);
		position += Len.DMM_TER_NBR;
		dmmTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dmmTerCltId = MarshalByte.readString(buffer, position, Len.DMM_TER_CLT_ID);
		position += Len.DMM_TER_CLT_ID;
		dmmNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dmmNmPfx = MarshalByte.readString(buffer, position, Len.DMM_NM_PFX);
		position += Len.DMM_NM_PFX;
		dmmFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dmmFstNm = MarshalByte.readString(buffer, position, Len.DMM_FST_NM);
		position += Len.DMM_FST_NM;
		dmmMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dmmMdlNm = MarshalByte.readString(buffer, position, Len.DMM_MDL_NM);
		position += Len.DMM_MDL_NM;
		dmmLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dmmLstNm = MarshalByte.readString(buffer, position, Len.DMM_LST_NM);
		position += Len.DMM_LST_NM;
		dmmNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dmmNm = MarshalByte.readString(buffer, position, Len.DMM_NM);
		position += Len.DMM_NM;
		dmmNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dmmNmSfx = MarshalByte.readString(buffer, position, Len.DMM_NM_SFX);
		position += Len.DMM_NM_SFX;
		dmmCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dmmCltId = MarshalByte.readString(buffer, position, Len.DMM_CLT_ID);
		position += Len.DMM_CLT_ID;
		rmmTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmmTerNbr = MarshalByte.readString(buffer, position, Len.RMM_TER_NBR);
		position += Len.RMM_TER_NBR;
		rmmTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmmTerCltId = MarshalByte.readString(buffer, position, Len.RMM_TER_CLT_ID);
		position += Len.RMM_TER_CLT_ID;
		rmmNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmmNmPfx = MarshalByte.readString(buffer, position, Len.RMM_NM_PFX);
		position += Len.RMM_NM_PFX;
		rmmFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmmFstNm = MarshalByte.readString(buffer, position, Len.RMM_FST_NM);
		position += Len.RMM_FST_NM;
		rmmMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmmMdlNm = MarshalByte.readString(buffer, position, Len.RMM_MDL_NM);
		position += Len.RMM_MDL_NM;
		rmmLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmmLstNm = MarshalByte.readString(buffer, position, Len.RMM_LST_NM);
		position += Len.RMM_LST_NM;
		rmmNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmmNm = MarshalByte.readString(buffer, position, Len.RMM_NM);
		position += Len.RMM_NM;
		rmmNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmmNmSfx = MarshalByte.readString(buffer, position, Len.RMM_NM_SFX);
		position += Len.RMM_NM_SFX;
		rmmCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		rmmCltId = MarshalByte.readString(buffer, position, Len.RMM_CLT_ID);
		position += Len.RMM_CLT_ID;
		dfoTerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dfoTerNbr = MarshalByte.readString(buffer, position, Len.DFO_TER_NBR);
		position += Len.DFO_TER_NBR;
		dfoTerCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dfoTerCltId = MarshalByte.readString(buffer, position, Len.DFO_TER_CLT_ID);
		position += Len.DFO_TER_CLT_ID;
		dfoNmPfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dfoNmPfx = MarshalByte.readString(buffer, position, Len.DFO_NM_PFX);
		position += Len.DFO_NM_PFX;
		dfoFstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dfoFstNm = MarshalByte.readString(buffer, position, Len.DFO_FST_NM);
		position += Len.DFO_FST_NM;
		dfoMdlNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dfoMdlNm = MarshalByte.readString(buffer, position, Len.DFO_MDL_NM);
		position += Len.DFO_MDL_NM;
		dfoLstNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dfoLstNm = MarshalByte.readString(buffer, position, Len.DFO_LST_NM);
		position += Len.DFO_LST_NM;
		dfoNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dfoNm = MarshalByte.readString(buffer, position, Len.DFO_NM);
		position += Len.DFO_NM;
		dfoNmSfxCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dfoNmSfx = MarshalByte.readString(buffer, position, Len.DFO_NM_SFX);
		position += Len.DFO_NM_SFX;
		dfoCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dfoCltId = MarshalByte.readString(buffer, position, Len.DFO_CLT_ID);
	}

	public byte[] getCltMutTerStcDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, mrTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mrTerNbr, Len.MR_TER_NBR);
		position += Len.MR_TER_NBR;
		MarshalByte.writeChar(buffer, position, mrTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mrTerCltId, Len.MR_TER_CLT_ID);
		position += Len.MR_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, mrNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mrNmPfx, Len.MR_NM_PFX);
		position += Len.MR_NM_PFX;
		MarshalByte.writeChar(buffer, position, mrFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mrFstNm, Len.MR_FST_NM);
		position += Len.MR_FST_NM;
		MarshalByte.writeChar(buffer, position, mrMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mrMdlNm, Len.MR_MDL_NM);
		position += Len.MR_MDL_NM;
		MarshalByte.writeChar(buffer, position, mrLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mrLstNm, Len.MR_LST_NM);
		position += Len.MR_LST_NM;
		MarshalByte.writeChar(buffer, position, mrNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mrNm, Len.MR_NM);
		position += Len.MR_NM;
		MarshalByte.writeChar(buffer, position, mrNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mrNmSfx, Len.MR_NM_SFX);
		position += Len.MR_NM_SFX;
		MarshalByte.writeChar(buffer, position, mrCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mrCltId, Len.MR_CLT_ID);
		position += Len.MR_CLT_ID;
		MarshalByte.writeChar(buffer, position, csrTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csrTerNbr, Len.CSR_TER_NBR);
		position += Len.CSR_TER_NBR;
		MarshalByte.writeChar(buffer, position, csrTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csrTerCltId, Len.CSR_TER_CLT_ID);
		position += Len.CSR_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, csrNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csrNmPfx, Len.CSR_NM_PFX);
		position += Len.CSR_NM_PFX;
		MarshalByte.writeChar(buffer, position, csrFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csrFstNm, Len.CSR_FST_NM);
		position += Len.CSR_FST_NM;
		MarshalByte.writeChar(buffer, position, csrMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csrMdlNm, Len.CSR_MDL_NM);
		position += Len.CSR_MDL_NM;
		MarshalByte.writeChar(buffer, position, csrLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csrLstNm, Len.CSR_LST_NM);
		position += Len.CSR_LST_NM;
		MarshalByte.writeChar(buffer, position, csrNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csrNm, Len.CSR_NM);
		position += Len.CSR_NM;
		MarshalByte.writeChar(buffer, position, csrNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csrNmSfx, Len.CSR_NM_SFX);
		position += Len.CSR_NM_SFX;
		MarshalByte.writeChar(buffer, position, csrCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, csrCltId, Len.CSR_CLT_ID);
		position += Len.CSR_CLT_ID;
		MarshalByte.writeChar(buffer, position, smrTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrTerNbr, Len.SMR_TER_NBR);
		position += Len.SMR_TER_NBR;
		MarshalByte.writeChar(buffer, position, smrTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrTerCltId, Len.SMR_TER_CLT_ID);
		position += Len.SMR_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, smrNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrNmPfx, Len.SMR_NM_PFX);
		position += Len.SMR_NM_PFX;
		MarshalByte.writeChar(buffer, position, smrFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrFstNm, Len.SMR_FST_NM);
		position += Len.SMR_FST_NM;
		MarshalByte.writeChar(buffer, position, smrMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrMdlNm, Len.SMR_MDL_NM);
		position += Len.SMR_MDL_NM;
		MarshalByte.writeChar(buffer, position, smrLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrLstNm, Len.SMR_LST_NM);
		position += Len.SMR_LST_NM;
		MarshalByte.writeChar(buffer, position, smrNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrNm, Len.SMR_NM);
		position += Len.SMR_NM;
		MarshalByte.writeChar(buffer, position, smrNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrNmSfx, Len.SMR_NM_SFX);
		position += Len.SMR_NM_SFX;
		MarshalByte.writeChar(buffer, position, smrCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, smrCltId, Len.SMR_CLT_ID);
		position += Len.SMR_CLT_ID;
		MarshalByte.writeChar(buffer, position, dmmTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dmmTerNbr, Len.DMM_TER_NBR);
		position += Len.DMM_TER_NBR;
		MarshalByte.writeChar(buffer, position, dmmTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dmmTerCltId, Len.DMM_TER_CLT_ID);
		position += Len.DMM_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, dmmNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dmmNmPfx, Len.DMM_NM_PFX);
		position += Len.DMM_NM_PFX;
		MarshalByte.writeChar(buffer, position, dmmFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dmmFstNm, Len.DMM_FST_NM);
		position += Len.DMM_FST_NM;
		MarshalByte.writeChar(buffer, position, dmmMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dmmMdlNm, Len.DMM_MDL_NM);
		position += Len.DMM_MDL_NM;
		MarshalByte.writeChar(buffer, position, dmmLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dmmLstNm, Len.DMM_LST_NM);
		position += Len.DMM_LST_NM;
		MarshalByte.writeChar(buffer, position, dmmNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dmmNm, Len.DMM_NM);
		position += Len.DMM_NM;
		MarshalByte.writeChar(buffer, position, dmmNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dmmNmSfx, Len.DMM_NM_SFX);
		position += Len.DMM_NM_SFX;
		MarshalByte.writeChar(buffer, position, dmmCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dmmCltId, Len.DMM_CLT_ID);
		position += Len.DMM_CLT_ID;
		MarshalByte.writeChar(buffer, position, rmmTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmmTerNbr, Len.RMM_TER_NBR);
		position += Len.RMM_TER_NBR;
		MarshalByte.writeChar(buffer, position, rmmTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmmTerCltId, Len.RMM_TER_CLT_ID);
		position += Len.RMM_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, rmmNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmmNmPfx, Len.RMM_NM_PFX);
		position += Len.RMM_NM_PFX;
		MarshalByte.writeChar(buffer, position, rmmFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmmFstNm, Len.RMM_FST_NM);
		position += Len.RMM_FST_NM;
		MarshalByte.writeChar(buffer, position, rmmMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmmMdlNm, Len.RMM_MDL_NM);
		position += Len.RMM_MDL_NM;
		MarshalByte.writeChar(buffer, position, rmmLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmmLstNm, Len.RMM_LST_NM);
		position += Len.RMM_LST_NM;
		MarshalByte.writeChar(buffer, position, rmmNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmmNm, Len.RMM_NM);
		position += Len.RMM_NM;
		MarshalByte.writeChar(buffer, position, rmmNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmmNmSfx, Len.RMM_NM_SFX);
		position += Len.RMM_NM_SFX;
		MarshalByte.writeChar(buffer, position, rmmCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, rmmCltId, Len.RMM_CLT_ID);
		position += Len.RMM_CLT_ID;
		MarshalByte.writeChar(buffer, position, dfoTerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dfoTerNbr, Len.DFO_TER_NBR);
		position += Len.DFO_TER_NBR;
		MarshalByte.writeChar(buffer, position, dfoTerCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dfoTerCltId, Len.DFO_TER_CLT_ID);
		position += Len.DFO_TER_CLT_ID;
		MarshalByte.writeChar(buffer, position, dfoNmPfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dfoNmPfx, Len.DFO_NM_PFX);
		position += Len.DFO_NM_PFX;
		MarshalByte.writeChar(buffer, position, dfoFstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dfoFstNm, Len.DFO_FST_NM);
		position += Len.DFO_FST_NM;
		MarshalByte.writeChar(buffer, position, dfoMdlNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dfoMdlNm, Len.DFO_MDL_NM);
		position += Len.DFO_MDL_NM;
		MarshalByte.writeChar(buffer, position, dfoLstNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dfoLstNm, Len.DFO_LST_NM);
		position += Len.DFO_LST_NM;
		MarshalByte.writeChar(buffer, position, dfoNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dfoNm, Len.DFO_NM);
		position += Len.DFO_NM;
		MarshalByte.writeChar(buffer, position, dfoNmSfxCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dfoNmSfx, Len.DFO_NM_SFX);
		position += Len.DFO_NM_SFX;
		MarshalByte.writeChar(buffer, position, dfoCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dfoCltId, Len.DFO_CLT_ID);
		return buffer;
	}

	public void setMrTerNbrCi(char mrTerNbrCi) {
		this.mrTerNbrCi = mrTerNbrCi;
	}

	public char getMrTerNbrCi() {
		return this.mrTerNbrCi;
	}

	public void setMrTerNbr(String mrTerNbr) {
		this.mrTerNbr = Functions.subString(mrTerNbr, Len.MR_TER_NBR);
	}

	public String getMrTerNbr() {
		return this.mrTerNbr;
	}

	public void setMrTerCltIdCi(char mrTerCltIdCi) {
		this.mrTerCltIdCi = mrTerCltIdCi;
	}

	public char getMrTerCltIdCi() {
		return this.mrTerCltIdCi;
	}

	public void setMrTerCltId(String mrTerCltId) {
		this.mrTerCltId = Functions.subString(mrTerCltId, Len.MR_TER_CLT_ID);
	}

	public String getMrTerCltId() {
		return this.mrTerCltId;
	}

	public void setMrNmPfxCi(char mrNmPfxCi) {
		this.mrNmPfxCi = mrNmPfxCi;
	}

	public char getMrNmPfxCi() {
		return this.mrNmPfxCi;
	}

	public void setMrNmPfx(String mrNmPfx) {
		this.mrNmPfx = Functions.subString(mrNmPfx, Len.MR_NM_PFX);
	}

	public String getMrNmPfx() {
		return this.mrNmPfx;
	}

	public void setMrFstNmCi(char mrFstNmCi) {
		this.mrFstNmCi = mrFstNmCi;
	}

	public char getMrFstNmCi() {
		return this.mrFstNmCi;
	}

	public void setMrFstNm(String mrFstNm) {
		this.mrFstNm = Functions.subString(mrFstNm, Len.MR_FST_NM);
	}

	public String getMrFstNm() {
		return this.mrFstNm;
	}

	public void setMrMdlNmCi(char mrMdlNmCi) {
		this.mrMdlNmCi = mrMdlNmCi;
	}

	public char getMrMdlNmCi() {
		return this.mrMdlNmCi;
	}

	public void setMrMdlNm(String mrMdlNm) {
		this.mrMdlNm = Functions.subString(mrMdlNm, Len.MR_MDL_NM);
	}

	public String getMrMdlNm() {
		return this.mrMdlNm;
	}

	public void setMrLstNmCi(char mrLstNmCi) {
		this.mrLstNmCi = mrLstNmCi;
	}

	public char getMrLstNmCi() {
		return this.mrLstNmCi;
	}

	public void setMrLstNm(String mrLstNm) {
		this.mrLstNm = Functions.subString(mrLstNm, Len.MR_LST_NM);
	}

	public String getMrLstNm() {
		return this.mrLstNm;
	}

	public void setMrNmCi(char mrNmCi) {
		this.mrNmCi = mrNmCi;
	}

	public char getMrNmCi() {
		return this.mrNmCi;
	}

	public void setMrNm(String mrNm) {
		this.mrNm = Functions.subString(mrNm, Len.MR_NM);
	}

	public String getMrNm() {
		return this.mrNm;
	}

	public void setMrNmSfxCi(char mrNmSfxCi) {
		this.mrNmSfxCi = mrNmSfxCi;
	}

	public char getMrNmSfxCi() {
		return this.mrNmSfxCi;
	}

	public void setMrNmSfx(String mrNmSfx) {
		this.mrNmSfx = Functions.subString(mrNmSfx, Len.MR_NM_SFX);
	}

	public String getMrNmSfx() {
		return this.mrNmSfx;
	}

	public void setMrCltIdCi(char mrCltIdCi) {
		this.mrCltIdCi = mrCltIdCi;
	}

	public char getMrCltIdCi() {
		return this.mrCltIdCi;
	}

	public void setMrCltId(String mrCltId) {
		this.mrCltId = Functions.subString(mrCltId, Len.MR_CLT_ID);
	}

	public String getMrCltId() {
		return this.mrCltId;
	}

	public void setCsrTerNbrCi(char csrTerNbrCi) {
		this.csrTerNbrCi = csrTerNbrCi;
	}

	public char getCsrTerNbrCi() {
		return this.csrTerNbrCi;
	}

	public void setCsrTerNbr(String csrTerNbr) {
		this.csrTerNbr = Functions.subString(csrTerNbr, Len.CSR_TER_NBR);
	}

	public String getCsrTerNbr() {
		return this.csrTerNbr;
	}

	public void setCsrTerCltIdCi(char csrTerCltIdCi) {
		this.csrTerCltIdCi = csrTerCltIdCi;
	}

	public char getCsrTerCltIdCi() {
		return this.csrTerCltIdCi;
	}

	public void setCsrTerCltId(String csrTerCltId) {
		this.csrTerCltId = Functions.subString(csrTerCltId, Len.CSR_TER_CLT_ID);
	}

	public String getCsrTerCltId() {
		return this.csrTerCltId;
	}

	public void setCsrNmPfxCi(char csrNmPfxCi) {
		this.csrNmPfxCi = csrNmPfxCi;
	}

	public char getCsrNmPfxCi() {
		return this.csrNmPfxCi;
	}

	public void setCsrNmPfx(String csrNmPfx) {
		this.csrNmPfx = Functions.subString(csrNmPfx, Len.CSR_NM_PFX);
	}

	public String getCsrNmPfx() {
		return this.csrNmPfx;
	}

	public void setCsrFstNmCi(char csrFstNmCi) {
		this.csrFstNmCi = csrFstNmCi;
	}

	public char getCsrFstNmCi() {
		return this.csrFstNmCi;
	}

	public void setCsrFstNm(String csrFstNm) {
		this.csrFstNm = Functions.subString(csrFstNm, Len.CSR_FST_NM);
	}

	public String getCsrFstNm() {
		return this.csrFstNm;
	}

	public void setCsrMdlNmCi(char csrMdlNmCi) {
		this.csrMdlNmCi = csrMdlNmCi;
	}

	public char getCsrMdlNmCi() {
		return this.csrMdlNmCi;
	}

	public void setCsrMdlNm(String csrMdlNm) {
		this.csrMdlNm = Functions.subString(csrMdlNm, Len.CSR_MDL_NM);
	}

	public String getCsrMdlNm() {
		return this.csrMdlNm;
	}

	public void setCsrLstNmCi(char csrLstNmCi) {
		this.csrLstNmCi = csrLstNmCi;
	}

	public char getCsrLstNmCi() {
		return this.csrLstNmCi;
	}

	public void setCsrLstNm(String csrLstNm) {
		this.csrLstNm = Functions.subString(csrLstNm, Len.CSR_LST_NM);
	}

	public String getCsrLstNm() {
		return this.csrLstNm;
	}

	public void setCsrNmCi(char csrNmCi) {
		this.csrNmCi = csrNmCi;
	}

	public char getCsrNmCi() {
		return this.csrNmCi;
	}

	public void setCsrNm(String csrNm) {
		this.csrNm = Functions.subString(csrNm, Len.CSR_NM);
	}

	public String getCsrNm() {
		return this.csrNm;
	}

	public void setCsrNmSfxCi(char csrNmSfxCi) {
		this.csrNmSfxCi = csrNmSfxCi;
	}

	public char getCsrNmSfxCi() {
		return this.csrNmSfxCi;
	}

	public void setCsrNmSfx(String csrNmSfx) {
		this.csrNmSfx = Functions.subString(csrNmSfx, Len.CSR_NM_SFX);
	}

	public String getCsrNmSfx() {
		return this.csrNmSfx;
	}

	public void setCsrCltIdCi(char csrCltIdCi) {
		this.csrCltIdCi = csrCltIdCi;
	}

	public char getCsrCltIdCi() {
		return this.csrCltIdCi;
	}

	public void setCsrCltId(String csrCltId) {
		this.csrCltId = Functions.subString(csrCltId, Len.CSR_CLT_ID);
	}

	public String getCsrCltId() {
		return this.csrCltId;
	}

	public void setSmrTerNbrCi(char smrTerNbrCi) {
		this.smrTerNbrCi = smrTerNbrCi;
	}

	public char getSmrTerNbrCi() {
		return this.smrTerNbrCi;
	}

	public void setSmrTerNbr(String smrTerNbr) {
		this.smrTerNbr = Functions.subString(smrTerNbr, Len.SMR_TER_NBR);
	}

	public String getSmrTerNbr() {
		return this.smrTerNbr;
	}

	public void setSmrTerCltIdCi(char smrTerCltIdCi) {
		this.smrTerCltIdCi = smrTerCltIdCi;
	}

	public char getSmrTerCltIdCi() {
		return this.smrTerCltIdCi;
	}

	public void setSmrTerCltId(String smrTerCltId) {
		this.smrTerCltId = Functions.subString(smrTerCltId, Len.SMR_TER_CLT_ID);
	}

	public String getSmrTerCltId() {
		return this.smrTerCltId;
	}

	public void setSmrNmPfxCi(char smrNmPfxCi) {
		this.smrNmPfxCi = smrNmPfxCi;
	}

	public char getSmrNmPfxCi() {
		return this.smrNmPfxCi;
	}

	public void setSmrNmPfx(String smrNmPfx) {
		this.smrNmPfx = Functions.subString(smrNmPfx, Len.SMR_NM_PFX);
	}

	public String getSmrNmPfx() {
		return this.smrNmPfx;
	}

	public void setSmrFstNmCi(char smrFstNmCi) {
		this.smrFstNmCi = smrFstNmCi;
	}

	public char getSmrFstNmCi() {
		return this.smrFstNmCi;
	}

	public void setSmrFstNm(String smrFstNm) {
		this.smrFstNm = Functions.subString(smrFstNm, Len.SMR_FST_NM);
	}

	public String getSmrFstNm() {
		return this.smrFstNm;
	}

	public void setSmrMdlNmCi(char smrMdlNmCi) {
		this.smrMdlNmCi = smrMdlNmCi;
	}

	public char getSmrMdlNmCi() {
		return this.smrMdlNmCi;
	}

	public void setSmrMdlNm(String smrMdlNm) {
		this.smrMdlNm = Functions.subString(smrMdlNm, Len.SMR_MDL_NM);
	}

	public String getSmrMdlNm() {
		return this.smrMdlNm;
	}

	public void setSmrLstNmCi(char smrLstNmCi) {
		this.smrLstNmCi = smrLstNmCi;
	}

	public char getSmrLstNmCi() {
		return this.smrLstNmCi;
	}

	public void setSmrLstNm(String smrLstNm) {
		this.smrLstNm = Functions.subString(smrLstNm, Len.SMR_LST_NM);
	}

	public String getSmrLstNm() {
		return this.smrLstNm;
	}

	public void setSmrNmCi(char smrNmCi) {
		this.smrNmCi = smrNmCi;
	}

	public char getSmrNmCi() {
		return this.smrNmCi;
	}

	public void setSmrNm(String smrNm) {
		this.smrNm = Functions.subString(smrNm, Len.SMR_NM);
	}

	public String getSmrNm() {
		return this.smrNm;
	}

	public void setSmrNmSfxCi(char smrNmSfxCi) {
		this.smrNmSfxCi = smrNmSfxCi;
	}

	public char getSmrNmSfxCi() {
		return this.smrNmSfxCi;
	}

	public void setSmrNmSfx(String smrNmSfx) {
		this.smrNmSfx = Functions.subString(smrNmSfx, Len.SMR_NM_SFX);
	}

	public String getSmrNmSfx() {
		return this.smrNmSfx;
	}

	public void setSmrCltIdCi(char smrCltIdCi) {
		this.smrCltIdCi = smrCltIdCi;
	}

	public char getSmrCltIdCi() {
		return this.smrCltIdCi;
	}

	public void setSmrCltId(String smrCltId) {
		this.smrCltId = Functions.subString(smrCltId, Len.SMR_CLT_ID);
	}

	public String getSmrCltId() {
		return this.smrCltId;
	}

	public void setDmmTerNbrCi(char dmmTerNbrCi) {
		this.dmmTerNbrCi = dmmTerNbrCi;
	}

	public char getDmmTerNbrCi() {
		return this.dmmTerNbrCi;
	}

	public void setDmmTerNbr(String dmmTerNbr) {
		this.dmmTerNbr = Functions.subString(dmmTerNbr, Len.DMM_TER_NBR);
	}

	public String getDmmTerNbr() {
		return this.dmmTerNbr;
	}

	public void setDmmTerCltIdCi(char dmmTerCltIdCi) {
		this.dmmTerCltIdCi = dmmTerCltIdCi;
	}

	public char getDmmTerCltIdCi() {
		return this.dmmTerCltIdCi;
	}

	public void setDmmTerCltId(String dmmTerCltId) {
		this.dmmTerCltId = Functions.subString(dmmTerCltId, Len.DMM_TER_CLT_ID);
	}

	public String getDmmTerCltId() {
		return this.dmmTerCltId;
	}

	public void setDmmNmPfxCi(char dmmNmPfxCi) {
		this.dmmNmPfxCi = dmmNmPfxCi;
	}

	public char getDmmNmPfxCi() {
		return this.dmmNmPfxCi;
	}

	public void setDmmNmPfx(String dmmNmPfx) {
		this.dmmNmPfx = Functions.subString(dmmNmPfx, Len.DMM_NM_PFX);
	}

	public String getDmmNmPfx() {
		return this.dmmNmPfx;
	}

	public void setDmmFstNmCi(char dmmFstNmCi) {
		this.dmmFstNmCi = dmmFstNmCi;
	}

	public char getDmmFstNmCi() {
		return this.dmmFstNmCi;
	}

	public void setDmmFstNm(String dmmFstNm) {
		this.dmmFstNm = Functions.subString(dmmFstNm, Len.DMM_FST_NM);
	}

	public String getDmmFstNm() {
		return this.dmmFstNm;
	}

	public void setDmmMdlNmCi(char dmmMdlNmCi) {
		this.dmmMdlNmCi = dmmMdlNmCi;
	}

	public char getDmmMdlNmCi() {
		return this.dmmMdlNmCi;
	}

	public void setDmmMdlNm(String dmmMdlNm) {
		this.dmmMdlNm = Functions.subString(dmmMdlNm, Len.DMM_MDL_NM);
	}

	public String getDmmMdlNm() {
		return this.dmmMdlNm;
	}

	public void setDmmLstNmCi(char dmmLstNmCi) {
		this.dmmLstNmCi = dmmLstNmCi;
	}

	public char getDmmLstNmCi() {
		return this.dmmLstNmCi;
	}

	public void setDmmLstNm(String dmmLstNm) {
		this.dmmLstNm = Functions.subString(dmmLstNm, Len.DMM_LST_NM);
	}

	public String getDmmLstNm() {
		return this.dmmLstNm;
	}

	public void setDmmNmCi(char dmmNmCi) {
		this.dmmNmCi = dmmNmCi;
	}

	public char getDmmNmCi() {
		return this.dmmNmCi;
	}

	public void setDmmNm(String dmmNm) {
		this.dmmNm = Functions.subString(dmmNm, Len.DMM_NM);
	}

	public String getDmmNm() {
		return this.dmmNm;
	}

	public void setDmmNmSfxCi(char dmmNmSfxCi) {
		this.dmmNmSfxCi = dmmNmSfxCi;
	}

	public char getDmmNmSfxCi() {
		return this.dmmNmSfxCi;
	}

	public void setDmmNmSfx(String dmmNmSfx) {
		this.dmmNmSfx = Functions.subString(dmmNmSfx, Len.DMM_NM_SFX);
	}

	public String getDmmNmSfx() {
		return this.dmmNmSfx;
	}

	public void setDmmCltIdCi(char dmmCltIdCi) {
		this.dmmCltIdCi = dmmCltIdCi;
	}

	public char getDmmCltIdCi() {
		return this.dmmCltIdCi;
	}

	public void setDmmCltId(String dmmCltId) {
		this.dmmCltId = Functions.subString(dmmCltId, Len.DMM_CLT_ID);
	}

	public String getDmmCltId() {
		return this.dmmCltId;
	}

	public void setRmmTerNbrCi(char rmmTerNbrCi) {
		this.rmmTerNbrCi = rmmTerNbrCi;
	}

	public char getRmmTerNbrCi() {
		return this.rmmTerNbrCi;
	}

	public void setRmmTerNbr(String rmmTerNbr) {
		this.rmmTerNbr = Functions.subString(rmmTerNbr, Len.RMM_TER_NBR);
	}

	public String getRmmTerNbr() {
		return this.rmmTerNbr;
	}

	public void setRmmTerCltIdCi(char rmmTerCltIdCi) {
		this.rmmTerCltIdCi = rmmTerCltIdCi;
	}

	public char getRmmTerCltIdCi() {
		return this.rmmTerCltIdCi;
	}

	public void setRmmTerCltId(String rmmTerCltId) {
		this.rmmTerCltId = Functions.subString(rmmTerCltId, Len.RMM_TER_CLT_ID);
	}

	public String getRmmTerCltId() {
		return this.rmmTerCltId;
	}

	public void setRmmNmPfxCi(char rmmNmPfxCi) {
		this.rmmNmPfxCi = rmmNmPfxCi;
	}

	public char getRmmNmPfxCi() {
		return this.rmmNmPfxCi;
	}

	public void setRmmNmPfx(String rmmNmPfx) {
		this.rmmNmPfx = Functions.subString(rmmNmPfx, Len.RMM_NM_PFX);
	}

	public String getRmmNmPfx() {
		return this.rmmNmPfx;
	}

	public void setRmmFstNmCi(char rmmFstNmCi) {
		this.rmmFstNmCi = rmmFstNmCi;
	}

	public char getRmmFstNmCi() {
		return this.rmmFstNmCi;
	}

	public void setRmmFstNm(String rmmFstNm) {
		this.rmmFstNm = Functions.subString(rmmFstNm, Len.RMM_FST_NM);
	}

	public String getRmmFstNm() {
		return this.rmmFstNm;
	}

	public void setRmmMdlNmCi(char rmmMdlNmCi) {
		this.rmmMdlNmCi = rmmMdlNmCi;
	}

	public char getRmmMdlNmCi() {
		return this.rmmMdlNmCi;
	}

	public void setRmmMdlNm(String rmmMdlNm) {
		this.rmmMdlNm = Functions.subString(rmmMdlNm, Len.RMM_MDL_NM);
	}

	public String getRmmMdlNm() {
		return this.rmmMdlNm;
	}

	public void setRmmLstNmCi(char rmmLstNmCi) {
		this.rmmLstNmCi = rmmLstNmCi;
	}

	public char getRmmLstNmCi() {
		return this.rmmLstNmCi;
	}

	public void setRmmLstNm(String rmmLstNm) {
		this.rmmLstNm = Functions.subString(rmmLstNm, Len.RMM_LST_NM);
	}

	public String getRmmLstNm() {
		return this.rmmLstNm;
	}

	public void setRmmNmCi(char rmmNmCi) {
		this.rmmNmCi = rmmNmCi;
	}

	public char getRmmNmCi() {
		return this.rmmNmCi;
	}

	public void setRmmNm(String rmmNm) {
		this.rmmNm = Functions.subString(rmmNm, Len.RMM_NM);
	}

	public String getRmmNm() {
		return this.rmmNm;
	}

	public void setRmmNmSfxCi(char rmmNmSfxCi) {
		this.rmmNmSfxCi = rmmNmSfxCi;
	}

	public char getRmmNmSfxCi() {
		return this.rmmNmSfxCi;
	}

	public void setRmmNmSfx(String rmmNmSfx) {
		this.rmmNmSfx = Functions.subString(rmmNmSfx, Len.RMM_NM_SFX);
	}

	public String getRmmNmSfx() {
		return this.rmmNmSfx;
	}

	public void setRmmCltIdCi(char rmmCltIdCi) {
		this.rmmCltIdCi = rmmCltIdCi;
	}

	public char getRmmCltIdCi() {
		return this.rmmCltIdCi;
	}

	public void setRmmCltId(String rmmCltId) {
		this.rmmCltId = Functions.subString(rmmCltId, Len.RMM_CLT_ID);
	}

	public String getRmmCltId() {
		return this.rmmCltId;
	}

	public void setDfoTerNbrCi(char dfoTerNbrCi) {
		this.dfoTerNbrCi = dfoTerNbrCi;
	}

	public char getDfoTerNbrCi() {
		return this.dfoTerNbrCi;
	}

	public void setDfoTerNbr(String dfoTerNbr) {
		this.dfoTerNbr = Functions.subString(dfoTerNbr, Len.DFO_TER_NBR);
	}

	public String getDfoTerNbr() {
		return this.dfoTerNbr;
	}

	public void setDfoTerCltIdCi(char dfoTerCltIdCi) {
		this.dfoTerCltIdCi = dfoTerCltIdCi;
	}

	public char getDfoTerCltIdCi() {
		return this.dfoTerCltIdCi;
	}

	public void setDfoTerCltId(String dfoTerCltId) {
		this.dfoTerCltId = Functions.subString(dfoTerCltId, Len.DFO_TER_CLT_ID);
	}

	public String getDfoTerCltId() {
		return this.dfoTerCltId;
	}

	public void setDfoNmPfxCi(char dfoNmPfxCi) {
		this.dfoNmPfxCi = dfoNmPfxCi;
	}

	public char getDfoNmPfxCi() {
		return this.dfoNmPfxCi;
	}

	public void setDfoNmPfx(String dfoNmPfx) {
		this.dfoNmPfx = Functions.subString(dfoNmPfx, Len.DFO_NM_PFX);
	}

	public String getDfoNmPfx() {
		return this.dfoNmPfx;
	}

	public void setDfoFstNmCi(char dfoFstNmCi) {
		this.dfoFstNmCi = dfoFstNmCi;
	}

	public char getDfoFstNmCi() {
		return this.dfoFstNmCi;
	}

	public void setDfoFstNm(String dfoFstNm) {
		this.dfoFstNm = Functions.subString(dfoFstNm, Len.DFO_FST_NM);
	}

	public String getDfoFstNm() {
		return this.dfoFstNm;
	}

	public void setDfoMdlNmCi(char dfoMdlNmCi) {
		this.dfoMdlNmCi = dfoMdlNmCi;
	}

	public char getDfoMdlNmCi() {
		return this.dfoMdlNmCi;
	}

	public void setDfoMdlNm(String dfoMdlNm) {
		this.dfoMdlNm = Functions.subString(dfoMdlNm, Len.DFO_MDL_NM);
	}

	public String getDfoMdlNm() {
		return this.dfoMdlNm;
	}

	public void setDfoLstNmCi(char dfoLstNmCi) {
		this.dfoLstNmCi = dfoLstNmCi;
	}

	public char getDfoLstNmCi() {
		return this.dfoLstNmCi;
	}

	public void setDfoLstNm(String dfoLstNm) {
		this.dfoLstNm = Functions.subString(dfoLstNm, Len.DFO_LST_NM);
	}

	public String getDfoLstNm() {
		return this.dfoLstNm;
	}

	public void setDfoNmCi(char dfoNmCi) {
		this.dfoNmCi = dfoNmCi;
	}

	public char getDfoNmCi() {
		return this.dfoNmCi;
	}

	public void setDfoNm(String dfoNm) {
		this.dfoNm = Functions.subString(dfoNm, Len.DFO_NM);
	}

	public String getDfoNm() {
		return this.dfoNm;
	}

	public void setDfoNmSfxCi(char dfoNmSfxCi) {
		this.dfoNmSfxCi = dfoNmSfxCi;
	}

	public char getDfoNmSfxCi() {
		return this.dfoNmSfxCi;
	}

	public void setDfoNmSfx(String dfoNmSfx) {
		this.dfoNmSfx = Functions.subString(dfoNmSfx, Len.DFO_NM_SFX);
	}

	public String getDfoNmSfx() {
		return this.dfoNmSfx;
	}

	public void setDfoCltIdCi(char dfoCltIdCi) {
		this.dfoCltIdCi = dfoCltIdCi;
	}

	public char getDfoCltIdCi() {
		return this.dfoCltIdCi;
	}

	public void setDfoCltId(String dfoCltId) {
		this.dfoCltId = Functions.subString(dfoCltId, Len.DFO_CLT_ID);
	}

	public String getDfoCltId() {
		return this.dfoCltId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MR_TER_NBR = 4;
		public static final int MR_TER_CLT_ID = 20;
		public static final int MR_NM_PFX = 4;
		public static final int MR_FST_NM = 30;
		public static final int MR_MDL_NM = 30;
		public static final int MR_LST_NM = 60;
		public static final int MR_NM = 120;
		public static final int MR_NM_SFX = 4;
		public static final int MR_CLT_ID = 20;
		public static final int CSR_TER_NBR = 4;
		public static final int CSR_TER_CLT_ID = 20;
		public static final int CSR_NM_PFX = 4;
		public static final int CSR_FST_NM = 30;
		public static final int CSR_MDL_NM = 30;
		public static final int CSR_LST_NM = 60;
		public static final int CSR_NM = 120;
		public static final int CSR_NM_SFX = 4;
		public static final int CSR_CLT_ID = 20;
		public static final int SMR_TER_NBR = 4;
		public static final int SMR_TER_CLT_ID = 20;
		public static final int SMR_NM_PFX = 4;
		public static final int SMR_FST_NM = 30;
		public static final int SMR_MDL_NM = 30;
		public static final int SMR_LST_NM = 60;
		public static final int SMR_NM = 120;
		public static final int SMR_NM_SFX = 4;
		public static final int SMR_CLT_ID = 20;
		public static final int DMM_TER_NBR = 4;
		public static final int DMM_TER_CLT_ID = 20;
		public static final int DMM_NM_PFX = 4;
		public static final int DMM_FST_NM = 30;
		public static final int DMM_MDL_NM = 30;
		public static final int DMM_LST_NM = 60;
		public static final int DMM_NM = 120;
		public static final int DMM_NM_SFX = 4;
		public static final int DMM_CLT_ID = 20;
		public static final int RMM_TER_NBR = 4;
		public static final int RMM_TER_CLT_ID = 20;
		public static final int RMM_NM_PFX = 4;
		public static final int RMM_FST_NM = 30;
		public static final int RMM_MDL_NM = 30;
		public static final int RMM_LST_NM = 60;
		public static final int RMM_NM = 120;
		public static final int RMM_NM_SFX = 4;
		public static final int RMM_CLT_ID = 20;
		public static final int DFO_TER_NBR = 4;
		public static final int DFO_TER_CLT_ID = 20;
		public static final int DFO_NM_PFX = 4;
		public static final int DFO_FST_NM = 30;
		public static final int DFO_MDL_NM = 30;
		public static final int DFO_LST_NM = 60;
		public static final int DFO_NM = 120;
		public static final int DFO_NM_SFX = 4;
		public static final int DFO_CLT_ID = 20;
		public static final int MR_TER_NBR_CI = 1;
		public static final int MR_TER_CLT_ID_CI = 1;
		public static final int MR_NM_PFX_CI = 1;
		public static final int MR_FST_NM_CI = 1;
		public static final int MR_MDL_NM_CI = 1;
		public static final int MR_LST_NM_CI = 1;
		public static final int MR_NM_CI = 1;
		public static final int MR_NM_SFX_CI = 1;
		public static final int MR_CLT_ID_CI = 1;
		public static final int CSR_TER_NBR_CI = 1;
		public static final int CSR_TER_CLT_ID_CI = 1;
		public static final int CSR_NM_PFX_CI = 1;
		public static final int CSR_FST_NM_CI = 1;
		public static final int CSR_MDL_NM_CI = 1;
		public static final int CSR_LST_NM_CI = 1;
		public static final int CSR_NM_CI = 1;
		public static final int CSR_NM_SFX_CI = 1;
		public static final int CSR_CLT_ID_CI = 1;
		public static final int SMR_TER_NBR_CI = 1;
		public static final int SMR_TER_CLT_ID_CI = 1;
		public static final int SMR_NM_PFX_CI = 1;
		public static final int SMR_FST_NM_CI = 1;
		public static final int SMR_MDL_NM_CI = 1;
		public static final int SMR_LST_NM_CI = 1;
		public static final int SMR_NM_CI = 1;
		public static final int SMR_NM_SFX_CI = 1;
		public static final int SMR_CLT_ID_CI = 1;
		public static final int DMM_TER_NBR_CI = 1;
		public static final int DMM_TER_CLT_ID_CI = 1;
		public static final int DMM_NM_PFX_CI = 1;
		public static final int DMM_FST_NM_CI = 1;
		public static final int DMM_MDL_NM_CI = 1;
		public static final int DMM_LST_NM_CI = 1;
		public static final int DMM_NM_CI = 1;
		public static final int DMM_NM_SFX_CI = 1;
		public static final int DMM_CLT_ID_CI = 1;
		public static final int RMM_TER_NBR_CI = 1;
		public static final int RMM_TER_CLT_ID_CI = 1;
		public static final int RMM_NM_PFX_CI = 1;
		public static final int RMM_FST_NM_CI = 1;
		public static final int RMM_MDL_NM_CI = 1;
		public static final int RMM_LST_NM_CI = 1;
		public static final int RMM_NM_CI = 1;
		public static final int RMM_NM_SFX_CI = 1;
		public static final int RMM_CLT_ID_CI = 1;
		public static final int DFO_TER_NBR_CI = 1;
		public static final int DFO_TER_CLT_ID_CI = 1;
		public static final int DFO_NM_PFX_CI = 1;
		public static final int DFO_FST_NM_CI = 1;
		public static final int DFO_MDL_NM_CI = 1;
		public static final int DFO_LST_NM_CI = 1;
		public static final int DFO_NM_CI = 1;
		public static final int DFO_NM_SFX_CI = 1;
		public static final int DFO_CLT_ID_CI = 1;
		public static final int CLT_MUT_TER_STC_DATA = MR_TER_NBR_CI + MR_TER_NBR + MR_TER_CLT_ID_CI + MR_TER_CLT_ID + MR_NM_PFX_CI + MR_NM_PFX
				+ MR_FST_NM_CI + MR_FST_NM + MR_MDL_NM_CI + MR_MDL_NM + MR_LST_NM_CI + MR_LST_NM + MR_NM_CI + MR_NM + MR_NM_SFX_CI + MR_NM_SFX
				+ MR_CLT_ID_CI + MR_CLT_ID + CSR_TER_NBR_CI + CSR_TER_NBR + CSR_TER_CLT_ID_CI + CSR_TER_CLT_ID + CSR_NM_PFX_CI + CSR_NM_PFX
				+ CSR_FST_NM_CI + CSR_FST_NM + CSR_MDL_NM_CI + CSR_MDL_NM + CSR_LST_NM_CI + CSR_LST_NM + CSR_NM_CI + CSR_NM + CSR_NM_SFX_CI
				+ CSR_NM_SFX + CSR_CLT_ID_CI + CSR_CLT_ID + SMR_TER_NBR_CI + SMR_TER_NBR + SMR_TER_CLT_ID_CI + SMR_TER_CLT_ID + SMR_NM_PFX_CI
				+ SMR_NM_PFX + SMR_FST_NM_CI + SMR_FST_NM + SMR_MDL_NM_CI + SMR_MDL_NM + SMR_LST_NM_CI + SMR_LST_NM + SMR_NM_CI + SMR_NM
				+ SMR_NM_SFX_CI + SMR_NM_SFX + SMR_CLT_ID_CI + SMR_CLT_ID + DMM_TER_NBR_CI + DMM_TER_NBR + DMM_TER_CLT_ID_CI + DMM_TER_CLT_ID
				+ DMM_NM_PFX_CI + DMM_NM_PFX + DMM_FST_NM_CI + DMM_FST_NM + DMM_MDL_NM_CI + DMM_MDL_NM + DMM_LST_NM_CI + DMM_LST_NM + DMM_NM_CI
				+ DMM_NM + DMM_NM_SFX_CI + DMM_NM_SFX + DMM_CLT_ID_CI + DMM_CLT_ID + RMM_TER_NBR_CI + RMM_TER_NBR + RMM_TER_CLT_ID_CI + RMM_TER_CLT_ID
				+ RMM_NM_PFX_CI + RMM_NM_PFX + RMM_FST_NM_CI + RMM_FST_NM + RMM_MDL_NM_CI + RMM_MDL_NM + RMM_LST_NM_CI + RMM_LST_NM + RMM_NM_CI
				+ RMM_NM + RMM_NM_SFX_CI + RMM_NM_SFX + RMM_CLT_ID_CI + RMM_CLT_ID + DFO_TER_NBR_CI + DFO_TER_NBR + DFO_TER_CLT_ID_CI + DFO_TER_CLT_ID
				+ DFO_NM_PFX_CI + DFO_NM_PFX + DFO_FST_NM_CI + DFO_FST_NM + DFO_MDL_NM_CI + DFO_MDL_NM + DFO_LST_NM_CI + DFO_LST_NM + DFO_NM_CI
				+ DFO_NM + DFO_NM_SFX_CI + DFO_NM_SFX + DFO_CLT_ID_CI + DFO_CLT_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
