/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program XZC03090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsXzc03090 {

	//==== PROPERTIES ====
	//Original name: SS-PI
	private short pi = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-CI
	private short ci = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-EM
	private short em = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setPi(short pi) {
		this.pi = pi;
	}

	public short getPi() {
		return this.pi;
	}

	public void setCi(short ci) {
		this.ci = ci;
	}

	public short getCi() {
		return this.ci;
	}

	public void setEm(short em) {
		this.em = em;
	}

	public short getEm() {
		return this.em;
	}
}
