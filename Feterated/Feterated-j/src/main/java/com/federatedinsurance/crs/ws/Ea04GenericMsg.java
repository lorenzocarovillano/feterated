/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-04-GENERIC-MSG<br>
 * Variable: EA-04-GENERIC-MSG from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea04GenericMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-04-GENERIC-MSG
	private String flr1 = "XZ004000 -";
	//Original name: FILLER-EA-04-GENERIC-MSG-1
	private String flr2 = "ERROR IN PARA :";
	//Original name: EA-04-PARA
	private String para = DefaultValues.stringVal(Len.PARA);
	//Original name: FILLER-EA-04-GENERIC-MSG-2
	private char flr3 = ' ';
	//Original name: EA-04-MSG
	private String msg = DefaultValues.stringVal(Len.MSG);

	//==== METHODS ====
	public String getEa04GenericMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa04GenericMsgBytes());
	}

	public byte[] getEa04GenericMsgBytes() {
		byte[] buffer = new byte[Len.EA04_GENERIC_MSG];
		return getEa04GenericMsgBytes(buffer, 1);
	}

	public byte[] getEa04GenericMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, para, Len.PARA);
		position += Len.PARA;
		MarshalByte.writeChar(buffer, position, flr3);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, msg, Len.MSG);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setPara(String para) {
		this.para = Functions.subString(para, Len.PARA);
	}

	public String getPara() {
		return this.para;
	}

	public char getFlr3() {
		return this.flr3;
	}

	public void setMsg(String msg) {
		this.msg = Functions.subString(msg, Len.MSG);
	}

	public String getMsg() {
		return this.msg;
	}

	public String getMsgFormatted() {
		return Functions.padBlanks(getMsg(), Len.MSG);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PARA = 4;
		public static final int MSG = 80;
		public static final int FLR1 = 11;
		public static final int FLR2 = 15;
		public static final int FLR3 = 1;
		public static final int EA04_GENERIC_MSG = PARA + MSG + FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
