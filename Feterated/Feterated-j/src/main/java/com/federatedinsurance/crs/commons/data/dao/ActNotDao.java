/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNot;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ACT_NOT]
 * 
 */
public class ActNotDao extends BaseSqlDao<IActNot> {

	private Cursor actNotCsr1;
	private Cursor actNotCsr2;
	private final IRowMapper<IActNot> fetchActNotCsr1Rm = buildNamedRowMapper(IActNot.class, "csrActNbr", "notPrcTs");
	private final IRowMapper<IActNot> selectRec2Rm = buildNamedRowMapper(IActNot.class, "notPrcTs", "saNotPrcTsTime", "notDt", "totFeeAmtObj",
			"stAbbObj");
	private final IRowMapper<IActNot> fetchActNotCsr2Rm = buildNamedRowMapper(IActNot.class, "csrActNbr", "notPrcTs", "actNotTypCd", "notDt",
			"actOwnCltId", "actOwnAdrId", "empIdObj", "staMdfTs", "actNotStaCd", "pdcNbrObj", "pdcNmObj", "segCdObj", "actTypCdObj", "totFeeAmtObj",
			"stAbbObj", "cerHldNotIndObj", "addCncDayObj", "reaDesObj");

	public ActNotDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNot> getToClass() {
		return IActNot.class;
	}

	public DbAccessStatus openActNotCsr1(String actNotStaCd) {
		actNotCsr1 = buildQuery("openActNotCsr1").bind("actNotStaCd", actNotStaCd).open();
		return dbStatus;
	}

	public IActNot fetchActNotCsr1(IActNot iActNot) {
		return fetch(actNotCsr1, iActNot, fetchActNotCsr1Rm);
	}

	public DbAccessStatus closeActNotCsr1() {
		return closeCursor(actNotCsr1);
	}

	public IActNot selectRec(String csrActNbr, String staMdfTs, IActNot iActNot) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("staMdfTs", staMdfTs).singleResult(iActNot);
	}

	public IActNot selectRec1(String csrActNbr, String notPrcTs, IActNot iActNot) {
	}

	public IActNot selectRec2(String srActNbr, String fImpCnCd, String fStatusDeleted, String fAddedByBusinessWorks, IActNot iActNot) {
		return buildQuery("selectRec2").bind("srActNbr", srActNbr).bind("fImpCnCd", fImpCnCd).bind("fStatusDeleted", fStatusDeleted)
				.bind("fAddedByBusinessWorks", fAddedByBusinessWorks).rowMapper(selectRec2Rm).singleResult(iActNot);
	}

	public DbAccessStatus openActNotCsr2(String csrActNbr, String notPrcTs) {
		actNotCsr2 = buildQuery("openActNotCsr2").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public DbAccessStatus closeActNotCsr2() {
		return closeCursor(actNotCsr2);
	}

	public IActNot fetchActNotCsr2(IActNot iActNot) {
		return fetch(actNotCsr2, iActNot, fetchActNotCsr2Rm);
	}

	public DbAccessStatus insertRec(IActNot iActNot) {
		return buildQuery("insertRec").bind(iActNot).executeInsert();
	}

	public IActNot selectRec3(String csrActNbr, String notPrcTs, IActNot iActNot) {
		return buildQuery("selectRec3").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).singleResult(iActNot);
	}

	public IActNot selectRec4(String csrActNbr, String notPrcTs, IActNot iActNot) {
	}

	public IActNot selectRec5(String csrActNbr, String notPrcTs, IActNot iActNot) {
		return buildQuery("selectRec5").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).singleResult(iActNot);
	}

	public String selectRec6(String csrActNbr, String notPrcTs, String dft) {
		return buildQuery("selectRec6").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).scalarResultString(dft);
	}

	public IActNot selectRec7(String csrActNbr, String notPrcTs, IActNot iActNot) {
		return buildQuery("selectRec7").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).singleResult(iActNot);
	}

	public DbAccessStatus updateRec(String actNotStaCd, String csrActNbr, String notPrcTs) {
		return buildQuery("updateRec").bind("actNotStaCd", actNotStaCd).bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).executeUpdate();
	}

	public String selectRec8(String csrActNbr, String notPrcTs, String dft) {
		return buildQuery("selectRec8").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).scalarResultString(dft);
	}

	public DbAccessStatus updateRec1(IActNot iActNot) {
		return buildQuery("updateRec1").bind(iActNot).executeUpdate();
	}

	public DbAccessStatus deleteRec(String csrActNbr, String notPrcTs) {
		return buildQuery("deleteRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).executeDelete();
	}

	public String selectRec9(String xzh001CsrActNbr, String notPrcTs, String dft) {
		return buildQuery("selectRec9").bind("xzh001CsrActNbr", xzh001CsrActNbr).bind("notPrcTs", notPrcTs).scalarResultString(dft);
	}
}
