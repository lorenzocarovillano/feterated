/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IActNotWrdStDes;
import com.federatedinsurance.crs.copy.DclactNotWrd;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclstWrdDes;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.SwEndOfCursorWordFlag;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9010<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9010Data implements IActNotWrdStDes {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b9010 constantFields = new ConstantFieldsXz0b9010();
	//Original name: EA-01-NOTHING-FOUND-MSG
	private Ea01NothingFoundMsg ea01NothingFoundMsg = new Ea01NothingFoundMsg();
	/**Original name: NI-ST-WRD-FONT-TYP<br>
	 * <pre>**  NULL-INDICATORS.</pre>*/
	private short niStWrdFontTyp = DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-END-OF-CURSOR-WORD-FLAG
	private SwEndOfCursorWordFlag swEndOfCursorWordFlag = new SwEndOfCursorWordFlag();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9010 workingStorageArea = new WorkingStorageAreaXz0b9010();
	//Original name: WS-XZ0A9010-ROW
	private WsXz0a9010Row wsXz0a9010Row = new WsXz0a9010Row();
	//Original name: WS-XZ0A9011-ROW
	private WsXz0a9011Row wsXz0a9011Row = new WsXz0a9011Row();
	//Original name: DCLACT-NOT-WRD
	private DclactNotWrd dclactNotWrd = new DclactNotWrd();
	//Original name: DCLST-WRD-DES
	private DclstWrdDes dclstWrdDes = new DclstWrdDes();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public void setNiStWrdFontTyp(short niStWrdFontTyp) {
		this.niStWrdFontTyp = niStWrdFontTyp;
	}

	public short getNiStWrdFontTyp() {
		return this.niStWrdFontTyp;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public ConstantFieldsXz0b9010 getConstantFields() {
		return constantFields;
	}

	public DclactNotWrd getDclactNotWrd() {
		return dclactNotWrd;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclstWrdDes getDclstWrdDes() {
		return dclstWrdDes;
	}

	public Ea01NothingFoundMsg getEa01NothingFoundMsg() {
		return ea01NothingFoundMsg;
	}

	@Override
	public String getFontTyp() {
		return dclstWrdDes.getStWrdFontTyp();
	}

	@Override
	public void setFontTyp(String fontTyp) {
		this.dclstWrdDes.setStWrdFontTyp(fontTyp);
	}

	@Override
	public String getFontTypObj() {
		if (getNiStWrdFontTyp() >= 0) {
			return getFontTyp();
		} else {
			return null;
		}
	}

	@Override
	public void setFontTypObj(String fontTypObj) {
		if (fontTypObj != null) {
			setFontTyp(fontTypObj);
			setNiStWrdFontTyp(((short) 0));
		} else {
			setNiStWrdFontTyp(((short) -1));
		}
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getSeqCd() {
		return dclstWrdDes.getStWrdSeqCd();
	}

	@Override
	public void setSeqCd(String seqCd) {
		this.dclstWrdDes.setStWrdSeqCd(seqCd);
	}

	public SwEndOfCursorWordFlag getSwEndOfCursorWordFlag() {
		return swEndOfCursorWordFlag;
	}

	@Override
	public String getTxt() {
		return FixedStrings.get(dclstWrdDes.getStWrdTxtText(), dclstWrdDes.getStWrdTxtLen());
	}

	@Override
	public void setTxt(String txt) {
		this.dclstWrdDes.setStWrdTxtText(txt);
		this.dclstWrdDes.setStWrdTxtLen((((short) strLen(txt))));
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b9010 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9010Row getWsXz0a9010Row() {
		return wsXz0a9010Row;
	}

	public WsXz0a9011Row getWsXz0a9011Row() {
		return wsXz0a9011Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
