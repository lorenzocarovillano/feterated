/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.UlliLegacyLockFunc;
import com.federatedinsurance.crs.ws.enums.UlliLegacyLockResp;

/**Original name: HALLULLI<br>
 * Variable: HALLULLI from copybook HALLULLI<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Hallulli {

	//==== PROPERTIES ====
	//Original name: ULLI-TCH-KEY
	private String tchKey = DefaultValues.stringVal(Len.TCH_KEY);
	//Original name: ULLI-APP-ID
	private String appId = DefaultValues.stringVal(Len.APP_ID);
	//Original name: ULLI-TABLE-NM
	private String tableNm = DefaultValues.stringVal(Len.TABLE_NM);
	//Original name: ULLI-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: ULLI-LEGACY-LOCK-FUNC
	private UlliLegacyLockFunc legacyLockFunc = new UlliLegacyLockFunc();
	//Original name: ULLI-EXPIRATION-TS
	private String expirationTs = DefaultValues.stringVal(Len.EXPIRATION_TS);
	//Original name: ULLI-LEGACY-LOCK-USERID
	private String legacyLockUserid = DefaultValues.stringVal(Len.LEGACY_LOCK_USERID);
	//Original name: ULLI-LEGACY-LOCK-RESP
	private UlliLegacyLockResp legacyLockResp = new UlliLegacyLockResp();

	//==== METHODS ====
	public void setLockDrvrInputBytes(byte[] buffer, int offset) {
		int position = offset;
		tchKey = MarshalByte.readString(buffer, position, Len.TCH_KEY);
		position += Len.TCH_KEY;
		appId = MarshalByte.readString(buffer, position, Len.APP_ID);
		position += Len.APP_ID;
		tableNm = MarshalByte.readString(buffer, position, Len.TABLE_NM);
		position += Len.TABLE_NM;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
	}

	public byte[] getLockDrvrInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, tchKey, Len.TCH_KEY);
		position += Len.TCH_KEY;
		MarshalByte.writeString(buffer, position, appId, Len.APP_ID);
		position += Len.APP_ID;
		MarshalByte.writeString(buffer, position, tableNm, Len.TABLE_NM);
		position += Len.TABLE_NM;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		return buffer;
	}

	public void setTchKey(String tchKey) {
		this.tchKey = Functions.subString(tchKey, Len.TCH_KEY);
	}

	public String getTchKey() {
		return this.tchKey;
	}

	public String getTchKeyFormatted() {
		return Functions.padBlanks(getTchKey(), Len.TCH_KEY);
	}

	public void setAppId(String appId) {
		this.appId = Functions.subString(appId, Len.APP_ID);
	}

	public String getAppId() {
		return this.appId;
	}

	public void setTableNm(String tableNm) {
		this.tableNm = Functions.subString(tableNm, Len.TABLE_NM);
	}

	public String getTableNm() {
		return this.tableNm;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public String getBusObjNmFormatted() {
		return Functions.padBlanks(getBusObjNm(), Len.BUS_OBJ_NM);
	}

	public void setLegacyLockInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		legacyLockFunc.setLegacyLockFunc(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		expirationTs = MarshalByte.readString(buffer, position, Len.EXPIRATION_TS);
		position += Len.EXPIRATION_TS;
		legacyLockUserid = MarshalByte.readString(buffer, position, Len.LEGACY_LOCK_USERID);
		position += Len.LEGACY_LOCK_USERID;
		legacyLockResp.setLegacyLockResp(MarshalByte.readChar(buffer, position));
	}

	public byte[] getLegacyLockInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, legacyLockFunc.getLegacyLockFunc());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationTs, Len.EXPIRATION_TS);
		position += Len.EXPIRATION_TS;
		MarshalByte.writeString(buffer, position, legacyLockUserid, Len.LEGACY_LOCK_USERID);
		position += Len.LEGACY_LOCK_USERID;
		MarshalByte.writeChar(buffer, position, legacyLockResp.getLegacyLockResp());
		return buffer;
	}

	public void setExpirationTs(String expirationTs) {
		this.expirationTs = Functions.subString(expirationTs, Len.EXPIRATION_TS);
	}

	public String getExpirationTs() {
		return this.expirationTs;
	}

	public void setLegacyLockUserid(String legacyLockUserid) {
		this.legacyLockUserid = Functions.subString(legacyLockUserid, Len.LEGACY_LOCK_USERID);
	}

	public String getLegacyLockUserid() {
		return this.legacyLockUserid;
	}

	public UlliLegacyLockFunc getLegacyLockFunc() {
		return legacyLockFunc;
	}

	public UlliLegacyLockResp getLegacyLockResp() {
		return legacyLockResp;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TCH_KEY = 126;
		public static final int APP_ID = 10;
		public static final int TABLE_NM = 18;
		public static final int BUS_OBJ_NM = 32;
		public static final int EXPIRATION_TS = 26;
		public static final int LEGACY_LOCK_USERID = 32;
		public static final int LOCK_DRVR_INPUT = TCH_KEY + APP_ID + TABLE_NM + BUS_OBJ_NM;
		public static final int LEGACY_LOCK_INFO = UlliLegacyLockFunc.Len.LEGACY_LOCK_FUNC + EXPIRATION_TS + LEGACY_LOCK_USERID
				+ UlliLegacyLockResp.Len.LEGACY_LOCK_RESP;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
