/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;

/**Original name: L-SC-STA-CD<br>
 * Variable: L-SC-STA-CD from copybook TT008001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LScStaCd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char ACY = 'A';
	public static final char ALL = 'L';
	public static final char N_A = Types.SPACE_CHAR;

	//==== METHODS ====
	public void setStaCd(char staCd) {
		this.value = staCd;
	}

	public char getStaCd() {
		return this.value;
	}

	public void setAcy() {
		value = ACY;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STA_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
