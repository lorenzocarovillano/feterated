/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9090<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9090 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9090_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9090() {
	}

	public LFrameworkResponseAreaXz0r9090(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a9090Formatted(int lFwRespXz0a9090Idx) {
		int position = Pos.lFwRespXz0a9090(lFwRespXz0a9090Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9090);
	}

	public void setlFwRespXz0a9090Bytes(int lFwRespXz0a9090Idx, byte[] buffer) {
		setlFwRespXz0a9090Bytes(lFwRespXz0a9090Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A9090<br>*/
	public byte[] getlFwRespXz0a9090Bytes(int lFwRespXz0a9090Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_XZ0A9090];
		return getlFwRespXz0a9090Bytes(lFwRespXz0a9090Idx, buffer, 1);
	}

	public void setlFwRespXz0a9090Bytes(int lFwRespXz0a9090Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9090(lFwRespXz0a9090Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A9090, position);
	}

	public byte[] getlFwRespXz0a9090Bytes(int lFwRespXz0a9090Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9090(lFwRespXz0a9090Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A9090, position);
		return buffer;
	}

	public void setXza990rMaxNotCoRows(int xza990rMaxNotCoRowsIdx, short xza990rMaxNotCoRows) {
		int position = Pos.xza990MaxNotCoRows(xza990rMaxNotCoRowsIdx - 1);
		writeBinaryShort(position, xza990rMaxNotCoRows);
	}

	/**Original name: XZA990R-MAX-NOT-CO-ROWS<br>*/
	public short getXza990rMaxNotCoRows(int xza990rMaxNotCoRowsIdx) {
		int position = Pos.xza990MaxNotCoRows(xza990rMaxNotCoRowsIdx - 1);
		return readBinaryShort(position);
	}

	public void setXza990rActNotTypCd(int xza990rActNotTypCdIdx, String xza990rActNotTypCd) {
		int position = Pos.xza990ActNotTypCd(xza990rActNotTypCdIdx - 1);
		writeString(position, xza990rActNotTypCd, Len.XZA990_ACT_NOT_TYP_CD);
	}

	/**Original name: XZA990R-ACT-NOT-TYP-CD<br>*/
	public String getXza990rActNotTypCd(int xza990rActNotTypCdIdx) {
		int position = Pos.xza990ActNotTypCd(xza990rActNotTypCdIdx - 1);
		return readString(position, Len.XZA990_ACT_NOT_TYP_CD);
	}

	public void setXza990rCoCd(int xza990rCoCdIdx, String xza990rCoCd) {
		int position = Pos.xza990CoCd(xza990rCoCdIdx - 1);
		writeString(position, xza990rCoCd, Len.XZA990_CO_CD);
	}

	/**Original name: XZA990R-CO-CD<br>*/
	public String getXza990rCoCd(int xza990rCoCdIdx) {
		int position = Pos.xza990CoCd(xza990rCoCdIdx - 1);
		return readString(position, Len.XZA990_CO_CD);
	}

	public void setXza990rCoDes(int xza990rCoDesIdx, String xza990rCoDes) {
		int position = Pos.xza990CoDes(xza990rCoDesIdx - 1);
		writeString(position, xza990rCoDes, Len.XZA990_CO_DES);
	}

	/**Original name: XZA990R-CO-DES<br>*/
	public String getXza990rCoDes(int xza990rCoDesIdx) {
		int position = Pos.xza990CoDes(xza990rCoDesIdx - 1);
		return readString(position, Len.XZA990_CO_DES);
	}

	public void setXza990rSpeCrtCd(int xza990rSpeCrtCdIdx, String xza990rSpeCrtCd) {
		int position = Pos.xza990SpeCrtCd(xza990rSpeCrtCdIdx - 1);
		writeString(position, xza990rSpeCrtCd, Len.XZA990_SPE_CRT_CD);
	}

	/**Original name: XZA990R-SPE-CRT-CD<br>*/
	public String getXza990rSpeCrtCd(int xza990rSpeCrtCdIdx) {
		int position = Pos.xza990SpeCrtCd(xza990rSpeCrtCdIdx - 1);
		return readString(position, Len.XZA990_SPE_CRT_CD);
	}

	public void setXza990rCoOfsNbr(int xza990rCoOfsNbrIdx, int xza990rCoOfsNbr) {
		int position = Pos.xza990CoOfsNbr(xza990rCoOfsNbrIdx - 1);
		writeInt(position, xza990rCoOfsNbr, Len.Int.XZA990R_CO_OFS_NBR);
	}

	/**Original name: XZA990R-CO-OFS-NBR<br>*/
	public int getXza990rCoOfsNbr(int xza990rCoOfsNbrIdx) {
		int position = Pos.xza990CoOfsNbr(xza990rCoOfsNbrIdx - 1);
		return readNumDispInt(position, Len.XZA990_CO_OFS_NBR);
	}

	public void setXza990rDtbCd(int xza990rDtbCdIdx, char xza990rDtbCd) {
		int position = Pos.xza990DtbCd(xza990rDtbCdIdx - 1);
		writeChar(position, xza990rDtbCd);
	}

	/**Original name: XZA990R-DTB-CD<br>*/
	public char getXza990rDtbCd(int xza990rDtbCdIdx) {
		int position = Pos.xza990DtbCd(xza990rDtbCdIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9090(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9090;
		}

		public static int xza990NotCoInfo(int idx) {
			return lFwRespXz0a9090(idx);
		}

		public static int xza990MaxNotCoRows(int idx) {
			return xza990NotCoInfo(idx);
		}

		public static int xza990NotCoRow(int idx) {
			return xza990MaxNotCoRows(idx) + Len.XZA990_MAX_NOT_CO_ROWS;
		}

		public static int xza990ActNotTypCd(int idx) {
			return xza990NotCoRow(idx);
		}

		public static int xza990CoCd(int idx) {
			return xza990ActNotTypCd(idx) + Len.XZA990_ACT_NOT_TYP_CD;
		}

		public static int xza990CoDes(int idx) {
			return xza990CoCd(idx) + Len.XZA990_CO_CD;
		}

		public static int xza990SpeCrtCd(int idx) {
			return xza990CoDes(idx) + Len.XZA990_CO_DES;
		}

		public static int xza990CoOfsNbr(int idx) {
			return xza990SpeCrtCd(idx) + Len.XZA990_SPE_CRT_CD;
		}

		public static int xza990DtbCd(int idx) {
			return xza990CoOfsNbr(idx) + Len.XZA990_CO_OFS_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA990_MAX_NOT_CO_ROWS = 2;
		public static final int XZA990_ACT_NOT_TYP_CD = 5;
		public static final int XZA990_CO_CD = 5;
		public static final int XZA990_CO_DES = 30;
		public static final int XZA990_SPE_CRT_CD = 5;
		public static final int XZA990_CO_OFS_NBR = 5;
		public static final int XZA990_DTB_CD = 1;
		public static final int XZA990_NOT_CO_ROW = XZA990_ACT_NOT_TYP_CD + XZA990_CO_CD + XZA990_CO_DES + XZA990_SPE_CRT_CD + XZA990_CO_OFS_NBR
				+ XZA990_DTB_CD;
		public static final int XZA990_NOT_CO_INFO = XZA990_MAX_NOT_CO_ROWS + XZA990_NOT_CO_ROW;
		public static final int L_FW_RESP_XZ0A9090 = XZA990_NOT_CO_INFO;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0r9090.L_FW_RESP_XZ0A9090_MAXOCCURS * L_FW_RESP_XZ0A9090;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA990R_CO_OFS_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
