/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-07-TAR-SYS-NOT-FND<br>
 * Variable: EA-07-TAR-SYS-NOT-FND from program FNC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea07TarSysNotFnd {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-07-TAR-SYS-NOT-FND
	private String flr1 = "****";
	//Original name: FILLER-EA-07-TAR-SYS-NOT-FND-1
	private String flr2 = "FNC02090 -";
	//Original name: FILLER-EA-07-TAR-SYS-NOT-FND-2
	private String flr3 = "UNABLE TO";
	//Original name: FILLER-EA-07-TAR-SYS-NOT-FND-3
	private String flr4 = "DETERMINE";
	//Original name: FILLER-EA-07-TAR-SYS-NOT-FND-4
	private String flr5 = "TARGET SYSTEM.";
	//Original name: FILLER-EA-07-TAR-SYS-NOT-FND-5
	private String flr6 = "CICS RGN:";
	//Original name: EA-07-CICS-RGN
	private String ea07CicsRgn = DefaultValues.stringVal(Len.EA07_CICS_RGN);

	//==== METHODS ====
	public String getEa07TarSysNotFndFormatted() {
		return MarshalByteExt.bufferToStr(getEa07TarSysNotFndBytes());
	}

	public byte[] getEa07TarSysNotFndBytes() {
		byte[] buffer = new byte[Len.EA07_TAR_SYS_NOT_FND];
		return getEa07TarSysNotFndBytes(buffer, 1);
	}

	public byte[] getEa07TarSysNotFndBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, ea07CicsRgn, Len.EA07_CICS_RGN);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setEa07CicsRgn(String ea07CicsRgn) {
		this.ea07CicsRgn = Functions.subString(ea07CicsRgn, Len.EA07_CICS_RGN);
	}

	public String getEa07CicsRgn() {
		return this.ea07CicsRgn;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA07_CICS_RGN = 8;
		public static final int FLR1 = 5;
		public static final int FLR2 = 11;
		public static final int FLR3 = 10;
		public static final int FLR5 = 15;
		public static final int EA07_TAR_SYS_NOT_FND = EA07_CICS_RGN + FLR1 + FLR2 + 3 * FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
