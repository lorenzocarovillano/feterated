/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-REQ-UMT-ROW-UPDATED-SW<br>
 * Variable: WS-REQ-UMT-ROW-UPDATED-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsReqUmtRowUpdatedSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char REQ_UMT_ROW_UPDATED = 'Y';
	public static final char NOT_REQ_UMT_ROW_UPDATED = 'N';

	//==== METHODS ====
	public void setReqUmtRowUpdatedSw(char reqUmtRowUpdatedSw) {
		this.value = reqUmtRowUpdatedSw;
	}

	public char getReqUmtRowUpdatedSw() {
		return this.value;
	}

	public void setWsReqUmtRowUpdated() {
		value = REQ_UMT_ROW_UPDATED;
	}

	public boolean isWsNotReqUmtRowUpdated() {
		return value == NOT_REQ_UMT_ROW_UPDATED;
	}

	public void setWsNotReqUmtRowUpdated() {
		value = NOT_REQ_UMT_ROW_UPDATED;
	}
}
