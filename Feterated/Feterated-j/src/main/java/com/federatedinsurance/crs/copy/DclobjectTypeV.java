/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLOBJECT-TYPE-V<br>
 * Variable: DCLOBJECT-TYPE-V from copybook MUH00303<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclobjectTypeV {

	//==== PROPERTIES ====
	//Original name: OBJ-CD
	private String objCd = DefaultValues.stringVal(Len.OBJ_CD);
	//Original name: CTR-NBR-CD
	private short ctrNbrCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: OBJ-DES
	private String objDes = DefaultValues.stringVal(Len.OBJ_DES);

	//==== METHODS ====
	public void setObjCd(String objCd) {
		this.objCd = Functions.subString(objCd, Len.OBJ_CD);
	}

	public String getObjCd() {
		return this.objCd;
	}

	public String getObjCdFormatted() {
		return Functions.padBlanks(getObjCd(), Len.OBJ_CD);
	}

	public void setCtrNbrCd(short ctrNbrCd) {
		this.ctrNbrCd = ctrNbrCd;
	}

	public short getCtrNbrCd() {
		return this.ctrNbrCd;
	}

	public void setObjDes(String objDes) {
		this.objDes = Functions.subString(objDes, Len.OBJ_DES);
	}

	public String getObjDes() {
		return this.objDes;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OBJ_CD = 10;
		public static final int OBJ_DES = 40;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
