/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-08-ACT-NOT-FOUND-MSG<br>
 * Variable: EA-08-ACT-NOT-FOUND-MSG from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea08ActNotFoundMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-08-ACT-NOT-FOUND-MSG
	private String flr1 = " PGM = XZ004000";
	//Original name: FILLER-EA-08-ACT-NOT-FOUND-MSG-1
	private String flr2 = " -";
	//Original name: FILLER-EA-08-ACT-NOT-FOUND-MSG-2
	private String flr3 = "ACCOUNT";
	//Original name: FILLER-EA-08-ACT-NOT-FOUND-MSG-3
	private String flr4 = "NUMBER NOT";
	//Original name: FILLER-EA-08-ACT-NOT-FOUND-MSG-4
	private String flr5 = "FOUND FOR";
	//Original name: FILLER-EA-08-ACT-NOT-FOUND-MSG-5
	private String flr6 = "POL-NBR=";
	//Original name: EA-08-POL-NBR
	private String nbr = DefaultValues.stringVal(Len.NBR);
	//Original name: FILLER-EA-08-ACT-NOT-FOUND-MSG-6
	private String flr7 = " POL-EFF-DT=";
	//Original name: EA-08-POL-EFF-DT
	private String effDt = DefaultValues.stringVal(Len.EFF_DT);

	//==== METHODS ====
	public String getEa08ActNotFoundMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa08ActNotFoundMsgBytes());
	}

	public byte[] getEa08ActNotFoundMsgBytes() {
		byte[] buffer = new byte[Len.EA08_ACT_NOT_FOUND_MSG];
		return getEa08ActNotFoundMsgBytes(buffer, 1);
	}

	public byte[] getEa08ActNotFoundMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, nbr, Len.NBR);
		position += Len.NBR;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, effDt, Len.EFF_DT);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setNbr(String nbr) {
		this.nbr = Functions.subString(nbr, Len.NBR);
	}

	public String getNbr() {
		return this.nbr;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setEffDt(String effDt) {
		this.effDt = Functions.subString(effDt, Len.EFF_DT);
	}

	public String getEffDt() {
		return this.effDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NBR = 16;
		public static final int EFF_DT = 10;
		public static final int FLR1 = 15;
		public static final int FLR2 = 3;
		public static final int FLR3 = 8;
		public static final int FLR4 = 11;
		public static final int FLR5 = 10;
		public static final int FLR6 = 9;
		public static final int FLR7 = 13;
		public static final int EA08_ACT_NOT_FOUND_MSG = NBR + EFF_DT + FLR1 + FLR2 + FLR3 + FLR4 + FLR5 + FLR6 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
