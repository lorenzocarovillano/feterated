/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CWGIKF-INQUIRE-KEY-ROW<br>
 * Variable: CWGIKF-INQUIRE-KEY-ROW from copybook CAWLFGIK<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class CwgikfInquireKeyRow {

	//==== PROPERTIES ====
	//Original name: CWGIKF-CLT-OBJ-RELATION
	private CwgikfCltObjRelation cltObjRelation = new CwgikfCltObjRelation();
	//Original name: CWGIKF-PHN-NBR
	private String phnNbr = DefaultValues.stringVal(Len.PHN_NBR);
	//Original name: CWGIKF-PHN-TYP-CD
	private String phnTypCd = DefaultValues.stringVal(Len.PHN_TYP_CD);
	//Original name: CWGIKF-EMAIL-ADR-TXT
	private String emailAdrTxt = DefaultValues.stringVal(Len.EMAIL_ADR_TXT);
	//Original name: CWGIKF-EMAIL-TYPE-CD
	private String emailTypeCd = DefaultValues.stringVal(Len.EMAIL_TYPE_CD);
	//Original name: CWGIKF-TAX-TYPE-CD
	private String taxTypeCd = DefaultValues.stringVal(Len.TAX_TYPE_CD);
	//Original name: CWGIKF-CITX-TAX-ID
	private String citxTaxId = DefaultValues.stringVal(Len.CITX_TAX_ID);
	//Original name: CWGIKF-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: CWGIKF-EFF-DT
	private String effDt = DefaultValues.stringVal(Len.EFF_DT);
	//Original name: CWGIKF-EXP-DT
	private String expDt = DefaultValues.stringVal(Len.EXP_DT);

	//==== METHODS ====
	public void setCwgikfInquireKeyRowFormatted(String data) {
		byte[] buffer = new byte[Len.CWGIKF_INQUIRE_KEY_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CWGIKF_INQUIRE_KEY_ROW);
		setCwgikfInquireKeyRowBytes(buffer, 1);
	}

	public String getCwgikfInquireKeyRowFormatted() {
		return MarshalByteExt.bufferToStr(getCwgikfInquireKeyRowBytes());
	}

	public byte[] getCwgikfInquireKeyRowBytes() {
		byte[] buffer = new byte[Len.CWGIKF_INQUIRE_KEY_ROW];
		return getCwgikfInquireKeyRowBytes(buffer, 1);
	}

	public void setCwgikfInquireKeyRowBytes(byte[] buffer, int offset) {
		int position = offset;
		cltObjRelation.setCltObjRelationBytes(buffer, position);
		position += CwgikfCltObjRelation.Len.CLT_OBJ_RELATION;
		setClientPhoneBytes(buffer, position);
		position += Len.CLIENT_PHONE;
		setClientEmailBytes(buffer, position);
		position += Len.CLIENT_EMAIL;
		setClientTaxBytes(buffer, position);
		position += Len.CLIENT_TAX;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		effDt = MarshalByte.readString(buffer, position, Len.EFF_DT);
		position += Len.EFF_DT;
		expDt = MarshalByte.readString(buffer, position, Len.EXP_DT);
	}

	public byte[] getCwgikfInquireKeyRowBytes(byte[] buffer, int offset) {
		int position = offset;
		cltObjRelation.getCltObjRelationBytes(buffer, position);
		position += CwgikfCltObjRelation.Len.CLT_OBJ_RELATION;
		getClientPhoneBytes(buffer, position);
		position += Len.CLIENT_PHONE;
		getClientEmailBytes(buffer, position);
		position += Len.CLIENT_EMAIL;
		getClientTaxBytes(buffer, position);
		position += Len.CLIENT_TAX;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, effDt, Len.EFF_DT);
		position += Len.EFF_DT;
		MarshalByte.writeString(buffer, position, expDt, Len.EXP_DT);
		return buffer;
	}

	public void setClientPhoneBytes(byte[] buffer, int offset) {
		int position = offset;
		phnNbr = MarshalByte.readString(buffer, position, Len.PHN_NBR);
		position += Len.PHN_NBR;
		phnTypCd = MarshalByte.readString(buffer, position, Len.PHN_TYP_CD);
	}

	public byte[] getClientPhoneBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, phnNbr, Len.PHN_NBR);
		position += Len.PHN_NBR;
		MarshalByte.writeString(buffer, position, phnTypCd, Len.PHN_TYP_CD);
		return buffer;
	}

	public void setPhnNbr(String phnNbr) {
		this.phnNbr = Functions.subString(phnNbr, Len.PHN_NBR);
	}

	public String getPhnNbr() {
		return this.phnNbr;
	}

	public void setPhnTypCd(String phnTypCd) {
		this.phnTypCd = Functions.subString(phnTypCd, Len.PHN_TYP_CD);
	}

	public String getPhnTypCd() {
		return this.phnTypCd;
	}

	public void setClientEmailBytes(byte[] buffer, int offset) {
		int position = offset;
		emailAdrTxt = MarshalByte.readString(buffer, position, Len.EMAIL_ADR_TXT);
		position += Len.EMAIL_ADR_TXT;
		emailTypeCd = MarshalByte.readString(buffer, position, Len.EMAIL_TYPE_CD);
	}

	public byte[] getClientEmailBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, emailAdrTxt, Len.EMAIL_ADR_TXT);
		position += Len.EMAIL_ADR_TXT;
		MarshalByte.writeString(buffer, position, emailTypeCd, Len.EMAIL_TYPE_CD);
		return buffer;
	}

	public void setEmailAdrTxt(String emailAdrTxt) {
		this.emailAdrTxt = Functions.subString(emailAdrTxt, Len.EMAIL_ADR_TXT);
	}

	public String getEmailAdrTxt() {
		return this.emailAdrTxt;
	}

	public void setEmailTypeCd(String emailTypeCd) {
		this.emailTypeCd = Functions.subString(emailTypeCd, Len.EMAIL_TYPE_CD);
	}

	public String getEmailTypeCd() {
		return this.emailTypeCd;
	}

	public void setClientTaxBytes(byte[] buffer, int offset) {
		int position = offset;
		taxTypeCd = MarshalByte.readString(buffer, position, Len.TAX_TYPE_CD);
		position += Len.TAX_TYPE_CD;
		citxTaxId = MarshalByte.readString(buffer, position, Len.CITX_TAX_ID);
	}

	public byte[] getClientTaxBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, taxTypeCd, Len.TAX_TYPE_CD);
		position += Len.TAX_TYPE_CD;
		MarshalByte.writeString(buffer, position, citxTaxId, Len.CITX_TAX_ID);
		return buffer;
	}

	public void setTaxTypeCd(String taxTypeCd) {
		this.taxTypeCd = Functions.subString(taxTypeCd, Len.TAX_TYPE_CD);
	}

	public String getTaxTypeCd() {
		return this.taxTypeCd;
	}

	public void setCitxTaxId(String citxTaxId) {
		this.citxTaxId = Functions.subString(citxTaxId, Len.CITX_TAX_ID);
	}

	public String getCitxTaxId() {
		return this.citxTaxId;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setEffDt(String effDt) {
		this.effDt = Functions.subString(effDt, Len.EFF_DT);
	}

	public String getEffDt() {
		return this.effDt;
	}

	public void setExpDt(String expDt) {
		this.expDt = Functions.subString(expDt, Len.EXP_DT);
	}

	public String getExpDt() {
		return this.expDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PHN_NBR = 21;
		public static final int PHN_TYP_CD = 2;
		public static final int EMAIL_ADR_TXT = 255;
		public static final int EMAIL_TYPE_CD = 3;
		public static final int TAX_TYPE_CD = 3;
		public static final int CITX_TAX_ID = 15;
		public static final int POL_NBR = 25;
		public static final int EFF_DT = 10;
		public static final int EXP_DT = 10;
		public static final int CLIENT_PHONE = PHN_NBR + PHN_TYP_CD;
		public static final int CLIENT_EMAIL = EMAIL_ADR_TXT + EMAIL_TYPE_CD;
		public static final int CLIENT_TAX = TAX_TYPE_CD + CITX_TAX_ID;
		public static final int CWGIKF_INQUIRE_KEY_ROW = CwgikfCltObjRelation.Len.CLT_OBJ_RELATION + CLIENT_PHONE + CLIENT_EMAIL + CLIENT_TAX
				+ POL_NBR + EFF_DT + EXP_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
