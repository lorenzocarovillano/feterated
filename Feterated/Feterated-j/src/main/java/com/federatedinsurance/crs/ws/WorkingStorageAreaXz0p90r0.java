/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P90R0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p90r0 {

	//==== PROPERTIES ====
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0P90R0";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "XZ_GET_ADDITIONAL_POL_INFO_BPO";

	//==== METHODS ====
	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
