/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [FRM_REC_OVL]
 * 
 */
public interface IFrmRecOvl extends BaseSqlTo {

	/**
	 * Host Variable FRM-NBR
	 * 
	 */
	String getFrmNbr();

	void setFrmNbr(String frmNbr);

	/**
	 * Host Variable FRM-EDT-DT
	 * 
	 */
	String getFrmEdtDt();

	void setFrmEdtDt(String frmEdtDt);

	/**
	 * Host Variable REC-TYP-CD
	 * 
	 */
	String getRecTypCd();

	void setRecTypCd(String recTypCd);

	/**
	 * Host Variable OVL-EDL-FRM-NM
	 * 
	 */
	String getOvlEdlFrmNm();

	void setOvlEdlFrmNm(String ovlEdlFrmNm);

	/**
	 * Host Variable ST-ABB
	 * 
	 */
	String getStAbb();

	void setStAbb(String stAbb);

	/**
	 * Host Variable SPE-PRC-CD
	 * 
	 */
	String getSpePrcCd();

	void setSpePrcCd(String spePrcCd);

	/**
	 * Nullable property for SPE-PRC-CD
	 * 
	 */
	String getSpePrcCdObj();

	void setSpePrcCdObj(String spePrcCdObj);

	/**
	 * Host Variable OVL-SR-CD
	 * 
	 */
	String getOvlSrCd();

	void setOvlSrCd(String ovlSrCd);
};
