/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-SOUNDEX-SW<br>
 * Variable: WS-SOUNDEX-SW from program CIWOSDX<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsSoundexSw {

	//==== PROPERTIES ====
	private char value = 'N';
	public static final char NOT_COMPLETE = 'N';
	public static final char COMPLETE = 'Y';

	//==== METHODS ====
	public void setWsSoundexSw(char wsSoundexSw) {
		this.value = wsSoundexSw;
	}

	public void setWsSoundexSwFormatted(String wsSoundexSw) {
		setWsSoundexSw(Functions.charAt(wsSoundexSw, Types.CHAR_SIZE));
	}

	public char getWsSoundexSw() {
		return this.value;
	}

	public boolean isNotComplete() {
		return value == NOT_COMPLETE;
	}

	public boolean isComplete() {
		return value == COMPLETE;
	}

	public void setComplete() {
		value = COMPLETE;
	}
}
