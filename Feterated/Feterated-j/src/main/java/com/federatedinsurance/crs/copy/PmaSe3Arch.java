/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: PMA-SE3-ARCH<br>
 * Variable: PMA-SE3-ARCH from copybook XPILPMA<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class PmaSe3Arch {

	//==== PROPERTIES ====
	//Original name: PMA-SE3-CCYY
	private String se3Ccyy = DefaultValues.stringVal(Len.SE3_CCYY);
	//Original name: PMA-SE3-MM
	private String se3Mm = DefaultValues.stringVal(Len.SE3_MM);
	//Original name: PMA-SE3-DD
	private String se3Dd = DefaultValues.stringVal(Len.SE3_DD);
	//Original name: PMA-SE3-OFFSET-SIGN
	private char se3OffsetSign = DefaultValues.CHAR_VAL;
	//Original name: PMA-SE3-OFFSET
	private String se3Offset = DefaultValues.stringVal(Len.SE3_OFFSET);

	//==== METHODS ====
	public void setSe3Ccyy(String se3Ccyy) {
		this.se3Ccyy = Functions.subString(se3Ccyy, Len.SE3_CCYY);
	}

	public String getSe3Ccyy() {
		return this.se3Ccyy;
	}

	public String getSe3CcyyFormatted() {
		return Functions.padBlanks(getSe3Ccyy(), Len.SE3_CCYY);
	}

	public void setSe3Mm(String se3Mm) {
		this.se3Mm = Functions.subString(se3Mm, Len.SE3_MM);
	}

	public String getSe3Mm() {
		return this.se3Mm;
	}

	public String getSe3MmFormatted() {
		return Functions.padBlanks(getSe3Mm(), Len.SE3_MM);
	}

	public void setSe3Dd(String se3Dd) {
		this.se3Dd = Functions.subString(se3Dd, Len.SE3_DD);
	}

	public String getSe3Dd() {
		return this.se3Dd;
	}

	public String getSe3DdFormatted() {
		return Functions.padBlanks(getSe3Dd(), Len.SE3_DD);
	}

	public void setSe3OffsetSign(char se3OffsetSign) {
		this.se3OffsetSign = se3OffsetSign;
	}

	public void setSe3OffsetSignFormatted(String se3OffsetSign) {
		setSe3OffsetSign(Functions.charAt(se3OffsetSign, Types.CHAR_SIZE));
	}

	public char getSe3OffsetSign() {
		return this.se3OffsetSign;
	}

	public void setSe3Offset(int se3Offset) {
		this.se3Offset = NumericDisplay.asString(se3Offset, Len.SE3_OFFSET);
	}

	public int getSe3Offset() {
		return NumericDisplay.asInt(this.se3Offset);
	}

	public String getSe3OffsetFormatted() {
		return this.se3Offset;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SE3_CCYY = 4;
		public static final int SE3_MM = 2;
		public static final int SE3_DD = 2;
		public static final int SE3_OFFSET = 5;
		public static final int START_UP_LOCK = 8;
		public static final int MONITOR_NM = 8;
		public static final int GOVERNOR = 5;
		public static final int SE3_DEFER_DATE_TIME = 26;
		public static final int SE3_DB_NAME = 8;
		public static final int SE3_MICRO_SEC = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
