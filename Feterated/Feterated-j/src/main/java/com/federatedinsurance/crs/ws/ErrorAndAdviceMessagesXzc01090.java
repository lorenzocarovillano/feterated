/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZC01090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXzc01090 {

	//==== PROPERTIES ====
	//Original name: EA-01-INVALID-INPUT
	private Ea01InvalidInput ea01InvalidInput = new Ea01InvalidInput();
	//Original name: EA-10-DB2-ERROR-ON-SELECT
	private Ea10Db2ErrorOnSelect ea10Db2ErrorOnSelect = new Ea10Db2ErrorOnSelect();
	//Original name: EA-20-CICS-LINK-ERROR
	private Ea20CicsLinkError ea20CicsLinkError = new Ea20CicsLinkError();
	//Original name: EA-21-PEOPLE-SEARCH-ERROR
	private Ea21PeopleSearchError ea21PeopleSearchError = new Ea21PeopleSearchError();

	//==== METHODS ====
	public Ea01InvalidInput getEa01InvalidInput() {
		return ea01InvalidInput;
	}

	public Ea10Db2ErrorOnSelect getEa10Db2ErrorOnSelect() {
		return ea10Db2ErrorOnSelect;
	}

	public Ea20CicsLinkError getEa20CicsLinkError() {
		return ea20CicsLinkError;
	}

	public Ea21PeopleSearchError getEa21PeopleSearchError() {
		return ea21PeopleSearchError;
	}
}
