/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-TSQ1-SW<br>
 * Variable: WS-TSQ1-SW from program HALOUSDH<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTsq1Sw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char START_OF_TSQ1 = 'S';
	public static final char END_OF_TSQ1 = 'E';

	//==== METHODS ====
	public void setTsq1Sw(char tsq1Sw) {
		this.value = tsq1Sw;
	}

	public char getTsq1Sw() {
		return this.value;
	}

	public void setStartOfTsq1() {
		value = START_OF_TSQ1;
	}

	public boolean isEndOfTsq1() {
		return value == END_OF_TSQ1;
	}

	public void setEndOfTsq1() {
		value = END_OF_TSQ1;
	}
}
