/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P90J0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p90j0 {

	//==== PROPERTIES ====
	//Original name: CF-YES
	private char yes = 'Y';
	//Original name: CF-UP-ATC-FRM-IND-PGM
	private String upAtcFrmIndPgm = "XZ0U8010";
	//Original name: CF-SP-PRP-CAN-WRD-SVC
	private String spPrpCanWrdSvc = "XZ0X9000";
	//Original name: CF-SP-PRP-CERT-LIST-SVC
	private String spPrpCertListSvc = "XZ0X90C0";
	//Original name: CF-SP-ATTACH-FORMS-SVC
	private String spAttachFormsSvc = "XZ0X90E0";

	//==== METHODS ====
	public char getYes() {
		return this.yes;
	}

	public String getUpAtcFrmIndPgm() {
		return this.upAtcFrmIndPgm;
	}

	public String getSpPrpCanWrdSvc() {
		return this.spPrpCanWrdSvc;
	}

	public String getSpPrpCertListSvc() {
		return this.spPrpCertListSvc;
	}

	public String getSpAttachFormsSvc() {
		return this.spAttachFormsSvc;
	}
}
