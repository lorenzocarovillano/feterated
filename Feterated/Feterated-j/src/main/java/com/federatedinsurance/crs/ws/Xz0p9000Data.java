/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotWrd;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclstCncWrdRqr;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0P9000<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0p9000Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0p9000 constantFields = new ConstantFieldsXz0p9000();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXz0p9000 errorAndAdviceMessages = new ErrorAndAdviceMessagesXz0p9000();
	//Original name: ES-01-FATAL-ERROR-MSG
	private Es01FatalErrorMsg es01FatalErrorMsg = new Es01FatalErrorMsg();
	//Original name: SA-PL-POL-TYP-CD
	private String saPlPolTypCd = DefaultValues.stringVal(Len.SA_PL_POL_TYP_CD);
	//Original name: SA-OOS-AS-OF-DATE
	private String saOosAsOfDate = DefaultValues.stringVal(Len.SA_OOS_AS_OF_DATE);
	//Original name: SUBSCRIPTS
	private SubscriptsXz0p9000 subscripts = new SubscriptsXz0p9000();
	//Original name: SWITCHES
	private SwitchesXz0p9000 switches = new SwitchesXz0p9000();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0p9000 workingStorageArea = new WorkingStorageAreaXz0p9000();
	//Original name: IX-AL
	private int ixAl = 1;
	//Original name: IX-ML
	private int ixMl = 1;
	//Original name: WS-PRP-CNC-WRD-BPO-ROW
	private WsXz0a90b0Row wsPrpCncWrdBpoRow = new WsXz0a90b0Row();
	//Original name: WS-PROXY-PROGRAM-AREA
	private WsProxyProgramArea wsProxyProgramArea = new WsProxyProgramArea();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLST-CNC-WRD-RQR
	private DclstCncWrdRqr dclstCncWrdRqr = new DclstCncWrdRqr();
	//Original name: DCLACT-NOT-WRD
	private DclactNotWrd dclactNotWrd = new DclactNotWrd();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public void setSaPlPolTypCd(String saPlPolTypCd) {
		this.saPlPolTypCd = Functions.subString(saPlPolTypCd, Len.SA_PL_POL_TYP_CD);
	}

	public String getSaPlPolTypCd() {
		return this.saPlPolTypCd;
	}

	public String getSaPlPolTypCdFormatted() {
		return Functions.padBlanks(getSaPlPolTypCd(), Len.SA_PL_POL_TYP_CD);
	}

	public void setSaOosAsOfDate(String saOosAsOfDate) {
		this.saOosAsOfDate = Functions.subString(saOosAsOfDate, Len.SA_OOS_AS_OF_DATE);
	}

	public String getSaOosAsOfDate() {
		return this.saOosAsOfDate;
	}

	public void setIxAl(int ixAl) {
		this.ixAl = ixAl;
	}

	public int getIxAl() {
		return this.ixAl;
	}

	public void setIxMl(int ixMl) {
		this.ixMl = ixMl;
	}

	public int getIxMl() {
		return this.ixMl;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public ConstantFieldsXz0p9000 getConstantFields() {
		return constantFields;
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotWrd getDclactNotWrd() {
		return dclactNotWrd;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclstCncWrdRqr getDclstCncWrdRqr() {
		return dclstCncWrdRqr;
	}

	public ErrorAndAdviceMessagesXz0p9000 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public Es01FatalErrorMsg getEs01FatalErrorMsg() {
		return es01FatalErrorMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public SubscriptsXz0p9000 getSubscripts() {
		return subscripts;
	}

	public SwitchesXz0p9000 getSwitches() {
		return switches;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0p9000 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsProxyProgramArea getWsProxyProgramArea() {
		return wsProxyProgramArea;
	}

	public WsXz0a90b0Row getWsPrpCncWrdBpoRow() {
		return wsPrpCncWrdBpoRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_PL_POL_TYP_CD = 3;
		public static final int SA_OOS_AS_OF_DATE = 10;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
