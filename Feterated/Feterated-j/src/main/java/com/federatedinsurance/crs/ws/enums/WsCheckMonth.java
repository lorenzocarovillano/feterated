/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-CHECK-MONTH<br>
 * Variable: WS-CHECK-MONTH from program XPIODAT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsCheckMonth {

	//==== PROPERTIES ====
	private short value = DefaultValues.SHORT_VAL;
	private static final short[] MONTH31_DAYS = new short[] { ((short) 1), ((short) 3), ((short) 5), ((short) 7), ((short) 8), ((short) 10),
			((short) 12) };
	private static final short[] MONTH30_DAYS = new short[] { ((short) 4), ((short) 6), ((short) 9), ((short) 11) };
	public static final short MONTH28_DAYS = ((short) 2);

	//==== METHODS ====
	public void setWsCheckMonth(short wsCheckMonth) {
		this.value = wsCheckMonth;
	}

	public short getWsCheckMonth() {
		return this.value;
	}

	public boolean isMonth31Days() {
		return ArrayUtils.contains(MONTH31_DAYS, value);
	}

	public boolean isMonth30Days() {
		return ArrayUtils.contains(MONTH30_DAYS, value);
	}

	public boolean isMonth28Days() {
		return value == MONTH28_DAYS;
	}
}
