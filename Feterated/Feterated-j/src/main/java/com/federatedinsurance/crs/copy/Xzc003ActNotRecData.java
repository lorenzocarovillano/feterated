/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC003-ACT-NOT-REC-DATA<br>
 * Variable: XZC003-ACT-NOT-REC-DATA from copybook XZ0C0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzc003ActNotRecData {

	//==== PROPERTIES ====
	/**Original name: XZC003-REC-TYP-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char recTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);
	//Original name: XZC003-REC-CLT-ID-CI
	private char recCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-REC-CLT-ID-NI
	private char recCltIdNi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-REC-CLT-ID
	private String recCltId = DefaultValues.stringVal(Len.REC_CLT_ID);
	//Original name: XZC003-REC-NM-CI
	private char recNmCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-REC-NM-NI
	private char recNmNi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-REC-NM
	private String recNm = DefaultValues.stringVal(Len.REC_NM);
	//Original name: XZC003-REC-ADR-ID-CI
	private char recAdrIdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-REC-ADR-ID-NI
	private char recAdrIdNi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-REC-ADR-ID
	private String recAdrId = DefaultValues.stringVal(Len.REC_ADR_ID);
	//Original name: XZC003-LIN-1-ADR-CI
	private char lin1AdrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-LIN-1-ADR-NI
	private char lin1AdrNi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-LIN-1-ADR
	private String lin1Adr = DefaultValues.stringVal(Len.LIN1_ADR);
	//Original name: XZC003-LIN-2-ADR-CI
	private char lin2AdrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-LIN-2-ADR-NI
	private char lin2AdrNi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-LIN-2-ADR
	private String lin2Adr = DefaultValues.stringVal(Len.LIN2_ADR);
	//Original name: XZC003-CIT-NM-CI
	private char citNmCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-CIT-NM-NI
	private char citNmNi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-CIT-NM
	private String citNm = DefaultValues.stringVal(Len.CIT_NM);
	//Original name: XZC003-ST-ABB-CI
	private char stAbbCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-ST-ABB-NI
	private char stAbbNi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZC003-PST-CD-CI
	private char pstCdCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-PST-CD-NI
	private char pstCdNi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-PST-CD
	private String pstCd = DefaultValues.stringVal(Len.PST_CD);
	//Original name: XZC003-MNL-IND-CI
	private char mnlIndCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-MNL-IND
	private char mnlInd = DefaultValues.CHAR_VAL;
	//Original name: XZC003-CER-NBR-CI
	private char cerNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-CER-NBR-NI
	private char cerNbrNi = DefaultValues.CHAR_VAL;
	//Original name: XZC003-CER-NBR
	private String cerNbr = DefaultValues.stringVal(Len.CER_NBR);

	//==== METHODS ====
	public void setActNotRecDataBytes(byte[] buffer, int offset) {
		int position = offset;
		recTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recTypCd = MarshalByte.readString(buffer, position, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		recCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recCltIdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recCltId = MarshalByte.readString(buffer, position, Len.REC_CLT_ID);
		position += Len.REC_CLT_ID;
		recNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recNmNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recNm = MarshalByte.readString(buffer, position, Len.REC_NM);
		position += Len.REC_NM;
		recAdrIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recAdrIdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		recAdrId = MarshalByte.readString(buffer, position, Len.REC_ADR_ID);
		position += Len.REC_ADR_ID;
		lin1AdrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		lin1AdrNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		lin1Adr = MarshalByte.readString(buffer, position, Len.LIN1_ADR);
		position += Len.LIN1_ADR;
		lin2AdrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		lin2AdrNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		lin2Adr = MarshalByte.readString(buffer, position, Len.LIN2_ADR);
		position += Len.LIN2_ADR;
		citNmCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		citNmNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		citNm = MarshalByte.readString(buffer, position, Len.CIT_NM);
		position += Len.CIT_NM;
		stAbbCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		stAbbNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		stAbb = MarshalByte.readString(buffer, position, Len.ST_ABB);
		position += Len.ST_ABB;
		pstCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pstCdNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pstCd = MarshalByte.readString(buffer, position, Len.PST_CD);
		position += Len.PST_CD;
		mnlIndCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		mnlInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cerNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cerNbrNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cerNbr = MarshalByte.readString(buffer, position, Len.CER_NBR);
	}

	public byte[] getActNotRecDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, recTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, recTypCd, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		MarshalByte.writeChar(buffer, position, recCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, recCltIdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, recCltId, Len.REC_CLT_ID);
		position += Len.REC_CLT_ID;
		MarshalByte.writeChar(buffer, position, recNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, recNmNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, recNm, Len.REC_NM);
		position += Len.REC_NM;
		MarshalByte.writeChar(buffer, position, recAdrIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, recAdrIdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, recAdrId, Len.REC_ADR_ID);
		position += Len.REC_ADR_ID;
		MarshalByte.writeChar(buffer, position, lin1AdrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, lin1AdrNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, lin1Adr, Len.LIN1_ADR);
		position += Len.LIN1_ADR;
		MarshalByte.writeChar(buffer, position, lin2AdrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, lin2AdrNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, lin2Adr, Len.LIN2_ADR);
		position += Len.LIN2_ADR;
		MarshalByte.writeChar(buffer, position, citNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, citNmNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, citNm, Len.CIT_NM);
		position += Len.CIT_NM;
		MarshalByte.writeChar(buffer, position, stAbbCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, stAbbNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeChar(buffer, position, pstCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pstCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pstCd, Len.PST_CD);
		position += Len.PST_CD;
		MarshalByte.writeChar(buffer, position, mnlIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, mnlInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cerNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cerNbrNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cerNbr, Len.CER_NBR);
		return buffer;
	}

	public void initActNotRecDataSpaces() {
		recTypCdCi = Types.SPACE_CHAR;
		recTypCd = "";
		recCltIdCi = Types.SPACE_CHAR;
		recCltIdNi = Types.SPACE_CHAR;
		recCltId = "";
		recNmCi = Types.SPACE_CHAR;
		recNmNi = Types.SPACE_CHAR;
		recNm = "";
		recAdrIdCi = Types.SPACE_CHAR;
		recAdrIdNi = Types.SPACE_CHAR;
		recAdrId = "";
		lin1AdrCi = Types.SPACE_CHAR;
		lin1AdrNi = Types.SPACE_CHAR;
		lin1Adr = "";
		lin2AdrCi = Types.SPACE_CHAR;
		lin2AdrNi = Types.SPACE_CHAR;
		lin2Adr = "";
		citNmCi = Types.SPACE_CHAR;
		citNmNi = Types.SPACE_CHAR;
		citNm = "";
		stAbbCi = Types.SPACE_CHAR;
		stAbbNi = Types.SPACE_CHAR;
		stAbb = "";
		pstCdCi = Types.SPACE_CHAR;
		pstCdNi = Types.SPACE_CHAR;
		pstCd = "";
		mnlIndCi = Types.SPACE_CHAR;
		mnlInd = Types.SPACE_CHAR;
		cerNbrCi = Types.SPACE_CHAR;
		cerNbrNi = Types.SPACE_CHAR;
		cerNbr = "";
	}

	public void setRecTypCdCi(char recTypCdCi) {
		this.recTypCdCi = recTypCdCi;
	}

	public char getRecTypCdCi() {
		return this.recTypCdCi;
	}

	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	public String getRecTypCd() {
		return this.recTypCd;
	}

	public String getXzc003RecTypCdFormatted() {
		return Functions.padBlanks(getRecTypCd(), Len.REC_TYP_CD);
	}

	public void setRecCltIdCi(char recCltIdCi) {
		this.recCltIdCi = recCltIdCi;
	}

	public char getRecCltIdCi() {
		return this.recCltIdCi;
	}

	public void setRecCltIdNi(char recCltIdNi) {
		this.recCltIdNi = recCltIdNi;
	}

	public void setXzc003RecCltIdNiFormatted(String xzc003RecCltIdNi) {
		setRecCltIdNi(Functions.charAt(xzc003RecCltIdNi, Types.CHAR_SIZE));
	}

	public char getRecCltIdNi() {
		return this.recCltIdNi;
	}

	public void setRecCltId(String recCltId) {
		this.recCltId = Functions.subString(recCltId, Len.REC_CLT_ID);
	}

	public String getRecCltId() {
		return this.recCltId;
	}

	public void setRecNmCi(char recNmCi) {
		this.recNmCi = recNmCi;
	}

	public char getRecNmCi() {
		return this.recNmCi;
	}

	public void setRecNmNi(char recNmNi) {
		this.recNmNi = recNmNi;
	}

	public void setXzc003RecNmNiFormatted(String xzc003RecNmNi) {
		setRecNmNi(Functions.charAt(xzc003RecNmNi, Types.CHAR_SIZE));
	}

	public char getRecNmNi() {
		return this.recNmNi;
	}

	public void setRecNm(String recNm) {
		this.recNm = Functions.subString(recNm, Len.REC_NM);
	}

	public String getRecNm() {
		return this.recNm;
	}

	public void setRecAdrIdCi(char recAdrIdCi) {
		this.recAdrIdCi = recAdrIdCi;
	}

	public char getRecAdrIdCi() {
		return this.recAdrIdCi;
	}

	public void setRecAdrIdNi(char recAdrIdNi) {
		this.recAdrIdNi = recAdrIdNi;
	}

	public void setXzc003RecAdrIdNiFormatted(String xzc003RecAdrIdNi) {
		setRecAdrIdNi(Functions.charAt(xzc003RecAdrIdNi, Types.CHAR_SIZE));
	}

	public char getRecAdrIdNi() {
		return this.recAdrIdNi;
	}

	public void setRecAdrId(String recAdrId) {
		this.recAdrId = Functions.subString(recAdrId, Len.REC_ADR_ID);
	}

	public String getRecAdrId() {
		return this.recAdrId;
	}

	public void setLin1AdrCi(char lin1AdrCi) {
		this.lin1AdrCi = lin1AdrCi;
	}

	public char getLin1AdrCi() {
		return this.lin1AdrCi;
	}

	public void setLin1AdrNi(char lin1AdrNi) {
		this.lin1AdrNi = lin1AdrNi;
	}

	public void setXzc003Lin1AdrNiFormatted(String xzc003Lin1AdrNi) {
		setLin1AdrNi(Functions.charAt(xzc003Lin1AdrNi, Types.CHAR_SIZE));
	}

	public char getLin1AdrNi() {
		return this.lin1AdrNi;
	}

	public void setLin1Adr(String lin1Adr) {
		this.lin1Adr = Functions.subString(lin1Adr, Len.LIN1_ADR);
	}

	public String getLin1Adr() {
		return this.lin1Adr;
	}

	public void setLin2AdrCi(char lin2AdrCi) {
		this.lin2AdrCi = lin2AdrCi;
	}

	public char getLin2AdrCi() {
		return this.lin2AdrCi;
	}

	public void setLin2AdrNi(char lin2AdrNi) {
		this.lin2AdrNi = lin2AdrNi;
	}

	public void setXzc003Lin2AdrNiFormatted(String xzc003Lin2AdrNi) {
		setLin2AdrNi(Functions.charAt(xzc003Lin2AdrNi, Types.CHAR_SIZE));
	}

	public char getLin2AdrNi() {
		return this.lin2AdrNi;
	}

	public void setLin2Adr(String lin2Adr) {
		this.lin2Adr = Functions.subString(lin2Adr, Len.LIN2_ADR);
	}

	public String getLin2Adr() {
		return this.lin2Adr;
	}

	public void setCitNmCi(char citNmCi) {
		this.citNmCi = citNmCi;
	}

	public char getCitNmCi() {
		return this.citNmCi;
	}

	public void setCitNmNi(char citNmNi) {
		this.citNmNi = citNmNi;
	}

	public void setXzc003CitNmNiFormatted(String xzc003CitNmNi) {
		setCitNmNi(Functions.charAt(xzc003CitNmNi, Types.CHAR_SIZE));
	}

	public char getCitNmNi() {
		return this.citNmNi;
	}

	public void setCitNm(String citNm) {
		this.citNm = Functions.subString(citNm, Len.CIT_NM);
	}

	public String getCitNm() {
		return this.citNm;
	}

	public void setStAbbCi(char stAbbCi) {
		this.stAbbCi = stAbbCi;
	}

	public char getStAbbCi() {
		return this.stAbbCi;
	}

	public void setStAbbNi(char stAbbNi) {
		this.stAbbNi = stAbbNi;
	}

	public void setXzc003StAbbNiFormatted(String xzc003StAbbNi) {
		setStAbbNi(Functions.charAt(xzc003StAbbNi, Types.CHAR_SIZE));
	}

	public char getStAbbNi() {
		return this.stAbbNi;
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public String getXzc003StAbbFormatted() {
		return Functions.padBlanks(getStAbb(), Len.ST_ABB);
	}

	public void setPstCdCi(char pstCdCi) {
		this.pstCdCi = pstCdCi;
	}

	public char getPstCdCi() {
		return this.pstCdCi;
	}

	public void setPstCdNi(char pstCdNi) {
		this.pstCdNi = pstCdNi;
	}

	public void setXzc003PstCdNiFormatted(String xzc003PstCdNi) {
		setPstCdNi(Functions.charAt(xzc003PstCdNi, Types.CHAR_SIZE));
	}

	public char getPstCdNi() {
		return this.pstCdNi;
	}

	public void setPstCd(String pstCd) {
		this.pstCd = Functions.subString(pstCd, Len.PST_CD);
	}

	public String getPstCd() {
		return this.pstCd;
	}

	public void setMnlIndCi(char mnlIndCi) {
		this.mnlIndCi = mnlIndCi;
	}

	public char getMnlIndCi() {
		return this.mnlIndCi;
	}

	public void setMnlInd(char mnlInd) {
		this.mnlInd = mnlInd;
	}

	public char getMnlInd() {
		return this.mnlInd;
	}

	public void setCerNbrCi(char cerNbrCi) {
		this.cerNbrCi = cerNbrCi;
	}

	public char getCerNbrCi() {
		return this.cerNbrCi;
	}

	public void setCerNbrNi(char cerNbrNi) {
		this.cerNbrNi = cerNbrNi;
	}

	public void setXzc003CerNbrNiFormatted(String xzc003CerNbrNi) {
		setCerNbrNi(Functions.charAt(xzc003CerNbrNi, Types.CHAR_SIZE));
	}

	public char getCerNbrNi() {
		return this.cerNbrNi;
	}

	public void setCerNbr(String cerNbr) {
		this.cerNbr = Functions.subString(cerNbr, Len.CER_NBR);
	}

	public String getCerNbr() {
		return this.cerNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_TYP_CD_CI = 1;
		public static final int REC_TYP_CD = 5;
		public static final int REC_CLT_ID_CI = 1;
		public static final int REC_CLT_ID_NI = 1;
		public static final int REC_CLT_ID = 64;
		public static final int REC_NM_CI = 1;
		public static final int REC_NM_NI = 1;
		public static final int REC_NM = 120;
		public static final int REC_ADR_ID_CI = 1;
		public static final int REC_ADR_ID_NI = 1;
		public static final int REC_ADR_ID = 64;
		public static final int LIN1_ADR_CI = 1;
		public static final int LIN1_ADR_NI = 1;
		public static final int LIN1_ADR = 45;
		public static final int LIN2_ADR_CI = 1;
		public static final int LIN2_ADR_NI = 1;
		public static final int LIN2_ADR = 45;
		public static final int CIT_NM_CI = 1;
		public static final int CIT_NM_NI = 1;
		public static final int CIT_NM = 30;
		public static final int ST_ABB_CI = 1;
		public static final int ST_ABB_NI = 1;
		public static final int ST_ABB = 2;
		public static final int PST_CD_CI = 1;
		public static final int PST_CD_NI = 1;
		public static final int PST_CD = 13;
		public static final int MNL_IND_CI = 1;
		public static final int MNL_IND = 1;
		public static final int CER_NBR_CI = 1;
		public static final int CER_NBR_NI = 1;
		public static final int CER_NBR = 25;
		public static final int ACT_NOT_REC_DATA = REC_TYP_CD_CI + REC_TYP_CD + REC_CLT_ID_CI + REC_CLT_ID_NI + REC_CLT_ID + REC_NM_CI + REC_NM_NI
				+ REC_NM + REC_ADR_ID_CI + REC_ADR_ID_NI + REC_ADR_ID + LIN1_ADR_CI + LIN1_ADR_NI + LIN1_ADR + LIN2_ADR_CI + LIN2_ADR_NI + LIN2_ADR
				+ CIT_NM_CI + CIT_NM_NI + CIT_NM + ST_ABB_CI + ST_ABB_NI + ST_ABB + PST_CD_CI + PST_CD_NI + PST_CD + MNL_IND_CI + MNL_IND + CER_NBR_CI
				+ CER_NBR_NI + CER_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
