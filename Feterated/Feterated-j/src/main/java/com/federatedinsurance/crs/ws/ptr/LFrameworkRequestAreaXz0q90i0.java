/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q90I0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q90i0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q90i0() {
	}

	public LFrameworkRequestAreaXz0q90i0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza9i0qMaxFrmRecRows(int xza9i0qMaxFrmRecRows) {
		writeBinaryInt(Pos.XZA9I0_MAX_FRM_REC_ROWS, xza9i0qMaxFrmRecRows);
	}

	/**Original name: XZA9I0Q-MAX-FRM-REC-ROWS<br>*/
	public int getXza9i0qMaxFrmRecRows() {
		return readBinaryInt(Pos.XZA9I0_MAX_FRM_REC_ROWS);
	}

	public void setXza9i0qCsrActNbr(String xza9i0qCsrActNbr) {
		writeString(Pos.XZA9I0_CSR_ACT_NBR, xza9i0qCsrActNbr, Len.XZA9I0_CSR_ACT_NBR);
	}

	/**Original name: XZA9I0Q-CSR-ACT-NBR<br>*/
	public String getXza9i0qCsrActNbr() {
		return readString(Pos.XZA9I0_CSR_ACT_NBR, Len.XZA9I0_CSR_ACT_NBR);
	}

	public void setXza9i0qNotPrcTs(String xza9i0qNotPrcTs) {
		writeString(Pos.XZA9I0_NOT_PRC_TS, xza9i0qNotPrcTs, Len.XZA9I0_NOT_PRC_TS);
	}

	/**Original name: XZA9I0Q-NOT-PRC-TS<br>*/
	public String getXza9i0qNotPrcTs() {
		return readString(Pos.XZA9I0_NOT_PRC_TS, Len.XZA9I0_NOT_PRC_TS);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A90I0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA9I0_GET_FRM_REC_LIST_KEY = L_FW_REQ_XZ0A90I0;
		public static final int XZA9I0_MAX_FRM_REC_ROWS = XZA9I0_GET_FRM_REC_LIST_KEY;
		public static final int XZA9I0_CSR_ACT_NBR = XZA9I0_MAX_FRM_REC_ROWS + Len.XZA9I0_MAX_FRM_REC_ROWS;
		public static final int XZA9I0_NOT_PRC_TS = XZA9I0_CSR_ACT_NBR + Len.XZA9I0_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9I0_MAX_FRM_REC_ROWS = 4;
		public static final int XZA9I0_CSR_ACT_NBR = 9;
		public static final int XZA9I0_NOT_PRC_TS = 26;
		public static final int XZA9I0_GET_FRM_REC_LIST_KEY = XZA9I0_MAX_FRM_REC_ROWS + XZA9I0_CSR_ACT_NBR + XZA9I0_NOT_PRC_TS;
		public static final int L_FW_REQ_XZ0A90I0 = XZA9I0_GET_FRM_REC_LIST_KEY;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A90I0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
