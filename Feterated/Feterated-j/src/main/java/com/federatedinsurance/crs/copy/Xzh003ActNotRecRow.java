/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotRec;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: XZH003-ACT-NOT-REC-ROW<br>
 * Variable: XZH003-ACT-NOT-REC-ROW from copybook XZ0H0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzh003ActNotRecRow implements IActNotRec {

	//==== PROPERTIES ====
	//Original name: XZH003-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZH003-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZH003-REC-SEQ-NBR
	private short recSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);
	//Original name: XZH003-REC-CLT-ID-NI
	private short recCltIdNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-REC-CLT-ID
	private String recCltId = DefaultValues.stringVal(Len.REC_CLT_ID);
	//Original name: XZH003-REC-ADR-ID-NI
	private short recAdrIdNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-REC-ADR-ID
	private String recAdrId = DefaultValues.stringVal(Len.REC_ADR_ID);
	//Original name: XZH003-REC-NM-NI
	private short recNmNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-REC-NM-LEN
	private short recNmLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-REC-NM-TEXT
	private String recNmText = DefaultValues.stringVal(Len.REC_NM_TEXT);
	//Original name: XZH003-LIN-1-ADR-NI
	private short lin1AdrNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-LIN-1-ADR
	private String lin1Adr = DefaultValues.stringVal(Len.LIN1_ADR);
	//Original name: XZH003-LIN-2-ADR-NI
	private short lin2AdrNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-LIN-2-ADR
	private String lin2Adr = DefaultValues.stringVal(Len.LIN2_ADR);
	//Original name: XZH003-CIT-NM-NI
	private short citNmNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-CIT-NM
	private String citNm = DefaultValues.stringVal(Len.CIT_NM);
	//Original name: XZH003-ST-ABB-NI
	private short stAbbNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZH003-PST-CD-NI
	private short pstCdNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-PST-CD
	private String pstCd = DefaultValues.stringVal(Len.PST_CD);
	//Original name: XZH003-MNL-IND
	private char mnlInd = DefaultValues.CHAR_VAL;
	//Original name: XZH003-CER-NBR-NI
	private short cerNbrNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH003-CER-NBR
	private String cerNbr = DefaultValues.stringVal(Len.CER_NBR);

	//==== METHODS ====
	public String getXzh003ActNotRecRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzh003ActNotRecRowBytes());
	}

	public byte[] getXzh003ActNotRecRowBytes() {
		byte[] buffer = new byte[Len.XZH003_ACT_NOT_REC_ROW];
		return getXzh003ActNotRecRowBytes(buffer, 1);
	}

	public byte[] getXzh003ActNotRecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeBinaryShort(buffer, position, recSeqNbr);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, recTypCd, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		MarshalByte.writeBinaryShort(buffer, position, recCltIdNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, recCltId, Len.REC_CLT_ID);
		position += Len.REC_CLT_ID;
		MarshalByte.writeBinaryShort(buffer, position, recAdrIdNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, recAdrId, Len.REC_ADR_ID);
		position += Len.REC_ADR_ID;
		MarshalByte.writeBinaryShort(buffer, position, recNmNi);
		position += Types.SHORT_SIZE;
		getRecNmBytes(buffer, position);
		position += Len.REC_NM;
		MarshalByte.writeBinaryShort(buffer, position, lin1AdrNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, lin1Adr, Len.LIN1_ADR);
		position += Len.LIN1_ADR;
		MarshalByte.writeBinaryShort(buffer, position, lin2AdrNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, lin2Adr, Len.LIN2_ADR);
		position += Len.LIN2_ADR;
		MarshalByte.writeBinaryShort(buffer, position, citNmNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, citNm, Len.CIT_NM);
		position += Len.CIT_NM;
		MarshalByte.writeBinaryShort(buffer, position, stAbbNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeBinaryShort(buffer, position, pstCdNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, pstCd, Len.PST_CD);
		position += Len.PST_CD;
		MarshalByte.writeChar(buffer, position, mnlInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeBinaryShort(buffer, position, cerNbrNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, cerNbr, Len.CER_NBR);
		return buffer;
	}

	public void initXzh003ActNotRecRowSpaces() {
		csrActNbr = "";
		notPrcTs = "";
		recSeqNbr = Types.INVALID_BINARY_SHORT_VAL;
		recTypCd = "";
		recCltIdNi = Types.INVALID_BINARY_SHORT_VAL;
		recCltId = "";
		recAdrIdNi = Types.INVALID_BINARY_SHORT_VAL;
		recAdrId = "";
		recNmNi = Types.INVALID_BINARY_SHORT_VAL;
		initRecNmSpaces();
		lin1AdrNi = Types.INVALID_BINARY_SHORT_VAL;
		lin1Adr = "";
		lin2AdrNi = Types.INVALID_BINARY_SHORT_VAL;
		lin2Adr = "";
		citNmNi = Types.INVALID_BINARY_SHORT_VAL;
		citNm = "";
		stAbbNi = Types.INVALID_BINARY_SHORT_VAL;
		stAbb = "";
		pstCdNi = Types.INVALID_BINARY_SHORT_VAL;
		pstCd = "";
		mnlInd = Types.SPACE_CHAR;
		cerNbrNi = Types.INVALID_BINARY_SHORT_VAL;
		cerNbr = "";
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public void setRecSeqNbr(short recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	@Override
	public short getRecSeqNbr() {
		return this.recSeqNbr;
	}

	@Override
	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	@Override
	public String getRecTypCd() {
		return this.recTypCd;
	}

	public void setRecCltIdNi(short recCltIdNi) {
		this.recCltIdNi = recCltIdNi;
	}

	public short getRecCltIdNi() {
		return this.recCltIdNi;
	}

	@Override
	public void setRecCltId(String recCltId) {
		this.recCltId = Functions.subString(recCltId, Len.REC_CLT_ID);
	}

	@Override
	public String getRecCltId() {
		return this.recCltId;
	}

	public void setRecAdrIdNi(short recAdrIdNi) {
		this.recAdrIdNi = recAdrIdNi;
	}

	public short getRecAdrIdNi() {
		return this.recAdrIdNi;
	}

	@Override
	public void setRecAdrId(String recAdrId) {
		this.recAdrId = Functions.subString(recAdrId, Len.REC_ADR_ID);
	}

	@Override
	public String getRecAdrId() {
		return this.recAdrId;
	}

	public void setRecNmNi(short recNmNi) {
		this.recNmNi = recNmNi;
	}

	public short getRecNmNi() {
		return this.recNmNi;
	}

	public byte[] getRecNmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, recNmLen);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, recNmText, Len.REC_NM_TEXT);
		return buffer;
	}

	public void initRecNmSpaces() {
		recNmLen = Types.INVALID_BINARY_SHORT_VAL;
		recNmText = "";
	}

	public void setRecNmLen(short recNmLen) {
		this.recNmLen = recNmLen;
	}

	public short getRecNmLen() {
		return this.recNmLen;
	}

	public void setRecNmText(String recNmText) {
		this.recNmText = Functions.subString(recNmText, Len.REC_NM_TEXT);
	}

	public String getRecNmText() {
		return this.recNmText;
	}

	public String getRecNmTextFormatted() {
		return Functions.padBlanks(getRecNmText(), Len.REC_NM_TEXT);
	}

	public void setLin1AdrNi(short lin1AdrNi) {
		this.lin1AdrNi = lin1AdrNi;
	}

	public short getLin1AdrNi() {
		return this.lin1AdrNi;
	}

	@Override
	public void setLin1Adr(String lin1Adr) {
		this.lin1Adr = Functions.subString(lin1Adr, Len.LIN1_ADR);
	}

	@Override
	public String getLin1Adr() {
		return this.lin1Adr;
	}

	public void setLin2AdrNi(short lin2AdrNi) {
		this.lin2AdrNi = lin2AdrNi;
	}

	public short getLin2AdrNi() {
		return this.lin2AdrNi;
	}

	@Override
	public void setLin2Adr(String lin2Adr) {
		this.lin2Adr = Functions.subString(lin2Adr, Len.LIN2_ADR);
	}

	@Override
	public String getLin2Adr() {
		return this.lin2Adr;
	}

	public void setCitNmNi(short citNmNi) {
		this.citNmNi = citNmNi;
	}

	public short getCitNmNi() {
		return this.citNmNi;
	}

	@Override
	public void setCitNm(String citNm) {
		this.citNm = Functions.subString(citNm, Len.CIT_NM);
	}

	@Override
	public String getCitNm() {
		return this.citNm;
	}

	public void setStAbbNi(short stAbbNi) {
		this.stAbbNi = stAbbNi;
	}

	public short getStAbbNi() {
		return this.stAbbNi;
	}

	@Override
	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	@Override
	public String getStAbb() {
		return this.stAbb;
	}

	public void setPstCdNi(short pstCdNi) {
		this.pstCdNi = pstCdNi;
	}

	public short getPstCdNi() {
		return this.pstCdNi;
	}

	@Override
	public void setPstCd(String pstCd) {
		this.pstCd = Functions.subString(pstCd, Len.PST_CD);
	}

	@Override
	public String getPstCd() {
		return this.pstCd;
	}

	@Override
	public void setMnlInd(char mnlInd) {
		this.mnlInd = mnlInd;
	}

	@Override
	public char getMnlInd() {
		return this.mnlInd;
	}

	public void setCerNbrNi(short cerNbrNi) {
		this.cerNbrNi = cerNbrNi;
	}

	public short getCerNbrNi() {
		return this.cerNbrNi;
	}

	@Override
	public void setCerNbr(String cerNbr) {
		this.cerNbr = Functions.subString(cerNbr, Len.CER_NBR);
	}

	@Override
	public String getCerNbr() {
		return this.cerNbr;
	}

	@Override
	public String getAdrId() {
		throw new FieldNotMappedException("adrId");
	}

	@Override
	public void setAdrId(String adrId) {
		throw new FieldNotMappedException("adrId");
	}

	@Override
	public String getAdrIdObj() {
		return getAdrId();
	}

	@Override
	public void setAdrIdObj(String adrIdObj) {
		setAdrId(adrIdObj);
	}

	@Override
	public String getCerNbrObj() {
		return getCerNbr();
	}

	@Override
	public void setCerNbrObj(String cerNbrObj) {
		setCerNbr(cerNbrObj);
	}

	@Override
	public String getCitNmObj() {
		return getCitNm();
	}

	@Override
	public void setCitNmObj(String citNmObj) {
		setCitNm(citNmObj);
	}

	@Override
	public String getCltId() {
		throw new FieldNotMappedException("cltId");
	}

	@Override
	public void setCltId(String cltId) {
		throw new FieldNotMappedException("cltId");
	}

	@Override
	public String getCltIdObj() {
		return getCltId();
	}

	@Override
	public void setCltIdObj(String cltIdObj) {
		setCltId(cltIdObj);
	}

	@Override
	public String getLin1AdrObj() {
		return getLin1Adr();
	}

	@Override
	public void setLin1AdrObj(String lin1AdrObj) {
		setLin1Adr(lin1AdrObj);
	}

	@Override
	public String getLin2AdrObj() {
		return getLin2Adr();
	}

	@Override
	public void setLin2AdrObj(String lin2AdrObj) {
		setLin2Adr(lin2AdrObj);
	}

	@Override
	public String getPstCdObj() {
		return getPstCd();
	}

	@Override
	public void setPstCdObj(String pstCdObj) {
		setPstCd(pstCdObj);
	}

	@Override
	public String getRecAdrIdObj() {
		return getRecAdrId();
	}

	@Override
	public void setRecAdrIdObj(String recAdrIdObj) {
		setRecAdrId(recAdrIdObj);
	}

	@Override
	public String getRecCltIdObj() {
		return getRecCltId();
	}

	@Override
	public void setRecCltIdObj(String recCltIdObj) {
		setRecCltId(recCltIdObj);
	}

	@Override
	public String getRecNm() {
		throw new FieldNotMappedException("recNm");
	}

	@Override
	public void setRecNm(String recNm) {
		throw new FieldNotMappedException("recNm");
	}

	@Override
	public String getRecNmObj() {
		return getRecNm();
	}

	@Override
	public void setRecNmObj(String recNmObj) {
		setRecNm(recNmObj);
	}

	@Override
	public String getStAbbObj() {
		return getStAbb();
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		setStAbb(stAbbObj);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int REC_TYP_CD = 5;
		public static final int REC_CLT_ID = 64;
		public static final int REC_ADR_ID = 64;
		public static final int REC_NM_TEXT = 120;
		public static final int LIN1_ADR = 45;
		public static final int LIN2_ADR = 45;
		public static final int CIT_NM = 30;
		public static final int ST_ABB = 2;
		public static final int PST_CD = 13;
		public static final int CER_NBR = 25;
		public static final int REC_SEQ_NBR = 2;
		public static final int REC_CLT_ID_NI = 2;
		public static final int REC_ADR_ID_NI = 2;
		public static final int REC_NM_NI = 2;
		public static final int REC_NM_LEN = 2;
		public static final int REC_NM = REC_NM_LEN + REC_NM_TEXT;
		public static final int LIN1_ADR_NI = 2;
		public static final int LIN2_ADR_NI = 2;
		public static final int CIT_NM_NI = 2;
		public static final int ST_ABB_NI = 2;
		public static final int PST_CD_NI = 2;
		public static final int MNL_IND = 1;
		public static final int CER_NBR_NI = 2;
		public static final int XZH003_ACT_NOT_REC_ROW = CSR_ACT_NBR + NOT_PRC_TS + REC_SEQ_NBR + REC_TYP_CD + REC_CLT_ID_NI + REC_CLT_ID
				+ REC_ADR_ID_NI + REC_ADR_ID + REC_NM_NI + REC_NM + LIN1_ADR_NI + LIN1_ADR + LIN2_ADR_NI + LIN2_ADR + CIT_NM_NI + CIT_NM + ST_ABB_NI
				+ ST_ABB + PST_CD_NI + PST_CD + MNL_IND + CER_NBR_NI + CER_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
