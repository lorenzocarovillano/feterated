/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.enums.W0420HdrExistsSw;
import com.federatedinsurance.crs.ws.enums.WsUdatBrowseSw;
import com.federatedinsurance.crs.ws.enums.WsUdatRowSw;
import com.federatedinsurance.crs.ws.enums.WsUhdrBrowseSw;
import com.federatedinsurance.crs.ws.enums.WsUhdrRowSw;

/**Original name: WS0420-WORK-AREAS<br>
 * Variable: WS0420-WORK-AREAS from program HALRRESP<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ws0420WorkAreas {

	//==== PROPERTIES ====
	//Original name: W0420-HDR-EXISTS-SW
	private W0420HdrExistsSw w0420HdrExistsSw = new W0420HdrExistsSw();
	//Original name: WS-UHDR-BROWSE-SW
	private WsUhdrBrowseSw sUhdrBrowseSw = new WsUhdrBrowseSw();
	//Original name: WS-UDAT-BROWSE-SW
	private WsUdatBrowseSw sUdatBrowseSw = new WsUdatBrowseSw();
	//Original name: WS-UHDR-ROW-SW
	private WsUhdrRowSw sUhdrRowSw = new WsUhdrRowSw();
	//Original name: WS-UDAT-ROW-SW
	private WsUdatRowSw sUdatRowSw = new WsUdatRowSw();

	//==== METHODS ====
	public W0420HdrExistsSw getW0420HdrExistsSw() {
		return w0420HdrExistsSw;
	}

	public WsUdatBrowseSw getsUdatBrowseSw() {
		return sUdatBrowseSw;
	}

	public WsUdatRowSw getsUdatRowSw() {
		return sUdatRowSw;
	}

	public WsUhdrBrowseSw getsUhdrBrowseSw() {
		return sUhdrBrowseSw;
	}

	public WsUhdrRowSw getsUhdrRowSw() {
		return sUhdrRowSw;
	}
}
