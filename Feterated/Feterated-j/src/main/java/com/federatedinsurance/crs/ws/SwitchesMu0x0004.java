/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SwitchesMu0x0004 {

	//==== PROPERTIES ====
	//Original name: SW-PRIMARY-FAX-NBR-FLAG
	private boolean primaryFaxNbrFlag = false;
	//Original name: SW-NAME-OVERFLOW-FOUND-FLAG
	private boolean nameOverflowFoundFlag = false;

	//==== METHODS ====
	public void setPrimaryFaxNbrFlag(boolean primaryFaxNbrFlag) {
		this.primaryFaxNbrFlag = primaryFaxNbrFlag;
	}

	public boolean isPrimaryFaxNbrFlag() {
		return this.primaryFaxNbrFlag;
	}

	public void setNameOverflowFoundFlag(boolean nameOverflowFoundFlag) {
		this.nameOverflowFoundFlag = nameOverflowFoundFlag;
	}

	public boolean isNameOverflowFoundFlag() {
		return this.nameOverflowFoundFlag;
	}
}
