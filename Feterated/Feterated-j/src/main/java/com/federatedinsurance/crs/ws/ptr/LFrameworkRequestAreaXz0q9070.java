/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q9070<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q9070 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q9070() {
	}

	public LFrameworkRequestAreaXz0q9070(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza970qMaxFrmRows(short xza970qMaxFrmRows) {
		writeBinaryShort(Pos.XZA970_MAX_FRM_ROWS, xza970qMaxFrmRows);
	}

	/**Original name: XZA970Q-MAX-FRM-ROWS<br>*/
	public short getXza970qMaxFrmRows() {
		return readBinaryShort(Pos.XZA970_MAX_FRM_ROWS);
	}

	public void setXza970qFrmNbr(String xza970qFrmNbr) {
		writeString(Pos.XZA970_FRM_NBR, xza970qFrmNbr, Len.XZA970_FRM_NBR);
	}

	/**Original name: XZA970Q-FRM-NBR<br>*/
	public String getXza970qFrmNbr() {
		return readString(Pos.XZA970_FRM_NBR, Len.XZA970_FRM_NBR);
	}

	public void setXza970qFrmEdtDt(String xza970qFrmEdtDt) {
		writeString(Pos.XZA970_FRM_EDT_DT, xza970qFrmEdtDt, Len.XZA970_FRM_EDT_DT);
	}

	/**Original name: XZA970Q-FRM-EDT-DT<br>*/
	public String getXza970qFrmEdtDt() {
		return readString(Pos.XZA970_FRM_EDT_DT, Len.XZA970_FRM_EDT_DT);
	}

	public void setXza970qActNotTypCd(String xza970qActNotTypCd) {
		writeString(Pos.XZA970_ACT_NOT_TYP_CD, xza970qActNotTypCd, Len.XZA970_ACT_NOT_TYP_CD);
	}

	/**Original name: XZA970Q-ACT-NOT-TYP-CD<br>*/
	public String getXza970qActNotTypCd() {
		return readString(Pos.XZA970_ACT_NOT_TYP_CD, Len.XZA970_ACT_NOT_TYP_CD);
	}

	public void setXza970qEdlFrmNm(String xza970qEdlFrmNm) {
		writeString(Pos.XZA970_EDL_FRM_NM, xza970qEdlFrmNm, Len.XZA970_EDL_FRM_NM);
	}

	/**Original name: XZA970Q-EDL-FRM-NM<br>*/
	public String getXza970qEdlFrmNm() {
		return readString(Pos.XZA970_EDL_FRM_NM, Len.XZA970_EDL_FRM_NM);
	}

	public void setXza970qFrmDes(String xza970qFrmDes) {
		writeString(Pos.XZA970_FRM_DES, xza970qFrmDes, Len.XZA970_FRM_DES);
	}

	/**Original name: XZA970Q-FRM-DES<br>*/
	public String getXza970qFrmDes() {
		return readString(Pos.XZA970_FRM_DES, Len.XZA970_FRM_DES);
	}

	public void setXza970qSpePrcCd(String xza970qSpePrcCd) {
		writeString(Pos.XZA970_SPE_PRC_CD, xza970qSpePrcCd, Len.XZA970_SPE_PRC_CD);
	}

	/**Original name: XZA970Q-SPE-PRC-CD<br>*/
	public String getXza970qSpePrcCd() {
		return readString(Pos.XZA970_SPE_PRC_CD, Len.XZA970_SPE_PRC_CD);
	}

	public void setXza970qDtnCd(int xza970qDtnCd) {
		writeInt(Pos.XZA970_DTN_CD, xza970qDtnCd, Len.Int.XZA970Q_DTN_CD);
	}

	/**Original name: XZA970Q-DTN-CD<br>*/
	public int getXza970qDtnCd() {
		return readNumDispInt(Pos.XZA970_DTN_CD, Len.XZA970_DTN_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A9070 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA970_ACY_FRM_ROW = L_FW_REQ_XZ0A9070;
		public static final int XZA970_MAX_FRM_ROWS = XZA970_ACY_FRM_ROW;
		public static final int XZA970_ACY_FRM_INFO = XZA970_MAX_FRM_ROWS + Len.XZA970_MAX_FRM_ROWS;
		public static final int XZA970_FRM_NBR = XZA970_ACY_FRM_INFO;
		public static final int XZA970_FRM_EDT_DT = XZA970_FRM_NBR + Len.XZA970_FRM_NBR;
		public static final int XZA970_ACT_NOT_TYP_CD = XZA970_FRM_EDT_DT + Len.XZA970_FRM_EDT_DT;
		public static final int XZA970_EDL_FRM_NM = XZA970_ACT_NOT_TYP_CD + Len.XZA970_ACT_NOT_TYP_CD;
		public static final int XZA970_FRM_DES = XZA970_EDL_FRM_NM + Len.XZA970_EDL_FRM_NM;
		public static final int XZA970_SPE_PRC_CD = XZA970_FRM_DES + Len.XZA970_FRM_DES;
		public static final int XZA970_DTN_CD = XZA970_SPE_PRC_CD + Len.XZA970_SPE_PRC_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA970_MAX_FRM_ROWS = 2;
		public static final int XZA970_FRM_NBR = 30;
		public static final int XZA970_FRM_EDT_DT = 10;
		public static final int XZA970_ACT_NOT_TYP_CD = 5;
		public static final int XZA970_EDL_FRM_NM = 30;
		public static final int XZA970_FRM_DES = 20;
		public static final int XZA970_SPE_PRC_CD = 8;
		public static final int XZA970_DTN_CD = 5;
		public static final int XZA970_ACY_FRM_INFO = XZA970_FRM_NBR + XZA970_FRM_EDT_DT + XZA970_ACT_NOT_TYP_CD + XZA970_EDL_FRM_NM + XZA970_FRM_DES
				+ XZA970_SPE_PRC_CD + XZA970_DTN_CD;
		public static final int XZA970_ACY_FRM_ROW = XZA970_MAX_FRM_ROWS + XZA970_ACY_FRM_INFO;
		public static final int L_FW_REQ_XZ0A9070 = XZA970_ACY_FRM_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A9070;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA970Q_DTN_CD = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
