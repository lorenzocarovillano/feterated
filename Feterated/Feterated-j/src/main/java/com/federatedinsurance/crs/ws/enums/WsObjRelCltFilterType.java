/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-OBJ-REL-CLT-FILTER-TYPE<br>
 * Variable: WS-OBJ-REL-CLT-FILTER-TYPE from program CAWPCORC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsObjRelCltFilterType {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.OBJ_REL_CLT_FILTER_TYPE);
	public static final String ALL = "00";
	public static final String OWN = "01";
	public static final String FNI = "02";
	public static final String OWN_AND_FNI = "03";
	public static final String COWN = "04";

	//==== METHODS ====
	public void setObjRelCltFilterType(String objRelCltFilterType) {
		this.value = Functions.subString(objRelCltFilterType, Len.OBJ_REL_CLT_FILTER_TYPE);
	}

	public String getObjRelCltFilterType() {
		return this.value;
	}

	public boolean isCown() {
		return value.equals(COWN);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OBJ_REL_CLT_FILTER_TYPE = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
