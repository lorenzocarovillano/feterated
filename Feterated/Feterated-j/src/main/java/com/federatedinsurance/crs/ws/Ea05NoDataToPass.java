/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-05-NO-DATA-TO-PASS<br>
 * Variable: EA-05-NO-DATA-TO-PASS from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea05NoDataToPass {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-05-NO-DATA-TO-PASS
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-05-NO-DATA-TO-PASS-1
	private String flr2 = "NO DATA WAS";
	//Original name: FILLER-EA-05-NO-DATA-TO-PASS-2
	private String flr3 = "FOUND TO PASS";
	//Original name: FILLER-EA-05-NO-DATA-TO-PASS-3
	private String flr4 = "TO THE CICS";
	//Original name: FILLER-EA-05-NO-DATA-TO-PASS-4
	private String flr5 = "PROGRAM.";
	//Original name: FILLER-EA-05-NO-DATA-TO-PASS-5
	private String flr6 = "PLEASE CORRECT.";

	//==== METHODS ====
	public String getEa05NoDataToPassFormatted() {
		return MarshalByteExt.bufferToStr(getEa05NoDataToPassBytes());
	}

	public byte[] getEa05NoDataToPassBytes() {
		byte[] buffer = new byte[Len.EA05_NO_DATA_TO_PASS];
		return getEa05NoDataToPassBytes(buffer, 1);
	}

	public byte[] getEa05NoDataToPassBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 12;
		public static final int FLR3 = 14;
		public static final int FLR5 = 10;
		public static final int FLR6 = 15;
		public static final int EA05_NO_DATA_TO_PASS = FLR1 + 2 * FLR2 + FLR3 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
