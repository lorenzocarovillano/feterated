/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclfedRmtPrtTab;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS030199<br>
 * Generated as a class for rule WS.<br>*/
public class Ts030199Data {

	//==== PROPERTIES ====
	//Original name: DCLFED-RMT-PRT-TAB
	private DclfedRmtPrtTab dclfedRmtPrtTab = new DclfedRmtPrtTab();
	//Original name: CF-SC-SUCCESSFUL-RUN
	private int cfScSuccessfulRun = 0;
	//Original name: CF-SC-NO-RECORD-FOUND
	private int cfScNoRecordFound = 100;
	//Original name: CF-SC-NOT-CONNECTED
	private int cfScNotConnected = -927;
	//Original name: EA-00-PROGRAM-MSG
	private Ea00ProgramMsg ea00ProgramMsg = new Ea00ProgramMsg();
	//Original name: EA-01-FATAL-ERROR-MSG
	private Ea01FatalErrorMsgTs030199 ea01FatalErrorMsg = new Ea01FatalErrorMsgTs030199();
	//Original name: SA-COUNT
	private short saCount = ((short) 0);
	//Original name: SA-REPORT-NBR
	private String saReportNbr = DefaultValues.stringVal(Len.SA_REPORT_NBR);

	//==== METHODS ====
	public int getCfScSuccessfulRun() {
		return this.cfScSuccessfulRun;
	}

	public int getCfScNoRecordFound() {
		return this.cfScNoRecordFound;
	}

	public int getCfScNotConnected() {
		return this.cfScNotConnected;
	}

	public void setSaCount(short saCount) {
		this.saCount = saCount;
	}

	public short getSaCount() {
		return this.saCount;
	}

	public void setSaReportNbr(String saReportNbr) {
		this.saReportNbr = Functions.subString(saReportNbr, Len.SA_REPORT_NBR);
	}

	public String getSaReportNbr() {
		return this.saReportNbr;
	}

	public DclfedRmtPrtTab getDclfedRmtPrtTab() {
		return dclfedRmtPrtTab;
	}

	public Ea00ProgramMsg getEa00ProgramMsg() {
		return ea00ProgramMsg;
	}

	public Ea01FatalErrorMsgTs030199 getEa01FatalErrorMsg() {
		return ea01FatalErrorMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_REPORT_NBR = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
