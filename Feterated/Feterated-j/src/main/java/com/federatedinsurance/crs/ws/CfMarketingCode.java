/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-MARKETING-CODE<br>
 * Variable: CF-MARKETING-CODE from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfMarketingCode {

	//==== PROPERTIES ====
	//Original name: CF-MC-UNDERWRITER
	private String underwriter = "UND";
	//Original name: CF-MC-RISK-ANALYST
	private String riskAnalyst = "RSKA";
	//Original name: CF-MC-FLD-PRODUCTION-UW
	private String fldProductionUw = "FPU";
	//Original name: CF-MC-LP-RISK-ANALYST
	private String lpRiskAnalyst = "LPRA";

	//==== METHODS ====
	public String getUnderwriter() {
		return this.underwriter;
	}

	public String getRiskAnalyst() {
		return this.riskAnalyst;
	}

	public String getFldProductionUw() {
		return this.fldProductionUw;
	}

	public String getLpRiskAnalyst() {
		return this.lpRiskAnalyst;
	}
}
