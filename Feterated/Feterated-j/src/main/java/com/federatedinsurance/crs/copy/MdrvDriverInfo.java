/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.notifier.StringChangeNotifier;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.MdrvCommitRollbackInd;
import com.federatedinsurance.crs.ws.enums.MdrvDeleteUowStore;
import com.federatedinsurance.crs.ws.enums.MdrvLoggableErrLogOnlySw;
import com.federatedinsurance.crs.ws.enums.MdrvMaindrvLoggableProblems;
import com.federatedinsurance.crs.ws.enums.MdrvMessageTransferCd;
import com.federatedinsurance.crs.ws.enums.MdrvMsgtranLoggableProblems;
import com.federatedinsurance.crs.ws.enums.MdrvRequestMsgProcessing;
import com.federatedinsurance.crs.ws.enums.MdrvResponseMsgProcessing;
import com.federatedinsurance.crs.ws.enums.MdrvReturnWarningsInd;
import com.federatedinsurance.crs.ws.enums.MdrvSyncpointLocation;
import com.federatedinsurance.crs.ws.enums.MdrvTransElapsedTimeSw;
import com.federatedinsurance.crs.ws.enums.MdrvUowLoggableProblems;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: MDRV-DRIVER-INFO<br>
 * Variable: MDRV-DRIVER-INFO from copybook HALLMDRV<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class MdrvDriverInfo {

	//==== PROPERTIES ====
	//Original name: MDRV-MSG-ID
	private String msgId = DefaultValues.stringVal(Len.MSG_ID);
	//Original name: MDRV-MSG-REC-SEQ
	private String msgRecSeq = DefaultValues.stringVal(Len.MSG_REC_SEQ);
	/**Original name: MDRV-MESSAGE-TRANSFER-CD<br>
	 * <pre>** INCOMING FIELDS</pre>*/
	private MdrvMessageTransferCd messageTransferCd = new MdrvMessageTransferCd();
	//Original name: MDRV-UNIT-OF-WORK
	private String unitOfWork = DefaultValues.stringVal(Len.UNIT_OF_WORK);
	//Original name: MDRV-AUTH-USERID
	private String authUserid = DefaultValues.stringVal(Len.AUTH_USERID);
	//Original name: MDRV-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: MDRV-DELETE-UOW-STORE
	private MdrvDeleteUowStore deleteUowStore = new MdrvDeleteUowStore();
	//Original name: FILLER-MDRV-DRIVER-INFO
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: MDRV-RETURN-WARNINGS-IND
	private MdrvReturnWarningsInd returnWarningsInd = new MdrvReturnWarningsInd();
	/**Original name: MDRV-TOTAL-LENGTH<br>
	 * <pre>** INCOMING FOR REQUEST OR OUTGOING FIELDS FOR RESPONSE.</pre>*/
	private String totalLength = DefaultValues.stringVal(Len.TOTAL_LENGTH);
	//Original name: MDRV-UOW-BUFFER-LENGTH
	private StringChangeNotifier uowBufferLength = new StringChangeNotifier(Len.UOW_BUFFER_LENGTH);
	//Original name: MDRV-NUM-CHUNKS
	private String numChunks = DefaultValues.stringVal(Len.NUM_CHUNKS);
	/**Original name: MDRV-COMMIT-ROLLBACK-IND<br>
	 * <pre>** OUTGOING FIELDS</pre>*/
	private MdrvCommitRollbackInd commitRollbackInd = new MdrvCommitRollbackInd();
	//Original name: MDRV-MAINDRV-LOGGABLE-PROBLEMS
	private MdrvMaindrvLoggableProblems maindrvLoggableProblems = new MdrvMaindrvLoggableProblems();
	//Original name: MDRV-MAIN-DRVR-ERRCODE
	private String mainDrvrErrcode = DefaultValues.stringVal(Len.MAIN_DRVR_ERRCODE);
	//Original name: MDRV-UOW-LOGGABLE-PROBLEMS
	private MdrvUowLoggableProblems uowLoggableProblems = new MdrvUowLoggableProblems();
	//Original name: MDRV-UNIT-OF-WORK-ERRCODE
	private String unitOfWorkErrcode = DefaultValues.stringVal(Len.UNIT_OF_WORK_ERRCODE);
	//Original name: MDRV-ERROR-DETAILS
	private MdrvErrorDetails errorDetails = new MdrvErrorDetails();
	//Original name: MDRV-SESSION-ID
	private String sessionId = DefaultValues.stringVal(Len.SESSION_ID);
	//Original name: FILLER-MDRV-SESSION-LVL-FIELDS
	private char flr2 = ' ';
	/**Original name: MDRV-LOGGABLE-ERR-LOG-ONLY-SW<br>
	 * <pre>**           10 MDRV-BYPASS-MAIN-DRIVER-SW      PIC X.
	 * **              88 MDRV-BYPASS-MAIN-DRIVER      VALUE 'Y'.
	 * **              88 MDRV-DONT-BYPASS-MAIN-DRIVER VALUES 'N', ' '.
	 * ** ERROR FLOOD PARAMETERS</pre>*/
	private MdrvLoggableErrLogOnlySw loggableErrLogOnlySw = new MdrvLoggableErrLogOnlySw();
	/**Original name: MDRV-SYNCPOINT-LOCATION<br>
	 * <pre>** SUPPORT OF MSG TRANSPORT/TRANSFORM AND SYNCPOINT OPTIONS.
	 * ** MSG TRANSPORT/TRANSFORM INCOMING INFO</pre>*/
	private MdrvSyncpointLocation syncpointLocation = new MdrvSyncpointLocation();
	//Original name: MDRV-REQUEST-MSG-PROCESSING
	private MdrvRequestMsgProcessing requestMsgProcessing = new MdrvRequestMsgProcessing();
	//Original name: MDRV-RESPONSE-MSG-PROCESSING
	private MdrvResponseMsgProcessing responseMsgProcessing = new MdrvResponseMsgProcessing();
	/**Original name: MDRV-MSGTRAN-LOGGABLE-PROBLEMS<br>
	 * <pre>** MSG TRANSPORT/TRANSFORM OUTGOING INFO</pre>*/
	private MdrvMsgtranLoggableProblems msgtranLoggableProblems = new MdrvMsgtranLoggableProblems();
	//Original name: MDRV-MSGTRAN-ERRCODE
	private String msgtranErrcode = DefaultValues.stringVal(Len.MSGTRAN_ERRCODE);
	/**Original name: MDRV-TRANS-ELAPSED-TIME-SW<br>
	 * <pre>** CAPTURE OF TRANSACTION ELASPED TIME</pre>*/
	private MdrvTransElapsedTimeSw transElapsedTimeSw = new MdrvTransElapsedTimeSw();
	//Original name: MDRV-TRANS-ELAPSED-TIME-AMT
	private String transElapsedTimeAmt = DefaultValues.stringVal(Len.TRANS_ELAPSED_TIME_AMT);
	/**Original name: FILLER-MDRV-DRIVER-INFO-1<br>
	 * <pre>** REMAINING FILLER FOR MDRV HEADER INFO</pre>*/
	private String flr3 = DefaultValues.stringVal(Len.FLR3);

	//==== METHODS ====
	public void setDriverInfoBytes(byte[] buffer) {
		setDriverInfoBytes(buffer, 1);
	}

	public byte[] getDriverInfoBytes() {
		byte[] buffer = new byte[Len.DRIVER_INFO];
		return getDriverInfoBytes(buffer, 1);
	}

	public void setDriverInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setMdrvMsgKeyBytes(buffer, position);
		position += Len.MSG_KEY;
		messageTransferCd.setMessageTransferCd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		unitOfWork = MarshalByte.readString(buffer, position, Len.UNIT_OF_WORK);
		position += Len.UNIT_OF_WORK;
		authUserid = MarshalByte.readString(buffer, position, Len.AUTH_USERID);
		position += Len.AUTH_USERID;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		deleteUowStore.setDeleteUowStore(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
		position += Len.FLR1;
		returnWarningsInd.setReturnWarningsInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		totalLength = MarshalByte.readFixedString(buffer, position, Len.TOTAL_LENGTH);
		position += Len.TOTAL_LENGTH;
		setUowBufferLength(MarshalByte.readInt(buffer, position, Len.UOW_BUFFER_LENGTH, SignType.NO_SIGN));
		position += Len.UOW_BUFFER_LENGTH;
		numChunks = MarshalByte.readFixedString(buffer, position, Len.NUM_CHUNKS);
		position += Len.NUM_CHUNKS;
		commitRollbackInd.setCommitRollbackInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		maindrvLoggableProblems.setMaindrvLoggableProblems(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		mainDrvrErrcode = MarshalByte.readString(buffer, position, Len.MAIN_DRVR_ERRCODE);
		position += Len.MAIN_DRVR_ERRCODE;
		uowLoggableProblems.setUowLoggableProblems(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		unitOfWorkErrcode = MarshalByte.readString(buffer, position, Len.UNIT_OF_WORK_ERRCODE);
		position += Len.UNIT_OF_WORK_ERRCODE;
		errorDetails.setMdrvErrorDetailsBytes(buffer, position);
		position += MdrvErrorDetails.Len.ERROR_DETAILS;
		setMdrvSessionLvlFieldsBytes(buffer, position);
		position += Len.SESSION_LVL_FIELDS;
		loggableErrLogOnlySw.setLoggableErrLogOnlySw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		syncpointLocation.setSyncpointLocation(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		requestMsgProcessing.setRequestMsgProcessing(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		responseMsgProcessing.setResponseMsgProcessing(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		msgtranLoggableProblems.setMsgtranLoggableProblems(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		msgtranErrcode = MarshalByte.readString(buffer, position, Len.MSGTRAN_ERRCODE);
		position += Len.MSGTRAN_ERRCODE;
		transElapsedTimeSw.setTransElapsedTimeSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		transElapsedTimeAmt = MarshalByte.readFixedString(buffer, position, Len.TRANS_ELAPSED_TIME_AMT);
		position += Len.TRANS_ELAPSED_TIME_AMT;
		flr3 = MarshalByte.readString(buffer, position, Len.FLR3);
	}

	public byte[] getDriverInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		getMsgKeyBytes(buffer, position);
		position += Len.MSG_KEY;
		MarshalByte.writeChar(buffer, position, messageTransferCd.getMessageTransferCd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, unitOfWork, Len.UNIT_OF_WORK);
		position += Len.UNIT_OF_WORK;
		MarshalByte.writeString(buffer, position, authUserid, Len.AUTH_USERID);
		position += Len.AUTH_USERID;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, deleteUowStore.getDeleteUowStore());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeChar(buffer, position, returnWarningsInd.getReturnWarningsInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, totalLength, Len.TOTAL_LENGTH);
		position += Len.TOTAL_LENGTH;
		MarshalByte.writeInt(buffer, position, getUowBufferLength0(), Len.UOW_BUFFER_LENGTH, SignType.NO_SIGN);
		position += Len.UOW_BUFFER_LENGTH;
		MarshalByte.writeString(buffer, position, numChunks, Len.NUM_CHUNKS);
		position += Len.NUM_CHUNKS;
		MarshalByte.writeChar(buffer, position, commitRollbackInd.getCommitRollbackInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, maindrvLoggableProblems.getMaindrvLoggableProblems());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mainDrvrErrcode, Len.MAIN_DRVR_ERRCODE);
		position += Len.MAIN_DRVR_ERRCODE;
		MarshalByte.writeChar(buffer, position, uowLoggableProblems.getUowLoggableProblems());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, unitOfWorkErrcode, Len.UNIT_OF_WORK_ERRCODE);
		position += Len.UNIT_OF_WORK_ERRCODE;
		errorDetails.getErrorDetailsBytes(buffer, position);
		position += MdrvErrorDetails.Len.ERROR_DETAILS;
		getSessionLvlFieldsBytes(buffer, position);
		position += Len.SESSION_LVL_FIELDS;
		MarshalByte.writeChar(buffer, position, loggableErrLogOnlySw.getLoggableErrLogOnlySw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, syncpointLocation.getSyncpointLocation());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, requestMsgProcessing.getRequestMsgProcessing());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, responseMsgProcessing.getResponseMsgProcessing());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, msgtranLoggableProblems.getMsgtranLoggableProblems());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, msgtranErrcode, Len.MSGTRAN_ERRCODE);
		position += Len.MSGTRAN_ERRCODE;
		MarshalByte.writeChar(buffer, position, transElapsedTimeSw.getTransElapsedTimeSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, transElapsedTimeAmt, Len.TRANS_ELAPSED_TIME_AMT);
		position += Len.TRANS_ELAPSED_TIME_AMT;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public void setMdrvMsgKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		msgId = MarshalByte.readString(buffer, position, Len.MSG_ID);
		position += Len.MSG_ID;
		msgRecSeq = MarshalByte.readFixedString(buffer, position, Len.MSG_REC_SEQ);
	}

	public byte[] getMsgKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, msgId, Len.MSG_ID);
		position += Len.MSG_ID;
		MarshalByte.writeString(buffer, position, msgRecSeq, Len.MSG_REC_SEQ);
		return buffer;
	}

	public void setMsgId(String msgId) {
		this.msgId = Functions.subString(msgId, Len.MSG_ID);
	}

	public String getMsgId() {
		return this.msgId;
	}

	public void setMsgRecSeq(short msgRecSeq) {
		this.msgRecSeq = NumericDisplay.asString(msgRecSeq, Len.MSG_REC_SEQ);
	}

	public void setMsgRecSeqFormatted(String msgRecSeq) {
		this.msgRecSeq = Trunc.toUnsignedNumeric(msgRecSeq, Len.MSG_REC_SEQ);
	}

	public short getMsgRecSeq() {
		return NumericDisplay.asShort(this.msgRecSeq);
	}

	public void setUnitOfWork(String unitOfWork) {
		this.unitOfWork = Functions.subString(unitOfWork, Len.UNIT_OF_WORK);
	}

	public String getUnitOfWork() {
		return this.unitOfWork;
	}

	public void setAuthUserid(String authUserid) {
		this.authUserid = Functions.subString(authUserid, Len.AUTH_USERID);
	}

	public String getAuthUserid() {
		return this.authUserid;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setFillerMdrvDriverInfo(String fillerMdrvDriverInfo) {
		this.flr1 = Functions.subString(fillerMdrvDriverInfo, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setTotalLength(int totalLength) {
		this.totalLength = NumericDisplay.asString(totalLength, Len.TOTAL_LENGTH);
	}

	public void setTotalLengthFormatted(String totalLength) {
		this.totalLength = Trunc.toUnsignedNumeric(totalLength, Len.TOTAL_LENGTH);
	}

	public int getTotalLength() {
		return NumericDisplay.asInt(this.totalLength);
	}

	public void setUowBufferLength(int uowBufferLength) {
		this.uowBufferLength.setValue(uowBufferLength);
	}

	public int getUowBufferLength0() {
		return uowBufferLength.getValue();
	}

	public void setNumChunks(short numChunks) {
		this.numChunks = NumericDisplay.asString(numChunks, Len.NUM_CHUNKS);
	}

	public void setNumChunksFormatted(String numChunks) {
		this.numChunks = Trunc.toUnsignedNumeric(numChunks, Len.NUM_CHUNKS);
	}

	public short getNumChunks() {
		return NumericDisplay.asShort(this.numChunks);
	}

	public void setMainDrvrErrcode(String mainDrvrErrcode) {
		this.mainDrvrErrcode = Functions.subString(mainDrvrErrcode, Len.MAIN_DRVR_ERRCODE);
	}

	public String getMainDrvrErrcode() {
		return this.mainDrvrErrcode;
	}

	public void setUnitOfWorkErrcode(String unitOfWorkErrcode) {
		this.unitOfWorkErrcode = Functions.subString(unitOfWorkErrcode, Len.UNIT_OF_WORK_ERRCODE);
	}

	public String getUnitOfWorkErrcode() {
		return this.unitOfWorkErrcode;
	}

	public void setMdrvSessionLvlFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		sessionId = MarshalByte.readString(buffer, position, Len.SESSION_ID);
		position += Len.SESSION_ID;
		flr2 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getSessionLvlFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
		position += Len.SESSION_ID;
		MarshalByte.writeChar(buffer, position, flr2);
		return buffer;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = Functions.subString(sessionId, Len.SESSION_ID);
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setFillerMdrvSessionLvlFields(char fillerMdrvSessionLvlFields) {
		this.flr2 = fillerMdrvSessionLvlFields;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setMsgtranErrcode(String msgtranErrcode) {
		this.msgtranErrcode = Functions.subString(msgtranErrcode, Len.MSGTRAN_ERRCODE);
	}

	public String getMsgtranErrcode() {
		return this.msgtranErrcode;
	}

	public void setTransElapsedTimeAmtFormatted(String transElapsedTimeAmt) {
		this.transElapsedTimeAmt = Trunc.toUnsignedNumeric(transElapsedTimeAmt, Len.TRANS_ELAPSED_TIME_AMT);
	}

	public int getTransElapsedTimeAmt() {
		return NumericDisplay.asInt(this.transElapsedTimeAmt);
	}

	public void setFillerMdrvDriverInfo1(String fillerMdrvDriverInfo1) {
		this.flr3 = Functions.subString(fillerMdrvDriverInfo1, Len.FLR3);
	}

	public String getFlr3() {
		return this.flr3;
	}

	public MdrvCommitRollbackInd getCommitRollbackInd() {
		return commitRollbackInd;
	}

	public MdrvDeleteUowStore getDeleteUowStore() {
		return deleteUowStore;
	}

	public MdrvErrorDetails getErrorDetails() {
		return errorDetails;
	}

	public MdrvLoggableErrLogOnlySw getLoggableErrLogOnlySw() {
		return loggableErrLogOnlySw;
	}

	public MdrvMaindrvLoggableProblems getMaindrvLoggableProblems() {
		return maindrvLoggableProblems;
	}

	public MdrvMessageTransferCd getMessageTransferCd() {
		return messageTransferCd;
	}

	public MdrvMsgtranLoggableProblems getMsgtranLoggableProblems() {
		return msgtranLoggableProblems;
	}

	public MdrvRequestMsgProcessing getRequestMsgProcessing() {
		return requestMsgProcessing;
	}

	public MdrvResponseMsgProcessing getResponseMsgProcessing() {
		return responseMsgProcessing;
	}

	public MdrvReturnWarningsInd getReturnWarningsInd() {
		return returnWarningsInd;
	}

	public MdrvSyncpointLocation getSyncpointLocation() {
		return syncpointLocation;
	}

	public MdrvTransElapsedTimeSw getTransElapsedTimeSw() {
		return transElapsedTimeSw;
	}

	public StringChangeNotifier getUowBufferLength() {
		return uowBufferLength;
	}

	public MdrvUowLoggableProblems getUowLoggableProblems() {
		return uowLoggableProblems;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MSG_ID = 32;
		public static final int MSG_REC_SEQ = 3;
		public static final int UNIT_OF_WORK = 32;
		public static final int AUTH_USERID = 32;
		public static final int TERMINAL_ID = 8;
		public static final int FLR1 = 2;
		public static final int TOTAL_LENGTH = 9;
		public static final int UOW_BUFFER_LENGTH = 5;
		public static final int NUM_CHUNKS = 3;
		public static final int MAIN_DRVR_ERRCODE = 10;
		public static final int UNIT_OF_WORK_ERRCODE = 10;
		public static final int SESSION_ID = 32;
		public static final int MSGTRAN_ERRCODE = 10;
		public static final int TRANS_ELAPSED_TIME_AMT = 8;
		public static final int FLR3 = 191;
		public static final int MSG_KEY = MSG_ID + MSG_REC_SEQ;
		public static final int FLR2 = 1;
		public static final int SESSION_LVL_FIELDS = SESSION_ID + FLR2;
		public static final int DRIVER_INFO = MSG_KEY + MdrvMessageTransferCd.Len.MESSAGE_TRANSFER_CD + UNIT_OF_WORK + AUTH_USERID + TERMINAL_ID
				+ MdrvDeleteUowStore.Len.DELETE_UOW_STORE + MdrvReturnWarningsInd.Len.RETURN_WARNINGS_IND + TOTAL_LENGTH + UOW_BUFFER_LENGTH
				+ NUM_CHUNKS + MdrvCommitRollbackInd.Len.COMMIT_ROLLBACK_IND + MdrvMaindrvLoggableProblems.Len.MAINDRV_LOGGABLE_PROBLEMS
				+ MAIN_DRVR_ERRCODE + MdrvUowLoggableProblems.Len.UOW_LOGGABLE_PROBLEMS + UNIT_OF_WORK_ERRCODE + MdrvErrorDetails.Len.ERROR_DETAILS
				+ SESSION_LVL_FIELDS + MdrvLoggableErrLogOnlySw.Len.LOGGABLE_ERR_LOG_ONLY_SW + MdrvSyncpointLocation.Len.SYNCPOINT_LOCATION
				+ MdrvRequestMsgProcessing.Len.REQUEST_MSG_PROCESSING + MdrvResponseMsgProcessing.Len.RESPONSE_MSG_PROCESSING
				+ MdrvMsgtranLoggableProblems.Len.MSGTRAN_LOGGABLE_PROBLEMS + MSGTRAN_ERRCODE + MdrvTransElapsedTimeSw.Len.TRANS_ELAPSED_TIME_SW
				+ TRANS_ELAPSED_TIME_AMT + FLR1 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
