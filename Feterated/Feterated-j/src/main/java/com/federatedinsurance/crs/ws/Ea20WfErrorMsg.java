/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-20-WF-ERROR-MSG<br>
 * Variable: EA-20-WF-ERROR-MSG from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea20WfErrorMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-20-WF-ERROR-MSG
	private String flr1 = "XZ001000 -";
	//Original name: FILLER-EA-20-WF-ERROR-MSG-1
	private String flr2 = "ERROR ON";
	//Original name: FILLER-EA-20-WF-ERROR-MSG-2
	private String flr3 = "WORKFLOW";
	//Original name: FILLER-EA-20-WF-ERROR-MSG-3
	private String flr4 = "CALL:";
	//Original name: FILLER-EA-20-WF-ERROR-MSG-4
	private String flr5 = "RETURN CODE =";
	//Original name: EA-20-WF-RETURN-CD
	private String ea20WfReturnCd = DefaultValues.stringVal(Len.EA20_WF_RETURN_CD);

	//==== METHODS ====
	public String getEa20WfErrorMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa20WfErrorMsgBytes());
	}

	public byte[] getEa20WfErrorMsgBytes() {
		byte[] buffer = new byte[Len.EA20_WF_ERROR_MSG];
		return getEa20WfErrorMsgBytes(buffer, 1);
	}

	public byte[] getEa20WfErrorMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea20WfReturnCd, Len.EA20_WF_RETURN_CD);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa20WfReturnCd(String ea20WfReturnCd) {
		this.ea20WfReturnCd = Functions.subString(ea20WfReturnCd, Len.EA20_WF_RETURN_CD);
	}

	public String getEa20WfReturnCd() {
		return this.ea20WfReturnCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA20_WF_RETURN_CD = 20;
		public static final int FLR1 = 11;
		public static final int FLR2 = 9;
		public static final int FLR4 = 7;
		public static final int FLR5 = 15;
		public static final int EA20_WF_ERROR_MSG = EA20_WF_RETURN_CD + FLR1 + 2 * FLR2 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
