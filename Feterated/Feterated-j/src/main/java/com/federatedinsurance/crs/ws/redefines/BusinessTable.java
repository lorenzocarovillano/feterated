/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: BUSINESS-TABLE<br>
 * Variable: BUSINESS-TABLE from program CIWBNSRB<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BusinessTable extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int COMPANY_FLD_MAXOCCURS = 76;

	//==== CONSTRUCTORS ====
	public BusinessTable() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.BUSINESS_TABLE;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "AGENCY", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "ASSOC", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "ASSUR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "BUR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CO", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CORP", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "FIRE", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "FUND", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "GROUP", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "INC", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "INS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "LTD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "AUTH", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "COOP", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "COMM", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "NATIONAL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "OFC", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "SAVINGS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "LOAN", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "AGTS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "BROTHERS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CASUALTY", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CATAS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CENTER", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CONSTR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CONTR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "DEPT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "DEVL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "DISTRC", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "DISTRB", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "DIV", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "DBA", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "ENG", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "EST", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "FED", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "FIDELITY", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "GENERAL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "INDEMNITY", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "INTL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MFG", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "NO", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "PROP", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "REINS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "SONS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "SUB", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "SURETY", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "UNDERWRITERS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "USA", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "FIRST", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "FIFTH", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "BANK", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "COLLEGE", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CONSULTANTS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "HOSPITAL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "INDUSTRIES", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "LAB", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MAINT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MGMT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MKTG", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "MUNICIPAL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "PRODUCTS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "RESTAURANT", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "RETAIL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "STORE", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "SYS", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "UNLTD", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "WHOLESALE", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "UNIVERSITY", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "GENRE", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "REP", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "FURN", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "PKG", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "APPL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "COND", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "CONDO", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "BUILDERS", Len.FLR1);
	}

	/**Original name: COMPANY-STD<br>*/
	public String getCompanyStd(int companyStdIdx) {
		int position = Pos.companyStd(companyStdIdx - 1);
		return readString(position, Len.COMPANY_STD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int BUSINESS_TABLE = 1;
		public static final int FLR1 = BUSINESS_TABLE;
		public static final int FLR2 = FLR1 + Len.FLR1;
		public static final int FLR3 = FLR2 + Len.FLR1;
		public static final int FLR4 = FLR3 + Len.FLR1;
		public static final int FLR5 = FLR4 + Len.FLR1;
		public static final int FLR6 = FLR5 + Len.FLR1;
		public static final int FLR7 = FLR6 + Len.FLR1;
		public static final int FLR8 = FLR7 + Len.FLR1;
		public static final int FLR9 = FLR8 + Len.FLR1;
		public static final int FLR10 = FLR9 + Len.FLR1;
		public static final int FLR11 = FLR10 + Len.FLR1;
		public static final int FLR12 = FLR11 + Len.FLR1;
		public static final int FLR13 = FLR12 + Len.FLR1;
		public static final int FLR14 = FLR13 + Len.FLR1;
		public static final int FLR15 = FLR14 + Len.FLR1;
		public static final int FLR16 = FLR15 + Len.FLR1;
		public static final int FLR17 = FLR16 + Len.FLR1;
		public static final int FLR18 = FLR17 + Len.FLR1;
		public static final int FLR19 = FLR18 + Len.FLR1;
		public static final int FLR20 = FLR19 + Len.FLR1;
		public static final int FLR21 = FLR20 + Len.FLR1;
		public static final int FLR22 = FLR21 + Len.FLR1;
		public static final int FLR23 = FLR22 + Len.FLR1;
		public static final int FLR24 = FLR23 + Len.FLR1;
		public static final int FLR25 = FLR24 + Len.FLR1;
		public static final int FLR26 = FLR25 + Len.FLR1;
		public static final int FLR27 = FLR26 + Len.FLR1;
		public static final int FLR28 = FLR27 + Len.FLR1;
		public static final int FLR29 = FLR28 + Len.FLR1;
		public static final int FLR30 = FLR29 + Len.FLR1;
		public static final int FLR31 = FLR30 + Len.FLR1;
		public static final int FLR32 = FLR31 + Len.FLR1;
		public static final int FLR33 = FLR32 + Len.FLR1;
		public static final int FLR34 = FLR33 + Len.FLR1;
		public static final int FLR35 = FLR34 + Len.FLR1;
		public static final int FLR36 = FLR35 + Len.FLR1;
		public static final int FLR37 = FLR36 + Len.FLR1;
		public static final int FLR38 = FLR37 + Len.FLR1;
		public static final int FLR39 = FLR38 + Len.FLR1;
		public static final int FLR40 = FLR39 + Len.FLR1;
		public static final int FLR41 = FLR40 + Len.FLR1;
		public static final int FLR42 = FLR41 + Len.FLR1;
		public static final int FLR43 = FLR42 + Len.FLR1;
		public static final int FLR44 = FLR43 + Len.FLR1;
		public static final int FLR45 = FLR44 + Len.FLR1;
		public static final int FLR46 = FLR45 + Len.FLR1;
		public static final int FLR47 = FLR46 + Len.FLR1;
		public static final int FLR48 = FLR47 + Len.FLR1;
		public static final int FLR49 = FLR48 + Len.FLR1;
		public static final int FLR50 = FLR49 + Len.FLR1;
		public static final int FLR51 = FLR50 + Len.FLR1;
		public static final int FLR52 = FLR51 + Len.FLR1;
		public static final int FLR53 = FLR52 + Len.FLR1;
		public static final int FLR54 = FLR53 + Len.FLR1;
		public static final int FLR55 = FLR54 + Len.FLR1;
		public static final int FLR56 = FLR55 + Len.FLR1;
		public static final int FLR57 = FLR56 + Len.FLR1;
		public static final int FLR58 = FLR57 + Len.FLR1;
		public static final int FLR59 = FLR58 + Len.FLR1;
		public static final int FLR60 = FLR59 + Len.FLR1;
		public static final int FLR61 = FLR60 + Len.FLR1;
		public static final int FLR62 = FLR61 + Len.FLR1;
		public static final int FLR63 = FLR62 + Len.FLR1;
		public static final int FLR64 = FLR63 + Len.FLR1;
		public static final int FLR65 = FLR64 + Len.FLR1;
		public static final int FLR66 = FLR65 + Len.FLR1;
		public static final int FLR67 = FLR66 + Len.FLR1;
		public static final int FLR68 = FLR67 + Len.FLR1;
		public static final int FLR69 = FLR68 + Len.FLR1;
		public static final int FLR70 = FLR69 + Len.FLR1;
		public static final int FLR71 = FLR70 + Len.FLR1;
		public static final int FLR72 = FLR71 + Len.FLR1;
		public static final int FLR73 = FLR72 + Len.FLR1;
		public static final int FLR74 = FLR73 + Len.FLR1;
		public static final int FLR75 = FLR74 + Len.FLR1;
		public static final int FLR76 = FLR75 + Len.FLR1;
		public static final int BUS_TABLE_REDEF = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int companyFld(int idx) {
			return BUS_TABLE_REDEF + idx * Len.COMPANY_FLD;
		}

		public static int companyStd(int idx) {
			return companyFld(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 12;
		public static final int COMPANY_STD = 12;
		public static final int COMPANY_FLD = COMPANY_STD;
		public static final int BUSINESS_TABLE = 76 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
