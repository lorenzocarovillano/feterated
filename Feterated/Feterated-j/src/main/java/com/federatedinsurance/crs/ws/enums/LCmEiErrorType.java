/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: L-CM-EI-ERROR-TYPE<br>
 * Variable: L-CM-EI-ERROR-TYPE from copybook TS54801<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LCmEiErrorType {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char REGION_SWAPPED = '1';
	public static final char SYSTEM_WARNING = '2';

	//==== METHODS ====
	public void setErrorType(char errorType) {
		this.value = errorType;
	}

	public char getErrorType() {
		return this.value;
	}

	public boolean isCmEiEtRegionSwapped() {
		return value == REGION_SWAPPED;
	}

	public void setRegionSwapped() {
		value = REGION_SWAPPED;
	}

	public boolean isCmEiEtSystemWarning() {
		return value == SYSTEM_WARNING;
	}

	public void setSystemWarning() {
		value = SYSTEM_WARNING;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_TYPE = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
