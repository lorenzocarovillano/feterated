/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW03H-CLIENT-ADDRESS-ROW<br>
 * Variable: CW03H-CLIENT-ADDRESS-ROW from copybook CAWLH003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Cw03hClientAddressRow {

	//==== PROPERTIES ====
	//Original name: CW03H-ADR-ID
	private String adrId = DefaultValues.stringVal(Len.ADR_ID);

	//==== METHODS ====
	public void setAdrId(String adrId) {
		this.adrId = Functions.subString(adrId, Len.ADR_ID);
	}

	public String getAdrId() {
		return this.adrId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ADR_ID = 20;
		public static final int CICA_ADR1 = 45;
		public static final int CICA_ADR2 = 45;
		public static final int CICA_CIT_NM = 30;
		public static final int CICA_CTY = 30;
		public static final int ST_CD = 3;
		public static final int CICA_PST_CD = 13;
		public static final int CTR_CD = 4;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CICA_EFF_ACY_TS = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
