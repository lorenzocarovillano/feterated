/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9010<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9010 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZA910_POL_NBR_MAXOCCURS = 100;
	public static final int L_FW_RESP_XZ0A9011_MAXOCCURS = 20;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9010() {
	}

	public LFrameworkResponseAreaXz0r9010(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza910MaxWrdRows(short xza910MaxWrdRows) {
		writeBinaryShort(Pos.XZA910_MAX_WRD_ROWS, xza910MaxWrdRows);
	}

	/**Original name: XZA910-MAX-WRD-ROWS<br>*/
	public short getXza910MaxWrdRows() {
		return readBinaryShort(Pos.XZA910_MAX_WRD_ROWS);
	}

	public void setXza910CsrActNbr(String xza910CsrActNbr) {
		writeString(Pos.XZA910_CSR_ACT_NBR, xza910CsrActNbr, Len.XZA910_CSR_ACT_NBR);
	}

	/**Original name: XZA910-CSR-ACT-NBR<br>*/
	public String getXza910CsrActNbr() {
		return readString(Pos.XZA910_CSR_ACT_NBR, Len.XZA910_CSR_ACT_NBR);
	}

	public void setXza910NotPrcTs(String xza910NotPrcTs) {
		writeString(Pos.XZA910_NOT_PRC_TS, xza910NotPrcTs, Len.XZA910_NOT_PRC_TS);
	}

	/**Original name: XZA910-NOT-PRC-TS<br>*/
	public String getXza910NotPrcTs() {
		return readString(Pos.XZA910_NOT_PRC_TS, Len.XZA910_NOT_PRC_TS);
	}

	public void setXza910PolNbr(int xza910PolNbrIdx, String xza910PolNbr) {
		int position = Pos.xza910PolNbr(xza910PolNbrIdx - 1);
		writeString(position, xza910PolNbr, Len.XZA910_POL_NBR);
	}

	/**Original name: XZA910-POL-NBR<br>*/
	public String getXza910PolNbr(int xza910PolNbrIdx) {
		int position = Pos.xza910PolNbr(xza910PolNbrIdx - 1);
		return readString(position, Len.XZA910_POL_NBR);
	}

	public String getlFwRespXz0a9011Formatted(int lFwRespXz0a9011Idx) {
		int position = Pos.lFwRespXz0a9011(lFwRespXz0a9011Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9011);
	}

	public void setXza911rWrdSeqCd(int xza911rWrdSeqCdIdx, String xza911rWrdSeqCd) {
		int position = Pos.xza911WrdSeqCd(xza911rWrdSeqCdIdx - 1);
		writeString(position, xza911rWrdSeqCd, Len.XZA911_WRD_SEQ_CD);
	}

	/**Original name: XZA911R-WRD-SEQ-CD<br>*/
	public String getXza911rWrdSeqCd(int xza911rWrdSeqCdIdx) {
		int position = Pos.xza911WrdSeqCd(xza911rWrdSeqCdIdx - 1);
		return readString(position, Len.XZA911_WRD_SEQ_CD);
	}

	public void setXza911rWrdFont(int xza911rWrdFontIdx, String xza911rWrdFont) {
		int position = Pos.xza911WrdFont(xza911rWrdFontIdx - 1);
		writeString(position, xza911rWrdFont, Len.XZA911_WRD_FONT);
	}

	/**Original name: XZA911R-WRD-FONT<br>*/
	public String getXza911rWrdFont(int xza911rWrdFontIdx) {
		int position = Pos.xza911WrdFont(xza911rWrdFontIdx - 1);
		return readString(position, Len.XZA911_WRD_FONT);
	}

	public void setXza911rWrdTxt(int xza911rWrdTxtIdx, String xza911rWrdTxt) {
		int position = Pos.xza911WrdTxt(xza911rWrdTxtIdx - 1);
		writeString(position, xza911rWrdTxt, Len.XZA911_WRD_TXT);
	}

	/**Original name: XZA911R-WRD-TXT<br>*/
	public String getXza911rWrdTxt(int xza911rWrdTxtIdx) {
		int position = Pos.xza911WrdTxt(xza911rWrdTxtIdx - 1);
		return readString(position, Len.XZA911_WRD_TXT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_XZ0A9010 = L_FRAMEWORK_RESPONSE_AREA;
		public static final int XZA910_ACT_NOT_WRD_ROW = L_FW_RESP_XZ0A9010;
		public static final int XZA910_MAX_WRD_ROWS = XZA910_ACT_NOT_WRD_ROW;
		public static final int XZA910_CSR_ACT_NBR = XZA910_MAX_WRD_ROWS + Len.XZA910_MAX_WRD_ROWS;
		public static final int XZA910_NOT_PRC_TS = XZA910_CSR_ACT_NBR + Len.XZA910_CSR_ACT_NBR;
		public static final int XZA910_POL_LIST = XZA910_NOT_PRC_TS + Len.XZA910_NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xza910PolNbr(int idx) {
			return XZA910_POL_LIST + idx * Len.XZA910_POL_NBR;
		}

		public static int lFwRespXz0a9011(int idx) {
			return xza910PolNbr(XZA910_POL_NBR_MAXOCCURS - 1) + Len.XZA910_POL_NBR + idx * Len.L_FW_RESP_XZ0A9011;
		}

		public static int xza911ActNotWrdTextRow(int idx) {
			return lFwRespXz0a9011(idx);
		}

		public static int xza911WrdSeqCd(int idx) {
			return xza911ActNotWrdTextRow(idx);
		}

		public static int xza911WrdFont(int idx) {
			return xza911WrdSeqCd(idx) + Len.XZA911_WRD_SEQ_CD;
		}

		public static int xza911WrdTxt(int idx) {
			return xza911WrdFont(idx) + Len.XZA911_WRD_FONT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA910_MAX_WRD_ROWS = 2;
		public static final int XZA910_CSR_ACT_NBR = 9;
		public static final int XZA910_NOT_PRC_TS = 26;
		public static final int XZA910_POL_NBR = 25;
		public static final int XZA911_WRD_SEQ_CD = 5;
		public static final int XZA911_WRD_FONT = 6;
		public static final int XZA911_WRD_TXT = 4500;
		public static final int XZA911_ACT_NOT_WRD_TEXT_ROW = XZA911_WRD_SEQ_CD + XZA911_WRD_FONT + XZA911_WRD_TXT;
		public static final int L_FW_RESP_XZ0A9011 = XZA911_ACT_NOT_WRD_TEXT_ROW;
		public static final int XZA910_POL_LIST = LFrameworkResponseAreaXz0r9010.XZA910_POL_NBR_MAXOCCURS * XZA910_POL_NBR;
		public static final int XZA910_ACT_NOT_WRD_ROW = XZA910_MAX_WRD_ROWS + XZA910_CSR_ACT_NBR + XZA910_NOT_PRC_TS + XZA910_POL_LIST;
		public static final int L_FW_RESP_XZ0A9010 = XZA910_ACT_NOT_WRD_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_XZ0A9010
				+ LFrameworkResponseAreaXz0r9010.L_FW_RESP_XZ0A9011_MAXOCCURS * L_FW_RESP_XZ0A9011;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
