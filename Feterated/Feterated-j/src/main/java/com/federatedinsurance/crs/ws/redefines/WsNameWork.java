/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-NAME-WORK<br>
 * Variable: WS-NAME-WORK from program CIWBNSRB<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsNameWork extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int FLR1_MAXOCCURS = 60;

	//==== CONSTRUCTORS ====
	public WsNameWork() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_NAME_WORK;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "", Len.WS_NAME_WORK);
	}

	public void setWsNameWork(String wsNameWork) {
		writeString(Pos.WS_NAME_WORK, wsNameWork, Len.WS_NAME_WORK);
	}

	/**Original name: WS-NAME-WORK<br>*/
	public String getWsNameWork() {
		return readString(Pos.WS_NAME_WORK, Len.WS_NAME_WORK);
	}

	public void setWorkByte(int workByteIdx, char workByte) {
		int position = Pos.wsWorkByte(workByteIdx - 1);
		writeChar(position, workByte);
	}

	/**Original name: WS-WORK-BYTE<br>*/
	public char getWorkByte(int workByteIdx) {
		int position = Pos.wsWorkByte(workByteIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_NAME_WORK = 1;
		public static final int WS_NAME_WORK_BYTE = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int flr1(int idx) {
			return WS_NAME_WORK_BYTE + idx * Len.FLR1;
		}

		public static int wsWorkByte(int idx) {
			return flr1(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int WORK_BYTE = 1;
		public static final int FLR1 = WORK_BYTE;
		public static final int WS_NAME_WORK = 60;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
