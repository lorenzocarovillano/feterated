/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HAL_ERR_TRAN_SUP_V]
 * 
 */
public interface IHalErrTranSupV extends BaseSqlTo {

	/**
	 * Host Variable HETS-ERR-PTY-NBR
	 * 
	 */
	short getPtyNbr();

	void setPtyNbr(short ptyNbr);

	/**
	 * Host Variable HETS-ERR-TXT
	 * 
	 */
	String getTxt();

	void setTxt(String txt);
};
