/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLHAL-BO-MDU-XRF-V<br>
 * Variable: DCLHAL-BO-MDU-XRF-V from copybook HALLGBMX<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclhalBoMduXrfV {

	//==== PROPERTIES ====
	//Original name: HBMX-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: HBMX-BOBJ-MDU-NM
	private String bobjMduNm = DefaultValues.stringVal(Len.BOBJ_MDU_NM);
	//Original name: HBMX-DP-DFL-MDU-NM
	private String dpDflMduNm = DefaultValues.stringVal(Len.DP_DFL_MDU_NM);

	//==== METHODS ====
	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public String getBusObjNmFormatted() {
		return Functions.padBlanks(getBusObjNm(), Len.BUS_OBJ_NM);
	}

	public void setBobjMduNm(String bobjMduNm) {
		this.bobjMduNm = Functions.subString(bobjMduNm, Len.BOBJ_MDU_NM);
	}

	public String getBobjMduNm() {
		return this.bobjMduNm;
	}

	public String getBobjMduNmFormatted() {
		return Functions.padBlanks(getBobjMduNm(), Len.BOBJ_MDU_NM);
	}

	public void setDpDflMduNm(String dpDflMduNm) {
		this.dpDflMduNm = Functions.subString(dpDflMduNm, Len.DP_DFL_MDU_NM);
	}

	public String getDpDflMduNm() {
		return this.dpDflMduNm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUS_OBJ_NM = 32;
		public static final int BOBJ_MDU_NM = 32;
		public static final int CPX_ED_MDU_NM = 32;
		public static final int DP_DFL_MDU_NM = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
