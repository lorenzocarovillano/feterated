/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [FED_RMT_PRT_TAB]
 * 
 */
public interface IFedRmtPrtTab extends BaseSqlTo {

	/**
	 * Host Variable RPT-NBR
	 * 
	 */
	String getRptNbr();

	void setRptNbr(String rptNbr);

	/**
	 * Host Variable RPT-DESC
	 * 
	 */
	String getRptDesc();

	void setRptDesc(String rptDesc);

	/**
	 * Host Variable RPT-ACT-FLG
	 * 
	 */
	char getRptActFlg();

	void setRptActFlg(char rptActFlg);

	/**
	 * Host Variable RPT-BAL-FLG
	 * 
	 */
	char getRptBalFlg();

	void setRptBalFlg(char rptBalFlg);

	/**
	 * Host Variable OFF-LOC-1
	 * 
	 */
	String getOffLoc1();

	void setOffLoc1(String offLoc1);

	/**
	 * Host Variable OFF-LOC-FLG-1
	 * 
	 */
	char getOffLocFlg1();

	void setOffLocFlg1(char offLocFlg1);

	/**
	 * Host Variable OFF-LOC-2
	 * 
	 */
	String getOffLoc2();

	void setOffLoc2(String offLoc2);

	/**
	 * Host Variable OFF-LOC-FLG-2
	 * 
	 */
	char getOffLocFlg2();

	void setOffLocFlg2(char offLocFlg2);

	/**
	 * Host Variable OFF-LOC-3
	 * 
	 */
	String getOffLoc3();

	void setOffLoc3(String offLoc3);

	/**
	 * Host Variable OFF-LOC-FLG-3
	 * 
	 */
	char getOffLocFlg3();

	void setOffLocFlg3(char offLocFlg3);

	/**
	 * Host Variable OFF-LOC-4
	 * 
	 */
	String getOffLoc4();

	void setOffLoc4(String offLoc4);

	/**
	 * Host Variable OFF-LOC-FLG-4
	 * 
	 */
	char getOffLocFlg4();

	void setOffLocFlg4(char offLocFlg4);

	/**
	 * Host Variable OFF-LOC-5
	 * 
	 */
	String getOffLoc5();

	void setOffLoc5(String offLoc5);

	/**
	 * Host Variable OFF-LOC-FLG-5
	 * 
	 */
	char getOffLocFlg5();

	void setOffLocFlg5(char offLocFlg5);

	/**
	 * Host Variable OFF-LOC-6
	 * 
	 */
	String getOffLoc6();

	void setOffLoc6(String offLoc6);

	/**
	 * Host Variable OFF-LOC-FLG-6
	 * 
	 */
	char getOffLocFlg6();

	void setOffLocFlg6(char offLocFlg6);

	/**
	 * Host Variable OFF-LOC-7
	 * 
	 */
	String getOffLoc7();

	void setOffLoc7(String offLoc7);

	/**
	 * Host Variable OFF-LOC-FLG-7
	 * 
	 */
	char getOffLocFlg7();

	void setOffLocFlg7(char offLocFlg7);

	/**
	 * Host Variable OFF-LOC-8
	 * 
	 */
	String getOffLoc8();

	void setOffLoc8(String offLoc8);

	/**
	 * Host Variable OFF-LOC-FLG-8
	 * 
	 */
	char getOffLocFlg8();

	void setOffLocFlg8(char offLocFlg8);

	/**
	 * Host Variable OFF-LOC-9
	 * 
	 */
	String getOffLoc9();

	void setOffLoc9(String offLoc9);

	/**
	 * Host Variable OFF-LOC-FLG-9
	 * 
	 */
	char getOffLocFlg9();

	void setOffLocFlg9(char offLocFlg9);

	/**
	 * Host Variable OFF-LOC-10
	 * 
	 */
	String getOffLoc10();

	void setOffLoc10(String offLoc10);

	/**
	 * Host Variable OFF-LOC-FLG-10
	 * 
	 */
	char getOffLocFlg10();

	void setOffLocFlg10(char offLocFlg10);

	/**
	 * Host Variable OFF-LOC-11
	 * 
	 */
	String getOffLoc11();

	void setOffLoc11(String offLoc11);

	/**
	 * Host Variable OFF-LOC-FLG-11
	 * 
	 */
	char getOffLocFlg11();

	void setOffLocFlg11(char offLocFlg11);

	/**
	 * Host Variable OFF-LOC-12
	 * 
	 */
	String getOffLoc12();

	void setOffLoc12(String offLoc12);

	/**
	 * Host Variable OFF-LOC-FLG-12
	 * 
	 */
	char getOffLocFlg12();

	void setOffLocFlg12(char offLocFlg12);

	/**
	 * Host Variable OFF-LOC-13
	 * 
	 */
	String getOffLoc13();

	void setOffLoc13(String offLoc13);

	/**
	 * Host Variable OFF-LOC-FLG-13
	 * 
	 */
	char getOffLocFlg13();

	void setOffLocFlg13(char offLocFlg13);

	/**
	 * Host Variable OFF-LOC-14
	 * 
	 */
	String getOffLoc14();

	void setOffLoc14(String offLoc14);

	/**
	 * Host Variable OFF-LOC-FLG-14
	 * 
	 */
	char getOffLocFlg14();

	void setOffLocFlg14(char offLocFlg14);

	/**
	 * Host Variable OFF-LOC-15
	 * 
	 */
	String getOffLoc15();

	void setOffLoc15(String offLoc15);

	/**
	 * Host Variable OFF-LOC-FLG-15
	 * 
	 */
	char getOffLocFlg15();

	void setOffLocFlg15(char offLocFlg15);

	/**
	 * Host Variable OFF-LOC-DEFLT-FLG
	 * 
	 */
	char getOffLocDefltFlg();

	void setOffLocDefltFlg(char offLocDefltFlg);
};
