/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.federatedinsurance.crs.ws.enums.WsAddUpdateSw;
import com.federatedinsurance.crs.ws.enums.WsEndOfCursor1Sw;
import com.federatedinsurance.crs.ws.enums.WsEndOfCursor2Sw;
import com.federatedinsurance.crs.ws.enums.WsEndOfCursor3Sw;
import com.federatedinsurance.crs.ws.enums.WsHeaderFoundSw;
import com.federatedinsurance.crs.ws.enums.WsUowLockTsqSw;

/**Original name: WS-WORKFIELDS<br>
 * Variable: WS-WORKFIELDS from program HALRLOMG<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkfieldsHalrlomg {

	//==== PROPERTIES ====
	//Original name: WS-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-UCT-ROW-COUNTER
	private short uctRowCounter = ((short) 0);
	//Original name: WS-TMO-VALUE
	private short tmoValue = ((short) 0);
	//Original name: WS-TS-LOCK-ROW-CNT
	private short tsLockRowCnt = ((short) 0);
	//Original name: WS-EXISTING-LOCK-TYPE
	private char existingLockType = Types.SPACE_CHAR;
	//Original name: WS-REQUESTED-LOCK-TYPE
	private char requestedLockType = Types.SPACE_CHAR;
	//Original name: WS-END-OF-CURSOR1-SW
	private WsEndOfCursor1Sw endOfCursor1Sw = new WsEndOfCursor1Sw();
	//Original name: WS-END-OF-CURSOR2-SW
	private WsEndOfCursor2Sw endOfCursor2Sw = new WsEndOfCursor2Sw();
	//Original name: WS-END-OF-CURSOR3-SW
	private WsEndOfCursor3Sw endOfCursor3Sw = new WsEndOfCursor3Sw();
	//Original name: WS-ADD-UPDATE-SW
	private WsAddUpdateSw addUpdateSw = new WsAddUpdateSw();
	//Original name: WS-HEADER-FOUND-SW
	private WsHeaderFoundSw headerFoundSw = new WsHeaderFoundSw();
	//Original name: WS-UOW-LOCK-TSQ-SW
	private WsUowLockTsqSw uowLockTsqSw = new WsUowLockTsqSw();

	//==== METHODS ====
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public void setUctRowCounter(short uctRowCounter) {
		this.uctRowCounter = uctRowCounter;
	}

	public short getUctRowCounter() {
		return this.uctRowCounter;
	}

	public void setTmoValue(short tmoValue) {
		this.tmoValue = tmoValue;
	}

	public short getTmoValue() {
		return this.tmoValue;
	}

	public void setTsLockRowCnt(short tsLockRowCnt) {
		this.tsLockRowCnt = tsLockRowCnt;
	}

	public short getTsLockRowCnt() {
		return this.tsLockRowCnt;
	}

	public void setExistingLockType(char existingLockType) {
		this.existingLockType = existingLockType;
	}

	public char getExistingLockType() {
		return this.existingLockType;
	}

	public void setRequestedLockType(char requestedLockType) {
		this.requestedLockType = requestedLockType;
	}

	public char getRequestedLockType() {
		return this.requestedLockType;
	}

	public WsAddUpdateSw getAddUpdateSw() {
		return addUpdateSw;
	}

	public WsEndOfCursor1Sw getEndOfCursor1Sw() {
		return endOfCursor1Sw;
	}

	public WsEndOfCursor2Sw getEndOfCursor2Sw() {
		return endOfCursor2Sw;
	}

	public WsEndOfCursor3Sw getEndOfCursor3Sw() {
		return endOfCursor3Sw;
	}

	public WsHeaderFoundSw getHeaderFoundSw() {
		return headerFoundSw;
	}

	public WsUowLockTsqSw getUowLockTsqSw() {
		return uowLockTsqSw;
	}
}
