/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ACT_NOT_TYP]
 * 
 */
public interface IActNotTyp extends BaseSqlTo {

	/**
	 * Host Variable ACT-NOT-TYP-CD
	 * 
	 */
	String getActNotTypCd();

	void setActNotTypCd(String actNotTypCd);

	/**
	 * Host Variable ACT-NOT-DES
	 * 
	 */
	String getActNotDes();

	void setActNotDes(String actNotDes);

	/**
	 * Host Variable DSY-ORD-NBR
	 * 
	 */
	short getDsyOrdNbr();

	void setDsyOrdNbr(short dsyOrdNbr);

	/**
	 * Nullable property for DSY-ORD-NBR
	 * 
	 */
	Short getDsyOrdNbrObj();

	void setDsyOrdNbrObj(Short dsyOrdNbrObj);

	/**
	 * Host Variable DOC-DES
	 * 
	 */
	String getDocDes();

	void setDocDes(String docDes);

	/**
	 * Nullable property for DOC-DES
	 * 
	 */
	String getDocDesObj();

	void setDocDesObj(String docDesObj);

	/**
	 * Host Variable ACT-NOT-PTH-CD
	 * 
	 */
	String getActNotPthCd();

	void setActNotPthCd(String actNotPthCd);

	/**
	 * Host Variable ACT-NOT-PTH-DES
	 * 
	 */
	String getActNotPthDes();

	void setActNotPthDes(String actNotPthDes);
};
