/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: RETURNED-ERROR-SW<br>
 * Variable: RETURNED-ERROR-SW from copybook CISLNSRB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class ReturnedErrorSw {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char ERROR_FOUND = 'E';

	//==== METHODS ====
	public void setReturnedErrorSw(char returnedErrorSw) {
		this.value = returnedErrorSw;
	}

	public void setReturnedErrorSwFormatted(String returnedErrorSw) {
		setReturnedErrorSw(Functions.charAt(returnedErrorSw, Types.CHAR_SIZE));
	}

	public char getReturnedErrorSw() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RETURNED_ERROR_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
