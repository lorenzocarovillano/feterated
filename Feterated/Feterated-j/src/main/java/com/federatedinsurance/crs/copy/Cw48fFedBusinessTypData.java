/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW48F-FED-BUSINESS-TYP-DATA<br>
 * Variable: CW48F-FED-BUSINESS-TYP-DATA from copybook CAWLF048<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw48fFedBusinessTypData {

	//==== PROPERTIES ====
	/**Original name: CW48F-TOB-SIC-TYP-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char tobSicTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-TOB-SIC-TYP-CD
	private String tobSicTypCd = DefaultValues.stringVal(Len.TOB_SIC_TYP_CD);
	//Original name: CW48F-TOB-PTY-CD-CI
	private char tobPtyCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-TOB-PTY-CD
	private String tobPtyCd = DefaultValues.stringVal(Len.TOB_PTY_CD);
	//Original name: CW48F-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW48F-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW48F-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW48F-EXPIRATION-DT-CI
	private char expirationDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-EXPIRATION-DT
	private String expirationDt = DefaultValues.stringVal(Len.EXPIRATION_DT);
	//Original name: CW48F-EFFECTIVE-ACY-TS-CI
	private char effectiveAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-EFFECTIVE-ACY-TS
	private String effectiveAcyTs = DefaultValues.stringVal(Len.EFFECTIVE_ACY_TS);
	//Original name: CW48F-EXPIRATION-ACY-TS-CI
	private char expirationAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-EXPIRATION-ACY-TS-NI
	private char expirationAcyTsNi = DefaultValues.CHAR_VAL;
	//Original name: CW48F-EXPIRATION-ACY-TS
	private String expirationAcyTs = DefaultValues.stringVal(Len.EXPIRATION_ACY_TS);
	//Original name: CW48F-TOB-CD-DESC
	private String tobCdDesc = DefaultValues.stringVal(Len.TOB_CD_DESC);

	//==== METHODS ====
	public void setFedBusinessTypDataBytes(byte[] buffer, int offset) {
		int position = offset;
		tobSicTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tobSicTypCd = MarshalByte.readString(buffer, position, Len.TOB_SIC_TYP_CD);
		position += Len.TOB_SIC_TYP_CD;
		tobPtyCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		tobPtyCd = MarshalByte.readString(buffer, position, Len.TOB_PTY_CD);
		position += Len.TOB_PTY_CD;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		expirationDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		expirationDt = MarshalByte.readString(buffer, position, Len.EXPIRATION_DT);
		position += Len.EXPIRATION_DT;
		effectiveAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		effectiveAcyTs = MarshalByte.readString(buffer, position, Len.EFFECTIVE_ACY_TS);
		position += Len.EFFECTIVE_ACY_TS;
		expirationAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		expirationAcyTsNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		expirationAcyTs = MarshalByte.readString(buffer, position, Len.EXPIRATION_ACY_TS);
		position += Len.EXPIRATION_ACY_TS;
		tobCdDesc = MarshalByte.readString(buffer, position, Len.TOB_CD_DESC);
	}

	public byte[] getFedBusinessTypDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, tobSicTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tobSicTypCd, Len.TOB_SIC_TYP_CD);
		position += Len.TOB_SIC_TYP_CD;
		MarshalByte.writeChar(buffer, position, tobPtyCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, tobPtyCd, Len.TOB_PTY_CD);
		position += Len.TOB_PTY_CD;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, expirationDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationDt, Len.EXPIRATION_DT);
		position += Len.EXPIRATION_DT;
		MarshalByte.writeChar(buffer, position, effectiveAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
		position += Len.EFFECTIVE_ACY_TS;
		MarshalByte.writeChar(buffer, position, expirationAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, expirationAcyTsNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationAcyTs, Len.EXPIRATION_ACY_TS);
		position += Len.EXPIRATION_ACY_TS;
		MarshalByte.writeString(buffer, position, tobCdDesc, Len.TOB_CD_DESC);
		return buffer;
	}

	public void setTobSicTypCdCi(char tobSicTypCdCi) {
		this.tobSicTypCdCi = tobSicTypCdCi;
	}

	public char getTobSicTypCdCi() {
		return this.tobSicTypCdCi;
	}

	public void setTobSicTypCd(String tobSicTypCd) {
		this.tobSicTypCd = Functions.subString(tobSicTypCd, Len.TOB_SIC_TYP_CD);
	}

	public String getTobSicTypCd() {
		return this.tobSicTypCd;
	}

	public void setTobPtyCdCi(char tobPtyCdCi) {
		this.tobPtyCdCi = tobPtyCdCi;
	}

	public char getTobPtyCdCi() {
		return this.tobPtyCdCi;
	}

	public void setTobPtyCd(String tobPtyCd) {
		this.tobPtyCd = Functions.subString(tobPtyCd, Len.TOB_PTY_CD);
	}

	public String getTobPtyCd() {
		return this.tobPtyCd;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setExpirationDtCi(char expirationDtCi) {
		this.expirationDtCi = expirationDtCi;
	}

	public char getExpirationDtCi() {
		return this.expirationDtCi;
	}

	public void setExpirationDt(String expirationDt) {
		this.expirationDt = Functions.subString(expirationDt, Len.EXPIRATION_DT);
	}

	public String getExpirationDt() {
		return this.expirationDt;
	}

	public void setEffectiveAcyTsCi(char effectiveAcyTsCi) {
		this.effectiveAcyTsCi = effectiveAcyTsCi;
	}

	public char getEffectiveAcyTsCi() {
		return this.effectiveAcyTsCi;
	}

	public void setEffectiveAcyTs(String effectiveAcyTs) {
		this.effectiveAcyTs = Functions.subString(effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
	}

	public String getEffectiveAcyTs() {
		return this.effectiveAcyTs;
	}

	public void setExpirationAcyTsCi(char expirationAcyTsCi) {
		this.expirationAcyTsCi = expirationAcyTsCi;
	}

	public char getExpirationAcyTsCi() {
		return this.expirationAcyTsCi;
	}

	public void setExpirationAcyTsNi(char expirationAcyTsNi) {
		this.expirationAcyTsNi = expirationAcyTsNi;
	}

	public char getExpirationAcyTsNi() {
		return this.expirationAcyTsNi;
	}

	public void setExpirationAcyTs(String expirationAcyTs) {
		this.expirationAcyTs = Functions.subString(expirationAcyTs, Len.EXPIRATION_ACY_TS);
	}

	public String getExpirationAcyTs() {
		return this.expirationAcyTs;
	}

	public void setTobCdDesc(String tobCdDesc) {
		this.tobCdDesc = Functions.subString(tobCdDesc, Len.TOB_CD_DESC);
	}

	public String getTobCdDesc() {
		return this.tobCdDesc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TOB_SIC_TYP_CD = 3;
		public static final int TOB_PTY_CD = 2;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int EXPIRATION_DT = 10;
		public static final int EFFECTIVE_ACY_TS = 26;
		public static final int EXPIRATION_ACY_TS = 26;
		public static final int TOB_CD_DESC = 40;
		public static final int TOB_SIC_TYP_CD_CI = 1;
		public static final int TOB_PTY_CD_CI = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int EXPIRATION_DT_CI = 1;
		public static final int EFFECTIVE_ACY_TS_CI = 1;
		public static final int EXPIRATION_ACY_TS_CI = 1;
		public static final int EXPIRATION_ACY_TS_NI = 1;
		public static final int FED_BUSINESS_TYP_DATA = TOB_SIC_TYP_CD_CI + TOB_SIC_TYP_CD + TOB_PTY_CD_CI + TOB_PTY_CD + USER_ID_CI + USER_ID
				+ STATUS_CD_CI + STATUS_CD + TERMINAL_ID_CI + TERMINAL_ID + EXPIRATION_DT_CI + EXPIRATION_DT + EFFECTIVE_ACY_TS_CI + EFFECTIVE_ACY_TS
				+ EXPIRATION_ACY_TS_CI + EXPIRATION_ACY_TS_NI + EXPIRATION_ACY_TS + TOB_CD_DESC;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
