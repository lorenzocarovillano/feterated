/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.INotDayRqr;
import com.federatedinsurance.crs.copy.DclnotDayRqr;
import com.federatedinsurance.crs.copy.DclpolDtaExt;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xzy800GetNotDayRqrRow;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0U8000<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0u8000Data implements INotDayRqr {

	//==== PROPERTIES ====
	//Original name: CF-ALL-POL-TYP
	private String cfAllPolTyp = "ZZ";
	//Original name: SW-GET-POLICY-DETAIL-FLAG
	private boolean swGetPolicyDetailFlag = false;
	//Original name: SW-MISSING-INPUT-FLAG
	private boolean swMissingInputFlag = false;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageArea workingStorageArea = new WorkingStorageArea();
	//Original name: XZY800-GET-NOT-DAY-RQR-ROW
	private Xzy800GetNotDayRqrRow xzy800GetNotDayRqrRow = new Xzy800GetNotDayRqrRow();
	//Original name: DCLNOT-DAY-RQR
	private DclnotDayRqr dclnotDayRqr = new DclnotDayRqr();
	//Original name: DCLPOL-DTA-EXT
	private DclpolDtaExt dclpolDtaExt = new DclpolDtaExt();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	@Override
	public void setCfAllPolTyp(String cfAllPolTyp) {
		this.cfAllPolTyp = Functions.subString(cfAllPolTyp, Len.CF_ALL_POL_TYP);
	}

	@Override
	public String getCfAllPolTyp() {
		return this.cfAllPolTyp;
	}

	public void setSwGetPolicyDetailFlag(boolean swGetPolicyDetailFlag) {
		this.swGetPolicyDetailFlag = swGetPolicyDetailFlag;
	}

	public boolean isSwGetPolicyDetailFlag() {
		return this.swGetPolicyDetailFlag;
	}

	public void setSwMissingInputFlag(boolean swMissingInputFlag) {
		this.swMissingInputFlag = swMissingInputFlag;
	}

	public boolean isSwMissingInputFlag() {
		return this.swMissingInputFlag;
	}

	public void setWsGetNotDayRqrRowFormatted(String data) {
		byte[] buffer = new byte[Len.WS_GET_NOT_DAY_RQR_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.WS_GET_NOT_DAY_RQR_ROW);
		setWsGetNotDayRqrRowBytes(buffer, 1);
	}

	public String getWsGetNotDayRqrRowFormatted() {
		return xzy800GetNotDayRqrRow.getXzy800GetNotDayRqrRowFormatted();
	}

	public void setWsGetNotDayRqrRowBytes(byte[] buffer, int offset) {
		int position = offset;
		xzy800GetNotDayRqrRow.setXzy800GetNotDayRqrRowBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	@Override
	public String getActNotTypCd() {
		return dclnotDayRqr.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.dclnotDayRqr.setActNotTypCd(actNotTypCd);
	}

	public DclnotDayRqr getDclnotDayRqr() {
		return dclnotDayRqr;
	}

	public DclpolDtaExt getDclpolDtaExt() {
		return dclpolDtaExt;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	@Override
	public short getNbrOfNotDay() {
		return dclnotDayRqr.getNbrOfNotDay();
	}

	@Override
	public void setNbrOfNotDay(short nbrOfNotDay) {
		this.dclnotDayRqr.setNbrOfNotDay(nbrOfNotDay);
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getPolTypCd() {
		return dclnotDayRqr.getPolTypCd();
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		this.dclnotDayRqr.setPolTypCd(polTypCd);
	}

	@Override
	public String getStAbb() {
		return dclnotDayRqr.getStAbb();
	}

	@Override
	public void setStAbb(String stAbb) {
		this.dclnotDayRqr.setStAbb(stAbb);
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageArea getWorkingStorageArea() {
		return workingStorageArea;
	}

	@Override
	public int getWsDaysDifference() {
		return workingStorageArea.getDaysDifference();
	}

	@Override
	public void setWsDaysDifference(int wsDaysDifference) {
		this.workingStorageArea.setDaysDifference(wsDaysDifference);
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public Xzy800GetNotDayRqrRow getXzy800GetNotDayRqrRow() {
		return xzy800GetNotDayRqrRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CF_ALL_POL_TYP = 3;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_GET_NOT_DAY_RQR_ROW = Xzy800GetNotDayRqrRow.Len.XZY800_GET_NOT_DAY_RQR_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
