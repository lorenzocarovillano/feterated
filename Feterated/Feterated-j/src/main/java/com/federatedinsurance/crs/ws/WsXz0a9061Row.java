/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9061-ROW<br>
 * Variable: WS-XZ0A9061-ROW from program XZ0B9060<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9061Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA961-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZA961-POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: XZA961-POL-TYP-DES
	private String polTypDes = DefaultValues.stringVal(Len.POL_TYP_DES);
	//Original name: XZA961-PRI-RSK-ST-ABB
	private String priRskStAbb = DefaultValues.stringVal(Len.PRI_RSK_ST_ABB);
	//Original name: XZA961-NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);
	//Original name: XZA961-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZA961-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZA961-POL-DUE-AMT
	private AfDecimal polDueAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9061_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9061RowBytes(buf);
	}

	public String getWsXz0a9061RowFormatted() {
		return getGetPolListDetailFormatted();
	}

	public void setWsXz0a9061RowBytes(byte[] buffer) {
		setWsXz0a9061RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9061RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9061_ROW];
		return getWsXz0a9061RowBytes(buffer, 1);
	}

	public void setWsXz0a9061RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetPolListDetailBytes(buffer, position);
	}

	public byte[] getWsXz0a9061RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetPolListDetailBytes(buffer, position);
		return buffer;
	}

	public String getGetPolListDetailFormatted() {
		return MarshalByteExt.bufferToStr(getGetPolListDetailBytes());
	}

	/**Original name: XZA961-GET-POL-LIST-DETAIL<br>
	 * <pre>*************************************************************
	 *  XZ0A9061 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_LAST_IMP_CN                        *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  26JAN2009 E404DLP    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetPolListDetailBytes() {
		byte[] buffer = new byte[Len.GET_POL_LIST_DETAIL];
		return getGetPolListDetailBytes(buffer, 1);
	}

	public void setGetPolListDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		polTypCd = MarshalByte.readString(buffer, position, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		polTypDes = MarshalByte.readString(buffer, position, Len.POL_TYP_DES);
		position += Len.POL_TYP_DES;
		priRskStAbb = MarshalByte.readString(buffer, position, Len.PRI_RSK_ST_ABB);
		position += Len.PRI_RSK_ST_ABB;
		notEffDt = MarshalByte.readString(buffer, position, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		polExpDt = MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		polDueAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.POL_DUE_AMT, Len.Fract.POL_DUE_AMT));
	}

	public byte[] getGetPolListDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polTypCd, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		MarshalByte.writeString(buffer, position, polTypDes, Len.POL_TYP_DES);
		position += Len.POL_TYP_DES;
		MarshalByte.writeString(buffer, position, priRskStAbb, Len.PRI_RSK_ST_ABB);
		position += Len.PRI_RSK_ST_ABB;
		MarshalByte.writeString(buffer, position, notEffDt, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeDecimal(buffer, position, polDueAmt.copy());
		return buffer;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	public String getPolTypCd() {
		return this.polTypCd;
	}

	public void setPolTypDes(String polTypDes) {
		this.polTypDes = Functions.subString(polTypDes, Len.POL_TYP_DES);
	}

	public String getPolTypDes() {
		return this.polTypDes;
	}

	public void setPriRskStAbb(String priRskStAbb) {
		this.priRskStAbb = Functions.subString(priRskStAbb, Len.PRI_RSK_ST_ABB);
	}

	public String getPriRskStAbb() {
		return this.priRskStAbb;
	}

	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	public String getNotEffDt() {
		return this.notEffDt;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.polDueAmt.assign(polDueAmt);
	}

	public AfDecimal getPolDueAmt() {
		return this.polDueAmt.copy();
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9061RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int POL_TYP_CD = 3;
		public static final int POL_TYP_DES = 30;
		public static final int PRI_RSK_ST_ABB = 2;
		public static final int NOT_EFF_DT = 10;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int POL_DUE_AMT = 10;
		public static final int GET_POL_LIST_DETAIL = POL_NBR + POL_TYP_CD + POL_TYP_DES + PRI_RSK_ST_ABB + NOT_EFF_DT + POL_EFF_DT + POL_EXP_DT
				+ POL_DUE_AMT;
		public static final int WS_XZ0A9061_ROW = GET_POL_LIST_DETAIL;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
