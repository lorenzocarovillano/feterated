/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: MDRV-UOW-LOGGABLE-PROBLEMS<br>
 * Variable: MDRV-UOW-LOGGABLE-PROBLEMS from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvUowLoggableProblems {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char LOGGABLE_ERRS = 'E';
	public static final char LOGGABLE_WARNS = 'W';
	public static final char OK = Types.SPACE_CHAR;

	//==== METHODS ====
	public void setUowLoggableProblems(char uowLoggableProblems) {
		this.value = uowLoggableProblems;
	}

	public void setUowLoggableProblemsFormatted(String uowLoggableProblems) {
		setUowLoggableProblems(Functions.charAt(uowLoggableProblems, Types.CHAR_SIZE));
	}

	public char getUowLoggableProblems() {
		return this.value;
	}

	public boolean isLoggableErrs() {
		return value == LOGGABLE_ERRS;
	}

	public void setLoggableErrs() {
		value = LOGGABLE_ERRS;
	}

	public boolean isLoggableWarns() {
		return value == LOGGABLE_WARNS;
	}

	public void setLoggableWarns() {
		value = LOGGABLE_WARNS;
	}

	public void setOk() {
		setUowLoggableProblemsFormatted(String.valueOf(OK));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_LOGGABLE_PROBLEMS = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
