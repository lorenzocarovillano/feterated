/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalBoLokTypeV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [HAL_BO_LOK_TYPE_V]
 * 
 */
public class HalBoLokTypeVDao extends BaseSqlDao<IHalBoLokTypeV> {

	public HalBoLokTypeVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalBoLokTypeV> getToClass() {
		return IHalBoLokTypeV.class;
	}

	public char selectRec(String uowNm, String busObjNm, char dft) {
		return buildQuery("selectRec").bind("uowNm", uowNm).bind("busObjNm", busObjNm).scalarResultChar(dft);
	}
}
