/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0012<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0012 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0012() {
	}

	public LServiceContractAreaXz0x0012(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt12iTkNotPrcTs(String xzt12iTkNotPrcTs) {
		writeString(Pos.XZT12I_TK_NOT_PRC_TS, xzt12iTkNotPrcTs, Len.XZT12I_TK_NOT_PRC_TS);
	}

	/**Original name: XZT12I-TK-NOT-PRC-TS<br>*/
	public String getXzt12iTkNotPrcTs() {
		return readString(Pos.XZT12I_TK_NOT_PRC_TS, Len.XZT12I_TK_NOT_PRC_TS);
	}

	public void setXzt12iTkRecSeqNbr(int xzt12iTkRecSeqNbr) {
		writeInt(Pos.XZT12I_TK_REC_SEQ_NBR, xzt12iTkRecSeqNbr, Len.Int.XZT12I_TK_REC_SEQ_NBR);
	}

	/**Original name: XZT12I-TK-REC-SEQ-NBR<br>*/
	public int getXzt12iTkRecSeqNbr() {
		return readNumDispInt(Pos.XZT12I_TK_REC_SEQ_NBR, Len.XZT12I_TK_REC_SEQ_NBR);
	}

	public String getXzt12iTkRecSeqNbrFormatted() {
		return readFixedString(Pos.XZT12I_TK_REC_SEQ_NBR, Len.XZT12I_TK_REC_SEQ_NBR);
	}

	public void setXzt12iTkRecCltId(String xzt12iTkRecCltId) {
		writeString(Pos.XZT12I_TK_REC_CLT_ID, xzt12iTkRecCltId, Len.XZT12I_TK_REC_CLT_ID);
	}

	/**Original name: XZT12I-TK-REC-CLT-ID<br>*/
	public String getXzt12iTkRecCltId() {
		return readString(Pos.XZT12I_TK_REC_CLT_ID, Len.XZT12I_TK_REC_CLT_ID);
	}

	public void setXzt12iTkRecAdrId(String xzt12iTkRecAdrId) {
		writeString(Pos.XZT12I_TK_REC_ADR_ID, xzt12iTkRecAdrId, Len.XZT12I_TK_REC_ADR_ID);
	}

	/**Original name: XZT12I-TK-REC-ADR-ID<br>*/
	public String getXzt12iTkRecAdrId() {
		return readString(Pos.XZT12I_TK_REC_ADR_ID, Len.XZT12I_TK_REC_ADR_ID);
	}

	public void setXzt12iTkMnlInd(char xzt12iTkMnlInd) {
		writeChar(Pos.XZT12I_TK_MNL_IND, xzt12iTkMnlInd);
	}

	/**Original name: XZT12I-TK-MNL-IND<br>*/
	public char getXzt12iTkMnlInd() {
		return readChar(Pos.XZT12I_TK_MNL_IND);
	}

	public void setXzt12iTkActNotRecCsumFormatted(String xzt12iTkActNotRecCsum) {
		writeString(Pos.XZT12I_TK_ACT_NOT_REC_CSUM, Trunc.toUnsignedNumeric(xzt12iTkActNotRecCsum, Len.XZT12I_TK_ACT_NOT_REC_CSUM),
				Len.XZT12I_TK_ACT_NOT_REC_CSUM);
	}

	/**Original name: XZT12I-TK-ACT-NOT-REC-CSUM<br>*/
	public int getXzt12iTkActNotRecCsum() {
		return readNumDispUnsignedInt(Pos.XZT12I_TK_ACT_NOT_REC_CSUM, Len.XZT12I_TK_ACT_NOT_REC_CSUM);
	}

	public String getXzt12iTkActNotRecCsumFormatted() {
		return readFixedString(Pos.XZT12I_TK_ACT_NOT_REC_CSUM, Len.XZT12I_TK_ACT_NOT_REC_CSUM);
	}

	public void setXzt12iCsrActNbr(String xzt12iCsrActNbr) {
		writeString(Pos.XZT12I_CSR_ACT_NBR, xzt12iCsrActNbr, Len.XZT12I_CSR_ACT_NBR);
	}

	/**Original name: XZT12I-CSR-ACT-NBR<br>*/
	public String getXzt12iCsrActNbr() {
		return readString(Pos.XZT12I_CSR_ACT_NBR, Len.XZT12I_CSR_ACT_NBR);
	}

	public void setXzt12iRecTypCd(String xzt12iRecTypCd) {
		writeString(Pos.XZT12I_REC_TYP_CD, xzt12iRecTypCd, Len.XZT12I_REC_TYP_CD);
	}

	/**Original name: XZT12I-REC-TYP-CD<br>*/
	public String getXzt12iRecTypCd() {
		return readString(Pos.XZT12I_REC_TYP_CD, Len.XZT12I_REC_TYP_CD);
	}

	public void setXzt12iRecName(String xzt12iRecName) {
		writeString(Pos.XZT12I_REC_NAME, xzt12iRecName, Len.XZT12I_REC_NAME);
	}

	/**Original name: XZT12I-REC-NAME<br>*/
	public String getXzt12iRecName() {
		return readString(Pos.XZT12I_REC_NAME, Len.XZT12I_REC_NAME);
	}

	public void setXzt12iLin1Adr(String xzt12iLin1Adr) {
		writeString(Pos.XZT12I_LIN1_ADR, xzt12iLin1Adr, Len.XZT12I_LIN1_ADR);
	}

	/**Original name: XZT12I-LIN-1-ADR<br>*/
	public String getXzt12iLin1Adr() {
		return readString(Pos.XZT12I_LIN1_ADR, Len.XZT12I_LIN1_ADR);
	}

	public void setXzt12iLin2Adr(String xzt12iLin2Adr) {
		writeString(Pos.XZT12I_LIN2_ADR, xzt12iLin2Adr, Len.XZT12I_LIN2_ADR);
	}

	/**Original name: XZT12I-LIN-2-ADR<br>*/
	public String getXzt12iLin2Adr() {
		return readString(Pos.XZT12I_LIN2_ADR, Len.XZT12I_LIN2_ADR);
	}

	public void setXzt12iCityName(String xzt12iCityName) {
		writeString(Pos.XZT12I_CITY_NAME, xzt12iCityName, Len.XZT12I_CITY_NAME);
	}

	/**Original name: XZT12I-CITY-NAME<br>*/
	public String getXzt12iCityName() {
		return readString(Pos.XZT12I_CITY_NAME, Len.XZT12I_CITY_NAME);
	}

	public void setXzt12iStateAbb(String xzt12iStateAbb) {
		writeString(Pos.XZT12I_STATE_ABB, xzt12iStateAbb, Len.XZT12I_STATE_ABB);
	}

	/**Original name: XZT12I-STATE-ABB<br>*/
	public String getXzt12iStateAbb() {
		return readString(Pos.XZT12I_STATE_ABB, Len.XZT12I_STATE_ABB);
	}

	public void setXzt12iPostalCd(String xzt12iPostalCd) {
		writeString(Pos.XZT12I_POSTAL_CD, xzt12iPostalCd, Len.XZT12I_POSTAL_CD);
	}

	/**Original name: XZT12I-POSTAL-CD<br>*/
	public String getXzt12iPostalCd() {
		return readString(Pos.XZT12I_POSTAL_CD, Len.XZT12I_POSTAL_CD);
	}

	public void setXzt12iCertNbr(String xzt12iCertNbr) {
		writeString(Pos.XZT12I_CERT_NBR, xzt12iCertNbr, Len.XZT12I_CERT_NBR);
	}

	/**Original name: XZT12I-CERT-NBR<br>*/
	public String getXzt12iCertNbr() {
		return readString(Pos.XZT12I_CERT_NBR, Len.XZT12I_CERT_NBR);
	}

	public void setXzt12iUserid(String xzt12iUserid) {
		writeString(Pos.XZT12I_USERID, xzt12iUserid, Len.XZT12I_USERID);
	}

	/**Original name: XZT12I-USERID<br>*/
	public String getXzt12iUserid() {
		return readString(Pos.XZT12I_USERID, Len.XZT12I_USERID);
	}

	public String getXzt12iUseridFormatted() {
		return Functions.padBlanks(getXzt12iUserid(), Len.XZT12I_USERID);
	}

	public void setXzt12oTkNotPrcTs(String xzt12oTkNotPrcTs) {
		writeString(Pos.XZT12O_TK_NOT_PRC_TS, xzt12oTkNotPrcTs, Len.XZT12O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT12O-TK-NOT-PRC-TS<br>*/
	public String getXzt12oTkNotPrcTs() {
		return readString(Pos.XZT12O_TK_NOT_PRC_TS, Len.XZT12O_TK_NOT_PRC_TS);
	}

	public void setXzt12oTkRecSeqNbr(int xzt12oTkRecSeqNbr) {
		writeInt(Pos.XZT12O_TK_REC_SEQ_NBR, xzt12oTkRecSeqNbr, Len.Int.XZT12O_TK_REC_SEQ_NBR);
	}

	public void setXzt12oTkRecSeqNbrFormatted(String xzt12oTkRecSeqNbr) {
		writeString(Pos.XZT12O_TK_REC_SEQ_NBR, Trunc.toUnsignedNumeric(xzt12oTkRecSeqNbr, Len.XZT12O_TK_REC_SEQ_NBR), Len.XZT12O_TK_REC_SEQ_NBR);
	}

	/**Original name: XZT12O-TK-REC-SEQ-NBR<br>*/
	public int getXzt12oTkRecSeqNbr() {
		return readNumDispInt(Pos.XZT12O_TK_REC_SEQ_NBR, Len.XZT12O_TK_REC_SEQ_NBR);
	}

	public void setXzt12oTkRecCltId(String xzt12oTkRecCltId) {
		writeString(Pos.XZT12O_TK_REC_CLT_ID, xzt12oTkRecCltId, Len.XZT12O_TK_REC_CLT_ID);
	}

	/**Original name: XZT12O-TK-REC-CLT-ID<br>*/
	public String getXzt12oTkRecCltId() {
		return readString(Pos.XZT12O_TK_REC_CLT_ID, Len.XZT12O_TK_REC_CLT_ID);
	}

	public void setXzt12oTkRecAdrId(String xzt12oTkRecAdrId) {
		writeString(Pos.XZT12O_TK_REC_ADR_ID, xzt12oTkRecAdrId, Len.XZT12O_TK_REC_ADR_ID);
	}

	/**Original name: XZT12O-TK-REC-ADR-ID<br>*/
	public String getXzt12oTkRecAdrId() {
		return readString(Pos.XZT12O_TK_REC_ADR_ID, Len.XZT12O_TK_REC_ADR_ID);
	}

	public void setXzt12oTkMnlInd(char xzt12oTkMnlInd) {
		writeChar(Pos.XZT12O_TK_MNL_IND, xzt12oTkMnlInd);
	}

	/**Original name: XZT12O-TK-MNL-IND<br>*/
	public char getXzt12oTkMnlInd() {
		return readChar(Pos.XZT12O_TK_MNL_IND);
	}

	public void setXzt12oTkActNotRecCsumFormatted(String xzt12oTkActNotRecCsum) {
		writeString(Pos.XZT12O_TK_ACT_NOT_REC_CSUM, Trunc.toUnsignedNumeric(xzt12oTkActNotRecCsum, Len.XZT12O_TK_ACT_NOT_REC_CSUM),
				Len.XZT12O_TK_ACT_NOT_REC_CSUM);
	}

	/**Original name: XZT12O-TK-ACT-NOT-REC-CSUM<br>*/
	public int getXzt12oTkActNotRecCsum() {
		return readNumDispUnsignedInt(Pos.XZT12O_TK_ACT_NOT_REC_CSUM, Len.XZT12O_TK_ACT_NOT_REC_CSUM);
	}

	public void setXzt12oCsrActNbr(String xzt12oCsrActNbr) {
		writeString(Pos.XZT12O_CSR_ACT_NBR, xzt12oCsrActNbr, Len.XZT12O_CSR_ACT_NBR);
	}

	/**Original name: XZT12O-CSR-ACT-NBR<br>*/
	public String getXzt12oCsrActNbr() {
		return readString(Pos.XZT12O_CSR_ACT_NBR, Len.XZT12O_CSR_ACT_NBR);
	}

	public void setXzt12oRecTypCd(String xzt12oRecTypCd) {
		writeString(Pos.XZT12O_REC_TYP_CD, xzt12oRecTypCd, Len.XZT12O_REC_TYP_CD);
	}

	/**Original name: XZT12O-REC-TYP-CD<br>*/
	public String getXzt12oRecTypCd() {
		return readString(Pos.XZT12O_REC_TYP_CD, Len.XZT12O_REC_TYP_CD);
	}

	public void setXzt12oRecTypShtDes(String xzt12oRecTypShtDes) {
		writeString(Pos.XZT12O_REC_TYP_SHT_DES, xzt12oRecTypShtDes, Len.XZT12O_REC_TYP_SHT_DES);
	}

	/**Original name: XZT12O-REC-TYP-SHT-DES<br>*/
	public String getXzt12oRecTypShtDes() {
		return readString(Pos.XZT12O_REC_TYP_SHT_DES, Len.XZT12O_REC_TYP_SHT_DES);
	}

	public void setXzt12oRecTypLngDes(String xzt12oRecTypLngDes) {
		writeString(Pos.XZT12O_REC_TYP_LNG_DES, xzt12oRecTypLngDes, Len.XZT12O_REC_TYP_LNG_DES);
	}

	/**Original name: XZT12O-REC-TYP-LNG-DES<br>*/
	public String getXzt12oRecTypLngDes() {
		return readString(Pos.XZT12O_REC_TYP_LNG_DES, Len.XZT12O_REC_TYP_LNG_DES);
	}

	public void setXzt12oRecTypSrOrdNbr(int xzt12oRecTypSrOrdNbr) {
		writeInt(Pos.XZT12O_REC_TYP_SR_ORD_NBR, xzt12oRecTypSrOrdNbr, Len.Int.XZT12O_REC_TYP_SR_ORD_NBR);
	}

	public void setXzt12oRecTypSrOrdNbrFormatted(String xzt12oRecTypSrOrdNbr) {
		writeString(Pos.XZT12O_REC_TYP_SR_ORD_NBR, Trunc.toUnsignedNumeric(xzt12oRecTypSrOrdNbr, Len.XZT12O_REC_TYP_SR_ORD_NBR),
				Len.XZT12O_REC_TYP_SR_ORD_NBR);
	}

	/**Original name: XZT12O-REC-TYP-SR-ORD-NBR<br>*/
	public int getXzt12oRecTypSrOrdNbr() {
		return readNumDispInt(Pos.XZT12O_REC_TYP_SR_ORD_NBR, Len.XZT12O_REC_TYP_SR_ORD_NBR);
	}

	public void setXzt12oRecName(String xzt12oRecName) {
		writeString(Pos.XZT12O_REC_NAME, xzt12oRecName, Len.XZT12O_REC_NAME);
	}

	/**Original name: XZT12O-REC-NAME<br>*/
	public String getXzt12oRecName() {
		return readString(Pos.XZT12O_REC_NAME, Len.XZT12O_REC_NAME);
	}

	public void setXzt12oLin1Adr(String xzt12oLin1Adr) {
		writeString(Pos.XZT12O_LIN1_ADR, xzt12oLin1Adr, Len.XZT12O_LIN1_ADR);
	}

	/**Original name: XZT12O-LIN-1-ADR<br>*/
	public String getXzt12oLin1Adr() {
		return readString(Pos.XZT12O_LIN1_ADR, Len.XZT12O_LIN1_ADR);
	}

	public void setXzt12oLin2Adr(String xzt12oLin2Adr) {
		writeString(Pos.XZT12O_LIN2_ADR, xzt12oLin2Adr, Len.XZT12O_LIN2_ADR);
	}

	/**Original name: XZT12O-LIN-2-ADR<br>*/
	public String getXzt12oLin2Adr() {
		return readString(Pos.XZT12O_LIN2_ADR, Len.XZT12O_LIN2_ADR);
	}

	public void setXzt12oCityName(String xzt12oCityName) {
		writeString(Pos.XZT12O_CITY_NAME, xzt12oCityName, Len.XZT12O_CITY_NAME);
	}

	/**Original name: XZT12O-CITY-NAME<br>*/
	public String getXzt12oCityName() {
		return readString(Pos.XZT12O_CITY_NAME, Len.XZT12O_CITY_NAME);
	}

	public void setXzt12oStateAbb(String xzt12oStateAbb) {
		writeString(Pos.XZT12O_STATE_ABB, xzt12oStateAbb, Len.XZT12O_STATE_ABB);
	}

	/**Original name: XZT12O-STATE-ABB<br>*/
	public String getXzt12oStateAbb() {
		return readString(Pos.XZT12O_STATE_ABB, Len.XZT12O_STATE_ABB);
	}

	public void setXzt12oPostalCd(String xzt12oPostalCd) {
		writeString(Pos.XZT12O_POSTAL_CD, xzt12oPostalCd, Len.XZT12O_POSTAL_CD);
	}

	/**Original name: XZT12O-POSTAL-CD<br>*/
	public String getXzt12oPostalCd() {
		return readString(Pos.XZT12O_POSTAL_CD, Len.XZT12O_POSTAL_CD);
	}

	public void setXzt12oCertNbr(String xzt12oCertNbr) {
		writeString(Pos.XZT12O_CERT_NBR, xzt12oCertNbr, Len.XZT12O_CERT_NBR);
	}

	/**Original name: XZT12O-CERT-NBR<br>*/
	public String getXzt12oCertNbr() {
		return readString(Pos.XZT12O_CERT_NBR, Len.XZT12O_CERT_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT012_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT12I_TECHNICAL_KEY = XZT012_SERVICE_INPUTS;
		public static final int XZT12I_TK_NOT_PRC_TS = XZT12I_TECHNICAL_KEY;
		public static final int XZT12I_TK_REC_SEQ_NBR = XZT12I_TK_NOT_PRC_TS + Len.XZT12I_TK_NOT_PRC_TS;
		public static final int XZT12I_TK_REC_CLT_ID = XZT12I_TK_REC_SEQ_NBR + Len.XZT12I_TK_REC_SEQ_NBR;
		public static final int XZT12I_TK_REC_ADR_ID = XZT12I_TK_REC_CLT_ID + Len.XZT12I_TK_REC_CLT_ID;
		public static final int XZT12I_TK_MNL_IND = XZT12I_TK_REC_ADR_ID + Len.XZT12I_TK_REC_ADR_ID;
		public static final int XZT12I_TK_ACT_NOT_REC_CSUM = XZT12I_TK_MNL_IND + Len.XZT12I_TK_MNL_IND;
		public static final int XZT12I_CSR_ACT_NBR = XZT12I_TK_ACT_NOT_REC_CSUM + Len.XZT12I_TK_ACT_NOT_REC_CSUM;
		public static final int XZT12I_REC_TYP_CD = XZT12I_CSR_ACT_NBR + Len.XZT12I_CSR_ACT_NBR;
		public static final int XZT12I_REC_NAME = XZT12I_REC_TYP_CD + Len.XZT12I_REC_TYP_CD;
		public static final int XZT12I_LIN1_ADR = XZT12I_REC_NAME + Len.XZT12I_REC_NAME;
		public static final int XZT12I_LIN2_ADR = XZT12I_LIN1_ADR + Len.XZT12I_LIN1_ADR;
		public static final int XZT12I_CITY_NAME = XZT12I_LIN2_ADR + Len.XZT12I_LIN2_ADR;
		public static final int XZT12I_STATE_ABB = XZT12I_CITY_NAME + Len.XZT12I_CITY_NAME;
		public static final int XZT12I_POSTAL_CD = XZT12I_STATE_ABB + Len.XZT12I_STATE_ABB;
		public static final int XZT12I_CERT_NBR = XZT12I_POSTAL_CD + Len.XZT12I_POSTAL_CD;
		public static final int XZT12I_USERID = XZT12I_CERT_NBR + Len.XZT12I_CERT_NBR;
		public static final int XZT012_SERVICE_OUTPUTS = XZT12I_USERID + Len.XZT12I_USERID;
		public static final int XZT12O_TECHNICAL_KEY = XZT012_SERVICE_OUTPUTS;
		public static final int XZT12O_TK_NOT_PRC_TS = XZT12O_TECHNICAL_KEY;
		public static final int XZT12O_TK_REC_SEQ_NBR = XZT12O_TK_NOT_PRC_TS + Len.XZT12O_TK_NOT_PRC_TS;
		public static final int XZT12O_TK_REC_CLT_ID = XZT12O_TK_REC_SEQ_NBR + Len.XZT12O_TK_REC_SEQ_NBR;
		public static final int XZT12O_TK_REC_ADR_ID = XZT12O_TK_REC_CLT_ID + Len.XZT12O_TK_REC_CLT_ID;
		public static final int XZT12O_TK_MNL_IND = XZT12O_TK_REC_ADR_ID + Len.XZT12O_TK_REC_ADR_ID;
		public static final int XZT12O_TK_ACT_NOT_REC_CSUM = XZT12O_TK_MNL_IND + Len.XZT12O_TK_MNL_IND;
		public static final int XZT12O_CSR_ACT_NBR = XZT12O_TK_ACT_NOT_REC_CSUM + Len.XZT12O_TK_ACT_NOT_REC_CSUM;
		public static final int XZT12O_REC_TYP = XZT12O_CSR_ACT_NBR + Len.XZT12O_CSR_ACT_NBR;
		public static final int XZT12O_REC_TYP_CD = XZT12O_REC_TYP;
		public static final int XZT12O_REC_TYP_SHT_DES = XZT12O_REC_TYP_CD + Len.XZT12O_REC_TYP_CD;
		public static final int XZT12O_REC_TYP_LNG_DES = XZT12O_REC_TYP_SHT_DES + Len.XZT12O_REC_TYP_SHT_DES;
		public static final int XZT12O_REC_TYP_SR_ORD_NBR = XZT12O_REC_TYP_LNG_DES + Len.XZT12O_REC_TYP_LNG_DES;
		public static final int XZT12O_REC_NAME = XZT12O_REC_TYP_SR_ORD_NBR + Len.XZT12O_REC_TYP_SR_ORD_NBR;
		public static final int XZT12O_LIN1_ADR = XZT12O_REC_NAME + Len.XZT12O_REC_NAME;
		public static final int XZT12O_LIN2_ADR = XZT12O_LIN1_ADR + Len.XZT12O_LIN1_ADR;
		public static final int XZT12O_CITY_NAME = XZT12O_LIN2_ADR + Len.XZT12O_LIN2_ADR;
		public static final int XZT12O_STATE_ABB = XZT12O_CITY_NAME + Len.XZT12O_CITY_NAME;
		public static final int XZT12O_POSTAL_CD = XZT12O_STATE_ABB + Len.XZT12O_STATE_ABB;
		public static final int XZT12O_CERT_NBR = XZT12O_POSTAL_CD + Len.XZT12O_POSTAL_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT12I_TK_NOT_PRC_TS = 26;
		public static final int XZT12I_TK_REC_SEQ_NBR = 5;
		public static final int XZT12I_TK_REC_CLT_ID = 64;
		public static final int XZT12I_TK_REC_ADR_ID = 64;
		public static final int XZT12I_TK_MNL_IND = 1;
		public static final int XZT12I_TK_ACT_NOT_REC_CSUM = 9;
		public static final int XZT12I_CSR_ACT_NBR = 9;
		public static final int XZT12I_REC_TYP_CD = 5;
		public static final int XZT12I_REC_NAME = 120;
		public static final int XZT12I_LIN1_ADR = 45;
		public static final int XZT12I_LIN2_ADR = 45;
		public static final int XZT12I_CITY_NAME = 30;
		public static final int XZT12I_STATE_ABB = 2;
		public static final int XZT12I_POSTAL_CD = 13;
		public static final int XZT12I_CERT_NBR = 25;
		public static final int XZT12I_USERID = 8;
		public static final int XZT12O_TK_NOT_PRC_TS = 26;
		public static final int XZT12O_TK_REC_SEQ_NBR = 5;
		public static final int XZT12O_TK_REC_CLT_ID = 64;
		public static final int XZT12O_TK_REC_ADR_ID = 64;
		public static final int XZT12O_TK_MNL_IND = 1;
		public static final int XZT12O_TK_ACT_NOT_REC_CSUM = 9;
		public static final int XZT12O_CSR_ACT_NBR = 9;
		public static final int XZT12O_REC_TYP_CD = 5;
		public static final int XZT12O_REC_TYP_SHT_DES = 13;
		public static final int XZT12O_REC_TYP_LNG_DES = 30;
		public static final int XZT12O_REC_TYP_SR_ORD_NBR = 5;
		public static final int XZT12O_REC_NAME = 120;
		public static final int XZT12O_LIN1_ADR = 45;
		public static final int XZT12O_LIN2_ADR = 45;
		public static final int XZT12O_CITY_NAME = 30;
		public static final int XZT12O_STATE_ABB = 2;
		public static final int XZT12O_POSTAL_CD = 13;
		public static final int XZT12I_TECHNICAL_KEY = XZT12I_TK_NOT_PRC_TS + XZT12I_TK_REC_SEQ_NBR + XZT12I_TK_REC_CLT_ID + XZT12I_TK_REC_ADR_ID
				+ XZT12I_TK_MNL_IND + XZT12I_TK_ACT_NOT_REC_CSUM;
		public static final int XZT012_SERVICE_INPUTS = XZT12I_TECHNICAL_KEY + XZT12I_CSR_ACT_NBR + XZT12I_REC_TYP_CD + XZT12I_REC_NAME
				+ XZT12I_LIN1_ADR + XZT12I_LIN2_ADR + XZT12I_CITY_NAME + XZT12I_STATE_ABB + XZT12I_POSTAL_CD + XZT12I_CERT_NBR + XZT12I_USERID;
		public static final int XZT12O_TECHNICAL_KEY = XZT12O_TK_NOT_PRC_TS + XZT12O_TK_REC_SEQ_NBR + XZT12O_TK_REC_CLT_ID + XZT12O_TK_REC_ADR_ID
				+ XZT12O_TK_MNL_IND + XZT12O_TK_ACT_NOT_REC_CSUM;
		public static final int XZT12O_REC_TYP = XZT12O_REC_TYP_CD + XZT12O_REC_TYP_SHT_DES + XZT12O_REC_TYP_LNG_DES + XZT12O_REC_TYP_SR_ORD_NBR;
		public static final int XZT12O_CERT_NBR = 25;
		public static final int XZT012_SERVICE_OUTPUTS = XZT12O_TECHNICAL_KEY + XZT12O_CSR_ACT_NBR + XZT12O_REC_TYP + XZT12O_REC_NAME
				+ XZT12O_LIN1_ADR + XZT12O_LIN2_ADR + XZT12O_CITY_NAME + XZT12O_STATE_ABB + XZT12O_POSTAL_CD + XZT12O_CERT_NBR;
		public static final int L_SERVICE_CONTRACT_AREA = XZT012_SERVICE_INPUTS + XZT012_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT12I_TK_REC_SEQ_NBR = 5;
			public static final int XZT12O_TK_REC_SEQ_NBR = 5;
			public static final int XZT12O_REC_TYP_SR_ORD_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
