/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-CICS-PIPE-INTERFACE-CONTRACT<br>
 * Variable: L-CICS-PIPE-INTERFACE-CONTRACT from copybook TS54701<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LCicsPipeInterfaceContract extends BytesClass {

	//==== PROPERTIES ====
	public static final char FN_OPEN_PIPE = '1';
	public static final char FN_CALL_CICS_PGM_NO_SYNC = '2';
	public static final char FN_CALL_CICS_PGM_SYNC = '3';
	public static final char FN_CLOSE_PIPE = '4';
	public static final char FN_GET_TOKEN_INF = '5';
	public static final String CICS_APPL_ID_P2 = "CICS2";
	public static final String CICS_APPL_ID_P3 = "CICS3";
	public static final String CICS_APPL_ID_P4 = "CICS4";
	public static final String CICS_APPL_ID_P5 = "CICS5";
	public static final String CICS_APPL_ID_P6 = "CICS6";
	public static final String CICS_APPL_ID_P7 = "CICS7";
	public static final String CICS_APPL_ID_P8 = "CICS8";
	public static final String CICS_APPL_ID_P9 = "CICS9";
	public static final String CICS_APPL_ID_PM = "CICSM";
	public static final String CICS_APPL_ID_D1 = "D1CICS";
	public static final String CICS_APPL_ID_DE = "DECICS";
	public static final String CICS_APPL_ID_B1 = "B1CICS";
	public static final String CICS_APPL_ID_BA = "BACICS";
	public static final String CICS_APPL_ID_EDUC1 = "E1CICS";
	public static final String CICS_APPL_ID_EDUC3 = "E3CICS";
	public static final char EI_ES_NO_ERROR = Types.SPACE_CHAR;
	public static final char EI_ES_WARNING = '1';
	public static final char EI_ES_LOGIC_ERROR = '2';
	public static final char EI_ES_SYSTEM_ERROR = '3';
	public static final char EI_ET_REGION_SWAPPED = '1';
	public static final char EI_ET_SYSTEM_WARNING = '2';
	public static final char EI_ET_DPL_RESP_CAPTURED = '3';

	//==== CONSTRUCTORS ====
	public LCicsPipeInterfaceContract() {
	}

	public LCicsPipeInterfaceContract(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_CICS_PIPE_INTERFACE_CONTRACT;
	}

	/**Original name: L-CI-FUNCTION<br>*/
	public char getFunction() {
		return readChar(Pos.FUNCTION);
	}

	public boolean isFnOpenPipe() {
		return getFunction() == FN_OPEN_PIPE;
	}

	public boolean isFnCallCicsPgmSync() {
		return getFunction() == FN_CALL_CICS_PGM_SYNC;
	}

	/**Original name: L-CI-CICS-APPL-ID<br>
	 * <pre> BELOW IS A LIST OF COMMONLY USED CICS REGIONS.
	 *   NOTE:  THERE IS NOT ANY LIMIT IN CONNECTING TO THESE REGIONS
	 *            ONLY.</pre>*/
	public String getCicsApplId() {
		return readString(Pos.CICS_APPL_ID, Len.CICS_APPL_ID);
	}

	public void setEiErrorSeverity(char eiErrorSeverity) {
		writeChar(Pos.EI_ERROR_SEVERITY, eiErrorSeverity);
	}

	/**Original name: L-CI-EI-ERROR-SEVERITY<br>*/
	public char getEiErrorSeverity() {
		return readChar(Pos.EI_ERROR_SEVERITY);
	}

	public void setEiEsWarning() {
		setEiErrorSeverity(EI_ES_WARNING);
	}

	public void setEiEsLogicError() {
		setEiErrorSeverity(EI_ES_LOGIC_ERROR);
	}

	public void setEiEsSystemError() {
		setEiErrorSeverity(EI_ES_SYSTEM_ERROR);
	}

	public void setEiErrorType(char eiErrorType) {
		writeChar(Pos.EI_ERROR_TYPE, eiErrorType);
	}

	/**Original name: L-CI-EI-ERROR-TYPE<br>*/
	public char getEiErrorType() {
		return readChar(Pos.EI_ERROR_TYPE);
	}

	public void setEiEtRegionSwapped() {
		setEiErrorType(EI_ET_REGION_SWAPPED);
	}

	public void setEiEtSystemWarning() {
		setEiErrorType(EI_ET_SYSTEM_WARNING);
	}

	public void setEiEtDplRespCaptured() {
		setEiErrorType(EI_ET_DPL_RESP_CAPTURED);
	}

	public void setEiMessage(String eiMessage) {
		writeString(Pos.EI_MESSAGE, eiMessage, Len.EI_MESSAGE);
	}

	/**Original name: L-CI-EI-MESSAGE<br>*/
	public String getEiMessage() {
		return readString(Pos.EI_MESSAGE, Len.EI_MESSAGE);
	}

	public void setEiExciResp(int eiExciResp) {
		writeBinaryInt(Pos.EI_EXCI_RESP, eiExciResp);
	}

	/**Original name: L-CI-EI-EXCI-RESP<br>*/
	public int getEiExciResp() {
		return readBinaryInt(Pos.EI_EXCI_RESP);
	}

	public void setEiExciResp2(int eiExciResp2) {
		writeBinaryInt(Pos.EI_EXCI_RESP2, eiExciResp2);
	}

	/**Original name: L-CI-EI-EXCI-RESP2<br>*/
	public int getEiExciResp2() {
		return readBinaryInt(Pos.EI_EXCI_RESP2);
	}

	public void setEiExciResp3(int eiExciResp3) {
		writeBinaryInt(Pos.EI_EXCI_RESP3, eiExciResp3);
	}

	/**Original name: L-CI-EI-EXCI-RESP3<br>*/
	public int getEiExciResp3() {
		return readBinaryInt(Pos.EI_EXCI_RESP3);
	}

	public void setEiDplResp(int eiDplResp) {
		writeBinaryInt(Pos.EI_DPL_RESP, eiDplResp);
	}

	/**Original name: L-CI-EI-DPL-RESP<br>*/
	public int getEiDplResp() {
		return readBinaryInt(Pos.EI_DPL_RESP);
	}

	public void setEiDplResp2(int eiDplResp2) {
		writeBinaryInt(Pos.EI_DPL_RESP2, eiDplResp2);
	}

	/**Original name: L-CI-EI-DPL-RESP2<br>*/
	public int getEiDplResp2() {
		return readBinaryInt(Pos.EI_DPL_RESP2);
	}

	/**Original name: L-CI-CP-CICS-PGM-NM<br>*/
	public String getCpCicsPgmNm() {
		return readString(Pos.CP_CICS_PGM_NM, Len.CP_CICS_PGM_NM);
	}

	public String getCpCicsPgmNmFormatted() {
		return Functions.padBlanks(getCpCicsPgmNm(), Len.CP_CICS_PGM_NM);
	}

	/**Original name: L-CI-CP-TARGET-TRAN-ID<br>*/
	public String getCpTargetTranId() {
		return readString(Pos.CP_TARGET_TRAN_ID, Len.CP_TARGET_TRAN_ID);
	}

	public String getCpTargetTranIdFormatted() {
		return Functions.padBlanks(getCpTargetTranId(), Len.CP_TARGET_TRAN_ID);
	}

	/**Original name: L-CI-CP-DATA-LEN<br>*/
	public int getCpDataLen() {
		return readBinaryInt(Pos.CP_DATA_LEN);
	}

	public void setGtUserToken(int gtUserToken) {
		writeBinaryInt(Pos.GT_USER_TOKEN, gtUserToken);
	}

	/**Original name: L-CI-GT-USER-TOKEN<br>*/
	public int getGtUserToken() {
		return readBinaryInt(Pos.GT_USER_TOKEN);
	}

	public void setGtPipeToken(int gtPipeToken) {
		writeBinaryInt(Pos.GT_PIPE_TOKEN, gtPipeToken);
	}

	/**Original name: L-CI-GT-PIPE-TOKEN<br>*/
	public int getGtPipeToken() {
		return readBinaryInt(Pos.GT_PIPE_TOKEN);
	}

	public void setGtApplId(String gtApplId) {
		writeString(Pos.GT_APPL_ID, gtApplId, Len.GT_APPL_ID);
	}

	/**Original name: L-CI-GT-APPL-ID<br>*/
	public String getGtApplId() {
		return readString(Pos.GT_APPL_ID, Len.GT_APPL_ID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_CICS_PIPE_INTERFACE_CONTRACT = 1;
		public static final int FUNCTION = L_CICS_PIPE_INTERFACE_CONTRACT;
		public static final int CICS_APPL_ID = FUNCTION + Len.FUNCTION;
		public static final int ERROR_INFORMATION = CICS_APPL_ID + Len.CICS_APPL_ID;
		public static final int EI_ERROR_CODE = ERROR_INFORMATION;
		public static final int EI_ERROR_SEVERITY = EI_ERROR_CODE;
		public static final int EI_ERROR_TYPE = EI_ERROR_SEVERITY + Len.EI_ERROR_SEVERITY;
		public static final int EI_MESSAGE = EI_ERROR_TYPE + Len.EI_ERROR_TYPE;
		public static final int EI_CODES = EI_MESSAGE + Len.EI_MESSAGE;
		public static final int EI_EXCI_RESP = EI_CODES;
		public static final int EI_EXCI_RESP2 = EI_EXCI_RESP + Len.EI_EXCI_RESP;
		public static final int EI_EXCI_RESP3 = EI_EXCI_RESP2 + Len.EI_EXCI_RESP2;
		public static final int EI_DPL_RESP = EI_EXCI_RESP3 + Len.EI_EXCI_RESP3;
		public static final int EI_DPL_RESP2 = EI_DPL_RESP + Len.EI_DPL_RESP;
		public static final int TRANSACTION_SPECIFIC_DATA = EI_DPL_RESP2 + Len.EI_DPL_RESP2;
		public static final int CALL_PGM_PARMS = TRANSACTION_SPECIFIC_DATA;
		public static final int CP_CICS_PGM_NM = CALL_PGM_PARMS;
		public static final int CP_TARGET_TRAN_ID = CP_CICS_PGM_NM + Len.CP_CICS_PGM_NM;
		public static final int CP_DATA_LEN = CP_TARGET_TRAN_ID + Len.CP_TARGET_TRAN_ID;
		public static final int GET_TOKEN_INF_PARMS = TRANSACTION_SPECIFIC_DATA;
		public static final int GT_USER_TOKEN = GET_TOKEN_INF_PARMS;
		public static final int GT_PIPE_TOKEN = GT_USER_TOKEN + Len.GT_USER_TOKEN;
		public static final int GT_APPL_ID = GT_PIPE_TOKEN + Len.GT_PIPE_TOKEN;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int FUNCTION = 1;
		public static final int CICS_APPL_ID = 8;
		public static final int EI_ERROR_SEVERITY = 1;
		public static final int EI_ERROR_TYPE = 1;
		public static final int EI_MESSAGE = 131;
		public static final int EI_EXCI_RESP = 4;
		public static final int EI_EXCI_RESP2 = 4;
		public static final int EI_EXCI_RESP3 = 4;
		public static final int EI_DPL_RESP = 4;
		public static final int EI_DPL_RESP2 = 4;
		public static final int CP_CICS_PGM_NM = 8;
		public static final int CP_TARGET_TRAN_ID = 4;
		public static final int GT_USER_TOKEN = 4;
		public static final int GT_PIPE_TOKEN = 4;
		public static final int EI_ERROR_CODE = EI_ERROR_SEVERITY + EI_ERROR_TYPE;
		public static final int EI_CODES = EI_EXCI_RESP + EI_EXCI_RESP2 + EI_EXCI_RESP3 + EI_DPL_RESP + EI_DPL_RESP2;
		public static final int ERROR_INFORMATION = EI_ERROR_CODE + EI_MESSAGE + EI_CODES;
		public static final int TRANSACTION_SPECIFIC_DATA = 16;
		public static final int L_CICS_PIPE_INTERFACE_CONTRACT = FUNCTION + CICS_APPL_ID + ERROR_INFORMATION + TRANSACTION_SPECIFIC_DATA;
		public static final int GT_APPL_ID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
