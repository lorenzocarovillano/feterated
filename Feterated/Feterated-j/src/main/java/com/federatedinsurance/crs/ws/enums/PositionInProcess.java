/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: POSITION-IN-PROCESS<br>
 * Variable: POSITION-IN-PROCESS from copybook XPXLDAT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class PositionInProcess {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FUNCTION_EDIT = '0';
	public static final char INPUT_PROCESS = '1';
	public static final char OUT_NO_ADJ = '2';
	public static final char OUT_PROCESS = '3';
	public static final char DIFF_FOR_OUT = '4';

	//==== METHODS ====
	public void setPositionInProcess(char positionInProcess) {
		this.value = positionInProcess;
	}

	public char getPositionInProcess() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POSITION_IN_PROCESS = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
