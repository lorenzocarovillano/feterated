/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90G1-ROW<br>
 * Variable: WS-XZ0A90G1-ROW from program XZ0B90G0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90g1Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9G1-CERT-NBR
	private String certNbr = DefaultValues.stringVal(Len.CERT_NBR);
	//Original name: XZA9G1-NM-ADR-LIN-1
	private String nmAdrLin1 = DefaultValues.stringVal(Len.NM_ADR_LIN1);
	//Original name: XZA9G1-NM-ADR-LIN-2
	private String nmAdrLin2 = DefaultValues.stringVal(Len.NM_ADR_LIN2);
	//Original name: XZA9G1-NM-ADR-LIN-3
	private String nmAdrLin3 = DefaultValues.stringVal(Len.NM_ADR_LIN3);
	//Original name: XZA9G1-NM-ADR-LIN-4
	private String nmAdrLin4 = DefaultValues.stringVal(Len.NM_ADR_LIN4);
	//Original name: XZA9G1-NM-ADR-LIN-5
	private String nmAdrLin5 = DefaultValues.stringVal(Len.NM_ADR_LIN5);
	//Original name: XZA9G1-NM-ADR-LIN-6
	private String nmAdrLin6 = DefaultValues.stringVal(Len.NM_ADR_LIN6);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90G1_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90g1RowBytes(buf);
	}

	public String getWsXz0a90g1RowFormatted() {
		return getGetCertRecDetailFormatted();
	}

	public void setWsXz0a90g1RowBytes(byte[] buffer) {
		setWsXz0a90g1RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90g1RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90G1_ROW];
		return getWsXz0a90g1RowBytes(buffer, 1);
	}

	public void setWsXz0a90g1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetCertRecDetailBytes(buffer, position);
	}

	public byte[] getWsXz0a90g1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetCertRecDetailBytes(buffer, position);
		return buffer;
	}

	public String getGetCertRecDetailFormatted() {
		return MarshalByteExt.bufferToStr(getGetCertRecDetailBytes());
	}

	/**Original name: XZA9G1-GET-CERT-REC-DETAIL<br>
	 * <pre>*************************************************************
	 *  XZ0A90G1 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_CERT_RECIPIENT_LIST                *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  27FEB2009 E404KXS    NEW                          *
	 *  PP02500  17OCT2012 E404BPO    CHANGE CERT NUMBER LENGTH    *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetCertRecDetailBytes() {
		byte[] buffer = new byte[Len.GET_CERT_REC_DETAIL];
		return getGetCertRecDetailBytes(buffer, 1);
	}

	public void setGetCertRecDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		certNbr = MarshalByte.readString(buffer, position, Len.CERT_NBR);
		position += Len.CERT_NBR;
		nmAdrLin1 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN1);
		position += Len.NM_ADR_LIN1;
		nmAdrLin2 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN2);
		position += Len.NM_ADR_LIN2;
		nmAdrLin3 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN3);
		position += Len.NM_ADR_LIN3;
		nmAdrLin4 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN4);
		position += Len.NM_ADR_LIN4;
		nmAdrLin5 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN5);
		position += Len.NM_ADR_LIN5;
		nmAdrLin6 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN6);
	}

	public byte[] getGetCertRecDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, certNbr, Len.CERT_NBR);
		position += Len.CERT_NBR;
		MarshalByte.writeString(buffer, position, nmAdrLin1, Len.NM_ADR_LIN1);
		position += Len.NM_ADR_LIN1;
		MarshalByte.writeString(buffer, position, nmAdrLin2, Len.NM_ADR_LIN2);
		position += Len.NM_ADR_LIN2;
		MarshalByte.writeString(buffer, position, nmAdrLin3, Len.NM_ADR_LIN3);
		position += Len.NM_ADR_LIN3;
		MarshalByte.writeString(buffer, position, nmAdrLin4, Len.NM_ADR_LIN4);
		position += Len.NM_ADR_LIN4;
		MarshalByte.writeString(buffer, position, nmAdrLin5, Len.NM_ADR_LIN5);
		position += Len.NM_ADR_LIN5;
		MarshalByte.writeString(buffer, position, nmAdrLin6, Len.NM_ADR_LIN6);
		return buffer;
	}

	public void setCertNbr(String certNbr) {
		this.certNbr = Functions.subString(certNbr, Len.CERT_NBR);
	}

	public String getCertNbr() {
		return this.certNbr;
	}

	public void setNmAdrLin1(String nmAdrLin1) {
		this.nmAdrLin1 = Functions.subString(nmAdrLin1, Len.NM_ADR_LIN1);
	}

	public String getNmAdrLin1() {
		return this.nmAdrLin1;
	}

	public void setNmAdrLin2(String nmAdrLin2) {
		this.nmAdrLin2 = Functions.subString(nmAdrLin2, Len.NM_ADR_LIN2);
	}

	public String getNmAdrLin2() {
		return this.nmAdrLin2;
	}

	public void setNmAdrLin3(String nmAdrLin3) {
		this.nmAdrLin3 = Functions.subString(nmAdrLin3, Len.NM_ADR_LIN3);
	}

	public String getNmAdrLin3() {
		return this.nmAdrLin3;
	}

	public void setNmAdrLin4(String nmAdrLin4) {
		this.nmAdrLin4 = Functions.subString(nmAdrLin4, Len.NM_ADR_LIN4);
	}

	public String getNmAdrLin4() {
		return this.nmAdrLin4;
	}

	public void setNmAdrLin5(String nmAdrLin5) {
		this.nmAdrLin5 = Functions.subString(nmAdrLin5, Len.NM_ADR_LIN5);
	}

	public String getNmAdrLin5() {
		return this.nmAdrLin5;
	}

	public void setNmAdrLin6(String nmAdrLin6) {
		this.nmAdrLin6 = Functions.subString(nmAdrLin6, Len.NM_ADR_LIN6);
	}

	public String getNmAdrLin6() {
		return this.nmAdrLin6;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90g1RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CERT_NBR = 25;
		public static final int NM_ADR_LIN1 = 30;
		public static final int NM_ADR_LIN2 = 30;
		public static final int NM_ADR_LIN3 = 30;
		public static final int NM_ADR_LIN4 = 30;
		public static final int NM_ADR_LIN5 = 30;
		public static final int NM_ADR_LIN6 = 30;
		public static final int GET_CERT_REC_DETAIL = CERT_NBR + NM_ADR_LIN1 + NM_ADR_LIN2 + NM_ADR_LIN3 + NM_ADR_LIN4 + NM_ADR_LIN5 + NM_ADR_LIN6;
		public static final int WS_XZ0A90G1_ROW = GET_CERT_REC_DETAIL;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
