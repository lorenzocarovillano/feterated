/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW01F-BUSINESS-CLIENT-DATA<br>
 * Variable: CW01F-BUSINESS-CLIENT-DATA from copybook CAWLF001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw01fBusinessClientData {

	//==== PROPERTIES ====
	/**Original name: CW01F-CIBC-EXP-DT-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char cibcExpDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-EXP-DT
	private String cibcExpDt = DefaultValues.stringVal(Len.CIBC_EXP_DT);
	//Original name: CW01F-GRS-REV-CD-CI
	private char grsRevCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-GRS-REV-CD
	private String grsRevCd = DefaultValues.stringVal(Len.GRS_REV_CD);
	//Original name: CW01F-IDY-TYP-CD-CI
	private char idyTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-IDY-TYP-CD
	private String idyTypCd = DefaultValues.stringVal(Len.IDY_TYP_CD);
	//Original name: CW01F-CIBC-NBR-EMP-CI
	private char cibcNbrEmpCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-NBR-EMP-NI
	private char cibcNbrEmpNi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-NBR-EMP-SIGN
	private char cibcNbrEmpSign = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-NBR-EMP
	private String cibcNbrEmp = DefaultValues.stringVal(Len.CIBC_NBR_EMP);
	//Original name: CW01F-CIBC-STR-DT-CI
	private char cibcStrDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-STR-DT-NI
	private char cibcStrDtNi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-STR-DT
	private String cibcStrDt = DefaultValues.stringVal(Len.CIBC_STR_DT);
	//Original name: CW01F-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW01F-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW01F-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW01F-CIBC-EFF-ACY-TS-CI
	private char cibcEffAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-EFF-ACY-TS
	private String cibcEffAcyTs = DefaultValues.stringVal(Len.CIBC_EFF_ACY_TS);
	//Original name: CW01F-CIBC-EXP-ACY-TS-CI
	private char cibcExpAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-EXP-ACY-TS
	private String cibcExpAcyTs = DefaultValues.stringVal(Len.CIBC_EXP_ACY_TS);

	//==== METHODS ====
	public void setBusinessClientDataBytes(byte[] buffer, int offset) {
		int position = offset;
		cibcExpDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcExpDt = MarshalByte.readString(buffer, position, Len.CIBC_EXP_DT);
		position += Len.CIBC_EXP_DT;
		grsRevCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		grsRevCd = MarshalByte.readString(buffer, position, Len.GRS_REV_CD);
		position += Len.GRS_REV_CD;
		idyTypCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		idyTypCd = MarshalByte.readString(buffer, position, Len.IDY_TYP_CD);
		position += Len.IDY_TYP_CD;
		cibcNbrEmpCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcNbrEmpNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcNbrEmpSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcNbrEmp = MarshalByte.readFixedString(buffer, position, Len.CIBC_NBR_EMP);
		position += Len.CIBC_NBR_EMP;
		cibcStrDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcStrDtNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcStrDt = MarshalByte.readString(buffer, position, Len.CIBC_STR_DT);
		position += Len.CIBC_STR_DT;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		cibcEffAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcEffAcyTs = MarshalByte.readString(buffer, position, Len.CIBC_EFF_ACY_TS);
		position += Len.CIBC_EFF_ACY_TS;
		cibcExpAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcExpAcyTs = MarshalByte.readString(buffer, position, Len.CIBC_EXP_ACY_TS);
	}

	public byte[] getBusinessClientDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, cibcExpDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cibcExpDt, Len.CIBC_EXP_DT);
		position += Len.CIBC_EXP_DT;
		MarshalByte.writeChar(buffer, position, grsRevCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, grsRevCd, Len.GRS_REV_CD);
		position += Len.GRS_REV_CD;
		MarshalByte.writeChar(buffer, position, idyTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, idyTypCd, Len.IDY_TYP_CD);
		position += Len.IDY_TYP_CD;
		MarshalByte.writeChar(buffer, position, cibcNbrEmpCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cibcNbrEmpNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cibcNbrEmpSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cibcNbrEmp, Len.CIBC_NBR_EMP);
		position += Len.CIBC_NBR_EMP;
		MarshalByte.writeChar(buffer, position, cibcStrDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cibcStrDtNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cibcStrDt, Len.CIBC_STR_DT);
		position += Len.CIBC_STR_DT;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, cibcEffAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cibcEffAcyTs, Len.CIBC_EFF_ACY_TS);
		position += Len.CIBC_EFF_ACY_TS;
		MarshalByte.writeChar(buffer, position, cibcExpAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cibcExpAcyTs, Len.CIBC_EXP_ACY_TS);
		return buffer;
	}

	public void setCibcExpDtCi(char cibcExpDtCi) {
		this.cibcExpDtCi = cibcExpDtCi;
	}

	public char getCibcExpDtCi() {
		return this.cibcExpDtCi;
	}

	public void setCibcExpDt(String cibcExpDt) {
		this.cibcExpDt = Functions.subString(cibcExpDt, Len.CIBC_EXP_DT);
	}

	public String getCibcExpDt() {
		return this.cibcExpDt;
	}

	public void setGrsRevCdCi(char grsRevCdCi) {
		this.grsRevCdCi = grsRevCdCi;
	}

	public char getGrsRevCdCi() {
		return this.grsRevCdCi;
	}

	public void setGrsRevCd(String grsRevCd) {
		this.grsRevCd = Functions.subString(grsRevCd, Len.GRS_REV_CD);
	}

	public String getGrsRevCd() {
		return this.grsRevCd;
	}

	public void setIdyTypCdCi(char idyTypCdCi) {
		this.idyTypCdCi = idyTypCdCi;
	}

	public char getIdyTypCdCi() {
		return this.idyTypCdCi;
	}

	public void setIdyTypCd(String idyTypCd) {
		this.idyTypCd = Functions.subString(idyTypCd, Len.IDY_TYP_CD);
	}

	public String getIdyTypCd() {
		return this.idyTypCd;
	}

	public void setCibcNbrEmpCi(char cibcNbrEmpCi) {
		this.cibcNbrEmpCi = cibcNbrEmpCi;
	}

	public char getCibcNbrEmpCi() {
		return this.cibcNbrEmpCi;
	}

	public void setCibcNbrEmpNi(char cibcNbrEmpNi) {
		this.cibcNbrEmpNi = cibcNbrEmpNi;
	}

	public char getCibcNbrEmpNi() {
		return this.cibcNbrEmpNi;
	}

	public void setCibcNbrEmpSign(char cibcNbrEmpSign) {
		this.cibcNbrEmpSign = cibcNbrEmpSign;
	}

	public char getCibcNbrEmpSign() {
		return this.cibcNbrEmpSign;
	}

	public void setCibcStrDtCi(char cibcStrDtCi) {
		this.cibcStrDtCi = cibcStrDtCi;
	}

	public char getCibcStrDtCi() {
		return this.cibcStrDtCi;
	}

	public void setCibcStrDtNi(char cibcStrDtNi) {
		this.cibcStrDtNi = cibcStrDtNi;
	}

	public char getCibcStrDtNi() {
		return this.cibcStrDtNi;
	}

	public void setCibcStrDt(String cibcStrDt) {
		this.cibcStrDt = Functions.subString(cibcStrDt, Len.CIBC_STR_DT);
	}

	public String getCibcStrDt() {
		return this.cibcStrDt;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setCibcEffAcyTsCi(char cibcEffAcyTsCi) {
		this.cibcEffAcyTsCi = cibcEffAcyTsCi;
	}

	public char getCibcEffAcyTsCi() {
		return this.cibcEffAcyTsCi;
	}

	public void setCibcEffAcyTs(String cibcEffAcyTs) {
		this.cibcEffAcyTs = Functions.subString(cibcEffAcyTs, Len.CIBC_EFF_ACY_TS);
	}

	public String getCibcEffAcyTs() {
		return this.cibcEffAcyTs;
	}

	public void setCibcExpAcyTsCi(char cibcExpAcyTsCi) {
		this.cibcExpAcyTsCi = cibcExpAcyTsCi;
	}

	public char getCibcExpAcyTsCi() {
		return this.cibcExpAcyTsCi;
	}

	public void setCibcExpAcyTs(String cibcExpAcyTs) {
		this.cibcExpAcyTs = Functions.subString(cibcExpAcyTs, Len.CIBC_EXP_ACY_TS);
	}

	public String getCibcExpAcyTs() {
		return this.cibcExpAcyTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CIBC_EXP_DT = 10;
		public static final int GRS_REV_CD = 2;
		public static final int IDY_TYP_CD = 10;
		public static final int CIBC_NBR_EMP = 10;
		public static final int CIBC_STR_DT = 10;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CIBC_EFF_ACY_TS = 26;
		public static final int CIBC_EXP_ACY_TS = 26;
		public static final int CIBC_EXP_DT_CI = 1;
		public static final int GRS_REV_CD_CI = 1;
		public static final int IDY_TYP_CD_CI = 1;
		public static final int CIBC_NBR_EMP_CI = 1;
		public static final int CIBC_NBR_EMP_NI = 1;
		public static final int CIBC_NBR_EMP_SIGN = 1;
		public static final int CIBC_STR_DT_CI = 1;
		public static final int CIBC_STR_DT_NI = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int CIBC_EFF_ACY_TS_CI = 1;
		public static final int CIBC_EXP_ACY_TS_CI = 1;
		public static final int BUSINESS_CLIENT_DATA = CIBC_EXP_DT_CI + CIBC_EXP_DT + GRS_REV_CD_CI + GRS_REV_CD + IDY_TYP_CD_CI + IDY_TYP_CD
				+ CIBC_NBR_EMP_CI + CIBC_NBR_EMP_NI + CIBC_NBR_EMP_SIGN + CIBC_NBR_EMP + CIBC_STR_DT_CI + CIBC_STR_DT_NI + CIBC_STR_DT + USER_ID_CI
				+ USER_ID + STATUS_CD_CI + STATUS_CD + TERMINAL_ID_CI + TERMINAL_ID + CIBC_EFF_ACY_TS_CI + CIBC_EFF_ACY_TS + CIBC_EXP_ACY_TS_CI
				+ CIBC_EXP_ACY_TS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
