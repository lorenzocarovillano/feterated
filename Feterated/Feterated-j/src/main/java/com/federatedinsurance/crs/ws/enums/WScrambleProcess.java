/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: W-SCRAMBLE-PROCESS<br>
 * Variable: W-SCRAMBLE-PROCESS from program HALOUIDG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WScrambleProcess {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char INCOMPLETE = Types.SPACE_CHAR;
	public static final char COMPLETE = 'C';

	//==== METHODS ====
	public void setScrambleProcess(char scrambleProcess) {
		this.value = scrambleProcess;
	}

	public void setScrambleProcessFormatted(String scrambleProcess) {
		setScrambleProcess(Functions.charAt(scrambleProcess, Types.CHAR_SIZE));
	}

	public char getScrambleProcess() {
		return this.value;
	}

	public boolean isIncomplete() {
		return value == INCOMPLETE;
	}

	public void setIncomplete() {
		setScrambleProcessFormatted(String.valueOf(INCOMPLETE));
	}

	public boolean isComplete() {
		return value == COMPLETE;
	}

	public void setComplete() {
		value = COMPLETE;
	}
}
