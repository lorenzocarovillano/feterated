/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;

/**Original name: O-VPS-STRUCT-FIELD-198-FILE<br>
 * File: O-VPS-STRUCT-FIELD-198-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OVpsStructField198FileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: VSO-STRUCT-198-CARRIAGE-CTRL
	private String vsoStruct198CarriageCtrl = DefaultValues.stringVal(Len.VSO_STRUCT198_CARRIAGE_CTRL);
	//Original name: VSO-STRUCT-198-DATA
	private String vsoStruct198Data = DefaultValues.stringVal(Len.VSO_STRUCT198_DATA);

	//==== METHODS ====
	public void setoVpsStructField198RecordBytes(byte[] buffer, int offset) {
		int position = offset;
		setVsoStruct198CarriageCtrlXBytes(buffer, position);
		position += Len.VSO_STRUCT198_CARRIAGE_CTRL_X;
		vsoStruct198Data = MarshalByte.readString(buffer, position, Len.VSO_STRUCT198_DATA);
	}

	public byte[] getoVpsStructField198RecordBytes(byte[] buffer, int offset) {
		int position = offset;
		getVsoStruct198CarriageCtrlXBytes(buffer, position);
		position += Len.VSO_STRUCT198_CARRIAGE_CTRL_X;
		MarshalByte.writeString(buffer, position, vsoStruct198Data, Len.VSO_STRUCT198_DATA);
		return buffer;
	}

	public void setVsoStruct198CarriageCtrlXFormatted(String data) {
		byte[] buffer = new byte[Len.VSO_STRUCT198_CARRIAGE_CTRL_X];
		MarshalByte.writeString(buffer, 1, data, Len.VSO_STRUCT198_CARRIAGE_CTRL_X);
		setVsoStruct198CarriageCtrlXBytes(buffer, 1);
	}

	public void setVsoStruct198CarriageCtrlXBytes(byte[] buffer, int offset) {
		int position = offset;
		vsoStruct198CarriageCtrl = MarshalByte.readFixedString(buffer, position, Len.VSO_STRUCT198_CARRIAGE_CTRL);
	}

	public byte[] getVsoStruct198CarriageCtrlXBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, vsoStruct198CarriageCtrl, Len.VSO_STRUCT198_CARRIAGE_CTRL);
		return buffer;
	}

	public void setVsoStruct198Data(String vsoStruct198Data) {
		this.vsoStruct198Data = Functions.subString(vsoStruct198Data, Len.VSO_STRUCT198_DATA);
	}

	public String getVsoStruct198Data() {
		return this.vsoStruct198Data;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoVpsStructField198RecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoVpsStructField198RecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_VPS_STRUCT_FIELD198_RECORD;
	}

	@Override
	public IBuffer copy() {
		OVpsStructField198FileTO copyTO = new OVpsStructField198FileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VSO_STRUCT198_CARRIAGE_CTRL = 1;
		public static final int VSO_STRUCT198_CARRIAGE_CTRL_X = VSO_STRUCT198_CARRIAGE_CTRL;
		public static final int VSO_STRUCT198_DATA = 198;
		public static final int O_VPS_STRUCT_FIELD198_RECORD = VSO_STRUCT198_CARRIAGE_CTRL_X + VSO_STRUCT198_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
