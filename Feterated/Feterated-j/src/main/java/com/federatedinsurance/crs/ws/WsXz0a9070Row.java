/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.copy.Xza970AcyFrmInfo;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9070-ROW<br>
 * Variable: WS-XZ0A9070-ROW from program XZ0B9070<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9070Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA970-MAX-FRM-ROWS
	private short maxFrmRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA970-ACY-FRM-INFO
	private Xza970AcyFrmInfo acyFrmInfo = new Xza970AcyFrmInfo();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9070_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9070RowBytes(buf);
	}

	public String getWsXz0a9070RowFormatted() {
		return getAcyFrmRowFormatted();
	}

	public void setWsXz0a9070RowBytes(byte[] buffer) {
		setWsXz0a9070RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9070RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9070_ROW];
		return getWsXz0a9070RowBytes(buffer, 1);
	}

	public void setWsXz0a9070RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setAcyFrmRowBytes(buffer, position);
	}

	public byte[] getWsXz0a9070RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getAcyFrmRowBytes(buffer, position);
		return buffer;
	}

	public String getAcyFrmRowFormatted() {
		return MarshalByteExt.bufferToStr(getAcyFrmRowBytes());
	}

	/**Original name: XZA970-ACY-FRM-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0A9070 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_ACY_FRM_LIST                       *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  04FEB2009 E404DAP    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getAcyFrmRowBytes() {
		byte[] buffer = new byte[Len.ACY_FRM_ROW];
		return getAcyFrmRowBytes(buffer, 1);
	}

	public void setAcyFrmRowBytes(byte[] buffer, int offset) {
		int position = offset;
		maxFrmRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		acyFrmInfo.setAcyFrmInfoBytes(buffer, position);
	}

	public byte[] getAcyFrmRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxFrmRows);
		position += Types.SHORT_SIZE;
		acyFrmInfo.getAcyFrmInfoBytes(buffer, position);
		return buffer;
	}

	public void setMaxFrmRows(short maxFrmRows) {
		this.maxFrmRows = maxFrmRows;
	}

	public short getMaxFrmRows() {
		return this.maxFrmRows;
	}

	public Xza970AcyFrmInfo getAcyFrmInfo() {
		return acyFrmInfo;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9070RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_FRM_ROWS = 2;
		public static final int ACY_FRM_ROW = MAX_FRM_ROWS + Xza970AcyFrmInfo.Len.ACY_FRM_INFO;
		public static final int WS_XZ0A9070_ROW = ACY_FRM_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
