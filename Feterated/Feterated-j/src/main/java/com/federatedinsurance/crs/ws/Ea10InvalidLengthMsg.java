/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-10-INVALID-LENGTH-MSG<br>
 * Variable: EA-10-INVALID-LENGTH-MSG from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea10InvalidLengthMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-10-INVALID-LENGTH-MSG
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-10-INVALID-LENGTH-MSG-1
	private String flr2 = "TS030099 -";
	//Original name: FILLER-EA-10-INVALID-LENGTH-MSG-2
	private String flr3 = "WARNING:";
	//Original name: FILLER-EA-10-INVALID-LENGTH-MSG-3
	private String flr4 = "INVALID LENGTH";
	//Original name: FILLER-EA-10-INVALID-LENGTH-MSG-4
	private String flr5 = "PARAMETER";
	//Original name: FILLER-EA-10-INVALID-LENGTH-MSG-5
	private String flr6 = "PASSED:";
	//Original name: EA-10-LENGTH-PARAMETER
	private String lengthParameter = DefaultValues.stringVal(Len.LENGTH_PARAMETER);
	//Original name: FILLER-EA-10-INVALID-LENGTH-MSG-6
	private String flr7 = "REPORT:";
	//Original name: EA-10-REPORT-NBR
	private String reportNbr = DefaultValues.stringVal(Len.REPORT_NBR);

	//==== METHODS ====
	public String getEa10InvalidLengthMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa10InvalidLengthMsgBytes());
	}

	public byte[] getEa10InvalidLengthMsgBytes() {
		byte[] buffer = new byte[Len.EA10_INVALID_LENGTH_MSG];
		return getEa10InvalidLengthMsgBytes(buffer, 1);
	}

	public byte[] getEa10InvalidLengthMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, lengthParameter, Len.LENGTH_PARAMETER);
		position += Len.LENGTH_PARAMETER;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, reportNbr, Len.REPORT_NBR);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setLengthParameter(long lengthParameter) {
		this.lengthParameter = PicFormatter.display("++(2)9").format(lengthParameter).toString();
	}

	public long getLengthParameter() {
		return PicParser.display("++(2)9").parseLong(this.lengthParameter);
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setReportNbr(String reportNbr) {
		this.reportNbr = Functions.subString(reportNbr, Len.REPORT_NBR);
	}

	public String getReportNbr() {
		return this.reportNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LENGTH_PARAMETER = 4;
		public static final int REPORT_NBR = 8;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 9;
		public static final int FLR4 = 15;
		public static final int FLR5 = 10;
		public static final int FLR6 = 8;
		public static final int EA10_INVALID_LENGTH_MSG = LENGTH_PARAMETER + REPORT_NBR + FLR1 + FLR2 + FLR3 + FLR4 + FLR5 + 2 * FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
