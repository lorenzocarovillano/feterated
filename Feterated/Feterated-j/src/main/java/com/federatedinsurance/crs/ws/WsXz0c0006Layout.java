/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-XZ0C0006-LAYOUT<br>
 * Variable: WS-XZ0C0006-LAYOUT from program XZ0F0006<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0c0006Layout extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZC006-ACT-NOT-FRM-CSUM
	private String actNotFrmCsum = DefaultValues.stringVal(Len.ACT_NOT_FRM_CSUM);
	//Original name: XZC006-CSR-ACT-NBR-KCRE
	private String csrActNbrKcre = DefaultValues.stringVal(Len.CSR_ACT_NBR_KCRE);
	//Original name: XZC006-NOT-PRC-TS-KCRE
	private String notPrcTsKcre = DefaultValues.stringVal(Len.NOT_PRC_TS_KCRE);
	//Original name: XZC006-FRM-SEQ-NBR-KCRE
	private String frmSeqNbrKcre = DefaultValues.stringVal(Len.FRM_SEQ_NBR_KCRE);
	//Original name: XZC006-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC006-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC006-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC006-FRM-SEQ-NBR-SIGN
	private char frmSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: XZC006-FRM-SEQ-NBR
	private String frmSeqNbr = DefaultValues.stringVal(Len.FRM_SEQ_NBR);
	//Original name: XZC006-CSR-ACT-NBR-CI
	private char csrActNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC006-NOT-PRC-TS-CI
	private char notPrcTsCi = DefaultValues.CHAR_VAL;
	//Original name: XZC006-FRM-SEQ-NBR-CI
	private char frmSeqNbrCi = DefaultValues.CHAR_VAL;
	/**Original name: XZC006-FRM-NBR-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char frmNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC006-FRM-NBR
	private String frmNbr = DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: XZC006-FRM-EDT-DT-CI
	private char frmEdtDtCi = DefaultValues.CHAR_VAL;
	//Original name: XZC006-FRM-EDT-DT
	private String frmEdtDt = DefaultValues.stringVal(Len.FRM_EDT_DT);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0C0006_LAYOUT;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0c0006LayoutBytes(buf);
	}

	public void setWsXz0c0006LayoutFormatted(String data) {
		byte[] buffer = new byte[Len.WS_XZ0C0006_LAYOUT];
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0C0006_LAYOUT);
		setWsXz0c0006LayoutBytes(buffer, 1);
	}

	public String getWsXz0c0006LayoutFormatted() {
		return getActNotFrmRowFormatted();
	}

	public void setWsXz0c0006LayoutBytes(byte[] buffer) {
		setWsXz0c0006LayoutBytes(buffer, 1);
	}

	public byte[] getWsXz0c0006LayoutBytes() {
		byte[] buffer = new byte[Len.WS_XZ0C0006_LAYOUT];
		return getWsXz0c0006LayoutBytes(buffer, 1);
	}

	public void setWsXz0c0006LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotFrmRowBytes(buffer, position);
	}

	public byte[] getWsXz0c0006LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotFrmRowBytes(buffer, position);
		return buffer;
	}

	public String getActNotFrmRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotFrmRowBytes());
	}

	/**Original name: XZC006-ACT-NOT-FRM-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0C0006 - ACT_NOT_FRM TABLE                                   *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 24 Dec 2008 E404GRK   GENERATED                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getActNotFrmRowBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_FRM_ROW];
		return getActNotFrmRowBytes(buffer, 1);
	}

	public void setActNotFrmRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotFrmFixedBytes(buffer, position);
		position += Len.ACT_NOT_FRM_FIXED;
		setActNotFrmDatesBytes(buffer, position);
		position += Len.ACT_NOT_FRM_DATES;
		setActNotFrmKeyBytes(buffer, position);
		position += Len.ACT_NOT_FRM_KEY;
		setActNotFrmKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_FRM_KEY_CI;
		setActNotFrmDataBytes(buffer, position);
	}

	public byte[] getActNotFrmRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotFrmFixedBytes(buffer, position);
		position += Len.ACT_NOT_FRM_FIXED;
		getActNotFrmDatesBytes(buffer, position);
		position += Len.ACT_NOT_FRM_DATES;
		getActNotFrmKeyBytes(buffer, position);
		position += Len.ACT_NOT_FRM_KEY;
		getActNotFrmKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_FRM_KEY_CI;
		getActNotFrmDataBytes(buffer, position);
		return buffer;
	}

	public void setActNotFrmFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotFrmCsum = MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_FRM_CSUM);
		position += Len.ACT_NOT_FRM_CSUM;
		csrActNbrKcre = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		notPrcTsKcre = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		frmSeqNbrKcre = MarshalByte.readString(buffer, position, Len.FRM_SEQ_NBR_KCRE);
	}

	public byte[] getActNotFrmFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotFrmCsum, Len.ACT_NOT_FRM_CSUM);
		position += Len.ACT_NOT_FRM_CSUM;
		MarshalByte.writeString(buffer, position, csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		MarshalByte.writeString(buffer, position, notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		MarshalByte.writeString(buffer, position, frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
		return buffer;
	}

	public void setActNotFrmCsumFormatted(String actNotFrmCsum) {
		this.actNotFrmCsum = Trunc.toUnsignedNumeric(actNotFrmCsum, Len.ACT_NOT_FRM_CSUM);
	}

	public int getActNotFrmCsum() {
		return NumericDisplay.asInt(this.actNotFrmCsum);
	}

	public void setCsrActNbrKcre(String csrActNbrKcre) {
		this.csrActNbrKcre = Functions.subString(csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
	}

	public String getCsrActNbrKcre() {
		return this.csrActNbrKcre;
	}

	public void setNotPrcTsKcre(String notPrcTsKcre) {
		this.notPrcTsKcre = Functions.subString(notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
	}

	public String getNotPrcTsKcre() {
		return this.notPrcTsKcre;
	}

	public void setFrmSeqNbrKcre(String frmSeqNbrKcre) {
		this.frmSeqNbrKcre = Functions.subString(frmSeqNbrKcre, Len.FRM_SEQ_NBR_KCRE);
	}

	public String getFrmSeqNbrKcre() {
		return this.frmSeqNbrKcre;
	}

	public void setActNotFrmDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getActNotFrmDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setActNotFrmKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		frmSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		frmSeqNbr = MarshalByte.readFixedString(buffer, position, Len.FRM_SEQ_NBR);
	}

	public byte[] getActNotFrmKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, frmSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public void setFrmSeqNbrSign(char frmSeqNbrSign) {
		this.frmSeqNbrSign = frmSeqNbrSign;
	}

	public char getFrmSeqNbrSign() {
		return this.frmSeqNbrSign;
	}

	public void setFrmSeqNbrFormatted(String frmSeqNbr) {
		this.frmSeqNbr = Trunc.toUnsignedNumeric(frmSeqNbr, Len.FRM_SEQ_NBR);
	}

	public int getFrmSeqNbr() {
		return NumericDisplay.asInt(this.frmSeqNbr);
	}

	public void setActNotFrmKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notPrcTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		frmSeqNbrCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotFrmKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, frmSeqNbrCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi = csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi = notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setFrmSeqNbrCi(char frmSeqNbrCi) {
		this.frmSeqNbrCi = frmSeqNbrCi;
	}

	public char getFrmSeqNbrCi() {
		return this.frmSeqNbrCi;
	}

	public void setActNotFrmDataBytes(byte[] buffer, int offset) {
		int position = offset;
		frmNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		frmNbr = MarshalByte.readString(buffer, position, Len.FRM_NBR);
		position += Len.FRM_NBR;
		frmEdtDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		frmEdtDt = MarshalByte.readString(buffer, position, Len.FRM_EDT_DT);
	}

	public byte[] getActNotFrmDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, frmNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, frmNbr, Len.FRM_NBR);
		position += Len.FRM_NBR;
		MarshalByte.writeChar(buffer, position, frmEdtDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, frmEdtDt, Len.FRM_EDT_DT);
		return buffer;
	}

	public void setFrmNbrCi(char frmNbrCi) {
		this.frmNbrCi = frmNbrCi;
	}

	public char getFrmNbrCi() {
		return this.frmNbrCi;
	}

	public void setFrmNbr(String frmNbr) {
		this.frmNbr = Functions.subString(frmNbr, Len.FRM_NBR);
	}

	public String getFrmNbr() {
		return this.frmNbr;
	}

	public void setFrmEdtDtCi(char frmEdtDtCi) {
		this.frmEdtDtCi = frmEdtDtCi;
	}

	public char getFrmEdtDtCi() {
		return this.frmEdtDtCi;
	}

	public void setFrmEdtDt(String frmEdtDt) {
		this.frmEdtDt = Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	public String getFrmEdtDt() {
		return this.frmEdtDt;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0c0006LayoutBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_FRM_CSUM = 9;
		public static final int CSR_ACT_NBR_KCRE = 32;
		public static final int NOT_PRC_TS_KCRE = 32;
		public static final int FRM_SEQ_NBR_KCRE = 32;
		public static final int ACT_NOT_FRM_FIXED = ACT_NOT_FRM_CSUM + CSR_ACT_NBR_KCRE + NOT_PRC_TS_KCRE + FRM_SEQ_NBR_KCRE;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int ACT_NOT_FRM_DATES = TRANS_PROCESS_DT;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FRM_SEQ_NBR_SIGN = 1;
		public static final int FRM_SEQ_NBR = 5;
		public static final int ACT_NOT_FRM_KEY = CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR_SIGN + FRM_SEQ_NBR;
		public static final int CSR_ACT_NBR_CI = 1;
		public static final int NOT_PRC_TS_CI = 1;
		public static final int FRM_SEQ_NBR_CI = 1;
		public static final int ACT_NOT_FRM_KEY_CI = CSR_ACT_NBR_CI + NOT_PRC_TS_CI + FRM_SEQ_NBR_CI;
		public static final int FRM_NBR_CI = 1;
		public static final int FRM_NBR = 30;
		public static final int FRM_EDT_DT_CI = 1;
		public static final int FRM_EDT_DT = 10;
		public static final int ACT_NOT_FRM_DATA = FRM_NBR_CI + FRM_NBR + FRM_EDT_DT_CI + FRM_EDT_DT;
		public static final int ACT_NOT_FRM_ROW = ACT_NOT_FRM_FIXED + ACT_NOT_FRM_DATES + ACT_NOT_FRM_KEY + ACT_NOT_FRM_KEY_CI + ACT_NOT_FRM_DATA;
		public static final int WS_XZ0C0006_LAYOUT = ACT_NOT_FRM_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
