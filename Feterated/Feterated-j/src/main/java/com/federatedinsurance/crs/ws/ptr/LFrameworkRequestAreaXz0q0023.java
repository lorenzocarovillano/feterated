/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q0023<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q0023 extends BytesClass {

	//==== PROPERTIES ====
	public static final String XZY023_GET_POL_PND_CNC_STA = "GetPolPndCncStatus";

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q0023() {
	}

	public LFrameworkRequestAreaXz0q0023(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzy023Operation(String xzy023Operation) {
		writeString(Pos.XZY023_OPERATION, xzy023Operation, Len.XZY023_OPERATION);
	}

	/**Original name: XZY023-OPERATION<br>*/
	public String getXzy023Operation() {
		return readString(Pos.XZY023_OPERATION, Len.XZY023_OPERATION);
	}

	public void setXzy023PolNbr(String xzy023PolNbr) {
		writeString(Pos.XZY023_POL_NBR, xzy023PolNbr, Len.XZY023_POL_NBR);
	}

	/**Original name: XZY023-POL-NBR<br>*/
	public String getXzy023PolNbr() {
		return readString(Pos.XZY023_POL_NBR, Len.XZY023_POL_NBR);
	}

	public void setXzy023PolEffDt(String xzy023PolEffDt) {
		writeString(Pos.XZY023_POL_EFF_DT, xzy023PolEffDt, Len.XZY023_POL_EFF_DT);
	}

	/**Original name: XZY023-POL-EFF-DT<br>*/
	public String getXzy023PolEffDt() {
		return readString(Pos.XZY023_POL_EFF_DT, Len.XZY023_POL_EFF_DT);
	}

	public void setXzy023PolExpDt(String xzy023PolExpDt) {
		writeString(Pos.XZY023_POL_EXP_DT, xzy023PolExpDt, Len.XZY023_POL_EXP_DT);
	}

	/**Original name: XZY023-POL-EXP-DT<br>*/
	public String getXzy023PolExpDt() {
		return readString(Pos.XZY023_POL_EXP_DT, Len.XZY023_POL_EXP_DT);
	}

	public void setXzy023PndCncInd(char xzy023PndCncInd) {
		writeChar(Pos.XZY023_PND_CNC_IND, xzy023PndCncInd);
	}

	/**Original name: XZY023-PND-CNC-IND<br>*/
	public char getXzy023PndCncInd() {
		return readChar(Pos.XZY023_PND_CNC_IND);
	}

	public void setXzy023SchCncDt(String xzy023SchCncDt) {
		writeString(Pos.XZY023_SCH_CNC_DT, xzy023SchCncDt, Len.XZY023_SCH_CNC_DT);
	}

	/**Original name: XZY023-SCH-CNC-DT<br>*/
	public String getXzy023SchCncDt() {
		return readString(Pos.XZY023_SCH_CNC_DT, Len.XZY023_SCH_CNC_DT);
	}

	public void setXzy023NotTypCd(String xzy023NotTypCd) {
		writeString(Pos.XZY023_NOT_TYP_CD, xzy023NotTypCd, Len.XZY023_NOT_TYP_CD);
	}

	/**Original name: XZY023-NOT-TYP-CD<br>*/
	public String getXzy023NotTypCd() {
		return readString(Pos.XZY023_NOT_TYP_CD, Len.XZY023_NOT_TYP_CD);
	}

	public void setXzy023NotTypDes(String xzy023NotTypDes) {
		writeString(Pos.XZY023_NOT_TYP_DES, xzy023NotTypDes, Len.XZY023_NOT_TYP_DES);
	}

	/**Original name: XZY023-NOT-TYP-DES<br>*/
	public String getXzy023NotTypDes() {
		return readString(Pos.XZY023_NOT_TYP_DES, Len.XZY023_NOT_TYP_DES);
	}

	public void setXzy023NotEmpId(String xzy023NotEmpId) {
		writeString(Pos.XZY023_NOT_EMP_ID, xzy023NotEmpId, Len.XZY023_NOT_EMP_ID);
	}

	/**Original name: XZY023-NOT-EMP-ID<br>*/
	public String getXzy023NotEmpId() {
		return readString(Pos.XZY023_NOT_EMP_ID, Len.XZY023_NOT_EMP_ID);
	}

	public void setXzy023NotEmpNm(String xzy023NotEmpNm) {
		writeString(Pos.XZY023_NOT_EMP_NM, xzy023NotEmpNm, Len.XZY023_NOT_EMP_NM);
	}

	/**Original name: XZY023-NOT-EMP-NM<br>*/
	public String getXzy023NotEmpNm() {
		return readString(Pos.XZY023_NOT_EMP_NM, Len.XZY023_NOT_EMP_NM);
	}

	public void setXzy023NotPrcDt(String xzy023NotPrcDt) {
		writeString(Pos.XZY023_NOT_PRC_DT, xzy023NotPrcDt, Len.XZY023_NOT_PRC_DT);
	}

	/**Original name: XZY023-NOT-PRC-DT<br>*/
	public String getXzy023NotPrcDt() {
		return readString(Pos.XZY023_NOT_PRC_DT, Len.XZY023_NOT_PRC_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_GET_POL_PND_CNC_STA = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZY023_GET_POL_PND_CNC_STA_ROW = L_FW_REQ_GET_POL_PND_CNC_STA;
		public static final int XZY023_OPERATION = XZY023_GET_POL_PND_CNC_STA_ROW;
		public static final int XZY023_POL_NBR = XZY023_OPERATION + Len.XZY023_OPERATION;
		public static final int XZY023_POL_EFF_DT = XZY023_POL_NBR + Len.XZY023_POL_NBR;
		public static final int XZY023_POL_EXP_DT = XZY023_POL_EFF_DT + Len.XZY023_POL_EFF_DT;
		public static final int XZY023_PND_CNC_IND = XZY023_POL_EXP_DT + Len.XZY023_POL_EXP_DT;
		public static final int XZY023_SCH_CNC_DT = XZY023_PND_CNC_IND + Len.XZY023_PND_CNC_IND;
		public static final int XZY023_NOT_TYP_CD = XZY023_SCH_CNC_DT + Len.XZY023_SCH_CNC_DT;
		public static final int XZY023_NOT_TYP_DES = XZY023_NOT_TYP_CD + Len.XZY023_NOT_TYP_CD;
		public static final int XZY023_NOT_EMP_ID = XZY023_NOT_TYP_DES + Len.XZY023_NOT_TYP_DES;
		public static final int XZY023_NOT_EMP_NM = XZY023_NOT_EMP_ID + Len.XZY023_NOT_EMP_ID;
		public static final int XZY023_NOT_PRC_DT = XZY023_NOT_EMP_NM + Len.XZY023_NOT_EMP_NM;
		public static final int FLR1 = XZY023_NOT_PRC_DT + Len.XZY023_NOT_PRC_DT;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY023_OPERATION = 32;
		public static final int XZY023_POL_NBR = 25;
		public static final int XZY023_POL_EFF_DT = 10;
		public static final int XZY023_POL_EXP_DT = 10;
		public static final int XZY023_PND_CNC_IND = 1;
		public static final int XZY023_SCH_CNC_DT = 10;
		public static final int XZY023_NOT_TYP_CD = 5;
		public static final int XZY023_NOT_TYP_DES = 35;
		public static final int XZY023_NOT_EMP_ID = 6;
		public static final int XZY023_NOT_EMP_NM = 67;
		public static final int XZY023_NOT_PRC_DT = 10;
		public static final int FLR1 = 100;
		public static final int XZY023_GET_POL_PND_CNC_STA_ROW = XZY023_OPERATION + XZY023_POL_NBR + XZY023_POL_EFF_DT + XZY023_POL_EXP_DT
				+ XZY023_PND_CNC_IND + XZY023_SCH_CNC_DT + XZY023_NOT_TYP_CD + XZY023_NOT_TYP_DES + XZY023_NOT_EMP_ID + XZY023_NOT_EMP_NM
				+ XZY023_NOT_PRC_DT + FLR1;
		public static final int L_FW_REQ_GET_POL_PND_CNC_STA = XZY023_GET_POL_PND_CNC_STA_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_GET_POL_PND_CNC_STA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
