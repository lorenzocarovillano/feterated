/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program XZ0P90H0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsXz0p90h0 {

	//==== PROPERTIES ====
	/**Original name: SS-CL<br>
	 * <pre>  SUBSCRIPT FOR THE ACCOUNT CLIENT LIST.</pre>*/
	private short cl = DefaultValues.BIN_SHORT_VAL;
	public static final short CL_MAX = ((short) 50);
	//Original name: SS-MSG-IDX
	private short msgIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short MSG_IDX_MAX = ((short) 10);
	//Original name: SS-WNG-IDX
	private short wngIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short WNG_IDX_MAX = ((short) 10);

	//==== METHODS ====
	public void setCl(short cl) {
		this.cl = cl;
	}

	public short getCl() {
		return this.cl;
	}

	public boolean isClMax() {
		return cl == CL_MAX;
	}

	public void setMsgIdx(short msgIdx) {
		this.msgIdx = msgIdx;
	}

	public short getMsgIdx() {
		return this.msgIdx;
	}

	public boolean isMsgIdxMax() {
		return msgIdx == MSG_IDX_MAX;
	}

	public void setWngIdx(short wngIdx) {
		this.wngIdx = wngIdx;
	}

	public short getWngIdx() {
		return this.wngIdx;
	}

	public boolean isWngIdxMax() {
		return wngIdx == WNG_IDX_MAX;
	}
}
