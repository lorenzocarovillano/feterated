/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;

/**Original name: HSDD-CMN-ATR-IND<br>
 * Variable: HSDD-CMN-ATR-IND from copybook HALLGSDD<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class HsddCmnAtrInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	private static final char[] VALID_COL_IND_VALUE = new char[] { ' ', 'E', 'A', 'D', 'V', 'X', 'F', 'B', 'C', 'Q', '*' };
	public static final char IS_UPDATABLE_COL_IND = Types.SPACE_CHAR;
	public static final char IS_ENCRYPTED_COL_IND = 'E';
	public static final char IS_ALTERED_COL_IND = 'A';
	public static final char IS_DEFAULTED_COL_IND = 'D';
	public static final char IS_VIEWONLY_COL_IND = 'V';
	public static final char IS_EXCLUDED_COL_IND = 'X';
	public static final char IS_ENCRYPTED_RON_COL_IND = 'F';
	public static final char IS_ALTERED_RON_COL_IND = 'B';
	public static final char IS_DEFAULTED_RON_COL_IND = 'C';
	public static final char IS_REQUIRED_COL_IND = 'Q';
	public static final char IGNORE_COL_IND = '*';

	//==== METHODS ====
	public void setCmnAtrInd(char cmnAtrInd) {
		this.value = cmnAtrInd;
	}

	public char getCmnAtrInd() {
		return this.value;
	}

	public boolean isValidColIndValue() {
		return ArrayUtils.contains(VALID_COL_IND_VALUE, value);
	}
}
