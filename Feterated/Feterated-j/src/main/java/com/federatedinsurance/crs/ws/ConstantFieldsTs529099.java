/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program TS529099<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class ConstantFieldsTs529099 {

	//==== PROPERTIES ====
	//Original name: CF-AL-LINE-1
	private short alLine1 = ((short) 1);
	//Original name: CF-AL-LINE-2
	private short alLine2 = ((short) 2);
	//Original name: CF-AL-LINE-3
	private short alLine3 = ((short) 3);
	//Original name: CF-AL-LINE-6
	private short alLine6 = ((short) 6);
	//Original name: CF-CITY-STATE-DLM
	private String cityStateDlm = ",";
	//Original name: CF-STATE-ZIP-DLM
	private char stateZipDlm = Types.SPACE_CHAR;

	//==== METHODS ====
	public short getAlLine1() {
		return this.alLine1;
	}

	public short getAlLine2() {
		return this.alLine2;
	}

	public short getAlLine3() {
		return this.alLine3;
	}

	public short getAlLine6() {
		return this.alLine6;
	}

	public String getCityStateDlm() {
		return this.cityStateDlm;
	}

	public String getCityStateDlmFormatted() {
		return Functions.padBlanks(getCityStateDlm(), Len.CITY_STATE_DLM);
	}

	public char getStateZipDlm() {
		return this.stateZipDlm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CITY_STATE_DLM = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
