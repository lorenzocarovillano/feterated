/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotPolFrm;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [ACT_NOT_POL, ACT_NOT_POL_FRM]
 * 
 */
public class ActNotPolFrmDao extends BaseSqlDao<IActNotPolFrm> {

	private Cursor polLstByFrmCsr;
	private final IRowMapper<IActNotPolFrm> fetchPolLstByFrmCsrRm = buildNamedRowMapper(IActNotPolFrm.class, "csrActNbr", "notPrcTs", "polNbr",
			"polTypCd", "polPriRskStAbb", "notEffDtObj", "polEffDt", "polExpDt", "polDueAmtObj", "ninCltId", "ninAdrId", "wfStartedIndObj",
			"polBilStaCdObj");

	public ActNotPolFrmDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotPolFrm> getToClass() {
		return IActNotPolFrm.class;
	}

	public DbAccessStatus openPolLstByFrmCsr(String csrActNbr, String notPrcTs, short frmSeqNbr) {
		polLstByFrmCsr = buildQuery("openPolLstByFrmCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr).open();
		return dbStatus;
	}

	public IActNotPolFrm fetchPolLstByFrmCsr(IActNotPolFrm iActNotPolFrm) {
		return fetch(polLstByFrmCsr, iActNotPolFrm, fetchPolLstByFrmCsrRm);
	}

	public DbAccessStatus closePolLstByFrmCsr() {
		return closeCursor(polLstByFrmCsr);
	}
}
