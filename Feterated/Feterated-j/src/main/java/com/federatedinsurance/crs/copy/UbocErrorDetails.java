/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.UbocLoggableErrLogOnlySw;
import com.federatedinsurance.crs.ws.enums.UbocObjectLoggableProblems;
import com.federatedinsurance.crs.ws.enums.UbocProcessingStatusSw;

/**Original name: UBOC-ERROR-DETAILS<br>
 * Variable: UBOC-ERROR-DETAILS from copybook HALLUBOC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class UbocErrorDetails {

	//==== PROPERTIES ====
	//Original name: UBOC-OBJECT-LOGGABLE-PROBLEMS
	private UbocObjectLoggableProblems ubocObjectLoggableProblems = new UbocObjectLoggableProblems();
	//Original name: UBOC-PROCESSING-STATUS-SW
	private UbocProcessingStatusSw ubocProcessingStatusSw = new UbocProcessingStatusSw();
	//Original name: FILLER-6
	private String filler6 = DefaultValues.stringVal(Len.FILLER6);
	//Original name: UBOC-OBJECT-ERROR-INFO
	private UbocObjectErrorInfo ubocObjectErrorInfo = new UbocObjectErrorInfo();
	//Original name: UBOC-LOGGABLE-ERR-LOG-ONLY-SW
	private UbocLoggableErrLogOnlySw ubocLoggableErrLogOnlySw = new UbocLoggableErrLogOnlySw();
	//Original name: FILLER-7
	private String filler7 = DefaultValues.stringVal(Len.FILLER7);
	//Original name: UBOC-ERROR-LOGGING-INFO
	private UbocErrorLoggingInfo ubocErrorLoggingInfo = new UbocErrorLoggingInfo();
	//Original name: FILLER-8
	private String filler8 = DefaultValues.stringVal(Len.FILLER8);

	//==== METHODS ====
	public void setUbocErrorDetailsBytes(byte[] buffer, int offset) {
		int position = offset;
		ubocObjectLoggableProblems.setUbocObjectLoggableProblems(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		ubocProcessingStatusSw.setUbocProcessingStatusSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		filler6 = MarshalByte.readString(buffer, position, Len.FILLER6);
		position += Len.FILLER6;
		ubocObjectErrorInfo.setUbocObjectErrorInfoBytes(buffer, position);
		position += UbocObjectErrorInfo.Len.UBOC_OBJECT_ERROR_INFO;
		ubocLoggableErrLogOnlySw.setUbocLoggableErrLogOnlySw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		filler7 = MarshalByte.readString(buffer, position, Len.FILLER7);
		position += Len.FILLER7;
		ubocErrorLoggingInfo.setUbocErrorLoggingInfoBytes(buffer, position);
		position += UbocErrorLoggingInfo.Len.UBOC_ERROR_LOGGING_INFO;
		filler8 = MarshalByte.readString(buffer, position, Len.FILLER8);
	}

	public byte[] getUbocErrorDetailsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, ubocObjectLoggableProblems.getUbocObjectLoggableProblems());
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ubocProcessingStatusSw.getUbocProcessingStatusSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, filler6, Len.FILLER6);
		position += Len.FILLER6;
		ubocObjectErrorInfo.getUbocObjectErrorInfoBytes(buffer, position);
		position += UbocObjectErrorInfo.Len.UBOC_OBJECT_ERROR_INFO;
		MarshalByte.writeChar(buffer, position, ubocLoggableErrLogOnlySw.getUbocLoggableErrLogOnlySw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, filler7, Len.FILLER7);
		position += Len.FILLER7;
		ubocErrorLoggingInfo.getUbocErrorLoggingInfoBytes(buffer, position);
		position += UbocErrorLoggingInfo.Len.UBOC_ERROR_LOGGING_INFO;
		MarshalByte.writeString(buffer, position, filler8, Len.FILLER8);
		return buffer;
	}

	public void setFiller6(String filler6) {
		this.filler6 = Functions.subString(filler6, Len.FILLER6);
	}

	public String getFiller6() {
		return this.filler6;
	}

	public void setFiller7(String filler7) {
		this.filler7 = Functions.subString(filler7, Len.FILLER7);
	}

	public String getFiller7() {
		return this.filler7;
	}

	public void setFiller8(String filler8) {
		this.filler8 = Functions.subString(filler8, Len.FILLER8);
	}

	public String getFiller8() {
		return this.filler8;
	}

	public UbocErrorLoggingInfo getUbocErrorLoggingInfo() {
		return ubocErrorLoggingInfo;
	}

	public UbocLoggableErrLogOnlySw getUbocLoggableErrLogOnlySw() {
		return ubocLoggableErrLogOnlySw;
	}

	public UbocObjectErrorInfo getUbocObjectErrorInfo() {
		return ubocObjectErrorInfo;
	}

	public UbocObjectLoggableProblems getUbocObjectLoggableProblems() {
		return ubocObjectLoggableProblems;
	}

	public UbocProcessingStatusSw getUbocProcessingStatusSw() {
		return ubocProcessingStatusSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FILLER6 = 9;
		public static final int FILLER7 = 9;
		public static final int FILLER8 = 10;
		public static final int UBOC_ERROR_DETAILS = UbocObjectLoggableProblems.Len.UBOC_OBJECT_LOGGABLE_PROBLEMS
				+ UbocProcessingStatusSw.Len.UBOC_PROCESSING_STATUS_SW + FILLER6 + UbocObjectErrorInfo.Len.UBOC_OBJECT_ERROR_INFO
				+ UbocLoggableErrLogOnlySw.Len.UBOC_LOGGABLE_ERR_LOG_ONLY_SW + FILLER7 + UbocErrorLoggingInfo.Len.UBOC_ERROR_LOGGING_INFO + FILLER8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
