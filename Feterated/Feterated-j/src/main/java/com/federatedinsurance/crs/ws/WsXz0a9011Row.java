/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9011-ROW<br>
 * Variable: WS-XZ0A9011-ROW from program XZ0B9010<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9011Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA911-WRD-SEQ-CD
	private String wrdSeqCd = DefaultValues.stringVal(Len.WRD_SEQ_CD);
	//Original name: XZA911-WRD-FONT
	private String wrdFont = DefaultValues.stringVal(Len.WRD_FONT);
	//Original name: XZA911-WRD-TXT
	private String wrdTxt = DefaultValues.stringVal(Len.WRD_TXT);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9011_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9011RowBytes(buf);
	}

	public String getWsXz0a9011RowFormatted() {
		return getActNotWrdTextRowFormatted();
	}

	public void setWsXz0a9011RowBytes(byte[] buffer) {
		setWsXz0a9011RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9011RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9011_ROW];
		return getWsXz0a9011RowBytes(buffer, 1);
	}

	public void setWsXz0a9011RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotWrdTextRowBytes(buffer, position);
	}

	public byte[] getWsXz0a9011RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotWrdTextRowBytes(buffer, position);
		return buffer;
	}

	public String getActNotWrdTextRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotWrdTextRowBytes());
	}

	/**Original name: XZA911-ACT-NOT-WRD-TEXT-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0A9011 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_ACT_NOT_WRD_LIST                   *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  14NOV2008 E404DAP    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getActNotWrdTextRowBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_WRD_TEXT_ROW];
		return getActNotWrdTextRowBytes(buffer, 1);
	}

	public void setActNotWrdTextRowBytes(byte[] buffer, int offset) {
		int position = offset;
		wrdSeqCd = MarshalByte.readString(buffer, position, Len.WRD_SEQ_CD);
		position += Len.WRD_SEQ_CD;
		wrdFont = MarshalByte.readString(buffer, position, Len.WRD_FONT);
		position += Len.WRD_FONT;
		wrdTxt = MarshalByte.readString(buffer, position, Len.WRD_TXT);
	}

	public byte[] getActNotWrdTextRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, wrdSeqCd, Len.WRD_SEQ_CD);
		position += Len.WRD_SEQ_CD;
		MarshalByte.writeString(buffer, position, wrdFont, Len.WRD_FONT);
		position += Len.WRD_FONT;
		MarshalByte.writeString(buffer, position, wrdTxt, Len.WRD_TXT);
		return buffer;
	}

	public void setWrdSeqCd(String wrdSeqCd) {
		this.wrdSeqCd = Functions.subString(wrdSeqCd, Len.WRD_SEQ_CD);
	}

	public String getWrdSeqCd() {
		return this.wrdSeqCd;
	}

	public void setWrdFont(String wrdFont) {
		this.wrdFont = Functions.subString(wrdFont, Len.WRD_FONT);
	}

	public String getWrdFont() {
		return this.wrdFont;
	}

	public void setWrdTxt(String wrdTxt) {
		this.wrdTxt = Functions.subString(wrdTxt, Len.WRD_TXT);
	}

	public String getWrdTxt() {
		return this.wrdTxt;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9011RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WRD_SEQ_CD = 5;
		public static final int WRD_FONT = 6;
		public static final int WRD_TXT = 4500;
		public static final int ACT_NOT_WRD_TEXT_ROW = WRD_SEQ_CD + WRD_FONT + WRD_TXT;
		public static final int WS_XZ0A9011_ROW = ACT_NOT_WRD_TEXT_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
