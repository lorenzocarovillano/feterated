/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Cawlf002;
import com.federatedinsurance.crs.copy.DcllegalEntityCodV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallresp;
import com.federatedinsurance.crs.copy.Hallurqa;
import com.federatedinsurance.crs.copy.Ts020tbl;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program CAWI002<br>
 * Generated as a class for rule WS.<br>*/
public class Cawi002Data {

	//==== PROPERTIES ====
	//Original name: CF-REQUEST-UMT-MODULE
	private String cfRequestUmtModule = "TS020100";
	//Original name: CF-RESPONSE-UMT-MODULE
	private String cfResponseUmtModule = "TS020200";
	//Original name: WS-MISC-WORK-FLDS
	private WsMiscWorkFldsCawi002 wsMiscWorkFlds = new WsMiscWorkFldsCawi002();
	//Original name: HALLURQA
	private Hallurqa hallurqa = new Hallurqa();
	//Original name: REQUEST-DATA-BUFFER
	private String requestDataBuffer = DefaultValues.stringVal(Len.REQUEST_DATA_BUFFER);
	//Original name: HALLRESP
	private Hallresp hallresp = new Hallresp();
	//Original name: RESPONSE-DATA-BUFFER
	private String responseDataBuffer = DefaultValues.stringVal(Len.RESPONSE_DATA_BUFFER);
	//Original name: TS020TBL
	private Ts020tbl ts020tbl = new Ts020tbl();
	//Original name: CAWLF002
	private Cawlf002 cawlf002 = new Cawlf002();
	//Original name: DCLLEGAL-ENTITY-COD-V
	private DcllegalEntityCodV dcllegalEntityCodV = new DcllegalEntityCodV();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public String getCfRequestUmtModule() {
		return this.cfRequestUmtModule;
	}

	public String getCfResponseUmtModule() {
		return this.cfResponseUmtModule;
	}

	public String getRequestUmtModuleDataFormatted() {
		return MarshalByteExt.bufferToStr(getRequestUmtModuleDataBytes());
	}

	/**Original name: REQUEST-UMT-MODULE-DATA<br>
	 * <pre>** LAYOUT USED TO PASS DATA TO FRAMEWORK SUPPLIED
	 * ** REQUEST UMT UPDATE MODULE.</pre>*/
	public byte[] getRequestUmtModuleDataBytes() {
		byte[] buffer = new byte[Len.REQUEST_UMT_MODULE_DATA];
		return getRequestUmtModuleDataBytes(buffer, 1);
	}

	public byte[] getRequestUmtModuleDataBytes(byte[] buffer, int offset) {
		int position = offset;
		hallurqa.getConstantsBytes(buffer, position);
		position += Hallurqa.Len.CONSTANTS;
		hallurqa.getInputLinkageBytes(buffer, position);
		position += Hallurqa.Len.INPUT_LINKAGE;
		hallurqa.getOutputLinkageBytes(buffer, position);
		position += Hallurqa.Len.OUTPUT_LINKAGE;
		hallurqa.getInputOutputLinkageBytes(buffer, position);
		position += Hallurqa.Len.INPUT_OUTPUT_LINKAGE;
		MarshalByte.writeString(buffer, position, requestDataBuffer, Len.REQUEST_DATA_BUFFER);
		return buffer;
	}

	public void setRequestDataBuffer(String requestDataBuffer) {
		this.requestDataBuffer = Functions.subString(requestDataBuffer, Len.REQUEST_DATA_BUFFER);
	}

	public String getRequestDataBuffer() {
		return this.requestDataBuffer;
	}

	public void setResponseUmtModuleDataFormatted(String data) {
		byte[] buffer = new byte[Len.RESPONSE_UMT_MODULE_DATA];
		MarshalByte.writeString(buffer, 1, data, Len.RESPONSE_UMT_MODULE_DATA);
		setResponseUmtModuleDataBytes(buffer, 1);
	}

	public String getResponseUmtModuleDataFormatted() {
		return MarshalByteExt.bufferToStr(getResponseUmtModuleDataBytes());
	}

	/**Original name: RESPONSE-UMT-MODULE-DATA<br>
	 * <pre>** LAYOUT USED TO PASS DATA TO FRAMEWORK SUPPLIED
	 * ** RESPONSE UMT READER MODULE.</pre>*/
	public byte[] getResponseUmtModuleDataBytes() {
		byte[] buffer = new byte[Len.RESPONSE_UMT_MODULE_DATA];
		return getResponseUmtModuleDataBytes(buffer, 1);
	}

	public void setResponseUmtModuleDataBytes(byte[] buffer, int offset) {
		int position = offset;
		hallresp.setConstantsBytes(buffer, position);
		position += Hallresp.Len.CONSTANTS;
		hallresp.setInputLinkageBytes(buffer, position);
		position += Hallresp.Len.INPUT_LINKAGE;
		hallresp.setOutputLinkageBytes(buffer, position);
		position += Hallresp.Len.OUTPUT_LINKAGE;
		hallresp.setInputOutputLinkageBytes(buffer, position);
		position += Hallresp.Len.INPUT_OUTPUT_LINKAGE;
		responseDataBuffer = MarshalByte.readString(buffer, position, Len.RESPONSE_DATA_BUFFER);
	}

	public byte[] getResponseUmtModuleDataBytes(byte[] buffer, int offset) {
		int position = offset;
		hallresp.getConstantsBytes(buffer, position);
		position += Hallresp.Len.CONSTANTS;
		hallresp.getInputLinkageBytes(buffer, position);
		position += Hallresp.Len.INPUT_LINKAGE;
		hallresp.getOutputLinkageBytes(buffer, position);
		position += Hallresp.Len.OUTPUT_LINKAGE;
		hallresp.getInputOutputLinkageBytes(buffer, position);
		position += Hallresp.Len.INPUT_OUTPUT_LINKAGE;
		MarshalByte.writeString(buffer, position, responseDataBuffer, Len.RESPONSE_DATA_BUFFER);
		return buffer;
	}

	public void setResponseDataBuffer(String responseDataBuffer) {
		this.responseDataBuffer = Functions.subString(responseDataBuffer, Len.RESPONSE_DATA_BUFFER);
	}

	public String getResponseDataBuffer() {
		return this.responseDataBuffer;
	}

	public String getResponseDataBufferFormatted() {
		return Functions.padBlanks(getResponseDataBuffer(), Len.RESPONSE_DATA_BUFFER);
	}

	public void setTableFormatterDataFormatted(String data) {
		byte[] buffer = new byte[Len.TABLE_FORMATTER_DATA];
		MarshalByte.writeString(buffer, 1, data, Len.TABLE_FORMATTER_DATA);
		setTableFormatterDataBytes(buffer, 1);
	}

	public String getTableFormatterDataFormatted() {
		return ts020tbl.getTableFormatterParmsFormatted();
	}

	public void setTableFormatterDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ts020tbl.setTableFormatterParmsBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public Cawlf002 getCawlf002() {
		return cawlf002;
	}

	public DcllegalEntityCodV getDcllegalEntityCodV() {
		return dcllegalEntityCodV;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallresp getHallresp() {
		return hallresp;
	}

	public Hallurqa getHallurqa() {
		return hallurqa;
	}

	public Ts020tbl getTs020tbl() {
		return ts020tbl;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsMiscWorkFldsCawi002 getWsMiscWorkFlds() {
		return wsMiscWorkFlds;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REQUEST_DATA_BUFFER = 5000;
		public static final int RESPONSE_DATA_BUFFER = 5000;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int TABLE_FORMATTER_DATA = Ts020tbl.Len.TABLE_FORMATTER_PARMS;
		public static final int REQUEST_UMT_MODULE_DATA = Hallurqa.Len.CONSTANTS + Hallurqa.Len.INPUT_LINKAGE + Hallurqa.Len.OUTPUT_LINKAGE
				+ Hallurqa.Len.INPUT_OUTPUT_LINKAGE + REQUEST_DATA_BUFFER;
		public static final int RESPONSE_UMT_MODULE_DATA = Hallresp.Len.CONSTANTS + Hallresp.Len.INPUT_LINKAGE + Hallresp.Len.OUTPUT_LINKAGE
				+ Hallresp.Len.INPUT_OUTPUT_LINKAGE + RESPONSE_DATA_BUFFER;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
