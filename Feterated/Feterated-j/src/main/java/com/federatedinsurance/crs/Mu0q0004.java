/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.ws.DfhcommareaTs020100;
import com.federatedinsurance.crs.ws.Mu0q0004Data;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.enums.WsOperationName;
import com.federatedinsurance.crs.ws.ptr.LFrameworkRequestArea;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: MU0Q0004<br>
 * <pre>AUTHOR.       ERIC LUND-DENN.
 * DATE-WRITTEN. 20 NOVEMBER 2007.
 * ***************************************************************
 *   PROGRAM TITLE - FRAMEWORK REQUEST MODULE FOR OPERATION      *
 *                     UOW        :GET_CLIENT_DETAIL             *
 *                                : GET_CLIENT_DETAIL_NO_SWAP    *
 *                     OPERATIONS :GetInsuredDetail              *
 *                                :GetInsuredNmAdrPhnTobDetail   *
 *                                                               *
 *   PLATFORM - HOST CICS                                        *
 *                                                               *
 *   PURPOSE -  CONTROL THE COMMUNICATION SHELL REQUEST PROCESS  *
 *                                                               *
 *   PROGRAM INITIATION - LINKED TO FROM A FRAMEWORK MAIN DRIVER *
 *                        PROGRAM(TS020000). NAME OF THE REQUEST *
 *                        MODULE IS PASSED AS A PARAMETER TO THE *
 *                        MAIN DRIVER.                           *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA (FOR   *
 *                         COMMON PARAMETERS) AND CICS MAIN      *
 *                         STORAGE (OPERATION SPECIFIC DATA)     *
 *                         OUTPUT TO REQUEST UMT AND DFHCOMMAREA *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    07JUL05   E404LJL   INITIAL TEMPLATE VERSION        *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *    yj249  20NOV07   E404EJD   INITIAL PROGRAM                 *
 * IM000554-02 5/5/09  E404CPM   CREATE CAMP OPERATION FOR       *
 *                               PERFORMANCE                     *
 * TO07609-01 05/06/09 E404CPM   ADDED NEW FIELDS FOR SIM PROJECT*
 *                               SE-SIC ARRAY, NBR of EMPS, and  *
 *                               FED_ACCOUNT_INFO ST,CTY,TOWN    *
 *  P02254-9 21DEC10   E404DLP   Removed call to get customer    *
 *                               service team information.       *
 *   17504    01/24/17 E404JAL   Return Seg Code.                *
 *                                                               *
 * ***************************************************************</pre>*/
public class Mu0q0004 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>***************************************************************
	 *  START OF:                                                    *
	 *      GENERAL BPO/COMM SHELL PROGRAM WORKING-STORAGE           *
	 *      (INCLUDED IN ALL BPOS/COMM SHELL PROGRAMS)               *
	 * ***************************************************************
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	@Inject
	private IPointerManager pointerManager;
	//Original name: WORKING-STORAGE
	private Mu0q0004Data ws = new Mu0q0004Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaTs020100 dfhcommarea;
	/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
	 * <pre>* FRAMEWORK REQUEST/INPUT LAYOUT FOR THIS OPERATION</pre>*/
	private LFrameworkRequestArea lFrameworkRequestArea = new LFrameworkRequestArea(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaTs020100 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Mu0q0004 getInstance() {
		return (Programs.getInstance(Mu0q0004.class));
	}

	/**Original name: 1000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-CREATE-OPERATION-REQ.
		createOperationReq();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PERFORM STARTUP/INITIALIZATION PROCESSING                    *
	 * ***************************************************************
	 *  INITIALIZE ERROR PROCESSING FIELDS</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE ESTO-STORE-INFO
		//                      ESTO-RETURN-INFO.
		initEstoStoreInfo();
		initEstoReturnInfo();
		// COB_CODE: PERFORM 2100-DETERMINE-OPERATION.
		determineOperation();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// CREATE NECESSARY UOW SWITCHES FOR EACH OPERATION
		// COB_CODE: PERFORM 2200-PARSE-REQUEST-SWITCHES.
		parseRequestSwitches();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// CREATE NECESSARY UOW FILTERS FOR EACH OPERATION
		// COB_CODE: PERFORM 2300-CREATE-REQUEST-FILTERS.
		createRequestFilters();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-DETERMINE-OPERATION_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  VALIDATE THAT THIS MODULE CAN BE CALLED FOR THIS OPERATION.  *
	 *  IF NOT, RAISE A LOGGABLE ERROR.                              *
	 * ***************************************************************</pre>*/
	private void determineOperation() {
		// COB_CODE: MOVE CSC-OPERATION          TO WS-OPERATION-NAME.
		ws.getWsMiscWorkFlds().getOperationName().setWsOperationName(dfhcommarea.getCommunicationShellCommon().getCscGeneralParms().getOperation());
		// COB_CODE: IF WS-VALID-OPERATION
		//               GO TO 2100-EXIT
		//           END-IF.
		if (ws.getWsMiscWorkFlds().getOperationName().isValidOperation()) {
			// COB_CODE: SET ADDRESS OF L-FRAMEWORK-REQUEST-AREA
			//                                   TO MA-INPUT-POINTER
			lFrameworkRequestArea = ((pointerManager.resolve(dfhcommarea.getCommunicationShellCommon().getMaInputPointer(),
					LFrameworkRequestArea.class)));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: MOVE CF-INVALID-OPERATION   TO EFAL-ACTION-BUFFER.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalActionBuffer(ws.getConstantFields().getInvalidOperation().getInvalidOperationFormatted());
		// COB_CODE: MOVE WS-PROGRAM-NAME        TO EFAL-ERR-OBJECT-NAME
		//                                          CSC-FAILED-MODULE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsMiscWorkFlds().getProgramName());
		dfhcommarea.getCommunicationShellCommon().setCscFailedModule(ws.getWsMiscWorkFlds().getProgramName());
		// COB_CODE: MOVE '2100-DETERMINE-OPERATION'
		//                                       TO EFAL-ERR-PARAGRAPH
		//                                          CSC-FAILED-PARAGRAPH
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-DETERMINE-OPERATION");
		dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("2100-DETERMINE-OPERATION");
		// COB_CODE: MOVE WS-OPERATION-NAME      TO WS-INVALID-OPERATION-NAME.
		ws.getWsMiscWorkFlds().getInvalidOperationMsg().setWsInvalidOperationName(ws.getWsMiscWorkFlds().getOperationName().getWsOperationName());
		// COB_CODE: MOVE WS-INVALID-OPERATION-MSG
		//                                       TO EFAL-ERR-COMMENT
		//                                          EFAL-OBJ-DATA-KEY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getWsMiscWorkFlds().getInvalidOperationMsg().getInvalidOperationMsgFormatted());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getWsMiscWorkFlds().getInvalidOperationMsg().getInvalidOperationMsgFormatted());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 2200-PARSE-REQUEST-SWITCHES_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  SET SWITCHES FOR BUSINESS DATA OBJECTS                       *
	 *  TO CUSTOMIZE FOR EACH OPERATION:                             *
	 *     1) ADD NEEDED 88 LEVELS TO WS-BUSINESS-OBJECT-SWITCH.     *
	 *     2) ADD OPERATION TO EVULATE STATEMENT.                    *
	 *     3) SET EACH BUSINESS OBJECT SWITCH NEEDED TO TRUE AND     *
	 *        AND CALL 2210-WRITE-TO-SWITCH-UMT AFTER EACH SWITCH    *
	 *        IS SET.                                                *
	 * ***************************************************************</pre>*/
	private void parseRequestSwitches() {
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN WS-GET-INSURED-DETAIL
		//                        PERFORM 2210-WRITE-TO-SWITCH-UMT
		//                    WHEN WS-GET-INSD-NAPT-DETAIL
		//                        PERFORM 2210-WRITE-TO-SWITCH-UMT
		//                    WHEN OTHER
		//           *            DEFAULT TO "RETURN ALL" SINCE OPERATION ALREADY
		//           *            DETERMINED TO BE VALID
		//                        PERFORM 2210-WRITE-TO-SWITCH-UMT
		//                END-EVALUATE.
		switch (ws.getWsMiscWorkFlds().getOperationName().getWsOperationName()) {

		case WsOperationName.GET_INSURED_DETAIL:// COB_CODE: SET WS-BUS-OBJ-SW-RETURN-ALL
			//                               TO TRUE
			ws.getWsMiscWorkFlds().getBusinessObjectSwitch().setReturnAll();
			// COB_CODE: PERFORM 2210-WRITE-TO-SWITCH-UMT
			writeToSwitchUmt();
			break;

		case WsOperationName.GET_INSD_NAPT_DETAIL:// COB_CODE: SET WS-BUS-OBJ-SW-CLIENT-TAB
			//                               TO TRUE
			ws.getWsMiscWorkFlds().getBusinessObjectSwitch().setClientTab();
			// COB_CODE: PERFORM 2210-WRITE-TO-SWITCH-UMT
			writeToSwitchUmt();
			// COB_CODE: SET WS-BUS-OBJ-SW-ADR-BEST-BSM
			//                               TO TRUE
			ws.getWsMiscWorkFlds().getBusinessObjectSwitch().setAdrBestBsm();
			// COB_CODE: PERFORM 2210-WRITE-TO-SWITCH-UMT
			writeToSwitchUmt();
			// COB_CODE: SET WS-BUS-OBJ-SW-ADR-BEST-BSL
			//                               TO TRUE
			ws.getWsMiscWorkFlds().getBusinessObjectSwitch().setAdrBestBsl();
			// COB_CODE: PERFORM 2210-WRITE-TO-SWITCH-UMT
			writeToSwitchUmt();
			// COB_CODE: SET WS-BUS-OBJ-SW-FED-BUS-TYP
			//                               TO TRUE
			ws.getWsMiscWorkFlds().getBusinessObjectSwitch().setFedBusTyp();
			// COB_CODE: PERFORM 2210-WRITE-TO-SWITCH-UMT
			writeToSwitchUmt();
			// COB_CODE: SET WS-BUS-OBJ-SW-CLIENT-PHONE
			//                               TO TRUE
			ws.getWsMiscWorkFlds().getBusinessObjectSwitch().setClientPhone();
			// COB_CODE: PERFORM 2210-WRITE-TO-SWITCH-UMT
			writeToSwitchUmt();
			//            RETRIEVED AS A CHILD OF CLIENT_TAB BY THE
			//            CLIENT_TAB BDO CAWD002
			// COB_CODE: SET WS-BUS-OBJ-SW-FED-SEG-INFO
			//                               TO TRUE
			ws.getWsMiscWorkFlds().getBusinessObjectSwitch().setFedSegInfo();
			// COB_CODE: PERFORM 2210-WRITE-TO-SWITCH-UMT
			writeToSwitchUmt();
			break;

		default://            DEFAULT TO "RETURN ALL" SINCE OPERATION ALREADY
			//            DETERMINED TO BE VALID
			// COB_CODE: SET WS-BUS-OBJ-SW-RETURN-ALL
			//                               TO TRUE
			ws.getWsMiscWorkFlds().getBusinessObjectSwitch().setReturnAll();
			// COB_CODE: PERFORM 2210-WRITE-TO-SWITCH-UMT
			writeToSwitchUmt();
			break;
		}
	}

	/**Original name: 2210-WRITE-TO-SWITCH-UMT_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  WRITE OUT INFORMATION IN SWITCH UMT FOR GIVEN BUSINESS OBJECT*
	 * ***************************************************************</pre>*/
	private void writeToSwitchUmt() {
		TpOutputData tsQueueData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID            TO USW-ID.
		ws.getHallusw().setId(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		// COB_CODE: MOVE WS-BUSINESS-OBJECT-SWITCH
		//                                       TO USW-BUS-OBJ-SWITCH.
		ws.getHallusw().setBusObjSwitch(ws.getWsMiscWorkFlds().getBusinessObjectSwitch().getBusinessObjectSwitch());
		// COB_CODE: EXEC CICS
		//               WRITEQ TS MAIN
		//               QNAME(CSC-REQ-SWITCHES-TSQ)
		//               FROM (WS-USW-MSG)
		//               RESP (WS-RESPONSE-CODE)
		//               RESP2 (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(Mu0q0004Data.Len.WS_USW_MSG);
		tsQueueData.setData(ws.getWsUswMsgBytes());
		TsQueueManager.insert(execContext, dfhcommarea.getCommunicationShellCommon().getCscGeneralParms().getCscReqSwitchesTsqFormatted(),
				tsQueueData);
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   GO TO 2210-EXIT
		//               WHEN OTHER
		//                   GO TO 2210-EXIT
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: GO TO 2210-EXIT
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-TSQ
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteTsq();
			// COB_CODE: MOVE CSC-REQ-SWITCHES-TSQ
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrObjectName(dfhcommarea.getCommunicationShellCommon().getCscGeneralParms().getReqSwitchesTsq());
			// COB_CODE: MOVE WS-PROGRAM-NAME
			//                               TO CSC-FAILED-MODULE
			dfhcommarea.getCommunicationShellCommon().setCscFailedModule(ws.getWsMiscWorkFlds().getProgramName());
			// COB_CODE: MOVE '2210-WRITE-TO-SWITCH-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2210-WRITE-TO-SWITCH-UMT");
			dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("2210-WRITE-TO-SWITCH-UMT");
			// COB_CODE: MOVE 'WRITE REQUEST SWITCHES TSQ FAILED IN TS020100'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE REQUEST SWITCHES TSQ FAILED IN TS020100");
			// COB_CODE: IF WS-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			dfhcommarea.getCommunicationShellCommon().setCscEibrespDisplay(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: IF WS-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			dfhcommarea.getCommunicationShellCommon().setCscEibresp2Display(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: STRING 'UBOC-MSG-ID='  UBOC-MSG-ID  ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UBOC-MSG-ID=",
					dfhcommarea.getUbocCommInfo().getUbocMsgIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2210-EXIT
			return;
		}
	}

	/**Original name: 2300-CREATE-REQUEST-FILTERS_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  SET FILTERS TO CUSTOMIZE DATA RETURNED FROM BUSINESS OBJECTS *
	 *  TO CUSTOMIZE FOR EACH OPERATION:                             *
	 *     1) ADD OPERATION TO EVULATE STATEMENT.                    *
	 *     2) MOVE FILTER CRITERIA TO CORRECT FRAMEWORK              *
	 *        REQUEST/INPUT LAYOUT COPYBOOK.                         *
	 *     3) CALL CORRECT TABLE FORMATTER PARAGRAPH (92?0-...)      *
	 * ***************************************************************</pre>*/
	private void createRequestFilters() {
		// COB_CODE: SET WS-FILTER-ACTION        TO TRUE.
		ws.getWsMiscWorkFlds().getActionCode().setFilterAction();
		// COB_CODE: EVALUATE TRUE
		//               WHEN WS-GET-INSURED-DETAIL
		//                   END-IF
		//           END-EVALUATE.
		switch (ws.getWsMiscWorkFlds().getOperationName().getWsOperationName()) {

		case WsOperationName.GET_INSURED_DETAIL:// COB_CODE: INITIALIZE CW06F-CLT-CLT-RELATION-ROW
			initCltCltRelationRow();
			// COB_CODE: MOVE CF-COMPANY     TO CW06F-CLT-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCltTypCd(ws.getConstantFields().getCompany());
			// COB_CODE: MOVE CF-TERRITORY   TO CW06F-XRF-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setXrfTypCd(ws.getConstantFields().getTerritory());
			// COB_CODE: PERFORM 9230-WRT-CLT-CLT-REL-RQ
			wrtCltCltRelRq();
			// COB_CODE: MOVE CF-INDIVIDUAL  TO CW06F-CLT-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCltTypCd(ws.getConstantFields().getIndividual());
			// COB_CODE: MOVE CF-TERRITORY   TO CW06F-XRF-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setXrfTypCd(ws.getConstantFields().getTerritory());
			// COB_CODE: PERFORM 9230-WRT-CLT-CLT-REL-RQ
			wrtCltCltRelRq();
			// COB_CODE: MOVE CF-COMPANY     TO CW06F-CLT-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCltTypCd(ws.getConstantFields().getCompany());
			// COB_CODE: MOVE CF-UNDERWRITER TO CW06F-XRF-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setXrfTypCd(ws.getConstantFields().getUnderwriter());
			// COB_CODE: PERFORM 9230-WRT-CLT-CLT-REL-RQ
			wrtCltCltRelRq();
			// COB_CODE: MOVE CF-COMPANY     TO CW06F-CLT-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCltTypCd(ws.getConstantFields().getCompany());
			// COB_CODE: MOVE CF-RSKA        TO CW06F-XRF-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setXrfTypCd(ws.getConstantFields().getRska());
			// COB_CODE: PERFORM 9230-WRT-CLT-CLT-REL-RQ
			wrtCltCltRelRq();
			// COB_CODE: MOVE CF-COMPANY     TO CW06F-CLT-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCltTypCd(ws.getConstantFields().getCompany());
			// COB_CODE: MOVE CF-FPU         TO CW06F-XRF-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setXrfTypCd(ws.getConstantFields().getFpu());
			// COB_CODE: PERFORM 9230-WRT-CLT-CLT-REL-RQ
			wrtCltCltRelRq();
			// COB_CODE: MOVE CF-COMPANY     TO CW06F-CLT-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCltTypCd(ws.getConstantFields().getCompany());
			// COB_CODE: MOVE CF-LPRA        TO CW06F-XRF-TYP-CD
			ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setXrfTypCd(ws.getConstantFields().getLpra());
			// COB_CODE: PERFORM 9230-WRT-CLT-CLT-REL-RQ
			wrtCltCltRelRq();
			// COB_CODE: INITIALIZE WS-FC-CLT-OBJ-RELATION
			initWsFcCltObjRelation();
			// COB_CODE: IF CW08Q-CIOR-SHW-OBJ-KEY NOT = SPACES
			//               PERFORM 9250-WRT-CLT-OBJ-RELATION-RQ
			//           END-IF
			if (!Characters.EQ_SPACE.test(lFrameworkRequestArea.getCw08qCiorShwObjKey())) {
				// COB_CODE: MOVE CW08Q-CIOR-SHW-OBJ-KEY
				//                           TO CW08F-CIOR-SHW-OBJ-KEY
				ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCiorShwObjKey(lFrameworkRequestArea.getCw08qCiorShwObjKey());
				// COB_CODE: MOVE CF-OWNER   TO CW08F-RLT-TYP-CD
				ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setRltTypCd(ws.getConstantFields().getOwner());
				// COB_CODE: PERFORM 9250-WRT-CLT-OBJ-RELATION-RQ
				wrtCltObjRelationRq();
			}
			// COB_CODE: INITIALIZE WS-FC-CLT-ADDR-COMPOSITE
			initWsFcCltAddrComposite();
			// COB_CODE: IF CW08Q-CIOR-SHW-OBJ-KEY NOT = SPACES
			//               PERFORM 9240-WRT-CLT-ADDR-COMPOSITE-RQ
			//           END-IF
			if (!Characters.EQ_SPACE.test(lFrameworkRequestArea.getCw08qCiorShwObjKey())) {
				// COB_CODE: MOVE CW08Q-CIOR-SHW-OBJ-KEY
				//                           TO CWCACF-CICA-SHW-OBJ-KEY
				ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData()
						.setCicaShwObjKey(lFrameworkRequestArea.getCw08qCiorShwObjKey());
				// COB_CODE: PERFORM 9240-WRT-CLT-ADDR-COMPOSITE-RQ
				wrtCltAddrCompositeRq();
			}
			break;

		default:
			break;
		}
	}

	/**Original name: 3000-CREATE-OPERATION-REQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CREATE FRAMEWORK REQUEST ROWS ON REQUEST UMT                   *
	 *  A SEPARATE SECTION IS INCLUDED FOR EACH ROW CREATED            *
	 * *****************************************************************</pre>*/
	private void createOperationReq() {
		// COB_CODE: PERFORM 3100-GET-CLIENT-DETAIL.
		getClientDetail();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3200-GET-RELATED-COWN-CLT.
		getRelatedCownClt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3100-GET-CLIENT-DETAIL_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DETERMINE WHAT TABLE FORMATTER TO LINK TO BASED ON INPUT       *
	 *  VALUES FOUND IN CAWLQ002 AND CAWLQGIK.                         *
	 * *****************************************************************</pre>*/
	private void getClientDetail() {
		// COB_CODE: SET WS-FETCH-ACTION         TO TRUE.
		ws.getWsMiscWorkFlds().getActionCode().setFetchAction();
		//*****************************************************************
		// IF A CLIENT ID IS PASSED, WRITE TO CLIENT TAB.  OTHERWISE
		// WRITE TO CLIENT INQUIRE KEY BPO.
		//*****************************************************************
		// COB_CODE:      IF CW02Q-CLIENT-ID NOT = SPACES
		//                    PERFORM 9210-WRT-CLIENT-TAB-RQ
		//                ELSE
		//           ******************************************************************
		//           *  WILL NEED TO DETERMINE WHAT INPUT PARMS WERE PASSED AND WHAT
		//           *  OPERATION WE ARE RUNNING
		//           ******************************************************************
		//                    PERFORM 9220-WRT-CLT-INQ-KEY-RQ
		//                END-IF.
		if (!Characters.EQ_SPACE.test(lFrameworkRequestArea.getCw02qClientId())) {
			// COB_CODE: PERFORM 9210-WRT-CLIENT-TAB-RQ
			wrtClientTabRq();
		} else {
			//*****************************************************************
			//  WILL NEED TO DETERMINE WHAT INPUT PARMS WERE PASSED AND WHAT
			//  OPERATION WE ARE RUNNING
			//*****************************************************************
			// COB_CODE: IF CWGIKQ-SHW-OBJ-KEY NOT = SPACES
			//               END-EVALUATE
			//           END-IF
			if (!Characters.EQ_SPACE.test(lFrameworkRequestArea.getCwgikqShwObjKey())) {
				// COB_CODE:              EVALUATE TRUE
				//                            WHEN WS-GET-INSURED-DETAIL
				//                                            TO CWGIKQ-RLT-TYP-CD
				//                            WHEN OTHER
				//           *******          WILL DEFAULT TO ALWAYS LOOK FOR ACCOUNT OWNER
				//                                            TO CWGIKQ-RLT-TYP-CD
				//                        END-EVALUATE
				switch (ws.getWsMiscWorkFlds().getOperationName().getWsOperationName()) {

				case WsOperationName.GET_INSURED_DETAIL:// COB_CODE: MOVE CF-OWNER
					//                       TO CWGIKQ-RLT-TYP-CD
					lFrameworkRequestArea.setCwgikqRltTypCd(ws.getConstantFields().getOwner());
					break;

				default://******          WILL DEFAULT TO ALWAYS LOOK FOR ACCOUNT OWNER
					// COB_CODE: MOVE CF-OWNER
					//                       TO CWGIKQ-RLT-TYP-CD
					lFrameworkRequestArea.setCwgikqRltTypCd(ws.getConstantFields().getOwner());
					break;
				}
			}
			//**  ADD IF STATEMENTS FOR OTHER INPUT PARMS HERE
			// COB_CODE: PERFORM 9220-WRT-CLT-INQ-KEY-RQ
			wrtCltInqKeyRq();
		}
	}

	/**Original name: 3200-GET-RELATED-COWN-CLT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  WRITE OUT A REQUEST UMT ROW FOR CLT_OBJECT_RELATED_CLIENT_BPO  *
	 * *****************************************************************</pre>*/
	private void getRelatedCownClt() {
		Halrurqa halrurqa = null;
		// COB_CODE: SET HALRURQA-WRITE-FUNC     TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE CF-BO-REL-CLT-OBJ-REL-BPO
		//                                       TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getConstantFields().getBoRelCltObjRelBpo());
		// COB_CODE: MOVE 'FETCH'                TO HALRURQA-ACTION-CODE.
		ws.getWsHalrurqaLinkage().setActionCode("FETCH");
		// COB_CODE: MOVE LENGTH OF CWORC-CLT-OBJ-RELATION-ROW
		//                                       TO HALRURQA-BUS-OBJ-DATA-LENGTH.
		ws.getWsHalrurqaLinkage().setBusObjDataLength(((short) LFrameworkRequestArea.Len.CWORC_CLT_OBJ_RELATION_ROW));
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                CWORC-CLT-OBJ-RELATION-ROW.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(),
				new BasicBytesClass(lFrameworkRequestArea.getArray(), LFrameworkRequestArea.Pos.CWORC_CLT_OBJ_RELATION_ROW - 1));
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3200-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3200-EXIT
			return;
		}
	}

	/**Original name: 9100-PROGRAM-LINK-FAILED_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PERFORM COMMON LOGIC TO LOG AN ERROR ON A CICS LINK          *
	 * ***************************************************************</pre>*/
	private void programLinkFailed() {
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-CICS-FAILED        TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
		// COB_CODE: SET ETRA-CICS-LINK          TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
		// COB_CODE: MOVE WS-PROGRAM-NAME        TO EFAL-ERR-OBJECT-NAME
		//                                          CSC-FAILED-MODULE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsMiscWorkFlds().getProgramName());
		dfhcommarea.getCommunicationShellCommon().setCscFailedModule(ws.getWsMiscWorkFlds().getProgramName());
		// COB_CODE: MOVE WS-PROGRAM-LINK-FAILED TO EFAL-ERR-COMMENT.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getWsMiscWorkFlds().getProgramLinkFailed().getProgramLinkFailedFormatted());
		// COB_CODE: IF WS-RESPONSE-CODE < ZERO
		//               MOVE '-'                TO EFAL-CICS-ERR-RESP-SIGN
		//           ELSE
		//               MOVE '+'                TO EFAL-CICS-ERR-RESP-SIGN
		//           END-IF.
		if (ws.getWsNotSpecificMisc().getResponseCode() < 0) {
			// COB_CODE: MOVE '-'                TO EFAL-CICS-ERR-RESP-SIGN
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+'                TO EFAL-CICS-ERR-RESP-SIGN
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		}
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EFAL-CICS-ERR-RESP
		//                                          CSC-EIBRESP-DISPLAY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
		dfhcommarea.getCommunicationShellCommon().setCscEibrespDisplay(ws.getWsNotSpecificMisc().getResponseCode());
		// COB_CODE: IF WS-RESPONSE-CODE2 < ZERO
		//               MOVE '-'                TO EFAL-CICS-ERR-RESP2-SIGN
		//           ELSE
		//               MOVE '+'                TO EFAL-CICS-ERR-RESP2-SIGN
		//           END-IF.
		if (ws.getWsNotSpecificMisc().getResponseCode2() < 0) {
			// COB_CODE: MOVE '-'                TO EFAL-CICS-ERR-RESP2-SIGN
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
		} else {
			// COB_CODE: MOVE '+'                TO EFAL-CICS-ERR-RESP2-SIGN
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		}
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EFAL-CICS-ERR-RESP2
		//                                          CSC-EIBRESP2-DISPLAY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
		dfhcommarea.getCommunicationShellCommon().setCscEibresp2Display(ws.getWsNotSpecificMisc().getResponseCode2());
		// COB_CODE: MOVE WS-PROGRAM-LINK-FAILED TO EFAL-OBJ-DATA-KEY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getWsMiscWorkFlds().getProgramLinkFailed().getProgramLinkFailedFormatted());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 9210-WRT-CLIENT-TAB-RQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LINK TO TABLE FORMATTER MODULE TO INSERT REQUEST UMT ROW FOR   *
	 *  CLIENT_TAB_V                                                   *
	 * *****************************************************************</pre>*/
	private void wrtClientTabRq() {
		// COB_CODE: INITIALIZE TABLE-FORMATTER-DATA.
		initTableFormatterData();
		// COB_CODE: MOVE WS-ACTION-CODE         TO TF-ACTION-CODE.
		ws.getTs020tbl().getTfActionCode().setTfActionCode(ws.getWsMiscWorkFlds().getActionCode().getActionCode());
		// COB_CODE: SET TF-REQUEST-FORMATTER-CALL
		//                                       TO TRUE.
		ws.getTs020tbl().getTfRequestResponseFlag().setTfRequestFormatterCall();
		// COB_CODE: MOVE CW02Q-CLIENT-TAB-ROW   TO TF-DATA-BUFFER.
		ws.getTs020tbl().setTfDataBuffer(lFrameworkRequestArea.getCw02qClientTabRowFormatted());
		// COB_CODE: MOVE LENGTH OF TABLE-FORMATTER-DATA
		//                                       TO CSC-DATA-BUFFER-LENGTH.
		dfhcommarea.getCommunicationShellCommon().setCscDataBufferLength(((short) Mu0q0004Data.Len.TABLE_FORMATTER_DATA));
		// COB_CODE: MOVE TABLE-FORMATTER-DATA   TO CSC-DATA-BUFFER.
		dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getTableFormatterDataFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CF-TF-CLIENT-TAB-FMT)
		//               COMMAREA   (DFHCOMMAREA)
		//               DATALENGTH (LENGTH OF DFHCOMMAREA)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("MU0Q0004", execContext).commarea(dfhcommarea).length(DfhcommareaTs020100.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getTableFormatterModules().getClientTabFmt(), new Cawi002());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* CHECKS THE SUCCESS OF THE LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP (NORMAL)
		//               GO TO 9210-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE '9210-WRT-CLIENT-TAB-RQ'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9210-WRT-CLIENT-TAB-RQ");
			dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("9210-WRT-CLIENT-TAB-RQ");
			// COB_CODE: MOVE CF-TF-CLIENT-TAB-FMT
			//                                   TO WS-FAILED-LINK-PGM-NAME
			ws.getWsMiscWorkFlds().getProgramLinkFailed().setWsFailedLinkPgmName(ws.getConstantFields().getTableFormatterModules().getClientTabFmt());
			// COB_CODE: PERFORM 9100-PROGRAM-LINK-FAILED
			programLinkFailed();
			// COB_CODE: GO TO 9210-EXIT
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9210-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9210-EXIT
			return;
		}
	}

	/**Original name: 9220-WRT-CLT-INQ-KEY-RQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LINK TO TABLE FORMATTER MODULE TO INSERT REQUEST UMT ROW FOR   *
	 *  CLT_GET_INQUIRE_KEY                                            *
	 * *****************************************************************</pre>*/
	private void wrtCltInqKeyRq() {
		// COB_CODE: INITIALIZE TABLE-FORMATTER-DATA.
		initTableFormatterData();
		// COB_CODE: MOVE WS-ACTION-CODE         TO TF-ACTION-CODE.
		ws.getTs020tbl().getTfActionCode().setTfActionCode(ws.getWsMiscWorkFlds().getActionCode().getActionCode());
		// COB_CODE: SET TF-REQUEST-FORMATTER-CALL
		//                                       TO TRUE.
		ws.getTs020tbl().getTfRequestResponseFlag().setTfRequestFormatterCall();
		// COB_CODE: MOVE CWGIKQ-INQUIRE-KEY-ROW TO TF-DATA-BUFFER.
		ws.getTs020tbl().setTfDataBuffer(lFrameworkRequestArea.getCwgikqInquireKeyRowFormatted());
		// COB_CODE: MOVE LENGTH OF TABLE-FORMATTER-DATA
		//                                       TO CSC-DATA-BUFFER-LENGTH.
		dfhcommarea.getCommunicationShellCommon().setCscDataBufferLength(((short) Mu0q0004Data.Len.TABLE_FORMATTER_DATA));
		// COB_CODE: MOVE TABLE-FORMATTER-DATA   TO CSC-DATA-BUFFER.
		dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getTableFormatterDataFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CF-TF-CLT-INQUIRE-KEY-FMT)
		//               COMMAREA   (DFHCOMMAREA)
		//               DATALENGTH (LENGTH OF DFHCOMMAREA)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("MU0Q0004", execContext).commarea(dfhcommarea).length(DfhcommareaTs020100.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getTableFormatterModules().getCltInquireKeyFmt(), new Cawigik());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* CHECKS THE SUCCESS OF THE LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP (NORMAL)
		//               GO TO 9220-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE '9220-WRT-CLT-INQ-KEY-RQ'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9220-WRT-CLT-INQ-KEY-RQ");
			dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("9220-WRT-CLT-INQ-KEY-RQ");
			// COB_CODE: MOVE CF-TF-CLT-INQUIRE-KEY-FMT
			//                                   TO WS-FAILED-LINK-PGM-NAME
			ws.getWsMiscWorkFlds().getProgramLinkFailed()
					.setWsFailedLinkPgmName(ws.getConstantFields().getTableFormatterModules().getCltInquireKeyFmt());
			// COB_CODE: PERFORM 9100-PROGRAM-LINK-FAILED
			programLinkFailed();
			// COB_CODE: GO TO 9220-EXIT
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9220-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9220-EXIT
			return;
		}
	}

	/**Original name: 9230-WRT-CLT-CLT-REL-RQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LINK TO TABLE FORMATTER MODULE TO INSERT FILTER REQUEST UMT    *
	 *  ROW FOR CLT_CLT_RELATION_V                                     *
	 * *****************************************************************</pre>*/
	private void wrtCltCltRelRq() {
		// COB_CODE: INITIALIZE TABLE-FORMATTER-DATA.
		initTableFormatterData();
		// COB_CODE: MOVE WS-ACTION-CODE         TO TF-ACTION-CODE.
		ws.getTs020tbl().getTfActionCode().setTfActionCode(ws.getWsMiscWorkFlds().getActionCode().getActionCode());
		// COB_CODE: SET TF-REQUEST-FORMATTER-CALL
		//                                       TO TRUE.
		ws.getTs020tbl().getTfRequestResponseFlag().setTfRequestFormatterCall();
		// COB_CODE: MOVE CW06F-CLT-CLT-RELATION-ROW
		//                                       TO TF-DATA-BUFFER.
		ws.getTs020tbl().setTfDataBuffer(ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationRowFormatted());
		// COB_CODE: MOVE LENGTH OF TABLE-FORMATTER-DATA
		//                                       TO CSC-DATA-BUFFER-LENGTH.
		dfhcommarea.getCommunicationShellCommon().setCscDataBufferLength(((short) Mu0q0004Data.Len.TABLE_FORMATTER_DATA));
		// COB_CODE: MOVE TABLE-FORMATTER-DATA   TO CSC-DATA-BUFFER.
		dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getTableFormatterDataFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CF-TF-CLT-CLT-REL-FMT)
		//               COMMAREA   (DFHCOMMAREA)
		//               DATALENGTH (LENGTH OF DFHCOMMAREA)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("MU0Q0004", execContext).commarea(dfhcommarea).length(DfhcommareaTs020100.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getTableFormatterModules().getCltCltRelFmt(), new Cawi006());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* CHECKS THE SUCCESS OF THE LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP (NORMAL)
		//               GO TO 9230-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE '9230-WRT-CLT-CLT-REL-RQ'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9230-WRT-CLT-CLT-REL-RQ");
			dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("9230-WRT-CLT-CLT-REL-RQ");
			// COB_CODE: MOVE CF-TF-CLT-CLT-REL-FMT
			//                                   TO WS-FAILED-LINK-PGM-NAME
			ws.getWsMiscWorkFlds().getProgramLinkFailed().setWsFailedLinkPgmName(ws.getConstantFields().getTableFormatterModules().getCltCltRelFmt());
			// COB_CODE: PERFORM 9100-PROGRAM-LINK-FAILED
			programLinkFailed();
			// COB_CODE: GO TO 9230-EXIT
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9230-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9230-EXIT
			return;
		}
	}

	/**Original name: 9240-WRT-CLT-ADDR-COMPOSITE-RQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LINK TO TABLE FORMATTER MODULE TO INSERT FILTER REQUEST UMT    *
	 *  ROW FOR CLT_ADR_COMPOSITE                                      *
	 * *****************************************************************</pre>*/
	private void wrtCltAddrCompositeRq() {
		// COB_CODE: INITIALIZE TABLE-FORMATTER-DATA.
		initTableFormatterData();
		// COB_CODE: MOVE WS-ACTION-CODE         TO TF-ACTION-CODE.
		ws.getTs020tbl().getTfActionCode().setTfActionCode(ws.getWsMiscWorkFlds().getActionCode().getActionCode());
		// COB_CODE: SET TF-REQUEST-FORMATTER-CALL
		//                                       TO TRUE.
		ws.getTs020tbl().getTfRequestResponseFlag().setTfRequestFormatterCall();
		// COB_CODE: MOVE CWCACF-CLIENT-ADDR-COMPOSITE
		//                                       TO TF-DATA-BUFFER.
		ws.getTs020tbl().setTfDataBuffer(ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCwcacfClientAddrCompositeFormatted());
		// COB_CODE: MOVE LENGTH OF TABLE-FORMATTER-DATA
		//                                       TO CSC-DATA-BUFFER-LENGTH.
		dfhcommarea.getCommunicationShellCommon().setCscDataBufferLength(((short) Mu0q0004Data.Len.TABLE_FORMATTER_DATA));
		// COB_CODE: MOVE TABLE-FORMATTER-DATA   TO CSC-DATA-BUFFER.
		dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getTableFormatterDataFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CF-TF-CLT-ADDR-COMPOSITE-FMT)
		//               COMMAREA   (DFHCOMMAREA)
		//               DATALENGTH (LENGTH OF DFHCOMMAREA)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("MU0Q0004", execContext).commarea(dfhcommarea).length(DfhcommareaTs020100.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getTableFormatterModules().getCltAddrCompositeFmt(), new Cawicac());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* CHECKS THE SUCCESS OF THE LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP (NORMAL)
		//               GO TO 9240-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE '9240-WRT-CLT-ADDR-COMPOSITE-RQ'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9240-WRT-CLT-ADDR-COMPOSITE-RQ");
			dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("9240-WRT-CLT-ADDR-COMPOSITE-RQ");
			// COB_CODE: MOVE CF-TF-CLT-ADDR-COMPOSITE-FMT
			//                                   TO WS-FAILED-LINK-PGM-NAME
			ws.getWsMiscWorkFlds().getProgramLinkFailed()
					.setWsFailedLinkPgmName(ws.getConstantFields().getTableFormatterModules().getCltAddrCompositeFmt());
			// COB_CODE: PERFORM 9100-PROGRAM-LINK-FAILED
			programLinkFailed();
			// COB_CODE: GO TO 9240-EXIT
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9240-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9240-EXIT
			return;
		}
	}

	/**Original name: 9250-WRT-CLT-OBJ-RELATION-RQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LINK TO TABLE FORMATTER MODULE TO INSERT FILTER REQUEST UMT    *
	 *  ROW FOR CLT_OBJ_RELATION                                       *
	 * *****************************************************************</pre>*/
	private void wrtCltObjRelationRq() {
		// COB_CODE: INITIALIZE TABLE-FORMATTER-DATA.
		initTableFormatterData();
		// COB_CODE: MOVE WS-ACTION-CODE         TO TF-ACTION-CODE.
		ws.getTs020tbl().getTfActionCode().setTfActionCode(ws.getWsMiscWorkFlds().getActionCode().getActionCode());
		// COB_CODE: SET TF-REQUEST-FORMATTER-CALL
		//                                       TO TRUE.
		ws.getTs020tbl().getTfRequestResponseFlag().setTfRequestFormatterCall();
		// COB_CODE: IF WS-FILTER-ACTION
		//                                       TO TF-DATA-BUFFER
		//           ELSE
		//                                       TO TF-DATA-BUFFER
		//           END-IF.
		if (ws.getWsMiscWorkFlds().getActionCode().isFilterAction()) {
			// COB_CODE: MOVE CW08F-CLT-OBJ-RELATION-ROW
			//                                   TO TF-DATA-BUFFER
			ws.getTs020tbl().setTfDataBuffer(ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationRowFormatted());
		} else {
			// COB_CODE: MOVE CW08Q-CLT-OBJ-RELATION-ROW
			//                                   TO TF-DATA-BUFFER
			ws.getTs020tbl().setTfDataBuffer(lFrameworkRequestArea.getCw08qCltObjRelationRowFormatted());
		}
		// COB_CODE: MOVE LENGTH OF TABLE-FORMATTER-DATA
		//                                       TO CSC-DATA-BUFFER-LENGTH.
		dfhcommarea.getCommunicationShellCommon().setCscDataBufferLength(((short) Mu0q0004Data.Len.TABLE_FORMATTER_DATA));
		// COB_CODE: MOVE TABLE-FORMATTER-DATA   TO CSC-DATA-BUFFER.
		dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getTableFormatterDataFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CF-TF-CLT-OBJ-RELATION-FMT)
		//               COMMAREA   (DFHCOMMAREA)
		//               DATALENGTH (LENGTH OF DFHCOMMAREA)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("MU0Q0004", execContext).commarea(dfhcommarea).length(DfhcommareaTs020100.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getTableFormatterModules().getCltObjRelationFmt(), new Cawi008());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* CHECKS THE SUCCESS OF THE LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP (NORMAL)
		//               GO TO 9250-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE '9250-WRT-CLT-OBJ-RELATION-RQ'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9250-WRT-CLT-OBJ-RELATION-RQ");
			dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("9250-WRT-CLT-OBJ-RELATION-RQ");
			// COB_CODE: MOVE CF-TF-CLT-OBJ-RELATION-FMT
			//                                   TO WS-FAILED-LINK-PGM-NAME
			ws.getWsMiscWorkFlds().getProgramLinkFailed()
					.setWsFailedLinkPgmName(ws.getConstantFields().getTableFormatterModules().getCltObjRelationFmt());
			// COB_CODE: PERFORM 9100-PROGRAM-LINK-FAILED
			programLinkFailed();
			// COB_CODE: GO TO 9250-EXIT
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 9250-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 9250-EXIT
			return;
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsMiscWorkFlds().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsMiscWorkFlds().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getUbocCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getUbocCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("MU0Q0004", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setUbocAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setUbocAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initCltCltRelationRow() {
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationFixed().setCltObjRelationCsumFormatted("000000000");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationFixed().setClientIdKcre("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationFixed().setCltTypCdKcre("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationFixed().setHistoryVldNbrKcre("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationFixed().setCicrXrfIdKcre("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationFixed().setXrfTypCdKcre("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationFixed().setCicrEffDtKcre("");
		ws.getWsFilterCopybooks().getCawlf006().setTransProcessDt("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setClientIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setClientId("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCltTypCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCltTypCd("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setHistoryVldNbrCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCw06fHistoryVldNbrSignedFormatted("000000");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCicrXrfIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCicrXrfId("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setXrfTypCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setXrfTypCd("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCicrEffDtCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationKey().setCicrEffDt("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setCicrExpDtCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setCicrExpDt("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setCicrNbrOprStrsCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setCicrNbrOprStrsNi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setCicrNbrOprStrs("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setUserIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setUserId("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setStatusCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setStatusCd(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setTerminalIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setTerminalId("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setCicrEffAcyTsCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setCicrEffAcyTs("");
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setCicrExpAcyTsCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf006().getCltCltRelationData().setCicrExpAcyTs("");
	}

	public void initWsFcCltObjRelation() {
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationFixed().setCltObjRelationCsumFormatted("000000000");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationFixed().setClientIdKcre("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationFixed().setCltTypCdKcre("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationFixed().setHistoryVldNbrKcre("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationFixed().setCicrXrfIdKcre("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationFixed().setXrfTypCdKcre("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationFixed().setCicrEffDtKcre("");
		ws.getWsFilterCopybooks().getCawlf008().setTransProcessDt("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setTchObjectKeyCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setTchObjectKey("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setHistoryVldNbrCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setHistoryVldNbrSign(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setHistoryVldNbrFormatted("00000");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setCiorEffDtCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setCiorEffDt("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setObjSysIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setObjSysId("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setCiorObjSeqNbrCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setCiorObjSeqNbrSign(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationKey().setCiorObjSeqNbrFormatted("00000");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setClientIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setClientId("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setRltTypCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setRltTypCd("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setObjCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setObjCd("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCiorShwObjKeyCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCiorShwObjKey("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setAdrSeqNbrCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setAdrSeqNbrSign(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCw08fAdrSeqNbrFormatted("00000");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setUserIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setUserId("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setStatusCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setStatusCd(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setTerminalIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setTerminalId("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCiorExpDtCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCiorExpDt("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCiorEffAcyTsCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCiorEffAcyTs("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCiorExpAcyTsCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setCiorExpAcyTs("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setAppType(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setBusObjNm("");
		ws.getWsFilterCopybooks().getCawlf008().getCltObjRelationData().setObjDesc("");
	}

	public void initWsFcCltAddrComposite() {
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().setClientAddrCompChksumFormatted("000000000");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().setAdrIdKcre("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().setClientIdKcre("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().setAdrSeqNbrKcre("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().setTchObjectKeyKcre("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().setAdrIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().setAdrId("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setClientIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setClientId("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setHistoryVldNbrCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setHistoryVldNbrSign(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setCwcacfHistoryVldNbrFormatted("00000");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setCibcBusSeqNbrCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setCibcBusSeqNbrSign(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setCwcacfAdrSeqNbrFormatted("00000");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setCibcEffDtCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getCltAdrRelationKey().setCibcEffDt("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().setTransProcessDt("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setUserIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setUserId("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setTerminalIdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setTerminalId("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaAdr1Ci(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaAdr1("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaAdr2Ci(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaAdr2Ni(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaAdr2("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaCitNmCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaCitNm("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaCtyCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaCtyNi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaCty("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setStCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setStCd("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaPstCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaPstCd("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCtrCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCtrCd("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaAddAdrIndCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaAddAdrIndNi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaAddAdrInd(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setAddrStatusCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setAddrStatusCd(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setAddNmIndCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setAddNmInd(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setAdrTypCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setAdrTypCd("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setTchObjectKeyCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setTchObjectKey("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarExpDtCi("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarExpDt("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarSerAdr1TxtCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarSerAdr1Txt("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarSerCitNmCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarSerCitNm("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarSerStCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarSerStCd("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarSerPstCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarSerPstCd("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setRelStatusCdCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setRelStatusCd(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setEffAcyTsCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setEffAcyTs("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarExpAcyTsCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCiarExpAcyTs("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().getMoreRowsSw().setMoreRowsSw(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaShwObjKeyCi(Types.SPACE_CHAR);
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setCicaShwObjKey("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setBusObjNm("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setStDesc("");
		ws.getWsFilterCopybooks().getCwcacfClientAddrComposite().getClientAddrCompData().setAdrTypDesc("");
	}

	public void initTableFormatterData() {
		ws.getTs020tbl().getTfRequestResponseFlag().setTfRequestResponseFlag("");
		ws.getTs020tbl().setTfBusinessObjectNm("");
		ws.getTs020tbl().getTfActionCode().setTfActionCode("");
		ws.getTs020tbl().setTfRecSeqFormatted("00000");
		ws.getTs020tbl().getTfRecFoundFlag().setTfRecFoundFlag(Types.SPACE_CHAR);
		ws.getTs020tbl().setTfDataBuffer("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
