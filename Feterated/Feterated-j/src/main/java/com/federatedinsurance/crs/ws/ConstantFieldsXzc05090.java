/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZC05090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXzc05090 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-STA-CD
	private short maxStaCd = ((short) 2);
	//Original name: CF-MAX-CER-INF
	private short maxCerInf = ((short) 2500);
	//Original name: CF-MAX-WRD-INF
	private short maxWrdInf = ((short) 20);
	//Original name: CF-SOAP-SERVICE
	private String soapService = "CRSCertificateListSvcCallable";
	//Original name: CF-SOAP-OPERATION
	private String soapOperation = "GetCertificateList";
	//Original name: CF-CALLABLE-SVC-PROGRAM
	private String callableSvcProgram = "GIICALS";
	//Original name: CF-TRACE-STOR-VALUE
	private String traceStorValue = "GWAO";
	//Original name: CF-PARAGRAPH-NAMES
	private CfParagraphNamesXzc05090 paragraphNames = new CfParagraphNamesXzc05090();
	//Original name: CF-CHK-STA-PGM
	private String chkStaPgm = "XWC01090";
	//Original name: CF-WEB-SVC-ID
	private String webSvcId = "CERLISSVC";
	//Original name: CF-WEB-SVC-LKU-PGM
	private String webSvcLkuPgm = "TS571099";
	//Original name: CF-LV-ZLINUX-SRV
	private String lvZlinuxSrv = "03";
	//Original name: CF-CONTAINER-INFO
	private CfContainerInfoXzc05090 containerInfo = new CfContainerInfoXzc05090();
	//Original name: CF-NO-RENEW
	private char noRenew = 'N';
	//Original name: CF-DASH
	private char dash = '-';

	//==== METHODS ====
	public short getMaxStaCd() {
		return this.maxStaCd;
	}

	public short getMaxCerInf() {
		return this.maxCerInf;
	}

	public short getMaxWrdInf() {
		return this.maxWrdInf;
	}

	public String getSoapService() {
		return this.soapService;
	}

	public String getSoapOperation() {
		return this.soapOperation;
	}

	public String getCallableSvcProgram() {
		return this.callableSvcProgram;
	}

	public String getTraceStorValue() {
		return this.traceStorValue;
	}

	public String getChkStaPgm() {
		return this.chkStaPgm;
	}

	public String getWebSvcId() {
		return this.webSvcId;
	}

	public String getWebSvcLkuPgm() {
		return this.webSvcLkuPgm;
	}

	public String getLvZlinuxSrv() {
		return this.lvZlinuxSrv;
	}

	public char getNoRenew() {
		return this.noRenew;
	}

	public char getDash() {
		return this.dash;
	}

	public CfContainerInfoXzc05090 getContainerInfo() {
		return containerInfo;
	}

	public CfParagraphNamesXzc05090 getParagraphNames() {
		return paragraphNames;
	}
}
