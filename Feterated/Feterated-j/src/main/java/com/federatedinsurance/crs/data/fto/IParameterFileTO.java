/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;

/**Original name: I-PARAMETER-FILE<br>
 * File: I-PARAMETER-FILE from program XZ001000<br>
 * Generated as a class for rule FTO.<br>*/
public class IParameterFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: PR-CICS-PARM
	private String prCicsParm = DefaultValues.stringVal(Len.PR_CICS_PARM);
	//Original name: FILLER-PARAMETER-RECORD
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public String getParameterRecordFormatted() {
		return MarshalByteExt.bufferToStr(getParameterRecordBytes());
	}

	/**Original name: PARAMETER-RECORD<br>*/
	public byte[] getParameterRecordBytes() {
		byte[] buffer = new byte[Len.PARAMETER_RECORD];
		return getParameterRecordBytes(buffer, 1);
	}

	public void setParameterRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		prCicsParm = MarshalByte.readString(buffer, position, Len.PR_CICS_PARM);
		position += Len.PR_CICS_PARM;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getParameterRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, prCicsParm, Len.PR_CICS_PARM);
		position += Len.PR_CICS_PARM;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void initParameterRecordSpaces() {
		prCicsParm = "";
		flr1 = "";
	}

	public void setPrCicsParm(String prCicsParm) {
		this.prCicsParm = Functions.subString(prCicsParm, Len.PR_CICS_PARM);
	}

	public String getPrCicsParm() {
		return this.prCicsParm;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getParameterRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setParameterRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.PARAMETER_RECORD;
	}

	@Override
	public IBuffer copy() {
		IParameterFileTO copyTO = new IParameterFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PR_CICS_PARM = 8;
		public static final int FLR1 = 72;
		public static final int PARAMETER_RECORD = PR_CICS_PARM + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
