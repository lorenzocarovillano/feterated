/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotTyp;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ACT_NOT_TYP]
 * 
 */
public class ActNotTypDao extends BaseSqlDao<IActNotTyp> {

	private Cursor actNotTypCsr;
	private final IRowMapper<IActNotTyp> fetchActNotTypCsrRm = buildNamedRowMapper(IActNotTyp.class, "actNotTypCd", "actNotDes", "dsyOrdNbrObj",
			"docDesObj", "actNotPthCd", "actNotPthDes");

	public ActNotTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotTyp> getToClass() {
		return IActNotTyp.class;
	}

	public DbAccessStatus openActNotTypCsr() {
		actNotTypCsr = buildQuery("openActNotTypCsr").open();
		return dbStatus;
	}

	public IActNotTyp fetchActNotTypCsr(IActNotTyp iActNotTyp) {
		return fetch(actNotTypCsr, iActNotTyp, fetchActNotTypCsrRm);
	}

	public DbAccessStatus closeActNotTypCsr() {
		return closeCursor(actNotTypCsr);
	}

	public short selectRec(String xzh001ActNotTypCd, char cfYes, short dft) {
		return buildQuery("selectRec").bind("xzh001ActNotTypCd", xzh001ActNotTypCd).bind("cfYes", String.valueOf(cfYes)).scalarResultShort(dft);
	}

	public String selectByActNotTypCd(String actNotTypCd, String dft) {
		return buildQuery("selectByActNotTypCd").bind("actNotTypCd", actNotTypCd).scalarResultString(dft);
	}
}
