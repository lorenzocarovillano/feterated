/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IFrmPrcTyp;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [FRM_PRC_TYP]
 * 
 */
public class FrmPrcTypDao extends BaseSqlDao<IFrmPrcTyp> {

	private Cursor formCsr1;
	private final IRowMapper<IFrmPrcTyp> fetchFormCsr1Rm = buildNamedRowMapper(IFrmPrcTyp.class, "frmNbr", "frmEdtDt", "actNotTypCd", "edlFrmNm",
			"frmDes", "spePrcCdObj", "dtnCd");

	public FrmPrcTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IFrmPrcTyp> getToClass() {
		return IFrmPrcTyp.class;
	}

	public DbAccessStatus openFormCsr1() {
		formCsr1 = buildQuery("openFormCsr1").open();
		return dbStatus;
	}

	public IFrmPrcTyp fetchFormCsr1(IFrmPrcTyp iFrmPrcTyp) {
		return fetch(formCsr1, iFrmPrcTyp, fetchFormCsr1Rm);
	}

	public DbAccessStatus closeFormCsr1() {
		return closeCursor(formCsr1);
	}
}
