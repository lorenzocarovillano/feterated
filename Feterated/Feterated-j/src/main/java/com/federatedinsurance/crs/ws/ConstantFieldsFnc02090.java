/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program FNC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsFnc02090 {

	//==== PROPERTIES ====
	//Original name: CF-SOAP-SERVICE
	private String cfSoapService = "CLTGetCityMatchCodeCallable";
	//Original name: CF-SOAP-OPERATION
	private String cfSoapOperation = "getMatchkey";
	//Original name: CF-SOAP-LAYOUT-VERSION
	private String cfSoapLayoutVersion = "03";
	//Original name: CF-WEB-SVC-ID
	private String cfWebSvcId = "PPDFLOOKUP";
	//Original name: CF-INVALID-CD
	private String cfInvalidCd = "-1";
	//Original name: CF-INVALID-IDENTIFIER
	private String cfInvalidIdentifier = "INVALID";
	//Original name: CF-PARAGRAPH-NAMES
	private CfParagraphNamesFnc02090 cfParagraphNames = new CfParagraphNamesFnc02090();
	//Original name: CF-TSQ-UID-LKU-RUT
	private String cfTsqUidLkuRut = "TS571098";
	//Original name: C-UID-PWD-TSQ
	private String cUidPwdTsq = "UIDPPP11";

	//==== METHODS ====
	public String getCfSoapService() {
		return this.cfSoapService;
	}

	public String getCfSoapOperation() {
		return this.cfSoapOperation;
	}

	public String getCfSoapLayoutVersion() {
		return this.cfSoapLayoutVersion;
	}

	public String getCfWebSvcId() {
		return this.cfWebSvcId;
	}

	public String getCfInvalidCd() {
		return this.cfInvalidCd;
	}

	public String getCfInvalidIdentifier() {
		return this.cfInvalidIdentifier;
	}

	public String getCfTsqUidLkuRut() {
		return this.cfTsqUidLkuRut;
	}

	public String getcUidPwdTsq() {
		return this.cUidPwdTsq;
	}

	public CfParagraphNamesFnc02090 getCfParagraphNames() {
		return cfParagraphNames;
	}
}
