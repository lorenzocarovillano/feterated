/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program MU0X0004<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class DfhcommareaMu0x0004 extends BytesClass {

	//==== PROPERTIES ====
	public static final int PPC_NON_LOGGABLE_ERRORS_MAXOCCURS = 10;
	public static final int PPC_WARNINGS_MAXOCCURS = 10;
	public static final char PPC_BYPASS_SYNCPOINT_IN_MDRV = 'B';
	private static final char[] PPC_DO_NOT_BYPASS_SYNCPOINT = new char[] { Types.SPACE_CHAR, Types.LOW_CHAR_VAL, Types.HIGH_CHAR_VAL };
	public static final String PPC_FATAL_ERROR_CODE = "0300";
	public static final String PPC_NLBE_CODE = "0200";
	public static final String PPC_WARNING_CODE = "0100";
	public static final String PPC_NO_ERROR_CODE = "0000";

	//==== CONSTRUCTORS ====
	public DfhcommareaMu0x0004() {
	}

	public DfhcommareaMu0x0004(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.DFHCOMMAREA, Pos.DFHCOMMAREA);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.DFHCOMMAREA, Pos.DFHCOMMAREA);
		return buffer;
	}

	public void setPpcInputParmsBytes(byte[] buffer) {
		setPpcInputParmsBytes(buffer, 1);
	}

	public void setPpcInputParmsBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.PPC_INPUT_PARMS, Pos.PPC_INPUT_PARMS);
	}

	public void setPpcServiceDataSize(int ppcServiceDataSize) {
		writeBinaryInt(Pos.PPC_SERVICE_DATA_SIZE, ppcServiceDataSize);
	}

	/**Original name: PPC-SERVICE-DATA-SIZE<br>*/
	public int getPpcServiceDataSize() {
		return readBinaryInt(Pos.PPC_SERVICE_DATA_SIZE);
	}

	public int getPpcServiceDataPointer() {
		return readBinaryInt(Pos.PPC_SERVICE_DATA_POINTER);
	}

	public void setPpcBypassSyncpointMdrvInd(char ppcBypassSyncpointMdrvInd) {
		writeChar(Pos.PPC_BYPASS_SYNCPOINT_MDRV_IND, ppcBypassSyncpointMdrvInd);
	}

	/**Original name: PPC-BYPASS-SYNCPOINT-MDRV-IND<br>
	 * <pre>* FLAG TO INDICATE WHETHER OR NOT MAIN DRIVER DOES SYNCPOINT</pre>*/
	public char getPpcBypassSyncpointMdrvInd() {
		return readChar(Pos.PPC_BYPASS_SYNCPOINT_MDRV_IND);
	}

	public boolean isPpcBypassSyncpointInMdrv() {
		return getPpcBypassSyncpointMdrvInd() == PPC_BYPASS_SYNCPOINT_IN_MDRV;
	}

	public void setPpcOperation(String ppcOperation) {
		writeString(Pos.PPC_OPERATION, ppcOperation, Len.PPC_OPERATION);
	}

	/**Original name: PPC-OPERATION<br>*/
	public String getPpcOperation() {
		return readString(Pos.PPC_OPERATION, Len.PPC_OPERATION);
	}

	/**Original name: PPC-OUTPUT-PARMS<br>*/
	public byte[] getPpcOutputParmsBytes() {
		byte[] buffer = new byte[Len.PPC_OUTPUT_PARMS];
		return getPpcOutputParmsBytes(buffer, 1);
	}

	public byte[] getPpcOutputParmsBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.PPC_OUTPUT_PARMS, Pos.PPC_OUTPUT_PARMS);
		return buffer;
	}

	public void setPpcErrorHandlingParmsBytes(byte[] buffer) {
		setPpcErrorHandlingParmsBytes(buffer, 1);
	}

	public void setPpcErrorHandlingParmsBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.PPC_ERROR_HANDLING_PARMS, Pos.PPC_ERROR_HANDLING_PARMS);
	}

	public void setPpcErrorReturnCode(short ppcErrorReturnCode) {
		writeShort(Pos.PPC_ERROR_RETURN_CODE, ppcErrorReturnCode, Len.Int.PPC_ERROR_RETURN_CODE, SignType.NO_SIGN);
	}

	public void setPpcErrorReturnCodeFormatted(String ppcErrorReturnCode) {
		writeString(Pos.PPC_ERROR_RETURN_CODE, Trunc.toUnsignedNumeric(ppcErrorReturnCode, Len.PPC_ERROR_RETURN_CODE), Len.PPC_ERROR_RETURN_CODE);
	}

	/**Original name: PPC-ERROR-RETURN-CODE<br>*/
	public short getPpcErrorReturnCode() {
		return readNumDispUnsignedShort(Pos.PPC_ERROR_RETURN_CODE, Len.PPC_ERROR_RETURN_CODE);
	}

	public String getPpcErrorReturnCodeFormatted() {
		return readFixedString(Pos.PPC_ERROR_RETURN_CODE, Len.PPC_ERROR_RETURN_CODE);
	}

	public boolean isPpcFatalErrorCode() {
		return getPpcErrorReturnCodeFormatted().equals(PPC_FATAL_ERROR_CODE);
	}

	public void setPpcFatalErrorCode() {
		setPpcErrorReturnCodeFormatted(PPC_FATAL_ERROR_CODE);
	}

	public boolean isPpcNlbeCode() {
		return getPpcErrorReturnCodeFormatted().equals(PPC_NLBE_CODE);
	}

	public boolean isPpcWarningCode() {
		return getPpcErrorReturnCodeFormatted().equals(PPC_WARNING_CODE);
	}

	public boolean isPpcNoErrorCode() {
		return getPpcErrorReturnCodeFormatted().equals(PPC_NO_ERROR_CODE);
	}

	public void setPpcNoErrorCode() {
		setPpcErrorReturnCodeFormatted(PPC_NO_ERROR_CODE);
	}

	public void setPpcFatalErrorMessage(String ppcFatalErrorMessage) {
		writeString(Pos.PPC_FATAL_ERROR_MESSAGE, ppcFatalErrorMessage, Len.PPC_FATAL_ERROR_MESSAGE);
	}

	/**Original name: PPC-FATAL-ERROR-MESSAGE<br>*/
	public String getPpcFatalErrorMessage() {
		return readString(Pos.PPC_FATAL_ERROR_MESSAGE, Len.PPC_FATAL_ERROR_MESSAGE);
	}

	public String getPpcFatalErrorMessageFormatted() {
		return Functions.padBlanks(getPpcFatalErrorMessage(), Len.PPC_FATAL_ERROR_MESSAGE);
	}

	public void setPpcNonLoggableErrorCnt(short ppcNonLoggableErrorCnt) {
		writeShort(Pos.PPC_NON_LOGGABLE_ERROR_CNT, ppcNonLoggableErrorCnt, Len.Int.PPC_NON_LOGGABLE_ERROR_CNT, SignType.NO_SIGN);
	}

	public void setPpcNonLoggableErrorCntFormatted(String ppcNonLoggableErrorCnt) {
		writeString(Pos.PPC_NON_LOGGABLE_ERROR_CNT, Trunc.toUnsignedNumeric(ppcNonLoggableErrorCnt, Len.PPC_NON_LOGGABLE_ERROR_CNT),
				Len.PPC_NON_LOGGABLE_ERROR_CNT);
	}

	/**Original name: PPC-NON-LOGGABLE-ERROR-CNT<br>*/
	public short getPpcNonLoggableErrorCnt() {
		return readNumDispUnsignedShort(Pos.PPC_NON_LOGGABLE_ERROR_CNT, Len.PPC_NON_LOGGABLE_ERROR_CNT);
	}

	public void setPpcNonLogErrMsg(int ppcNonLogErrMsgIdx, String ppcNonLogErrMsg) {
		int position = Pos.ppcNonLogErrMsg(ppcNonLogErrMsgIdx - 1);
		writeString(position, ppcNonLogErrMsg, Len.PPC_NON_LOG_ERR_MSG);
	}

	/**Original name: PPC-NON-LOG-ERR-MSG<br>*/
	public String getPpcNonLogErrMsg(int ppcNonLogErrMsgIdx) {
		int position = Pos.ppcNonLogErrMsg(ppcNonLogErrMsgIdx - 1);
		return readString(position, Len.PPC_NON_LOG_ERR_MSG);
	}

	public void setPpcWarningCnt(short ppcWarningCnt) {
		writeShort(Pos.PPC_WARNING_CNT, ppcWarningCnt, Len.Int.PPC_WARNING_CNT, SignType.NO_SIGN);
	}

	public void setPpcWarningCntFormatted(String ppcWarningCnt) {
		writeString(Pos.PPC_WARNING_CNT, Trunc.toUnsignedNumeric(ppcWarningCnt, Len.PPC_WARNING_CNT), Len.PPC_WARNING_CNT);
	}

	/**Original name: PPC-WARNING-CNT<br>*/
	public short getPpcWarningCnt() {
		return readNumDispUnsignedShort(Pos.PPC_WARNING_CNT, Len.PPC_WARNING_CNT);
	}

	public void setPpcWarnMsg(int ppcWarnMsgIdx, String ppcWarnMsg) {
		int position = Pos.ppcWarnMsg(ppcWarnMsgIdx - 1);
		writeString(position, ppcWarnMsg, Len.PPC_WARN_MSG);
	}

	/**Original name: PPC-WARN-MSG<br>*/
	public String getPpcWarnMsg(int ppcWarnMsgIdx) {
		int position = Pos.ppcWarnMsg(ppcWarnMsgIdx - 1);
		return readString(position, Len.PPC_WARN_MSG);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int DFHCOMMAREA = 1;
		public static final int PROXY_PROGRAM_COMMON = DFHCOMMAREA;
		public static final int PPC_INPUT_PARMS = PROXY_PROGRAM_COMMON;
		public static final int PPC_MEMORY_ALLOCATION_PARMS = PPC_INPUT_PARMS;
		public static final int PPC_SERVICE_DATA_SIZE = PPC_MEMORY_ALLOCATION_PARMS;
		public static final int PPC_SERVICE_DATA_POINTER = PPC_SERVICE_DATA_SIZE + Len.PPC_SERVICE_DATA_SIZE;
		public static final int PPC_BYPASS_SYNCPOINT_MDRV_IND = PPC_SERVICE_DATA_POINTER + Len.PPC_SERVICE_DATA_POINTER;
		public static final int PPC_OPERATION = PPC_BYPASS_SYNCPOINT_MDRV_IND + Len.PPC_BYPASS_SYNCPOINT_MDRV_IND;
		public static final int PPC_OUTPUT_PARMS = PPC_OPERATION + Len.PPC_OPERATION;
		public static final int PPC_ERROR_HANDLING_PARMS = PPC_OUTPUT_PARMS;
		public static final int PPC_ERROR_RETURN_CODE = PPC_ERROR_HANDLING_PARMS;
		public static final int PPC_FATAL_ERROR_MESSAGE = PPC_ERROR_RETURN_CODE + Len.PPC_ERROR_RETURN_CODE;
		public static final int PPC_NON_LOGGABLE_ERROR_CNT = PPC_FATAL_ERROR_MESSAGE + Len.PPC_FATAL_ERROR_MESSAGE;
		public static final int PPC_WARNING_CNT = ppcNonLogErrMsg(PPC_NON_LOGGABLE_ERRORS_MAXOCCURS - 1) + Len.PPC_NON_LOG_ERR_MSG;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int ppcNonLoggableErrors(int idx) {
			return PPC_NON_LOGGABLE_ERROR_CNT + Len.PPC_NON_LOGGABLE_ERROR_CNT + idx * Len.PPC_NON_LOGGABLE_ERRORS;
		}

		public static int ppcNonLogErrMsg(int idx) {
			return ppcNonLoggableErrors(idx);
		}

		public static int ppcWarnings(int idx) {
			return PPC_WARNING_CNT + Len.PPC_WARNING_CNT + idx * Len.PPC_WARNINGS;
		}

		public static int ppcWarnMsg(int idx) {
			return ppcWarnings(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int PPC_SERVICE_DATA_SIZE = 4;
		public static final int PPC_SERVICE_DATA_POINTER = 4;
		public static final int PPC_BYPASS_SYNCPOINT_MDRV_IND = 1;
		public static final int PPC_OPERATION = 32;
		public static final int PPC_ERROR_RETURN_CODE = 4;
		public static final int PPC_FATAL_ERROR_MESSAGE = 250;
		public static final int PPC_NON_LOGGABLE_ERROR_CNT = 4;
		public static final int PPC_NON_LOG_ERR_MSG = 500;
		public static final int PPC_NON_LOGGABLE_ERRORS = PPC_NON_LOG_ERR_MSG;
		public static final int PPC_WARNING_CNT = 4;
		public static final int PPC_WARN_MSG = 500;
		public static final int PPC_WARNINGS = PPC_WARN_MSG;
		public static final int PPC_MEMORY_ALLOCATION_PARMS = PPC_SERVICE_DATA_SIZE + PPC_SERVICE_DATA_POINTER;
		public static final int PPC_INPUT_PARMS = PPC_MEMORY_ALLOCATION_PARMS + PPC_BYPASS_SYNCPOINT_MDRV_IND + PPC_OPERATION;
		public static final int PPC_ERROR_HANDLING_PARMS = PPC_ERROR_RETURN_CODE + PPC_FATAL_ERROR_MESSAGE + PPC_NON_LOGGABLE_ERROR_CNT
				+ DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS * PPC_NON_LOGGABLE_ERRORS + PPC_WARNING_CNT
				+ DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS * PPC_WARNINGS;
		public static final int PPC_OUTPUT_PARMS = PPC_ERROR_HANDLING_PARMS;
		public static final int PROXY_PROGRAM_COMMON = PPC_INPUT_PARMS + PPC_OUTPUT_PARMS;
		public static final int DFHCOMMAREA = PROXY_PROGRAM_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int PPC_ERROR_RETURN_CODE = 4;
			public static final int PPC_NON_LOGGABLE_ERROR_CNT = 4;
			public static final int PPC_WARNING_CNT = 4;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
