/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90P1-ROW<br>
 * Variable: WS-XZ0A90P1-ROW from program XZ0B90P0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90p1Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9P1-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZA9P1-POL-TYP
	private String polTyp = DefaultValues.stringVal(Len.POL_TYP);
	//Original name: XZA9P1-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZA9P1-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90P1_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90p1RowBytes(buf);
	}

	public String getWsXz0a90p1RowFormatted() {
		return getGetCertPolListDtlFormatted();
	}

	public void setWsXz0a90p1RowBytes(byte[] buffer) {
		setWsXz0a90p1RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90p1RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90P1_ROW];
		return getWsXz0a90p1RowBytes(buffer, 1);
	}

	public void setWsXz0a90p1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetCertPolListDtlBytes(buffer, position);
	}

	public byte[] getWsXz0a90p1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetCertPolListDtlBytes(buffer, position);
		return buffer;
	}

	public String getGetCertPolListDtlFormatted() {
		return MarshalByteExt.bufferToStr(getGetCertPolListDtlBytes());
	}

	/**Original name: XZA9P1-GET-CERT-POL-LIST-DTL<br>
	 * <pre>*************************************************************
	 *  XZ0A90P0 - BPO COPYBOOK FOR                                *
	 *             UOW : XY_GET_NOT_CER_POL_LIST                   *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  14APR2009 E404KXS    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetCertPolListDtlBytes() {
		byte[] buffer = new byte[Len.GET_CERT_POL_LIST_DTL];
		return getGetCertPolListDtlBytes(buffer, 1);
	}

	public void setGetCertPolListDtlBytes(byte[] buffer, int offset) {
		int position = offset;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		polTyp = MarshalByte.readString(buffer, position, Len.POL_TYP);
		position += Len.POL_TYP;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		polExpDt = MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
	}

	public byte[] getGetCertPolListDtlBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polTyp, Len.POL_TYP);
		position += Len.POL_TYP;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		return buffer;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolTyp(String polTyp) {
		this.polTyp = Functions.subString(polTyp, Len.POL_TYP);
	}

	public String getPolTyp() {
		return this.polTyp;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90p1RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int POL_TYP = 4;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int GET_CERT_POL_LIST_DTL = POL_NBR + POL_TYP + POL_EFF_DT + POL_EXP_DT;
		public static final int WS_XZ0A90P1_ROW = GET_CERT_POL_LIST_DTL;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
