/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SA-RETURN-CODE<br>
 * Variable: SA-RETURN-CODE from program XZ004000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaReturnCodeXz004000 {

	//==== PROPERTIES ====
	private short value = ((short) 0);
	public static final short IVD_NOT_TYP = ((short) 200);
	public static final short FATAL_DB2_ERROR = ((short) 500);
	public static final short PARM_ERROR = ((short) 501);

	//==== METHODS ====
	public void setSaReturnCode(short saReturnCode) {
		this.value = saReturnCode;
	}

	public short getSaReturnCode() {
		return this.value;
	}

	public void setIvdNotTyp() {
		value = IVD_NOT_TYP;
	}

	public void setFatalDb2Error() {
		value = FATAL_DB2_ERROR;
	}

	public void setParmError() {
		value = PARM_ERROR;
	}
}
