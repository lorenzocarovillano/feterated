/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import static com.bphx.ctu.af.util.StringUtil.parseHexChar;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.federatedinsurance.crs.ws.ExciReturnCode;

/**Original name: DFHXCPLO<br>
 * Variable: DFHXCPLO from copybook DFHXCPLO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Dfhxcplo {

	//==== PROPERTIES ====
	//Original name: EXCI-RETURN-CODE
	private ExciReturnCode exciReturnCode = new ExciReturnCode();
	//Original name: EXCI-DPL-RETAREA
	private ExciDplRetarea exciDplRetarea = new ExciDplRetarea();
	/**Original name: VERSION-1<br>
	 * <pre>****************************************************************
	 *    CALL API Parameter values
	 * ****************************************************************
	 * ****************************************************************
	 *   Constants for use with VERSION_NUMBER parameter
	 * ****************************************************************</pre>*/
	private long version1 = 1L;
	/**Original name: INIT-USER<br>
	 * <pre>****************************************************************
	 *   Constants for use with the CALL_TYPE parameter
	 * ****************************************************************</pre>*/
	private long initUser = 1L;
	//Original name: ALLOCATE-PIPE
	private long allocatePipe = 2L;
	//Original name: OPEN-PIPE
	private long openPipe = 3L;
	//Original name: CLOSE-PIPE
	private long closePipe = 4L;
	//Original name: DEALLOCATE-PIPE
	private long deallocatePipe = 5L;
	/**Original name: SPECIFIC-PIPE<br>
	 * <pre>****************************************************************
	 *   Constants for use with ALLOCATE_OPTS parameter
	 * ****************************************************************</pre>*/
	private char specificPipe = parseHexChar("00");

	//==== METHODS ====
	public void setVersion1(long version1) {
		this.version1 = version1;
	}

	public void setVersion1FromBuffer(byte[] buffer) {
		version1 = MarshalByte.readBinaryUnsignedInt(buffer, 1);
	}

	public long getVersion1() {
		return this.version1;
	}

	public String getVersion1Formatted() {
		return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.BINARY)).format(getVersion1()).toString();
	}

	public void setInitUser(long initUser) {
		this.initUser = initUser;
	}

	public void setInitUserFromBuffer(byte[] buffer) {
		initUser = MarshalByte.readBinaryUnsignedInt(buffer, 1);
	}

	public long getInitUser() {
		return this.initUser;
	}

	public String getInitUserFormatted() {
		return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.BINARY)).format(getInitUser()).toString();
	}

	public void setAllocatePipe(long allocatePipe) {
		this.allocatePipe = allocatePipe;
	}

	public void setAllocatePipeFromBuffer(byte[] buffer) {
		allocatePipe = MarshalByte.readBinaryUnsignedInt(buffer, 1);
	}

	public long getAllocatePipe() {
		return this.allocatePipe;
	}

	public String getAllocatePipeFormatted() {
		return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.BINARY)).format(getAllocatePipe()).toString();
	}

	public void setOpenPipe(long openPipe) {
		this.openPipe = openPipe;
	}

	public void setOpenPipeFromBuffer(byte[] buffer) {
		openPipe = MarshalByte.readBinaryUnsignedInt(buffer, 1);
	}

	public long getOpenPipe() {
		return this.openPipe;
	}

	public String getOpenPipeFormatted() {
		return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.BINARY)).format(getOpenPipe()).toString();
	}

	public void setClosePipe(long closePipe) {
		this.closePipe = closePipe;
	}

	public void setClosePipeFromBuffer(byte[] buffer) {
		closePipe = MarshalByte.readBinaryUnsignedInt(buffer, 1);
	}

	public long getClosePipe() {
		return this.closePipe;
	}

	public String getClosePipeFormatted() {
		return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.BINARY)).format(getClosePipe()).toString();
	}

	public void setDeallocatePipe(long deallocatePipe) {
		this.deallocatePipe = deallocatePipe;
	}

	public void setDeallocatePipeFromBuffer(byte[] buffer) {
		deallocatePipe = MarshalByte.readBinaryUnsignedInt(buffer, 1);
	}

	public long getDeallocatePipe() {
		return this.deallocatePipe;
	}

	public String getDeallocatePipeFormatted() {
		return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.BINARY)).format(getDeallocatePipe()).toString();
	}

	public void setSpecificPipe(char specificPipe) {
		this.specificPipe = specificPipe;
	}

	public void setSpecificPipeFromBuffer(byte[] buffer) {
		specificPipe = MarshalByte.readChar(buffer, 1);
	}

	public char getSpecificPipe() {
		return this.specificPipe;
	}

	public ExciDplRetarea getExciDplRetarea() {
		return exciDplRetarea;
	}

	public ExciReturnCode getExciReturnCode() {
		return exciReturnCode;
	}
}
