/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: EDTLC-HAL-ERR-LOG-DTL-ROW<br>
 * Variable: EDTLC-HAL-ERR-LOG-DTL-ROW from copybook HALLCEDT<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class EdtlcHalErrLogDtlRow {

	//==== PROPERTIES ====
	//Original name: EDTLC-HAL-ERR-LOG-DTL-CSUM
	private String halErrLogDtlCsum = DefaultValues.stringVal(Len.HAL_ERR_LOG_DTL_CSUM);
	//Original name: EDTLC-ERR-RFR-NBR-KCRE
	private String errRfrNbrKcre = DefaultValues.stringVal(Len.ERR_RFR_NBR_KCRE);
	//Original name: EDTLC-FAIL-TS-KCRE
	private String failTsKcre = DefaultValues.stringVal(Len.FAIL_TS_KCRE);
	//Original name: EDTLC-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: EDTLC-ERR-RFR-NBR-CI
	private char errRfrNbrCi = DefaultValues.CHAR_VAL;
	//Original name: EDTLC-ERR-RFR-NBR
	private String errRfrNbr = DefaultValues.stringVal(Len.ERR_RFR_NBR);
	//Original name: EDTLC-FAIL-TS-CI
	private char failTsCi = DefaultValues.CHAR_VAL;
	//Original name: EDTLC-FAIL-TS
	private String failTs = DefaultValues.stringVal(Len.FAIL_TS);
	/**Original name: EDTLC-USERID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char useridCi = DefaultValues.CHAR_VAL;
	//Original name: EDTLC-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);
	//Original name: EDTLC-FAIL-KEY-TXT-CI
	private char failKeyTxtCi = DefaultValues.CHAR_VAL;
	//Original name: EDTLC-FAIL-KEY-TXT
	private String failKeyTxt = DefaultValues.stringVal(Len.FAIL_KEY_TXT);

	//==== METHODS ====
	public String getEdtlcHalErrLogDtlRowFormatted() {
		return MarshalByteExt.bufferToStr(getEdtlcHalErrLogDtlRowBytes());
	}

	public byte[] getEdtlcHalErrLogDtlRowBytes() {
		byte[] buffer = new byte[Len.EDTLC_HAL_ERR_LOG_DTL_ROW];
		return getEdtlcHalErrLogDtlRowBytes(buffer, 1);
	}

	public byte[] getEdtlcHalErrLogDtlRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getHalErrLogDtlFixedBytes(buffer, position);
		position += Len.HAL_ERR_LOG_DTL_FIXED;
		getHalErrLogDtlDatesBytes(buffer, position);
		position += Len.HAL_ERR_LOG_DTL_DATES;
		getHalErrLogDtlKeyBytes(buffer, position);
		position += Len.HAL_ERR_LOG_DTL_KEY;
		getHalErrLogDtlDataBytes(buffer, position);
		return buffer;
	}

	public byte[] getHalErrLogDtlFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, halErrLogDtlCsum, Len.HAL_ERR_LOG_DTL_CSUM);
		position += Len.HAL_ERR_LOG_DTL_CSUM;
		MarshalByte.writeString(buffer, position, errRfrNbrKcre, Len.ERR_RFR_NBR_KCRE);
		position += Len.ERR_RFR_NBR_KCRE;
		MarshalByte.writeString(buffer, position, failTsKcre, Len.FAIL_TS_KCRE);
		return buffer;
	}

	public void setHalErrLogDtlCsumFormatted(String halErrLogDtlCsum) {
		this.halErrLogDtlCsum = Trunc.toUnsignedNumeric(halErrLogDtlCsum, Len.HAL_ERR_LOG_DTL_CSUM);
	}

	public int getHalErrLogDtlCsum() {
		return NumericDisplay.asInt(this.halErrLogDtlCsum);
	}

	public void setErrRfrNbrKcre(String errRfrNbrKcre) {
		this.errRfrNbrKcre = Functions.subString(errRfrNbrKcre, Len.ERR_RFR_NBR_KCRE);
	}

	public String getErrRfrNbrKcre() {
		return this.errRfrNbrKcre;
	}

	public void setFailTsKcre(String failTsKcre) {
		this.failTsKcre = Functions.subString(failTsKcre, Len.FAIL_TS_KCRE);
	}

	public String getFailTsKcre() {
		return this.failTsKcre;
	}

	public byte[] getHalErrLogDtlDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public byte[] getHalErrLogDtlKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, errRfrNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, errRfrNbr, Len.ERR_RFR_NBR);
		position += Len.ERR_RFR_NBR;
		MarshalByte.writeChar(buffer, position, failTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, failTs, Len.FAIL_TS);
		return buffer;
	}

	public void setErrRfrNbrCi(char errRfrNbrCi) {
		this.errRfrNbrCi = errRfrNbrCi;
	}

	public char getErrRfrNbrCi() {
		return this.errRfrNbrCi;
	}

	public void setErrRfrNbr(String errRfrNbr) {
		this.errRfrNbr = Functions.subString(errRfrNbr, Len.ERR_RFR_NBR);
	}

	public String getErrRfrNbr() {
		return this.errRfrNbr;
	}

	public void setFailTsCi(char failTsCi) {
		this.failTsCi = failTsCi;
	}

	public char getFailTsCi() {
		return this.failTsCi;
	}

	public void setFailTs(String failTs) {
		this.failTs = Functions.subString(failTs, Len.FAIL_TS);
	}

	public String getFailTs() {
		return this.failTs;
	}

	public byte[] getHalErrLogDtlDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, useridCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position += Len.USERID;
		MarshalByte.writeChar(buffer, position, failKeyTxtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, failKeyTxt, Len.FAIL_KEY_TXT);
		return buffer;
	}

	public void setUseridCi(char useridCi) {
		this.useridCi = useridCi;
	}

	public char getUseridCi() {
		return this.useridCi;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	public void setFailKeyTxtCi(char failKeyTxtCi) {
		this.failKeyTxtCi = failKeyTxtCi;
	}

	public char getFailKeyTxtCi() {
		return this.failKeyTxtCi;
	}

	public void setFailKeyTxt(String failKeyTxt) {
		this.failKeyTxt = Functions.subString(failKeyTxt, Len.FAIL_KEY_TXT);
	}

	public String getFailKeyTxt() {
		return this.failKeyTxt;
	}

	public String getFailKeyTxtFormatted() {
		return Functions.padBlanks(getFailKeyTxt(), Len.FAIL_KEY_TXT);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HAL_ERR_LOG_DTL_CSUM = 9;
		public static final int ERR_RFR_NBR_KCRE = 32;
		public static final int FAIL_TS_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int ERR_RFR_NBR = 10;
		public static final int FAIL_TS = 26;
		public static final int USERID = 32;
		public static final int FAIL_KEY_TXT = 200;
		public static final int HAL_ERR_LOG_DTL_FIXED = HAL_ERR_LOG_DTL_CSUM + ERR_RFR_NBR_KCRE + FAIL_TS_KCRE;
		public static final int HAL_ERR_LOG_DTL_DATES = TRANS_PROCESS_DT;
		public static final int ERR_RFR_NBR_CI = 1;
		public static final int FAIL_TS_CI = 1;
		public static final int HAL_ERR_LOG_DTL_KEY = ERR_RFR_NBR_CI + ERR_RFR_NBR + FAIL_TS_CI + FAIL_TS;
		public static final int USERID_CI = 1;
		public static final int FAIL_KEY_TXT_CI = 1;
		public static final int HAL_ERR_LOG_DTL_DATA = USERID_CI + USERID + FAIL_KEY_TXT_CI + FAIL_KEY_TXT;
		public static final int EDTLC_HAL_ERR_LOG_DTL_ROW = HAL_ERR_LOG_DTL_FIXED + HAL_ERR_LOG_DTL_DATES + HAL_ERR_LOG_DTL_KEY
				+ HAL_ERR_LOG_DTL_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
