/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-AUDIT-TRIGGER-SW<br>
 * Variable: WS-AUDIT-TRIGGER-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsAuditTriggerSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char REQUIRED = 'Y';
	public static final char NOT_REQUIRED = 'N';

	//==== METHODS ====
	public void setAuditTriggerSw(char auditTriggerSw) {
		this.value = auditTriggerSw;
	}

	public char getAuditTriggerSw() {
		return this.value;
	}

	public boolean isWsAuditRequired() {
		return value == REQUIRED;
	}

	public void setWsAuditNotRequired() {
		value = NOT_REQUIRED;
	}
}
