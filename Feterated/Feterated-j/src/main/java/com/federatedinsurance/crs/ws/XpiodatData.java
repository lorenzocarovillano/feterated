/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.copy.PmPlfV;
import com.federatedinsurance.crs.copy.PmaSe3Arch;
import com.federatedinsurance.crs.ws.redefines.DclholidayPrc;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XPIODAT<br>
 * Generated as a class for rule WS.<br>*/
public class XpiodatData {

	//==== PROPERTIES ====
	//Original name: WS-RETURN-CODES
	private WsReturnCodes wsReturnCodes = new WsReturnCodes();
	//Original name: WS-WORK-FIELDS
	private WsWorkFieldsXpiodat wsWorkFields = new WsWorkFieldsXpiodat();
	//Original name: WS-VAR-DATE-NDX
	private int wsVarDateNdx = 1;
	//Original name: WS-DATE-NDX
	private int wsDateNdx = 1;
	//Original name: WS-DATE-NDX2
	private int wsDateNdx2 = 1;
	//Original name: WS-MONTH-NDX
	private int wsMonthNdx = 1;
	//Original name: WS-LIT-MONTH-NDX
	private int wsLitMonthNdx = 1;
	//Original name: SQL-TIMESTAMP-FOR
	private SqlTimestampFor sqlTimestampFor = new SqlTimestampFor();
	//Original name: WS-HOLIDAY-WORK-FIELDS
	private WsHolidayWorkFields wsHolidayWorkFields = new WsHolidayWorkFields();
	//Original name: PMA-SE3-ARCH
	private PmaSe3Arch pmaSe3Arch = new PmaSe3Arch();
	//Original name: PM-PLF-V
	private PmPlfV pmPlfV = new PmPlfV();
	/**Original name: DCLHOLIDAY-PRC<br>
	 * <pre>*****************************************************************
	 *  COBOL DECLARATION FOR TABLE HOLIDAY_PRC                        *
	 * *****************************************************************</pre>*/
	private DclholidayPrc dclholidayPrc = new DclholidayPrc();

	//==== METHODS ====
	public void setWsVarDateNdx(int wsVarDateNdx) {
		this.wsVarDateNdx = wsVarDateNdx;
	}

	public int getWsVarDateNdx() {
		return this.wsVarDateNdx;
	}

	public void setWsDateNdx(int wsDateNdx) {
		this.wsDateNdx = wsDateNdx;
	}

	public int getWsDateNdx() {
		return this.wsDateNdx;
	}

	public void setWsDateNdx2(int wsDateNdx2) {
		this.wsDateNdx2 = wsDateNdx2;
	}

	public int getWsDateNdx2() {
		return this.wsDateNdx2;
	}

	public void setWsMonthNdx(int wsMonthNdx) {
		this.wsMonthNdx = wsMonthNdx;
	}

	public int getWsMonthNdx() {
		return this.wsMonthNdx;
	}

	public void setWsLitMonthNdx(int wsLitMonthNdx) {
		this.wsLitMonthNdx = wsLitMonthNdx;
	}

	public int getWsLitMonthNdx() {
		return this.wsLitMonthNdx;
	}

	public DclholidayPrc getDclholidayPrc() {
		return dclholidayPrc;
	}

	public PmPlfV getPmPlfV() {
		return pmPlfV;
	}

	public PmaSe3Arch getPmaSe3Arch() {
		return pmaSe3Arch;
	}

	public SqlTimestampFor getSqlTimestampFor() {
		return sqlTimestampFor;
	}

	public WsHolidayWorkFields getWsHolidayWorkFields() {
		return wsHolidayWorkFields;
	}

	public WsReturnCodes getWsReturnCodes() {
		return wsReturnCodes;
	}

	public WsWorkFieldsXpiodat getWsWorkFields() {
		return wsWorkFields;
	}
}
