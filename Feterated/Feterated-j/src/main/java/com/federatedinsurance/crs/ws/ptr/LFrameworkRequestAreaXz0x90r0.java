/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X90R0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x90r0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x90r0() {
	}

	public LFrameworkRequestAreaXz0x90r0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzy9r0qPolNbr(String xzy9r0qPolNbr) {
		writeString(Pos.XZY9R0Q_POL_NBR, xzy9r0qPolNbr, Len.XZY9R0Q_POL_NBR);
	}

	/**Original name: XZY9R0Q-POL-NBR<br>*/
	public String getXzy9r0qPolNbr() {
		return readString(Pos.XZY9R0Q_POL_NBR, Len.XZY9R0Q_POL_NBR);
	}

	public void setXzy9r0qPolEffDt(String xzy9r0qPolEffDt) {
		writeString(Pos.XZY9R0Q_POL_EFF_DT, xzy9r0qPolEffDt, Len.XZY9R0Q_POL_EFF_DT);
	}

	/**Original name: XZY9R0Q-POL-EFF-DT<br>*/
	public String getXzy9r0qPolEffDt() {
		return readString(Pos.XZY9R0Q_POL_EFF_DT, Len.XZY9R0Q_POL_EFF_DT);
	}

	public void setXzy9r0qPolExpDt(String xzy9r0qPolExpDt) {
		writeString(Pos.XZY9R0Q_POL_EXP_DT, xzy9r0qPolExpDt, Len.XZY9R0Q_POL_EXP_DT);
	}

	/**Original name: XZY9R0Q-POL-EXP-DT<br>*/
	public String getXzy9r0qPolExpDt() {
		return readString(Pos.XZY9R0Q_POL_EXP_DT, Len.XZY9R0Q_POL_EXP_DT);
	}

	public void setXzy9r0qUserid(String xzy9r0qUserid) {
		writeString(Pos.XZY9R0Q_USERID, xzy9r0qUserid, Len.XZY9R0Q_USERID);
	}

	/**Original name: XZY9R0Q-USERID<br>*/
	public String getXzy9r0qUserid() {
		return readString(Pos.XZY9R0Q_USERID, Len.XZY9R0Q_USERID);
	}

	public void setXzy9r0qCdPndCncInd(char xzy9r0qCdPndCncInd) {
		writeChar(Pos.XZY9R0Q_CD_PND_CNC_IND, xzy9r0qCdPndCncInd);
	}

	/**Original name: XZY9R0Q-CD-PND-CNC-IND<br>*/
	public char getXzy9r0qCdPndCncInd() {
		return readChar(Pos.XZY9R0Q_CD_PND_CNC_IND);
	}

	public void setXzy9r0qCdSchCncDt(String xzy9r0qCdSchCncDt) {
		writeString(Pos.XZY9R0Q_CD_SCH_CNC_DT, xzy9r0qCdSchCncDt, Len.XZY9R0Q_CD_SCH_CNC_DT);
	}

	/**Original name: XZY9R0Q-CD-SCH-CNC-DT<br>*/
	public String getXzy9r0qCdSchCncDt() {
		return readString(Pos.XZY9R0Q_CD_SCH_CNC_DT, Len.XZY9R0Q_CD_SCH_CNC_DT);
	}

	public void setXzy9r0qCdNotTypCd(String xzy9r0qCdNotTypCd) {
		writeString(Pos.XZY9R0Q_CD_NOT_TYP_CD, xzy9r0qCdNotTypCd, Len.XZY9R0Q_CD_NOT_TYP_CD);
	}

	/**Original name: XZY9R0Q-CD-NOT-TYP-CD<br>*/
	public String getXzy9r0qCdNotTypCd() {
		return readString(Pos.XZY9R0Q_CD_NOT_TYP_CD, Len.XZY9R0Q_CD_NOT_TYP_CD);
	}

	public void setXzy9r0qCdNotTypDes(String xzy9r0qCdNotTypDes) {
		writeString(Pos.XZY9R0Q_CD_NOT_TYP_DES, xzy9r0qCdNotTypDes, Len.XZY9R0Q_CD_NOT_TYP_DES);
	}

	/**Original name: XZY9R0Q-CD-NOT-TYP-DES<br>*/
	public String getXzy9r0qCdNotTypDes() {
		return readString(Pos.XZY9R0Q_CD_NOT_TYP_DES, Len.XZY9R0Q_CD_NOT_TYP_DES);
	}

	public void setXzy9r0qCdNotEmpId(String xzy9r0qCdNotEmpId) {
		writeString(Pos.XZY9R0Q_CD_NOT_EMP_ID, xzy9r0qCdNotEmpId, Len.XZY9R0Q_CD_NOT_EMP_ID);
	}

	/**Original name: XZY9R0Q-CD-NOT-EMP-ID<br>*/
	public String getXzy9r0qCdNotEmpId() {
		return readString(Pos.XZY9R0Q_CD_NOT_EMP_ID, Len.XZY9R0Q_CD_NOT_EMP_ID);
	}

	public void setXzy9r0qCdNotEmpNm(String xzy9r0qCdNotEmpNm) {
		writeString(Pos.XZY9R0Q_CD_NOT_EMP_NM, xzy9r0qCdNotEmpNm, Len.XZY9R0Q_CD_NOT_EMP_NM);
	}

	/**Original name: XZY9R0Q-CD-NOT-EMP-NM<br>*/
	public String getXzy9r0qCdNotEmpNm() {
		return readString(Pos.XZY9R0Q_CD_NOT_EMP_NM, Len.XZY9R0Q_CD_NOT_EMP_NM);
	}

	public void setXzy9r0qCdNotPrcDt(String xzy9r0qCdNotPrcDt) {
		writeString(Pos.XZY9R0Q_CD_NOT_PRC_DT, xzy9r0qCdNotPrcDt, Len.XZY9R0Q_CD_NOT_PRC_DT);
	}

	/**Original name: XZY9R0Q-CD-NOT-PRC-DT<br>*/
	public String getXzy9r0qCdNotPrcDt() {
		return readString(Pos.XZY9R0Q_CD_NOT_PRC_DT, Len.XZY9R0Q_CD_NOT_PRC_DT);
	}

	public void setXzy9r0qTdUwTmnFlg(char xzy9r0qTdUwTmnFlg) {
		writeChar(Pos.XZY9R0Q_TD_UW_TMN_FLG, xzy9r0qTdUwTmnFlg);
	}

	/**Original name: XZY9R0Q-TD-UW-TMN-FLG<br>*/
	public char getXzy9r0qTdUwTmnFlg() {
		return readChar(Pos.XZY9R0Q_TD_UW_TMN_FLG);
	}

	public void setXzy9r0qTdTmnEmpId(String xzy9r0qTdTmnEmpId) {
		writeString(Pos.XZY9R0Q_TD_TMN_EMP_ID, xzy9r0qTdTmnEmpId, Len.XZY9R0Q_TD_TMN_EMP_ID);
	}

	/**Original name: XZY9R0Q-TD-TMN-EMP-ID<br>*/
	public String getXzy9r0qTdTmnEmpId() {
		return readString(Pos.XZY9R0Q_TD_TMN_EMP_ID, Len.XZY9R0Q_TD_TMN_EMP_ID);
	}

	public void setXzy9r0qTdTmnEmpNm(String xzy9r0qTdTmnEmpNm) {
		writeString(Pos.XZY9R0Q_TD_TMN_EMP_NM, xzy9r0qTdTmnEmpNm, Len.XZY9R0Q_TD_TMN_EMP_NM);
	}

	/**Original name: XZY9R0Q-TD-TMN-EMP-NM<br>*/
	public String getXzy9r0qTdTmnEmpNm() {
		return readString(Pos.XZY9R0Q_TD_TMN_EMP_NM, Len.XZY9R0Q_TD_TMN_EMP_NM);
	}

	public void setXzy9r0qTdTmnPrcDt(String xzy9r0qTdTmnPrcDt) {
		writeString(Pos.XZY9R0Q_TD_TMN_PRC_DT, xzy9r0qTdTmnPrcDt, Len.XZY9R0Q_TD_TMN_PRC_DT);
	}

	/**Original name: XZY9R0Q-TD-TMN-PRC-DT<br>*/
	public String getXzy9r0qTdTmnPrcDt() {
		return readString(Pos.XZY9R0Q_TD_TMN_PRC_DT, Len.XZY9R0Q_TD_TMN_PRC_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0Y90R0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZY9R0Q_GET_ADD_POL_INFO_ROW = L_FW_REQ_XZ0Y90R0;
		public static final int XZY9R0Q_GET_INFO_INPUT_ROW = XZY9R0Q_GET_ADD_POL_INFO_ROW;
		public static final int XZY9R0Q_POL_NBR = XZY9R0Q_GET_INFO_INPUT_ROW;
		public static final int XZY9R0Q_POL_EFF_DT = XZY9R0Q_POL_NBR + Len.XZY9R0Q_POL_NBR;
		public static final int XZY9R0Q_POL_EXP_DT = XZY9R0Q_POL_EFF_DT + Len.XZY9R0Q_POL_EFF_DT;
		public static final int XZY9R0Q_USERID = XZY9R0Q_POL_EXP_DT + Len.XZY9R0Q_POL_EXP_DT;
		public static final int FLR1 = XZY9R0Q_USERID + Len.XZY9R0Q_USERID;
		public static final int XZY9R0Q_GET_INFO_OUTPUT_ROW = FLR1 + Len.FLR1;
		public static final int XZY9R0Q_CNC_DATA = XZY9R0Q_GET_INFO_OUTPUT_ROW;
		public static final int XZY9R0Q_CD_PND_CNC_IND = XZY9R0Q_CNC_DATA;
		public static final int XZY9R0Q_CD_SCH_CNC_DT = XZY9R0Q_CD_PND_CNC_IND + Len.XZY9R0Q_CD_PND_CNC_IND;
		public static final int XZY9R0Q_CD_NOT_TYP_CD = XZY9R0Q_CD_SCH_CNC_DT + Len.XZY9R0Q_CD_SCH_CNC_DT;
		public static final int XZY9R0Q_CD_NOT_TYP_DES = XZY9R0Q_CD_NOT_TYP_CD + Len.XZY9R0Q_CD_NOT_TYP_CD;
		public static final int XZY9R0Q_CD_NOT_EMP_ID = XZY9R0Q_CD_NOT_TYP_DES + Len.XZY9R0Q_CD_NOT_TYP_DES;
		public static final int XZY9R0Q_CD_NOT_EMP_NM = XZY9R0Q_CD_NOT_EMP_ID + Len.XZY9R0Q_CD_NOT_EMP_ID;
		public static final int XZY9R0Q_CD_NOT_PRC_DT = XZY9R0Q_CD_NOT_EMP_NM + Len.XZY9R0Q_CD_NOT_EMP_NM;
		public static final int XZY9R0Q_TMN_DATA = XZY9R0Q_CD_NOT_PRC_DT + Len.XZY9R0Q_CD_NOT_PRC_DT;
		public static final int XZY9R0Q_TD_UW_TMN_FLG = XZY9R0Q_TMN_DATA;
		public static final int XZY9R0Q_TD_TMN_EMP_ID = XZY9R0Q_TD_UW_TMN_FLG + Len.XZY9R0Q_TD_UW_TMN_FLG;
		public static final int XZY9R0Q_TD_TMN_EMP_NM = XZY9R0Q_TD_TMN_EMP_ID + Len.XZY9R0Q_TD_TMN_EMP_ID;
		public static final int XZY9R0Q_TD_TMN_PRC_DT = XZY9R0Q_TD_TMN_EMP_NM + Len.XZY9R0Q_TD_TMN_EMP_NM;
		public static final int FLR2 = XZY9R0Q_TD_TMN_PRC_DT + Len.XZY9R0Q_TD_TMN_PRC_DT;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY9R0Q_POL_NBR = 9;
		public static final int XZY9R0Q_POL_EFF_DT = 10;
		public static final int XZY9R0Q_POL_EXP_DT = 10;
		public static final int XZY9R0Q_USERID = 8;
		public static final int FLR1 = 100;
		public static final int XZY9R0Q_CD_PND_CNC_IND = 1;
		public static final int XZY9R0Q_CD_SCH_CNC_DT = 10;
		public static final int XZY9R0Q_CD_NOT_TYP_CD = 5;
		public static final int XZY9R0Q_CD_NOT_TYP_DES = 35;
		public static final int XZY9R0Q_CD_NOT_EMP_ID = 6;
		public static final int XZY9R0Q_CD_NOT_EMP_NM = 67;
		public static final int XZY9R0Q_CD_NOT_PRC_DT = 10;
		public static final int XZY9R0Q_TD_UW_TMN_FLG = 1;
		public static final int XZY9R0Q_TD_TMN_EMP_ID = 6;
		public static final int XZY9R0Q_TD_TMN_EMP_NM = 67;
		public static final int XZY9R0Q_TD_TMN_PRC_DT = 10;
		public static final int XZY9R0Q_GET_INFO_INPUT_ROW = XZY9R0Q_POL_NBR + XZY9R0Q_POL_EFF_DT + XZY9R0Q_POL_EXP_DT + XZY9R0Q_USERID + FLR1;
		public static final int XZY9R0Q_CNC_DATA = XZY9R0Q_CD_PND_CNC_IND + XZY9R0Q_CD_SCH_CNC_DT + XZY9R0Q_CD_NOT_TYP_CD + XZY9R0Q_CD_NOT_TYP_DES
				+ XZY9R0Q_CD_NOT_EMP_ID + XZY9R0Q_CD_NOT_EMP_NM + XZY9R0Q_CD_NOT_PRC_DT;
		public static final int XZY9R0Q_TMN_DATA = XZY9R0Q_TD_UW_TMN_FLG + XZY9R0Q_TD_TMN_EMP_ID + XZY9R0Q_TD_TMN_EMP_NM + XZY9R0Q_TD_TMN_PRC_DT;
		public static final int FLR2 = 800;
		public static final int XZY9R0Q_GET_INFO_OUTPUT_ROW = XZY9R0Q_CNC_DATA + XZY9R0Q_TMN_DATA + FLR2;
		public static final int XZY9R0Q_GET_ADD_POL_INFO_ROW = XZY9R0Q_GET_INFO_INPUT_ROW + XZY9R0Q_GET_INFO_OUTPUT_ROW;
		public static final int L_FW_REQ_XZ0Y90R0 = XZY9R0Q_GET_ADD_POL_INFO_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0Y90R0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
