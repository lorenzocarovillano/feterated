/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-BDO-BUSINESS-OBJECT-NAMES<br>
 * Variable: CF-BDO-BUSINESS-OBJECT-NAMES from program MU0R0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfBdoBusinessObjectNames {

	//==== PROPERTIES ====
	//Original name: CF-BUS-OBJ-CLT-ADR-BEST-BSM
	private String busObjCltAdrBestBsm = "CLIENT_ADDRESS_BEST_BSM";
	//Original name: CF-BUS-OBJ-CLT-ADR-BEST-BSL
	private String busObjCltAdrBestBsl = "CLIENT_ADDRESS_BEST_BSL";
	//Original name: CF-BUS-OBJ-CLT-OBJ-REL-BDO
	private String busObjCltObjRelBdo = "CLT_OBJ_RELATION_V";
	//Original name: CF-BUS-OBJ-CLT-OBJ-REL-BDO-OWN
	private String busObjCltObjRelBdoOwn = "CLT_OBJ_RELATION_OWNER";
	//Original name: CF-BUS-OBJ-CLT-UW-BPO
	private String busObjCltUwBpo = "CLT_UW_BPO";
	//Original name: CF-BO-REL-CLT-OBJ-REL-BPO
	private String boRelCltObjRelBpo = "CLT_OBJECT_RELATED_CLIENT_BPO";
	//Original name: CF-BO-CLT-CLT-REL-BDO
	private String boCltCltRelBdo = "CLT_CLT_RELATION_V";

	//==== METHODS ====
	public String getBusObjCltAdrBestBsm() {
		return this.busObjCltAdrBestBsm;
	}

	public String getBusObjCltAdrBestBsl() {
		return this.busObjCltAdrBestBsl;
	}

	public String getBusObjCltObjRelBdo() {
		return this.busObjCltObjRelBdo;
	}

	public String getBusObjCltObjRelBdoOwn() {
		return this.busObjCltObjRelBdoOwn;
	}

	public String getBusObjCltUwBpo() {
		return this.busObjCltUwBpo;
	}

	public String getBoRelCltObjRelBpo() {
		return this.boRelCltObjRelBpo;
	}

	public String getBoCltCltRelBdo() {
		return this.boCltCltRelBdo;
	}
}
