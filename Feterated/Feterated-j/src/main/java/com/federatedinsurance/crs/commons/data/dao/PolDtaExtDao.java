/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IPolDtaExt;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [POL_DTA_EXT]
 * 
 */
public class PolDtaExtDao extends BaseSqlDao<IPolDtaExt> {

	private final IRowMapper<IPolDtaExt> selectRec2Rm = buildNamedRowMapper(IPolDtaExt.class, "olTypCd", "riRskStAbb", "lnExpDt");
	private final IRowMapper<IPolDtaExt> selectRec4Rm = buildNamedRowMapper(IPolDtaExt.class, "lnExpDt", "actNbr");
	private final IRowMapper<IPolDtaExt> selectRec9Rm = buildNamedRowMapper(IPolDtaExt.class, "olTypCd", "riRskStAbb");
	private final IRowMapper<IPolDtaExt> selectByRRm = buildNamedRowMapper(IPolDtaExt.class, "polEffDt", "lnExpDt", "actNbr");

	public PolDtaExtDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IPolDtaExt> getToClass() {
		return IPolDtaExt.class;
	}

	public String selectRec(String nbr, String effDt, String dft) {
		return buildQuery("selectRec").bind("nbr", nbr).bind("effDt", effDt).scalarResultString(dft);
	}

	public String selectRec1(String polNbr, String wsCurDt, String dft) {
		return buildQuery("selectRec1").bind("polNbr", polNbr).bind("wsCurDt", wsCurDt).scalarResultString(dft);
	}

	public IPolDtaExt selectRec2(String nbr, String effDt, IPolDtaExt iPolDtaExt) {
		return buildQuery("selectRec2").bind("nbr", nbr).bind("effDt", effDt).rowMapper(selectRec2Rm).singleResult(iPolDtaExt);
	}

	public String selectByWsPolNbr(String wsPolNbr, String dft) {
		return buildQuery("selectByWsPolNbr").bind("wsPolNbr", wsPolNbr).scalarResultString(dft);
	}

	public IPolDtaExt selectRec3(String olNbr, String olEffDt, String lnExpDt, IPolDtaExt iPolDtaExt) {
		return buildQuery("selectRec3").bind("olNbr", olNbr).bind("olEffDt", olEffDt).bind("lnExpDt", lnExpDt).singleResult(iPolDtaExt);
	}

	public IPolDtaExt selectRec4(String nbr, String effDt, IPolDtaExt iPolDtaExt) {
		return buildQuery("selectRec4").bind("nbr", nbr).bind("effDt", effDt).rowMapper(selectRec4Rm).singleResult(iPolDtaExt);
	}

	public IPolDtaExt selectRec5(String olNbr, String lnExpDt, IPolDtaExt iPolDtaExt) {
		return buildQuery("selectRec5").bind("olNbr", olNbr).bind("lnExpDt", lnExpDt).singleResult(iPolDtaExt);
	}

	public String selectRec6(String olNbr, String olEffDt, String lnExpDt, String dft) {
		return buildQuery("selectRec6").bind("olNbr", olNbr).bind("olEffDt", olEffDt).bind("lnExpDt", lnExpDt).scalarResultString(dft);
	}

	public IPolDtaExt selectRec7(String nbr, String effDt, IPolDtaExt iPolDtaExt) {
		return buildQuery("selectRec7").bind("nbr", nbr).bind("effDt", effDt).rowMapper(selectRec4Rm).singleResult(iPolDtaExt);
	}

	public IPolDtaExt selectRec8(String olNbr, String lnExpDt, IPolDtaExt iPolDtaExt) {
		return buildQuery("selectRec8").bind("olNbr", olNbr).bind("lnExpDt", lnExpDt).singleResult(iPolDtaExt);
	}

	public IPolDtaExt selectRec9(String nbr, String effDt, IPolDtaExt iPolDtaExt) {
		return buildQuery("selectRec9").bind("nbr", nbr).bind("effDt", effDt).rowMapper(selectRec9Rm).singleResult(iPolDtaExt);
	}

	public String selectByWsPolNbr1(String wsPolNbr, String dft) {
		return buildQuery("selectByWsPolNbr1").bind("wsPolNbr", wsPolNbr).scalarResultString(dft);
	}

	public IPolDtaExt selectByR(String r, IPolDtaExt iPolDtaExt) {
		return buildQuery("selectByR").bind("r", r).rowMapper(selectByRRm).singleResult(iPolDtaExt);
	}
}
