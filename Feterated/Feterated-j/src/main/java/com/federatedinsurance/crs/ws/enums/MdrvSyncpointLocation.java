/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: MDRV-SYNCPOINT-LOCATION<br>
 * Variable: MDRV-SYNCPOINT-LOCATION from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvSyncpointLocation {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char INVOKER = Types.SPACE_CHAR;
	public static final char MAINDRVR = 'M';

	//==== METHODS ====
	public void setSyncpointLocation(char syncpointLocation) {
		this.value = syncpointLocation;
	}

	public void setSyncpointLocationFormatted(String syncpointLocation) {
		setSyncpointLocation(Functions.charAt(syncpointLocation, Types.CHAR_SIZE));
	}

	public char getSyncpointLocation() {
		return this.value;
	}

	public void setInvoker() {
		setSyncpointLocationFormatted(String.valueOf(INVOKER));
	}

	public boolean isMaindrvr() {
		return value == MAINDRVR;
	}

	public void setMaindrvr() {
		value = MAINDRVR;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SYNCPOINT_LOCATION = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
