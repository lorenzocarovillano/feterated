/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90I1-ROW<br>
 * Variable: WS-XZ0A90I1-ROW from program XZ0B90I0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90i1Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9I1-FRM-SEQ-NBR
	private int frmSeqNbr = DefaultValues.INT_VAL;
	//Original name: XZA9I1-REC-SEQ-NBR
	private int recSeqNbr = DefaultValues.INT_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90I1_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90i1RowBytes(buf);
	}

	public String getWsXz0a90i1RowFormatted() {
		return getGetFrmRecListDtlFormatted();
	}

	public void setWsXz0a90i1RowBytes(byte[] buffer) {
		setWsXz0a90i1RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90i1RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90I1_ROW];
		return getWsXz0a90i1RowBytes(buffer, 1);
	}

	public void setWsXz0a90i1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetFrmRecListDtlBytes(buffer, position);
	}

	public byte[] getWsXz0a90i1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetFrmRecListDtlBytes(buffer, position);
		return buffer;
	}

	public String getGetFrmRecListDtlFormatted() {
		return MarshalByteExt.bufferToStr(getGetFrmRecListDtlBytes());
	}

	/**Original name: XZA9I1-GET-FRM-REC-LIST-DTL<br>
	 * <pre>*************************************************************
	 *  XZ0A90I1 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_FRM_REC_LIST                       *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  04MAR2009 E404KXS    NEW                          *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetFrmRecListDtlBytes() {
		byte[] buffer = new byte[Len.GET_FRM_REC_LIST_DTL];
		return getGetFrmRecListDtlBytes(buffer, 1);
	}

	public void setGetFrmRecListDtlBytes(byte[] buffer, int offset) {
		int position = offset;
		frmSeqNbr = MarshalByte.readInt(buffer, position, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		recSeqNbr = MarshalByte.readInt(buffer, position, Len.REC_SEQ_NBR);
	}

	public byte[] getGetFrmRecListDtlBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeInt(buffer, position, frmSeqNbr, Len.FRM_SEQ_NBR);
		position += Len.FRM_SEQ_NBR;
		MarshalByte.writeInt(buffer, position, recSeqNbr, Len.REC_SEQ_NBR);
		return buffer;
	}

	public void setFrmSeqNbr(int frmSeqNbr) {
		this.frmSeqNbr = frmSeqNbr;
	}

	public int getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	public void setRecSeqNbr(int recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	public int getRecSeqNbr() {
		return this.recSeqNbr;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90i1RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FRM_SEQ_NBR = 5;
		public static final int REC_SEQ_NBR = 5;
		public static final int GET_FRM_REC_LIST_DTL = FRM_SEQ_NBR + REC_SEQ_NBR;
		public static final int WS_XZ0A90I1_ROW = GET_FRM_REC_LIST_DTL;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
