/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-07-INVLD-RPT-OFFICE-DST-MSG<br>
 * Variable: EA-07-INVLD-RPT-OFFICE-DST-MSG from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea07InvldRptOfficeDstMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-07-INVLD-RPT-OFFICE-DST-MSG
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-07-INVLD-RPT-OFFICE-DST-MSG-1
	private String flr2 = "TS030099 -";
	//Original name: FILLER-EA-07-INVLD-RPT-OFFICE-DST-MSG-2
	private String flr3 = "INVALID REPORT";
	//Original name: FILLER-EA-07-INVLD-RPT-OFFICE-DST-MSG-3
	private String flr4 = "DESTINATION:";
	//Original name: EA-07-OFFICE-DESTINATION-FLAG
	private char officeDestinationFlag = DefaultValues.CHAR_VAL;
	//Original name: FILLER-EA-07-INVLD-RPT-OFFICE-DST-MSG-4
	private String flr5 = ", REPORT:";
	//Original name: EA-07-REPORT-NUMBER
	private String reportNumber = DefaultValues.stringVal(Len.REPORT_NUMBER);
	//Original name: FILLER-EA-07-INVLD-RPT-OFFICE-DST-MSG-5
	private String flr6 = ", LOCATION:";
	//Original name: EA-07-OFFICE-LOCATION
	private String officeLocation = DefaultValues.stringVal(Len.OFFICE_LOCATION);
	public static final String DEFAULT_OFFICE_LOCATION = "DEFAULT";

	//==== METHODS ====
	public String getEa07InvldRptOfficeDstMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa07InvldRptOfficeDstMsgBytes());
	}

	public byte[] getEa07InvldRptOfficeDstMsgBytes() {
		byte[] buffer = new byte[Len.EA07_INVLD_RPT_OFFICE_DST_MSG];
		return getEa07InvldRptOfficeDstMsgBytes(buffer, 1);
	}

	public byte[] getEa07InvldRptOfficeDstMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeChar(buffer, position, officeDestinationFlag);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, reportNumber, Len.REPORT_NUMBER);
		position += Len.REPORT_NUMBER;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, officeLocation, Len.OFFICE_LOCATION);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setOfficeDestinationFlag(char officeDestinationFlag) {
		this.officeDestinationFlag = officeDestinationFlag;
	}

	public char getOfficeDestinationFlag() {
		return this.officeDestinationFlag;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setReportNumber(String reportNumber) {
		this.reportNumber = Functions.subString(reportNumber, Len.REPORT_NUMBER);
	}

	public String getReportNumber() {
		return this.reportNumber;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setOfficeLocation(String officeLocation) {
		this.officeLocation = Functions.subString(officeLocation, Len.OFFICE_LOCATION);
	}

	public String getOfficeLocation() {
		return this.officeLocation;
	}

	public void setDefaultOfficeLocation() {
		officeLocation = DEFAULT_OFFICE_LOCATION;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REPORT_NUMBER = 6;
		public static final int OFFICE_LOCATION = 7;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 15;
		public static final int FLR4 = 13;
		public static final int OFFICE_DESTINATION_FLAG = 1;
		public static final int FLR5 = 10;
		public static final int FLR6 = 12;
		public static final int EA07_INVLD_RPT_OFFICE_DST_MSG = OFFICE_DESTINATION_FLAG + REPORT_NUMBER + OFFICE_LOCATION + FLR1 + FLR2 + FLR3 + FLR4
				+ FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
