/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: PST-BRNCH-PROC-TYP<br>
 * Variable: PST-BRNCH-PROC-TYP from copybook HALLPST<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class PstBrnchProcTyp {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char EDIT_BRANCH = 'E';
	public static final char DATA_PROC_BRANCH = 'D';
	public static final char ADDL_PROC_BRANCH = 'A';

	//==== METHODS ====
	public void setBrnchProcTyp(char brnchProcTyp) {
		this.value = brnchProcTyp;
	}

	public char getBrnchProcTyp() {
		return this.value;
	}

	public void setPstDataProcBranch() {
		value = DATA_PROC_BRANCH;
	}
}
