/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-NOT-TYP-CD<br>
 * Variable: SA-NOT-TYP-CD from program XZ004000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaNotTypCd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.SA_NOT_TYP_CD);
	public static final String CNC_TYP = "IMP";
	public static final String REN_TYP = "RES";
	private static final String[] RPT_TRS = new String[] { "IMP", "RES" };

	//==== METHODS ====
	public void setSaNotTypCd(String saNotTypCd) {
		this.value = Functions.subString(saNotTypCd, Len.SA_NOT_TYP_CD);
	}

	public String getSaNotTypCd() {
		return this.value;
	}

	public boolean isRenTyp() {
		return value.equals(REN_TYP);
	}

	public boolean isRptTrs() {
		return ArrayUtils.contains(RPT_TRS, value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_NOT_TYP_CD = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
