/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-PLACEHOLDER<br>
 * Variable: WS-PLACEHOLDER from program HALRPLAC<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsPlaceholder extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int WS_PLACEHOLDER_X_MAXOCCURS = 500;
	/**Original name: WS-PLACEHOLDER-X<br>
	 * <pre>                                     OCCURS 50
	 *  C14849                              OCCURS 100</pre>*/
	private char[] wsPlaceholderX = new char[WS_PLACEHOLDER_X_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public WsPlaceholder() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.PLACEHOLDER;
	}

	@Override
	public void deserialize(byte[] buf) {
		setPlaceholderBytes(buf);
	}

	public void init() {
		for (int wsPlaceholderXIdx = 1; wsPlaceholderXIdx <= WS_PLACEHOLDER_X_MAXOCCURS; wsPlaceholderXIdx++) {
			setWsPlaceholderX(wsPlaceholderXIdx, DefaultValues.CHAR_VAL);
		}
	}

	public void setPlaceholderFormatted(String data) {
		byte[] buffer = new byte[Len.PLACEHOLDER];
		MarshalByte.writeString(buffer, 1, data, Len.PLACEHOLDER);
		setPlaceholderBytes(buffer, 1);
	}

	public String getPlaceholderFormatted() {
		return MarshalByteExt.bufferToStr(getPlaceholderBytes());
	}

	public void setPlaceholderBytes(byte[] buffer) {
		setPlaceholderBytes(buffer, 1);
	}

	public byte[] getPlaceholderBytes() {
		byte[] buffer = new byte[Len.PLACEHOLDER];
		return getPlaceholderBytes(buffer, 1);
	}

	public void setPlaceholderBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= WS_PLACEHOLDER_X_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setWsPlaceholderX(idx, MarshalByte.readChar(buffer, position));
				position += Types.CHAR_SIZE;
			} else {
				setWsPlaceholderX(idx, Types.SPACE_CHAR);
				position += Types.CHAR_SIZE;
			}
		}
	}

	public byte[] getPlaceholderBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= WS_PLACEHOLDER_X_MAXOCCURS; idx++) {
			MarshalByte.writeChar(buffer, position, getWsPlaceholderX(idx));
			position += Types.CHAR_SIZE;
		}
		return buffer;
	}

	public void setWsPlaceholderX(int wsPlaceholderXIdx, char wsPlaceholderX) {
		this.wsPlaceholderX[wsPlaceholderXIdx - 1] = wsPlaceholderX;
	}

	public char getWsPlaceholderX(int wsPlaceholderXIdx) {
		return this.wsPlaceholderX[wsPlaceholderXIdx - 1];
	}

	@Override
	public byte[] serialize() {
		return getPlaceholderBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_PLACEHOLDER_X = 1;
		public static final int PLACEHOLDER = WsPlaceholder.WS_PLACEHOLDER_X_MAXOCCURS * WS_PLACEHOLDER_X;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
