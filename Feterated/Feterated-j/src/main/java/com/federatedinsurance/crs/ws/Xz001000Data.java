/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.federatedinsurance.crs.commons.data.to.IActNotPolDtaExt;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.ws.enums.TpPolicyNbrs;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ001000<br>
 * Generated as a class for rule WS.<br>*/
public class Xz001000Data implements IActNotPolDtaExt {

	//==== PROPERTIES ====
	public static final int TP_POLICY_NBRS_MAXOCCURS = 10;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz001000 constantFields = new ConstantFieldsXz001000();
	//Original name: DUMP-PARAMETERS
	private DumpParameters dumpParameters = new DumpParameters();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXz001000 errorAndAdviceMessages = new ErrorAndAdviceMessagesXz001000();
	//Original name: PRINT-LINE
	private PrintLine printLine = new PrintLine();
	//Original name: FILLER-RP-PRTR
	private String flr1 = "    PRTR";
	//Original name: SAVE-AREA
	private SaveAreaXz001000 saveArea = new SaveAreaXz001000();
	//Original name: SUBPROGRAM-INFO
	private SubprogramInfo subprogramInfo = new SubprogramInfo();
	//Original name: SS-ATB-IDX
	private short ssAtbIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short SS_ATB_IDX_MAX = ((short) 100);
	//Original name: SS-TXT-POINTER
	private short ssTxtPointer = DefaultValues.BIN_SHORT_VAL;
	//Original name: SWITCHES
	private SwitchesXz001000 switches = new SwitchesXz001000();
	//Original name: TP-POLICY-NBRS
	private TpPolicyNbrs[] tpPolicyNbrs = new TpPolicyNbrs[TP_POLICY_NBRS_MAXOCCURS];
	//Original name: IX-TP
	private int ixTp = 1;
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();

	//==== CONSTRUCTORS ====
	public Xz001000Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int tpPolicyNbrsIdx = 1; tpPolicyNbrsIdx <= TP_POLICY_NBRS_MAXOCCURS; tpPolicyNbrsIdx++) {
			tpPolicyNbrs[tpPolicyNbrsIdx - 1] = new TpPolicyNbrs();
		}
	}

	/**Original name: RP-PRTR<br>*/
	public byte[] getRpPrtrBytes() {
		byte[] buffer = new byte[Len.RP_PRTR];
		return getRpPrtrBytes(buffer, 1);
	}

	public byte[] getRpPrtrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setSsAtbIdx(short ssAtbIdx) {
		this.ssAtbIdx = ssAtbIdx;
	}

	public short getSsAtbIdx() {
		return this.ssAtbIdx;
	}

	public void setSsTxtPointer(short ssTxtPointer) {
		this.ssTxtPointer = ssTxtPointer;
	}

	public short getSsTxtPointer() {
		return this.ssTxtPointer;
	}

	public void initTableOfPolicyNbrsHighValues() {
		for (int idx = 1; idx <= TP_POLICY_NBRS_MAXOCCURS; idx++) {
			tpPolicyNbrs[idx - 1].initTpPolicyNbrsHighValues();
		}
	}

	public void setIxTp(int ixTp) {
		this.ixTp = ixTp;
	}

	public int getIxTp() {
		return this.ixTp;
	}

	@Override
	public String getActTypCd() {
		return dclactNot.getActTypCd();
	}

	@Override
	public void setActTypCd(String actTypCd) {
		this.dclactNot.setActTypCd(actTypCd);
	}

	@Override
	public String getCfActNotImpending() {
		return constantFields.getActNotTypCd().getImpending();
	}

	@Override
	public void setCfActNotImpending(String cfActNotImpending) {
		this.constantFields.getActNotTypCd().setImpending(cfActNotImpending);
	}

	@Override
	public String getCfActNotNonpay() {
		return constantFields.getActNotTypCd().getNonpay();
	}

	@Override
	public void setCfActNotNonpay(String cfActNotNonpay) {
		this.constantFields.getActNotTypCd().setNonpay(cfActNotNonpay);
	}

	@Override
	public String getCfActNotRescind() {
		return constantFields.getActNotTypCd().getRescind();
	}

	@Override
	public void setCfActNotRescind(String cfActNotRescind) {
		this.constantFields.getActNotTypCd().setRescind(cfActNotRescind);
	}

	@Override
	public String getCfAddedByBusinessWorks() {
		return constantFields.getAddedByBusinessWorks();
	}

	@Override
	public void setCfAddedByBusinessWorks(String cfAddedByBusinessWorks) {
		this.constantFields.setAddedByBusinessWorks(cfAddedByBusinessWorks);
	}

	public ConstantFieldsXz001000 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DumpParameters getDumpParameters() {
		return dumpParameters;
	}

	public ErrorAndAdviceMessagesXz001000 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	@Override
	public String getNotEffDt() {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		throw new FieldNotMappedException("notEffDt");
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getPolEffDt() {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public String getPolNbr() {
		throw new FieldNotMappedException("polNbr");
	}

	@Override
	public void setPolNbr(String polNbr) {
		throw new FieldNotMappedException("polNbr");
	}

	public PrintLine getPrintLine() {
		return printLine;
	}

	public SaveAreaXz001000 getSaveArea() {
		return saveArea;
	}

	public SubprogramInfo getSubprogramInfo() {
		return subprogramInfo;
	}

	public SwitchesXz001000 getSwitches() {
		return switches;
	}

	public TpPolicyNbrs getTpPolicyNbrs(int idx) {
		return tpPolicyNbrs[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 12;
		public static final int RP_PRTR = FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
