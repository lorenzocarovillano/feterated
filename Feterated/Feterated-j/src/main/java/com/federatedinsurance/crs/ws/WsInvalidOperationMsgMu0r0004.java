/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-INVALID-OPERATION-MSG<br>
 * Variable: WS-INVALID-OPERATION-MSG from program MU0R0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsInvalidOperationMsgMu0r0004 {

	//==== PROPERTIES ====
	//Original name: FILLER-WS-INVALID-OPERATION-MSG
	private String flr1 = "MU0R0004 -";
	//Original name: FILLER-WS-INVALID-OPERATION-MSG-1
	private String flr2 = "INVALID OPER:";
	//Original name: WS-INVALID-OPERATION-NAME
	private String wsInvalidOperationName = DefaultValues.stringVal(Len.WS_INVALID_OPERATION_NAME);

	//==== METHODS ====
	public String getInvalidOperationMsgFormatted() {
		return MarshalByteExt.bufferToStr(getInvalidOperationMsgBytes());
	}

	public byte[] getInvalidOperationMsgBytes() {
		byte[] buffer = new byte[Len.INVALID_OPERATION_MSG];
		return getInvalidOperationMsgBytes(buffer, 1);
	}

	public byte[] getInvalidOperationMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, wsInvalidOperationName, Len.WS_INVALID_OPERATION_NAME);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setWsInvalidOperationName(String wsInvalidOperationName) {
		this.wsInvalidOperationName = Functions.subString(wsInvalidOperationName, Len.WS_INVALID_OPERATION_NAME);
	}

	public String getWsInvalidOperationName() {
		return this.wsInvalidOperationName;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_INVALID_OPERATION_NAME = 32;
		public static final int FLR1 = 11;
		public static final int FLR2 = 14;
		public static final int INVALID_OPERATION_MSG = WS_INVALID_OPERATION_NAME + FLR1 + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
