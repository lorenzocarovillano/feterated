/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ08CI1I<br>
 * Variable: XZ08CI1I from copybook XZ08CI1I<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz08ci1i {

	//==== PROPERTIES ====
	//Original name: XZ08CI-URI
	private String uri = DefaultValues.stringVal(Len.URI);
	//Original name: XZ08CI-USR-ID
	private String usrId = DefaultValues.stringVal(Len.USR_ID);
	//Original name: XZ08CI-PWD
	private String pwd = DefaultValues.stringVal(Len.PWD);
	//Original name: XZ08CI-TAR-SYS
	private String tarSys = DefaultValues.stringVal(Len.TAR_SYS);
	//Original name: XZ08CI-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZ08CI-TRM-EXP-DT
	private String trmExpDt = DefaultValues.stringVal(Len.TRM_EXP_DT);

	//==== METHODS ====
	public void initXz08ci1iLowValues() {
		initServiceInputsLowValues();
	}

	public byte[] getServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, uri, Len.URI);
		position += Len.URI;
		MarshalByte.writeString(buffer, position, usrId, Len.USR_ID);
		position += Len.USR_ID;
		MarshalByte.writeString(buffer, position, pwd, Len.PWD);
		position += Len.PWD;
		MarshalByte.writeString(buffer, position, tarSys, Len.TAR_SYS);
		position += Len.TAR_SYS;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, trmExpDt, Len.TRM_EXP_DT);
		return buffer;
	}

	public void initServiceInputsLowValues() {
		uri = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.URI);
		usrId = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.USR_ID);
		pwd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.PWD);
		tarSys = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.TAR_SYS);
		polNbr = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.POL_NBR);
		trmExpDt = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.TRM_EXP_DT);
	}

	public void setUri(String uri) {
		this.uri = Functions.subString(uri, Len.URI);
	}

	public String getUri() {
		return this.uri;
	}

	public void setUsrId(String usrId) {
		this.usrId = Functions.subString(usrId, Len.USR_ID);
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setPwd(String pwd) {
		this.pwd = Functions.subString(pwd, Len.PWD);
	}

	public String getPwd() {
		return this.pwd;
	}

	public void setTarSys(String tarSys) {
		this.tarSys = Functions.subString(tarSys, Len.TAR_SYS);
	}

	public String getTarSys() {
		return this.tarSys;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setTrmExpDt(String trmExpDt) {
		this.trmExpDt = Functions.subString(trmExpDt, Len.TRM_EXP_DT);
	}

	public String getTrmExpDt() {
		return this.trmExpDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int URI = 256;
		public static final int USR_ID = 8;
		public static final int PWD = 8;
		public static final int TAR_SYS = 5;
		public static final int POL_NBR = 25;
		public static final int TRM_EXP_DT = 10;
		public static final int SERVICE_INPUTS = URI + USR_ID + PWD + TAR_SYS + POL_NBR + TRM_EXP_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
