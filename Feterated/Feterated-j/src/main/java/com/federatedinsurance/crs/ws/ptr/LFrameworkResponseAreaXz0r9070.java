/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9070<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9070 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9070_MAXOCCURS = 2000;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9070() {
	}

	public LFrameworkResponseAreaXz0r9070(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a9070Formatted(int lFwRespXz0a9070Idx) {
		int position = Pos.lFwRespXz0a9070(lFwRespXz0a9070Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9070);
	}

	public void setlFwRespXz0a9070Bytes(int lFwRespXz0a9070Idx, byte[] buffer) {
		setlFwRespXz0a9070Bytes(lFwRespXz0a9070Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A9070<br>*/
	public byte[] getlFwRespXz0a9070Bytes(int lFwRespXz0a9070Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_XZ0A9070];
		return getlFwRespXz0a9070Bytes(lFwRespXz0a9070Idx, buffer, 1);
	}

	public void setlFwRespXz0a9070Bytes(int lFwRespXz0a9070Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9070(lFwRespXz0a9070Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A9070, position);
	}

	public byte[] getlFwRespXz0a9070Bytes(int lFwRespXz0a9070Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a9070(lFwRespXz0a9070Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A9070, position);
		return buffer;
	}

	public void setXza970rMaxFrmRows(int xza970rMaxFrmRowsIdx, short xza970rMaxFrmRows) {
		int position = Pos.xza970MaxFrmRows(xza970rMaxFrmRowsIdx - 1);
		writeBinaryShort(position, xza970rMaxFrmRows);
	}

	/**Original name: XZA970R-MAX-FRM-ROWS<br>*/
	public short getXza970rMaxFrmRows(int xza970rMaxFrmRowsIdx) {
		int position = Pos.xza970MaxFrmRows(xza970rMaxFrmRowsIdx - 1);
		return readBinaryShort(position);
	}

	/**Original name: XZA970R-ACY-FRM-INFO<br>*/
	public byte[] getXza970rAcyFrmInfoBytes(int xza970rAcyFrmInfoIdx) {
		byte[] buffer = new byte[Len.XZA970_ACY_FRM_INFO];
		return getXza970rAcyFrmInfoBytes(xza970rAcyFrmInfoIdx, buffer, 1);
	}

	public byte[] getXza970rAcyFrmInfoBytes(int xza970rAcyFrmInfoIdx, byte[] buffer, int offset) {
		int position = Pos.xza970AcyFrmInfo(xza970rAcyFrmInfoIdx - 1);
		getBytes(buffer, offset, Len.XZA970_ACY_FRM_INFO, position);
		return buffer;
	}

	public void setXza970rFrmNbr(int xza970rFrmNbrIdx, String xza970rFrmNbr) {
		int position = Pos.xza970FrmNbr(xza970rFrmNbrIdx - 1);
		writeString(position, xza970rFrmNbr, Len.XZA970_FRM_NBR);
	}

	/**Original name: XZA970R-FRM-NBR<br>*/
	public String getXza970rFrmNbr(int xza970rFrmNbrIdx) {
		int position = Pos.xza970FrmNbr(xza970rFrmNbrIdx - 1);
		return readString(position, Len.XZA970_FRM_NBR);
	}

	public void setXza970rFrmEdtDt(int xza970rFrmEdtDtIdx, String xza970rFrmEdtDt) {
		int position = Pos.xza970FrmEdtDt(xza970rFrmEdtDtIdx - 1);
		writeString(position, xza970rFrmEdtDt, Len.XZA970_FRM_EDT_DT);
	}

	/**Original name: XZA970R-FRM-EDT-DT<br>*/
	public String getXza970rFrmEdtDt(int xza970rFrmEdtDtIdx) {
		int position = Pos.xza970FrmEdtDt(xza970rFrmEdtDtIdx - 1);
		return readString(position, Len.XZA970_FRM_EDT_DT);
	}

	public void setXza970rActNotTypCd(int xza970rActNotTypCdIdx, String xza970rActNotTypCd) {
		int position = Pos.xza970ActNotTypCd(xza970rActNotTypCdIdx - 1);
		writeString(position, xza970rActNotTypCd, Len.XZA970_ACT_NOT_TYP_CD);
	}

	/**Original name: XZA970R-ACT-NOT-TYP-CD<br>*/
	public String getXza970rActNotTypCd(int xza970rActNotTypCdIdx) {
		int position = Pos.xza970ActNotTypCd(xza970rActNotTypCdIdx - 1);
		return readString(position, Len.XZA970_ACT_NOT_TYP_CD);
	}

	public void setXza970rEdlFrmNm(int xza970rEdlFrmNmIdx, String xza970rEdlFrmNm) {
		int position = Pos.xza970EdlFrmNm(xza970rEdlFrmNmIdx - 1);
		writeString(position, xza970rEdlFrmNm, Len.XZA970_EDL_FRM_NM);
	}

	/**Original name: XZA970R-EDL-FRM-NM<br>*/
	public String getXza970rEdlFrmNm(int xza970rEdlFrmNmIdx) {
		int position = Pos.xza970EdlFrmNm(xza970rEdlFrmNmIdx - 1);
		return readString(position, Len.XZA970_EDL_FRM_NM);
	}

	public void setXza970rFrmDes(int xza970rFrmDesIdx, String xza970rFrmDes) {
		int position = Pos.xza970FrmDes(xza970rFrmDesIdx - 1);
		writeString(position, xza970rFrmDes, Len.XZA970_FRM_DES);
	}

	/**Original name: XZA970R-FRM-DES<br>*/
	public String getXza970rFrmDes(int xza970rFrmDesIdx) {
		int position = Pos.xza970FrmDes(xza970rFrmDesIdx - 1);
		return readString(position, Len.XZA970_FRM_DES);
	}

	public void setXza970rSpePrcCd(int xza970rSpePrcCdIdx, String xza970rSpePrcCd) {
		int position = Pos.xza970SpePrcCd(xza970rSpePrcCdIdx - 1);
		writeString(position, xza970rSpePrcCd, Len.XZA970_SPE_PRC_CD);
	}

	/**Original name: XZA970R-SPE-PRC-CD<br>*/
	public String getXza970rSpePrcCd(int xza970rSpePrcCdIdx) {
		int position = Pos.xza970SpePrcCd(xza970rSpePrcCdIdx - 1);
		return readString(position, Len.XZA970_SPE_PRC_CD);
	}

	public void setXza970rDtnCd(int xza970rDtnCdIdx, int xza970rDtnCd) {
		int position = Pos.xza970DtnCd(xza970rDtnCdIdx - 1);
		writeInt(position, xza970rDtnCd, Len.Int.XZA970R_DTN_CD);
	}

	/**Original name: XZA970R-DTN-CD<br>*/
	public int getXza970rDtnCd(int xza970rDtnCdIdx) {
		int position = Pos.xza970DtnCd(xza970rDtnCdIdx - 1);
		return readNumDispInt(position, Len.XZA970_DTN_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9070(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9070;
		}

		public static int xza970AcyFrmRow(int idx) {
			return lFwRespXz0a9070(idx);
		}

		public static int xza970MaxFrmRows(int idx) {
			return xza970AcyFrmRow(idx);
		}

		public static int xza970AcyFrmInfo(int idx) {
			return xza970MaxFrmRows(idx) + Len.XZA970_MAX_FRM_ROWS;
		}

		public static int xza970FrmNbr(int idx) {
			return xza970AcyFrmInfo(idx);
		}

		public static int xza970FrmEdtDt(int idx) {
			return xza970FrmNbr(idx) + Len.XZA970_FRM_NBR;
		}

		public static int xza970ActNotTypCd(int idx) {
			return xza970FrmEdtDt(idx) + Len.XZA970_FRM_EDT_DT;
		}

		public static int xza970EdlFrmNm(int idx) {
			return xza970ActNotTypCd(idx) + Len.XZA970_ACT_NOT_TYP_CD;
		}

		public static int xza970FrmDes(int idx) {
			return xza970EdlFrmNm(idx) + Len.XZA970_EDL_FRM_NM;
		}

		public static int xza970SpePrcCd(int idx) {
			return xza970FrmDes(idx) + Len.XZA970_FRM_DES;
		}

		public static int xza970DtnCd(int idx) {
			return xza970SpePrcCd(idx) + Len.XZA970_SPE_PRC_CD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA970_MAX_FRM_ROWS = 2;
		public static final int XZA970_FRM_NBR = 30;
		public static final int XZA970_FRM_EDT_DT = 10;
		public static final int XZA970_ACT_NOT_TYP_CD = 5;
		public static final int XZA970_EDL_FRM_NM = 30;
		public static final int XZA970_FRM_DES = 20;
		public static final int XZA970_SPE_PRC_CD = 8;
		public static final int XZA970_DTN_CD = 5;
		public static final int XZA970_ACY_FRM_INFO = XZA970_FRM_NBR + XZA970_FRM_EDT_DT + XZA970_ACT_NOT_TYP_CD + XZA970_EDL_FRM_NM + XZA970_FRM_DES
				+ XZA970_SPE_PRC_CD + XZA970_DTN_CD;
		public static final int XZA970_ACY_FRM_ROW = XZA970_MAX_FRM_ROWS + XZA970_ACY_FRM_INFO;
		public static final int L_FW_RESP_XZ0A9070 = XZA970_ACY_FRM_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0r9070.L_FW_RESP_XZ0A9070_MAXOCCURS * L_FW_RESP_XZ0A9070;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA970R_DTN_CD = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
