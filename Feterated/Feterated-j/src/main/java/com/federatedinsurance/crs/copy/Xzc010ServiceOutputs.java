/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC010-SERVICE-OUTPUTS<br>
 * Variable: XZC010-SERVICE-OUTPUTS from copybook XZC010C1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc010ServiceOutputs {

	//==== PROPERTIES ====
	//Original name: XZC01O-POL-KEYS
	private Xzc01oPolKeys polKeys = new Xzc01oPolKeys();
	//Original name: XZC01O-PND-CNC-IND
	private char pndCncInd = DefaultValues.CHAR_VAL;
	//Original name: XZC01O-SCH-CNC-DT
	private String schCncDt = DefaultValues.stringVal(Len.SCH_CNC_DT);
	//Original name: XZC01O-NOT-TYP-CD
	private String notTypCd = DefaultValues.stringVal(Len.NOT_TYP_CD);
	//Original name: XZC01O-NOT-TYP-DES
	private String notTypDes = DefaultValues.stringVal(Len.NOT_TYP_DES);
	//Original name: XZC01O-NOT-EMP-ID
	private String notEmpId = DefaultValues.stringVal(Len.NOT_EMP_ID);
	//Original name: XZC01O-NOT-EMP-NM
	private String notEmpNm = DefaultValues.stringVal(Len.NOT_EMP_NM);
	//Original name: XZC01O-NOT-PRC-DT
	private String notPrcDt = DefaultValues.stringVal(Len.NOT_PRC_DT);
	//Original name: FILLER-XZC010-SERVICE-OUTPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setXzc010ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		polKeys.setPolKeysBytes(buffer, position);
		position += Xzc01oPolKeys.Len.POL_KEYS;
		pndCncInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		schCncDt = MarshalByte.readString(buffer, position, Len.SCH_CNC_DT);
		position += Len.SCH_CNC_DT;
		notTypCd = MarshalByte.readString(buffer, position, Len.NOT_TYP_CD);
		position += Len.NOT_TYP_CD;
		notTypDes = MarshalByte.readString(buffer, position, Len.NOT_TYP_DES);
		position += Len.NOT_TYP_DES;
		notEmpId = MarshalByte.readString(buffer, position, Len.NOT_EMP_ID);
		position += Len.NOT_EMP_ID;
		notEmpNm = MarshalByte.readString(buffer, position, Len.NOT_EMP_NM);
		position += Len.NOT_EMP_NM;
		notPrcDt = MarshalByte.readString(buffer, position, Len.NOT_PRC_DT);
		position += Len.NOT_PRC_DT;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzc010ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		polKeys.getPolKeysBytes(buffer, position);
		position += Xzc01oPolKeys.Len.POL_KEYS;
		MarshalByte.writeChar(buffer, position, pndCncInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, schCncDt, Len.SCH_CNC_DT);
		position += Len.SCH_CNC_DT;
		MarshalByte.writeString(buffer, position, notTypCd, Len.NOT_TYP_CD);
		position += Len.NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, notTypDes, Len.NOT_TYP_DES);
		position += Len.NOT_TYP_DES;
		MarshalByte.writeString(buffer, position, notEmpId, Len.NOT_EMP_ID);
		position += Len.NOT_EMP_ID;
		MarshalByte.writeString(buffer, position, notEmpNm, Len.NOT_EMP_NM);
		position += Len.NOT_EMP_NM;
		MarshalByte.writeString(buffer, position, notPrcDt, Len.NOT_PRC_DT);
		position += Len.NOT_PRC_DT;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setPndCncInd(char pndCncInd) {
		this.pndCncInd = pndCncInd;
	}

	public char getPndCncInd() {
		return this.pndCncInd;
	}

	public void setSchCncDt(String schCncDt) {
		this.schCncDt = Functions.subString(schCncDt, Len.SCH_CNC_DT);
	}

	public String getSchCncDt() {
		return this.schCncDt;
	}

	public void setNotTypCd(String notTypCd) {
		this.notTypCd = Functions.subString(notTypCd, Len.NOT_TYP_CD);
	}

	public String getNotTypCd() {
		return this.notTypCd;
	}

	public void setNotTypDes(String notTypDes) {
		this.notTypDes = Functions.subString(notTypDes, Len.NOT_TYP_DES);
	}

	public String getNotTypDes() {
		return this.notTypDes;
	}

	public void setNotEmpId(String notEmpId) {
		this.notEmpId = Functions.subString(notEmpId, Len.NOT_EMP_ID);
	}

	public String getNotEmpId() {
		return this.notEmpId;
	}

	public void setNotEmpNm(String notEmpNm) {
		this.notEmpNm = Functions.subString(notEmpNm, Len.NOT_EMP_NM);
	}

	public String getNotEmpNm() {
		return this.notEmpNm;
	}

	public void setNotPrcDt(String notPrcDt) {
		this.notPrcDt = Functions.subString(notPrcDt, Len.NOT_PRC_DT);
	}

	public String getNotPrcDt() {
		return this.notPrcDt;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public Xzc01oPolKeys getPolKeys() {
		return polKeys;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PND_CNC_IND = 1;
		public static final int SCH_CNC_DT = 10;
		public static final int NOT_TYP_CD = 5;
		public static final int NOT_TYP_DES = 35;
		public static final int NOT_EMP_ID = 6;
		public static final int NOT_EMP_NM = 67;
		public static final int NOT_PRC_DT = 10;
		public static final int FLR1 = 400;
		public static final int XZC010_SERVICE_OUTPUTS = Xzc01oPolKeys.Len.POL_KEYS + PND_CNC_IND + SCH_CNC_DT + NOT_TYP_CD + NOT_TYP_DES + NOT_EMP_ID
				+ NOT_EMP_NM + NOT_PRC_DT + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
