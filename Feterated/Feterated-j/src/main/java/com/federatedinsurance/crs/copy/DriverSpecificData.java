/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.DsdBypassSyncpointMdrvInd;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.occurs.DsdNonLoggableErrors;
import com.federatedinsurance.crs.ws.occurs.DsdWarnings;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DRIVER-SPECIFIC-DATA<br>
 * Variable: DRIVER-SPECIFIC-DATA from copybook TS020DRV<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class DriverSpecificData {

	//==== PROPERTIES ====
	public static final int NON_LOGGABLE_ERRORS_MAXOCCURS = 10;
	public static final int WARNINGS_MAXOCCURS = 10;
	/**Original name: DSD-BYPASS-SYNCPOINT-MDRV-IND<br>
	 * <pre>* FLAG TO INDICATE WHETHER OR NOT MAIN DRIVER DOES SYNCPOINT</pre>*/
	private DsdBypassSyncpointMdrvInd bypassSyncpointMdrvInd = new DsdBypassSyncpointMdrvInd();
	//Original name: DSD-ERROR-RETURN-CODE
	private DsdErrorReturnCode errorReturnCode = new DsdErrorReturnCode();
	//Original name: DSD-FATAL-ERROR-MESSAGE
	private String fatalErrorMessage = DefaultValues.stringVal(Len.FATAL_ERROR_MESSAGE);
	//Original name: DSD-NON-LOGGABLE-ERROR-CNT
	private String nonLoggableErrorCnt = DefaultValues.stringVal(Len.NON_LOGGABLE_ERROR_CNT);
	//Original name: DSD-NON-LOGGABLE-ERRORS
	private DsdNonLoggableErrors[] nonLoggableErrors = new DsdNonLoggableErrors[NON_LOGGABLE_ERRORS_MAXOCCURS];
	//Original name: DSD-WARNING-CNT
	private String warningCnt = DefaultValues.stringVal(Len.WARNING_CNT);
	//Original name: DSD-WARNINGS
	private DsdWarnings[] warnings = new DsdWarnings[WARNINGS_MAXOCCURS];
	//Original name: DSD-PERF-MONITORING-PARMS
	private DsdPerfMonitoringParms perfMonitoringParms = new DsdPerfMonitoringParms();

	//==== CONSTRUCTORS ====
	public DriverSpecificData() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int nonLoggableErrorsIdx = 1; nonLoggableErrorsIdx <= NON_LOGGABLE_ERRORS_MAXOCCURS; nonLoggableErrorsIdx++) {
			nonLoggableErrors[nonLoggableErrorsIdx - 1] = new DsdNonLoggableErrors();
		}
		for (int warningsIdx = 1; warningsIdx <= WARNINGS_MAXOCCURS; warningsIdx++) {
			warnings[warningsIdx - 1] = new DsdWarnings();
		}
	}

	public void setDriverSpecificDataBytes(byte[] buffer, int offset) {
		int position = offset;
		bypassSyncpointMdrvInd.setBypassSyncpointMdrvInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		setErrorHandlingParmsBytes(buffer, position);
		position += Len.ERROR_HANDLING_PARMS;
		perfMonitoringParms.setPerfMonitoringParmsBytes(buffer, position);
	}

	public byte[] getDriverSpecificDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, bypassSyncpointMdrvInd.getBypassSyncpointMdrvInd());
		position += Types.CHAR_SIZE;
		getErrorHandlingParmsBytes(buffer, position);
		position += Len.ERROR_HANDLING_PARMS;
		perfMonitoringParms.getPerfMonitoringParmsBytes(buffer, position);
		return buffer;
	}

	/**Original name: DSD-ERROR-HANDLING-PARMS<br>
	 * <pre>* ERROR HANDLING</pre>*/
	public byte[] getDsdErrorHandlingParmsBytes() {
		byte[] buffer = new byte[Len.ERROR_HANDLING_PARMS];
		return getErrorHandlingParmsBytes(buffer, 1);
	}

	public void setErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		errorReturnCode.value = MarshalByte.readFixedString(buffer, position, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		fatalErrorMessage = MarshalByte.readString(buffer, position, Len.FATAL_ERROR_MESSAGE);
		position += Len.FATAL_ERROR_MESSAGE;
		nonLoggableErrorCnt = MarshalByte.readFixedString(buffer, position, Len.NON_LOGGABLE_ERROR_CNT);
		position += Len.NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				nonLoggableErrors[idx - 1].setNonLoggableErrorsBytes(buffer, position);
				position += DsdNonLoggableErrors.Len.NON_LOGGABLE_ERRORS;
			} else {
				nonLoggableErrors[idx - 1].initNonLoggableErrorsSpaces();
				position += DsdNonLoggableErrors.Len.NON_LOGGABLE_ERRORS;
			}
		}
		warningCnt = MarshalByte.readFixedString(buffer, position, Len.WARNING_CNT);
		position += Len.WARNING_CNT;
		for (int idx = 1; idx <= WARNINGS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				warnings[idx - 1].setWarningsBytes(buffer, position);
				position += DsdWarnings.Len.WARNINGS;
			} else {
				warnings[idx - 1].initWarningsSpaces();
				position += DsdWarnings.Len.WARNINGS;
			}
		}
	}

	public byte[] getErrorHandlingParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, errorReturnCode.value, DsdErrorReturnCode.Len.ERROR_RETURN_CODE);
		position += DsdErrorReturnCode.Len.ERROR_RETURN_CODE;
		MarshalByte.writeString(buffer, position, fatalErrorMessage, Len.FATAL_ERROR_MESSAGE);
		position += Len.FATAL_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, nonLoggableErrorCnt, Len.NON_LOGGABLE_ERROR_CNT);
		position += Len.NON_LOGGABLE_ERROR_CNT;
		for (int idx = 1; idx <= NON_LOGGABLE_ERRORS_MAXOCCURS; idx++) {
			nonLoggableErrors[idx - 1].getNonLoggableErrorsBytes(buffer, position);
			position += DsdNonLoggableErrors.Len.NON_LOGGABLE_ERRORS;
		}
		MarshalByte.writeString(buffer, position, warningCnt, Len.WARNING_CNT);
		position += Len.WARNING_CNT;
		for (int idx = 1; idx <= WARNINGS_MAXOCCURS; idx++) {
			warnings[idx - 1].getWarningsBytes(buffer, position);
			position += DsdWarnings.Len.WARNINGS;
		}
		return buffer;
	}

	public void setFatalErrorMessage(String fatalErrorMessage) {
		this.fatalErrorMessage = Functions.subString(fatalErrorMessage, Len.FATAL_ERROR_MESSAGE);
	}

	public String getFatalErrorMessage() {
		return this.fatalErrorMessage;
	}

	public void setNonLoggableErrorCntFormatted(String nonLoggableErrorCnt) {
		this.nonLoggableErrorCnt = Trunc.toUnsignedNumeric(nonLoggableErrorCnt, Len.NON_LOGGABLE_ERROR_CNT);
	}

	public short getNonLoggableErrorCnt() {
		return NumericDisplay.asShort(this.nonLoggableErrorCnt);
	}

	public void setWarningCntFormatted(String warningCnt) {
		this.warningCnt = Trunc.toUnsignedNumeric(warningCnt, Len.WARNING_CNT);
	}

	public short getWarningCnt() {
		return NumericDisplay.asShort(this.warningCnt);
	}

	public DsdBypassSyncpointMdrvInd getBypassSyncpointMdrvInd() {
		return bypassSyncpointMdrvInd;
	}

	public DsdErrorReturnCode getErrorReturnCode() {
		return errorReturnCode;
	}

	public DsdNonLoggableErrors getNonLoggableErrors(int idx) {
		return nonLoggableErrors[idx - 1];
	}

	public DsdPerfMonitoringParms getPerfMonitoringParms() {
		return perfMonitoringParms;
	}

	public DsdWarnings getWarnings(int idx) {
		return warnings[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FATAL_ERROR_MESSAGE = 250;
		public static final int NON_LOGGABLE_ERROR_CNT = 4;
		public static final int WARNING_CNT = 4;
		public static final int ERROR_HANDLING_PARMS = DsdErrorReturnCode.Len.ERROR_RETURN_CODE + FATAL_ERROR_MESSAGE + NON_LOGGABLE_ERROR_CNT
				+ DriverSpecificData.NON_LOGGABLE_ERRORS_MAXOCCURS * DsdNonLoggableErrors.Len.NON_LOGGABLE_ERRORS + WARNING_CNT
				+ DriverSpecificData.WARNINGS_MAXOCCURS * DsdWarnings.Len.WARNINGS;
		public static final int DRIVER_SPECIFIC_DATA = DsdBypassSyncpointMdrvInd.Len.BYPASS_SYNCPOINT_MDRV_IND + ERROR_HANDLING_PARMS
				+ DsdPerfMonitoringParms.Len.PERF_MONITORING_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
