/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-66-USR-CICS-PGM-ABENDED-MSG<br>
 * Variable: EA-66-USR-CICS-PGM-ABENDED-MSG from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea66UsrCicsPgmAbendedMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-66-USR-CICS-PGM-ABENDED-MSG
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-66-USR-CICS-PGM-ABENDED-MSG-1
	private String flr2 = "A CICS PROGRAM";
	//Original name: FILLER-EA-66-USR-CICS-PGM-ABENDED-MSG-2
	private String flr3 = "HAS ABENDED";
	//Original name: FILLER-EA-66-USR-CICS-PGM-ABENDED-MSG-3
	private String flr4 = "WITH AN ABEND";
	//Original name: FILLER-EA-66-USR-CICS-PGM-ABENDED-MSG-4
	private String flr5 = "CODE OF";
	//Original name: EA-66-ABEND-CODE
	private String ea66AbendCode = DefaultValues.stringVal(Len.EA66_ABEND_CODE);
	//Original name: FILLER-EA-66-USR-CICS-PGM-ABENDED-MSG-5
	private String flr6 = ".  PLEASE FIX.";

	//==== METHODS ====
	public String getEa66UsrCicsPgmAbendedMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa66UsrCicsPgmAbendedMsgBytes());
	}

	public byte[] getEa66UsrCicsPgmAbendedMsgBytes() {
		byte[] buffer = new byte[Len.EA66_USR_CICS_PGM_ABENDED_MSG];
		return getEa66UsrCicsPgmAbendedMsgBytes(buffer, 1);
	}

	public byte[] getEa66UsrCicsPgmAbendedMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea66AbendCode, Len.EA66_ABEND_CODE);
		position += Len.EA66_ABEND_CODE;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setEa66AbendCode(String ea66AbendCode) {
		this.ea66AbendCode = Functions.subString(ea66AbendCode, Len.EA66_ABEND_CODE);
	}

	public String getEa66AbendCode() {
		return this.ea66AbendCode;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA66_ABEND_CODE = 4;
		public static final int FLR1 = 11;
		public static final int FLR2 = 15;
		public static final int FLR3 = 12;
		public static final int FLR4 = 14;
		public static final int FLR5 = 8;
		public static final int EA66_USR_CICS_PGM_ABENDED_MSG = EA66_ABEND_CODE + FLR1 + FLR2 + FLR3 + 2 * FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
