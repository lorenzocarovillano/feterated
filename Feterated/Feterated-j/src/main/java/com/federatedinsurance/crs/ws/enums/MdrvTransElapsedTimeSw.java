/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;

/**Original name: MDRV-TRANS-ELAPSED-TIME-SW<br>
 * Variable: MDRV-TRANS-ELAPSED-TIME-SW from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvTransElapsedTimeSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NO_ELAPSED_TIME = Types.SPACE_CHAR;
	public static final char CALC_ELAPSED_TIME = 'Y';
	public static final char ELAPSED_TIME_STARTED = 'S';

	//==== METHODS ====
	public void setTransElapsedTimeSw(char transElapsedTimeSw) {
		this.value = transElapsedTimeSw;
	}

	public char getTransElapsedTimeSw() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRANS_ELAPSED_TIME_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
