/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IStsPolTyp;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [STS_POL_TYP]
 * 
 */
public class StsPolTypDao extends BaseSqlDao<IStsPolTyp> {

	public StsPolTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IStsPolTyp> getToClass() {
		return IStsPolTyp.class;
	}

	public String selectRec(String siiiPolTypCd, String effDt, String dft) {
		return buildQuery("selectRec").bind("siiiPolTypCd", siiiPolTypCd).bind("effDt", effDt).scalarResultString(dft);
	}

	public String selectRec1(String siiiPolTypCd, String effDt, String dft) {
		return buildQuery("selectRec1").bind("siiiPolTypCd", siiiPolTypCd).bind("effDt", effDt).scalarResultString(dft);
	}

	public short selectRec2(String typCd, String effDt, short dft) {
		return buildQuery("selectRec2").bind("typCd", typCd).bind("effDt", effDt).scalarResultShort(dft);
	}

	public String selectRec3(String siiiPolTypCd, String effDt, String dft) {
		return buildQuery("selectRec3").bind("siiiPolTypCd", siiiPolTypCd).bind("effDt", effDt).scalarResultString(dft);
	}
}
