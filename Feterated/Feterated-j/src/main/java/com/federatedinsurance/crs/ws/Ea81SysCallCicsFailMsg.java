/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-81-SYS-CALL-CICS-FAIL-MSG<br>
 * Variable: EA-81-SYS-CALL-CICS-FAIL-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea81SysCallCicsFailMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-81-SYS-CALL-CICS-FAIL-MSG
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-81-SYS-CALL-CICS-FAIL-MSG-1
	private String flr2 = "A CALL TO THE";
	//Original name: FILLER-EA-81-SYS-CALL-CICS-FAIL-MSG-2
	private String flr3 = "CICS SVC FAILED";
	//Original name: FILLER-EA-81-SYS-CALL-CICS-FAIL-MSG-3
	private char flr4 = '.';

	//==== METHODS ====
	public String getEa81SysCallCicsFailMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa81SysCallCicsFailMsgBytes());
	}

	public byte[] getEa81SysCallCicsFailMsgBytes() {
		byte[] buffer = new byte[Len.EA81_SYS_CALL_CICS_FAIL_MSG];
		return getEa81SysCallCicsFailMsgBytes(buffer, 1);
	}

	public byte[] getEa81SysCallCicsFailMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeChar(buffer, position, flr4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public char getFlr4() {
		return this.flr4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 14;
		public static final int FLR3 = 15;
		public static final int FLR4 = 1;
		public static final int EA81_SYS_CALL_CICS_FAIL_MSG = FLR1 + FLR2 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
