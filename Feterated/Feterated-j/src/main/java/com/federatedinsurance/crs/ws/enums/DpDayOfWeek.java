/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DP-DAY-OF-WEEK<br>
 * Variable: DP-DAY-OF-WEEK from program XZ0P0021<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class DpDayOfWeek extends SerializableParameter {

	//==== PROPERTIES ====
	private int value = DefaultValues.BIN_INT_VAL;
	private static final int[] WEEKEND = new int[] { 1, 7 };
	public static final int SAT = 7;
	public static final int SUN = 1;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DP_DAY_OF_WEEK;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDpDayOfWeekFromBuffer(buf);
	}

	public void setDpDayOfWeek(int dpDayOfWeek) {
		this.value = dpDayOfWeek;
	}

	public void setDpDayOfWeekFromBuffer(byte[] buffer) {
		value = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getDpDayOfWeek() {
		return this.value;
	}

	public String getDpDayOfWeekFormatted() {
		return PicFormatter.display(new PicParams("S9(9)").setUsage(PicUsage.BINARY)).format(getDpDayOfWeek()).toString();
	}

	public boolean isWeekend() {
		return ArrayUtils.contains(WEEKEND, value);
	}

	public boolean isSat() {
		return value == SAT;
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.binIntToBuffer(getDpDayOfWeek());
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DP_DAY_OF_WEEK = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int DP_DAY_OF_WEEK = 9;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int DP_DAY_OF_WEEK = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
