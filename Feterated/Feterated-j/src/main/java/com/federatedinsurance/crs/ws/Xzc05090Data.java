/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.federatedinsurance.crs.copy.Fwppcstk;
import com.federatedinsurance.crs.copy.Ivoryh;
import com.federatedinsurance.crs.copy.Xz05cl1i;
import com.federatedinsurance.crs.copy.Xzc050c1;
import com.federatedinsurance.crs.ws.occurs.Xz005oCerInf;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZC05090<br>
 * Generated as a class for rule WS.<br>*/
public class Xzc05090Data {

	//==== PROPERTIES ====
	public static final int XZ005O_CER_INF_MAXOCCURS = 2500;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXzc05090 constantFields = new ConstantFieldsXzc05090();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXzc05090 errorAndAdviceMessages = new ErrorAndAdviceMessagesXzc05090();
	//Original name: SUBSCRIPTS
	private SubscriptsTs547099 subscripts = new SubscriptsTs547099();
	//Original name: URI-LKU-LINKAGE
	private Ts571cb1 ts571cb1 = new Ts571cb1();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXzc05090 workingStorageArea = new WorkingStorageAreaXzc05090();
	//Original name: GET-CER-ACY-STA
	private GetCerAcySta getCerAcySta = new GetCerAcySta();
	//Original name: XZC050C1
	private Xzc050c1 xzc050c1 = new Xzc050c1();
	//Original name: IVORYH
	private Ivoryh ivoryh = new Ivoryh();
	//Original name: FWPPCSTK
	private Fwppcstk fwppcstk = new Fwppcstk();
	//Original name: XZ05CL1I
	private Xz05cl1i xz05cl1i = new Xz05cl1i();
	//Original name: XZ005O-CER-INF
	private LazyArrayCopy<Xz005oCerInf> xz005oCerInf = new LazyArrayCopy<>(new Xz005oCerInf(), 1, XZ005O_CER_INF_MAXOCCURS);

	//==== METHODS ====
	public void setGetCerLisIntfBytes(byte[] buffer) {
		setGetCerLisIntfBytes(buffer, 1);
	}

	/**Original name: GET-CER-LIS-INTF<br>*/
	public byte[] getGetCerLisIntfBytes() {
		byte[] buffer = new byte[Len.GET_CER_LIS_INTF];
		return getGetCerLisIntfBytes(buffer, 1);
	}

	public void setGetCerLisIntfBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc050c1.setXzc050ProgramInputBytes(buffer, position);
		position += Xzc050c1.Len.XZC050_PROGRAM_INPUT;
		xzc050c1.setXzc050ProgramOutputBytes(buffer, position);
	}

	public byte[] getGetCerLisIntfBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc050c1.getXzc050ProgramInputBytes(buffer, position);
		position += Xzc050c1.Len.XZC050_PROGRAM_INPUT;
		xzc050c1.getXzc050ProgramOutputBytes(buffer, position);
		return buffer;
	}

	/**Original name: CALLABLE-INPUTS<br>*/
	public byte[] getCallableInputsBytes() {
		byte[] buffer = new byte[Len.CALLABLE_INPUTS];
		return getCallableInputsBytes(buffer, 1);
	}

	public byte[] getCallableInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		fwppcstk.getTechnicalKeysBytes(buffer, position);
		position += Fwppcstk.Len.TECHNICAL_KEYS;
		xz05cl1i.getGetCerLisBytes(buffer, position);
		return buffer;
	}

	public void initCallableInputsLowValues() {
		fwppcstk.initFwppcstkLowValues();
		xz05cl1i.initXz05cl1iLowValues();
	}

	public void setCallableOutputsBytes(byte[] buffer) {
		setCallableOutputsBytes(buffer, 1);
	}

	/**Original name: CALLABLE-OUTPUTS<br>*/
	public byte[] getCallableOutputsBytes() {
		byte[] buffer = new byte[Len.CALLABLE_OUTPUTS];
		return getCallableOutputsBytes(buffer, 1);
	}

	public void setCallableOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		setXz005oGetCerLisBytes(buffer, position);
	}

	public byte[] getCallableOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		getXz005oGetCerLisBytes(buffer, position);
		return buffer;
	}

	public void initCallableOutputsLowValues() {
		initXz005oGetCerLisLowValues();
	}

	public void setXz005oGetCerLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= XZ005O_CER_INF_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				xz005oCerInf.get(idx - 1).setXz005oCerInfBytes(buffer, position);
				position += Xz005oCerInf.Len.XZ005O_CER_INF;
			} else {
				Xz005oCerInf temp_xz005oCerInf = new Xz005oCerInf();
				temp_xz005oCerInf.initXz005oCerInfSpaces();
				getXz005oCerInfObj().fill(temp_xz005oCerInf);
				position += Xz005oCerInf.Len.XZ005O_CER_INF * (XZ005O_CER_INF_MAXOCCURS - idx + 1);
				break;
			}
		}
	}

	public byte[] getXz005oGetCerLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= XZ005O_CER_INF_MAXOCCURS; idx++) {
			xz005oCerInf.get(idx - 1).getXz005oCerInfBytes(buffer, position);
			position += Xz005oCerInf.Len.XZ005O_CER_INF;
		}
		return buffer;
	}

	public void initXz005oGetCerLisLowValues() {
		getXz005oCerInfObj().fill(new Xz005oCerInf().initXz005oCerInfLowValues());
	}

	public ConstantFieldsXzc05090 getConstantFields() {
		return constantFields;
	}

	public ErrorAndAdviceMessagesXzc05090 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public Fwppcstk getFwppcstk() {
		return fwppcstk;
	}

	public GetCerAcySta getGetCerAcySta() {
		return getCerAcySta;
	}

	public Ivoryh getIvoryh() {
		return ivoryh;
	}

	public SubscriptsTs547099 getSubscripts() {
		return subscripts;
	}

	public Ts571cb1 getTs571cb1() {
		return ts571cb1;
	}

	public WorkingStorageAreaXzc05090 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public Xz005oCerInf getXz005oCerInf(int idx) {
		return xz005oCerInf.get(idx - 1);
	}

	public LazyArrayCopy<Xz005oCerInf> getXz005oCerInfObj() {
		return xz005oCerInf;
	}

	public Xz05cl1i getXz05cl1i() {
		return xz05cl1i;
	}

	public Xzc050c1 getXzc050c1() {
		return xzc050c1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int GET_CER_LIS_INTF = Xzc050c1.Len.XZC050_PROGRAM_INPUT + Xzc050c1.Len.XZC050_PROGRAM_OUTPUT;
		public static final int CALLABLE_INPUTS = Fwppcstk.Len.TECHNICAL_KEYS + Xz05cl1i.Len.GET_CER_LIS;
		public static final int XZ005O_GET_CER_LIS = Xzc05090Data.XZ005O_CER_INF_MAXOCCURS * Xz005oCerInf.Len.XZ005O_CER_INF;
		public static final int CALLABLE_OUTPUTS = XZ005O_GET_CER_LIS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
