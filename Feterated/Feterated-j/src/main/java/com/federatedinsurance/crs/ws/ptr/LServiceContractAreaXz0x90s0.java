/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90S0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90s0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int O_POLICY_LIST_MAXOCCURS = 250;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90s0() {
	}

	public LServiceContractAreaXz0x90s0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiActNbr(String iActNbr) {
		writeString(Pos.I_ACT_NBR, iActNbr, Len.I_ACT_NBR);
	}

	/**Original name: XZT9SI-ACT-NBR<br>*/
	public String getiActNbr() {
		return readString(Pos.I_ACT_NBR, Len.I_ACT_NBR);
	}

	public void setiUserid(String iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT9SI-USERID<br>*/
	public String getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	public String getiUseridFormatted() {
		return Functions.padBlanks(getiUserid(), Len.I_USERID);
	}

	public void setoTkPolId(int oTkPolIdIdx, String oTkPolId) {
		int position = Pos.xzt9soTkPolId(oTkPolIdIdx - 1);
		writeString(position, oTkPolId, Len.O_TK_POL_ID);
	}

	/**Original name: XZT9SO-TK-POL-ID<br>*/
	public String getoTkPolId(int oTkPolIdIdx) {
		int position = Pos.xzt9soTkPolId(oTkPolIdIdx - 1);
		return readString(position, Len.O_TK_POL_ID);
	}

	public void setoTkRltTypCd(int oTkRltTypCdIdx, String oTkRltTypCd) {
		int position = Pos.xzt9soTkRltTypCd(oTkRltTypCdIdx - 1);
		writeString(position, oTkRltTypCd, Len.O_TK_RLT_TYP_CD);
	}

	/**Original name: XZT9SO-TK-RLT-TYP-CD<br>*/
	public String getoTkRltTypCd(int oTkRltTypCdIdx) {
		int position = Pos.xzt9soTkRltTypCd(oTkRltTypCdIdx - 1);
		return readString(position, Len.O_TK_RLT_TYP_CD);
	}

	public void setoPolNbr(int oPolNbrIdx, String oPolNbr) {
		int position = Pos.xzt9soPolNbr(oPolNbrIdx - 1);
		writeString(position, oPolNbr, Len.O_POL_NBR);
	}

	/**Original name: XZT9SO-POL-NBR<br>*/
	public String getoPolNbr(int oPolNbrIdx) {
		int position = Pos.xzt9soPolNbr(oPolNbrIdx - 1);
		return readString(position, Len.O_POL_NBR);
	}

	public void setoPolEffDt(int oPolEffDtIdx, String oPolEffDt) {
		int position = Pos.xzt9soPolEffDt(oPolEffDtIdx - 1);
		writeString(position, oPolEffDt, Len.O_POL_EFF_DT);
	}

	/**Original name: XZT9SO-POL-EFF-DT<br>*/
	public String getoPolEffDt(int oPolEffDtIdx) {
		int position = Pos.xzt9soPolEffDt(oPolEffDtIdx - 1);
		return readString(position, Len.O_POL_EFF_DT);
	}

	public void setoPolExpDt(int oPolExpDtIdx, String oPolExpDt) {
		int position = Pos.xzt9soPolExpDt(oPolExpDtIdx - 1);
		writeString(position, oPolExpDt, Len.O_POL_EXP_DT);
	}

	/**Original name: XZT9SO-POL-EXP-DT<br>*/
	public String getoPolExpDt(int oPolExpDtIdx) {
		int position = Pos.xzt9soPolExpDt(oPolExpDtIdx - 1);
		return readString(position, Len.O_POL_EXP_DT);
	}

	public void setoQteNbr(int oQteNbrIdx, int oQteNbr) {
		int position = Pos.xzt9soQteNbr(oQteNbrIdx - 1);
		writeInt(position, oQteNbr, Len.Int.O_QTE_NBR);
	}

	/**Original name: XZT9SO-QTE-NBR<br>*/
	public int getoQteNbr(int oQteNbrIdx) {
		int position = Pos.xzt9soQteNbr(oQteNbrIdx - 1);
		return readNumDispInt(position, Len.O_QTE_NBR);
	}

	public void setoActNbr(int oActNbrIdx, String oActNbr) {
		int position = Pos.xzt9soActNbr(oActNbrIdx - 1);
		writeString(position, oActNbr, Len.O_ACT_NBR);
	}

	/**Original name: XZT9SO-ACT-NBR<br>*/
	public String getoActNbr(int oActNbrIdx) {
		int position = Pos.xzt9soActNbr(oActNbrIdx - 1);
		return readString(position, Len.O_ACT_NBR);
	}

	public void setoActNbrFmt(int oActNbrFmtIdx, String oActNbrFmt) {
		int position = Pos.xzt9soActNbrFmt(oActNbrFmtIdx - 1);
		writeString(position, oActNbrFmt, Len.O_ACT_NBR_FMT);
	}

	/**Original name: XZT9SO-ACT-NBR-FMT<br>*/
	public String getoActNbrFmt(int oActNbrFmtIdx) {
		int position = Pos.xzt9soActNbrFmt(oActNbrFmtIdx - 1);
		return readString(position, Len.O_ACT_NBR_FMT);
	}

	public void setoPriRskStAbb(int oPriRskStAbbIdx, String oPriRskStAbb) {
		int position = Pos.xzt9soPriRskStAbb(oPriRskStAbbIdx - 1);
		writeString(position, oPriRskStAbb, Len.O_PRI_RSK_ST_ABB);
	}

	/**Original name: XZT9SO-PRI-RSK-ST-ABB<br>*/
	public String getoPriRskStAbb(int oPriRskStAbbIdx) {
		int position = Pos.xzt9soPriRskStAbb(oPriRskStAbbIdx - 1);
		return readString(position, Len.O_PRI_RSK_ST_ABB);
	}

	public void setoPriRskStDes(int oPriRskStDesIdx, String oPriRskStDes) {
		int position = Pos.xzt9soPriRskStDes(oPriRskStDesIdx - 1);
		writeString(position, oPriRskStDes, Len.O_PRI_RSK_ST_DES);
	}

	/**Original name: XZT9SO-PRI-RSK-ST-DES<br>*/
	public String getoPriRskStDes(int oPriRskStDesIdx) {
		int position = Pos.xzt9soPriRskStDes(oPriRskStDesIdx - 1);
		return readString(position, Len.O_PRI_RSK_ST_DES);
	}

	public void setoLobCd(int oLobCdIdx, String oLobCd) {
		int position = Pos.xzt9soLobCd(oLobCdIdx - 1);
		writeString(position, oLobCd, Len.O_LOB_CD);
	}

	/**Original name: XZT9SO-LOB-CD<br>*/
	public String getoLobCd(int oLobCdIdx) {
		int position = Pos.xzt9soLobCd(oLobCdIdx - 1);
		return readString(position, Len.O_LOB_CD);
	}

	public void setoLobDes(int oLobDesIdx, String oLobDes) {
		int position = Pos.xzt9soLobDes(oLobDesIdx - 1);
		writeString(position, oLobDes, Len.O_LOB_DES);
	}

	/**Original name: XZT9SO-LOB-DES<br>*/
	public String getoLobDes(int oLobDesIdx) {
		int position = Pos.xzt9soLobDes(oLobDesIdx - 1);
		return readString(position, Len.O_LOB_DES);
	}

	public void setoTobCd(int oTobCdIdx, String oTobCd) {
		int position = Pos.xzt9soTobCd(oTobCdIdx - 1);
		writeString(position, oTobCd, Len.O_TOB_CD);
	}

	/**Original name: XZT9SO-TOB-CD<br>*/
	public String getoTobCd(int oTobCdIdx) {
		int position = Pos.xzt9soTobCd(oTobCdIdx - 1);
		return readString(position, Len.O_TOB_CD);
	}

	public void setoTobDes(int oTobDesIdx, String oTobDes) {
		int position = Pos.xzt9soTobDes(oTobDesIdx - 1);
		writeString(position, oTobDes, Len.O_TOB_DES);
	}

	/**Original name: XZT9SO-TOB-DES<br>*/
	public String getoTobDes(int oTobDesIdx) {
		int position = Pos.xzt9soTobDes(oTobDesIdx - 1);
		return readString(position, Len.O_TOB_DES);
	}

	public void setoSegCd(int oSegCdIdx, String oSegCd) {
		int position = Pos.xzt9soSegCd(oSegCdIdx - 1);
		writeString(position, oSegCd, Len.O_SEG_CD);
	}

	/**Original name: XZT9SO-SEG-CD<br>*/
	public String getoSegCd(int oSegCdIdx) {
		int position = Pos.xzt9soSegCd(oSegCdIdx - 1);
		return readString(position, Len.O_SEG_CD);
	}

	public void setoSegDes(int oSegDesIdx, String oSegDes) {
		int position = Pos.xzt9soSegDes(oSegDesIdx - 1);
		writeString(position, oSegDes, Len.O_SEG_DES);
	}

	/**Original name: XZT9SO-SEG-DES<br>*/
	public String getoSegDes(int oSegDesIdx) {
		int position = Pos.xzt9soSegDes(oSegDesIdx - 1);
		return readString(position, Len.O_SEG_DES);
	}

	public void setoLgeActInd(int oLgeActIndIdx, char oLgeActInd) {
		int position = Pos.xzt9soLgeActInd(oLgeActIndIdx - 1);
		writeChar(position, oLgeActInd);
	}

	/**Original name: XZT9SO-LGE-ACT-IND<br>*/
	public char getoLgeActInd(int oLgeActIndIdx) {
		int position = Pos.xzt9soLgeActInd(oLgeActIndIdx - 1);
		return readChar(position);
	}

	public void setoMnlPolInd(int oMnlPolIndIdx, char oMnlPolInd) {
		int position = Pos.xzt9soMnlPolInd(oMnlPolIndIdx - 1);
		writeChar(position, oMnlPolInd);
	}

	/**Original name: XZT9SO-MNL-POL-IND<br>
	 * <pre>            Y = MANUAL ONLY
	 *             N = EITHER SERIES 3 OR BOTH</pre>*/
	public char getoMnlPolInd(int oMnlPolIndIdx) {
		int position = Pos.xzt9soMnlPolInd(oMnlPolIndIdx - 1);
		return readChar(position);
	}

	public void setoCurStaCd(int oCurStaCdIdx, char oCurStaCd) {
		int position = Pos.xzt9soCurStaCd(oCurStaCdIdx - 1);
		writeChar(position, oCurStaCd);
	}

	/**Original name: XZT9SO-CUR-STA-CD<br>*/
	public char getoCurStaCd(int oCurStaCdIdx) {
		int position = Pos.xzt9soCurStaCd(oCurStaCdIdx - 1);
		return readChar(position);
	}

	public void setoCurStaDes(int oCurStaDesIdx, String oCurStaDes) {
		int position = Pos.xzt9soCurStaDes(oCurStaDesIdx - 1);
		writeString(position, oCurStaDes, Len.O_CUR_STA_DES);
	}

	/**Original name: XZT9SO-CUR-STA-DES<br>*/
	public String getoCurStaDes(int oCurStaDesIdx) {
		int position = Pos.xzt9soCurStaDes(oCurStaDesIdx - 1);
		return readString(position, Len.O_CUR_STA_DES);
	}

	public void setoCurTrsTypCd(int oCurTrsTypCdIdx, char oCurTrsTypCd) {
		int position = Pos.xzt9soCurTrsTypCd(oCurTrsTypCdIdx - 1);
		writeChar(position, oCurTrsTypCd);
	}

	/**Original name: XZT9SO-CUR-TRS-TYP-CD<br>
	 * <pre>                WHAT WAS THE LAST THING DONE ON THE POLICY
	 *                 AND IS IT ISSUED?</pre>*/
	public char getoCurTrsTypCd(int oCurTrsTypCdIdx) {
		int position = Pos.xzt9soCurTrsTypCd(oCurTrsTypCdIdx - 1);
		return readChar(position);
	}

	public void setoCurTrsTypDes(int oCurTrsTypDesIdx, String oCurTrsTypDes) {
		int position = Pos.xzt9soCurTrsTypDes(oCurTrsTypDesIdx - 1);
		writeString(position, oCurTrsTypDes, Len.O_CUR_TRS_TYP_DES);
	}

	/**Original name: XZT9SO-CUR-TRS-TYP-DES<br>*/
	public String getoCurTrsTypDes(int oCurTrsTypDesIdx) {
		int position = Pos.xzt9soCurTrsTypDes(oCurTrsTypDesIdx - 1);
		return readString(position, Len.O_CUR_TRS_TYP_DES);
	}

	public void setoCurPndTrsInd(int oCurPndTrsIndIdx, char oCurPndTrsInd) {
		int position = Pos.xzt9soCurPndTrsInd(oCurPndTrsIndIdx - 1);
		writeChar(position, oCurPndTrsInd);
	}

	/**Original name: XZT9SO-CUR-PND-TRS-IND<br>*/
	public char getoCurPndTrsInd(int oCurPndTrsIndIdx) {
		int position = Pos.xzt9soCurPndTrsInd(oCurPndTrsIndIdx - 1);
		return readChar(position);
	}

	public void setoCurOgnCd(int oCurOgnCdIdx, char oCurOgnCd) {
		int position = Pos.xzt9soCurOgnCd(oCurOgnCdIdx - 1);
		writeChar(position, oCurOgnCd);
	}

	/**Original name: XZT9SO-CUR-OGN-CD<br>
	 * <pre>                    S = SERIES 3 ONLY
	 *                     M = MANUAL ONLY
	 *                     B = BOTH SERIES 3 AND MANUAL</pre>*/
	public char getoCurOgnCd(int oCurOgnCdIdx) {
		int position = Pos.xzt9soCurOgnCd(oCurOgnCdIdx - 1);
		return readChar(position);
	}

	public void setoCurOgnDes(int oCurOgnDesIdx, String oCurOgnDes) {
		int position = Pos.xzt9soCurOgnDes(oCurOgnDesIdx - 1);
		writeString(position, oCurOgnDes, Len.O_CUR_OGN_DES);
	}

	/**Original name: XZT9SO-CUR-OGN-DES<br>*/
	public String getoCurOgnDes(int oCurOgnDesIdx) {
		int position = Pos.xzt9soCurOgnDes(oCurOgnDesIdx - 1);
		return readString(position, Len.O_CUR_OGN_DES);
	}

	public void setoCoverageParts(int oCoveragePartsIdx, String oCoverageParts) {
		int position = Pos.xzt9soCoverageParts(oCoveragePartsIdx - 1);
		writeString(position, oCoverageParts, Len.O_COVERAGE_PARTS);
	}

	/**Original name: XZT9SO-COVERAGE-PARTS<br>
	 * <pre>                3 CHARACTER CODES STRUNG TOGETHER</pre>*/
	public String getoCoverageParts(int oCoveragePartsIdx) {
		int position = Pos.xzt9soCoverageParts(oCoveragePartsIdx - 1);
		return readString(position, Len.O_COVERAGE_PARTS);
	}

	public void setoBondInd(int oBondIndIdx, char oBondInd) {
		int position = Pos.xzt9soBondInd(oBondIndIdx - 1);
		writeChar(position, oBondInd);
	}

	/**Original name: XZT9SO-BOND-IND<br>*/
	public char getoBondInd(int oBondIndIdx) {
		int position = Pos.xzt9soBondInd(oBondIndIdx - 1);
		return readChar(position);
	}

	public void setoPbBranchNbr(int oPbBranchNbrIdx, String oPbBranchNbr) {
		int position = Pos.xzt9soPbBranchNbr(oPbBranchNbrIdx - 1);
		writeString(position, oPbBranchNbr, Len.O_PB_BRANCH_NBR);
	}

	/**Original name: XZT9SO-PB-BRANCH-NBR<br>*/
	public String getoPbBranchNbr(int oPbBranchNbrIdx) {
		int position = Pos.xzt9soPbBranchNbr(oPbBranchNbrIdx - 1);
		return readString(position, Len.O_PB_BRANCH_NBR);
	}

	public void setoPbBranchDesc(int oPbBranchDescIdx, String oPbBranchDesc) {
		int position = Pos.xzt9soPbBranchDesc(oPbBranchDescIdx - 1);
		writeString(position, oPbBranchDesc, Len.O_PB_BRANCH_DESC);
	}

	/**Original name: XZT9SO-PB-BRANCH-DESC<br>*/
	public String getoPbBranchDesc(int oPbBranchDescIdx) {
		int position = Pos.xzt9soPbBranchDesc(oPbBranchDescIdx - 1);
		return readString(position, Len.O_PB_BRANCH_DESC);
	}

	public void setoWrtPremAmt(int oWrtPremAmtIdx, long oWrtPremAmt) {
		int position = Pos.xzt9soWrtPremAmt(oWrtPremAmtIdx - 1);
		writeLong(position, oWrtPremAmt, Len.Int.O_WRT_PREM_AMT);
	}

	/**Original name: XZT9SO-WRT-PREM-AMT<br>*/
	public long getoWrtPremAmt(int oWrtPremAmtIdx) {
		int position = Pos.xzt9soWrtPremAmt(oWrtPremAmtIdx - 1);
		return readNumDispLong(position, Len.O_WRT_PREM_AMT);
	}

	public void setoPolActInd(int oPolActIndIdx, char oPolActInd) {
		int position = Pos.xzt9soPolActInd(oPolActIndIdx - 1);
		writeChar(position, oPolActInd);
	}

	/**Original name: XZT9SO-POL-ACT-IND<br>*/
	public char getoPolActInd(int oPolActIndIdx) {
		int position = Pos.xzt9soPolActInd(oPolActIndIdx - 1);
		return readChar(position);
	}

	public void setoPndCncInd(int oPndCncIndIdx, char oPndCncInd) {
		int position = Pos.xzt9soPndCncInd(oPndCncIndIdx - 1);
		writeChar(position, oPndCncInd);
	}

	/**Original name: XZT9SO-PND-CNC-IND<br>*/
	public char getoPndCncInd(int oPndCncIndIdx) {
		int position = Pos.xzt9soPndCncInd(oPndCncIndIdx - 1);
		return readChar(position);
	}

	public void setoSchCncDt(int oSchCncDtIdx, String oSchCncDt) {
		int position = Pos.xzt9soSchCncDt(oSchCncDtIdx - 1);
		writeString(position, oSchCncDt, Len.O_SCH_CNC_DT);
	}

	/**Original name: XZT9SO-SCH-CNC-DT<br>*/
	public String getoSchCncDt(int oSchCncDtIdx) {
		int position = Pos.xzt9soSchCncDt(oSchCncDtIdx - 1);
		return readString(position, Len.O_SCH_CNC_DT);
	}

	public void setoNotTypCd(int oNotTypCdIdx, String oNotTypCd) {
		int position = Pos.xzt9soNotTypCd(oNotTypCdIdx - 1);
		writeString(position, oNotTypCd, Len.O_NOT_TYP_CD);
	}

	/**Original name: XZT9SO-NOT-TYP-CD<br>*/
	public String getoNotTypCd(int oNotTypCdIdx) {
		int position = Pos.xzt9soNotTypCd(oNotTypCdIdx - 1);
		return readString(position, Len.O_NOT_TYP_CD);
	}

	public void setoNotTypDes(int oNotTypDesIdx, String oNotTypDes) {
		int position = Pos.xzt9soNotTypDes(oNotTypDesIdx - 1);
		writeString(position, oNotTypDes, Len.O_NOT_TYP_DES);
	}

	/**Original name: XZT9SO-NOT-TYP-DES<br>*/
	public String getoNotTypDes(int oNotTypDesIdx) {
		int position = Pos.xzt9soNotTypDes(oNotTypDesIdx - 1);
		return readString(position, Len.O_NOT_TYP_DES);
	}

	public void setoNotEmpId(int oNotEmpIdIdx, String oNotEmpId) {
		int position = Pos.xzt9soNotEmpId(oNotEmpIdIdx - 1);
		writeString(position, oNotEmpId, Len.O_NOT_EMP_ID);
	}

	/**Original name: XZT9SO-NOT-EMP-ID<br>*/
	public String getoNotEmpId(int oNotEmpIdIdx) {
		int position = Pos.xzt9soNotEmpId(oNotEmpIdIdx - 1);
		return readString(position, Len.O_NOT_EMP_ID);
	}

	public void setoNotEmpNm(int oNotEmpNmIdx, String oNotEmpNm) {
		int position = Pos.xzt9soNotEmpNm(oNotEmpNmIdx - 1);
		writeString(position, oNotEmpNm, Len.O_NOT_EMP_NM);
	}

	/**Original name: XZT9SO-NOT-EMP-NM<br>*/
	public String getoNotEmpNm(int oNotEmpNmIdx) {
		int position = Pos.xzt9soNotEmpNm(oNotEmpNmIdx - 1);
		return readString(position, Len.O_NOT_EMP_NM);
	}

	public void setoNotPrcDt(int oNotPrcDtIdx, String oNotPrcDt) {
		int position = Pos.xzt9soNotPrcDt(oNotPrcDtIdx - 1);
		writeString(position, oNotPrcDt, Len.O_NOT_PRC_DT);
	}

	/**Original name: XZT9SO-NOT-PRC-DT<br>*/
	public String getoNotPrcDt(int oNotPrcDtIdx) {
		int position = Pos.xzt9soNotPrcDt(oNotPrcDtIdx - 1);
		return readString(position, Len.O_NOT_PRC_DT);
	}

	public void setoTmnInd(int oTmnIndIdx, char oTmnInd) {
		int position = Pos.xzt9soTmnInd(oTmnIndIdx - 1);
		writeChar(position, oTmnInd);
	}

	/**Original name: XZT9SO-TMN-IND<br>*/
	public char getoTmnInd(int oTmnIndIdx) {
		int position = Pos.xzt9soTmnInd(oTmnIndIdx - 1);
		return readChar(position);
	}

	public void setoTmnEmpId(int oTmnEmpIdIdx, String oTmnEmpId) {
		int position = Pos.xzt9soTmnEmpId(oTmnEmpIdIdx - 1);
		writeString(position, oTmnEmpId, Len.O_TMN_EMP_ID);
	}

	/**Original name: XZT9SO-TMN-EMP-ID<br>*/
	public String getoTmnEmpId(int oTmnEmpIdIdx) {
		int position = Pos.xzt9soTmnEmpId(oTmnEmpIdIdx - 1);
		return readString(position, Len.O_TMN_EMP_ID);
	}

	public void setoTmnEmpNm(int oTmnEmpNmIdx, String oTmnEmpNm) {
		int position = Pos.xzt9soTmnEmpNm(oTmnEmpNmIdx - 1);
		writeString(position, oTmnEmpNm, Len.O_TMN_EMP_NM);
	}

	/**Original name: XZT9SO-TMN-EMP-NM<br>*/
	public String getoTmnEmpNm(int oTmnEmpNmIdx) {
		int position = Pos.xzt9soTmnEmpNm(oTmnEmpNmIdx - 1);
		return readString(position, Len.O_TMN_EMP_NM);
	}

	public void setoTmnPrcDt(int oTmnPrcDtIdx, String oTmnPrcDt) {
		int position = Pos.xzt9soTmnPrcDt(oTmnPrcDtIdx - 1);
		writeString(position, oTmnPrcDt, Len.O_TMN_PRC_DT);
	}

	/**Original name: XZT9SO-TMN-PRC-DT<br>*/
	public String getoTmnPrcDt(int oTmnPrcDtIdx) {
		int position = Pos.xzt9soTmnPrcDt(oTmnPrcDtIdx - 1);
		return readString(position, Len.O_TMN_PRC_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT9S0_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_ACT_NBR = XZT9S0_SERVICE_INPUTS;
		public static final int I_USERID = I_ACT_NBR + Len.I_ACT_NBR;
		public static final int FLR1 = I_USERID + Len.I_USERID;
		public static final int XZT9S0_SERVICE_OUTPUTS = FLR1 + Len.FLR1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt9soPolicyList(int idx) {
			return XZT9S0_SERVICE_OUTPUTS + idx * Len.O_POLICY_LIST;
		}

		public static int xzt9soTechnicalKeys(int idx) {
			return xzt9soPolicyList(idx);
		}

		public static int xzt9soTkPolId(int idx) {
			return xzt9soTechnicalKeys(idx);
		}

		public static int xzt9soTkRltTypCd(int idx) {
			return xzt9soTkPolId(idx) + Len.O_TK_POL_ID;
		}

		public static int xzt9soPolNbr(int idx) {
			return xzt9soTkRltTypCd(idx) + Len.O_TK_RLT_TYP_CD;
		}

		public static int xzt9soPolicyPeriod(int idx) {
			return xzt9soPolNbr(idx) + Len.O_POL_NBR;
		}

		public static int xzt9soPolEffDt(int idx) {
			return xzt9soPolicyPeriod(idx);
		}

		public static int xzt9soPolExpDt(int idx) {
			return xzt9soPolEffDt(idx) + Len.O_POL_EFF_DT;
		}

		public static int xzt9soQteNbr(int idx) {
			return xzt9soPolExpDt(idx) + Len.O_POL_EXP_DT;
		}

		public static int xzt9soActNbr(int idx) {
			return xzt9soQteNbr(idx) + Len.O_QTE_NBR;
		}

		public static int xzt9soActNbrFmt(int idx) {
			return xzt9soActNbr(idx) + Len.O_ACT_NBR;
		}

		public static int xzt9soPrimaryRiskState(int idx) {
			return xzt9soActNbrFmt(idx) + Len.O_ACT_NBR_FMT;
		}

		public static int xzt9soPriRskStAbb(int idx) {
			return xzt9soPrimaryRiskState(idx);
		}

		public static int xzt9soPriRskStDes(int idx) {
			return xzt9soPriRskStAbb(idx) + Len.O_PRI_RSK_ST_ABB;
		}

		public static int xzt9soLineOfBusiness(int idx) {
			return xzt9soPriRskStDes(idx) + Len.O_PRI_RSK_ST_DES;
		}

		public static int xzt9soLobCd(int idx) {
			return xzt9soLineOfBusiness(idx);
		}

		public static int xzt9soLobDes(int idx) {
			return xzt9soLobCd(idx) + Len.O_LOB_CD;
		}

		public static int xzt9soTypeOfBusiness(int idx) {
			return xzt9soLobDes(idx) + Len.O_LOB_DES;
		}

		public static int xzt9soTobCd(int idx) {
			return xzt9soTypeOfBusiness(idx);
		}

		public static int xzt9soTobDes(int idx) {
			return xzt9soTobCd(idx) + Len.O_TOB_CD;
		}

		public static int xzt9soSegmentInformation(int idx) {
			return xzt9soTobDes(idx) + Len.O_TOB_DES;
		}

		public static int xzt9soSegCd(int idx) {
			return xzt9soSegmentInformation(idx);
		}

		public static int xzt9soSegDes(int idx) {
			return xzt9soSegCd(idx) + Len.O_SEG_CD;
		}

		public static int xzt9soLgeActInd(int idx) {
			return xzt9soSegDes(idx) + Len.O_SEG_DES;
		}

		public static int xzt9soMnlPolInd(int idx) {
			return xzt9soLgeActInd(idx) + Len.O_LGE_ACT_IND;
		}

		public static int xzt9soCurrentStatus(int idx) {
			return xzt9soMnlPolInd(idx) + Len.O_MNL_POL_IND;
		}

		public static int xzt9soCurStaCd(int idx) {
			return xzt9soCurrentStatus(idx);
		}

		public static int xzt9soCurStaDes(int idx) {
			return xzt9soCurStaCd(idx) + Len.O_CUR_STA_CD;
		}

		public static int xzt9soCurrentTransaction(int idx) {
			return xzt9soCurStaDes(idx) + Len.O_CUR_STA_DES;
		}

		public static int xzt9soCurTrsTypCd(int idx) {
			return xzt9soCurrentTransaction(idx);
		}

		public static int xzt9soCurTrsTypDes(int idx) {
			return xzt9soCurTrsTypCd(idx) + Len.O_CUR_TRS_TYP_CD;
		}

		public static int xzt9soCurPndTrsInd(int idx) {
			return xzt9soCurTrsTypDes(idx) + Len.O_CUR_TRS_TYP_DES;
		}

		public static int xzt9soCurOgnCd(int idx) {
			return xzt9soCurPndTrsInd(idx) + Len.O_CUR_PND_TRS_IND;
		}

		public static int xzt9soCurOgnDes(int idx) {
			return xzt9soCurOgnCd(idx) + Len.O_CUR_OGN_CD;
		}

		public static int xzt9soCoverageParts(int idx) {
			return xzt9soCurOgnDes(idx) + Len.O_CUR_OGN_DES;
		}

		public static int xzt9soBondInd(int idx) {
			return xzt9soCoverageParts(idx) + Len.O_COVERAGE_PARTS;
		}

		public static int xzt9soPolBranchInfo(int idx) {
			return xzt9soBondInd(idx) + Len.O_BOND_IND;
		}

		public static int xzt9soPbBranchNbr(int idx) {
			return xzt9soPolBranchInfo(idx);
		}

		public static int xzt9soPbBranchDesc(int idx) {
			return xzt9soPbBranchNbr(idx) + Len.O_PB_BRANCH_NBR;
		}

		public static int xzt9soWrtPremAmt(int idx) {
			return xzt9soPbBranchDesc(idx) + Len.O_PB_BRANCH_DESC;
		}

		public static int xzt9soPolActInd(int idx) {
			return xzt9soWrtPremAmt(idx) + Len.O_WRT_PREM_AMT;
		}

		public static int xzt9soCncData(int idx) {
			return xzt9soPolActInd(idx) + Len.O_POL_ACT_IND;
		}

		public static int xzt9soPndCncInd(int idx) {
			return xzt9soCncData(idx);
		}

		public static int xzt9soSchCncDt(int idx) {
			return xzt9soPndCncInd(idx) + Len.O_PND_CNC_IND;
		}

		public static int xzt9soNotTypCd(int idx) {
			return xzt9soSchCncDt(idx) + Len.O_SCH_CNC_DT;
		}

		public static int xzt9soNotTypDes(int idx) {
			return xzt9soNotTypCd(idx) + Len.O_NOT_TYP_CD;
		}

		public static int xzt9soNotEmpId(int idx) {
			return xzt9soNotTypDes(idx) + Len.O_NOT_TYP_DES;
		}

		public static int xzt9soNotEmpNm(int idx) {
			return xzt9soNotEmpId(idx) + Len.O_NOT_EMP_ID;
		}

		public static int xzt9soNotPrcDt(int idx) {
			return xzt9soNotEmpNm(idx) + Len.O_NOT_EMP_NM;
		}

		public static int xzt9soTmnData(int idx) {
			return xzt9soNotPrcDt(idx) + Len.O_NOT_PRC_DT;
		}

		public static int xzt9soTmnInd(int idx) {
			return xzt9soTmnData(idx);
		}

		public static int xzt9soTmnEmpId(int idx) {
			return xzt9soTmnInd(idx) + Len.O_TMN_IND;
		}

		public static int xzt9soTmnEmpNm(int idx) {
			return xzt9soTmnEmpId(idx) + Len.O_TMN_EMP_ID;
		}

		public static int xzt9soTmnPrcDt(int idx) {
			return xzt9soTmnEmpNm(idx) + Len.O_TMN_EMP_NM;
		}

		public static int flr2(int idx) {
			return xzt9soTmnPrcDt(idx) + Len.O_TMN_PRC_DT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_ACT_NBR = 9;
		public static final int I_USERID = 8;
		public static final int FLR1 = 50;
		public static final int O_TK_POL_ID = 16;
		public static final int O_TK_RLT_TYP_CD = 4;
		public static final int O_TECHNICAL_KEYS = O_TK_POL_ID + O_TK_RLT_TYP_CD;
		public static final int O_POL_NBR = 25;
		public static final int O_POL_EFF_DT = 10;
		public static final int O_POL_EXP_DT = 10;
		public static final int O_POLICY_PERIOD = O_POL_EFF_DT + O_POL_EXP_DT;
		public static final int O_QTE_NBR = 5;
		public static final int O_ACT_NBR = 9;
		public static final int O_ACT_NBR_FMT = 9;
		public static final int O_PRI_RSK_ST_ABB = 3;
		public static final int O_PRI_RSK_ST_DES = 25;
		public static final int O_PRIMARY_RISK_STATE = O_PRI_RSK_ST_ABB + O_PRI_RSK_ST_DES;
		public static final int O_LOB_CD = 3;
		public static final int O_LOB_DES = 45;
		public static final int O_LINE_OF_BUSINESS = O_LOB_CD + O_LOB_DES;
		public static final int O_TOB_CD = 3;
		public static final int O_TOB_DES = 40;
		public static final int O_TYPE_OF_BUSINESS = O_TOB_CD + O_TOB_DES;
		public static final int O_SEG_CD = 3;
		public static final int O_SEG_DES = 40;
		public static final int O_SEGMENT_INFORMATION = O_SEG_CD + O_SEG_DES;
		public static final int O_LGE_ACT_IND = 1;
		public static final int O_MNL_POL_IND = 1;
		public static final int O_CUR_STA_CD = 1;
		public static final int O_CUR_STA_DES = 40;
		public static final int O_CURRENT_STATUS = O_CUR_STA_CD + O_CUR_STA_DES;
		public static final int O_CUR_TRS_TYP_CD = 1;
		public static final int O_CUR_TRS_TYP_DES = 35;
		public static final int O_CUR_PND_TRS_IND = 1;
		public static final int O_CUR_OGN_CD = 1;
		public static final int O_CUR_OGN_DES = 20;
		public static final int O_CURRENT_TRANSACTION = O_CUR_TRS_TYP_CD + O_CUR_TRS_TYP_DES + O_CUR_PND_TRS_IND + O_CUR_OGN_CD + O_CUR_OGN_DES;
		public static final int O_COVERAGE_PARTS = 40;
		public static final int O_BOND_IND = 1;
		public static final int O_PB_BRANCH_NBR = 2;
		public static final int O_PB_BRANCH_DESC = 45;
		public static final int O_POL_BRANCH_INFO = O_PB_BRANCH_NBR + O_PB_BRANCH_DESC;
		public static final int O_WRT_PREM_AMT = 11;
		public static final int O_POL_ACT_IND = 1;
		public static final int O_PND_CNC_IND = 1;
		public static final int O_SCH_CNC_DT = 10;
		public static final int O_NOT_TYP_CD = 5;
		public static final int O_NOT_TYP_DES = 35;
		public static final int O_NOT_EMP_ID = 6;
		public static final int O_NOT_EMP_NM = 67;
		public static final int O_NOT_PRC_DT = 10;
		public static final int O_CNC_DATA = O_PND_CNC_IND + O_SCH_CNC_DT + O_NOT_TYP_CD + O_NOT_TYP_DES + O_NOT_EMP_ID + O_NOT_EMP_NM + O_NOT_PRC_DT;
		public static final int O_TMN_IND = 1;
		public static final int O_TMN_EMP_ID = 6;
		public static final int O_TMN_EMP_NM = 67;
		public static final int O_TMN_PRC_DT = 10;
		public static final int O_TMN_DATA = O_TMN_IND + O_TMN_EMP_ID + O_TMN_EMP_NM + O_TMN_PRC_DT;
		public static final int FLR2 = 150;
		public static final int O_POLICY_LIST = O_TECHNICAL_KEYS + O_POL_NBR + O_POLICY_PERIOD + O_QTE_NBR + O_ACT_NBR + O_ACT_NBR_FMT
				+ O_PRIMARY_RISK_STATE + O_LINE_OF_BUSINESS + O_TYPE_OF_BUSINESS + O_SEGMENT_INFORMATION + O_LGE_ACT_IND + O_MNL_POL_IND
				+ O_CURRENT_STATUS + O_CURRENT_TRANSACTION + O_COVERAGE_PARTS + O_BOND_IND + O_POL_BRANCH_INFO + O_WRT_PREM_AMT + O_POL_ACT_IND
				+ O_CNC_DATA + O_TMN_DATA + FLR2;
		public static final int XZT9S0_SERVICE_INPUTS = I_ACT_NBR + I_USERID + FLR1;
		public static final int XZT9S0_SERVICE_OUTPUTS = LServiceContractAreaXz0x90s0.O_POLICY_LIST_MAXOCCURS * O_POLICY_LIST;
		public static final int L_SERVICE_CONTRACT_AREA = XZT9S0_SERVICE_INPUTS + XZT9S0_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int O_QTE_NBR = 5;
			public static final int O_WRT_PREM_AMT = 11;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
