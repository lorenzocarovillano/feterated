/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0021<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0021 extends BytesClass {

	//==== PROPERTIES ====
	public static final int XZT21I_POL_NBR_LIST_MAXOCCURS = 150;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0021() {
	}

	public LServiceContractAreaXz0x0021(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt21iCsrActNbr(String xzt21iCsrActNbr) {
		writeString(Pos.XZT21I_CSR_ACT_NBR, xzt21iCsrActNbr, Len.XZT21I_CSR_ACT_NBR);
	}

	/**Original name: XZT21I-CSR-ACT-NBR<br>*/
	public String getXzt21iCsrActNbr() {
		return readString(Pos.XZT21I_CSR_ACT_NBR, Len.XZT21I_CSR_ACT_NBR);
	}

	public void setXzt21iActTmnDt(String xzt21iActTmnDt) {
		writeString(Pos.XZT21I_ACT_TMN_DT, xzt21iActTmnDt, Len.XZT21I_ACT_TMN_DT);
	}

	/**Original name: XZT21I-ACT-TMN-DT<br>*/
	public String getXzt21iActTmnDt() {
		return readString(Pos.XZT21I_ACT_TMN_DT, Len.XZT21I_ACT_TMN_DT);
	}

	public void setXzt21iNotTypCd(String xzt21iNotTypCd) {
		writeString(Pos.XZT21I_NOT_TYP_CD, xzt21iNotTypCd, Len.XZT21I_NOT_TYP_CD);
	}

	/**Original name: XZT21I-NOT-TYP-CD<br>*/
	public String getXzt21iNotTypCd() {
		return readString(Pos.XZT21I_NOT_TYP_CD, Len.XZT21I_NOT_TYP_CD);
	}

	/**Original name: XZT21I-POL-NBR-LIST<br>*/
	public byte[] getXzt21iPolNbrListBytes(int xzt21iPolNbrListIdx) {
		byte[] buffer = new byte[Len.XZT21I_POL_NBR_LIST];
		return getXzt21iPolNbrListBytes(xzt21iPolNbrListIdx, buffer, 1);
	}

	public byte[] getXzt21iPolNbrListBytes(int xzt21iPolNbrListIdx, byte[] buffer, int offset) {
		int position = Pos.xzt21iPolNbrList(xzt21iPolNbrListIdx - 1);
		getBytes(buffer, offset, Len.XZT21I_POL_NBR_LIST, position);
		return buffer;
	}

	public void setXzt21iPolNbr(int xzt21iPolNbrIdx, String xzt21iPolNbr) {
		int position = Pos.xzt21iPolNbr(xzt21iPolNbrIdx - 1);
		writeString(position, xzt21iPolNbr, Len.XZT21I_POL_NBR);
	}

	/**Original name: XZT21I-POL-NBR<br>*/
	public String getXzt21iPolNbr(int xzt21iPolNbrIdx) {
		int position = Pos.xzt21iPolNbr(xzt21iPolNbrIdx - 1);
		return readString(position, Len.XZT21I_POL_NBR);
	}

	public void setXzt21iUserid(String xzt21iUserid) {
		writeString(Pos.XZT21I_USERID, xzt21iUserid, Len.XZT21I_USERID);
	}

	/**Original name: XZT21I-USERID<br>*/
	public String getXzt21iUserid() {
		return readString(Pos.XZT21I_USERID, Len.XZT21I_USERID);
	}

	public void setXzt21oCsrActNbr(String xzt21oCsrActNbr) {
		writeString(Pos.XZT21O_CSR_ACT_NBR, xzt21oCsrActNbr, Len.XZT21O_CSR_ACT_NBR);
	}

	/**Original name: XZT21O-CSR-ACT-NBR<br>*/
	public String getXzt21oCsrActNbr() {
		return readString(Pos.XZT21O_CSR_ACT_NBR, Len.XZT21O_CSR_ACT_NBR);
	}

	public void setXzt21oNotDt(String xzt21oNotDt) {
		writeString(Pos.XZT21O_NOT_DT, xzt21oNotDt, Len.XZT21O_NOT_DT);
	}

	/**Original name: XZT21O-NOT-DT<br>*/
	public String getXzt21oNotDt() {
		return readString(Pos.XZT21O_NOT_DT, Len.XZT21O_NOT_DT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT021_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT21I_CSR_ACT_NBR = XZT021_SERVICE_INPUTS;
		public static final int XZT21I_ACT_TMN_DT = XZT21I_CSR_ACT_NBR + Len.XZT21I_CSR_ACT_NBR;
		public static final int XZT21I_NOT_TYP_CD = XZT21I_ACT_TMN_DT + Len.XZT21I_ACT_TMN_DT;
		public static final int XZT21I_USERID = xzt21iPolNbr(XZT21I_POL_NBR_LIST_MAXOCCURS - 1) + Len.XZT21I_POL_NBR;
		public static final int XZT021_SERVICE_OUTPUTS = XZT21I_USERID + Len.XZT21I_USERID;
		public static final int XZT21O_CSR_ACT_NBR = XZT021_SERVICE_OUTPUTS;
		public static final int XZT21O_NOT_DT = XZT21O_CSR_ACT_NBR + Len.XZT21O_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt21iPolNbrList(int idx) {
			return XZT21I_NOT_TYP_CD + Len.XZT21I_NOT_TYP_CD + idx * Len.XZT21I_POL_NBR_LIST;
		}

		public static int xzt21iPolNbr(int idx) {
			return xzt21iPolNbrList(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT21I_CSR_ACT_NBR = 9;
		public static final int XZT21I_ACT_TMN_DT = 10;
		public static final int XZT21I_NOT_TYP_CD = 5;
		public static final int XZT21I_POL_NBR = 25;
		public static final int XZT21I_POL_NBR_LIST = XZT21I_POL_NBR;
		public static final int XZT21I_USERID = 8;
		public static final int XZT21O_CSR_ACT_NBR = 9;
		public static final int XZT021_SERVICE_INPUTS = XZT21I_CSR_ACT_NBR + XZT21I_ACT_TMN_DT + XZT21I_NOT_TYP_CD
				+ LServiceContractAreaXz0x0021.XZT21I_POL_NBR_LIST_MAXOCCURS * XZT21I_POL_NBR_LIST + XZT21I_USERID;
		public static final int XZT21O_NOT_DT = 10;
		public static final int XZT021_SERVICE_OUTPUTS = XZT21O_CSR_ACT_NBR + XZT21O_NOT_DT;
		public static final int L_SERVICE_CONTRACT_AREA = XZT021_SERVICE_INPUTS + XZT021_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
