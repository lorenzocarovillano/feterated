/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R90I0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r90i0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A90I1_MAXOCCURS = 10000;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r90i0() {
	}

	public LFrameworkResponseAreaXz0r90i0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza9i0MaxFrmRecRows(int xza9i0MaxFrmRecRows) {
		writeBinaryInt(Pos.XZA9I0_MAX_FRM_REC_ROWS, xza9i0MaxFrmRecRows);
	}

	/**Original name: XZA9I0-MAX-FRM-REC-ROWS<br>*/
	public int getXza9i0MaxFrmRecRows() {
		return readBinaryInt(Pos.XZA9I0_MAX_FRM_REC_ROWS);
	}

	public void setXza9i0CsrActNbr(String xza9i0CsrActNbr) {
		writeString(Pos.XZA9I0_CSR_ACT_NBR, xza9i0CsrActNbr, Len.XZA9I0_CSR_ACT_NBR);
	}

	/**Original name: XZA9I0-CSR-ACT-NBR<br>*/
	public String getXza9i0CsrActNbr() {
		return readString(Pos.XZA9I0_CSR_ACT_NBR, Len.XZA9I0_CSR_ACT_NBR);
	}

	public void setXza9i0NotPrcTs(String xza9i0NotPrcTs) {
		writeString(Pos.XZA9I0_NOT_PRC_TS, xza9i0NotPrcTs, Len.XZA9I0_NOT_PRC_TS);
	}

	/**Original name: XZA9I0-NOT-PRC-TS<br>*/
	public String getXza9i0NotPrcTs() {
		return readString(Pos.XZA9I0_NOT_PRC_TS, Len.XZA9I0_NOT_PRC_TS);
	}

	public String getlFwRespXz0a90i1Formatted(int lFwRespXz0a90i1Idx) {
		int position = Pos.lFwRespXz0a90i1(lFwRespXz0a90i1Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A90I1);
	}

	public void setXza9i1rFrmSeqNbr(int xza9i1rFrmSeqNbrIdx, int xza9i1rFrmSeqNbr) {
		int position = Pos.xza9i1FrmSeqNbr(xza9i1rFrmSeqNbrIdx - 1);
		writeInt(position, xza9i1rFrmSeqNbr, Len.Int.XZA9I1R_FRM_SEQ_NBR);
	}

	/**Original name: XZA9I1R-FRM-SEQ-NBR<br>*/
	public int getXza9i1rFrmSeqNbr(int xza9i1rFrmSeqNbrIdx) {
		int position = Pos.xza9i1FrmSeqNbr(xza9i1rFrmSeqNbrIdx - 1);
		return readNumDispInt(position, Len.XZA9I1_FRM_SEQ_NBR);
	}

	public void setXza9i1rRecSeqNbr(int xza9i1rRecSeqNbrIdx, int xza9i1rRecSeqNbr) {
		int position = Pos.xza9i1RecSeqNbr(xza9i1rRecSeqNbrIdx - 1);
		writeInt(position, xza9i1rRecSeqNbr, Len.Int.XZA9I1R_REC_SEQ_NBR);
	}

	/**Original name: XZA9I1R-REC-SEQ-NBR<br>*/
	public int getXza9i1rRecSeqNbr(int xza9i1rRecSeqNbrIdx) {
		int position = Pos.xza9i1RecSeqNbr(xza9i1rRecSeqNbrIdx - 1);
		return readNumDispInt(position, Len.XZA9I1_REC_SEQ_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_XZ0A90I0 = L_FRAMEWORK_RESPONSE_AREA;
		public static final int XZA9I0_GET_FRM_REC_LIST_KEY = L_FW_RESP_XZ0A90I0;
		public static final int XZA9I0_MAX_FRM_REC_ROWS = XZA9I0_GET_FRM_REC_LIST_KEY;
		public static final int XZA9I0_CSR_ACT_NBR = XZA9I0_MAX_FRM_REC_ROWS + Len.XZA9I0_MAX_FRM_REC_ROWS;
		public static final int XZA9I0_NOT_PRC_TS = XZA9I0_CSR_ACT_NBR + Len.XZA9I0_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a90i1(int idx) {
			return XZA9I0_NOT_PRC_TS + Len.XZA9I0_NOT_PRC_TS + idx * Len.L_FW_RESP_XZ0A90I1;
		}

		public static int xza9i1GetFrmRecListDtl(int idx) {
			return lFwRespXz0a90i1(idx);
		}

		public static int xza9i1FrmSeqNbr(int idx) {
			return xza9i1GetFrmRecListDtl(idx);
		}

		public static int xza9i1RecSeqNbr(int idx) {
			return xza9i1FrmSeqNbr(idx) + Len.XZA9I1_FRM_SEQ_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9I0_MAX_FRM_REC_ROWS = 4;
		public static final int XZA9I0_CSR_ACT_NBR = 9;
		public static final int XZA9I0_NOT_PRC_TS = 26;
		public static final int XZA9I1_FRM_SEQ_NBR = 5;
		public static final int XZA9I1_REC_SEQ_NBR = 5;
		public static final int XZA9I1_GET_FRM_REC_LIST_DTL = XZA9I1_FRM_SEQ_NBR + XZA9I1_REC_SEQ_NBR;
		public static final int L_FW_RESP_XZ0A90I1 = XZA9I1_GET_FRM_REC_LIST_DTL;
		public static final int XZA9I0_GET_FRM_REC_LIST_KEY = XZA9I0_MAX_FRM_REC_ROWS + XZA9I0_CSR_ACT_NBR + XZA9I0_NOT_PRC_TS;
		public static final int L_FW_RESP_XZ0A90I0 = XZA9I0_GET_FRM_REC_LIST_KEY;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_XZ0A90I0
				+ LFrameworkResponseAreaXz0r90i0.L_FW_RESP_XZ0A90I1_MAXOCCURS * L_FW_RESP_XZ0A90I1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA9I1R_FRM_SEQ_NBR = 5;
			public static final int XZA9I1R_REC_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
