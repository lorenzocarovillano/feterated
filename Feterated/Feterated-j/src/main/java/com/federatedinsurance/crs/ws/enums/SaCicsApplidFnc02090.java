/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SA-CICS-APPLID<br>
 * Variable: SA-CICS-APPLID from program FNC02090<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaCicsApplidFnc02090 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.SA_CICS_APPLID);
	private static final String[] DEV1 = new String[] { "DCCICS", "D5CICS" };
	private static final String[] DEV2 = new String[] { "DECICS", "DBCICS", "DKCICS" };
	private static final String[] BETA1 = new String[] { "B1CICS", "B5CICS", "B7CICS" };
	private static final String[] BETA2 = new String[] { "B2CICS", "B4CICS", "B6CICS", "BECICS" };
	private static final String[] EDUC1 = new String[] { "E1CICS", "E3CICS" };
	private static final String[] EDUC2 = new String[] { "E4CICS", "E5CICS" };
	private static final String[] PROD = new String[] { "CICS6", "CICS9" };

	//==== METHODS ====
	public void setSaCicsApplid(String saCicsApplid) {
		this.value = Functions.subString(saCicsApplid, Len.SA_CICS_APPLID);
	}

	public String getSaCicsApplid() {
		return this.value;
	}

	public boolean isDev1() {
		return ArrayUtils.contains(DEV1, value);
	}

	public boolean isDev2() {
		return ArrayUtils.contains(DEV2, value);
	}

	public boolean isBeta1() {
		return ArrayUtils.contains(BETA1, value);
	}

	public boolean isBeta2() {
		return ArrayUtils.contains(BETA2, value);
	}

	public boolean isEduc1() {
		return ArrayUtils.contains(EDUC1, value);
	}

	public boolean isEduc2() {
		return ArrayUtils.contains(EDUC2, value);
	}

	public boolean isProd() {
		return ArrayUtils.contains(PROD, value);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_CICS_APPLID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
