/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: W0420-HDR-EXISTS-SW<br>
 * Variable: W0420-HDR-EXISTS-SW from program HALRRESP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class W0420HdrExistsSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char EXISTS = 'Y';
	public static final char NOT_EXISTS = 'N';

	//==== METHODS ====
	public void setW0420HdrExistsSw(char w0420HdrExistsSw) {
		this.value = w0420HdrExistsSw;
	}

	public char getW0420HdrExistsSw() {
		return this.value;
	}

	public boolean isExists() {
		return value == EXISTS;
	}

	public void setExists() {
		value = EXISTS;
	}

	public void setNotExists() {
		value = NOT_EXISTS;
	}
}
