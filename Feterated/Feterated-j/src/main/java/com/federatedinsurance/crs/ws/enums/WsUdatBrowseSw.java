/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-UDAT-BROWSE-SW<br>
 * Variable: WS-UDAT-BROWSE-SW from program HALRRESP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsUdatBrowseSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char OK = 'Y';
	public static final char NOT_OK = 'N';

	//==== METHODS ====
	public void setsUdatBrowseSw(char sUdatBrowseSw) {
		this.value = sUdatBrowseSw;
	}

	public char getsUdatBrowseSw() {
		return this.value;
	}

	public boolean isOk() {
		return value == OK;
	}

	public void setOk() {
		value = OK;
	}

	public void setNotOk() {
		value = NOT_OK;
	}
}
