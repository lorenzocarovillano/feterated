/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X0005<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x0005 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x0005() {
	}

	public LServiceContractAreaXz0x0005(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setXzt005ServiceInputsBytes(byte[] buffer) {
		setXzt005ServiceInputsBytes(buffer, 1);
	}

	public void setXzt005ServiceInputsBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.XZT005_SERVICE_INPUTS, Pos.XZT005_SERVICE_INPUTS);
	}

	public void setXzt05iCsrActNbr(String xzt05iCsrActNbr) {
		writeString(Pos.XZT05I_CSR_ACT_NBR, xzt05iCsrActNbr, Len.XZT05I_CSR_ACT_NBR);
	}

	/**Original name: XZT05I-CSR-ACT-NBR<br>*/
	public String getXzt05iCsrActNbr() {
		return readString(Pos.XZT05I_CSR_ACT_NBR, Len.XZT05I_CSR_ACT_NBR);
	}

	public void setXzt05iNotPrcTs(String xzt05iNotPrcTs) {
		writeString(Pos.XZT05I_NOT_PRC_TS, xzt05iNotPrcTs, Len.XZT05I_NOT_PRC_TS);
	}

	/**Original name: XZT05I-NOT-PRC-TS<br>*/
	public String getXzt05iNotPrcTs() {
		return readString(Pos.XZT05I_NOT_PRC_TS, Len.XZT05I_NOT_PRC_TS);
	}

	public void setXzt05iUserid(String xzt05iUserid) {
		writeString(Pos.XZT05I_USERID, xzt05iUserid, Len.XZT05I_USERID);
	}

	/**Original name: XZT05I-USERID<br>*/
	public String getXzt05iUserid() {
		return readString(Pos.XZT05I_USERID, Len.XZT05I_USERID);
	}

	public String getXzt05iUseridFormatted() {
		return Functions.padBlanks(getXzt05iUserid(), Len.XZT05I_USERID);
	}

	/**Original name: XZT005-SERVICE-OUTPUTS<br>*/
	public byte[] getXzt005ServiceOutputsBytes() {
		byte[] buffer = new byte[Len.XZT005_SERVICE_OUTPUTS];
		return getXzt005ServiceOutputsBytes(buffer, 1);
	}

	public byte[] getXzt005ServiceOutputsBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.XZT005_SERVICE_OUTPUTS, Pos.XZT005_SERVICE_OUTPUTS);
		return buffer;
	}

	public void setXzt05oTkNotPrcTs(String xzt05oTkNotPrcTs) {
		writeString(Pos.XZT05O_TK_NOT_PRC_TS, xzt05oTkNotPrcTs, Len.XZT05O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT05O-TK-NOT-PRC-TS<br>*/
	public String getXzt05oTkNotPrcTs() {
		return readString(Pos.XZT05O_TK_NOT_PRC_TS, Len.XZT05O_TK_NOT_PRC_TS);
	}

	public void setXzt05oTkActOwnCltId(String xzt05oTkActOwnCltId) {
		writeString(Pos.XZT05O_TK_ACT_OWN_CLT_ID, xzt05oTkActOwnCltId, Len.XZT05O_TK_ACT_OWN_CLT_ID);
	}

	/**Original name: XZT05O-TK-ACT-OWN-CLT-ID<br>*/
	public String getXzt05oTkActOwnCltId() {
		return readString(Pos.XZT05O_TK_ACT_OWN_CLT_ID, Len.XZT05O_TK_ACT_OWN_CLT_ID);
	}

	public void setXzt05oTkActOwnAdrId(String xzt05oTkActOwnAdrId) {
		writeString(Pos.XZT05O_TK_ACT_OWN_ADR_ID, xzt05oTkActOwnAdrId, Len.XZT05O_TK_ACT_OWN_ADR_ID);
	}

	/**Original name: XZT05O-TK-ACT-OWN-ADR-ID<br>*/
	public String getXzt05oTkActOwnAdrId() {
		return readString(Pos.XZT05O_TK_ACT_OWN_ADR_ID, Len.XZT05O_TK_ACT_OWN_ADR_ID);
	}

	public void setXzt05oTkEmpId(String xzt05oTkEmpId) {
		writeString(Pos.XZT05O_TK_EMP_ID, xzt05oTkEmpId, Len.XZT05O_TK_EMP_ID);
	}

	/**Original name: XZT05O-TK-EMP-ID<br>*/
	public String getXzt05oTkEmpId() {
		return readString(Pos.XZT05O_TK_EMP_ID, Len.XZT05O_TK_EMP_ID);
	}

	public void setXzt05oTkStaMdfTs(String xzt05oTkStaMdfTs) {
		writeString(Pos.XZT05O_TK_STA_MDF_TS, xzt05oTkStaMdfTs, Len.XZT05O_TK_STA_MDF_TS);
	}

	/**Original name: XZT05O-TK-STA-MDF-TS<br>*/
	public String getXzt05oTkStaMdfTs() {
		return readString(Pos.XZT05O_TK_STA_MDF_TS, Len.XZT05O_TK_STA_MDF_TS);
	}

	public void setXzt05oTkActNotStaCd(String xzt05oTkActNotStaCd) {
		writeString(Pos.XZT05O_TK_ACT_NOT_STA_CD, xzt05oTkActNotStaCd, Len.XZT05O_TK_ACT_NOT_STA_CD);
	}

	/**Original name: XZT05O-TK-ACT-NOT-STA-CD<br>*/
	public String getXzt05oTkActNotStaCd() {
		return readString(Pos.XZT05O_TK_ACT_NOT_STA_CD, Len.XZT05O_TK_ACT_NOT_STA_CD);
	}

	public void setXzt05oTkSegCd(String xzt05oTkSegCd) {
		writeString(Pos.XZT05O_TK_SEG_CD, xzt05oTkSegCd, Len.XZT05O_TK_SEG_CD);
	}

	/**Original name: XZT05O-TK-SEG-CD<br>*/
	public String getXzt05oTkSegCd() {
		return readString(Pos.XZT05O_TK_SEG_CD, Len.XZT05O_TK_SEG_CD);
	}

	public void setXzt05oTkActTypCd(String xzt05oTkActTypCd) {
		writeString(Pos.XZT05O_TK_ACT_TYP_CD, xzt05oTkActTypCd, Len.XZT05O_TK_ACT_TYP_CD);
	}

	/**Original name: XZT05O-TK-ACT-TYP-CD<br>*/
	public String getXzt05oTkActTypCd() {
		return readString(Pos.XZT05O_TK_ACT_TYP_CD, Len.XZT05O_TK_ACT_TYP_CD);
	}

	public void setXzt05oTkActNotCsumFormatted(String xzt05oTkActNotCsum) {
		writeString(Pos.XZT05O_TK_ACT_NOT_CSUM, Trunc.toUnsignedNumeric(xzt05oTkActNotCsum, Len.XZT05O_TK_ACT_NOT_CSUM), Len.XZT05O_TK_ACT_NOT_CSUM);
	}

	/**Original name: XZT05O-TK-ACT-NOT-CSUM<br>*/
	public int getXzt05oTkActNotCsum() {
		return readNumDispUnsignedInt(Pos.XZT05O_TK_ACT_NOT_CSUM, Len.XZT05O_TK_ACT_NOT_CSUM);
	}

	public void setXzt05oCsrActNbr(String xzt05oCsrActNbr) {
		writeString(Pos.XZT05O_CSR_ACT_NBR, xzt05oCsrActNbr, Len.XZT05O_CSR_ACT_NBR);
	}

	/**Original name: XZT05O-CSR-ACT-NBR<br>*/
	public String getXzt05oCsrActNbr() {
		return readString(Pos.XZT05O_CSR_ACT_NBR, Len.XZT05O_CSR_ACT_NBR);
	}

	public void setXzt05oActNotTypCd(String xzt05oActNotTypCd) {
		writeString(Pos.XZT05O_ACT_NOT_TYP_CD, xzt05oActNotTypCd, Len.XZT05O_ACT_NOT_TYP_CD);
	}

	/**Original name: XZT05O-ACT-NOT-TYP-CD<br>*/
	public String getXzt05oActNotTypCd() {
		return readString(Pos.XZT05O_ACT_NOT_TYP_CD, Len.XZT05O_ACT_NOT_TYP_CD);
	}

	public void setXzt05oActNotTypDesc(String xzt05oActNotTypDesc) {
		writeString(Pos.XZT05O_ACT_NOT_TYP_DESC, xzt05oActNotTypDesc, Len.XZT05O_ACT_NOT_TYP_DESC);
	}

	/**Original name: XZT05O-ACT-NOT-TYP-DESC<br>*/
	public String getXzt05oActNotTypDesc() {
		return readString(Pos.XZT05O_ACT_NOT_TYP_DESC, Len.XZT05O_ACT_NOT_TYP_DESC);
	}

	public void setXzt05oNotDt(String xzt05oNotDt) {
		writeString(Pos.XZT05O_NOT_DT, xzt05oNotDt, Len.XZT05O_NOT_DT);
	}

	/**Original name: XZT05O-NOT-DT<br>*/
	public String getXzt05oNotDt() {
		return readString(Pos.XZT05O_NOT_DT, Len.XZT05O_NOT_DT);
	}

	public void setXzt05oPdcNbr(String xzt05oPdcNbr) {
		writeString(Pos.XZT05O_PDC_NBR, xzt05oPdcNbr, Len.XZT05O_PDC_NBR);
	}

	/**Original name: XZT05O-PDC-NBR<br>*/
	public String getXzt05oPdcNbr() {
		return readString(Pos.XZT05O_PDC_NBR, Len.XZT05O_PDC_NBR);
	}

	public void setXzt05oPdcNm(String xzt05oPdcNm) {
		writeString(Pos.XZT05O_PDC_NM, xzt05oPdcNm, Len.XZT05O_PDC_NM);
	}

	/**Original name: XZT05O-PDC-NM<br>*/
	public String getXzt05oPdcNm() {
		return readString(Pos.XZT05O_PDC_NM, Len.XZT05O_PDC_NM);
	}

	public void setXzt05oTotFeeAmt(AfDecimal xzt05oTotFeeAmt) {
		writeDecimal(Pos.XZT05O_TOT_FEE_AMT, xzt05oTotFeeAmt.copy());
	}

	/**Original name: XZT05O-TOT-FEE-AMT<br>*/
	public AfDecimal getXzt05oTotFeeAmt() {
		return readDecimal(Pos.XZT05O_TOT_FEE_AMT, Len.Int.XZT05O_TOT_FEE_AMT, Len.Fract.XZT05O_TOT_FEE_AMT);
	}

	public void setXzt05oStAbb(String xzt05oStAbb) {
		writeString(Pos.XZT05O_ST_ABB, xzt05oStAbb, Len.XZT05O_ST_ABB);
	}

	/**Original name: XZT05O-ST-ABB<br>*/
	public String getXzt05oStAbb() {
		return readString(Pos.XZT05O_ST_ABB, Len.XZT05O_ST_ABB);
	}

	public void setXzt05oCerHldNotInd(char xzt05oCerHldNotInd) {
		writeChar(Pos.XZT05O_CER_HLD_NOT_IND, xzt05oCerHldNotInd);
	}

	/**Original name: XZT05O-CER-HLD-NOT-IND<br>*/
	public char getXzt05oCerHldNotInd() {
		return readChar(Pos.XZT05O_CER_HLD_NOT_IND);
	}

	public void setXzt05oAddCncDay(int xzt05oAddCncDay) {
		writeInt(Pos.XZT05O_ADD_CNC_DAY, xzt05oAddCncDay, Len.Int.XZT05O_ADD_CNC_DAY);
	}

	public void setXzt05oAddCncDayFormatted(String xzt05oAddCncDay) {
		writeString(Pos.XZT05O_ADD_CNC_DAY, Trunc.toUnsignedNumeric(xzt05oAddCncDay, Len.XZT05O_ADD_CNC_DAY), Len.XZT05O_ADD_CNC_DAY);
	}

	/**Original name: XZT05O-ADD-CNC-DAY<br>*/
	public int getXzt05oAddCncDay() {
		return readNumDispInt(Pos.XZT05O_ADD_CNC_DAY, Len.XZT05O_ADD_CNC_DAY);
	}

	public void setXzt05oReaDes(String xzt05oReaDes) {
		writeString(Pos.XZT05O_REA_DES, xzt05oReaDes, Len.XZT05O_REA_DES);
	}

	/**Original name: XZT05O-REA-DES<br>*/
	public String getXzt05oReaDes() {
		return readString(Pos.XZT05O_REA_DES, Len.XZT05O_REA_DES);
	}

	public void setXzt05oFrmAtcInd(char xzt05oFrmAtcInd) {
		writeChar(Pos.XZT05O_FRM_ATC_IND, xzt05oFrmAtcInd);
	}

	/**Original name: XZT05O-FRM-ATC-IND<br>*/
	public char getXzt05oFrmAtcInd() {
		return readChar(Pos.XZT05O_FRM_ATC_IND);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT005_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int XZT05I_CSR_ACT_NBR = XZT005_SERVICE_INPUTS;
		public static final int XZT05I_NOT_PRC_TS = XZT05I_CSR_ACT_NBR + Len.XZT05I_CSR_ACT_NBR;
		public static final int XZT05I_USERID = XZT05I_NOT_PRC_TS + Len.XZT05I_NOT_PRC_TS;
		public static final int FLR1 = XZT05I_USERID + Len.XZT05I_USERID;
		public static final int XZT005_SERVICE_OUTPUTS = FLR1 + Len.FLR1;
		public static final int XZT05O_TECHNICAL_KEY = XZT005_SERVICE_OUTPUTS;
		public static final int XZT05O_TK_NOT_PRC_TS = XZT05O_TECHNICAL_KEY;
		public static final int XZT05O_TK_ACT_OWN_CLT_ID = XZT05O_TK_NOT_PRC_TS + Len.XZT05O_TK_NOT_PRC_TS;
		public static final int XZT05O_TK_ACT_OWN_ADR_ID = XZT05O_TK_ACT_OWN_CLT_ID + Len.XZT05O_TK_ACT_OWN_CLT_ID;
		public static final int XZT05O_TK_EMP_ID = XZT05O_TK_ACT_OWN_ADR_ID + Len.XZT05O_TK_ACT_OWN_ADR_ID;
		public static final int XZT05O_TK_STA_MDF_TS = XZT05O_TK_EMP_ID + Len.XZT05O_TK_EMP_ID;
		public static final int XZT05O_TK_ACT_NOT_STA_CD = XZT05O_TK_STA_MDF_TS + Len.XZT05O_TK_STA_MDF_TS;
		public static final int XZT05O_TK_SEG_CD = XZT05O_TK_ACT_NOT_STA_CD + Len.XZT05O_TK_ACT_NOT_STA_CD;
		public static final int XZT05O_TK_ACT_TYP_CD = XZT05O_TK_SEG_CD + Len.XZT05O_TK_SEG_CD;
		public static final int XZT05O_TK_ACT_NOT_CSUM = XZT05O_TK_ACT_TYP_CD + Len.XZT05O_TK_ACT_TYP_CD;
		public static final int XZT05O_CSR_ACT_NBR = XZT05O_TK_ACT_NOT_CSUM + Len.XZT05O_TK_ACT_NOT_CSUM;
		public static final int XZT05O_ACT_NOT_TYP = XZT05O_CSR_ACT_NBR + Len.XZT05O_CSR_ACT_NBR;
		public static final int XZT05O_ACT_NOT_TYP_CD = XZT05O_ACT_NOT_TYP;
		public static final int XZT05O_ACT_NOT_TYP_DESC = XZT05O_ACT_NOT_TYP_CD + Len.XZT05O_ACT_NOT_TYP_CD;
		public static final int XZT05O_NOT_DT = XZT05O_ACT_NOT_TYP_DESC + Len.XZT05O_ACT_NOT_TYP_DESC;
		public static final int XZT05O_PDC = XZT05O_NOT_DT + Len.XZT05O_NOT_DT;
		public static final int XZT05O_PDC_NBR = XZT05O_PDC;
		public static final int XZT05O_PDC_NM = XZT05O_PDC_NBR + Len.XZT05O_PDC_NBR;
		public static final int XZT05O_TOT_FEE_AMT = XZT05O_PDC_NM + Len.XZT05O_PDC_NM;
		public static final int XZT05O_ST_ABB = XZT05O_TOT_FEE_AMT + Len.XZT05O_TOT_FEE_AMT;
		public static final int XZT05O_CER_HLD_NOT_IND = XZT05O_ST_ABB + Len.XZT05O_ST_ABB;
		public static final int XZT05O_ADD_CNC_DAY = XZT05O_CER_HLD_NOT_IND + Len.XZT05O_CER_HLD_NOT_IND;
		public static final int XZT05O_REA_DES = XZT05O_ADD_CNC_DAY + Len.XZT05O_ADD_CNC_DAY;
		public static final int XZT05O_FRM_ATC_IND = XZT05O_REA_DES + Len.XZT05O_REA_DES;
		public static final int FLR2 = XZT05O_FRM_ATC_IND + Len.XZT05O_FRM_ATC_IND;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZT05I_CSR_ACT_NBR = 9;
		public static final int XZT05I_NOT_PRC_TS = 26;
		public static final int XZT05I_USERID = 8;
		public static final int FLR1 = 100;
		public static final int XZT05O_TK_NOT_PRC_TS = 26;
		public static final int XZT05O_TK_ACT_OWN_CLT_ID = 64;
		public static final int XZT05O_TK_ACT_OWN_ADR_ID = 64;
		public static final int XZT05O_TK_EMP_ID = 6;
		public static final int XZT05O_TK_STA_MDF_TS = 26;
		public static final int XZT05O_TK_ACT_NOT_STA_CD = 2;
		public static final int XZT05O_TK_SEG_CD = 3;
		public static final int XZT05O_TK_ACT_TYP_CD = 2;
		public static final int XZT05O_TK_ACT_NOT_CSUM = 9;
		public static final int XZT05O_CSR_ACT_NBR = 9;
		public static final int XZT05O_ACT_NOT_TYP_CD = 5;
		public static final int XZT05O_ACT_NOT_TYP_DESC = 35;
		public static final int XZT05O_NOT_DT = 10;
		public static final int XZT05O_PDC_NBR = 5;
		public static final int XZT05O_PDC_NM = 120;
		public static final int XZT05O_TOT_FEE_AMT = 10;
		public static final int XZT05O_ST_ABB = 2;
		public static final int XZT05O_CER_HLD_NOT_IND = 1;
		public static final int XZT05O_ADD_CNC_DAY = 5;
		public static final int XZT05O_REA_DES = 500;
		public static final int XZT05O_FRM_ATC_IND = 1;
		public static final int XZT005_SERVICE_INPUTS = XZT05I_CSR_ACT_NBR + XZT05I_NOT_PRC_TS + XZT05I_USERID + FLR1;
		public static final int XZT05O_TECHNICAL_KEY = XZT05O_TK_NOT_PRC_TS + XZT05O_TK_ACT_OWN_CLT_ID + XZT05O_TK_ACT_OWN_ADR_ID + XZT05O_TK_EMP_ID
				+ XZT05O_TK_STA_MDF_TS + XZT05O_TK_ACT_NOT_STA_CD + XZT05O_TK_SEG_CD + XZT05O_TK_ACT_TYP_CD + XZT05O_TK_ACT_NOT_CSUM;
		public static final int XZT05O_ACT_NOT_TYP = XZT05O_ACT_NOT_TYP_CD + XZT05O_ACT_NOT_TYP_DESC;
		public static final int XZT05O_PDC = XZT05O_PDC_NBR + XZT05O_PDC_NM;
		public static final int FLR2 = 500;
		public static final int XZT005_SERVICE_OUTPUTS = XZT05O_TECHNICAL_KEY + XZT05O_CSR_ACT_NBR + XZT05O_ACT_NOT_TYP + XZT05O_NOT_DT + XZT05O_PDC
				+ XZT05O_TOT_FEE_AMT + XZT05O_ST_ABB + XZT05O_CER_HLD_NOT_IND + XZT05O_ADD_CNC_DAY + XZT05O_REA_DES + XZT05O_FRM_ATC_IND + FLR2;
		public static final int L_SERVICE_CONTRACT_AREA = XZT005_SERVICE_INPUTS + XZT005_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZT05O_TOT_FEE_AMT = 8;
			public static final int XZT05O_ADD_CNC_DAY = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZT05O_TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
