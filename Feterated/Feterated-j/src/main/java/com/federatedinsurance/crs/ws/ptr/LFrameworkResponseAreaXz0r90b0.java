/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R90B0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r90b0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A90B1_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r90b0() {
	}

	public LFrameworkResponseAreaXz0r90b0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public String getlFwRespXz0a90b1Formatted(int lFwRespXz0a90b1Idx) {
		int position = Pos.lFwRespXz0a90b1(lFwRespXz0a90b1Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A90B1);
	}

	public void setlFwRespXz0a90b1Bytes(int lFwRespXz0a90b1Idx, byte[] buffer) {
		setlFwRespXz0a90b1Bytes(lFwRespXz0a90b1Idx, buffer, 1);
	}

	/**Original name: L-FW-RESP-XZ0A90B1<br>*/
	public byte[] getlFwRespXz0a90b1Bytes(int lFwRespXz0a90b1Idx) {
		byte[] buffer = new byte[Len.L_FW_RESP_XZ0A90B1];
		return getlFwRespXz0a90b1Bytes(lFwRespXz0a90b1Idx, buffer, 1);
	}

	public void setlFwRespXz0a90b1Bytes(int lFwRespXz0a90b1Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a90b1(lFwRespXz0a90b1Idx - 1);
		setBytes(buffer, offset, Len.L_FW_RESP_XZ0A90B1, position);
	}

	public byte[] getlFwRespXz0a90b1Bytes(int lFwRespXz0a90b1Idx, byte[] buffer, int offset) {
		int position = Pos.lFwRespXz0a90b1(lFwRespXz0a90b1Idx - 1);
		getBytes(buffer, offset, Len.L_FW_RESP_XZ0A90B1, position);
		return buffer;
	}

	public void setXza9b1rPolNbr(int xza9b1rPolNbrIdx, String xza9b1rPolNbr) {
		int position = Pos.xza9b1PolNbr(xza9b1rPolNbrIdx - 1);
		writeString(position, xza9b1rPolNbr, Len.XZA9B1_POL_NBR);
	}

	/**Original name: XZA9B1R-POL-NBR<br>*/
	public String getXza9b1rPolNbr(int xza9b1rPolNbrIdx) {
		int position = Pos.xza9b1PolNbr(xza9b1rPolNbrIdx - 1);
		return readString(position, Len.XZA9B1_POL_NBR);
	}

	public void setXza9b1rNbrOfNotDay(int xza9b1rNbrOfNotDayIdx, int xza9b1rNbrOfNotDay) {
		int position = Pos.xza9b1NbrOfNotDay(xza9b1rNbrOfNotDayIdx - 1);
		writeInt(position, xza9b1rNbrOfNotDay, Len.Int.XZA9B1R_NBR_OF_NOT_DAY);
	}

	/**Original name: XZA9B1R-NBR-OF-NOT-DAY<br>*/
	public int getXza9b1rNbrOfNotDay(int xza9b1rNbrOfNotDayIdx) {
		int position = Pos.xza9b1NbrOfNotDay(xza9b1rNbrOfNotDayIdx - 1);
		return readNumDispInt(position, Len.XZA9B1_NBR_OF_NOT_DAY);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a90b1(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A90B1;
		}

		public static int xza9b1GetNotDaysRqrDtl(int idx) {
			return lFwRespXz0a90b1(idx);
		}

		public static int xza9b1PolNbr(int idx) {
			return xza9b1GetNotDaysRqrDtl(idx);
		}

		public static int xza9b1NbrOfNotDay(int idx) {
			return xza9b1PolNbr(idx) + Len.XZA9B1_POL_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9B1_POL_NBR = 25;
		public static final int XZA9B1_NBR_OF_NOT_DAY = 5;
		public static final int XZA9B1_GET_NOT_DAYS_RQR_DTL = XZA9B1_POL_NBR + XZA9B1_NBR_OF_NOT_DAY;
		public static final int L_FW_RESP_XZ0A90B1 = XZA9B1_GET_NOT_DAYS_RQR_DTL;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0r90b0.L_FW_RESP_XZ0A90B1_MAXOCCURS * L_FW_RESP_XZ0A90B1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA9B1R_NBR_OF_NOT_DAY = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
