/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: O-IN-HOUSE-LASER-198-FILE<br>
 * File: O-IN-HOUSE-LASER-198-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OInHouseLaser198FileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: ILO-CARRIAGE-CONTROL
	private String iloCarriageControl = DefaultValues.stringVal(Len.ILO_CARRIAGE_CONTROL);
	//Original name: ILO-DATA
	private String iloData = DefaultValues.stringVal(Len.ILO_DATA);

	//==== METHODS ====
	public void setoInHouseLaser198RecordBytes(byte[] buffer, int offset) {
		int position = offset;
		iloCarriageControl = MarshalByte.readFixedString(buffer, position, Len.ILO_CARRIAGE_CONTROL);
		position += Len.ILO_CARRIAGE_CONTROL;
		iloData = MarshalByte.readString(buffer, position, Len.ILO_DATA);
	}

	public byte[] getoInHouseLaser198RecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, iloCarriageControl, Len.ILO_CARRIAGE_CONTROL);
		position += Len.ILO_CARRIAGE_CONTROL;
		MarshalByte.writeString(buffer, position, iloData, Len.ILO_DATA);
		return buffer;
	}

	public void setIloCarriageControlFormatted(String iloCarriageControl) {
		this.iloCarriageControl = Trunc.toUnsignedNumeric(iloCarriageControl, Len.ILO_CARRIAGE_CONTROL);
	}

	public short getIloCarriageControl() {
		return NumericDisplay.asShort(this.iloCarriageControl);
	}

	public void setIloData(String iloData) {
		this.iloData = Functions.subString(iloData, Len.ILO_DATA);
	}

	public String getIloData() {
		return this.iloData;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoInHouseLaser198RecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoInHouseLaser198RecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_IN_HOUSE_LASER198_RECORD;
	}

	@Override
	public IBuffer copy() {
		OInHouseLaser198FileTO copyTO = new OInHouseLaser198FileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ILO_CARRIAGE_CONTROL = 1;
		public static final int ILO_DATA = 198;
		public static final int O_IN_HOUSE_LASER198_RECORD = ILO_CARRIAGE_CONTROL + ILO_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
