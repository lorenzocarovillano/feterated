/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-NOTHING-ADDED-MSG<br>
 * Variable: EA-01-NOTHING-ADDED-MSG from program XZ0P90H0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01NothingAddedMsgXz0p90h0 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG
	private String flr1 = "No policy was";
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-1
	private String flr2 = "added for this";
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-2
	private String flr3 = "Account =";
	//Original name: EA-01-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-3
	private String flr4 = "; Notification";
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-4
	private String flr5 = "TimeStamp =";
	//Original name: EA-01-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FILLER-EA-01-NOTHING-ADDED-MSG-5
	private char flr6 = '.';

	//==== METHODS ====
	public String getEa01NothingAddedMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01NothingAddedMsgBytes());
	}

	public byte[] getEa01NothingAddedMsgBytes() {
		byte[] buffer = new byte[Len.EA01_NOTHING_ADDED_MSG];
		return getEa01NothingAddedMsgBytes(buffer, 1);
	}

	public byte[] getEa01NothingAddedMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeChar(buffer, position, flr6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public char getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 14;
		public static final int FLR2 = 15;
		public static final int FLR3 = 10;
		public static final int FLR5 = 12;
		public static final int FLR6 = 1;
		public static final int EA01_NOTHING_ADDED_MSG = CSR_ACT_NBR + NOT_PRC_TS + FLR1 + 2 * FLR2 + FLR3 + FLR5 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
