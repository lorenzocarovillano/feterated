/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TB-RA-INF<br>
 * Variables: TB-RA-INF from program TS548099<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TbRaInf {

	//==== PROPERTIES ====
	//Original name: TB-RA-REGION
	private String tbRaRegion = DefaultValues.stringVal(Len.TB_RA_REGION);

	//==== METHODS ====
	public void setTbRaRegion(String tbRaRegion) {
		this.tbRaRegion = Functions.subString(tbRaRegion, Len.TB_RA_REGION);
	}

	public String getTbRaRegion() {
		return this.tbRaRegion;
	}

	public String getTbRaRegionFormatted() {
		return Functions.padBlanks(getTbRaRegion(), Len.TB_RA_REGION);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TB_RA_REGION = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
