/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: HELFH-HAL-ERR-LOG-FAIL-ROW<br>
 * Variable: HELFH-HAL-ERR-LOG-FAIL-ROW from copybook HALLHELF<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class HelfhHalErrLogFailRow {

	//==== PROPERTIES ====
	//Original name: HELFH-HELF-ERR-RFR-NBR
	private String helfErrRfrNbr = DefaultValues.stringVal(Len.HELF_ERR_RFR_NBR);

	//==== METHODS ====
	public void setHelfErrRfrNbr(String helfErrRfrNbr) {
		this.helfErrRfrNbr = Functions.subString(helfErrRfrNbr, Len.HELF_ERR_RFR_NBR);
	}

	public String getHelfErrRfrNbr() {
		return this.helfErrRfrNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_ID = 32;
		public static final int FAIL_TS = 26;
		public static final int HELF_FAIL_APP_NM = 10;
		public static final int HELF_FAIL_PLF = 10;
		public static final int FAIL_ACY_CD = 8;
		public static final int HELF_FAIL_PGM_NM = 32;
		public static final int HELF_FAIL_PARA_NM = 30;
		public static final int HELF_ERR_ADD_TXT = 32;
		public static final int HELF_ERR_CMT_TXT = 50;
		public static final int HELF_FAIL_UOW_NM = 32;
		public static final int HELF_FAIL_LOC_NM = 32;
		public static final int HELF_SQLERRMC_TXT = 70;
		public static final int USERID = 32;
		public static final int HELF_FAIL_KEY_TXT = 200;
		public static final int HELF_ACY_TXT = 250;
		public static final int HELF_ERR_RFR_NBR = 10;
		public static final int HELF_ERR_TXT = 100;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
