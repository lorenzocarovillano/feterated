/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: W-UBOC-MSG-ID-SCAN<br>
 * Variable: W-UBOC-MSG-ID-SCAN from program HALOUIDG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WUbocMsgIdScan {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char START_OF_UBOC_MSG_ID = Types.SPACE_CHAR;
	public static final char END_OF_UBOC_MSG_ID = 'E';

	//==== METHODS ====
	public void setUbocMsgIdScan(char ubocMsgIdScan) {
		this.value = ubocMsgIdScan;
	}

	public void setUbocMsgIdScanFormatted(String ubocMsgIdScan) {
		setUbocMsgIdScan(Functions.charAt(ubocMsgIdScan, Types.CHAR_SIZE));
	}

	public char getUbocMsgIdScan() {
		return this.value;
	}

	public void setStartOfUbocMsgId() {
		setUbocMsgIdScanFormatted(String.valueOf(START_OF_UBOC_MSG_ID));
	}

	public boolean isEndOfUbocMsgId() {
		return value == END_OF_UBOC_MSG_ID;
	}

	public void setEndOfUbocMsgId() {
		value = END_OF_UBOC_MSG_ID;
	}
}
