/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ActNotPol1Xz0b9050;
import com.federatedinsurance.crs.ActNotPolFrmXz0b9050;
import com.federatedinsurance.crs.commons.data.to.IActNotPol;
import com.federatedinsurance.crs.commons.data.to.IPolDtaExtRfrCmp;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclactNotPolFrm;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclpolDtaExt;
import com.federatedinsurance.crs.copy.DclrfrCmp;
import com.federatedinsurance.crs.copy.DclstsPolTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.ToPolByPolOwn;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.redefines.TableOfPolTypCdToSearch;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9050<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9050Data implements IActNotPol, IPolDtaExtRfrCmp {

	//==== PROPERTIES ====
	public static final int TO_POL_BY_POL_OWN_MAXOCCURS = 100;
	private ActNotPolFrmXz0b9050 actNotPolFrmXz0b9050 = new ActNotPolFrmXz0b9050(this);
	private ActNotPol1Xz0b9050 actNotPol1Xz0b9050 = new ActNotPol1Xz0b9050(this);
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b9050 constantFields = new ConstantFieldsXz0b9050();
	//Original name: EA-01-NFD-POL-LIST-BY-NOT-MSG
	private Ea01NfdPolListByNotMsg ea01NfdPolListByNotMsg = new Ea01NfdPolListByNotMsg();
	//Original name: EA-02-NFD-POL-LIST-BY-FRM-MSG
	private Ea02NfdPolListByFrmMsg ea02NfdPolListByFrmMsg = new Ea02NfdPolListByFrmMsg();
	/**Original name: NI-NOT-EFF-DT<br>
	 * <pre>* NULL INDICATORS</pre>*/
	private short niNotEffDt = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-POL-DUE-AMT
	private short niPolDueAmt = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-WF-STARTED-IND
	private short niWfStartedInd = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-POL-BIL-STA-CD
	private short niPolBilStaCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-TOT-FEE-AMT
	private short niTotFeeAmt = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-TP
	private short ssTp = DefaultValues.BIN_SHORT_VAL;
	//Original name: SWITCHES
	private SwitchesXz0b9050 switches = new SwitchesXz0b9050();
	/**Original name: TABLE-OF-POL-TYP-CD-TO-SEARCH<br>
	 * <pre>*****************************************************************
	 *     POLICY TYPE CODES TO SEARCH FOR
	 * *****************************************************************</pre>*/
	private TableOfPolTypCdToSearch tableOfPolTypCdToSearch = new TableOfPolTypCdToSearch();
	//Original name: TO-POL-BY-POL-OWN
	private ToPolByPolOwn[] toPolByPolOwn = new ToPolByPolOwn[TO_POL_BY_POL_OWN_MAXOCCURS];
	//Original name: IX-TP
	private int ixTp = 1;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9050 workingStorageArea = new WorkingStorageAreaXz0b9050();
	//Original name: WS-XZ0A9050-ROW
	private WsXz0a9050Row wsXz0a9050Row = new WsXz0a9050Row();
	//Original name: WS-XZ0A9051-ROW
	private WsXz0a9051Row wsXz0a9051Row = new WsXz0a9051Row();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: DCLACT-NOT-POL-FRM
	private DclactNotPolFrm dclactNotPolFrm = new DclactNotPolFrm();
	//Original name: DCLRFR-CMP
	private DclrfrCmp dclrfrCmp = new DclrfrCmp();
	//Original name: DCLSTS-POL-TYP
	private DclstsPolTyp dclstsPolTyp = new DclstsPolTyp();
	//Original name: DCLPOL-DTA-EXT
	private DclpolDtaExt dclpolDtaExt = new DclpolDtaExt();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== CONSTRUCTORS ====
	public Xz0b9050Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int toPolByPolOwnIdx = 1; toPolByPolOwnIdx <= TO_POL_BY_POL_OWN_MAXOCCURS; toPolByPolOwnIdx++) {
			toPolByPolOwn[toPolByPolOwnIdx - 1] = new ToPolByPolOwn();
		}
	}

	public void setNiNotEffDt(short niNotEffDt) {
		this.niNotEffDt = niNotEffDt;
	}

	public short getNiNotEffDt() {
		return this.niNotEffDt;
	}

	public void setNiPolDueAmt(short niPolDueAmt) {
		this.niPolDueAmt = niPolDueAmt;
	}

	public short getNiPolDueAmt() {
		return this.niPolDueAmt;
	}

	public void setNiWfStartedInd(short niWfStartedInd) {
		this.niWfStartedInd = niWfStartedInd;
	}

	public short getNiWfStartedInd() {
		return this.niWfStartedInd;
	}

	public void setNiPolBilStaCd(short niPolBilStaCd) {
		this.niPolBilStaCd = niPolBilStaCd;
	}

	public short getNiPolBilStaCd() {
		return this.niPolBilStaCd;
	}

	public void setNiTotFeeAmt(short niTotFeeAmt) {
		this.niTotFeeAmt = niTotFeeAmt;
	}

	public short getNiTotFeeAmt() {
		return this.niTotFeeAmt;
	}

	public void setSsTp(short ssTp) {
		this.ssTp = ssTp;
	}

	public short getSsTp() {
		return this.ssTp;
	}

	public void initTableOfPolByPolOwnHighValues() {
		for (int idx = 1; idx <= TO_POL_BY_POL_OWN_MAXOCCURS; idx++) {
			toPolByPolOwn[idx - 1].initToPolByPolOwnHighValues();
		}
	}

	public void setIxTp(int ixTp) {
		this.ixTp = ixTp;
	}

	public int getIxTp() {
		return this.ixTp;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public ActNotPol1Xz0b9050 getActNotPol1Xz0b9050() {
		return actNotPol1Xz0b9050;
	}

	public ActNotPolFrmXz0b9050 getActNotPolFrmXz0b9050() {
		return actNotPolFrmXz0b9050;
	}

	@Override
	public String getCd() {
		return dclpolDtaExt.getCmpCd();
	}

	@Override
	public void setCd(String cd) {
		this.dclpolDtaExt.setCmpCd(cd);
	}

	public ConstantFieldsXz0b9050 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		return dclactNotPol.getCsrActNbr();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.dclactNotPol.setCsrActNbr(csrActNbr);
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclactNotPolFrm getDclactNotPolFrm() {
		return dclactNotPolFrm;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclpolDtaExt getDclpolDtaExt() {
		return dclpolDtaExt;
	}

	public DclrfrCmp getDclrfrCmp() {
		return dclrfrCmp;
	}

	public DclstsPolTyp getDclstsPolTyp() {
		return dclstsPolTyp;
	}

	@Override
	public String getDes() {
		return dclrfrCmp.getCmpDes();
	}

	@Override
	public void setDes(String des) {
		this.dclrfrCmp.setCmpDes(des);
	}

	public Ea01NfdPolListByNotMsg getEa01NfdPolListByNotMsg() {
		return ea01NfdPolListByNotMsg;
	}

	public Ea02NfdPolListByFrmMsg getEa02NfdPolListByFrmMsg() {
		return ea02NfdPolListByFrmMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	@Override
	public String getNinAdrId() {
		return dclactNotPol.getNinAdrId();
	}

	@Override
	public void setNinAdrId(String ninAdrId) {
		this.dclactNotPol.setNinAdrId(ninAdrId);
	}

	@Override
	public String getNinCltId() {
		return dclactNotPol.getNinCltId();
	}

	@Override
	public void setNinCltId(String ninCltId) {
		this.dclactNotPol.setNinCltId(ninCltId);
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotEffDt() {
		return dclactNotPol.getNotEffDt();
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		this.dclactNotPol.setNotEffDt(notEffDt);
	}

	@Override
	public String getNotEffDtObj() {
		if (getNiNotEffDt() >= 0) {
			return getNotEffDt();
		} else {
			return null;
		}
	}

	@Override
	public void setNotEffDtObj(String notEffDtObj) {
		if (notEffDtObj != null) {
			setNotEffDt(notEffDtObj);
			setNiNotEffDt(((short) 0));
		} else {
			setNiNotEffDt(((short) -1));
		}
	}

	@Override
	public String getNotPrcTs() {
		return dclactNotPol.getNotPrcTs();
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.dclactNotPol.setNotPrcTs(notPrcTs);
	}

	@Override
	public char getPolBilStaCd() {
		return dclactNotPol.getPolBilStaCd();
	}

	@Override
	public void setPolBilStaCd(char polBilStaCd) {
		this.dclactNotPol.setPolBilStaCd(polBilStaCd);
	}

	@Override
	public Character getPolBilStaCdObj() {
		if (getNiPolBilStaCd() >= 0) {
			return (getPolBilStaCd());
		} else {
			return null;
		}
	}

	@Override
	public void setPolBilStaCdObj(Character polBilStaCdObj) {
		if (polBilStaCdObj != null) {
			setPolBilStaCd((polBilStaCdObj));
			setNiPolBilStaCd(((short) 0));
		} else {
			setNiPolBilStaCd(((short) -1));
		}
	}

	@Override
	public AfDecimal getPolDueAmt() {
		return dclactNotPol.getPolDueAmt();
	}

	@Override
	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.dclactNotPol.setPolDueAmt(polDueAmt.copy());
	}

	@Override
	public AfDecimal getPolDueAmtObj() {
		if (getNiPolDueAmt() >= 0) {
			return getPolDueAmt().toString();
		} else {
			return null;
		}
	}

	@Override
	public void setPolDueAmtObj(AfDecimal polDueAmtObj) {
		if (polDueAmtObj != null) {
			setPolDueAmt(new AfDecimal(polDueAmtObj, 10, 2));
			setNiPolDueAmt(((short) 0));
		} else {
			setNiPolDueAmt(((short) -1));
		}
	}

	@Override
	public String getPolEffDt() {
		return dclactNotPol.getPolEffDt();
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		this.dclactNotPol.setPolEffDt(polEffDt);
	}

	@Override
	public String getPolExpDt() {
		return dclactNotPol.getPolExpDt();
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		this.dclactNotPol.setPolExpDt(polExpDt);
	}

	@Override
	public String getPolNbr() {
		return dclactNotPol.getPolNbr();
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.dclactNotPol.setPolNbr(polNbr);
	}

	@Override
	public String getPolPriRskStAbb() {
		return dclactNotPol.getPolPriRskStAbb();
	}

	@Override
	public void setPolPriRskStAbb(String polPriRskStAbb) {
		this.dclactNotPol.setPolPriRskStAbb(polPriRskStAbb);
	}

	@Override
	public String getPolTypCd() {
		return dclactNotPol.getPolTypCd();
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		this.dclactNotPol.setPolTypCd(polTypCd);
	}

	public SwitchesXz0b9050 getSwitches() {
		return switches;
	}

	public TableOfPolTypCdToSearch getTableOfPolTypCdToSearch() {
		return tableOfPolTypCdToSearch;
	}

	public ToPolByPolOwn getToPolByPolOwn(int idx) {
		return toPolByPolOwn[idx - 1];
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	@Override
	public char getWfStartedInd() {
		return dclactNotPol.getWfStartedInd();
	}

	@Override
	public void setWfStartedInd(char wfStartedInd) {
		this.dclactNotPol.setWfStartedInd(wfStartedInd);
	}

	@Override
	public Character getWfStartedIndObj() {
		if (getNiWfStartedInd() >= 0) {
			return (getWfStartedInd());
		} else {
			return null;
		}
	}

	@Override
	public void setWfStartedIndObj(Character wfStartedIndObj) {
		if (wfStartedIndObj != null) {
			setWfStartedInd((wfStartedIndObj));
			setNiWfStartedInd(((short) 0));
		} else {
			setNiWfStartedInd(((short) -1));
		}
	}

	public WorkingStorageAreaXz0b9050 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9050Row getWsXz0a9050Row() {
		return wsXz0a9050Row;
	}

	public WsXz0a9051Row getWsXz0a9051Row() {
		return wsXz0a9051Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
