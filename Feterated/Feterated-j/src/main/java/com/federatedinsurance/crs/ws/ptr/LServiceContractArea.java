/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program MU0X0004<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractArea extends BytesClass {

	//==== PROPERTIES ====
	public static final int MUT04O_SE_SIC_MAXOCCURS = 4;

	//==== CONSTRUCTORS ====
	public LServiceContractArea() {
	}

	public LServiceContractArea(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setMut04iTkCltId(String mut04iTkCltId) {
		writeString(Pos.MUT04I_TK_CLT_ID, mut04iTkCltId, Len.MUT04I_TK_CLT_ID);
	}

	/**Original name: MUT04I-TK-CLT-ID<br>*/
	public String getMut04iTkCltId() {
		return readString(Pos.MUT04I_TK_CLT_ID, Len.MUT04I_TK_CLT_ID);
	}

	public void setMut04iTkCiorShwObjKey(String mut04iTkCiorShwObjKey) {
		writeString(Pos.MUT04I_TK_CIOR_SHW_OBJ_KEY, mut04iTkCiorShwObjKey, Len.MUT04I_TK_CIOR_SHW_OBJ_KEY);
	}

	/**Original name: MUT04I-TK-CIOR-SHW-OBJ-KEY<br>*/
	public String getMut04iTkCiorShwObjKey() {
		return readString(Pos.MUT04I_TK_CIOR_SHW_OBJ_KEY, Len.MUT04I_TK_CIOR_SHW_OBJ_KEY);
	}

	public String getMut04iTkCiorShwObjKeyFormatted() {
		return Functions.padBlanks(getMut04iTkCiorShwObjKey(), Len.MUT04I_TK_CIOR_SHW_OBJ_KEY);
	}

	public void setMut04iTkAdrSeqNbr(int mut04iTkAdrSeqNbr) {
		writeInt(Pos.MUT04I_TK_ADR_SEQ_NBR, mut04iTkAdrSeqNbr, Len.Int.MUT04I_TK_ADR_SEQ_NBR);
	}

	/**Original name: MUT04I-TK-ADR-SEQ-NBR<br>*/
	public int getMut04iTkAdrSeqNbr() {
		return readNumDispInt(Pos.MUT04I_TK_ADR_SEQ_NBR, Len.MUT04I_TK_ADR_SEQ_NBR);
	}

	public String getMut04iTkAdrSeqNbrFormatted() {
		return readFixedString(Pos.MUT04I_TK_ADR_SEQ_NBR, Len.MUT04I_TK_ADR_SEQ_NBR);
	}

	public void setMut04iUsrId(String mut04iUsrId) {
		writeString(Pos.MUT04I_USR_ID, mut04iUsrId, Len.MUT04I_USR_ID);
	}

	/**Original name: MUT04I-USR-ID<br>*/
	public String getMut04iUsrId() {
		return readString(Pos.MUT04I_USR_ID, Len.MUT04I_USR_ID);
	}

	public void setMut04iActNbr(String mut04iActNbr) {
		writeString(Pos.MUT04I_ACT_NBR, mut04iActNbr, Len.MUT04I_ACT_NBR);
	}

	/**Original name: MUT04I-ACT-NBR<br>*/
	public String getMut04iActNbr() {
		return readString(Pos.MUT04I_ACT_NBR, Len.MUT04I_ACT_NBR);
	}

	public String getMut04iActNbrFormatted() {
		return Functions.padBlanks(getMut04iActNbr(), Len.MUT04I_ACT_NBR);
	}

	public void setMut04iTerNbr(String mut04iTerNbr) {
		writeString(Pos.MUT04I_TER_NBR, mut04iTerNbr, Len.MUT04I_TER_NBR);
	}

	/**Original name: MUT04I-TER-NBR<br>*/
	public String getMut04iTerNbr() {
		return readString(Pos.MUT04I_TER_NBR, Len.MUT04I_TER_NBR);
	}

	public void setMut04iAsOfDt(String mut04iAsOfDt) {
		writeString(Pos.MUT04I_AS_OF_DT, mut04iAsOfDt, Len.MUT04I_AS_OF_DT);
	}

	/**Original name: MUT04I-AS-OF-DT<br>*/
	public String getMut04iAsOfDt() {
		return readString(Pos.MUT04I_AS_OF_DT, Len.MUT04I_AS_OF_DT);
	}

	public void setMut04iFein(String mut04iFein) {
		writeString(Pos.MUT04I_FEIN, mut04iFein, Len.MUT04I_FEIN);
	}

	/**Original name: MUT04I-FEIN<br>*/
	public String getMut04iFein() {
		return readString(Pos.MUT04I_FEIN, Len.MUT04I_FEIN);
	}

	public String getMut04iFeinFormatted() {
		return Functions.padBlanks(getMut04iFein(), Len.MUT04I_FEIN);
	}

	public void setMut04iSsn(String mut04iSsn) {
		writeString(Pos.MUT04I_SSN, mut04iSsn, Len.MUT04I_SSN);
	}

	/**Original name: MUT04I-SSN<br>*/
	public String getMut04iSsn() {
		return readString(Pos.MUT04I_SSN, Len.MUT04I_SSN);
	}

	public String getMut04iSsnFormatted() {
		return Functions.padBlanks(getMut04iSsn(), Len.MUT04I_SSN);
	}

	public void setMut04iPhnAcd(String mut04iPhnAcd) {
		writeString(Pos.MUT04I_PHN_ACD, mut04iPhnAcd, Len.MUT04I_PHN_ACD);
	}

	/**Original name: MUT04I-PHN-ACD<br>*/
	public String getMut04iPhnAcd() {
		return readString(Pos.MUT04I_PHN_ACD, Len.MUT04I_PHN_ACD);
	}

	public void setMut04iPhnPfxNbr(String mut04iPhnPfxNbr) {
		writeString(Pos.MUT04I_PHN_PFX_NBR, mut04iPhnPfxNbr, Len.MUT04I_PHN_PFX_NBR);
	}

	/**Original name: MUT04I-PHN-PFX-NBR<br>*/
	public String getMut04iPhnPfxNbr() {
		return readString(Pos.MUT04I_PHN_PFX_NBR, Len.MUT04I_PHN_PFX_NBR);
	}

	public void setMut04iPhnLinNbr(String mut04iPhnLinNbr) {
		writeString(Pos.MUT04I_PHN_LIN_NBR, mut04iPhnLinNbr, Len.MUT04I_PHN_LIN_NBR);
	}

	/**Original name: MUT04I-PHN-LIN-NBR<br>*/
	public String getMut04iPhnLinNbr() {
		return readString(Pos.MUT04I_PHN_LIN_NBR, Len.MUT04I_PHN_LIN_NBR);
	}

	public void setMut04oTkCltId(String mut04oTkCltId) {
		writeString(Pos.MUT04O_TK_CLT_ID, mut04oTkCltId, Len.MUT04O_TK_CLT_ID);
	}

	/**Original name: MUT04O-TK-CLT-ID<br>*/
	public String getMut04oTkCltId() {
		return readString(Pos.MUT04O_TK_CLT_ID, Len.MUT04O_TK_CLT_ID);
	}

	public void setMut04oCnIdvNmInd(char mut04oCnIdvNmInd) {
		writeChar(Pos.MUT04O_CN_IDV_NM_IND, mut04oCnIdvNmInd);
	}

	/**Original name: MUT04O-CN-IDV-NM-IND<br>*/
	public char getMut04oCnIdvNmInd() {
		return readChar(Pos.MUT04O_CN_IDV_NM_IND);
	}

	public void setMut04oCnDsyNm(String mut04oCnDsyNm) {
		writeString(Pos.MUT04O_CN_DSY_NM, mut04oCnDsyNm, Len.MUT04O_CN_DSY_NM);
	}

	/**Original name: MUT04O-CN-DSY-NM<br>*/
	public String getMut04oCnDsyNm() {
		return readString(Pos.MUT04O_CN_DSY_NM, Len.MUT04O_CN_DSY_NM);
	}

	public String getMut04oCnDsyNmFormatted() {
		return Functions.padBlanks(getMut04oCnDsyNm(), Len.MUT04O_CN_DSY_NM);
	}

	public void setMut04oCnSrNm(String mut04oCnSrNm) {
		writeString(Pos.MUT04O_CN_SR_NM, mut04oCnSrNm, Len.MUT04O_CN_SR_NM);
	}

	/**Original name: MUT04O-CN-SR-NM<br>*/
	public String getMut04oCnSrNm() {
		return readString(Pos.MUT04O_CN_SR_NM, Len.MUT04O_CN_SR_NM);
	}

	public void setMut04oCnFstNm(String mut04oCnFstNm) {
		writeString(Pos.MUT04O_CN_FST_NM, mut04oCnFstNm, Len.MUT04O_CN_FST_NM);
	}

	/**Original name: MUT04O-CN-FST-NM<br>*/
	public String getMut04oCnFstNm() {
		return readString(Pos.MUT04O_CN_FST_NM, Len.MUT04O_CN_FST_NM);
	}

	public void setMut04oCnMdlNm(String mut04oCnMdlNm) {
		writeString(Pos.MUT04O_CN_MDL_NM, mut04oCnMdlNm, Len.MUT04O_CN_MDL_NM);
	}

	/**Original name: MUT04O-CN-MDL-NM<br>*/
	public String getMut04oCnMdlNm() {
		return readString(Pos.MUT04O_CN_MDL_NM, Len.MUT04O_CN_MDL_NM);
	}

	public void setMut04oCnLstNm(String mut04oCnLstNm) {
		writeString(Pos.MUT04O_CN_LST_NM, mut04oCnLstNm, Len.MUT04O_CN_LST_NM);
	}

	/**Original name: MUT04O-CN-LST-NM<br>*/
	public String getMut04oCnLstNm() {
		return readString(Pos.MUT04O_CN_LST_NM, Len.MUT04O_CN_LST_NM);
	}

	public void setMut04oCnSfx(String mut04oCnSfx) {
		writeString(Pos.MUT04O_CN_SFX, mut04oCnSfx, Len.MUT04O_CN_SFX);
	}

	/**Original name: MUT04O-CN-SFX<br>*/
	public String getMut04oCnSfx() {
		return readString(Pos.MUT04O_CN_SFX, Len.MUT04O_CN_SFX);
	}

	public void setMut04oCnPfx(String mut04oCnPfx) {
		writeString(Pos.MUT04O_CN_PFX, mut04oCnPfx, Len.MUT04O_CN_PFX);
	}

	/**Original name: MUT04O-CN-PFX<br>*/
	public String getMut04oCnPfx() {
		return readString(Pos.MUT04O_CN_PFX, Len.MUT04O_CN_PFX);
	}

	public void setMut04oCiClientId(String mut04oCiClientId) {
		writeString(Pos.MUT04O_CI_CLIENT_ID, mut04oCiClientId, Len.MUT04O_CI_CLIENT_ID);
	}

	/**Original name: MUT04O-CI-CLIENT-ID<br>*/
	public String getMut04oCiClientId() {
		return readString(Pos.MUT04O_CI_CLIENT_ID, Len.MUT04O_CI_CLIENT_ID);
	}

	public void setMut04oCiIdvNmInd(char mut04oCiIdvNmInd) {
		writeChar(Pos.MUT04O_CI_IDV_NM_IND, mut04oCiIdvNmInd);
	}

	/**Original name: MUT04O-CI-IDV-NM-IND<br>*/
	public char getMut04oCiIdvNmInd() {
		return readChar(Pos.MUT04O_CI_IDV_NM_IND);
	}

	public void setMut04oCiDsyNm(String mut04oCiDsyNm) {
		writeString(Pos.MUT04O_CI_DSY_NM, mut04oCiDsyNm, Len.MUT04O_CI_DSY_NM);
	}

	/**Original name: MUT04O-CI-DSY-NM<br>*/
	public String getMut04oCiDsyNm() {
		return readString(Pos.MUT04O_CI_DSY_NM, Len.MUT04O_CI_DSY_NM);
	}

	public void setMut04oCiSrNm(String mut04oCiSrNm) {
		writeString(Pos.MUT04O_CI_SR_NM, mut04oCiSrNm, Len.MUT04O_CI_SR_NM);
	}

	/**Original name: MUT04O-CI-SR-NM<br>*/
	public String getMut04oCiSrNm() {
		return readString(Pos.MUT04O_CI_SR_NM, Len.MUT04O_CI_SR_NM);
	}

	public void setMut04oCiFstNm(String mut04oCiFstNm) {
		writeString(Pos.MUT04O_CI_FST_NM, mut04oCiFstNm, Len.MUT04O_CI_FST_NM);
	}

	/**Original name: MUT04O-CI-FST-NM<br>*/
	public String getMut04oCiFstNm() {
		return readString(Pos.MUT04O_CI_FST_NM, Len.MUT04O_CI_FST_NM);
	}

	public void setMut04oCiMdlNm(String mut04oCiMdlNm) {
		writeString(Pos.MUT04O_CI_MDL_NM, mut04oCiMdlNm, Len.MUT04O_CI_MDL_NM);
	}

	/**Original name: MUT04O-CI-MDL-NM<br>*/
	public String getMut04oCiMdlNm() {
		return readString(Pos.MUT04O_CI_MDL_NM, Len.MUT04O_CI_MDL_NM);
	}

	public void setMut04oCiLstNm(String mut04oCiLstNm) {
		writeString(Pos.MUT04O_CI_LST_NM, mut04oCiLstNm, Len.MUT04O_CI_LST_NM);
	}

	/**Original name: MUT04O-CI-LST-NM<br>*/
	public String getMut04oCiLstNm() {
		return readString(Pos.MUT04O_CI_LST_NM, Len.MUT04O_CI_LST_NM);
	}

	public void setMut04oCiSfx(String mut04oCiSfx) {
		writeString(Pos.MUT04O_CI_SFX, mut04oCiSfx, Len.MUT04O_CI_SFX);
	}

	/**Original name: MUT04O-CI-SFX<br>*/
	public String getMut04oCiSfx() {
		return readString(Pos.MUT04O_CI_SFX, Len.MUT04O_CI_SFX);
	}

	public void setMut04oCiPfx(String mut04oCiPfx) {
		writeString(Pos.MUT04O_CI_PFX, mut04oCiPfx, Len.MUT04O_CI_PFX);
	}

	/**Original name: MUT04O-CI-PFX<br>*/
	public String getMut04oCiPfx() {
		return readString(Pos.MUT04O_CI_PFX, Len.MUT04O_CI_PFX);
	}

	public void setMut04oBsmAdrSeqNbr(int mut04oBsmAdrSeqNbr) {
		writeInt(Pos.MUT04O_BSM_ADR_SEQ_NBR, mut04oBsmAdrSeqNbr, Len.Int.MUT04O_BSM_ADR_SEQ_NBR);
	}

	public void setMut04oBsmAdrSeqNbrFormatted(String mut04oBsmAdrSeqNbr) {
		writeString(Pos.MUT04O_BSM_ADR_SEQ_NBR, Trunc.toUnsignedNumeric(mut04oBsmAdrSeqNbr, Len.MUT04O_BSM_ADR_SEQ_NBR), Len.MUT04O_BSM_ADR_SEQ_NBR);
	}

	/**Original name: MUT04O-BSM-ADR-SEQ-NBR<br>*/
	public int getMut04oBsmAdrSeqNbr() {
		return readNumDispInt(Pos.MUT04O_BSM_ADR_SEQ_NBR, Len.MUT04O_BSM_ADR_SEQ_NBR);
	}

	public void setMut04oBsmAdrId(String mut04oBsmAdrId) {
		writeString(Pos.MUT04O_BSM_ADR_ID, mut04oBsmAdrId, Len.MUT04O_BSM_ADR_ID);
	}

	/**Original name: MUT04O-BSM-ADR-ID<br>*/
	public String getMut04oBsmAdrId() {
		return readString(Pos.MUT04O_BSM_ADR_ID, Len.MUT04O_BSM_ADR_ID);
	}

	public void setMut04oBsmLin1Adr(String mut04oBsmLin1Adr) {
		writeString(Pos.MUT04O_BSM_LIN1_ADR, mut04oBsmLin1Adr, Len.MUT04O_BSM_LIN1_ADR);
	}

	/**Original name: MUT04O-BSM-LIN-1-ADR<br>*/
	public String getMut04oBsmLin1Adr() {
		return readString(Pos.MUT04O_BSM_LIN1_ADR, Len.MUT04O_BSM_LIN1_ADR);
	}

	public void setMut04oBsmLin2Adr(String mut04oBsmLin2Adr) {
		writeString(Pos.MUT04O_BSM_LIN2_ADR, mut04oBsmLin2Adr, Len.MUT04O_BSM_LIN2_ADR);
	}

	/**Original name: MUT04O-BSM-LIN-2-ADR<br>*/
	public String getMut04oBsmLin2Adr() {
		return readString(Pos.MUT04O_BSM_LIN2_ADR, Len.MUT04O_BSM_LIN2_ADR);
	}

	public void setMut04oBsmCit(String mut04oBsmCit) {
		writeString(Pos.MUT04O_BSM_CIT, mut04oBsmCit, Len.MUT04O_BSM_CIT);
	}

	/**Original name: MUT04O-BSM-CIT<br>*/
	public String getMut04oBsmCit() {
		return readString(Pos.MUT04O_BSM_CIT, Len.MUT04O_BSM_CIT);
	}

	public void setMut04oBsmStAbb(String mut04oBsmStAbb) {
		writeString(Pos.MUT04O_BSM_ST_ABB, mut04oBsmStAbb, Len.MUT04O_BSM_ST_ABB);
	}

	/**Original name: MUT04O-BSM-ST-ABB<br>*/
	public String getMut04oBsmStAbb() {
		return readString(Pos.MUT04O_BSM_ST_ABB, Len.MUT04O_BSM_ST_ABB);
	}

	public void setMut04oBsmPstCd(String mut04oBsmPstCd) {
		writeString(Pos.MUT04O_BSM_PST_CD, mut04oBsmPstCd, Len.MUT04O_BSM_PST_CD);
	}

	/**Original name: MUT04O-BSM-PST-CD<br>*/
	public String getMut04oBsmPstCd() {
		return readString(Pos.MUT04O_BSM_PST_CD, Len.MUT04O_BSM_PST_CD);
	}

	public void setMut04oBsmCty(String mut04oBsmCty) {
		writeString(Pos.MUT04O_BSM_CTY, mut04oBsmCty, Len.MUT04O_BSM_CTY);
	}

	/**Original name: MUT04O-BSM-CTY<br>*/
	public String getMut04oBsmCty() {
		return readString(Pos.MUT04O_BSM_CTY, Len.MUT04O_BSM_CTY);
	}

	public void setMut04oBsmCtrCd(String mut04oBsmCtrCd) {
		writeString(Pos.MUT04O_BSM_CTR_CD, mut04oBsmCtrCd, Len.MUT04O_BSM_CTR_CD);
	}

	/**Original name: MUT04O-BSM-CTR-CD<br>*/
	public String getMut04oBsmCtrCd() {
		return readString(Pos.MUT04O_BSM_CTR_CD, Len.MUT04O_BSM_CTR_CD);
	}

	public void setMut04oBslAdrSeqNbr(int mut04oBslAdrSeqNbr) {
		writeInt(Pos.MUT04O_BSL_ADR_SEQ_NBR, mut04oBslAdrSeqNbr, Len.Int.MUT04O_BSL_ADR_SEQ_NBR);
	}

	public void setMut04oBslAdrSeqNbrFormatted(String mut04oBslAdrSeqNbr) {
		writeString(Pos.MUT04O_BSL_ADR_SEQ_NBR, Trunc.toUnsignedNumeric(mut04oBslAdrSeqNbr, Len.MUT04O_BSL_ADR_SEQ_NBR), Len.MUT04O_BSL_ADR_SEQ_NBR);
	}

	/**Original name: MUT04O-BSL-ADR-SEQ-NBR<br>*/
	public int getMut04oBslAdrSeqNbr() {
		return readNumDispInt(Pos.MUT04O_BSL_ADR_SEQ_NBR, Len.MUT04O_BSL_ADR_SEQ_NBR);
	}

	public void setMut04oBslAdrId(String mut04oBslAdrId) {
		writeString(Pos.MUT04O_BSL_ADR_ID, mut04oBslAdrId, Len.MUT04O_BSL_ADR_ID);
	}

	/**Original name: MUT04O-BSL-ADR-ID<br>*/
	public String getMut04oBslAdrId() {
		return readString(Pos.MUT04O_BSL_ADR_ID, Len.MUT04O_BSL_ADR_ID);
	}

	public void setMut04oBslLin1Adr(String mut04oBslLin1Adr) {
		writeString(Pos.MUT04O_BSL_LIN1_ADR, mut04oBslLin1Adr, Len.MUT04O_BSL_LIN1_ADR);
	}

	/**Original name: MUT04O-BSL-LIN-1-ADR<br>*/
	public String getMut04oBslLin1Adr() {
		return readString(Pos.MUT04O_BSL_LIN1_ADR, Len.MUT04O_BSL_LIN1_ADR);
	}

	public void setMut04oBslLin2Adr(String mut04oBslLin2Adr) {
		writeString(Pos.MUT04O_BSL_LIN2_ADR, mut04oBslLin2Adr, Len.MUT04O_BSL_LIN2_ADR);
	}

	/**Original name: MUT04O-BSL-LIN-2-ADR<br>*/
	public String getMut04oBslLin2Adr() {
		return readString(Pos.MUT04O_BSL_LIN2_ADR, Len.MUT04O_BSL_LIN2_ADR);
	}

	public void setMut04oBslCit(String mut04oBslCit) {
		writeString(Pos.MUT04O_BSL_CIT, mut04oBslCit, Len.MUT04O_BSL_CIT);
	}

	/**Original name: MUT04O-BSL-CIT<br>*/
	public String getMut04oBslCit() {
		return readString(Pos.MUT04O_BSL_CIT, Len.MUT04O_BSL_CIT);
	}

	public void setMut04oBslStAbb(String mut04oBslStAbb) {
		writeString(Pos.MUT04O_BSL_ST_ABB, mut04oBslStAbb, Len.MUT04O_BSL_ST_ABB);
	}

	/**Original name: MUT04O-BSL-ST-ABB<br>*/
	public String getMut04oBslStAbb() {
		return readString(Pos.MUT04O_BSL_ST_ABB, Len.MUT04O_BSL_ST_ABB);
	}

	public void setMut04oBslPstCd(String mut04oBslPstCd) {
		writeString(Pos.MUT04O_BSL_PST_CD, mut04oBslPstCd, Len.MUT04O_BSL_PST_CD);
	}

	/**Original name: MUT04O-BSL-PST-CD<br>*/
	public String getMut04oBslPstCd() {
		return readString(Pos.MUT04O_BSL_PST_CD, Len.MUT04O_BSL_PST_CD);
	}

	public void setMut04oBslCty(String mut04oBslCty) {
		writeString(Pos.MUT04O_BSL_CTY, mut04oBslCty, Len.MUT04O_BSL_CTY);
	}

	/**Original name: MUT04O-BSL-CTY<br>*/
	public String getMut04oBslCty() {
		return readString(Pos.MUT04O_BSL_CTY, Len.MUT04O_BSL_CTY);
	}

	public void setMut04oBslCtrCd(String mut04oBslCtrCd) {
		writeString(Pos.MUT04O_BSL_CTR_CD, mut04oBslCtrCd, Len.MUT04O_BSL_CTR_CD);
	}

	/**Original name: MUT04O-BSL-CTR-CD<br>*/
	public String getMut04oBslCtrCd() {
		return readString(Pos.MUT04O_BSL_CTR_CD, Len.MUT04O_BSL_CTR_CD);
	}

	public void setMut04oBusPhnAcd(String mut04oBusPhnAcd) {
		writeString(Pos.MUT04O_BUS_PHN_ACD, mut04oBusPhnAcd, Len.MUT04O_BUS_PHN_ACD);
	}

	/**Original name: MUT04O-BUS-PHN-ACD<br>*/
	public String getMut04oBusPhnAcd() {
		return readString(Pos.MUT04O_BUS_PHN_ACD, Len.MUT04O_BUS_PHN_ACD);
	}

	public void setMut04oBusPhnPfxNbr(String mut04oBusPhnPfxNbr) {
		writeString(Pos.MUT04O_BUS_PHN_PFX_NBR, mut04oBusPhnPfxNbr, Len.MUT04O_BUS_PHN_PFX_NBR);
	}

	/**Original name: MUT04O-BUS-PHN-PFX-NBR<br>*/
	public String getMut04oBusPhnPfxNbr() {
		return readString(Pos.MUT04O_BUS_PHN_PFX_NBR, Len.MUT04O_BUS_PHN_PFX_NBR);
	}

	public void setMut04oBusPhnLinNbr(String mut04oBusPhnLinNbr) {
		writeString(Pos.MUT04O_BUS_PHN_LIN_NBR, mut04oBusPhnLinNbr, Len.MUT04O_BUS_PHN_LIN_NBR);
	}

	/**Original name: MUT04O-BUS-PHN-LIN-NBR<br>*/
	public String getMut04oBusPhnLinNbr() {
		return readString(Pos.MUT04O_BUS_PHN_LIN_NBR, Len.MUT04O_BUS_PHN_LIN_NBR);
	}

	public void setMut04oBusPhnExtNbr(String mut04oBusPhnExtNbr) {
		writeString(Pos.MUT04O_BUS_PHN_EXT_NBR, mut04oBusPhnExtNbr, Len.MUT04O_BUS_PHN_EXT_NBR);
	}

	/**Original name: MUT04O-BUS-PHN-EXT-NBR<br>*/
	public String getMut04oBusPhnExtNbr() {
		return readString(Pos.MUT04O_BUS_PHN_EXT_NBR, Len.MUT04O_BUS_PHN_EXT_NBR);
	}

	public void setMut04oBusPhnNbrFmt(String mut04oBusPhnNbrFmt) {
		writeString(Pos.MUT04O_BUS_PHN_NBR_FMT, mut04oBusPhnNbrFmt, Len.MUT04O_BUS_PHN_NBR_FMT);
	}

	/**Original name: MUT04O-BUS-PHN-NBR-FMT<br>*/
	public String getMut04oBusPhnNbrFmt() {
		return readString(Pos.MUT04O_BUS_PHN_NBR_FMT, Len.MUT04O_BUS_PHN_NBR_FMT);
	}

	public String getMut04oBusPhnNbrFmtFormatted() {
		return Functions.padBlanks(getMut04oBusPhnNbrFmt(), Len.MUT04O_BUS_PHN_NBR_FMT);
	}

	public void setMut04oFaxPhnAcd(String mut04oFaxPhnAcd) {
		writeString(Pos.MUT04O_FAX_PHN_ACD, mut04oFaxPhnAcd, Len.MUT04O_FAX_PHN_ACD);
	}

	/**Original name: MUT04O-FAX-PHN-ACD<br>*/
	public String getMut04oFaxPhnAcd() {
		return readString(Pos.MUT04O_FAX_PHN_ACD, Len.MUT04O_FAX_PHN_ACD);
	}

	public void setMut04oFaxPhnPfxNbr(String mut04oFaxPhnPfxNbr) {
		writeString(Pos.MUT04O_FAX_PHN_PFX_NBR, mut04oFaxPhnPfxNbr, Len.MUT04O_FAX_PHN_PFX_NBR);
	}

	/**Original name: MUT04O-FAX-PHN-PFX-NBR<br>*/
	public String getMut04oFaxPhnPfxNbr() {
		return readString(Pos.MUT04O_FAX_PHN_PFX_NBR, Len.MUT04O_FAX_PHN_PFX_NBR);
	}

	public void setMut04oFaxPhnLinNbr(String mut04oFaxPhnLinNbr) {
		writeString(Pos.MUT04O_FAX_PHN_LIN_NBR, mut04oFaxPhnLinNbr, Len.MUT04O_FAX_PHN_LIN_NBR);
	}

	/**Original name: MUT04O-FAX-PHN-LIN-NBR<br>*/
	public String getMut04oFaxPhnLinNbr() {
		return readString(Pos.MUT04O_FAX_PHN_LIN_NBR, Len.MUT04O_FAX_PHN_LIN_NBR);
	}

	public void setMut04oFaxPhnExtNbr(String mut04oFaxPhnExtNbr) {
		writeString(Pos.MUT04O_FAX_PHN_EXT_NBR, mut04oFaxPhnExtNbr, Len.MUT04O_FAX_PHN_EXT_NBR);
	}

	/**Original name: MUT04O-FAX-PHN-EXT-NBR<br>*/
	public String getMut04oFaxPhnExtNbr() {
		return readString(Pos.MUT04O_FAX_PHN_EXT_NBR, Len.MUT04O_FAX_PHN_EXT_NBR);
	}

	public void setMut04oFaxPhnNbrFmt(String mut04oFaxPhnNbrFmt) {
		writeString(Pos.MUT04O_FAX_PHN_NBR_FMT, mut04oFaxPhnNbrFmt, Len.MUT04O_FAX_PHN_NBR_FMT);
	}

	/**Original name: MUT04O-FAX-PHN-NBR-FMT<br>*/
	public String getMut04oFaxPhnNbrFmt() {
		return readString(Pos.MUT04O_FAX_PHN_NBR_FMT, Len.MUT04O_FAX_PHN_NBR_FMT);
	}

	public String getMut04oFaxPhnNbrFmtFormatted() {
		return Functions.padBlanks(getMut04oFaxPhnNbrFmt(), Len.MUT04O_FAX_PHN_NBR_FMT);
	}

	public void setMut04oCtcPhnAcd(String mut04oCtcPhnAcd) {
		writeString(Pos.MUT04O_CTC_PHN_ACD, mut04oCtcPhnAcd, Len.MUT04O_CTC_PHN_ACD);
	}

	/**Original name: MUT04O-CTC-PHN-ACD<br>*/
	public String getMut04oCtcPhnAcd() {
		return readString(Pos.MUT04O_CTC_PHN_ACD, Len.MUT04O_CTC_PHN_ACD);
	}

	public void setMut04oCtcPhnPfxNbr(String mut04oCtcPhnPfxNbr) {
		writeString(Pos.MUT04O_CTC_PHN_PFX_NBR, mut04oCtcPhnPfxNbr, Len.MUT04O_CTC_PHN_PFX_NBR);
	}

	/**Original name: MUT04O-CTC-PHN-PFX-NBR<br>*/
	public String getMut04oCtcPhnPfxNbr() {
		return readString(Pos.MUT04O_CTC_PHN_PFX_NBR, Len.MUT04O_CTC_PHN_PFX_NBR);
	}

	public void setMut04oCtcPhnLinNbr(String mut04oCtcPhnLinNbr) {
		writeString(Pos.MUT04O_CTC_PHN_LIN_NBR, mut04oCtcPhnLinNbr, Len.MUT04O_CTC_PHN_LIN_NBR);
	}

	/**Original name: MUT04O-CTC-PHN-LIN-NBR<br>*/
	public String getMut04oCtcPhnLinNbr() {
		return readString(Pos.MUT04O_CTC_PHN_LIN_NBR, Len.MUT04O_CTC_PHN_LIN_NBR);
	}

	public void setMut04oCtcPhnExtNbr(String mut04oCtcPhnExtNbr) {
		writeString(Pos.MUT04O_CTC_PHN_EXT_NBR, mut04oCtcPhnExtNbr, Len.MUT04O_CTC_PHN_EXT_NBR);
	}

	/**Original name: MUT04O-CTC-PHN-EXT-NBR<br>*/
	public String getMut04oCtcPhnExtNbr() {
		return readString(Pos.MUT04O_CTC_PHN_EXT_NBR, Len.MUT04O_CTC_PHN_EXT_NBR);
	}

	public void setMut04oCtcPhnNbrFmt(String mut04oCtcPhnNbrFmt) {
		writeString(Pos.MUT04O_CTC_PHN_NBR_FMT, mut04oCtcPhnNbrFmt, Len.MUT04O_CTC_PHN_NBR_FMT);
	}

	/**Original name: MUT04O-CTC-PHN-NBR-FMT<br>*/
	public String getMut04oCtcPhnNbrFmt() {
		return readString(Pos.MUT04O_CTC_PHN_NBR_FMT, Len.MUT04O_CTC_PHN_NBR_FMT);
	}

	public String getMut04oCtcPhnNbrFmtFormatted() {
		return Functions.padBlanks(getMut04oCtcPhnNbrFmt(), Len.MUT04O_CTC_PHN_NBR_FMT);
	}

	public void setMut04oFein(String mut04oFein) {
		writeString(Pos.MUT04O_FEIN, mut04oFein, Len.MUT04O_FEIN);
	}

	/**Original name: MUT04O-FEIN<br>*/
	public String getMut04oFein() {
		return readString(Pos.MUT04O_FEIN, Len.MUT04O_FEIN);
	}

	public void setMut04oSsn(String mut04oSsn) {
		writeString(Pos.MUT04O_SSN, mut04oSsn, Len.MUT04O_SSN);
	}

	/**Original name: MUT04O-SSN<br>*/
	public String getMut04oSsn() {
		return readString(Pos.MUT04O_SSN, Len.MUT04O_SSN);
	}

	public void setMut04oFeinFmt(String mut04oFeinFmt) {
		writeString(Pos.MUT04O_FEIN_FMT, mut04oFeinFmt, Len.MUT04O_FEIN_FMT);
	}

	/**Original name: MUT04O-FEIN-FMT<br>*/
	public String getMut04oFeinFmt() {
		return readString(Pos.MUT04O_FEIN_FMT, Len.MUT04O_FEIN_FMT);
	}

	public String getMut04oFeinFmtFormatted() {
		return Functions.padBlanks(getMut04oFeinFmt(), Len.MUT04O_FEIN_FMT);
	}

	public void setMut04oSsnFmt(String mut04oSsnFmt) {
		writeString(Pos.MUT04O_SSN_FMT, mut04oSsnFmt, Len.MUT04O_SSN_FMT);
	}

	/**Original name: MUT04O-SSN-FMT<br>*/
	public String getMut04oSsnFmt() {
		return readString(Pos.MUT04O_SSN_FMT, Len.MUT04O_SSN_FMT);
	}

	public String getMut04oSsnFmtFormatted() {
		return Functions.padBlanks(getMut04oSsnFmt(), Len.MUT04O_SSN_FMT);
	}

	public void setMut04oLegEtyCd(String mut04oLegEtyCd) {
		writeString(Pos.MUT04O_LEG_ETY_CD, mut04oLegEtyCd, Len.MUT04O_LEG_ETY_CD);
	}

	/**Original name: MUT04O-LEG-ETY-CD<br>*/
	public String getMut04oLegEtyCd() {
		return readString(Pos.MUT04O_LEG_ETY_CD, Len.MUT04O_LEG_ETY_CD);
	}

	public void setMut04oLegEtyDes(String mut04oLegEtyDes) {
		writeString(Pos.MUT04O_LEG_ETY_DES, mut04oLegEtyDes, Len.MUT04O_LEG_ETY_DES);
	}

	/**Original name: MUT04O-LEG-ETY-DES<br>*/
	public String getMut04oLegEtyDes() {
		return readString(Pos.MUT04O_LEG_ETY_DES, Len.MUT04O_LEG_ETY_DES);
	}

	public void setMut04oTobCd(String mut04oTobCd) {
		writeString(Pos.MUT04O_TOB_CD, mut04oTobCd, Len.MUT04O_TOB_CD);
	}

	/**Original name: MUT04O-TOB-CD<br>*/
	public String getMut04oTobCd() {
		return readString(Pos.MUT04O_TOB_CD, Len.MUT04O_TOB_CD);
	}

	public void setMut04oTobDes(String mut04oTobDes) {
		writeString(Pos.MUT04O_TOB_DES, mut04oTobDes, Len.MUT04O_TOB_DES);
	}

	/**Original name: MUT04O-TOB-DES<br>*/
	public String getMut04oTobDes() {
		return readString(Pos.MUT04O_TOB_DES, Len.MUT04O_TOB_DES);
	}

	public void setMut04oSicCd(String mut04oSicCd) {
		writeString(Pos.MUT04O_SIC_CD, mut04oSicCd, Len.MUT04O_SIC_CD);
	}

	/**Original name: MUT04O-SIC-CD<br>*/
	public String getMut04oSicCd() {
		return readString(Pos.MUT04O_SIC_CD, Len.MUT04O_SIC_CD);
	}

	public void setMut04oSicDes(String mut04oSicDes) {
		writeString(Pos.MUT04O_SIC_DES, mut04oSicDes, Len.MUT04O_SIC_DES);
	}

	/**Original name: MUT04O-SIC-DES<br>*/
	public String getMut04oSicDes() {
		return readString(Pos.MUT04O_SIC_DES, Len.MUT04O_SIC_DES);
	}

	public void setMut04oSegCd(String mut04oSegCd) {
		writeString(Pos.MUT04O_SEG_CD, mut04oSegCd, Len.MUT04O_SEG_CD);
	}

	/**Original name: MUT04O-SEG-CD<br>*/
	public String getMut04oSegCd() {
		return readString(Pos.MUT04O_SEG_CD, Len.MUT04O_SEG_CD);
	}

	public void setMut04oSegDes(String mut04oSegDes) {
		writeString(Pos.MUT04O_SEG_DES, mut04oSegDes, Len.MUT04O_SEG_DES);
	}

	/**Original name: MUT04O-SEG-DES<br>*/
	public String getMut04oSegDes() {
		return readString(Pos.MUT04O_SEG_DES, Len.MUT04O_SEG_DES);
	}

	public void setMut04oPcPsptActInd(char mut04oPcPsptActInd) {
		writeChar(Pos.MUT04O_PC_PSPT_ACT_IND, mut04oPcPsptActInd);
	}

	/**Original name: MUT04O-PC-PSPT-ACT-IND<br>
	 * <pre>                    IS THIS A PROSPECT ACCOUNT?</pre>*/
	public char getMut04oPcPsptActInd() {
		return readChar(Pos.MUT04O_PC_PSPT_ACT_IND);
	}

	public void setMut04oPcActNbr(String mut04oPcActNbr) {
		writeString(Pos.MUT04O_PC_ACT_NBR, mut04oPcActNbr, Len.MUT04O_PC_ACT_NBR);
	}

	/**Original name: MUT04O-PC-ACT-NBR<br>*/
	public String getMut04oPcActNbr() {
		return readString(Pos.MUT04O_PC_ACT_NBR, Len.MUT04O_PC_ACT_NBR);
	}

	public void setMut04oPcActNbr2(String mut04oPcActNbr2) {
		writeString(Pos.MUT04O_PC_ACT_NBR2, mut04oPcActNbr2, Len.MUT04O_PC_ACT_NBR2);
	}

	/**Original name: MUT04O-PC-ACT-NBR-2<br>*/
	public String getMut04oPcActNbr2() {
		return readString(Pos.MUT04O_PC_ACT_NBR2, Len.MUT04O_PC_ACT_NBR2);
	}

	public void setMut04oPcActNbrFmt(String mut04oPcActNbrFmt) {
		writeString(Pos.MUT04O_PC_ACT_NBR_FMT, mut04oPcActNbrFmt, Len.MUT04O_PC_ACT_NBR_FMT);
	}

	/**Original name: MUT04O-PC-ACT-NBR-FMT<br>*/
	public String getMut04oPcActNbrFmt() {
		return readString(Pos.MUT04O_PC_ACT_NBR_FMT, Len.MUT04O_PC_ACT_NBR_FMT);
	}

	public void setMut04oPcActNbr2Fmt(String mut04oPcActNbr2Fmt) {
		writeString(Pos.MUT04O_PC_ACT_NBR2_FMT, mut04oPcActNbr2Fmt, Len.MUT04O_PC_ACT_NBR2_FMT);
	}

	/**Original name: MUT04O-PC-ACT-NBR-2-FMT<br>*/
	public String getMut04oPcActNbr2Fmt() {
		return readString(Pos.MUT04O_PC_ACT_NBR2_FMT, Len.MUT04O_PC_ACT_NBR2_FMT);
	}

	public void setMut04oPsptActNbr(String mut04oPsptActNbr) {
		writeString(Pos.MUT04O_PSPT_ACT_NBR, mut04oPsptActNbr, Len.MUT04O_PSPT_ACT_NBR);
	}

	/**Original name: MUT04O-PSPT-ACT-NBR<br>
	 * <pre>                    FILLED IN IF DIFFERENT FROM P&C ACCT</pre>*/
	public String getMut04oPsptActNbr() {
		return readString(Pos.MUT04O_PSPT_ACT_NBR, Len.MUT04O_PSPT_ACT_NBR);
	}

	public void setMut04oPsptActNbrFmt(String mut04oPsptActNbrFmt) {
		writeString(Pos.MUT04O_PSPT_ACT_NBR_FMT, mut04oPsptActNbrFmt, Len.MUT04O_PSPT_ACT_NBR_FMT);
	}

	/**Original name: MUT04O-PSPT-ACT-NBR-FMT<br>*/
	public String getMut04oPsptActNbrFmt() {
		return readString(Pos.MUT04O_PSPT_ACT_NBR_FMT, Len.MUT04O_PSPT_ACT_NBR_FMT);
	}

	public void setMut04oActNmId(int mut04oActNmId) {
		writeInt(Pos.MUT04O_ACT_NM_ID, mut04oActNmId, Len.Int.MUT04O_ACT_NM_ID);
	}

	public void setMut04oActNmIdFormatted(String mut04oActNmId) {
		writeString(Pos.MUT04O_ACT_NM_ID, Trunc.toUnsignedNumeric(mut04oActNmId, Len.MUT04O_ACT_NM_ID), Len.MUT04O_ACT_NM_ID);
	}

	/**Original name: MUT04O-ACT-NM-ID<br>*/
	public int getMut04oActNmId() {
		return readNumDispInt(Pos.MUT04O_ACT_NM_ID, Len.MUT04O_ACT_NM_ID);
	}

	public void setMut04oAct2NmId(int mut04oAct2NmId) {
		writeInt(Pos.MUT04O_ACT2_NM_ID, mut04oAct2NmId, Len.Int.MUT04O_ACT2_NM_ID);
	}

	public void setMut04oAct2NmIdFormatted(String mut04oAct2NmId) {
		writeString(Pos.MUT04O_ACT2_NM_ID, Trunc.toUnsignedNumeric(mut04oAct2NmId, Len.MUT04O_ACT2_NM_ID), Len.MUT04O_ACT2_NM_ID);
	}

	/**Original name: MUT04O-ACT-2-NM-ID<br>*/
	public int getMut04oAct2NmId() {
		return readNumDispInt(Pos.MUT04O_ACT2_NM_ID, Len.MUT04O_ACT2_NM_ID);
	}

	public void setMut04oObjCd(String mut04oObjCd) {
		writeString(Pos.MUT04O_OBJ_CD, mut04oObjCd, Len.MUT04O_OBJ_CD);
	}

	/**Original name: MUT04O-OBJ-CD<br>*/
	public String getMut04oObjCd() {
		return readString(Pos.MUT04O_OBJ_CD, Len.MUT04O_OBJ_CD);
	}

	public void setMut04oObjCd2(String mut04oObjCd2) {
		writeString(Pos.MUT04O_OBJ_CD2, mut04oObjCd2, Len.MUT04O_OBJ_CD2);
	}

	/**Original name: MUT04O-OBJ-CD-2<br>*/
	public String getMut04oObjCd2() {
		return readString(Pos.MUT04O_OBJ_CD2, Len.MUT04O_OBJ_CD2);
	}

	public void setMut04oObjDes(String mut04oObjDes) {
		writeString(Pos.MUT04O_OBJ_DES, mut04oObjDes, Len.MUT04O_OBJ_DES);
	}

	/**Original name: MUT04O-OBJ-DES<br>*/
	public String getMut04oObjDes() {
		return readString(Pos.MUT04O_OBJ_DES, Len.MUT04O_OBJ_DES);
	}

	public void setMut04oObjDes2(String mut04oObjDes2) {
		writeString(Pos.MUT04O_OBJ_DES2, mut04oObjDes2, Len.MUT04O_OBJ_DES2);
	}

	/**Original name: MUT04O-OBJ-DES-2<br>*/
	public String getMut04oObjDes2() {
		return readString(Pos.MUT04O_OBJ_DES2, Len.MUT04O_OBJ_DES2);
	}

	public void setMut04oSinWcActNbr(String mut04oSinWcActNbr) {
		writeString(Pos.MUT04O_SIN_WC_ACT_NBR, mut04oSinWcActNbr, Len.MUT04O_SIN_WC_ACT_NBR);
	}

	/**Original name: MUT04O-SIN-WC-ACT-NBR<br>
	 * <pre>                SELF-INSURED WC ACCOUNT</pre>*/
	public String getMut04oSinWcActNbr() {
		return readString(Pos.MUT04O_SIN_WC_ACT_NBR, Len.MUT04O_SIN_WC_ACT_NBR);
	}

	public void setMut04oSinWcActNbrFmt(String mut04oSinWcActNbrFmt) {
		writeString(Pos.MUT04O_SIN_WC_ACT_NBR_FMT, mut04oSinWcActNbrFmt, Len.MUT04O_SIN_WC_ACT_NBR_FMT);
	}

	/**Original name: MUT04O-SIN-WC-ACT-NBR-FMT<br>*/
	public String getMut04oSinWcActNbrFmt() {
		return readString(Pos.MUT04O_SIN_WC_ACT_NBR_FMT, Len.MUT04O_SIN_WC_ACT_NBR_FMT);
	}

	public void setMut04oSinWcActNmId(int mut04oSinWcActNmId) {
		writeInt(Pos.MUT04O_SIN_WC_ACT_NM_ID, mut04oSinWcActNmId, Len.Int.MUT04O_SIN_WC_ACT_NM_ID);
	}

	public void setMut04oSinWcActNmIdFormatted(String mut04oSinWcActNmId) {
		writeString(Pos.MUT04O_SIN_WC_ACT_NM_ID, Trunc.toUnsignedNumeric(mut04oSinWcActNmId, Len.MUT04O_SIN_WC_ACT_NM_ID),
				Len.MUT04O_SIN_WC_ACT_NM_ID);
	}

	/**Original name: MUT04O-SIN-WC-ACT-NM-ID<br>*/
	public int getMut04oSinWcActNmId() {
		return readNumDispInt(Pos.MUT04O_SIN_WC_ACT_NM_ID, Len.MUT04O_SIN_WC_ACT_NM_ID);
	}

	public void setMut04oSinWcObjCd(String mut04oSinWcObjCd) {
		writeString(Pos.MUT04O_SIN_WC_OBJ_CD, mut04oSinWcObjCd, Len.MUT04O_SIN_WC_OBJ_CD);
	}

	/**Original name: MUT04O-SIN-WC-OBJ-CD<br>*/
	public String getMut04oSinWcObjCd() {
		return readString(Pos.MUT04O_SIN_WC_OBJ_CD, Len.MUT04O_SIN_WC_OBJ_CD);
	}

	public void setMut04oSinWcObjDes(String mut04oSinWcObjDes) {
		writeString(Pos.MUT04O_SIN_WC_OBJ_DES, mut04oSinWcObjDes, Len.MUT04O_SIN_WC_OBJ_DES);
	}

	/**Original name: MUT04O-SIN-WC-OBJ-DES<br>*/
	public String getMut04oSinWcObjDes() {
		return readString(Pos.MUT04O_SIN_WC_OBJ_DES, Len.MUT04O_SIN_WC_OBJ_DES);
	}

	public void setMut04oSplBillActNbr(String mut04oSplBillActNbr) {
		writeString(Pos.MUT04O_SPL_BILL_ACT_NBR, mut04oSplBillActNbr, Len.MUT04O_SPL_BILL_ACT_NBR);
	}

	/**Original name: MUT04O-SPL-BILL-ACT-NBR<br>
	 * <pre>                SPLIT-BILLED ACCOUNT</pre>*/
	public String getMut04oSplBillActNbr() {
		return readString(Pos.MUT04O_SPL_BILL_ACT_NBR, Len.MUT04O_SPL_BILL_ACT_NBR);
	}

	public void setMut04oSplBillActNbrFmt(String mut04oSplBillActNbrFmt) {
		writeString(Pos.MUT04O_SPL_BILL_ACT_NBR_FMT, mut04oSplBillActNbrFmt, Len.MUT04O_SPL_BILL_ACT_NBR_FMT);
	}

	/**Original name: MUT04O-SPL-BILL-ACT-NBR-FMT<br>*/
	public String getMut04oSplBillActNbrFmt() {
		return readString(Pos.MUT04O_SPL_BILL_ACT_NBR_FMT, Len.MUT04O_SPL_BILL_ACT_NBR_FMT);
	}

	public void setMut04oSplBillActNmId(int mut04oSplBillActNmId) {
		writeInt(Pos.MUT04O_SPL_BILL_ACT_NM_ID, mut04oSplBillActNmId, Len.Int.MUT04O_SPL_BILL_ACT_NM_ID);
	}

	public void setMut04oSplBillActNmIdFormatted(String mut04oSplBillActNmId) {
		writeString(Pos.MUT04O_SPL_BILL_ACT_NM_ID, Trunc.toUnsignedNumeric(mut04oSplBillActNmId, Len.MUT04O_SPL_BILL_ACT_NM_ID),
				Len.MUT04O_SPL_BILL_ACT_NM_ID);
	}

	/**Original name: MUT04O-SPL-BILL-ACT-NM-ID<br>*/
	public int getMut04oSplBillActNmId() {
		return readNumDispInt(Pos.MUT04O_SPL_BILL_ACT_NM_ID, Len.MUT04O_SPL_BILL_ACT_NM_ID);
	}

	public void setMut04oSplBillObjCd(String mut04oSplBillObjCd) {
		writeString(Pos.MUT04O_SPL_BILL_OBJ_CD, mut04oSplBillObjCd, Len.MUT04O_SPL_BILL_OBJ_CD);
	}

	/**Original name: MUT04O-SPL-BILL-OBJ-CD<br>*/
	public String getMut04oSplBillObjCd() {
		return readString(Pos.MUT04O_SPL_BILL_OBJ_CD, Len.MUT04O_SPL_BILL_OBJ_CD);
	}

	public void setMut04oSplBillObjDes(String mut04oSplBillObjDes) {
		writeString(Pos.MUT04O_SPL_BILL_OBJ_DES, mut04oSplBillObjDes, Len.MUT04O_SPL_BILL_OBJ_DES);
	}

	/**Original name: MUT04O-SPL-BILL-OBJ-DES<br>*/
	public String getMut04oSplBillObjDes() {
		return readString(Pos.MUT04O_SPL_BILL_OBJ_DES, Len.MUT04O_SPL_BILL_OBJ_DES);
	}

	public void setMut04oPrsLinActNbr(String mut04oPrsLinActNbr) {
		writeString(Pos.MUT04O_PRS_LIN_ACT_NBR, mut04oPrsLinActNbr, Len.MUT04O_PRS_LIN_ACT_NBR);
	}

	/**Original name: MUT04O-PRS-LIN-ACT-NBR<br>
	 * <pre>                PERSONAL LINES ACCOUNT</pre>*/
	public String getMut04oPrsLinActNbr() {
		return readString(Pos.MUT04O_PRS_LIN_ACT_NBR, Len.MUT04O_PRS_LIN_ACT_NBR);
	}

	public void setMut04oPrsLinActNbrFmt(String mut04oPrsLinActNbrFmt) {
		writeString(Pos.MUT04O_PRS_LIN_ACT_NBR_FMT, mut04oPrsLinActNbrFmt, Len.MUT04O_PRS_LIN_ACT_NBR_FMT);
	}

	/**Original name: MUT04O-PRS-LIN-ACT-NBR-FMT<br>*/
	public String getMut04oPrsLinActNbrFmt() {
		return readString(Pos.MUT04O_PRS_LIN_ACT_NBR_FMT, Len.MUT04O_PRS_LIN_ACT_NBR_FMT);
	}

	public void setMut04oPrsLinActNmId(int mut04oPrsLinActNmId) {
		writeInt(Pos.MUT04O_PRS_LIN_ACT_NM_ID, mut04oPrsLinActNmId, Len.Int.MUT04O_PRS_LIN_ACT_NM_ID);
	}

	public void setMut04oPrsLinActNmIdFormatted(String mut04oPrsLinActNmId) {
		writeString(Pos.MUT04O_PRS_LIN_ACT_NM_ID, Trunc.toUnsignedNumeric(mut04oPrsLinActNmId, Len.MUT04O_PRS_LIN_ACT_NM_ID),
				Len.MUT04O_PRS_LIN_ACT_NM_ID);
	}

	/**Original name: MUT04O-PRS-LIN-ACT-NM-ID<br>*/
	public int getMut04oPrsLinActNmId() {
		return readNumDispInt(Pos.MUT04O_PRS_LIN_ACT_NM_ID, Len.MUT04O_PRS_LIN_ACT_NM_ID);
	}

	public void setMut04oPrsLinObjCd(String mut04oPrsLinObjCd) {
		writeString(Pos.MUT04O_PRS_LIN_OBJ_CD, mut04oPrsLinObjCd, Len.MUT04O_PRS_LIN_OBJ_CD);
	}

	/**Original name: MUT04O-PRS-LIN-OBJ-CD<br>*/
	public String getMut04oPrsLinObjCd() {
		return readString(Pos.MUT04O_PRS_LIN_OBJ_CD, Len.MUT04O_PRS_LIN_OBJ_CD);
	}

	public void setMut04oPrsLinObjDes(String mut04oPrsLinObjDes) {
		writeString(Pos.MUT04O_PRS_LIN_OBJ_DES, mut04oPrsLinObjDes, Len.MUT04O_PRS_LIN_OBJ_DES);
	}

	/**Original name: MUT04O-PRS-LIN-OBJ-DES<br>*/
	public String getMut04oPrsLinObjDes() {
		return readString(Pos.MUT04O_PRS_LIN_OBJ_DES, Len.MUT04O_PRS_LIN_OBJ_DES);
	}

	public void setMut04oCstNbr(String mut04oCstNbr) {
		writeString(Pos.MUT04O_CST_NBR, mut04oCstNbr, Len.MUT04O_CST_NBR);
	}

	/**Original name: MUT04O-CST-NBR<br>*/
	public String getMut04oCstNbr() {
		return readString(Pos.MUT04O_CST_NBR, Len.MUT04O_CST_NBR);
	}

	public void setMut04oMktCltId(String mut04oMktCltId) {
		writeString(Pos.MUT04O_MKT_CLT_ID, mut04oMktCltId, Len.MUT04O_MKT_CLT_ID);
	}

	/**Original name: MUT04O-MKT-CLT-ID<br>*/
	public String getMut04oMktCltId() {
		return readString(Pos.MUT04O_MKT_CLT_ID, Len.MUT04O_MKT_CLT_ID);
	}

	public void setMut04oMktTerNbr(String mut04oMktTerNbr) {
		writeString(Pos.MUT04O_MKT_TER_NBR, mut04oMktTerNbr, Len.MUT04O_MKT_TER_NBR);
	}

	/**Original name: MUT04O-MKT-TER-NBR<br>*/
	public String getMut04oMktTerNbr() {
		return readString(Pos.MUT04O_MKT_TER_NBR, Len.MUT04O_MKT_TER_NBR);
	}

	public String getMut04oMktTerNbrFormatted() {
		return Functions.padBlanks(getMut04oMktTerNbr(), Len.MUT04O_MKT_TER_NBR);
	}

	public void setMut04oMktSvcDsyNm(String mut04oMktSvcDsyNm) {
		writeString(Pos.MUT04O_MKT_SVC_DSY_NM, mut04oMktSvcDsyNm, Len.MUT04O_MKT_SVC_DSY_NM);
	}

	/**Original name: MUT04O-MKT-SVC-DSY-NM<br>
	 * <pre>***         NM OF THE INDIVIDUAL/AGENCY/BRANCH SERVICING
	 * ***         THE TERRITORY</pre>*/
	public String getMut04oMktSvcDsyNm() {
		return readString(Pos.MUT04O_MKT_SVC_DSY_NM, Len.MUT04O_MKT_SVC_DSY_NM);
	}

	public void setMut04oMktTypeCd(char mut04oMktTypeCd) {
		writeChar(Pos.MUT04O_MKT_TYPE_CD, mut04oMktTypeCd);
	}

	/**Original name: MUT04O-MKT-TYPE-CD<br>*/
	public char getMut04oMktTypeCd() {
		return readChar(Pos.MUT04O_MKT_TYPE_CD);
	}

	public void setMut04oMktTypeDes(String mut04oMktTypeDes) {
		writeString(Pos.MUT04O_MKT_TYPE_DES, mut04oMktTypeDes, Len.MUT04O_MKT_TYPE_DES);
	}

	/**Original name: MUT04O-MKT-TYPE-DES<br>*/
	public String getMut04oMktTypeDes() {
		return readString(Pos.MUT04O_MKT_TYPE_DES, Len.MUT04O_MKT_TYPE_DES);
	}

	public void setMut04oMtMktCltId(String mut04oMtMktCltId) {
		writeString(Pos.MUT04O_MT_MKT_CLT_ID, mut04oMtMktCltId, Len.MUT04O_MT_MKT_CLT_ID);
	}

	/**Original name: MUT04O-MT-MKT-CLT-ID<br>*/
	public String getMut04oMtMktCltId() {
		return readString(Pos.MUT04O_MT_MKT_CLT_ID, Len.MUT04O_MT_MKT_CLT_ID);
	}

	public void setMut04oMtMktTerNbr(String mut04oMtMktTerNbr) {
		writeString(Pos.MUT04O_MT_MKT_TER_NBR, mut04oMtMktTerNbr, Len.MUT04O_MT_MKT_TER_NBR);
	}

	/**Original name: MUT04O-MT-MKT-TER-NBR<br>*/
	public String getMut04oMtMktTerNbr() {
		return readString(Pos.MUT04O_MT_MKT_TER_NBR, Len.MUT04O_MT_MKT_TER_NBR);
	}

	public void setMut04oMtMktDsyNm(String mut04oMtMktDsyNm) {
		writeString(Pos.MUT04O_MT_MKT_DSY_NM, mut04oMtMktDsyNm, Len.MUT04O_MT_MKT_DSY_NM);
	}

	/**Original name: MUT04O-MT-MKT-DSY-NM<br>*/
	public String getMut04oMtMktDsyNm() {
		return readString(Pos.MUT04O_MT_MKT_DSY_NM, Len.MUT04O_MT_MKT_DSY_NM);
	}

	public void setMut04oMtMktFstNm(String mut04oMtMktFstNm) {
		writeString(Pos.MUT04O_MT_MKT_FST_NM, mut04oMtMktFstNm, Len.MUT04O_MT_MKT_FST_NM);
	}

	/**Original name: MUT04O-MT-MKT-FST-NM<br>*/
	public String getMut04oMtMktFstNm() {
		return readString(Pos.MUT04O_MT_MKT_FST_NM, Len.MUT04O_MT_MKT_FST_NM);
	}

	public void setMut04oMtMktMdlNm(String mut04oMtMktMdlNm) {
		writeString(Pos.MUT04O_MT_MKT_MDL_NM, mut04oMtMktMdlNm, Len.MUT04O_MT_MKT_MDL_NM);
	}

	/**Original name: MUT04O-MT-MKT-MDL-NM<br>*/
	public String getMut04oMtMktMdlNm() {
		return readString(Pos.MUT04O_MT_MKT_MDL_NM, Len.MUT04O_MT_MKT_MDL_NM);
	}

	public void setMut04oMtMktLstNm(String mut04oMtMktLstNm) {
		writeString(Pos.MUT04O_MT_MKT_LST_NM, mut04oMtMktLstNm, Len.MUT04O_MT_MKT_LST_NM);
	}

	/**Original name: MUT04O-MT-MKT-LST-NM<br>*/
	public String getMut04oMtMktLstNm() {
		return readString(Pos.MUT04O_MT_MKT_LST_NM, Len.MUT04O_MT_MKT_LST_NM);
	}

	public void setMut04oMtMktSfx(String mut04oMtMktSfx) {
		writeString(Pos.MUT04O_MT_MKT_SFX, mut04oMtMktSfx, Len.MUT04O_MT_MKT_SFX);
	}

	/**Original name: MUT04O-MT-MKT-SFX<br>*/
	public String getMut04oMtMktSfx() {
		return readString(Pos.MUT04O_MT_MKT_SFX, Len.MUT04O_MT_MKT_SFX);
	}

	public void setMut04oMtMktPfx(String mut04oMtMktPfx) {
		writeString(Pos.MUT04O_MT_MKT_PFX, mut04oMtMktPfx, Len.MUT04O_MT_MKT_PFX);
	}

	/**Original name: MUT04O-MT-MKT-PFX<br>*/
	public String getMut04oMtMktPfx() {
		return readString(Pos.MUT04O_MT_MKT_PFX, Len.MUT04O_MT_MKT_PFX);
	}

	public void setMut04oMtCsrCltId(String mut04oMtCsrCltId) {
		writeString(Pos.MUT04O_MT_CSR_CLT_ID, mut04oMtCsrCltId, Len.MUT04O_MT_CSR_CLT_ID);
	}

	/**Original name: MUT04O-MT-CSR-CLT-ID<br>*/
	public String getMut04oMtCsrCltId() {
		return readString(Pos.MUT04O_MT_CSR_CLT_ID, Len.MUT04O_MT_CSR_CLT_ID);
	}

	public void setMut04oMtCsrTerNbr(String mut04oMtCsrTerNbr) {
		writeString(Pos.MUT04O_MT_CSR_TER_NBR, mut04oMtCsrTerNbr, Len.MUT04O_MT_CSR_TER_NBR);
	}

	/**Original name: MUT04O-MT-CSR-TER-NBR<br>*/
	public String getMut04oMtCsrTerNbr() {
		return readString(Pos.MUT04O_MT_CSR_TER_NBR, Len.MUT04O_MT_CSR_TER_NBR);
	}

	public void setMut04oMtCsrDsyNm(String mut04oMtCsrDsyNm) {
		writeString(Pos.MUT04O_MT_CSR_DSY_NM, mut04oMtCsrDsyNm, Len.MUT04O_MT_CSR_DSY_NM);
	}

	/**Original name: MUT04O-MT-CSR-DSY-NM<br>*/
	public String getMut04oMtCsrDsyNm() {
		return readString(Pos.MUT04O_MT_CSR_DSY_NM, Len.MUT04O_MT_CSR_DSY_NM);
	}

	public void setMut04oMtCsrFstNm(String mut04oMtCsrFstNm) {
		writeString(Pos.MUT04O_MT_CSR_FST_NM, mut04oMtCsrFstNm, Len.MUT04O_MT_CSR_FST_NM);
	}

	/**Original name: MUT04O-MT-CSR-FST-NM<br>*/
	public String getMut04oMtCsrFstNm() {
		return readString(Pos.MUT04O_MT_CSR_FST_NM, Len.MUT04O_MT_CSR_FST_NM);
	}

	public void setMut04oMtCsrMdlNm(String mut04oMtCsrMdlNm) {
		writeString(Pos.MUT04O_MT_CSR_MDL_NM, mut04oMtCsrMdlNm, Len.MUT04O_MT_CSR_MDL_NM);
	}

	/**Original name: MUT04O-MT-CSR-MDL-NM<br>*/
	public String getMut04oMtCsrMdlNm() {
		return readString(Pos.MUT04O_MT_CSR_MDL_NM, Len.MUT04O_MT_CSR_MDL_NM);
	}

	public void setMut04oMtCsrLstNm(String mut04oMtCsrLstNm) {
		writeString(Pos.MUT04O_MT_CSR_LST_NM, mut04oMtCsrLstNm, Len.MUT04O_MT_CSR_LST_NM);
	}

	/**Original name: MUT04O-MT-CSR-LST-NM<br>*/
	public String getMut04oMtCsrLstNm() {
		return readString(Pos.MUT04O_MT_CSR_LST_NM, Len.MUT04O_MT_CSR_LST_NM);
	}

	public void setMut04oMtCsrSfx(String mut04oMtCsrSfx) {
		writeString(Pos.MUT04O_MT_CSR_SFX, mut04oMtCsrSfx, Len.MUT04O_MT_CSR_SFX);
	}

	/**Original name: MUT04O-MT-CSR-SFX<br>*/
	public String getMut04oMtCsrSfx() {
		return readString(Pos.MUT04O_MT_CSR_SFX, Len.MUT04O_MT_CSR_SFX);
	}

	public void setMut04oMtCsrPfx(String mut04oMtCsrPfx) {
		writeString(Pos.MUT04O_MT_CSR_PFX, mut04oMtCsrPfx, Len.MUT04O_MT_CSR_PFX);
	}

	/**Original name: MUT04O-MT-CSR-PFX<br>*/
	public String getMut04oMtCsrPfx() {
		return readString(Pos.MUT04O_MT_CSR_PFX, Len.MUT04O_MT_CSR_PFX);
	}

	public void setMut04oMtSmrCltId(String mut04oMtSmrCltId) {
		writeString(Pos.MUT04O_MT_SMR_CLT_ID, mut04oMtSmrCltId, Len.MUT04O_MT_SMR_CLT_ID);
	}

	/**Original name: MUT04O-MT-SMR-CLT-ID<br>*/
	public String getMut04oMtSmrCltId() {
		return readString(Pos.MUT04O_MT_SMR_CLT_ID, Len.MUT04O_MT_SMR_CLT_ID);
	}

	public void setMut04oMtSmrTerNbr(String mut04oMtSmrTerNbr) {
		writeString(Pos.MUT04O_MT_SMR_TER_NBR, mut04oMtSmrTerNbr, Len.MUT04O_MT_SMR_TER_NBR);
	}

	/**Original name: MUT04O-MT-SMR-TER-NBR<br>*/
	public String getMut04oMtSmrTerNbr() {
		return readString(Pos.MUT04O_MT_SMR_TER_NBR, Len.MUT04O_MT_SMR_TER_NBR);
	}

	public void setMut04oMtSmrDsyNm(String mut04oMtSmrDsyNm) {
		writeString(Pos.MUT04O_MT_SMR_DSY_NM, mut04oMtSmrDsyNm, Len.MUT04O_MT_SMR_DSY_NM);
	}

	/**Original name: MUT04O-MT-SMR-DSY-NM<br>*/
	public String getMut04oMtSmrDsyNm() {
		return readString(Pos.MUT04O_MT_SMR_DSY_NM, Len.MUT04O_MT_SMR_DSY_NM);
	}

	public void setMut04oMtSmrFstNm(String mut04oMtSmrFstNm) {
		writeString(Pos.MUT04O_MT_SMR_FST_NM, mut04oMtSmrFstNm, Len.MUT04O_MT_SMR_FST_NM);
	}

	/**Original name: MUT04O-MT-SMR-FST-NM<br>*/
	public String getMut04oMtSmrFstNm() {
		return readString(Pos.MUT04O_MT_SMR_FST_NM, Len.MUT04O_MT_SMR_FST_NM);
	}

	public void setMut04oMtSmrMdlNm(String mut04oMtSmrMdlNm) {
		writeString(Pos.MUT04O_MT_SMR_MDL_NM, mut04oMtSmrMdlNm, Len.MUT04O_MT_SMR_MDL_NM);
	}

	/**Original name: MUT04O-MT-SMR-MDL-NM<br>*/
	public String getMut04oMtSmrMdlNm() {
		return readString(Pos.MUT04O_MT_SMR_MDL_NM, Len.MUT04O_MT_SMR_MDL_NM);
	}

	public void setMut04oMtSmrLstNm(String mut04oMtSmrLstNm) {
		writeString(Pos.MUT04O_MT_SMR_LST_NM, mut04oMtSmrLstNm, Len.MUT04O_MT_SMR_LST_NM);
	}

	/**Original name: MUT04O-MT-SMR-LST-NM<br>*/
	public String getMut04oMtSmrLstNm() {
		return readString(Pos.MUT04O_MT_SMR_LST_NM, Len.MUT04O_MT_SMR_LST_NM);
	}

	public void setMut04oMtSmrSfx(String mut04oMtSmrSfx) {
		writeString(Pos.MUT04O_MT_SMR_SFX, mut04oMtSmrSfx, Len.MUT04O_MT_SMR_SFX);
	}

	/**Original name: MUT04O-MT-SMR-SFX<br>*/
	public String getMut04oMtSmrSfx() {
		return readString(Pos.MUT04O_MT_SMR_SFX, Len.MUT04O_MT_SMR_SFX);
	}

	public void setMut04oMtSmrPfx(String mut04oMtSmrPfx) {
		writeString(Pos.MUT04O_MT_SMR_PFX, mut04oMtSmrPfx, Len.MUT04O_MT_SMR_PFX);
	}

	/**Original name: MUT04O-MT-SMR-PFX<br>*/
	public String getMut04oMtSmrPfx() {
		return readString(Pos.MUT04O_MT_SMR_PFX, Len.MUT04O_MT_SMR_PFX);
	}

	public void setMut04oMtDmmCltId(String mut04oMtDmmCltId) {
		writeString(Pos.MUT04O_MT_DMM_CLT_ID, mut04oMtDmmCltId, Len.MUT04O_MT_DMM_CLT_ID);
	}

	/**Original name: MUT04O-MT-DMM-CLT-ID<br>*/
	public String getMut04oMtDmmCltId() {
		return readString(Pos.MUT04O_MT_DMM_CLT_ID, Len.MUT04O_MT_DMM_CLT_ID);
	}

	public void setMut04oMtDmmTerNbr(String mut04oMtDmmTerNbr) {
		writeString(Pos.MUT04O_MT_DMM_TER_NBR, mut04oMtDmmTerNbr, Len.MUT04O_MT_DMM_TER_NBR);
	}

	/**Original name: MUT04O-MT-DMM-TER-NBR<br>*/
	public String getMut04oMtDmmTerNbr() {
		return readString(Pos.MUT04O_MT_DMM_TER_NBR, Len.MUT04O_MT_DMM_TER_NBR);
	}

	public void setMut04oMtDmmDsyNm(String mut04oMtDmmDsyNm) {
		writeString(Pos.MUT04O_MT_DMM_DSY_NM, mut04oMtDmmDsyNm, Len.MUT04O_MT_DMM_DSY_NM);
	}

	/**Original name: MUT04O-MT-DMM-DSY-NM<br>*/
	public String getMut04oMtDmmDsyNm() {
		return readString(Pos.MUT04O_MT_DMM_DSY_NM, Len.MUT04O_MT_DMM_DSY_NM);
	}

	public void setMut04oMtDmmFstNm(String mut04oMtDmmFstNm) {
		writeString(Pos.MUT04O_MT_DMM_FST_NM, mut04oMtDmmFstNm, Len.MUT04O_MT_DMM_FST_NM);
	}

	/**Original name: MUT04O-MT-DMM-FST-NM<br>*/
	public String getMut04oMtDmmFstNm() {
		return readString(Pos.MUT04O_MT_DMM_FST_NM, Len.MUT04O_MT_DMM_FST_NM);
	}

	public void setMut04oMtDmmMdlNm(String mut04oMtDmmMdlNm) {
		writeString(Pos.MUT04O_MT_DMM_MDL_NM, mut04oMtDmmMdlNm, Len.MUT04O_MT_DMM_MDL_NM);
	}

	/**Original name: MUT04O-MT-DMM-MDL-NM<br>*/
	public String getMut04oMtDmmMdlNm() {
		return readString(Pos.MUT04O_MT_DMM_MDL_NM, Len.MUT04O_MT_DMM_MDL_NM);
	}

	public void setMut04oMtDmmLstNm(String mut04oMtDmmLstNm) {
		writeString(Pos.MUT04O_MT_DMM_LST_NM, mut04oMtDmmLstNm, Len.MUT04O_MT_DMM_LST_NM);
	}

	/**Original name: MUT04O-MT-DMM-LST-NM<br>*/
	public String getMut04oMtDmmLstNm() {
		return readString(Pos.MUT04O_MT_DMM_LST_NM, Len.MUT04O_MT_DMM_LST_NM);
	}

	public void setMut04oMtDmmSfx(String mut04oMtDmmSfx) {
		writeString(Pos.MUT04O_MT_DMM_SFX, mut04oMtDmmSfx, Len.MUT04O_MT_DMM_SFX);
	}

	/**Original name: MUT04O-MT-DMM-SFX<br>*/
	public String getMut04oMtDmmSfx() {
		return readString(Pos.MUT04O_MT_DMM_SFX, Len.MUT04O_MT_DMM_SFX);
	}

	public void setMut04oMtDmmPfx(String mut04oMtDmmPfx) {
		writeString(Pos.MUT04O_MT_DMM_PFX, mut04oMtDmmPfx, Len.MUT04O_MT_DMM_PFX);
	}

	/**Original name: MUT04O-MT-DMM-PFX<br>*/
	public String getMut04oMtDmmPfx() {
		return readString(Pos.MUT04O_MT_DMM_PFX, Len.MUT04O_MT_DMM_PFX);
	}

	public void setMut04oMtRmmCltId(String mut04oMtRmmCltId) {
		writeString(Pos.MUT04O_MT_RMM_CLT_ID, mut04oMtRmmCltId, Len.MUT04O_MT_RMM_CLT_ID);
	}

	/**Original name: MUT04O-MT-RMM-CLT-ID<br>*/
	public String getMut04oMtRmmCltId() {
		return readString(Pos.MUT04O_MT_RMM_CLT_ID, Len.MUT04O_MT_RMM_CLT_ID);
	}

	public void setMut04oMtRmmTerNbr(String mut04oMtRmmTerNbr) {
		writeString(Pos.MUT04O_MT_RMM_TER_NBR, mut04oMtRmmTerNbr, Len.MUT04O_MT_RMM_TER_NBR);
	}

	/**Original name: MUT04O-MT-RMM-TER-NBR<br>*/
	public String getMut04oMtRmmTerNbr() {
		return readString(Pos.MUT04O_MT_RMM_TER_NBR, Len.MUT04O_MT_RMM_TER_NBR);
	}

	public void setMut04oMtRmmDsyNm(String mut04oMtRmmDsyNm) {
		writeString(Pos.MUT04O_MT_RMM_DSY_NM, mut04oMtRmmDsyNm, Len.MUT04O_MT_RMM_DSY_NM);
	}

	/**Original name: MUT04O-MT-RMM-DSY-NM<br>*/
	public String getMut04oMtRmmDsyNm() {
		return readString(Pos.MUT04O_MT_RMM_DSY_NM, Len.MUT04O_MT_RMM_DSY_NM);
	}

	public void setMut04oMtRmmFstNm(String mut04oMtRmmFstNm) {
		writeString(Pos.MUT04O_MT_RMM_FST_NM, mut04oMtRmmFstNm, Len.MUT04O_MT_RMM_FST_NM);
	}

	/**Original name: MUT04O-MT-RMM-FST-NM<br>*/
	public String getMut04oMtRmmFstNm() {
		return readString(Pos.MUT04O_MT_RMM_FST_NM, Len.MUT04O_MT_RMM_FST_NM);
	}

	public void setMut04oMtRmmMdlNm(String mut04oMtRmmMdlNm) {
		writeString(Pos.MUT04O_MT_RMM_MDL_NM, mut04oMtRmmMdlNm, Len.MUT04O_MT_RMM_MDL_NM);
	}

	/**Original name: MUT04O-MT-RMM-MDL-NM<br>*/
	public String getMut04oMtRmmMdlNm() {
		return readString(Pos.MUT04O_MT_RMM_MDL_NM, Len.MUT04O_MT_RMM_MDL_NM);
	}

	public void setMut04oMtRmmLstNm(String mut04oMtRmmLstNm) {
		writeString(Pos.MUT04O_MT_RMM_LST_NM, mut04oMtRmmLstNm, Len.MUT04O_MT_RMM_LST_NM);
	}

	/**Original name: MUT04O-MT-RMM-LST-NM<br>*/
	public String getMut04oMtRmmLstNm() {
		return readString(Pos.MUT04O_MT_RMM_LST_NM, Len.MUT04O_MT_RMM_LST_NM);
	}

	public void setMut04oMtRmmSfx(String mut04oMtRmmSfx) {
		writeString(Pos.MUT04O_MT_RMM_SFX, mut04oMtRmmSfx, Len.MUT04O_MT_RMM_SFX);
	}

	/**Original name: MUT04O-MT-RMM-SFX<br>*/
	public String getMut04oMtRmmSfx() {
		return readString(Pos.MUT04O_MT_RMM_SFX, Len.MUT04O_MT_RMM_SFX);
	}

	public void setMut04oMtRmmPfx(String mut04oMtRmmPfx) {
		writeString(Pos.MUT04O_MT_RMM_PFX, mut04oMtRmmPfx, Len.MUT04O_MT_RMM_PFX);
	}

	/**Original name: MUT04O-MT-RMM-PFX<br>*/
	public String getMut04oMtRmmPfx() {
		return readString(Pos.MUT04O_MT_RMM_PFX, Len.MUT04O_MT_RMM_PFX);
	}

	public void setMut04oMtDfoCltId(String mut04oMtDfoCltId) {
		writeString(Pos.MUT04O_MT_DFO_CLT_ID, mut04oMtDfoCltId, Len.MUT04O_MT_DFO_CLT_ID);
	}

	/**Original name: MUT04O-MT-DFO-CLT-ID<br>*/
	public String getMut04oMtDfoCltId() {
		return readString(Pos.MUT04O_MT_DFO_CLT_ID, Len.MUT04O_MT_DFO_CLT_ID);
	}

	public void setMut04oMtDfoTerNbr(String mut04oMtDfoTerNbr) {
		writeString(Pos.MUT04O_MT_DFO_TER_NBR, mut04oMtDfoTerNbr, Len.MUT04O_MT_DFO_TER_NBR);
	}

	/**Original name: MUT04O-MT-DFO-TER-NBR<br>*/
	public String getMut04oMtDfoTerNbr() {
		return readString(Pos.MUT04O_MT_DFO_TER_NBR, Len.MUT04O_MT_DFO_TER_NBR);
	}

	public void setMut04oMtDfoDsyNm(String mut04oMtDfoDsyNm) {
		writeString(Pos.MUT04O_MT_DFO_DSY_NM, mut04oMtDfoDsyNm, Len.MUT04O_MT_DFO_DSY_NM);
	}

	/**Original name: MUT04O-MT-DFO-DSY-NM<br>*/
	public String getMut04oMtDfoDsyNm() {
		return readString(Pos.MUT04O_MT_DFO_DSY_NM, Len.MUT04O_MT_DFO_DSY_NM);
	}

	public void setMut04oMtDfoFstNm(String mut04oMtDfoFstNm) {
		writeString(Pos.MUT04O_MT_DFO_FST_NM, mut04oMtDfoFstNm, Len.MUT04O_MT_DFO_FST_NM);
	}

	/**Original name: MUT04O-MT-DFO-FST-NM<br>*/
	public String getMut04oMtDfoFstNm() {
		return readString(Pos.MUT04O_MT_DFO_FST_NM, Len.MUT04O_MT_DFO_FST_NM);
	}

	public void setMut04oMtDfoMdlNm(String mut04oMtDfoMdlNm) {
		writeString(Pos.MUT04O_MT_DFO_MDL_NM, mut04oMtDfoMdlNm, Len.MUT04O_MT_DFO_MDL_NM);
	}

	/**Original name: MUT04O-MT-DFO-MDL-NM<br>*/
	public String getMut04oMtDfoMdlNm() {
		return readString(Pos.MUT04O_MT_DFO_MDL_NM, Len.MUT04O_MT_DFO_MDL_NM);
	}

	public void setMut04oMtDfoLstNm(String mut04oMtDfoLstNm) {
		writeString(Pos.MUT04O_MT_DFO_LST_NM, mut04oMtDfoLstNm, Len.MUT04O_MT_DFO_LST_NM);
	}

	/**Original name: MUT04O-MT-DFO-LST-NM<br>*/
	public String getMut04oMtDfoLstNm() {
		return readString(Pos.MUT04O_MT_DFO_LST_NM, Len.MUT04O_MT_DFO_LST_NM);
	}

	public void setMut04oMtDfoSfx(String mut04oMtDfoSfx) {
		writeString(Pos.MUT04O_MT_DFO_SFX, mut04oMtDfoSfx, Len.MUT04O_MT_DFO_SFX);
	}

	/**Original name: MUT04O-MT-DFO-SFX<br>*/
	public String getMut04oMtDfoSfx() {
		return readString(Pos.MUT04O_MT_DFO_SFX, Len.MUT04O_MT_DFO_SFX);
	}

	public void setMut04oMtDfoPfx(String mut04oMtDfoPfx) {
		writeString(Pos.MUT04O_MT_DFO_PFX, mut04oMtDfoPfx, Len.MUT04O_MT_DFO_PFX);
	}

	/**Original name: MUT04O-MT-DFO-PFX<br>*/
	public String getMut04oMtDfoPfx() {
		return readString(Pos.MUT04O_MT_DFO_PFX, Len.MUT04O_MT_DFO_PFX);
	}

	public void setMut04oAtBrnCltId(String mut04oAtBrnCltId) {
		writeString(Pos.MUT04O_AT_BRN_CLT_ID, mut04oAtBrnCltId, Len.MUT04O_AT_BRN_CLT_ID);
	}

	/**Original name: MUT04O-AT-BRN-CLT-ID<br>*/
	public String getMut04oAtBrnCltId() {
		return readString(Pos.MUT04O_AT_BRN_CLT_ID, Len.MUT04O_AT_BRN_CLT_ID);
	}

	public void setMut04oAtBrnTerNbr(String mut04oAtBrnTerNbr) {
		writeString(Pos.MUT04O_AT_BRN_TER_NBR, mut04oAtBrnTerNbr, Len.MUT04O_AT_BRN_TER_NBR);
	}

	/**Original name: MUT04O-AT-BRN-TER-NBR<br>*/
	public String getMut04oAtBrnTerNbr() {
		return readString(Pos.MUT04O_AT_BRN_TER_NBR, Len.MUT04O_AT_BRN_TER_NBR);
	}

	public void setMut04oAtBrnNm(String mut04oAtBrnNm) {
		writeString(Pos.MUT04O_AT_BRN_NM, mut04oAtBrnNm, Len.MUT04O_AT_BRN_NM);
	}

	/**Original name: MUT04O-AT-BRN-NM<br>*/
	public String getMut04oAtBrnNm() {
		return readString(Pos.MUT04O_AT_BRN_NM, Len.MUT04O_AT_BRN_NM);
	}

	public void setMut04oAtAgcCltId(String mut04oAtAgcCltId) {
		writeString(Pos.MUT04O_AT_AGC_CLT_ID, mut04oAtAgcCltId, Len.MUT04O_AT_AGC_CLT_ID);
	}

	/**Original name: MUT04O-AT-AGC-CLT-ID<br>*/
	public String getMut04oAtAgcCltId() {
		return readString(Pos.MUT04O_AT_AGC_CLT_ID, Len.MUT04O_AT_AGC_CLT_ID);
	}

	public void setMut04oAtAgcTerNbr(String mut04oAtAgcTerNbr) {
		writeString(Pos.MUT04O_AT_AGC_TER_NBR, mut04oAtAgcTerNbr, Len.MUT04O_AT_AGC_TER_NBR);
	}

	/**Original name: MUT04O-AT-AGC-TER-NBR<br>*/
	public String getMut04oAtAgcTerNbr() {
		return readString(Pos.MUT04O_AT_AGC_TER_NBR, Len.MUT04O_AT_AGC_TER_NBR);
	}

	public void setMut04oAtAgcNm(String mut04oAtAgcNm) {
		writeString(Pos.MUT04O_AT_AGC_NM, mut04oAtAgcNm, Len.MUT04O_AT_AGC_NM);
	}

	/**Original name: MUT04O-AT-AGC-NM<br>*/
	public String getMut04oAtAgcNm() {
		return readString(Pos.MUT04O_AT_AGC_NM, Len.MUT04O_AT_AGC_NM);
	}

	public void setMut04oAtSmrCltId(String mut04oAtSmrCltId) {
		writeString(Pos.MUT04O_AT_SMR_CLT_ID, mut04oAtSmrCltId, Len.MUT04O_AT_SMR_CLT_ID);
	}

	/**Original name: MUT04O-AT-SMR-CLT-ID<br>*/
	public String getMut04oAtSmrCltId() {
		return readString(Pos.MUT04O_AT_SMR_CLT_ID, Len.MUT04O_AT_SMR_CLT_ID);
	}

	public void setMut04oAtSmrTerNbr(String mut04oAtSmrTerNbr) {
		writeString(Pos.MUT04O_AT_SMR_TER_NBR, mut04oAtSmrTerNbr, Len.MUT04O_AT_SMR_TER_NBR);
	}

	/**Original name: MUT04O-AT-SMR-TER-NBR<br>*/
	public String getMut04oAtSmrTerNbr() {
		return readString(Pos.MUT04O_AT_SMR_TER_NBR, Len.MUT04O_AT_SMR_TER_NBR);
	}

	public void setMut04oAtSmrNm(String mut04oAtSmrNm) {
		writeString(Pos.MUT04O_AT_SMR_NM, mut04oAtSmrNm, Len.MUT04O_AT_SMR_NM);
	}

	/**Original name: MUT04O-AT-SMR-NM<br>*/
	public String getMut04oAtSmrNm() {
		return readString(Pos.MUT04O_AT_SMR_NM, Len.MUT04O_AT_SMR_NM);
	}

	public void setMut04oAtAmmCltId(String mut04oAtAmmCltId) {
		writeString(Pos.MUT04O_AT_AMM_CLT_ID, mut04oAtAmmCltId, Len.MUT04O_AT_AMM_CLT_ID);
	}

	/**Original name: MUT04O-AT-AMM-CLT-ID<br>*/
	public String getMut04oAtAmmCltId() {
		return readString(Pos.MUT04O_AT_AMM_CLT_ID, Len.MUT04O_AT_AMM_CLT_ID);
	}

	public void setMut04oAtAmmTerNbr(String mut04oAtAmmTerNbr) {
		writeString(Pos.MUT04O_AT_AMM_TER_NBR, mut04oAtAmmTerNbr, Len.MUT04O_AT_AMM_TER_NBR);
	}

	/**Original name: MUT04O-AT-AMM-TER-NBR<br>*/
	public String getMut04oAtAmmTerNbr() {
		return readString(Pos.MUT04O_AT_AMM_TER_NBR, Len.MUT04O_AT_AMM_TER_NBR);
	}

	public void setMut04oAtAmmDsyNm(String mut04oAtAmmDsyNm) {
		writeString(Pos.MUT04O_AT_AMM_DSY_NM, mut04oAtAmmDsyNm, Len.MUT04O_AT_AMM_DSY_NM);
	}

	/**Original name: MUT04O-AT-AMM-DSY-NM<br>*/
	public String getMut04oAtAmmDsyNm() {
		return readString(Pos.MUT04O_AT_AMM_DSY_NM, Len.MUT04O_AT_AMM_DSY_NM);
	}

	public void setMut04oAtAmmFstNm(String mut04oAtAmmFstNm) {
		writeString(Pos.MUT04O_AT_AMM_FST_NM, mut04oAtAmmFstNm, Len.MUT04O_AT_AMM_FST_NM);
	}

	/**Original name: MUT04O-AT-AMM-FST-NM<br>*/
	public String getMut04oAtAmmFstNm() {
		return readString(Pos.MUT04O_AT_AMM_FST_NM, Len.MUT04O_AT_AMM_FST_NM);
	}

	public void setMut04oAtAmmMdlNm(String mut04oAtAmmMdlNm) {
		writeString(Pos.MUT04O_AT_AMM_MDL_NM, mut04oAtAmmMdlNm, Len.MUT04O_AT_AMM_MDL_NM);
	}

	/**Original name: MUT04O-AT-AMM-MDL-NM<br>*/
	public String getMut04oAtAmmMdlNm() {
		return readString(Pos.MUT04O_AT_AMM_MDL_NM, Len.MUT04O_AT_AMM_MDL_NM);
	}

	public void setMut04oAtAmmLstNm(String mut04oAtAmmLstNm) {
		writeString(Pos.MUT04O_AT_AMM_LST_NM, mut04oAtAmmLstNm, Len.MUT04O_AT_AMM_LST_NM);
	}

	/**Original name: MUT04O-AT-AMM-LST-NM<br>*/
	public String getMut04oAtAmmLstNm() {
		return readString(Pos.MUT04O_AT_AMM_LST_NM, Len.MUT04O_AT_AMM_LST_NM);
	}

	public void setMut04oAtAmmSfx(String mut04oAtAmmSfx) {
		writeString(Pos.MUT04O_AT_AMM_SFX, mut04oAtAmmSfx, Len.MUT04O_AT_AMM_SFX);
	}

	/**Original name: MUT04O-AT-AMM-SFX<br>*/
	public String getMut04oAtAmmSfx() {
		return readString(Pos.MUT04O_AT_AMM_SFX, Len.MUT04O_AT_AMM_SFX);
	}

	public void setMut04oAtAmmPfx(String mut04oAtAmmPfx) {
		writeString(Pos.MUT04O_AT_AMM_PFX, mut04oAtAmmPfx, Len.MUT04O_AT_AMM_PFX);
	}

	/**Original name: MUT04O-AT-AMM-PFX<br>*/
	public String getMut04oAtAmmPfx() {
		return readString(Pos.MUT04O_AT_AMM_PFX, Len.MUT04O_AT_AMM_PFX);
	}

	public void setMut04oAtAfmCltId(String mut04oAtAfmCltId) {
		writeString(Pos.MUT04O_AT_AFM_CLT_ID, mut04oAtAfmCltId, Len.MUT04O_AT_AFM_CLT_ID);
	}

	/**Original name: MUT04O-AT-AFM-CLT-ID<br>*/
	public String getMut04oAtAfmCltId() {
		return readString(Pos.MUT04O_AT_AFM_CLT_ID, Len.MUT04O_AT_AFM_CLT_ID);
	}

	public void setMut04oAtAfmTerNbr(String mut04oAtAfmTerNbr) {
		writeString(Pos.MUT04O_AT_AFM_TER_NBR, mut04oAtAfmTerNbr, Len.MUT04O_AT_AFM_TER_NBR);
	}

	/**Original name: MUT04O-AT-AFM-TER-NBR<br>*/
	public String getMut04oAtAfmTerNbr() {
		return readString(Pos.MUT04O_AT_AFM_TER_NBR, Len.MUT04O_AT_AFM_TER_NBR);
	}

	public void setMut04oAtAfmDsyNm(String mut04oAtAfmDsyNm) {
		writeString(Pos.MUT04O_AT_AFM_DSY_NM, mut04oAtAfmDsyNm, Len.MUT04O_AT_AFM_DSY_NM);
	}

	/**Original name: MUT04O-AT-AFM-DSY-NM<br>*/
	public String getMut04oAtAfmDsyNm() {
		return readString(Pos.MUT04O_AT_AFM_DSY_NM, Len.MUT04O_AT_AFM_DSY_NM);
	}

	public void setMut04oAtAfmFstNm(String mut04oAtAfmFstNm) {
		writeString(Pos.MUT04O_AT_AFM_FST_NM, mut04oAtAfmFstNm, Len.MUT04O_AT_AFM_FST_NM);
	}

	/**Original name: MUT04O-AT-AFM-FST-NM<br>*/
	public String getMut04oAtAfmFstNm() {
		return readString(Pos.MUT04O_AT_AFM_FST_NM, Len.MUT04O_AT_AFM_FST_NM);
	}

	public void setMut04oAtAfmMdlNm(String mut04oAtAfmMdlNm) {
		writeString(Pos.MUT04O_AT_AFM_MDL_NM, mut04oAtAfmMdlNm, Len.MUT04O_AT_AFM_MDL_NM);
	}

	/**Original name: MUT04O-AT-AFM-MDL-NM<br>*/
	public String getMut04oAtAfmMdlNm() {
		return readString(Pos.MUT04O_AT_AFM_MDL_NM, Len.MUT04O_AT_AFM_MDL_NM);
	}

	public void setMut04oAtAfmLstNm(String mut04oAtAfmLstNm) {
		writeString(Pos.MUT04O_AT_AFM_LST_NM, mut04oAtAfmLstNm, Len.MUT04O_AT_AFM_LST_NM);
	}

	/**Original name: MUT04O-AT-AFM-LST-NM<br>*/
	public String getMut04oAtAfmLstNm() {
		return readString(Pos.MUT04O_AT_AFM_LST_NM, Len.MUT04O_AT_AFM_LST_NM);
	}

	public void setMut04oAtAfmSfx(String mut04oAtAfmSfx) {
		writeString(Pos.MUT04O_AT_AFM_SFX, mut04oAtAfmSfx, Len.MUT04O_AT_AFM_SFX);
	}

	/**Original name: MUT04O-AT-AFM-SFX<br>*/
	public String getMut04oAtAfmSfx() {
		return readString(Pos.MUT04O_AT_AFM_SFX, Len.MUT04O_AT_AFM_SFX);
	}

	public void setMut04oAtAfmPfx(String mut04oAtAfmPfx) {
		writeString(Pos.MUT04O_AT_AFM_PFX, mut04oAtAfmPfx, Len.MUT04O_AT_AFM_PFX);
	}

	/**Original name: MUT04O-AT-AFM-PFX<br>*/
	public String getMut04oAtAfmPfx() {
		return readString(Pos.MUT04O_AT_AFM_PFX, Len.MUT04O_AT_AFM_PFX);
	}

	public void setMut04oAtAvpCltId(String mut04oAtAvpCltId) {
		writeString(Pos.MUT04O_AT_AVP_CLT_ID, mut04oAtAvpCltId, Len.MUT04O_AT_AVP_CLT_ID);
	}

	/**Original name: MUT04O-AT-AVP-CLT-ID<br>*/
	public String getMut04oAtAvpCltId() {
		return readString(Pos.MUT04O_AT_AVP_CLT_ID, Len.MUT04O_AT_AVP_CLT_ID);
	}

	public void setMut04oAtAvpTerNbr(String mut04oAtAvpTerNbr) {
		writeString(Pos.MUT04O_AT_AVP_TER_NBR, mut04oAtAvpTerNbr, Len.MUT04O_AT_AVP_TER_NBR);
	}

	/**Original name: MUT04O-AT-AVP-TER-NBR<br>*/
	public String getMut04oAtAvpTerNbr() {
		return readString(Pos.MUT04O_AT_AVP_TER_NBR, Len.MUT04O_AT_AVP_TER_NBR);
	}

	public void setMut04oAtAvpDsyNm(String mut04oAtAvpDsyNm) {
		writeString(Pos.MUT04O_AT_AVP_DSY_NM, mut04oAtAvpDsyNm, Len.MUT04O_AT_AVP_DSY_NM);
	}

	/**Original name: MUT04O-AT-AVP-DSY-NM<br>*/
	public String getMut04oAtAvpDsyNm() {
		return readString(Pos.MUT04O_AT_AVP_DSY_NM, Len.MUT04O_AT_AVP_DSY_NM);
	}

	public void setMut04oAtAvpFstNm(String mut04oAtAvpFstNm) {
		writeString(Pos.MUT04O_AT_AVP_FST_NM, mut04oAtAvpFstNm, Len.MUT04O_AT_AVP_FST_NM);
	}

	/**Original name: MUT04O-AT-AVP-FST-NM<br>*/
	public String getMut04oAtAvpFstNm() {
		return readString(Pos.MUT04O_AT_AVP_FST_NM, Len.MUT04O_AT_AVP_FST_NM);
	}

	public void setMut04oAtAvpMdlNm(String mut04oAtAvpMdlNm) {
		writeString(Pos.MUT04O_AT_AVP_MDL_NM, mut04oAtAvpMdlNm, Len.MUT04O_AT_AVP_MDL_NM);
	}

	/**Original name: MUT04O-AT-AVP-MDL-NM<br>*/
	public String getMut04oAtAvpMdlNm() {
		return readString(Pos.MUT04O_AT_AVP_MDL_NM, Len.MUT04O_AT_AVP_MDL_NM);
	}

	public void setMut04oAtAvpLstNm(String mut04oAtAvpLstNm) {
		writeString(Pos.MUT04O_AT_AVP_LST_NM, mut04oAtAvpLstNm, Len.MUT04O_AT_AVP_LST_NM);
	}

	/**Original name: MUT04O-AT-AVP-LST-NM<br>*/
	public String getMut04oAtAvpLstNm() {
		return readString(Pos.MUT04O_AT_AVP_LST_NM, Len.MUT04O_AT_AVP_LST_NM);
	}

	public void setMut04oAtAvpSfx(String mut04oAtAvpSfx) {
		writeString(Pos.MUT04O_AT_AVP_SFX, mut04oAtAvpSfx, Len.MUT04O_AT_AVP_SFX);
	}

	/**Original name: MUT04O-AT-AVP-SFX<br>*/
	public String getMut04oAtAvpSfx() {
		return readString(Pos.MUT04O_AT_AVP_SFX, Len.MUT04O_AT_AVP_SFX);
	}

	public void setMut04oAtAvpPfx(String mut04oAtAvpPfx) {
		writeString(Pos.MUT04O_AT_AVP_PFX, mut04oAtAvpPfx, Len.MUT04O_AT_AVP_PFX);
	}

	/**Original name: MUT04O-AT-AVP-PFX<br>*/
	public String getMut04oAtAvpPfx() {
		return readString(Pos.MUT04O_AT_AVP_PFX, Len.MUT04O_AT_AVP_PFX);
	}

	public void setMut04oSvcUwDsyNm(String mut04oSvcUwDsyNm) {
		writeString(Pos.MUT04O_SVC_UW_DSY_NM, mut04oSvcUwDsyNm, Len.MUT04O_SVC_UW_DSY_NM);
	}

	/**Original name: MUT04O-SVC-UW-DSY-NM<br>*/
	public String getMut04oSvcUwDsyNm() {
		return readString(Pos.MUT04O_SVC_UW_DSY_NM, Len.MUT04O_SVC_UW_DSY_NM);
	}

	public void setMut04oSvcUwTypeCd(String mut04oSvcUwTypeCd) {
		writeString(Pos.MUT04O_SVC_UW_TYPE_CD, mut04oSvcUwTypeCd, Len.MUT04O_SVC_UW_TYPE_CD);
	}

	/**Original name: MUT04O-SVC-UW-TYPE-CD<br>*/
	public String getMut04oSvcUwTypeCd() {
		return readString(Pos.MUT04O_SVC_UW_TYPE_CD, Len.MUT04O_SVC_UW_TYPE_CD);
	}

	public void setMut04oSvcUwTypeDes(String mut04oSvcUwTypeDes) {
		writeString(Pos.MUT04O_SVC_UW_TYPE_DES, mut04oSvcUwTypeDes, Len.MUT04O_SVC_UW_TYPE_DES);
	}

	/**Original name: MUT04O-SVC-UW-TYPE-DES<br>*/
	public String getMut04oSvcUwTypeDes() {
		return readString(Pos.MUT04O_SVC_UW_TYPE_DES, Len.MUT04O_SVC_UW_TYPE_DES);
	}

	public void setMut04oUwCltId(String mut04oUwCltId) {
		writeString(Pos.MUT04O_UW_CLT_ID, mut04oUwCltId, Len.MUT04O_UW_CLT_ID);
	}

	/**Original name: MUT04O-UW-CLT-ID<br>*/
	public String getMut04oUwCltId() {
		return readString(Pos.MUT04O_UW_CLT_ID, Len.MUT04O_UW_CLT_ID);
	}

	public void setMut04oUwDsyNm(String mut04oUwDsyNm) {
		writeString(Pos.MUT04O_UW_DSY_NM, mut04oUwDsyNm, Len.MUT04O_UW_DSY_NM);
	}

	/**Original name: MUT04O-UW-DSY-NM<br>*/
	public String getMut04oUwDsyNm() {
		return readString(Pos.MUT04O_UW_DSY_NM, Len.MUT04O_UW_DSY_NM);
	}

	public void setMut04oUwFstNm(String mut04oUwFstNm) {
		writeString(Pos.MUT04O_UW_FST_NM, mut04oUwFstNm, Len.MUT04O_UW_FST_NM);
	}

	/**Original name: MUT04O-UW-FST-NM<br>*/
	public String getMut04oUwFstNm() {
		return readString(Pos.MUT04O_UW_FST_NM, Len.MUT04O_UW_FST_NM);
	}

	public void setMut04oUwMdlNm(String mut04oUwMdlNm) {
		writeString(Pos.MUT04O_UW_MDL_NM, mut04oUwMdlNm, Len.MUT04O_UW_MDL_NM);
	}

	/**Original name: MUT04O-UW-MDL-NM<br>*/
	public String getMut04oUwMdlNm() {
		return readString(Pos.MUT04O_UW_MDL_NM, Len.MUT04O_UW_MDL_NM);
	}

	public void setMut04oUwLstNm(String mut04oUwLstNm) {
		writeString(Pos.MUT04O_UW_LST_NM, mut04oUwLstNm, Len.MUT04O_UW_LST_NM);
	}

	/**Original name: MUT04O-UW-LST-NM<br>*/
	public String getMut04oUwLstNm() {
		return readString(Pos.MUT04O_UW_LST_NM, Len.MUT04O_UW_LST_NM);
	}

	public void setMut04oUwSfx(String mut04oUwSfx) {
		writeString(Pos.MUT04O_UW_SFX, mut04oUwSfx, Len.MUT04O_UW_SFX);
	}

	/**Original name: MUT04O-UW-SFX<br>*/
	public String getMut04oUwSfx() {
		return readString(Pos.MUT04O_UW_SFX, Len.MUT04O_UW_SFX);
	}

	public void setMut04oUwPfx(String mut04oUwPfx) {
		writeString(Pos.MUT04O_UW_PFX, mut04oUwPfx, Len.MUT04O_UW_PFX);
	}

	/**Original name: MUT04O-UW-PFX<br>*/
	public String getMut04oUwPfx() {
		return readString(Pos.MUT04O_UW_PFX, Len.MUT04O_UW_PFX);
	}

	public void setMut04oRskAlsCltId(String mut04oRskAlsCltId) {
		writeString(Pos.MUT04O_RSK_ALS_CLT_ID, mut04oRskAlsCltId, Len.MUT04O_RSK_ALS_CLT_ID);
	}

	/**Original name: MUT04O-RSK-ALS-CLT-ID<br>*/
	public String getMut04oRskAlsCltId() {
		return readString(Pos.MUT04O_RSK_ALS_CLT_ID, Len.MUT04O_RSK_ALS_CLT_ID);
	}

	public void setMut04oRskAlsDsyNm(String mut04oRskAlsDsyNm) {
		writeString(Pos.MUT04O_RSK_ALS_DSY_NM, mut04oRskAlsDsyNm, Len.MUT04O_RSK_ALS_DSY_NM);
	}

	/**Original name: MUT04O-RSK-ALS-DSY-NM<br>*/
	public String getMut04oRskAlsDsyNm() {
		return readString(Pos.MUT04O_RSK_ALS_DSY_NM, Len.MUT04O_RSK_ALS_DSY_NM);
	}

	public void setMut04oRskAlsFstNm(String mut04oRskAlsFstNm) {
		writeString(Pos.MUT04O_RSK_ALS_FST_NM, mut04oRskAlsFstNm, Len.MUT04O_RSK_ALS_FST_NM);
	}

	/**Original name: MUT04O-RSK-ALS-FST-NM<br>*/
	public String getMut04oRskAlsFstNm() {
		return readString(Pos.MUT04O_RSK_ALS_FST_NM, Len.MUT04O_RSK_ALS_FST_NM);
	}

	public void setMut04oRskAlsMdlNm(String mut04oRskAlsMdlNm) {
		writeString(Pos.MUT04O_RSK_ALS_MDL_NM, mut04oRskAlsMdlNm, Len.MUT04O_RSK_ALS_MDL_NM);
	}

	/**Original name: MUT04O-RSK-ALS-MDL-NM<br>*/
	public String getMut04oRskAlsMdlNm() {
		return readString(Pos.MUT04O_RSK_ALS_MDL_NM, Len.MUT04O_RSK_ALS_MDL_NM);
	}

	public void setMut04oRskAlsLstNm(String mut04oRskAlsLstNm) {
		writeString(Pos.MUT04O_RSK_ALS_LST_NM, mut04oRskAlsLstNm, Len.MUT04O_RSK_ALS_LST_NM);
	}

	/**Original name: MUT04O-RSK-ALS-LST-NM<br>*/
	public String getMut04oRskAlsLstNm() {
		return readString(Pos.MUT04O_RSK_ALS_LST_NM, Len.MUT04O_RSK_ALS_LST_NM);
	}

	public void setMut04oRskAlsSfx(String mut04oRskAlsSfx) {
		writeString(Pos.MUT04O_RSK_ALS_SFX, mut04oRskAlsSfx, Len.MUT04O_RSK_ALS_SFX);
	}

	/**Original name: MUT04O-RSK-ALS-SFX<br>*/
	public String getMut04oRskAlsSfx() {
		return readString(Pos.MUT04O_RSK_ALS_SFX, Len.MUT04O_RSK_ALS_SFX);
	}

	public void setMut04oRskAlsPfx(String mut04oRskAlsPfx) {
		writeString(Pos.MUT04O_RSK_ALS_PFX, mut04oRskAlsPfx, Len.MUT04O_RSK_ALS_PFX);
	}

	/**Original name: MUT04O-RSK-ALS-PFX<br>*/
	public String getMut04oRskAlsPfx() {
		return readString(Pos.MUT04O_RSK_ALS_PFX, Len.MUT04O_RSK_ALS_PFX);
	}

	public void setMut04oDumCltId(String mut04oDumCltId) {
		writeString(Pos.MUT04O_DUM_CLT_ID, mut04oDumCltId, Len.MUT04O_DUM_CLT_ID);
	}

	/**Original name: MUT04O-DUM-CLT-ID<br>*/
	public String getMut04oDumCltId() {
		return readString(Pos.MUT04O_DUM_CLT_ID, Len.MUT04O_DUM_CLT_ID);
	}

	public void setMut04oDumDsyNm(String mut04oDumDsyNm) {
		writeString(Pos.MUT04O_DUM_DSY_NM, mut04oDumDsyNm, Len.MUT04O_DUM_DSY_NM);
	}

	/**Original name: MUT04O-DUM-DSY-NM<br>*/
	public String getMut04oDumDsyNm() {
		return readString(Pos.MUT04O_DUM_DSY_NM, Len.MUT04O_DUM_DSY_NM);
	}

	public void setMut04oDumTer(String mut04oDumTer) {
		writeString(Pos.MUT04O_DUM_TER, mut04oDumTer, Len.MUT04O_DUM_TER);
	}

	/**Original name: MUT04O-DUM-TER<br>*/
	public String getMut04oDumTer() {
		return readString(Pos.MUT04O_DUM_TER, Len.MUT04O_DUM_TER);
	}

	public void setMut04oDumFstNm(String mut04oDumFstNm) {
		writeString(Pos.MUT04O_DUM_FST_NM, mut04oDumFstNm, Len.MUT04O_DUM_FST_NM);
	}

	/**Original name: MUT04O-DUM-FST-NM<br>*/
	public String getMut04oDumFstNm() {
		return readString(Pos.MUT04O_DUM_FST_NM, Len.MUT04O_DUM_FST_NM);
	}

	public void setMut04oDumMdlNm(String mut04oDumMdlNm) {
		writeString(Pos.MUT04O_DUM_MDL_NM, mut04oDumMdlNm, Len.MUT04O_DUM_MDL_NM);
	}

	/**Original name: MUT04O-DUM-MDL-NM<br>*/
	public String getMut04oDumMdlNm() {
		return readString(Pos.MUT04O_DUM_MDL_NM, Len.MUT04O_DUM_MDL_NM);
	}

	public void setMut04oDumLstNm(String mut04oDumLstNm) {
		writeString(Pos.MUT04O_DUM_LST_NM, mut04oDumLstNm, Len.MUT04O_DUM_LST_NM);
	}

	/**Original name: MUT04O-DUM-LST-NM<br>*/
	public String getMut04oDumLstNm() {
		return readString(Pos.MUT04O_DUM_LST_NM, Len.MUT04O_DUM_LST_NM);
	}

	public void setMut04oDumSfx(String mut04oDumSfx) {
		writeString(Pos.MUT04O_DUM_SFX, mut04oDumSfx, Len.MUT04O_DUM_SFX);
	}

	/**Original name: MUT04O-DUM-SFX<br>*/
	public String getMut04oDumSfx() {
		return readString(Pos.MUT04O_DUM_SFX, Len.MUT04O_DUM_SFX);
	}

	public void setMut04oDumPfx(String mut04oDumPfx) {
		writeString(Pos.MUT04O_DUM_PFX, mut04oDumPfx, Len.MUT04O_DUM_PFX);
	}

	/**Original name: MUT04O-DUM-PFX<br>*/
	public String getMut04oDumPfx() {
		return readString(Pos.MUT04O_DUM_PFX, Len.MUT04O_DUM_PFX);
	}

	public void setMut04oRumCltId(String mut04oRumCltId) {
		writeString(Pos.MUT04O_RUM_CLT_ID, mut04oRumCltId, Len.MUT04O_RUM_CLT_ID);
	}

	/**Original name: MUT04O-RUM-CLT-ID<br>*/
	public String getMut04oRumCltId() {
		return readString(Pos.MUT04O_RUM_CLT_ID, Len.MUT04O_RUM_CLT_ID);
	}

	public void setMut04oRumDsyNm(String mut04oRumDsyNm) {
		writeString(Pos.MUT04O_RUM_DSY_NM, mut04oRumDsyNm, Len.MUT04O_RUM_DSY_NM);
	}

	/**Original name: MUT04O-RUM-DSY-NM<br>*/
	public String getMut04oRumDsyNm() {
		return readString(Pos.MUT04O_RUM_DSY_NM, Len.MUT04O_RUM_DSY_NM);
	}

	public void setMut04oRumTer(String mut04oRumTer) {
		writeString(Pos.MUT04O_RUM_TER, mut04oRumTer, Len.MUT04O_RUM_TER);
	}

	/**Original name: MUT04O-RUM-TER<br>*/
	public String getMut04oRumTer() {
		return readString(Pos.MUT04O_RUM_TER, Len.MUT04O_RUM_TER);
	}

	public void setMut04oRumFstNm(String mut04oRumFstNm) {
		writeString(Pos.MUT04O_RUM_FST_NM, mut04oRumFstNm, Len.MUT04O_RUM_FST_NM);
	}

	/**Original name: MUT04O-RUM-FST-NM<br>*/
	public String getMut04oRumFstNm() {
		return readString(Pos.MUT04O_RUM_FST_NM, Len.MUT04O_RUM_FST_NM);
	}

	public void setMut04oRumMdlNm(String mut04oRumMdlNm) {
		writeString(Pos.MUT04O_RUM_MDL_NM, mut04oRumMdlNm, Len.MUT04O_RUM_MDL_NM);
	}

	/**Original name: MUT04O-RUM-MDL-NM<br>*/
	public String getMut04oRumMdlNm() {
		return readString(Pos.MUT04O_RUM_MDL_NM, Len.MUT04O_RUM_MDL_NM);
	}

	public void setMut04oRumLstNm(String mut04oRumLstNm) {
		writeString(Pos.MUT04O_RUM_LST_NM, mut04oRumLstNm, Len.MUT04O_RUM_LST_NM);
	}

	/**Original name: MUT04O-RUM-LST-NM<br>*/
	public String getMut04oRumLstNm() {
		return readString(Pos.MUT04O_RUM_LST_NM, Len.MUT04O_RUM_LST_NM);
	}

	public void setMut04oRumSfx(String mut04oRumSfx) {
		writeString(Pos.MUT04O_RUM_SFX, mut04oRumSfx, Len.MUT04O_RUM_SFX);
	}

	/**Original name: MUT04O-RUM-SFX<br>*/
	public String getMut04oRumSfx() {
		return readString(Pos.MUT04O_RUM_SFX, Len.MUT04O_RUM_SFX);
	}

	public void setMut04oRumPfx(String mut04oRumPfx) {
		writeString(Pos.MUT04O_RUM_PFX, mut04oRumPfx, Len.MUT04O_RUM_PFX);
	}

	/**Original name: MUT04O-RUM-PFX<br>*/
	public String getMut04oRumPfx() {
		return readString(Pos.MUT04O_RUM_PFX, Len.MUT04O_RUM_PFX);
	}

	public void setMut04oFpuCltId(String mut04oFpuCltId) {
		writeString(Pos.MUT04O_FPU_CLT_ID, mut04oFpuCltId, Len.MUT04O_FPU_CLT_ID);
	}

	/**Original name: MUT04O-FPU-CLT-ID<br>*/
	public String getMut04oFpuCltId() {
		return readString(Pos.MUT04O_FPU_CLT_ID, Len.MUT04O_FPU_CLT_ID);
	}

	public void setMut04oFpuDsyNm(String mut04oFpuDsyNm) {
		writeString(Pos.MUT04O_FPU_DSY_NM, mut04oFpuDsyNm, Len.MUT04O_FPU_DSY_NM);
	}

	/**Original name: MUT04O-FPU-DSY-NM<br>*/
	public String getMut04oFpuDsyNm() {
		return readString(Pos.MUT04O_FPU_DSY_NM, Len.MUT04O_FPU_DSY_NM);
	}

	public void setMut04oFpuFstNm(String mut04oFpuFstNm) {
		writeString(Pos.MUT04O_FPU_FST_NM, mut04oFpuFstNm, Len.MUT04O_FPU_FST_NM);
	}

	/**Original name: MUT04O-FPU-FST-NM<br>*/
	public String getMut04oFpuFstNm() {
		return readString(Pos.MUT04O_FPU_FST_NM, Len.MUT04O_FPU_FST_NM);
	}

	public void setMut04oFpuMdlNm(String mut04oFpuMdlNm) {
		writeString(Pos.MUT04O_FPU_MDL_NM, mut04oFpuMdlNm, Len.MUT04O_FPU_MDL_NM);
	}

	/**Original name: MUT04O-FPU-MDL-NM<br>*/
	public String getMut04oFpuMdlNm() {
		return readString(Pos.MUT04O_FPU_MDL_NM, Len.MUT04O_FPU_MDL_NM);
	}

	public void setMut04oFpuLstNm(String mut04oFpuLstNm) {
		writeString(Pos.MUT04O_FPU_LST_NM, mut04oFpuLstNm, Len.MUT04O_FPU_LST_NM);
	}

	/**Original name: MUT04O-FPU-LST-NM<br>*/
	public String getMut04oFpuLstNm() {
		return readString(Pos.MUT04O_FPU_LST_NM, Len.MUT04O_FPU_LST_NM);
	}

	public void setMut04oFpuSfx(String mut04oFpuSfx) {
		writeString(Pos.MUT04O_FPU_SFX, mut04oFpuSfx, Len.MUT04O_FPU_SFX);
	}

	/**Original name: MUT04O-FPU-SFX<br>*/
	public String getMut04oFpuSfx() {
		return readString(Pos.MUT04O_FPU_SFX, Len.MUT04O_FPU_SFX);
	}

	public void setMut04oFpuPfx(String mut04oFpuPfx) {
		writeString(Pos.MUT04O_FPU_PFX, mut04oFpuPfx, Len.MUT04O_FPU_PFX);
	}

	/**Original name: MUT04O-FPU-PFX<br>*/
	public String getMut04oFpuPfx() {
		return readString(Pos.MUT04O_FPU_PFX, Len.MUT04O_FPU_PFX);
	}

	public void setMut04oFpuDumCltId(String mut04oFpuDumCltId) {
		writeString(Pos.MUT04O_FPU_DUM_CLT_ID, mut04oFpuDumCltId, Len.MUT04O_FPU_DUM_CLT_ID);
	}

	/**Original name: MUT04O-FPU-DUM-CLT-ID<br>*/
	public String getMut04oFpuDumCltId() {
		return readString(Pos.MUT04O_FPU_DUM_CLT_ID, Len.MUT04O_FPU_DUM_CLT_ID);
	}

	public void setMut04oFpuDumDsyNm(String mut04oFpuDumDsyNm) {
		writeString(Pos.MUT04O_FPU_DUM_DSY_NM, mut04oFpuDumDsyNm, Len.MUT04O_FPU_DUM_DSY_NM);
	}

	/**Original name: MUT04O-FPU-DUM-DSY-NM<br>*/
	public String getMut04oFpuDumDsyNm() {
		return readString(Pos.MUT04O_FPU_DUM_DSY_NM, Len.MUT04O_FPU_DUM_DSY_NM);
	}

	public void setMut04oFpuDumTer(String mut04oFpuDumTer) {
		writeString(Pos.MUT04O_FPU_DUM_TER, mut04oFpuDumTer, Len.MUT04O_FPU_DUM_TER);
	}

	/**Original name: MUT04O-FPU-DUM-TER<br>*/
	public String getMut04oFpuDumTer() {
		return readString(Pos.MUT04O_FPU_DUM_TER, Len.MUT04O_FPU_DUM_TER);
	}

	public void setMut04oFpuDumFstNm(String mut04oFpuDumFstNm) {
		writeString(Pos.MUT04O_FPU_DUM_FST_NM, mut04oFpuDumFstNm, Len.MUT04O_FPU_DUM_FST_NM);
	}

	/**Original name: MUT04O-FPU-DUM-FST-NM<br>*/
	public String getMut04oFpuDumFstNm() {
		return readString(Pos.MUT04O_FPU_DUM_FST_NM, Len.MUT04O_FPU_DUM_FST_NM);
	}

	public void setMut04oFpuDumMdlNm(String mut04oFpuDumMdlNm) {
		writeString(Pos.MUT04O_FPU_DUM_MDL_NM, mut04oFpuDumMdlNm, Len.MUT04O_FPU_DUM_MDL_NM);
	}

	/**Original name: MUT04O-FPU-DUM-MDL-NM<br>*/
	public String getMut04oFpuDumMdlNm() {
		return readString(Pos.MUT04O_FPU_DUM_MDL_NM, Len.MUT04O_FPU_DUM_MDL_NM);
	}

	public void setMut04oFpuDumLstNm(String mut04oFpuDumLstNm) {
		writeString(Pos.MUT04O_FPU_DUM_LST_NM, mut04oFpuDumLstNm, Len.MUT04O_FPU_DUM_LST_NM);
	}

	/**Original name: MUT04O-FPU-DUM-LST-NM<br>*/
	public String getMut04oFpuDumLstNm() {
		return readString(Pos.MUT04O_FPU_DUM_LST_NM, Len.MUT04O_FPU_DUM_LST_NM);
	}

	public void setMut04oFpuDumSfx(String mut04oFpuDumSfx) {
		writeString(Pos.MUT04O_FPU_DUM_SFX, mut04oFpuDumSfx, Len.MUT04O_FPU_DUM_SFX);
	}

	/**Original name: MUT04O-FPU-DUM-SFX<br>*/
	public String getMut04oFpuDumSfx() {
		return readString(Pos.MUT04O_FPU_DUM_SFX, Len.MUT04O_FPU_DUM_SFX);
	}

	public void setMut04oFpuDumPfx(String mut04oFpuDumPfx) {
		writeString(Pos.MUT04O_FPU_DUM_PFX, mut04oFpuDumPfx, Len.MUT04O_FPU_DUM_PFX);
	}

	/**Original name: MUT04O-FPU-DUM-PFX<br>*/
	public String getMut04oFpuDumPfx() {
		return readString(Pos.MUT04O_FPU_DUM_PFX, Len.MUT04O_FPU_DUM_PFX);
	}

	public void setMut04oFpuRumCltId(String mut04oFpuRumCltId) {
		writeString(Pos.MUT04O_FPU_RUM_CLT_ID, mut04oFpuRumCltId, Len.MUT04O_FPU_RUM_CLT_ID);
	}

	/**Original name: MUT04O-FPU-RUM-CLT-ID<br>*/
	public String getMut04oFpuRumCltId() {
		return readString(Pos.MUT04O_FPU_RUM_CLT_ID, Len.MUT04O_FPU_RUM_CLT_ID);
	}

	public void setMut04oFpuRumDsyNm(String mut04oFpuRumDsyNm) {
		writeString(Pos.MUT04O_FPU_RUM_DSY_NM, mut04oFpuRumDsyNm, Len.MUT04O_FPU_RUM_DSY_NM);
	}

	/**Original name: MUT04O-FPU-RUM-DSY-NM<br>*/
	public String getMut04oFpuRumDsyNm() {
		return readString(Pos.MUT04O_FPU_RUM_DSY_NM, Len.MUT04O_FPU_RUM_DSY_NM);
	}

	public void setMut04oFpuRumTer(String mut04oFpuRumTer) {
		writeString(Pos.MUT04O_FPU_RUM_TER, mut04oFpuRumTer, Len.MUT04O_FPU_RUM_TER);
	}

	/**Original name: MUT04O-FPU-RUM-TER<br>*/
	public String getMut04oFpuRumTer() {
		return readString(Pos.MUT04O_FPU_RUM_TER, Len.MUT04O_FPU_RUM_TER);
	}

	public void setMut04oFpuRumFstNm(String mut04oFpuRumFstNm) {
		writeString(Pos.MUT04O_FPU_RUM_FST_NM, mut04oFpuRumFstNm, Len.MUT04O_FPU_RUM_FST_NM);
	}

	/**Original name: MUT04O-FPU-RUM-FST-NM<br>*/
	public String getMut04oFpuRumFstNm() {
		return readString(Pos.MUT04O_FPU_RUM_FST_NM, Len.MUT04O_FPU_RUM_FST_NM);
	}

	public void setMut04oFpuRumMdlNm(String mut04oFpuRumMdlNm) {
		writeString(Pos.MUT04O_FPU_RUM_MDL_NM, mut04oFpuRumMdlNm, Len.MUT04O_FPU_RUM_MDL_NM);
	}

	/**Original name: MUT04O-FPU-RUM-MDL-NM<br>*/
	public String getMut04oFpuRumMdlNm() {
		return readString(Pos.MUT04O_FPU_RUM_MDL_NM, Len.MUT04O_FPU_RUM_MDL_NM);
	}

	public void setMut04oFpuRumLstNm(String mut04oFpuRumLstNm) {
		writeString(Pos.MUT04O_FPU_RUM_LST_NM, mut04oFpuRumLstNm, Len.MUT04O_FPU_RUM_LST_NM);
	}

	/**Original name: MUT04O-FPU-RUM-LST-NM<br>*/
	public String getMut04oFpuRumLstNm() {
		return readString(Pos.MUT04O_FPU_RUM_LST_NM, Len.MUT04O_FPU_RUM_LST_NM);
	}

	public void setMut04oFpuRumSfx(String mut04oFpuRumSfx) {
		writeString(Pos.MUT04O_FPU_RUM_SFX, mut04oFpuRumSfx, Len.MUT04O_FPU_RUM_SFX);
	}

	/**Original name: MUT04O-FPU-RUM-SFX<br>*/
	public String getMut04oFpuRumSfx() {
		return readString(Pos.MUT04O_FPU_RUM_SFX, Len.MUT04O_FPU_RUM_SFX);
	}

	public void setMut04oFpuRumPfx(String mut04oFpuRumPfx) {
		writeString(Pos.MUT04O_FPU_RUM_PFX, mut04oFpuRumPfx, Len.MUT04O_FPU_RUM_PFX);
	}

	/**Original name: MUT04O-FPU-RUM-PFX<br>*/
	public String getMut04oFpuRumPfx() {
		return readString(Pos.MUT04O_FPU_RUM_PFX, Len.MUT04O_FPU_RUM_PFX);
	}

	public void setMut04oLpRskAlsCltId(String mut04oLpRskAlsCltId) {
		writeString(Pos.MUT04O_LP_RSK_ALS_CLT_ID, mut04oLpRskAlsCltId, Len.MUT04O_LP_RSK_ALS_CLT_ID);
	}

	/**Original name: MUT04O-LP-RSK-ALS-CLT-ID<br>*/
	public String getMut04oLpRskAlsCltId() {
		return readString(Pos.MUT04O_LP_RSK_ALS_CLT_ID, Len.MUT04O_LP_RSK_ALS_CLT_ID);
	}

	public void setMut04oLpRskAlsDsyNm(String mut04oLpRskAlsDsyNm) {
		writeString(Pos.MUT04O_LP_RSK_ALS_DSY_NM, mut04oLpRskAlsDsyNm, Len.MUT04O_LP_RSK_ALS_DSY_NM);
	}

	/**Original name: MUT04O-LP-RSK-ALS-DSY-NM<br>*/
	public String getMut04oLpRskAlsDsyNm() {
		return readString(Pos.MUT04O_LP_RSK_ALS_DSY_NM, Len.MUT04O_LP_RSK_ALS_DSY_NM);
	}

	public void setMut04oLpRskAlsFstNm(String mut04oLpRskAlsFstNm) {
		writeString(Pos.MUT04O_LP_RSK_ALS_FST_NM, mut04oLpRskAlsFstNm, Len.MUT04O_LP_RSK_ALS_FST_NM);
	}

	/**Original name: MUT04O-LP-RSK-ALS-FST-NM<br>*/
	public String getMut04oLpRskAlsFstNm() {
		return readString(Pos.MUT04O_LP_RSK_ALS_FST_NM, Len.MUT04O_LP_RSK_ALS_FST_NM);
	}

	public void setMut04oLpRskAlsMdlNm(String mut04oLpRskAlsMdlNm) {
		writeString(Pos.MUT04O_LP_RSK_ALS_MDL_NM, mut04oLpRskAlsMdlNm, Len.MUT04O_LP_RSK_ALS_MDL_NM);
	}

	/**Original name: MUT04O-LP-RSK-ALS-MDL-NM<br>*/
	public String getMut04oLpRskAlsMdlNm() {
		return readString(Pos.MUT04O_LP_RSK_ALS_MDL_NM, Len.MUT04O_LP_RSK_ALS_MDL_NM);
	}

	public void setMut04oLpRskAlsLstNm(String mut04oLpRskAlsLstNm) {
		writeString(Pos.MUT04O_LP_RSK_ALS_LST_NM, mut04oLpRskAlsLstNm, Len.MUT04O_LP_RSK_ALS_LST_NM);
	}

	/**Original name: MUT04O-LP-RSK-ALS-LST-NM<br>*/
	public String getMut04oLpRskAlsLstNm() {
		return readString(Pos.MUT04O_LP_RSK_ALS_LST_NM, Len.MUT04O_LP_RSK_ALS_LST_NM);
	}

	public void setMut04oLpRskAlsSfx(String mut04oLpRskAlsSfx) {
		writeString(Pos.MUT04O_LP_RSK_ALS_SFX, mut04oLpRskAlsSfx, Len.MUT04O_LP_RSK_ALS_SFX);
	}

	/**Original name: MUT04O-LP-RSK-ALS-SFX<br>*/
	public String getMut04oLpRskAlsSfx() {
		return readString(Pos.MUT04O_LP_RSK_ALS_SFX, Len.MUT04O_LP_RSK_ALS_SFX);
	}

	public void setMut04oLpRskAlsPfx(String mut04oLpRskAlsPfx) {
		writeString(Pos.MUT04O_LP_RSK_ALS_PFX, mut04oLpRskAlsPfx, Len.MUT04O_LP_RSK_ALS_PFX);
	}

	/**Original name: MUT04O-LP-RSK-ALS-PFX<br>*/
	public String getMut04oLpRskAlsPfx() {
		return readString(Pos.MUT04O_LP_RSK_ALS_PFX, Len.MUT04O_LP_RSK_ALS_PFX);
	}

	public void setMut04oLpraDumCltId(String mut04oLpraDumCltId) {
		writeString(Pos.MUT04O_LPRA_DUM_CLT_ID, mut04oLpraDumCltId, Len.MUT04O_LPRA_DUM_CLT_ID);
	}

	/**Original name: MUT04O-LPRA-DUM-CLT-ID<br>*/
	public String getMut04oLpraDumCltId() {
		return readString(Pos.MUT04O_LPRA_DUM_CLT_ID, Len.MUT04O_LPRA_DUM_CLT_ID);
	}

	public void setMut04oLpraDumDsyNm(String mut04oLpraDumDsyNm) {
		writeString(Pos.MUT04O_LPRA_DUM_DSY_NM, mut04oLpraDumDsyNm, Len.MUT04O_LPRA_DUM_DSY_NM);
	}

	/**Original name: MUT04O-LPRA-DUM-DSY-NM<br>*/
	public String getMut04oLpraDumDsyNm() {
		return readString(Pos.MUT04O_LPRA_DUM_DSY_NM, Len.MUT04O_LPRA_DUM_DSY_NM);
	}

	public void setMut04oLpraDumTer(String mut04oLpraDumTer) {
		writeString(Pos.MUT04O_LPRA_DUM_TER, mut04oLpraDumTer, Len.MUT04O_LPRA_DUM_TER);
	}

	/**Original name: MUT04O-LPRA-DUM-TER<br>*/
	public String getMut04oLpraDumTer() {
		return readString(Pos.MUT04O_LPRA_DUM_TER, Len.MUT04O_LPRA_DUM_TER);
	}

	public void setMut04oLpraDumFstNm(String mut04oLpraDumFstNm) {
		writeString(Pos.MUT04O_LPRA_DUM_FST_NM, mut04oLpraDumFstNm, Len.MUT04O_LPRA_DUM_FST_NM);
	}

	/**Original name: MUT04O-LPRA-DUM-FST-NM<br>*/
	public String getMut04oLpraDumFstNm() {
		return readString(Pos.MUT04O_LPRA_DUM_FST_NM, Len.MUT04O_LPRA_DUM_FST_NM);
	}

	public void setMut04oLpraDumMdlNm(String mut04oLpraDumMdlNm) {
		writeString(Pos.MUT04O_LPRA_DUM_MDL_NM, mut04oLpraDumMdlNm, Len.MUT04O_LPRA_DUM_MDL_NM);
	}

	/**Original name: MUT04O-LPRA-DUM-MDL-NM<br>*/
	public String getMut04oLpraDumMdlNm() {
		return readString(Pos.MUT04O_LPRA_DUM_MDL_NM, Len.MUT04O_LPRA_DUM_MDL_NM);
	}

	public void setMut04oLpraDumLstNm(String mut04oLpraDumLstNm) {
		writeString(Pos.MUT04O_LPRA_DUM_LST_NM, mut04oLpraDumLstNm, Len.MUT04O_LPRA_DUM_LST_NM);
	}

	/**Original name: MUT04O-LPRA-DUM-LST-NM<br>*/
	public String getMut04oLpraDumLstNm() {
		return readString(Pos.MUT04O_LPRA_DUM_LST_NM, Len.MUT04O_LPRA_DUM_LST_NM);
	}

	public void setMut04oLpraDumSfx(String mut04oLpraDumSfx) {
		writeString(Pos.MUT04O_LPRA_DUM_SFX, mut04oLpraDumSfx, Len.MUT04O_LPRA_DUM_SFX);
	}

	/**Original name: MUT04O-LPRA-DUM-SFX<br>*/
	public String getMut04oLpraDumSfx() {
		return readString(Pos.MUT04O_LPRA_DUM_SFX, Len.MUT04O_LPRA_DUM_SFX);
	}

	public void setMut04oLpraDumPfx(String mut04oLpraDumPfx) {
		writeString(Pos.MUT04O_LPRA_DUM_PFX, mut04oLpraDumPfx, Len.MUT04O_LPRA_DUM_PFX);
	}

	/**Original name: MUT04O-LPRA-DUM-PFX<br>*/
	public String getMut04oLpraDumPfx() {
		return readString(Pos.MUT04O_LPRA_DUM_PFX, Len.MUT04O_LPRA_DUM_PFX);
	}

	public void setMut04oLpraRumCltId(String mut04oLpraRumCltId) {
		writeString(Pos.MUT04O_LPRA_RUM_CLT_ID, mut04oLpraRumCltId, Len.MUT04O_LPRA_RUM_CLT_ID);
	}

	/**Original name: MUT04O-LPRA-RUM-CLT-ID<br>*/
	public String getMut04oLpraRumCltId() {
		return readString(Pos.MUT04O_LPRA_RUM_CLT_ID, Len.MUT04O_LPRA_RUM_CLT_ID);
	}

	public void setMut04oLpraRumDsyNm(String mut04oLpraRumDsyNm) {
		writeString(Pos.MUT04O_LPRA_RUM_DSY_NM, mut04oLpraRumDsyNm, Len.MUT04O_LPRA_RUM_DSY_NM);
	}

	/**Original name: MUT04O-LPRA-RUM-DSY-NM<br>*/
	public String getMut04oLpraRumDsyNm() {
		return readString(Pos.MUT04O_LPRA_RUM_DSY_NM, Len.MUT04O_LPRA_RUM_DSY_NM);
	}

	public void setMut04oLpraRumTer(String mut04oLpraRumTer) {
		writeString(Pos.MUT04O_LPRA_RUM_TER, mut04oLpraRumTer, Len.MUT04O_LPRA_RUM_TER);
	}

	/**Original name: MUT04O-LPRA-RUM-TER<br>*/
	public String getMut04oLpraRumTer() {
		return readString(Pos.MUT04O_LPRA_RUM_TER, Len.MUT04O_LPRA_RUM_TER);
	}

	public void setMut04oLpraRumFstNm(String mut04oLpraRumFstNm) {
		writeString(Pos.MUT04O_LPRA_RUM_FST_NM, mut04oLpraRumFstNm, Len.MUT04O_LPRA_RUM_FST_NM);
	}

	/**Original name: MUT04O-LPRA-RUM-FST-NM<br>*/
	public String getMut04oLpraRumFstNm() {
		return readString(Pos.MUT04O_LPRA_RUM_FST_NM, Len.MUT04O_LPRA_RUM_FST_NM);
	}

	public void setMut04oLpraRumMdlNm(String mut04oLpraRumMdlNm) {
		writeString(Pos.MUT04O_LPRA_RUM_MDL_NM, mut04oLpraRumMdlNm, Len.MUT04O_LPRA_RUM_MDL_NM);
	}

	/**Original name: MUT04O-LPRA-RUM-MDL-NM<br>*/
	public String getMut04oLpraRumMdlNm() {
		return readString(Pos.MUT04O_LPRA_RUM_MDL_NM, Len.MUT04O_LPRA_RUM_MDL_NM);
	}

	public void setMut04oLpraRumLstNm(String mut04oLpraRumLstNm) {
		writeString(Pos.MUT04O_LPRA_RUM_LST_NM, mut04oLpraRumLstNm, Len.MUT04O_LPRA_RUM_LST_NM);
	}

	/**Original name: MUT04O-LPRA-RUM-LST-NM<br>*/
	public String getMut04oLpraRumLstNm() {
		return readString(Pos.MUT04O_LPRA_RUM_LST_NM, Len.MUT04O_LPRA_RUM_LST_NM);
	}

	public void setMut04oLpraRumSfx(String mut04oLpraRumSfx) {
		writeString(Pos.MUT04O_LPRA_RUM_SFX, mut04oLpraRumSfx, Len.MUT04O_LPRA_RUM_SFX);
	}

	/**Original name: MUT04O-LPRA-RUM-SFX<br>*/
	public String getMut04oLpraRumSfx() {
		return readString(Pos.MUT04O_LPRA_RUM_SFX, Len.MUT04O_LPRA_RUM_SFX);
	}

	public void setMut04oLpraRumPfx(String mut04oLpraRumPfx) {
		writeString(Pos.MUT04O_LPRA_RUM_PFX, mut04oLpraRumPfx, Len.MUT04O_LPRA_RUM_PFX);
	}

	/**Original name: MUT04O-LPRA-RUM-PFX<br>*/
	public String getMut04oLpraRumPfx() {
		return readString(Pos.MUT04O_LPRA_RUM_PFX, Len.MUT04O_LPRA_RUM_PFX);
	}

	public void setMut04oAlStateCd(String mut04oAlStateCd) {
		writeString(Pos.MUT04O_AL_STATE_CD, mut04oAlStateCd, Len.MUT04O_AL_STATE_CD);
	}

	/**Original name: MUT04O-AL-STATE-CD<br>*/
	public String getMut04oAlStateCd() {
		return readString(Pos.MUT04O_AL_STATE_CD, Len.MUT04O_AL_STATE_CD);
	}

	public void setMut04oAlCountyCd(String mut04oAlCountyCd) {
		writeString(Pos.MUT04O_AL_COUNTY_CD, mut04oAlCountyCd, Len.MUT04O_AL_COUNTY_CD);
	}

	/**Original name: MUT04O-AL-COUNTY-CD<br>*/
	public String getMut04oAlCountyCd() {
		return readString(Pos.MUT04O_AL_COUNTY_CD, Len.MUT04O_AL_COUNTY_CD);
	}

	public void setMut04oAlTownCd(String mut04oAlTownCd) {
		writeString(Pos.MUT04O_AL_TOWN_CD, mut04oAlTownCd, Len.MUT04O_AL_TOWN_CD);
	}

	/**Original name: MUT04O-AL-TOWN-CD<br>*/
	public String getMut04oAlTownCd() {
		return readString(Pos.MUT04O_AL_TOWN_CD, Len.MUT04O_AL_TOWN_CD);
	}

	public void setMut04oNbrEmployees(int mut04oNbrEmployees) {
		writeInt(Pos.MUT04O_NBR_EMPLOYEES, mut04oNbrEmployees, Len.Int.MUT04O_NBR_EMPLOYEES);
	}

	public void setMut04oNbrEmployeesFormatted(String mut04oNbrEmployees) {
		writeString(Pos.MUT04O_NBR_EMPLOYEES, Trunc.toUnsignedNumeric(mut04oNbrEmployees, Len.MUT04O_NBR_EMPLOYEES), Len.MUT04O_NBR_EMPLOYEES);
	}

	/**Original name: MUT04O-NBR-EMPLOYEES<br>*/
	public int getMut04oNbrEmployees() {
		return readNumDispInt(Pos.MUT04O_NBR_EMPLOYEES, Len.MUT04O_NBR_EMPLOYEES);
	}

	public void setMut04oSeSicCd(int mut04oSeSicCdIdx, String mut04oSeSicCd) {
		int position = Pos.mut04oSeSicCd(mut04oSeSicCdIdx - 1);
		writeString(position, mut04oSeSicCd, Len.MUT04O_SE_SIC_CD);
	}

	/**Original name: MUT04O-SE-SIC-CD<br>*/
	public String getMut04oSeSicCd(int mut04oSeSicCdIdx) {
		int position = Pos.mut04oSeSicCd(mut04oSeSicCdIdx - 1);
		return readString(position, Len.MUT04O_SE_SIC_CD);
	}

	public void setMut04oSeSicDes(int mut04oSeSicDesIdx, String mut04oSeSicDes) {
		int position = Pos.mut04oSeSicDes(mut04oSeSicDesIdx - 1);
		writeString(position, mut04oSeSicDes, Len.MUT04O_SE_SIC_DES);
	}

	/**Original name: MUT04O-SE-SIC-DES<br>*/
	public String getMut04oSeSicDes(int mut04oSeSicDesIdx) {
		int position = Pos.mut04oSeSicDes(mut04oSeSicDesIdx - 1);
		return readString(position, Len.MUT04O_SE_SIC_DES);
	}

	public void setMut04oOfcLocCdFormatted(String data) {
		writeString(Pos.MUT04O_OFC_LOC_CD, data, Len.MUT04O_OFC_LOC_CD);
	}

	public void setMut04oOlDivision(char mut04oOlDivision) {
		writeChar(Pos.MUT04O_OL_DIVISION, mut04oOlDivision);
	}

	/**Original name: MUT04O-OL-DIVISION<br>*/
	public char getMut04oOlDivision() {
		return readChar(Pos.MUT04O_OL_DIVISION);
	}

	public void setMut04oOlSubDivision(char mut04oOlSubDivision) {
		writeChar(Pos.MUT04O_OL_SUB_DIVISION, mut04oOlSubDivision);
	}

	/**Original name: MUT04O-OL-SUB-DIVISION<br>*/
	public char getMut04oOlSubDivision() {
		return readChar(Pos.MUT04O_OL_SUB_DIVISION);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int MUT004_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int MUT04I_TECHNICAL_KEYS = MUT004_SERVICE_INPUTS;
		public static final int MUT04I_TK_CLT_ID = MUT04I_TECHNICAL_KEYS;
		public static final int MUT04I_TK_CIOR_SHW_OBJ_KEY = MUT04I_TK_CLT_ID + Len.MUT04I_TK_CLT_ID;
		public static final int MUT04I_TK_ADR_SEQ_NBR = MUT04I_TK_CIOR_SHW_OBJ_KEY + Len.MUT04I_TK_CIOR_SHW_OBJ_KEY;
		public static final int MUT04I_USR_ID = MUT04I_TK_ADR_SEQ_NBR + Len.MUT04I_TK_ADR_SEQ_NBR;
		public static final int MUT04I_ACT_NBR = MUT04I_USR_ID + Len.MUT04I_USR_ID;
		public static final int MUT04I_TER_NBR = MUT04I_ACT_NBR + Len.MUT04I_ACT_NBR;
		public static final int MUT04I_AS_OF_DT = MUT04I_TER_NBR + Len.MUT04I_TER_NBR;
		public static final int MUT04I_FEIN = MUT04I_AS_OF_DT + Len.MUT04I_AS_OF_DT;
		public static final int MUT04I_SSN = MUT04I_FEIN + Len.MUT04I_FEIN;
		public static final int MUT04I_PHN_ACD = MUT04I_SSN + Len.MUT04I_SSN;
		public static final int MUT04I_PHN_PFX_NBR = MUT04I_PHN_ACD + Len.MUT04I_PHN_ACD;
		public static final int MUT04I_PHN_LIN_NBR = MUT04I_PHN_PFX_NBR + Len.MUT04I_PHN_PFX_NBR;
		public static final int FLR1 = MUT04I_PHN_LIN_NBR + Len.MUT04I_PHN_LIN_NBR;
		public static final int MUT004_SERVICE_OUTPUTS = FLR1 + Len.FLR1;
		public static final int MUT04O_TECHNICAL_KEYS = MUT004_SERVICE_OUTPUTS;
		public static final int MUT04O_TK_CLT_ID = MUT04O_TECHNICAL_KEYS;
		public static final int FLR2 = MUT04O_TK_CLT_ID + Len.MUT04O_TK_CLT_ID;
		public static final int MUT04O_CLIENT_NAME = FLR2 + Len.FLR2;
		public static final int MUT04O_CN_IDV_NM_IND = MUT04O_CLIENT_NAME;
		public static final int MUT04O_CN_DSY_NM = MUT04O_CN_IDV_NM_IND + Len.MUT04O_CN_IDV_NM_IND;
		public static final int MUT04O_CN_SR_NM = MUT04O_CN_DSY_NM + Len.MUT04O_CN_DSY_NM;
		public static final int MUT04O_CN_FST_NM = MUT04O_CN_SR_NM + Len.MUT04O_CN_SR_NM;
		public static final int MUT04O_CN_MDL_NM = MUT04O_CN_FST_NM + Len.MUT04O_CN_FST_NM;
		public static final int MUT04O_CN_LST_NM = MUT04O_CN_MDL_NM + Len.MUT04O_CN_MDL_NM;
		public static final int MUT04O_CN_SFX = MUT04O_CN_LST_NM + Len.MUT04O_CN_LST_NM;
		public static final int MUT04O_CN_PFX = MUT04O_CN_SFX + Len.MUT04O_CN_SFX;
		public static final int FLR3 = MUT04O_CN_PFX + Len.MUT04O_CN_PFX;
		public static final int MUT04O_COWN_INFO = FLR3 + Len.FLR3;
		public static final int MUT04O_CI_CLIENT_ID = MUT04O_COWN_INFO;
		public static final int MUT04O_CI_IDV_NM_IND = MUT04O_CI_CLIENT_ID + Len.MUT04O_CI_CLIENT_ID;
		public static final int MUT04O_CI_DSY_NM = MUT04O_CI_IDV_NM_IND + Len.MUT04O_CI_IDV_NM_IND;
		public static final int MUT04O_CI_SR_NM = MUT04O_CI_DSY_NM + Len.MUT04O_CI_DSY_NM;
		public static final int MUT04O_CI_FST_NM = MUT04O_CI_SR_NM + Len.MUT04O_CI_SR_NM;
		public static final int MUT04O_CI_MDL_NM = MUT04O_CI_FST_NM + Len.MUT04O_CI_FST_NM;
		public static final int MUT04O_CI_LST_NM = MUT04O_CI_MDL_NM + Len.MUT04O_CI_MDL_NM;
		public static final int MUT04O_CI_SFX = MUT04O_CI_LST_NM + Len.MUT04O_CI_LST_NM;
		public static final int MUT04O_CI_PFX = MUT04O_CI_SFX + Len.MUT04O_CI_SFX;
		public static final int FLR4 = MUT04O_CI_PFX + Len.MUT04O_CI_PFX;
		public static final int MUT04O_CLIENT_ADDRESS = FLR4 + Len.FLR3;
		public static final int MUT04O_BSM_ADR = MUT04O_CLIENT_ADDRESS;
		public static final int MUT04O_BSM_TECHNICAL_KEYS = MUT04O_BSM_ADR;
		public static final int MUT04O_BSM_ADR_SEQ_NBR = MUT04O_BSM_TECHNICAL_KEYS;
		public static final int MUT04O_BSM_ADR_ID = MUT04O_BSM_ADR_SEQ_NBR + Len.MUT04O_BSM_ADR_SEQ_NBR;
		public static final int MUT04O_BSM_LIN1_ADR = MUT04O_BSM_ADR_ID + Len.MUT04O_BSM_ADR_ID;
		public static final int MUT04O_BSM_LIN2_ADR = MUT04O_BSM_LIN1_ADR + Len.MUT04O_BSM_LIN1_ADR;
		public static final int MUT04O_BSM_CIT = MUT04O_BSM_LIN2_ADR + Len.MUT04O_BSM_LIN2_ADR;
		public static final int MUT04O_BSM_ST_ABB = MUT04O_BSM_CIT + Len.MUT04O_BSM_CIT;
		public static final int MUT04O_BSM_PST_CD = MUT04O_BSM_ST_ABB + Len.MUT04O_BSM_ST_ABB;
		public static final int MUT04O_BSM_CTY = MUT04O_BSM_PST_CD + Len.MUT04O_BSM_PST_CD;
		public static final int MUT04O_BSM_CTR_CD = MUT04O_BSM_CTY + Len.MUT04O_BSM_CTY;
		public static final int FLR5 = MUT04O_BSM_CTR_CD + Len.MUT04O_BSM_CTR_CD;
		public static final int MUT04O_BSL_ADR = FLR5 + Len.FLR3;
		public static final int MUT04O_BSL_TECHNICAL_KEYS = MUT04O_BSL_ADR;
		public static final int MUT04O_BSL_ADR_SEQ_NBR = MUT04O_BSL_TECHNICAL_KEYS;
		public static final int MUT04O_BSL_ADR_ID = MUT04O_BSL_ADR_SEQ_NBR + Len.MUT04O_BSL_ADR_SEQ_NBR;
		public static final int MUT04O_BSL_LIN1_ADR = MUT04O_BSL_ADR_ID + Len.MUT04O_BSL_ADR_ID;
		public static final int MUT04O_BSL_LIN2_ADR = MUT04O_BSL_LIN1_ADR + Len.MUT04O_BSL_LIN1_ADR;
		public static final int MUT04O_BSL_CIT = MUT04O_BSL_LIN2_ADR + Len.MUT04O_BSL_LIN2_ADR;
		public static final int MUT04O_BSL_ST_ABB = MUT04O_BSL_CIT + Len.MUT04O_BSL_CIT;
		public static final int MUT04O_BSL_PST_CD = MUT04O_BSL_ST_ABB + Len.MUT04O_BSL_ST_ABB;
		public static final int MUT04O_BSL_CTY = MUT04O_BSL_PST_CD + Len.MUT04O_BSL_PST_CD;
		public static final int MUT04O_BSL_CTR_CD = MUT04O_BSL_CTY + Len.MUT04O_BSL_CTY;
		public static final int FLR6 = MUT04O_BSL_CTR_CD + Len.MUT04O_BSL_CTR_CD;
		public static final int MUT04O_CLIENT_PHONE = FLR6 + Len.FLR3;
		public static final int MUT04O_BUSINESS_PHONE = MUT04O_CLIENT_PHONE;
		public static final int MUT04O_BUS_PHN_ACD = MUT04O_BUSINESS_PHONE;
		public static final int MUT04O_BUS_PHN_PFX_NBR = MUT04O_BUS_PHN_ACD + Len.MUT04O_BUS_PHN_ACD;
		public static final int MUT04O_BUS_PHN_LIN_NBR = MUT04O_BUS_PHN_PFX_NBR + Len.MUT04O_BUS_PHN_PFX_NBR;
		public static final int MUT04O_BUS_PHN_EXT_NBR = MUT04O_BUS_PHN_LIN_NBR + Len.MUT04O_BUS_PHN_LIN_NBR;
		public static final int MUT04O_BUS_PHN_NBR_FMT = MUT04O_BUS_PHN_EXT_NBR + Len.MUT04O_BUS_PHN_EXT_NBR;
		public static final int MUT04O_FAX_NUMBER = MUT04O_BUS_PHN_NBR_FMT + Len.MUT04O_BUS_PHN_NBR_FMT;
		public static final int MUT04O_FAX_PHN_ACD = MUT04O_FAX_NUMBER;
		public static final int MUT04O_FAX_PHN_PFX_NBR = MUT04O_FAX_PHN_ACD + Len.MUT04O_FAX_PHN_ACD;
		public static final int MUT04O_FAX_PHN_LIN_NBR = MUT04O_FAX_PHN_PFX_NBR + Len.MUT04O_FAX_PHN_PFX_NBR;
		public static final int MUT04O_FAX_PHN_EXT_NBR = MUT04O_FAX_PHN_LIN_NBR + Len.MUT04O_FAX_PHN_LIN_NBR;
		public static final int MUT04O_FAX_PHN_NBR_FMT = MUT04O_FAX_PHN_EXT_NBR + Len.MUT04O_FAX_PHN_EXT_NBR;
		public static final int MUT04O_CONTACT_NUMBER = MUT04O_FAX_PHN_NBR_FMT + Len.MUT04O_FAX_PHN_NBR_FMT;
		public static final int MUT04O_CTC_PHN_ACD = MUT04O_CONTACT_NUMBER;
		public static final int MUT04O_CTC_PHN_PFX_NBR = MUT04O_CTC_PHN_ACD + Len.MUT04O_CTC_PHN_ACD;
		public static final int MUT04O_CTC_PHN_LIN_NBR = MUT04O_CTC_PHN_PFX_NBR + Len.MUT04O_CTC_PHN_PFX_NBR;
		public static final int MUT04O_CTC_PHN_EXT_NBR = MUT04O_CTC_PHN_LIN_NBR + Len.MUT04O_CTC_PHN_LIN_NBR;
		public static final int MUT04O_CTC_PHN_NBR_FMT = MUT04O_CTC_PHN_EXT_NBR + Len.MUT04O_CTC_PHN_EXT_NBR;
		public static final int MUT04O_CLIENT_TAX = MUT04O_CTC_PHN_NBR_FMT + Len.MUT04O_CTC_PHN_NBR_FMT;
		public static final int MUT04O_FEIN = MUT04O_CLIENT_TAX;
		public static final int MUT04O_SSN = MUT04O_FEIN + Len.MUT04O_FEIN;
		public static final int MUT04O_FEIN_FMT = MUT04O_SSN + Len.MUT04O_SSN;
		public static final int MUT04O_SSN_FMT = MUT04O_FEIN_FMT + Len.MUT04O_FEIN_FMT;
		public static final int MUT04O_LEGAL_ENTITY = MUT04O_SSN_FMT + Len.MUT04O_SSN_FMT;
		public static final int MUT04O_LEG_ETY_CD = MUT04O_LEGAL_ENTITY;
		public static final int MUT04O_LEG_ETY_DES = MUT04O_LEG_ETY_CD + Len.MUT04O_LEG_ETY_CD;
		public static final int MUT04O_TYPE_OF_BUSINESS = MUT04O_LEG_ETY_DES + Len.MUT04O_LEG_ETY_DES;
		public static final int MUT04O_TOB_CD = MUT04O_TYPE_OF_BUSINESS;
		public static final int MUT04O_TOB_DES = MUT04O_TOB_CD + Len.MUT04O_TOB_CD;
		public static final int MUT04O_SIC = MUT04O_TOB_DES + Len.MUT04O_TOB_DES;
		public static final int MUT04O_SIC_CD = MUT04O_SIC;
		public static final int MUT04O_SIC_DES = MUT04O_SIC_CD + Len.MUT04O_SIC_CD;
		public static final int MUT04O_SEGMENT_INFORMATION = MUT04O_SIC_DES + Len.MUT04O_SIC_DES;
		public static final int MUT04O_SEG_CD = MUT04O_SEGMENT_INFORMATION;
		public static final int MUT04O_SEG_DES = MUT04O_SEG_CD + Len.MUT04O_SEG_CD;
		public static final int MUT04O_CLIENT_ACCOUNT = MUT04O_SEG_DES + Len.MUT04O_SEG_DES;
		public static final int MUT04O_PROPERTY_CASUALTY = MUT04O_CLIENT_ACCOUNT;
		public static final int MUT04O_PC_PSPT_ACT_IND = MUT04O_PROPERTY_CASUALTY;
		public static final int MUT04O_PC_ACT_NBR = MUT04O_PC_PSPT_ACT_IND + Len.MUT04O_PC_PSPT_ACT_IND;
		public static final int MUT04O_PC_ACT_NBR2 = MUT04O_PC_ACT_NBR + Len.MUT04O_PC_ACT_NBR;
		public static final int MUT04O_PC_ACT_NBR_FMT = MUT04O_PC_ACT_NBR2 + Len.MUT04O_PC_ACT_NBR2;
		public static final int MUT04O_PC_ACT_NBR2_FMT = MUT04O_PC_ACT_NBR_FMT + Len.MUT04O_PC_ACT_NBR_FMT;
		public static final int MUT04O_PSPT_ACT_NBR = MUT04O_PC_ACT_NBR2_FMT + Len.MUT04O_PC_ACT_NBR2_FMT;
		public static final int MUT04O_PSPT_ACT_NBR_FMT = MUT04O_PSPT_ACT_NBR + Len.MUT04O_PSPT_ACT_NBR;
		public static final int MUT04O_ACT_NM_ID = MUT04O_PSPT_ACT_NBR_FMT + Len.MUT04O_PSPT_ACT_NBR_FMT;
		public static final int MUT04O_ACT2_NM_ID = MUT04O_ACT_NM_ID + Len.MUT04O_ACT_NM_ID;
		public static final int MUT04O_OBJ_CD = MUT04O_ACT2_NM_ID + Len.MUT04O_ACT2_NM_ID;
		public static final int MUT04O_OBJ_CD2 = MUT04O_OBJ_CD + Len.MUT04O_OBJ_CD;
		public static final int MUT04O_OBJ_DES = MUT04O_OBJ_CD2 + Len.MUT04O_OBJ_CD2;
		public static final int MUT04O_OBJ_DES2 = MUT04O_OBJ_DES + Len.MUT04O_OBJ_DES;
		public static final int MUT04O_SIN_WC_ACT_NBR = MUT04O_OBJ_DES2 + Len.MUT04O_OBJ_DES2;
		public static final int MUT04O_SIN_WC_ACT_NBR_FMT = MUT04O_SIN_WC_ACT_NBR + Len.MUT04O_SIN_WC_ACT_NBR;
		public static final int MUT04O_SIN_WC_ACT_NM_ID = MUT04O_SIN_WC_ACT_NBR_FMT + Len.MUT04O_SIN_WC_ACT_NBR_FMT;
		public static final int MUT04O_SIN_WC_OBJ_CD = MUT04O_SIN_WC_ACT_NM_ID + Len.MUT04O_SIN_WC_ACT_NM_ID;
		public static final int MUT04O_SIN_WC_OBJ_DES = MUT04O_SIN_WC_OBJ_CD + Len.MUT04O_SIN_WC_OBJ_CD;
		public static final int MUT04O_SPL_BILL_ACT_NBR = MUT04O_SIN_WC_OBJ_DES + Len.MUT04O_SIN_WC_OBJ_DES;
		public static final int MUT04O_SPL_BILL_ACT_NBR_FMT = MUT04O_SPL_BILL_ACT_NBR + Len.MUT04O_SPL_BILL_ACT_NBR;
		public static final int MUT04O_SPL_BILL_ACT_NM_ID = MUT04O_SPL_BILL_ACT_NBR_FMT + Len.MUT04O_SPL_BILL_ACT_NBR_FMT;
		public static final int MUT04O_SPL_BILL_OBJ_CD = MUT04O_SPL_BILL_ACT_NM_ID + Len.MUT04O_SPL_BILL_ACT_NM_ID;
		public static final int MUT04O_SPL_BILL_OBJ_DES = MUT04O_SPL_BILL_OBJ_CD + Len.MUT04O_SPL_BILL_OBJ_CD;
		public static final int MUT04O_PRS_LIN_ACT_NBR = MUT04O_SPL_BILL_OBJ_DES + Len.MUT04O_SPL_BILL_OBJ_DES;
		public static final int MUT04O_PRS_LIN_ACT_NBR_FMT = MUT04O_PRS_LIN_ACT_NBR + Len.MUT04O_PRS_LIN_ACT_NBR;
		public static final int MUT04O_PRS_LIN_ACT_NM_ID = MUT04O_PRS_LIN_ACT_NBR_FMT + Len.MUT04O_PRS_LIN_ACT_NBR_FMT;
		public static final int MUT04O_PRS_LIN_OBJ_CD = MUT04O_PRS_LIN_ACT_NM_ID + Len.MUT04O_PRS_LIN_ACT_NM_ID;
		public static final int MUT04O_PRS_LIN_OBJ_DES = MUT04O_PRS_LIN_OBJ_CD + Len.MUT04O_PRS_LIN_OBJ_CD;
		public static final int MUT04O_CST_NBR = MUT04O_PRS_LIN_OBJ_DES + Len.MUT04O_PRS_LIN_OBJ_DES;
		public static final int MUT04O_MARKETING_INFO = MUT04O_CST_NBR + Len.MUT04O_CST_NBR;
		public static final int MUT04O_MKT_TECHNICAL_KEYS = MUT04O_MARKETING_INFO;
		public static final int MUT04O_MKT_CLT_ID = MUT04O_MKT_TECHNICAL_KEYS;
		public static final int MUT04O_MKT_TER_NBR = MUT04O_MKT_CLT_ID + Len.MUT04O_MKT_CLT_ID;
		public static final int MUT04O_MKT_SVC_DSY_NM = MUT04O_MKT_TER_NBR + Len.MUT04O_MKT_TER_NBR;
		public static final int MUT04O_MUTUAL_OR_AGENCY = MUT04O_MKT_SVC_DSY_NM + Len.MUT04O_MKT_SVC_DSY_NM;
		public static final int MUT04O_MKT_TYPE_CD = MUT04O_MUTUAL_OR_AGENCY;
		public static final int MUT04O_MKT_TYPE_DES = MUT04O_MKT_TYPE_CD + Len.MUT04O_MKT_TYPE_CD;
		public static final int MUT04O_MUTUAL_MARKETING = MUT04O_MKT_TYPE_DES + Len.MUT04O_MKT_TYPE_DES;
		public static final int MUT04O_MUT_MR_INFO = MUT04O_MUTUAL_MARKETING;
		public static final int MUT04O_MT_MKT_TECHNICAL_KEYS = MUT04O_MUT_MR_INFO;
		public static final int MUT04O_MT_MKT_CLT_ID = MUT04O_MT_MKT_TECHNICAL_KEYS;
		public static final int MUT04O_MT_MKT_TER_NBR = MUT04O_MT_MKT_CLT_ID + Len.MUT04O_MT_MKT_CLT_ID;
		public static final int MUT04O_MT_MKT_DSY_NM = MUT04O_MT_MKT_TER_NBR + Len.MUT04O_MT_MKT_TER_NBR;
		public static final int MUT04O_MT_MKT_FST_NM = MUT04O_MT_MKT_DSY_NM + Len.MUT04O_MT_MKT_DSY_NM;
		public static final int MUT04O_MT_MKT_MDL_NM = MUT04O_MT_MKT_FST_NM + Len.MUT04O_MT_MKT_FST_NM;
		public static final int MUT04O_MT_MKT_LST_NM = MUT04O_MT_MKT_MDL_NM + Len.MUT04O_MT_MKT_MDL_NM;
		public static final int MUT04O_MT_MKT_SFX = MUT04O_MT_MKT_LST_NM + Len.MUT04O_MT_MKT_LST_NM;
		public static final int MUT04O_MT_MKT_PFX = MUT04O_MT_MKT_SFX + Len.MUT04O_MT_MKT_SFX;
		public static final int MUT04O_MUT_CSR_INFO = MUT04O_MT_MKT_PFX + Len.MUT04O_MT_MKT_PFX;
		public static final int MUT04O_MT_CSR_TECHNICAL_KEYS = MUT04O_MUT_CSR_INFO;
		public static final int MUT04O_MT_CSR_CLT_ID = MUT04O_MT_CSR_TECHNICAL_KEYS;
		public static final int MUT04O_MT_CSR_TER_NBR = MUT04O_MT_CSR_CLT_ID + Len.MUT04O_MT_CSR_CLT_ID;
		public static final int MUT04O_MT_CSR_DSY_NM = MUT04O_MT_CSR_TER_NBR + Len.MUT04O_MT_CSR_TER_NBR;
		public static final int MUT04O_MT_CSR_FST_NM = MUT04O_MT_CSR_DSY_NM + Len.MUT04O_MT_CSR_DSY_NM;
		public static final int MUT04O_MT_CSR_MDL_NM = MUT04O_MT_CSR_FST_NM + Len.MUT04O_MT_CSR_FST_NM;
		public static final int MUT04O_MT_CSR_LST_NM = MUT04O_MT_CSR_MDL_NM + Len.MUT04O_MT_CSR_MDL_NM;
		public static final int MUT04O_MT_CSR_SFX = MUT04O_MT_CSR_LST_NM + Len.MUT04O_MT_CSR_LST_NM;
		public static final int MUT04O_MT_CSR_PFX = MUT04O_MT_CSR_SFX + Len.MUT04O_MT_CSR_SFX;
		public static final int MUT04O_MUT_SMR_INFO = MUT04O_MT_CSR_PFX + Len.MUT04O_MT_CSR_PFX;
		public static final int MUT04O_MT_SMR_TECHNICAL_KEYS = MUT04O_MUT_SMR_INFO;
		public static final int MUT04O_MT_SMR_CLT_ID = MUT04O_MT_SMR_TECHNICAL_KEYS;
		public static final int MUT04O_MT_SMR_TER_NBR = MUT04O_MT_SMR_CLT_ID + Len.MUT04O_MT_SMR_CLT_ID;
		public static final int MUT04O_MT_SMR_DSY_NM = MUT04O_MT_SMR_TER_NBR + Len.MUT04O_MT_SMR_TER_NBR;
		public static final int MUT04O_MT_SMR_FST_NM = MUT04O_MT_SMR_DSY_NM + Len.MUT04O_MT_SMR_DSY_NM;
		public static final int MUT04O_MT_SMR_MDL_NM = MUT04O_MT_SMR_FST_NM + Len.MUT04O_MT_SMR_FST_NM;
		public static final int MUT04O_MT_SMR_LST_NM = MUT04O_MT_SMR_MDL_NM + Len.MUT04O_MT_SMR_MDL_NM;
		public static final int MUT04O_MT_SMR_SFX = MUT04O_MT_SMR_LST_NM + Len.MUT04O_MT_SMR_LST_NM;
		public static final int MUT04O_MT_SMR_PFX = MUT04O_MT_SMR_SFX + Len.MUT04O_MT_SMR_SFX;
		public static final int MUT04O_MUT_DMM_INFO = MUT04O_MT_SMR_PFX + Len.MUT04O_MT_SMR_PFX;
		public static final int MUT04O_MT_DMM_TECHNICAL_KEYS = MUT04O_MUT_DMM_INFO;
		public static final int MUT04O_MT_DMM_CLT_ID = MUT04O_MT_DMM_TECHNICAL_KEYS;
		public static final int MUT04O_MT_DMM_TER_NBR = MUT04O_MT_DMM_CLT_ID + Len.MUT04O_MT_DMM_CLT_ID;
		public static final int MUT04O_MT_DMM_DSY_NM = MUT04O_MT_DMM_TER_NBR + Len.MUT04O_MT_DMM_TER_NBR;
		public static final int MUT04O_MT_DMM_FST_NM = MUT04O_MT_DMM_DSY_NM + Len.MUT04O_MT_DMM_DSY_NM;
		public static final int MUT04O_MT_DMM_MDL_NM = MUT04O_MT_DMM_FST_NM + Len.MUT04O_MT_DMM_FST_NM;
		public static final int MUT04O_MT_DMM_LST_NM = MUT04O_MT_DMM_MDL_NM + Len.MUT04O_MT_DMM_MDL_NM;
		public static final int MUT04O_MT_DMM_SFX = MUT04O_MT_DMM_LST_NM + Len.MUT04O_MT_DMM_LST_NM;
		public static final int MUT04O_MT_DMM_PFX = MUT04O_MT_DMM_SFX + Len.MUT04O_MT_DMM_SFX;
		public static final int MUT04O_MUT_RMM_INFO = MUT04O_MT_DMM_PFX + Len.MUT04O_MT_DMM_PFX;
		public static final int MUT04O_MT_RMM_TECHNICAL_KEYS = MUT04O_MUT_RMM_INFO;
		public static final int MUT04O_MT_RMM_CLT_ID = MUT04O_MT_RMM_TECHNICAL_KEYS;
		public static final int MUT04O_MT_RMM_TER_NBR = MUT04O_MT_RMM_CLT_ID + Len.MUT04O_MT_RMM_CLT_ID;
		public static final int MUT04O_MT_RMM_DSY_NM = MUT04O_MT_RMM_TER_NBR + Len.MUT04O_MT_RMM_TER_NBR;
		public static final int MUT04O_MT_RMM_FST_NM = MUT04O_MT_RMM_DSY_NM + Len.MUT04O_MT_RMM_DSY_NM;
		public static final int MUT04O_MT_RMM_MDL_NM = MUT04O_MT_RMM_FST_NM + Len.MUT04O_MT_RMM_FST_NM;
		public static final int MUT04O_MT_RMM_LST_NM = MUT04O_MT_RMM_MDL_NM + Len.MUT04O_MT_RMM_MDL_NM;
		public static final int MUT04O_MT_RMM_SFX = MUT04O_MT_RMM_LST_NM + Len.MUT04O_MT_RMM_LST_NM;
		public static final int MUT04O_MT_RMM_PFX = MUT04O_MT_RMM_SFX + Len.MUT04O_MT_RMM_SFX;
		public static final int MUT04O_MUT_DFO_INFO = MUT04O_MT_RMM_PFX + Len.MUT04O_MT_RMM_PFX;
		public static final int MUT04O_MT_DFO_TECHNICAL_KEYS = MUT04O_MUT_DFO_INFO;
		public static final int MUT04O_MT_DFO_CLT_ID = MUT04O_MT_DFO_TECHNICAL_KEYS;
		public static final int MUT04O_MT_DFO_TER_NBR = MUT04O_MT_DFO_CLT_ID + Len.MUT04O_MT_DFO_CLT_ID;
		public static final int MUT04O_MT_DFO_DSY_NM = MUT04O_MT_DFO_TER_NBR + Len.MUT04O_MT_DFO_TER_NBR;
		public static final int MUT04O_MT_DFO_FST_NM = MUT04O_MT_DFO_DSY_NM + Len.MUT04O_MT_DFO_DSY_NM;
		public static final int MUT04O_MT_DFO_MDL_NM = MUT04O_MT_DFO_FST_NM + Len.MUT04O_MT_DFO_FST_NM;
		public static final int MUT04O_MT_DFO_LST_NM = MUT04O_MT_DFO_MDL_NM + Len.MUT04O_MT_DFO_MDL_NM;
		public static final int MUT04O_MT_DFO_SFX = MUT04O_MT_DFO_LST_NM + Len.MUT04O_MT_DFO_LST_NM;
		public static final int MUT04O_MT_DFO_PFX = MUT04O_MT_DFO_SFX + Len.MUT04O_MT_DFO_SFX;
		public static final int FLR7 = MUT04O_MT_DFO_PFX + Len.MUT04O_MT_DFO_PFX;
		public static final int MUT04O_AGENCY_MARKETING = FLR7 + Len.FLR3;
		public static final int MUT04O_AGC_BRN_INFO = MUT04O_AGENCY_MARKETING;
		public static final int MUT04O_AT_BRN_TECHNICAL_KEYS = MUT04O_AGC_BRN_INFO;
		public static final int MUT04O_AT_BRN_CLT_ID = MUT04O_AT_BRN_TECHNICAL_KEYS;
		public static final int MUT04O_AT_BRN_TER_NBR = MUT04O_AT_BRN_CLT_ID + Len.MUT04O_AT_BRN_CLT_ID;
		public static final int MUT04O_AT_BRN_NM = MUT04O_AT_BRN_TER_NBR + Len.MUT04O_AT_BRN_TER_NBR;
		public static final int MUT04O_AGC_INFO = MUT04O_AT_BRN_NM + Len.MUT04O_AT_BRN_NM;
		public static final int MUT04O_AT_AGC_TECHNICAL_KEYS = MUT04O_AGC_INFO;
		public static final int MUT04O_AT_AGC_CLT_ID = MUT04O_AT_AGC_TECHNICAL_KEYS;
		public static final int MUT04O_AT_AGC_TER_NBR = MUT04O_AT_AGC_CLT_ID + Len.MUT04O_AT_AGC_CLT_ID;
		public static final int MUT04O_AT_AGC_NM = MUT04O_AT_AGC_TER_NBR + Len.MUT04O_AT_AGC_TER_NBR;
		public static final int MUT04O_AGC_SMR_INFO = MUT04O_AT_AGC_NM + Len.MUT04O_AT_AGC_NM;
		public static final int MUT04O_AT_SMR_TECHNICAL_KEYS = MUT04O_AGC_SMR_INFO;
		public static final int MUT04O_AT_SMR_CLT_ID = MUT04O_AT_SMR_TECHNICAL_KEYS;
		public static final int MUT04O_AT_SMR_TER_NBR = MUT04O_AT_SMR_CLT_ID + Len.MUT04O_AT_SMR_CLT_ID;
		public static final int MUT04O_AT_SMR_NM = MUT04O_AT_SMR_TER_NBR + Len.MUT04O_AT_SMR_TER_NBR;
		public static final int MUT04O_AGC_AMM_INFO = MUT04O_AT_SMR_NM + Len.MUT04O_AT_SMR_NM;
		public static final int MUT04O_AT_AMM_TECHNICAL_KEYS = MUT04O_AGC_AMM_INFO;
		public static final int MUT04O_AT_AMM_CLT_ID = MUT04O_AT_AMM_TECHNICAL_KEYS;
		public static final int MUT04O_AT_AMM_TER_NBR = MUT04O_AT_AMM_CLT_ID + Len.MUT04O_AT_AMM_CLT_ID;
		public static final int MUT04O_AT_AMM_DSY_NM = MUT04O_AT_AMM_TER_NBR + Len.MUT04O_AT_AMM_TER_NBR;
		public static final int MUT04O_AT_AMM_FST_NM = MUT04O_AT_AMM_DSY_NM + Len.MUT04O_AT_AMM_DSY_NM;
		public static final int MUT04O_AT_AMM_MDL_NM = MUT04O_AT_AMM_FST_NM + Len.MUT04O_AT_AMM_FST_NM;
		public static final int MUT04O_AT_AMM_LST_NM = MUT04O_AT_AMM_MDL_NM + Len.MUT04O_AT_AMM_MDL_NM;
		public static final int MUT04O_AT_AMM_SFX = MUT04O_AT_AMM_LST_NM + Len.MUT04O_AT_AMM_LST_NM;
		public static final int MUT04O_AT_AMM_PFX = MUT04O_AT_AMM_SFX + Len.MUT04O_AT_AMM_SFX;
		public static final int MUT04O_AGC_AFM_INFO = MUT04O_AT_AMM_PFX + Len.MUT04O_AT_AMM_PFX;
		public static final int MUT04O_AT_AFM_TECHNICAL_KEYS = MUT04O_AGC_AFM_INFO;
		public static final int MUT04O_AT_AFM_CLT_ID = MUT04O_AT_AFM_TECHNICAL_KEYS;
		public static final int MUT04O_AT_AFM_TER_NBR = MUT04O_AT_AFM_CLT_ID + Len.MUT04O_AT_AFM_CLT_ID;
		public static final int MUT04O_AT_AFM_DSY_NM = MUT04O_AT_AFM_TER_NBR + Len.MUT04O_AT_AFM_TER_NBR;
		public static final int MUT04O_AT_AFM_FST_NM = MUT04O_AT_AFM_DSY_NM + Len.MUT04O_AT_AFM_DSY_NM;
		public static final int MUT04O_AT_AFM_MDL_NM = MUT04O_AT_AFM_FST_NM + Len.MUT04O_AT_AFM_FST_NM;
		public static final int MUT04O_AT_AFM_LST_NM = MUT04O_AT_AFM_MDL_NM + Len.MUT04O_AT_AFM_MDL_NM;
		public static final int MUT04O_AT_AFM_SFX = MUT04O_AT_AFM_LST_NM + Len.MUT04O_AT_AFM_LST_NM;
		public static final int MUT04O_AT_AFM_PFX = MUT04O_AT_AFM_SFX + Len.MUT04O_AT_AFM_SFX;
		public static final int MUT04O_AGC_AVP_INFO = MUT04O_AT_AFM_PFX + Len.MUT04O_AT_AFM_PFX;
		public static final int MUT04O_AT_AVP_TECHNICAL_KEYS = MUT04O_AGC_AVP_INFO;
		public static final int MUT04O_AT_AVP_CLT_ID = MUT04O_AT_AVP_TECHNICAL_KEYS;
		public static final int MUT04O_AT_AVP_TER_NBR = MUT04O_AT_AVP_CLT_ID + Len.MUT04O_AT_AVP_CLT_ID;
		public static final int MUT04O_AT_AVP_DSY_NM = MUT04O_AT_AVP_TER_NBR + Len.MUT04O_AT_AVP_TER_NBR;
		public static final int MUT04O_AT_AVP_FST_NM = MUT04O_AT_AVP_DSY_NM + Len.MUT04O_AT_AVP_DSY_NM;
		public static final int MUT04O_AT_AVP_MDL_NM = MUT04O_AT_AVP_FST_NM + Len.MUT04O_AT_AVP_FST_NM;
		public static final int MUT04O_AT_AVP_LST_NM = MUT04O_AT_AVP_MDL_NM + Len.MUT04O_AT_AVP_MDL_NM;
		public static final int MUT04O_AT_AVP_SFX = MUT04O_AT_AVP_LST_NM + Len.MUT04O_AT_AVP_LST_NM;
		public static final int MUT04O_AT_AVP_PFX = MUT04O_AT_AVP_SFX + Len.MUT04O_AT_AVP_SFX;
		public static final int FLR8 = MUT04O_AT_AVP_PFX + Len.MUT04O_AT_AVP_PFX;
		public static final int MUT04O_SERVICING_UW_INFO = FLR8 + Len.FLR3;
		public static final int MUT04O_SVC_UW_DSY_NM = MUT04O_SERVICING_UW_INFO;
		public static final int MUT04O_SERVICING_UW_TYPE = MUT04O_SVC_UW_DSY_NM + Len.MUT04O_SVC_UW_DSY_NM;
		public static final int MUT04O_SVC_UW_TYPE_CD = MUT04O_SERVICING_UW_TYPE;
		public static final int MUT04O_SVC_UW_TYPE_DES = MUT04O_SVC_UW_TYPE_CD + Len.MUT04O_SVC_UW_TYPE_CD;
		public static final int MUT04O_UNDERWRITER = MUT04O_SVC_UW_TYPE_DES + Len.MUT04O_SVC_UW_TYPE_DES;
		public static final int MUT04O_UW_TECHNICAL_KEYS = MUT04O_UNDERWRITER;
		public static final int MUT04O_UW_CLT_ID = MUT04O_UW_TECHNICAL_KEYS;
		public static final int MUT04O_UW_DSY_NM = MUT04O_UW_CLT_ID + Len.MUT04O_UW_CLT_ID;
		public static final int MUT04O_UW_FST_NM = MUT04O_UW_DSY_NM + Len.MUT04O_UW_DSY_NM;
		public static final int MUT04O_UW_MDL_NM = MUT04O_UW_FST_NM + Len.MUT04O_UW_FST_NM;
		public static final int MUT04O_UW_LST_NM = MUT04O_UW_MDL_NM + Len.MUT04O_UW_MDL_NM;
		public static final int MUT04O_UW_SFX = MUT04O_UW_LST_NM + Len.MUT04O_UW_LST_NM;
		public static final int MUT04O_UW_PFX = MUT04O_UW_SFX + Len.MUT04O_UW_SFX;
		public static final int MUT04O_RISK_ANALYST = MUT04O_UW_PFX + Len.MUT04O_UW_PFX;
		public static final int MUT04O_RSK_ALS_TECHNICAL_KEYS = MUT04O_RISK_ANALYST;
		public static final int MUT04O_RSK_ALS_CLT_ID = MUT04O_RSK_ALS_TECHNICAL_KEYS;
		public static final int MUT04O_RSK_ALS_DSY_NM = MUT04O_RSK_ALS_CLT_ID + Len.MUT04O_RSK_ALS_CLT_ID;
		public static final int MUT04O_RSK_ALS_FST_NM = MUT04O_RSK_ALS_DSY_NM + Len.MUT04O_RSK_ALS_DSY_NM;
		public static final int MUT04O_RSK_ALS_MDL_NM = MUT04O_RSK_ALS_FST_NM + Len.MUT04O_RSK_ALS_FST_NM;
		public static final int MUT04O_RSK_ALS_LST_NM = MUT04O_RSK_ALS_MDL_NM + Len.MUT04O_RSK_ALS_MDL_NM;
		public static final int MUT04O_RSK_ALS_SFX = MUT04O_RSK_ALS_LST_NM + Len.MUT04O_RSK_ALS_LST_NM;
		public static final int MUT04O_RSK_ALS_PFX = MUT04O_RSK_ALS_SFX + Len.MUT04O_RSK_ALS_SFX;
		public static final int MUT04O_DISTRICT_UW_MANAGER = MUT04O_RSK_ALS_PFX + Len.MUT04O_RSK_ALS_PFX;
		public static final int MUT04O_DUM_TECHNICAL_KEYS = MUT04O_DISTRICT_UW_MANAGER;
		public static final int MUT04O_DUM_CLT_ID = MUT04O_DUM_TECHNICAL_KEYS;
		public static final int MUT04O_DUM_DSY_NM = MUT04O_DUM_CLT_ID + Len.MUT04O_DUM_CLT_ID;
		public static final int MUT04O_DUM_TER = MUT04O_DUM_DSY_NM + Len.MUT04O_DUM_DSY_NM;
		public static final int MUT04O_DUM_FST_NM = MUT04O_DUM_TER + Len.MUT04O_DUM_TER;
		public static final int MUT04O_DUM_MDL_NM = MUT04O_DUM_FST_NM + Len.MUT04O_DUM_FST_NM;
		public static final int MUT04O_DUM_LST_NM = MUT04O_DUM_MDL_NM + Len.MUT04O_DUM_MDL_NM;
		public static final int MUT04O_DUM_SFX = MUT04O_DUM_LST_NM + Len.MUT04O_DUM_LST_NM;
		public static final int MUT04O_DUM_PFX = MUT04O_DUM_SFX + Len.MUT04O_DUM_SFX;
		public static final int MUT04O_REGIONAL_UW_MANAGER = MUT04O_DUM_PFX + Len.MUT04O_DUM_PFX;
		public static final int MUT04O_RUM_TECHNICAL_KEYS = MUT04O_REGIONAL_UW_MANAGER;
		public static final int MUT04O_RUM_CLT_ID = MUT04O_RUM_TECHNICAL_KEYS;
		public static final int MUT04O_RUM_DSY_NM = MUT04O_RUM_CLT_ID + Len.MUT04O_RUM_CLT_ID;
		public static final int MUT04O_RUM_TER = MUT04O_RUM_DSY_NM + Len.MUT04O_RUM_DSY_NM;
		public static final int MUT04O_RUM_FST_NM = MUT04O_RUM_TER + Len.MUT04O_RUM_TER;
		public static final int MUT04O_RUM_MDL_NM = MUT04O_RUM_FST_NM + Len.MUT04O_RUM_FST_NM;
		public static final int MUT04O_RUM_LST_NM = MUT04O_RUM_MDL_NM + Len.MUT04O_RUM_MDL_NM;
		public static final int MUT04O_RUM_SFX = MUT04O_RUM_LST_NM + Len.MUT04O_RUM_LST_NM;
		public static final int MUT04O_RUM_PFX = MUT04O_RUM_SFX + Len.MUT04O_RUM_SFX;
		public static final int MUT04O_FIELD_PRODUCTION = MUT04O_RUM_PFX + Len.MUT04O_RUM_PFX;
		public static final int MUT04O_FPU = MUT04O_FIELD_PRODUCTION;
		public static final int MUT04O_FPU_TECHNICAL_KEYS = MUT04O_FPU;
		public static final int MUT04O_FPU_CLT_ID = MUT04O_FPU_TECHNICAL_KEYS;
		public static final int MUT04O_FPU_DSY_NM = MUT04O_FPU_CLT_ID + Len.MUT04O_FPU_CLT_ID;
		public static final int MUT04O_FPU_FST_NM = MUT04O_FPU_DSY_NM + Len.MUT04O_FPU_DSY_NM;
		public static final int MUT04O_FPU_MDL_NM = MUT04O_FPU_FST_NM + Len.MUT04O_FPU_FST_NM;
		public static final int MUT04O_FPU_LST_NM = MUT04O_FPU_MDL_NM + Len.MUT04O_FPU_MDL_NM;
		public static final int MUT04O_FPU_SFX = MUT04O_FPU_LST_NM + Len.MUT04O_FPU_LST_NM;
		public static final int MUT04O_FPU_PFX = MUT04O_FPU_SFX + Len.MUT04O_FPU_SFX;
		public static final int FLR9 = MUT04O_FPU_PFX + Len.MUT04O_FPU_PFX;
		public static final int MUT04O_FPU_DISTRICT_MANAGER = FLR9 + Len.FLR3;
		public static final int MUT04O_FPU_DUM_TECHNICAL_KEYS = MUT04O_FPU_DISTRICT_MANAGER;
		public static final int MUT04O_FPU_DUM_CLT_ID = MUT04O_FPU_DUM_TECHNICAL_KEYS;
		public static final int MUT04O_FPU_DUM_DSY_NM = MUT04O_FPU_DUM_CLT_ID + Len.MUT04O_FPU_DUM_CLT_ID;
		public static final int MUT04O_FPU_DUM_TER = MUT04O_FPU_DUM_DSY_NM + Len.MUT04O_FPU_DUM_DSY_NM;
		public static final int MUT04O_FPU_DUM_FST_NM = MUT04O_FPU_DUM_TER + Len.MUT04O_FPU_DUM_TER;
		public static final int MUT04O_FPU_DUM_MDL_NM = MUT04O_FPU_DUM_FST_NM + Len.MUT04O_FPU_DUM_FST_NM;
		public static final int MUT04O_FPU_DUM_LST_NM = MUT04O_FPU_DUM_MDL_NM + Len.MUT04O_FPU_DUM_MDL_NM;
		public static final int MUT04O_FPU_DUM_SFX = MUT04O_FPU_DUM_LST_NM + Len.MUT04O_FPU_DUM_LST_NM;
		public static final int MUT04O_FPU_DUM_PFX = MUT04O_FPU_DUM_SFX + Len.MUT04O_FPU_DUM_SFX;
		public static final int MUT04O_FPU_REGIONAL_MANAGER = MUT04O_FPU_DUM_PFX + Len.MUT04O_FPU_DUM_PFX;
		public static final int MUT04O_FPU_RUM_TECHNICAL_KEYS = MUT04O_FPU_REGIONAL_MANAGER;
		public static final int MUT04O_FPU_RUM_CLT_ID = MUT04O_FPU_RUM_TECHNICAL_KEYS;
		public static final int MUT04O_FPU_RUM_DSY_NM = MUT04O_FPU_RUM_CLT_ID + Len.MUT04O_FPU_RUM_CLT_ID;
		public static final int MUT04O_FPU_RUM_TER = MUT04O_FPU_RUM_DSY_NM + Len.MUT04O_FPU_RUM_DSY_NM;
		public static final int MUT04O_FPU_RUM_FST_NM = MUT04O_FPU_RUM_TER + Len.MUT04O_FPU_RUM_TER;
		public static final int MUT04O_FPU_RUM_MDL_NM = MUT04O_FPU_RUM_FST_NM + Len.MUT04O_FPU_RUM_FST_NM;
		public static final int MUT04O_FPU_RUM_LST_NM = MUT04O_FPU_RUM_MDL_NM + Len.MUT04O_FPU_RUM_MDL_NM;
		public static final int MUT04O_FPU_RUM_SFX = MUT04O_FPU_RUM_LST_NM + Len.MUT04O_FPU_RUM_LST_NM;
		public static final int MUT04O_FPU_RUM_PFX = MUT04O_FPU_RUM_SFX + Len.MUT04O_FPU_RUM_SFX;
		public static final int MUT04O_LP_RISK_ANALYST = MUT04O_FPU_RUM_PFX + Len.MUT04O_FPU_RUM_PFX;
		public static final int MUT04O_LPRA = MUT04O_LP_RISK_ANALYST;
		public static final int MUT04O_LP_RSK_ALS_TECH_KEYS = MUT04O_LPRA;
		public static final int MUT04O_LP_RSK_ALS_CLT_ID = MUT04O_LP_RSK_ALS_TECH_KEYS;
		public static final int MUT04O_LP_RSK_ALS_DSY_NM = MUT04O_LP_RSK_ALS_CLT_ID + Len.MUT04O_LP_RSK_ALS_CLT_ID;
		public static final int MUT04O_LP_RSK_ALS_FST_NM = MUT04O_LP_RSK_ALS_DSY_NM + Len.MUT04O_LP_RSK_ALS_DSY_NM;
		public static final int MUT04O_LP_RSK_ALS_MDL_NM = MUT04O_LP_RSK_ALS_FST_NM + Len.MUT04O_LP_RSK_ALS_FST_NM;
		public static final int MUT04O_LP_RSK_ALS_LST_NM = MUT04O_LP_RSK_ALS_MDL_NM + Len.MUT04O_LP_RSK_ALS_MDL_NM;
		public static final int MUT04O_LP_RSK_ALS_SFX = MUT04O_LP_RSK_ALS_LST_NM + Len.MUT04O_LP_RSK_ALS_LST_NM;
		public static final int MUT04O_LP_RSK_ALS_PFX = MUT04O_LP_RSK_ALS_SFX + Len.MUT04O_LP_RSK_ALS_SFX;
		public static final int MUT04O_LPRA_DISTRICT_MANAGER = MUT04O_LP_RSK_ALS_PFX + Len.MUT04O_LP_RSK_ALS_PFX;
		public static final int MUT04O_LPRA_DUM_TECHNICAL_KEYS = MUT04O_LPRA_DISTRICT_MANAGER;
		public static final int MUT04O_LPRA_DUM_CLT_ID = MUT04O_LPRA_DUM_TECHNICAL_KEYS;
		public static final int MUT04O_LPRA_DUM_DSY_NM = MUT04O_LPRA_DUM_CLT_ID + Len.MUT04O_LPRA_DUM_CLT_ID;
		public static final int MUT04O_LPRA_DUM_TER = MUT04O_LPRA_DUM_DSY_NM + Len.MUT04O_LPRA_DUM_DSY_NM;
		public static final int MUT04O_LPRA_DUM_FST_NM = MUT04O_LPRA_DUM_TER + Len.MUT04O_LPRA_DUM_TER;
		public static final int MUT04O_LPRA_DUM_MDL_NM = MUT04O_LPRA_DUM_FST_NM + Len.MUT04O_LPRA_DUM_FST_NM;
		public static final int MUT04O_LPRA_DUM_LST_NM = MUT04O_LPRA_DUM_MDL_NM + Len.MUT04O_LPRA_DUM_MDL_NM;
		public static final int MUT04O_LPRA_DUM_SFX = MUT04O_LPRA_DUM_LST_NM + Len.MUT04O_LPRA_DUM_LST_NM;
		public static final int MUT04O_LPRA_DUM_PFX = MUT04O_LPRA_DUM_SFX + Len.MUT04O_LPRA_DUM_SFX;
		public static final int MUT04O_LPRA_REGIONAL_MANAGER = MUT04O_LPRA_DUM_PFX + Len.MUT04O_LPRA_DUM_PFX;
		public static final int MUT04O_LPRA_RUM_TECHNICAL_KEYS = MUT04O_LPRA_REGIONAL_MANAGER;
		public static final int MUT04O_LPRA_RUM_CLT_ID = MUT04O_LPRA_RUM_TECHNICAL_KEYS;
		public static final int MUT04O_LPRA_RUM_DSY_NM = MUT04O_LPRA_RUM_CLT_ID + Len.MUT04O_LPRA_RUM_CLT_ID;
		public static final int MUT04O_LPRA_RUM_TER = MUT04O_LPRA_RUM_DSY_NM + Len.MUT04O_LPRA_RUM_DSY_NM;
		public static final int MUT04O_LPRA_RUM_FST_NM = MUT04O_LPRA_RUM_TER + Len.MUT04O_LPRA_RUM_TER;
		public static final int MUT04O_LPRA_RUM_MDL_NM = MUT04O_LPRA_RUM_FST_NM + Len.MUT04O_LPRA_RUM_FST_NM;
		public static final int MUT04O_LPRA_RUM_LST_NM = MUT04O_LPRA_RUM_MDL_NM + Len.MUT04O_LPRA_RUM_MDL_NM;
		public static final int MUT04O_LPRA_RUM_SFX = MUT04O_LPRA_RUM_LST_NM + Len.MUT04O_LPRA_RUM_LST_NM;
		public static final int MUT04O_LPRA_RUM_PFX = MUT04O_LPRA_RUM_SFX + Len.MUT04O_LPRA_RUM_SFX;
		public static final int MUT04O_ACT_LOCATION = MUT04O_LPRA_RUM_PFX + Len.MUT04O_LPRA_RUM_PFX;
		public static final int MUT04O_AL_STATE_CD = MUT04O_ACT_LOCATION;
		public static final int MUT04O_AL_COUNTY_CD = MUT04O_AL_STATE_CD + Len.MUT04O_AL_STATE_CD;
		public static final int MUT04O_AL_TOWN_CD = MUT04O_AL_COUNTY_CD + Len.MUT04O_AL_COUNTY_CD;
		public static final int MUT04O_NBR_EMPLOYEES = MUT04O_AL_TOWN_CD + Len.MUT04O_AL_TOWN_CD;
		public static final int MUT04O_OFC_LOC_CD = mut04oSeSicDes(MUT04O_SE_SIC_MAXOCCURS - 1) + Len.MUT04O_SE_SIC_DES;
		public static final int MUT04O_OL_DIVISION = MUT04O_OFC_LOC_CD;
		public static final int MUT04O_OL_SUB_DIVISION = MUT04O_OL_DIVISION + Len.MUT04O_OL_DIVISION;
		public static final int FLR10 = MUT04O_OL_SUB_DIVISION + Len.MUT04O_OL_SUB_DIVISION;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int mut04oSeSic(int idx) {
			return MUT04O_NBR_EMPLOYEES + Len.MUT04O_NBR_EMPLOYEES + idx * Len.MUT04O_SE_SIC;
		}

		public static int mut04oSeSicCd(int idx) {
			return mut04oSeSic(idx);
		}

		public static int mut04oSeSicDes(int idx) {
			return mut04oSeSicCd(idx) + Len.MUT04O_SE_SIC_CD;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int MUT04I_TK_CLT_ID = 20;
		public static final int MUT04I_TK_CIOR_SHW_OBJ_KEY = 30;
		public static final int MUT04I_TK_ADR_SEQ_NBR = 5;
		public static final int MUT04I_USR_ID = 8;
		public static final int MUT04I_ACT_NBR = 9;
		public static final int MUT04I_TER_NBR = 4;
		public static final int MUT04I_AS_OF_DT = 10;
		public static final int MUT04I_FEIN = 10;
		public static final int MUT04I_SSN = 11;
		public static final int MUT04I_PHN_ACD = 3;
		public static final int MUT04I_PHN_PFX_NBR = 3;
		public static final int MUT04I_PHN_LIN_NBR = 4;
		public static final int FLR1 = 100;
		public static final int MUT04O_TK_CLT_ID = 20;
		public static final int FLR2 = 60;
		public static final int MUT04O_CN_IDV_NM_IND = 1;
		public static final int MUT04O_CN_DSY_NM = 120;
		public static final int MUT04O_CN_SR_NM = 30;
		public static final int MUT04O_CN_FST_NM = 30;
		public static final int MUT04O_CN_MDL_NM = 30;
		public static final int MUT04O_CN_LST_NM = 60;
		public static final int MUT04O_CN_SFX = 4;
		public static final int MUT04O_CN_PFX = 4;
		public static final int FLR3 = 20;
		public static final int MUT04O_CI_CLIENT_ID = 20;
		public static final int MUT04O_CI_IDV_NM_IND = 1;
		public static final int MUT04O_CI_DSY_NM = 120;
		public static final int MUT04O_CI_SR_NM = 30;
		public static final int MUT04O_CI_FST_NM = 30;
		public static final int MUT04O_CI_MDL_NM = 30;
		public static final int MUT04O_CI_LST_NM = 60;
		public static final int MUT04O_CI_SFX = 4;
		public static final int MUT04O_CI_PFX = 4;
		public static final int MUT04O_BSM_ADR_SEQ_NBR = 5;
		public static final int MUT04O_BSM_ADR_ID = 20;
		public static final int MUT04O_BSM_LIN1_ADR = 45;
		public static final int MUT04O_BSM_LIN2_ADR = 45;
		public static final int MUT04O_BSM_CIT = 30;
		public static final int MUT04O_BSM_ST_ABB = 3;
		public static final int MUT04O_BSM_PST_CD = 13;
		public static final int MUT04O_BSM_CTY = 30;
		public static final int MUT04O_BSM_CTR_CD = 4;
		public static final int MUT04O_BSL_ADR_SEQ_NBR = 5;
		public static final int MUT04O_BSL_ADR_ID = 20;
		public static final int MUT04O_BSL_LIN1_ADR = 45;
		public static final int MUT04O_BSL_LIN2_ADR = 45;
		public static final int MUT04O_BSL_CIT = 30;
		public static final int MUT04O_BSL_ST_ABB = 3;
		public static final int MUT04O_BSL_PST_CD = 13;
		public static final int MUT04O_BSL_CTY = 30;
		public static final int MUT04O_BSL_CTR_CD = 4;
		public static final int MUT04O_BUS_PHN_ACD = 3;
		public static final int MUT04O_BUS_PHN_PFX_NBR = 3;
		public static final int MUT04O_BUS_PHN_LIN_NBR = 4;
		public static final int MUT04O_BUS_PHN_EXT_NBR = 5;
		public static final int MUT04O_BUS_PHN_NBR_FMT = 21;
		public static final int MUT04O_FAX_PHN_ACD = 3;
		public static final int MUT04O_FAX_PHN_PFX_NBR = 3;
		public static final int MUT04O_FAX_PHN_LIN_NBR = 4;
		public static final int MUT04O_FAX_PHN_EXT_NBR = 5;
		public static final int MUT04O_FAX_PHN_NBR_FMT = 21;
		public static final int MUT04O_CTC_PHN_ACD = 3;
		public static final int MUT04O_CTC_PHN_PFX_NBR = 3;
		public static final int MUT04O_CTC_PHN_LIN_NBR = 4;
		public static final int MUT04O_CTC_PHN_EXT_NBR = 5;
		public static final int MUT04O_CTC_PHN_NBR_FMT = 21;
		public static final int MUT04O_FEIN = 9;
		public static final int MUT04O_SSN = 9;
		public static final int MUT04O_FEIN_FMT = 15;
		public static final int MUT04O_SSN_FMT = 15;
		public static final int MUT04O_LEG_ETY_CD = 2;
		public static final int MUT04O_LEG_ETY_DES = 40;
		public static final int MUT04O_TOB_CD = 4;
		public static final int MUT04O_TOB_DES = 40;
		public static final int MUT04O_SIC_CD = 4;
		public static final int MUT04O_SIC_DES = 40;
		public static final int MUT04O_SEG_CD = 3;
		public static final int MUT04O_SEG_DES = 40;
		public static final int MUT04O_PC_PSPT_ACT_IND = 1;
		public static final int MUT04O_PC_ACT_NBR = 7;
		public static final int MUT04O_PC_ACT_NBR2 = 7;
		public static final int MUT04O_PC_ACT_NBR_FMT = 9;
		public static final int MUT04O_PC_ACT_NBR2_FMT = 9;
		public static final int MUT04O_PSPT_ACT_NBR = 7;
		public static final int MUT04O_PSPT_ACT_NBR_FMT = 9;
		public static final int MUT04O_ACT_NM_ID = 5;
		public static final int MUT04O_ACT2_NM_ID = 5;
		public static final int MUT04O_OBJ_CD = 4;
		public static final int MUT04O_OBJ_CD2 = 4;
		public static final int MUT04O_OBJ_DES = 40;
		public static final int MUT04O_OBJ_DES2 = 40;
		public static final int MUT04O_SIN_WC_ACT_NBR = 7;
		public static final int MUT04O_SIN_WC_ACT_NBR_FMT = 9;
		public static final int MUT04O_SIN_WC_ACT_NM_ID = 5;
		public static final int MUT04O_SIN_WC_OBJ_CD = 4;
		public static final int MUT04O_SIN_WC_OBJ_DES = 40;
		public static final int MUT04O_SPL_BILL_ACT_NBR = 7;
		public static final int MUT04O_SPL_BILL_ACT_NBR_FMT = 9;
		public static final int MUT04O_SPL_BILL_ACT_NM_ID = 5;
		public static final int MUT04O_SPL_BILL_OBJ_CD = 4;
		public static final int MUT04O_SPL_BILL_OBJ_DES = 40;
		public static final int MUT04O_PRS_LIN_ACT_NBR = 7;
		public static final int MUT04O_PRS_LIN_ACT_NBR_FMT = 9;
		public static final int MUT04O_PRS_LIN_ACT_NM_ID = 5;
		public static final int MUT04O_PRS_LIN_OBJ_CD = 4;
		public static final int MUT04O_PRS_LIN_OBJ_DES = 40;
		public static final int MUT04O_CST_NBR = 3;
		public static final int MUT04O_MKT_CLT_ID = 20;
		public static final int MUT04O_MKT_TER_NBR = 4;
		public static final int MUT04O_MKT_SVC_DSY_NM = 120;
		public static final int MUT04O_MKT_TYPE_CD = 1;
		public static final int MUT04O_MKT_TYPE_DES = 10;
		public static final int MUT04O_MT_MKT_CLT_ID = 20;
		public static final int MUT04O_MT_MKT_TER_NBR = 4;
		public static final int MUT04O_MT_MKT_DSY_NM = 120;
		public static final int MUT04O_MT_MKT_FST_NM = 30;
		public static final int MUT04O_MT_MKT_MDL_NM = 30;
		public static final int MUT04O_MT_MKT_LST_NM = 60;
		public static final int MUT04O_MT_MKT_SFX = 4;
		public static final int MUT04O_MT_MKT_PFX = 4;
		public static final int MUT04O_MT_CSR_CLT_ID = 20;
		public static final int MUT04O_MT_CSR_TER_NBR = 4;
		public static final int MUT04O_MT_CSR_DSY_NM = 120;
		public static final int MUT04O_MT_CSR_FST_NM = 30;
		public static final int MUT04O_MT_CSR_MDL_NM = 30;
		public static final int MUT04O_MT_CSR_LST_NM = 60;
		public static final int MUT04O_MT_CSR_SFX = 4;
		public static final int MUT04O_MT_CSR_PFX = 4;
		public static final int MUT04O_MT_SMR_CLT_ID = 20;
		public static final int MUT04O_MT_SMR_TER_NBR = 4;
		public static final int MUT04O_MT_SMR_DSY_NM = 120;
		public static final int MUT04O_MT_SMR_FST_NM = 30;
		public static final int MUT04O_MT_SMR_MDL_NM = 30;
		public static final int MUT04O_MT_SMR_LST_NM = 60;
		public static final int MUT04O_MT_SMR_SFX = 4;
		public static final int MUT04O_MT_SMR_PFX = 4;
		public static final int MUT04O_MT_DMM_CLT_ID = 20;
		public static final int MUT04O_MT_DMM_TER_NBR = 4;
		public static final int MUT04O_MT_DMM_DSY_NM = 120;
		public static final int MUT04O_MT_DMM_FST_NM = 30;
		public static final int MUT04O_MT_DMM_MDL_NM = 30;
		public static final int MUT04O_MT_DMM_LST_NM = 60;
		public static final int MUT04O_MT_DMM_SFX = 4;
		public static final int MUT04O_MT_DMM_PFX = 4;
		public static final int MUT04O_MT_RMM_CLT_ID = 20;
		public static final int MUT04O_MT_RMM_TER_NBR = 4;
		public static final int MUT04O_MT_RMM_DSY_NM = 120;
		public static final int MUT04O_MT_RMM_FST_NM = 30;
		public static final int MUT04O_MT_RMM_MDL_NM = 30;
		public static final int MUT04O_MT_RMM_LST_NM = 60;
		public static final int MUT04O_MT_RMM_SFX = 4;
		public static final int MUT04O_MT_RMM_PFX = 4;
		public static final int MUT04O_MT_DFO_CLT_ID = 20;
		public static final int MUT04O_MT_DFO_TER_NBR = 4;
		public static final int MUT04O_MT_DFO_DSY_NM = 120;
		public static final int MUT04O_MT_DFO_FST_NM = 30;
		public static final int MUT04O_MT_DFO_MDL_NM = 30;
		public static final int MUT04O_MT_DFO_LST_NM = 60;
		public static final int MUT04O_MT_DFO_SFX = 4;
		public static final int MUT04O_MT_DFO_PFX = 4;
		public static final int MUT04O_AT_BRN_CLT_ID = 20;
		public static final int MUT04O_AT_BRN_TER_NBR = 4;
		public static final int MUT04O_AT_BRN_NM = 60;
		public static final int MUT04O_AT_AGC_CLT_ID = 20;
		public static final int MUT04O_AT_AGC_TER_NBR = 4;
		public static final int MUT04O_AT_AGC_NM = 60;
		public static final int MUT04O_AT_SMR_CLT_ID = 20;
		public static final int MUT04O_AT_SMR_TER_NBR = 4;
		public static final int MUT04O_AT_SMR_NM = 60;
		public static final int MUT04O_AT_AMM_CLT_ID = 20;
		public static final int MUT04O_AT_AMM_TER_NBR = 4;
		public static final int MUT04O_AT_AMM_DSY_NM = 120;
		public static final int MUT04O_AT_AMM_FST_NM = 30;
		public static final int MUT04O_AT_AMM_MDL_NM = 30;
		public static final int MUT04O_AT_AMM_LST_NM = 60;
		public static final int MUT04O_AT_AMM_SFX = 4;
		public static final int MUT04O_AT_AMM_PFX = 4;
		public static final int MUT04O_AT_AFM_CLT_ID = 20;
		public static final int MUT04O_AT_AFM_TER_NBR = 4;
		public static final int MUT04O_AT_AFM_DSY_NM = 120;
		public static final int MUT04O_AT_AFM_FST_NM = 30;
		public static final int MUT04O_AT_AFM_MDL_NM = 30;
		public static final int MUT04O_AT_AFM_LST_NM = 60;
		public static final int MUT04O_AT_AFM_SFX = 4;
		public static final int MUT04O_AT_AFM_PFX = 4;
		public static final int MUT04O_AT_AVP_CLT_ID = 20;
		public static final int MUT04O_AT_AVP_TER_NBR = 4;
		public static final int MUT04O_AT_AVP_DSY_NM = 120;
		public static final int MUT04O_AT_AVP_FST_NM = 30;
		public static final int MUT04O_AT_AVP_MDL_NM = 30;
		public static final int MUT04O_AT_AVP_LST_NM = 60;
		public static final int MUT04O_AT_AVP_SFX = 4;
		public static final int MUT04O_AT_AVP_PFX = 4;
		public static final int MUT04O_SVC_UW_DSY_NM = 120;
		public static final int MUT04O_SVC_UW_TYPE_CD = 4;
		public static final int MUT04O_SVC_UW_TYPE_DES = 30;
		public static final int MUT04O_UW_CLT_ID = 20;
		public static final int MUT04O_UW_DSY_NM = 120;
		public static final int MUT04O_UW_FST_NM = 30;
		public static final int MUT04O_UW_MDL_NM = 30;
		public static final int MUT04O_UW_LST_NM = 60;
		public static final int MUT04O_UW_SFX = 4;
		public static final int MUT04O_UW_PFX = 4;
		public static final int MUT04O_RSK_ALS_CLT_ID = 20;
		public static final int MUT04O_RSK_ALS_DSY_NM = 120;
		public static final int MUT04O_RSK_ALS_FST_NM = 30;
		public static final int MUT04O_RSK_ALS_MDL_NM = 30;
		public static final int MUT04O_RSK_ALS_LST_NM = 60;
		public static final int MUT04O_RSK_ALS_SFX = 4;
		public static final int MUT04O_RSK_ALS_PFX = 4;
		public static final int MUT04O_DUM_CLT_ID = 20;
		public static final int MUT04O_DUM_DSY_NM = 120;
		public static final int MUT04O_DUM_TER = 40;
		public static final int MUT04O_DUM_FST_NM = 30;
		public static final int MUT04O_DUM_MDL_NM = 30;
		public static final int MUT04O_DUM_LST_NM = 60;
		public static final int MUT04O_DUM_SFX = 4;
		public static final int MUT04O_DUM_PFX = 4;
		public static final int MUT04O_RUM_CLT_ID = 20;
		public static final int MUT04O_RUM_DSY_NM = 120;
		public static final int MUT04O_RUM_TER = 40;
		public static final int MUT04O_RUM_FST_NM = 30;
		public static final int MUT04O_RUM_MDL_NM = 30;
		public static final int MUT04O_RUM_LST_NM = 60;
		public static final int MUT04O_RUM_SFX = 4;
		public static final int MUT04O_RUM_PFX = 4;
		public static final int MUT04O_FPU_CLT_ID = 20;
		public static final int MUT04O_FPU_DSY_NM = 120;
		public static final int MUT04O_FPU_FST_NM = 30;
		public static final int MUT04O_FPU_MDL_NM = 30;
		public static final int MUT04O_FPU_LST_NM = 60;
		public static final int MUT04O_FPU_SFX = 4;
		public static final int MUT04O_FPU_PFX = 4;
		public static final int MUT04O_FPU_DUM_CLT_ID = 20;
		public static final int MUT04O_FPU_DUM_DSY_NM = 120;
		public static final int MUT04O_FPU_DUM_TER = 40;
		public static final int MUT04O_FPU_DUM_FST_NM = 30;
		public static final int MUT04O_FPU_DUM_MDL_NM = 30;
		public static final int MUT04O_FPU_DUM_LST_NM = 60;
		public static final int MUT04O_FPU_DUM_SFX = 4;
		public static final int MUT04O_FPU_DUM_PFX = 4;
		public static final int MUT04O_FPU_RUM_CLT_ID = 20;
		public static final int MUT04O_FPU_RUM_DSY_NM = 120;
		public static final int MUT04O_FPU_RUM_TER = 40;
		public static final int MUT04O_FPU_RUM_FST_NM = 30;
		public static final int MUT04O_FPU_RUM_MDL_NM = 30;
		public static final int MUT04O_FPU_RUM_LST_NM = 60;
		public static final int MUT04O_FPU_RUM_SFX = 4;
		public static final int MUT04O_FPU_RUM_PFX = 4;
		public static final int MUT04O_LP_RSK_ALS_CLT_ID = 20;
		public static final int MUT04O_LP_RSK_ALS_DSY_NM = 120;
		public static final int MUT04O_LP_RSK_ALS_FST_NM = 30;
		public static final int MUT04O_LP_RSK_ALS_MDL_NM = 30;
		public static final int MUT04O_LP_RSK_ALS_LST_NM = 60;
		public static final int MUT04O_LP_RSK_ALS_SFX = 4;
		public static final int MUT04O_LP_RSK_ALS_PFX = 4;
		public static final int MUT04O_LPRA_DUM_CLT_ID = 20;
		public static final int MUT04O_LPRA_DUM_DSY_NM = 120;
		public static final int MUT04O_LPRA_DUM_TER = 40;
		public static final int MUT04O_LPRA_DUM_FST_NM = 30;
		public static final int MUT04O_LPRA_DUM_MDL_NM = 30;
		public static final int MUT04O_LPRA_DUM_LST_NM = 60;
		public static final int MUT04O_LPRA_DUM_SFX = 4;
		public static final int MUT04O_LPRA_DUM_PFX = 4;
		public static final int MUT04O_LPRA_RUM_CLT_ID = 20;
		public static final int MUT04O_LPRA_RUM_DSY_NM = 120;
		public static final int MUT04O_LPRA_RUM_TER = 40;
		public static final int MUT04O_LPRA_RUM_FST_NM = 30;
		public static final int MUT04O_LPRA_RUM_MDL_NM = 30;
		public static final int MUT04O_LPRA_RUM_LST_NM = 60;
		public static final int MUT04O_LPRA_RUM_SFX = 4;
		public static final int MUT04O_LPRA_RUM_PFX = 4;
		public static final int MUT04O_AL_STATE_CD = 3;
		public static final int MUT04O_AL_COUNTY_CD = 3;
		public static final int MUT04O_AL_TOWN_CD = 4;
		public static final int MUT04O_NBR_EMPLOYEES = 5;
		public static final int MUT04O_SE_SIC_CD = 4;
		public static final int MUT04O_SE_SIC_DES = 40;
		public static final int MUT04O_SE_SIC = MUT04O_SE_SIC_CD + MUT04O_SE_SIC_DES;
		public static final int MUT04O_OL_DIVISION = 1;
		public static final int MUT04O_OL_SUB_DIVISION = 1;
		public static final int MUT04I_TECHNICAL_KEYS = MUT04I_TK_CLT_ID + MUT04I_TK_CIOR_SHW_OBJ_KEY + MUT04I_TK_ADR_SEQ_NBR;
		public static final int MUT004_SERVICE_INPUTS = MUT04I_TECHNICAL_KEYS + MUT04I_USR_ID + MUT04I_ACT_NBR + MUT04I_TER_NBR + MUT04I_AS_OF_DT
				+ MUT04I_FEIN + MUT04I_SSN + MUT04I_PHN_ACD + MUT04I_PHN_PFX_NBR + MUT04I_PHN_LIN_NBR + FLR1;
		public static final int MUT04O_TECHNICAL_KEYS = MUT04O_TK_CLT_ID + FLR2;
		public static final int MUT04O_CLIENT_NAME = MUT04O_CN_IDV_NM_IND + MUT04O_CN_DSY_NM + MUT04O_CN_SR_NM + MUT04O_CN_FST_NM + MUT04O_CN_MDL_NM
				+ MUT04O_CN_LST_NM + MUT04O_CN_SFX + MUT04O_CN_PFX + FLR3;
		public static final int MUT04O_COWN_INFO = MUT04O_CI_CLIENT_ID + MUT04O_CI_IDV_NM_IND + MUT04O_CI_DSY_NM + MUT04O_CI_SR_NM + MUT04O_CI_FST_NM
				+ MUT04O_CI_MDL_NM + MUT04O_CI_LST_NM + MUT04O_CI_SFX + MUT04O_CI_PFX + FLR3;
		public static final int MUT04O_BSM_TECHNICAL_KEYS = MUT04O_BSM_ADR_SEQ_NBR + MUT04O_BSM_ADR_ID;
		public static final int MUT04O_BSM_ADR = MUT04O_BSM_TECHNICAL_KEYS + MUT04O_BSM_LIN1_ADR + MUT04O_BSM_LIN2_ADR + MUT04O_BSM_CIT
				+ MUT04O_BSM_ST_ABB + MUT04O_BSM_PST_CD + MUT04O_BSM_CTY + MUT04O_BSM_CTR_CD + FLR3;
		public static final int MUT04O_BSL_TECHNICAL_KEYS = MUT04O_BSL_ADR_SEQ_NBR + MUT04O_BSL_ADR_ID;
		public static final int MUT04O_BSL_ADR = MUT04O_BSL_TECHNICAL_KEYS + MUT04O_BSL_LIN1_ADR + MUT04O_BSL_LIN2_ADR + MUT04O_BSL_CIT
				+ MUT04O_BSL_ST_ABB + MUT04O_BSL_PST_CD + MUT04O_BSL_CTY + MUT04O_BSL_CTR_CD + FLR3;
		public static final int MUT04O_CLIENT_ADDRESS = MUT04O_BSM_ADR + MUT04O_BSL_ADR;
		public static final int MUT04O_BUSINESS_PHONE = MUT04O_BUS_PHN_ACD + MUT04O_BUS_PHN_PFX_NBR + MUT04O_BUS_PHN_LIN_NBR + MUT04O_BUS_PHN_EXT_NBR;
		public static final int MUT04O_FAX_NUMBER = MUT04O_FAX_PHN_ACD + MUT04O_FAX_PHN_PFX_NBR + MUT04O_FAX_PHN_LIN_NBR + MUT04O_FAX_PHN_EXT_NBR;
		public static final int MUT04O_CONTACT_NUMBER = MUT04O_CTC_PHN_ACD + MUT04O_CTC_PHN_PFX_NBR + MUT04O_CTC_PHN_LIN_NBR + MUT04O_CTC_PHN_EXT_NBR;
		public static final int MUT04O_CLIENT_PHONE = MUT04O_BUSINESS_PHONE + MUT04O_BUS_PHN_NBR_FMT + MUT04O_FAX_NUMBER + MUT04O_FAX_PHN_NBR_FMT
				+ MUT04O_CONTACT_NUMBER + MUT04O_CTC_PHN_NBR_FMT;
		public static final int MUT04O_CLIENT_TAX = MUT04O_FEIN + MUT04O_SSN + MUT04O_FEIN_FMT + MUT04O_SSN_FMT;
		public static final int MUT04O_LEGAL_ENTITY = MUT04O_LEG_ETY_CD + MUT04O_LEG_ETY_DES;
		public static final int MUT04O_TYPE_OF_BUSINESS = MUT04O_TOB_CD + MUT04O_TOB_DES;
		public static final int MUT04O_SIC = MUT04O_SIC_CD + MUT04O_SIC_DES;
		public static final int MUT04O_SEGMENT_INFORMATION = MUT04O_SEG_CD + MUT04O_SEG_DES;
		public static final int MUT04O_PROPERTY_CASUALTY = MUT04O_PC_PSPT_ACT_IND + MUT04O_PC_ACT_NBR + MUT04O_PC_ACT_NBR2 + MUT04O_PC_ACT_NBR_FMT
				+ MUT04O_PC_ACT_NBR2_FMT + MUT04O_PSPT_ACT_NBR + MUT04O_PSPT_ACT_NBR_FMT + MUT04O_ACT_NM_ID + MUT04O_ACT2_NM_ID + MUT04O_OBJ_CD
				+ MUT04O_OBJ_CD2 + MUT04O_OBJ_DES + MUT04O_OBJ_DES2;
		public static final int MUT04O_CLIENT_ACCOUNT = MUT04O_PROPERTY_CASUALTY + MUT04O_SIN_WC_ACT_NBR + MUT04O_SIN_WC_ACT_NBR_FMT
				+ MUT04O_SIN_WC_ACT_NM_ID + MUT04O_SIN_WC_OBJ_CD + MUT04O_SIN_WC_OBJ_DES + MUT04O_SPL_BILL_ACT_NBR + MUT04O_SPL_BILL_ACT_NBR_FMT
				+ MUT04O_SPL_BILL_ACT_NM_ID + MUT04O_SPL_BILL_OBJ_CD + MUT04O_SPL_BILL_OBJ_DES + MUT04O_PRS_LIN_ACT_NBR + MUT04O_PRS_LIN_ACT_NBR_FMT
				+ MUT04O_PRS_LIN_ACT_NM_ID + MUT04O_PRS_LIN_OBJ_CD + MUT04O_PRS_LIN_OBJ_DES;
		public static final int MUT04O_MKT_TECHNICAL_KEYS = MUT04O_MKT_CLT_ID;
		public static final int MUT04O_MUTUAL_OR_AGENCY = MUT04O_MKT_TYPE_CD + MUT04O_MKT_TYPE_DES;
		public static final int MUT04O_MARKETING_INFO = MUT04O_MKT_TECHNICAL_KEYS + MUT04O_MKT_TER_NBR + MUT04O_MKT_SVC_DSY_NM
				+ MUT04O_MUTUAL_OR_AGENCY;
		public static final int MUT04O_MT_MKT_TECHNICAL_KEYS = MUT04O_MT_MKT_CLT_ID;
		public static final int MUT04O_MUT_MR_INFO = MUT04O_MT_MKT_TECHNICAL_KEYS + MUT04O_MT_MKT_TER_NBR + MUT04O_MT_MKT_DSY_NM
				+ MUT04O_MT_MKT_FST_NM + MUT04O_MT_MKT_MDL_NM + MUT04O_MT_MKT_LST_NM + MUT04O_MT_MKT_SFX + MUT04O_MT_MKT_PFX;
		public static final int MUT04O_MT_CSR_TECHNICAL_KEYS = MUT04O_MT_CSR_CLT_ID;
		public static final int MUT04O_MUT_CSR_INFO = MUT04O_MT_CSR_TECHNICAL_KEYS + MUT04O_MT_CSR_TER_NBR + MUT04O_MT_CSR_DSY_NM
				+ MUT04O_MT_CSR_FST_NM + MUT04O_MT_CSR_MDL_NM + MUT04O_MT_CSR_LST_NM + MUT04O_MT_CSR_SFX + MUT04O_MT_CSR_PFX;
		public static final int MUT04O_MT_SMR_TECHNICAL_KEYS = MUT04O_MT_SMR_CLT_ID;
		public static final int MUT04O_MUT_SMR_INFO = MUT04O_MT_SMR_TECHNICAL_KEYS + MUT04O_MT_SMR_TER_NBR + MUT04O_MT_SMR_DSY_NM
				+ MUT04O_MT_SMR_FST_NM + MUT04O_MT_SMR_MDL_NM + MUT04O_MT_SMR_LST_NM + MUT04O_MT_SMR_SFX + MUT04O_MT_SMR_PFX;
		public static final int MUT04O_MT_DMM_TECHNICAL_KEYS = MUT04O_MT_DMM_CLT_ID;
		public static final int MUT04O_MUT_DMM_INFO = MUT04O_MT_DMM_TECHNICAL_KEYS + MUT04O_MT_DMM_TER_NBR + MUT04O_MT_DMM_DSY_NM
				+ MUT04O_MT_DMM_FST_NM + MUT04O_MT_DMM_MDL_NM + MUT04O_MT_DMM_LST_NM + MUT04O_MT_DMM_SFX + MUT04O_MT_DMM_PFX;
		public static final int MUT04O_MT_RMM_TECHNICAL_KEYS = MUT04O_MT_RMM_CLT_ID;
		public static final int MUT04O_MUT_RMM_INFO = MUT04O_MT_RMM_TECHNICAL_KEYS + MUT04O_MT_RMM_TER_NBR + MUT04O_MT_RMM_DSY_NM
				+ MUT04O_MT_RMM_FST_NM + MUT04O_MT_RMM_MDL_NM + MUT04O_MT_RMM_LST_NM + MUT04O_MT_RMM_SFX + MUT04O_MT_RMM_PFX;
		public static final int MUT04O_MT_DFO_TECHNICAL_KEYS = MUT04O_MT_DFO_CLT_ID;
		public static final int MUT04O_MUT_DFO_INFO = MUT04O_MT_DFO_TECHNICAL_KEYS + MUT04O_MT_DFO_TER_NBR + MUT04O_MT_DFO_DSY_NM
				+ MUT04O_MT_DFO_FST_NM + MUT04O_MT_DFO_MDL_NM + MUT04O_MT_DFO_LST_NM + MUT04O_MT_DFO_SFX + MUT04O_MT_DFO_PFX + FLR3;
		public static final int MUT04O_MUTUAL_MARKETING = MUT04O_MUT_MR_INFO + MUT04O_MUT_CSR_INFO + MUT04O_MUT_SMR_INFO + MUT04O_MUT_DMM_INFO
				+ MUT04O_MUT_RMM_INFO + MUT04O_MUT_DFO_INFO;
		public static final int MUT04O_AT_BRN_TECHNICAL_KEYS = MUT04O_AT_BRN_CLT_ID;
		public static final int MUT04O_AGC_BRN_INFO = MUT04O_AT_BRN_TECHNICAL_KEYS + MUT04O_AT_BRN_TER_NBR + MUT04O_AT_BRN_NM;
		public static final int MUT04O_AT_AGC_TECHNICAL_KEYS = MUT04O_AT_AGC_CLT_ID;
		public static final int MUT04O_AGC_INFO = MUT04O_AT_AGC_TECHNICAL_KEYS + MUT04O_AT_AGC_TER_NBR + MUT04O_AT_AGC_NM;
		public static final int MUT04O_AT_SMR_TECHNICAL_KEYS = MUT04O_AT_SMR_CLT_ID;
		public static final int MUT04O_AGC_SMR_INFO = MUT04O_AT_SMR_TECHNICAL_KEYS + MUT04O_AT_SMR_TER_NBR + MUT04O_AT_SMR_NM;
		public static final int MUT04O_AT_AMM_TECHNICAL_KEYS = MUT04O_AT_AMM_CLT_ID;
		public static final int MUT04O_AGC_AMM_INFO = MUT04O_AT_AMM_TECHNICAL_KEYS + MUT04O_AT_AMM_TER_NBR + MUT04O_AT_AMM_DSY_NM
				+ MUT04O_AT_AMM_FST_NM + MUT04O_AT_AMM_MDL_NM + MUT04O_AT_AMM_LST_NM + MUT04O_AT_AMM_SFX + MUT04O_AT_AMM_PFX;
		public static final int MUT04O_AT_AFM_TECHNICAL_KEYS = MUT04O_AT_AFM_CLT_ID;
		public static final int MUT04O_AGC_AFM_INFO = MUT04O_AT_AFM_TECHNICAL_KEYS + MUT04O_AT_AFM_TER_NBR + MUT04O_AT_AFM_DSY_NM
				+ MUT04O_AT_AFM_FST_NM + MUT04O_AT_AFM_MDL_NM + MUT04O_AT_AFM_LST_NM + MUT04O_AT_AFM_SFX + MUT04O_AT_AFM_PFX;
		public static final int MUT04O_AT_AVP_TECHNICAL_KEYS = MUT04O_AT_AVP_CLT_ID;
		public static final int MUT04O_AGC_AVP_INFO = MUT04O_AT_AVP_TECHNICAL_KEYS + MUT04O_AT_AVP_TER_NBR + MUT04O_AT_AVP_DSY_NM
				+ MUT04O_AT_AVP_FST_NM + MUT04O_AT_AVP_MDL_NM + MUT04O_AT_AVP_LST_NM + MUT04O_AT_AVP_SFX + MUT04O_AT_AVP_PFX + FLR3;
		public static final int MUT04O_AGENCY_MARKETING = MUT04O_AGC_BRN_INFO + MUT04O_AGC_INFO + MUT04O_AGC_SMR_INFO + MUT04O_AGC_AMM_INFO
				+ MUT04O_AGC_AFM_INFO + MUT04O_AGC_AVP_INFO;
		public static final int MUT04O_SERVICING_UW_TYPE = MUT04O_SVC_UW_TYPE_CD + MUT04O_SVC_UW_TYPE_DES;
		public static final int MUT04O_UW_TECHNICAL_KEYS = MUT04O_UW_CLT_ID;
		public static final int MUT04O_UNDERWRITER = MUT04O_UW_TECHNICAL_KEYS + MUT04O_UW_DSY_NM + MUT04O_UW_FST_NM + MUT04O_UW_MDL_NM
				+ MUT04O_UW_LST_NM + MUT04O_UW_SFX + MUT04O_UW_PFX;
		public static final int MUT04O_RSK_ALS_TECHNICAL_KEYS = MUT04O_RSK_ALS_CLT_ID;
		public static final int MUT04O_RISK_ANALYST = MUT04O_RSK_ALS_TECHNICAL_KEYS + MUT04O_RSK_ALS_DSY_NM + MUT04O_RSK_ALS_FST_NM
				+ MUT04O_RSK_ALS_MDL_NM + MUT04O_RSK_ALS_LST_NM + MUT04O_RSK_ALS_SFX + MUT04O_RSK_ALS_PFX;
		public static final int MUT04O_DUM_TECHNICAL_KEYS = MUT04O_DUM_CLT_ID;
		public static final int MUT04O_DISTRICT_UW_MANAGER = MUT04O_DUM_TECHNICAL_KEYS + MUT04O_DUM_DSY_NM + MUT04O_DUM_TER + MUT04O_DUM_FST_NM
				+ MUT04O_DUM_MDL_NM + MUT04O_DUM_LST_NM + MUT04O_DUM_SFX + MUT04O_DUM_PFX;
		public static final int MUT04O_RUM_TECHNICAL_KEYS = MUT04O_RUM_CLT_ID;
		public static final int MUT04O_REGIONAL_UW_MANAGER = MUT04O_RUM_TECHNICAL_KEYS + MUT04O_RUM_DSY_NM + MUT04O_RUM_TER + MUT04O_RUM_FST_NM
				+ MUT04O_RUM_MDL_NM + MUT04O_RUM_LST_NM + MUT04O_RUM_SFX + MUT04O_RUM_PFX;
		public static final int MUT04O_SERVICING_UW_INFO = MUT04O_SVC_UW_DSY_NM + MUT04O_SERVICING_UW_TYPE + MUT04O_UNDERWRITER + MUT04O_RISK_ANALYST
				+ MUT04O_DISTRICT_UW_MANAGER + MUT04O_REGIONAL_UW_MANAGER;
		public static final int MUT04O_FPU_TECHNICAL_KEYS = MUT04O_FPU_CLT_ID;
		public static final int MUT04O_FPU = MUT04O_FPU_TECHNICAL_KEYS + MUT04O_FPU_DSY_NM + MUT04O_FPU_FST_NM + MUT04O_FPU_MDL_NM + MUT04O_FPU_LST_NM
				+ MUT04O_FPU_SFX + MUT04O_FPU_PFX + FLR3;
		public static final int MUT04O_FPU_DUM_TECHNICAL_KEYS = MUT04O_FPU_DUM_CLT_ID;
		public static final int MUT04O_FPU_DISTRICT_MANAGER = MUT04O_FPU_DUM_TECHNICAL_KEYS + MUT04O_FPU_DUM_DSY_NM + MUT04O_FPU_DUM_TER
				+ MUT04O_FPU_DUM_FST_NM + MUT04O_FPU_DUM_MDL_NM + MUT04O_FPU_DUM_LST_NM + MUT04O_FPU_DUM_SFX + MUT04O_FPU_DUM_PFX;
		public static final int MUT04O_FPU_RUM_TECHNICAL_KEYS = MUT04O_FPU_RUM_CLT_ID;
		public static final int MUT04O_FPU_REGIONAL_MANAGER = MUT04O_FPU_RUM_TECHNICAL_KEYS + MUT04O_FPU_RUM_DSY_NM + MUT04O_FPU_RUM_TER
				+ MUT04O_FPU_RUM_FST_NM + MUT04O_FPU_RUM_MDL_NM + MUT04O_FPU_RUM_LST_NM + MUT04O_FPU_RUM_SFX + MUT04O_FPU_RUM_PFX;
		public static final int MUT04O_FIELD_PRODUCTION = MUT04O_FPU + MUT04O_FPU_DISTRICT_MANAGER + MUT04O_FPU_REGIONAL_MANAGER;
		public static final int MUT04O_LP_RSK_ALS_TECH_KEYS = MUT04O_LP_RSK_ALS_CLT_ID;
		public static final int MUT04O_LPRA = MUT04O_LP_RSK_ALS_TECH_KEYS + MUT04O_LP_RSK_ALS_DSY_NM + MUT04O_LP_RSK_ALS_FST_NM
				+ MUT04O_LP_RSK_ALS_MDL_NM + MUT04O_LP_RSK_ALS_LST_NM + MUT04O_LP_RSK_ALS_SFX + MUT04O_LP_RSK_ALS_PFX;
		public static final int MUT04O_LPRA_DUM_TECHNICAL_KEYS = MUT04O_LPRA_DUM_CLT_ID;
		public static final int MUT04O_LPRA_DISTRICT_MANAGER = MUT04O_LPRA_DUM_TECHNICAL_KEYS + MUT04O_LPRA_DUM_DSY_NM + MUT04O_LPRA_DUM_TER
				+ MUT04O_LPRA_DUM_FST_NM + MUT04O_LPRA_DUM_MDL_NM + MUT04O_LPRA_DUM_LST_NM + MUT04O_LPRA_DUM_SFX + MUT04O_LPRA_DUM_PFX;
		public static final int MUT04O_LPRA_RUM_TECHNICAL_KEYS = MUT04O_LPRA_RUM_CLT_ID;
		public static final int MUT04O_LPRA_REGIONAL_MANAGER = MUT04O_LPRA_RUM_TECHNICAL_KEYS + MUT04O_LPRA_RUM_DSY_NM + MUT04O_LPRA_RUM_TER
				+ MUT04O_LPRA_RUM_FST_NM + MUT04O_LPRA_RUM_MDL_NM + MUT04O_LPRA_RUM_LST_NM + MUT04O_LPRA_RUM_SFX + MUT04O_LPRA_RUM_PFX;
		public static final int MUT04O_LP_RISK_ANALYST = MUT04O_LPRA + MUT04O_LPRA_DISTRICT_MANAGER + MUT04O_LPRA_REGIONAL_MANAGER;
		public static final int MUT04O_ACT_LOCATION = MUT04O_AL_STATE_CD + MUT04O_AL_COUNTY_CD + MUT04O_AL_TOWN_CD;
		public static final int MUT04O_OFC_LOC_CD = MUT04O_OL_DIVISION + MUT04O_OL_SUB_DIVISION;
		public static final int FLR10 = 500;
		public static final int MUT004_SERVICE_OUTPUTS = MUT04O_TECHNICAL_KEYS + MUT04O_CLIENT_NAME + MUT04O_COWN_INFO + MUT04O_CLIENT_ADDRESS
				+ MUT04O_CLIENT_PHONE + MUT04O_CLIENT_TAX + MUT04O_LEGAL_ENTITY + MUT04O_TYPE_OF_BUSINESS + MUT04O_SIC + MUT04O_SEGMENT_INFORMATION
				+ MUT04O_CLIENT_ACCOUNT + MUT04O_CST_NBR + MUT04O_MARKETING_INFO + MUT04O_MUTUAL_MARKETING + MUT04O_AGENCY_MARKETING
				+ MUT04O_SERVICING_UW_INFO + MUT04O_FIELD_PRODUCTION + MUT04O_LP_RISK_ANALYST + MUT04O_ACT_LOCATION + MUT04O_NBR_EMPLOYEES
				+ LServiceContractArea.MUT04O_SE_SIC_MAXOCCURS * MUT04O_SE_SIC + MUT04O_OFC_LOC_CD + FLR10;
		public static final int L_SERVICE_CONTRACT_AREA = MUT004_SERVICE_INPUTS + MUT004_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int MUT04I_TK_ADR_SEQ_NBR = 5;
			public static final int MUT04O_BSM_ADR_SEQ_NBR = 5;
			public static final int MUT04O_BSL_ADR_SEQ_NBR = 5;
			public static final int MUT04O_ACT_NM_ID = 5;
			public static final int MUT04O_ACT2_NM_ID = 5;
			public static final int MUT04O_SIN_WC_ACT_NM_ID = 5;
			public static final int MUT04O_SPL_BILL_ACT_NM_ID = 5;
			public static final int MUT04O_PRS_LIN_ACT_NM_ID = 5;
			public static final int MUT04O_NBR_EMPLOYEES = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
