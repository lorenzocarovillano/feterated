/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.federatedinsurance.crs.ws.enums.WsEndOfAssemSw;
import com.federatedinsurance.crs.ws.enums.WsEndOfMessSw;
import com.federatedinsurance.crs.ws.enums.WsEndOfPlaceholderSw;
import com.federatedinsurance.crs.ws.enums.WsPlaceholderFoundSw;

/**Original name: WS-WORK-FIELDS<br>
 * Variable: WS-WORK-FIELDS from program HALRPLAC<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class WsWorkFields {

	//==== PROPERTIES ====
	public static final int ASSEM_STRING_X_MAXOCCURS = 500;
	/**Original name: WS-ASSEM-STRING-X<br>
	 * <pre>                                     OCCURS 100</pre>*/
	private char[] assemStringX = new char[ASSEM_STRING_X_MAXOCCURS];
	//Original name: WS-PLACEHOLDER
	private WsPlaceholder placeholder = new WsPlaceholder();
	//Original name: WS-HALRLEN1-IN-LEN
	private short halrlen1InLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-HALRLEN1-OUT-LEN
	private short halrlen1OutLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-END-OF-MESS-SW
	private WsEndOfMessSw endOfMessSw = new WsEndOfMessSw();
	//Original name: WS-END-OF-ASSEM-SW
	private WsEndOfAssemSw endOfAssemSw = new WsEndOfAssemSw();
	//Original name: WS-PLACEHOLDER-FOUND-SW
	private WsPlaceholderFoundSw placeholderFoundSw = new WsPlaceholderFoundSw();
	//Original name: WS-END-OF-PLACEHOLDER-SW
	private WsEndOfPlaceholderSw endOfPlaceholderSw = new WsEndOfPlaceholderSw();

	//==== CONSTRUCTORS ====
	public WsWorkFields() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int assemStringXIdx = 1; assemStringXIdx <= ASSEM_STRING_X_MAXOCCURS; assemStringXIdx++) {
			setAssemStringX(assemStringXIdx, DefaultValues.CHAR_VAL);
		}
	}

	/**Original name: WS-ASSEM-STRING<br>*/
	public byte[] getAssemStringBytes() {
		byte[] buffer = new byte[Len.ASSEM_STRING];
		return getAssemStringBytes(buffer, 1);
	}

	public byte[] getAssemStringBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= ASSEM_STRING_X_MAXOCCURS; idx++) {
			MarshalByte.writeChar(buffer, position, getAssemStringX(idx));
			position += Types.CHAR_SIZE;
		}
		return buffer;
	}

	public void initAssemStringSpaces() {
		for (int idx = 1; idx <= ASSEM_STRING_X_MAXOCCURS; idx++) {
			assemStringX[idx - 1] = (Types.SPACE_CHAR);
		}
	}

	public void setAssemStringX(int assemStringXIdx, char assemStringX) {
		this.assemStringX[assemStringXIdx - 1] = assemStringX;
	}

	public char getAssemStringX(int assemStringXIdx) {
		return this.assemStringX[assemStringXIdx - 1];
	}

	public void setHalrlen1InLen(short halrlen1InLen) {
		this.halrlen1InLen = halrlen1InLen;
	}

	public void setHalrlen1InLenFromBuffer(byte[] buffer) {
		halrlen1InLen = MarshalByte.readBinaryShort(buffer, 1);
	}

	public short getHalrlen1InLen() {
		return this.halrlen1InLen;
	}

	public String getHalrlen1InLenFormatted() {
		return PicFormatter.display(new PicParams("S9(4)").setUsage(PicUsage.BINARY)).format(getHalrlen1InLen()).toString();
	}

	public void setHalrlen1OutLen(short halrlen1OutLen) {
		this.halrlen1OutLen = halrlen1OutLen;
	}

	public void setHalrlen1OutLenFromBuffer(byte[] buffer) {
		halrlen1OutLen = MarshalByte.readBinaryShort(buffer, 1);
	}

	public short getHalrlen1OutLen() {
		return this.halrlen1OutLen;
	}

	public String getHalrlen1OutLenFormatted() {
		return PicFormatter.display(new PicParams("S9(4)").setUsage(PicUsage.BINARY)).format(getHalrlen1OutLen()).toString();
	}

	public WsEndOfAssemSw getEndOfAssemSw() {
		return endOfAssemSw;
	}

	public WsEndOfMessSw getEndOfMessSw() {
		return endOfMessSw;
	}

	public WsEndOfPlaceholderSw getEndOfPlaceholderSw() {
		return endOfPlaceholderSw;
	}

	public WsPlaceholder getPlaceholder() {
		return placeholder;
	}

	public WsPlaceholderFoundSw getPlaceholderFoundSw() {
		return placeholderFoundSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ASSEM_STRING_X = 1;
		public static final int ASSEM_STRING = WsWorkFields.ASSEM_STRING_X_MAXOCCURS * ASSEM_STRING_X;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
