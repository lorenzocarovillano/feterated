/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: FULL-NAME-IN<br>
 * Variable: FULL-NAME-IN from program CIWBNSRB<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class FullNameIn extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int FLR1_MAXOCCURS = 60;

	//==== CONSTRUCTORS ====
	public FullNameIn() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.FULL_NAME_IN;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "", Len.FULL_NAME_IN);
	}

	public void setFullNameIn(String fullNameIn) {
		writeString(Pos.FULL_NAME_IN, fullNameIn, Len.FULL_NAME_IN);
	}

	public void setFullNameInFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.FULL_NAME_IN, Pos.FULL_NAME_IN);
	}

	/**Original name: FULL-NAME-IN<br>*/
	public String getFullNameIn() {
		return readString(Pos.FULL_NAME_IN, Len.FULL_NAME_IN);
	}

	public byte[] getFullNameInAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.FULL_NAME_IN, Pos.FULL_NAME_IN);
		return buffer;
	}

	/**Original name: NAME-IN-BYTE<br>*/
	public char getNameInByte(int nameInByteIdx) {
		int position = Pos.nameInByte(nameInByteIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int FULL_NAME_IN = 1;
		public static final int FULL_NAME_BYTE = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int flr1(int idx) {
			return FULL_NAME_BYTE + idx * Len.FLR1;
		}

		public static int nameInByte(int idx) {
			return flr1(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int NAME_IN_BYTE = 1;
		public static final int FLR1 = NAME_IN_BYTE;
		public static final int FULL_NAME_IN = 60;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
