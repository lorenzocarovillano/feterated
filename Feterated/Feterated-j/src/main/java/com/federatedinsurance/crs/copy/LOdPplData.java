/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: L-OD-PPL-DATA<br>
 * Variable: L-OD-PPL-DATA from copybook TT008001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class LOdPplData {

	//==== PROPERTIES ====
	//Original name: L-OD-FUL-NM
	private String fulNm = DefaultValues.stringVal(Len.FUL_NM);

	//==== METHODS ====
	public String getFulNm() {
		return this.fulNm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EMP_ID = 6;
		public static final int LST_NM = 30;
		public static final int FST_NM = 15;
		public static final int MDL_NM = 15;
		public static final int SFX_NM = 4;
		public static final int FUL_NM = 67;
		public static final int FUL_ALT_NM = 67;
		public static final int HIRE_DT = 10;
		public static final int TMN_DT = 10;
		public static final int MAL_CD = 10;
		public static final int PHN_WRK_TXT = 18;
		public static final int PHN_VMAL_TXT = 18;
		public static final int ACT_ALN_CD = 2;
		public static final int ACT_ALN_DES = 32;
		public static final int ACT_SVC_CD = 2;
		public static final int ACT_SVC_DES = 32;
		public static final int DPT_CD = 5;
		public static final int DPT_DES = 30;
		public static final int JOB_CD = 9;
		public static final int JOB_DES = 30;
		public static final int LOC_CD = 10;
		public static final int LOC_DES = 30;
		public static final int OFC_CIT_CD = 10;
		public static final int OFC_CIT_DES = 30;
		public static final int PRI_CCTR_ID = 15;
		public static final int PRI_CCTR_DES = 30;
		public static final int SPV_EMP_ID = 6;
		public static final int SPV_FUL_NM = 67;
		public static final int USR_ID = 8;
		public static final int MR_TER_ID = 4;
		public static final int DMM_TER_ID = 4;
		public static final int DMM_EMP_ID = 6;
		public static final int RMM_TER_ID = 4;
		public static final int RMM_EMP_ID = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
