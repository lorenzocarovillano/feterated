/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: O-VPS-REPORT-FILE<br>
 * File: O-VPS-REPORT-FILE from program TS030099<br>
 * Generated as a class for rule FTO.<br>*/
public class OVpsReportFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: VRO-CARRIAGE-CONTROL
	private String vroCarriageControl = DefaultValues.stringVal(Len.VRO_CARRIAGE_CONTROL);
	//Original name: VRO-DATA
	private String vroData = DefaultValues.stringVal(Len.VRO_DATA);

	//==== METHODS ====
	public void setoVpsReportRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		vroCarriageControl = MarshalByte.readFixedString(buffer, position, Len.VRO_CARRIAGE_CONTROL);
		position += Len.VRO_CARRIAGE_CONTROL;
		vroData = MarshalByte.readString(buffer, position, Len.VRO_DATA);
	}

	public byte[] getoVpsReportRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, vroCarriageControl, Len.VRO_CARRIAGE_CONTROL);
		position += Len.VRO_CARRIAGE_CONTROL;
		MarshalByte.writeString(buffer, position, vroData, Len.VRO_DATA);
		return buffer;
	}

	public void setVroCarriageControlFormatted(String vroCarriageControl) {
		this.vroCarriageControl = Trunc.toUnsignedNumeric(vroCarriageControl, Len.VRO_CARRIAGE_CONTROL);
	}

	public short getVroCarriageControl() {
		return NumericDisplay.asShort(this.vroCarriageControl);
	}

	public void setVroData(String vroData) {
		this.vroData = Functions.subString(vroData, Len.VRO_DATA);
	}

	public String getVroData() {
		return this.vroData;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getoVpsReportRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setoVpsReportRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.O_VPS_REPORT_RECORD;
	}

	@Override
	public IBuffer copy() {
		OVpsReportFileTO copyTO = new OVpsReportFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int VRO_CARRIAGE_CONTROL = 1;
		public static final int VRO_DATA = 132;
		public static final int O_VPS_REPORT_RECORD = VRO_CARRIAGE_CONTROL + VRO_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
