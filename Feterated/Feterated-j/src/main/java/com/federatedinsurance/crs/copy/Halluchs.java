/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HALLUCHS<br>
 * Variable: HALLUCHS from copybook HALLUCHS<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Halluchs {

	//==== PROPERTIES ====
	public static final int STRING_X_MAXOCCURS = 150;
	/**Original name: HALOUCHS-LENGTH<br>
	 * <pre>***************************************************************
	 *  COMMAREA FOR HALOUCHS (CHECK SUM ROUTINE)                    *
	 * ***************************************************************
	 *  CHANGE LOG                                                   *
	 *                                                               *
	 *  SI#      DATE     PROG#     DESCRIPTION                      *
	 *  -------- -------- --------- ---------------------------------*
	 *  SAVANNAH 22MAR00  JAF       NEW                              *
	 *  C15425   18JUL01  ARSI600   INCREASE LENGTH OF STRING THAT   *
	 *                              HOLDS THE KEY BEING CHECKED.     *
	 * ***************************************************************</pre>*/
	private short length2 = DefaultValues.BIN_SHORT_VAL;
	/**Original name: HALOUCHS-STRING-X<br>
	 * <pre>      07 HALOUCHS-STRING-X           PIC X  OCCURS 100.</pre>*/
	private char[] stringX = new char[STRING_X_MAXOCCURS];
	//Original name: HALOUCHS-CHECK-SUM
	public String checkSum = DefaultValues.stringVal(Len.CHECK_SUM);

	//==== CONSTRUCTORS ====
	public Halluchs() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int stringXIdx = 1; stringXIdx <= STRING_X_MAXOCCURS; stringXIdx++) {
			setStringX(stringXIdx, DefaultValues.CHAR_VAL);
		}
	}

	public void setLength2(short length2) {
		this.length2 = length2;
	}

	public short getLength2() {
		return this.length2;
	}

	public void setStringFldBytes(byte[] buffer) {
		setStringFldBytes(buffer, 1);
	}

	public void setStringFldBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= STRING_X_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setStringX(idx, MarshalByte.readChar(buffer, position));
				position += Types.CHAR_SIZE;
			} else {
				setStringX(idx, Types.SPACE_CHAR);
				position += Types.CHAR_SIZE;
			}
		}
	}

	public byte[] getStringFldBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= STRING_X_MAXOCCURS; idx++) {
			MarshalByte.writeChar(buffer, position, getStringX(idx));
			position += Types.CHAR_SIZE;
		}
		return buffer;
	}

	public void setStringX(int stringXIdx, char stringX) {
		this.stringX[stringXIdx - 1] = stringX;
	}

	public char getStringX(int stringXIdx) {
		return this.stringX[stringXIdx - 1];
	}

	public void setCheckSum(int checkSum) {
		this.checkSum = NumericDisplay.asString(checkSum, Len.CHECK_SUM);
	}

	public int getCheckSum() {
		return NumericDisplay.asInt(this.checkSum);
	}

	public String getCheckSumFormatted() {
		return this.checkSum;
	}

	public String getCheckSumAsString() {
		return getCheckSumFormatted();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CHECK_SUM = 9;
		public static final int LENGTH2 = 2;
		public static final int STRING_X = 1;
		public static final int STRING_FLD = Halluchs.STRING_X_MAXOCCURS * STRING_X;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
