/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-SCRIPT-PROCESSING<br>
 * Variable: WS-SCRIPT-PROCESSING from program HALOUSDH<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsScriptProcessing {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char NOT_END_OF_SCRIPTCUR = 'N';
	public static final char END_OF_SCRIPTCUR = 'E';

	//==== METHODS ====
	public void setScriptProcessing(char scriptProcessing) {
		this.value = scriptProcessing;
	}

	public char getScriptProcessing() {
		return this.value;
	}

	public void setNotEndOfScriptcur() {
		value = NOT_END_OF_SCRIPTCUR;
	}

	public boolean isEndOfScriptcur() {
		return value == END_OF_SCRIPTCUR;
	}

	public void setEndOfScriptcur() {
		value = END_OF_SCRIPTCUR;
	}
}
