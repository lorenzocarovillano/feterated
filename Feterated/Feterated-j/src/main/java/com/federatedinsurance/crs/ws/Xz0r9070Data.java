/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0R9070<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0r9070Data {

	//==== PROPERTIES ====
	//Original name: CF-BUS-OBJ-NM-ACY-FRM-LIST
	private String cfBusObjNmAcyFrmList = "XZ_GET_ACY_FRM_LIST";
	//Original name: FILLER-CF-INVALID-OPERATION
	private String flr1 = "RESPONSE MODULE";
	//Original name: FILLER-CF-INVALID-OPERATION-1
	private String flr2 = " - INVALID";
	//Original name: FILLER-CF-INVALID-OPERATION-2
	private String flr3 = "OPERATION";
	//Original name: FILLER-EA-02-INVALID-OPERATION-MSG
	private String flr6 = "XZ0R9070 -";
	//Original name: FILLER-EA-02-INVALID-OPERATION-MSG-1
	private String flr7 = "INVALID OPER:";
	//Original name: EA-02-INVALID-OPERATION-NAME
	private String ea02InvalidOperationName = DefaultValues.stringVal(Len.EA02_INVALID_OPERATION_NAME);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0r9070 workingStorageArea = new WorkingStorageAreaXz0r9070();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: IX-RS
	private int ixRs = 1;

	//==== METHODS ====
	public String getCfBusObjNmAcyFrmList() {
		return this.cfBusObjNmAcyFrmList;
	}

	public String getCfInvalidOperationFormatted() {
		return MarshalByteExt.bufferToStr(getCfInvalidOperationBytes());
	}

	/**Original name: CF-INVALID-OPERATION<br>*/
	public byte[] getCfInvalidOperationBytes() {
		byte[] buffer = new byte[Len.CF_INVALID_OPERATION];
		return getCfInvalidOperationBytes(buffer, 1);
	}

	public byte[] getCfInvalidOperationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getEa02InvalidOperationMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidOperationMsgBytes());
	}

	/**Original name: EA-02-INVALID-OPERATION-MSG<br>*/
	public byte[] getEa02InvalidOperationMsgBytes() {
		byte[] buffer = new byte[Len.EA02_INVALID_OPERATION_MSG];
		return getEa02InvalidOperationMsgBytes(buffer, 1);
	}

	public byte[] getEa02InvalidOperationMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, ea02InvalidOperationName, Len.EA02_INVALID_OPERATION_NAME);
		return buffer;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setEa02InvalidOperationName(String ea02InvalidOperationName) {
		this.ea02InvalidOperationName = Functions.subString(ea02InvalidOperationName, Len.EA02_INVALID_OPERATION_NAME);
	}

	public String getEa02InvalidOperationName() {
		return this.ea02InvalidOperationName;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public void setIxRs(int ixRs) {
		this.ixRs = ixRs;
	}

	public int getIxRs() {
		return this.ixRs;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public WorkingStorageAreaXz0r9070 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA01_FAILED_LINK_PGM_NAME = 8;
		public static final int EA02_INVALID_OPERATION_NAME = 32;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int FLR1 = 15;
		public static final int FLR2 = 11;
		public static final int FLR3 = 9;
		public static final int CF_INVALID_OPERATION = FLR1 + FLR2 + FLR3;
		public static final int FLR7 = 14;
		public static final int EA02_INVALID_OPERATION_MSG = EA02_INVALID_OPERATION_NAME + FLR2 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
