/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IFrmRecAtcTyp;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [FRM_REC_ATC_TYP]
 * 
 */
public class FrmRecAtcTypDao extends BaseSqlDao<IFrmRecAtcTyp> {

	private Cursor formAttachCsr;
	private final IRowMapper<IFrmRecAtcTyp> fetchFormAttachCsrRm = buildNamedRowMapper(IFrmRecAtcTyp.class, "frmNbr", "frmEdtDt", "frmSpeAtcCdObj",
			"insdRecInd", "addInsRecInd", "lpeMgeRecInd", "cerRecInd", "aniRecInd");

	public FrmRecAtcTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IFrmRecAtcTyp> getToClass() {
		return IFrmRecAtcTyp.class;
	}

	public short selectRec(String nbr, String edtDt, short dft) {
		return buildQuery("selectRec").bind("nbr", nbr).bind("edtDt", edtDt).scalarResultShort(dft);
	}

	public DbAccessStatus openFormAttachCsr(String actNotTypCd) {
		formAttachCsr = buildQuery("openFormAttachCsr").bind("actNotTypCd", actNotTypCd).open();
		return dbStatus;
	}

	public IFrmRecAtcTyp fetchFormAttachCsr(IFrmRecAtcTyp iFrmRecAtcTyp) {
		return fetch(formAttachCsr, iFrmRecAtcTyp, fetchFormAttachCsrRm);
	}

	public DbAccessStatus closeFormAttachCsr() {
		return closeCursor(formAttachCsr);
	}
}
