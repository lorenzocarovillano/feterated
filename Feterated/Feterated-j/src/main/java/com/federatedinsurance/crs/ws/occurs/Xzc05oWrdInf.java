/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: XZC05O-WRD-INF<br>
 * Variables: XZC05O-WRD-INF from copybook XZC050C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzc05oWrdInf implements ICopyable<Xzc05oWrdInf> {

	//==== PROPERTIES ====
	//Original name: XZC05O-WRD-NM
	private String xzc05oWrdNm = DefaultValues.stringVal(Len.XZC05O_WRD_NM);

	//==== CONSTRUCTORS ====
	public Xzc05oWrdInf() {
	}

	public Xzc05oWrdInf(Xzc05oWrdInf wrdInf) {
		this();
		this.xzc05oWrdNm = wrdInf.xzc05oWrdNm;
	}

	//==== METHODS ====
	public void setWrdInfBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc05oWrdNm = MarshalByte.readString(buffer, position, Len.XZC05O_WRD_NM);
	}

	public byte[] getWrdInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc05oWrdNm, Len.XZC05O_WRD_NM);
		return buffer;
	}

	public void initWrdInfSpaces() {
		xzc05oWrdNm = "";
	}

	public void setXzc05oWrdNm(String xzc05oWrdNm) {
		this.xzc05oWrdNm = Functions.subString(xzc05oWrdNm, Len.XZC05O_WRD_NM);
	}

	public String getXzc05oWrdNm() {
		return this.xzc05oWrdNm;
	}

	@Override
	public Xzc05oWrdInf copy() {
		return new Xzc05oWrdInf(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC05O_WRD_NM = 10;
		public static final int WRD_INF = XZC05O_WRD_NM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
