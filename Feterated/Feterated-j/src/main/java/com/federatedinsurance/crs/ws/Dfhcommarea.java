/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program HALOUIDG<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Dfhcommarea extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: UBOC-COMM-INFO
	private UbocCommInfo commInfo = new UbocCommInfo();
	//Original name: UBOC-APP-DATA-BUFFER-LENGTH
	private String appDataBufferLength = DefaultValues.stringVal(Len.APP_DATA_BUFFER_LENGTH);
	//Original name: UBOC-APP-DATA-BUFFER
	private String appDataBuffer = DefaultValues.stringVal(Len.APP_DATA_BUFFER);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setWsUbocLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		MarshalByte.writeString(buffer, 1, data, Len.DFHCOMMAREA);
		setDfhcommareaBytes(buffer, 1);
	}

	public String getWsHallubocLinkageFormatted() {
		return getRecordFormatted();
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		setRecordBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		getRecordBytes(buffer, position);
		return buffer;
	}

	public String getRecordFormatted() {
		return MarshalByteExt.bufferToStr(getRecordBytes());
	}

	/**Original name: UBOC-RECORD<br>
	 * <pre>*************************************************************
	 *  HALLUBOC                                                   *
	 * *************************************************************
	 *  UOW BUSINESS OBJECT COMMUNICATION COPYBOOK                 *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  SAVANNAH FEB 01 00            CREATED.                     *
	 *  017184   SEP 19 01  07077     ADDED SECURITY GROUP NAME    *
	 *  20072    MAR 28 02  07077     ADDED 88 LVL FOR CONTEXT     *
	 *  32098    MAY 02 03  18448     ADD LOCKING TSQ NAME         *
	 *  32701    JUN 23 03  18448     GROUP LOCKING CAPABILITIES   *
	 *  33817    JUL 31 03  18448     PASS APPLICATION DATE        *
	 *  34103    AUG 11 03  18448     ALLOW USE OF TSQ MAIN ALONG  *
	 *                                WITH UMTS.                   *
	 * *************************************************************</pre>*/
	public byte[] getRecordBytes() {
		byte[] buffer = new byte[Len.RECORD];
		return getRecordBytes(buffer, 1);
	}

	public void setRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		commInfo.setCommInfoBytes(buffer, position);
		position += UbocCommInfo.Len.COMM_INFO;
		setExtraDataBytes(buffer, position);
	}

	public byte[] getRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		commInfo.getCommInfoBytes(buffer, position);
		position += UbocCommInfo.Len.COMM_INFO;
		getExtraDataBytes(buffer, position);
		return buffer;
	}

	public void setExtraDataBytes(byte[] buffer, int offset) {
		int position = offset;
		appDataBufferLength = MarshalByte.readFixedString(buffer, position, Len.APP_DATA_BUFFER_LENGTH);
		position += Len.APP_DATA_BUFFER_LENGTH;
		appDataBuffer = MarshalByte.readString(buffer, position, Len.APP_DATA_BUFFER);
	}

	public byte[] getExtraDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, appDataBufferLength, Len.APP_DATA_BUFFER_LENGTH);
		position += Len.APP_DATA_BUFFER_LENGTH;
		MarshalByte.writeString(buffer, position, appDataBuffer, Len.APP_DATA_BUFFER);
		return buffer;
	}

	public void setAppDataBufferLength(short appDataBufferLength) {
		this.appDataBufferLength = NumericDisplay.asString(appDataBufferLength, Len.APP_DATA_BUFFER_LENGTH);
	}

	public void setAppDataBufferLengthFormatted(String appDataBufferLength) {
		this.appDataBufferLength = Trunc.toUnsignedNumeric(appDataBufferLength, Len.APP_DATA_BUFFER_LENGTH);
	}

	public short getAppDataBufferLength() {
		return NumericDisplay.asShort(this.appDataBufferLength);
	}

	public void setAppDataBuffer(String appDataBuffer) {
		this.appDataBuffer = Functions.subString(appDataBuffer, Len.APP_DATA_BUFFER);
	}

	public String getAppDataBuffer() {
		return this.appDataBuffer;
	}

	public String getAppDataBufferFormatted() {
		return Functions.padBlanks(getAppDataBuffer(), Len.APP_DATA_BUFFER);
	}

	public UbocCommInfo getCommInfo() {
		return commInfo;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int APP_DATA_BUFFER_LENGTH = 4;
		public static final int APP_DATA_BUFFER = 5000;
		public static final int EXTRA_DATA = APP_DATA_BUFFER_LENGTH + APP_DATA_BUFFER;
		public static final int RECORD = UbocCommInfo.Len.COMM_INFO + EXTRA_DATA;
		public static final int DFHCOMMAREA = RECORD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
