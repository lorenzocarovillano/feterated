/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LI-ERR-COL-NAME1<br>
 * Variable: LI-ERR-COL-NAME1 from program HALRPLAC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class LiErrColName1 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: LI-ERR-COL-NAME1
	private String liErrColName1 = "";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.LI_ERR_COL_NAME1;
	}

	@Override
	public void deserialize(byte[] buf) {
		setLiErrColName1FromBuffer(buf);
	}

	public void setLiErrColName1(String liErrColName1) {
		this.liErrColName1 = Functions.subString(liErrColName1, Len.LI_ERR_COL_NAME1);
	}

	public void setLiErrColName1FromBuffer(byte[] buffer, int offset) {
		setLiErrColName1(MarshalByte.readString(buffer, offset, Len.LI_ERR_COL_NAME1));
	}

	public void setLiErrColName1FromBuffer(byte[] buffer) {
		setLiErrColName1FromBuffer(buffer, 1);
	}

	public String getLiErrColName1() {
		return this.liErrColName1;
	}

	public String getLiErrColName1Formatted() {
		return Functions.padBlanks(getLiErrColName1(), Len.LI_ERR_COL_NAME1);
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.strToBuffer(getLiErrColName1(), Len.LI_ERR_COL_NAME1);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LI_ERR_COL_NAME1 = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int LI_ERR_COL_NAME1 = 32;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int LI_ERR_COL_NAME1 = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
