/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: CI-TRANSACTION-SPECIFIC-DATA<br>
 * Variable: CI-TRANSACTION-SPECIFIC-DATA from program XZ003000<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CiTransactionSpecificData extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public CiTransactionSpecificData() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.CI_TRANSACTION_SPECIFIC_DATA;
	}

	public void setCiTransactionSpecificData(String ciTransactionSpecificData) {
		writeString(Pos.CI_TRANSACTION_SPECIFIC_DATA, ciTransactionSpecificData, Len.CI_TRANSACTION_SPECIFIC_DATA);
	}

	public void setCiTransactionSpecificDataFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.CI_TRANSACTION_SPECIFIC_DATA, Pos.CI_TRANSACTION_SPECIFIC_DATA);
	}

	/**Original name: CI-TRANSACTION-SPECIFIC-DATA<br>*/
	public String getCiTransactionSpecificData() {
		return readString(Pos.CI_TRANSACTION_SPECIFIC_DATA, Len.CI_TRANSACTION_SPECIFIC_DATA);
	}

	public byte[] getCiTransactionSpecificDataAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.CI_TRANSACTION_SPECIFIC_DATA, Pos.CI_TRANSACTION_SPECIFIC_DATA);
		return buffer;
	}

	public void setCpCicsPgmNm(String cpCicsPgmNm) {
		writeString(Pos.CP_CICS_PGM_NM, cpCicsPgmNm, Len.CP_CICS_PGM_NM);
	}

	/**Original name: CI-CP-CICS-PGM-NM<br>*/
	public String getCpCicsPgmNm() {
		return readString(Pos.CP_CICS_PGM_NM, Len.CP_CICS_PGM_NM);
	}

	public void setCpTargetTranId(String cpTargetTranId) {
		writeString(Pos.CP_TARGET_TRAN_ID, cpTargetTranId, Len.CP_TARGET_TRAN_ID);
	}

	/**Original name: CI-CP-TARGET-TRAN-ID<br>*/
	public String getCpTargetTranId() {
		return readString(Pos.CP_TARGET_TRAN_ID, Len.CP_TARGET_TRAN_ID);
	}

	public void setCpDataLen(int cpDataLen) {
		writeBinaryInt(Pos.CP_DATA_LEN, cpDataLen);
	}

	/**Original name: CI-CP-DATA-LEN<br>*/
	public int getCpDataLen() {
		return readBinaryInt(Pos.CP_DATA_LEN);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int CI_TRANSACTION_SPECIFIC_DATA = 1;
		public static final int CI_CALL_PGM_PARMS = 1;
		public static final int CP_CICS_PGM_NM = CI_CALL_PGM_PARMS;
		public static final int CP_TARGET_TRAN_ID = CP_CICS_PGM_NM + Len.CP_CICS_PGM_NM;
		public static final int CP_DATA_LEN = CP_TARGET_TRAN_ID + Len.CP_TARGET_TRAN_ID;
		public static final int CI_GET_TOKEN_INF_PARMS = 1;
		public static final int GT_USER_TOKEN = CI_GET_TOKEN_INF_PARMS;
		public static final int GT_PIPE_TOKEN = GT_USER_TOKEN + Len.GT_USER_TOKEN;
		public static final int GT_APPL_ID = GT_PIPE_TOKEN + Len.GT_PIPE_TOKEN;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CP_CICS_PGM_NM = 8;
		public static final int CP_TARGET_TRAN_ID = 4;
		public static final int GT_USER_TOKEN = 4;
		public static final int GT_PIPE_TOKEN = 4;
		public static final int CI_TRANSACTION_SPECIFIC_DATA = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
