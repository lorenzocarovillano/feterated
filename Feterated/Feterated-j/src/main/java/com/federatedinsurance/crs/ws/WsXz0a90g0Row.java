/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90G0-ROW<br>
 * Variable: WS-XZ0A90G0-ROW from program XZ0B90G0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90g0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9G0-MAX-REC-ROWS
	private short maxRecRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA9G0-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA9G0-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA9G0-RECALL-CERT-NBR
	private String recallCertNbr = DefaultValues.stringVal(Len.RECALL_CERT_NBR);
	//Original name: XZA9G0-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90G0_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90g0RowBytes(buf);
	}

	public String getWsXz0a90g0RowFormatted() {
		return getGetCertRecListKeyFormatted();
	}

	public void setWsXz0a90g0RowBytes(byte[] buffer) {
		setWsXz0a90g0RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90g0RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90G0_ROW];
		return getWsXz0a90g0RowBytes(buffer, 1);
	}

	public void setWsXz0a90g0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetCertRecListKeyBytes(buffer, position);
	}

	public byte[] getWsXz0a90g0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetCertRecListKeyBytes(buffer, position);
		return buffer;
	}

	public String getGetCertRecListKeyFormatted() {
		return MarshalByteExt.bufferToStr(getGetCertRecListKeyBytes());
	}

	/**Original name: XZA9G0-GET-CERT-REC-LIST-KEY<br>
	 * <pre>*************************************************************
	 *  XZ0A90G0 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_CERT_RECIPIENT                     *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  27FEB2009 E404KXS    NEW                          *
	 *  PP02500  17OCT2012 E404BPO    CHANGE CERT NUMBER LENGTH    *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetCertRecListKeyBytes() {
		byte[] buffer = new byte[Len.GET_CERT_REC_LIST_KEY];
		return getGetCertRecListKeyBytes(buffer, 1);
	}

	public void setGetCertRecListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		maxRecRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		recallCertNbr = MarshalByte.readString(buffer, position, Len.RECALL_CERT_NBR);
		position += Len.RECALL_CERT_NBR;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
	}

	public byte[] getGetCertRecListKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxRecRows);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, recallCertNbr, Len.RECALL_CERT_NBR);
		position += Len.RECALL_CERT_NBR;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setMaxRecRows(short maxRecRows) {
		this.maxRecRows = maxRecRows;
	}

	public short getMaxRecRows() {
		return this.maxRecRows;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setRecallCertNbr(String recallCertNbr) {
		this.recallCertNbr = Functions.subString(recallCertNbr, Len.RECALL_CERT_NBR);
	}

	public String getRecallCertNbr() {
		return this.recallCertNbr;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90g0RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_REC_ROWS = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int RECALL_CERT_NBR = 25;
		public static final int USERID = 8;
		public static final int GET_CERT_REC_LIST_KEY = MAX_REC_ROWS + CSR_ACT_NBR + NOT_PRC_TS + RECALL_CERT_NBR + USERID;
		public static final int WS_XZ0A90G0_ROW = GET_CERT_REC_LIST_KEY;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
