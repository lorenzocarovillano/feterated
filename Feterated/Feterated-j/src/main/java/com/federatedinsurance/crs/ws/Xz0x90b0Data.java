/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0X90B0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0x90b0Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0x90b0 constantFields = new ConstantFieldsXz0x90b0();
	//Original name: SS-ND
	private short ssNd = ((short) 0);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0x90b0 workingStorageArea = new WorkingStorageAreaXz0x90b0();
	//Original name: MAIN-DRIVER-DATA
	private DfhcommareaTs020000 mainDriverData = new DfhcommareaTs020000();
	//Original name: IX-RS
	private int ixRs = 1;

	//==== METHODS ====
	public void setSsNd(short ssNd) {
		this.ssNd = ssNd;
	}

	public short getSsNd() {
		return this.ssNd;
	}

	public void setIxRs(int ixRs) {
		this.ixRs = ixRs;
	}

	public int getIxRs() {
		return this.ixRs;
	}

	public ConstantFieldsXz0x90b0 getConstantFields() {
		return constantFields;
	}

	public DfhcommareaTs020000 getMainDriverData() {
		return mainDriverData;
	}

	public WorkingStorageAreaXz0x90b0 getWorkingStorageArea() {
		return workingStorageArea;
	}
}
