/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IClientAddressV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [CLIENT_ADDRESS_V]
 * 
 */
public class ClientAddressVDao extends BaseSqlDao<IClientAddressV> {

	public ClientAddressVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IClientAddressV> getToClass() {
		return IClientAddressV.class;
	}

	public long selectByCw03hAdrId(String cw03hAdrId, long dft) {
		return buildQuery("selectByCw03hAdrId").bind("cw03hAdrId", cw03hAdrId).scalarResultLong(dft);
	}
}
