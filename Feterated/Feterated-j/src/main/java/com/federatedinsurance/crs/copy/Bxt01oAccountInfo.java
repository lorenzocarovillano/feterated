/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BXT01O-ACCOUNT-INFO<br>
 * Variable: BXT01O-ACCOUNT-INFO from copybook BX0T0001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Bxt01oAccountInfo {

	//==== PROPERTIES ====
	//Original name: BXT01O-OPEN-BALANCE
	private AfDecimal openBalance = new AfDecimal(DefaultValues.DEC_VAL, 17, 2);
	//Original name: BXT01O-OVERDUE-AMT
	private AfDecimal overdueAmt = new AfDecimal(DefaultValues.DEC_VAL, 17, 2);
	//Original name: BXT01O-WRITEOFF-AMT
	private AfDecimal writeoffAmt = new AfDecimal(DefaultValues.DEC_VAL, 17, 2);
	//Original name: BXT01O-SPLIT-BILL-IND
	private char splitBillInd = DefaultValues.CHAR_VAL;
	//Original name: BXT01O-PCN-IND
	private char pcnInd = DefaultValues.CHAR_VAL;
	//Original name: BXT01O-BANKRUPTCY-IND
	private char bankruptcyInd = DefaultValues.CHAR_VAL;
	//Original name: BXT01O-PRE-COL-IND
	private char preColInd = DefaultValues.CHAR_VAL;
	//Original name: BXT01O-COLLECTIONS-IND
	private char collectionsInd = DefaultValues.CHAR_VAL;
	//Original name: BXT01O-COLLECTION-DT
	private String collectionDt = DefaultValues.stringVal(Len.COLLECTION_DT);
	//Original name: BXT01O-BILLING-METHOD
	private String billingMethod = DefaultValues.stringVal(Len.BILLING_METHOD);
	//Original name: BXT01O-BILLING-METHOD-TXT
	private String billingMethodTxt = DefaultValues.stringVal(Len.BILLING_METHOD_TXT);
	//Original name: BXT01O-INVOICE-DT
	private String invoiceDt = DefaultValues.stringVal(Len.INVOICE_DT);
	//Original name: BXT01O-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: BXT01O-STATUS-CD-TXT
	private String statusCdTxt = DefaultValues.stringVal(Len.STATUS_CD_TXT);
	//Original name: BXT01O-RESUME-DT
	private String resumeDt = DefaultValues.stringVal(Len.RESUME_DT);
	//Original name: BXT01O-BIL-TYP-CD
	private String bilTypCd = DefaultValues.stringVal(Len.BIL_TYP_CD);
	//Original name: BXT01O-SUS-FU-REA-CD
	private String susFuReaCd = DefaultValues.stringVal(Len.SUS_FU_REA_CD);
	//Original name: BXT01O-EFT-IND
	private char eftInd = DefaultValues.CHAR_VAL;
	//Original name: BXT01O-BIL-ACCOUNT-ID
	private String bilAccountId = DefaultValues.stringVal(Len.BIL_ACCOUNT_ID);
	//Original name: FILLER-BXT01O-ACCOUNT-INFO
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setBxt01oAccountInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		openBalance.assign(MarshalByte.readDecimal(buffer, position, Len.Int.OPEN_BALANCE, Len.Fract.OPEN_BALANCE));
		position += Len.OPEN_BALANCE;
		overdueAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.OVERDUE_AMT, Len.Fract.OVERDUE_AMT));
		position += Len.OVERDUE_AMT;
		writeoffAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.WRITEOFF_AMT, Len.Fract.WRITEOFF_AMT));
		position += Len.WRITEOFF_AMT;
		splitBillInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pcnInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		bankruptcyInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		preColInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		collectionsInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		collectionDt = MarshalByte.readString(buffer, position, Len.COLLECTION_DT);
		position += Len.COLLECTION_DT;
		billingMethod = MarshalByte.readString(buffer, position, Len.BILLING_METHOD);
		position += Len.BILLING_METHOD;
		billingMethodTxt = MarshalByte.readString(buffer, position, Len.BILLING_METHOD_TXT);
		position += Len.BILLING_METHOD_TXT;
		invoiceDt = MarshalByte.readString(buffer, position, Len.INVOICE_DT);
		position += Len.INVOICE_DT;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCdTxt = MarshalByte.readString(buffer, position, Len.STATUS_CD_TXT);
		position += Len.STATUS_CD_TXT;
		resumeDt = MarshalByte.readString(buffer, position, Len.RESUME_DT);
		position += Len.RESUME_DT;
		bilTypCd = MarshalByte.readString(buffer, position, Len.BIL_TYP_CD);
		position += Len.BIL_TYP_CD;
		susFuReaCd = MarshalByte.readString(buffer, position, Len.SUS_FU_REA_CD);
		position += Len.SUS_FU_REA_CD;
		eftInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		bilAccountId = MarshalByte.readString(buffer, position, Len.BIL_ACCOUNT_ID);
		position += Len.BIL_ACCOUNT_ID;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getBxt01oAccountInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeDecimal(buffer, position, openBalance.copy());
		position += Len.OPEN_BALANCE;
		MarshalByte.writeDecimal(buffer, position, overdueAmt.copy());
		position += Len.OVERDUE_AMT;
		MarshalByte.writeDecimal(buffer, position, writeoffAmt.copy());
		position += Len.WRITEOFF_AMT;
		MarshalByte.writeChar(buffer, position, splitBillInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, pcnInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, bankruptcyInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, preColInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, collectionsInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, collectionDt, Len.COLLECTION_DT);
		position += Len.COLLECTION_DT;
		MarshalByte.writeString(buffer, position, billingMethod, Len.BILLING_METHOD);
		position += Len.BILLING_METHOD;
		MarshalByte.writeString(buffer, position, billingMethodTxt, Len.BILLING_METHOD_TXT);
		position += Len.BILLING_METHOD_TXT;
		MarshalByte.writeString(buffer, position, invoiceDt, Len.INVOICE_DT);
		position += Len.INVOICE_DT;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, statusCdTxt, Len.STATUS_CD_TXT);
		position += Len.STATUS_CD_TXT;
		MarshalByte.writeString(buffer, position, resumeDt, Len.RESUME_DT);
		position += Len.RESUME_DT;
		MarshalByte.writeString(buffer, position, bilTypCd, Len.BIL_TYP_CD);
		position += Len.BIL_TYP_CD;
		MarshalByte.writeString(buffer, position, susFuReaCd, Len.SUS_FU_REA_CD);
		position += Len.SUS_FU_REA_CD;
		MarshalByte.writeChar(buffer, position, eftInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, bilAccountId, Len.BIL_ACCOUNT_ID);
		position += Len.BIL_ACCOUNT_ID;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setOpenBalance(AfDecimal openBalance) {
		this.openBalance.assign(openBalance);
	}

	public AfDecimal getOpenBalance() {
		return this.openBalance.copy();
	}

	public void setOverdueAmt(AfDecimal overdueAmt) {
		this.overdueAmt.assign(overdueAmt);
	}

	public AfDecimal getOverdueAmt() {
		return this.overdueAmt.copy();
	}

	public void setWriteoffAmt(AfDecimal writeoffAmt) {
		this.writeoffAmt.assign(writeoffAmt);
	}

	public AfDecimal getWriteoffAmt() {
		return this.writeoffAmt.copy();
	}

	public void setSplitBillInd(char splitBillInd) {
		this.splitBillInd = splitBillInd;
	}

	public char getSplitBillInd() {
		return this.splitBillInd;
	}

	public void setPcnInd(char pcnInd) {
		this.pcnInd = pcnInd;
	}

	public char getPcnInd() {
		return this.pcnInd;
	}

	public void setBankruptcyInd(char bankruptcyInd) {
		this.bankruptcyInd = bankruptcyInd;
	}

	public char getBankruptcyInd() {
		return this.bankruptcyInd;
	}

	public void setPreColInd(char preColInd) {
		this.preColInd = preColInd;
	}

	public char getPreColInd() {
		return this.preColInd;
	}

	public void setCollectionsInd(char collectionsInd) {
		this.collectionsInd = collectionsInd;
	}

	public char getCollectionsInd() {
		return this.collectionsInd;
	}

	public void setCollectionDt(String collectionDt) {
		this.collectionDt = Functions.subString(collectionDt, Len.COLLECTION_DT);
	}

	public String getCollectionDt() {
		return this.collectionDt;
	}

	public void setBillingMethod(String billingMethod) {
		this.billingMethod = Functions.subString(billingMethod, Len.BILLING_METHOD);
	}

	public String getBillingMethod() {
		return this.billingMethod;
	}

	public void setBillingMethodTxt(String billingMethodTxt) {
		this.billingMethodTxt = Functions.subString(billingMethodTxt, Len.BILLING_METHOD_TXT);
	}

	public String getBillingMethodTxt() {
		return this.billingMethodTxt;
	}

	public void setInvoiceDt(String invoiceDt) {
		this.invoiceDt = Functions.subString(invoiceDt, Len.INVOICE_DT);
	}

	public String getInvoiceDt() {
		return this.invoiceDt;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setStatusCdTxt(String statusCdTxt) {
		this.statusCdTxt = Functions.subString(statusCdTxt, Len.STATUS_CD_TXT);
	}

	public String getStatusCdTxt() {
		return this.statusCdTxt;
	}

	public void setResumeDt(String resumeDt) {
		this.resumeDt = Functions.subString(resumeDt, Len.RESUME_DT);
	}

	public String getResumeDt() {
		return this.resumeDt;
	}

	public void setBilTypCd(String bilTypCd) {
		this.bilTypCd = Functions.subString(bilTypCd, Len.BIL_TYP_CD);
	}

	public String getBilTypCd() {
		return this.bilTypCd;
	}

	public void setSusFuReaCd(String susFuReaCd) {
		this.susFuReaCd = Functions.subString(susFuReaCd, Len.SUS_FU_REA_CD);
	}

	public String getSusFuReaCd() {
		return this.susFuReaCd;
	}

	public void setEftInd(char eftInd) {
		this.eftInd = eftInd;
	}

	public char getEftInd() {
		return this.eftInd;
	}

	public void setBilAccountId(String bilAccountId) {
		this.bilAccountId = Functions.subString(bilAccountId, Len.BIL_ACCOUNT_ID);
	}

	public String getBilAccountId() {
		return this.bilAccountId;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPEN_BALANCE = 17;
		public static final int OVERDUE_AMT = 17;
		public static final int WRITEOFF_AMT = 17;
		public static final int SPLIT_BILL_IND = 1;
		public static final int PCN_IND = 1;
		public static final int BANKRUPTCY_IND = 1;
		public static final int PRE_COL_IND = 1;
		public static final int COLLECTIONS_IND = 1;
		public static final int COLLECTION_DT = 10;
		public static final int BILLING_METHOD = 4;
		public static final int BILLING_METHOD_TXT = 30;
		public static final int INVOICE_DT = 10;
		public static final int STATUS_CD = 1;
		public static final int STATUS_CD_TXT = 22;
		public static final int RESUME_DT = 10;
		public static final int BIL_TYP_CD = 2;
		public static final int SUS_FU_REA_CD = 3;
		public static final int EFT_IND = 1;
		public static final int BIL_ACCOUNT_ID = 8;
		public static final int FLR1 = 88;
		public static final int BXT01O_ACCOUNT_INFO = OPEN_BALANCE + OVERDUE_AMT + WRITEOFF_AMT + SPLIT_BILL_IND + PCN_IND + BANKRUPTCY_IND
				+ PRE_COL_IND + COLLECTIONS_IND + COLLECTION_DT + BILLING_METHOD + BILLING_METHOD_TXT + INVOICE_DT + STATUS_CD + STATUS_CD_TXT
				+ RESUME_DT + BIL_TYP_CD + SUS_FU_REA_CD + EFT_IND + BIL_ACCOUNT_ID + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int OPEN_BALANCE = 15;
			public static final int OVERDUE_AMT = 15;
			public static final int WRITEOFF_AMT = 15;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int OPEN_BALANCE = 2;
			public static final int OVERDUE_AMT = 2;
			public static final int WRITEOFF_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
