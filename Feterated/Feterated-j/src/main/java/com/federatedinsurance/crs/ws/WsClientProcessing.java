/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-CLIENT-PROCESSING<br>
 * Variable: WS-CLIENT-PROCESSING from program CAWPCORC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsClientProcessing {

	//==== PROPERTIES ====
	//Original name: WS-CUR-LONGNAME-LEN
	private short curLongnameLen = ((short) 0);
	//Original name: WS-TRUNC-LEN
	private short truncLen = ((short) 0);
	//Original name: WS-LONGNAME-DELIM
	private char longnameDelim = '+';
	//Original name: WS-HOLD-UNSTR-DUMMY
	private char holdUnstrDummy = DefaultValues.CHAR_VAL;
	//Original name: WS-HOLD-PREFIX-VERBIAGE
	private String holdPrefixVerbiage = DefaultValues.stringVal(Len.HOLD_PREFIX_VERBIAGE);
	//Original name: WS-HOLD-FIRST-NAME
	private String holdFirstName = DefaultValues.stringVal(Len.HOLD_FIRST_NAME);
	//Original name: WS-HOLD-MIDDLE-NAME
	private String holdMiddleName = DefaultValues.stringVal(Len.HOLD_MIDDLE_NAME);
	//Original name: WS-HOLD-LAST-NAME
	private String holdLastName = DefaultValues.stringVal(Len.HOLD_LAST_NAME);
	//Original name: WS-HOLD-SUFFIX-VERBIAGE
	private String holdSuffixVerbiage = DefaultValues.stringVal(Len.HOLD_SUFFIX_VERBIAGE);
	//Original name: WS-MAX-TRUNC-LEN
	private short maxTruncLen = ((short) 132);
	//Original name: WS-TRUNC-STRING
	private String truncString = DefaultValues.stringVal(Len.TRUNC_STRING);
	//Original name: WS-PRIOR-CLIENT-ID
	private String priorClientId = DefaultValues.stringVal(Len.PRIOR_CLIENT_ID);

	//==== METHODS ====
	public void setCurLongnameLen(short curLongnameLen) {
		this.curLongnameLen = curLongnameLen;
	}

	public short getCurLongnameLen() {
		return this.curLongnameLen;
	}

	public void setTruncLen(short truncLen) {
		this.truncLen = truncLen;
	}

	public short getTruncLen() {
		return this.truncLen;
	}

	public char getLongnameDelim() {
		return this.longnameDelim;
	}

	public void setHoldUnstrDummy(char holdUnstrDummy) {
		this.holdUnstrDummy = holdUnstrDummy;
	}

	public char getHoldUnstrDummy() {
		return this.holdUnstrDummy;
	}

	public void setHoldPrefixVerbiage(String holdPrefixVerbiage) {
		this.holdPrefixVerbiage = Functions.subString(holdPrefixVerbiage, Len.HOLD_PREFIX_VERBIAGE);
	}

	public String getHoldPrefixVerbiage() {
		return this.holdPrefixVerbiage;
	}

	public String getHoldPrefixVerbiageFormatted() {
		return Functions.padBlanks(getHoldPrefixVerbiage(), Len.HOLD_PREFIX_VERBIAGE);
	}

	public void setHoldFirstName(String holdFirstName) {
		this.holdFirstName = Functions.subString(holdFirstName, Len.HOLD_FIRST_NAME);
	}

	public String getHoldFirstName() {
		return this.holdFirstName;
	}

	public String getHoldFirstNameFormatted() {
		return Functions.padBlanks(getHoldFirstName(), Len.HOLD_FIRST_NAME);
	}

	public void setHoldMiddleName(String holdMiddleName) {
		this.holdMiddleName = Functions.subString(holdMiddleName, Len.HOLD_MIDDLE_NAME);
	}

	public String getHoldMiddleName() {
		return this.holdMiddleName;
	}

	public String getHoldMiddleNameFormatted() {
		return Functions.padBlanks(getHoldMiddleName(), Len.HOLD_MIDDLE_NAME);
	}

	public void setHoldLastName(String holdLastName) {
		this.holdLastName = Functions.subString(holdLastName, Len.HOLD_LAST_NAME);
	}

	public String getHoldLastName() {
		return this.holdLastName;
	}

	public String getHoldLastNameFormatted() {
		return Functions.padBlanks(getHoldLastName(), Len.HOLD_LAST_NAME);
	}

	public void setHoldSuffixVerbiage(String holdSuffixVerbiage) {
		this.holdSuffixVerbiage = Functions.subString(holdSuffixVerbiage, Len.HOLD_SUFFIX_VERBIAGE);
	}

	public String getHoldSuffixVerbiage() {
		return this.holdSuffixVerbiage;
	}

	public String getHoldSuffixVerbiageFormatted() {
		return Functions.padBlanks(getHoldSuffixVerbiage(), Len.HOLD_SUFFIX_VERBIAGE);
	}

	public short getMaxTruncLen() {
		return this.maxTruncLen;
	}

	public void setTruncString(String truncString) {
		this.truncString = Functions.subString(truncString, Len.TRUNC_STRING);
	}

	public String getTruncString() {
		return this.truncString;
	}

	public String getTruncStringFormatted() {
		return Functions.padBlanks(getTruncString(), Len.TRUNC_STRING);
	}

	public void setPriorClientId(String priorClientId) {
		this.priorClientId = Functions.subString(priorClientId, Len.PRIOR_CLIENT_ID);
	}

	public String getPriorClientId() {
		return this.priorClientId;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HOLD_PREFIX_VERBIAGE = 15;
		public static final int HOLD_FIRST_NAME = 30;
		public static final int HOLD_MIDDLE_NAME = 30;
		public static final int HOLD_LAST_NAME = 60;
		public static final int HOLD_SUFFIX_VERBIAGE = 15;
		public static final int TRUNC_STRING = 132;
		public static final int PRIOR_CLIENT_ID = 20;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
