/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.occurs.WsIlAutomatedLines;
import com.federatedinsurance.crs.ws.occurs.WsIlManualLines;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P9000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p9000 {

	//==== PROPERTIES ====
	public static final int IL_AUTOMATED_LINES_MAXOCCURS = 10;
	public static final int IL_MANUAL_LINES_MAXOCCURS = 10;
	//Original name: WS-MAX-POLICY-ROWS
	private short maxPolicyRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0P9000";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-EIBRESP-CD
	private short eibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-BO-PRP-CNC-WRD-BPO
	private String boPrpCncWrdBpo = "XZ_PREPARE_CANCELLATION_WRD_BPO";
	//Original name: WS-OPERATIONS-CALLED
	private WsOperationsCalledXz0p9000 operationsCalled = new WsOperationsCalledXz0p9000();
	//Original name: WS-IL-AUTOMATED-LINES
	private WsIlAutomatedLines[] ilAutomatedLines = new WsIlAutomatedLines[IL_AUTOMATED_LINES_MAXOCCURS];
	//Original name: WS-IL-MANUAL-LINES
	private WsIlManualLines[] ilManualLines = new WsIlManualLines[IL_MANUAL_LINES_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public WorkingStorageAreaXz0p9000() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int ilAutomatedLinesIdx = 1; ilAutomatedLinesIdx <= IL_AUTOMATED_LINES_MAXOCCURS; ilAutomatedLinesIdx++) {
			ilAutomatedLines[ilAutomatedLinesIdx - 1] = new WsIlAutomatedLines();
		}
		for (int ilManualLinesIdx = 1; ilManualLinesIdx <= IL_MANUAL_LINES_MAXOCCURS; ilManualLinesIdx++) {
			ilManualLines[ilManualLinesIdx - 1] = new WsIlManualLines();
		}
	}

	public void setMaxPolicyRows(short maxPolicyRows) {
		this.maxPolicyRows = maxPolicyRows;
	}

	public short getMaxPolicyRows() {
		return this.maxPolicyRows;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd = eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public String getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd = eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public String getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public String getBoPrpCncWrdBpo() {
		return this.boPrpCncWrdBpo;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	public WsIlAutomatedLines getIlAutomatedLines(int idx) {
		return ilAutomatedLines[idx - 1];
	}

	public WsIlManualLines getIlManualLines(int idx) {
		return ilManualLines[idx - 1];
	}

	public WsOperationsCalledXz0p9000 getOperationsCalled() {
		return operationsCalled;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int EIBRESP_CD = 4;
		public static final int EIBRESP2_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
