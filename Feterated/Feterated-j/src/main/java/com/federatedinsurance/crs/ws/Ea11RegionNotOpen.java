/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-11-REGION-NOT-OPEN<br>
 * Variable: EA-11-REGION-NOT-OPEN from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea11RegionNotOpen {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-11-REGION-NOT-OPEN
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-11-REGION-NOT-OPEN-1
	private String flr2 = "CICS REGION";
	//Original name: EA-11-CICS-APPL-ID
	private String ea11CicsApplId = DefaultValues.stringVal(Len.EA11_CICS_APPL_ID);
	//Original name: FILLER-EA-11-REGION-NOT-OPEN-2
	private String flr3 = " IS NOT FOUND";
	//Original name: FILLER-EA-11-REGION-NOT-OPEN-3
	private String flr4 = "TO BE OPEN.";
	//Original name: FILLER-EA-11-REGION-NOT-OPEN-4
	private String flr5 = "PLEASE OPEN";
	//Original name: FILLER-EA-11-REGION-NOT-OPEN-5
	private String flr6 = "REGION.";

	//==== METHODS ====
	public String getEa11RegionNotOpenFormatted() {
		return MarshalByteExt.bufferToStr(getEa11RegionNotOpenBytes());
	}

	public byte[] getEa11RegionNotOpenBytes() {
		byte[] buffer = new byte[Len.EA11_REGION_NOT_OPEN];
		return getEa11RegionNotOpenBytes(buffer, 1);
	}

	public byte[] getEa11RegionNotOpenBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, ea11CicsApplId, Len.EA11_CICS_APPL_ID);
		position += Len.EA11_CICS_APPL_ID;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setEa11CicsApplId(String ea11CicsApplId) {
		this.ea11CicsApplId = Functions.subString(ea11CicsApplId, Len.EA11_CICS_APPL_ID);
	}

	public String getEa11CicsApplId() {
		return this.ea11CicsApplId;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA11_CICS_APPL_ID = 8;
		public static final int FLR1 = 11;
		public static final int FLR2 = 12;
		public static final int FLR3 = 14;
		public static final int FLR4 = 13;
		public static final int FLR6 = 7;
		public static final int EA11_REGION_NOT_OPEN = EA11_CICS_APPL_ID + FLR1 + 2 * FLR2 + FLR3 + FLR4 + FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
