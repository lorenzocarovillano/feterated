/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-03-MAX-CERTS-FOUND-MSG<br>
 * Variable: EA-03-MAX-CERTS-FOUND-MSG from program XZ0B9081<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea03MaxCertsFoundMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-03-MAX-CERTS-FOUND-MSG
	private String flr1 = "The maximum of";
	//Original name: FILLER-EA-03-MAX-CERTS-FOUND-MSG-1
	private String flr2 = "2500 certs";
	//Original name: FILLER-EA-03-MAX-CERTS-FOUND-MSG-2
	private String flr3 = "found. Some";
	//Original name: FILLER-EA-03-MAX-CERTS-FOUND-MSG-3
	private String flr4 = "certificate";
	//Original name: FILLER-EA-03-MAX-CERTS-FOUND-MSG-4
	private String flr5 = "holders will";
	//Original name: FILLER-EA-03-MAX-CERTS-FOUND-MSG-5
	private String flr6 = "not be";
	//Original name: FILLER-EA-03-MAX-CERTS-FOUND-MSG-6
	private String flr7 = "notified.";

	//==== METHODS ====
	public String getEa03MaxCertsFoundMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa03MaxCertsFoundMsgBytes());
	}

	public byte[] getEa03MaxCertsFoundMsgBytes() {
		byte[] buffer = new byte[Len.EA03_MAX_CERTS_FOUND_MSG];
		return getEa03MaxCertsFoundMsgBytes(buffer, 1);
	}

	public byte[] getEa03MaxCertsFoundMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 15;
		public static final int FLR2 = 11;
		public static final int FLR3 = 12;
		public static final int FLR5 = 13;
		public static final int FLR6 = 7;
		public static final int FLR7 = 10;
		public static final int EA03_MAX_CERTS_FOUND_MSG = FLR1 + FLR2 + 2 * FLR3 + FLR5 + FLR6 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
