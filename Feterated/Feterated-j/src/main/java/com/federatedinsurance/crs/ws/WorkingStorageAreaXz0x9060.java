/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.redefines.WsCaPtr;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0X9060<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class WorkingStorageAreaXz0x9060 {

	//==== PROPERTIES ====
	//Original name: WS-SERVICE-CONTRACT-ATB
	private WsServiceContractAtb wsServiceContractAtb = new WsServiceContractAtb();
	//Original name: WS-PROXY-CONTRACT-ATB
	private WsServiceContractAtb wsProxyContractAtb = new WsServiceContractAtb();
	//Original name: WS-CA-PTR
	private WsCaPtr wsCaPtr = new WsCaPtr();
	//Original name: WS-RESPONSE-CODE
	private int wsResponseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int wsResponseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-MAX-POL-ROWS
	private short wsMaxPolRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "XZ0X9060";
	//Original name: WS-EIBRESP-DISPLAY
	private String wsEibrespDisplay = DefaultValues.stringVal(Len.WS_EIBRESP_DISPLAY);
	//Original name: WS-EIBRESP2-DISPLAY
	private String wsEibresp2Display = DefaultValues.stringVal(Len.WS_EIBRESP2_DISPLAY);

	//==== METHODS ====
	public void setWsResponseCode(int wsResponseCode) {
		this.wsResponseCode = wsResponseCode;
	}

	public int getWsResponseCode() {
		return this.wsResponseCode;
	}

	public void setWsResponseCode2(int wsResponseCode2) {
		this.wsResponseCode2 = wsResponseCode2;
	}

	public int getWsResponseCode2() {
		return this.wsResponseCode2;
	}

	public void setWsMaxPolRows(short wsMaxPolRows) {
		this.wsMaxPolRows = wsMaxPolRows;
	}

	public short getWsMaxPolRows() {
		return this.wsMaxPolRows;
	}

	public void setWsProgramName(String wsProgramName) {
		this.wsProgramName = Functions.subString(wsProgramName, Len.WS_PROGRAM_NAME);
	}

	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public void setWsEibrespDisplay(long wsEibrespDisplay) {
		this.wsEibrespDisplay = PicFormatter.display("-Z(8)9").format(wsEibrespDisplay).toString();
	}

	public long getWsEibrespDisplay() {
		return PicParser.display("-Z(8)9").parseLong(this.wsEibrespDisplay);
	}

	public String getWsEibrespDisplayFormatted() {
		return this.wsEibrespDisplay;
	}

	public String getWsEibrespDisplayAsString() {
		return getWsEibrespDisplayFormatted();
	}

	public void setWsEibresp2Display(long wsEibresp2Display) {
		this.wsEibresp2Display = PicFormatter.display("-Z(8)9").format(wsEibresp2Display).toString();
	}

	public long getWsEibresp2Display() {
		return PicParser.display("-Z(8)9").parseLong(this.wsEibresp2Display);
	}

	public String getWsEibresp2DisplayFormatted() {
		return this.wsEibresp2Display;
	}

	public String getWsEibresp2DisplayAsString() {
		return getWsEibresp2DisplayFormatted();
	}

	public WsCaPtr getWsCaPtr() {
		return wsCaPtr;
	}

	public WsServiceContractAtb getWsProxyContractAtb() {
		return wsProxyContractAtb;
	}

	public WsServiceContractAtb getWsServiceContractAtb() {
		return wsServiceContractAtb;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_EIBRESP_DISPLAY = 10;
		public static final int WS_EIBRESP2_DISPLAY = 10;
		public static final int WS_PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
