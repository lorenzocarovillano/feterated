/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRecAtcTyp;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [ACT_NOT, FRM_REC_ATC_TYP]
 * 
 */
public class ActNotFrmRecAtcTypDao extends BaseSqlDao<IActNotFrmRecAtcTyp> {

	private final IRowMapper<IActNotFrmRecAtcTyp> selectRecRm = buildNamedRowMapper(IActNotFrmRecAtcTyp.class, "ownCltId", "notTypCd", "ownAdrId");

	public ActNotFrmRecAtcTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotFrmRecAtcTyp> getToClass() {
		return IActNotFrmRecAtcTyp.class;
	}

	public IActNotFrmRecAtcTyp selectRec(String csrActNbr, String notPrcTs, IActNotFrmRecAtcTyp iActNotFrmRecAtcTyp) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).rowMapper(selectRecRm)
				.singleResult(iActNotFrmRecAtcTyp);
	}
}
