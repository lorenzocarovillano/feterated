/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpReturnException;
import com.federatedinsurance.crs.copy.Ts020tbl;
import com.federatedinsurance.crs.ws.DfhcommareaTs020100;
import com.federatedinsurance.crs.ws.WsXz0c0007Layout;
import com.federatedinsurance.crs.ws.Xz0f0007Data;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0F0007<br>
 * <pre>AUTHOR.       DAWN POSSEHL.
 * DATE-WRITTEN. 30 DEC 2008.
 * ***************************************************************
 *   PROGRAM TITLE - COMM SHELL REQUEST/RESPONSE FORMATTER FOR   *
 *                   ACT_NOT_FRM_REC                             *
 *                                                               *
 *   PLATFORM - HOST CICS                                        *
 *                                                               *
 *   PURPOSE -  INSERT A SINGLE ROW ON THE REQUEST UMT, OR       *
 *              RETRIEVE A SINGLE ROW FROM THE RESPONSE UMT FOR  *
 *              ACT_NOT_FRM_REC                                  *
 *              ANY FRAMEWORK REQUEST OR RESPONSE MODULE SHOULD  *
 *              USE THIS PROGRAM.                                *
 *                                                               *
 *   PROGRAM INITIATION - LINKED TO FROM A FRAMEWORK REQUEST     *
 *                        MODULE OR A FRAMEWORK RESPONSE MODULE. *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
 *                         OUTPUT RETURNED VIA DFHCOMMAREA       *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
 *  YJ249    27APR07   E404NEM   STDS CHGS                       *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  ------- ---------- -------   ------------------------------- *
 *  TO07614 12/30/2008 E404DLP   INITIAL PROGRAM                 *
 *                                                               *
 * ***************************************************************</pre>*/
public class Xz0f0007 extends Program {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private Xz0f0007Data ws = new Xz0f0007Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaTs020100 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaTs020100 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0f0007 getInstance() {
		return (Programs.getInstance(Xz0f0007.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: IF TF-REQUEST-FORMATTER-CALL
		//               PERFORM 3000-CREATE-REQUEST-ROW
		//           ELSE
		//               END-IF
		//           END-IF.
		if (ws.getTs020tbl().getTfRequestResponseFlag().isRequestFormatterCall()) {
			// COB_CODE: PERFORM 3000-CREATE-REQUEST-ROW
			createRequestRow();
		} else if (ws.getTs020tbl().getTfRequestResponseFlag().isResponseFormatterCall()) {
			// COB_CODE: IF TF-RESPONSE-FORMATTER-CALL
			//               PERFORM 4000-CREATE-RESPONSE-ROW
			//           END-IF
			// COB_CODE: PERFORM 4000-CREATE-RESPONSE-ROW
			createResponseRow();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PERFORM STARTUP/INITIALIZATION PROCESSING                    *
	 * ***************************************************************
	 *  INITIALIZE ERROR PROCESSING FIELDS</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE ESTO-STORE-INFO
		//                      ESTO-RETURN-INFO.
		initEstoStoreInfo();
		initEstoReturnInfo();
		// RETRIEVE THE DATA PASSED TO THIS MODULE
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER(1:UBOC-APP-DATA-BUFFER-LENGTH)
		//                                       TO TABLE-FORMATTER-DATA.
		ws.setTableFormatterDataFormatted(dfhcommarea.getUbocAppDataBufferFormatted().substring((1) - 1, dfhcommarea.getUbocAppDataBufferLength()));
	}

	/**Original name: 3000-CREATE-REQUEST-ROW_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  CREATE A SINGLE REQUEST ROW ON THE REQUEST UMT.              *
	 *  USES A FRAMEWORK SUPPLIED ROUTINE TO UPDATE THE UMT.         *
	 * ***************************************************************</pre>*/
	private void createRequestRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: MOVE TF-DATA-BUFFER(1:LENGTH OF WS-XZ0C0007-LAYOUT)
		//                                       TO WS-XZ0C0007-LAYOUT.
		ws.getWsXz0c0007Layout().setWsXz0c0007LayoutFormatted(
				ws.getTs020tbl().getTfDataBufferFormatted().substring((1) - 1, WsXz0c0007Layout.Len.WS_XZ0C0007_LAYOUT));
		// COB_CODE: SET HALRURQA-WRITE-FUNC     TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: IF TF-BUSINESS-OBJECT-NM = SPACES
		//             OR
		//              TF-BUSINESS-OBJECT-NM = LOW-VALUES
		//               MOVE WS-BUS-OBJ-NAME    TO HALRURQA-BUS-OBJ-NM
		//           ELSE
		//                                       TO HALRURQA-BUS-OBJ-NM
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getTs020tbl().getTfBusinessObjectNm())
				|| Characters.EQ_LOW.test(ws.getTs020tbl().getTfBusinessObjectNm(), Ts020tbl.Len.TF_BUSINESS_OBJECT_NM)) {
			// COB_CODE: MOVE WS-BUS-OBJ-NAME    TO HALRURQA-BUS-OBJ-NM
			ws.getWsHalrurqaLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjName());
		} else {
			// COB_CODE: MOVE TF-BUSINESS-OBJECT-NM
			//                                   TO HALRURQA-BUS-OBJ-NM
			ws.getWsHalrurqaLinkage().setBusObjNm(ws.getTs020tbl().getTfBusinessObjectNm());
		}
		// COB_CODE: MOVE TF-ACTION-CODE         TO HALRURQA-ACTION-CODE.
		ws.getWsHalrurqaLinkage().setActionCode(ws.getTs020tbl().getTfActionCode().getTfActionCode());
		// COB_CODE: MOVE LENGTH OF WS-XZ0C0007-LAYOUT
		//                                       TO HALRURQA-BUS-OBJ-DATA-LENGTH.
		ws.getWsHalrurqaLinkage().setBusObjDataLength(((short) WsXz0c0007Layout.Len.WS_XZ0C0007_LAYOUT));
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                WS-XZ0C0007-LAYOUT.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), ws.getWsXz0c0007Layout());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 4000-CREATE-RESPONSE-ROW_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  RETRIEVE A SINGLE RESPONSE ROW FROM THE RESPONSE UMT.        *
	 *  USES A FRAMEWORK SUPPLIED ROUTINE TO READ THE UMT.           *
	 * ***************************************************************</pre>*/
	private void createResponseRow() {
		Halrresp halrresp = null;
		// COB_CODE: SET HALRRESP-READ-FUNC      TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrrespReadFunc();
		// COB_CODE: IF TF-BUSINESS-OBJECT-NM = SPACES
		//             OR
		//              TF-BUSINESS-OBJECT-NM = LOW-VALUES
		//               MOVE WS-BUS-OBJ-NAME    TO HALRRESP-BUS-OBJ-NM
		//           ELSE
		//                                       TO HALRRESP-BUS-OBJ-NM
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getTs020tbl().getTfBusinessObjectNm())
				|| Characters.EQ_LOW.test(ws.getTs020tbl().getTfBusinessObjectNm(), Ts020tbl.Len.TF_BUSINESS_OBJECT_NM)) {
			// COB_CODE: MOVE WS-BUS-OBJ-NAME    TO HALRRESP-BUS-OBJ-NM
			ws.getWsHalrrespLinkage().setBusObjNm(ws.getWorkingStorageArea().getBusObjName());
		} else {
			// COB_CODE: MOVE TF-BUSINESS-OBJECT-NM
			//                                   TO HALRRESP-BUS-OBJ-NM
			ws.getWsHalrrespLinkage().setBusObjNm(ws.getTs020tbl().getTfBusinessObjectNm());
		}
		// COB_CODE: MOVE TF-REC-SEQ             TO HALRRESP-REC-SEQ.
		ws.getWsHalrrespLinkage().setRecSeqFormatted(ws.getTs020tbl().getTfRecSeqFormatted());
		// COB_CODE: MOVE LENGTH OF WS-XZ0C0007-LAYOUT
		//                                       TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(((short) WsXz0c0007Layout.Len.WS_XZ0C0007_LAYOUT));
		// COB_CODE: INITIALIZE WS-XZ0C0007-LAYOUT.
		initWsXz0c0007Layout();
		// COB_CODE: CALL HALRRESP-HALRRESP-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRRESP-LINKAGE
		//                WS-XZ0C0007-LAYOUT.
		halrresp = Halrresp.getInstance();
		halrresp.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrrespLinkage(), ws.getWsXz0c0007Layout());
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		// COB_CODE: MOVE HALRRESP-REC-FOUND-SW  TO TF-REC-FOUND-FLAG.
		ws.getTs020tbl().getTfRecFoundFlag().setTfRecFoundFlag(ws.getWsHalrrespLinkage().getRecFoundSw().getRecFoundSw());
		// COB_CODE: IF TF-RECORD-NOT-FOUND
		//               GO TO 4000-EXIT
		//           END-IF.
		if (ws.getTs020tbl().getTfRecFoundFlag().isNotFound()) {
			// COB_CODE: MOVE LENGTH OF TABLE-FORMATTER-DATA
			//                                   TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setUbocAppDataBufferLength(((short) Xz0f0007Data.Len.TABLE_FORMATTER_DATA));
			// COB_CODE: MOVE TABLE-FORMATTER-DATA
			//                                   TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setUbocAppDataBuffer(ws.getTableFormatterDataFormatted());
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		// CURRENTLY NO FIELD NEEDS TO BE TRANSLATED.
		//    PERFORM 4200-TRANSLATE-SUPPORT-VALUE.
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		// COB_CODE: MOVE WS-XZ0C0007-LAYOUT     TO TF-DATA-BUFFER.
		ws.getTs020tbl().setTfDataBuffer(ws.getWsXz0c0007Layout().getWsXz0c0007LayoutFormatted());
		// COB_CODE: MOVE LENGTH OF TABLE-FORMATTER-DATA
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.setUbocAppDataBufferLength(((short) Xz0f0007Data.Len.TABLE_FORMATTER_DATA));
		// COB_CODE: MOVE TABLE-FORMATTER-DATA   TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.setUbocAppDataBuffer(ws.getTableFormatterDataFormatted());
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsXz0c0007Layout() {
		ws.getWsXz0c0007Layout().setActNotFrmRecCsumFormatted("000000000");
		ws.getWsXz0c0007Layout().setCsrActNbrKcre("");
		ws.getWsXz0c0007Layout().setNotPrcTsKcre("");
		ws.getWsXz0c0007Layout().setFrmSeqNbrKcre("");
		ws.getWsXz0c0007Layout().setRecSeqNbrKcre("");
		ws.getWsXz0c0007Layout().setTransProcessDt("");
		ws.getWsXz0c0007Layout().setCsrActNbr("");
		ws.getWsXz0c0007Layout().setNotPrcTs("");
		ws.getWsXz0c0007Layout().setFrmSeqNbrSign(Types.SPACE_CHAR);
		ws.getWsXz0c0007Layout().setFrmSeqNbrFormatted("00000");
		ws.getWsXz0c0007Layout().setRecSeqNbrSign(Types.SPACE_CHAR);
		ws.getWsXz0c0007Layout().setRecSeqNbrFormatted("00000");
		ws.getWsXz0c0007Layout().setCsrActNbrCi(Types.SPACE_CHAR);
		ws.getWsXz0c0007Layout().setNotPrcTsCi(Types.SPACE_CHAR);
		ws.getWsXz0c0007Layout().setFrmSeqNbrCi(Types.SPACE_CHAR);
		ws.getWsXz0c0007Layout().setRecSeqNbrCi(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
