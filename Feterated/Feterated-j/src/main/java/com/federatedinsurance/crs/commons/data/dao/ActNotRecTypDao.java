/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotRecTyp;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [ACT_NOT_REC, REC_TYP]
 * 
 */
public class ActNotRecTypDao extends BaseSqlDao<IActNotRecTyp> {

	private Cursor recipientByNotCsr;
	private final IRowMapper<IActNotRecTyp> fetchRecipientByNotCsrRm = buildNamedRowMapper(IActNotRecTyp.class, "recSeqNbr", "recTypCd", "recNmObj",
			"lin1AdrObj", "lin2AdrObj", "citNmObj", "stAbbObj", "pstCdObj", "cerNbrObj", "recLngDes", "recSrOrdNbr");

	public ActNotRecTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotRecTyp> getToClass() {
		return IActNotRecTyp.class;
	}

	public DbAccessStatus openRecipientByNotCsr(String csrActNbr, String notPrcTs, short recSeqNbr) {
		recipientByNotCsr = buildQuery("openRecipientByNotCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("recSeqNbr", recSeqNbr)
				.open();
		return dbStatus;
	}

	public IActNotRecTyp fetchRecipientByNotCsr(IActNotRecTyp iActNotRecTyp) {
		return fetch(recipientByNotCsr, iActNotRecTyp, fetchRecipientByNotCsrRm);
	}

	public DbAccessStatus closeRecipientByNotCsr() {
		return closeCursor(recipientByNotCsr);
	}
}
