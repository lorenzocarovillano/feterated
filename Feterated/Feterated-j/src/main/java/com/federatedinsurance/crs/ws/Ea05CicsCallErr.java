/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-05-CICS-CALL-ERR<br>
 * Variable: EA-05-CICS-CALL-ERR from program XZ003000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea05CicsCallErr {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-05-CICS-CALL-ERR
	private String flr1 = "XZ003000";
	//Original name: FILLER-EA-05-CICS-CALL-ERR-1
	private String flr2 = "CICS ERROR";
	//Original name: FILLER-EA-05-CICS-CALL-ERR-2
	private String flr3 = " EXCI RESP=";
	//Original name: EA-05-EXCI-RESP
	private String exciResp = DefaultValues.stringVal(Len.EXCI_RESP);
	//Original name: FILLER-EA-05-CICS-CALL-ERR-3
	private String flr4 = " EXCI RESP2=";
	//Original name: EA-05-EXCI-RESP2
	private String exciResp2 = DefaultValues.stringVal(Len.EXCI_RESP2);
	//Original name: FILLER-EA-05-CICS-CALL-ERR-4
	private String flr5 = " EXCI RESP3=";
	//Original name: EA-05-EXCI-RESP3
	private String exciResp3 = DefaultValues.stringVal(Len.EXCI_RESP3);
	//Original name: FILLER-EA-05-CICS-CALL-ERR-5
	private String flr6 = " DPL RESP=";
	//Original name: EA-05-DPL-RESP
	private String dplResp = DefaultValues.stringVal(Len.DPL_RESP);
	//Original name: FILLER-EA-05-CICS-CALL-ERR-6
	private String flr7 = " DPL RESP2=";
	//Original name: EA-05-DPL-RESP2
	private String dplResp2 = DefaultValues.stringVal(Len.DPL_RESP2);
	//Original name: FILLER-EA-05-CICS-CALL-ERR-7
	private String flr8 = ";";

	//==== METHODS ====
	public String getEa05CicsCallErrFormatted() {
		return MarshalByteExt.bufferToStr(getEa05CicsCallErrBytes());
	}

	public byte[] getEa05CicsCallErrBytes() {
		byte[] buffer = new byte[Len.EA05_CICS_CALL_ERR];
		return getEa05CicsCallErrBytes(buffer, 1);
	}

	public byte[] getEa05CicsCallErrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, exciResp, Len.EXCI_RESP);
		position += Len.EXCI_RESP;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, exciResp2, Len.EXCI_RESP2);
		position += Len.EXCI_RESP2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, exciResp3, Len.EXCI_RESP3);
		position += Len.EXCI_RESP3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, dplResp, Len.DPL_RESP);
		position += Len.DPL_RESP;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, dplResp2, Len.DPL_RESP2);
		position += Len.DPL_RESP2;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setExciResp(long exciResp) {
		this.exciResp = PicFormatter.display("Z(9)9").format(exciResp).toString();
	}

	public long getExciResp() {
		return PicParser.display("Z(9)9").parseLong(this.exciResp);
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setExciResp2(long exciResp2) {
		this.exciResp2 = PicFormatter.display("Z(9)9").format(exciResp2).toString();
	}

	public long getExciResp2() {
		return PicParser.display("Z(9)9").parseLong(this.exciResp2);
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setExciResp3(long exciResp3) {
		this.exciResp3 = PicFormatter.display("Z(9)9").format(exciResp3).toString();
	}

	public long getExciResp3() {
		return PicParser.display("Z(9)9").parseLong(this.exciResp3);
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setDplResp(long dplResp) {
		this.dplResp = PicFormatter.display("Z(9)9").format(dplResp).toString();
	}

	public long getDplResp() {
		return PicParser.display("Z(9)9").parseLong(this.dplResp);
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setDplResp2(long dplResp2) {
		this.dplResp2 = PicFormatter.display("Z(9)9").format(dplResp2).toString();
	}

	public long getDplResp2() {
		return PicParser.display("Z(9)9").parseLong(this.dplResp2);
	}

	public String getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EXCI_RESP = 10;
		public static final int EXCI_RESP2 = 10;
		public static final int EXCI_RESP3 = 10;
		public static final int DPL_RESP = 10;
		public static final int DPL_RESP2 = 10;
		public static final int FLR1 = 9;
		public static final int FLR2 = 10;
		public static final int FLR3 = 12;
		public static final int FLR4 = 13;
		public static final int FLR6 = 11;
		public static final int FLR8 = 2;
		public static final int EA05_CICS_CALL_ERR = EXCI_RESP + EXCI_RESP2 + EXCI_RESP3 + DPL_RESP + DPL_RESP2 + FLR1 + FLR2 + 2 * FLR3 + 2 * FLR4
				+ FLR6 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
