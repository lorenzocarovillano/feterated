/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.LFaAddressInputs;
import com.federatedinsurance.crs.copy.LFaFormattedAddressLines;
import com.federatedinsurance.crs.ws.enums.LFaReturnCode;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-FORMAT-ADDRESS-PARMS<br>
 * Variable: L-FORMAT-ADDRESS-PARMS from program TS529099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ts52901 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: L-FA-ADDRESS-INPUTS
	private LFaAddressInputs addressInputs = new LFaAddressInputs();
	//Original name: L-FA-FORMATTED-ADDRESS-LINES
	private LFaFormattedAddressLines formattedAddressLines = new LFaFormattedAddressLines();
	//Original name: L-FA-RETURN-CODE
	private LFaReturnCode returnCode = new LFaReturnCode();
	//Original name: FILLER-L-FORMAT-ADDRESS-PARMS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.TS52901;
	}

	@Override
	public void deserialize(byte[] buf) {
		setTs52901Bytes(buf);
	}

	public String getTs52901Formatted() {
		return MarshalByteExt.bufferToStr(getTs52901Bytes());
	}

	public void setTs52901Bytes(byte[] buffer) {
		setTs52901Bytes(buffer, 1);
	}

	public byte[] getTs52901Bytes() {
		byte[] buffer = new byte[Len.TS52901];
		return getTs52901Bytes(buffer, 1);
	}

	public void setTs52901Bytes(byte[] buffer, int offset) {
		int position = offset;
		addressInputs.setAddressInputsBytes(buffer, position);
		position += LFaAddressInputs.Len.ADDRESS_INPUTS;
		formattedAddressLines.setFormattedAddressLinesBytes(buffer, position);
		position += LFaFormattedAddressLines.Len.FORMATTED_ADDRESS_LINES;
		returnCode.value = MarshalByte.readFixedString(buffer, position, LFaReturnCode.Len.RETURN_CODE);
		position += LFaReturnCode.Len.RETURN_CODE;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getTs52901Bytes(byte[] buffer, int offset) {
		int position = offset;
		addressInputs.getAddressInputsBytes(buffer, position);
		position += LFaAddressInputs.Len.ADDRESS_INPUTS;
		formattedAddressLines.getFormattedAddressLinesBytes(buffer, position);
		position += LFaFormattedAddressLines.Len.FORMATTED_ADDRESS_LINES;
		MarshalByte.writeString(buffer, position, returnCode.value, LFaReturnCode.Len.RETURN_CODE);
		position += LFaReturnCode.Len.RETURN_CODE;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public LFaAddressInputs getAddressInputs() {
		return addressInputs;
	}

	public LFaFormattedAddressLines getFormattedAddressLines() {
		return formattedAddressLines;
	}

	public LFaReturnCode getReturnCode() {
		return returnCode;
	}

	@Override
	public byte[] serialize() {
		return getTs52901Bytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 50;
		public static final int TS52901 = LFaAddressInputs.Len.ADDRESS_INPUTS + LFaFormattedAddressLines.Len.FORMATTED_ADDRESS_LINES
				+ LFaReturnCode.Len.RETURN_CODE + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
