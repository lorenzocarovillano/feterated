/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-MONTH-TABLE<br>
 * Variable: WS-MONTH-TABLE from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsMonthTable extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int ENTRY_MAXOCCURS = 12;

	//==== CONSTRUCTORS ====
	public WsMonthTable() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_MONTH_TABLE;
	}

	@Override
	public void init() {
		int position = 1;
		writeBinaryUnsignedShort(position, 1);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 31);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 2);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 28);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 3);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 31);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 4);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 30);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 5);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 31);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 6);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 30);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 7);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 31);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 8);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 31);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 9);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 30);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 10);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 31);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 11);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 30);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 12);
		position += Types.SHORT_SIZE;
		writeBinaryUnsignedShort(position, 31);
	}

	public void setFebDays(int febDays) {
		writeBinaryUnsignedShort(Pos.FEB_DAYS, febDays);
	}

	/**Original name: WS-MONTH-FEB-DAYS<br>*/
	public int getFebDays() {
		return readBinaryUnsignedShort(Pos.FEB_DAYS);
	}

	/**Original name: WS-MONTH-ID<br>*/
	public int getId(int idIdx) {
		int position = Pos.wsMonthId(idIdx - 1);
		return readBinaryUnsignedShort(position);
	}

	/**Original name: WS-MONTH-DAYS<br>*/
	public int getDays(int daysIdx) {
		int position = Pos.wsMonthDays(daysIdx - 1);
		return readBinaryUnsignedShort(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_MONTH_TABLE = 1;
		public static final int JAN = WS_MONTH_TABLE;
		public static final int JAN_ID = JAN;
		public static final int JAN_DAYS = JAN_ID + Len.JAN_ID;
		public static final int FEB = JAN_DAYS + Len.JAN_DAYS;
		public static final int FEB_ID = FEB;
		public static final int FEB_DAYS = FEB_ID + Len.FEB_ID;
		public static final int MAR = FEB_DAYS + Len.FEB_DAYS;
		public static final int MAR_ID = MAR;
		public static final int MAR_DAYS = MAR_ID + Len.MAR_ID;
		public static final int APR = MAR_DAYS + Len.MAR_DAYS;
		public static final int APR_ID = APR;
		public static final int APR_DAYS = APR_ID + Len.APR_ID;
		public static final int MAY = APR_DAYS + Len.APR_DAYS;
		public static final int MAY_ID = MAY;
		public static final int MAY_DAYS = MAY_ID + Len.MAY_ID;
		public static final int JUN = MAY_DAYS + Len.MAY_DAYS;
		public static final int JUN_ID = JUN;
		public static final int JUN_DAYS = JUN_ID + Len.JUN_ID;
		public static final int JUL = JUN_DAYS + Len.JUN_DAYS;
		public static final int JUL_ID = JUL;
		public static final int JUL_DAYS = JUL_ID + Len.JUL_ID;
		public static final int AUG = JUL_DAYS + Len.JUL_DAYS;
		public static final int AUG_ID = AUG;
		public static final int AUG_DAYS = AUG_ID + Len.AUG_ID;
		public static final int SEP = AUG_DAYS + Len.AUG_DAYS;
		public static final int SEP_ID = SEP;
		public static final int SEP_DAYS = SEP_ID + Len.SEP_ID;
		public static final int OCT = SEP_DAYS + Len.SEP_DAYS;
		public static final int OCT_ID = OCT;
		public static final int OCT_DAYS = OCT_ID + Len.OCT_ID;
		public static final int NOV = OCT_DAYS + Len.OCT_DAYS;
		public static final int NOV_ID = NOV;
		public static final int NOV_DAYS = NOV_ID + Len.NOV_ID;
		public static final int DEC_FLD = NOV_DAYS + Len.NOV_DAYS;
		public static final int DEC_ID = DEC_FLD;
		public static final int DEC_DAYS = DEC_ID + Len.DEC_ID;
		public static final int WS_MONTH_TABLE_RED = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int wsMonthEntry(int idx) {
			return WS_MONTH_TABLE_RED + idx * Len.ENTRY;
		}

		public static int wsMonthId(int idx) {
			return wsMonthEntry(idx);
		}

		public static int wsMonthDays(int idx) {
			return wsMonthId(idx) + Len.ID;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int JAN_ID = 2;
		public static final int JAN_DAYS = 2;
		public static final int FEB_ID = 2;
		public static final int FEB_DAYS = 2;
		public static final int MAR_ID = 2;
		public static final int MAR_DAYS = 2;
		public static final int APR_ID = 2;
		public static final int APR_DAYS = 2;
		public static final int MAY_ID = 2;
		public static final int MAY_DAYS = 2;
		public static final int JUN_ID = 2;
		public static final int JUN_DAYS = 2;
		public static final int JUL_ID = 2;
		public static final int JUL_DAYS = 2;
		public static final int AUG_ID = 2;
		public static final int AUG_DAYS = 2;
		public static final int SEP_ID = 2;
		public static final int SEP_DAYS = 2;
		public static final int OCT_ID = 2;
		public static final int OCT_DAYS = 2;
		public static final int NOV_ID = 2;
		public static final int NOV_DAYS = 2;
		public static final int DEC_ID = 2;
		public static final int ID = 2;
		public static final int DAYS = 2;
		public static final int ENTRY = ID + DAYS;
		public static final int JAN = JAN_ID + JAN_DAYS;
		public static final int FEB = FEB_ID + FEB_DAYS;
		public static final int MAR = MAR_ID + MAR_DAYS;
		public static final int APR = APR_ID + APR_DAYS;
		public static final int MAY = MAY_ID + MAY_DAYS;
		public static final int JUN = JUN_ID + JUN_DAYS;
		public static final int JUL = JUL_ID + JUL_DAYS;
		public static final int AUG = AUG_ID + AUG_DAYS;
		public static final int SEP = SEP_ID + SEP_DAYS;
		public static final int OCT = OCT_ID + OCT_DAYS;
		public static final int NOV = NOV_ID + NOV_DAYS;
		public static final int DEC_DAYS = 2;
		public static final int DEC_FLD = DEC_ID + DEC_DAYS;
		public static final int WS_MONTH_TABLE = JAN + FEB + MAR + APR + MAY + JUN + JUL + AUG + SEP + OCT + NOV + DEC_FLD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
