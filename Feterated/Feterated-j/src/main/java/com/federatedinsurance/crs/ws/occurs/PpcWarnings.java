/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: PPC-WARNINGS<br>
 * Variables: PPC-WARNINGS from copybook TS020PRO<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class PpcWarnings {

	//==== PROPERTIES ====
	//Original name: PPC-WARN-MSG
	private String ppcWarnMsg = DefaultValues.stringVal(Len.PPC_WARN_MSG);

	//==== METHODS ====
	public void setPpcWarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		ppcWarnMsg = MarshalByte.readString(buffer, position, Len.PPC_WARN_MSG);
	}

	public byte[] getPpcWarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ppcWarnMsg, Len.PPC_WARN_MSG);
		return buffer;
	}

	public void initPpcWarningsSpaces() {
		ppcWarnMsg = "";
	}

	public void setPpcWarnMsg(String ppcWarnMsg) {
		this.ppcWarnMsg = Functions.subString(ppcWarnMsg, Len.PPC_WARN_MSG);
	}

	public String getPpcWarnMsg() {
		return this.ppcWarnMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PPC_WARN_MSG = 500;
		public static final int PPC_WARNINGS = PPC_WARN_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
