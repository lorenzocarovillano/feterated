/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-MORE-KEY-REPL-UMT-RECS-IND<br>
 * Variable: WS-MORE-KEY-REPL-UMT-RECS-IND from program HALOUKRP<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMoreKeyReplUmtRecsInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char MORE_KEY_REPL_UMT_RECS = 'N';
	public static final char NO_MORE_KEY_REPL_UMT_RECS = 'Y';

	//==== METHODS ====
	public void setWsMoreKeyReplUmtRecsInd(char wsMoreKeyReplUmtRecsInd) {
		this.value = wsMoreKeyReplUmtRecsInd;
	}

	public char getWsMoreKeyReplUmtRecsInd() {
		return this.value;
	}

	public void setMoreKeyReplUmtRecs() {
		value = MORE_KEY_REPL_UMT_RECS;
	}

	public boolean isNoMoreKeyReplUmtRecs() {
		return value == NO_MORE_KEY_REPL_UMT_RECS;
	}

	public void setNoMoreKeyReplUmtRecs() {
		value = NO_MORE_KEY_REPL_UMT_RECS;
	}
}
