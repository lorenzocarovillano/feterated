/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-22-WF-PARM-MSG<br>
 * Variable: EA-22-WF-PARM-MSG from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea22WfParmMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-22-WF-PARM-MSG
	private String flr1 = " ACT NBR=";
	//Original name: EA-22-ACCOUNT-NBR
	private String accountNbr = DefaultValues.stringVal(Len.ACCOUNT_NBR);
	//Original name: FILLER-EA-22-WF-PARM-MSG-1
	private String flr2 = " CANC DT=";
	//Original name: EA-22-CANCEL-DT
	private String cancelDt = DefaultValues.stringVal(Len.CANCEL_DT);
	//Original name: FILLER-EA-22-WF-PARM-MSG-2
	private String flr3 = " POL(S)=";
	//Original name: EA-22-POLICY-LIST
	private String policyList = DefaultValues.stringVal(Len.POLICY_LIST);

	//==== METHODS ====
	public String getEa22WfParmMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa22WfParmMsgBytes());
	}

	public byte[] getEa22WfParmMsgBytes() {
		byte[] buffer = new byte[Len.EA22_WF_PARM_MSG];
		return getEa22WfParmMsgBytes(buffer, 1);
	}

	public byte[] getEa22WfParmMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, accountNbr, Len.ACCOUNT_NBR);
		position += Len.ACCOUNT_NBR;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, cancelDt, Len.CANCEL_DT);
		position += Len.CANCEL_DT;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, policyList, Len.POLICY_LIST);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setAccountNbr(String accountNbr) {
		this.accountNbr = Functions.subString(accountNbr, Len.ACCOUNT_NBR);
	}

	public String getAccountNbr() {
		return this.accountNbr;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setCancelDt(String cancelDt) {
		this.cancelDt = Functions.subString(cancelDt, Len.CANCEL_DT);
	}

	public String getCancelDt() {
		return this.cancelDt;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setPolicyList(String policyList) {
		this.policyList = Functions.subString(policyList, Len.POLICY_LIST);
	}

	public String getPolicyList() {
		return this.policyList;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACCOUNT_NBR = 9;
		public static final int CANCEL_DT = 10;
		public static final int POLICY_LIST = 80;
		public static final int FLR1 = 10;
		public static final int FLR3 = 9;
		public static final int EA22_WF_PARM_MSG = ACCOUNT_NBR + CANCEL_DT + POLICY_LIST + 2 * FLR1 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
