/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HEMDC-HAL-ERR-LOG-MDRV-DATA<br>
 * Variable: HEMDC-HAL-ERR-LOG-MDRV-DATA from copybook HALLCEMD<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class HemdcHalErrLogMdrvData {

	//==== PROPERTIES ====
	/**Original name: HEMDC-TERMINAL-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: HEMDC-HEMD-MSG-TRF-CD-CI
	private char hemdMsgTrfCdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-MSG-TRF-CD
	private char hemdMsgTrfCd = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-CUR-CHUNK-NBR-CI
	private char hemdCurChunkNbrCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-CUR-CHUNK-NBR-SIGN
	private char hemdCurChunkNbrSign = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-CUR-CHUNK-NBR
	private String hemdCurChunkNbr = DefaultValues.stringVal(Len.HEMD_CUR_CHUNK_NBR);
	//Original name: HEMDC-HEMD-DEL-STG-IND-CI
	private char hemdDelStgIndCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-DEL-STG-IND
	private char hemdDelStgInd = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-DEL-MECH-IND-CI
	private char hemdDelMechIndCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-DEL-MECH-IND
	private char hemdDelMechInd = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-DEL-WNG-IND-CI
	private char hemdDelWngIndCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-DEL-WNG-IND
	private char hemdDelWngInd = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-RET-WNG-IND-CI
	private char hemdRetWngIndCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-RET-WNG-IND
	private char hemdRetWngInd = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-TOT-LEN-QTY-CI
	private char hemdTotLenQtyCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-TOT-LEN-QTY-SIGN
	private char hemdTotLenQtySign = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-TOT-LEN-QTY
	private String hemdTotLenQty = DefaultValues.stringVal(Len.HEMD_TOT_LEN_QTY);
	//Original name: HEMDC-HEMD-BUF-LEN-QTY-CI
	private char hemdBufLenQtyCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-BUF-LEN-QTY-SIGN
	private char hemdBufLenQtySign = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-BUF-LEN-QTY
	private String hemdBufLenQty = DefaultValues.stringVal(Len.HEMD_BUF_LEN_QTY);
	//Original name: HEMDC-HEMD-NBR-CHUNKS-CI
	private char hemdNbrChunksCi = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-NBR-CHUNKS-SIGN
	private char hemdNbrChunksSign = DefaultValues.CHAR_VAL;
	//Original name: HEMDC-HEMD-NBR-CHUNKS
	private String hemdNbrChunks = DefaultValues.stringVal(Len.HEMD_NBR_CHUNKS);

	//==== METHODS ====
	public byte[] getHalErrLogMdrvDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, hemdMsgTrfCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdMsgTrfCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdCurChunkNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdCurChunkNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemdCurChunkNbr, Len.HEMD_CUR_CHUNK_NBR);
		position += Len.HEMD_CUR_CHUNK_NBR;
		MarshalByte.writeChar(buffer, position, hemdDelStgIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdDelStgInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdDelMechIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdDelMechInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdDelWngIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdDelWngInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdRetWngIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdRetWngInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdTotLenQtyCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdTotLenQtySign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemdTotLenQty, Len.HEMD_TOT_LEN_QTY);
		position += Len.HEMD_TOT_LEN_QTY;
		MarshalByte.writeChar(buffer, position, hemdBufLenQtyCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdBufLenQtySign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemdBufLenQty, Len.HEMD_BUF_LEN_QTY);
		position += Len.HEMD_BUF_LEN_QTY;
		MarshalByte.writeChar(buffer, position, hemdNbrChunksCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemdNbrChunksSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemdNbrChunks, Len.HEMD_NBR_CHUNKS);
		return buffer;
	}

	public void initHalErrLogMdrvDataSpaces() {
		terminalIdCi = Types.SPACE_CHAR;
		terminalId = "";
		hemdMsgTrfCdCi = Types.SPACE_CHAR;
		hemdMsgTrfCd = Types.SPACE_CHAR;
		hemdCurChunkNbrCi = Types.SPACE_CHAR;
		hemdCurChunkNbrSign = Types.SPACE_CHAR;
		hemdCurChunkNbr = "";
		hemdDelStgIndCi = Types.SPACE_CHAR;
		hemdDelStgInd = Types.SPACE_CHAR;
		hemdDelMechIndCi = Types.SPACE_CHAR;
		hemdDelMechInd = Types.SPACE_CHAR;
		hemdDelWngIndCi = Types.SPACE_CHAR;
		hemdDelWngInd = Types.SPACE_CHAR;
		hemdRetWngIndCi = Types.SPACE_CHAR;
		hemdRetWngInd = Types.SPACE_CHAR;
		hemdTotLenQtyCi = Types.SPACE_CHAR;
		hemdTotLenQtySign = Types.SPACE_CHAR;
		hemdTotLenQty = "";
		hemdBufLenQtyCi = Types.SPACE_CHAR;
		hemdBufLenQtySign = Types.SPACE_CHAR;
		hemdBufLenQty = "";
		hemdNbrChunksCi = Types.SPACE_CHAR;
		hemdNbrChunksSign = Types.SPACE_CHAR;
		hemdNbrChunks = "";
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public char getHemdMsgTrfCdCi() {
		return this.hemdMsgTrfCdCi;
	}

	public void setHemdMsgTrfCd(char hemdMsgTrfCd) {
		this.hemdMsgTrfCd = hemdMsgTrfCd;
	}

	public char getHemdMsgTrfCd() {
		return this.hemdMsgTrfCd;
	}

	public char getHemdCurChunkNbrCi() {
		return this.hemdCurChunkNbrCi;
	}

	public void setHemdCurChunkNbrSign(char hemdCurChunkNbrSign) {
		this.hemdCurChunkNbrSign = hemdCurChunkNbrSign;
	}

	public char getHemdCurChunkNbrSign() {
		return this.hemdCurChunkNbrSign;
	}

	public void setHemdCurChunkNbrFormatted(String hemdCurChunkNbr) {
		this.hemdCurChunkNbr = Trunc.toUnsignedNumeric(hemdCurChunkNbr, Len.HEMD_CUR_CHUNK_NBR);
	}

	public int getHemdCurChunkNbr() {
		return NumericDisplay.asInt(this.hemdCurChunkNbr);
	}

	public char getHemdDelStgIndCi() {
		return this.hemdDelStgIndCi;
	}

	public void setHemdDelStgInd(char hemdDelStgInd) {
		this.hemdDelStgInd = hemdDelStgInd;
	}

	public char getHemdDelStgInd() {
		return this.hemdDelStgInd;
	}

	public char getHemdDelMechIndCi() {
		return this.hemdDelMechIndCi;
	}

	public char getHemdDelMechInd() {
		return this.hemdDelMechInd;
	}

	public char getHemdDelWngIndCi() {
		return this.hemdDelWngIndCi;
	}

	public char getHemdDelWngInd() {
		return this.hemdDelWngInd;
	}

	public char getHemdRetWngIndCi() {
		return this.hemdRetWngIndCi;
	}

	public void setHemdRetWngInd(char hemdRetWngInd) {
		this.hemdRetWngInd = hemdRetWngInd;
	}

	public char getHemdRetWngInd() {
		return this.hemdRetWngInd;
	}

	public char getHemdTotLenQtyCi() {
		return this.hemdTotLenQtyCi;
	}

	public void setHemdTotLenQtySign(char hemdTotLenQtySign) {
		this.hemdTotLenQtySign = hemdTotLenQtySign;
	}

	public char getHemdTotLenQtySign() {
		return this.hemdTotLenQtySign;
	}

	public void setHemdTotLenQtyFormatted(String hemdTotLenQty) {
		this.hemdTotLenQty = Trunc.toUnsignedNumeric(hemdTotLenQty, Len.HEMD_TOT_LEN_QTY);
	}

	public long getHemdTotLenQty() {
		return NumericDisplay.asLong(this.hemdTotLenQty);
	}

	public char getHemdBufLenQtyCi() {
		return this.hemdBufLenQtyCi;
	}

	public void setHemdBufLenQtySign(char hemdBufLenQtySign) {
		this.hemdBufLenQtySign = hemdBufLenQtySign;
	}

	public char getHemdBufLenQtySign() {
		return this.hemdBufLenQtySign;
	}

	public void setHemdBufLenQtyFormatted(String hemdBufLenQty) {
		this.hemdBufLenQty = Trunc.toUnsignedNumeric(hemdBufLenQty, Len.HEMD_BUF_LEN_QTY);
	}

	public long getHemdBufLenQty() {
		return NumericDisplay.asLong(this.hemdBufLenQty);
	}

	public char getHemdNbrChunksCi() {
		return this.hemdNbrChunksCi;
	}

	public void setHemdNbrChunksSign(char hemdNbrChunksSign) {
		this.hemdNbrChunksSign = hemdNbrChunksSign;
	}

	public char getHemdNbrChunksSign() {
		return this.hemdNbrChunksSign;
	}

	public void setHemdNbrChunksFormatted(String hemdNbrChunks) {
		this.hemdNbrChunks = Trunc.toUnsignedNumeric(hemdNbrChunks, Len.HEMD_NBR_CHUNKS);
	}

	public int getHemdNbrChunks() {
		return NumericDisplay.asInt(this.hemdNbrChunks);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TERMINAL_ID = 8;
		public static final int HEMD_CUR_CHUNK_NBR = 5;
		public static final int HEMD_TOT_LEN_QTY = 10;
		public static final int HEMD_BUF_LEN_QTY = 10;
		public static final int HEMD_NBR_CHUNKS = 5;
		public static final int TERMINAL_ID_CI = 1;
		public static final int HEMD_MSG_TRF_CD_CI = 1;
		public static final int HEMD_MSG_TRF_CD = 1;
		public static final int HEMD_CUR_CHUNK_NBR_CI = 1;
		public static final int HEMD_CUR_CHUNK_NBR_SIGN = 1;
		public static final int HEMD_DEL_STG_IND_CI = 1;
		public static final int HEMD_DEL_STG_IND = 1;
		public static final int HEMD_DEL_MECH_IND_CI = 1;
		public static final int HEMD_DEL_MECH_IND = 1;
		public static final int HEMD_DEL_WNG_IND_CI = 1;
		public static final int HEMD_DEL_WNG_IND = 1;
		public static final int HEMD_RET_WNG_IND_CI = 1;
		public static final int HEMD_RET_WNG_IND = 1;
		public static final int HEMD_TOT_LEN_QTY_CI = 1;
		public static final int HEMD_TOT_LEN_QTY_SIGN = 1;
		public static final int HEMD_BUF_LEN_QTY_CI = 1;
		public static final int HEMD_BUF_LEN_QTY_SIGN = 1;
		public static final int HEMD_NBR_CHUNKS_CI = 1;
		public static final int HEMD_NBR_CHUNKS_SIGN = 1;
		public static final int HAL_ERR_LOG_MDRV_DATA = TERMINAL_ID_CI + TERMINAL_ID + HEMD_MSG_TRF_CD_CI + HEMD_MSG_TRF_CD + HEMD_CUR_CHUNK_NBR_CI
				+ HEMD_CUR_CHUNK_NBR_SIGN + HEMD_CUR_CHUNK_NBR + HEMD_DEL_STG_IND_CI + HEMD_DEL_STG_IND + HEMD_DEL_MECH_IND_CI + HEMD_DEL_MECH_IND
				+ HEMD_DEL_WNG_IND_CI + HEMD_DEL_WNG_IND + HEMD_RET_WNG_IND_CI + HEMD_RET_WNG_IND + HEMD_TOT_LEN_QTY_CI + HEMD_TOT_LEN_QTY_SIGN
				+ HEMD_TOT_LEN_QTY + HEMD_BUF_LEN_QTY_CI + HEMD_BUF_LEN_QTY_SIGN + HEMD_BUF_LEN_QTY + HEMD_NBR_CHUNKS_CI + HEMD_NBR_CHUNKS_SIGN
				+ HEMD_NBR_CHUNKS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
