/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-ERROR-CHECK-INFO<br>
 * Variable: WS-ERROR-CHECK-INFO from program XZ0B9080<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsErrorCheckInfo {

	//==== PROPERTIES ====
	//Original name: WS-EC-MODULE
	private String module = DefaultValues.stringVal(Len.MODULE);
	//Original name: WS-EC-PARAGRAPH
	private String paragraph = DefaultValues.stringVal(Len.PARAGRAPH);
	//Original name: WS-EC-TABLE-OR-FILE
	private String tableOrFile = DefaultValues.stringVal(Len.TABLE_OR_FILE);
	//Original name: WS-EC-COLUMN-OR-FIELD
	private String columnOrField = DefaultValues.stringVal(Len.COLUMN_OR_FIELD);

	//==== METHODS ====
	public void setModule(String module) {
		this.module = Functions.subString(module, Len.MODULE);
	}

	public String getModule() {
		return this.module;
	}

	public void setParagraph(String paragraph) {
		this.paragraph = Functions.subString(paragraph, Len.PARAGRAPH);
	}

	public String getParagraph() {
		return this.paragraph;
	}

	public void setTableOrFile(String tableOrFile) {
		this.tableOrFile = Functions.subString(tableOrFile, Len.TABLE_OR_FILE);
	}

	public String getTableOrFile() {
		return this.tableOrFile;
	}

	public void setColumnOrField(String columnOrField) {
		this.columnOrField = Functions.subString(columnOrField, Len.COLUMN_OR_FIELD);
	}

	public String getColumnOrField() {
		return this.columnOrField;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MODULE = 8;
		public static final int PARAGRAPH = 30;
		public static final int TABLE_OR_FILE = 32;
		public static final int COLUMN_OR_FIELD = 18;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
