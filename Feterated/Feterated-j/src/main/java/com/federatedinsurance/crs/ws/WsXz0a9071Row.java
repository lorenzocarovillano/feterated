/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.copy.Xza971AcyOvlInfo;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9071-ROW<br>
 * Variable: WS-XZ0A9071-ROW from program XZ0B9071<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9071Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA971-MAX-OVL-ROWS
	private short maxOvlRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA971-ACY-OVL-INFO
	private Xza971AcyOvlInfo acyOvlInfo = new Xza971AcyOvlInfo();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9071_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9071RowBytes(buf);
	}

	public String getWsXz0a9071RowFormatted() {
		return getAcyOvlRowFormatted();
	}

	public void setWsXz0a9071RowBytes(byte[] buffer) {
		setWsXz0a9071RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9071RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9071_ROW];
		return getWsXz0a9071RowBytes(buffer, 1);
	}

	public void setWsXz0a9071RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setAcyOvlRowBytes(buffer, position);
	}

	public byte[] getWsXz0a9071RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getAcyOvlRowBytes(buffer, position);
		return buffer;
	}

	public String getAcyOvlRowFormatted() {
		return MarshalByteExt.bufferToStr(getAcyOvlRowBytes());
	}

	/**Original name: XZA971-ACY-OVL-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0A9071 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_ACY_OVL_LIST                       *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  04FEB2009 E404DAP    NEW                          *
	 *  TO07614  22APR2009 E404KXS    ADDED ST-ABB                 *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getAcyOvlRowBytes() {
		byte[] buffer = new byte[Len.ACY_OVL_ROW];
		return getAcyOvlRowBytes(buffer, 1);
	}

	public void setAcyOvlRowBytes(byte[] buffer, int offset) {
		int position = offset;
		maxOvlRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		acyOvlInfo.setAcyOvlInfoBytes(buffer, position);
	}

	public byte[] getAcyOvlRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxOvlRows);
		position += Types.SHORT_SIZE;
		acyOvlInfo.getAcyOvlInfoBytes(buffer, position);
		return buffer;
	}

	public void setMaxOvlRows(short maxOvlRows) {
		this.maxOvlRows = maxOvlRows;
	}

	public short getMaxOvlRows() {
		return this.maxOvlRows;
	}

	public Xza971AcyOvlInfo getAcyOvlInfo() {
		return acyOvlInfo;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9071RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_OVL_ROWS = 2;
		public static final int ACY_OVL_ROW = MAX_OVL_ROWS + Xza971AcyOvlInfo.Len.ACY_OVL_INFO;
		public static final int WS_XZ0A9071_ROW = ACY_OVL_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
